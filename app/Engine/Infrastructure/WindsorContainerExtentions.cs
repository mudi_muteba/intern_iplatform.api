﻿using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;

namespace Engine.Infrastructure
{
    public static class WindsorContainerExtentions
    {
        public static void SetLifeCycle<T>(this IWindsorContainer container, ComponentRegistration<T> registration)
            where T : class
        {
            container.Register(registration.LifestylePerThread());
        }

        public static void Install<TType>(this IWindsorContainer container, IRegistration registration)
        {
            if (!container.Kernel.HasComponent(typeof(TType)))
            {
                container.Register(registration);
            }
        }
    }
}
