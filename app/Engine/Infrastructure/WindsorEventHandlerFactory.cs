﻿using System.Collections.Generic;
using System.Linq;
using Castle.Windsor;
using Domain.Base.Events;

namespace Engine.Infrastructure
{
    public class WindsorEventHandlerFactory : ICreateEventHandlers
    {
        private readonly IWindsorContainer container;

        public WindsorEventHandlerFactory(IWindsorContainer container)
        {
            this.container = container;
        }

        public IEnumerable<IHandleEvent> Create<TEvent>(TEvent @event) where TEvent : IDomainEvent
        {
            var handlerType = typeof (IHandleEvent<>).MakeGenericType(@event.GetType());

            var handlers = container.ResolveAll(handlerType);

            var realHandlers = handlers.Cast<IHandleEvent>().ToList();

            return realHandlers;
        }

        public void Release(IHandleEvent handler)
        {
            container.Release(handler);
        }
    }
}