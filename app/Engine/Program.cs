﻿using System;
using Microsoft.Owin.Hosting;
using Shared.Extentions;
using Workflow.Infrastructure;

namespace Engine
{
    class Program
    {
        private static IDisposable _webApp;

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            new Workflow.Engine.Program { BeforeStart = () => Start(), BeforeStop = () => Stop() }.Main();
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;
            if (ex == null)
                typeof(Program).Error(() => "UnhandledException error");
            else
                typeof(Program).Error(() => "Error starting hangfire webapp", ex);
            Stop();
        }

        static void Start()
        {
            try
            {
                var url = string.Format("http://{0}", new WorkflowEngineConfiguration().ServiceName);
                _webApp = WebApp.Start<Startup>(url);
                typeof(Program).Info(() => string.Format("Hangfire can be accessed at: {0}", url + "/hangfire"));
            }
            catch (Exception ex)
            {
                typeof(Program).Error(() => "Error starting hangfire webapp", ex);
                Stop();
            }
        }

        static void Stop()
        {
            typeof(Program).Info(() => "Disposing WebApp...");
            if (_webApp != null) _webApp.Dispose();
        }
    }
}