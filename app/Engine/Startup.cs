﻿using System;
using Engine;
using Hangfire;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace Engine
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var conn = "iBrokerConnectionString";
#if DEBUG
            conn = string.Format("{0}-{1}", Environment.MachineName, "iBrokerConnectionString");
#endif
            GlobalConfiguration.Configuration.UseSqlServerStorage(conn);
            app.UseHangfireServer();
            app.UseHangfireDashboard();
        }
    }
}