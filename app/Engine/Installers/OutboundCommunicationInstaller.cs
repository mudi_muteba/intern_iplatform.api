﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using iPlatform.OutboundCommunication.SMS;
using iPlatform.OutboundCommunication.SMS.CellFind;
using iPlatform.OutboundCommunication.za.co.cellfind.cfwinqa;

namespace Engine.Installers
{
    public class OutboundCommunicationInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<ICellFindClient>().ImplementedBy<IMPGWS>().LifestyleTransient());
            container.Register(Component.For<CellFindSender>().ImplementedBy<CellFindSender>().LifestyleTransient());
            container.Register(Component.For<ICreateSMSSenders>().ImplementedBy<SMSSendingFactory>().LifestyleTransient());
        }
    }
}