using System;
using Domain;
using Domain.Admin;
using Domain.Base;
using Domain.Party.Quotes;
using Domain.Users;
using FluentNHibernate.Automapping;
using FluentNHibernate.Conventions.Helpers;
using iPlatform.UserTypes.Overrides;
using Infrastructure.NHibernate.Attributes;
using Infrastructure.NHibernate.Convensions;

namespace Engine.Installers
{
    public class EntityConfiguration : DefaultAutomappingConfiguration
    {
        public override bool ShouldMap(Type type)
        {
            var entityType = typeof(Entity);

            if (type.IsAbstract && type != entityType)
                return false;

            return entityType.IsAssignableFrom(type);
        }

        public override bool IsComponent(Type type)
        {
            return type == typeof(UserTokens) 
                   || type == typeof(QuoteAcceptance) 
                   || type == typeof(UserStatus) 
                   || type == typeof(UserLogin) 
                   || type == typeof(EntityAudit);
        }

        public AutoPersistenceModel GetAutoPersistenceModel()
        {
            return AutoMap.AssemblyOf<User>(this)
                .IncludeBase<ChannelReference>()
                .Conventions.AddFromAssemblyOf<TableNameConvention>()
                .Conventions.AddFromAssemblyOf<AccessoryTypeConvention>()
                .Conventions.Add
                (
                    ForeignKey.EndsWith("Id")
                )
                .OverrideAll(x =>
                {
                    x.IgnoreProperty("Audit");
                    x.IgnoreProperty("SimpleAudit");
                    x.IgnoreProperties(member => member.MemberInfo.GetCustomAttributes(typeof(DoNotMapAttribute), false).Length > 0);
                })
                .UseOverridesFromAssemblyOf<DomainMarker>();
        }
    }
}