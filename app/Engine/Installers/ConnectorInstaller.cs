using System.Configuration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums;
using Shared.Extentions;

namespace Engine.Installers
{
    public class ConnectorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            this.Info(() => "Registering Connector");
            container.Register(Component.For<IConnector>().ImplementedBy<Connector>().OnCreate(x =>
            {
                var userName = ConfigurationManager.AppSettings[@"iPlatform/connector/userName"];
                var userPassword = ConfigurationManager.AppSettings[@"iPlatform/connector/apiKey"];
                var token = x.Authentication.Authenticate(userName, userPassword, "local", "", ApiRequestType.Workflow);
                x.SetToken(token.Token);
            }).LifestyleTransient());
        }
    }
}