﻿using System.Linq;
using AutoMapper;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Base;
using Domain.Base.Mapping;
using Domain.Party.Quotes.Mapping;
using Common.Logging;
using Engine.Infrastructure;

namespace Engine.Installers
{
    public class AutoMapperInstaller : IWindsorInstaller
    {
        private static readonly ILog log = LogManager.GetLogger<AutoMapperInstaller>();

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining<SMSQuoteMessageConverter>().BasedOn(typeof(ITypeConverter<,>)).WithServiceAllInterfaces().LifestyleTransient());

            RegisterIdToEntityConverters(container);

            Mapper.Configuration.ConstructServicesUsing(type =>
            {
                var resolve = container.Resolve(type);
                container.Release(resolve);
                return resolve;
            });

            log.InfoFormat("Registering Automapper");

            AutoMapperConfiguration.Configure();
        }

        private static void RegisterIdToEntityConverters(IWindsorContainer container)
        {
            var entityTypes = typeof (Entity).Assembly.GetTypes().Where(type => type.IsSubclassOf(typeof (Entity)));
            foreach (var entityType in entityTypes)
            {
                var generic = typeof (IdToEntityConverter<>);
                var constructed = generic.MakeGenericType(entityType);

                var typeConverterType = typeof (ITypeConverter<,>);
                var typeConverterGenericType = typeConverterType.MakeGenericType(typeof (int), entityType);
                container.Register(Component.For(typeConverterGenericType).ImplementedBy(constructed).LifestyleTransient());
            }
        }
    }
}