﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Common.Logging;
using Domain;
using Workflow.Domain;
using Workflow.Messages;

namespace Engine.Installers
{
    public class ConsumerInstaller : IWindsorInstaller
    {
        private static readonly ILog Log = LogManager.GetLogger<ConsumerInstaller>();

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            DynamicallyRegister(container);
        }

        private static void DynamicallyRegister(IWindsorContainer container)
        {
            var assemblies = new List<Assembly>
            {
                typeof(DomainMarker).Assembly,
                typeof(Workflow.QuoteAcceptance.Domain.DomainMarker).Assembly,
                typeof(Workflow.PolicyBinding.Domain.DomainMarker).Assembly
            };

            var selectedMessageType = typeof(IWorkflowExecutionMessage);
            var consumerType = typeof(IConsumeMessages);

            foreach (var assembly in assemblies)
            {
                var types = assembly.GetTypes().Where(t => t.IsClass && !t.IsAbstract).ToList();
                foreach (var type in types.Where(consumerType.IsAssignableFrom))
                {
                    var interfaces = type.GetInterfaces().Where(i => i.IsGenericType);
                    foreach (var @interface in interfaces)
                    {
                        var messageType = @interface.GetGenericArguments();
                        foreach (var singleMessageType in messageType)
                        {
                            if (selectedMessageType.IsAssignableFrom(singleMessageType))
                            {
                                var currentConsumerType = typeof(IConsumeMessages<>).MakeGenericType(singleMessageType);

                                Log.InfoFormat("ENGINE - Registering {0} as a workflow engine message consumer handling {1}", type,
                                    singleMessageType);

                                if(!container.Kernel.HasComponent(currentConsumerType))
                                {
                                    container.Register(Component.For(currentConsumerType)
                                        .ImplementedBy(type)
                                        .LifestylePerThread()
                                        .Named(type.FullName));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}