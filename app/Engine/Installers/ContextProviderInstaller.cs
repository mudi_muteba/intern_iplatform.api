using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Base.Execution;
using Engine.Infrastructure;

namespace Engine.Installers
{
    public class ContextProviderInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.SetLifeCycle(Component.For<IProvideContext>()
                .ImplementedBy<DefaultContextProvider>());
        }
    }
}