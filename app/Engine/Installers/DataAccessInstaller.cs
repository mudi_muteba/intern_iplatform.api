﻿using System;
using System.IO;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Base.Repository;
using Engine.Infrastructure;
using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.Cfg;
using Environment = System.Environment;

namespace Engine.Installers
{
    public class DataAccessInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.SetLifeCycle(Component.For<Configuration>().UsingFactoryMethod(() =>
            {
                var configure = Configure();
                return configure;
            }).LifestyleTransient());

            container.SetLifeCycle(Component.For<IRepository>()
                .ImplementedBy<NHibernateRepository>());

            container.Register(Component.For<ISessionFactory>()
                .UsingFactoryMethod(kernel =>
                {
                    var configuration = container.Resolve<Configuration>();
                    container.Release(configuration);
                    return configuration.BuildSessionFactory();
                })
                .LifestyleSingleton());

            container.SetLifeCycle(Component.For<ISession>()
                .UsingFactoryMethod(kernel =>
                {
                    var sessionFactory = kernel.Resolve<ISessionFactory>();
                    container.Release(sessionFactory);
                    return sessionFactory.OpenSession();
                })
                .OnDestroy(s =>
                {
                    s.Close();
                    s.Dispose();
                }));

            container.SetLifeCycle(Component.For<IPaginateResults>().ImplementedBy<NHibernatePaginator>());
        }

        public static Configuration Configure()
        {
            var configurationFileLocation = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration/hibernate.cfg.xml");
            var configuration = new Configuration().Configure(configurationFileLocation);

			var storedProcedureDefinitionsLocation = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration/mapping.hbm.xml");
            configuration.AddFile(storedProcedureDefinitionsLocation);
#if DEBUG
            var key = string.Format("{0}-{1}", Environment.MachineName, "iBrokerConnectionString");
            configuration.SetProperty("connection.connection_string_name", key);
#endif

            var fluentConfiguration = Fluently.Configure(configuration)
                .Mappings(cfg =>
                {
                    cfg.AutoMappings.Add(new EntityConfiguration().GetAutoPersistenceModel());
                    cfg.AutoMappings.Add(new MasterDataConfiguration().GetAutoPersistenceModel());
                })
                .BuildConfiguration();

            return fluentConfiguration;
        }
    }
}