using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Base.Workflow;
using Workflow.Domain;

namespace Engine.Installers
{
    public class RetryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IWorkflowRetryMessageHandler>().ImplementedBy<WorkflowRetryMessageHandler>().LifestyleTransient());
        }
    }
}