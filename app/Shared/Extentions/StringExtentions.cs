﻿using System;
using System.Linq;

namespace Shared.Extentions
{
    public static class StringExtentions
    {
        public static bool ContainsAny(this string haystack, params string[] needles)
        {
            foreach (string needle in needles)
            {
                if (haystack.Contains(needle))
                    return true;
            }

            return false;
        }
        public static string AddExtToNumber(this string @this)
        {
            if (string.IsNullOrEmpty(@this) || @this[0] != '0')
                return @this;

            @this = @this.Remove(0, 1);
            @this = String.Format("{0}{1}",27,@this);
            return @this;
        }
        
        public static string RemoveAllSpaces(this string @this)
        {
            return string.IsNullOrEmpty(@this) ? @this : @this.Replace(" ", string.Empty);
        }

        public static bool EqualsIgnoreCase(this string @this, string other)
        {
            return string.Equals(@this, other, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string RandomString(this string @this, int length = 8)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }
    }
}