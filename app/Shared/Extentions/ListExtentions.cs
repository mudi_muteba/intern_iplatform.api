using System;
using System.Collections.Generic;

namespace Shared.Extentions
{
    public static class ListExtentions
    {
        public static string FlattenListToString<T>(this List<T> list, Func<T, string> value, string start = "", string end = "")
        {
            var flatten = start;

            if (list == null)
                return string.Empty;

            foreach (var item in list)
            {
                flatten += string.Format("{0}, ", value(item));
            }

            if (!string.IsNullOrWhiteSpace(flatten))
            {
                return flatten.Substring(0, flatten.Length - 2);
            }

            return flatten + end;
        }
    }
}