﻿using System;
using System.Configuration;
using log4net.Appender;

namespace Shared.Logging
{
    public class MssqlAppender : AdoNetAppender
    {
        public new string ConnectionString
        {
            get { return base.ConnectionString; }
            set
            {
                var conn = "";
#if DEBUG
                conn = string.Format("{0}-{1}", Environment.MachineName, "iBrokerConnectionString");
#endif
                base.ConnectionString = ConfigurationManager.ConnectionStrings[conn].ConnectionString;
            }
        }
    }

}