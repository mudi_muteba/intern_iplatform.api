﻿using System;

namespace Shared
{
    public class ExceptionPrettyPrinter
    {
        public string Print(Exception e)
        {
            var message = string.Format("{0}{1}{2}{1}", e.Message, Environment.NewLine, e.StackTrace);
            var currentException = e.InnerException;
            const int maxLevels = 3;
            var currentLevel = 1;

            while (currentException != null && currentLevel <= maxLevels)
            {
                message += string.Format("{0}{1}{2}{1}", currentException.Message, Environment.NewLine,
                    currentException.StackTrace);

                currentException = currentException.InnerException;
                currentLevel++;
            }

            return message;
        }
    }
}