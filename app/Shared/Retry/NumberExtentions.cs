﻿using System;

namespace Shared.Retry
{
    public static class NumberExtensions
    {
        public static TimeSpan Seconds(this int value)
        {
            var fromSeconds = TimeSpan.FromSeconds(value);

            return fromSeconds;
        }

        public static RepeatValue Times(this int value)
        {
            return new RepeatValue(value);
        }
    }
}