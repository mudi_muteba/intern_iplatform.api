﻿using System;
using System.Threading;
using Common.Logging;

namespace Shared.Retry
{
    public class RepeatAction
    {
        private readonly Func<bool> condition;
        private TimeSpan initialDelay = new TimeSpan(0);
        private TimeSpan incrementDelay = new TimeSpan(0);
        private int maxAttempts;
        private TimeSpan retry;
        private static readonly ILog log = LogManager.GetLogger<RepeatAction>();

        public RepeatAction()
        {
            retry = new TimeSpan(0, 0, 0, 5);
            maxAttempts = 5;
        }

        private RepeatAction(Func<bool> condition)
        {
            this.condition = condition;
            retry = new TimeSpan(0, 0, 0, 5);
            maxAttempts = 5;
        }

        public static TimeSpan GetWaitTime()
        {
            return GetWaitTime(1, 5);
        }

        public static TimeSpan GetWaitTime(int minimumSeconds, int maximumSeconds)
        {
            return GenerateRandomDelay(minimumSeconds, maximumSeconds);
        }

        private static TimeSpan GenerateRandomDelay(int minimumSeconds, int maximumSeconds)
        {
            var random = new Random(DateTime.UtcNow.Millisecond);

            var waitTime = random.Next(minimumSeconds, maximumSeconds).Seconds();
#if DEBUG
            waitTime = 0.Seconds();
#endif

            return waitTime;
        }

        public static RepeatAction While(Func<bool> func)
        {
            return new RepeatAction(func);
        }

        public RepeatAction RetryAfter(TimeSpan retry)
        {
            this.retry = retry;
            return this;
        }

        /// <summary>
        /// Default wait time is 5 seconds
        /// </summary>
        /// <returns></returns>
        public RepeatAction RetryAfter()
        {
            retry = new TimeSpan(0, 0, 0, 5);
            return this;
        }

        public RepeatAction UpTo(RepeatValue repeatValue)
        {
            maxAttempts = repeatValue.Times;
            return this;
        }

        /// <summary>
        /// Default retry is 5
        /// </summary>
        /// <returns></returns>
        public RepeatAction UpTo()
        {
            maxAttempts = 5;
            return this;
        }

        public RepeatAction Debug()
        {
            return this;
        }

        public void Do(Action action)
        {
            Thread.Sleep(initialDelay);

            var didRetry = false;

            log.InfoFormat("Starting the repeat action process");
            var attempt = 0;
            while (condition())
            {
                didRetry = true;

                attempt++;
                log.InfoFormat("Executing attempt {0} of {1}", attempt, maxAttempts);

                action();

                log.InfoFormat("Done executing action");

                if (condition())
                {
                    log.InfoFormat("Condition is true, will wait for {0}", retry);
                    Thread.Sleep(retry);

                    log.InfoFormat("Done waiting");
                }

                if (attempt >= maxAttempts)
                {
                    log.InfoFormat("Done trying. Tried {0} times with a delay of {1} between each attempt.",
                        maxAttempts, retry);

                    break;
                }
            }

            if (!didRetry)
            {
                log.InfoFormat("Did not have to retry as the condition was met immediately");
            }
        }

        public RepeatAction WithInitialDelayOf(TimeSpan initialDelay)
        {
            this.initialDelay = initialDelay;
            return this;
        }
    }
}