﻿namespace Shared.Retry
{
    public class RepeatValue
    {
        public RepeatValue(int times)
        {
            Times = times;
        }

        public int Times { get; private set; }
    }
}