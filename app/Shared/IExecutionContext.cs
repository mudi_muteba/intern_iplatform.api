﻿using System;
using System.Collections.Generic;

namespace Shared
{
    public interface IExecutionContext
    {
        string UserAgent { get; }
        string Referrer { get; }
        Guid RequestId { get; }
        DateTime RequestDate { get; }
        string RequestType { get; }
        string RequestIp { get; }
        string RequestMethod { get; }
        string RequestUrl { get; }
        Dictionary<string, IEnumerable<string>> RequestHeaders { get; }
        List<int> Channels { get; }
        string ChannelIds { get; }
        int ActiveChannelId { get; }
        string Token { get; }
        int UserId { get; }
        string Username { get; }
        string UserFullname { get; }

        Guid ActiveChannelSystemId { get; }
    }
}