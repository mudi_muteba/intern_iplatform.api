﻿using Api.Installers;
using Castle.Windsor;
using System;
using Xunit;
using Xunit.Extensions;

namespace QuoteUpload.Acceptance.Tests.Infrastructure
{
    public abstract class BaseTest : Specification
    {
        protected readonly WindsorContainer Container = new WindsorContainer();
        public BaseTest()
        {
            AutoMapperConfiguration.Configure();
            Container.Install(new AutoMapperInstaller());
        }
    }
}
