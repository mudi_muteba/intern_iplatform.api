﻿using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Ratings.Quoting;
using System;
using System.ServiceModel;
using TestObjects.Mothers.Ratings;
using Xunit.Extensions;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Common.Logging;
using iPlatform.Api.DTOs.QuoteAcceptance;
using Workflow.QuoteAcceptance.SAU.SAUService;
using Workflow.QuoteAcceptance.SAU.Lead;

namespace QuoteUpload.Acceptance.Tests
{
    public class when_uploading_a_quote_to_sau : Specification
    {
        private readonly SAUBlackBoxServiceSoap _client;
        private readonly PublishQuoteUploadMessageDto _quote;
        private readonly QuoteAcceptedMetadata _metadata;
        private SubmitLeadResponse result;
        public when_uploading_a_quote_to_sau()
        {
            //Hardcoded an already quoted SAU LeadId and Asset Number
            _quote = QuoteAcceptanceDtoObjectMother.SauQuoteAcceptance();
            var config = QuoteAcceptanceConfigurationMother.SAUConfig();
            _quote.AgentDetail = new AgentDetailDto { Comments = "Test comment" };
            _metadata = MetadataConfigurationReader.SAU.Generate(_quote, _quote.Policy.ProductCode, _quote.ChannelInfo.SystemId, config);
            _client = new SAUBlackBoxServiceSoapClient(new BasicHttpBinding(), new EndpointAddress(_metadata.ApiMetadata.Url));
            var message = new QuoteUploadMessage(_quote, _metadata, new RetryStrategy(2, 2));
        }

        public override void Observe()
        {
            result = SAUServiceProvider.Upload(_client, LeadFactory.CreateLead(_quote, _metadata.ApiMetadata));
        }

        [Observation(Skip = "should be run manually")]
        public void IsSuccess()
        {
            result.IsSuccess().ShouldBeTrue();
        }
    }
}
