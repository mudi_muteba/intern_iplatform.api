﻿using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using System.ServiceModel;
using iPlatform.Api.DTOs.QuoteAcceptance;
using TestObjects.Mothers.Ratings;
using Workflow.QuoteAcceptance.Regent.Lead;
using Workflow.QuoteAcceptance.Regent.RegentService;
using Xunit.Extensions;

namespace QuoteUpload.Acceptance.Tests
{
    public class when_uploading_a_quote_to_regent : Specification
    {
        private readonly IRegentServiceClient _client;
        private readonly LeadFactory _leadFactory;
        private bool result;
        public when_uploading_a_quote_to_regent()
        {
            var dto = QuoteAcceptanceDtoObjectMother.RegentQuoteAcceptance();
            var config = QuoteAcceptanceConfigurationMother.RegentConfig();
            dto.AgentDetail = new AgentDetailDto { EmailAddress = "test@test.com" };
            var metadata = MetadataConfigurationReader.Regent.Generate(dto, dto.Policy.ProductCode, dto.ChannelInfo.SystemId, config);
            _client = new ClientInterfaceSoapClient(new BasicHttpBinding(), new EndpointAddress(metadata.ApiMetadata.Url));
            _leadFactory = new LeadFactory(new QuoteUploadMessage(dto, metadata, new RetryStrategy(2, 2)));
        }

        public override void Observe()
        { 
            result = RegentServiceProvider.Upload(_client, _leadFactory.CreateLead());
        }

        [Observation(Skip = "should be run manually")]
        public void IsSuccess()
        {
            result.ShouldEqual(true);
        }
    }
}
