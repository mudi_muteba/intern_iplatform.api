﻿using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Ratings.Quoting;
using System;
using System.ServiceModel;
using TestObjects.Mothers.Ratings;
using Xunit.Extensions;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Common.Logging;
using iPlatform.Workflow.QuoteAcceptance.KingPriceV2.KingPriceServiceV2;
using Workflow.QuoteAcceptance.Transferer.Leads;
using Thinktecture.IdentityModel.WSTrust;
using System.ServiceModel.Description;
using System.IdentityModel.Tokens;

namespace QuoteUpload.Acceptance.Tests
{
    public class when_uploading_a_quote_to_kingprice2 : Specification
    {
        private readonly PartnerService _client;
        private readonly PublishQuoteUploadMessageDto _quote;
        private readonly QuoteAcceptedMetadata _metadata;
        private ActivateLeadResponseV2 result;
        private readonly SecurityContext _security;
        private readonly string _reference;
        public when_uploading_a_quote_to_kingprice2()
        {
            //do a quote on any test environment, use the insurer reference and change _reference below to test upload
            //USE the same setting as the test environment irate has for KP
            _reference = "1728523";
            _quote = QuoteAcceptanceDtoObjectMother.KingPrice2QuoteAcceptance();
            var config = QuoteAcceptanceConfigurationMother.KingPriceV2Config();
            _metadata = MetadataConfigurationReader.KingPriceV2.Generate(_quote, _quote.Policy.ProductCode, _quote.ChannelInfo.SystemId, config);
            var taskmetadata = _metadata.TaskMetadata.Get<LeadTransferKingPriceV2TaskMetadata>();

            _client = TransferLeadToKingPriceV2.BuildClient(_metadata.ApiMetadata.Url, _metadata.ApiMetadata.User, _metadata.ApiMetadata.Password,
                        taskmetadata.WsTrustClientIdentityServerUrl, taskmetadata.WsTrustClientServicesUrl);

            _security = TransferLeadToKingPriceV2.GetContext(_metadata.ApiMetadata, taskmetadata);
        }

        public override void Observe()
        {
            var request = new ActivateLeadRequestV2 { BucketKey = int.Parse(_reference), EmailAddress = "monitoring@iplatform.co.za" };

            result = _client.ActivateContactV2(_security, request);
        }

        [Observation(Skip = "should be run manually")]
        public void IsSuccess()
        {
            (result != null && result.Result != null && result.Result.Success).ShouldEqual(true);
        }
    }
}
