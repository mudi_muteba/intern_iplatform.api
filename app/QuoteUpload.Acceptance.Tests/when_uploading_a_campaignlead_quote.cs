﻿using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using System.ServiceModel;
using TestObjects.Mothers.Ratings;
using Workflow.QuoteAcceptance.Campaign.Lead;
using Workflow.QuoteAcceptance.Campaign.LeadWebService;
using Workflow.QuoteAcceptance.Campaign.Helpers;
using Xunit.Extensions;

namespace QuoteUpload.Acceptance.Tests
{
    public class when_uploading_a_campaignlead_quote : Specification
    {
        private readonly ICampaignServiceClient _client;
        private readonly LeadFactory _leadFactory;
        private LeadImportResult result;
        public when_uploading_a_campaignlead_quote()
        {
            var dto = QuoteAcceptanceDtoObjectMother.CampaignQuoteAcceptance();
            var config = QuoteAcceptanceConfigurationMother.CampaignConfig();
            var metadata = MetadataConfigurationReader.Campaign.Generate(dto, dto.Policy.ProductCode, dto.ChannelInfo.SystemId, config);
            _client = new LeadWebServiceSoapClient(new BasicHttpBinding(), new EndpointAddress(metadata.ApiMetadata.Url));
            _leadFactory = new LeadFactory(new QuoteUploadMessage(dto, metadata, new RetryStrategy(2, 2)));
        }

        public override void Observe()
        {
            result = CampaignServiceProvider.Upload(_client, _leadFactory.CreateLead().SerializeToXml());
        }

        [Observation(Skip = "should be run manually")]
        public void IsSuccess()
        {
            result.ImportResult.ShouldEqual("1");
        }
    }
}
