﻿using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using System.ServiceModel;
using TestObjects.Mothers.Ratings;
using Workflow.QuoteAcceptance.Hollard.HollardService;
using Workflow.QuoteAcceptance.Hollard.Lead;
using Xunit.Extensions;

namespace QuoteUpload.Acceptance.Tests
{
    public class when_uploading_a_quote_to_hollard : Specification
    {
        private readonly IHollardServiceClient _client;
        private readonly LeadFactory _leadFactory;
        private readonly string _authtoken;
        private Response result;
        public when_uploading_a_quote_to_hollard()
        {
            var dto = QuoteAcceptanceDtoObjectMother.HollardQuoteAcceptance();
            var config = QuoteAcceptanceConfigurationMother.HollardConfig();
            var metadata = MetadataConfigurationReader.Hollard.Generate(dto, dto.Policy.ProductCode, dto.ChannelInfo.SystemId, config);
            _authtoken = metadata.ApiMetadata.AuthenticationToken;
            _client = new HollardSwitchingClient("WSHttpBinding_IHollardSwitching",new EndpointAddress(metadata.ApiMetadata.Url));
            _leadFactory = new LeadFactory(new QuoteUploadMessage(dto, metadata, new RetryStrategy(2, 2)));
        }

        public override void Observe()
        {
            result = HollardServiceProvider.Upload(_client, _authtoken, _leadFactory.CreateLead());
        }

        [Observation(Skip = "should be run manually - does not work need to review hollard's url")]
        public void IsSuccess()
        {
            result.ResponseStatus.ShouldEqual(ResponseStatus.Success);
        }
    }
}
