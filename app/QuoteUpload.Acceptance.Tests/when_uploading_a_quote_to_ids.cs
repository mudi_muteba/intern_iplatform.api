﻿using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Xunit.Extensions;
using Workflow.QuoteAcceptance.Ids.IDSService;
using Workflow.QuoteAcceptance.Ids.Lead;
using QuoteUpload.Acceptance.Tests.Infrastructure;
using TestObjects.Mothers.Ratings;
using System.ServiceModel;

namespace QuoteUpload.Acceptance.Tests
{
    public class when_uploading_a_quote_to_ids : BaseTest
    {
        private readonly IIdsServiceClient _client;
        private readonly LeadFactory _leadFactory;
        private string result;
        public when_uploading_a_quote_to_ids()
        {
            var dto = QuoteAcceptanceDtoObjectMother.IDSQuoteAcceptance();
            var config = QuoteAcceptanceConfigurationMother.IDSConfig();
            var metadata = MetadataConfigurationReader.Ids.Generate(dto, dto.Policy.ProductCode, dto.ChannelInfo.SystemId, config);
            _client = new CommandClient(new BasicHttpBinding(), new EndpointAddress(metadata.ApiMetadata.Url));
            _leadFactory = new LeadFactory(new QuoteUploadMessage(dto, metadata, new RetryStrategy(2, 2)));
        }

        public override void Observe()
        { 
            result = IdsServiceProvider.Upload(_client, _leadFactory.CreateLead());
        }

        [Observation(Skip = "should be run manually")]
        public void IsSuccess()
        {
            var success = IdsServiceProvider.IsSuccessfulResult(result);
            success.ShouldEqual(true);
        }
    }
}
