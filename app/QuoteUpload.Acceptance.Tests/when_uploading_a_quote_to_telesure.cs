﻿using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Ratings.Quoting;
using System;
using System.ServiceModel;
using TestObjects.Mothers.Ratings;
using Workflow.QuoteAcceptance.Telesure.Lead;
using Workflow.QuoteAcceptance.Telesure.TelesureService;
using Xunit.Extensions;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance.Metadata.Insurers;
using Common.Logging;
using iPlatform.Api.DTOs.QuoteAcceptance;

namespace QuoteUpload.Acceptance.Tests
{
    public class when_uploading_a_quote_to_telesure : Specification
    {
        private readonly ITelesureServiceClient _client;
        private readonly AuthenticationHeader _authenticationHeader;
        private readonly LeadFactory _leadFactory;
        private readonly PublishQuoteUploadMessageDto _quote;
        private readonly QuoteAcceptedMetadata _metadata;
        private TransferCompleted result;
        public when_uploading_a_quote_to_telesure()
        {
            //PLEASE USE PSG-UAT-RATE in appsetting

            _quote = QuoteAcceptanceDtoObjectMother.TelesureQuoteAcceptance();
            var config = QuoteAcceptanceConfigurationMother.TelesureUnityConfig();
            _quote.AgentDetail = new AgentDetailDto { EmailAddress = "test@test.com" };
            _metadata = MetadataConfigurationReader.Telesure.Generate(_quote, _quote.Policy.ProductCode, _quote.ChannelInfo.SystemId, config);
            _client = new APIServiceSoapClient(new BasicHttpBinding(BasicHttpSecurityMode.Transport), new EndpointAddress(_metadata.ApiMetadata.Url));
            _authenticationHeader = new AuthenticationHeader() { Password = _metadata.ApiMetadata.Password, Username = _metadata.ApiMetadata.User };
            _leadFactory = new LeadFactory(new QuoteUploadMessage(_quote, _metadata, new RetryStrategy(2, 2)));
        }

        public override void Observe()
        {
            var sessionId = TelesureServiceProvider.GetSessionId(_client, _authenticationHeader);
            var metadata = _metadata.TaskMetadata.Get<LeadTransferTelesureTaskMetadata>();
            var productMapping = TelesureServiceProvider.GetCompanyDetails(_quote, metadata, _metadata.ApiMetadata);
            if (sessionId != Guid.Empty.ToString())
                result = TelesureServiceProvider.Upload(_client, _leadFactory.CreateLead(productMapping, metadata.IRateUrl), _authenticationHeader, sessionId);
        }

        [Observation(Skip = "should be run manually")]
        public void IsSuccess()
        {
            result.ErrorStatus.Equals("E", StringComparison.CurrentCultureIgnoreCase).ShouldEqual(false);
        }
    }
}
