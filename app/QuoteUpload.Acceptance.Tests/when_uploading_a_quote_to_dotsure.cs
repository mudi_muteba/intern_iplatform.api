﻿using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using System.ServiceModel;
using iPlatform.Api.DTOs.QuoteAcceptance;
using TestObjects.Mothers.Ratings;
using Workflow.QuoteAcceptance.Dotsure.DotsureService;
using Workflow.QuoteAcceptance.Dotsure.Lead;
using Xunit.Extensions;

namespace QuoteUpload.Acceptance.Tests
{
    public class when_uploading_a_quote_to_dotsure : Specification
    {
        private readonly IDotsuresureServiceClient _client;
        private readonly LeadFactory _leadFactory;
        private readonly AuthHeader _authHeader;
        private ServiceResults result;
        public when_uploading_a_quote_to_dotsure()
        {
            var dto = QuoteAcceptanceDtoObjectMother.DotsureQuoteAcceptance();
            var config = QuoteAcceptanceConfigurationMother.DotsureConfig();
            dto.AgentDetail = new AgentDetailDto { EmailAddress = "test@test.com" };

            var metadata = MetadataConfigurationReader.Dotsure.Generate(dto, dto.Policy.ProductCode, dto.ChannelInfo.SystemId, config);
            _authHeader = Authentication.AuthHeader(metadata.ApiMetadata.User, metadata.ApiMetadata.Password);
            _client = new ServiceSoapClient(new BasicHttpBinding(), new EndpointAddress(metadata.ApiMetadata.Url));
            _leadFactory = new LeadFactory(new QuoteUploadMessage(dto, metadata, new RetryStrategy(2, 2)));
        }

        public override void Observe()
        { 
            result = DotsureServiceProvider.Upload(_client, _authHeader, _leadFactory.CreateLead());
        }

        [Observation(Skip = "should be run manually")]
        public void IsSuccess()
        {
            result.Status.ShouldEqual(ResultsCode.Success);
        }
    }
}
