﻿using System;
using System.Linq;
using Common.Logging;
using Domain.Policies.Message;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.HollardEasy.Service_References.Response;
using Workflow.QuoteAcceptance.HollardEasy.Service_References.ServiceModels;

namespace Workflow.QuoteAcceptance.HollardEasy.HollardEasyService
{
    public class HollardEasyServiceProvider : ITransferLeadToInsurer
    {
        private readonly CommunicationMetadata m_Mail;
        private readonly Guid m_ChannelId;
        public ExecutionPlan ExecutionPlan { get; set; }
        public IRetryStrategy RetryStrategy { get; set; }
        private static readonly ILog Log = LogManager.GetLogger<HollardEasyServiceProvider>();
        private IHollardTiaClient Client { get; set; }


        public HollardEasyServiceProvider(IHollardTiaClient client, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId)
        {
            m_Mail = mail;
            m_ChannelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan;
            Client = client;
        }
        public LeadTransferResult Transfer(QuoteUploadMessage message)
        {
            var quote = message.Quote;
            var result = Execute(quote);

            if (result.Error.HasError)
            {
                var error = result.Error.Error;
                Log.ErrorFormat("Quote [{0}] failed to upload to Hollard Easy with failure reason {1}", quote.Request.Id.ToString(), error);
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Failure Reason {0}", error), error, quote.Request.Id, InsurerName.HollardEasy.Name(), m_ChannelId, m_Mail, quote.QuoteAcceptanceEnvironment));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, m_Mail.CommunicationMessage.Address));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureWithMessage(InsurerName.HollardEasy.Name(), quote.Request.Id, string.Format("Failure Reason {0}", error), m_ChannelId));
                return new LeadTransferResult(false);
            }

            Log.InfoFormat("Quote [{0}] successfully uploaded to Hollard Easy", quote.Request.Id.ToString());
            return new LeadTransferResult(!result.Error.HasError);
        }

        private QuoteDetails Execute(PublishQuoteUploadMessageDto quote)
        {
            var retry = true;
            var errorMessage = string.Empty;
            var email = string.Format("{0};{1}", "monitoring@iplatform.co.za", quote.AgentDetail.EmailAddress);
            var acceptQuoteResult = new QuoteDetails();

            var referenceNumber = "";

            RetryStrategy
                .OnSuccess(
                    () =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(quote.Policy.ProductCode, InsurerName.HollardEasy.Name(), quote.Request.Id, m_ChannelId));
                        ExecutionPlan.AddMessage(LeadQuoteUploadMessage.CreateQuoteUploadLog(quote, errorMessage, referenceNumber));
                        ExecutionPlan.AddMessage(CustomerSatisfactionSurveyMessage.CreateCustomerSatisfactionSurveyMessage(quote, referenceNumber));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(quote, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, m_Mail.CommunicationMessage.Address));
                    })
                .OnFailure(
                    () =>
                    {
                        ExecutionPlan.AddMessage(DeleteQuoteAcceptMessage.Create(quote.Id));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(quote.Policy.ProductCode, InsurerName.HollardEasy.Name(), quote.Request.Id, m_ChannelId));
                    })
                .OnStart(
                    () => ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(quote.Policy.ProductCode, InsurerName.HollardEasy.Name(), quote.Request.Id, quote.SerializeAsXml(), m_ChannelId))
                    )
                .OnFinish(
                    (response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(quote.Policy.ProductCode, InsurerName.HollardEasy.Name(), quote.Request.Id, response.ToString(), m_ChannelId))
                    )
                .While(() => retry)
                .Execute(() =>
                {
                    try
                    {
                        //1.Login Get Auth Token
                        var authToken = GetAuthenticationToken(quote.Request, quote.Policy.ProductCode);

                        //2.Get the Quote using quoteId
                        int quoteId;
                        if (!int.TryParse(quote.Policy.InsurerReference, out quoteId))
                        {
                            throw new Exception("Invalid quote id: " + quote.Policy.InsurerReference);
                        }
                        var result = Client.ExecuteRequest<QuoteResponseModel, QuoteDetails>(Client.GetProposal(quoteId, authToken));

                        //3.Update the quote with the selected cover Orange/Purple
                        var isOrange = string.Equals(quote.Policy.ProductCode, "HOLEASYO", StringComparison.InvariantCultureIgnoreCase);
                        foreach (var line in result.Data.Lines)
                        {
                            SetSelectedProduct(line, isOrange);
                        }

                        //4.Submit the quote
                        var client = GetClient(result.Data, authToken);
                        var paymentDetails = GeneratePaymentDetails();
                        var submitQuoteModel = GenerateSubmitQuoteModel(client.Data, result.Data, paymentDetails);

                        var submittedQuote = Client.ExecuteRequest<QuoteResponseModel, QuoteDetails>(Client.SubmitQuote(submitQuoteModel, authToken));

                        //5.Generate Proposal document
                        Client.ExecuteRequest<QuoteResponseModel, QuoteDetails>(Client.GenerateQuoteProposal(m_Mail.CommunicationMessage.Address, quoteId, authToken));

                        //6.Accept the quote
                        var acceptQuote = GenerateAcceptQuoteModel(client.Data, paymentDetails);
                        Client.ExecuteRequest<QuoteResponseModel, QuoteDetails>(Client.AcceptQuote(submittedQuote.Data.TiaClientPartyNo, acceptQuote, authToken));

                        retry = false;
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.HollardEasy.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.HollardEasy.Name(), quote.Request.Id, ex, m_ChannelId));
                        acceptQuoteResult.Error = new Errors(true, ex.Message);
                        retry = false;
                    }
                    return retry;
                });
            return acceptQuoteResult;
        }

        private string GetAuthenticationToken(RatingRequestDto request, string productCode)
        {
            var userSetting = request.UserRatingSettings.FirstOrDefault(x => string.Equals(x.Product.ProductCode, productCode, StringComparison.InvariantCultureIgnoreCase));

            if (userSetting == null)
            { throw new Exception("Invalid user credentials settings supplied. Please ensure that this user has Rating Settings for this product."); }

            var loginResult = Login(new LoginRequestModel(userSetting.RatingUsername, userSetting.RatingPassword));
            return loginResult.Data.Token;
        }

        private LoginResponseModel Login(LoginRequestModel input)
        {
            var result = Client.ExecuteRequest<LoginResponseModel, LoginDetails>(Client.CreateLoginRequest(input));
            return result;
        }

        private void SetSelectedProduct(Line section, bool isOrangeSelected)
        {
            var orangeRiskItem = section.Risks.FirstOrDefault(o => string.Equals(o.Name, "Comprehensive cover - Orange", StringComparison.InvariantCultureIgnoreCase));
            var purpuleRiskItem = section.Risks.FirstOrDefault(o => string.Equals(o.Name, "Comprehensive cover - Purple", StringComparison.InvariantCultureIgnoreCase));

            if (orangeRiskItem != null)
            { orangeRiskItem.Selected = isOrangeSelected; }

            if (purpuleRiskItem != null)
            { purpuleRiskItem.Selected = !isOrangeSelected; }
        }

        private AcceptQuoteModel GenerateAcceptQuoteModel(Client client, PaymentDetailsApiModel paymentDetails)
        {
            var acceptQuote = new AcceptQuoteModel();
            acceptQuote.Client = client;
            acceptQuote.InceptionDate = DateTime.Now;
            acceptQuote.PaymentDetails = paymentDetails;
            acceptQuote.StrikeDay = 1;
            acceptQuote.SignedProposal = new DocumentApiModel();

            return acceptQuote;
        }

        private SubmitQuoteModel GenerateSubmitQuoteModel(Client client, QuoteDetails quote, PaymentDetailsApiModel paymentDetails)
        {
            var acceptQuote = new SubmitQuoteModel();
            acceptQuote.Client = client;
            acceptQuote.Quote = quote;
            acceptQuote.PaymentDetails = paymentDetails;
            acceptQuote.Proxy = new ProxyApiModel(quote.TiaClientPartyNo, client.IdNumber, client.Name, client.Surname);
            return acceptQuote;
        }

        //Todo: Add Payment Details
        private PaymentDetailsApiModel GeneratePaymentDetails()
        {

            var paymentDetails = new PaymentDetailsApiModel();
            return paymentDetails;
            var accountType = (int)AccountType.ChequeAccount;
            paymentDetails.Values.Add(" ACCOUNT_HOLDER_NAME", "Test");
            paymentDetails.Values.Add(" ACCOUNT_TYPE", accountType.ToString());
            paymentDetails.Values.Add(" BANK_ACCOUNT_NO", 123456);
            paymentDetails.Values.Add(" BANK_ID_NO", "8501015188088");
            paymentDetails.Values.Add(" CDV_SERVICE_STATUS", "");
            paymentDetails.Values.Add(" ENABLE_NAEDO", 0);
            paymentDetails.Values.Add(" PAYMENT_METHOD", "");
            return paymentDetails;
        }

        private CreateClientResponse GetClient(QuoteDetails quote, string authToken)
        {
            var searchClient = Client.ExecuteRequest<CreateClientResponse, Client>(Client.SearchClientByTiaPartyNumber(quote.TiaClientPartyNo.ToString(), authToken));

            return searchClient;
        }
    }
}
