﻿using System;
using System.Text;
using Domain.QuoteAcceptance.Leads;
using iPlatform.Api.DTOs.Ratings.Quoting;

namespace Workflow.QuoteAcceptance.HollardEasy.Lead
{
    public class LeadFactory
    {
        // private static readonly ILog Log = LogManager.GetLogger<LeadFactory>();
        private readonly PublishQuoteUploadMessageDto m_Quote;
        private readonly StringBuilder m_Lead;
        public LeadFactory(QuoteUploadMessage message)
        {
            m_Lead = new StringBuilder();
            m_Quote = message.Quote;
        }

        public string CreateLead()
        {
            var validation = ValidateQuote();
            if (!string.IsNullOrEmpty(validation))
            { throw new Exception(string.Format("Validation for Hollard Easy failed with message: {0}", validation)); }

            try
            {


            }
            catch (Exception ex)
            {
                //       Log.ErrorFormat("Creating Hollard Easy Lead Request exception: {0} {1}", ex.Message, _lead.ToString());
                throw;
            }
            return m_Lead.ToString();
        }


        private string ValidateQuote()
        {
            if (m_Quote == null)
            { return "Quote Dto in null"; }
            if (m_Quote.InsuredInfo == null)
            { return "No Insured Info in Quote Response Dto"; }
            if (m_Quote.Policy == null)
            { return "No Policy Info in Quote Response Dto"; }
            if (m_Quote.Policy.Items == null)
            { return "No Items in Quote Response Dto"; }
            if (m_Quote.Request.Policy == null)
            { return "No Policy Info in Quote Request Dto"; }
            return m_Quote.Request.Items == null ? "No Items in Quote Request Dto" : "";
        }
    }
}
