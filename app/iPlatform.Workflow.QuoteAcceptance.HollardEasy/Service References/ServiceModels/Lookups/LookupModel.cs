﻿using System;

namespace Workflow.QuoteAcceptance.HollardEasy.Service_References.ServiceModels.Lookups
{
    public class LookupModel
    {
        public LookupModel(string type, string text, int value, string help, string dependancy, bool visible)
        {
            LookupType lookupType;
            if (Enum.TryParse(type, out lookupType))
                Type = lookupType;

            Text = text;
            Value = value;
            Help = help;
            Dependency = dependancy;
            Visible = visible;
        }

        public LookupType Type { get; set; }
        public string Text { get; set; }
        public int Value { get; set; }
        public string Help { get; set; }
        public string Dependency { get; set; }
        public bool Visible { get; set; }
    }
}