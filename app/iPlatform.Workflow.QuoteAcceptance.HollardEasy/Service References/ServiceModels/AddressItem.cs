﻿using System.Text.RegularExpressions;
using MasterData;

namespace Workflow.QuoteAcceptance.HollardEasy.Service_References.ServiceModels
{
    public class AddressItem
    {
        public AddressType AddressType { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string Suburb { get; set; }
        public string AddressLine4 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public int AddressId { get; set; }
        public string StreetName
        {
            get { return GetStreetNameFromAddressLine(AddressLine1); }
        }

        public string StreetNumber
        {
            get { return GetStreetNumberFromAddressLine(AddressLine1); }
        }

        private string GetStreetNumberFromAddressLine(string input)
        {
            var match = Regex.Match(input, @"(\d*)(.*)");
            return match.Groups.Count > 0 ? match.Groups[1].Value.Trim() : "";
        }

        private string GetStreetNameFromAddressLine(string input)
        {
            var match = Regex.Match(input, @"(\d*)(.*)");
            return match.Groups.Count > 0 ? match.Groups[2].Value.Trim() : "";
        }
    }
}