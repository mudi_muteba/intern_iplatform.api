﻿using System.Collections.Generic;

namespace Workflow.QuoteAcceptance.HollardEasy.Service_References.ServiceModels
{
    public static class DictionaryExtentions
    {
        public static Dictionary<string, object> AddString(this Dictionary<string, object> dictionary, string key, object value)
        {
            dictionary.Add(key, value.ToString());        
            return dictionary;
        }
    }


}