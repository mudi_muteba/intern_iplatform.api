﻿namespace Workflow.QuoteAcceptance.HollardEasy.Service_References.ServiceModels
{
    public class Attribute
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public int DataType { get; set; }
        public string LookupTable { get; set; }
        public string Description { get; set; }
    }
}