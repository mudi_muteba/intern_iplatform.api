﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Workflow.QuoteAcceptance.HollardEasy.Service_References.ServiceModels
{
    [XmlInclude(typeof(List<Line>))]
    public class QuoteDetails
    {
        public QuoteDetails()
        {
            Values = new SerializableDictionary<string, object>();
            Error = new Errors();
            Lines = new List<Line>();
        }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientSurname { get; set; }
        public int Id { get; set; }
        public string PolicyNo { get; set; }
        public object ParentPolicyNo { get; set; }
        public int Status { get; set; }
        public double TotalPremium { get; set; }
        public object InceptionDate { get; set; }
        public string QuoteDate { get; set; }
        public object AcceptedDate { get; set; }
        public int TiaClientPartyNo { get; set; }
        //public int TiaPolicyLineNo { get; set; }
        //public int TiaSeqNo { get; set; }
        public object CancelCode { get; set; }
        public object CancellationDate { get; set; }
        public List<Line> Lines { get; set; }
        public SerializableDictionary<string, object> Values { get; set; }
        public Errors Error { get; set; }

    }

    [XmlInclude(typeof(List<Benefit>))]
    public class Risk
    {
        public Risk()
        {
            Benefits = new List<Benefit>();
            Dependencies = new List<string>();
            DependentMessages = new List<string>();
        }
        public int Id { get; set; }
        public int RiskNo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double? Premium { get; set; }
        public bool Selected { get; set; }
        public string Price { get; set; }
        public int Category { get; set; }
        public List<Benefit> Benefits { get; set; }
        public List<string> Dependencies { get; set; }
        public List<string> DependentMessages { get; set; }
    }




    public class Benefit
    {
        public Benefit()
        {

        }
        public string Name { get; set; }
        public string Cover { get; set; }
        public bool Primary { get; set; }
    }

    [XmlInclude(typeof(List<Risk>))]
    public class Line
    {
        public Line()
        {
            Values = new SerializableDictionary<string, object>();
            Losses =new List<LossItem>();
            SpecifiedItems = new List<SpecifiedItem>();
            Risks = new List<Risk>();
            Clauses = new List<Clause>();
        }
        public int Id { get; set; }
        public object Description { get; set; }
        public string CoverType { get; set; }
        public List<object> Dependencies { get; set; }
        public object InceptionDate { get; set; }
        public double SumInsured { get; set; }
        public double? Premium { get; set; }
        public object OnceOff { get; set; }
        public object TiaPolicyLineNo { get; set; }
        public object TiaSeqNo { get; set; }
        public bool Selected { get; set; }
        public bool Deleted { get; set; }
        public SerializableDictionary<string, object> Values { get; set; }
        public List<LossItem> Losses { get; set; }
        public List<SpecifiedItem> SpecifiedItems { get; set; }
        public List<Risk> Risks { get; set; }
        public List<Clause> Clauses { get; set; }
        public object ParentLineId { get; set; }
        public object ParentLineNo { get; set; }
        public object ProductLineVersionNo { get; set; }
    }


    public class ValuesModel
    {
        public ValuesModel()
        {
            Values = new Dictionary<string, object>();
        }

        public Dictionary<string, object> Values { get; set; }
    }

    public class Errors
    {
        public Errors(bool hasError, string error)
        {
            HasError = hasError;
            Error = error;
        }
        public Errors()
        {
            HasError = false;
        }

        public bool HasError { get; set; }
        public string Error { get; set; }
    }


    public class SpecifiedItem
    {
        public SpecifiedItem()
        {
            Values = new SerializableDictionary<string, object>();
        }
        public int RiskNo { get; set; }
        public int RiskId { get; set; }
        public string Description { get; set; }
        public int Value { get; set; }
        public SerializableDictionary<string, object> Values { get; set; }

        public SpecifiedItem CreateFactoryFittedItem()
        {
            Values.AddString("ITEM_CATEGORY", "FAC");
            return this;
        }

        public SpecifiedItem CreateNonFactoryFittedItem()
        {
            Values.AddString("ITEM_CATEGORY", "NONFAC");
            return this;
        }

    }

    public class LossItem
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public int Value { get; set; }
        public object LossIdentifier { get; set; }
    }

    public class Clause
    {
        public string ClauseText { get; set; }
    }

}