﻿using System.Web.Script.Services;
using Newtonsoft.Json;
using RestSharp;
using Workflow.QuoteAcceptance.HollardEasy.Service_References.Response;
using Workflow.QuoteAcceptance.HollardEasy.Service_References.ServiceModels;

namespace Workflow.QuoteAcceptance.HollardEasy.Service_References.ApiRequests
{
    public class TiaApiService
    {
        private readonly IRestClient m_Client;
        //private static readonly ILog log = LogManager.GetLogger<TiaApiService>();

        public TiaApiService(IRestClient client)
        {
            this.m_Client = client;
        }

        public T Execute<T, TOBj>(IRestRequest apiRequest) where T : IStatusReponse<TOBj>, new() where TOBj : new()
        {
            var response = m_Client.Execute<T>(apiRequest);
            return HandleResponse<T, TOBj>(response);
        }


        private string m_QueryPath = "";
        private ResponseFormat m_Format = ResponseFormat.Json;

        private IRestRequest CreateRequest<T>(T requestBody, string url, string authorisationToken, Method method = Method.POST)
        {
            //client.Authenticator = new HttpBasicAuthenticator(username, password);

            var apiRequest = new RestRequest(m_QueryPath + url)
            {
                RequestFormat = DataFormat.Json,
                Method = method
            };

            if (requestBody != null)
            { apiRequest.AddBody(requestBody); }

            apiRequest.AddHeader("Authorization", "Basic aXBsYXRmb3JtOlNKQGZzJSNmV0F0KGopbyFa");

            if (!string.IsNullOrEmpty(authorisationToken))
            { apiRequest.AddHeader("X-Authorisation-Token", authorisationToken); }


            apiRequest.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            return apiRequest;
        }

        public IRestRequest CreateClientRequest(Client client, string authorisationToken)
        {
            return CreateRequest(client, "clients/create", authorisationToken);
        }

        public IRestRequest CreateQuoteRequest(CreateQuote quote, string authorisationToken)
        {
            return CreateRequest(quote, "quotes/create", authorisationToken);
        }

        public IRestRequest UpdateQuoteRequest(string quoteId, string productCode, string authorisationToken)
        {
            return CreateRequest(new { }, string.Format("quotes/{0}/add/{1}", quoteId, productCode), authorisationToken);
        }

        public IRestRequest GenerateQuoteProposal(string email, int quoteId, string authorisationToken)
        {
            var myAnonymousType = new { Email = email };
            return CreateRequest(myAnonymousType, string.Format("quotes/{0}/generateProposal", quoteId), authorisationToken);
        }

        public IRestRequest GetProposal(int quoteId, string authorisationToken)
        {
            return CreateRequest<QuoteDetails>(null, string.Format("quotes/{0}", quoteId), authorisationToken, Method.GET);
        }

        public IRestRequest SubmitQuote(SubmitQuoteModel submitQuoteModel, string authorisationToken)
        {
            return CreateRequest(submitQuoteModel, string.Format("quotes/{0}/submit", submitQuoteModel.Quote.Id), authorisationToken);
        }

        public IRestRequest CreateAuthorisationToken(LoginRequestModel loginCredentials)
        {
            return CreateRequest(loginCredentials, "broker/login/", "");
        }

        public IRestRequest SearchClientById(SearchClient searchClient, string authorisationToken)
        {
            return CreateRequest(searchClient, "clients/search", authorisationToken);
        }

        public IRestRequest SearchClientByTiaPartyNumber(string tiaPartyNo, string authorisationToken)
        {
            return CreateRequest((object)null, "clients/tia/" + tiaPartyNo, authorisationToken, Method.GET);
        }

        public IRestRequest SearchPolicy(string policyNo, string authorisationToken)
        {
            return CreateRequest((object)null, "policies/" + policyNo, authorisationToken, Method.GET);
        }

        public IRestRequest AcceptQuote(int quoteId, AcceptQuoteModel acceptQuote, string authorisationToken)
        {
            return CreateRequest(acceptQuote, string.Format("quotes/{0}/accept", quoteId), authorisationToken, Method.GET);
        }

        private T HandleResponse<T, TOBj>(IRestResponse apiResponse) where T : IStatusReponse<TOBj>, new() where TOBj : new()
        {
            if (apiResponse.ResponseStatus != ResponseStatus.Completed)
            {
                var errorResponse = JsonConvert.DeserializeObject<GenericResponse>(apiResponse.Content);
                //        log.ErrorFormat("errors: {1} {0} data: {2}", Environment.NewLine, string.Join(" ", errorResponse.Messages.Select(m => .Message ?? "")), (errorResponse ?? new GenericResponse()).Data ?? "");
                return new T() { Data = new TOBj(), StatusCode = errorResponse.StatusCode, Messages = errorResponse.Messages };
            }
            return JsonConvert.DeserializeObject<T>(apiResponse.Content);
        }
    }

}