﻿using System.Collections.Generic;

namespace Workflow.QuoteAcceptance.HollardEasy.Service_References.Response
{
    public interface IStatusReponse<T>
    {
        int StatusCode { get; set; }
        List<ResponseMessage> Messages { get; set; }
        T Data { get; set; }
    }
}