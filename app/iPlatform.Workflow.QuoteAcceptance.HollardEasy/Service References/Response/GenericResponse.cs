﻿using System.Collections.Generic;

namespace Workflow.QuoteAcceptance.HollardEasy.Service_References.Response
{
    public class GenericResponse :  IStatusReponse<string>
    {
        public int StatusCode { get; set; }
        public List<ResponseMessage> Messages { get; set; }
        public string Data { get; set; }
    }
}