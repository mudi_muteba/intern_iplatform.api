﻿using System.Collections.Generic;
using Workflow.QuoteAcceptance.HollardEasy.Service_References.ServiceModels;

namespace Workflow.QuoteAcceptance.HollardEasy.Service_References.Response
{
    public class CreateClientResponse :  IStatusReponse<Client>
    {
        public int StatusCode { get; set; }
        public List<ResponseMessage> Messages { get; set; }
        public Client Data { get; set; }
    }
}