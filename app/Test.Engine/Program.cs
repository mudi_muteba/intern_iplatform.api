﻿using System;
using System.Diagnostics;
using System.IO;

namespace Test.Engine
{
    class Program
    {
        static void Main(string[] args)
        {
            var configuration = "debug";
#if (!DEBUG)
            configuration = "release";
#endif
            var appPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            var fileName = Path.Combine(string.Format("{0}\\Test.Engine\\bin\\{1}", appPath, configuration), "iPlatform.Workflow.Engine.exe");
            Console.WriteLine(fileName);
            var start = new ProcessStartInfo
            {
                FileName = fileName,
                WindowStyle = ProcessWindowStyle.Normal
            };

            var process = new Process {StartInfo = start};
            process.Start();
        }
    }
}
