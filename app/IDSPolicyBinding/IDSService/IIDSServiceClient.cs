﻿namespace Workflow.PolicyBinding.IDS.IDSService
{
    public partial class CommandClient : IIdsServiceClient
    {
    }

    public interface IIdsServiceClient
    {
        string Submit(string inMessage);
    }
}
