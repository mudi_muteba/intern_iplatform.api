﻿using System;
using Common.Logging;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Infrastructure;
using Domain.QuoteAcceptance.Metadata;
using Workflow.Messages;
using Workflow.PolicyBinding.Domain;
using Workflow.PolicyBinding.IDSPolicyBinding.PolicyBinding;
using Domain.PolicyBindings.Messages;
using iPlatform.Api.DTOs.PolicyBindings;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.EmailNotification.Messages;
using Workflow.PolicyBinding.IDS.PolicyBinding;

namespace Workflow.PolicyBinding.IDS.IDSService
{
    public class IdsServiceProvider : ITransferPolicyBindingToInsurer
    {
        private static readonly ILog m_Log = LogManager.GetLogger<IdsServiceProvider>();
        private readonly PolicyBindingFactory m_PolicyBindingFactory;
        private readonly IIdsServiceClient m_Client;
        private static CommunicationMetadata m_Mail;
        private readonly Guid m_ChannelId;
        public IdsServiceProvider(IIdsServiceClient client, PolicyBindingFactory policyBindingFactory, IRetryStrategy retryStrategy, ExecutionPlan executionPlan, CommunicationMetadata mail, Guid channelId)
        {
            m_Client = client;
            m_PolicyBindingFactory = policyBindingFactory;
            m_Mail = mail;
            m_ChannelId = channelId;
            RetryStrategy = retryStrategy;
            ExecutionPlan = executionPlan; 
        }

        public PolicyBindingTransferResult Transfer(SubmitPolicyBindingMessage message)
        {
            var dto = message.Dto;
            var request = m_PolicyBindingFactory.CreatePolicyBinding();

            if (string.IsNullOrEmpty(request))
            {
                m_Log.ErrorFormat("Policy Binding [{0}] failed to upload to IDS. Validation failed.", dto.RequestId.ToString());
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage("No details were found in the Policy Binding", message.SerializeAsXml(), dto.RequestId, InsurerName.Ids.Name(), m_ChannelId, m_Mail, dto.Environment));
                ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailureNoQuote(InsurerName.Ids.Name(), dto.RequestId, m_ChannelId));
                ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(dto, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, m_Mail.CommunicationMessage.Address));
                return new PolicyBindingTransferResult(false);
            }

            var result = Execute(dto, request);
            if (IsSuccessfulResult(result))
            {
                m_Log.InfoFormat("Policy Binding [{0}] successfully uploaded to IDS", dto.RequestId.ToString());
                return new PolicyBindingTransferResult(true);
            }

            m_Log.ErrorFormat("Quote [{0}] failed to upload to IDS with failure reason {1}", dto.RequestId.ToString(), result);
            ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(string.Format("Quote [{0}] failed to upload to IDS with reason {1}", dto.RequestId, result), request, dto.RequestId,InsurerName.Ids.Name(), m_ChannelId, m_Mail, dto.Environment));
            ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(dto.RequestInfo.ProductCode, InsurerName.Ids.Name(), dto.RequestId,m_ChannelId));
            ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(dto, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Failure, m_Mail.CommunicationMessage.Address));

            return new PolicyBindingTransferResult(false);
        }

        private string Execute(PolicyBindingRequestDto dto, string request)
        {
            string result = null;
            var responseSuccess = false;
            var errorMessage = string.Empty;
            RetryStrategy
                .OnSuccess(
                    () =>
                    {
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateSuccesfull(dto.RequestInfo.ProductCode, InsurerName.Ids.Name(), dto.RequestId, m_ChannelId));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationMessage(dto, QuoteAcceptanceNotificationMetaData.QuoteUploadResult.Success, m_Mail.CommunicationMessage.Address));
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateNotificationPdfEmailMessage(dto.ChannelId, dto.QuoteId, dto.LeadId, dto.PartyId, dto.AgentId, dto.AgentDetail, dto.AgentPartyId));
                    })
                .OnFailure(
                    () =>
                    {
                        ExecutionPlan.AddMessage(DeletePolicyBindingCompletedMessage.Create(dto.QuoteId, dto.RequestId));
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateFailure(dto.RequestInfo.ProductCode, InsurerName.Ids.Name(), dto.RequestId, m_ChannelId));
                    })
                .OnStart(
                    () =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateStart(dto.RequestInfo.ProductCode, InsurerName.Ids.Name(), dto.RequestId, dto.SerializeAsXml(), m_ChannelId)))
                .OnFinish(
                    (response) =>
                        ExecutionPlan.AddMessage(LeadTransferMessage.CreateFinished(dto.RequestInfo.ProductCode, InsurerName.Ids.Name(), dto.RequestId, response.SerializeAsXml(), m_ChannelId))
                        )
                .While(() => !responseSuccess)
                .Execute(() =>
                {
                    try
                    {
                        result = "Success/True";
                        m_Log.DebugFormat("Policy Binding Request to IDS: {0}", request);

                        //TODO: IPL-1238 - to enable once Ruann complete cims integration
                        result = Upload(m_Client, request);


                        responseSuccess = IsSuccessfulResult(result);
                        errorMessage = responseSuccess ? "Success" : "Failure";
                        ReferenceNumber = Guid.NewGuid().ToString();                       
                    }
                    catch (Exception ex)
                    {
                        m_Log.ErrorFormat("Failed to upload quote to {0}, because of {1}", InsurerName.Ids.Name(), ex.Message);
                        ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, InsurerName.Ids.Name(), dto.RequestId, ex, m_ChannelId));
                    }

                    return result;
                });

            return result;
        }

        public static string Upload(IIdsServiceClient client, string policyBinding)
        {
            return PolicyBindingTransfer.TransferPolicyBinding(client, policyBinding);
        }
        public static bool IsSuccessfulResult(string result)
        {
            const string searchString = "Success/True";
            return !string.IsNullOrEmpty(result) && result.IndexOf(searchString, 0, StringComparison.InvariantCultureIgnoreCase) != -1;
        }

        public IRetryStrategy RetryStrategy { get; private set; }
        public ExecutionPlan ExecutionPlan { get; private set; }
        public string ReferenceNumber { get; private set; }
    }
}
