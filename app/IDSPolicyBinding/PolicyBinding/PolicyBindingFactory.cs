﻿using System;
using System.Text;
using Common.Logging;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using iPlatform.Api.DTOs.PolicyBindings;
using Domain.PolicyBindings.Messages;

namespace Workflow.PolicyBinding.IDS.PolicyBinding
{
    public class PolicyBindingFactory
    {
        private static readonly ILog m_Log = LogManager.GetLogger<PolicyBindingFactory>();
        private readonly PolicyBindingRequestDto m_PolicyBindingRequestDto;
        private readonly StringBuilder m_PolicyBindingRequest;

        public PolicyBindingFactory(SubmitPolicyBindingMessage message)
        {
            m_PolicyBindingRequest = new StringBuilder();
            m_PolicyBindingRequestDto = message.Dto;
        }

        public string CreatePolicyBinding()
        {
            var validation = ValidateQuote();
            if (!string.IsNullOrEmpty(validation)) throw new Exception(string.Format("Validation for IDS failed with message: {0}", validation));

            try
            {
                if (m_PolicyBindingRequestDto.RequestInfo.Items.Count <= 0)
                {
                    return m_PolicyBindingRequest.ToString();
                }
                PassToRequestInfo();

                CreateHeader();
                CreatePolicy();
                CreateFooter();

            }
            catch (Exception ex)
            {
                m_Log.ErrorFormat("Creating IDS Policy Binding Request exception: {0} {1}", ex.Message, m_PolicyBindingRequest.ToString());
                throw;
            }
            return m_PolicyBindingRequest.ToString();
        }



        private void CreateFooter()
        {
            try
            {
                m_Log.Debug("Creating IDS Lead Header.");
                m_PolicyBindingRequest.AppendLine("</PayLoad>");
                m_PolicyBindingRequest.AppendLine("</STPCommand>");

            }
            catch (Exception ex)
            {
                m_Log.ErrorFormat("Creating IDS Lead Footer exception: {0}", ex.Message);
                throw;
            }
        }

        private void CreateHeader()
        {
            
            try
            {
                m_Log.Debug("Creating IDS Lead Header.");

                if (!string.IsNullOrEmpty(m_PolicyBindingRequest.ToString()))
                {
                    m_PolicyBindingRequest.Clear();
                }

                m_PolicyBindingRequest.AppendLine("<STPCommand>");
                m_PolicyBindingRequest.AppendLine("<Source>iPlatform</Source>");
                m_PolicyBindingRequest.AppendLine("<Function>Decompose</Function>");
                m_PolicyBindingRequest.AppendLine("<Parameters>");
                m_PolicyBindingRequest.AppendLine("<InterfaceTypeID>iPlatform_PolicyBinding</InterfaceTypeID>");
                m_PolicyBindingRequest.AppendLine("<RequestGuid>" + m_PolicyBindingRequestDto.QuotePlatformId + "</RequestGuid>");
                m_PolicyBindingRequest.AppendLine("</Parameters>");
                m_PolicyBindingRequest.AppendLine("<PayLoad>");
            }
            catch (Exception ex)
            {
                m_Log.ErrorFormat("Creating IDS Lead Header exception because {0}", ex, ex.Message);
                throw;
            }
        }

        private void CreatePolicy()
        {
            
            try
            {
                m_Log.Debug("Creating IDS Lead Policy.");
                var quoteToUpload = m_PolicyBindingRequestDto.RequestInfo.SerializeAsXml();
                m_PolicyBindingRequest.AppendLine(quoteToUpload.Replace(@"<?xml version=""1.0""?>", ""));
            }
            catch (Exception ex)
            {
                m_Log.ErrorFormat("Creating IDS Lead Policy exception because {0}", ex, ex.Message);
                throw;
            }
        }
        private void PassToRequestInfo()
        {
            m_PolicyBindingRequestDto.RequestInfo.BrokerUserId = m_PolicyBindingRequestDto.BrokerUserId;
            m_PolicyBindingRequestDto.RequestInfo.BrokerUserExternalReference = m_PolicyBindingRequestDto.BrokerUserExternalReference;
            m_PolicyBindingRequestDto.RequestInfo.AccountExecutiveUserId = m_PolicyBindingRequestDto.AccountExecutiveUserId;
            m_PolicyBindingRequestDto.RequestInfo.AccountExecutiveExternalReference = m_PolicyBindingRequestDto.AccountExecutiveExternalReference;
            m_PolicyBindingRequestDto.RequestInfo.ChannelId = m_PolicyBindingRequestDto.ChannelId;
            m_PolicyBindingRequestDto.RequestInfo.ChannelExternalReference = m_PolicyBindingRequestDto.ChannelExternalReference;
            m_PolicyBindingRequestDto.RequestInfo.ApiUrl = m_PolicyBindingRequestDto.ApiURL;
            m_PolicyBindingRequestDto.RequestInfo.Country = m_PolicyBindingRequestDto.Country;
            m_PolicyBindingRequestDto.RequestInfo.ChannelName = m_PolicyBindingRequestDto.ChannelInfo.Name;
            m_PolicyBindingRequestDto.RequestInfo.ChannelCode = m_PolicyBindingRequestDto.ChannelInfo.Code;
            m_PolicyBindingRequestDto.RequestInfo.InsuredInfo = m_PolicyBindingRequestDto.InsuredInfo;
        }

        private string ValidateQuote()
        {
            if (m_PolicyBindingRequestDto == null)
            {
                return "PolicyBindingRequestDto is null";
            }
            if (m_PolicyBindingRequestDto.InsuredInfo == null)
            {
                return "No Insured Info in PolicyBindingRequestDto";
            }
            if (m_PolicyBindingRequestDto.RequestInfo == null)
            {
                return "No Policy Info in PolicyBindingRequestDto";
            }
            if (m_PolicyBindingRequestDto.RequestInfo.Items == null)
            {
                return "No Items in PolicyBindingRequestDto";
            }
                
            return m_PolicyBindingRequestDto.RequestInfo.Items == null ? "No Items in PolicyBindingRequestDto" : "";
        }

    }
}
