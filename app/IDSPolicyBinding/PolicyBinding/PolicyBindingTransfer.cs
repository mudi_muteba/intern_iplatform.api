﻿using Workflow.PolicyBinding.IDS.IDSService;

namespace Workflow.PolicyBinding.IDSPolicyBinding.PolicyBinding
{
    public static class PolicyBindingTransfer
    {
        public static string TransferPolicyBinding(IIdsServiceClient client, string lead)
        {
            var response = client.Submit(lead);
            return response;
        }
    }
}
