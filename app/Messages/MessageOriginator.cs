﻿namespace Messages
{
    public class MessageOriginator
    {
        public string ApiKey { get; private set; }
        public string UserName { get; private set; }

        public MessageOriginator()
        {
        }

        public MessageOriginator(string apiKey, string userName)
        {
            ApiKey = apiKey;
            UserName = userName;
        }

        public override string ToString()
        {
            return ApiKey;
        }
    }
}