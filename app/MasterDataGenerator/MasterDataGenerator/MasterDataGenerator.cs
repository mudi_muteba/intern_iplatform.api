﻿using System;
using System.Collections.Generic;
using Common.Logging;
using MasterData;
using NHibernate;

namespace MasterDataGenerator
{
    public class MasterDataGenerator
    {
        private static readonly ILog _log = LogManager.GetLogger<MasterDataGenerator>();

        public static void Generate(ISession session, IReadOnlyCollection<BaseEntity> collection, bool useInMemoryDataBase = false)
        {
            _log.InfoFormat("Generating {0}", collection.GetType().Name);
            try
            {
                CreateMasterDataSchema(session, useInMemoryDataBase);
                foreach (var item in collection)
                    session.SaveOrUpdate(item);
                session.Flush();
            }
            catch (Exception exception)
            {
                _log.ErrorFormat("Error creating master data", exception);
                throw;
            }
            _log.InfoFormat("Generated {0}", collection.GetType().Name);
        }

        private static void CreateMasterDataSchema(ISession session, bool useInMemoryDataBase)
        {
            if (useInMemoryDataBase) return;
            var createMdSchemaSql = string.Format("IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = '{0}') BEGIN EXEC('CREATE SCHEMA [{0}]') END", "md");
            session.CreateSQLQuery(createMdSchemaSql).ExecuteUpdate();
        }
    }
}