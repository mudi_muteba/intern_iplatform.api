﻿using System;
using System.Collections.Generic;
using Api.Installers;
using Castle.Core;
using Castle.Windsor;
using Common.Logging;
using MasterData;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace MasterDataGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var log = LogManager.GetLogger<Program>();
            try
            {
                GenerateMasterData(log);
            }
            catch (Exception exception)
            {
                log.Error(exception);
                throw;
            }
        }

        protected static void ExportSchemaConfig(Configuration config)
        {
            SchemaMetadataUpdater.QuoteTableAndColumns(config);
            var update = new SchemaUpdate(config);
            update.Execute(false, true);
        }

        private static void GenerateMasterData(ILog log)
        {
            var container = new WindsorContainer();
            container.Kernel.ComponentModelCreated += OverrideContainerLifestyle;
            log.Info("Installing dependencies");
            container.Install(new DataAccessInstaller(), new MasterDataInstaller());
            log.Info("Dependencies installed");
            log.Info("Amending DB schema");
            var configuration = container.Resolve<Configuration>();
            container.Release(configuration);
            ExportSchemaConfig(configuration);
            log.Info("Schema Amended");

            var session = container.Resolve<ISession>();
            var collections = container.ResolveAll<iMasterDataCollection>();
            container.Release(collections);

            foreach (var collection in collections)
                MasterDataGenerator.Generate(session, collection as IReadOnlyCollection<BaseEntity>);

            container.Release(session);
        }

        private static void OverrideContainerLifestyle(ComponentModel model)
        {
            if (model.LifestyleType == LifestyleType.Undefined || model.LifestyleType == LifestyleType.PerWebRequest || model.LifestyleType == LifestyleType.Scoped)
                model.LifestyleType = LifestyleType.Transient;
        }
    }
}
