﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CustomerSatisfactionSurvey.EchoTCF.EchoTCFServiceReference
{
    public interface IEchoTCFClient
    {
        bool ReceiveBrokerXML(System.Xml.XmlNode xmlBroker, System.Guid tToken);
        Task<bool> ReceiveBrokerXMLAsync(XmlNode xmlBroker, Guid tToken);
    }

    public partial class wsReceiveXMLSoapClient : IEchoTCFClient
    {
        
    }

}
