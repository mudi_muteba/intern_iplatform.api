﻿using System;
using System.ServiceModel;
using System.Xml;
using Common.Logging;
using CustomerSatisfactionSurvey.Domain;
using CustomerSatisfactionSurvey.Domain.Data;
using CustomerSatisfactionSurvey.EchoTCF.EchoTCFServiceReference;
using Polly;
using Polly.CircuitBreaker;
using System.Diagnostics;
using Polly.Wrap;

namespace CustomerSatisfactionSurvey.EchoTCF
{
    public class EchoTCFService : ICustomerSatisfactionSurveyService
    {
        private readonly IEchoTCFClient _service;
        protected static readonly ILog Log = LogManager.GetLogger<EchoTCFService>();
        public EchoTCFService(string url)
        {
            _service = new wsReceiveXMLSoapClient(new BasicHttpBinding(), new EndpointAddress(url));
        }

        public void Send(CustomerSatisfactionSurveyRequest request)
        {
            try
            {
                Log.InfoFormat("Echo TCF: Validating Request");
                if (!request.Validate())
                {
                    request.LogErrors();
                    return;
                }

                var xmlRequest = new XmlDocument();
                xmlRequest.LoadXml(request.XmlRequest);

                SendRequest(xmlRequest, request.Token);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Echo TCF: Executing {0} (Message: {1}, InnerExeption: {2}, StackTrace: {3})",
                    this.GetType().ToString(), ex.Message, ex.InnerException.ToString(), ex.StackTrace.ToString());
            }
        }

        private void SendRequest(XmlDocument request, string token)
        {
            var waitAndRetryPolicy = Policy
                .Handle<Exception>(e => !(e is BrokenCircuitException))
                .WaitAndRetry(
                retryCount: 5,
                sleepDurationProvider: attempt => TimeSpan.FromMilliseconds(200),
                onRetry: (exception, calculatedWaitDuration) =>
                {
                    Log.ErrorFormat("Retry: {0}", exception.Message);
                });

            var circuitBreakerPolicy = Policy
                .Handle<Exception>()
                .CircuitBreaker(
                    exceptionsAllowedBeforeBreaking: 3,
                    durationOfBreak: TimeSpan.FromSeconds(10),
                    onBreak: (ex, breakDelay) =>
                    {
                        Log.ErrorFormat(".Breaker logging: Breaking the circuit for {0}ms!", breakDelay.TotalMilliseconds);
                        Log.ErrorFormat("..due to: {0}", ex.Message);
                    },
                    onReset: () => Log.InfoFormat(".Breaker logging: Call ok! Closed the circuit again!"),
                    onHalfOpen: () => Log.InfoFormat(".Breaker logging: Half-open: Next call is a trial!")
                );

            PolicyWrap policyWrap = Policy.Wrap(waitAndRetryPolicy, circuitBreakerPolicy);

            Stopwatch watch = new Stopwatch();
            watch.Start();

            try
            {
                policyWrap.Execute(() =>
                {
                    Log.InfoFormat("Echo TCF: Trying to send to call api");
                    if (_service.ReceiveBrokerXML(request, Guid.Parse(token)))
                        Log.InfoFormat("Echo TCF: Successful call");
                    else
                        throw new Exception("Echo TCF: Failed to complete call");
                    watch.Stop();
                });
            }
            catch (BrokenCircuitException b)
            {
                watch.Stop();
                Log.ErrorFormat("Request failed with: {0}. (after {1} ms)", b.GetType().Name, watch.ElapsedMilliseconds);
            }
            catch (Exception e)
            {
                watch.Stop();
                Log.ErrorFormat("Request failed with: {0}. (after {1} ms)", e.Message, watch.ElapsedMilliseconds);
            }

        }
    }
}
