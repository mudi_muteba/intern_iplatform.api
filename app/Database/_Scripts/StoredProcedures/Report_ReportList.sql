use ibroker
go

if exists (select * from sys.objects where object_id = object_id(N'Report_ReportList') and type in (N'P', N'PC'))
	begin
		drop procedure Report_ReportList
	end
go

create procedure Report_ReportList
as
	select
		Report.Id,
		Report.ReportCategoryId,
		Report.ReportTypeId,
		Report.ReportFormatId,
		Report.Name,
		Report.[Description],
		Report.SourceFile,
		Report.IsVisibleInReportViewer,
		Report.IsVisibleInScheduler,
		Report.IsActive,
		Report.VisibleIndex,
		Report.IconId,
		Report.Info
	from Report
	where
		Report.IsDeleted = 0 and
		ChannelId = (select top 1 Id from Channel where Code = 'IP')
	order by
		Report.Name
go

exec Report_ReportList