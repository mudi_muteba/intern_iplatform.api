use ibroker
go

if exists (select * from sys.objects where object_id = object_id(N'Report_Layout_ByChannelId') and type in (N'P', N'PC'))
	begin
		drop procedure Report_Layout_ByChannelId
	end
go

create procedure Report_Layout_ByChannelId
(
	@ChannelId int,
	@ReportName varchar(50)
)
as
	if (select count(*) from ReportChannel where ChannelId = @ChannelId) > 0
		begin
			select
				ReportSection.Name SectionName,
				ReportSection.Label SectionLabel,
				ReportSection.IsVisible SectionVisible,
				ReportLayout.Name FieldName,
				ReportChannelLayout.Label FieldLabel,
				ReportChannelLayout.[Value] FieldValue,
				ReportChannelLayout.IsVisible FieldVisible
			from Report
				inner join ReportChannel on ReportChannel.ReportId = Report.Id
				inner join ReportChannelLayout on ReportChannelLayout.ReportChannelId = ReportChannel.Id
				inner join ReportLayout on ReportLayout.Id = ReportChannelLayout.ReportLayoutId
				inner join ReportSection on ReportSection.Id = ReportLayout.ReportSectionId
			where
				ReportChannel.ChannelId = @ChannelId and
				Report.Name = @ReportName
			order by
				ReportChannelLayout.Id
		end
	else
		begin
			select
				ReportSection.Name SectionName,
				ReportSection.Label SectionLabel,
				ReportSection.IsVisible SectionVisible,
				ReportLayout.Name FieldName,
				ReportChannelLayout.Label FieldLabel,
				ReportChannelLayout.[Value] FieldValue,
				ReportChannelLayout.IsVisible FieldVisible
			from Report
				inner join ReportChannel on ReportChannel.ReportId = Report.Id
				inner join ReportChannelLayout on ReportChannelLayout.ReportChannelId = ReportChannel.Id
				inner join ReportLayout on ReportLayout.Id = ReportChannelLayout.ReportLayoutId
				inner join ReportSection on ReportSection.Id = ReportLayout.ReportSectionId
			where
				ReportChannel.ChannelId = (select top 1 Id from Channel where Code = 'IP') and
				Report.Name = @ReportName
			order by
				ReportChannelLayout.Id
		end
go

exec Report_Layout_ByChannelId 26, 'Comparative Quote'