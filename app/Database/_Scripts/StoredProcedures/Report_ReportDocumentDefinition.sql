if exists (select * from sys.objects where object_id = object_id(N'Report_ReportDocumentDefinition') and type in (N'P', N'PC'))
	begin
		drop procedure Report_ReportDocumentDefinition
	end
go

create procedure Report_ReportDocumentDefinition
(
	@ReportId int
)
as
	select ReportId, DocumentDefinitionId
	from ReportDocumentDefinition
	where
		(
			IsDeleted is null or
			IsDeleted = 0
		) and
		ReportId = @ReportId

go