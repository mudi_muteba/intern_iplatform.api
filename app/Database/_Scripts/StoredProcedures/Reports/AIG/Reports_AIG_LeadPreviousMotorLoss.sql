if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_LeadPreviousMotorLoss') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_AIG_LeadPreviousMotorLoss
	end
go
Create procedure Reports_AIG_LeadPreviousMotorLoss
--(
--	--@PartyId int
--)
as
	select * from (	
		SELECT
			ROW_NUMBER() OVER(PARTITION BY mlh.PartyId  ORDER BY mlh.PartyId) AS Row , 
			mlh.PartyId,'Auto' as [Type], mlh.DateOfLoss, mtl.Answer as MotorLossType, mca.Answer as MotorLossClaim from MotorLossHistory as mlh
			join md.MotorTypeOfLoss as mtl on mtl.id = mlh.MotorTypeOfLossId
			join md.MotorClaimAmount as mca on mca.Id = mlh.MotorClaimAmountId
			where mlh.IsDeleted = 0 ) as T
	where t.Row < = 10