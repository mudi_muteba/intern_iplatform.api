if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_FuneralExtract') and type in (N'P', N'PC'))
begin
	drop procedure Reports_AIG_FuneralExtract
end
go

create procedure Reports_AIG_FuneralExtract
(
--@PartyId int,
@StartDate nvarchar(50),
@EndDate nvarchar(50)
)
as

----declare @PartyId int
--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
----set @PartyId = 1268 
--set @StartDate = '2016-07-01'
--set @EndDate = '2016-09-30'

DECLARE @tbl TABLE(ProposalHeaderId int, ProposalDefinitionId int, PartyId int, QuestionId int, CoverId int, DisplayName nvarchar(255),Answer nvarchar(255), CoverDefinitionId int, QuoteId int)

insert into @tbl

				select ph.Id as ProposalHeaderId, pd.id as ProposalDefinitionId, pd.PartyId as PartyId,qd.QuestionId,pd.CoverId,qd.DisplayName,pqa.Answer, qi.CoverDefinitionId, q.id as QuoteId from ProposalHeader as ph --qa.Answer, 
				join ProposalDefinition as pd on pd.ProposalHeaderId = ph.Id
				join QuoteHeader as qh on qh.ProposalHeaderId =pd.ProposalHeaderId
				join Quote as q on q.QuoteHeaderId = qh.Id
				join QuoteItem as qi on qi.QuoteId = q.Id
				join ProposalQuestionAnswer as pqa on pd.Id = pqa.ProposalDefinitionId
				join QuestionDefinition as qd on qd.Id = pqa.QuestionDefinitionId
				join md.Question as quest on quest.Id = qd.QuestionId					
				where ph.IsDeleted = 0
				and pd.IsDeleted =0
				and q.IsDeleted =0
				and pd.CoverId = 142			
				order by qd.DisplayName


Select 
	
	(select [user].UserName from LeadActivity as la
	join [user] on [user].Id = la.UserId
	where la.Id = prh.LeadActivityId
	) as AgentName,

	CASE 
		WHEN Prh.Source IS NULL THEN ('Call center')   
		ELSE Prh.Source                                     
    END as Channel, 

	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'Date of Quote',
	CASE
		WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y'
		ELSE 'N'
	END as 'QuoteExpired',

	CASE 
	WHEN PhS.Id IS NULL THEN ('N')   
	WHEN PhS.Id = 1 THEN ('N')
	WHEN PhS.Id = 2 THEN ('N')
	WHEN PhS.Id = 3 THEN ('Y')                                         
	END AS 'Quote-Bound',

	case when Qute.AcceptedOn IS NULL Then '' else CONVERT(VARCHAR(10),Qute.AcceptedOn ,111) end as 'QuoteBindDate',

	(select cast(round (Premium ,2) as numeric(36,2)) from QuoteItem as Qi		
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
		where qi.Id = QteI.Id and cd.CoverId in (218, 17, 45, 142, 373, 372, 258) 
	) as 'QuoteTotalPremium',

 	CASE WHEN (select Premium from QuoteItem as Qi		
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
		where --QuoteId = Qute.Id and 
		cd.CoverId = 218 and qi.Id = QteI.Id
	 ) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId = 218 and qi.Id = QteI.Id)                                          
     END AS 'QuoteMotorPremium',
	 
	 CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (17,45) and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select  cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (17,45) and qi.Id = QteI.Id)                                          
     END AS 'QuoteHomePremium',

	CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId = 142 and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId = 142 and qi.Id = QteI.Id)                                          
     END AS 'QuoteFuneralPremium',	

	CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (373, 372, 258) and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (373, 372, 258) and qi.Id = QteI.Id)                                          
     END as 'QuoteOtherPremium',

	(select top 1 CONVERT(VARCHAR(10),InceptionDate ,111) from PaymentDetails where PartyId = p.Id and QuoteId = qute.Id order by Id desc) as 'QuoteIncepptionDate',
	ph.PolicyNo as 'QuotePolicyNumber',
	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'Quote-CreateDate',
	CONVERT(VARCHAR(10),Qute.CreatedAt,108) as 'Quote-CreateTime',
	l.Id as 'LeadText',

		QteI.AssetNumber as 'PropertyNumber',			

		(select Line1 from address where id in (select top 1 Answer from @tbl as temp		
		where temp.QuestionId in (88) and PartyId = prd.PartyId and CoverDefinitionId = QteI.CoverDefinitionId and QuoteId = Qute.id and ProposalDefinitionId = prd.Id and ProposalHeaderId = Prh.Id 
		order by QuoteId desc)) as 'Address-Suburb',

		(select Code from address where id in (select top 1 Answer from @tbl as temp		
		where temp.QuestionId in (88) and PartyId = prd.PartyId and CoverDefinitionId = QteI.CoverDefinitionId and QuoteId = Qute.id and ProposalDefinitionId = prd.Id and ProposalHeaderId = Prh.Id 
		order by QuoteId desc)) as 'Address-PostCode',

		(select md.StateProvince.Name from address 
		join md.StateProvince on md.StateProvince.Id = address.StateProvinceId
		where [address].id in (select top 1 Answer from @tbl as temp		
		where temp.QuestionId in (88) and PartyId = prd.PartyId and CoverDefinitionId = QteI.CoverDefinitionId and QuoteId = Qute.id and ProposalDefinitionId = prd.Id and ProposalHeaderId = Prh.Id 
		order by QuoteId desc)) as 'Address-Province',

		(select top 1 qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.Answer = qa.Id
		where temp.QuestionId in (158) and PartyId = prd.PartyId and CoverDefinitionId = QteI.CoverDefinitionId and QuoteId = Qute.id and ProposalDefinitionId = prd.Id and ProposalHeaderId = Prh.Id 
		order by QuoteId desc) as 'CoverType',

		(select top 1 qa.Answer from @tbl as temp	
		join md.QuestionAnswer as qa on temp.Answer = qa.Id
		where temp.QuestionId in (830) and PartyId = prd.PartyId and CoverDefinitionId = QteI.CoverDefinitionId and QuoteId = Qute.id and ProposalDefinitionId = prd.Id and ProposalHeaderId = Prh.Id 
		order by QuoteId desc) as 'Occupation',

		(select top 1 Answer from @tbl as temp	
		where temp.QuestionId in (833) and PartyId = prd.PartyId and CoverDefinitionId = QteI.CoverDefinitionId and QuoteId = Qute.id and ProposalDefinitionId = prd.Id and ProposalHeaderId = Prh.Id 
		order by QuoteId desc) as 'BeneficiaryIDNumber',

		(select top 1 Answer from @tbl as temp
		where temp.QuestionId in (831) and PartyId = prd.PartyId and CoverDefinitionId = QteI.CoverDefinitionId and QuoteId = Qute.id and ProposalDefinitionId = prd.Id and ProposalHeaderId = Prh.Id 
		order by QuoteId desc) as 'BeneficiaryFirstname',

		(select top 1 Answer from @tbl as temp
		where temp.QuestionId in (832) and PartyId = prd.PartyId and CoverDefinitionId = QteI.CoverDefinitionId and QuoteId = Qute.id and ProposalDefinitionId = prd.Id and ProposalHeaderId = Prh.Id 
		order by QuoteId desc) as 'Beneficiary Surname',

		(select top 1 Answer from @tbl as temp	
		where temp.QuestionId in (834) and PartyId = prd.PartyId and CoverDefinitionId = QteI.CoverDefinitionId and QuoteId = Qute.id and ProposalDefinitionId = prd.Id and ProposalHeaderId = Prh.Id 
		order by QuoteId desc) as 'BeneficiaryContactNumber',

		(select top 1 Answer from @tbl as temp		
		where temp.QuestionId in (835) and PartyId = prd.PartyId and CoverDefinitionId = QteI.CoverDefinitionId and QuoteId = Qute.id and ProposalDefinitionId = prd.Id and ProposalHeaderId = Prh.Id 
		order by QuoteId desc) as 'BeneficiaryRelationship',		

		cast(round(QteI.Premium,2) as numeric(36,2)) as 'PremiumTotal', 
		prd.Id as 'ProposalDefinitionId'


from Party as P
join Lead as L on l.PartyId = p.Id
join Channel as Ch on ch.Id = p.ChannelId
join ProposalHeader as Prh on Prh.PartyId = p.Id 
join ProposalDefinition as prd on Prd.ProposalHeaderId = Prh.Id
join QuoteHeader as Qh on Qh.ProposalHeaderId = Prh.Id
join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
join QuoteItem as QteI on QteI.QuoteId = Qute.Id
join CoverDefinition as covD on covD.Id = QteI.CoverDefinitionId
left join PolicyHeader as Ph on Ph.QuoteId = Qute.Id
left join md.PolicyStatus as PhS on Ph.PolicyStatusId = PhS.Id
join QuoteAcceptedLeadActivity as qala on qala.QuoteId = Qute.Id
join LeadActivity as la on la.Id = qala.LeadActivityId
where Qute.Id is not null
and L.IsDeleted = 0
and Qute.IsDeleted = 0
--and p.Id = 2005
and prd.IsDeleted = 0
and prd.CoverId = 142
and QteI.CoverDefinitionId in (select id from CoverDefinition where CoverId = 142)
--and ph.DateEffective >= @StartDate
--and ph.DateRenewal <= @EndDate
and la.DateCreated >= @StartDate
and la.DateUpdated <= @EndDate
and la.ActivityTypeId = 5
and Qh.IsDeleted = 0
--and Prh.IsQuoted = 1
order by AgentName


