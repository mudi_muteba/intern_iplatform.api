if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_DisasterCashExtract') and type in (N'P', N'PC'))
begin
	drop procedure Reports_AIG_DisasterCashExtract
end
go

create procedure Reports_AIG_DisasterCashExtract
(
--@PartyId int
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as


--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
--set @StartDate = '2016-09-09'
--set @EndDate = '2016-09-17'

DECLARE @tbl TABLE(QuestionId int, QuoteId int, AnswerId nvarchar(255), PartyId int, Created datetime, ProposalHeaderId int, ProposalDefinitionId int)

insert into @tbl
select qd.QuestionId, qte.Id as QuoteId,pqa.Answer as AnswerId, pd.PartyId, pd.Created, pd.ProposalHeaderId, pd.Id from ProposalDefinition as pd
				join ProposalQuestionAnswer as pqa on pqa.ProposalDefinitionId = pd.Id
				join QuestionDefinition as qd on qd.Id = pqa.QuestionDefinitionId
				join md.Question as q on q.Id = qd.QuestionId
				join ProposalHeader as ph on ph.Id = pd.ProposalHeaderId
				join QuoteHeader as qh on qh.ProposalHeaderId = ph.Id
				join Quote as qte on qte.QuoteHeaderId = qh.Id			
				where 
				pd.CoverId = 372 and pd.IsDeleted = 0 and qte.IsDeleted =0 and qd.id = pqa.QuestionDefinitionId and qd.QuestionId in (89,90,47,488)


Select 

	
	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'Quote-CreateDate',
	CONVERT(VARCHAR(10),Qute.CreatedAt,108) as 'Quote-CreateTime',
	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'Date of Quote',	

	ph.PolicyNo as 'QuotePolicyNumber',

	l.Id as 'LeadText',


	(select  AnswerId from @tbl where QuestionId = 89 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)  as 'Address-Suburb',
	(select  AnswerId from @tbl where QuestionId = 90 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)  as 'Address-PostCode',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 47 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'Address-Province',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 488 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)  as 'CashLimit',

	cast(round(QteI.Premium,2) as numeric(36,2)) as 'Premium'

from Party as P
join Lead as L on l.PartyId = p.Id
left join ProposalHeader as Prh on Prh.PartyId = p.Id
left join ProposalDefinition as prd on Prd.ProposalHeaderId = Prh.Id
left join QuoteHeader as Qh on qh.ProposalHeaderId = Prh.Id
left join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
left join PolicyHeader as Ph on Ph.QuoteId = Qute.Id
left join md.PolicyStatus as PhS on Ph.PolicyStatusId = PhS.Id
left join QuoteItem as QteI on QteI.QuoteId = Qute.Id
join QuoteAcceptedLeadActivity as qala on qala.QuoteId = Qute.Id
join LeadActivity as la on la.Id = qala.LeadActivityId
where Qute.Id is not null
and L.IsDeleted = 0
and Qute.IsDeleted = 0
and prd.IsDeleted = 0
and prd.CoverId = 372
and la.DateCreated >= @StartDate
and la.DateUpdated <= @EndDate
and la.ActivityTypeId = 5
and Qh.IsDeleted = 0
and QteI.CoverDefinitionId in (select Id from CoverDefinition where CoverId = 372)
order by p.DisplayName



--select * from  Party as P
--join Lead as L on l.PartyId = p.Id
--join ProposalHeader as Prh on Prh.PartyId = p.Id
--join ProposalDefinition as prd on Prd.ProposalHeaderId = Prh.Id
--join QuoteHeader as Qh on qh.ProposalHeaderId = Prh.Id
--join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
--join QuoteItem as QteI on QteI.QuoteId = Qute.Id
--where l.Id = 1970

--select * from lead where id = 1970
--select * from ProposalHeader where PartyId = 3412 and Id = 4175
--select * from ProposalDefinition where ProposalHeaderId in (4175) and CoverId = 258
--select * from QuoteHeader where ProposalHeaderId in (4175) 
--select * from quote where QuoteHeaderid = 3892
--select * from QuoteItem where quoteid = 7320 and CoverDefinitionId in (select Id from CoverDefinition where CoverId=258)

--select * from QuoteAcceptedLeadActivity where QuoteId = 7320