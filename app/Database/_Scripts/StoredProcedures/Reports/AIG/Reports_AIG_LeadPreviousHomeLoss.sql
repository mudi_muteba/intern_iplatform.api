if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_LeadPreviousHomeLoss') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_AIG_LeadPreviousHomeLoss
	end
go
Create procedure Reports_AIG_LeadPreviousHomeLoss
--(
--	@PartyId int
--)
as
	SELECT ROW_NUMBER() OVER(ORDER BY hlh.id DESC) AS Row, hlh.PartyId,'Home' as [Type], hlh.DateOfLoss, htl.Answer as HomeLossType, hca.Answer as HomeLossClaim, hcl.Answer as Location from HomeLossHistory as hlh
	join md.HomeTypeOfLoss as htl on htl.id = hlh.HomeTypeOfLossId
	join md.HomeClaimAmount as hca on hca.Id = hlh.HomeClaimAmountId
	join md.HomeClaimLocation as hcl on hcl.Id = hlh.HomeClaimLocationId
	where --PartyId = @PartyId and 
	hlh.IsDeleted = 0