use ibroker
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_OutboundRatios_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_OutboundRatios_Header
	end
go

create procedure Report_CallCentre_OutboundRatios_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
				inner join Campaign on Campaign.ChannelId = Channel.Id
			where
				Campaign.Id = @CampaignId and
				Campaign.IsDeleted = 0 and
				Channel.IsDeleted = 0
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_OutboundRatios_GetAgentLeadStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_OutboundRatios_GetAgentLeadStatistics
	end
go

create procedure Report_CallCentre_OutboundRatios_GetAgentLeadStatistics
(
	@CampaignID int,
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(@CampaignId > 0)
		begin
			;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
			as 
			(
				select distinct
					[User].Id AgentId,
					Campaign.Id CampaignId,
					Individual.Surname + ', ' + Individual.FirstName Agent,
					Campaign.Name Campaign
				from Campaign
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					Campaign.Id = @CampaignID and
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo
				group by
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName,
					Campaign.Name
				)
			select
				Agent,
				Campaign,
				(
					select
						cast(count(*) as numeric(12, 2))
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId
				) LeadToSales,
				(
					select
						cast(count(*) as numeric(12, 2))
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (5, 6, 7, 8, 9, 10, 11, 12, 13, 14)
				) ContactedToSales,
				(
					select
						cast(count(*) as numeric(12, 2))
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (15, 16, 17, 18, 19, 20, 21, 22)
				) ContactedToLeadsWorked,
				(
					select
						cast(count(*) as numeric(12, 2))
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (7, 10, 9, 11, 20, 21)
				) SalesToPresentation,
				(
					select
						cast(count(*) as numeric(12, 2))
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (5)
				) PresentationToLeads
			from
				Agents_CTE
			order by
				Agent asc,
				Campaign asc
		end
	else
		begin
			;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
			as 
			(
				select distinct
					[User].Id AgentId,
					Campaign.Id CampaignId,
					Individual.Surname + ', ' + Individual.FirstName Agent,
					Campaign.Name Campaign
				from Campaign
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo
				group by
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName,
					Campaign.Name
				)
			select
				Agent,
				Campaign,
				(
					select
						cast(count(*) as numeric(12, 2))
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId
				) LeadToSales,
				(
					select
						cast(count(*) as numeric(12, 2))
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (5, 6, 7, 8, 9, 10, 11, 12, 13, 14)
				) ContactedToSales,
				(
					select
						cast(count(*) as numeric(12, 2))
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (15, 16, 17, 18, 19, 20, 21, 22)
				) ContactedToLeadsWorked,
				(
					select
						cast(count(*) as numeric(12, 2))
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (7, 10, 9, 11, 20, 21)
				) SalesToPresentation,
				(
					select
						cast(count(*) as numeric(12, 2))
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (5)
				) PresentationToLeads
			from
				Agents_CTE
			order by
				Agent asc,
				Campaign asc
		end
go

exec Report_CallCentre_OutboundRatios_Header 4
exec Report_CallCentre_OutboundRatios_GetAgentLeadStatistics 4, '2000-01-01', '2016-12-31'

go