use ibroker
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_ManagerLevelQuoteUpload_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_ManagerLevelQuoteUpload_Header
	end
go

create procedure Report_CallCentre_ManagerLevelQuoteUpload_Header
(
	@ChannelId int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Id) from Channel where Id = @ChannelId and IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @ChannelId
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_ManagerLevelQuoteUpload_GetUploadStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_ManagerLevelQuoteUpload_GetUploadStatistics
	end
go

create procedure Report_CallCentre_ManagerLevelQuoteUpload_GetUploadStatistics
(
	@ChannelIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select 1
go

exec Report_CallCentre_ManagerLevelQuoteUpload_Header 7
exec Report_CallCentre_ManagerLevelQuoteUpload_GetUploadStatistics '19', '2017-01-01', '2017-01-31'

go