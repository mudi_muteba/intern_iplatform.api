use ibroker
go



--Report_CallCentre_BrokerQuoteUpload_Header
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_Header_ChannelExists') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_Header_ChannelExists
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_Header_ChannelExists
(
	@ChannelId int
)
as
	select top 1
		Code
		, ''
	from Channel
	where
		Id = @ChannelId and
		IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_Header_ChannelDoesNotExist') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_Header_ChannelDoesNotExist
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_Header_ChannelDoesNotExist
(
	@ChannelId int
)
as
	select top 1
		Code
		, ''
	from Channel
	where
		Id = @ChannelId and
		IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_Header
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_Header
(
	@ChannelId int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Id) from Channel where Id = @ChannelId and IsDeleted = 0) > 0)
		begin
			exec Report_CallCentre_BrokerQuoteUpload_Header_ChannelExists @ChannelId
		end
	else
		begin
			exec Report_CallCentre_BrokerQuoteUpload_Header_ChannelDoesNotExist @IpChannelId
		end
go



--Report_CallCentre_BrokerQuoteUpload_GetSummary
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetSummary_WithChannelIds_WithInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetSummary_WithChannelIds_WithInsurerIds
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetSummary_WithChannelIds_WithInsurerIds
(
	@ChannelIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	;with Summary_CTE (ChannelId, AgentId, Channel, Agent)
	as 
	(
		select distinct
			Channel.Id,
			[User].Id,
			Channel.Name,
			Individual.Surname + ', ' + Individual.FirstName
		from QuoteHeader
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
			inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
			inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			inner join Channel on Channel.Id = Campaign.ChannelId
			inner join [User] on [User].Id = LeadActivity.UserId
			inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
			inner join UserIndividual on UserIndividual.UserId = [User].Id
			inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		where
			Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
			Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0
		group by
			Channel.Id,
			[User].Id,
			Channel.Name,
			Individual.Surname + ', ' + Individual.FirstName
		)
	select
		Channel BrokerName,
		Agent,
		(
			select
				isnull(count(distinct(LeadActivity.LeadId)), 0)
			from LeadActivity
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo
		) LeadsCount,
		(
			select
				isnull(count(distinct(LeadActivity.Id)), 0)
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) NoOfQuotes,
		(
			select
				isnull(count(distinct(Quote.Id)), 0)
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Quote.Id in
				(
					select
						QuoteId
					from QuoteUploadLog
					where
						QuoteUploadLog.QuoteId = Quote.Id and
						QuoteUploadLog.IsDeleted = 0
				) and
				Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) NoOfQuotesUploaded,
		(
			isnull(
				cast
				(
					(
						select
							cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
						from LeadActivity
							inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
						where
							Quote.Id in
							(
								select
									QuoteId
								from QuoteUploadLog
								where
									QuoteUploadLog.QuoteId = Quote.Id and
									QuoteUploadLog.IsDeleted = 0
							) and
							Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
							Campaign.ChannelId = Summary_CTE.ChannelId and
							LeadActivity.UserId = Summary_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)

					/

					(
						select
							nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
						from LeadActivity
							inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
						where
							Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
							Campaign.ChannelId = Summary_CTE.ChannelId and
							LeadActivity.UserId = Summary_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)

					* 100

					as numeric( 18, 2)
				)
			, 0)
		) Closing,
		(
			select
				cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2))
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Quote.Id in
				(
					select
						QuoteId
					from QuoteUploadLog
					where
						QuoteUploadLog.QuoteId = Quote.Id and
						QuoteUploadLog.IsDeleted = 0
				) and
				Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) LeadsSPV
	from
		Summary_CTE
	order by
		BrokerName,
		Agent
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetSummary_WithChannelIds_WithoutInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetSummary_WithChannelIds_WithoutInsurerIds
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetSummary_WithChannelIds_WithoutInsurerIds
(
	@ChannelIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	;with Summary_CTE (ChannelId, AgentId, Channel, Agent)
	as 
	(
		select distinct
			Channel.Id,
			[User].Id,
			Channel.Name,
			Individual.Surname + ', ' + Individual.FirstName
		from QuoteHeader
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
			inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
			inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			inner join Channel on Channel.Id = Campaign.ChannelId
			inner join [User] on [User].Id = LeadActivity.UserId
			inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
			inner join UserIndividual on UserIndividual.UserId = [User].Id
			inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		where
			Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
			--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0
		group by
			Channel.Id,
			[User].Id,
			Channel.Name,
			Individual.Surname + ', ' + Individual.FirstName
		)
	select
		Channel BrokerName,
		Agent,
		(
			select
				isnull(count(distinct(LeadActivity.LeadId)), 0)
			from LeadActivity
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo
		) LeadsCount,
		(
			select
				isnull(count(distinct(LeadActivity.Id)), 0)
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) NoOfQuotes,
		(
			select
				isnull(count(distinct(Quote.Id)), 0)
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Quote.Id in
				(
					select
						QuoteId
					from QuoteUploadLog
					where
						QuoteUploadLog.QuoteId = Quote.Id and
						QuoteUploadLog.IsDeleted = 0
				) and
				--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) NoOfQuotesUploaded,
		(
			isnull(
				cast
				(
					(
						select
							cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
						from LeadActivity
							inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
						where
							Quote.Id in
							(
								select
									QuoteId
								from QuoteUploadLog
								where
									QuoteUploadLog.QuoteId = Quote.Id and
									QuoteUploadLog.IsDeleted = 0
							) and
							--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
							Campaign.ChannelId = Summary_CTE.ChannelId and
							LeadActivity.UserId = Summary_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)

					/

					(
						select
							nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
						from LeadActivity
							inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
						where
							--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
							Campaign.ChannelId = Summary_CTE.ChannelId and
							LeadActivity.UserId = Summary_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)

					* 100

					as numeric( 18, 2)
				)
			, 0)
		) Closing,
		(
			select
				cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2))
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Quote.Id in
				(
					select
						QuoteId
					from QuoteUploadLog
					where
						QuoteUploadLog.QuoteId = Quote.Id and
						QuoteUploadLog.IsDeleted = 0
				) and
				--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) LeadsSPV
	from
		Summary_CTE
	order by
		BrokerName,
		Agent
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithInsurerIds
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithInsurerIds
(
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	;with Summary_CTE (ChannelId, AgentId, Channel, Agent)
	as 
	(
		select distinct
			Channel.Id,
			[User].Id,
			Channel.Name,
			Individual.Surname + ', ' + Individual.FirstName
		from QuoteHeader
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
			inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
			inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			inner join Channel on Channel.Id = Campaign.ChannelId
			inner join [User] on [User].Id = LeadActivity.UserId
			inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
			inner join UserIndividual on UserIndividual.UserId = [User].Id
			inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		where
			--Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
			Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0
		group by
			Channel.Id,
			[User].Id,
			Channel.Name,
			Individual.Surname + ', ' + Individual.FirstName
		)
	select
		Channel BrokerName,
		Agent,
		(
			select
				isnull(count(distinct(LeadActivity.LeadId)), 0)
			from LeadActivity
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo
		) LeadsCount,
		(
			select
				isnull(count(distinct(LeadActivity.Id)), 0)
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) NoOfQuotes,
		(
			select
				isnull(count(distinct(Quote.Id)), 0)
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Quote.Id in
				(
					select
						QuoteId
					from QuoteUploadLog
					where
						QuoteUploadLog.QuoteId = Quote.Id and
						QuoteUploadLog.IsDeleted = 0
				) and
				Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) NoOfQuotesUploaded,
		(
			isnull(
				cast
				(
					(
						select
							cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
						from LeadActivity
							inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
						where
							Quote.Id in
							(
								select
									QuoteId
								from QuoteUploadLog
								where
									QuoteUploadLog.QuoteId = Quote.Id and
									QuoteUploadLog.IsDeleted = 0
							) and
							Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
							Campaign.ChannelId = Summary_CTE.ChannelId and
							LeadActivity.UserId = Summary_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)

					/

					(
						select
							nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
						from LeadActivity
							inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
						where
							Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
							Campaign.ChannelId = Summary_CTE.ChannelId and
							LeadActivity.UserId = Summary_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)

					* 100

					as numeric( 18, 2)
				)
			, 0)
		) Closing,
		(
			select
				cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2))
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Quote.Id in
				(
					select
						QuoteId
					from QuoteUploadLog
					where
						QuoteUploadLog.QuoteId = Quote.Id and
						QuoteUploadLog.IsDeleted = 0
				) and
				Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) LeadsSPV
	from
		Summary_CTE
	order by
		BrokerName,
		Agent
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithoutInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithoutInsurerIds
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithoutInsurerIds
(
	@DateFrom datetime,
	@DateTo datetime
)
as
	;with Summary_CTE (ChannelId, AgentId, Channel, Agent)
	as 
	(
		select distinct
			Channel.Id,
			[User].Id,
			Channel.Name,
			Individual.Surname + ', ' + Individual.FirstName
		from QuoteHeader
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
			inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
			inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			inner join Channel on Channel.Id = Campaign.ChannelId
			inner join [User] on [User].Id = LeadActivity.UserId
			inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
			inner join UserIndividual on UserIndividual.UserId = [User].Id
			inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		where
			--Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
			--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0
		group by
			Channel.Id,
			[User].Id,
			Channel.Name,
			Individual.Surname + ', ' + Individual.FirstName
		)
	select
		Channel BrokerName,
		Agent,
		(
			select
				isnull(count(distinct(LeadActivity.LeadId)), 0)
			from LeadActivity
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo
		) LeadsCount,
		(
			select
				isnull(count(distinct(LeadActivity.Id)), 0)
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) NoOfQuotes,
		(
			select
				isnull(count(distinct(Quote.Id)), 0)
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Quote.Id in
				(
					select
						QuoteId
					from QuoteUploadLog
					where
						QuoteUploadLog.QuoteId = Quote.Id and
						QuoteUploadLog.IsDeleted = 0
				) and
				--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) NoOfQuotesUploaded,
		(
			isnull(
				cast
				(
					(
						select
							cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
						from LeadActivity
							inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
						where
							Quote.Id in
							(
								select
									QuoteId
								from QuoteUploadLog
								where
									QuoteUploadLog.QuoteId = Quote.Id and
									QuoteUploadLog.IsDeleted = 0
							) and
							--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
							Campaign.ChannelId = Summary_CTE.ChannelId and
							LeadActivity.UserId = Summary_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)

					/

					(
						select
							nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
						from LeadActivity
							inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
						where
							--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
							Campaign.ChannelId = Summary_CTE.ChannelId and
							LeadActivity.UserId = Summary_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)

					* 100

					as numeric( 18, 2)
				)
			, 0)
		) Closing,
		(
			select
				cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2))
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Quote.Id in
				(
					select
						QuoteId
					from QuoteUploadLog
					where
						QuoteUploadLog.QuoteId = Quote.Id and
						QuoteUploadLog.IsDeleted = 0
				) and
				--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) LeadsSPV
	from
		Summary_CTE
	order by
		BrokerName,
		Agent
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetSummary') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetSummary
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetSummary
(
	@ChannelIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@ChannelIds) > 0)
		begin
			if(len(@InsurerIds) > 0)
				begin
					exec Report_CallCentre_BrokerQuoteUpload_GetSummary_WithChannelIds_WithInsurerIds @ChannelIds, @InsurerIds, @DateFrom, @DateTo
				end
			else
				begin
					exec Report_CallCentre_BrokerQuoteUpload_GetSummary_WithChannelIds_WithoutInsurerIds @ChannelIds, @DateFrom, @DateTo
				end
		end
	else
		begin
			if(len(@InsurerIds) > 0)
				begin
					exec Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithInsurerIds @InsurerIds, @DateFrom, @DateTo
				end
			else
				begin
					exec Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithoutInsurerIds @DateFrom, @DateTo
				end
		end
go



--Report_CallCentre_BrokerQuoteUpload_GetQuotes
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithChannelIds_WithInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithChannelIds_WithInsurerIds
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithChannelIds_WithInsurerIds
(
	@ChannelIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Channel.Name [BrokerName],
		Campaign.Name [Campaign],
		Customer.FirstName + ' ' + Customer.Surname [CustomerName],
		Customer.IdentityNo [CustomerId],
		isnull(
		(
			select top 1
				InsurerReference
			from QuoteUploadLog
			where
				QuoteUploadLog.QuoteId = Quote.Id and
				QuoteUploadLog.IsDeleted = 0
			order by
				QuoteUploadLog.DateCreated desc
		), 'Not Available') QuoteNumber,
		Individual.FirstName + ' ' + Individual.Surname [ContactCentreAgent],
		QuoteHeader.CreatedAt [QuoteCreateDate],
		(
			select
				cast(sum(qi.Premium) as numeric(18, 2))
			from QuoteItem qi
			where
				qi.QuoteId = Quote.Id
		) [QuotedPremium],
		case when exists
			(
				select
					QuoteId
				from QuoteUploadLog
				where
					QuoteUploadLog.QuoteId = Quote.Id and
					QuoteUploadLog.IsDeleted = 0
			)
			then 'Submitted'
			else 'Not Submitted'
		end [Status],
		case when exists
			(
				select
					QuoteUploadLog.QuoteId
				from QuoteUploadLog
					inner join Quote q on q.Id = QuoteUploadLog.QuoteId
					inner join Product on Product.Id = q.ProductId
				where
					QuoteUploadLog.QuoteId = Quote.Id and
					QuoteUploadLog.IsDeleted = 0
			)
			then 
			(
				select
					(
						select top 1
							'Taken up with insurer: Lead uploaded to ' + o.TradingName
						from QuoteHeader qhqh
							inner join Quote qq on qq.QuoteHeaderId = qhqh.Id
							inner join Product pp on pp.Id = qq.ProductId
							inner join Organization oo on oo.PartyId = pp.ProductOwnerId
							inner join QuoteUploadLog qul on qul.QuoteId = qq.Id
						where
							qhqh.Id = qh.Id
					)
				from QuoteUploadLog
					inner join Quote q on q.Id = QuoteUploadLog.QuoteId
					inner join QuoteHeader qh on qh.Id = q.QuoteHeaderId
					inner join Product p on p.Id = q.ProductId
					inner join Organization o on o.PartyId = p.ProductOwnerId
				where
					QuoteUploadLog.QuoteId = Quote.Id and
					QuoteUploadLog.IsDeleted = 0
			)
			else 'Not taken Up: Lead not uploaded to any insurer'
		end [Reason],
		cast(Quote.Fees as numeric(18, 2)) [BrokerFee]
	from QuoteHeader
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
		inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
		inner join Campaign on Campaign.Id = LeadActivity.CampaignId
		inner join Channel on Channel.Id = Campaign.ChannelId
		inner join [User] on [User].Id = LeadActivity.UserId
		inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
		inner join UserIndividual on UserIndividual.UserId = [User].Id
		inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		inner join Lead on Lead.Id = LeadActivity.LeadId
		inner join Individual Customer on Customer.PartyId = Lead.PartyId
	where
		Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
		Product.ProductOwnerId in (select * from fn_StringListToTable(@InsurerIds)) and
		LeadActivity.DateUpdated >= @DateFrom and
		LeadActivity.DateUpdated <= @DateTo and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0
	order by
		Channel.Name,
		Campaign.Name
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithChannelIds_WithoutInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithChannelIds_WithoutInsurerIds
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithChannelIds_WithoutInsurerIds
(
	@ChannelIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Channel.Name [BrokerName],
		Campaign.Name [Campaign],
		Customer.FirstName + ' ' + Customer.Surname [CustomerName],
		Customer.IdentityNo [CustomerId],
		isnull(
		(
			select top 1
				InsurerReference
			from QuoteUploadLog
			where
				QuoteUploadLog.QuoteId = Quote.Id and
				QuoteUploadLog.IsDeleted = 0
			order by
				QuoteUploadLog.DateCreated desc
		), 'Not Available') QuoteNumber,
		Individual.FirstName + ' ' + Individual.Surname [ContactCentreAgent],
		QuoteHeader.CreatedAt [QuoteCreateDate],
		(
			select
				cast(sum(qi.Premium) as numeric(18, 2))
			from QuoteItem qi
			where
				qi.QuoteId = Quote.Id
		) [QuotedPremium],
		case when exists
			(
				select
					QuoteId
				from QuoteUploadLog
				where
					QuoteUploadLog.QuoteId = Quote.Id and
					QuoteUploadLog.IsDeleted = 0
			)
			then 'Submitted'
			else 'Not Submitted'
		end [Status],
		case when exists
			(
				select
					QuoteUploadLog.QuoteId
				from QuoteUploadLog
					inner join Quote q on q.Id = QuoteUploadLog.QuoteId
					inner join Product on Product.Id = q.ProductId
				where
					QuoteUploadLog.QuoteId = Quote.Id and
					QuoteUploadLog.IsDeleted = 0
			)
			then 
			(
				select
					(
						select top 1
							'Taken up with insurer: Lead uploaded to ' + o.TradingName
						from QuoteHeader qhqh
							inner join Quote qq on qq.QuoteHeaderId = qhqh.Id
							inner join Product pp on pp.Id = qq.ProductId
							inner join Organization oo on oo.PartyId = pp.ProductOwnerId
							inner join QuoteUploadLog qul on qul.QuoteId = qq.Id
						where
							qhqh.Id = qh.Id
					)
				from QuoteUploadLog
					inner join Quote q on q.Id = QuoteUploadLog.QuoteId
					inner join QuoteHeader qh on qh.Id = q.QuoteHeaderId
					inner join Product p on p.Id = q.ProductId
					inner join Organization o on o.PartyId = p.ProductOwnerId
				where
					QuoteUploadLog.QuoteId = Quote.Id and
					QuoteUploadLog.IsDeleted = 0
			)
			else 'Not taken Up: Lead not uploaded to any insurer'
		end [Reason],
		cast(Quote.Fees as numeric(18, 2)) [BrokerFee]
	from QuoteHeader
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
		inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
		inner join Campaign on Campaign.Id = LeadActivity.CampaignId
		inner join Channel on Channel.Id = Campaign.ChannelId
		inner join [User] on [User].Id = LeadActivity.UserId
		inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
		inner join UserIndividual on UserIndividual.UserId = [User].Id
		inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		inner join Lead on Lead.Id = LeadActivity.LeadId
		inner join Individual Customer on Customer.PartyId = Lead.PartyId
	where
		Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
		--Product.ProductOwnerId in (select * from fn_StringListToTable(@InsurerIds)) and
		LeadActivity.DateUpdated >= @DateFrom and
		LeadActivity.DateUpdated <= @DateTo and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0
	order by
		Channel.Name,
		Campaign.Name
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithoutChannelIds_WithInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithoutChannelIds_WithInsurerIds
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithoutChannelIds_WithInsurerIds
(
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Channel.Name [BrokerName],
		Campaign.Name [Campaign],
		Customer.FirstName + ' ' + Customer.Surname [CustomerName],
		Customer.IdentityNo [CustomerId],
		isnull(
		(
			select top 1
				InsurerReference
			from QuoteUploadLog
			where
				QuoteUploadLog.QuoteId = Quote.Id and
				QuoteUploadLog.IsDeleted = 0
			order by
				QuoteUploadLog.DateCreated desc
		), 'Not Available') QuoteNumber,
		Individual.FirstName + ' ' + Individual.Surname [ContactCentreAgent],
		QuoteHeader.CreatedAt [QuoteCreateDate],
		(
			select
				cast(sum(qi.Premium) as numeric(18, 2))
			from QuoteItem qi
			where
				qi.QuoteId = Quote.Id
		) [QuotedPremium],
		case when exists
			(
				select
					QuoteId
				from QuoteUploadLog
				where
					QuoteUploadLog.QuoteId = Quote.Id and
					QuoteUploadLog.IsDeleted = 0
			)
			then 'Submitted'
			else 'Not Submitted'
		end [Status],
		case when exists
			(
				select
					QuoteUploadLog.QuoteId
				from QuoteUploadLog
					inner join Quote q on q.Id = QuoteUploadLog.QuoteId
					inner join Product on Product.Id = q.ProductId
				where
					QuoteUploadLog.QuoteId = Quote.Id and
					QuoteUploadLog.IsDeleted = 0
			)
			then 
			(
				select
					(
						select top 1
							'Taken up with insurer: Lead uploaded to ' + o.TradingName
						from QuoteHeader qhqh
							inner join Quote qq on qq.QuoteHeaderId = qhqh.Id
							inner join Product pp on pp.Id = qq.ProductId
							inner join Organization oo on oo.PartyId = pp.ProductOwnerId
							inner join QuoteUploadLog qul on qul.QuoteId = qq.Id
						where
							qhqh.Id = qh.Id
					)
				from QuoteUploadLog
					inner join Quote q on q.Id = QuoteUploadLog.QuoteId
					inner join QuoteHeader qh on qh.Id = q.QuoteHeaderId
					inner join Product p on p.Id = q.ProductId
					inner join Organization o on o.PartyId = p.ProductOwnerId
				where
					QuoteUploadLog.QuoteId = Quote.Id and
					QuoteUploadLog.IsDeleted = 0
			)
			else 'Not taken Up: Lead not uploaded to any insurer'
		end [Reason],
		cast(Quote.Fees as numeric(18, 2)) [BrokerFee]
	from QuoteHeader
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
		inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
		inner join Campaign on Campaign.Id = LeadActivity.CampaignId
		inner join Channel on Channel.Id = Campaign.ChannelId
		inner join [User] on [User].Id = LeadActivity.UserId
		inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
		inner join UserIndividual on UserIndividual.UserId = [User].Id
		inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		inner join Lead on Lead.Id = LeadActivity.LeadId
		inner join Individual Customer on Customer.PartyId = Lead.PartyId
	where
		--Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
		Product.ProductOwnerId in (select * from fn_StringListToTable(@InsurerIds)) and
		LeadActivity.DateUpdated >= @DateFrom and
		LeadActivity.DateUpdated <= @DateTo and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0
	order by
		Channel.Name,
		Campaign.Name
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithoutChannelIds_WithoutInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithoutChannelIds_WithoutInsurerIds
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithoutChannelIds_WithoutInsurerIds
(
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Channel.Name [BrokerName],
		Campaign.Name [Campaign],
		Customer.FirstName + ' ' + Customer.Surname [CustomerName],
		Customer.IdentityNo [CustomerId],
		isnull(
		(
			select top 1
				InsurerReference
			from QuoteUploadLog
			where
				QuoteUploadLog.QuoteId = Quote.Id and
				QuoteUploadLog.IsDeleted = 0
			order by
				QuoteUploadLog.DateCreated desc
		), 'Not Available') QuoteNumber,
		Individual.FirstName + ' ' + Individual.Surname [ContactCentreAgent],
		QuoteHeader.CreatedAt [QuoteCreateDate],
		(
			select
				cast(sum(qi.Premium) as numeric(18, 2))
			from QuoteItem qi
			where
				qi.QuoteId = Quote.Id
		) [QuotedPremium],
		case when exists
			(
				select
					QuoteId
				from QuoteUploadLog
				where
					QuoteUploadLog.QuoteId = Quote.Id and
					QuoteUploadLog.IsDeleted = 0
			)
			then 'Submitted'
			else 'Not Submitted'
		end [Status],
		case when exists
			(
				select
					QuoteUploadLog.QuoteId
				from QuoteUploadLog
					inner join Quote q on q.Id = QuoteUploadLog.QuoteId
					inner join Product on Product.Id = q.ProductId
				where
					QuoteUploadLog.QuoteId = Quote.Id and
					QuoteUploadLog.IsDeleted = 0
			)
			then 
			(
				select
					(
						select top 1
							'Taken up with insurer: Lead uploaded to ' + o.TradingName
						from QuoteHeader qhqh
							inner join Quote qq on qq.QuoteHeaderId = qhqh.Id
							inner join Product pp on pp.Id = qq.ProductId
							inner join Organization oo on oo.PartyId = pp.ProductOwnerId
							inner join QuoteUploadLog qul on qul.QuoteId = qq.Id
						where
							qhqh.Id = qh.Id
					)
				from QuoteUploadLog
					inner join Quote q on q.Id = QuoteUploadLog.QuoteId
					inner join QuoteHeader qh on qh.Id = q.QuoteHeaderId
					inner join Product p on p.Id = q.ProductId
					inner join Organization o on o.PartyId = p.ProductOwnerId
				where
					QuoteUploadLog.QuoteId = Quote.Id and
					QuoteUploadLog.IsDeleted = 0
			)
			else 'Not taken Up: Lead not uploaded to any insurer'
		end [Reason],
		cast(Quote.Fees as numeric(18, 2)) [BrokerFee]
	from QuoteHeader
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
		inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
		inner join Campaign on Campaign.Id = LeadActivity.CampaignId
		inner join Channel on Channel.Id = Campaign.ChannelId
		inner join [User] on [User].Id = LeadActivity.UserId
		inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
		inner join UserIndividual on UserIndividual.UserId = [User].Id
		inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		inner join Lead on Lead.Id = LeadActivity.LeadId
		inner join Individual Customer on Customer.PartyId = Lead.PartyId
	where
		--Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
		--Product.ProductOwnerId in (select * from fn_StringListToTable(@InsurerIds)) and
		LeadActivity.DateUpdated >= @DateFrom and
		LeadActivity.DateUpdated <= @DateTo and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0
	order by
		Channel.Name,
		Campaign.Name
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetQuotes') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetQuotes
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetQuotes
(
	@ChannelIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@ChannelIds) > 0)
		begin
			if(len(@InsurerIds) > 0)
				begin
					exec Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithChannelIds_WithInsurerIds @ChannelIds, @InsurerIds, @DateFrom, @DateTo
				end
			else
				begin
					exec Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithChannelIds_WithoutInsurerIds @ChannelIds, @DateFrom, @DateTo
				end
		end
	else
		begin
			if(len(@InsurerIds) > 0)
				begin
					exec Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithoutChannelIds_WithInsurerIds @InsurerIds, @DateFrom, @DateTo
				end
			else
				begin
					exec Report_CallCentre_BrokerQuoteUpload_GetQuotes_WithoutChannelIds_WithoutInsurerIds @DateFrom, @DateTo
				end
		end
go



--Report_CallCentre_BrokerQuoteUpload_GetUploads
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetUploads_WithChannelIds_WithInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetUploads_WithChannelIds_WithInsurerIds
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetUploads_WithChannelIds_WithInsurerIds
(
	@ChannelIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Channel.Name [BrokerName],
		Campaign.Name [Campaign],
		Customer.FirstName + ' ' + Customer.Surname [CustomerName],
		Customer.IdentityNo [CustomerId],
		isnull(
		(
			select top 1
				InsurerReference
			from QuoteUploadLog
			where
				QuoteUploadLog.QuoteId = Quote.Id and
				QuoteUploadLog.IsDeleted = 0
			order by
				QuoteUploadLog.DateCreated desc
		), 'Not Available') QuoteNumber,
		Individual.FirstName + ' ' + Individual.Surname [ContactCentreAgent],
		QuoteUploadLog.DateCreated [DateTimeUploaded],
		(
			select
				cast(sum(qi.Premium) + sum(q.Fees) + sum(qi.Sasria) as numeric(18, 2))
			from QuoteItem qi
				inner join Quote q on q.Id = qi.QuoteId
			where
				qi.QuoteId = Quote.Id
		) [QuotedPremium]
	from QuoteHeader
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
		inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
		inner join Campaign on Campaign.Id = LeadActivity.CampaignId
		inner join Channel on Channel.Id = Campaign.ChannelId
		inner join [User] on [User].Id = LeadActivity.UserId
		inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
		inner join UserIndividual on UserIndividual.UserId = [User].Id
		inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		inner join Lead on Lead.Id = LeadActivity.LeadId
		inner join Individual Customer on Customer.PartyId = Lead.PartyId
		inner join QuoteUploadLog on QuoteUploadLog.QuoteId = Quote.Id
	where
		Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
		Product.ProductOwnerId in (select * from fn_StringListToTable(@InsurerIds)) and
		LeadActivity.DateUpdated >= @DateFrom and
		LeadActivity.DateUpdated <= @DateTo and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0
	order by
		Channel.Name,
		Campaign.Name
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetUploads_WithChannelIds_WithoutInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetUploads_WithChannelIds_WithoutInsurerIds
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetUploads_WithChannelIds_WithoutInsurerIds
(
	@ChannelIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Channel.Name [BrokerName],
		Campaign.Name [Campaign],
		Customer.FirstName + ' ' + Customer.Surname [CustomerName],
		Customer.IdentityNo [CustomerId],
		isnull(
		(
			select top 1
				InsurerReference
			from QuoteUploadLog
			where
				QuoteUploadLog.QuoteId = Quote.Id and
				QuoteUploadLog.IsDeleted = 0
			order by
				QuoteUploadLog.DateCreated desc
		), 'Not Available') QuoteNumber,
		Individual.FirstName + ' ' + Individual.Surname [ContactCentreAgent],
		QuoteUploadLog.DateCreated [DateTimeUploaded],
		(
			select
				cast(sum(qi.Premium) + sum(q.Fees) + sum(qi.Sasria) as numeric(18, 2))
			from QuoteItem qi
				inner join Quote q on q.Id = qi.QuoteId
			where
				qi.QuoteId = Quote.Id
		) [QuotedPremium]
	from QuoteHeader
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
		inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
		inner join Campaign on Campaign.Id = LeadActivity.CampaignId
		inner join Channel on Channel.Id = Campaign.ChannelId
		inner join [User] on [User].Id = LeadActivity.UserId
		inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
		inner join UserIndividual on UserIndividual.UserId = [User].Id
		inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		inner join Lead on Lead.Id = LeadActivity.LeadId
		inner join Individual Customer on Customer.PartyId = Lead.PartyId
		inner join QuoteUploadLog on QuoteUploadLog.QuoteId = Quote.Id
	where
		Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
		--Product.ProductOwnerId in (select * from fn_StringListToTable(@InsurerIds)) and
		LeadActivity.DateUpdated >= @DateFrom and
		LeadActivity.DateUpdated <= @DateTo and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0
	order by
		Channel.Name,
		Campaign.Name
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetUploads_WithoutChannelIds_WithInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetUploads_WithoutChannelIds_WithInsurerIds
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetUploads_WithoutChannelIds_WithInsurerIds
(
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Channel.Name [BrokerName],
		Campaign.Name [Campaign],
		Customer.FirstName + ' ' + Customer.Surname [CustomerName],
		Customer.IdentityNo [CustomerId],
		isnull(
		(
			select top 1
				InsurerReference
			from QuoteUploadLog
			where
				QuoteUploadLog.QuoteId = Quote.Id and
				QuoteUploadLog.IsDeleted = 0
			order by
				QuoteUploadLog.DateCreated desc
		), 'Not Available') QuoteNumber,
		Individual.FirstName + ' ' + Individual.Surname [ContactCentreAgent],
		QuoteUploadLog.DateCreated [DateTimeUploaded],
		(
			select
				cast(sum(qi.Premium) + sum(q.Fees) + sum(qi.Sasria) as numeric(18, 2))
			from QuoteItem qi
				inner join Quote q on q.Id = qi.QuoteId
			where
				qi.QuoteId = Quote.Id
		) [QuotedPremium]
	from QuoteHeader
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
		inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
		inner join Campaign on Campaign.Id = LeadActivity.CampaignId
		inner join Channel on Channel.Id = Campaign.ChannelId
		inner join [User] on [User].Id = LeadActivity.UserId
		inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
		inner join UserIndividual on UserIndividual.UserId = [User].Id
		inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		inner join Lead on Lead.Id = LeadActivity.LeadId
		inner join Individual Customer on Customer.PartyId = Lead.PartyId
		inner join QuoteUploadLog on QuoteUploadLog.QuoteId = Quote.Id
	where
		--Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
		Product.ProductOwnerId in (select * from fn_StringListToTable(@InsurerIds)) and
		LeadActivity.DateUpdated >= @DateFrom and
		LeadActivity.DateUpdated <= @DateTo and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0
	order by
		Channel.Name,
		Campaign.Name
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetUploads_WithoutChannelIds_WithoutInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetUploads_WithoutChannelIds_WithoutInsurerIds
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetUploads_WithoutChannelIds_WithoutInsurerIds
(
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Channel.Name [BrokerName],
		Campaign.Name [Campaign],
		Customer.FirstName + ' ' + Customer.Surname [CustomerName],
		Customer.IdentityNo [CustomerId],
		isnull(
		(
			select top 1
				InsurerReference
			from QuoteUploadLog
			where
				QuoteUploadLog.QuoteId = Quote.Id and
				QuoteUploadLog.IsDeleted = 0
			order by
				QuoteUploadLog.DateCreated desc
		), 'Not Available') QuoteNumber,
		Individual.FirstName + ' ' + Individual.Surname [ContactCentreAgent],
		QuoteUploadLog.DateCreated [DateTimeUploaded],
		(
			select
				cast(sum(qi.Premium) + sum(q.Fees) + sum(qi.Sasria) as numeric(18, 2))
			from QuoteItem qi
				inner join Quote q on q.Id = qi.QuoteId
			where
				qi.QuoteId = Quote.Id
		) [QuotedPremium]
	from QuoteHeader
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
		inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
		inner join Campaign on Campaign.Id = LeadActivity.CampaignId
		inner join Channel on Channel.Id = Campaign.ChannelId
		inner join [User] on [User].Id = LeadActivity.UserId
		inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
		inner join UserIndividual on UserIndividual.UserId = [User].Id
		inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		inner join Lead on Lead.Id = LeadActivity.LeadId
		inner join Individual Customer on Customer.PartyId = Lead.PartyId
		inner join QuoteUploadLog on QuoteUploadLog.QuoteId = Quote.Id
	where
		--Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
		--Product.ProductOwnerId in (select * from fn_StringListToTable(@InsurerIds)) and
		LeadActivity.DateUpdated >= @DateFrom and
		LeadActivity.DateUpdated <= @DateTo and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0
	order by
		Channel.Name,
		Campaign.Name
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetUploads') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_GetUploads
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_GetUploads
(
	@ChannelIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@ChannelIds) > 0)
		begin
			if(len(@InsurerIds) > 0)
				begin
					exec Report_CallCentre_BrokerQuoteUpload_GetUploads_WithChannelIds_WithInsurerIds @ChannelIds, @InsurerIds, @DateFrom, @DateTo
				end
			else
				begin
					exec Report_CallCentre_BrokerQuoteUpload_GetUploads_WithChannelIds_WithoutInsurerIds @ChannelIds, @DateFrom, @DateTo
				end
		end
	else
		begin
			if(len(@InsurerIds) > 0)
				begin
					exec Report_CallCentre_BrokerQuoteUpload_GetUploads_WithoutChannelIds_WithInsurerIds @InsurerIds, @DateFrom, @DateTo
				end
			else
				begin
					exec Report_CallCentre_BrokerQuoteUpload_GetUploads_WithoutChannelIds_WithoutInsurerIds @DateFrom, @DateTo
				end
		end
go



--Execute Stored Procedures
exec Report_CallCentre_BrokerQuoteUpload_Header 7
exec Report_CallCentre_BrokerQuoteUpload_GetSummary '13', '', '2017-05-01', '2017-05-31'
exec Report_CallCentre_BrokerQuoteUpload_GetQuotes '13', '', '2017-05-01', '2017-05-31'
exec Report_CallCentre_BrokerQuoteUpload_GetUploads '13', '', '2017-05-01', '2017-05-31'

go