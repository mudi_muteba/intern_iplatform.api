use ibroker
go



--Report_CallCentre_User_Header
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_User_Header_ChannelExists') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_User_Header_ChannelExists
	end
go

create procedure Report_CallCentre_User_Header_ChannelExists
(
	@ChannelId int
)
as
	select top 1
		Code
		, ''
	from Channel
	where
		Id = @ChannelId and
		IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_User_Header_ChannelDoesNotExist') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_User_Header_ChannelDoesNotExist
	end
go

create procedure Report_CallCentre_User_Header_ChannelDoesNotExist
(
	@ChannelId int
)
as
	select top 1
		Code
		, ''
	from Channel
	where
		Id = @ChannelId and
		IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_User_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_User_Header
	end
go

create procedure Report_CallCentre_User_Header
(
	@ChannelId int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Id) from Channel where Id = @ChannelId and IsDeleted = 0) > 0)
		begin
			exec Report_CallCentre_User_Header_ChannelExists @ChannelId
		end
	else
		begin
			exec Report_CallCentre_User_Header_ChannelDoesNotExist @IpChannelId
		end
go



--Report_CallCentre_User_GetUserStatistics
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_User_GetUserStatistics_WithChannelIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_User_GetUserStatistics_WithChannelIds
	end
go

create procedure Report_CallCentre_User_GetUserStatistics_WithChannelIds
(
	@ChannelIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Channel.Name [Channel],
		md.AuthorisationGroup.Name [Role],
		dbo.ToCamelCase(ltrim(rtrim(Individual.FirstName + ' ' + Individual.Surname))) [Agent],
		[User].UserName [EMail],
		case when (select count(AuditLog.Id) from AuditLog where AuditLog.EntityId = [User].Id and AuditLog.AuditEntryType = 'UserAuthenticationSuccess' and AuditLog.RequestDate >= @DateFrom and AuditLog.RequestDate <= @DateTo) > 0
			then 'Yes' else 'No'
		End [Active]
	from [User]
		inner join UserIndividual on UserIndividual.UserId = [User].Id
		inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		inner join UserChannel on UserChannel.UserId = [User].Id
		inner join Channel on Channel.Id = UserChannel.ChannelId
		inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
		inner join md.AuthorisationGroup on md.AuthorisationGroup.Id = UserAuthorisationGroup.AuthorisationGroupId
	where
		Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
		[User].IsDeleted = 0 and
		UserIndividual.IsDeleted = 0 and
		UserChannel.IsDeleted = 0 and
		Channel.IsDeleted = 0 and
		UserAuthorisationGroup.IsDeleted = 0
	order by
		Channel.Name,
		md.AuthorisationGroup.Name
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_User_GetUserStatistics_WithoutChannelIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_User_GetUserStatistics_WithoutChannelIds
	end
go

create procedure Report_CallCentre_User_GetUserStatistics_WithoutChannelIds
(
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Channel.Name [Channel],
		md.AuthorisationGroup.Name [Role],
		dbo.ToCamelCase(ltrim(rtrim(Individual.FirstName + ' ' + Individual.Surname))) [Agent],
		[User].UserName [EMail],
		case when (select count(AuditLog.Id) from AuditLog where AuditLog.EntityId = [User].Id and AuditLog.AuditEntryType = 'UserAuthenticationSuccess' and AuditLog.RequestDate >= @DateFrom and AuditLog.RequestDate <= @DateTo) > 0
			then 'Yes' else 'No'
		End [Active]
	from [User]
		inner join UserIndividual on UserIndividual.UserId = [User].Id
		inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		inner join UserChannel on UserChannel.UserId = [User].Id
		inner join Channel on Channel.Id = UserChannel.ChannelId
		inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
		inner join md.AuthorisationGroup on md.AuthorisationGroup.Id = UserAuthorisationGroup.AuthorisationGroupId
	where
		--Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
		[User].IsDeleted = 0 and
		UserIndividual.IsDeleted = 0 and
		UserChannel.IsDeleted = 0 and
		Channel.IsDeleted = 0 and
		UserAuthorisationGroup.IsDeleted = 0
	order by
		Channel.Name,
		md.AuthorisationGroup.Name
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_User_GetUserStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_User_GetUserStatistics
	end
go

create procedure Report_CallCentre_User_GetUserStatistics
(
	@ChannelIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@ChannelIds) > 0)
		begin
			exec Report_CallCentre_User_GetUserStatistics_WithChannelIds @ChannelIds, @DateFrom, @DateTo
		end
	else
		begin
			exec Report_CallCentre_User_GetUserStatistics_WithoutChannelIds  @DateFrom, @DateTo
		end
go



--Execute Stored Procedures
exec Report_CallCentre_User_Header 7
exec Report_CallCentre_User_GetUserStatistics '13', '2017-05-01', '2017-05-31'

go