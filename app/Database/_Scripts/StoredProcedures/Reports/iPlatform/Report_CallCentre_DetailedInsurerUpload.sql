use ibroker
go



---Report_CallCentre_DetailedInsurerUpload_Header
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_Header_ChannelExists') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_Header_ChannelExists
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_Header_ChannelExists
(
	@CampaignID int
)
as
	select top 1
		Code
		, ''
	from Channel
		inner join Campaign on Campaign.ChannelId = Channel.Id
	where
		Campaign.Id = @CampaignId and
		Campaign.IsDeleted = 0 and
		Channel.IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_Header_ChannelDoesNotExist') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_Header_ChannelDoesNotExist
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_Header_ChannelDoesNotExist
(
	@ChannelId int
)
as
	select top 1
		Code
		, ''
	from Channel
	where
		Id = @ChannelId and
		IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_Header
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			exec Report_CallCentre_DetailedInsurerUpload_Header_ChannelExists @CampaignID
		end
	else
		begin
			exec Report_CallCentre_DetailedInsurerUpload_Header_ChannelDoesNotExist @IpChannelId
		end
go



--Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithCampaignIds_WithInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithCampaignIds_WithInsurerIds
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithCampaignIds_WithInsurerIds
(
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select
		Organization.TradingName [Insurer],
		Product.Name [Product],
		(
			select top 1
				Individual.Surname + ', ' + Individual.FirstName
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join [User] on [User].Id = LeadActivity.UserId
				inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
				inner join UserIndividual on UserIndividual.UserId = [User].Id
				inner join Individual on Individual.PartyId = UserIndividual.IndividualId
			where
				QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			order by
				LeadActivity.DateUpdated desc
		) Agent,
		(
			select top 1
				Individual.Surname + ', ' + Individual.FirstName
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Lead on Lead.Id = LeadActivity.LeadId
				inner join Individual on Individual.PartyId = Lead.PartyId
			where
				QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			order by
				LeadActivity.DateUpdated desc
		) Client,
		QuoteUploadLog.InsurerReference [InsurerReference],
		QuoteUploadLog.DateCreated [UploadDate],
		QuoteUploadLog.Premium [PremiumValue]
	from QuoteUploadLog
		inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
		inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
		inner join Product on Product.Id = Quote.ProductId
		inner join Organization on Organization.PartyId = Product.ProductProviderId
	where
		(
			select
				LeadActivity.CampaignId
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			where
				QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		) in (select * from fn_StringListToTable(@CampaignIds)) and
		Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
		QuoteUploadLog.DateCreated >= @DateFrom and
		QuoteUploadLog.DateCreated <= @DateTo and
		QuoteUploadLog.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0
	order by
		Organization.TradingName,
		Product.Name,
		QuoteUploadLog.DateCreated
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithCampaignIds_WithoutInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithCampaignIds_WithoutInsurerIds
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithCampaignIds_WithoutInsurerIds
(
	@CampaignIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select
		Organization.TradingName [Insurer],
		Product.Name [Product],
		(
			select top 1
				Individual.Surname + ', ' + Individual.FirstName
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join [User] on [User].Id = LeadActivity.UserId
				inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
				inner join UserIndividual on UserIndividual.UserId = [User].Id
				inner join Individual on Individual.PartyId = UserIndividual.IndividualId
			where
				QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			order by
				LeadActivity.DateUpdated desc
		) Agent,
		(
			select top 1
				Individual.Surname + ', ' + Individual.FirstName
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Lead on Lead.Id = LeadActivity.LeadId
				inner join Individual on Individual.PartyId = Lead.PartyId
			where
				QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			order by
				LeadActivity.DateUpdated desc
		) Client,
		QuoteUploadLog.InsurerReference [InsurerReference],
		QuoteUploadLog.DateCreated [UploadDate],
		QuoteUploadLog.Premium [PremiumValue]
	from QuoteUploadLog
		inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
		inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
		inner join Product on Product.Id = Quote.ProductId
		inner join Organization on Organization.PartyId = Product.ProductProviderId
	where
		(
			select
				LeadActivity.CampaignId
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			where
				QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		) in (select * from fn_StringListToTable(@CampaignIds)) and
		--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
		QuoteUploadLog.DateCreated >= @DateFrom and
		QuoteUploadLog.DateCreated <= @DateTo and
		QuoteUploadLog.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0
	order by
		Organization.TradingName,
		Product.Name,
		QuoteUploadLog.DateCreated
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithoutCampaignIds_WithInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithoutCampaignIds_WithInsurerIds
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithoutCampaignIds_WithInsurerIds
(
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select
		Organization.TradingName [Insurer],
		Product.Name [Product],
		(
			select top 1
				Individual.Surname + ', ' + Individual.FirstName
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join [User] on [User].Id = LeadActivity.UserId
				inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
				inner join UserIndividual on UserIndividual.UserId = [User].Id
				inner join Individual on Individual.PartyId = UserIndividual.IndividualId
			where
				QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			order by
				LeadActivity.DateUpdated desc
		) Agent,
		(
			select top 1
				Individual.Surname + ', ' + Individual.FirstName
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Lead on Lead.Id = LeadActivity.LeadId
				inner join Individual on Individual.PartyId = Lead.PartyId
			where
				QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			order by
				LeadActivity.DateUpdated desc
		) Client,
		QuoteUploadLog.InsurerReference [InsurerReference],
		QuoteUploadLog.DateCreated [UploadDate],
		QuoteUploadLog.Premium [PremiumValue]
	from QuoteUploadLog
		inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
		inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
		inner join Product on Product.Id = Quote.ProductId
		inner join Organization on Organization.PartyId = Product.ProductProviderId
	where
		--(
		--	select
		--		LeadActivity.CampaignId
		--	from LeadActivity
		--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
		--	where
		--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		--) in (select * from fn_StringListToTable(@CampaignIds)) and
		Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
		QuoteUploadLog.DateCreated >= @DateFrom and
		QuoteUploadLog.DateCreated <= @DateTo and
		QuoteUploadLog.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0
	order by
		Organization.TradingName,
		Product.Name,
		QuoteUploadLog.DateCreated
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithoutCampaignIds_WithoutInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithoutCampaignIds_WithoutInsurerIds
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithoutCampaignIds_WithoutInsurerIds
(
	@DateFrom datetime,
	@DateTo datetime
)
as
	select
		Organization.TradingName [Insurer],
		Product.Name [Product],
		(
			select top 1
				Individual.Surname + ', ' + Individual.FirstName
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join [User] on [User].Id = LeadActivity.UserId
				inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
				inner join UserIndividual on UserIndividual.UserId = [User].Id
				inner join Individual on Individual.PartyId = UserIndividual.IndividualId
			where
				QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			order by
				LeadActivity.DateUpdated desc
		) Agent,
		(
			select top 1
				Individual.Surname + ', ' + Individual.FirstName
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Lead on Lead.Id = LeadActivity.LeadId
				inner join Individual on Individual.PartyId = Lead.PartyId
			where
				QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			order by
				LeadActivity.DateUpdated desc
		) Client,
		QuoteUploadLog.InsurerReference [InsurerReference],
		QuoteUploadLog.DateCreated [UploadDate],
		QuoteUploadLog.Premium [PremiumValue]
	from QuoteUploadLog
		inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
		inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
		inner join Product on Product.Id = Quote.ProductId
		inner join Organization on Organization.PartyId = Product.ProductProviderId
	where
		--(
		--	select
		--		LeadActivity.CampaignId
		--	from LeadActivity
		--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
		--	where
		--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		--) in (select * from fn_StringListToTable(@CampaignIds)) and
		--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
		QuoteUploadLog.DateCreated >= @DateFrom and
		QuoteUploadLog.DateCreated <= @DateTo and
		QuoteUploadLog.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0
	order by
		Organization.TradingName,
		Product.Name,
		QuoteUploadLog.DateCreated
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics
(
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			if(len(@InsurerIds) > 0)
				begin
					exec Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithCampaignIds_WithInsurerIds @CampaignIds, @InsurerIds, @DateFrom, @DateTo
				end
			else
				begin
					exec Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithCampaignIds_WithoutInsurerIds @CampaignIds, @DateFrom, @DateTo
				end
		end
	else
		begin
			if(len(@InsurerIds) > 0)
				begin
					exec Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithoutCampaignIds_WithInsurerIds @InsurerIds, @DateFrom, @DateTo
				end
			else
				begin
					exec Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics_WithoutCampaignIds_WithoutInsurerIds @DateFrom, @DateTo
				end
		end
go



--Report_CallCentre_DetailedInsurerUpload_GetUploadTotals
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithCampaignIds_WithInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithCampaignIds_WithInsurerIds
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithCampaignIds_WithInsurerIds
(
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Organization.TradingName [Insurer],
		Product.Name [Product],
		(
			select
				count(Quote.Id)
			from QuoteUploadLog
				inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
				inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
			where
				(
					select
						LeadActivity.CampaignId
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
					where
						QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				) in (select * from fn_StringListToTable(@CampaignIds)) and
				Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Insurer.PartyId = Organization.PartyId and
				InsurerProduct.Id = Product.Id and
				QuoteUploadLog.DateCreated >= @DateFrom and
				QuoteUploadLog.DateCreated <= @DateTo and
				QuoteUploadLog.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0
		) [Uploads],
		(
			select
				avg(QuoteUploadLog.Premium)
			from QuoteUploadLog
				inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
				inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
			where
				(
					select
						LeadActivity.CampaignId
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
					where
						QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				) in (select * from fn_StringListToTable(@CampaignIds)) and
				Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Insurer.PartyId = Organization.PartyId and
				InsurerProduct.Id = Product.Id and
				QuoteUploadLog.DateCreated >= @DateFrom and
				QuoteUploadLog.DateCreated <= @DateTo and
				QuoteUploadLog.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0
		) [AveragePremiumValue],
		(
			select
				sum(QuoteUploadLog.Premium)
			from QuoteUploadLog
				inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
				inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
			where
				(
					select
						LeadActivity.CampaignId
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
					where
						QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				) in (select * from fn_StringListToTable(@CampaignIds)) and
				Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Insurer.PartyId = Organization.PartyId and
				InsurerProduct.Id = Product.Id and
				QuoteUploadLog.DateCreated >= @DateFrom and
				QuoteUploadLog.DateCreated <= @DateTo and
				QuoteUploadLog.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0
		) [TotalPremiumValue]
	from QuoteUploadLog
		inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
		inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
		inner join Product on Product.Id = Quote.ProductId
		inner join Organization on Organization.PartyId = Product.ProductProviderId
	where
		(
			select
				LeadActivity.CampaignId
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			where
				QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		) in (select * from fn_StringListToTable(@CampaignIds)) and
		Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
		QuoteUploadLog.DateCreated >= @DateFrom and
		QuoteUploadLog.DateCreated <= @DateTo and
		QuoteUploadLog.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithCampaignIds_WithoutInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithCampaignIds_WithoutInsurerIds
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithCampaignIds_WithoutInsurerIds
(
	@CampaignIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Organization.TradingName [Insurer],
		Product.Name [Product],
		(
			select
				count(Quote.Id)
			from QuoteUploadLog
				inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
				inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
			where
				(
					select
						LeadActivity.CampaignId
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
					where
						QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				) in (select * from fn_StringListToTable(@CampaignIds)) and
				--Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Insurer.PartyId = Organization.PartyId and
				InsurerProduct.Id = Product.Id and
				QuoteUploadLog.DateCreated >= @DateFrom and
				QuoteUploadLog.DateCreated <= @DateTo and
				QuoteUploadLog.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0
		) [Uploads],
		(
			select
				avg(QuoteUploadLog.Premium)
			from QuoteUploadLog
				inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
				inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
			where
				(
					select
						LeadActivity.CampaignId
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
					where
						QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				) in (select * from fn_StringListToTable(@CampaignIds)) and
				--Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Insurer.PartyId = Organization.PartyId and
				InsurerProduct.Id = Product.Id and
				QuoteUploadLog.DateCreated >= @DateFrom and
				QuoteUploadLog.DateCreated <= @DateTo and
				QuoteUploadLog.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0
		) [AveragePremiumValue],
		(
			select
				sum(QuoteUploadLog.Premium)
			from QuoteUploadLog
				inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
				inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
			where
				(
					select
						LeadActivity.CampaignId
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
					where
						QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				) in (select * from fn_StringListToTable(@CampaignIds)) and
				--Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Insurer.PartyId = Organization.PartyId and
				InsurerProduct.Id = Product.Id and
				QuoteUploadLog.DateCreated >= @DateFrom and
				QuoteUploadLog.DateCreated <= @DateTo and
				QuoteUploadLog.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0
		) [TotalPremiumValue]
	from QuoteUploadLog
		inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
		inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
		inner join Product on Product.Id = Quote.ProductId
		inner join Organization on Organization.PartyId = Product.ProductProviderId
	where
		(
			select
				LeadActivity.CampaignId
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			where
				QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		) in (select * from fn_StringListToTable(@CampaignIds)) and
		--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
		QuoteUploadLog.DateCreated >= @DateFrom and
		QuoteUploadLog.DateCreated <= @DateTo and
		QuoteUploadLog.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithoutCampaignIds_WithInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithoutCampaignIds_WithInsurerIds
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithoutCampaignIds_WithInsurerIds
(
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Organization.TradingName [Insurer],
		Product.Name [Product],
		(
			select
				count(Quote.Id)
			from QuoteUploadLog
				inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
				inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
			where
				--(
				--	select
				--		LeadActivity.CampaignId
				--	from LeadActivity
				--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				--	where
				--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				--) in (select * from fn_StringListToTable(@CampaignIds)) and
				Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Insurer.PartyId = Organization.PartyId and
				InsurerProduct.Id = Product.Id and
				QuoteUploadLog.DateCreated >= @DateFrom and
				QuoteUploadLog.DateCreated <= @DateTo and
				QuoteUploadLog.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0
		) [Uploads],
		(
			select
				avg(QuoteUploadLog.Premium)
			from QuoteUploadLog
				inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
				inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
			where
				--(
				--	select
				--		LeadActivity.CampaignId
				--	from LeadActivity
				--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				--	where
				--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				--) in (select * from fn_StringListToTable(@CampaignIds)) and
				Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Insurer.PartyId = Organization.PartyId and
				InsurerProduct.Id = Product.Id and
				QuoteUploadLog.DateCreated >= @DateFrom and
				QuoteUploadLog.DateCreated <= @DateTo and
				QuoteUploadLog.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0
		) [AveragePremiumValue],
		(
			select
				sum(QuoteUploadLog.Premium)
			from QuoteUploadLog
				inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
				inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
			where
				--(
				--	select
				--		LeadActivity.CampaignId
				--	from LeadActivity
				--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				--	where
				--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				--) in (select * from fn_StringListToTable(@CampaignIds)) and
				Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Insurer.PartyId = Organization.PartyId and
				InsurerProduct.Id = Product.Id and
				QuoteUploadLog.DateCreated >= @DateFrom and
				QuoteUploadLog.DateCreated <= @DateTo and
				QuoteUploadLog.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0
		) [TotalPremiumValue]
	from QuoteUploadLog
		inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
		inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
		inner join Product on Product.Id = Quote.ProductId
		inner join Organization on Organization.PartyId = Product.ProductProviderId
	where
		--(
		--	select
		--		LeadActivity.CampaignId
		--	from LeadActivity
		--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
		--	where
		--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		--) in (select * from fn_StringListToTable(@CampaignIds)) and
		Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
		QuoteUploadLog.DateCreated >= @DateFrom and
		QuoteUploadLog.DateCreated <= @DateTo and
		QuoteUploadLog.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithoutCampaignIds_WithoutInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithoutCampaignIds_WithoutInsurerIds
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithoutCampaignIds_WithoutInsurerIds
(
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Organization.TradingName [Insurer],
		Product.Name [Product],
		(
			select
				count(Quote.Id)
			from QuoteUploadLog
				inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
				inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
			where
				--(
				--	select
				--		LeadActivity.CampaignId
				--	from LeadActivity
				--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				--	where
				--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				--) in (select * from fn_StringListToTable(@CampaignIds)) and
				--Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Insurer.PartyId = Organization.PartyId and
				InsurerProduct.Id = Product.Id and
				QuoteUploadLog.DateCreated >= @DateFrom and
				QuoteUploadLog.DateCreated <= @DateTo and
				QuoteUploadLog.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0
		) [Uploads],
		(
			select
				avg(QuoteUploadLog.Premium)
			from QuoteUploadLog
				inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
				inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
			where
				--(
				--	select
				--		LeadActivity.CampaignId
				--	from LeadActivity
				--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				--	where
				--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				--) in (select * from fn_StringListToTable(@CampaignIds)) and
				--Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Insurer.PartyId = Organization.PartyId and
				InsurerProduct.Id = Product.Id and
				QuoteUploadLog.DateCreated >= @DateFrom and
				QuoteUploadLog.DateCreated <= @DateTo and
				QuoteUploadLog.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0
		) [AveragePremiumValue],
		(
			select
				sum(QuoteUploadLog.Premium)
			from QuoteUploadLog
				inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
				inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
			where
				--(
				--	select
				--		LeadActivity.CampaignId
				--	from LeadActivity
				--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				--	where
				--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				--) in (select * from fn_StringListToTable(@CampaignIds)) and
				--Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Insurer.PartyId = Organization.PartyId and
				InsurerProduct.Id = Product.Id and
				QuoteUploadLog.DateCreated >= @DateFrom and
				QuoteUploadLog.DateCreated <= @DateTo and
				QuoteUploadLog.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0
		) [TotalPremiumValue]
	from QuoteUploadLog
		inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
		inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
		inner join Product on Product.Id = Quote.ProductId
		inner join Organization on Organization.PartyId = Product.ProductProviderId
	where
		--(
		--	select
		--		LeadActivity.CampaignId
		--	from LeadActivity
		--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
		--	where
		--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		--) in (select * from fn_StringListToTable(@CampaignIds)) and
		--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
		QuoteUploadLog.DateCreated >= @DateFrom and
		QuoteUploadLog.DateCreated <= @DateTo and
		QuoteUploadLog.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_GetUploadTotals') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_GetUploadTotals
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_GetUploadTotals
(
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			if(len(@InsurerIds) > 0)
				begin
					exec Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithCampaignIds_WithInsurerIds @CampaignIds, @InsurerIds, @DateFrom, @DateTo
				end
			else
				begin
					exec Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithCampaignIds_WithoutInsurerIds @CampaignIds, @DateFrom, @DateTo
				end
		end
	else
		begin
			if(len(@InsurerIds) > 0)
				begin
					exec Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithoutCampaignIds_WithInsurerIds @InsurerIds, @DateFrom, @DateTo
				end
			else
				begin
					exec Report_CallCentre_DetailedInsurerUpload_GetUploadTotals_WithoutCampaignIds_WithoutInsurerIds @DateFrom, @DateTo
				end
		end
go



--Execute Stored Procudures
exec Report_CallCentre_DetailedInsurerUpload_Header 7
exec Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics '', '', '2017-05-01', '2017-05-31'
exec Report_CallCentre_DetailedInsurerUpload_GetUploadTotals '', '', '2017-05-01', '2017-05-31'

go