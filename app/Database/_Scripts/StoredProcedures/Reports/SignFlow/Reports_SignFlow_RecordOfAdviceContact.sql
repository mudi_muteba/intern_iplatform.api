if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_RecordOfAdviceContact') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_RecordOfAdviceContact
	end
go

create procedure Reports_SignFlow_RecordOfAdviceContact
(	
	@Id int
	
)
as

select top 1
	ISNULL(ind.FirstName,'') + ' ' + ISNULL(ind.Surname,'')  as 'InsuredName',
	ind.IdentityNo   as 'InsuredId',
	''   as 'Broker',
	occup.Name   as 'Occupation',
	cd.Email   as 'EmailAddress',
	cd.Work  as 'ContactWork',
	cd.Cell as 'ContactMobile',
	cd.Home as 'ContactHome',
	ad.Description as 'PostalAddress',
	ad.Description as 'PhysicalAddress'
from Party as p
join ContactDetail as cd on cd.Id = p.ContactDetailId
join [Address] as ad on ad.PartyId = p.Id
join Individual as ind on ind.PartyId = p.Id
join Occupation as occup on occup.Id = ind.OccupationId
where p.Id = @Id
