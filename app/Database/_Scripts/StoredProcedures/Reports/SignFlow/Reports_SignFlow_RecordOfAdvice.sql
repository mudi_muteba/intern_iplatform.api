if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_RecordOfAdvice') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_RecordOfAdvice
	end
go

create procedure Reports_SignFlow_RecordOfAdvice
(	
	@Id int
	
)
as

-- To be implemented later
SELECT cov.Name, SumInsured, Description from ProposalDefinition as pd
join md.Cover as cov on cov.Id = pd.CoverId
where pd.IsDeleted <> 1
and pd.PartyId = @Id


