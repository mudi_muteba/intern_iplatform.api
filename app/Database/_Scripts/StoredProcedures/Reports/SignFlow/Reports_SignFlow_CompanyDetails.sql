if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_CompanyDetails') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_CompanyDetails
	end
go

create procedure Reports_SignFlow_CompanyDetails
(	
	@Code nvarchar(10)	
)
as

select cd.cell, cd.Work, [add].Description 
from Channel as c
join Party as p on p.Id = c.OrganizationId
join ContactDetail as cd on cd.Id = p.ContactDetailId
join [Address] as [add] on [add].PartyId = p.Id
where c.Code = @Code
and [add].IsDefault = 1