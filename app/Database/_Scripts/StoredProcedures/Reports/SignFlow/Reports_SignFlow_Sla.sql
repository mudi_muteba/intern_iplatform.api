if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_Sla') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_Sla
	end
go

create procedure Reports_SignFlow_Sla
(	
	@Id int,
	@InsurerCode nvarchar
)
as

select top 1
	(select top 1 RegisteredName from Organization where Code = @InsurerCode)   as 'BrokerName',
	ISNULL(ind.FirstName,'') + ' ' + ISNULL(ind.Surname,'')  as 'ClientName',
	ind.IdentityNo  as 'ClientIndentityNo',
	ad.Description     as 'PostalAddress'	
from Party as p
join [Address] as ad on ad.PartyId = p.Id
join Individual as ind on ind.PartyId = p.Id
where p.Id = @Id