if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_BrokerNote') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_BrokerNote
	end
go

create procedure Reports_SignFlow_BrokerNote
(	
	@Id int,
	@InsurerCode nvarchar
)
as

-- To be implemented later
select top 1
	ISNULL(ind.FirstName,'') + ' ' + ISNULL(ind.Surname,'')  as 'InsuredName',
	 (select top 1 RegisteredName from Organization where Code = @InsurerCode)  as 'Appointee'
from Party as p
join Individual as ind on ind.PartyId = p.Id
where p.Id = @Id

