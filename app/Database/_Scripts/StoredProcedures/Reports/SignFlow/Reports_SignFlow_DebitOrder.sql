if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_DebitOrder') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_DebitOrder
	end
go

create procedure Reports_SignFlow_DebitOrder
(	
	@Id int
	
)
as

select top 1
	''   as 'InsuredName',
	''   as 'InsuredId',
	cd.Work  as 'ContactWork',
	cd.Cell as 'ContactMobile',
	cd.Home       as 'ContactHome',
	ad.Description     as 'PostalAddress',
	bk.BankAccHolder     as 'BankDetailAccountName',
	bk.Bank    as 'BankDetailsBankName',
	bk.BankBranch    as 'BankDetailBranchName',
	bk.BankBranchCode as 'BankDetailBranchCode',
	bk.AccountNo    as 'BankDetailAccountNumber',
	bk.TypeAccount  as 'BankDetailAccountType'
from Party as p
join ContactDetail as cd on cd.Id = p.ContactDetailId
join [Address] as ad on ad.PartyId = p.Id
left join BankDetails as bk on bk.PartyId = p.Id
where p.Id = @Id
