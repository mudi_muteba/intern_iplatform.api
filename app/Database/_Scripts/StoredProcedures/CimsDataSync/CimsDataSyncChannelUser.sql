IF EXISTS 
(
	SELECT * 
	FROM sys.objects AS O
	WHERE O.[type] = 'P'
		AND O.[object_id] = OBJECT_ID('dbo.CimsDataSyncChannelUser')
)
BEGIN
	DROP PROCEDURE dbo.CimsDataSyncChannelUser;
END
GO

CREATE PROC dbo.CimsDataSyncChannelUser
(
    @ProductCodes XML = '<list><y ProductCode="DOMESTIC_ARMOUR" /></list>'
	,@DatabaseName VARCHAR(128) = 'Cardinal'
	,@Client NVARCHAR(13) = 'Smart'
)
AS /*
	How To:
	iPlatform procedure parameter requirements: 

		@ProductCodes in XML format eg. '<list><y ProductCode="DOMESTIC_ARMOUR" /></list>'
		@DatabaseName Cims 3 DB name
		@Client Appended to the EntityGUID when generating the external reference

	ProductCodeIplatform column needs to be populated in Cims3s PB_ProductMaster table to link the products between the 2 DBs

	SettingsQuoteUpload data still needs to be supplied, I have commented out the insert component in the procedure below with an example

	SettingsiRate data has not been populated in the procedure as this will not be necessary for SMART


	TODO:  
	#SettingsQuoteUpload_Info require Info
	#SettingsiRate_Info require Info
	*/
	DECLARE @SQLExec NVARCHAR(MAX)
	  , @SQLExec2 NVARCHAR(MAX)
	  , @SQLExec2_Part2 NVARCHAR(MAX)
	  , @SQLExec3 NVARCHAR(MAX)
	  , @SQLExec4 NVARCHAR(MAX)
	  , @SQLExec_Update NVARCHAR(MAX)
	  , @LastID INT;

	SET @SQLExec = '';
	SET @SQLExec2 = '';
	SET @SQLExec2_Part2 = '';
	SET @SQLExec3 = '';
	SET @SQLExec4 = '';
	SET @SQLExec_Update = '';

	CREATE TABLE #Campaign (Id INT, ChannelId INT);
	CREATE TABLE #Channel
		(
		 ID INT NOT NULL
				DEFAULT 0
	   , EntityGUID UNIQUEIDENTIFIER
	   , TradingName NVARCHAR(500)
	   , BranchName NVARCHAR(200)
	   , ExternalReference NVARCHAR(50)
	   , BrokerCode NVARCHAR(50)
	   , WorkPhone NVARCHAR(50)
	   , Email NVARCHAR(255)
	   , SystemId UNIQUEIDENTIFIER NOT NULL
								   DEFAULT NEWID()
	   , AlreadyExists BIT DEFAULT 0
	   , Rowno INT DEFAULT 0
	   , ParentExternalReference NVARCHAR(50)
	   , ParentChannelId INT NOT NULL
							 DEFAULT 0
		);
	CREATE TABLE #User
		(
		 ID INT NOT NULL
				DEFAULT 0
	   , EntityGUID UNIQUEIDENTIFIER
	   , ParentEntityGUID UNIQUEIDENTIFIER
	   , Firstnames NVARCHAR(500)
	   , Surname NVARCHAR(500)
	   , EntityName NVARCHAR(500)
	   , IsBrokerageContact BIT
	   , IsBrokerageAccountsExec BIT
	   , ExternalReference NVARCHAR(50)
	   , Username NVARCHAR(512)
	--		Password= "password"
	   , Password NVARCHAR(2048)
			NOT NULL
			DEFAULT '100:WVS59p/ESIW9Z5ALCBQFav4mPPfS8grWz70aG5ndw7eUYJq40ukwwSwDwn84Efa37EpN8SIyezw59pOFvrmFtbl+R/+qS1LqLyRT6Rr2jGgjJRNlymKIWqZ+l2L9PlhuOWnUBw92JJat0J1kPi54ptYOVrZT8/XeMRZobwxcKP/ruo4VMVHGf2IHMIKsHDV/LMXtpOrQ7fHAxNwMwfcAdzxrxgI0rAAhKhT1h9Tiv4tVcVQn/MJ6289AGm41qqWlTd4bNSiZ3RLYU+QDrmOvmm1XbHK84/P7rPBFpjFUX1hdTwOvYahz43URP0XFuXpUpExkJkXM/JUBwS+DqmxpSw==:3R5FsZqhZNPiISSDrW6iLHPepKheDx8qlsZ1K8vIpJWRr+hUXw3ow5ubJ7cxas2cziOUALi0ywYKlkOHeuhU8PKKAkuZvauzJn0QFrx8Z0LUuBnc/6qeynwSOLEN5EK4mZoNJ8bsjAJ/T1unrj0Zm/aNCGqkcXMFniHEiKW+qd0M/Kmo40eK+4kGp69zB1ZTbL2rP0KMQvoNaM+nh82yJ/Cac9UU6zD/CcRKb0G9EYb9f8CwIPC9dgQfQ/7iDHwsm6sNWunPJkc88N74FND0wQVR9jtot5TK3PKpU4XYlTZL1X2mOdobsqB0YXbkxDH3eN2x4mzge2nVRMZV4jmDSw=='
	   , AlreadyExists BIT DEFAULT 0
	   , Rowno INT DEFAULT 0
	   , IDNo NVARCHAR(50)
		);
	CREATE TABLE #UserMap
		(
		 ID INT
	   , EntityGUID UNIQUEIDENTIFIER
	   , UserName NVARCHAR(512)
	   , FirstName NVARCHAR(255)
	   , Surname NVARCHAR(255)
	   , IdentityNo NVARCHAR(20)
		);
	CREATE TABLE #BrokerageProducts
		(
		 EntityGUID UNIQUEIDENTIFIER
	   , ProductCode NVARCHAR(255)
	   , EventName NVARCHAR(255) NOT NULL
								 DEFAULT 'ExternalQuoteAcceptedWithUnderwritingEnabledEvent'
	   , ChannelId INT NOT NULL
					   DEFAULT 0
	   , ProductCodeIplatform NVARCHAR(255)
		);
	CREATE TABLE #ChannelEvent
		(
		 EventName NVARCHAR(255)
	   , ChannelId INT
	   , IsDeleted BIT
	   , ProductCode NVARCHAR(255)
		);
	CREATE TABLE #EmailCommunicationSetting_Info
		(
		 Host NVARCHAR(255)
	   , Port INT
	   , UseSSL BIT
	   , UseDefaultCredentials BIT
	   , UserName NVARCHAR(255)
	   , Password NVARCHAR(255)
	   , DefaultBCC NVARCHAR(255)
	   , SubAccountID NVARCHAR(255)
		);
	CREATE TABLE #SettingsQuoteUpload_Info
		(
		 SettingName NVARCHAR(255)
	   , SettingValue NVARCHAR(255)
	   , Environment NVARCHAR(255)
	   , ProductId INT
		);
	CREATE TABLE #SettingsiRate_Info
		(
		 ProductId INT
	   , Password NVARCHAR(50)
	   , AgentCode NVARCHAR(50)
	   , BrokerCode NVARCHAR(50)
	   , AuthCode NVARCHAR(50)
	   , SchemeCode NVARCHAR(50)
	   , Token NVARCHAR(50)
	   , Environment NVARCHAR(255)
	   , UserId NVARCHAR(255)
	   , SubscriberCode NVARCHAR(255)
	   , CompanyCode NVARCHAR(512)
	   , UwCompanyCode NVARCHAR(512)
	   , UwProductCode NVARCHAR(512)
		);
	CREATE TABLE #VehicleGuideSetting
		(
		 IsDeleted BIT DEFAULT ((0))
	   , CountryId INT
	   , ApiKey NVARCHAR(512)
	   , Email NVARCHAR(512)
	   , MMBookEnabled BIT
	   , LightstoneEnabled BIT
	   , LSA_UserId UNIQUEIDENTIFIER
	   , LSA_ContractId UNIQUEIDENTIFIER
	   , LSA_CustomerId UNIQUEIDENTIFIER
	   , LSA_PackageId UNIQUEIDENTIFIER
	   , LSA_UserName NVARCHAR(255)
	   , LSA_Password NVARCHAR(2048)
	   , KeAutoKey NVARCHAR(255)
	   , KeAutoSecret NVARCHAR(255)
	   , Evalue8Username NVARCHAR(255)
	   , Evalue8Password NVARCHAR(255)
	   , Evalue8Enabled BIT NULL
	   , TransUnionAutoEnabled BIT NULL
	   , RegCheckUkEnabled BIT NULL
	   , VehicleValuesPassword NVARCHAR(255)
	   , VehicleValuesHashKey NVARCHAR(255)
	   , VehicleValuesApiKey NVARCHAR(255)
	   , VehicleValuesUserId NVARCHAR(255)
	   , VehicleValuesReportCode NVARCHAR(255)
		);
	CREATE TABLE #PersonLookupSetting
		(
		 IsDeleted BIT
	   , CountryId INT
	   , ApiKey NVARCHAR(512)
	   , Email NVARCHAR(512)
	   , InvoiceReference NVARCHAR(512)
	   , PCubedEnabled BIT
	   , PCubedUsername NVARCHAR(512)
	   , PCubedPassword NVARCHAR(512)
	   , IPRSEnabled BIT
	   , IPRSUsername NVARCHAR(255)
	   , IPRSPassword NVARCHAR(255)
	   , LightStonePropertyEnabled BIT
	   , LightStonePropertyUserId NVARCHAR(255)
	   , TransunionEnabled BIT
	   , TransunionFirstName NVARCHAR(255)
	   , TransunionSurname NVARCHAR(255)
	   , TransunionSubscriberCode NVARCHAR(255)
	   , TransunionSecurityCode NVARCHAR(255)
	   , TransUnionEndPoint BIT
	   , TransUnionGetClaims BIT
	   , TransUnionGetDrivers BIT
		);
	CREATE TABLE #ChannelEventTask
		(
		 TaskName INT
	   , ChannelEventId INT
	   , IsDeleted BIT
		);
	CREATE TABLE #ProductCodes
		(
		 ProductCode NVARCHAR(255)
		);
	CREATE TABLE #ProductFee
		(
		 ChannelId INT
	   , MasterId INT
	   , ProductCode NVARCHAR(255)
	   , ProductFeeTypeId INT
	   , PaymentPlanId INT
	   , BrokerageId INT
	   , Value DECIMAL(19, 5)
	   , MinValue DECIMAL(19, 5)
	   , MaxValue DECIMAL(19, 5)
	   , IsOnceOff BIT
	   , IsPercentage BIT
	   , AllowRefund BIT
	   , AllowProRata BIT
	   , IsDeleted BIT
		);
	CREATE TABLE #BrokerageFee
		(
		 ChannelId INT NOT NULL
					   DEFAULT 0
	   , BrokerageGUID UNIQUEIDENTIFIER
	   , ProductCode NVARCHAR(255)
	   , PercentageFees DECIMAL(19, 5)
	   , RandFees DECIMAL(19, 5)
		);
	CREATE TABLE #BrokerageFeeProductOnly
		(
		 ChannelId INT NOT NULL
					   DEFAULT 0
	   , BrokerageGUID UNIQUEIDENTIFIER
	   , ProductCode NVARCHAR(255)
	   , PercentageFees DECIMAL(19, 5)
	   , RandFees DECIMAL(19, 5)
		);
	CREATE TABLE #CimsFees
		(
		 BrokerageGUID UNIQUEIDENTIFIER NOT NULL
	   , ProductCodeIplatform NVARCHAR(255) NOT NULL
	   , ProductCode NVARCHAR(50) NOT NULL
	   , PaymentPlanId INT NOT NULL
	   , U_PercentageFees DECIMAL(19, 5) NOT NULL
										 DEFAULT 0
	   , B_PercentageFees DECIMAL(19, 5) NOT NULL
										 DEFAULT 0
	   , A_PercentageFees DECIMAL(19, 5) NOT NULL
										 DEFAULT 0
	   , U_RandFees DECIMAL(19, 5) NOT NULL
								   DEFAULT 0
	   , B_RandFees DECIMAL(19, 5) NOT NULL
								   DEFAULT 0
	   , A_RandFees DECIMAL(19, 5) NOT NULL
								   DEFAULT 0
	   , U_AllowRefund INT NOT NULL
						   DEFAULT 0
	   , B_AllowRefund INT NOT NULL
						   DEFAULT 0
	   , A_AllowRefund INT NOT NULL
						   DEFAULT 0
	   , U_Percentage_MinValue DECIMAL(19, 5) NOT NULL
											  DEFAULT 0
	   , U_Percentage_MaxValue DECIMAL(19, 5) NOT NULL
											  DEFAULT 0
	   , B_Percentage_MinValue DECIMAL(19, 5) NOT NULL
											  DEFAULT 0
	   , B_Percentage_MaxValue DECIMAL(19, 5) NOT NULL
											  DEFAULT 0
	   , A_Percentage_MinValue DECIMAL(19, 5) NOT NULL
											  DEFAULT 0
	   , A_Percentage_MaxValue DECIMAL(19, 5) NOT NULL
											  DEFAULT 0
	   , U_Rand_MinValue DECIMAL(19, 5) NOT NULL
										DEFAULT 0
	   , U_Rand_MaxValue DECIMAL(19, 5) NOT NULL
										DEFAULT 0
	   , B_Rand_MinValue DECIMAL(19, 5) NOT NULL
										DEFAULT 0
	   , B_Rand_MaxValue DECIMAL(19, 5) NOT NULL
										DEFAULT 0
	   , A_Rand_MinValue DECIMAL(19, 5) NOT NULL
										DEFAULT 0
	   , A_Rand_MaxValue DECIMAL(19, 5) NOT NULL
										DEFAULT 0
	   , IsOnceOff BIT NOT NULL
					   DEFAULT 0
	   , ChannelId INT NULL
	   , U_AllowProRata INT NOT NULL
							DEFAULT 0
	   , B_AllowProRata INT NOT NULL
							DEFAULT 0
	   , A_AllowProRata INT NOT NULL
							DEFAULT 0
		);
	CREATE TABLE #Party
		(
		 ID INT
	   , UserID INT
	   , DisplayName NVARCHAR(550)
	   , FirstName NVARCHAR(255)
	   , Surname NVARCHAR(255)
	   , ChannelId INT
	   , IdentityNo NVARCHAR(20)
		);
	CREATE TABLE #ChannelDeactivate
		(
		 EntityGUID UNIQUEIDENTIFIER
	   , ExternalReference NVARCHAR(50)
		);
	CREATE TABLE #UserDeactivate
		(
		 EntityGUID UNIQUEIDENTIFIER
	   , ExternalReference NVARCHAR(50)
		);
	--		Product Codes
	INSERT  #ProductCodes (ProductCode)
			SELECT  x.y.value('./@ProductCode', 'VARCHAR(255)') ProductCode
			FROM    @ProductCodes.nodes('list/y') AS x (y);

	INSERT  #EmailCommunicationSetting_Info (Host, Port, UseSSL, UseDefaultCredentials, UserName, Password, DefaultBCC, SubAccountID)
			SELECT  'smtp.gmail.com', 587, 1, 0, 'iplatform@iplatform.co.za', 'p8nynyocRLhoCeN2wdoN8A', '', 'QuoteAcceptance';

	INSERT  #VehicleGuideSetting (IsDeleted, CountryId, ApiKey, Email, MMBookEnabled, LightstoneEnabled, LSA_UserId, LSA_ContractId, LSA_CustomerId, LSA_PackageId,
								  LSA_UserName, LSA_Password, KeAutoKey, KeAutoSecret, Evalue8Username, Evalue8Password, Evalue8Enabled, TransUnionAutoEnabled,
								  RegCheckUkEnabled, VehicleValuesPassword, VehicleValuesHashKey, VehicleValuesApiKey, VehicleValuesUserId, VehicleValuesReportCode)
			SELECT  0, 197, '2kw9a6njlSxqG7wRYYzHIU1m4Hee9UJZ23Y0Z99aSwo', 'iguide-root@iplatform.co.za', 1, 0, 'A238D2A5-85DE-4028-B351-5D14E0E016D5',
					'8092E1AC-FC52-4182-8580-A9EFF8F2D70B', 'F4844CD8-7731-4D4A-9083-55450740C0E0', '23634A56-6B41-4F73-A960-AA44F1EB2FB2', 'VIGeneric',
					'testpassword', 'C26C64040DBEE49500013CBCFEC11122', '6678979C75A70BBA85762F4D488AFB6F', 'Imagin8Test', 'Imagin8Test2016', 0, 1, 0, 'T3st194#',
					'Cli3ntH@sah', 'C4EA646D-7C82-48D5-8D29-3F4518A4F805', '183194', '9849A63B-FC07-4BBD-B1D9-3BFAF6B2EE21'

	INSERT  #PersonLookupSetting (IsDeleted, CountryId, ApiKey, Email, InvoiceReference, PCubedEnabled, PCubedUsername, PCubedPassword, IPRSEnabled, IPRSUsername,
								  IPRSPassword, LightStonePropertyEnabled, LightStonePropertyUserId, TransunionEnabled, TransunionFirstName, TransunionSurname,
								  TransunionSubscriberCode, TransunionSecurityCode, TransUnionEndPoint, TransUnionGetClaims, TransUnionGetDrivers)
			SELECT  0, 197, '3BQmEDTa3FoZRXALMY82y38qXQqrpgcJPlO4kUmXP8Zab9siaNnG1X__41YpN3mn3eK6I72tdLN0DzPpYIRbu6jsmOASwyJnndGxZW9Dmv4QKax6M90FLKtywSkv6Jxf',
					'iperson-root@iplatform.co.za', NULL, 1, 'CardinalDemoNonCPA', 'ERdfg45YqPzz', 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL,
					NULL;

	--		INSERT  #SettingsQuoteUpload_Info (SettingName, SettingValue, Environment, ProductId)
	--		Example:	SELECT  'Telesureag/Upload/ServiceURL', 'https://api.telesure.co.za/quickquotes/APIService.asmx', 'Live', 6--Auto & General


	--		User Channel
	SET @SQLExec = '
	INSERT  #Channel (EntityGUID, TradingName, BranchName, ExternalReference, BrokerCode, WorkPhone, Email, ParentExternalReference)
			SELECT  ed.EntityGUID, ed.TradingName, ed.BranchName, CASE WHEN ed.ExternalReference = '''' THEN UPPER(''' + @Client
		+ ''') + ''_'' + CONVERT(VARCHAR(36), ed.EntityGUID) ELSE ed.ExternalReference END ExternalReference, ed.BrokerCode, ed.WorkPhone, ed.Email,
			p.ExternalReference ParentExternalReference
			FROM    ' + @DatabaseName + '.dbo.EntityDetails ed
			LEFT JOIN ' + @DatabaseName
		+ '.dbo.EntityDetails p ON	CASE WHEN ed.DealerGroupEntityGUID != ''00000000-0000-0000-0000-000000000000'' 
													THEN ed.DealerGroupEntityGUID
												ELSE CASE WHEN ed.EntityGUID = ed.BCodeGUID 
													THEN ''00000000-0000-0000-0000-000000000000''
												ELSE ed.BCodeGUID END END = p.EntityGUID 
			WHERE   ed.Deleted = 0
					AND (ed.IsBrokerageBranch = 1
							OR ed.IsBrokerage = 1)
					--AND ed.EntityGUID = ed.BCodeGUID
					AND ed.ActiveCompany = 1;

	INSERT  #User (EntityGUID, ParentEntityGUID, Firstnames, Surname, EntityName, IsBrokerageContact, IsBrokerageAccountsExec, ExternalReference, Username, IDNo)
			SELECT DISTINCT		ed.EntityGUID, br.EntityGUID ParentEntityGUID, LEFT(ed.Firstnames,255) Firstnames, LEFT(ed.Surname,255) Surname, ed.EntityName, ed.IsBrokerageContact, 
								ed.IsBrokerageAccountsExec, CASE WHEN ed.ExternalReference = '''' THEN UPPER(''' + @Client
		+ ''') + ''_'' + CONVERT(VARCHAR(36), ed.EntityGUID) ELSE ed.ExternalReference END ExternalReference, CASE ed.Email
																					WHEN '''' THEN REPLACE(ed.Firstnames,'' '','''') + ''@'' + ''' + @Client
		+ ''' + ''iplatform.co.za''
																					ELSE ed.Email
																				END Username, CASE WHEN ed.IDNo = '''' THEN NULL ELSE ed.IDNo END IDNo
			FROM    ' + @DatabaseName + '.dbo.EntityDetails ed
					JOIN ' + @DatabaseName + '.dbo.EntityDetails br ON ed.BCodeGUID = br.EntityGUID
			WHERE   ed.Deleted = 0
					AND (ed.IsBrokerageContact = 1
						 OR ed.IsBrokerageAccountsExec = 1)
					AND EXISTS ( SELECT 1
								 FROM   ' + @DatabaseName + '.dbo.Contacts c
								 WHERE  c.Deleted = 0
										AND ed.EntityGUID = c.ContactGUID )
					AND ed.ActiveCompany = 1
					AND br.Deleted = 0
					AND (br.IsBrokerageBranch = 1
						 OR br.IsBrokerage = 1)
					AND br.EntityGUID IN	(SELECT		c.EntityGUID
											FROM		#Channel c);'

	--Deactivate
	SET @SQLExec4 = '
	INSERT  #ChannelDeactivate (EntityGUID, ExternalReference)
			SELECT  ed.EntityGUID, ed.ExternalReference
			FROM    ' + @DatabaseName + '.dbo.EntityDetails ed
			WHERE   ed.Deleted = 0
					AND ed.IsBrokerage = 1
					AND (ed.IsBrokerageBranch = 1
							OR ed.IsBrokerage = 1)
					AND ed.ActiveCompany = 0
					AND ed.ExternalReference != '''';

	INSERT  #UserDeactivate (EntityGUID, ExternalReference)
			SELECT  ed.EntityGUID, ed.ExternalReference
			FROM    ' + @DatabaseName + '.dbo.EntityDetails ed
					JOIN ' + @DatabaseName + '.dbo.EntityDetails br ON ed.BCodeGUID = br.EntityGUID
					JOIN ' + @DatabaseName + '.dbo.EntityDetails b ON br.BCodeGUID = b.EntityGUID
			WHERE   ed.Deleted = 0
					AND (ed.IsBrokerageContact = 1
						 OR ed.IsBrokerageAccountsExec = 1)
					AND EXISTS ( SELECT 1
								 FROM   ' + @DatabaseName + '.dbo.Contacts c
								 WHERE  c.Deleted = 0
										AND ed.EntityGUID = c.ContactGUID )
					AND ed.ActiveCompany = 1
					AND br.Deleted = 0
					AND b.Deleted = 0
					AND b.IsBrokerage = 1
					AND (br.IsBrokerageBranch = 1
						 OR br.IsBrokerage = 1)
					AND (b.EntityGUID IN (SELECT		c.EntityGUID
										 FROM			#Channel c)
							OR b.EntityGUID IN (SELECT		c.EntityGUID
												FROM		#ChannelDeactivate c))
					AND ed.ExternalReference != ''''; '

	--		Get Cims3 Data
	EXEC sys.sp_sqlexec @p1 = @SQLExec;
	EXEC sys.sp_sqlexec @p1 = @SQLExec4;

	--		Deactivate Channels & Users
	UPDATE  c
	SET     c.IsActive = 0
	FROM    dbo.Channel c
	WHERE   c.IsDeleted = 0
			AND c.ExternalReference COLLATE SQL_Latin1_General_CP1_CI_AS IN (SELECT cd.ExternalReference
																			 FROM   #ChannelDeactivate cd)
	UPDATE  u
	SET     u.IsActive = 0
	FROM    dbo.[User] u
	WHERE   u.IsDeleted = 0
			AND u.ExternalReference COLLATE SQL_Latin1_General_CP1_CI_AS IN (SELECT ud.ExternalReference
																			 FROM   #UserDeactivate ud)

	--		Indicate What Exists
	UPDATE  c
	SET     c.AlreadyExists = 1
	FROM    #Channel c
	WHERE   c.ExternalReference IN (SELECT  c2.ExternalReference COLLATE SQL_Latin1_General_CP1_CI_AS
									FROM    dbo.Channel c2);

	UPDATE  u
	SET     u.AlreadyExists = 1
	FROM    #User u
	WHERE   u.ExternalReference IN (SELECT  u2.ExternalReference COLLATE SQL_Latin1_General_CP1_CI_AS
									FROM    dbo.[User] u2);

	SET @SQLExec2 = '
	WITH    fee
			  AS (SELECT    pf.BrokerageGUID, pm.ProductCode, pm.ProductCodeIplatform, CASE WHEN pp.PaymentPlanName = ''Annual'' THEN 1
																							ELSE 3
																					   END PaymentPlanId,
							CASE WHEN pf.U_CollectionFee_IsPercentage = 1 THEN pf.U_CollectionFee_Value
								 ELSE 0
							END + CASE WHEN pf.U_AdminFee_IsPercentage = 1 THEN pf.U_AdminFee_Value
									   ELSE 0
								  END + CASE WHEN pf.U_PolicyFee_IsPercentage = 1 THEN pf.U_PolicyFee_Value
											 ELSE 0
										END + CASE WHEN pf.U_SpottersFee_IsPercentage = 1
														AND pf.U_Spot_OnceOff = 0 THEN pf.U_SpottersFee_Value
												   ELSE 0
											  END U_PercentageFees, CASE WHEN pf.B_CollectionFee_IsPercentage = 1 THEN pf.B_CollectionFee_Value
																		 ELSE 0
																	END + CASE WHEN pf.B_AdminFee_IsPercentage = 1 THEN pf.B_AdminFee_Value
																			   ELSE 0
																		  END + CASE WHEN pf.B_PolicyFee_IsPercentage = 1 THEN pf.B_PolicyFee_Value
																					 ELSE 0
																				END + CASE WHEN pf.B_SpottersFee_IsPercentage = 1
																								AND pf.B_Spot_OnceOff = 0 THEN pf.B_SpottersFee_Value
																						   ELSE 0
																					  END + CASE WHEN pf.B_Brokerfee_IsPercentage = 1 THEN pf.B_BrokerFee_Value
																								 ELSE 0
																							END B_PercentageFees,
							+CASE WHEN pf.A_CollectionFee_IsPercentage = 1 THEN pf.A_CollectionFee_Value
								  ELSE 0
							 END + CASE WHEN pf.A_AdminFee_IsPercentage = 1 THEN pf.A_AdminFee_Value
										ELSE 0
								   END + CASE WHEN pf.A_PolicyFee_IsPercentage = 1 THEN pf.A_PolicyFee_Value
											  ELSE 0
										 END A_PercentageFees, 
									 
										 CASE WHEN pf.U_CollectionFee_IsPercentage = 0 THEN pf.U_CollectionFee_Value
																	ELSE 0
															   END + CASE WHEN pf.U_AdminFee_IsPercentage = 0 THEN pf.U_AdminFee_Value
																		  ELSE 0
																	 END + CASE WHEN pf.U_PolicyFee_IsPercentage = 0 THEN pf.U_PolicyFee_Value
																				ELSE 0
																		   END + CASE WHEN pf.U_SpottersFee_IsPercentage = 0
																						   AND pf.U_Spot_OnceOff = 0 THEN pf.U_SpottersFee_Value
																					  ELSE 0
																				 END + pf.U_ComputerFee_Amt U_RandFees,
							CASE WHEN pf.B_CollectionFee_IsPercentage = 0 THEN pf.B_CollectionFee_Value
								 ELSE 0
							END + CASE WHEN pf.B_AdminFee_IsPercentage = 0 THEN pf.B_AdminFee_Value
									   ELSE 0
								  END + CASE WHEN pf.B_PolicyFee_IsPercentage = 0 THEN pf.B_PolicyFee_Value
											 ELSE 0
										END + CASE WHEN pf.B_SpottersFee_IsPercentage = 0
														AND pf.B_Spot_OnceOff = 0 THEN pf.B_SpottersFee_Value
												   ELSE 0
											  END + pf.B_ComputerFee_Amt + CASE WHEN pf.B_Brokerfee_IsPercentage = 0 THEN pf.B_BrokerFee_Value
																				ELSE 0
																		   END B_RandFees,
							CASE WHEN pf.A_CollectionFee_IsPercentage = 0 THEN pf.A_CollectionFee_Value
								 ELSE 0
							END + CASE WHEN pf.A_AdminFee_IsPercentage = 0 THEN pf.A_AdminFee_Value
									   ELSE 0
								  END + CASE WHEN pf.A_PolicyFee_IsPercentage = 0 THEN pf.A_PolicyFee_Value
											 ELSE 0
										END A_RandFees, CASE WHEN pf.U_CollectionFee_Refund = 1
																  OR pf.U_AdminFee_Refund = 1
																  OR pf.U_PolicyFee_Refund = 1
																  OR pf.U_SpottersFee_Refund = 1 
																  OR pf.U_ComputerFee_Refund = 1
																  THEN 1
															 ELSE 0
														END U_AllowRefund, CASE WHEN pf.B_CollectionFee_Refund = 1
																					 OR pf.B_AdminFee_Refund = 1
																					 OR pf.B_PolicyFee_Refund = 1
																					 OR pf.B_SpottersFee_Refund = 1
																					 OR pf.B_BrokerFee_Refund = 1 
																					 OR pf.B_ComputerFee_Refund = 1 THEN 1
																				ELSE 0
																		   END B_AllowRefund, CASE WHEN pf.A_CollectionFee_Refund = 1
																										OR pf.A_AdminFee_Refund = 1
																										OR pf.A_PolicyFee_Refund = 1 THEN 1
																								   ELSE 0
																							  END A_AllowRefund,
							(SELECT MIN(MinValue.U_A_MinValue)
							 FROM   ( VALUES ( pf.U_CF_MinP), ( pf.U_AF_MinP), ( pf.U_PF_MinP), ( pf.U_Spot_MinP) ) MinValue (U_A_MinValue)
							) U_Percentage_MinValue,
							(SELECT MIN(MinValue.U_A_MinValue)
							 FROM   ( VALUES ( pf.B_CF_MinP), ( pf.B_AF_MinP), ( pf.B_PF_MinP), ( pf.B_Spot_MinP) ) MinValue (U_A_MinValue)
							) B_Percentage_MinValue, (SELECT    MIN(MinValue.U_A_MinValue)
													  FROM      ( VALUES ( pf.A_CF_MinP), ( pf.A_AF_MinP), ( pf.A_PF_MinP) ) MinValue (U_A_MinValue)
													 ) A_Percentage_MinValue,
							(SELECT MIN(MinValue.U_A_MinValue)
							 FROM   ( VALUES ( pf.U_CF_MinA), ( pf.U_AF_MinA), ( pf.U_PF_MinA), ( pf.U_Comp_MinA), ( pf.U_Spot_MinA) ) MinValue (U_A_MinValue)
							) U_Rand_MinValue,
							(SELECT MIN(MinValue.U_A_MinValue)
							 FROM   ( VALUES ( pf.B_CF_MinA), ( pf.B_AF_MinA), ( pf.B_PF_MinA), ( pf.B_Comp_MinA), ( pf.B_Spot_MinA) ) MinValue (U_A_MinValue)
							) B_Rand_MinValue, (SELECT  MIN(MinValue.U_A_MinValue)
												FROM    ( VALUES ( pf.A_CF_MinA), ( pf.A_AF_MinA), ( pf.A_PF_MinA) ) MinValue (U_A_MinValue)
											   ) A_Rand_MinValue,
							(SELECT MAX(MaxValue.U_A_MaxValue)
							 FROM   ( VALUES ( pf.U_CF_MaxP), ( pf.U_AF_MaxP), ( pf.U_PF_MaxP), ( pf.U_Spot_MaxP) ) MaxValue (U_A_MaxValue)
							) U_Percentage_MaxValue,
							(SELECT MAX(MaxValue.U_A_MaxValue)
							 FROM   ( VALUES ( pf.B_CF_MaxP), ( pf.B_AF_MaxP), ( pf.B_PF_MaxP), ( pf.B_Spot_MaxP) ) MaxValue (U_A_MaxValue)
							) B_Percentage_MaxValue, (SELECT    MAX(MaxValue.U_A_MaxValue)
													  FROM      ( VALUES ( pf.A_CF_MaxP), ( pf.A_AF_MaxP), ( pf.A_PF_MaxP) ) MaxValue (U_A_MaxValue)
													 ) A_Percentage_MaxValue,
							(SELECT MAX(MaxValue.U_A_MaxValue)
							 FROM   ( VALUES ( pf.U_CF_MaxA), ( pf.U_AF_MaxA), ( pf.U_PF_MaxA), ( pf.U_Comp_MaxA), ( pf.U_Spot_MaxA) ) MaxValue (U_A_MaxValue)
							) U_Rand_MaxValue,
							(SELECT MAX(MaxValue.U_A_MaxValue)
							 FROM   ( VALUES ( pf.B_CF_MaxA), ( pf.B_AF_MaxA), ( pf.B_PF_MaxA), ( pf.B_Comp_MaxA), ( pf.B_Spot_MaxA) ) MaxValue (U_A_MaxValue)
							) B_Rand_MaxValue, (SELECT  MAX(MaxValue.U_A_MaxValue)
												FROM    ( VALUES ( pf.A_CF_MaxA), ( pf.A_AF_MaxA), ( pf.A_PF_MaxA) ) MaxValue (U_A_MaxValue)
											   ) A_Rand_MaxValue, CASE WHEN pf.U_CollectionFee_ProRata = 1
																  OR pf.U_AdminFee_ProRata = 1
																  OR pf.U_PolicyFee_ProRata = 1
																  OR pf.U_SpottersFee_ProRata = 1 
																  OR pf.U_ComputerFee_ProRata = 1
																  THEN 1
															 ELSE 0
														END U_AllowProRata, CASE WHEN pf.B_CollectionFee_ProRata = 1
																					 OR pf.B_AdminFee_ProRata = 1
																					 OR pf.B_PolicyFee_ProRata = 1
																					 OR pf.B_SpottersFee_ProRata = 1
																					 OR pf.B_BrokerFee_ProRata = 1 
																					 OR pf.B_ComputerFee_ProRata = 1 THEN 1
																				ELSE 0
																		   END B_AllowProRata, CASE WHEN pf.A_CollectionFee_ProRata = 1
																										OR pf.A_AdminFee_ProRata = 1
																										OR pf.A_PolicyFee_ProRata = 1 THEN 1
																								   ELSE 0
																							  END A_AllowProRata, 
							ROW_NUMBER() OVER (PARTITION BY pf.BrokerageGUID, pf.ProductGUID ORDER BY (SELECT 0 )) rowno
				  FROM      ' + @DatabaseName + '.dbo.PB_Fees pf
							JOIN ' + @DatabaseName + '.dbo.SS_PaymentPlans pp ON pp.PaymentPlanGUID = pf.PaymentPlanGUID
							JOIN ' + @DatabaseName
		+ '.dbo.PB_ProductMaster pm ON pm.ProductGUID = pf.ProductGUID
				  ';
	SET @SQLExec2_Part2 = '
				  WHERE     pf.Deleted = 0
							AND pf.PolicyContentLinkGUID = ''00000000-0000-0000-0000-000000000000''
							--AND pm.ProductCodeIplatform != ''''
							AND pp.PaymentPlanName IN (''Annual'', ''Monthly'')
				 )
		INSERT  #CimsFees (BrokerageGUID, ProductCodeIplatform, ProductCode, PaymentPlanId, U_PercentageFees, B_PercentageFees, A_PercentageFees, U_RandFees,
						   B_RandFees, A_RandFees, U_AllowRefund, B_AllowRefund, A_AllowRefund, U_Percentage_MinValue, U_Percentage_MaxValue, B_Percentage_MinValue,
						   B_Percentage_MaxValue, A_Percentage_MinValue, A_Percentage_MaxValue, U_Rand_MinValue, U_Rand_MaxValue, B_Rand_MinValue, B_Rand_MaxValue,
						   A_Rand_MinValue, A_Rand_MaxValue)
				SELECT  fee.BrokerageGUID, fee.ProductCodeIplatform, fee.ProductCode, fee.PaymentPlanId,
						CONVERT(DECIMAL(19, 5), fee.U_PercentageFees) U_PercentageFees, CONVERT(DECIMAL(19, 5), fee.B_PercentageFees) B_PercentageFees,
						CONVERT(DECIMAL(19, 5), fee.A_PercentageFees) A_PercentageFees, CONVERT(DECIMAL(19, 5), fee.U_RandFees) U_RandFees,
						CONVERT(DECIMAL(19, 5), fee.B_RandFees) B_RandFees, CONVERT(DECIMAL(19, 5), fee.A_RandFees) A_RandFees, fee.U_AllowRefund, fee.B_AllowRefund,
						fee.A_AllowRefund, CONVERT(DECIMAL(19, 5), fee.U_Percentage_MinValue) U_Percentage_MinValue,
						CONVERT(DECIMAL(19, 5), fee.U_Percentage_MaxValue) U_Percentage_MaxValue,
						CONVERT(DECIMAL(19, 5), fee.B_Percentage_MinValue) B_Percentage_MinValue,
						CONVERT(DECIMAL(19, 5), fee.B_Percentage_MaxValue) B_Percentage_MaxValue,
						CONVERT(DECIMAL(19, 5), fee.A_Percentage_MinValue) A_Percentage_MinValue,
						CONVERT(DECIMAL(19, 5), fee.A_Percentage_MaxValue) A_Percentage_MaxValue, CONVERT(DECIMAL(19, 5), fee.U_Rand_MinValue) U_Rand_MinValue,
						CONVERT(DECIMAL(19, 5), fee.U_Rand_MaxValue) U_Rand_MaxValue, CONVERT(DECIMAL(19, 5), fee.B_Rand_MinValue) B_Rand_MinValue,
						CONVERT(DECIMAL(19, 5), fee.B_Rand_MaxValue) B_Rand_MaxValue, CONVERT(DECIMAL(19, 5), fee.A_Rand_MinValue) A_Rand_MinValue,
						CONVERT(DECIMAL(19, 5), fee.A_Rand_MaxValue) A_Rand_MaxValue
				FROM    fee
				WHERE   fee.rowno = 1 ;
			
	WITH    fee
			  AS (SELECT    pf.BrokerageGUID, pm.ProductCode, pm.ProductCodeIplatform, CASE WHEN pp.PaymentPlanName = ''Annual'' THEN 1
																							ELSE 3
																					   END PaymentPlanId,
							CASE WHEN pf.U_SpottersFee_IsPercentage = 1 THEN pf.U_SpottersFee_Value
								 ELSE 0
							END U_PercentageFees, CASE WHEN pf.U_SpottersFee_IsPercentage = 0 THEN pf.U_SpottersFee_Value
													   ELSE 0
												  END U_RandFees, pf.U_SpottersFee_Refund U_AllowRefund, pf.U_Spot_MinP U_Percentage_MinValue,
							pf.U_Spot_MaxP U_Percentage_MaxValue, pf.U_Spot_MinA U_Rand_MinValue, pf.U_Spot_MaxA U_Rand_MaxValue, pf.U_SpottersFee_ProRata U_AllowProRata,
							ROW_NUMBER() OVER (PARTITION BY pf.BrokerageGUID, pf.ProductGUID ORDER BY (SELECT   0
																									  )) rowno
				  FROM      ' + @DatabaseName + '.dbo.PB_Fees pf
							JOIN ' + @DatabaseName + '.dbo.SS_PaymentPlans pp ON pp.PaymentPlanGUID = pf.PaymentPlanGUID
							JOIN ' + @DatabaseName
		+ '.dbo.PB_ProductMaster pm ON pm.ProductGUID = pf.ProductGUID
				  WHERE     pf.Deleted = 0
							AND pf.PolicyContentLinkGUID = ''00000000-0000-0000-0000-000000000000''
							--AND pm.ProductCodeIplatform != ''
							AND pp.PaymentPlanName IN (''Annual'', ''Monthly'')
							AND pf.U_Spot_OnceOff = 1
				 )
		INSERT  #CimsFees (BrokerageGUID, ProductCode, ProductCodeIplatform, PaymentPlanId, U_PercentageFees, U_RandFees, U_AllowRefund, U_Percentage_MinValue,
						   U_Percentage_MaxValue, U_Rand_MinValue, U_Rand_MaxValue, U_AllowProRata)
				SELECT  fee.BrokerageGUID, fee.ProductCode, fee.ProductCodeIplatform, fee.PaymentPlanId, fee.U_PercentageFees, fee.U_RandFees, fee.U_AllowRefund,
						fee.U_Percentage_MinValue, fee.U_Percentage_MaxValue, fee.U_Rand_MinValue, fee.U_Rand_MaxValue, fee.U_AllowProRata
				FROM    fee
				WHERE   fee.rowno = 1;			

	WITH    fee
			  AS (SELECT    pf.BrokerageGUID, pm.ProductCode, pm.ProductCodeIplatform, CASE WHEN pp.PaymentPlanName = ''Annual'' THEN 1
																							ELSE 3
																					   END PaymentPlanId,
							CASE WHEN pf.B_SpottersFee_IsPercentage = 1 THEN pf.B_SpottersFee_Value
								 ELSE 0
							END B_PercentageFees, CASE WHEN pf.B_SpottersFee_IsPercentage = 0 THEN pf.B_SpottersFee_Value
													   ELSE 0
												  END B_RandFees, pf.B_SpottersFee_Refund B_AllowRefund, pf.B_Spot_MinP B_Percentage_MinValue,
							pf.B_Spot_MaxP B_Percentage_MaxValue, pf.B_Spot_MinA B_Rand_MinValue, pf.B_Spot_MaxA B_Rand_MaxValue, pf.B_SpottersFee_ProRata B_AllowProRata,
							ROW_NUMBER() OVER (PARTITION BY pf.BrokerageGUID, pf.ProductGUID ORDER BY (SELECT   0
																									  )) rowno
				  FROM      ' + @DatabaseName + '.dbo.PB_Fees pf
							JOIN ' + @DatabaseName + '.dbo.SS_PaymentPlans pp ON pp.PaymentPlanGUID = pf.PaymentPlanGUID
							JOIN ' + @DatabaseName + '.dbo.PB_ProductMaster pm ON pm.ProductGUID = pf.ProductGUID
				  WHERE     pf.Deleted = 0
							AND pf.PolicyContentLinkGUID = ''00000000-0000-0000-0000-000000000000''
							--AND pm.ProductCodeIplatform != ''
							AND pp.PaymentPlanName IN (''Annual'', ''Monthly'')
							AND pf.B_Spot_OnceOff = 1
				 )
		INSERT  #CimsFees (BrokerageGUID, ProductCode, ProductCodeIplatform, PaymentPlanId, B_PercentageFees, B_RandFees, B_AllowRefund, B_Percentage_MinValue,
						   B_Percentage_MaxValue, B_Rand_MinValue, B_Rand_MaxValue, B_AllowProRata)
				SELECT  fee.BrokerageGUID, fee.ProductCode, fee.ProductCodeIplatform, fee.PaymentPlanId, fee.B_PercentageFees, fee.B_RandFees, fee.B_AllowRefund,
						fee.B_Percentage_MinValue, fee.B_Percentage_MaxValue, fee.B_Rand_MinValue, fee.B_Rand_MaxValue, fee.B_AllowProRata
				FROM    fee
				WHERE   fee.rowno = 1; '

	--		Get Cims3 Data
	EXEC(@SQLExec2 + @SQLExec2_Part2);

	DELETE  #CimsFees
	WHERE   ProductCodeIplatform NOT IN (SELECT p.ProductCode
										 FROM   #ProductCodes p);

	DELETE  #CimsFees
	WHERE   BrokerageGUID NOT IN (SELECT    c.EntityGUID
								  FROM      #Channel c)
			AND BrokerageGUID != '00000000-0000-0000-0000-000000000000';

	SET @SQLExec_Update = '
	UPDATE  ed
	SET     ed.ExternalReference = u.ExternalReference
	FROM    ' + @DatabaseName + '.dbo.EntityDetails ed
			JOIN #User u ON u.EntityGUID = ed.EntityGUID
	WHERE   u.AlreadyExists = 0

	UPDATE  ch
	SET     ch.ExternalReference = c.ExternalReference
	FROM    ' + @DatabaseName + '.dbo.EntityDetails ch
			JOIN #Channel c ON c.EntityGUID = ch.EntityGUID
	WHERE   c.AlreadyExists = 0';

	--		Update External reference in Cims3
	EXEC sys.sp_sqlexec @p1 = @SQLExec_Update;



	SET @LastID = (SELECT   MAX(c.Id)
				   FROM     dbo.Channel c
				  );

	WITH    up
			  AS (SELECT    c.Rowno, ROW_NUMBER() OVER (ORDER BY (SELECT    0
																 )) Rowno_up
				  FROM      #Channel c
				  WHERE     c.AlreadyExists = 0
				 )
		UPDATE  up
		SET     up.Rowno = up.Rowno_up;

	--		Channel
	INSERT  dbo.Channel (SystemId, IsActive, ActivatedOn, IsDefault, IsDeleted, CurrencyId, DateFormat, Name, LanguageId, PasswordStrengthEnabled, Code,
						 CountryId, ExternalReference, RequestBroker, RequestAccountExecutive)
			SELECT  c.SystemId, 1, GETDATE(), 0, 0, 1, 'dd MMM yyyy', CASE c.TradingName
																						   WHEN '' THEN c.BranchName
																						   ELSE c.TradingName + ' ' + c.BranchName
																						 END, 2, 0, c.BrokerCode, 197, c.ExternalReference, 1, 1
			FROM    #Channel c
			WHERE   c.AlreadyExists = 0;

	UPDATE  c
	SET     c.ID = ch.Id
	FROM    #Channel c
			JOIN dbo.Channel ch ON ch.ExternalReference COLLATE SQL_Latin1_General_CP1_CI_AS = c.ExternalReference;

	UPDATE  c
	SET     c.ParentChannelId = c2.Id
	FROM    dbo.Channel c
			JOIN #Channel ch ON ch.ExternalReference COLLATE SQL_Latin1_General_CP1_CI_AS = c.ExternalReference
			JOIN dbo.Channel c2 ON ch.ParentExternalReference = c2.ExternalReference COLLATE SQL_Latin1_General_CP1_CI_AS;

	UPDATE  bf
	SET     bf.ChannelId = c.ID
	FROM    #BrokerageFee bf
			JOIN #Channel c ON c.EntityGUID = bf.BrokerageGUID; 

	UPDATE  cf
	SET     cf.ChannelId = c.ID
	FROM    #CimsFees cf
			JOIN #Channel c ON c.EntityGUID = cf.BrokerageGUID; 


	INSERT  dbo.Campaign (Name, Reference, StartDate, EndDate, DateCreated, DateUpdated, DefaultCampaign, IsDeleted, IsUsed, ChannelId, IsDisabled, Audit,
						  CampaignHoursFrom, CampaignHoursTo, MaximumLeadCallbackLimit, AutoRescheduleCallbacks, AutoRescheduleCallbackHours,
						  AllowSimilarCampaignLeadCallbacks, AllowLeadCallbacksOutsideCampaignHours, UseLastInFirstOutCallScheme, DailySalesTarget,
						  DailyPremiumTarget, TotalWeekWorkDays, IsWorkdayMonday, IsWorkdayTuesday, IsWorkdayWednesday, IsWorkdayThursday, IsWorkdayFriday,
						  IsWorkdaySaturday, IsWorkdaySunday, MaximumLeadsPerAgent)
	OUTPUT  Inserted.Id, Inserted.ChannelId
			INTO #Campaign (Id, ChannelId)
			SELECT  'Rodel', CASE c.TradingName
							   WHEN '' THEN c.BranchName
							   ELSE c.TradingName
							 END Name, GETDATE(), NULL, GETDATE(), GETDATE(), 0, 0, 0, c.ID, 0, NULL, '08:00', '17:00', 6, 0, 1, 0, 0, 0, 0, 0.00000, 0, 0, 0, 0, 0,
					0, 0, 0, 5
			FROM    #Channel c
			WHERE   NOT EXISTS ( SELECT 1
								 FROM   dbo.Campaign cp
								 WHERE  cp.IsDeleted = 0
										AND cp.ChannelId = c.ID )
					AND c.AlreadyExists = 0;

	INSERT  #BrokerageProducts (EntityGUID, ProductCode, ChannelId, ProductCodeIplatform)
			SELECT  c.EntityGUID, p.ProductCode, c.ID, p.ProductCode
			FROM    #Channel c
					CROSS JOIN #ProductCodes p;
	--SET @SQLExec3 = '
	--INSERT  #BrokerageProducts (EntityGUID, ProductCode, ProductCodeIplatform, ChannelId)
	--        SELECT DISTINCT
	--                c.EntityGUID, p.ProductCode, p.ProductCodeIplatform, c.ID
	--        FROM    ' + @DatabaseName + '.dbo.PB_ProductMaster p
	--                JOIN ' + @DatabaseName + '.dbo.PB_BrokerageProducts bp ON p.ProductGUID = bp.ProductGUID
	--                JOIN #Channel c ON c.EntityGUID = bp.EntityGUID
	--        WHERE   p.Deleted = 0
	--                AND bp.ActiveProduct = 1
	--                AND bp.Deleted = 0 ;'

	----		Get Cims3 Data
	--EXEC sys.sp_sqlexec @p1 = @SQLExec3;


	INSERT  #ProductFee (MasterId, ProductCode, ProductFeeTypeId, PaymentPlanId, BrokerageId, Value, MinValue, MaxValue, IsOnceOff, IsPercentage, AllowRefund,
						 AllowProRata, IsDeleted, ChannelId)
			SELECT  0 MasterId, cf.ProductCodeIplatform ProductCode, CASE WHEN cf.U_PercentageFees > 0
																			   OR cf.U_RandFees > 0 THEN 3
																		  WHEN cf.B_PercentageFees > 0
																			   OR cf.B_RandFees > 0 THEN 2
																		  WHEN cf.A_PercentageFees > 0
																			   OR cf.A_RandFees > 0 THEN 1
																		  ELSE 2
																	 END ProductFeeTypeId, cf.PaymentPlanId, NULL BrokerageId,
					(SELECT MAX(MaxValue.Value)
					 FROM   ( VALUES ( cf.U_PercentageFees), ( cf.B_PercentageFees), ( cf.A_PercentageFees), ( cf.U_RandFees), ( cf.B_RandFees), ( cf.A_RandFees) ) MaxValue (Value)
					) Value,
					(SELECT MIN(MinValue.MinValue)
					 FROM   ( VALUES ( cf.U_Percentage_MinValue), ( cf.B_Percentage_MinValue ), ( cf.A_Percentage_MinValue), ( cf.U_Rand_MinValue ),
							( cf.B_Rand_MinValue), ( cf.A_Rand_MinValue ) ) MinValue (MinValue)
					) MinValue,
					(SELECT MAX(MaxValue.MaxValue)
					 FROM   ( VALUES ( cf.U_Percentage_MaxValue ), ( cf.B_Percentage_MaxValue), ( cf.A_Percentage_MaxValue), ( cf.U_Rand_MaxValue ),
							( cf.B_Rand_MaxValue ), ( cf.A_Rand_MaxValue ) ) MaxValue (MaxValue)
					) MaxValue, cf.IsOnceOff, CASE WHEN cf.U_PercentageFees > 0
														OR cf.B_PercentageFees > 0
														OR cf.A_PercentageFees > 0 THEN 1
												   ELSE 0
											  END IsPercentage,
					(SELECT MAX(MaxValue.AllowRefund)
					 FROM   ( VALUES ( cf.U_AllowRefund ), ( cf.B_AllowRefund ), ( cf.A_AllowRefund ) ) MaxValue (AllowRefund)
					) AllowRefund, (SELECT  MAX(MaxValue.AllowProRata)
									FROM    ( VALUES ( cf.U_AllowProRata ), ( cf.B_AllowProRata ), ( cf.A_AllowProRata ) ) MaxValue (AllowProRata)
								   ) AllowProRata, 0 IsDeleted, cf.ChannelId
			FROM    #CimsFees cf;
	
	--		User 
	MERGE INTO dbo.[User]
	USING #User u
	ON 0 = 1
	WHEN NOT MATCHED AND u.AlreadyExists = 0 THEN
		INSERT (UserName, Password, SystemId, IsApproved, IsLoggedIn, IsActive, CreatedOn, ModifiedOn, IsDeleted, RegistrationToken, ExternalReference, IsBroker,
				IsAccountExecutive)
		VALUES (u.Username, u.Password, '00000000-0000-0000-0000-000000000000', 1, 0, 1, GETDATE(), GETDATE(), 0, '00000000-0000-0000-0000-000000000000',
				u.ExternalReference, u.IsBrokerageContact, u.IsBrokerageAccountsExec)
	OUTPUT
		Inserted.Id, u.EntityGUID, u.Firstnames, u.Surname, u.Username, u.IDNo
		INTO #UserMap (ID, EntityGUID, FirstName, Surname, UserName, IdentityNo);

	--		UserChannel    
	INSERT  dbo.UserChannel (CreatedAt, ModifiedAt, UserId, ChannelId, IsDeleted, IsDefault)
			SELECT  GETDATE(), GETDATE(), um.ID, c.ID, 0, 1
			FROM    #UserMap um
					JOIN #User u ON u.EntityGUID = um.EntityGUID
					JOIN #Channel c ON u.ParentEntityGUID = c.EntityGUID
			WHERE   u.AlreadyExists = 0;

	--		Add admin@iplatform.co.za to all Channels
	INSERT  dbo.UserChannel (CreatedAt, ModifiedAt, UserId, ChannelId, IsDeleted, IsDefault)
			SELECT  GETDATE(), GETDATE(), um.Id, c.ID, 0, 0
			FROM    #Channel c
					CROSS JOIN (SELECT  u.Id
								FROM    dbo.[User] u
								WHERE   u.UserName = 'admin@iplatform.co.za'
							   ) um
			WHERE   c.AlreadyExists = 0
					AND NOT EXISTS ( SELECT 1
									 FROM   dbo.UserChannel uc
									 WHERE  uc.IsDeleted = 0
											AND c.ID = uc.ChannelId
											AND um.Id = uc.UserId );
	
	--		UserAuthorisationGroup	
	INSERT  dbo.UserAuthorisationGroup (AuthorisationGroupId, UserId, ChannelId, IsDeleted)
			SELECT  2, um.ID, c.ID, 0
			FROM    #UserMap um
					JOIN #User u ON u.EntityGUID = um.EntityGUID
					JOIN #Channel c ON u.ParentEntityGUID = c.EntityGUID
			WHERE   u.AlreadyExists = 0;

	--		Add admin@iplatform.co.za to all UserAuthorisationGroup
	INSERT  dbo.UserAuthorisationGroup (AuthorisationGroupId, UserId, ChannelId, IsDeleted)
			SELECT  2, um.Id, c.ID, 0
			FROM    #Channel c
					CROSS JOIN (SELECT  u.Id
								FROM    dbo.[User] u
								WHERE   u.UserName = 'admin@iplatform.co.za'
							   ) um
			WHERE   c.AlreadyExists = 0
					AND NOT EXISTS ( SELECT 1
									 FROM   dbo.UserAuthorisationGroup ag
									 WHERE  ag.IsDeleted = 0
											AND c.ID = ag.ChannelId
											AND um.Id = ag.UserId );

	DELETE  bp
	FROM    #BrokerageProducts bp
	WHERE   bp.ProductCode NOT IN (SELECT   p.ProductCode COLLATE SQL_Latin1_General_CP1_CI_AS
								   FROM     dbo.Product p);

	--		ChannelEvent & ChannelEventTask
	INSERT  #ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode)
			SELECT  bp.EventName, bp.ChannelId, 0, bp.ProductCode
			FROM    #BrokerageProducts bp
			WHERE   NOT EXISTS ( SELECT 1
								 FROM   dbo.ChannelEvent ce
								 WHERE  ce.IsDeleted = 0
										AND bp.ProductCode = ce.ProductCode COLLATE SQL_Latin1_General_CP1_CI_AS
										AND bp.ChannelId = ce.ChannelId );

	INSERT  dbo.ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode)
	OUTPUT  2, Inserted.Id, 0
			INTO #ChannelEventTask (TaskName, ChannelEventId, IsDeleted)
			SELECT  ce.EventName, ce.ChannelId, ce.IsDeleted, ce.ProductCode
			FROM    #ChannelEvent ce;

	INSERT  dbo.ChannelEventTask (TaskName, ChannelEventId, IsDeleted)
			SELECT  et.TaskName, et.ChannelEventId, et.IsDeleted
			FROM    #ChannelEventTask et;

	--		ProductFee
	--Delete Product Fees where exists for insert
	DELETE  pf
	FROM    dbo.ProductFee pf
	WHERE   EXISTS ( SELECT 1
					 FROM   #ProductFee p
					 WHERE  pf.ProductId = pf.ProductId
							AND (p.ChannelId = pf.ChannelId
								 OR (p.ChannelId IS NULL
									 AND pf.ChannelId IS NULL)) );

	INSERT  dbo.ProductFee (MasterId, ProductId, ProductFeeTypeId, PaymentPlanId, BrokerageId, Value, MinValue, MaxValue, IsOnceOff, IsPercentage, AllowRefund,
							AllowProRata, IsDeleted, ChannelId)
			SELECT  f.MasterId, p.Id ProductId, f.ProductFeeTypeId, f.PaymentPlanId, f.BrokerageId, f.Value, f.MinValue, f.MaxValue, f.IsOnceOff, f.IsPercentage,
					f.AllowRefund, f.AllowProRata, f.IsDeleted, f.ChannelId
			FROM    #ProductFee f
					JOIN dbo.Product p ON p.ProductCode COLLATE SQL_Latin1_General_CP1_CI_AS = f.ProductCode
			WHERE   NOT EXISTS ( SELECT 1
								 FROM   dbo.ProductFee pf2
								 WHERE  pf2.IsDeleted = 0
										AND p.Id = pf2.ProductId
										AND f.ChannelId = pf2.ChannelId );

	--		SettingsiRate
	INSERT  dbo.SettingsiRate (ProductId, Password, AgentCode, BrokerCode, AuthCode, SchemeCode, Token, Environment, UserId, SubscriberCode, CompanyCode,
							   UwCompanyCode, UwProductCode, IsDeleted, ProductCode, ChannelSystemId, ChannelId)
			SELECT  p.Id, i.Password, i.AgentCode, c.BrokerCode, i.AuthCode, i.SchemeCode, i.Token, i.Environment, i.UserId, i.SubscriberCode, i.CompanyCode,
					i.UwCompanyCode, i.UwProductCode, 0 IsDeleted, ce.ProductCode, c.SystemId, c.ID
			FROM    #ChannelEvent ce
					JOIN dbo.Product p ON p.ProductCode COLLATE SQL_Latin1_General_CP1_CI_AS = ce.ProductCode
					JOIN #Channel c ON ce.ChannelId = c.ID
					JOIN #SettingsiRate_Info i ON p.Id = i.ProductId
			WHERE   NOT EXISTS ( SELECT 1
								 FROM   dbo.SettingsiRate sr
								 WHERE  sr.IsDeleted = 0
										AND sr.ProductId = p.Id
										AND sr.ChannelId = c.ID );

	--		SettingsQuoteUpload
	INSERT  dbo.SettingsQuoteUpload (SettingName, SettingValue, Environment, IsDeleted, ProductId, ChannelId)
			SELECT  l.SettingName, l.SettingValue, l.Environment, ce.IsDeleted, p.Id, ce.ChannelId
			FROM    #ChannelEvent ce
					CROSS JOIN #SettingsQuoteUpload_Info l
					JOIN dbo.Product p ON p.ProductCode COLLATE SQL_Latin1_General_CP1_CI_AS = ce.ProductCode
										  AND l.ProductId = p.Id
			WHERE   NOT EXISTS ( SELECT 1
								 FROM   dbo.SettingsQuoteUpload sq
								 WHERE  sq.IsDeleted = 0
										AND ce.ChannelId = sq.ChannelId
										AND p.Id = sq.ProductId );

	--		EmailCommunicationSetting
	INSERT  dbo.EmailCommunicationSetting (Host, Port, UseSSL, UseDefaultCredentials, UserName, Password, DefaultFrom, DefaultBCC, DefaultContactNumber,
										   SubAccountID, IsDeleted, ChannelId)
			SELECT  i.Host, i.Port, i.UseSSL, i.UseDefaultCredentials, i.UserName, i.Password, c.Email, i.DefaultBCC, c.WorkPhone, i.SubAccountID, 0, c.ID
			FROM    #Channel c
					CROSS JOIN #EmailCommunicationSetting_Info i
			WHERE   c.AlreadyExists = 0;

	--		VehicleGuideSetting
	INSERT  dbo.VehicleGuideSetting (IsDeleted, ChannelId, CountryId, ApiKey, Email, MMBookEnabled, LightstoneEnabled, LSA_UserId, LSA_ContractId, LSA_CustomerId,
									 LSA_PackageId, LSA_UserName, LSA_Password, KeAutoKey, KeAutoSecret, Evalue8Username, Evalue8Password, Evalue8Enabled,
									 TransUnionAutoEnabled, RegCheckUkEnabled, VehicleValuesPassword, VehicleValuesHashKey, VehicleValuesApiKey, VehicleValuesUserId,
									 VehicleValuesReportCode)
			SELECT  vg.IsDeleted, c.ID, vg.CountryId, vg.ApiKey, vg.Email, vg.MMBookEnabled, vg.LightstoneEnabled, vg.LSA_UserId, vg.LSA_ContractId,
					vg.LSA_CustomerId, vg.LSA_PackageId, vg.LSA_UserName, vg.LSA_Password, vg.KeAutoKey, vg.KeAutoSecret, vg.Evalue8Username, vg.Evalue8Password,
					vg.Evalue8Enabled, vg.TransUnionAutoEnabled, vg.RegCheckUkEnabled, vg.VehicleValuesPassword, vg.VehicleValuesHashKey, vg.VehicleValuesApiKey,
					vg.VehicleValuesUserId, vg.VehicleValuesReportCode
			FROM    #VehicleGuideSetting vg
					CROSS JOIN #Channel c
			WHERE   c.AlreadyExists = 0;

	INSERT  dbo.PersonLookupSetting (IsDeleted, ChannelId, CountryId, ApiKey, Email, InvoiceReference, PCubedEnabled, PCubedUsername, PCubedPassword, IPRSEnabled,
									 IPRSUsername, IPRSPassword, LightStonePropertyEnabled, LightStonePropertyUserId, TransunionEnabled, TransunionFirstName,
									 TransunionSurname, TransunionSubscriberCode, TransunionSecurityCode, TransUnionEndPoint, TransUnionGetClaims,
									 TransUnionGetDrivers)
			SELECT  pl.IsDeleted, c.ID, pl.CountryId, pl.ApiKey, pl.Email, pl.InvoiceReference, pl.PCubedEnabled, pl.PCubedUsername, pl.PCubedPassword,
					pl.IPRSEnabled, pl.IPRSUsername, pl.IPRSPassword, pl.LightStonePropertyEnabled, pl.LightStonePropertyUserId, pl.TransunionEnabled,
					pl.TransunionFirstName, pl.TransunionSurname, pl.TransunionSubscriberCode, pl.TransunionSecurityCode, pl.TransUnionEndPoint,
					pl.TransUnionGetClaims, pl.TransUnionGetDrivers
			FROM    #PersonLookupSetting pl
					CROSS JOIN #Channel c
			WHERE   c.AlreadyExists = 0;

	INSERT  dbo.CampaignProduct (CampaignId, ProductId, IsDeleted)
			SELECT  c.Id, pr.Id, 0
			FROM    #Campaign c
					CROSS JOIN (SELECT  p.Id
								FROM    dbo.Product p
										JOIN #ProductCodes pc ON pc.ProductCode = p.ProductCode COLLATE SQL_Latin1_General_CP1_CI_AS
							   ) pr
			WHERE   NOT EXISTS ( SELECT 1
								 FROM   dbo.CampaignProduct cp
								 WHERE  cp.IsDeleted = 0
										AND c.Id = cp.CampaignId
										AND pr.Id = cp.ProductId );

	INSERT  dbo.CampaignManager (IsDeleted, UserId, CampaignId)
			SELECT  0, um.ID, cp.Id
			FROM    #UserMap um
					JOIN #User u ON u.EntityGUID = um.EntityGUID
					JOIN #Channel c ON u.ParentEntityGUID = c.EntityGUID
					JOIN #Campaign cp ON cp.ChannelId = c.ID
			WHERE   NOT EXISTS ( SELECT 1
								 FROM   dbo.CampaignManager cm
								 WHERE  cm.UserId = um.ID
										AND cm.CampaignId = cp.Id );

	/*
	--		Links for User table
	SELECT  u.UserName, i.FirstName, i.Surname, i.IdentityNo
	FROM    dbo.UserIndividual ui
			JOIN dbo.[User] u ON u.Id = ui.UserId
			JOIN dbo.Individual i ON i.PartyId = ui.IndividualId
	*/
	MERGE INTO dbo.Party
	USING #UserMap um
		JOIN #User u ON u.EntityGUID = um.EntityGUID
		JOIN #Channel c ON u.ParentEntityGUID = c.EntityGUID
	ON 0 = 1
		AND u.AlreadyExists = 0
	WHEN NOT MATCHED THEN
		INSERT (MasterId, PartyTypeId, ContactDetailId, DisplayName, DateCreated, DateUpdated, IsDeleted, ChannelId, ExternalReference, Audit)
		VALUES (0, 2, NULL, u.Username, NULL, NULL, 0, c.ID, NULL, NULL)
	OUTPUT
		Inserted.Id, um.ID, Inserted.DisplayName, um.FirstName, u.Surname, c.ID, um.IdentityNo
		INTO #Party (ID, UserID, DisplayName, FirstName, Surname, ChannelId, IdentityNo);

	INSERT  dbo.Individual (PartyId, TitleId, FirstName, MiddleName, Surname, DateOfBirth, IdentityNo, PassportNo, GenderId, MaritalStatusId, LanguageId,
							OccupationId, PreferredName, AnyJudgements, AnyJudgementsReason, UnderAdministrationOrDebtReview, UnderAdministrationOrDebtReviewReason,
							BeenSequestratedOrLiquidated, BeenSequestratedOrLiquidatedReason, PinNo, ExternalReference, InsurancePolicyCancelledOrRenewalRefused,
							InsurancePolicyCancelledOrRenewalRefusedReason)
			SELECT  p.ID, NULL, p.FirstName, NULL, p.Surname, NULL, p.IdentityNo, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL,
					LEFT(p.DisplayName, 40) DisplayName, 0, NULL
			FROM    #Party p;

	INSERT  dbo.UserIndividual (UserId, IndividualId, CreatedAt, ModifiedAt, IsDeleted)
			SELECT  UserID, ID, GETDATE(), GETDATE(), 0
			FROM    #Party;

	INSERT  dbo.CampaignAgent (CampaignId, PartyId, IsDeleted)
			SELECT  c.Id, p.ID, 0
			FROM    #Party p
					JOIN #Campaign c ON p.ChannelId = c.ChannelId;

	--		Update Individual info
	UPDATE  i
	SET     i.FirstName = u2.Firstnames
	FROM    dbo.UserIndividual ui
			JOIN dbo.[User] u ON u.Id = ui.UserId
			JOIN dbo.Individual i ON i.PartyId = ui.IndividualId
			JOIN #User u2 ON u2.ExternalReference = u.ExternalReference COLLATE SQL_Latin1_General_CP1_CI_AS
	WHERE   u2.AlreadyExists = 1
			AND i.FirstName COLLATE SQL_Latin1_General_CP1_CI_AS != u2.Firstnames;


	UPDATE  i
	SET     i.Surname = u2.Surname
	FROM    dbo.UserIndividual ui
			JOIN dbo.[User] u ON u.Id = ui.UserId
			JOIN dbo.Individual i ON i.PartyId = ui.IndividualId
			JOIN #User u2 ON u2.ExternalReference = u.ExternalReference COLLATE SQL_Latin1_General_CP1_CI_AS
	WHERE   u2.AlreadyExists = 1
			AND i.Surname COLLATE SQL_Latin1_General_CP1_CI_AS != u2.Surname;


	UPDATE  i
	SET     i.IdentityNo = u2.IDNo
	FROM    dbo.UserIndividual ui
			JOIN dbo.[User] u ON u.Id = ui.UserId
			JOIN dbo.Individual i ON i.PartyId = ui.IndividualId
			JOIN #User u2 ON u2.ExternalReference = u.ExternalReference COLLATE SQL_Latin1_General_CP1_CI_AS
	WHERE   u2.AlreadyExists = 1
			AND (i.IdentityNo COLLATE SQL_Latin1_General_CP1_CI_AS != u2.IDNo
				 OR i.IdentityNo IS NULL);


GO