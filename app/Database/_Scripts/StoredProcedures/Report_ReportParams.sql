use ibroker
go

if exists (select * from sys.objects where object_id = object_id(N'Report_ReportParams') and type in (N'P', N'PC'))
	begin
		drop procedure Report_ReportParams
	end
go

create procedure Report_ReportParams
(
	@ReportId int
)
as
	select
		[Field],
		DataTypeId,
		VisibleIndex,
		--IconId,
        Name,
        [Description],
        IsVisibleInReportViewer,
        IsVisibleInScheduler
	from ReportParam
	where
		(
			IsDeleted is null or
			IsDeleted = 0
		) and
		ReportId = @ReportId
	order by
		VisibleIndex asc
go

exec Report_ReportParams 2