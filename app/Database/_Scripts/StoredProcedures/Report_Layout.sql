use ibroker
go

if exists (select * from sys.objects where object_id = object_id(N'Report_Layout') and type in (N'P', N'PC'))
	begin
		drop procedure Report_Layout
	end
go

create procedure Report_Layout
(
	@ChannelId int,
	@ReportName varchar(50)
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select count(ReportLayout.Id) from Report inner join ReportLayout on ReportLayout.ReportId = Report.Id where Report.Name = @ReportName and Report.ChannelId = @ChannelId) > 0)
		begin
			select
				ReportLayout.Name,
				ReportLayout.Label,
				ReportLayout.[Value],
				ReportLayout.IsVisible
			from Report
				inner join ReportLayout on ReportLayout.ReportId = Report.Id
			where
				Report.Name = @ReportName and
				Report.ChannelId = @ChannelId
		end
	else
		begin
			select
				ReportLayout.Name,
				ReportLayout.Label,
				ReportLayout.[Value],
				ReportLayout.IsVisible
			from Report
				inner join ReportLayout on ReportLayout.ReportId = Report.Id
			where
				Report.Name = @ReportName and
				Report.ChannelId = @IpChannelId
		end
go

exec Report_Layout 26, 'Comparative Quote'