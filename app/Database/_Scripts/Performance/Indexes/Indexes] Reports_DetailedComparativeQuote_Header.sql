use [ibroker]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Quote_6_1490104349__K16_K4_K1_K8_K13_K5] ON [dbo].[Quote]
(
	[QuoteHeaderId] ASC,
	[ProductId] ASC,
	[Id] ASC,
	[IsDeleted] ASC,
	[InsurerReference] ASC,
	[Fees] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1490104349_16_13_5_1_8] ON [dbo].[Quote]([QuoteHeaderId], [InsurerReference], [Fees], [Id], [IsDeleted])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Party_6_293576084__K1_K8_K4_5] ON [dbo].[Party]
(
	[Id] ASC,
	[IsDeleted] ASC,
	[ContactDetailId] ASC
)
INCLUDE ( 	[DisplayName]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_293576084_1_4] ON [dbo].[Party]([Id], [ContactDetailId])
go

CREATE STATISTICS [_dta_stat_293576084_8_1] ON [dbo].[Party]([IsDeleted], [Id])
go

CREATE STATISTICS [_dta_stat_293576084_8_4_1] ON [dbo].[Party]([IsDeleted], [ContactDetailId], [Id])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K15_K2_K6_K5] ON [dbo].[QuoteItem]
(
	[IsDeleted] ASC,
	[QuoteId] ASC,
	[Sasria] ASC,
	[Premium] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1570104634_2_6] ON [dbo].[QuoteItem]([QuoteId], [Sasria])
go

CREATE STATISTICS [_dta_stat_1570104634_2_5_6] ON [dbo].[QuoteItem]([QuoteId], [Premium], [Sasria])
go

CREATE STATISTICS [_dta_stat_1570104634_15_6_2] ON [dbo].[QuoteItem]([IsDeleted], [Sasria], [QuoteId])
go

CREATE STATISTICS [_dta_stat_1570104634_5_6_15_2] ON [dbo].[QuoteItem]([Premium], [Sasria], [IsDeleted], [QuoteId])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_ContactDetail_6_261575970__K1_K13_K2_K5_K6_K4_K7] ON [dbo].[ContactDetail]
(
	[Id] ASC,
	[IsDeleted] ASC,
	[Work] ASC,
	[Home] ASC,
	[Fax] ASC,
	[Cell] ASC,
	[Email] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_261575970_2_5_6_4_7_13_1] ON [dbo].[ContactDetail]([Work], [Home], [Fax], [Cell], [Email], [IsDeleted], [Id])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Address_6_201767776__K19_K2_K14_K1_K15_5_6_7_8_9_10_16] ON [dbo].[Address]
(
	[IsDeleted] ASC,
	[PartyId] ASC,
	[AddressTypeId] ASC,
	[Id] ASC,
	[IsDefault] ASC
)
INCLUDE ( 	[Complex],
	[Line1],
	[Line2],
	[Line3],
	[Line4],
	[Code],
	[IsComplex]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Address_6_201767776__K2_K14_K1_K19_K15_5_6_7_8_9_10_16] ON [dbo].[Address]
(
	[PartyId] ASC,
	[AddressTypeId] ASC,
	[Id] ASC,
	[IsDeleted] ASC,
	[IsDefault] ASC
)
INCLUDE ( 	[Complex],
	[Line1],
	[Line2],
	[Line3],
	[Line4],
	[Code],
	[IsComplex]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_201767776_2_1] ON [dbo].[Address]([PartyId], [Id])
go

CREATE STATISTICS [_dta_stat_201767776_15_19_14] ON [dbo].[Address]([IsDefault], [IsDeleted], [AddressTypeId])
go

CREATE STATISTICS [_dta_stat_201767776_14_1_2] ON [dbo].[Address]([AddressTypeId], [Id], [PartyId])
go

CREATE STATISTICS [_dta_stat_201767776_1_19_2] ON [dbo].[Address]([Id], [IsDeleted], [PartyId])
go

CREATE STATISTICS [_dta_stat_201767776_1_15_2_14] ON [dbo].[Address]([Id], [IsDefault], [PartyId], [AddressTypeId])
go

CREATE STATISTICS [_dta_stat_201767776_19_15_2_14] ON [dbo].[Address]([IsDeleted], [IsDefault], [PartyId], [AddressTypeId])
go

CREATE STATISTICS [_dta_stat_201767776_15_1_2_19] ON [dbo].[Address]([IsDefault], [Id], [PartyId], [IsDeleted])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteHeader_6_1495676376__K14_K4_K1] ON [dbo].[QuoteHeader]
(
	[IsRerated] ASC,
	[ProposalHeaderId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K14_K1_K10] ON [dbo].[LeadActivity]
(
	[IsDeleted] ASC,
	[Id] ASC,
	[PartyId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_434100587_10_1] ON [dbo].[LeadActivity]([PartyId], [Id])
go

CREATE STATISTICS [_dta_stat_434100587_14_10_1] ON [dbo].[LeadActivity]([IsDeleted], [PartyId], [Id])
go

