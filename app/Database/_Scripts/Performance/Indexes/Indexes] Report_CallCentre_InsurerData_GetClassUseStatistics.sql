use [ibroker]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalQuestionAnswer_6_1262627541__K2_K6_K7] ON [dbo].[ProposalQuestionAnswer]
(
	[ProposalDefinitionId] ASC,
	[QuestionDefinitionId] ASC,
	[Answer] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1262627541_7_2_6] ON [dbo].[ProposalQuestionAnswer]([Answer], [ProposalDefinitionId], [QuestionDefinitionId])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuestionDefinition_6_1733581214__K5_K1] ON [dbo].[QuestionDefinition]
(
	[QuestionId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1733581214_1_5] ON [dbo].[QuestionDefinition]([Id], [QuestionId])
go

CREATE STATISTICS [_dta_stat_1733581214_10_1_5] ON [dbo].[QuestionDefinition]([RatingFactor], [Id], [QuestionId])
go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalDefinition_6_1102626971__K5_K2_K1] ON [dbo].[ProposalDefinition]
(
	[CoverId] ASC,
	[ProposalHeaderId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1102626971_1_2] ON [dbo].[ProposalDefinition]([Id], [ProposalHeaderId])
go

CREATE STATISTICS [_dta_stat_1102626971_5_1_2] ON [dbo].[ProposalDefinition]([CoverId], [Id], [ProposalHeaderId])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteHeader_6_1495676376__K14_1_4] ON [dbo].[QuoteHeader]
(
	[IsRerated] ASC
)
INCLUDE ( 	[Id],
	[ProposalHeaderId]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1495676376_4_14] ON [dbo].[QuoteHeader]([ProposalHeaderId], [IsRerated])
go

CREATE STATISTICS [_dta_stat_1495676376_4_1] ON [dbo].[QuoteHeader]([ProposalHeaderId], [Id])
go

CREATE STATISTICS [_dta_stat_1495676376_14_1_4] ON [dbo].[QuoteHeader]([IsRerated], [Id], [ProposalHeaderId])
go

CREATE STATISTICS [_dta_stat_434100587_2_6] ON [dbo].[LeadActivity]([LeadId], [CampaignId])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_QuestionAnswer_6_771533832__K1_K3] ON [md].[QuestionAnswer]
(
	[Id] ASC,
	[Answer] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_739533718_4_1] ON [md].[Question]([QuestionTypeId], [Id])
go

