use [ibroker]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Question_6_739533718__K1_K3_K2] ON [md].[Question]
(
	[Id] ASC,
	[QuestionGroupId] ASC,
	[Name] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Question_6_739533718__K1_K2] ON [md].[Question]
(
	[Id] ASC,
	[Name] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_739533718_3_1] ON [md].[Question]([QuestionGroupId], [Id])
go

CREATE STATISTICS [_dta_stat_739533718_1_2_3] ON [md].[Question]([Id], [Name], [QuestionGroupId])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuestionDefinition_6_1733581214__K1_5] ON [dbo].[QuestionDefinition]
(
	[Id] ASC
)
INCLUDE ( 	[QuestionId]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalDefinition_6_1102626971__K2_K1] ON [dbo].[ProposalDefinition]
(
	[ProposalHeaderId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1262627541_6_7] ON [dbo].[ProposalQuestionAnswer]([QuestionDefinitionId], [Answer])
go

use [ibroker]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_ProductAdditionalExcess_6_89819432__K13_K2_1_3_4_5_6_7_8_9_10_11_14_15] ON [dbo].[ProductAdditionalExcess]
(
	[CoverDefinitionId] ASC,
	[Index] ASC
)
INCLUDE ( 	[Id],
	[Category],
	[Description],
	[ActualExcess],
	[MinExcess],
	[MaxExcess],
	[Percentage],
	[IsPercentageOfClaim],
	[IsPercentageOfItem],
	[IsPercentageOfSumInsured],
	[ShouldDisplayExcessValues],
	[ShouldApplySelectedExcess]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_89819432_2_13] ON [dbo].[ProductAdditionalExcess]([Index], [CoverDefinitionId])
go

use [ibroker]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_ProductAdditionalExcess_6_89819432__K13_3] ON [dbo].[ProductAdditionalExcess]
(
	[CoverDefinitionId] ASC
)
INCLUDE ( 	[Category]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1102626971_11_5] ON [dbo].[ProposalDefinition]([IsDeleted], [CoverId])
go

CREATE STATISTICS [_dta_stat_1102626971_2_11] ON [dbo].[ProposalDefinition]([ProposalHeaderId], [IsDeleted])
go

use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K2_K15_K1_K3_K4] ON [dbo].[QuoteItem]
(
	[QuoteId] ASC,
	[IsDeleted] ASC,
	[Id] ASC,
	[CoverDefinitionId] ASC,
	[SumInsured] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K15_K2_K1_K3_K7_K4] ON [dbo].[QuoteItem]
(
	[IsDeleted] ASC,
	[QuoteId] ASC,
	[Id] ASC,
	[CoverDefinitionId] ASC,
	[AssetNumber] ASC,
	[SumInsured] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1570104634_1_4] ON [dbo].[QuoteItem]([Id], [SumInsured])
go

CREATE STATISTICS [_dta_stat_1570104634_4_15_2] ON [dbo].[QuoteItem]([SumInsured], [IsDeleted], [QuoteId])
go

CREATE STATISTICS [_dta_stat_1570104634_3_15_1_2_4] ON [dbo].[QuoteItem]([CoverDefinitionId], [IsDeleted], [Id], [QuoteId], [SumInsured])
go

CREATE STATISTICS [_dta_stat_1570104634_15_1_2_3_7_4] ON [dbo].[QuoteItem]([IsDeleted], [Id], [QuoteId], [CoverDefinitionId], [AssetNumber], [SumInsured])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Asset_6_1198627313__K4_K6_K1_2] ON [dbo].[Asset]
(
	[IsDeleted] ASC,
	[AssetNo] ASC,
	[Id] ASC
)
INCLUDE ( 	[Description]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Asset_6_1198627313__K4_K1_2] ON [dbo].[Asset]
(
	[IsDeleted] ASC,
	[Id] ASC
)
INCLUDE ( 	[Description]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1198627313_1_4] ON [dbo].[Asset]([Id], [IsDeleted])
go

CREATE STATISTICS [_dta_stat_1198627313_1_6_4] ON [dbo].[Asset]([Id], [AssetNo], [IsDeleted])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteHeader_6_1495676376__K4_K14] ON [dbo].[QuoteHeader]
(
	[ProposalHeaderId] ASC,
	[IsRerated] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalDefinition_6_1102626971__K5_K2_K11_K1_K13] ON [dbo].[ProposalDefinition]
(
	[CoverId] ASC,
	[ProposalHeaderId] ASC,
	[IsDeleted] ASC,
	[Id] ASC,
	[AssetId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalDefinition_6_1102626971__K2_K5_K11_1] ON [dbo].[ProposalDefinition]
(
	[ProposalHeaderId] ASC,
	[CoverId] ASC,
	[IsDeleted] ASC
)
INCLUDE ( 	[Id]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1102626971_11_1_5] ON [dbo].[ProposalDefinition]([IsDeleted], [Id], [CoverId])
go

CREATE STATISTICS [_dta_stat_1102626971_13_11_1_5] ON [dbo].[ProposalDefinition]([AssetId], [IsDeleted], [Id], [CoverId])
go

CREATE STATISTICS [_dta_stat_1102626971_1_13_5_2] ON [dbo].[ProposalDefinition]([Id], [AssetId], [CoverId], [ProposalHeaderId])
go

CREATE STATISTICS [_dta_stat_1102626971_2_5_1_11_13] ON [dbo].[ProposalDefinition]([ProposalHeaderId], [CoverId], [Id], [IsDeleted], [AssetId])
go

use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K15_K2_K1_K3_K7] ON [dbo].[QuoteItem]
(
	[IsDeleted] ASC,
	[QuoteId] ASC,
	[Id] ASC,
	[CoverDefinitionId] ASC,
	[AssetNumber] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1570104634_15_1_3_7] ON [dbo].[QuoteItem]([IsDeleted], [Id], [CoverDefinitionId], [AssetNumber])
go

CREATE STATISTICS [_dta_stat_1570104634_15_7_2_1_3] ON [dbo].[QuoteItem]([IsDeleted], [AssetNumber], [QuoteId], [Id], [CoverDefinitionId])
go

use [ibroker]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Quote_6_1490104349__K16_K4_K1_K8_K13_K5] ON [dbo].[Quote]
(
	[QuoteHeaderId] ASC,
	[ProductId] ASC,
	[Id] ASC,
	[IsDeleted] ASC,
	[InsurerReference] ASC,
	[Fees] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1490104349_16_13_5_1_8] ON [dbo].[Quote]([QuoteHeaderId], [InsurerReference], [Fees], [Id], [IsDeleted])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Party_6_293576084__K1_K8_K4_5] ON [dbo].[Party]
(
	[Id] ASC,
	[IsDeleted] ASC,
	[ContactDetailId] ASC
)
INCLUDE ( 	[DisplayName]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_293576084_1_4] ON [dbo].[Party]([Id], [ContactDetailId])
go

CREATE STATISTICS [_dta_stat_293576084_8_1] ON [dbo].[Party]([IsDeleted], [Id])
go

CREATE STATISTICS [_dta_stat_293576084_8_4_1] ON [dbo].[Party]([IsDeleted], [ContactDetailId], [Id])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K15_K2_K6_K5] ON [dbo].[QuoteItem]
(
	[IsDeleted] ASC,
	[QuoteId] ASC,
	[Sasria] ASC,
	[Premium] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1570104634_2_6] ON [dbo].[QuoteItem]([QuoteId], [Sasria])
go

CREATE STATISTICS [_dta_stat_1570104634_2_5_6] ON [dbo].[QuoteItem]([QuoteId], [Premium], [Sasria])
go

CREATE STATISTICS [_dta_stat_1570104634_15_6_2] ON [dbo].[QuoteItem]([IsDeleted], [Sasria], [QuoteId])
go

CREATE STATISTICS [_dta_stat_1570104634_5_6_15_2] ON [dbo].[QuoteItem]([Premium], [Sasria], [IsDeleted], [QuoteId])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_ContactDetail_6_261575970__K1_K13_K2_K5_K6_K4_K7] ON [dbo].[ContactDetail]
(
	[Id] ASC,
	[IsDeleted] ASC,
	[Work] ASC,
	[Home] ASC,
	[Fax] ASC,
	[Cell] ASC,
	[Email] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_261575970_2_5_6_4_7_13_1] ON [dbo].[ContactDetail]([Work], [Home], [Fax], [Cell], [Email], [IsDeleted], [Id])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Address_6_201767776__K19_K2_K14_K1_K15_5_6_7_8_9_10_16] ON [dbo].[Address]
(
	[IsDeleted] ASC,
	[PartyId] ASC,
	[AddressTypeId] ASC,
	[Id] ASC,
	[IsDefault] ASC
)
INCLUDE ( 	[Complex],
	[Line1],
	[Line2],
	[Line3],
	[Line4],
	[Code],
	[IsComplex]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Address_6_201767776__K2_K14_K1_K19_K15_5_6_7_8_9_10_16] ON [dbo].[Address]
(
	[PartyId] ASC,
	[AddressTypeId] ASC,
	[Id] ASC,
	[IsDeleted] ASC,
	[IsDefault] ASC
)
INCLUDE ( 	[Complex],
	[Line1],
	[Line2],
	[Line3],
	[Line4],
	[Code],
	[IsComplex]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_201767776_2_1] ON [dbo].[Address]([PartyId], [Id])
go

CREATE STATISTICS [_dta_stat_201767776_15_19_14] ON [dbo].[Address]([IsDefault], [IsDeleted], [AddressTypeId])
go

CREATE STATISTICS [_dta_stat_201767776_14_1_2] ON [dbo].[Address]([AddressTypeId], [Id], [PartyId])
go

CREATE STATISTICS [_dta_stat_201767776_1_19_2] ON [dbo].[Address]([Id], [IsDeleted], [PartyId])
go

CREATE STATISTICS [_dta_stat_201767776_1_15_2_14] ON [dbo].[Address]([Id], [IsDefault], [PartyId], [AddressTypeId])
go

CREATE STATISTICS [_dta_stat_201767776_19_15_2_14] ON [dbo].[Address]([IsDeleted], [IsDefault], [PartyId], [AddressTypeId])
go

CREATE STATISTICS [_dta_stat_201767776_15_1_2_19] ON [dbo].[Address]([IsDefault], [Id], [PartyId], [IsDeleted])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteHeader_6_1495676376__K14_K4_K1] ON [dbo].[QuoteHeader]
(
	[IsRerated] ASC,
	[ProposalHeaderId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K14_K1_K10] ON [dbo].[LeadActivity]
(
	[IsDeleted] ASC,
	[Id] ASC,
	[PartyId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_434100587_10_1] ON [dbo].[LeadActivity]([PartyId], [Id])
go

CREATE STATISTICS [_dta_stat_434100587_14_10_1] ON [dbo].[LeadActivity]([IsDeleted], [PartyId], [Id])
go

use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K2_K1_K7_K3_5_6_9] ON [dbo].[QuoteItem]
(
	[QuoteId] ASC,
	[Id] ASC,
	[AssetNumber] ASC,
	[CoverDefinitionId] ASC
)
INCLUDE ( 	[Premium],
	[Sasria],
	[ExcessBasic]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K2_K1_K3_K7_5_6_9] ON [dbo].[QuoteItem]
(
	[QuoteId] ASC,
	[Id] ASC,
	[CoverDefinitionId] ASC,
	[AssetNumber] ASC
)
INCLUDE ( 	[Premium],
	[Sasria],
	[ExcessBasic]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K2_K3_5] ON [dbo].[QuoteItem]
(
	[QuoteId] ASC,
	[CoverDefinitionId] ASC
)
INCLUDE ( 	[Premium]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1570104634_7_2] ON [dbo].[QuoteItem]([AssetNumber], [QuoteId])
go

CREATE STATISTICS [_dta_stat_1570104634_7_1_2] ON [dbo].[QuoteItem]([AssetNumber], [Id], [QuoteId])
go

CREATE STATISTICS [_dta_stat_1570104634_6_2_3] ON [dbo].[QuoteItem]([Sasria], [QuoteId], [CoverDefinitionId])
go

CREATE STATISTICS [_dta_stat_1570104634_5_2_3] ON [dbo].[QuoteItem]([Premium], [QuoteId], [CoverDefinitionId])
go

CREATE STATISTICS [_dta_stat_1570104634_1_3_7_2] ON [dbo].[QuoteItem]([Id], [CoverDefinitionId], [AssetNumber], [QuoteId])
go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalDefinition_6_1102626971__K2_K13_K1] ON [dbo].[ProposalDefinition]
(
	[ProposalHeaderId] ASC,
	[AssetId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1102626971_1_13] ON [dbo].[ProposalDefinition]([Id], [AssetId])
go

CREATE NONCLUSTERED INDEX [_dta_index_Asset_6_1198627313__K6_K1] ON [dbo].[Asset]
(
	[AssetNo] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_Quote_6_1490104349__K4_K8_K1_K16] ON [dbo].[Quote]
(
	[ProductId] ASC,
	[IsDeleted] ASC,
	[Id] ASC,
	[QuoteHeaderId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_Quote_6_1490104349__K8_K1_4_16] ON [dbo].[Quote]
(
	[IsDeleted] ASC,
	[Id] ASC
)
INCLUDE ( 	[ProductId],
	[QuoteHeaderId]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1490104349_8_16_1] ON [dbo].[Quote]([IsDeleted], [QuoteHeaderId], [Id])
go

CREATE STATISTICS [_dta_stat_1490104349_1_4_16] ON [dbo].[Quote]([Id], [ProductId], [QuoteHeaderId])
go

CREATE STATISTICS [_dta_stat_1490104349_8_1_4_16] ON [dbo].[Quote]([IsDeleted], [Id], [ProductId], [QuoteHeaderId])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Individual_6_693577509__K1_4_6] ON [dbo].[Individual]
(
	[PartyId] ASC
)
INCLUDE ( 	[FirstName],
	[Surname]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_UserIndividual_6_944722418__K2_3] ON [dbo].[UserIndividual]
(
	[UserId] ASC
)
INCLUDE ( 	[IndividualId]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_944722418_2_3] ON [dbo].[UserIndividual]([UserId], [IndividualId])
go

CREATE NONCLUSTERED INDEX [_dta_index_UserAuthorisationGroup_6_432720594__K3] ON [dbo].[UserAuthorisationGroup]
(
	[UserId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K2_K15_K1_K3_5] ON [dbo].[QuoteItem]
(
	[QuoteId] ASC,
	[IsDeleted] ASC,
	[Id] ASC,
	[CoverDefinitionId] ASC
)
INCLUDE ( 	[Premium]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K15_K2_K3_K1] ON [dbo].[QuoteItem]
(
	[IsDeleted] ASC,
	[QuoteId] ASC,
	[CoverDefinitionId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1570104634_2_3] ON [dbo].[QuoteItem]([QuoteId], [CoverDefinitionId])
go

CREATE STATISTICS [_dta_stat_1570104634_1_3] ON [dbo].[QuoteItem]([Id], [CoverDefinitionId])
go

CREATE STATISTICS [_dta_stat_1570104634_15_1_3] ON [dbo].[QuoteItem]([IsDeleted], [Id], [CoverDefinitionId])
go

CREATE STATISTICS [_dta_stat_1570104634_2_1_3] ON [dbo].[QuoteItem]([QuoteId], [Id], [CoverDefinitionId])
go

CREATE STATISTICS [_dta_stat_1570104634_15_2_1] ON [dbo].[QuoteItem]([IsDeleted], [QuoteId], [Id])
go

CREATE STATISTICS [_dta_stat_1570104634_15_3_2_1] ON [dbo].[QuoteItem]([IsDeleted], [CoverDefinitionId], [QuoteId], [Id])
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K1_K9_K6_K15_K2] ON [dbo].[LeadActivity]
(
	[Id] ASC,
	[DateUpdated] ASC,
	[CampaignId] ASC,
	[UserId] ASC,
	[LeadId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K6_K15_K9_K2_K1] ON [dbo].[LeadActivity]
(
	[CampaignId] ASC,
	[UserId] ASC,
	[DateUpdated] ASC,
	[LeadId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K15_K6_K2_K9_1] ON [dbo].[LeadActivity]
(
	[UserId] ASC,
	[CampaignId] ASC,
	[LeadId] ASC,
	[DateUpdated] ASC
)
INCLUDE ( 	[Id]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K9_K6_K15_K1] ON [dbo].[LeadActivity]
(
	[DateUpdated] ASC,
	[CampaignId] ASC,
	[UserId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_434100587_15_9] ON [dbo].[LeadActivity]([UserId], [DateUpdated])
go

CREATE STATISTICS [_dta_stat_434100587_15_6_9_2] ON [dbo].[LeadActivity]([UserId], [CampaignId], [DateUpdated], [LeadId])
go

CREATE STATISTICS [_dta_stat_434100587_6_1_15_9_2] ON [dbo].[LeadActivity]([CampaignId], [Id], [UserId], [DateUpdated], [LeadId])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Channel_6_581681220__K1_K10] ON [dbo].[Channel]
(
	[Id] ASC,
	[Name] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_581681220_10_1] ON [dbo].[Channel]([Name], [Id])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Campaign_6_1250103494__K12_K1_K2] ON [dbo].[Campaign]
(
	[ChannelId] ASC,
	[Id] ASC,
	[Name] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1250103494_2_1] ON [dbo].[Campaign]([Name], [Id])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteHeader_6_1495676376__K1_K14] ON [dbo].[QuoteHeader]
(
	[Id] ASC,
	[IsRerated] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteHeader_6_1495676376__K14_K1] ON [dbo].[QuoteHeader]
(
	[IsRerated] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_User_6_71671303__K1] ON [dbo].[User]
(
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K2_K1_K3_K15_5_6] ON [dbo].[QuoteItem]
(
	[QuoteId] ASC,
	[Id] ASC,
	[CoverDefinitionId] ASC,
	[IsDeleted] ASC
)
INCLUDE ( 	[Premium],
	[Sasria]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_693577509_1_8] ON [dbo].[Individual]([PartyId], [IdentityNo])
go

CREATE STATISTICS [_dta_stat_434100587_2_9_6] ON [dbo].[LeadActivity]([LeadId], [DateUpdated], [CampaignId])
go

CREATE STATISTICS [_dta_stat_434100587_9_1_6_2] ON [dbo].[LeadActivity]([DateUpdated], [Id], [CampaignId], [LeadId])
go

CREATE STATISTICS [_dta_stat_1389248004_8_10] ON [dbo].[QuoteUploadLog]([DateCreated], [IsDeleted])
go

CREATE STATISTICS [_dta_stat_1389248004_11_8] ON [dbo].[QuoteUploadLog]([QuoteId], [DateCreated])
go

CREATE STATISTICS [_dta_stat_1389248004_10_11_8] ON [dbo].[QuoteUploadLog]([IsDeleted], [QuoteId], [DateCreated])
go

CREATE STATISTICS [_dta_stat_1250103494_12_2] ON [dbo].[Campaign]([ChannelId], [Name])
go

CREATE STATISTICS [_dta_stat_432720594_5_3] ON [dbo].[UserAuthorisationGroup]([IsDeleted], [UserId])
go

use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_Individual_6_693577509__K1_K10] ON [dbo].[Individual]
(
	[PartyId] ASC,
	[GenderId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_693577509_10_1] ON [dbo].[Individual]([GenderId], [PartyId])
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K6_K9_K1_K2] ON [dbo].[LeadActivity]
(
	[CampaignId] ASC,
	[DateUpdated] ASC,
	[Id] ASC,
	[LeadId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_434100587_1_2_6] ON [dbo].[LeadActivity]([Id], [LeadId], [CampaignId])
go

use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_Quote_6_1490104349__K16_K1_K4_K8] ON [dbo].[Quote]
(
	[QuoteHeaderId] ASC,
	[Id] ASC,
	[ProductId] ASC,
	[IsDeleted] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1490104349_4_16] ON [dbo].[Quote]([ProductId], [QuoteHeaderId])
go

CREATE STATISTICS [_dta_stat_1490104349_8_4_16] ON [dbo].[Quote]([IsDeleted], [ProductId], [QuoteHeaderId])
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K6_K9_K1] ON [dbo].[LeadActivity]
(
	[CampaignId] ASC,
	[DateUpdated] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

use [ibroker]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalQuestionAnswer_6_1262627541__K2_K6_K7] ON [dbo].[ProposalQuestionAnswer]
(
	[ProposalDefinitionId] ASC,
	[QuestionDefinitionId] ASC,
	[Answer] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1262627541_7_2_6] ON [dbo].[ProposalQuestionAnswer]([Answer], [ProposalDefinitionId], [QuestionDefinitionId])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuestionDefinition_6_1733581214__K5_K1] ON [dbo].[QuestionDefinition]
(
	[QuestionId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1733581214_1_5] ON [dbo].[QuestionDefinition]([Id], [QuestionId])
go

CREATE STATISTICS [_dta_stat_1733581214_10_1_5] ON [dbo].[QuestionDefinition]([RatingFactor], [Id], [QuestionId])
go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalDefinition_6_1102626971__K5_K2_K1] ON [dbo].[ProposalDefinition]
(
	[CoverId] ASC,
	[ProposalHeaderId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1102626971_1_2] ON [dbo].[ProposalDefinition]([Id], [ProposalHeaderId])
go

CREATE STATISTICS [_dta_stat_1102626971_5_1_2] ON [dbo].[ProposalDefinition]([CoverId], [Id], [ProposalHeaderId])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteHeader_6_1495676376__K14_1_4] ON [dbo].[QuoteHeader]
(
	[IsRerated] ASC
)
INCLUDE ( 	[Id],
	[ProposalHeaderId]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1495676376_4_14] ON [dbo].[QuoteHeader]([ProposalHeaderId], [IsRerated])
go

CREATE STATISTICS [_dta_stat_1495676376_4_1] ON [dbo].[QuoteHeader]([ProposalHeaderId], [Id])
go

CREATE STATISTICS [_dta_stat_1495676376_14_1_4] ON [dbo].[QuoteHeader]([IsRerated], [Id], [ProposalHeaderId])
go

CREATE STATISTICS [_dta_stat_434100587_2_6] ON [dbo].[LeadActivity]([LeadId], [CampaignId])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_QuestionAnswer_6_771533832__K1_K3] ON [md].[QuestionAnswer]
(
	[Id] ASC,
	[Answer] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_739533718_4_1] ON [md].[Question]([QuestionTypeId], [Id])
go

use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_Address_6_201767776__K14_K2_K13] ON [dbo].[Address]
(
	[AddressTypeId] ASC,
	[PartyId] ASC,
	[StateProvinceId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_201767776_2_14] ON [dbo].[Address]([PartyId], [AddressTypeId])
go

CREATE STATISTICS [_dta_stat_201767776_2_13] ON [dbo].[Address]([PartyId], [StateProvinceId])
go

CREATE STATISTICS [_dta_stat_201767776_13_14_2] ON [dbo].[Address]([StateProvinceId], [AddressTypeId], [PartyId])
go

CREATE STATISTICS [_dta_stat_693577509_15_1] ON [dbo].[Individual]([AnyJudgements], [PartyId])
go

use [ibroker]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalQuestionAnswer_6_1262627541__K6_K2_K7] ON [dbo].[ProposalQuestionAnswer]
(
	[QuestionDefinitionId] ASC,
	[ProposalDefinitionId] ASC,
	[Answer] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_Quote_6_1490104349__K16_K8_K4] ON [dbo].[Quote]
(
	[QuoteHeaderId] ASC,
	[IsDeleted] ASC,
	[ProductId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuestionDefinition_6_1733581214__K5] ON [dbo].[QuestionDefinition]
(
	[QuestionId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalDefinition_6_1102626971__K1_K5_K2] ON [dbo].[ProposalDefinition]
(
	[Id] ASC,
	[CoverId] ASC,
	[ProposalHeaderId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_CampaignLeadBucket_6_635201363__K12_K10_K11_K7_K13_K9] ON [dbo].[CampaignLeadBucket]
(
	[AgentId] ASC,
	[CampaignId] ASC,
	[LeadId] ASC,
	[CreatedOn] ASC,
	[LeadCallCentreCodeId] ASC,
	[IsDeleted] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_CampaignLeadBucket_6_635201363__K7_K13_K9_K10_K12] ON [dbo].[CampaignLeadBucket]
(
	[CreatedOn] ASC,
	[LeadCallCentreCodeId] ASC,
	[IsDeleted] ASC,
	[CampaignId] ASC,
	[AgentId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_CampaignLeadBucket_6_635201363__K9_K10_K12_K7_K13_K11] ON [dbo].[CampaignLeadBucket]
(
	[IsDeleted] ASC,
	[CampaignId] ASC,
	[AgentId] ASC,
	[CreatedOn] ASC,
	[LeadCallCentreCodeId] ASC,
	[LeadId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_CampaignLeadBucket_6_635201363__K13_K10_K12_K7_K9_K11] ON [dbo].[CampaignLeadBucket]
(
	[LeadCallCentreCodeId] ASC,
	[CampaignId] ASC,
	[AgentId] ASC,
	[CreatedOn] ASC,
	[IsDeleted] ASC,
	[LeadId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_635201363_7_10] ON [dbo].[CampaignLeadBucket]([CreatedOn], [CampaignId])
go

CREATE STATISTICS [_dta_stat_635201363_7_13_12] ON [dbo].[CampaignLeadBucket]([CreatedOn], [LeadCallCentreCodeId], [AgentId])
go

CREATE STATISTICS [_dta_stat_635201363_7_9_10] ON [dbo].[CampaignLeadBucket]([CreatedOn], [IsDeleted], [CampaignId])
go

CREATE STATISTICS [_dta_stat_635201363_12_7_9] ON [dbo].[CampaignLeadBucket]([AgentId], [CreatedOn], [IsDeleted])
go

CREATE STATISTICS [_dta_stat_635201363_12_7_13_9] ON [dbo].[CampaignLeadBucket]([AgentId], [CreatedOn], [LeadCallCentreCodeId], [IsDeleted])
go

CREATE STATISTICS [_dta_stat_635201363_7_13_10_12_11] ON [dbo].[CampaignLeadBucket]([CreatedOn], [LeadCallCentreCodeId], [CampaignId], [AgentId], [LeadId])
go

CREATE STATISTICS [_dta_stat_635201363_12_10_11_7_9] ON [dbo].[CampaignLeadBucket]([AgentId], [CampaignId], [LeadId], [CreatedOn], [IsDeleted])
go

CREATE STATISTICS [_dta_stat_635201363_12_10_7_9_13_11] ON [dbo].[CampaignLeadBucket]([AgentId], [CampaignId], [CreatedOn], [IsDeleted], [LeadCallCentreCodeId], [LeadId])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Campaign_6_1250103494__K1_K2] ON [dbo].[Campaign]
(
	[Id] ASC,
	[Name] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_UserAuthorisationGroup_6_432720594__K5_K3_K1_K2] ON [dbo].[UserAuthorisationGroup]
(
	[IsDeleted] ASC,
	[UserId] ASC,
	[Id] ASC,
	[AuthorisationGroupId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_432720594_5_1] ON [dbo].[UserAuthorisationGroup]([IsDeleted], [Id])
go

CREATE STATISTICS [_dta_stat_432720594_1_2] ON [dbo].[UserAuthorisationGroup]([Id], [AuthorisationGroupId])
go

CREATE STATISTICS [_dta_stat_432720594_3_1_2] ON [dbo].[UserAuthorisationGroup]([UserId], [Id], [AuthorisationGroupId])
go

CREATE STATISTICS [_dta_stat_432720594_2_5_1] ON [dbo].[UserAuthorisationGroup]([AuthorisationGroupId], [IsDeleted], [Id])
go

CREATE STATISTICS [_dta_stat_432720594_1_3_5_2] ON [dbo].[UserAuthorisationGroup]([Id], [UserId], [IsDeleted], [AuthorisationGroupId])
go

CREATE NONCLUSTERED INDEX [_dta_index_UserIndividual_6_944722418__K6_K2_K1_K3] ON [dbo].[UserIndividual]
(
	[IsDeleted] ASC,
	[UserId] ASC,
	[Id] ASC,
	[IndividualId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_944722418_6_1] ON [dbo].[UserIndividual]([IsDeleted], [Id])
go

CREATE STATISTICS [_dta_stat_944722418_1_3_2] ON [dbo].[UserIndividual]([Id], [IndividualId], [UserId])
go

CREATE STATISTICS [_dta_stat_944722418_2_1_6_3] ON [dbo].[UserIndividual]([UserId], [Id], [IsDeleted], [IndividualId])
go

CREATE NONCLUSTERED INDEX [_dta_index_UserChannel_6_151671588__K6_K5_K4] ON [dbo].[UserChannel]
(
	[IsDeleted] ASC,
	[ChannelId] ASC,
	[UserId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_151671588_4_6] ON [dbo].[UserChannel]([UserId], [IsDeleted])
go

CREATE STATISTICS [_dta_stat_151671588_5_4] ON [dbo].[UserChannel]([ChannelId], [UserId])
go

CREATE STATISTICS [_dta_stat_151671588_5_6_4] ON [dbo].[UserChannel]([ChannelId], [IsDeleted], [UserId])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_User_6_71671303__K18_1_4] ON [dbo].[User]
(
	[IsDeleted] ASC
)
INCLUDE ( 	[Id],
	[UserName]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_71671303_18_1] ON [dbo].[User]([IsDeleted], [Id])
go

CREATE STATISTICS [_dta_stat_149679681_16_12] ON [dbo].[AuditLog]([AuditEntryType], [RequestDate])
go

CREATE STATISTICS [_dta_stat_149679681_12_17] ON [dbo].[AuditLog]([RequestDate], [EntityId])
go

CREATE STATISTICS [_dta_stat_149679681_14_17_16_12] ON [dbo].[AuditLog]([UserId], [EntityId], [AuditEntryType], [RequestDate])
go

CREATE STATISTICS [_dta_stat_581681220_7_1_10] ON [dbo].[Channel]([IsDeleted], [Id], [Name])
go

