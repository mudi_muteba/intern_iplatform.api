use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_Quote_6_1490104349__K4_K8_K1_K16] ON [dbo].[Quote]
(
	[ProductId] ASC,
	[IsDeleted] ASC,
	[Id] ASC,
	[QuoteHeaderId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_Quote_6_1490104349__K8_K1_4_16] ON [dbo].[Quote]
(
	[IsDeleted] ASC,
	[Id] ASC
)
INCLUDE ( 	[ProductId],
	[QuoteHeaderId]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1490104349_8_16_1] ON [dbo].[Quote]([IsDeleted], [QuoteHeaderId], [Id])
go

CREATE STATISTICS [_dta_stat_1490104349_1_4_16] ON [dbo].[Quote]([Id], [ProductId], [QuoteHeaderId])
go

CREATE STATISTICS [_dta_stat_1490104349_8_1_4_16] ON [dbo].[Quote]([IsDeleted], [Id], [ProductId], [QuoteHeaderId])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Individual_6_693577509__K1_4_6] ON [dbo].[Individual]
(
	[PartyId] ASC
)
INCLUDE ( 	[FirstName],
	[Surname]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_UserIndividual_6_944722418__K2_3] ON [dbo].[UserIndividual]
(
	[UserId] ASC
)
INCLUDE ( 	[IndividualId]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_944722418_2_3] ON [dbo].[UserIndividual]([UserId], [IndividualId])
go

CREATE NONCLUSTERED INDEX [_dta_index_UserAuthorisationGroup_6_432720594__K3] ON [dbo].[UserAuthorisationGroup]
(
	[UserId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K2_K15_K1_K3_5] ON [dbo].[QuoteItem]
(
	[QuoteId] ASC,
	[IsDeleted] ASC,
	[Id] ASC,
	[CoverDefinitionId] ASC
)
INCLUDE ( 	[Premium]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K15_K2_K3_K1] ON [dbo].[QuoteItem]
(
	[IsDeleted] ASC,
	[QuoteId] ASC,
	[CoverDefinitionId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1570104634_2_3] ON [dbo].[QuoteItem]([QuoteId], [CoverDefinitionId])
go

CREATE STATISTICS [_dta_stat_1570104634_1_3] ON [dbo].[QuoteItem]([Id], [CoverDefinitionId])
go

CREATE STATISTICS [_dta_stat_1570104634_15_1_3] ON [dbo].[QuoteItem]([IsDeleted], [Id], [CoverDefinitionId])
go

CREATE STATISTICS [_dta_stat_1570104634_2_1_3] ON [dbo].[QuoteItem]([QuoteId], [Id], [CoverDefinitionId])
go

CREATE STATISTICS [_dta_stat_1570104634_15_2_1] ON [dbo].[QuoteItem]([IsDeleted], [QuoteId], [Id])
go

CREATE STATISTICS [_dta_stat_1570104634_15_3_2_1] ON [dbo].[QuoteItem]([IsDeleted], [CoverDefinitionId], [QuoteId], [Id])
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K1_K9_K6_K15_K2] ON [dbo].[LeadActivity]
(
	[Id] ASC,
	[DateUpdated] ASC,
	[CampaignId] ASC,
	[UserId] ASC,
	[LeadId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K6_K15_K9_K2_K1] ON [dbo].[LeadActivity]
(
	[CampaignId] ASC,
	[UserId] ASC,
	[DateUpdated] ASC,
	[LeadId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K15_K6_K2_K9_1] ON [dbo].[LeadActivity]
(
	[UserId] ASC,
	[CampaignId] ASC,
	[LeadId] ASC,
	[DateUpdated] ASC
)
INCLUDE ( 	[Id]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K9_K6_K15_K1] ON [dbo].[LeadActivity]
(
	[DateUpdated] ASC,
	[CampaignId] ASC,
	[UserId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_434100587_15_9] ON [dbo].[LeadActivity]([UserId], [DateUpdated])
go

CREATE STATISTICS [_dta_stat_434100587_15_6_9_2] ON [dbo].[LeadActivity]([UserId], [CampaignId], [DateUpdated], [LeadId])
go

CREATE STATISTICS [_dta_stat_434100587_6_1_15_9_2] ON [dbo].[LeadActivity]([CampaignId], [Id], [UserId], [DateUpdated], [LeadId])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Channel_6_581681220__K1_K10] ON [dbo].[Channel]
(
	[Id] ASC,
	[Name] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_581681220_10_1] ON [dbo].[Channel]([Name], [Id])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Campaign_6_1250103494__K12_K1_K2] ON [dbo].[Campaign]
(
	[ChannelId] ASC,
	[Id] ASC,
	[Name] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1250103494_2_1] ON [dbo].[Campaign]([Name], [Id])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteHeader_6_1495676376__K1_K14] ON [dbo].[QuoteHeader]
(
	[Id] ASC,
	[IsRerated] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteHeader_6_1495676376__K14_K1] ON [dbo].[QuoteHeader]
(
	[IsRerated] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_User_6_71671303__K1] ON [dbo].[User]
(
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

