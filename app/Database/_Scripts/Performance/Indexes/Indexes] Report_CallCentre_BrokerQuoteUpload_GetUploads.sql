use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K2_K1_K3_K15_5_6] ON [dbo].[QuoteItem]
(
	[QuoteId] ASC,
	[Id] ASC,
	[CoverDefinitionId] ASC,
	[IsDeleted] ASC
)
INCLUDE ( 	[Premium],
	[Sasria]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_693577509_1_8] ON [dbo].[Individual]([PartyId], [IdentityNo])
go

CREATE STATISTICS [_dta_stat_434100587_2_9_6] ON [dbo].[LeadActivity]([LeadId], [DateUpdated], [CampaignId])
go

CREATE STATISTICS [_dta_stat_434100587_9_1_6_2] ON [dbo].[LeadActivity]([DateUpdated], [Id], [CampaignId], [LeadId])
go

CREATE STATISTICS [_dta_stat_1389248004_8_10] ON [dbo].[QuoteUploadLog]([DateCreated], [IsDeleted])
go

CREATE STATISTICS [_dta_stat_1389248004_11_8] ON [dbo].[QuoteUploadLog]([QuoteId], [DateCreated])
go

CREATE STATISTICS [_dta_stat_1389248004_10_11_8] ON [dbo].[QuoteUploadLog]([IsDeleted], [QuoteId], [DateCreated])
go

CREATE STATISTICS [_dta_stat_1250103494_12_2] ON [dbo].[Campaign]([ChannelId], [Name])
go

CREATE STATISTICS [_dta_stat_432720594_5_3] ON [dbo].[UserAuthorisationGroup]([IsDeleted], [UserId])
go

