use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_Address_6_201767776__K14_K2_K13] ON [dbo].[Address]
(
	[AddressTypeId] ASC,
	[PartyId] ASC,
	[StateProvinceId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_201767776_2_14] ON [dbo].[Address]([PartyId], [AddressTypeId])
go

CREATE STATISTICS [_dta_stat_201767776_2_13] ON [dbo].[Address]([PartyId], [StateProvinceId])
go

CREATE STATISTICS [_dta_stat_201767776_13_14_2] ON [dbo].[Address]([StateProvinceId], [AddressTypeId], [PartyId])
go

CREATE STATISTICS [_dta_stat_693577509_15_1] ON [dbo].[Individual]([AnyJudgements], [PartyId])
go

