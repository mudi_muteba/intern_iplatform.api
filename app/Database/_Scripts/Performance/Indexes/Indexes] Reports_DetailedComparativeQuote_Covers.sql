use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K15_K2_K1_K3_K7] ON [dbo].[QuoteItem]
(
	[IsDeleted] ASC,
	[QuoteId] ASC,
	[Id] ASC,
	[CoverDefinitionId] ASC,
	[AssetNumber] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1570104634_15_1_3_7] ON [dbo].[QuoteItem]([IsDeleted], [Id], [CoverDefinitionId], [AssetNumber])
go

CREATE STATISTICS [_dta_stat_1570104634_15_7_2_1_3] ON [dbo].[QuoteItem]([IsDeleted], [AssetNumber], [QuoteId], [Id], [CoverDefinitionId])
go

