use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_UserAuthorisationGroup_6_432720594__K5_K3_K1_K2] ON [dbo].[UserAuthorisationGroup]
(
	[IsDeleted] ASC,
	[UserId] ASC,
	[Id] ASC,
	[AuthorisationGroupId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_432720594_5_1] ON [dbo].[UserAuthorisationGroup]([IsDeleted], [Id])
go

CREATE STATISTICS [_dta_stat_432720594_1_2] ON [dbo].[UserAuthorisationGroup]([Id], [AuthorisationGroupId])
go

CREATE STATISTICS [_dta_stat_432720594_3_1_2] ON [dbo].[UserAuthorisationGroup]([UserId], [Id], [AuthorisationGroupId])
go

CREATE STATISTICS [_dta_stat_432720594_2_5_1] ON [dbo].[UserAuthorisationGroup]([AuthorisationGroupId], [IsDeleted], [Id])
go

CREATE STATISTICS [_dta_stat_432720594_1_3_5_2] ON [dbo].[UserAuthorisationGroup]([Id], [UserId], [IsDeleted], [AuthorisationGroupId])
go

CREATE NONCLUSTERED INDEX [_dta_index_UserIndividual_6_944722418__K6_K2_K1_K3] ON [dbo].[UserIndividual]
(
	[IsDeleted] ASC,
	[UserId] ASC,
	[Id] ASC,
	[IndividualId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_944722418_6_1] ON [dbo].[UserIndividual]([IsDeleted], [Id])
go

CREATE STATISTICS [_dta_stat_944722418_1_3_2] ON [dbo].[UserIndividual]([Id], [IndividualId], [UserId])
go

CREATE STATISTICS [_dta_stat_944722418_2_1_6_3] ON [dbo].[UserIndividual]([UserId], [Id], [IsDeleted], [IndividualId])
go

CREATE NONCLUSTERED INDEX [_dta_index_UserChannel_6_151671588__K6_K5_K4] ON [dbo].[UserChannel]
(
	[IsDeleted] ASC,
	[ChannelId] ASC,
	[UserId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_151671588_4_6] ON [dbo].[UserChannel]([UserId], [IsDeleted])
go

CREATE STATISTICS [_dta_stat_151671588_5_4] ON [dbo].[UserChannel]([ChannelId], [UserId])
go

CREATE STATISTICS [_dta_stat_151671588_5_6_4] ON [dbo].[UserChannel]([ChannelId], [IsDeleted], [UserId])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_User_6_71671303__K18_1_4] ON [dbo].[User]
(
	[IsDeleted] ASC
)
INCLUDE ( 	[Id],
	[UserName]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_71671303_18_1] ON [dbo].[User]([IsDeleted], [Id])
go

CREATE STATISTICS [_dta_stat_149679681_16_12] ON [dbo].[AuditLog]([AuditEntryType], [RequestDate])
go

CREATE STATISTICS [_dta_stat_149679681_12_17] ON [dbo].[AuditLog]([RequestDate], [EntityId])
go

CREATE STATISTICS [_dta_stat_149679681_14_17_16_12] ON [dbo].[AuditLog]([UserId], [EntityId], [AuditEntryType], [RequestDate])
go

CREATE STATISTICS [_dta_stat_581681220_7_1_10] ON [dbo].[Channel]([IsDeleted], [Id], [Name])
go

