use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_Individual_6_693577509__K1_K10] ON [dbo].[Individual]
(
	[PartyId] ASC,
	[GenderId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_693577509_10_1] ON [dbo].[Individual]([GenderId], [PartyId])
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K6_K9_K1_K2] ON [dbo].[LeadActivity]
(
	[CampaignId] ASC,
	[DateUpdated] ASC,
	[Id] ASC,
	[LeadId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_434100587_1_2_6] ON [dbo].[LeadActivity]([Id], [LeadId], [CampaignId])
go

