use [ibroker]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_ProductAdditionalExcess_6_89819432__K13_K2_1_3_4_5_6_7_8_9_10_11_14_15] ON [dbo].[ProductAdditionalExcess]
(
	[CoverDefinitionId] ASC,
	[Index] ASC
)
INCLUDE ( 	[Id],
	[Category],
	[Description],
	[ActualExcess],
	[MinExcess],
	[MaxExcess],
	[Percentage],
	[IsPercentageOfClaim],
	[IsPercentageOfItem],
	[IsPercentageOfSumInsured],
	[ShouldDisplayExcessValues],
	[ShouldApplySelectedExcess]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_89819432_2_13] ON [dbo].[ProductAdditionalExcess]([Index], [CoverDefinitionId])
go

