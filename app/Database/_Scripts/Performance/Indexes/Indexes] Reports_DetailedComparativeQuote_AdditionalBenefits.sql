use [ibroker]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Question_6_739533718__K1_K3_K2] ON [md].[Question]
(
	[Id] ASC,
	[QuestionGroupId] ASC,
	[Name] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Question_6_739533718__K1_K2] ON [md].[Question]
(
	[Id] ASC,
	[Name] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_739533718_3_1] ON [md].[Question]([QuestionGroupId], [Id])
go

CREATE STATISTICS [_dta_stat_739533718_1_2_3] ON [md].[Question]([Id], [Name], [QuestionGroupId])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuestionDefinition_6_1733581214__K1_5] ON [dbo].[QuestionDefinition]
(
	[Id] ASC
)
INCLUDE ( 	[QuestionId]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalDefinition_6_1102626971__K2_K1] ON [dbo].[ProposalDefinition]
(
	[ProposalHeaderId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1262627541_6_7] ON [dbo].[ProposalQuestionAnswer]([QuestionDefinitionId], [Answer])
go

