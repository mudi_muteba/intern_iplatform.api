use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_Quote_6_1490104349__K16_K1_K4_K8] ON [dbo].[Quote]
(
	[QuoteHeaderId] ASC,
	[Id] ASC,
	[ProductId] ASC,
	[IsDeleted] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1490104349_4_16] ON [dbo].[Quote]([ProductId], [QuoteHeaderId])
go

CREATE STATISTICS [_dta_stat_1490104349_8_4_16] ON [dbo].[Quote]([IsDeleted], [ProductId], [QuoteHeaderId])
go

CREATE NONCLUSTERED INDEX [_dta_index_LeadActivity_6_434100587__K6_K9_K1] ON [dbo].[LeadActivity]
(
	[CampaignId] ASC,
	[DateUpdated] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

