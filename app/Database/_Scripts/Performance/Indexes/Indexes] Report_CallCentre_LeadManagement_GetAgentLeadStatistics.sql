use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_CampaignLeadBucket_6_635201363__K12_K10_K11_K7_K13_K9] ON [dbo].[CampaignLeadBucket]
(
	[AgentId] ASC,
	[CampaignId] ASC,
	[LeadId] ASC,
	[CreatedOn] ASC,
	[LeadCallCentreCodeId] ASC,
	[IsDeleted] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_CampaignLeadBucket_6_635201363__K7_K13_K9_K10_K12] ON [dbo].[CampaignLeadBucket]
(
	[CreatedOn] ASC,
	[LeadCallCentreCodeId] ASC,
	[IsDeleted] ASC,
	[CampaignId] ASC,
	[AgentId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_CampaignLeadBucket_6_635201363__K9_K10_K12_K7_K13_K11] ON [dbo].[CampaignLeadBucket]
(
	[IsDeleted] ASC,
	[CampaignId] ASC,
	[AgentId] ASC,
	[CreatedOn] ASC,
	[LeadCallCentreCodeId] ASC,
	[LeadId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_CampaignLeadBucket_6_635201363__K13_K10_K12_K7_K9_K11] ON [dbo].[CampaignLeadBucket]
(
	[LeadCallCentreCodeId] ASC,
	[CampaignId] ASC,
	[AgentId] ASC,
	[CreatedOn] ASC,
	[IsDeleted] ASC,
	[LeadId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_635201363_7_10] ON [dbo].[CampaignLeadBucket]([CreatedOn], [CampaignId])
go

CREATE STATISTICS [_dta_stat_635201363_7_13_12] ON [dbo].[CampaignLeadBucket]([CreatedOn], [LeadCallCentreCodeId], [AgentId])
go

CREATE STATISTICS [_dta_stat_635201363_7_9_10] ON [dbo].[CampaignLeadBucket]([CreatedOn], [IsDeleted], [CampaignId])
go

CREATE STATISTICS [_dta_stat_635201363_12_7_9] ON [dbo].[CampaignLeadBucket]([AgentId], [CreatedOn], [IsDeleted])
go

CREATE STATISTICS [_dta_stat_635201363_12_7_13_9] ON [dbo].[CampaignLeadBucket]([AgentId], [CreatedOn], [LeadCallCentreCodeId], [IsDeleted])
go

CREATE STATISTICS [_dta_stat_635201363_7_13_10_12_11] ON [dbo].[CampaignLeadBucket]([CreatedOn], [LeadCallCentreCodeId], [CampaignId], [AgentId], [LeadId])
go

CREATE STATISTICS [_dta_stat_635201363_12_10_11_7_9] ON [dbo].[CampaignLeadBucket]([AgentId], [CampaignId], [LeadId], [CreatedOn], [IsDeleted])
go

CREATE STATISTICS [_dta_stat_635201363_12_10_7_9_13_11] ON [dbo].[CampaignLeadBucket]([AgentId], [CampaignId], [CreatedOn], [IsDeleted], [LeadCallCentreCodeId], [LeadId])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Campaign_6_1250103494__K1_K2] ON [dbo].[Campaign]
(
	[Id] ASC,
	[Name] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

