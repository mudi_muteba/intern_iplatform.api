use [ibroker]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_ProductAdditionalExcess_6_89819432__K13_3] ON [dbo].[ProductAdditionalExcess]
(
	[CoverDefinitionId] ASC
)
INCLUDE ( 	[Category]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1102626971_11_5] ON [dbo].[ProposalDefinition]([IsDeleted], [CoverId])
go

CREATE STATISTICS [_dta_stat_1102626971_2_11] ON [dbo].[ProposalDefinition]([ProposalHeaderId], [IsDeleted])
go

