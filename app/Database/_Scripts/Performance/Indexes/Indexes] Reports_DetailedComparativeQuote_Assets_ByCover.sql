use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K2_K15_K1_K3_K4] ON [dbo].[QuoteItem]
(
	[QuoteId] ASC,
	[IsDeleted] ASC,
	[Id] ASC,
	[CoverDefinitionId] ASC,
	[SumInsured] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K15_K2_K1_K3_K7_K4] ON [dbo].[QuoteItem]
(
	[IsDeleted] ASC,
	[QuoteId] ASC,
	[Id] ASC,
	[CoverDefinitionId] ASC,
	[AssetNumber] ASC,
	[SumInsured] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1570104634_1_4] ON [dbo].[QuoteItem]([Id], [SumInsured])
go

CREATE STATISTICS [_dta_stat_1570104634_4_15_2] ON [dbo].[QuoteItem]([SumInsured], [IsDeleted], [QuoteId])
go

CREATE STATISTICS [_dta_stat_1570104634_3_15_1_2_4] ON [dbo].[QuoteItem]([CoverDefinitionId], [IsDeleted], [Id], [QuoteId], [SumInsured])
go

CREATE STATISTICS [_dta_stat_1570104634_15_1_2_3_7_4] ON [dbo].[QuoteItem]([IsDeleted], [Id], [QuoteId], [CoverDefinitionId], [AssetNumber], [SumInsured])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Asset_6_1198627313__K4_K6_K1_2] ON [dbo].[Asset]
(
	[IsDeleted] ASC,
	[AssetNo] ASC,
	[Id] ASC
)
INCLUDE ( 	[Description]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_Asset_6_1198627313__K4_K1_2] ON [dbo].[Asset]
(
	[IsDeleted] ASC,
	[Id] ASC
)
INCLUDE ( 	[Description]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1198627313_1_4] ON [dbo].[Asset]([Id], [IsDeleted])
go

CREATE STATISTICS [_dta_stat_1198627313_1_6_4] ON [dbo].[Asset]([Id], [AssetNo], [IsDeleted])
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteHeader_6_1495676376__K4_K14] ON [dbo].[QuoteHeader]
(
	[ProposalHeaderId] ASC,
	[IsRerated] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalDefinition_6_1102626971__K5_K2_K11_K1_K13] ON [dbo].[ProposalDefinition]
(
	[CoverId] ASC,
	[ProposalHeaderId] ASC,
	[IsDeleted] ASC,
	[Id] ASC,
	[AssetId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalDefinition_6_1102626971__K2_K5_K11_1] ON [dbo].[ProposalDefinition]
(
	[ProposalHeaderId] ASC,
	[CoverId] ASC,
	[IsDeleted] ASC
)
INCLUDE ( 	[Id]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1102626971_11_1_5] ON [dbo].[ProposalDefinition]([IsDeleted], [Id], [CoverId])
go

CREATE STATISTICS [_dta_stat_1102626971_13_11_1_5] ON [dbo].[ProposalDefinition]([AssetId], [IsDeleted], [Id], [CoverId])
go

CREATE STATISTICS [_dta_stat_1102626971_1_13_5_2] ON [dbo].[ProposalDefinition]([Id], [AssetId], [CoverId], [ProposalHeaderId])
go

CREATE STATISTICS [_dta_stat_1102626971_2_5_1_11_13] ON [dbo].[ProposalDefinition]([ProposalHeaderId], [CoverId], [Id], [IsDeleted], [AssetId])
go

