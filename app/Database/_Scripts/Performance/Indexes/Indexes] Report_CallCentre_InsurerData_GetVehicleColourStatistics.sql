use [ibroker]
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalQuestionAnswer_6_1262627541__K6_K2_K7] ON [dbo].[ProposalQuestionAnswer]
(
	[QuestionDefinitionId] ASC,
	[ProposalDefinitionId] ASC,
	[Answer] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_Quote_6_1490104349__K16_K8_K4] ON [dbo].[Quote]
(
	[QuoteHeaderId] ASC,
	[IsDeleted] ASC,
	[ProductId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuestionDefinition_6_1733581214__K5] ON [dbo].[QuestionDefinition]
(
	[QuestionId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalDefinition_6_1102626971__K1_K5_K2] ON [dbo].[ProposalDefinition]
(
	[Id] ASC,
	[CoverId] ASC,
	[ProposalHeaderId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

