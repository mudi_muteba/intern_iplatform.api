use [ibroker]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K2_K1_K7_K3_5_6_9] ON [dbo].[QuoteItem]
(
	[QuoteId] ASC,
	[Id] ASC,
	[AssetNumber] ASC,
	[CoverDefinitionId] ASC
)
INCLUDE ( 	[Premium],
	[Sasria],
	[ExcessBasic]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K2_K1_K3_K7_5_6_9] ON [dbo].[QuoteItem]
(
	[QuoteId] ASC,
	[Id] ASC,
	[CoverDefinitionId] ASC,
	[AssetNumber] ASC
)
INCLUDE ( 	[Premium],
	[Sasria],
	[ExcessBasic]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_QuoteItem_6_1570104634__K2_K3_5] ON [dbo].[QuoteItem]
(
	[QuoteId] ASC,
	[CoverDefinitionId] ASC
)
INCLUDE ( 	[Premium]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1570104634_7_2] ON [dbo].[QuoteItem]([AssetNumber], [QuoteId])
go

CREATE STATISTICS [_dta_stat_1570104634_7_1_2] ON [dbo].[QuoteItem]([AssetNumber], [Id], [QuoteId])
go

CREATE STATISTICS [_dta_stat_1570104634_6_2_3] ON [dbo].[QuoteItem]([Sasria], [QuoteId], [CoverDefinitionId])
go

CREATE STATISTICS [_dta_stat_1570104634_5_2_3] ON [dbo].[QuoteItem]([Premium], [QuoteId], [CoverDefinitionId])
go

CREATE STATISTICS [_dta_stat_1570104634_1_3_7_2] ON [dbo].[QuoteItem]([Id], [CoverDefinitionId], [AssetNumber], [QuoteId])
go

CREATE NONCLUSTERED INDEX [_dta_index_ProposalDefinition_6_1102626971__K2_K13_K1] ON [dbo].[ProposalDefinition]
(
	[ProposalHeaderId] ASC,
	[AssetId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1102626971_1_13] ON [dbo].[ProposalDefinition]([Id], [AssetId])
go

CREATE NONCLUSTERED INDEX [_dta_index_Asset_6_1198627313__K6_K1] ON [dbo].[Asset]
(
	[AssetNo] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

