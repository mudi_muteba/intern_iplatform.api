use ibroker
go

declare
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@Code varchar(50),
	@CoverId int,
	@CoverDefinitionId int

select
	ProductAdditionalExcess.Category,
	'Not Used' NotUsed
from ProposalHeader
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
	inner join CoverDefinition on CoverDefinition.CoverId = md.Cover.Id
	inner join ProductAdditionalExcess on ProductAdditionalExcess.CoverDefinitionId = CoverDefinition.Id
where
	ProposalHeader.Id = @ProposalHeaderId and
	ProposalDefinition.IsDeleted = 0 and
	CoverDefinition.Id = @CoverDefinitionId
group by
	ProductAdditionalExcess.Category