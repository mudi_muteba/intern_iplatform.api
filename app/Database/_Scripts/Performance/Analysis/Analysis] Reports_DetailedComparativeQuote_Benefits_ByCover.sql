use ibroker
go

declare
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@Code varchar(50),
	@CoverId int

	declare
		@Columns as nvarchar(max),
		@DefaultColumns as varchar(max),
		@Query as nvarchar(max),
		@AssetId int

	create table #Breakdown
	(
		Col_1 varchar(255),
		Col_2 varchar(255),
		Col_3 varchar(255),
		Col_4 varchar(255)
	)

	select @DefaultColumns = ''''' [Description], '''' [col_0], '''' [col_1], '''' [col_2]'

			declare summary_Cursor cursor fast_forward for
				select distinct
					Asset.Id
				from ProposalHeader
					inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
					inner join Asset on Asset.Id = ProposalDefinition.AssetId
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
				where
					ProposalHeader.IsDeleted = 0 and
					ProposalDefinition.IsDeleted = 0 and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0 and
					Asset.IsDeleted = 0 and
					ProposalHeader.Id = @ProposalHeaderId and
					ProposalDefinition.CoverId = @CoverId and
					CoverDefinition.CoverId = @CoverId and
					Quote.Id in
					(
						select * from fn_StringListToTable(@QuoteIds)
					)
				group by
					Asset.Id,
					ProposalHeader.Id

			open summary_Cursor
			fetch next from summary_Cursor into @AssetId

			while @@fetch_status = 0
				begin
					set @Columns =
						stuff
						(
							(
								select distinct
									',' + quotename(Organization.Code)
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = @ProposalHeaderId and
									ProposalDefinition.CoverId = @CoverId and
									CoverDefinition.CoverId = @CoverId and
									Quote.Id in
									(
										select * from fn_StringListToTable(@QuoteIds)
									)
							for xml path(''), type)
						.value('.', 'nvarchar(max)' ), 1, 1, '')

					if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 0
						begin
							select @Columns = @Columns + ', [col_1], [col_2]'
						end

					if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 1
						begin
							select @Columns = @Columns + ', [col_1]'
						end

					set @Query =
						'
							insert into #Breakdown (Col_1, Col_2, Col_3, Col_4)
							select
								' + @DefaultColumns + '

							union all 

							select
								Description,
								' + @Columns + '
							from
							(
								select
									Upper(Asset.Description) Description,
									Organization.Code Insurer
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
								where
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
									ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									Quote.Id in
									(
										select * from fn_StringListToTable(''' + @QuoteIds + ''')
									)
							) x
							pivot 
							(
								max(Insurer)
								for Insurer in (' + @Columns + ')
							) p

							union all

							select
								''Premium'' Description,
								' + @Columns + '
							from
							(
								select
									Organization.Code Insurer,
									cast(QuoteItem.Premium as varchar) Premium
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
								where
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
									ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									Quote.Id in
									(
										select * from fn_StringListToTable(''' + @QuoteIds + ''')
									)
							) x
							pivot 
							(
								max(Premium)
								for Insurer in (' + @Columns + ')
							) p

							union all

							select
								''Basic Excess'' Description,
								' + @Columns + '
							from
							(
								select
									Organization.Code Insurer,
									cast(QuoteItem.ExcessBasic as varchar) ExcessBasic
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
								where
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
									ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									Quote.Id in
									(
										select * from fn_StringListToTable(''' + @QuoteIds + ''')
									)
							) x
							pivot 
							(
								max(ExcessBasic)
								for Insurer in (' + @Columns + ')
							) p

							union all

							select
								''SASRIA'' Description,
								' + @Columns + '
							from
							(
								select
									Organization.Code Insurer,
									cast(QuoteItem.Sasria as varchar) SASRIA
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
								where	
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
									ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									Quote.Id in
									(
										select * from fn_StringListToTable(''' + @QuoteIds + ''')
									)
							) x
							pivot 
							(
								max(SASRIA)
								for Insurer in (' + @Columns + ')
							) p

							union all

							select
								''Total Payment'' Description,
								' + @Columns + '
							from
							(
								select
									Organization.Code Insurer,
									cast((QuoteItem.Premium + QuoteItem.Sasria) as varchar) TotalPayment
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
								where
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
									ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									Quote.Id in
									(
										select * from fn_StringListToTable(''' + @QuoteIds + ''')
									)
							) x
							pivot 
							(
								max(TotalPayment)
								for Insurer in (' + @Columns + ')
							) p

							union all

							select
								Description,
								' + @Columns + '
							from
							(
								select
									Organization.Code Insurer,
									ProductBenefit.Name Description,
									ProductBenefit.Value BenefitValue
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
								where
									ProductBenefit.Name is not null and
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
									ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									Quote.Id in
									(
										select * from fn_StringListToTable(''' + @QuoteIds + ''')
									)
							) x
							pivot 
							(
								max(BenefitValue)
								for Insurer in (' + @Columns + ')
							) p
					'

					execute(@query);

					fetch next from summary_Cursor into @AssetId
				end

			close summary_Cursor
			deallocate summary_Cursor

			select * from #Breakdown
			drop table #Breakdown




			declare summary_Cursor cursor fast_forward for
				select distinct
					Asset.Id
				from ProposalHeader
					inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
					inner join Asset on Asset.Id = ProposalDefinition.AssetId
					--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
				where
					ProposalHeader.IsDeleted = 0 and
					ProposalDefinition.IsDeleted = 0 and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0 and
					Asset.IsDeleted = 0 and
					ProposalHeader.Id = @ProposalHeaderId and
					ProposalDefinition.CoverId = @CoverId and
					CoverDefinition.CoverId = @CoverId and
					Quote.Id in
					(
						select * from fn_StringListToTable(@QuoteIds)
					)
				group by
					Asset.Id

			open summary_Cursor
			fetch next from summary_Cursor into @AssetId

			while @@fetch_status = 0
				begin
					set @Columns =
						stuff
						(
							(
								select distinct
									',' + quotename(Organization.Code)
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId
									--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									Asset.Id = @AssetId and
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = @ProposalHeaderId and
									ProposalDefinition.CoverId = @CoverId and
									CoverDefinition.CoverId = @CoverId and
									Quote.Id in
									(
										select * from fn_StringListToTable(@QuoteIds)
									)
							for xml path(''), type)
						.value('.', 'nvarchar(max)' ), 1, 1, '')

					if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 0
						begin
							select @Columns = @Columns + ', [col_1], [col_2]'
						end

					if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 1
						begin
							select @Columns = @Columns + ', [col_1]'
						end

					set @Query =
						'
							insert into #Breakdown (Col_1, Col_2, Col_3, Col_4)
							select
								' + @DefaultColumns + '

							union all 

							select
								Description,
								' + @Columns + '
							from
							(
								select
									Upper(Asset.Description) Description,
									Organization.Code Insurer
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId
									--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
								where
									Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
									ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									Quote.Id in
									(
										select * from fn_StringListToTable(''' + @QuoteIds + ''')
									)
							) x
							pivot 
							(
								max(Insurer)
								for Insurer in (' + @Columns + ')
							) p

							union all

							select
								''Premium'' Description,
								' + @Columns + '
							from
							(
								select
									Organization.Code Insurer,
									cast(QuoteItem.Premium as varchar) Premium
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId
									--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
								where
									Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
									ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									Quote.Id in
									(
										select * from fn_StringListToTable(''' + @QuoteIds + ''')
									)
							) x
							pivot 
							(
								max(Premium)
								for Insurer in (' + @Columns + ')
							) p

							union all

							select
								''Basic Excess'' Description,
								' + @Columns + '
							from
							(
								select
									Organization.Code Insurer,
									cast(QuoteItem.ExcessBasic as varchar) ExcessBasic
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId
									--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
								where
									Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
									ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									Quote.Id in
									(
										select * from fn_StringListToTable(''' + @QuoteIds + ''')
									)
							) x
							pivot 
							(
								max(ExcessBasic)
								for Insurer in (' + @Columns + ')
							) p

							union all

							select
								''SASRIA'' Description,
								' + @Columns + '
							from
							(
								select
									Organization.Code Insurer,
									cast(QuoteItem.Sasria as varchar) SASRIA
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId
									--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
								where
									Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
									ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									Quote.Id in
									(
										select * from fn_StringListToTable(''' + @QuoteIds + ''')
									)
							) x
							pivot 
							(
								max(SASRIA)
								for Insurer in (' + @Columns + ')
							) p

							union all

							select
								''Total Payment'' Description,
								' + @Columns + '
							from
							(
								select
									Organization.Code Insurer,
									cast((QuoteItem.Premium + QuoteItem.Sasria) as varchar) TotalPayment
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId
									--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
								where
									Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
									ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									Quote.Id in
									(
										select * from fn_StringListToTable(''' + @QuoteIds + ''')
									)
							) x
							pivot 
							(
								max(TotalPayment)
								for Insurer in (' + @Columns + ')
							) p

							union all

							select
								Description,
								' + @Columns + '
							from
							(
								select
									Organization.Code Insurer,
									ProductBenefit.Name Description,
									ProductBenefit.Value BenefitValue
								from ProposalHeader
									inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
									inner join Asset on Asset.Id = ProposalDefinition.AssetId
									--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join Product on Product.Id = Quote.ProductId
									inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
								where
									Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
									ProductBenefit.Name is not null and
									ProposalHeader.IsDeleted = 0 and
									ProposalDefinition.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0 and
									Asset.IsDeleted = 0 and
									ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
									ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
									Quote.Id in
									(
										select * from fn_StringListToTable(''' + @QuoteIds + ''')
									)
							) x
							pivot 
							(
								max(BenefitValue)
								for Insurer in (' + @Columns + ')
							) p
					'

					execute(@query);

					fetch next from summary_Cursor into @AssetId
				end

			close summary_Cursor
			deallocate summary_Cursor

			select * from #Breakdown
			drop table #Breakdown