use ibroker
go

declare
	@CoverId int,
	@ProductIds varchar(max),
	@ChannelIds varchar(max),
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime

select
	@DateFrom = '2017-05-01',
	@DateTo = '2017-05-31',
	@ChannelIds = '5',
	@InsurerIds = '5'

declare
	@AgeRangeTotal numeric(18, 2),
	@AgeRange18To20 numeric(18, 2),
	@AgeRange21To25 numeric(18, 2),
	@AgeRange26To30 numeric(18, 2),
	@AgeRange31To35 numeric(18, 2),
	@AgeRange36To40 numeric(18, 2),
	@AgeRange41To45 numeric(18, 2),
	@AgeRange46To50 numeric(18, 2),
	@AgeRange51To55 numeric(18, 2),
	@AgeRange56Plus numeric(18, 2)

select
	@AgeRangeTotal = 
	(
		select
			count(LeadActivity.Id)
		from Lead
			inner join Individual on Individual.PartyId = Lead.PartyId
			inner join md.Gender on md.Gender.Id = Individual.GenderId
			inner join LeadActivity on LeadActivity.LeadId = Lead.Id
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join Product on Product.Id = Quote.ProductId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			LeadActivity.DateUpdated >= @DateFrom and
			LeadActivity.DateUpdated <= @DateTo
	),
	@AgeRange18To20 =
	(
		select
			count(LeadActivity.Id)
		from Lead
			inner join Individual on Individual.PartyId = Lead.PartyId
			inner join md.Gender on md.Gender.Id = Individual.GenderId
			inner join LeadActivity on LeadActivity.LeadId = Lead.Id
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join Product on Product.Id = Quote.ProductId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			LeadActivity.DateUpdated >= @DateFrom and
			LeadActivity.DateUpdated <= @DateTo and
			datediff(year, Individual.DateOfBirth, getdate()) between 18 and 20
	),
	@AgeRange21To25 =
	(
		select
			count(LeadActivity.Id)
		from Lead
			inner join Individual on Individual.PartyId = Lead.PartyId
			inner join md.Gender on md.Gender.Id = Individual.GenderId
			inner join LeadActivity on LeadActivity.LeadId = Lead.Id
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join Product on Product.Id = Quote.ProductId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			LeadActivity.DateUpdated >= @DateFrom and
			LeadActivity.DateUpdated <= @DateTo and
			datediff(year, Individual.DateOfBirth, getdate()) between 21 and 25
	),
	@AgeRange26To30 =
	(
		select
			count(LeadActivity.Id)
		from Lead
			inner join Individual on Individual.PartyId = Lead.PartyId
			inner join md.Gender on md.Gender.Id = Individual.GenderId
			inner join LeadActivity on LeadActivity.LeadId = Lead.Id
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join Product on Product.Id = Quote.ProductId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			LeadActivity.DateUpdated >= @DateFrom and
			LeadActivity.DateUpdated <= @DateTo and
			datediff(year, Individual.DateOfBirth, getdate()) between 26 and 30
	),
	@AgeRange31To35 =
	(
		select
			count(LeadActivity.Id)
		from Lead
			inner join Individual on Individual.PartyId = Lead.PartyId
			inner join md.Gender on md.Gender.Id = Individual.GenderId
			inner join LeadActivity on LeadActivity.LeadId = Lead.Id
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join Product on Product.Id = Quote.ProductId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			LeadActivity.DateUpdated >= @DateFrom and
			LeadActivity.DateUpdated <= @DateTo and
			datediff(year, Individual.DateOfBirth, getdate()) between 31 and 35
	),
	@AgeRange36To40 =
	(
		select
			count(LeadActivity.Id)
		from Lead
			inner join Individual on Individual.PartyId = Lead.PartyId
			inner join md.Gender on md.Gender.Id = Individual.GenderId
			inner join LeadActivity on LeadActivity.LeadId = Lead.Id
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join Product on Product.Id = Quote.ProductId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			LeadActivity.DateUpdated >= @DateFrom and
			LeadActivity.DateUpdated <= @DateTo and
			datediff(year, Individual.DateOfBirth, getdate()) between 36 and 40
	),
	@AgeRange41To45 =
	(
		select
			count(LeadActivity.Id)
		from Lead
			inner join Individual on Individual.PartyId = Lead.PartyId
			inner join md.Gender on md.Gender.Id = Individual.GenderId
			inner join LeadActivity on LeadActivity.LeadId = Lead.Id
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join Product on Product.Id = Quote.ProductId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			LeadActivity.DateUpdated >= @DateFrom and
			LeadActivity.DateUpdated <= @DateTo and
			datediff(year, Individual.DateOfBirth, getdate()) between 41 and 45
	),
	@AgeRange46To50 =
	(
		select
			count(LeadActivity.Id)
		from Lead
			inner join Individual on Individual.PartyId = Lead.PartyId
			inner join md.Gender on md.Gender.Id = Individual.GenderId
			inner join LeadActivity on LeadActivity.LeadId = Lead.Id
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join Product on Product.Id = Quote.ProductId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			LeadActivity.DateUpdated >= @DateFrom and
			LeadActivity.DateUpdated <= @DateTo and
			datediff(year, Individual.DateOfBirth, getdate()) between 46 and 50
	),
	@AgeRange51To55 =
	(
		select
			count(LeadActivity.Id)
		from Lead
			inner join Individual on Individual.PartyId = Lead.PartyId
			inner join md.Gender on md.Gender.Id = Individual.GenderId
			inner join LeadActivity on LeadActivity.LeadId = Lead.Id
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join Product on Product.Id = Quote.ProductId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			LeadActivity.DateUpdated >= @DateFrom and
			LeadActivity.DateUpdated <= @DateTo and
			datediff(year, Individual.DateOfBirth, getdate()) between 51 and 55
	),
	@AgeRange56Plus =
	(
		select
			count(LeadActivity.Id)
		from Lead
			inner join Individual on Individual.PartyId = Lead.PartyId
			inner join md.Gender on md.Gender.Id = Individual.GenderId
			inner join LeadActivity on LeadActivity.LeadId = Lead.Id
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join Product on Product.Id = Quote.ProductId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			LeadActivity.DateUpdated >= @DateFrom and
			LeadActivity.DateUpdated <= @DateTo and
			datediff(year, Individual.DateOfBirth, getdate()) > 55
	)

if @AgeRangeTotal = 0
	begin
		select @AgeRangeTotal = 1
	end
									
select '18-20' Label, '18-20' Category, @AgeRange18To20 Total, (@AgeRange18To20 / @AgeRangeTotal * 100) [Percentage] union all
select '21-25' Label, '21-25' Category, @AgeRange21To25 Total, (@AgeRange21To25 / @AgeRangeTotal * 100) [Percentage] union all
select '26-30' Label, '26-30' Category, @AgeRange26To30 Total, (@AgeRange26To30 / @AgeRangeTotal * 100) [Percentage] union all
select '31-35' Label, '31-35' Category, @AgeRange31To35 Total, (@AgeRange31To35 / @AgeRangeTotal * 100) [Percentage] union all
select '36-40' Label, '36-40' Category, @AgeRange36To40 Total, (@AgeRange36To40 / @AgeRangeTotal * 100) [Percentage] union all
select '41-45' Label, '41-45' Category, @AgeRange41To45 Total, (@AgeRange41To45 / @AgeRangeTotal * 100) [Percentage] union all
select '46-50' Label, '46-50' Category, @AgeRange46To50 Total, (@AgeRange46To50 / @AgeRangeTotal * 100) [Percentage] union all
select '51-55' Label, '51-55' Category, @AgeRange51To55 Total, (@AgeRange51To55 / @AgeRangeTotal * 100) [Percentage] union all
select '56+' Label, '56+' Category, @AgeRange56Plus Total, (@AgeRange56Plus / @AgeRangeTotal * 100) [Percentage]