use ibroker
go

declare
	@CoverId int,
	@ProductIds varchar(max),
	@ChannelIds varchar(max),
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime,
	@Total varchar

select
	@DateFrom = '2017-05-01',
	@DateTo = '2017-05-31',
	@ChannelIds = '5',
	@InsurerIds = '5'

select distinct
	Channel.Name [Channel],
	md.AuthorisationGroup.Name [Role],
	dbo.ToCamelCase(ltrim(rtrim(Individual.FirstName + ' ' + Individual.Surname))) [Agent],
	[User].UserName [EMail],
	case when (select count(AuditLog.Id) from AuditLog where AuditLog.EntityId = [User].Id and AuditLog.AuditEntryType = 'UserAuthenticationSuccess' and AuditLog.RequestDate >= @DateFrom and AuditLog.RequestDate <= @DateTo) > 0
		then 'Yes' else 'No'
	End [Active]
from [User]
	inner join UserIndividual on UserIndividual.UserId = [User].Id
	inner join Individual on Individual.PartyId = UserIndividual.IndividualId
	inner join UserChannel on UserChannel.UserId = [User].Id
	inner join Channel on Channel.Id = UserChannel.ChannelId
	inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
	inner join md.AuthorisationGroup on md.AuthorisationGroup.Id = UserAuthorisationGroup.AuthorisationGroupId
where
	Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
	[User].IsDeleted = 0 and
	UserIndividual.IsDeleted = 0 and
	UserChannel.IsDeleted = 0 and
	Channel.IsDeleted = 0 and
	UserAuthorisationGroup.IsDeleted = 0
order by
	Channel.Name,
	md.AuthorisationGroup.Name