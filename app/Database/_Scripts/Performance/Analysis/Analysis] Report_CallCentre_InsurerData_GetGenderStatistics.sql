use ibroker
go

declare
	@CoverId int,
	@ProductIds varchar(max),
	@ChannelIds varchar(max),
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime,
	@Total varchar

select
	@DateFrom = '2017-05-01',
	@DateTo = '2017-05-31',
	@ChannelIds = '5',
	@InsurerIds = '5'


select
	@Total = isnull(count(md.Gender.Id), 0)
from Lead
	inner join Individual on Individual.PartyId = Lead.PartyId
	inner join md.Gender on md.Gender.Id = Individual.GenderId
	inner join LeadActivity on LeadActivity.LeadId = Lead.Id
	inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
	inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	inner join Product on Product.Id = Quote.ProductId
	inner join Organization on Organization.PartyId = Product.ProductOwnerId
where
	LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
	Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	LeadActivity.DateUpdated >= @DateFrom and
	LeadActivity.DateUpdated <= @DateTo

select
	md.Gender.Name Label,
	md.Gender.Name Category,
	cast(count(md.Gender.id) as numeric(18, 2)) Total,
	isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
from Lead
	inner join Individual on Individual.PartyId = Lead.PartyId
	inner join md.Gender on md.Gender.Id = Individual.GenderId
	inner join LeadActivity on LeadActivity.LeadId = Lead.Id
	inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
	inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	inner join Product on Product.Id = Quote.ProductId
	inner join Organization on Organization.PartyId = Product.ProductOwnerId
where
	LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
	Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	LeadActivity.DateUpdated >= @DateFrom and
	LeadActivity.DateUpdated <= @DateTo
group by
	md.Gender.Id,
	md.Gender.Name