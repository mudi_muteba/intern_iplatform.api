use ibroker
go

declare
	@ChannelIds varchar(max),
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime

select
	@DateFrom = '2017-05-01',
	@DateTo = '2017-05-31',
	@ChannelIds = '5',
	@InsurerIds = '5'

select distinct
	Organization.TradingName [Insurer],
	Product.Name [Product],
	(
		select
			count(Quote.Id)
		from QuoteUploadLog
			inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
			inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
			inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
			inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
		where
			(
				select
					LeadActivity.CampaignId
				from LeadActivity
					inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				where
					QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			) in (select * from fn_StringListToTable(@CampaignIds)) and
			Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
			Insurer.PartyId = Organization.PartyId and
			InsurerProduct.Id = Product.Id and
			QuoteUploadLog.DateCreated >= @DateFrom and
			QuoteUploadLog.DateCreated <= @DateTo and
			QuoteUploadLog.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0
	) [Uploads],
	(
		select
			avg(QuoteUploadLog.Premium)
		from QuoteUploadLog
			inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
			inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
			inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
			inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
		where
			(
				select
					LeadActivity.CampaignId
				from LeadActivity
					inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				where
					QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			) in (select * from fn_StringListToTable(@CampaignIds)) and
			Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
			Insurer.PartyId = Organization.PartyId and
			InsurerProduct.Id = Product.Id and
			QuoteUploadLog.DateCreated >= @DateFrom and
			QuoteUploadLog.DateCreated <= @DateTo and
			QuoteUploadLog.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0
	) [AveragePremiumValue],
	(
		select
			sum(QuoteUploadLog.Premium)
		from QuoteUploadLog
			inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
			inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
			inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
			inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
		where
			(
				select
					LeadActivity.CampaignId
				from LeadActivity
					inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				where
					QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			) in (select * from fn_StringListToTable(@CampaignIds)) and
			Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
			Insurer.PartyId = Organization.PartyId and
			InsurerProduct.Id = Product.Id and
			QuoteUploadLog.DateCreated >= @DateFrom and
			QuoteUploadLog.DateCreated <= @DateTo and
			QuoteUploadLog.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0
	) [TotalPremiumValue]
from QuoteUploadLog
	inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
	inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
	inner join Product on Product.Id = Quote.ProductId
	inner join Organization on Organization.PartyId = Product.ProductProviderId
where
	(
		select
			LeadActivity.CampaignId
		from LeadActivity
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
		where
			QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
	) in (select * from fn_StringListToTable(@CampaignIds)) and
	Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
	QuoteUploadLog.DateCreated >= @DateFrom and
	QuoteUploadLog.DateCreated <= @DateTo and
	QuoteUploadLog.IsDeleted = 0 and
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0