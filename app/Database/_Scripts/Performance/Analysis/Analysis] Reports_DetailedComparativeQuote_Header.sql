use ibroker
go

declare
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@Code varchar(50)

select distinct top 1
	Client.DisplayName Client,
	(
		select top 1
			case when [Address].IsComplex = 1
				then dbo.ToCamelCase(ltrim(rtrim([Address].Complex)) + ' ' + ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' + ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim([Address].Code)))
				else dbo.ToCamelCase(ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' +  ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim( [Address].Code)))
			end
		from [Address]
		where
			[Address].PartyId = Client.Id and
			AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address') and
			[Address].IsDeleted = 0
	) PhysicalAddress,
	(
		select top 1
			case when [Address].IsComplex = 1
				then dbo.ToCamelCase(ltrim(rtrim([Address].Complex)) + ' ' + ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' + ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim([Address].Code)))
				else dbo.ToCamelCase(ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' +  ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim( [Address].Code)))
			end
		from [Address]
		where
			[Address].PartyId = Client.Id and
			AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address') and
			[Address].IsDeleted = 0
	) PostalAddress,

	ClientContactDetail.Work WorkNo,
	ClientContactDetail.Home HomeNo,
	ClientContactDetail.Fax FaxNo,
	ClientContactDetail.Cell CellNo,
	ClientContactDetail.Email EmailAddress,

	case
		when len(ltrim(rtrim(ClientContact.IdentityNo))) = 0 then ClientContact.PassportNo
		else ClientContact.IdentityNo
	end IDNumber,

	Quote.QuoteHeaderId,
	Quote.InsurerReference QuoteNo,
	Product.QuoteExpiration,
	Quote.Fees,
	(
		select
			Sum(QuoteItem.Sasria)
		from QuoteItem
		where
			QuoteItem.IsDeleted = 0 and
			QuoteItem.QuoteId = Quote.Id and
			QuoteItem.Sasria is not null and
			QuoteItem.Sasria > 0
	) SASRIA,
	(
		select
			Sum(QuoteItem.Premium) + Sum(QuoteItem.Sasria) + Quote.Fees
		from QuoteItem
		where
			QuoteItem.IsDeleted = 0 and
			QuoteItem.QuoteId = Quote.Id and
			QuoteItem.Premium is not null and
			QuoteItem.Premium > 0
	) Total,
	(
		select top 1
			QuoteEMailBody
		from Organization
		where
			Code = @Code
	) QuoteEMailBody,
	(
		select top 1
			dbo.ToCamelCase(Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code)
		from [Address]
		where
			[Address].PartyId = Client.Id and
			[Address].IsDefault = 1 and
			AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address')
	) DefaultPhysical,
	(
		select top 1
			dbo.ToCamelCase(Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code)
		from [Address]
		where
			[Address].PartyId = Client.Id and
			[Address].IsDefault = 1 and
			AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address')
	) DefaultPostal,
	(
		select top 1
			dbo.ToCamelCase(Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code)
		from [Address]
		where
			[Address].PartyId = Client.Id and
			[Address].IsDefault = 0 and
			AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address')
	) NoDefaultPhysical,
	(
		select top 1
			dbo.ToCamelCase(Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code)
		from [Address]
		where
			[Address].PartyId = Client.Id and
			[Address].IsDefault = 0 and
			AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address')
	) NoDefaultPostal,
	Individual.FirstName,
	Individual.Surname
from ProposalHeader
	inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	inner join Product on Product.Id = Quote.ProductId
	inner join LeadActivity on LeadActivity.Id = ProposalHeader.LeadActivityId
	inner join Party Client on Client.Id = LeadActivity.PartyId
	inner join Individual on Individual.PartyId = Client.Id
	--inner join Party [Owner] on [Owner].Id = Product.ProductOwnerId
	--inner join Party [Provider] on [Provider].Id = Product.ProductProviderId
	--inner join Organization OwnerOrg on OwnerOrg.PartyId = [Owner].Id
	--inner join Organization ProviderOrg on ProviderOrg.PartyId = [Provider].Id
	left join [Address] ClientAddress on ClientAddress.PartyId = Client.Id
	left join Contact ClientContact on ClientContact.PartyId = Client.Id
	left join ContactDetail ClientContactDetail on ClientContactDetail.Id = Client.ContactDetailId
where
	ProposalHeader.Id = @ProposalHeaderId and
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	Product.IsDeleted = 0 and
	LeadActivity.IsDeleted = 0 and
	Client.IsDeleted = 0 and
	ClientAddress.IsDeleted = 0 and
	ClientContactDetail.IsDeleted = 0 and
	Quote.Id in (select * from fn_StringListToTable(@QuoteIds))