use ibroker
go

declare
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@Code varchar(50),
	@CoverId int,
	@CoverDefinitionId int

select distinct
	Organization.TradingName Insurer,
	Product.Name Product,
	Product.QuoteExpiration QuoteValidFor,
	'<ol>' +
	stuff
	(
		(
			select distinct
				'<li>' + pd.[Description] + '</li>'
			from ProposalHeader ph
				inner join QuoteHeader qh on qh.ProposalHeaderId = ph.Id
				inner join Quote q on q.QuoteHeaderId = qh.Id
				inner join Product p  on p.Id = q.ProductId
				inner join ProductDescription pd on pd.ProductId = p.Id
			where
				p.Id = Product.Id
			--order by
			--	pd.[Index] asc
			for xml path(''), type
		).value('.', 'NVARCHAR(MAX)'), 1, 0, ''
	)
	+ '</ol>' Addendum
from ProposalHeader
	inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
	inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
	inner join Product on Product.Id = Quote.ProductId
	inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
	inner join Organization on Organization.PartyId = Product.ProductOwnerId
where
	ProposalHeader.IsDeleted = 0 and
	ProposalDefinition.IsDeleted = 0 and
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	QuoteItem.IsDeleted = 0 and
	Asset.IsDeleted = 0 and
	ProposalHeader.Id = @ProposalHeaderId and
	Quote.Id in (select * from fn_StringListToTable(@QuoteIds))
order by
	Organization.TradingName