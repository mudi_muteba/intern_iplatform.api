use ibroker
go

declare
	@ChannelIds varchar(max),
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime

select
	@DateFrom = '2017-05-01',
	@DateTo = '2017-05-31',
	@ChannelIds = '5',
	@InsurerIds = '5'

select
	Organization.TradingName [Insurer],
	Product.Name [Product],
	(
		select top 1
			Individual.Surname + ', ' + Individual.FirstName
		from LeadActivity
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			inner join [User] on [User].Id = LeadActivity.UserId
			inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
			inner join UserIndividual on UserIndividual.UserId = [User].Id
			inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		where
			QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		order by
			LeadActivity.DateUpdated desc
	) Agent,
	(
		select top 1
			Individual.Surname + ', ' + Individual.FirstName
		from LeadActivity
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
			inner join Lead on Lead.Id = LeadActivity.LeadId
			inner join Individual on Individual.PartyId = Lead.PartyId
		where
			QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
		order by
			LeadActivity.DateUpdated desc
	) Client,
	QuoteUploadLog.InsurerReference [InsurerReference],
	QuoteUploadLog.DateCreated [UploadDate],
	QuoteUploadLog.Premium [PremiumValue]
from QuoteUploadLog
	inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
	inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
	inner join Product on Product.Id = Quote.ProductId
	inner join Organization on Organization.PartyId = Product.ProductProviderId
where
	(
		select
			LeadActivity.CampaignId
		from LeadActivity
			inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
		where
			QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
	) in (select * from fn_StringListToTable(@CampaignIds)) and
	Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
	QuoteUploadLog.DateCreated >= @DateFrom and
	QuoteUploadLog.DateCreated <= @DateTo and
	QuoteUploadLog.IsDeleted = 0 and
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0
order by
	Organization.TradingName,
	Product.Name,
	QuoteUploadLog.DateCreated