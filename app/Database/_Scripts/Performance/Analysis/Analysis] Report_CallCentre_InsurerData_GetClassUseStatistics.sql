use ibroker
go

declare
	@CoverId int,
	@ProductIds varchar(max),
	@ChannelIds varchar(max),
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime,
	@Total varchar

select
	@DateFrom = '2017-05-01',
	@DateTo = '2017-05-31',
	@ChannelIds = '5',
	@InsurerIds = '5'


select
	@Total = count(md.QuestionAnswer.Answer)
from Lead
	inner join Individual on Individual.PartyId = Lead.PartyId
	inner join md.Gender on md.Gender.Id = Individual.GenderId
	inner join LeadActivity on LeadActivity.LeadId = Lead.Id
	inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
	inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	inner join Product on Product.Id = Quote.ProductId
	inner join Organization on Organization.PartyId = Product.ProductOwnerId
	inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
	inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
	inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
	inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
	inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
where
	LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
	Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
	ProposalDefinition.CoverId = 218 and --Motor
	md.Question.Id = 48 and --Class Of Use
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	LeadActivity.DateUpdated >= @DateFrom and
	LeadActivity.DateUpdated <= @DateTo and
	isnumeric(ProposalQuestionAnswer.Answer) = 1 and
	len(ProposalQuestionAnswer.Answer) <= 10

select
	md.QuestionAnswer.Answer Label,
	md.QuestionAnswer.Answer Category,
	cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
	isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
from Lead
	inner join Individual on Individual.PartyId = Lead.PartyId
	inner join md.Gender on md.Gender.Id = Individual.GenderId
	inner join LeadActivity on LeadActivity.LeadId = Lead.Id
	inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
	inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	inner join Product on Product.Id = Quote.ProductId
	inner join Organization on Organization.PartyId = Product.ProductOwnerId
	inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
	inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
	inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
	inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
	inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
where
	LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
	Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
	ProposalDefinition.CoverId = 218 and --Motor
	md.Question.Id = 48 and --Class Of Use
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	LeadActivity.DateUpdated >= @DateFrom and
	LeadActivity.DateUpdated <= @DateTo and
	isnumeric(ProposalQuestionAnswer.Answer) = 1 and
	len(ProposalQuestionAnswer.Answer) <= 10
group by
	md.QuestionAnswer.Answer