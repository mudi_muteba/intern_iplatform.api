use ibroker
go

declare
	@CoverId int,
	@ProductIds varchar(max),
	@ChannelIds varchar(max),
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime,
	@Total varchar

select
	@DateFrom = '2017-05-01',
	@DateTo = '2017-05-31',
	@ChannelIds = '5',
	@InsurerIds = '5'

select
	@Total = count(md.LossHistoryQuestionAnswer.Id)
from Lead
	inner join Individual on Individual.PartyId = Lead.PartyId
	inner join md.Gender on md.Gender.Id = Individual.GenderId
	inner join LeadActivity on LeadActivity.LeadId = Lead.Id
	inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
	inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	inner join Product on Product.Id = Quote.ProductId
	inner join Organization on Organization.PartyId = Product.ProductOwnerId
	inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join LossHistory on LossHistory.PartyId = Individual.PartyId
	inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
where
	LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
	Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	LeadActivity.DateUpdated >= @DateFrom and
	LeadActivity.DateUpdated <= @DateTo

select
	case
		when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
		when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
		when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
		when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
		else md.LossHistoryQuestionAnswer.Answer
	end Label,
	case
		when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
		when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
		when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
		when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
		else md.LossHistoryQuestionAnswer.Answer
	end Category,
	cast(count(md.LossHistoryQuestionAnswer.Id) as numeric(18, 2)) Total,
	isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
from Lead
	inner join Individual on Individual.PartyId = Lead.PartyId
	inner join md.Gender on md.Gender.Id = Individual.GenderId
	inner join LeadActivity on LeadActivity.LeadId = Lead.Id
	inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
	inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	inner join Product on Product.Id = Quote.ProductId
	inner join Organization on Organization.PartyId = Product.ProductOwnerId
	inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join LossHistory on LossHistory.PartyId = Individual.PartyId
	inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
where
	LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
	Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	LeadActivity.DateUpdated >= @DateFrom and
	LeadActivity.DateUpdated <= @DateTo
group by
	md.LossHistoryQuestionAnswer.Answer