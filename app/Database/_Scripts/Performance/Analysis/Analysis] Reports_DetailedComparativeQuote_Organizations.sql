use ibroker
go

declare
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@Code varchar(50),
	@CoverId int

;with Covers_CTE (OrganizationId, TradingName, Code, CoverDefinitionIds)
	as 
	(
		select distinct
			Organization.PartyId,
			Organization.TradingName,
			Organization.Code,
			(
				stuff
				(
					(
						select
							',' + cast(CoverDefinition.Id as nvarchar)
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization o on o.PartyId = Product.ProductOwnerId
						where
							ProposalHeader.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							CoverDefinition.IsDeleted = 0 and
							ProposalHeader.Id = @ProposalHeaderId and
							Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
							o.PartyId = Organization.PartyId and
							md.Cover.Id = @CoverId
						for xml path(''), type
					).value('.', 'NVARCHAR(MAX)'), 1, 1, '')
			)
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			ProposalHeader.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			CoverDefinition.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
			md.Cover.Id = @CoverId and
			CoverDefinition.Id in (select CoverDefinitionId from ProductAdditionalExcess where CoverDefinitionId = CoverDefinition.Id)
		)
	select
		OrganizationId,
		TradingName,
		Code,
		CoverDefinitionIds
	from
		Covers_CTE
	order by
		TradingName asc