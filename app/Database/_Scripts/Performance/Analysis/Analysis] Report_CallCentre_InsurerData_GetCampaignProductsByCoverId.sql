use ibroker
go

declare
	@CoverId int,
	@ChannelIds varchar(max),
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime

select
	@DateFrom = '2017-05-01',
	@DateTo = '2017-05-31',
	@ChannelIds = '5',
	@InsurerIds = '5'

select distinct
	ProductOwner.PartyId ProductOwnerId,
	ProductOwner.TradingName ProductOwnerName,
	ProductProvider.PartyId ProductProviderId,
	ProductProvider.TradingName ProductProviderName,
	Product.Id ProductId,
	Product.Name ProductName
from Product
	inner join CoverDefinition on CoverDefinition.ProductId = Product.Id
	inner join Quote on Quote.ProductId = Product.Id
	inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
	inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
	inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
	inner join Organization ProductOwner on ProductOwner.PartyId = Product.ProductOwnerId
	inner join Organization ProductProvider on ProductProvider.PartyId = Product.ProductOwnerId
where
	LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
	LeadActivity.DateUpdated >= @DateFrom and
	LeadActivity.DateUpdated <= @DateTo and
	CoverDefinition.CoverId = @CoverId and
	CoverDefinition.IsDeleted = 0 and
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	Product.IsDeleted = 0
group by
	ProductOwner.PartyId,
	ProductOwner.TradingName,
	ProductProvider.PartyId,
	ProductProvider.TradingName,
	Product.Id,
	Product.Name