use ibroker
go

declare
	@CoverId int,
	@ProductIds varchar(max),
	@ChannelIds varchar(max),
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime,
	@Total varchar

select
	@DateFrom = '2017-05-01',
	@DateTo = '2017-05-31',
	@ChannelIds = '5',
	@InsurerIds = '5'

declare
	@AverageNoClaimBonus numeric(18, 2),
	@AverageSumInsured numeric(18, 2)

select
	@AverageNoClaimBonus = isnull(avg(cast(md.QuestionAnswer.Answer as numeric(18, 2))), 0)
from Lead
	inner join Individual on Individual.PartyId = Lead.PartyId
	inner join md.Gender on md.Gender.Id = Individual.GenderId
	inner join LeadActivity on LeadActivity.LeadId = Lead.Id
	inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
	inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	inner join Product on Product.Id = Quote.ProductId
	inner join Organization on Organization.PartyId = Product.ProductOwnerId
	inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
	inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
	inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
	inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
	inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
where
	LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
	Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
	ProposalDefinition.CoverId = 218 and --Motor
	md.Question.Id = 43 and --No Claim Bonus
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	LeadActivity.DateUpdated >= @DateFrom and
	LeadActivity.DateUpdated <= @DateTo and
	isnumeric(ProposalQuestionAnswer.Answer) = 1 and
	len(ProposalQuestionAnswer.Answer) <= 10 and
	isnumeric(md.QuestionAnswer.Answer) = 1 and
	len(md.QuestionAnswer.Answer) <= 10

select
	@AverageSumInsured = isnull(avg(cast(ProposalQuestionAnswer.Answer as numeric(18, 2))), 0)
from Lead
	inner join Individual on Individual.PartyId = Lead.PartyId
	inner join md.Gender on md.Gender.Id = Individual.GenderId
	inner join LeadActivity on LeadActivity.LeadId = Lead.Id
	inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
	inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	inner join Product on Product.Id = Quote.ProductId
	inner join Organization on Organization.PartyId = Product.ProductOwnerId
	inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
	inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
	inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
	inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
where
	LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
	Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
	ProposalDefinition.CoverId = 218 and --Motor
	md.Question.Id = 102 and --Sum Insured
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	LeadActivity.DateUpdated >= @DateFrom and
	LeadActivity.DateUpdated <= @DateTo and
	isnumeric(ProposalQuestionAnswer.Answer) = 1 and
	len(ProposalQuestionAnswer.Answer) <= 10

select
	@AverageNoClaimBonus AverageNoClaimBonus,
	@AverageSumInsured AverageSumInsured

union all

select
	@AverageNoClaimBonus AverageNoClaimBonus,
	@AverageSumInsured AverageSumInsured