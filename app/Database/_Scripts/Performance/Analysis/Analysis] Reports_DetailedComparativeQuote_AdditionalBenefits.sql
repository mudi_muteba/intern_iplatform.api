use ibroker
go

declare
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@QuestionIds varchar(50),
	@Code varchar(50),
	@CoverId int,
	@CoverDefinitionId int

select
	md.Question.Name,
	md.QuestionAnswer.Answer
from ProposalHeader
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
	inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
	inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
	inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
where
	md.Question.Id in (select * from fn_StringListToTable(@QuestionIds)) and
	ProposalHeader.Id = @ProposalHeaderId and
	isnumeric(ProposalQuestionAnswer.Answer) = 1 and
	md.QuestionAnswer.Answer <> '0' and
	md.QuestionAnswer.Answer <> ''

union all

select
	md.Question.Name,
	case
		when ProposalQuestionAnswer.Answer = 'True' then 'Yes'
		else ProposalQuestionAnswer.Answer
	end Answer
from ProposalHeader
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
	inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
	inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
	inner join md.QuestionGroup on md.QuestionGroup.Id = md.Question.QuestionGroupId
where
	md.Question.Id not in (select * from fn_StringListToTable(@QuestionIds)) and
	ProposalHeader.Id = @ProposalHeaderId and
	md.QuestionGroup.Name = 'Additional Options' and
	ProposalQuestionAnswer.Answer <> 'False' and
	ProposalQuestionAnswer.Answer <> '0' and
	ProposalQuestionAnswer.Answer <> ''
order by
	md.Question.name asc