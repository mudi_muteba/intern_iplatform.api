use ibroker
go

declare
	@CoverId int,
	@ProductIds varchar(max),
	@ChannelIds varchar(max),
	@CampaignIds varchar(max),
	@InsurerIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime,
	@Total varchar

select
	@DateFrom = '2017-05-01',
	@DateTo = '2017-05-31',
	@ChannelIds = '5',
	@InsurerIds = '5'

select
	@Total = isnull(count([Address].Id), 0)
from Lead
	inner join Individual on Individual.PartyId = Lead.PartyId
	inner join [Address] on [Address].PartyId = Individual.PartyId
	inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
	inner join LeadActivity on LeadActivity.LeadId = Lead.Id
	inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
	inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	inner join Product on Product.Id = Quote.ProductId
	inner join Organization on Organization.PartyId = Product.ProductOwnerId
where
	LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
	Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
	[Address].AddressTypeId = 1 and --Physical Address
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	LeadActivity.DateUpdated >= @DateFrom and
	LeadActivity.DateUpdated <= @DateTo

select
	md.StateProvince.Name Label,
	md.StateProvince.Name Category,
	cast(count(md.StateProvince.id) as numeric(18, 2)) Total,
	isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
from Lead
	inner join Individual on Individual.PartyId = Lead.PartyId
	inner join [Address] on [Address].PartyId = Individual.PartyId
	inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
	inner join LeadActivity on LeadActivity.LeadId = Lead.Id
	inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
	inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	inner join Product on Product.Id = Quote.ProductId
	inner join Organization on Organization.PartyId = Product.ProductOwnerId
where
	LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
	Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
	[Address].AddressTypeId = 1 and --Physical Address
	QuoteHeader.IsRerated = 0 and
	Quote.IsDeleted = 0 and
	LeadActivity.DateUpdated >= @DateFrom and
	LeadActivity.DateUpdated <= @DateTo
group by
	md.StateProvince.Id,
	md.StateProvince.Name