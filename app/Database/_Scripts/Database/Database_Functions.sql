USE [iBroker]
GO
/****** Object:  UserDefinedFunction [dbo].[ToCamelCase]    Script Date: 2016/05/18 8:19:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[ToCamelCase] ( @InputString nvarchar(max) ) 
RETURNS VARCHAR(4000)
AS
BEGIN

DECLARE @Index          INT
DECLARE @Char           CHAR(1)
DECLARE @PrevChar       CHAR(1)
DECLARE @OutputString   VARCHAR(255)

SET @OutputString = LOWER(@InputString)
SET @Index = 1

WHILE @Index <= LEN(@InputString)
BEGIN
    SET @Char     = SUBSTRING(@InputString, @Index, 1)
    SET @PrevChar = CASE WHEN @Index = 1 THEN ' '
                         ELSE SUBSTRING(@InputString, @Index - 1, 1)
                    END

    IF @PrevChar IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&', '''', '(')
    BEGIN
        IF @PrevChar != '''' OR UPPER(@Char) != 'S'
            SET @OutputString = STUFF(@OutputString, @Index, 1, UPPER(@Char))
    END

    SET @Index = @Index + 1
END

RETURN @OutputString

END
go


USE [iBroker]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_StringListToTable]    Script Date: 2016/05/18 8:22:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER function [dbo].[fn_StringListToTable]
(
    @list varchar(max)
)
returns 
	@parsedlist table
	(
		item int
	)
as
	begin
		declare @item varchar(800), @pos int
		set @list = ltrim(rtrim(@list))+ ','
		set @pos = charindex(',', @list, 1)
		while @pos > 0
		begin
			set @item = ltrim(rtrim(left(@list, @pos - 1)))
			if @item <> ''
			begin
				insert into @parsedlist (item) 
				values (cast(@item as int))
			end
			set @list = right(@list, len(@list) - @pos)
			set @pos = charindex(',', @list, 1)
		end
		return
	end
go