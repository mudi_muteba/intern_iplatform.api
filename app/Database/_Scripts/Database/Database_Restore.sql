use [master]
go

alter database ibroker set single_user with rollback immediate
go

drop database ibroker
go

USE [master]
--RESTORE DATABASE [iBroker] FROM  DISK = N'F:\Installs\Program Files\Microsoft SQL Server 2012\MSSQL11.MSSQLSERVER\MSSQL\Backup\iBroker_20151117.bak' WITH  FILE = 1,  NOUNLOAD,  REPLACE,  STATS = 5
RESTORE DATABASE [iBroker] FROM  DISK = N'C:\bk\staging_iplatform_20160517.bak' WITH  FILE = 1,  NOUNLOAD,  REPLACE,  STATS = 5

GO

use ibroker
go		

sp_change_users_login 'Update_One', 'iBroker_User', 'iBroker_User'