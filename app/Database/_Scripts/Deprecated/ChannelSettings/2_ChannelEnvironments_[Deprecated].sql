--COMMENT NEXT TWO LINES WHEN USING SCRIPT AS MIGRATION
use ibroker
go

--DISABLE QUERY COUNT FOR OUTPUT READABILITY & SPEED
set nocount on
go

--TEMPORARILY DISABLE FOREIGN KEY CONTRAINTS ON CHANNELSETTING TABLE
alter table ChannelSetting nocheck constraint all
go

--CLEAR CHANNELENVIRONMENT TABLE
delete from ChannelEnvironment
go

--RESEED IDENTITY COLUMN OF CHANNELENVIRONMENT TABLE
dbcc checkident ('ChannelEnvironment', reseed, 0)
go

--ENABLE MAX LENGTH IS SET ON DESCTION, NAME & VALUE FIELDS OF ChannelEnvironments TABLE
alter table ChannelEnvironment
	alter column [Description] varchar(max)
go

--CREATE CURSOR TO LOOP THROUGH AVAILABLE CHANNELS
declare
	@ChannelCursor cursor,
	@CursorChannelId int,
	@CursorChannelCode varchar(50),
	@CursorChannelName varchar(50),
	@CursorChannelDetail varchar(255)

set @ChannelCursor = cursor for
	select Id, Code, Name from Channel

open @ChannelCursor

fetch next from @ChannelCursor into @CursorChannelId, @CursorChannelCode, @CursorChannelName
	while @@fetch_status = 0
		begin
			select
				@CursorChannelDetail = @CursorChannelName + ' (' + @CursorChannelCode + ')'

			print @CursorChannelDetail + ' environment setup in progress.'

			--DEV ENVIRONMENT CHANNEL DEFAULT
			insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'DEV', 'Development', 'Development environment of' + @CursorChannelCode, 1, 0)

			--OTHER CHANNEL ENVIRONMENTS
			if(upper(@CursorChannelCode) = 'AIG')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Staging', 'Staging', 'Staging environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'SIT', 'System Integration Testing', 'System Integration Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'UAT', 'User Acceptance Testing', 'User Acceptance Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Pre-Production', 'Pre-Production', 'Pre-Production environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
				end
			else if(upper(@CursorChannelCode) = 'AON')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'UAT', 'User Acceptance Testing', 'User Acceptance Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
				end
			else if(upper(@CursorChannelCode) = 'AutoPedigree')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'UAT', 'User Acceptance Testing', 'User Acceptance Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
				end
			else if(upper(@CursorChannelCode) = 'BIDVEST')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'UAT', 'User Acceptance Testing', 'User Acceptance Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
				end
			else if(upper(@CursorChannelCode) = 'BOWER')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
				end
			else if(upper(@CursorChannelCode) = 'HOL')
				begin
					print '   > No Channel Environment Found In Configs For ' + @CursorChannelDetail
				end
			else if(upper(@CursorChannelCode) = 'INDWE')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'UAT', 'User Acceptance Testing', 'User Acceptance Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
				end
			else if(upper(@CursorChannelCode) = 'IP')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'TeamCity', 'TeamCity', 'TeamCity environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'iPlatformDevTest', 'iPlatform Development Testing', 'iPlatform Development Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Staging', 'Staging', 'Staging environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'UAT', 'User Acceptance Testing', 'User Acceptance Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Demo', 'Demo', 'Demo environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Oaksure', 'Oaksure', 'Oaksure environment of ' + @CursorChannelDetail, 0, 0)
				end
			else if(upper(@CursorChannelCode) = 'KP')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'UAT', 'User Acceptance Testing', 'User Acceptance Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
				end
			else if(upper(@CursorChannelCode) = 'MIWAY')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'UAT', 'User Acceptance Testing', 'User Acceptance Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
				end
			else if(upper(@CursorChannelCode) = 'PSG')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'UAT', 'User Acceptance Testing', 'User Acceptance Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
				end
			else if(upper(@CursorChannelCode) = 'PSGCC')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'UAT', 'User Acceptance Testing', 'User Acceptance Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
				end
			else if(upper(@CursorChannelCode) = 'TELESURE')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'UAT', 'User Acceptance Testing', 'User Acceptance Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
				end
			else if(upper(@CursorChannelCode) = 'UAG')
				begin
					print '   > No Channel Environment Found In Configs For ' + @CursorChannelDetail
				end
			else if(upper(@CursorChannelCode) = 'UAP')
				begin
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Staging', 'Staging', 'Staging environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Staging-Internal', 'Internal Staging', 'Internal Staging environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'UAT', 'User Acceptance Testing', 'User Acceptance Testing environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Production', 'Production', 'Production environment of ' + @CursorChannelDetail, 0, 0)
					insert into ChannelEnvironment (ChannelId, [Key], Name, [Description], IsDefault, IsDeleted) values (@CursorChannelId, 'Live', 'Live', 'Live environment of ' + @CursorChannelDetail, 0, 0)
				end

			print @CursorChannelDetail + ' environment setup complete.'
			print ''

			fetch next from @ChannelCursor into @CursorChannelId, @CursorChannelCode, @CursorChannelName
		end

close @ChannelCursor
deallocate @ChannelCursor

print 'Done...'
go

--RE-ENABLE FOREIGN KEY CONTRAINTS ON CHANNELSETTING TABLE
alter table ChannelSetting with check check constraint all
go

--RE-ENABLE QUERY COUNT
set nocount off
go