--COMMENT NEXT TWO LINES WHEN USING SCRIPT AS MIGRATION
use ibroker
go

--DISABLE QUERY COUNT FOR OUTPUT READABILITY & SPEED
set nocount on
go

print 'Creating or updating channels in progress...'
print''

if not exists(select * from Channel where Code = 'Default')
	begin
		print 'Channel "Default" not found, creating.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (0, newid(), 197, 1, 2, null, 'Default', 'DEFAULT', 'dd MMM yyyy', 0, getdate(), null, 1, 1, 0)
		
		print 'Channel "Default" created.'
	end
else
	begin
		print 'Channel "Default" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'Default'

		print 'Channel "Default" updated.'
	end
go


if not exists(select * from Channel where Code = 'AIG')
	begin
		print 'Channel "AIG" not found, creating.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (1, newid(), 197, 1, 2, null, 'AIG', 'AIG', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "AIG" created.'
	end
else
	begin
		print 'Channel "AIG" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'AIG'

		print 'Channel "AIG" updated.'
	end
go


if not exists(select * from Channel where Code = 'PSG')
	begin
		print 'Channel "PSG" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (2, newid(), 197, 1, 2, null, 'PSG', 'PSG', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "PSG" created.'
	end
else
	begin
		print 'Channel "PSG" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'PSG'

		print 'Channel "PSG" updated.'
	end
go


if not exists(select * from Channel where Code = 'UAP')
	begin
		print 'Channel "UAP" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (3, newid(), 197, 1, 2, null, 'UAP', 'UAP', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "UAP" created.'
	end
else
	begin
		print 'Channel "UAP" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'UAP'

		print 'Channel "UAP" updated.'
	end
go


if not exists(select * from Channel where Code = 'HOL')
	begin
		print 'Channel "HOL" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (4, newid(), 197, 1, 2, null, 'HOL', 'HOL', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "HOL" created.'
	end
else
	begin
		print 'Channel "HOL" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'HOL'

		print 'Channel "HOL" updated.'
	end
go


if not exists(select * from Channel where Code = 'KP')
	begin
		print 'Channel "KP" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (5, newid(), 197, 1, 2, null, 'KingPrice', 'KP', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "KP" created.'
	end
else
	begin
		print 'Channel "KP" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'KP'

		print 'Channel "KP" updated.'
	end
go


if not exists(select * from Channel where Code = 'UAG')
	begin
		print 'Channel "UAG" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (6, newid(), 197, 1, 2, null, 'Telesure', 'UAG', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "UAG" created.'
	end
else
	begin
		print 'Channel "UAG" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'UAG'

		print 'Channel "UAG" updated.'
	end
go


if not exists(select * from Channel where Code = 'IP')
	begin
		print 'Channel "IP" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (7, newid(), 197, 1, 2, null, 'iPlatform', 'IP', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "IP" created.'
	end
else
	begin
		print 'Channel "IP" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'IP'

		print 'Channel "IP" updated.'
	end
go


if not exists(select * from Channel where Code = 'INDWE')
	begin
		print 'Channel "INDWE" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (8, newid(), 197, 1, 2, null, 'Indwe', 'INDWE', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "INDWE" created.'
	end
else
	begin
		print 'Channel "INDWE" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'INDWE'

		print 'Channel "INDWE" updated.'
	end
go


if not exists(select * from Channel where Code = 'BIDVEST')
	begin
		print 'Channel "BIDVEST" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (9, newid(), 197, 1, 2, null, 'Bidvest', 'BIDVEST', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "BIDVEST" created.'
	end
else
	begin
		print 'Channel "BIDVEST" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'BIDVEST'

		print 'Channel "BIDVEST" updated.'
	end
go


if not exists(select * from Channel where Code = 'PSGCC')
	begin
		print 'Channel "PSGCC" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (10, newid(), 197, 1, 2, null, 'PSG Client Care', 'PSGCC', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "PSGCC" created.'
	end
else
	begin
		print 'Channel "PSGCC" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'PSGCC'

		print 'Channel "PSGCC" updated.'
	end
go


if not exists(select * from Channel where Code = 'TELESURE')
	begin
		print 'Channel "TELESURE" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (11, newid(), 197, 1, 2, null, 'Telesure', 'TELESURE', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "TELESURE" created.'
	end
else
	begin
		print 'Channel "TELESURE" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'TELESURE'

		print 'Channel "TELESURE" updated.'
	end
go


if not exists(select * from Channel where Code = 'MIWAY')
	begin
		print 'Channel "MIWAY" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (12, newid(), 197, 1, 2, null, 'MiWay', 'MIWAY', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "MIWAY" created.'
	end
else
	begin
		print 'Channel "MIWAY" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'MIWAY'

		print 'Channel "MIWAY" updated.'
	end
go


if not exists(select * from Channel where Code = 'BOWER')
	begin
		print 'Channel "BOWER" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (13, newid(), 197, 1, 2, null, 'BOWER', 'BOWER', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "BOWER" created.'
	end
else
	begin
		print 'Channel "BOWER" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'BOWER'

		print 'Channel "BOWER" updated.'
	end
go


if not exists(select * from Channel where Code = 'AutoPedigree')
	begin
		print 'Channel "AutoPedigree" not found, creating channel.'

		insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values (14, newid(), 197, 1, 2, null, 'Auto Pedigree', 'AutoPedigree', 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)
		
		print 'Channel "AutoPedigree" created.'
	end
else
	begin
		print 'Channel "AutoPedigree" found, updating channel.'

		update Channel
		set
			Id = Id,
			SystemId = SystemId,
			CountryId = CountryId,
			CurrencyId = CurrencyId,
			LanguageId = LanguageId,
			OrganizationId = OrganizationId,
			Name = Name,
			Code = Code,
			[DateFormat] = [DateFormat],
			PasswordStrengthEnabled = PasswordStrengthEnabled,
			ActivatedOn = ActivatedOn,
			DeactivatedOn = DeactivatedOn,
			IsDefault = IsDefault,
			IsActive = IsActive,
			IsDeleted = IsDeleted
		where
			Code = 'AutoPedigree'

		print 'Channel "AutoPedigree" updated.'
	end
go


print ''
print 'Done...'
go

--RE-ENABLE QUERY COUNT
set nocount off
go