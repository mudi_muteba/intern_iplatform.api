--COMMENT NEXT TWO LINES WHEN USING SCRIPT AS MIGRATION
use ibroker
go

--DISABLE QUERY COUNT FOR OUTPUT READABILITY & SPEED
set nocount on
go

--CLEAR CHANNELSETTING TABLE
delete from ChannelSetting
go

--RESEED IDENTITY COLUMN OF CHANNELSETTING TABLE
dbcc checkident ('ChannelSetting', reseed, 0)
go

--ENABLE MAX LENGTH IS SET ON DESCTION, NAME & VALUE FIELDS OF CHANNELSETTING TABLE
alter table ChannelSetting
	alter column ChannelSettingDescription varchar(max)
go

alter table ChannelSetting
	alter column ChannelSettingName varchar(max)
go

alter table ChannelSetting
	alter column ChannelSettingValue varchar(max)
go

--CHANNEL SETTING NAMES, MAINTAIN ALPHABETICAL ORDER
declare
	@Name_ApiV1TestingUri varchar(255) = 'ApiV1TestingUri',
	@Name_ApiV1Uri varchar(255) = 'ApiV1Uri',
	@Name_ApiV2TestingUri varchar(255) = 'ApiV2TestingUri',
	@Name_ApiV2Uri varchar(255) = 'ApiV2Uri',
	@Name_AuthenticationSuccessEvent varchar(255) = 'AuthenticationSuccessEvent',
	@Name_BrokerName varchar(255) = 'BrokerName',
	@Name_BrowserFactory varchar(255) = 'BrowserFactory',
	@Name_ClientCode varchar(255)= 'ClientCode',
	@Name_ClientName varchar(255) = 'ClientName',
	@Name_ConsumerViewPassword varchar(255) = 'ConsumerViewPassword',
	@Name_ConsumerViewUserName varchar(255) = 'ConsumerViewUserName',
	@Name_CountryCode varchar(255) = 'CountryCode',
	@Name_CurrencySymbol varchar(255) = 'CurrencySymbol',
	@Name_DatabaseConnectionString varchar(255) = 'DatabaseConnectionString',
	@Name_DefaultMapLatitude varchar(255) = 'DefaultMapLatitude',
	@Name_DefaultMapLongitude varchar(255) = 'DefaultMapLongitude',
	@Name_EchoTCFApiKey varchar(255) = 'EchoTCFApiKey',
	@Name_EchoTCFBypass varchar(255) = 'EchoTCFBypass',
	@Name_EchoTCFToken varchar(255) = 'EchoTCFToken',
	@Name_EchoTCFUri varchar(255) = 'EchoTCFUri',
	@Name_EchoTCFUserName varchar(255) = 'EchoTCFUserName',
	@Name_EmailTemplateSource varchar(255) = 'EmailTemplateSource',
	@Name_EnableAddressEditorAddressType varchar(255) = 'EnableAddressEditorAddressType',
	@Name_EnableAddressEditorMap varchar(255) = 'EnableAddressEditorMap',
	@Name_EnableAgentLeadMosaicInfo varchar(255) = 'EnableAgentLeadMosaicInfo',
	@Name_EnableAgentLeadWealthIndex varchar(255) = 'EnableAgentLeadWealthIndex',
	@Name_EnableAnnualValues varchar(255) = 'EnableAnnualValues',
	@Name_EnableBankAccountValidation varchar(255) = 'EnableBankAccountValidation',
	@Name_EnableBundling varchar(255) = 'EnableBundling',
	@Name_EnableClientConsole varchar(255) = 'EnableClientConsole',
	@Name_EnableDatabaseUpgrade varchar(255) = 'EnableDatabaseUpgrade',
	@Name_EnableDemoMode varchar(255) = 'EnableDemoMode',
	@Name_EnableExternalReferenceValidation varchar(255) = 'EnableExternalReferenceValidation',
	@Name_EnableInsurerTemplate varchar(255) = 'EnableInsurerTemplate',
	@Name_EnableLeadIdentityNumberValidation varchar(255) = 'EnableLeadIdentityNumberValidation',
	@Name_EnableMultipleQuoteAcceptance varchar(255) = 'EnableMultipleQuoteAcceptance',
	@Name_EnablePerformanceCounter varchar(255) = 'EnablePerformanceCounter', 
	@Name_EnablePostalCodeInAddressEditor varchar(255) = 'EnablePostalCodeInAddressEditor',
	@Name_EnableQuoteToPolicyInfo varchar(255) = 'EnableQuoteToPolicyInfo',
	@Name_EnableRate varchar(255) = 'EnableRate',
	@Name_EnableSASRIA varchar(255) = 'EnableSASRIA',
	@Name_EnableSecondLevelUnderwriting varchar(255) = 'EnableSecondLevelUnderwriting',
	@Name_EnableSignFlowOnQuoteEMail varchar(255) = 'EnableSignFlowOnQuoteEMail',
	@Name_EnableUnderwritingRejectionNotice varchar(255) = 'EnableUnderwritingRejectionNotice',
	@Name_EnableVehicleLookup varchar(255) = 'EnableVehicleLookup',
	@Name_Environment varchar(255) = 'Environment',
	@Name_iAdminUri varchar(255) = 'iAdminUri',
	@Name_IDSUri varchar(255) = 'IDSUri',
	@Name_iGuideApiKey varchar(255) = 'iGuideApiKey',
	@Name_iGuideEMail varchar(255) = 'iGuideEMail',
	@Name_iGuideV1Uri varchar(255) = 'iGuideV1Uri',
	@Name_iGuideV2Uri varchar(255) = 'iGuideV2Uri',
	@Name_InsurerCode varchar(255) = 'InsurerCode',
	@Name_iPersonApiKey varchar(255) = 'iPersonApiKey',
	@Name_iPersonDefaultSource varchar(255) = 'iPersonDefaultSource',
	@Name_iPersonEMail varchar(255) = 'iPersonEMail',
	@Name_iPersonEnableRetry varchar(255) = 'iPersonEnableRetry',
	@Name_iPersonUri varchar(255) = 'iPersonUri ',
	@Name_iPlatformEnvironment varchar(50) = 'iPlatformEnvironment',
	@Name_iRateUri varchar(255) = 'iRateUri',
	@Name_KenyaAutoKey varchar(255) = 'KenyaAutoKey',
	@Name_KenyaAutoSecret varchar(255) = 'KenyaAutoSecret',
	@Name_LanguageCode varchar(255) = 'LanguageCode',
	@Name_LightStoneContractId varchar(255) = 'LightStoneContractId',
	@Name_LightStoneCustomerId varchar(255) = 'LightStoneCustomerId',
	@Name_LightStonePackageId varchar(255) = 'LightStonePackageId',
	@Name_LightStonePassword varchar(255) = 'LightStonePassword',
	@Name_LightStonePropertyUserId varchar(255) = 'LightStonePropertyUserId',
	@Name_LightStoneUserId varchar(255) = 'LightStoneUserId',
	@Name_LightStoneUserName varchar(255) = 'LightStoneUserName',
	@Name_LoadBalancerApiV1Uri varchar(255) = 'LoadBalancerApiV1Uri',
	@Name_LoadBalancerApiV2Uri varchar(255) = 'LoadBalancerApiV2Uri',
	@Name_LoadBalancerWebUri varchar(255) = 'LoadBalancerWebUri',
	@Name_Log4NetEnvironment varchar(255) = 'Log4NetEnvironment',
	@Name_MachineSpecificBindingApiV1Uri varchar(255) = 'MachineSpecificBindingApiV1Uri',
	@Name_MachineSpecificBindingApiV2Uri varchar(255) = 'MachineSpecificBindingApiV2Uri',
	@Name_MachineSpecificBindingWebUri varchar(255) = 'MachineSpecificBindingWebUri',
	@Name_MigrationAssemblyApiV1Path varchar(255) = 'MigrationAssemblyApiV1Path',
	@Name_MigrationAssemblyApiV2Path varchar(255) = 'MigrationAssemblyApiV2Path',
	@Name_MigrationAssemblyExecutablePath varchar(255) = 'MigrationAssemblyExecutablePath',
	@Name_MigrationToolExecutablePath varchar(255) = 'MigrationToolExecutablePath',
	@Name_NullResultDefault varchar(255) = 'NullResultDefault',
	@Name_PasswordResetUri varchar(255) = 'PasswordResetUri',
	@Name_QuoteAcceptanceEnvironment varchar(50) = 'QuoteAcceptanceEnvironment',
	@Name_QuoteAcceptanceMessage varchar(255) = 'QuoteAcceptanceMessage',
	@Name_QuoteToPolicyDetailsHeader varchar(255) = 'QuoteToPolicyDetailsHeader',
	@Name_ReportSource varchar(255) = 'ReportSource',
	@Name_ReportStorage varchar(255) = 'ReportStorage',
	@Name_RootPassword varchar(255) = 'RootPassword',
	@Name_RootUserName varchar(255) = 'RootUserName',
	@Name_SalesForceConsumerKey varchar(255) = 'SalesForceConsumerKey',
	@Name_SalesForceConsumerSecret varchar(255) = 'SalesForceConsumerSecret',
	@Name_SalesForceDelay varchar(255) = 'SalesForceDelay',
	@Name_SalesForceLeadSource varchar(255) = 'SalesForceLeadSource',
	@Name_SalesForcePassword varchar(255) = 'SalesForcePassword',
	@Name_SalesForceSecurityKey varchar(255) = 'SalesForceSecurityKey',
	@Name_SalesForceStatus varchar(255) = 'SalesForceStatus',
	@Name_SalesForceUri varchar(255) = 'SalesForceUri',
	@Name_SalesForceUserName varchar(255) = 'SalesForceUserName',
	@Name_SalesForceWebsiteSubmissionPath varchar(255) = 'SalesForceWebsiteSubmissionPath',
	@Name_SecretSecurityKey varchar(255) = 'SecretSecurityKey',
	@Name_SelenoURL varchar(255) = 'SelenoURL',
	@Name_ServiceBusEnvironment varchar(255) = 'ServiceBusEnvironment',
	@Name_SignFlowPassword varchar(255) = 'SignFlowPassword',
	@Name_SignFlowSecretKey varchar(255) = 'SignFlowSecretKey',
	@Name_SignFlowUri varchar(255) = 'SignFlowUri',
	@Name_SignFlowUserName varchar(255) = 'SignFlowUserName',
	@Name_SiteName varchar(255) = 'SiteName',
	@Name_SSLIPAddress varchar(255) = 'SSLIPAddress',
	@Name_SSLThumbPrint varchar(255) = 'SSLThumbPrint',
	@Name_SubscriberServiceName varchar(255) = 'SubscriberServiceName',
	@Name_TransUnionSecurityCode varchar(255) = 'TransUnionSecurityCode',
	@Name_TransUnionSubscriberCode varchar(255) = 'TransUnionSubscriberCode',
	@Name_WarningLabel varchar(255) = 'WarningLabel',
	@Name_WebBranding varchar(255) = 'WebBranding',
	@Name_WebUri varchar(255) = 'WebUri',
	@Name_WidgetFeeDiscount varchar(255) = 'WidgetFeeDiscount',
	@Name_WorkFlowEngineServiceDescription varchar(255) = 'WorkFlowEngineServiceDescription',
	@Name_WorkFlowEngineServiceDisplayName varchar(255) = 'WorkFlowEngineServiceDisplayName',
	@Name_WorkFlowEngineServiceExchangeName varchar(255) = 'WorkFlowEngineServiceExchangeName',
	@Name_WorkFlowEngineServiceExchangeNameError varchar(255) = 'WorkFlowEngineServiceExchangeNameError',
	@Name_WorkFlowEngineServiceName varchar(255) = 'WorkFlowEngineServiceName',
	@Name_WorkFlowEngineServiceQueueNameError varchar(255) = 'WorkFlowEngineServiceQueueNameError',
	@Name_WorkFlowEnvironment varchar(50) = 'WorkFlowEnvironment',
	@Name_WorkFlowRouterServiceDescription varchar(255) = 'WorkFlowRouterServiceDescription',
	@Name_WorkFlowRouterServiceDisplayName varchar(255) = 'WorkFlowRouterServiceDisplayName',
	@Name_WorkFlowRouterServiceExchangeName varchar(255) = 'WorkFlowRouterServiceExchangeName',
	@Name_WorkFlowRouterServiceExchangeNameError varchar(255) = 'WorkFlowRouterServiceExchangeNameError',
	@Name_WorkFlowRouterServiceName varchar(255) = 'WorkFlowRouterServiceName',
	@Name_WorkFlowRouterServiceQueueNameError varchar(255) = 'WorkFlowRouterServiceQueueNameError'

--CHANNEL SETTING DESCRIPTIONS, MAINTAIN ALPHABETICAL ORDER
declare
	@Description_ApiV1TestingUri varchar(255) = '',
	@Description_ApiV1Uri varchar(255) = '',
	@Description_ApiV2TestingUri varchar(255) = '',
	@Description_ApiV2Uri varchar(255) = '',
	@Description_AuthenticationSuccessEvent varchar(255) = '',
	@Description_BrokerName varchar(255) = 'Refactor',
	@Description_BrowserFactory varchar(255) = '',
	@Description_ClientCode varchar(255)= 'Refactor',
	@Description_ClientName varchar(255) = 'Refactor',
	@Description_ConsumerViewPassword varchar(255) = '',
	@Description_ConsumerViewUserName varchar(255) = '',
	@Description_CountryCode varchar(255) = 'The channel''s ISO-639-1 standard country code, see: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes',
	@Description_CurrencySymbol varchar(255) = 'The channel''s currency symbol',
	@Description_DatabaseConnectionString varchar(255) = '',
	@Description_DefaultMapLatitude varchar(255) = 'The channel''s default latitude, the website maps will use this co-ordinate in conjunction with specified longitude',
	@Description_DefaultMapLongitude varchar(255) = 'The channel''s default longitude, the website maps will use this co-ordinate in conjunction with specified latitude',
	@Description_EchoTCFApiKey varchar(255) = '',
	@Description_EchoTCFBypass varchar(255) = '',
	@Description_EchoTCFToken varchar(255) = '',
	@Description_EchoTCFUri varchar(255) = '',
	@Description_EchoTCFUserName varchar(255) = '',
	@Description_EmailTemplateSource varchar(255) = '',
	@Description_EnableAddressEditorAddressType varchar(255) = 'Whether or not to display the address type in the web''s address editor on this channel',
	@Description_EnableAddressEditorMap varchar(255) = 'Whether or not to display the map in the web''s address editor on this channel',
	@Description_EnableAgentLeadMosaicInfo varchar(255) = 'Whether or not to display a lead''s mosaic info to call centre agents on this channel',
	@Description_EnableAgentLeadWealthIndex varchar(255) = 'Whether or not to display a lead''s wealth index to call centre agents on this channel',
	@Description_EnableAnnualValues varchar(255) = 'Whether or not to calculate and display annual values on quotes on this channel',
	@Description_EnableBankAccountValidation varchar(255) = 'Whether or not to validate a lead''s bank account details on this channel',
	@Description_EnableBundling varchar(255) = 'Whether or not to reduce the payload size of website scripts on this channel.',
	@Description_EnableClientConsole varchar(255) = 'Whether or not to output values to the browser''s console window on this channel',
	@Description_EnableDatabaseUpgrade varchar(255) = '',
	@Description_EnableDemoMode varchar(255) = 'Whether or not to demonstration mode is active on this channel',
	@Description_EnableExternalReferenceValidation varchar(255) = 'Whether or not to validate a lead''s external reference number on this channel',
	@Description_EnableInsurerTemplate varchar(255) = '',
	@Description_EnableLeadIdentityNumberValidation varchar(255) = 'Whether or not to validate a lead''s idenitity number on this channel',
	@Description_EnableMultipleQuoteAcceptance varchar(255) = 'Whether or not to allow a client to accept multiple quotes on this channel',
	@Description_EnablePerformanceCounter varchar(255) = '', 
	@Description_EnablePostalCodeInAddressEditor varchar(255) = 'Whether or not to display the postal code field in the web''s address editor on this channel',
	@Description_EnableQuoteToPolicyInfo varchar(255) = 'Whether or not to display the insurer''s reference and other details when a quote becomes a policy on this channel',
	@Description_EnableRate varchar(255) = 'Whether or not to display the rate for the premium in comparison to the insured amount on this channel',
	@Description_EnableSASRIA varchar(255) = 'Whether or not to calculate and display SASRIA rates for quotes on this channel',
	@Description_EnableSecondLevelUnderwriting varchar(255) = 'Whether or not to enable second level underwriting on this channel',
	@Description_EnableSignFlowOnQuoteEMail varchar(255) = 'Whether or not to allow sending of optional digitally signable documents when sending quotes to a client on this channel',
	@Description_EnableUnderwritingRejectionNotice varchar(255) = '',
	@Description_EnableVehicleLookup varchar(255) = 'Whether or not to display the map in the web''s address editor on this channel',
	@Description_Environment varchar(255) = '',
	@Description_iAdminUri varchar(255) = '',
	@Description_IDSUri varchar(255) = '',
	@Description_iGuideApiKey varchar(255) = '',
	@Description_iGuideEMail varchar(255) = '',
	@Description_iGuideV1Uri varchar(255) = '',
	@Description_iGuideV2Uri varchar(255) = '',
	@Description_InsurerCode varchar(255) = '',
	@Description_iPersonApiKey varchar(255) = '',
	@Description_iPersonDefaultSource varchar(255) = '',
	@Description_iPersonEMail varchar(255) = '',
	@Description_iPersonEnableRetry varchar(255) = '',
	@Description_iPersonUri varchar(255) = '',
	@Description_iPlatformEnvironment varchar(50) = '',
	@Description_iRateUri varchar(255) = '',
	@Description_KenyaAutoKey varchar(255) = '',
	@Description_KenyaAutoSecret varchar(255) = '',
	@Description_LanguageCode varchar(255) = 'The channel''s ISO-3166-Alpha-2 standard language code, see: https://en.wikipedia.org/wiki/ISO_3166-1',
	@Description_LightStoneContractId varchar(255) = '',
	@Description_LightStoneCustomerId varchar(255) = '',
	@Description_LightStonePackageId varchar(255) = '',
	@Description_LightStonePassword varchar(255) = '',
	@Description_LightStonePropertyUserId varchar(255) = '',
	@Description_LightStoneUserId varchar(255) = '',
	@Description_LightStoneUserName varchar(255) = '',
	@Description_LoadBalancerApiV1Uri varchar(255) = '',
	@Description_LoadBalancerApiV2Uri varchar(255) = '',
	@Description_LoadBalancerWebUri varchar(255) = '',
	@Description_Log4NetEnvironment varchar(255) = '',
	@Description_MachineSpecificBindingApiV1Uri varchar(255) = '',
	@Description_MachineSpecificBindingApiV2Uri varchar(255) = '',
	@Description_MachineSpecificBindingWebUri varchar(255) = '',
	@Description_MigrationAssemblyApiV1Path varchar(255) = '',
	@Description_MigrationAssemblyApiV2Path varchar(255) = '',
	@Description_MigrationAssemblyExecutablePath varchar(255) = '',
	@Description_MigrationToolExecutablePath varchar(255) = '',
	@Description_NullResultDefault varchar(255) = 'The channel''s default value thar replaces null values on certain inputs for the website',
	@Description_PasswordResetUri varchar(255) = '',
	@Description_QuoteAcceptanceEnvironment varchar(50) = '',
	@Description_QuoteAcceptanceMessage varchar(255) = 'The channel''s message to the client when a quote is accepted',
	@Description_QuoteToPolicyDetailsHeader varchar(255) = 'The channel''s default header for policies',
	@Description_ReportSource varchar(255) = '',
	@Description_ReportStorage varchar(255) = '',
	@Description_RootPassword varchar(255) = '',
	@Description_RootUserName varchar(255) = '',
	@Description_SalesForceConsumerKey varchar(255) = '',
	@Description_SalesForceConsumerSecret varchar(255) = '',
	@Description_SalesForceDelay varchar(255) = '',
	@Description_SalesForceLeadSource varchar(255) = '',
	@Description_SalesForcePassword varchar(255) = '',
	@Description_SalesForceSecurityKey varchar(255) = '',
	@Description_SalesForceStatus varchar(255) = '',
	@Description_SalesForceUri varchar(255) = '',
	@Description_SalesForceUserName varchar(255) = '',
	@Description_SalesForceWebsiteSubmissionPath varchar(255) = '',
	@Description_SecretSecurityKey varchar(255) = '',
	@Description_SelenoURL varchar(255) = '',
	@Description_ServiceBusEnvironment varchar(255) = '',
	@Description_SignFlowPassword varchar(255) = '',
	@Description_SignFlowSecretKey varchar(255) = '',
	@Description_SignFlowUri varchar(255) = '',
	@Description_SignFlowUserName varchar(255) = '',
	@Description_SiteName varchar(255) = '',
	@Description_SSLIPAddress varchar(255) = '',
	@Description_SSLThumbPrint varchar(255) = '',
	@Description_SubscriberServiceName varchar(255) = '',
	@Description_TransUnionSecurityCode varchar(255) = '',
	@Description_TransUnionSubscriberCode varchar(255) = '',
	@Description_WarningLabel varchar(255) = 'The channel''s warning label displayed when quoting returns warnings',
	@Description_WebBranding varchar(255) = 'The channel''s website branding, i.e. theme, skin, look & feel',
	@Description_WebUri varchar(255) = '',
	@Description_WidgetFeeDiscount varchar(255) = '',
	@Description_WorkFlowEngineServiceDescription varchar(255) = '',
	@Description_WorkFlowEngineServiceDisplayName varchar(255) = '',
	@Description_WorkFlowEngineServiceExchangeName varchar(255) = '',
	@Description_WorkFlowEngineServiceExchangeNameError varchar(255) = '',
	@Description_WorkFlowEngineServiceName varchar(255) = '',
	@Description_WorkFlowEngineServiceQueueNameError varchar(255) = '',
	@Description_WorkFlowEnvironment varchar(50) = '',
	@Description_WorkFlowRouterServiceDescription varchar(255) = '',
	@Description_WorkFlowRouterServiceDisplayName varchar(255) = '',
	@Description_WorkFlowRouterServiceExchangeName varchar(255) = '',
	@Description_WorkFlowRouterServiceExchangeNameError varchar(255) = '',
	@Description_WorkFlowRouterServiceName varchar(255) = '',
	@Description_WorkFlowRouterServiceQueueNameError varchar(255) = ''

--CHANNEL SETTING OVERRIDABLE VALUES, MAINTAIN ALPHABETICAL ORDER
declare
	@Value_ApiV1TestingUri varchar(255),
	@Value_ApiV1Uri varchar(255),
	@Value_ApiV2TestingUri varchar(255),
	@Value_ApiV2Uri varchar(255),
	@Value_AuthenticationSuccessEvent int,
	@Value_BrokerName varchar(255),
	@Value_BrowserFactory varchar(255),
	@Value_ClientCode varchar(255),
	@Value_ClientName varchar(255),
	@Value_ConsumerViewPassword varchar(255),
	@Value_ConsumerViewUserName varchar(255),
	@Value_CountryCode varchar(255),
	@Value_CurrencySymbol varchar(255),
	@Value_DatabaseConnectionString varchar(255),
	@Value_DefaultMapLatitude varchar(255),
	@Value_DefaultMapLongitude varchar(255),
	@Value_EchoTCFApiKey varchar(255),
	@Value_EchoTCFBypass bit = 1,
	@Value_EchoTCFToken varchar(255),
	@Value_EchoTCFUri varchar(255),
	@Value_EchoTCFUserName varchar(255),
	@Value_EmailTemplateSource varchar(255),
	@Value_EnableAddressEditorAddressType bit,
	@Value_EnableAddressEditorMap bit,
	@Value_EnableAgentLeadMosaicInfo bit,
	@Value_EnableAgentLeadWealthIndex bit,
	@Value_EnableAnnualValues bit,
	@Value_EnableBankAccountValidation bit,
	@Value_EnableBundling bit,
	@Value_EnableClientConsole bit,
	@Value_EnableDatabaseUpgrade bit,
	@Value_EnableDemoMode bit,
	@Value_EnableExternalReferenceValidation bit,
	@Value_EnableInsurerTemplate bit,
	@Value_EnableLeadIdentityNumberValidation bit,
	@Value_EnableMultipleQuoteAcceptance bit,
	@Value_EnablePerformanceCounter bit,
	@Value_EnablePostalCodeInAddressEditor bit,
	@Value_EnableQuoteToPolicyInfo bit,
	@Value_EnableRate bit,
	@Value_EnableSASRIA bit,
	@Value_EnableSecondLevelUnderwriting bit,
	@Value_EnableSignFlowOnQuoteEMail bit,
	@Value_EnableUnderwritingRejectionNotice bit,
	@Value_EnableVehicleLookup bit,
	@Value_Environment varchar(255),
	@Value_iAdminUri varchar(255),
	@Value_IDSUri varchar(255),
	@Value_iGuideApiKey varchar(255),
	@Value_iGuideEMail varchar(255),
	@Value_iGuideV1Uri varchar(255),
	@Value_iGuideV2Uri varchar(255),
	@Value_InsurerCode varchar(255),
	@Value_iPersonApiKey varchar(255),
	@Value_iPersonDefaultSource varchar(255),
	@Value_iPersonEMail varchar(255),
	@Value_iPersonEnableRetry bit,
	@Value_iPersonUri varchar(255),
	@Value_iPlatformEnvironment varchar(50),
	@Value_iRateUri varchar(255),
	@Value_KenyaAutoKey varchar(255),
	@Value_KenyaAutoSecret varchar(255),
	@Value_LanguageCode varchar(255),
	@Value_LightStoneContractId varchar(255),
	@Value_LightStoneCustomerId varchar(255),
	@Value_LightStonePackageId varchar(255),
	@Value_LightStonePassword varchar(255),
	@Value_LightStonePropertyUserId varchar(255),
	@Value_LightStoneUserId varchar(255),
	@Value_LightStoneUserName varchar(255),
	@Value_LoadBalancerApiV1Uri varchar(255),
	@Value_LoadBalancerApiV2Uri varchar(255),
	@Value_LoadBalancerWebUri varchar(255),
	@Value_Log4NetEnvironment varchar(255),
	@Value_MachineSpecificBindingApiV1Uri varchar(255),
	@Value_MachineSpecificBindingApiV2Uri varchar(255),
	@Value_MachineSpecificBindingWebUri varchar(255),
	@Value_MigrationAssemblyApiV1Path varchar(255),
	@Value_MigrationAssemblyApiV2Path varchar(255),
	@Value_MigrationAssemblyExecutablePath varchar(255),
	@Value_MigrationToolExecutablePath varchar(255),
	@Value_NullResultDefault varchar(255),
	@Value_PasswordResetUri varchar(255),
	@Value_QuoteAcceptanceEnvironment varchar(50),
	@Value_QuoteAcceptanceMessage varchar(255),
	@Value_QuoteToPolicyDetailsHeader varchar(255),
	@Value_ReportSource varchar(255),
	@Value_ReportStorage varchar(255),
	@Value_RootPassword varchar(255),
	@Value_RootUserName varchar(255),
	@Value_SalesForceConsumerKey varchar(255),
	@Value_SalesForceConsumerSecret varchar(255),
	@Value_SalesForceDelay int,
	@Value_SalesForceLeadSource varchar(255),
	@Value_SalesForcePassword varchar(255),
	@Value_SalesForceSecurityKey varchar(255),
	@Value_SalesForceStatus varchar(255),
	@Value_SalesForceUri varchar(255),
	@Value_SalesForceUserName varchar(255),
	@Value_SalesForceWebsiteSubmissionPath varchar(255),
	@Value_SecretSecurityKey varchar(255),
	@Value_SelenoURL varchar(255),
	@Value_ServiceBusEnvironment varchar(255),
	@Value_SignFlowPassword varchar(255),
	@Value_SignFlowSecretKey varchar(255),
	@Value_SignFlowUri varchar(255),
	@Value_SignFlowUserName varchar(255),
	@Value_SiteName varchar(255),
	@Value_SSLIPAddress varchar(255),
	@Value_SSLThumbPrint varchar(255),
	@Value_SubscriberServiceName varchar(255),
	@Value_TransUnionSecurityCode varchar(255),
	@Value_TransUnionSubscriberCode varchar(255),
	@Value_WarningLabel varchar(255),
	@Value_WebBranding varchar(255),
	@Value_WebUri varchar(255),
	@Value_WidgetFeeDiscount numeric(18, 2),
	@Value_WorkFlowEngineServiceDescription varchar(255),
	@Value_WorkFlowEngineServiceDisplayName varchar(255),
	@Value_WorkFlowEngineServiceExchangeName varchar(255),
	@Value_WorkFlowEngineServiceExchangeNameError varchar(255),
	@Value_WorkFlowEngineServiceName varchar(255),
	@Value_WorkFlowEngineServiceQueueNameError varchar(255),
	@Value_WorkFlowEnvironment varchar(50),
	@Value_WorkFlowRouterServiceDescription varchar(255),
	@Value_WorkFlowRouterServiceDisplayName varchar(255),
	@Value_WorkFlowRouterServiceExchangeName varchar(255),
	@Value_WorkFlowRouterServiceExchangeNameError varchar(255),
	@Value_WorkFlowRouterServiceName varchar(255),
	@Value_WorkFlowRouterServiceQueueNameError varchar(255)

--CHANNEL SETTING DEFAULT VALUES, MAINTAIN ALPHABETICAL ORDER
declare
	@Default_Value_ApiV1TestingUri varchar(255) = 'http://teamcity-api.iplatform.co.za/', --REFACTOR REFERENCES, SETTING RENAMED FROM "test/iBrokerApiUrl"
	@Default_Value_ApiV1Uri varchar(255) = 'http://dev.ibroker.api/', --REFACTOR REFERENCES, SETTING RENAMED FROM "iplatform/connector/baseUrl"
	@Default_Value_ApiV2TestingUri varchar(255) = 'http://teamcity-apiv2.iplatform.co.za/', --REFACTOR REFERENCES, SETTING RENAMED FROM "test/iPlatformApiUrl"
	@Default_Value_ApiV2Uri varchar(255) = 'http://dev.iplatform.api/', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/serviceLocator/iPlatform/baseUrl"
	@Default_Value_AuthenticationSuccessEvent int = 5, --POSSIBLE REMOVAL? NO REFERENCES FOUND!
	@Default_Value_BrokerName varchar(255) = 'iPlatform',
	@Default_Value_BrowserFactory varchar(255) = 'PhantomJS',
	@Default_Value_ClientCode varchar(255)= '',
	@Default_Value_ClientName varchar(255) = '',
	@Default_Value_ConsumerViewPassword varchar(255) = 'ERdfg45YqPzz', --REFACTOR REFERENCES, SETTING RENAMED FROM "ConsumerViewApiPassword"
	@Default_Value_ConsumerViewUserName varchar(255) = 'CardinalDemoNonCPA', --REFACTOR REFERENCES, SETTING RENAMED FROM "ConsumerViewApiUsername"
	@Default_Value_CountryCode varchar(255) = 'ZA',
	@Default_Value_CurrencySymbol varchar(255) = 'R',
	@Default_Value_DatabaseConnectionString varchar(255) = 'data source=.;Persist Security Info=False;User ID=iBroker_user; Password=iBroker_user; Initial Catalog=iBroker', --REFACTOR REFERENCES, SETTING RENAMED FROM "iBrokerConnectionString"
	@Default_Value_DefaultMapLatitude varchar(255) = '',
	@Default_Value_DefaultMapLongitude varchar(255) = '',
	@Default_Value_EchoTCFApiKey varchar(255) = '96108A52-2A0D-4A58-882F-4D59C30EADC1', --REFACTOR REFERENCES, SETTING RENAMED FROM "iadmin/connector/apiKey"
	@Default_Value_EchoTCFBypass bit = 1, --REFACTOR REFERENCES, SETTING RENAMED FROM "echotcf/connector/bypass"
	@Default_Value_EchoTCFToken varchar(255) = '96108A52-2A0D-4A58-882F-4D59C30EADC1', --REFACTOR REFERENCES, SETTING RENAMED FROM "echotcf/connector/token"
	@Default_Value_EchoTCFUri varchar(255) = 'http://www.rgtsmart.co.za/isd1/services/wsReceiveXML.asmx', --REFACTOR REFERENCES, SETTING RENAMED FROM "echotcf/connector/baseUrl"
	@Default_Value_EchoTCFUserName varchar(255) = '96108A52-2A0D-4A58-882F-4D59C30EADC1', --REFACTOR REFERENCES, SETTING RENAMED FROM "iadmin/connector/userName"
	@Default_Value_EmailTemplateSource varchar(255) = 'Content\Emailing\Templates', --REFACTOR REFERENCES, SETTING RENAMED FROM "templates/baseFolder"
	@Default_Value_EnableAddressEditorAddressType bit = 1,
	@Default_Value_EnableAddressEditorMap bit = 1,
	@Default_Value_EnableAgentLeadMosaicInfo bit = 1,
	@Default_Value_EnableAgentLeadWealthIndex bit = 1,
	@Default_Value_EnableAnnualValues bit = 1,
	@Default_Value_EnableBankAccountValidation bit = 0,
	@Default_Value_EnableBundling bit = 0,
	@Default_Value_EnableClientConsole bit = 0,
	@Default_Value_EnableDatabaseUpgrade bit = 1,
	@Default_Value_EnableDemoMode bit = 1,
	@Default_Value_EnableExternalReferenceValidation bit = 0,
	@Default_Value_EnableInsurerTemplate bit = 1, --REFACTOR REFERENCES, SETTING RENAMED FROM "templates/useInsurerTemplate"
	@Default_Value_EnableLeadIdentityNumberValidation bit = 1,
	@Default_Value_EnableMultipleQuoteAcceptance bit = 1,
	@Default_Value_EnablePerformanceCounter bit = 0, 
	@Default_Value_EnablePostalCodeInAddressEditor bit = 1,
	@Default_Value_EnableQuoteToPolicyInfo bit = 0,
	@Default_Value_EnableRate bit = 1,
	@Default_Value_EnableSASRIA bit = 1,
	@Default_Value_EnableSecondLevelUnderwriting bit = 0,
	@Default_Value_EnableSignFlowOnQuoteEMail bit = 0,
	@Default_Value_EnableUnderwritingRejectionNotice bit = 1, --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/leadstatus/underwriting"
	@Default_Value_EnableVehicleLookup bit = 1,
	@Default_Value_Environment varchar(255) = 'DEV',
	@Default_Value_iAdminUri varchar(255) = 'http://dev.iadmin.api/', --REFACTOR REFERENCES, SETTING RENAMED FROM "iadmin/connector/baseUrl"
	@Default_Value_IDSUri varchar(255) = 'http://195.202.66.179/STPWS/Command.svc', --REFACTOR REFERENCES, SETTING RENAMED FROM "IDS/Upload/ServiceURL"
	@Default_Value_iGuideApiKey varchar(255) = '2kw9a6njlSxqG7wRYYzHIU1m4Hee9UJZ23Y0Z99aSwo', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/iguide/apiKey"
	@Default_Value_iGuideEMail varchar(255) = 'iguide-root@iplatform.co.za', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/iguide/email"
	@Default_Value_iGuideV1Uri varchar(255) = 'http://staging-iguide.iplatform.co.za/api/', --REFACTOR REFERENCES, SETTING RENAMED FROM "iGuideApiUrl"
	@Default_Value_iGuideV2Uri varchar(255) = 'http://staging-iguide.iplatform.co.za', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/serviceLocator/iGuide/baseUrl"
	@Default_Value_InsurerCode varchar(255) = 'IP', --REFACTOR REFERENCES, QUERY DEFAULT CHANNEL OF INSTANCE INSTEAD
	@Default_Value_iPersonApiKey varchar(255) = '3BQmEDTa3FoZRXALMY82y38qXQqrpgcJPlO4kUmXP8Zab9siaNnG1X__41YpN3mn3eK6I72tdLN0DzPpYIRbu6jsmOASwyJnndGxZW9Dmv4QKax6M90FLKtywSkv6Jxf', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/iPerson/root/apiKey"
	@Default_Value_iPersonDefaultSource varchar(255) = 'PCubed', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/iPerson/root/defaultSource"
	@Default_Value_iPersonEMail varchar(255) = 'iperson-root@iplatform.co.za', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/iPerson/root/email"
	@Default_Value_iPersonEnableRetry bit = 1, --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/serviceLocator/iPerson/retry"
	@Default_Value_iPersonUri varchar(255) = 'http://staging-personv2.iplatform.co.za/', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/serviceLocator/iPerson/baseUrl"
	@Default_Value_iPlatformEnvironment varchar(50) = 'DEV', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/environment"
	@Default_Value_iRateUri varchar(255) = 'http://staging-rate.iplatform.co.za/', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/serviceLocator/iRate/baseUrl"
	@Default_Value_KenyaAutoKey varchar(255) = 'C26C64040DBEE49500013CBCFEC11122', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/keAuto/key"
	@Default_Value_KenyaAutoSecret varchar(255) = '6678979C75A70BBA85762F4D488AFB6F', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/keAuto/secret"
	@Default_Value_LanguageCode varchar(255) = 'EN',
	@Default_Value_LightStoneContractId varchar(255) = '8092e1ac-fc52-4182-8580-a9eff8f2d70b', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/lsAuto/contractId"
	@Default_Value_LightStoneCustomerId varchar(255) = 'f4844cd8-7731-4d4a-9083-55450740c0e0', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/lsAuto/customerId"
	@Default_Value_LightStonePackageId varchar(255) = '23634a56-6b41-4f73-a960-aa44f1eb2fb2', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/lsAuto/packageId"
	@Default_Value_LightStonePassword varchar(255) = 'iPlatf0rmap1', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/lsAuto/password"
	@Default_Value_LightStonePropertyUserId varchar(255) = 'd9f5dd64-95a4-4460-97ba-13c4f0ccb721', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/iPerson/lightStonePropertyUserId"
	@Default_Value_LightStoneUserId varchar(255) = 'a238d2a5-85de-4028-b351-5d14e0e016d5', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/lsAuto/userId"
	@Default_Value_LightStoneUserName varchar(255) = 'info@iplatform.co.za', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/lsAuto/username"
	@Default_Value_LoadBalancerApiV1Uri varchar(255) = '#{Octopus.Environment.Name}-api.iplatform.co.za', --REFACTOR REFERENCES, SETTING RENAMED FROM "LoadBalancerBinding"
	@Default_Value_LoadBalancerApiV2Uri varchar(255) = '#{Octopus.Environment.Name}-apiv2.iplatform.co.za', --REFACTOR REFERENCES, SETTING RENAMED FROM "LoadBalancerBindingAPI"
	@Default_Value_LoadBalancerWebUri varchar(255) = '#{Octopus.Environment.Name}.iplatform.co.za', --REFACTOR REFERENCES, SETTING RENAMED FROM "LoadBalancerBindingAPIv2"
	@Default_Value_Log4NetEnvironment varchar(255) = '#{Octopus.Environment.Name}', --REFACTOR REFERENCES, SETTING RENAMED FROM "log4net"
	@Default_Value_MachineSpecificBindingApiV1Uri varchar(255) = '#{Octopus.Environment.Name}-apiv2.iplatform.co.za', --REFACTOR REFERENCES, SETTING RENAMED FROM "MachineSpecificBinding"
	@Default_Value_MachineSpecificBindingApiV2Uri varchar(255) = '#{Octopus.Environment.Name}-apiv2.iplatform.co.za', --REFACTOR REFERENCES, SETTING RENAMED FROM "MachineSpecificBindingAPI"
	@Default_Value_MachineSpecificBindingWebUri varchar(255) = '#{Octopus.Environment.Name}-api.iplatform.co.za', --REFACTOR REFERENCES, SETTING RENAMED FROM "MachineSpecificBindingAPIv2"
	@Default_Value_MigrationAssemblyApiV1Path varchar(255) = '#{OctopusOriginalPackageDirectoryPath}\content\bin\iBroker.Database.dll', --REFACTOR REFERENCES, SETTING RENAMED FROM "MigrationAssemblyPath"
	@Default_Value_MigrationAssemblyApiV2Path varchar(255) = '#{OctopusOriginalPackageDirectoryPath}\content\bin\iPlatform.Database.dll', --REFACTOR REFERENCES, SETTING RENAMED FROM "MigrationAssemblyPathv2"
	@Default_Value_MigrationAssemblyExecutablePath varchar(255) = '#{OctopusOriginalPackageDirectoryPath}\content\bin\MasterDataGenerator.exe', --REFACTOR REFERENCES, SETTING RENAMED FROM "MigrationAssemblyConsolePath"
	@Default_Value_MigrationToolExecutablePath varchar(255) = '#{OctopusOriginalPackageDirectoryPath}\tools\migrator\migrate.exe', --REFACTOR REFERENCES, SETTING RENAMED FROM "MigratorPath"
	@Default_Value_NullResultDefault varchar(255) = '',
	@Default_Value_PasswordResetUri varchar(255) = 'http://dev.iplatform.web/Account/ResetPassword/', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/serviceLocator/iPlatform/baseUrl"
	@Default_Value_QuoteAcceptanceEnvironment varchar(50) = 'DEV', --REFACTOR REFERENCES, SETTING RENAMED FROM "quoteacceptance/environment"
	@Default_Value_QuoteAcceptanceMessage varchar(255) = '',
	@Default_Value_QuoteToPolicyDetailsHeader varchar(255) = 'Details',
	@Default_Value_ReportSource varchar(255) = 'Content\Reports',
	@Default_Value_ReportStorage varchar(255) = 'Content\Reports\Storage',
	@Default_Value_RootPassword varchar(255) = 'q1w2e3r4t5 ?_', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/apiKey"
	@Default_Value_RootUserName varchar(255) = 'root@iplatform.co.za', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/connector/userName"
	@Default_Value_SalesForceConsumerKey varchar(255) = '3MVG9RHx1QGZ7OshBkhlVsM6.qtqPEHvZCBesEg5Uo9bLXCS159yAxu2pSEuidos5ryHgL.pl03FGjQtJxxUx', --REFACTOR REFERENCES, SETTING RENAMED FROM "salesforce/consumerKey"
	@Default_Value_SalesForceConsumerSecret varchar(255) = '1824387848479415236', --REFACTOR REFERENCES, SETTING RENAMED FROM "salesforce/consumerSecret"
	@Default_Value_SalesForceDelay int = 900, --REFACTOR REFERENCES, SETTING RENAMED FROM "salesforce/delay"
	@Default_Value_SalesForceLeadSource varchar(255) = 'Quote Request', --REFACTOR REFERENCES, SETTING RENAMED FROM "salesforce/LeadSource"
	@Default_Value_SalesForcePassword varchar(255) = 'h0Ne11W8ss', --REFACTOR REFERENCES, SETTING RENAMED FROM "salesforce/password"
	@Default_Value_SalesForceSecurityKey varchar(255) = 'Z7UkcUrijsmbRO3WafVjyxMim', --REFACTOR REFERENCES, SETTING RENAMED FROM "salesforce/securityKey"
	@Default_Value_SalesForceStatus varchar(255) = 'New', --REFACTOR REFERENCES, SETTING RENAMED FROM "salesforce/Status"
	@Default_Value_SalesForceUri varchar(255) = 'https://test.salesforce.com/services/oauth2/token?', --REFACTOR REFERENCES, SETTING RENAMED FROM "salesforce/authServiceUrl"
	@Default_Value_SalesForceUserName varchar(255) = 'vmsaintegration@aig.com.gcc.uat', --REFACTOR REFERENCES, SETTING RENAMED FROM "salesforce/username"
	@Default_Value_SalesForceWebsiteSubmissionPath varchar(255) = '/quote/', --REFACTOR REFERENCES, SETTING RENAMED FROM "salesforce/SubmittedFromWebsitePage"
	@Default_Value_SecretSecurityKey varchar(255) = 'GQDstcKsx0NHjPOuXOYg5MbeJ1XT0uFiwDVvVBrk',
	@Default_Value_SelenoURL varchar(255) = 'http://dev.iplatform.web/',  --REFACTOR REFERENCES, USE @Value_WebUri INSTEAD,
	@Default_Value_ServiceBusEnvironment varchar(255) = '#{Octopus.Environment.Name}', --REFACTOR REFERENCES, SETTING RENAMED FROM "ServiceBus"
	@Default_Value_SignFlowPassword varchar(255) = 'iplatformuser01',
	@Default_Value_SignFlowSecretKey varchar(255) = 'mySecretKey',
	@Default_Value_SignFlowUri varchar(255) = 'https://flow.signflow.co.za/api/signFlowAPIservice.svc', --REFACTOR REFERENCES, SETTING RENAMED FROM "SignFlow/ServiceURL"
	@Default_Value_SignFlowUserName varchar(255) = 'taariq@iplatform.co.za',
	@Default_Value_SiteName varchar(255) = '#{Octopus.Environment.Name}-iplatform',
	@Default_Value_SSLIPAddress varchar(255) = '', --REFACTOR REFERENCES, SETTING RENAMED FROM "SSL_IP_Address"
	@Default_Value_SSLThumbPrint varchar(255) = '', --REFACTOR REFERENCES, SETTING RENAMED FROM "SSL_Thumbprint"
	@Default_Value_SubscriberServiceName varchar(255) = '#{Octopus.Environment.Name}.iPlatform.Subscriber.Service', --REFACTOR REFERENCES, SETTING RENAMED FROM "SubscriberServiceName"
	@Default_Value_TransUnionSecurityCode varchar(255) = 'AIG63', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/iPerson/TransUnion/SecurityCode"
	@Default_Value_TransUnionSubscriberCode varchar(255) = '2063', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/iPerson/TransUnion/SubscriberCode"
	@Default_Value_WarningLabel varchar(255) = 'warning(s)',
	@Default_Value_WebBranding varchar(255) = 'IP',
	@Default_Value_WebUri varchar(255) = 'http://dev.iplatform.web/', --REFACTOR REFERENCES, SETTING RENAMED FROM "iPlatform/serviceLocator/iPlatform/baseUrl"
	@Default_Value_WidgetFeeDiscount numeric(18, 2) = 0.5, --REFACTOR REFERENCES, SETTING RENAMED FROM "templates/useInsurerTemplate"
	@Default_Value_WorkFlowEngineServiceDescription varchar(255) = 'iPlatform.Workflow.Engine', --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/engine/serviceDescription"
	@Default_Value_WorkFlowEngineServiceDisplayName varchar(255) = 'iPlatform.Workflow.Engine', --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/engine/serviceDisplayName"
	@Default_Value_WorkFlowEngineServiceExchangeName varchar(255) = 'iplatform-workflow-engine', --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/engine/exchangeName"
	@Default_Value_WorkFlowEngineServiceExchangeNameError varchar(255) = 'iplatform-workflow-engine-errors', --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/engine/errorExchangeName"
	@Default_Value_WorkFlowEngineServiceName varchar(255) = 'iPlatform.Workflow.Engine', --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/engine/serviceName"
	@Default_Value_WorkFlowEngineServiceQueueNameError varchar(255) = 'iplatform-workflow-engine-errors', --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/engine/errorQueueName"
	@Default_Value_WorkFlowEnvironment varchar(50) = 'DEV', --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/environment"
	@Default_Value_WorkFlowRouterServiceDescription varchar(255) = 'iPlatform.Workflow.Router', --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/router/serviceDescription"
	@Default_Value_WorkFlowRouterServiceDisplayName varchar(255) = 'iPlatform.Workflow.Router', --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/router/serviceDisplayName"
	@Default_Value_WorkFlowRouterServiceExchangeName varchar(255) = 'iplatform-workflow-router', --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/router/exchangeName"
	@Default_Value_WorkFlowRouterServiceExchangeNameError varchar(255) = 'iplatform-workflow-router-errors', --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/router/errorExchangeName"
	@Default_Value_WorkFlowRouterServiceName varchar(255) = 'iPlatform.Workflow.Router', --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/router/serviceName"
	@Default_Value_WorkFlowRouterServiceQueueNameError varchar(255) = 'iplatform-workflow-router-errors' --REFACTOR REFERENCES, SETTING RENAMED FROM "workflow/router/errorQueueName"

--CHANNEL COMPANY INFORMATION SETTING NAMES
declare
	@Name_CompanyName varchar(255) = 'CompanyName',
	@Name_CompanyPhysicalAddress varchar(max) = 'CompanyPhysicalAddress',
	@Name_CompanyPostalAddress varchar(max) = 'CompanyPostalAddress',
	@Name_CompanyWorkNo varchar(255) = 'CompanyWorkNo',
	@Name_CompanyFaxNo varchar(255) = 'CompanyFaxNo',
	@Name_CompanyEmailAddress varchar(255) = 'CompanyEmailAddress',
	@Name_CompanyWebsiteUri varchar(max) = 'CompanyWebsiteUri',
	@Name_CompanyRegistrationNumber varchar(255) = 'CompanyRegistrationNumber',
	@Name_CompanyTaxNumber varchar(255) = 'CompanyTaxNumber',
	@Name_CompanyFSPNumber varchar(255) = 'CompanyFSPNumber'

--CHANNEL COMPANY INFORMATION SETTING DESCRIPTIONS
declare
	@Description_CompanyName varchar(255) = '',
	@Description_CompanyPhysicalAddress varchar(255) = '',
	@Description_CompanyPostalAddress varchar(255) = '',
	@Description_CompanyWorkNo varchar(255) = '',
	@Description_CompanyFaxNo varchar(255) = '',
	@Description_CompanyEmailAddress varchar(255) = '',
	@Description_CompanyWebsiteUri varchar(255) = '',
	@Description_CompanyRegistrationNumber varchar(255) = '',
	@Description_CompanyTaxNumber varchar(255) = '',
	@Description_CompanyFSPNumber varchar(255) = ''

--CHANNEL COMPANY INFORMATION SETTING OVERRIDE VALUES
declare
	@Value_CompanyName varchar(255),
	@Value_CompanyPhysicalAddress varchar(255),
	@Value_CompanyPostalAddress varchar(255),
	@Value_CompanyWorkNo varchar(255),
	@Value_CompanyFaxNo varchar(255),
	@Value_CompanyEmailAddress varchar(255),
	@Value_CompanyWebsiteUri varchar(255),
	@Value_CompanyRegistrationNumber varchar(255),
	@Value_CompanyTaxNumber varchar(255),
	@Value_CompanyFSPNumber varchar(255)

--CHANNEL COMPANY INFORMATION SETTING DEFAULT VALUES
declare
	@Default_Value_CompanyName varchar(255) = 'iPlatform',
	@Default_Value_CompanyPhysicalAddress varchar(255) = '1st Floor Block A, 28 on Sloane Office Park, 28 Sloane Street, Bryanston, South Africa',
	@Default_Value_CompanyPostalAddress varchar(255) = '',
	@Default_Value_CompanyWorkNo varchar(255) = '0861 988 888',
	@Default_Value_CompanyFaxNo varchar(255) = '',
	@Default_Value_CompanyEmailAddress varchar(255) = 'quotes@iplatform.co.za',
	@Default_Value_CompanyWebsiteUri varchar(255) = 'http://www.iplatform.co.za',
	@Default_Value_CompanyRegistrationNumber varchar(255) = '1999/006725/07',
	@Default_Value_CompanyTaxNumber varchar(255) = '',
	@Default_Value_CompanyFSPNumber varchar(255) = ''

--CREATE CURSOR FOR LOOPING THROUGH AVAILABLE CHANNELS & ASSOCIATED ENVIRONMENTS
declare
	@ChannelCursor cursor,
	@CursorChannelId int,
	@CursorChannelCode varchar(50),
	@CursorChannelName varchar(50),
	@CursorChannelEnvironmentId varchar(50),
	@CursorChannelEnvironmentKey varchar(50),
	@CursorChannelDetail varchar(255)

set @ChannelCursor = cursor for
	select
		Channel.Id,
		Channel.Code,
		Channel.Name,
		ChannelEnvironment.Id EnvironmentId,
		ChannelEnvironment.[Key] Environment
	from Channel
		inner join ChannelEnvironment on ChannelEnvironment.ChannelId = Channel.Id
	order by
		Channel.Name, Channel.Id, ChannelEnvironment.Id

open @ChannelCursor

--LOOP THROUGH AVAILABLE CHANNELS & ASSOCIATED ENVIRONMENTS
fetch next from @ChannelCursor into @CursorChannelId, @CursorChannelCode, @CursorChannelName, @CursorChannelEnvironmentId, @CursorChannelEnvironmentKey
	while @@fetch_status = 0
		begin
			select
				@CursorChannelDetail = @CursorChannelName + ' (' + @CursorChannelCode + '-' + @CursorChannelEnvironmentKey + ')'

			--RESET CHANNEL SETTING VALUES TO DEFAULT FOR EACH ITERATION
			select
				@Value_ApiV1TestingUri = @Default_Value_ApiV1TestingUri,
				@Value_ApiV1Uri = @Default_Value_ApiV1Uri,
				@Value_ApiV2TestingUri = @Default_Value_ApiV2TestingUri,
				@Value_ApiV2Uri = @Default_Value_ApiV2Uri,
				@Value_AuthenticationSuccessEvent = @Default_Value_AuthenticationSuccessEvent,
				@Value_BrokerName = @Default_Value_BrokerName,
				@Value_BrowserFactory = @Default_Value_BrowserFactory,
				@Value_ClientCode = @Default_Value_ClientCode,
				@Value_ClientName = @Default_Value_ClientName,
				@Value_ConsumerViewPassword = @Default_Value_ConsumerViewPassword,
				@Value_ConsumerViewUserName = @Default_Value_ConsumerViewUserName,
				@Value_CountryCode = @Default_Value_CountryCode,
				@Value_CurrencySymbol = @Default_Value_CurrencySymbol,
				@Value_DatabaseConnectionString = @Default_Value_DatabaseConnectionString,
				@Value_DefaultMapLatitude = @Default_Value_DefaultMapLatitude,
				@Value_DefaultMapLongitude = @Default_Value_DefaultMapLongitude,
				@Value_EchoTCFApiKey = @Default_Value_EchoTCFApiKey,
				@Value_EchoTCFBypass = @Default_Value_EchoTCFBypass,
				@Value_EchoTCFToken = @Default_Value_EchoTCFToken,
				@Value_EchoTCFUri = @Default_Value_EchoTCFUri,
				@Value_EchoTCFUserName = @Default_Value_EchoTCFUserName,
				@Value_EmailTemplateSource = @Default_Value_EmailTemplateSource,
				@Value_EnableAddressEditorAddressType = @Default_Value_EnableAddressEditorAddressType,
				@Value_EnableAddressEditorMap = @Default_Value_EnableAddressEditorMap,
				@Value_EnableAgentLeadMosaicInfo = @Default_Value_EnableAgentLeadMosaicInfo,
				@Value_EnableAgentLeadWealthIndex = @Default_Value_EnableAgentLeadWealthIndex,
				@Value_EnableAnnualValues = @Default_Value_EnableAnnualValues,
				@Value_EnableBankAccountValidation = @Default_Value_EnableBankAccountValidation,
				@Value_EnableBundling = @Default_Value_EnableBundling,
				@Value_EnableClientConsole = @Default_Value_EnableClientConsole,
				@Value_EnableDatabaseUpgrade = @Default_Value_EnableDatabaseUpgrade,
				@Value_EnableDemoMode = @Default_Value_EnableDemoMode,
				@Value_EnableExternalReferenceValidation = @Default_Value_EnableExternalReferenceValidation,
				@Value_EnableInsurerTemplate = @Default_Value_EnableInsurerTemplate,
				@Value_EnableLeadIdentityNumberValidation = @Default_Value_EnableLeadIdentityNumberValidation,
				@Value_EnableMultipleQuoteAcceptance = @Default_Value_EnableMultipleQuoteAcceptance,
				@Value_EnablePerformanceCounter = @Default_Value_EnablePerformanceCounter,
				@Value_EnablePostalCodeInAddressEditor = @Default_Value_EnablePostalCodeInAddressEditor,
				@Value_EnableQuoteToPolicyInfo = @Default_Value_EnableQuoteToPolicyInfo,
				@Value_EnableRate = @Default_Value_EnableRate,
				@Value_EnableSASRIA = @Default_Value_EnableSASRIA,
				@Value_EnableSecondLevelUnderwriting = @Default_Value_EnableSecondLevelUnderwriting,
				@Value_EnableSignFlowOnQuoteEMail = @Default_Value_EnableSignFlowOnQuoteEMail,
				@Value_EnableUnderwritingRejectionNotice = @Default_Value_EnableUnderwritingRejectionNotice,
				@Value_EnableVehicleLookup = @Default_Value_EnableVehicleLookup,
				@Value_Environment = @Default_Value_Environment,
				@Value_iAdminUri = @Default_Value_iAdminUri,
				@Value_IDSUri = @Default_Value_IDSUri,
				@Value_iGuideApiKey = @Default_Value_iGuideApiKey,
				@Value_iGuideEMail = @Default_Value_iGuideEMail,
				@Value_iGuideV1Uri = @Default_Value_iGuideV1Uri,
				@Value_iGuideV2Uri = @Default_Value_iGuideV2Uri,
				@Value_InsurerCode = @Default_Value_InsurerCode,
				@Value_iPersonApiKey = @Default_Value_iPersonApiKey,
				@Value_iPersonDefaultSource = @Default_Value_iPersonDefaultSource,
				@Value_iPersonEMail = @Default_Value_iPersonEMail,
				@Value_iPersonEnableRetry = @Default_Value_iPersonEnableRetry,
				@Value_iPersonUri = @Default_Value_iPersonUri,
				@Value_iPlatformEnvironment = @Default_Value_iPlatformEnvironment,
				@Value_iRateUri = @Default_Value_iRateUri,
				@Value_KenyaAutoKey = @Default_Value_KenyaAutoKey,
				@Value_KenyaAutoSecret = @Default_Value_KenyaAutoSecret,
				@Value_LanguageCode = @Default_Value_LanguageCode,
				@Value_LightStoneContractId = @Default_Value_LightStoneContractId,
				@Value_LightStoneCustomerId = @Default_Value_LightStoneCustomerId,
				@Value_LightStonePackageId = @Default_Value_LightStonePackageId,
				@Value_LightStonePassword = @Default_Value_LightStonePassword,
				@Value_LightStonePropertyUserId = @Default_Value_LightStonePropertyUserId,
				@Value_LightStoneUserId = @Default_Value_LightStoneUserId,
				@Value_LightStoneUserName = @Default_Value_LightStoneUserName,
				@Value_LoadBalancerApiV1Uri = @Default_Value_LoadBalancerApiV1Uri,
				@Value_LoadBalancerApiV2Uri = @Default_Value_LoadBalancerApiV2Uri,
				@Value_LoadBalancerWebUri = @Default_Value_LoadBalancerWebUri,
				@Value_Log4NetEnvironment = @Default_Value_Log4NetEnvironment,
				@Value_MachineSpecificBindingApiV1Uri = @Default_Value_MachineSpecificBindingApiV1Uri,
				@Value_MachineSpecificBindingApiV2Uri = @Default_Value_MachineSpecificBindingApiV2Uri,
				@Value_MachineSpecificBindingWebUri = @Default_Value_MachineSpecificBindingWebUri,
				@Value_MigrationAssemblyApiV1Path = @Default_Value_MigrationAssemblyApiV1Path,
				@Value_MigrationAssemblyApiV2Path = @Default_Value_MigrationAssemblyApiV2Path,
				@Value_MigrationAssemblyExecutablePath = @Default_Value_MigrationAssemblyExecutablePath,
				@Value_MigrationToolExecutablePath = @Default_Value_MigrationToolExecutablePath,
				@Value_NullResultDefault = @Default_Value_NullResultDefault,
				@Value_PasswordResetUri = @Default_Value_PasswordResetUri,
				@Value_QuoteAcceptanceEnvironment = @Default_Value_QuoteAcceptanceEnvironment,
				@Value_QuoteAcceptanceMessage = @Default_Value_QuoteAcceptanceMessage,
				@Value_QuoteToPolicyDetailsHeader = @Default_Value_QuoteToPolicyDetailsHeader,
				@Value_ReportSource = @Default_Value_ReportSource,
				@Value_ReportStorage = @Default_Value_ReportStorage,
				@Value_RootPassword = @Default_Value_RootPassword,
				@Value_RootUserName = @Default_Value_RootUserName,
				@Value_SalesForceConsumerKey = @Default_Value_SalesForceConsumerKey,
				@Value_SalesForceConsumerSecret = @Default_Value_SalesForceConsumerSecret,
				@Value_SalesForceDelay = @Default_Value_SalesForceDelay,
				@Value_SalesForceLeadSource = @Default_Value_SalesForceLeadSource,
				@Value_SalesForcePassword = @Default_Value_SalesForcePassword,
				@Value_SalesForceSecurityKey = @Default_Value_SalesForceSecurityKey,
				@Value_SalesForceStatus = @Default_Value_SalesForceStatus,
				@Value_SalesForceUri = @Default_Value_SalesForceUri,
				@Value_SalesForceUserName = @Default_Value_SalesForceUserName,
				@Value_SalesForceWebsiteSubmissionPath = @Default_Value_SalesForceWebsiteSubmissionPath,
				@Value_SecretSecurityKey = @Default_Value_SecretSecurityKey,
				@Value_SelenoURL = @Default_Value_SelenoURL,
				@Value_ServiceBusEnvironment = @Default_Value_ServiceBusEnvironment,
				@Value_SignFlowPassword = @Default_Value_SignFlowPassword,
				@Value_SignFlowSecretKey = @Default_Value_SignFlowSecretKey,
				@Value_SignFlowUri = @Default_Value_SignFlowUri,
				@Value_SignFlowUserName = @Default_Value_SignFlowUserName,
				@Value_SiteName = @Default_Value_SiteName,
				@Value_SSLIPAddress = @Default_Value_SSLIPAddress,
				@Value_SSLThumbPrint = @Default_Value_SSLThumbPrint,
				@Value_SubscriberServiceName = @Default_Value_SubscriberServiceName,
				@Value_TransUnionSecurityCode = @Default_Value_TransUnionSecurityCode,
				@Value_TransUnionSubscriberCode = @Default_Value_TransUnionSubscriberCode,
				@Value_WarningLabel = @Default_Value_WarningLabel,
				@Value_WebBranding = @Default_Value_WebBranding,
				@Value_WebUri = @Default_Value_WebUri,
				@Value_WidgetFeeDiscount = @Default_Value_WidgetFeeDiscount,
				@Value_WorkFlowEngineServiceDescription = @Default_Value_WorkFlowEngineServiceDescription,
				@Value_WorkFlowEngineServiceDisplayName = @Default_Value_WorkFlowEngineServiceDisplayName,
				@Value_WorkFlowEngineServiceExchangeName = @Default_Value_WorkFlowEngineServiceExchangeName,
				@Value_WorkFlowEngineServiceExchangeNameError = @Default_Value_WorkFlowEngineServiceExchangeNameError,
				@Value_WorkFlowEngineServiceName = @Default_Value_WorkFlowEngineServiceName,
				@Value_WorkFlowEngineServiceQueueNameError = @Default_Value_WorkFlowEngineServiceQueueNameError,
				@Value_WorkFlowEnvironment = @Default_Value_WorkFlowEnvironment,
				@Value_WorkFlowRouterServiceDescription = @Default_Value_WorkFlowRouterServiceDescription,
				@Value_WorkFlowRouterServiceDisplayName = @Default_Value_WorkFlowRouterServiceDisplayName,
				@Value_WorkFlowRouterServiceExchangeName = @Default_Value_WorkFlowRouterServiceExchangeName,
				@Value_WorkFlowRouterServiceExchangeNameError = @Default_Value_WorkFlowRouterServiceExchangeNameError,
				@Value_WorkFlowRouterServiceName = @Default_Value_WorkFlowRouterServiceName,
				@Value_WorkFlowRouterServiceQueueNameError = @Default_Value_WorkFlowRouterServiceQueueNameError

			--RESET CHANNEL COMPANY INFORMATION SETTING VALUES TO DEFAULT FOR EACH ITERATION
			select
				@Value_CompanyName = @Default_Value_CompanyName,
				@Value_CompanyPhysicalAddress = @Default_Value_CompanyPhysicalAddress,
				@Value_CompanyPostalAddress = @Default_Value_CompanyPostalAddress,
				@Value_CompanyWorkNo = @Default_Value_CompanyWorkNo,
				@Value_CompanyFaxNo = @Default_Value_CompanyFaxNo,
				@Value_CompanyEmailAddress = @Default_Value_CompanyEmailAddress,
				@Value_CompanyWebsiteUri = @Default_Value_CompanyWebsiteUri,
				@Value_CompanyRegistrationNumber = @Default_Value_CompanyRegistrationNumber,
				@Value_CompanyTaxNumber = @Default_Value_CompanyTaxNumber,
				@Value_CompanyFSPNumber = @Default_Value_CompanyFSPNumber

			print @CursorChannelDetail + ' environment settings setup in progress.'

			--OVERRIDE DEFAULT VALUES BASED ON CHANNEL CODE / ENVIRONMENT KEY
			if(upper(@CursorChannelCode) = 'AIG')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = '',
						@Value_CompanyPhysicalAddress = '',
						@Value_CompanyPostalAddress = '',
						@Value_CompanyWorkNo = '',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = '',
						@Value_CompanyWebsiteUri = '',
						@Value_CompanyRegistrationNumber = '',
						@Value_CompanyTaxNumber = '',
						@Value_CompanyFSPNumber = ''

					if(upper(@CursorChannelEnvironmentKey) = 'Staging')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Api) 
							select
								@Value_ApiV2Uri = 'http://aig-staging-apiv2.iplatform.co.za/',
								@Value_InsurerCode = 'AIG',
								@Value_QuoteAcceptanceEnvironment = 'AIG-Staging',
								@Value_WorkFlowEnvironment = 'AIG-Staging'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://aig-staging-api.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://aig-staging-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_ConsumerViewPassword = 'ERdfg45YqPzz',
								@Value_ConsumerViewUserName = 'CardinalDemoNonCPA',
								@Value_DatabaseConnectionString = 'data source=db01;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=staging_aig',
								@Value_EnableBundling = 1,
								@Value_Environment = 'AIG-Staging', --#{Octopus.Environment.Name}
								@Value_IDSUri = 'http://196.29.140.109/IDSSTP/Command.svc',
								@Value_iGuideV1Uri = 'http://aig-staging-iguide.iplatform.co.za/api/', --http://#{Octopus.Environment.Name}-iguide.iplatform.co.za/api/
								@Value_iGuideV2Uri = 'http://aig-staging-iguide.iplatform.co.za', --http://#{Octopus.Environment.Name}-iguide.iplatform.co.za
								@Value_InsurerCode = 'AIG',
								@value_iPersonUri = 'http://aig-staging-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_iRateUri = 'http://aig-staging-rate.iplatform.co.za/', --http://#{Octopus.Environment.Name}-rate.iplatform.co.za/
								@Value_LoadBalancerApiV1Uri = 'aig-staging-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'aig-staging-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'aig-staging.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_MachineSpecificBindingApiV1Uri = 'aig-staging-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'aig-staging-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'aig-staging.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://aig-staging.iplatform.co.za/Account/ResetPassword/', --http://#{Octopus.Environment.Name}.iplatform.co.za/Account/ResetPassword/
								@Value_SalesForceDelay = 300,
								@Value_SiteName = 'aig-staging-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'aig',
								@Value_WorkFlowEnvironment = 'aig-staging' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'SIT')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Api) 
							select
								@Value_ApiV2Uri = 'http://aig-sit-apiv2',
								@Value_InsurerCode = 'AIG',
								@Value_QuoteAcceptanceEnvironment = 'AIG-SIT',
								@Value_WorkFlowEnvironment = 'AIG-SIT'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://aig-sit-api', --http://#{Octopus.Environment.Name}-api
								@Value_ApiV2Uri = 'http://aig-sit-apiv2', --http://#{Octopus.Environment.Name}-apiv2
								@Value_DatabaseConnectionString = 'data source=10.11.41.201;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=SIT_iPlatform',
								@Value_EnableBundling = 1,
								@Value_Environment = 'AIG-SIT', --#{Octopus.Environment.Name}
								@Value_IDSUri = 'http://10.11.41.141/stpws/command.svc',
								@Value_iGuideV1Uri = 'http://aig-sit-iguide/api', --http://#{Octopus.Environment.Name}-iguide/api
								@Value_iGuideV2Uri = 'http://aig-sit-iguide', --http://#{Octopus.Environment.Name}-iguide
								@Value_InsurerCode = 'AIG',
								@value_iPersonUri = 'http://aig-sit-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_iRateUri = 'http://aig-sit-rate.iplatform.co.za/', --http://#{Octopus.Environment.Name}-rate.iplatform.co.za/
								@Value_LoadBalancerApiV1Uri = 'aig-sit-api', --#{Octopus.Environment.Name}-api
								@Value_LoadBalancerApiV2Uri = 'aig-sit-apiv2', --#{Octopus.Environment.Name}-apiv2
								@Value_LoadBalancerWebUri = 'aig-sit', --#{Octopus.Environment.Name}
								@Value_MachineSpecificBindingApiV1Uri = 'aig-sit-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'aig-sit-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'aig-sit.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://10.11.41.78.iplatform.co.za/Account/ResetPassword/',
								@Value_SiteName = 'aig-sit-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'aig',
								@Value_WorkFlowEnvironment = 'aig-sit' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Api) 
							select
								@Value_ApiV2Uri = 'http://aig-uat-apiv2/',
								@Value_InsurerCode = 'AIG',
								@Value_QuoteAcceptanceEnvironment = 'AIG-UAT',
								@Value_WorkFlowEnvironment = 'AIG-UAT'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://aig-uat-api', --http://#{Octopus.Environment.Name}-api
								@Value_ApiV2Uri = 'http://aig-uat-apiv2', --http://#{Octopus.Environment.Name}-apiv2
								@Value_DatabaseConnectionString = 'data source=10.11.41.201;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iPlatform',
								@Value_EnableBundling = 1,
								@Value_Environment = 'AIG-UAT', --#{Octopus.Environment.Name}
								@Value_IDSUri = 'http://10.11.41.140/stpws/command.svc',
								@Value_iGuideV1Uri = 'http://aig-uat-iguide/api', --http://#{Octopus.Environment.Name}-iguide/api
								@Value_iGuideV2Uri = 'http://aig-uat-iguide', --http://#{Octopus.Environment.Name}-iguide
								@Value_InsurerCode = 'AIG',
								@value_iPersonUri = 'http://aig-uat-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_iRateUri = 'http://aig-uat-rate.iplatform.co.za/', --http://#{Octopus.Environment.Name}-rate.iplatform.co.za/
								@Value_LoadBalancerApiV1Uri = 'aig-uat-api', --#{Octopus.Environment.Name}-api
								@Value_LoadBalancerApiV2Uri = 'aig-uat-apiv2', --#{Octopus.Environment.Name}-apiv2
								@Value_LoadBalancerWebUri = 'aig-uat', --#{Octopus.Environment.Name}
								@Value_MachineSpecificBindingApiV1Uri = 'aig-uat-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'aig-uat-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'aig-uat.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://10.11.41.77.iplatform.co.za/Account/ResetPassword/',
								@Value_SiteName = 'aig-uat-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'aig',
								@Value_WorkFlowEnvironment = 'aig-uat' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Pre-Production')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Api) 
							select
								@Value_ApiV2Uri = 'http://aig-pre-production-apiv2/',
								@Value_InsurerCode = 'AIG',
								@Value_QuoteAcceptanceEnvironment = 'AIG-Pre-Production',
								@Value_WorkFlowEnvironment = 'AIG-Pre-Production'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://aig-pre-production-api', --http://#{Octopus.Environment.Name}-api
								@Value_ApiV2Uri = 'http://aig-pre-production-apiv2', --http://#{Octopus.Environment.Name}-apiv2
								@Value_DatabaseConnectionString = 'data source=10.11.41.201;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iPlatformPP',
								@Value_EnableBundling = 1,
								@Value_Environment = 'AIG-Pre-Production', --#{Octopus.Environment.Name}
								@Value_IDSUri = 'http://10.11.41.148/STPWS/Command.svc',
								@Value_iGuideV1Uri = 'http://aig-pre-production-iguide/api', --http://#{Octopus.Environment.Name}-iguide/api
								@Value_iGuideV2Uri = 'http://aig-pre-production-iguide', --http://#{Octopus.Environment.Name}-iguide
								@Value_InsurerCode = 'AIG',
								@value_iPersonUri = 'http://aig-pre-production-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_iRateUri = 'http://aig-pre-production-rate.iplatform.co.za/', --http://#{Octopus.Environment.Name}-rate.iplatform.co.za/
								@Value_LoadBalancerApiV1Uri = 'aig-pre-production-api', --#{Octopus.Environment.Name}-api
								@Value_LoadBalancerApiV2Uri = 'aig-pre-production-apiv2', --#{Octopus.Environment.Name}-apiv2
								@Value_LoadBalancerWebUri = 'aig-pre-production', --#{Octopus.Environment.Name}
								@Value_MachineSpecificBindingApiV1Uri = 'aig-pre-production-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'aig-pre-production-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'aig-pre-production.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://10.11.41.78.iplatform.co.za/Account/ResetPassword/',
								@Value_SiteName = 'aig-pre-production-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'aig',
								@Value_WorkFlowEnvironment = 'aig-pre-production' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Api) 
							select
								@Value_ApiV2Uri = 'http://aig-live-apiv2/',
								@Value_InsurerCode = 'AIG',
								@Value_QuoteAcceptanceEnvironment = 'AIG-Live',
								@Value_WorkFlowEnvironment = 'AIG-Live'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://aig-live-api', --http://#{Octopus.Environment.Name}-api
								@Value_ApiV2Uri = 'http://aig-live-apiv2', --http://#{Octopus.Environment.Name}-apiv2
								@Value_ConsumerViewPassword = '1d9f2e48-0d09-4d0d-b027-2c00188ddb2f',
								@Value_ConsumerViewUserName = 'CardinalNonCPA',
								@Value_DatabaseConnectionString = 'data source=10.11.41.200;Persist Security Info=False;User ID=Iplatform; password=CVX\K6+6; Initial Catalog=iPlatform',
								@Value_EnableBundling = 1,
								@Value_Environment = 'AIG-Live', --#{Octopus.Environment.Name}
								@Value_IDSUri = 'http://10.11.41.137/STPWS/Command.svc',
								@Value_iGuideV1Uri = 'http://aig-live-iguide/api', --http://#{Octopus.Environment.Name}-iguide/api
								@Value_iGuideV2Uri = 'http://aig-live-iguide', --http://#{Octopus.Environment.Name}-iguide
								@Value_InsurerCode = 'AIG',
								@value_iPersonUri = 'http://aig-live-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_iRateUri = 'http://aig-live-rate.iplatform.co.za/', --http://#{Octopus.Environment.Name}-rate.iplatform.co.za/
								@Value_LightStonePackageId = '8b1a118c-2e1d-47af-a3a1-2b0b1ab2b7d7',
								@Value_LightStonePassword = 'AIGPr0D',
								@Value_LightStoneUserId = '4a822e9a-44a3-41f3-b6b3-5d6174e21315',
								@Value_LightStoneUserName = 'info@cardinal.co.za',
								@Value_TransUnionSecurityCode = 'AGW94',
								@Value_TransUnionSubscriberCode = '55494',
								@Value_LoadBalancerApiV1Uri = 'aig-live-api', --#{Octopus.Environment.Name}-api
								@Value_LoadBalancerApiV2Uri = 'aig-live-apiv2', --#{Octopus.Environment.Name}-apiv2
								@Value_LoadBalancerWebUri = 'aig-live', --#{Octopus.Environment.Name}
								@Value_MachineSpecificBindingApiV1Uri = 'aig-live-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'aig-live-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'aig-live.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://10.11.41.81.iplatform.co.za/Account/ResetPassword/',
								@Value_SalesForceUri = 'https://login.salesforce.com/services/oauth2/token?',
								@Value_SalesForceConsumerKey  = '3MVG9sG9Z3Q1RlbcVZNJf7YysRVdIp6ZYe_Q_whJoAsG7qGlyu1_sxQqVBD2OYkdNOu3941bMDDWDgRb_Wzly',
								@Value_SalesForceConsumerSecret = '5709214593029917475',
								@Value_SalesForcePassword = 'VMSA!nteg123',
								@Value_SalesForceSecurityKey = 'BzcbC0oexQxeIirxlvuuHmmOU',
								@Value_SalesForceUserName  = 'vmsaintegration@aig.com.gcc',
								@Value_SiteName = 'aig-live-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'AIG',
								@Value_WorkFlowEnvironment = 'aig-live' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
				end
			else if(upper(@CursorChannelCode) = 'AON')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = '',
						@Value_CompanyPhysicalAddress = '',
						@Value_CompanyPostalAddress = '',
						@Value_CompanyWorkNo = '',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = '',
						@Value_CompanyWebsiteUri = '',
						@Value_CompanyRegistrationNumber = '',
						@Value_CompanyTaxNumber = '',
						@Value_CompanyFSPNumber = ''

					if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web) 
							select
								@Value_ApiV2Uri = 'http://aon-apiv2.iplatform.co.za/',
								@Value_BrokerName = 'AON',
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_InsurerCode = 'aon',
								@Value_WebBranding = 'aon'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://aon.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://aon-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_BrokerName = 'AONSouthAfrica',
								@Value_DatabaseConnectionString = 'data source=db02;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iplatform_aon',
								@Value_iGuideV2Uri = 'http://guide-v2.iplatform.co.za/',
								@Value_InsurerCode = 'aon',
								@value_iPersonUri = 'http://aon-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_iRateUri = 'http://rate.iplatform.co.za/',
								@Value_LoadBalancerApiV1Uri = 'aon-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'aon-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'aon.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_MachineSpecificBindingApiV1Uri = 'aon-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'aon-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'aon.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://aig-staging.iplatform.co.za/Account/ResetPassword/', --http://#{Octopus.Environment.Name}.iplatform.co.za/Account/ResetPassword/
								@Value_SiteName = 'aon-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'aon',
								@Value_WorkFlowEnvironment = 'aon' --#{Octopus.Environment.Name}
						end
				end
			else if(upper(@CursorChannelCode) = 'AutoPedigree')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = '',
						@Value_CompanyPhysicalAddress = '',
						@Value_CompanyPostalAddress = '',
						@Value_CompanyWorkNo = '',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = '',
						@Value_CompanyWebsiteUri = '',
						@Value_CompanyRegistrationNumber = '',
						@Value_CompanyTaxNumber = '',
						@Value_CompanyFSPNumber = ''

					if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Api & Web) 

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://autopedigree-uat.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://autopedigree-uat-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_BrokerName = 'AutoPedigree',
								@Value_DatabaseConnectionString = 'data source=db02;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iplatform_autopedigree-UAT',
								@Value_iGuideV1Uri = 'http://staging-iguide.iplatform.co.za/api',
								@Value_InsurerCode = 'kp',
								@Value_iRateUri = 'http://kp-uat-rate.iplatform.co.za/',
								@Value_LoadBalancerApiV1Uri = 'autopedigree-uat-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'autopedigree-uat-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'autopedigree-uat.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_MachineSpecificBindingApiV1Uri = 'autopedigree-uat-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'autopedigree-uat-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'autopedigree-uat.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_SiteName = 'autopedigree-uat-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'kp',
								@Value_WorkFlowEnvironment = 'autopedigree-uat' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Api & Web) 

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://autopedigree.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://autopedigree-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_BrokerName = 'AutoPedigree',
								@Value_DatabaseConnectionString = 'data source=db02;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iplatform_autopedigree',
								@Value_iGuideV1Uri = 'http://staging-iguide.iplatform.co.za/api',
								@Value_InsurerCode = 'kp',
								@Value_iRateUri = 'http://kp-rate.iplatform.co.za/',
								@Value_LoadBalancerApiV1Uri = 'autopedigree-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'autopedigree-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'autopedigree.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_MachineSpecificBindingApiV1Uri = 'autopedigree-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'autopedigree-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'autopedigree.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_SiteName = 'autopedigree-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'kp',
								@Value_WorkFlowEnvironment = 'autopedigree' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
				end
			else if(upper(@CursorChannelCode) = 'BIDVEST')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = '',
						@Value_CompanyPhysicalAddress = '',
						@Value_CompanyPostalAddress = '',
						@Value_CompanyWorkNo = '',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = '',
						@Value_CompanyWebsiteUri = '',
						@Value_CompanyRegistrationNumber = '',
						@Value_CompanyTaxNumber = '',
						@Value_CompanyFSPNumber = ''

					if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
				end
			else if(upper(@CursorChannelCode) = 'BOWER')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = '',
						@Value_CompanyPhysicalAddress = '',
						@Value_CompanyPostalAddress = '',
						@Value_CompanyWorkNo = '',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = '',
						@Value_CompanyWebsiteUri = '',
						@Value_CompanyRegistrationNumber = '',
						@Value_CompanyTaxNumber = '',
						@Value_CompanyFSPNumber = ''

					if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web)
							select
								@Value_ApiV2Uri = 'http://live-apiv2.iplatform.co.za/',
								@Value_BrokerName = 'BOWER',
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_InsurerCode = 'IP',
								@Value_WebBranding = 'IP'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://live.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://live-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_BrokerName = 'BOWER',
								@Value_DatabaseConnectionString = 'data source=db02;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iplatform_live',
								@Value_Environment = 'live', --#{Octopus.Environment.Name}
								@Value_iGuideV2Uri = 'http://guide-v2.iplatform.co.za/',
								@Value_InsurerCode = 'IP',
								@Value_iPersonUri = 'http://live-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_iRateUri = 'http://rate.iplatform.co.za/',
								@Value_LoadBalancerApiV1Uri = 'live-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'live-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'live.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_Log4NetEnvironment = 'live', --#{Octopus.Environment.Name}
								@Value_MachineSpecificBindingApiV1Uri = 'live-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'live-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'live.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://live.iplatform.co.za/Account/ResetPassword/', --http://#{Octopus.Environment.Name}.iplatform.co.za/Account/ResetPassword/
								@Value_QuoteAcceptanceEnvironment = 'live', --#{Octopus.Environment.Name}
								@Value_SiteName = 'live-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'IP',
								@Value_WorkFlowEnvironment = 'live' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
				end
			else if(upper(@CursorChannelCode) = 'HOL')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = '',
						@Value_CompanyPhysicalAddress = '',
						@Value_CompanyPostalAddress = '',
						@Value_CompanyWorkNo = '',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = '',
						@Value_CompanyWebsiteUri = '',
						@Value_CompanyRegistrationNumber = '',
						@Value_CompanyTaxNumber = '',
						@Value_CompanyFSPNumber = ''

					if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
				end
			else if(upper(@CursorChannelCode) = 'INDWE')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = 'Indwe Risk Services',
						@Value_CompanyPhysicalAddress = 'Pamodzi House | 5 Willowbrook Close | Melrose North | 2196',
						@Value_CompanyPostalAddress = '',
						@Value_CompanyWorkNo = '0860 13 13 14',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = 'quotes@indwerisk.co.za',
						@Value_CompanyWebsiteUri = 'http://www.indwe.co.za',
						@Value_CompanyRegistrationNumber = '1994/004436/07',
						@Value_CompanyTaxNumber = '',
						@Value_CompanyFSPNumber = '3425'

					if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_ApiV2Uri = 'http://indwe-uat-apiv2.iplatform.co.za/',
								@Value_BrokerName = 'Indwe',
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_Environment = 'UAT',
								@Value_iAdminUri = 'http://admin.iplatform.co.za/',
								@Value_iGuideV2Uri = 'http://guide-v2.iplatform.co.za/',
								@Value_InsurerCode = 'INDWE',
								@Value_iPersonUri = 'http://staging-personv2.iplatform.co.za/',
								@Value_iRateUri = 'http://psg-uat-rate.iplatform.co.za/',
								@Value_PasswordResetUri = 'http://indwe-uat.iplatform.co.za/Account/ResetPassword/',
								@Value_WebBranding = 'indwe',
								@Value_EnableUnderwritingRejectionNotice = 1,
								@Value_QuoteAcceptanceEnvironment = 'INDWE-UAT',
								@Value_WorkFlowEnvironment = 'INDWE-UAT'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://indwe-uat.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://indwe-uat-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_BrokerName = 'INDWE',
								@Value_DatabaseConnectionString = 'data source=db01;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iplatform_indweuat',
								@Value_Environment = 'live', --#{Octopus.Environment.Name}
								@Value_iGuideV1Uri = 'http://staging-iguide.iplatform.co.za/api',
								@Value_iGuideV2Uri = 'http://guide-v2.iplatform.co.za/',
								@Value_InsurerCode = 'INDWE',
								@Value_iRateUri = 'http://rate.iplatform.co.za/',
								@Value_LoadBalancerApiV1Uri = 'indwe-uat-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'indwe-uat-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'indwe-uat.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_MachineSpecificBindingApiV1Uri = 'indwe-uat-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'indwe-uat-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'indwe-uat.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://indwe-uat.iplatform.co.za/Account/ResetPassword/', --http://#{Octopus.Environment.Name}.iplatform.co.za/Account/ResetPassword/
								@Value_QuoteAcceptanceEnvironment = 'indwe-uat', --#{Octopus.Environment.Name}
								@Value_SiteName = 'indwe-uat-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'indwe',
								@Value_WorkFlowEnvironment = 'indwe-uat' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_ApiV2Uri = 'http://indwe-apiv2.iplatform.co.za/',
								@Value_BrokerName = 'Indwe',
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_Environment = 'Live',
								@Value_iAdminUri = 'http://admin.iplatform.co.za/',
								@Value_iGuideV2Uri = 'http://guide-v2.iplatform.co.za/',
								@Value_InsurerCode = 'INDWE',
								@Value_iPersonUri = 'http://staging-personv2.iplatform.co.za/',
								@Value_iRateUri = 'http://psg-rate.iplatform.co.za/',
								@Value_PasswordResetUri = 'http://indwe.iplatform.co.za/Account/ResetPassword/',
								@Value_WebBranding = 'indwe',
								@Value_EnableUnderwritingRejectionNotice = 1,
								@Value_QuoteAcceptanceEnvironment = 'INDWE',
								@Value_WorkFlowEnvironment = 'INDWE',
								@Value_EchoTCFBypass = 0

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://indwe.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://indwe-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_BrokerName = 'INDWE',
								@Value_DatabaseConnectionString = 'data source=db02;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iplatform_indwe',
								@Value_Environment = 'Indwe', --#{Octopus.Environment.Name}
								@Value_iGuideV1Uri = 'http://staging-iguide.iplatform.co.za/api',
								@Value_iGuideV2Uri = 'http://guide-v2.iplatform.co.za/',
								@Value_InsurerCode = 'INDWE',
								@Value_iRateUri = 'http://rate.iplatform.co.za/',
								@Value_LoadBalancerApiV1Uri = 'indwe-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'indwe-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'indwe.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_MachineSpecificBindingApiV1Uri = 'indwe-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'indwe-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'indwe-uat.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://live.iplatform.co.za/Account/ResetPassword/', --http://#{Octopus.Environment.Name}.iplatform.co.za/Account/ResetPassword/
								@Value_QuoteAcceptanceEnvironment = 'indwe', --#{Octopus.Environment.Name}
								@Value_SiteName = 'indwe-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'indwe',
								@Value_WorkFlowEnvironment = 'indwe' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
				end
			else if(upper(@CursorChannelCode) = 'IP')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = @Default_Value_CompanyName,
						@Value_CompanyPhysicalAddress = @Default_Value_CompanyPhysicalAddress,
						@Value_CompanyPostalAddress = @Default_Value_CompanyPostalAddress,
						@Value_CompanyWorkNo = @Default_Value_CompanyWorkNo,
						@Value_CompanyFaxNo = @Default_Value_CompanyFaxNo,
						@Value_CompanyEmailAddress = @Default_Value_CompanyEmailAddress,
						@Value_CompanyWebsiteUri = @Default_Value_CompanyWebsiteUri,
						@Value_CompanyRegistrationNumber = @Default_Value_CompanyRegistrationNumber,
						@Value_CompanyTaxNumber = @Default_Value_CompanyTaxNumber,
						@Value_CompanyFSPNumber = @Default_Value_CompanyFSPNumber

					if(upper(@CursorChannelEnvironmentKey) = 'TeamCity')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_ApiV2Uri = 'http://teamcity-apiv2.iplatform.co.za/',
								@Value_BrokerName = 'iPlatform',
								@Value_EchoTCFBypass = 0,
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_Environment = 'Dev',
								@Value_iAdminUri = 'http://admin.iplatform.co.za/',
								@Value_iGuideV2Uri = 'http://dev.iguide.api/',
								@Value_InsurerCode = 'IP',
								@Value_iPersonUri = 'http://dev.iperson.api',
								@Value_iRateUri = 'http://aig-staging-rate.iplatform.co.za/',
								@Value_QuoteAcceptanceEnvironment = 'TeamCity',
								@Value_WebBranding = 'IP',
								@Value_WorkFlowEnvironment = 'TeamCity'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1TestingUri = 'http://teamcity-api.iplatform.co.za/',
								@Value_ApiV1Uri = 'http://teamcity.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2TestingUri = 'http://teamcity-apiv2.iplatform.co.za/',
								@Value_ApiV2Uri = 'http://teamcity-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_DatabaseConnectionString = 'data source=(local);Persist Security Info=False;User ID=iBroker_user; password=iBroker_user; Initial Catalog=iBroker.Dev',
								@Value_Environment = 'TeamCity', --#{Octopus.Environment.Name}
								@Value_iGuideV2Uri = 'http://teamcity-guide.iplatform.co.za/',
								@Value_iPersonUri = 'http://teamcity-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_iRateUri = 'http://aig-staging-rate.iplatform.co.za/',
								@Value_LoadBalancerApiV1Uri = 'teamcity-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'teamcity-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'teamcity.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_Log4NetEnvironment = 'teamcity', --#{Octopus.Environment.Name}
								@Value_MachineSpecificBindingApiV1Uri = 'teamcity-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'teamcity-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'teamcity.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://teamcity.iplatform.co.za/Account/ResetPassword/', --http://#{Octopus.Environment.Name}.iplatform.co.za/Account/ResetPassword/
								@Value_QuoteAcceptanceEnvironment = 'TeamCity', --#{Octopus.Environment.Name}
								@Value_ServiceBusEnvironment = 'TeamCity', --#{Octopus.Environment.Name}
								@Value_SiteName = 'teamcity-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'aig',
								@Value_WorkFlowEnvironment = 'TeamCity' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'iPlatformDevTest')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1TestingUri = 'http://iplatformdevtest-api.iplatform.co.za/',
								@Value_ApiV1Uri = 'http://iplatformdevtest.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2TestingUri = 'http://iplatformdevtest-apiv2.iplatform.co.za/',
								@Value_ApiV2Uri = 'http://iplatformdevtest-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_DatabaseConnectionString = 'data source=db01;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=demo_iplatform',
								@Value_Environment = 'iPlatformDevTest', --#{Octopus.Environment.Name}
								@Value_iGuideV2Uri = 'http://iplatformdevtest-guide.iplatform.co.za/',
								@Value_iPersonUri = 'http://iplatformdevtest-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_LoadBalancerApiV1Uri = 'iplatformdevtest-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'iplatformdevtest-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'iplatformdevtest.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_Log4NetEnvironment = 'iPlatformDevTest', --#{Octopus.Environment.Name}
								@Value_MachineSpecificBindingApiV1Uri = 'iplatformdevtest-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'iplatformdevtest-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'iplatformdevtest.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://iplatformdevtest.iplatform.co.za/Account/ResetPassword/', --http://#{Octopus.Environment.Name}.iplatform.co.za/Account/ResetPassword/
								@Value_QuoteAcceptanceEnvironment = 'iPlatformDevTest', --#{Octopus.Environment.Name}
								@Value_ServiceBusEnvironment = 'iPlatformDevTest', --#{Octopus.Environment.Name}
								@Value_SiteName = 'iPlatformDevTest-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'ip',
								@Value_WorkFlowEnvironment = 'iPlatformDevTest' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Staging')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							
							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_ApiV2Uri = 'http://staging-apiv2.iplatform.co.za/',
								@Value_BrokerName = 'iPlatform',
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_Environment = 'Staging',
								@Value_iAdminUri = 'http://admin.iplatform.co.za/',
								@Value_iGuideV2Uri = 'http://guide-v2.iplatform.co.za/',
								@Value_InsurerCode = 'IP',
								@Value_iPersonUri = 'http://staging-personv2.iplatform.co.za/',
								@Value_iRateUri = 'http://staging-rate.iplatform.co.za/',
								@Value_PasswordResetUri = 'http://staging.iplatform.co.za/Account/ResetPassword/',
								@Value_QuoteAcceptanceEnvironment = 'Staging',
								@Value_WebBranding = 'IP',
								@Value_WorkFlowEnvironment = 'Staging'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://staging.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://staging-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_DatabaseConnectionString = 'data source=db01;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=staging_iplatform',
								@Value_Environment = 'Staging', --#{Octopus.Environment.Name}
								@Value_iGuideV2Uri = 'http://staging-iguide.iplatform.co.za/',
								@Value_iPersonUri = 'http://staging-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_iRateUri = 'http://staging-rate.iplatform.co.za', --http://#{Octopus.Environment.Name}-rate.iplatform.co.za
								@Value_LoadBalancerApiV1Uri = 'staging-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'staging-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'staging.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_Log4NetEnvironment = 'Staging', --#{Octopus.Environment.Name}
								@Value_MachineSpecificBindingApiV1Uri = 'staging-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'staging-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'staging.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://staging.iplatform.co.za/Account/ResetPassword/', --http://#{Octopus.Environment.Name}.iplatform.co.za/Account/ResetPassword/
								@Value_QuoteAcceptanceEnvironment = 'Staging', --#{Octopus.Environment.Name}
								@Value_ServiceBusEnvironment = 'Staging', --#{Octopus.Environment.Name}
								@Value_SiteName = 'staging-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'ip',
								@Value_WorkFlowEnvironment = 'Staging' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_ApiV2Uri = 'http://uat-apiv2.iplatform.co.za/',
								@Value_Environment = 'UAT',
								@Value_InsurerCode = 'IP',
								@Value_QuoteAcceptanceEnvironment = 'UAT',
								@Value_WorkFlowEnvironment = 'UAT'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://uat.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://uat-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_DatabaseConnectionString = 'data source=(local);Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=uat-ibroker',
								@Value_Environment = 'UAT', --#{Octopus.Environment.Name}
								@Value_iGuideV2Uri = 'http://uat-iguide.iplatform.co.za/',
								@Value_iPersonUri = 'http://uat-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_iRateUri = 'http://uat-rate.iplatform.co.za', --http://#{Octopus.Environment.Name}-rate.iplatform.co.za
								@Value_LoadBalancerApiV1Uri = 'uat-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'uat-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'uat.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_Log4NetEnvironment = 'UAT', --#{Octopus.Environment.Name}
								@Value_MachineSpecificBindingApiV1Uri = 'uat-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'uat-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'uat.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://staging.iplatform.co.za/Account/ResetPassword/', --http://#{Octopus.Environment.Name}.iplatform.co.za/Account/ResetPassword/
								@Value_QuoteAcceptanceEnvironment = 'UAT', --#{Octopus.Environment.Name}
								@Value_ServiceBusEnvironment = 'UAT', --#{Octopus.Environment.Name}
								@Value_SiteName = 'uat-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'ip',
								@Value_WorkFlowEnvironment = 'uat' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Demo')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_ApiV2Uri = 'http://demo-apiv2.iplatform.co.za/',
								@Value_BrokerName = 'iPlatform',
								@Value_EchoTCFBypass = 0,
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_Environment = 'UAT',
								@Value_iAdminUri = 'http://admin.iplatform.co.za/',
								@Value_iGuideV2Uri = 'http://guide-v2.iplatform.co.za/',
								@Value_InsurerCode = 'IP',
								@Value_iPersonUri = 'http://staging-personv2.iplatform.co.za/',
								@Value_iRateUri = 'http://demo-rate.iplatform.co.za/',
								@Value_PasswordResetUri = 'http://demo.iplatform.co.za/Account/ResetPassword/',
								@Value_QuoteAcceptanceEnvironment = 'Demo',
								@Value_WebBranding = 'IP',
								@Value_WorkFlowEnvironment = 'Demo'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://demo.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://demo-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_DatabaseConnectionString = 'data source=(local);Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=demoibroker2',
								@Value_Environment = 'UAT', --#{Octopus.Environment.Name}
								@Value_iGuideV2Uri = 'http://demo-iguide.iplatform.co.za/',
								@Value_iPersonUri = 'http://demo-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_iRateUri = 'http://demo-rate.iplatform.co.za', --http://#{Octopus.Environment.Name}-rate.iplatform.co.za
								@Value_LoadBalancerApiV1Uri = 'demo-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'demo-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'demo.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_Log4NetEnvironment = 'Demo', --#{Octopus.Environment.Name}
								@Value_MachineSpecificBindingApiV1Uri = 'demo-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'demo-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'demo.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://staging.iplatform.co.za/Account/ResetPassword/', --http://#{Octopus.Environment.Name}.iplatform.co.za/Account/ResetPassword/
								@Value_QuoteAcceptanceEnvironment = 'Demo', --#{Octopus.Environment.Name}
								@Value_ServiceBusEnvironment = 'Demo', --#{Octopus.Environment.Name}
								@Value_SiteName = 'demo-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'ip',
								@Value_WorkFlowEnvironment = 'Demo' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is configured on Octopus but is used by Channel Code "BOWER"...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Oaksure')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_BrokerName = 'Oaksure',
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_WebBranding = 'IP'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://oaksure.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://oaksure-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_DatabaseConnectionString = 'data source=db02;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=oaksure_iplatform',
								@Value_Environment = 'Oaksure', --#{Octopus.Environment.Name}
								@Value_iPersonUri = 'http://oaksure-personv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-personv2.iplatform.co.za
								@Value_iRateUri = 'http://oaksure-rate.iplatform.co.za', --http://#{Octopus.Environment.Name}-rate.iplatform.co.za
								@Value_LoadBalancerApiV1Uri = 'oaksure-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'oaksure-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'oaksure.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_Log4NetEnvironment = 'Oaksure', --#{Octopus.Environment.Name}
								@Value_MachineSpecificBindingApiV1Uri = 'oaksure-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'oaksure-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'oaksure.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_PasswordResetUri = 'http://staging.iplatform.co.za/Account/ResetPassword/', --http://#{Octopus.Environment.Name}.iplatform.co.za/Account/ResetPassword/
								@Value_QuoteAcceptanceEnvironment = 'Oaksure', --#{Octopus.Environment.Name}
								@Value_ServiceBusEnvironment = 'Oaksure', --#{Octopus.Environment.Name}
								@Value_SiteName = 'oaksure-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'ip',
								@Value_WorkFlowEnvironment = 'Oaksure' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
				end
			else if(upper(@CursorChannelCode) = 'KP')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = 'King Price Insurance Company Ltd.',
						@Value_CompanyPhysicalAddress = 'Menlyn Corporate Park, Block A, 3rd Floor, Corner of Garsfontein Road & Corobay Avenue, Waterkloof Glen X11, Pretoria, 0181',
						@Value_CompanyPostalAddress = 'PO Box 284, Menlyn, 0063',
						@Value_CompanyWorkNo = '0860 00 55 00',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = 'BrokerCC@kingprice.co.za',
						@Value_CompanyWebsiteUri = 'http://www.kingprice.co.za',
						@Value_CompanyRegistrationNumber = '2009/012496/06',
						@Value_CompanyTaxNumber = '4710259724',
						@Value_CompanyFSPNumber = '43862'

					if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_ApiV2Uri = 'http://kingprice-uat-apiv2.iplatform.co.za/',
								@Value_BrokerName = 'KingPrice',
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_Environment = 'UAT',
								@Value_iAdminUri = 'http://admin.iplatform.co.za/',
								@Value_iGuideV2Uri = 'http://guide-v2.iplatform.co.za/',
								@Value_InsurerCode = 'KP',
								@Value_iPersonUri = 'http://staging-personv2.iplatform.co.za/',
								@Value_iRateUri = 'http://staging-rate.iplatform.co.za/',
								@Value_PasswordResetUri = 'http://kingprice-uat.iplatform.co.za/Account/ResetPassword/',
								@Value_WebBranding = 'KP',
								@Value_EnableUnderwritingRejectionNotice = 0,
								@Value_QuoteAcceptanceEnvironment = 'KP-UAT',
								@Value_WorkFlowEnvironment = 'KP-UAT'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://kingprice-uat.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://kingprice-uat-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_BrokerName = 'KP',
								@Value_DatabaseConnectionString = 'data source=db01;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iplatform_kingprice_uat',
								@Value_iGuideV1Uri = 'http://staging-iguide.iplatform.co.za/api',
								@Value_InsurerCode = 'KP',
								@Value_iRateUri = 'http://kp-uat-rate.iplatform.co.za/',
								@Value_LoadBalancerApiV1Uri = 'kingprice-uat-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'kingprice-uat-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'kingprice-uat.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_MachineSpecificBindingApiV1Uri = 'kingprice-uat-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'kingprice-uat-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'kingprice-uat.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_SiteName = 'kingprice-uat-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'kp',
								@Value_WorkFlowEnvironment = 'kingprice-uat' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_ApiV2Uri = 'http://kingprice-apiv2.iplatform.co.za/',
								@Value_BrokerName = 'KingPrice',
								@Value_EchoTCFBypass = 0,
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_Environment = 'Live',
								@Value_iAdminUri = 'http://admin.iplatform.co.za/',
								@Value_iGuideV2Uri = 'http://guide-v2.iplatform.co.za/',
								@Value_InsurerCode = 'KP',
								@Value_iPersonUri = 'http://staging-personv2.iplatform.co.za/',
								@Value_iRateUri = 'http://staging-rate.iplatform.co.za/',
								@Value_PasswordResetUri = 'http://kingprice.iplatform.co.za/Account/ResetPassword/',
								@Value_WebBranding = 'KP',
								@Value_EnableUnderwritingRejectionNotice = 0,
								@Value_QuoteAcceptanceEnvironment = 'KP',
								@Value_WorkFlowEnvironment = 'KP'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://kingprice.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://kingprice-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_BrokerName = 'KP',
								@Value_DatabaseConnectionString = 'data source=db01;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iplatform_kingprice_uat',
								@Value_iGuideV1Uri = 'http://staging-iguide.iplatform.co.za/api',
								@Value_InsurerCode = 'KP',
								@Value_iRateUri = 'http://kp-rate.iplatform.co.za/',
								@Value_LoadBalancerApiV1Uri = 'kingprice-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'kingprice-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'kingprice.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_MachineSpecificBindingApiV1Uri = 'kingprice-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'kingprice-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'kingprice.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_SiteName = 'kingprice-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'kp',
								@Value_WorkFlowEnvironment = 'kingprice' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
				end
			else if(upper(@CursorChannelCode) = 'MIWAY')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = '',
						@Value_CompanyPhysicalAddress = '',
						@Value_CompanyPostalAddress = '',
						@Value_CompanyWorkNo = '',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = '',
						@Value_CompanyWebsiteUri = '',
						@Value_CompanyRegistrationNumber = '',
						@Value_CompanyTaxNumber = '',
						@Value_CompanyFSPNumber = ''

					if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
				end
			else if(upper(@CursorChannelCode) = 'PSG')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = 'PSG Wealth Financial Planning',
						@Value_CompanyPhysicalAddress = 'Block B, Corporate 66 Office Park, 269 Vonwillich Avenue, Centurion, 0175',
						@Value_CompanyPostalAddress = 'Block B, Corporate 66 Office Park, 269 Vonwillich Avenue, Centurion, 0175',
						@Value_CompanyWorkNo = '0860 774 566',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = 'ipquotes@psg.co.za',
						@Value_CompanyWebsiteUri = 'http://www.psg.co.za',
						@Value_CompanyRegistrationNumber = '1999/006725/07',
						@Value_CompanyTaxNumber = '4230182216',
						@Value_CompanyFSPNumber = '728'

					if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_ApiV2Uri = 'http://psg-uat-apiv2.iplatform.co.za/',
								@Value_BrokerName = 'PSG',
								@Value_EchoTCFBypass = 0,
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_Environment = 'UAT',
								@Value_iAdminUri = 'http://admin.iplatform.co.za/',
								@Value_iGuideV2Uri = 'http://staging-iguide.iplatform.co.za/',
								@Value_InsurerCode = 'PSG',
								@Value_iPersonUri = 'http://staging-personv2.iplatform.co.za/',
								@Value_iRateUri = 'http://psg-uat-rate.iplatform.co.za/',
								@Value_PasswordResetUri = 'http://psg-uat.iplatform.co.za/Account/ResetPassword/',
								@Value_WebBranding = 'PSG',
								@Value_EnableUnderwritingRejectionNotice = 0,
								@Value_QuoteAcceptanceEnvironment = 'PSG-UAT',
								@Value_WorkFlowEnvironment = 'PSG-UAT'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://psg-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://psg-uat-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_BrokerName = 'PSG',
								@Value_DatabaseConnectionString = 'data source=db01;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iplatform_psguat',
								@Value_IDSUri = 'http://TEST/cimsconnect/cimsconnect.svc',
								@Value_iGuideV1Uri = 'http://staging-iguide.iplatform.co.za/api',
								@Value_InsurerCode = 'PSG',
								@Value_iRateUri = 'http://psg-uat-rate.iplatform.co.za/',
								@Value_LoadBalancerApiV1Uri = 'psg-uat-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'psg-uat-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'psg-uat.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_MachineSpecificBindingApiV1Uri = 'psg-uat-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'psg-uat-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'psg-uat.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_SiteName = 'psg-uat-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'PSG',
								@Value_WorkFlowEnvironment = 'psg-uat' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_ApiV2Uri = 'http://psg-apiv2.iplatform.co.za/',
								@Value_BrokerName = 'PSG',
								@Value_EchoTCFBypass = 0,
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_Environment = 'Live',
								@Value_iAdminUri = 'http://admin.iplatform.co.za/',
								@Value_iGuideV2Uri = 'http://guide-v2.iplatform.co.za/',
								@Value_InsurerCode = 'PSG',
								@Value_iPersonUri = 'http://staging-personv2.iplatform.co.za/',
								@Value_iRateUri = 'http://psg-rate.iplatform.co.za/',
								@Value_PasswordResetUri = 'http://psg.iplatform.co.za/Account/ResetPassword/',
								@Value_WebBranding = 'PSG',
								@Value_EnableUnderwritingRejectionNotice = 1,
								@Value_QuoteAcceptanceEnvironment = 'PSG',
								@Value_WorkFlowEnvironment = 'PSG'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://psg-api.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://psg-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_BrokerName = 'PSG',
								@Value_DatabaseConnectionString = 'data source=db02;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iplatform_psglive',
								@Value_IDSUri = 'http://TEST/cimsconnect/cimsconnect.svc',
								@Value_iGuideV1Uri = 'http://staging-iguide.iplatform.co.za/api',
								@Value_InsurerCode = 'PSG',
								@Value_LoadBalancerApiV1Uri = 'psg-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'psg-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'psg.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_MachineSpecificBindingApiV1Uri = 'psg-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'psg-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'psg.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_SiteName = 'psg-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'PSG',
								@Value_WorkFlowEnvironment = 'psg' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
				end
			else if(upper(@CursorChannelCode) = 'PSGCC')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = '',
						@Value_CompanyPhysicalAddress = '',
						@Value_CompanyPostalAddress = '',
						@Value_CompanyWorkNo = '',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = '',
						@Value_CompanyWebsiteUri = '',
						@Value_CompanyRegistrationNumber = '',
						@Value_CompanyTaxNumber = '',
						@Value_CompanyFSPNumber = ''

					if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
				end
			else if(upper(@CursorChannelCode) = 'TELESURE')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = '',
						@Value_CompanyPhysicalAddress = '',
						@Value_CompanyPostalAddress = '',
						@Value_CompanyWorkNo = '',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = '',
						@Value_CompanyWebsiteUri = '',
						@Value_CompanyRegistrationNumber = '',
						@Value_CompanyTaxNumber = '',
						@Value_CompanyFSPNumber = ''

					if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_ApiV2Uri = 'http://telesure-uat-apiv2.iplatform.co.za/',
								@Value_BrokerName = 'Telesure',
								@Value_EchoTCFBypass = 0,
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_Environment = 'UAT',
								@Value_iAdminUri = 'http://admin.iplatform.co.za/',
								@Value_iGuideV2Uri = 'http://staging-iguide.iplatform.co.za/',
								@Value_InsurerCode = 'Telesure',
								@Value_iPersonUri = 'http://staging-personv2.iplatform.co.za/',
								@Value_iRateUri = 'http://telesure-uat-rate.iplatform.co.za/',
								@Value_PasswordResetUri = 'http://telesure-uat.iplatform.co.za/Account/ResetPassword/',
								@Value_WebBranding = 'Telesure',
								@Value_EnableUnderwritingRejectionNotice = 1,
								@Value_QuoteAcceptanceEnvironment = 'TELESURE-UAT',
								@Value_WorkFlowEnvironment = 'TELESURE-UAT'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://telesure-api.iplatform.co.za/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://telesure-apiv2.iplatform.co.za/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_BrokerName = 'Telesure',
								@Value_DatabaseConnectionString = 'data source=db01;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iplatform_telesureuat',
								@Value_IDSUri = 'http://TEST/cimsconnect/cimsconnect.svc',
								@Value_LoadBalancerApiV1Uri = 'telesure-api.iplatform.co.za', --#{Octopus.Environment.Name}-api.iplatform.co.za
								@Value_LoadBalancerApiV2Uri = 'telesure-apiv2.iplatform.co.za', --#{Octopus.Environment.Name}-apiv2.iplatform.co.za
								@Value_LoadBalancerWebUri = 'telesure.iplatform.co.za', --#{Octopus.Environment.Name}.iplatform.co.za
								@Value_MachineSpecificBindingApiV1Uri = 'telesure-api.iplatform.co.za', --#{Octopus.Machine.Name}-api.iplatform.co.za
								@Value_MachineSpecificBindingApiV2Uri = 'telesure-apiv2.iplatform.co.za', --#{Octopus.Machine.Name}-apiv2.iplatform.co.za
								@Value_MachineSpecificBindingWebUri = 'telesure.iplatform.co.za', --#{Octopus.Machine.Name}.iplatform.co.za
								@Value_SiteName = 'telesure-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'Telesure',
								@Value_WorkFlowEnvironment = 'telesure' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
				end
			else if(upper(@CursorChannelCode) = 'UAG')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = '',
						@Value_CompanyPhysicalAddress = '',
						@Value_CompanyPostalAddress = '',
						@Value_CompanyWorkNo = '',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = '',
						@Value_CompanyWebsiteUri = '',
						@Value_CompanyRegistrationNumber = '',
						@Value_CompanyTaxNumber = '',
						@Value_CompanyFSPNumber = ''

					if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
				end
			else if(upper(@CursorChannelCode) = 'UAP')
				begin
					--OVERRIDE COMPANY INFORMATION REGARDLESS OF ENVIRONMENT
					select
						@Value_CompanyName = 'UAP Insurance Company Limited',
						@Value_CompanyPhysicalAddress = 'Bishop Gardens Towers, Bishop Ropad, Nairobi',
						@Value_CompanyPostalAddress = 'P.O. Box 43013-00100, Nairobi, Kenya',
						@Value_CompanyWorkNo = '+254 20 285 0000',
						@Value_CompanyFaxNo = '',
						@Value_CompanyEmailAddress = 'easydirect@uap-group.com',
						@Value_CompanyWebsiteUri = 'http://www.uap-group.com | http://www.easydirect.co.ke',
						@Value_CompanyRegistrationNumber = 'C.17074',
						@Value_CompanyTaxNumber = 'P000609359J',
						@Value_CompanyFSPNumber = 'IRA01/030/0'

					if(upper(@CursorChannelEnvironmentKey) = 'Staging')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Staging-Internal')
						begin
							print @CursorChannelDetail + ' overriding settings...'

							--FOUND IN APPSETTINGS.CONFIG (Web & Api)
							select
								@Value_ApiV2Uri = 'http://uap-staging-internal-apiv2.iplatform.co.ke/',
								@Value_BrokerName = 'Telesure',
								@Value_EnableBankAccountValidation = 0,
								@Value_EnableClientConsole = 0,
								@Value_EnableExternalReferenceValidation = 0,
								@Value_EnableSignFlowOnQuoteEMail = 0,
								@Value_EnableVehicleLookup = 1,
								@Value_Environment = 'Staging',
								@Value_iAdminUri = 'http://admin.iplatform.co.za/',
								@Value_iGuideV2Uri = 'http://uap-staging-internal.iguide.iplatform.co.za/',
								@Value_InsurerCode = 'UAP',
								@Value_iPersonUri = 'http://uap-staging-internal-ipersonv2.iplatform.co.za/',
								@Value_iRateUri = 'http://uap-staging-internal-rate.iplatform.co.ke/',
								@Value_PasswordResetUri = 'http://stagingke.uap-group.com/Account/ResetPassword/',
								@Value_WebBranding = 'UAP',
								@Value_EnableUnderwritingRejectionNotice = 0,
								@Value_QuoteAcceptanceEnvironment = 'UAP-Staging-Internal',
								@Value_WorkFlowEnvironment = 'UAP-Staging-Internal'

							--FOUND IN OCTOPUS VARIABLES (OVERRIDES APP SETTINGS)
							select
								@Value_ApiV1Uri = 'http://uap-staging-internal-api.iplatform.co.ke/', --http://#{Octopus.Environment.Name}-api.iplatform.co.za/
								@Value_ApiV2Uri = 'http://uap-staging-internal-apiv2.iplatform.co.ke/', --http://#{Octopus.Environment.Name}-apiv2.iplatform.co.za/
								@Value_BrokerName = 'UAP',
								@Value_CountryCode = 'KE',
								@Value_LanguageCode = 'sw',
								@Value_DatabaseConnectionString = 'data source=195.202.85.106;Persist Security Info=False;User ID=iBroker_user; password=axt44XMmkfqA; Initial Catalog=iPlatform',
								@Value_IDSUri = 'http://195.202.66.179/STPWS/Command.svc',
								@Value_iGuideV1Uri = 'http://uap-staging-internal.iguide.iplatform.co.za/api/', --http://#{Octopus.Environment.Name}.iguide.iplatform.co.za/api/
								@Value_iGuideV2Uri = 'http://guide-v2.iplatform.co.za/',
								@Value_iGuideEMail = 'iguide-root@iplatform.co.za',
								@Value_KenyaAutoKey = 'iguide-root@iplatform.co.za',
								@Value_KenyaAutoSecret = '6678979C75A70BBA85762F4D488AFB6F',
								@Value_iPersonDefaultSource = 'IPRS',
								@Value_iPersonUri = 'http://uap-staging-internal-ipersonv2.iplatform.co.za', --http://#{Octopus.Environment.Name}-ipersonv2.iplatform.co.za
								@Value_iRateUri = 'http://uap-staging-internal-rate.iplatform.co.ke', --http://#{Octopus.Environment.Name}-rate.iplatform.co.ke
								@Value_LoadBalancerApiV1Uri = 'uap-staging-internal-api.iplatform.co.ke', --#{Octopus.Environment.Name}-api.iplatform.co.ke
								@Value_LoadBalancerApiV2Uri = 'uap-staging-internal-apiv2.iplatform.co.ke', --#{Octopus.Environment.Name}-apiv2.iplatform.co.ke
								@Value_LoadBalancerWebUri = 'stagingke.uap-group.com',
								@Value_MachineSpecificBindingApiV1Uri = 'uap-staging-internal-api.iplatform.co.ke', --#{Octopus.Machine.Name}-api.iplatform.co.ke
								@Value_MachineSpecificBindingApiV2Uri = 'uap-staging-internal-apiv2.iplatform.co.ke', --#{Octopus.Machine.Name}-apiv2.iplatform.co.ke
								@Value_MachineSpecificBindingWebUri = 'uap-staging-internal.iplatform.co.ke', --#{Octopus.Machine.Name}.iplatform.co.ke
								@Value_SiteName = 'uap-staging-internal-iplatform', --#{Octopus.Environment.Name}-iplatform
								@Value_WebBranding = 'UAP',
								@Value_WorkFlowEnvironment = 'uap-staging-internal' --#{Octopus.Environment.Name}

							print @CursorChannelDetail + ' settings overridden...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'UAT')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Production')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
					else if(upper(@CursorChannelEnvironmentKey) = 'Live')
						begin
							print @CursorChannelDetail + ' overriding settings...'
							print @CursorChannelDetail + ' environment is not configured on Octopus...'
						end
				end

			--INSERT STATEMENTS, CHANNEL COMPANY INFORMATION
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_CompanyName, @Description_CompanyName, @Value_CompanyName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_CompanyPhysicalAddress, @Description_CompanyPhysicalAddress, @Value_CompanyPhysicalAddress, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_CompanyPostalAddress, @Description_CompanyPostalAddress, @Value_CompanyPostalAddress, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_CompanyWorkNo, @Description_CompanyWorkNo, @Value_CompanyWorkNo, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_CompanyFaxNo, @Description_CompanyFaxNo, @Value_CompanyFaxNo, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_CompanyEmailAddress, @Description_CompanyEmailAddress, @Value_CompanyEmailAddress, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_CompanyWebsiteUri, @Description_CompanyWebsiteUri, @Value_CompanyWebsiteUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_CompanyRegistrationNumber, @Description_CompanyRegistrationNumber, @Value_CompanyRegistrationNumber, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_CompanyTaxNumber, @Description_CompanyTaxNumber, @Value_CompanyTaxNumber, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_CompanyFSPNumber, @Description_CompanyFSPNumber, @Value_CompanyFSPNumber, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)

			--INSERT STATEMENTS, MAINTAIN ALPHABETICAL ORDER
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_ApiV1TestingUri, @Description_ApiV1TestingUri, @Value_ApiV1TestingUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_ApiV1Uri, @Description_ApiV1Uri, @Value_ApiV1Uri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_ApiV2TestingUri, @Description_ApiV2TestingUri, @Value_ApiV2TestingUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_ApiV2Uri, @Description_ApiV2Uri, @Value_ApiV2Uri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_AuthenticationSuccessEvent, @Description_AuthenticationSuccessEvent, @Value_AuthenticationSuccessEvent, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_BrokerName, @Description_BrokerName, @Value_BrokerName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_BrowserFactory, @Description_BrowserFactory, @Value_BrowserFactory, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_ClientCode, @Description_ClientCode, @Value_ClientCode, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_ClientName, @Description_ClientName, @Value_ClientName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_ConsumerViewPassword, @Description_ConsumerViewPassword, @Value_ConsumerViewPassword, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_ConsumerViewUserName, @Description_ConsumerViewUserName, @Value_ConsumerViewUserName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_CountryCode, @Description_CountryCode, @Value_CountryCode, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_CurrencySymbol, @Description_CurrencySymbol, @Value_CurrencySymbol, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_DatabaseConnectionString, @Description_DatabaseConnectionString, @Value_DatabaseConnectionString, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_DefaultMapLatitude, @Description_DefaultMapLatitude, @Value_DefaultMapLatitude, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_DefaultMapLongitude, @Description_DefaultMapLongitude, @Value_DefaultMapLongitude, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EchoTCFApiKey, @Description_EchoTCFApiKey, @Value_EchoTCFApiKey, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EchoTCFBypass, @Description_EchoTCFBypass, @Value_EchoTCFBypass, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EchoTCFToken, @Description_EchoTCFToken, @Value_EchoTCFToken, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EchoTCFUri, @Description_EchoTCFUri, @Value_EchoTCFUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EchoTCFUserName, @Description_EchoTCFUserName, @Value_EchoTCFUserName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EmailTemplateSource, @Description_EmailTemplateSource, @Value_EmailTemplateSource, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableAddressEditorAddressType, @Description_EnableAddressEditorAddressType, @Value_EnableAddressEditorAddressType, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableAddressEditorMap, @Description_EnableAddressEditorMap, @Value_EnableAddressEditorMap, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableAgentLeadMosaicInfo, @Description_EnableAgentLeadMosaicInfo, @Value_EnableAgentLeadMosaicInfo, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableAgentLeadWealthIndex, @Description_EnableAgentLeadWealthIndex, @Value_EnableAgentLeadWealthIndex, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableAnnualValues, @Description_EnableAnnualValues, @Value_EnableAnnualValues, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableBankAccountValidation, @Description_EnableBankAccountValidation, @Value_EnableBankAccountValidation, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableBundling, @Description_EnableBundling, @Value_EnableBundling, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableClientConsole, @Description_EnableClientConsole, @Value_EnableClientConsole, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableDatabaseUpgrade, @Description_EnableDatabaseUpgrade, @Value_EnableDatabaseUpgrade, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableDemoMode, @Description_EnableDemoMode, @Value_EnableDemoMode, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableExternalReferenceValidation, @Description_EnableExternalReferenceValidation, @Value_EnableExternalReferenceValidation, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableInsurerTemplate, @Description_EnableInsurerTemplate, @Value_EnableInsurerTemplate, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableLeadIdentityNumberValidation, @Description_EnableLeadIdentityNumberValidation, @Value_EnableLeadIdentityNumberValidation, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableMultipleQuoteAcceptance, @Description_EnableMultipleQuoteAcceptance, @Value_EnableMultipleQuoteAcceptance, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnablePerformanceCounter , @Description_EnablePerformanceCounter, @Value_EnablePerformanceCounter, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnablePostalCodeInAddressEditor, @Description_EnablePostalCodeInAddressEditor, @Value_EnablePostalCodeInAddressEditor, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableQuoteToPolicyInfo, @Description_EnableQuoteToPolicyInfo, @Value_EnableQuoteToPolicyInfo, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableRate, @Description_EnableRate, @Value_EnableRate, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableSASRIA, @Description_EnableSASRIA, @Value_EnableSASRIA, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableSecondLevelUnderwriting, @Description_EnableSecondLevelUnderwriting, @Value_EnableSecondLevelUnderwriting, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableSignFlowOnQuoteEMail, @Description_EnableSignFlowOnQuoteEMail, @Value_EnableSignFlowOnQuoteEMail, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableUnderwritingRejectionNotice, @Description_ClientName, @Value_ClientName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_EnableVehicleLookup, @Description_EnableVehicleLookup, @Value_EnableVehicleLookup, 0, @CursorChannelEnvironmentId, 2, 1, 8, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_Environment, @Description_Environment, @Value_Environment, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_iAdminUri, @Description_iAdminUri, @Value_iAdminUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_IDSUri, @Description_IDSUri, @Value_IDSUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_iGuideApiKey, @Description_iGuideApiKey, @Value_iGuideApiKey, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_iGuideEMail, @Description_iGuideEMail, @Value_iGuideEMail, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_iGuideV1Uri, @Description_iGuideV1Uri, @Value_iGuideV1Uri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_iGuideV2Uri, @Description_iGuideV2Uri, @Value_iGuideV2Uri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_InsurerCode, @Description_InsurerCode, @Value_InsurerCode, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_iPersonApiKey, @Description_iPersonApiKey, @Value_iPersonApiKey, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_iPersonDefaultSource, @Description_iPersonDefaultSource, @Value_iPersonDefaultSource, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_iPersonEMail, @Description_iPersonEMail, @Value_iPersonEMail, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_iPersonEnableRetry, @Description_iPersonEnableRetry, @Value_iPersonEnableRetry, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_iPersonUri, @Description_iPersonUri, @Value_iPersonUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_iPlatformEnvironment, @Description_iPlatformEnvironment, @Value_iPlatformEnvironment, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_iRateUri, @Description_iRateUri, @Value_iRateUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_KenyaAutoKey, @Description_KenyaAutoKey, @Value_KenyaAutoKey, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_KenyaAutoSecret, @Description_KenyaAutoSecret, @Value_KenyaAutoSecret, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_LanguageCode, @Description_LanguageCode, @Value_LanguageCode, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_LightStoneContractId, @Description_LightStoneContractId, @Value_LightStoneContractId, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_LightStoneCustomerId, @Description_LightStoneCustomerId, @Value_LightStoneCustomerId, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_LightStonePackageId, @Description_LightStonePackageId, @Value_LightStonePackageId, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_LightStonePassword, @Description_LightStonePassword, @Value_LightStonePassword, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_LightStonePropertyUserId, @Description_LightStonePropertyUserId, @Value_LightStonePropertyUserId, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_LightStoneUserId, @Description_LightStoneUserId, @Value_LightStoneUserId, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_LightStoneUserName, @Description_LightStoneUserName, @Value_LightStoneUserName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_LoadBalancerApiV1Uri, @Description_LoadBalancerApiV1Uri, @Value_LoadBalancerApiV1Uri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_LoadBalancerApiV2Uri, @Description_LoadBalancerApiV2Uri, @Value_LoadBalancerApiV2Uri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_LoadBalancerWebUri, @Description_LoadBalancerWebUri, @Value_LoadBalancerWebUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_Log4NetEnvironment, @Description_Log4NetEnvironment, @Value_Log4NetEnvironment, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_MachineSpecificBindingApiV1Uri, @Description_MachineSpecificBindingApiV1Uri, @Value_MachineSpecificBindingApiV1Uri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_MachineSpecificBindingApiV2Uri, @Description_MachineSpecificBindingApiV2Uri, @Value_MachineSpecificBindingApiV2Uri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_MachineSpecificBindingWebUri, @Description_MachineSpecificBindingWebUri, @Value_MachineSpecificBindingWebUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_MigrationAssemblyApiV1Path, @Description_MigrationAssemblyApiV1Path, @Value_MigrationAssemblyApiV1Path, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_MigrationAssemblyApiV2Path, @Description_MigrationAssemblyApiV2Path, @Value_MigrationAssemblyApiV2Path, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_MigrationAssemblyExecutablePath, @Description_MigrationAssemblyExecutablePath, @Value_MigrationAssemblyExecutablePath, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_MigrationToolExecutablePath, @Description_MigrationToolExecutablePath, @Value_MigrationToolExecutablePath, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_NullResultDefault, @Description_NullResultDefault, @Value_NullResultDefault, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_PasswordResetUri, @Description_PasswordResetUri, @Value_PasswordResetUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_QuoteAcceptanceEnvironment, @Description_QuoteAcceptanceEnvironment, @Value_QuoteAcceptanceEnvironment, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_QuoteAcceptanceMessage, @Description_QuoteAcceptanceMessage, @Value_QuoteAcceptanceMessage, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_QuoteToPolicyDetailsHeader, @Description_QuoteToPolicyDetailsHeader, @Value_QuoteToPolicyDetailsHeader, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_ReportSource, @Description_ReportSource, @Value_ReportSource, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_ReportStorage, @Description_ReportStorage, @Value_ReportStorage, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_RootPassword, @Description_RootPassword, @Value_RootPassword, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_RootUserName, @Description_RootUserName, @Value_RootUserName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SalesForceConsumerKey, @Description_SalesForceConsumerKey, @Value_SalesForceConsumerKey, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SalesForceConsumerSecret, @Description_SalesForceConsumerSecret, @Value_SalesForceConsumerSecret, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SalesForceDelay, @Description_SalesForceDelay, @Value_SalesForceDelay, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SalesForceLeadSource, @Description_SalesForceLeadSource, @Value_SalesForceLeadSource, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SalesForcePassword, @Description_SalesForcePassword, @Value_SalesForcePassword, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SalesForceSecurityKey, @Description_SalesForceSecurityKey, @Value_SalesForceSecurityKey, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SalesForceStatus, @Description_SalesForceStatus, @Value_SalesForceStatus, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SalesForceUri, @Description_SalesForceUri, @Value_SalesForceUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SalesForceUserName, @Description_SalesForceUserName, @Value_SalesForceUserName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SalesForceWebsiteSubmissionPath, @Description_SalesForceWebsiteSubmissionPath, @Value_SalesForceWebsiteSubmissionPath, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SecretSecurityKey, @Description_SecretSecurityKey, @Value_SecretSecurityKey, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SelenoURL, @Description_SelenoURL, @Value_SelenoURL, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_ServiceBusEnvironment, @Description_ServiceBusEnvironment, @Value_ServiceBusEnvironment, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SignFlowPassword, @Description_SignFlowPassword, @Value_SignFlowPassword, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SignFlowSecretKey, @Description_SignFlowSecretKey, @Value_SignFlowSecretKey, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SignFlowUri, @Description_SignFlowUri, @Value_SignFlowUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SignFlowUserName, @Description_SignFlowUserName, @Value_SignFlowUserName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SiteName, @Description_SiteName, @Value_SiteName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SSLIPAddress, @Description_SSLIPAddress, @Value_SSLIPAddress, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SSLThumbPrint, @Description_SSLThumbPrint, @Value_SSLThumbPrint, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_SubscriberServiceName, @Description_SubscriberServiceName, @Value_SubscriberServiceName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_TransUnionSecurityCode, @Description_TransUnionSecurityCode, @Value_TransUnionSecurityCode, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_TransUnionSubscriberCode, @Description_TransUnionSubscriberCode, @Value_TransUnionSubscriberCode, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WarningLabel, @Description_WarningLabel, @Value_WarningLabel, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WebBranding, @Description_WebBranding, @Value_WebBranding, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WebUri, @Description_WebUri, @Value_WebUri, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WidgetFeeDiscount, @Description_WidgetFeeDiscount, @Value_WidgetFeeDiscount, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowEngineServiceDescription, @Description_WorkFlowEngineServiceDescription, @Value_WorkFlowEngineServiceDescription, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowEngineServiceDisplayName, @Description_WorkFlowEngineServiceDisplayName, @Value_WorkFlowEngineServiceDisplayName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowEngineServiceExchangeName, @Description_WorkFlowEngineServiceExchangeName, @Value_WorkFlowEngineServiceExchangeName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowEngineServiceExchangeNameError, @Description_WorkFlowEngineServiceExchangeNameError, @Value_WorkFlowEngineServiceExchangeNameError, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowEngineServiceName, @Description_WorkFlowEngineServiceName, @Value_WorkFlowEngineServiceName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowEngineServiceQueueNameError, @Description_WorkFlowEngineServiceQueueNameError, @Value_WorkFlowEngineServiceQueueNameError, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowEnvironment, @Description_WorkFlowEnvironment, @Value_WorkFlowEnvironment, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowRouterServiceDescription, @Description_WorkFlowRouterServiceDescription, @Value_WorkFlowRouterServiceDescription, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowRouterServiceDisplayName, @Description_WorkFlowRouterServiceDisplayName, @Value_WorkFlowRouterServiceDisplayName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowRouterServiceExchangeName, @Description_WorkFlowRouterServiceExchangeName, @Value_WorkFlowRouterServiceExchangeName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowRouterServiceExchangeNameError, @Description_WorkFlowRouterServiceExchangeNameError, @Value_WorkFlowRouterServiceExchangeNameError, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowRouterServiceName, @Description_WorkFlowRouterServiceName, @Value_WorkFlowRouterServiceName, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)
			insert into ChannelSetting (ChannelSettingName, ChannelSettingDescription, ChannelSettingValue, IsDeleted, ChannelEnvironmentId, ChannelSettingOwnerId, ChannelSettingTypeId, ChannelSettingDataTypeId, ChannelId) values (@Name_WorkFlowRouterServiceQueueNameError, @Description_WorkFlowRouterServiceQueueNameError, @Value_WorkFlowRouterServiceQueueNameError, 0, @CursorChannelEnvironmentId, 2, 1, 2, @CursorChannelId)

			print @CursorChannelDetail + ' environment settings setup complete.'
			print ''

			fetch next from @ChannelCursor into @CursorChannelId, @CursorChannelCode, @CursorChannelName, @CursorChannelEnvironmentId, @CursorChannelEnvironmentKey
		end

close @ChannelCursor
deallocate @ChannelCursor

print 'Done...'
go

--RE-ENABLE QUERY COUNT
set nocount off
go