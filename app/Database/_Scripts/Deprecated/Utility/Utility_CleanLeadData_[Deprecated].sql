-------------------------------
--Reset iPlatform Database Data
-------------------------------

----------------------------------------------------------------------------------------------------------------------------------------
--WARNING! Before running this script, be sure to have a backup of the database you are executing it on, these actions are irreversible!
----------------------------------------------------------------------------------------------------------------------------------------
--WARNING! Before running this script, be sure to have a backup of the database you are executing it on, these actions are irreversible!
----------------------------------------------------------------------------------------------------------------------------------------
--WARNING! Before running this script, be sure to have a backup of the database you are executing it on, these actions are irreversible!
----------------------------------------------------------------------------------------------------------------------------------------

set nocount on

print 'Resetting data!'
print ''


-------------------------
--Delete data from tables
-------------------------

print 'Deleting data from tables...'

delete from soldleadactivity
delete from quotedleadactivity
delete from sentforunderwritingleadactivity
delete from quoteintenttobuyleadactivity
delete from rejectedatunderwritingleadactivity
delete from surveyrespondedleadactivity
delete from claimsleadactivity
delete from createleadactivity
delete from deadleadactivity
delete from delayleadactivity
delete from importedleadactivity
delete from lossleadactivity
delete from policyleadactivity
delete from paymentdetails
delete from claimsitemquestionanswer
delete from claimsitem
delete from claimsheader
delete from policyaccident
delete from policyallrisk
delete from policycontent
delete from policycoverage
delete from policyfinance
delete from policyfuneral
delete from policybuilding
delete from policyliability
delete from policypersonalvehicle
delete from policyspecial
delete from policywatercraft
delete from policyitem
delete from policyheader
delete from quoteacceptedleadactivity
delete from quotedleadactivity
delete from quoteheaderstateentry
delete from quoteheaderstate
delete from secondlevelquestionsavedanswer
delete from quoteitem
delete from quotestateentry
delete from quotestate
delete from quoteuploadlog
delete from paymentdetails
delete from quote
delete from quoteheader
delete from proposalquestionanswer
delete from proposalleadactivity
delete from proposaldefinition
delete from proposalheader
delete from campaignleadbucketverbose
delete from campaignleadbucket
delete from leadactivity
delete FROM SalesDetails
delete FROM SalesStructure
delete FROM SalesFNI
delete from lead
delete from motorlosshistory
delete from homelosshistory
delete from losshistory
delete from leadquality
delete from individualuploaddetail
delete from individualuploadheader
delete from leadimport
delete from campaignagent
delete from bankdetails
delete from partycorrespondencepreference
delete from partycorrespondenceattachment
delete from partycorrespondence
delete from customersatisfactionsurveyanswers
delete from customersatisfactionsurvey
delete from relationship
delete from assetaddress
delete from [address]
delete from assetextras
delete from assetriskitem
delete from assetvehicle
delete from asset
delete from note
delete from audit
delete from ELMAH_Error
delete from CampaignManager


---------------------------
--Reset PK fields on tables
---------------------------

--print 'Resetting PK fields on tables...'
--print ''

--dbcc checkident('claimsleadactivity', reseed, 0)
--dbcc checkident('paymentdetails', reseed, 0)
--dbcc checkident('claimsitemquestionanswer', reseed, 0)
--dbcc checkident('claimsitem', reseed, 0)
--dbcc checkident('claimsheader', reseed, 0)
--dbcc checkident('policyfuneral', reseed, 0)
--dbcc checkident('policyitem', reseed, 0)
--dbcc checkident('policyheader', reseed, 0)
--dbcc checkident('quoteheaderstateentry', reseed, 0)
--dbcc checkident('quoteheaderstate', reseed, 0)
--dbcc checkident('secondlevelquestionsavedanswer', reseed, 0)
--dbcc checkident('quoteitem', reseed, 0)
--dbcc checkident('quotestateentry', reseed, 0)
--dbcc checkident('quotestate', reseed, 0)
--dbcc checkident('quoteuploadlog', reseed, 0)
--dbcc checkident('paymentdetails', reseed, 0)
--dbcc checkident('quote', reseed, 0)
--dbcc checkident('quoteheader', reseed, 0)
--dbcc checkident('proposalquestionanswer', reseed, 0)
--dbcc checkident('proposaldefinition', reseed, 0)
--dbcc checkident('proposalheader', reseed, 0)
--dbcc checkident('campaignleadbucketverbose', reseed, 0)
--dbcc checkident('campaignleadbucket', reseed, 0)
--dbcc checkident('leadactivity', reseed, 0)
--dbcc checkident('lead', reseed, 0)
--dbcc checkident('motorlosshistory', reseed, 0)
--dbcc checkident('homelosshistory', reseed, 0)
--dbcc checkident('losshistory', reseed, 0)
--dbcc checkident('leadquality', reseed, 0)
--dbcc checkident('individualuploaddetail', reseed, 0)
--dbcc checkident('individualuploadheader', reseed, 0)
--dbcc checkident('leadimport', reseed, 0)
--dbcc checkident('campaignagent', reseed, 0)
--dbcc checkident('bankdetails', reseed, 0)
--dbcc checkident('partycorrespondencepreference', reseed, 0)
--dbcc checkident('partycorrespondenceattachment', reseed, 0)
--dbcc checkident('partycorrespondence', reseed, 0)
--dbcc checkident('customersatisfactionsurveyanswers', reseed, 0)
--dbcc checkident('customersatisfactionsurvey', reseed, 0)
--dbcc checkident('relationship', reseed, 0)
--dbcc checkident('address', reseed, 0)
--dbcc checkident('assetextras', reseed, 0)
--dbcc checkident('asset', reseed, 0)
--dbcc checkident('note', reseed, 0)

----------------------------------------------
--Remove all users except root@iplatform.co.za
----------------------------------------------

print 'Resetting users...'

declare @userid int, @partyid int;

select @userid = id from [user] where username = 'root@iplatform.co.za'
select @partyid = individualid from userindividual where userid = @userid

delete from userauthorisationgroup where userid != @userid
delete from userchannel where userid != @userid
delete from userindividual where userid != @userid
delete from [user] where id != @userid

delete from partycampaign where partyid != @partyid and partyid not in (select partyid from organization)
delete from contact where partyid != @partyid and partyid not in (select partyid from organization)
delete from individual where partyid != @partyid and partyid not in (select partyid from organization)
delete from party where id != @partyid and id not in (select partyid from organization)

set nocount off

------------
--End script
------------

print ''
print 'Reset complete!'