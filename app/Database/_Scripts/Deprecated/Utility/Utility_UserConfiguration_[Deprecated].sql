declare
	@ChannelDefaultCount int,
	@ChannelId int,
	@AuthorisationGroupId int,
	@UserId int,
	@Username varchar(255),
	@Identity int

set @ChannelDefaultCount = (select top 1 Count(Id) from Channel where IsDefault = 1)

if isnull(@ChannelDefaultCount, 0) > 1
	begin
		set noexec on
		print 'More than one channel is specified as default for this environment. Set only one channel as default and try running this script again.'
		return
	end

set @ChannelId = (select top 1 Id from Channel where IsDefault = 1)

if isnull(@ChannelId, 0) = 0
	begin
		set noexec on
		print 'Unable to locate the default channel for this environment. Please set the default channel and try running this script again.'
		return
	end

set nocount on

print ''
print 'Default channel (Id: ' + cast(@ChannelId as varchar) + ') found! Finding users.'

declare user_cursor cursor for
(
	select
		u.Id,
		u.UserName
	from [User] u
	where
		u.IsDeleted = 0
)
open user_cursor  
fetch next from user_cursor into @UserId, @Username

while @@fetch_status = 0  
	begin
		print ''
		print @Username + ' found. Checking configuration.'

		if not exists (select top 1 UserId from UserChannel where UserId = @UserId and ChannelId = @ChannelId)
			begin
				print @Username + ' is not assigned to any channel, assigning to default channel (Id: ' + cast(@ChannelId as varchar) + ').'

				insert into UserChannel (CreatedAt, ModifiedAt, UserId, ChannelId, IsDeleted, IsDefault)
				values (getdate(), getdate(), @UserId, @ChannelId, 0, 1)

				print @Username + ' was assigned to default channel (' + cast(@ChannelId as varchar) + ').'
			end

		if not exists (select top 1 UserId from UserAuthorisationGroup where UserId = @UserId)
			begin
				print @Username + ' has no authorisation setup, authorising as "Call Centre Agent" by default (role with least rights).'

				insert into UserAuthorisationGroup (AuthorisationGroupId, UserId, ChannelId, IsDeleted)
				values (1, @UserId, @ChannelId, 0)

				print @Username + ' is authorised as "Call Centre Agent".'
			end
		else
			begin
				if not exists (select top 1 UserId from UserAuthorisationGroup where UserId = @UserId and ChannelId = @ChannelId)
					begin
						print @Username + ' has no authorisation setup for the default channel, authorising with highest user authorisation right.'

						set @AuthorisationGroupId = (select max(AuthorisationGroupId) from UserAuthorisationGroup where UserId = @UserId)

						insert into UserAuthorisationGroup (AuthorisationGroupId, UserId, ChannelId, IsDeleted)
						values (@AuthorisationGroupId, @UserId, @ChannelId, 0)

						print @Username + ' was authorised with highest user authorisation right.'
					end
			end

		if not exists (select top 1 UserId from UserIndividual where UserId = @UserId)
			begin
				print @Username + ' does not have any individual entries which is a requirement for Apiv2.'

				insert into Party (PartyTypeId, DisplayName, IsDeleted, ChannelId) values (1, @Username, 0, @ChannelId)
				set @Identity = scope_identity()

				insert into Individual (PartyId, FirstName, Surname) values (@Identity, @Username, @Username)
				insert into UserIndividual (UserId, IndividualId, CreatedAt, ModifiedAt, IsDeleted) values (@UserId, @Identity, getdate(), getdate(), 0)

				print @Username + ' was added as an individual.'
			end

		print @Username + ' setup complete.'

		fetch next from user_cursor into @UserId, @Username
	end

close user_cursor  
deallocate user_cursor

print ''
print 'Moving all campaigns to default channel.'

update Campaign set ChannelId = @ChannelId

print 'All campaigns moved.'

print ''
print 'Creating default campaign references where required.'

insert into CampaignReference (CampaignId, Reference, IsDeleted)
select
	Id,
	'Default',
	0
from Campaign
where
	Campaign.IsDeleted = 0 and
	Campaign.Id not in (select CampaignId from CampaignReference where CampaignReference.CampaignId = Campaign.Id)

print 'Campaign references created.'

set nocount off