@ECHO OFF
FOR /F %%H IN ('hostname') DO SET hostnamevar=%%H
SET connectionstring="%hostnamevar%"-iBrokerConnectionString

..\..\..\..\Tools\Migrator\migrate.exe --a iPlatform.Database.dll --profile="Development" --conn=%connectionstring% --provider sqlserver2008 --verbose=true --task migrate --connectionStringConfigPath=iPlatform.Database.dll.config
