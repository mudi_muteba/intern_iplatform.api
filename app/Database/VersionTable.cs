﻿using FluentMigrator.VersionTableInfo;

namespace Database
{
    [VersionTableMetaData]
    public class VersionTable : IVersionTableMetaData
    {
        public object ApplicationContext { get; set; }

        public virtual bool OwnsSchema
        {
            get { return true; }
        }

        public string SchemaName
        {
            get { return string.Empty; }
        }

        public string TableName
        {
            get { return "_Version_iBroker"; }
        }

        public string ColumnName
        {
            get { return "Version"; }
        }

        public string DescriptionColumnName
        {
            get { return "XXXXX"; }
        }

        public string UniqueIndexName
        {
            get { return "UC_Version_iBroker"; }
        }

        public string AppliedOnColumnName
        {
            get { return ColumnName; }
        }
    }
}