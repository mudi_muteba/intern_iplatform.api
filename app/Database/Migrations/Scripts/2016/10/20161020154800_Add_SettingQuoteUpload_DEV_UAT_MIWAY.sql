﻿DECLARE @ChannelId int, @ProductId int;

SELECT @ChannelId = Id FROM Channel WHERE Code = 'MIWAY' AND IsDeleted <> 1
if(@ChannelId > 0)
BEGIN
	--MW (MiWay)
	SELECT @ProductId = Id FROM Product WHERE ProductCode = 'MW' AND IsDeleted <> 1
	if(@ProductId > 0)
	BEGIN
		INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/ServiceURL','http://197.97.185.243/api/uat'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/RetryDelayIncrement','5'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/MaxRetries','2'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/subject','UPLOADING LEAD TO MiWay'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/template','LeadUploadTemplate'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/AgentFullName','BIDVEST'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/AgentId','34011419'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/UserId','BIDVEST'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/ProductId','RB'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/SiteSequence','2'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/SourceSystemId','CARDINAL'),
		(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/ApiKey','d293d2ViYTp3b3d3ZWJhNzg5dWF0')
	END
	--SAME Settings for UAT 
	INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue])
	SELECT [ChannelId],[ProductId],[IsDeleted],'UAT',[SettingName],[SettingValue] FROM SettingsQuoteUpload WHERE ChannelId = @ChannelId AND Environment = 'Dev'
END

