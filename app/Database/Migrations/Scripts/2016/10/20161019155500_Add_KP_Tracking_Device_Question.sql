﻿declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode = 'KP')
	begin

declare @ProductId int
select @ProductId = id from Product where ProductCode = 'KPIPERS2'

--Motor
declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 218)

if not exists(select * from QuestionDefinition where QuestionId=87 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Tracking Device',@CoverDefinitionId,87,NULL,1,12,1,1,0,'Is there a tracking device installed in this vehicle?',N'',N'',0,NULL,0)
		END

	end
else
	begin
		return
    end