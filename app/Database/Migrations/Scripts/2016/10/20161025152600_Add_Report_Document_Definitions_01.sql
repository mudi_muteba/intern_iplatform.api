declare @ReportId int
declare @DocumentDefinitionId int

if exists(select * from Report where Name = 'Record Of Advice')
begin
	select @ReportId = Id from Report  where Name = 'Record Of Advice'

	if exists(select * from md.DocumentDefinition where Name = 'Advice record document details')
	begin
		select @DocumentDefinitionId = Id from md.DocumentDefinition  where Name = 'Advice record document details'
		insert into ReportDocumentDefinition (DocumentDefinitionId, IsDeleted, ReportId) values (@DocumentDefinitionId, 0, @ReportId)
	end
end

if exists(select * from Report where Name = 'Broker Note')
begin
	select @ReportId = Id from Report  where Name = 'Broker Note'

	if exists(select * from md.DocumentDefinition where Name = 'Broker appointment details')
	begin
		select @DocumentDefinitionId = Id from md.DocumentDefinition  where Name = 'Broker appointment details'
		insert into ReportDocumentDefinition (DocumentDefinitionId, IsDeleted, ReportId) values (@DocumentDefinitionId, 0, @ReportId)
	end
end

if exists(select * from Report where Name = 'POPI document')
begin
	select @ReportId = Id from Report  where Name = 'POPI document'

	if exists(select * from md.DocumentDefinition where Name = 'Protection Of Personal Information document')
	begin
		select @DocumentDefinitionId = Id from md.DocumentDefinition  where Name = 'Protection Of Personal Information document'
		insert into ReportDocumentDefinition (DocumentDefinitionId, IsDeleted, ReportId) values (@DocumentDefinitionId, 0, @ReportId)
	end
end

if exists(select * from Report where Name = 'SLA document')
begin
	select @ReportId = Id from Report  where Name = 'SLA document'

	if exists(select * from md.DocumentDefinition where Name = 'Service level agreement document')
	begin
		select @DocumentDefinitionId = Id from md.DocumentDefinition  where Name = 'Service level agreement document'
		insert into ReportDocumentDefinition (DocumentDefinitionId, IsDeleted, ReportId) values (@DocumentDefinitionId, 0, @ReportId)
	end
end

if exists(select * from Report where Name = 'Debit order instruction')
begin
	select @ReportId = Id from Report  where Name = 'Debit order instruction'

	if exists(select * from md.DocumentDefinition where Name = 'Debit order details')
	begin
		select @DocumentDefinitionId = Id from md.DocumentDefinition  where Name = 'Debit order details'
		insert into ReportDocumentDefinition (DocumentDefinitionId, IsDeleted, ReportId) values (@DocumentDefinitionId, 0, @ReportId)
	end

	if exists(select * from md.DocumentDefinition where Name = 'Debit order details signature extra 1')
	begin
		select @DocumentDefinitionId = Id from md.DocumentDefinition  where Name = 'Debit order details signature extra 1'
		insert into ReportDocumentDefinition (DocumentDefinitionId, IsDeleted, ReportId) values (@DocumentDefinitionId, 0, @ReportId)
	end

	if exists(select * from md.DocumentDefinition where Name = 'Debit order details plain text 1')
	begin
		select @DocumentDefinitionId = Id from md.DocumentDefinition  where Name = 'Debit order details plain text 1'
		insert into ReportDocumentDefinition (DocumentDefinitionId, IsDeleted, ReportId) values (@DocumentDefinitionId, 0, @ReportId)
	end
end
