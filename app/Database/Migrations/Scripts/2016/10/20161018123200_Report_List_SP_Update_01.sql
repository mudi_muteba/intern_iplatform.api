﻿if exists (select * from sys.objects where object_id = object_id(N'Report_ReportList') and type in (N'P', N'PC'))
	begin
		drop procedure Report_ReportList
	end
go

create procedure Report_ReportList
as
	select
		Report.Id,
		Report.ReportCategoryId,
		Report.ReportTypeId,
		Report.ReportFormatId,
		Report.Name,
		Report.[Description],
		Report.SourceFile,
		Report.IsVisibleInReportViewer,
		Report.IsVisibleInScheduler,
		Report.IsActive,
		Report.VisibleIndex--,
		--Report.Icon
	from Report
	where
		Report.IsDeleted = 0
	order by
		Report.Name
go

if exists (select * from sys.objects where object_id = object_id(N'Report_ReportList_ByChannelId') and type in (N'P', N'PC'))
	begin
		drop procedure Report_ReportList_ByChannelId
	end
go

create procedure Report_ReportList_ByChannelId
(
	@ChannelId int
)
as
	select
		Report.Id,
		Report.ReportCategoryId,
		Report.ReportTypeId,
		Report.ReportFormatId,
		Report.Name,
		Report.[Description],
		Report.SourceFile,
		Report.IsVisibleInReportViewer,
		Report.IsVisibleInScheduler,
		Report.IsActive,
		Report.VisibleIndex--,
		--Report.Icon
	from Report
		inner join ReportChannel on ReportChannel.ReportId = Report.Id
		inner join Channel on Channel.Id = ReportChannel.ChannelId
	where
		Report.IsDeleted = 0 and
		Channel.IsDeleted = 0 and
		Channel.IsActive = 1 and
		Channel.Id = @ChannelId
	order by
		Report.Name
go