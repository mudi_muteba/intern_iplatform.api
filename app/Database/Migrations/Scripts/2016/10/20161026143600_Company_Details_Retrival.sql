if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_CompanyDetails') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_CompanyDetails
	end
go

create procedure Reports_SignFlow_CompanyDetails
(

	@Code nvarchar(50)
)
as


select 
	[add].Description, 
	cd.Direct, 
	cd.Cell 
from Organization as org
join Channel as c on c.OrganizationId = org.PartyId
join Party as p on p.Id = org.PartyId
join ContactDetail as cd on cd.Id = p.ContactDetailId
join [Address] as [add] on [add].PartyId = p.Id
where c.Code = @Code
and c.IsDeleted != 1
and [add].IsDefault = 1
and p.IsDeleted != 1

