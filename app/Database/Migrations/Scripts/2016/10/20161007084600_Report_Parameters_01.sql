﻿delete from ReportParam
dbcc checkident ('ReportParam', reseed, 0)

declare @ReportId int

if exists(select * from Report where Name = 'Agent Performance')
	begin
		select @ReportId = Id from Report  where Name = 'Agent Performance'

		insert into ReportParam (ReportId, Field, DataTypeId, VisibleIndex) values (@ReportId, 'Campaign', 6, 1)
		insert into ReportParam (ReportId, Field, DataTypeId, VisibleIndex) values (@ReportId, 'Start Date', 3, 2)
		insert into ReportParam (ReportId, Field, DataTypeId, VisibleIndex) values (@ReportId, 'End Date', 3, 3)

		update Report set VisibleIndex = 1  where Id = @ReportId
	end

if exists(select * from Report where Name = 'Insurer Quote Breakdown')
	begin
		select @ReportId = Id from Report  where Name = 'Insurer Quote Breakdown'

		insert into ReportParam (ReportId, Field, DataTypeId, VisibleIndex) values (@ReportId, 'Campaign', 6, 1)
		insert into ReportParam (ReportId, Field, DataTypeId, VisibleIndex) values (@ReportId, 'Start Date', 3, 2)
		insert into ReportParam (ReportId, Field, DataTypeId, VisibleIndex) values (@ReportId, 'End Date', 3, 3)

		update Report set VisibleIndex = 2  where Id = @ReportId
	end

if exists(select * from Report where Name = 'Lead Management')
	begin
		select @ReportId = Id from Report  where Name = 'Lead Management'

		insert into ReportParam (ReportId, Field, DataTypeId, VisibleIndex) values (@ReportId, 'Campaign', 6, 1)
		insert into ReportParam (ReportId, Field, DataTypeId, VisibleIndex) values (@ReportId, 'Start Date', 3, 2)
		insert into ReportParam (ReportId, Field, DataTypeId, VisibleIndex) values (@ReportId, 'End Date', 3, 3)

		update Report set VisibleIndex = 3 where Id = @ReportId
	end

if exists(select * from Report where Name = 'Target & Sales Management')
	begin
		select @ReportId = Id from Report  where Name = 'Target & Sales Management'

		insert into ReportParam (ReportId, Field, DataTypeId, VisibleIndex) values (@ReportId, 'Campaign', 6, 1)
		insert into ReportParam (ReportId, Field, DataTypeId, VisibleIndex) values (@ReportId, 'Start Date', 3, 2)
		insert into ReportParam (ReportId, Field, DataTypeId, VisibleIndex) values (@ReportId, 'End Date', 3, 3)

		update Report set VisibleIndex = 4 , IsActive = 0 where Id = @ReportId
	end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_ReportParams') and type in (N'P', N'PC'))
	begin
		drop procedure Report_ReportParams
	end
go

create procedure Report_ReportParams
(
	@ReportId int
)
as
	select
		[Field],
		DataTypeId,
		VisibleIndex--,
		--Icon
	from ReportParam
	where
		(
			IsDeleted is null or
			IsDeleted = 0
		) and
		ReportId = @ReportId
	order by
		VisibleIndex asc
go