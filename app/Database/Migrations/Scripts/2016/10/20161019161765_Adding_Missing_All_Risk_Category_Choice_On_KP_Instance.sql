﻿declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode = 'KP')
	begin

if not exists(select Id from [md].[QuestionAnswer] where Id = 11764 and Name = 'KP - All Risk Category - Cell Phones')
  begin

  INSERT [md].[QuestionAnswer] ([Id], [Name], [Answer] , [VisibleIndex] ,[QuestionId] ) VALUES ( 11764, N'KP - All Risk Category - Cell Phones', N'Cell Phones' , 6 , 1033 )
  
  end

if not exists(select Id from [md].[QuestionAnswer] where Id = 11765 and Name = 'KP - All Risk Category - CDs')
  begin

  INSERT [md].[QuestionAnswer] ([Id], [Name], [Answer] , [VisibleIndex] ,[QuestionId] ) VALUES ( 11765, N'KP - All Risk Category - CDs', N'CDs' , 7 , 1033 )
  
  end

if not exists(select Id from [md].[QuestionAnswer] where Id = 11766 and Name = 'KP - All Risk Category - R1 Apple Watch')
  begin

  INSERT [md].[QuestionAnswer] ([Id], [Name], [Answer] , [VisibleIndex] ,[QuestionId] ) VALUES ( 11766, N'KP - All Risk Category - R1 Apple Watch', N'R1 Apple Watch' , 31 , 1033 )
  
  end


	end
else
	begin
		return
    end