﻿BEGIN TRANSACTION
	--Create temp tables
	CREATE TABLE #DropTempTable(script nvarchar(max));
	CREATE TABLE #CreateTempTable(script nvarchar(max));

	-- insert create constraint for SalesFNI fk
	INSERT INTO #CreateTempTable
	SELECT 
	N'
	  ALTER TABLE ' + QUOTENAME(s.name) + N'.'
	  + QUOTENAME(p.name) + N' WITH CHECK ADD CONSTRAINT '
	  + QUOTENAME(f.name) + 'FOREIGN KEY([AgentId]) REFERENCES [dbo].[SalesFNI] ([Id]);'
	FROM 
	   sys.foreign_keys AS f
	INNER JOIN 
	   sys.foreign_key_columns AS fc 
		  ON f.OBJECT_ID = fc.constraint_object_id
	INNER JOIN 
	   sys.tables t 
		  ON t.OBJECT_ID = fc.referenced_object_id
	INNER JOIN 
	   sys.tables p 
		  ON p.OBJECT_ID = fc.parent_object_id
	INNER JOIN sys.schemas AS s 
		ON p.[schema_id] = s.[schema_id]
	WHERE 
	   OBJECT_NAME (f.referenced_object_id) = 'SalesFNI'

	-- insert drop constraint for channel fk
	INSERT INTO #DropTempTable
	SELECT 
	N'
	  ALTER TABLE ' + QUOTENAME(s.name) + N'.'
	  + QUOTENAME(p.name) + N' DROP CONSTRAINT '
	  + QUOTENAME(f.name) + ';'
	FROM 
	   sys.foreign_keys AS f
	INNER JOIN 
	   sys.foreign_key_columns AS fc 
		  ON f.OBJECT_ID = fc.constraint_object_id
	INNER JOIN 
	   sys.tables t 
		  ON t.OBJECT_ID = fc.referenced_object_id
	INNER JOIN 
	   sys.tables p 
		  ON p.OBJECT_ID = fc.parent_object_id
	INNER JOIN sys.schemas AS s 
		ON p.[schema_id] = s.[schema_id]
	WHERE 
	   OBJECT_NAME (f.referenced_object_id) = 'SalesFNI' OR OBJECT_NAME (f.parent_object_id) = 'SalesFNI'
	
	DECLARE @MyCursor CURSOR, @script nvarchar(max)
	SET @MyCursor = CURSOR FOR select script from #DropTempTable      
	OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @script

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC (@script);
			FETCH NEXT FROM @MyCursor INTO @script  
		END
		
	CLOSE @MyCursor 
	DEALLOCATE @MyCursor;

	--create temp table
	CREATE TABLE [dbo].[tmp_SalesFNI](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[EmployeeNumber] [nvarchar](255) NULL,
		[IdNumber] [nvarchar](255) NULL,
		[Username] [nvarchar](255) NULL,
		[Firstname] [nvarchar](255) NULL,
		[Lastname] [nvarchar](255) NULL,
		[WorkTelephoneCode] [nvarchar](255) NULL,
		[WorkTelephoneNumber] [nvarchar](255) NULL,
		[EmailAddress] [nvarchar](255) NULL,
		[PartyId] [int] NULL,
		[IsDeleted] [bit] NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	--insert rows
	IF EXISTS(SELECT * FROM dbo.SalesFNI)
		EXEC('INSERT INTO [dbo].[tmp_SalesFNI](EmployeeNumber, IdNumber, Username, Firstname, Lastname, WorkTelephoneCode, WorkTelephoneNumber, EmailAddress, PartyId, IsDeleted)
		SELECT [EmployeeNumber],[IdNumber],[Username],[Firstname],[Lastname],[WorkTelephoneCode],[WorkTelephoneNumber],[EmailAddress],[PartyId],0 FROM [dbo].[SalesFNI]');
	
	--drop old SalesFNI table 
	DROP table SalesFNI;

	--rename temp SalesFNI table to channel
	EXECUTE sp_rename N'dbo.tmp_SalesFNI', N'SalesFNI';

	-- update SalesDetails Record
	Update SalesDetails Set AgentId = (SELECT Id FROM SalesFNI WHERE AgentId = PartyId);

	-- create SalesFNI fk constraints
	SET @MyCursor = CURSOR FOR select script from #CreateTempTable      
	OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @script

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC(@script);
			FETCH NEXT FROM @MyCursor INTO @script 
		END
	CLOSE @MyCursor 
	DEALLOCATE @MyCursor;

	-- Drop temp tables
	drop table #DropTempTable;
	drop table #CreateTempTable;
 


COMMIT TRANSACTION;