select * from dbo.SignFlowDocument
select * from dbo.SignFlowCorrespondant

IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'SignFlowDocument'))
BEGIN	
	IF (EXISTS (SELECT * 
					FROM INFORMATION_SCHEMA.COLUMNS 
					WHERE TABLE_NAME = 'SignFlowDocument'
					AND COLUMN_NAME = 'CorrespondantUserEmail'))
	BEGIN		
		ALTER TABLE dbo.SignFlowDocument DROP COLUMN CorrespondantUserEmail ;
	END
	IF (EXISTS (SELECT * 
					FROM INFORMATION_SCHEMA.COLUMNS 
					WHERE TABLE_NAME = 'SignFlowDocument'
					AND COLUMN_NAME = 'StatusId'))
	BEGIN	
		IF EXISTS (SELECT * 
					FROM sys.foreign_keys 
					WHERE object_id = OBJECT_ID(N'dbo.FK_SignFlowDocument_DocumentStatus_StatusId')
					AND parent_object_id = OBJECT_ID(N'dbo.SignFlowDocument'))
		BEGIN
			ALTER TABLE [SignFlowDocument] DROP CONSTRAINT [FK_SignFlowDocument_DocumentStatus_StatusId]  	
		
			ALTER TABLE SignFlowDocument DROP COLUMN StatusId ;

			ALTER TABLE [SignFlowDocument]  WITH CHECK ADD  CONSTRAINT [FK_SignFlowDocument_DocumentStatus_DocumentStatusId] FOREIGN KEY([DocumentStatusId])
			REFERENCES [md].[DocumentStatus] ([Id])

			ALTER TABLE [SignFlowDocument] CHECK CONSTRAINT [FK_SignFlowDocument_DocumentStatus_DocumentStatusId]
		END		
	END
	IF (EXISTS (SELECT * 
						FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE TABLE_NAME = 'SignFlowDocument'
						AND COLUMN_NAME = 'DoctypeId'))
	BEGIN	
		IF EXISTS (SELECT * 
					FROM sys.foreign_keys 
					WHERE object_id = OBJECT_ID(N'dbo.FK_SignFlowDocument_DocumentType_DocTypeId')
					AND parent_object_id = OBJECT_ID(N'dbo.SignFlowDocument'))
		BEGIN
			ALTER TABLE [SignFlowDocument] DROP CONSTRAINT [FK_SignFlowDocument_DocumentType_DocTypeId]  	
		
			ALTER TABLE SignFlowDocument DROP COLUMN DocTypeId ;

			ALTER TABLE [SignFlowDocument]  WITH CHECK ADD  CONSTRAINT [FK_SignFlowDocument_DocumentType_DocumentTypeId] FOREIGN KEY([DocumentTypeId])
			REFERENCES [md].[DocumentType] ([Id])

			ALTER TABLE [SignFlowDocument] CHECK CONSTRAINT [FK_SignFlowDocument_DocumentType_DocumentTypeId]
		END		
	END
	BEGIN	
		IF EXISTS (SELECT * 
					FROM sys.foreign_keys 
					WHERE object_id = OBJECT_ID(N'dbo.FK_SignFlowDocument_DocumentType_DocumentTypeId')
					AND parent_object_id = OBJECT_ID(N'dbo.SignFlowDocument'))
		BEGIN
			ALTER TABLE [SignFlowDocument] DROP CONSTRAINT [FK_SignFlowDocument_DocumentType_DocumentTypeId]  	
		
			ALTER TABLE SignFlowDocument DROP COLUMN DocumentTypeId ;			
		END		
	END
END

