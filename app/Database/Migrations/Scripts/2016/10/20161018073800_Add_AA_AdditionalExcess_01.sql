﻿--AA Excess (Motor), Product Codes: 'AA'
declare
	@AA_AA_Motor_CoverDefinitionId int
	
select
	@AA_AA_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AA') and
	Cover.Name in ('Motor')

if	isnull(@AA_AA_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 108, 'Motor Vehicles', 'Basic excess - Additional excess may apply', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 109, 'Motorbikes', 'Flat excess - Additional excess as per motor vehicles', 3000, 3000, 3000, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 110, 'Caravans', 'Flat excess', 2500, 2500, 2500, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 111, 'Trailers', 'Flat excess', 2500, 2500, 2500, 0, 0, 0, 0, 0, 0)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 112, 'Accessories', 'Specified radio', 750, 750, 750, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 112, 'Accessories ', 'Windscreen replacement (Excluding panoramic glass)', 1500, 1500, 1500, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 112, 'Accessories', 'Vehicle glass (Excluding panoramic glass)', 1500, 1500, 1500, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 112, 'Accessories ', 'Panoramic glass', 1500, 1500, 1500, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 113, 'Additional Excess', 'If the incident driver is not the regular driver and is younger than 25 years old', 3000, 3000, 3000, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 113, 'Additional Excess', 'If the incident driver has a drivers licence for less than 2 years', 3000, 3000, 3000, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 113, 'Additional Excess', 'If the incident occurred outside of South Africa and the vehicle is not drivable', 7500, 7500, 7500, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 113, 'Additional Excess', 'With regards to a car claim that occurs within the first 6 months of cover', 7500, 7500, 7500, 0, 0, 0, 0, 0, 0)
	end


--AA Excess (All Risk), Product Codes: 'AA'
declare
	@AA_AA_AllRisk_CoverDefinitionId int
	
select
	@AA_AA_AllRisk_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AA') and
	Cover.Name in ('All Risk')

if	isnull(@AA_AA_AllRisk_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_AllRisk_CoverDefinitionId, 114, 'General', 'Basic excess as per selected excess', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_AllRisk_CoverDefinitionId, 115, 'Factory Fitted Radio', 'If unspecified basic vehicle excess applies', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_AllRisk_CoverDefinitionId, 116, 'Portable Possessions', 'Specified (Flat excess)', 500, 500, 500, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_AllRisk_CoverDefinitionId, 116, 'Portable Possessions', 'Unspecified (Flat excess)', 500, 500, 500, 0, 0, 0, 0, 0, 0)
	end


--AA Excess (Contents), Product Codes: 'AA'
declare
	@AA_AA_Contents_CoverDefinitionId int
	
select
	@AA_AA_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AA') and
	Cover.Name in ('Contents')

if	isnull(@AA_AA_Contents_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Contents_CoverDefinitionId, 117, 'Home Contents', 'Minimum R1,500 or maximum R4,500', 0, 1500, 4500, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Contents_CoverDefinitionId, 117, 'Additional Contents Cover', 'No excess payable with the exception of garden furniture', 0, 0, 0, 0, 0, 0, 0, 0, 0)
	end


--AA Excess (Building), Product Codes: 'AA'
declare
	@AA_AA_Building_CoverDefinitionId int
	
select
	@AA_AA_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AA') and
	Cover.Name in ('Building')

if	isnull(@AA_AA_Building_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Building_CoverDefinitionId, 118, 'Buildings', 'Minimum R1,500 or maximum R4,500', 0, 1500, 4500, 0, 0, 0, 0, 0, 0)
	end