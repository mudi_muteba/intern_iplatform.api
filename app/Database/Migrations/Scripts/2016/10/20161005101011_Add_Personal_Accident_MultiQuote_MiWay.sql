﻿
DECLARE @pid INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT
 
SET @pid = 27 --MultiQuote
SET @Name = N'MultiQuote'
 
SET @productid = ( SELECT ID FROM dbo.Product where  Name= @Name AND ProductOwnerID = @pid)

if (@productid IS NULL)
return

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = 257) -- Personal Accident

IF (@coverDefId IS NULL)
BEGIN
	INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, N'Personal Accident', @productid, 257, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END
if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 1, 0, 0,
               N'I''d like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'25000' , N'^[0-9\.]+$', 0, NULL)
              end

SET @Name = N'MIWHEELS'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  Name= @Name)

if (@productid IS NULL)
return

SET @coverDefId = ( SELECT TOP 1 ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = 257) -- Personal Accident

IF (@coverDefId IS NULL)
BEGIN
	INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, N'Personal Accident', @productid, 257, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END
if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 1, 0, 0,
               N'I''d like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'25000' , N'^[0-9\.]+$', 0, NULL)
              end
