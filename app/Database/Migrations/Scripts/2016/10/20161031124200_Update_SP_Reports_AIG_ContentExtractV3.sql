if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_ContentExtract') and type in (N'P', N'PC'))
begin
	drop procedure Reports_AIG_ContentExtract
end
go

create procedure Reports_AIG_ContentExtract
(
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as

--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
--set @StartDate = '2016-06-09'
--set @EndDate = '2016-09-10'

DECLARE @tbl TABLE(QuestionId int, QuoteId int, AnswerId nvarchar(255), PartyId int, Created datetime, ProposalHeaderId int, ProposalDefinitionId int, QuestionDefDisplayName nvarchar(255))

insert into @tbl
select qd.QuestionId, qte.Id as QuoteId,pqa.Answer as AnswerId, pd.PartyId, pd.Created, pd.ProposalHeaderId, pd.Id, qd.DisplayName from ProposalDefinition as pd
				join ProposalQuestionAnswer as pqa on pqa.ProposalDefinitionId = pd.Id
				join QuestionDefinition as qd on qd.Id = pqa.QuestionDefinitionId
				join md.Question as q on q.Id = qd.QuestionId
				join ProposalHeader as ph on ph.Id = pd.ProposalHeaderId
				join QuoteHeader as qh on qh.ProposalHeaderId = ph.Id
				join Quote as qte on qte.QuoteHeaderId = qh.Id	
				join CoverDefinition as covd on pd.CoverId = covd.CoverId		
				where covd.ProductId = 46 and
				pd.CoverId = 78 and pd.IsDeleted = 0 and qte.IsDeleted =0 and qd.id = pqa.QuestionDefinitionId

--select * from @tbl where QuestionDefDisplayName like '%Discount / (Loading)%'

Select 

	CASE 
		WHEN Prh.Source IS NULL THEN ('Call center')   
		ELSE Prh.Source                                     
    END as Channel, 

	CASE
		WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y'
		ELSE 'N'
	END as 'QuoteExpired',

	CASE 
		WHEN PhS.Id IS NULL THEN ('N')   
		WHEN PhS.Id = 1 THEN ('N')
		WHEN PhS.Id = 2 THEN ('N')
		WHEN PhS.Id = 3 THEN ('Y')                                         
	END AS 'Quote-Bound',

	case when Qute.AcceptedOn IS NULL Then '' else CONVERT(VARCHAR(10),Qute.AcceptedOn ,111) end as 'QuoteBindDate',

	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'Date of Quote',
	
	case when (select cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2)) + cast(round (Qute.fees ,2) as numeric(36,2)) from QuoteItem as Qi		
				join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
				where qi.Id = QteI.Id and cd.CoverId in (78, 17, 45, 142, 373, 372, 258) 
	) is null
	THEN (0) 
	ELSE (select cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2)) + cast(round (Qute.fees ,2) as numeric(36,2)) from QuoteItem as Qi		
			join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
			where qi.Id = QteI.Id and cd.CoverId in (78, 17, 45, 142, 373, 372, 258) )                                          
	END 
	as 'QuoteTotalPremium',

 	CASE WHEN (select Premium from QuoteItem as Qi		
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
		where 
		cd.CoverId = 218 and qi.Id = QteI.Id
	 ) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId = 218 and qi.Id = QteI.Id)                                          
     END AS 'QuoteMotorPremium',
	 
	 CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId in (17,45) and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select  cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId in (17,45) and qi.Id = QteI.Id)                                          
     END AS 'QuoteHomePremium',

	CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId = 142 and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId = 142 and qi.Id = QteI.Id)                                          
     END AS 'QuoteFuneralPremium',	

	CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId in (373, 372, 258) and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId in (373, 372, 258) and qi.Id = QteI.Id)                                          
     END as 'QuoteOtherPremium',

	(cast(round(Qute.fees,2) as numeric(36,2))) as 'QuoteFees',
	(cast(round(QteI.Sasria,2) as numeric(36,2))) as 'QuoteSasria',

	(select top 1 CONVERT(VARCHAR(10),InceptionDate ,111) from PaymentDetails where PartyId = p.Id and QuoteId = qute.Id order by Id desc) as 'QuoteIncepptionDate',
		
	ph.PolicyNo as 'QuotePolicyNumber',
	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'Quote-CreateDate',
	CONVERT(VARCHAR(5),Qute.CreatedAt,108) as 'Quote-CreateTime',


	qute.Id as 'QuoteID',
	Ind.ExternalReference as 'VMSAID',

		QteI.AssetNumber as 'PropertyNumber',
		
		covD.DisplayName as 'CoverType',			
		

		(select line1
				from Address as [add]
				join AssetAddress as astAdd on astAdd.AddressId = [add].Id				
				join ProposalDefinition as pr on pr.AssetId = astAdd.AssetId
				where astAdd.AssetId = prd.AssetId
				and pr.Id = prd.Id
				and [add].IsDeleted = 0) as 'Address-Suburb',

		(select [add].Code
				from Address as [add]
				join AssetAddress as astAdd on astAdd.AddressId = [add].Id				
				join ProposalDefinition as pr on pr.AssetId = astAdd.AssetId
				where astAdd.AssetId = prd.AssetId
				and pr.Id = prd.Id
				and [add].IsDeleted = 0) as 'Address-PostCode',

		(select sp.Name
				from Address as [add]
				join AssetAddress as astAdd on astAdd.AddressId = [add].Id
				join md.StateProvince as sp on sp.Id = [add].StateProvinceId
				join ProposalDefinition as pr on pr.AssetId = astAdd.AssetId
				where astAdd.AssetId = prd.AssetId
				and pr.Id = prd.Id
				and [add].IsDeleted = 0) as 'Address-Province',

		(select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 848 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'HomeType',


		(select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 480 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		 as 'WallConstruction',

		(select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 462 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		 as 'RoofConstruction',

		(select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 850 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		 as 'ThatchLapa',

		(case 
			(select AnswerId from @tbl as temp	
				where temp.QuestionId = 394 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
					when null then 'N'
					when 'False' then 'N'
					when 'true' then 'Y'
		End)   as 'ExcludingTheft',

		(case 
			(select AnswerId from @tbl as temp	
				where temp.QuestionId = 851 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
					when null then 'N'
					when 'False' then 'N'
					when 'true' then 'Y'
		End)  as 'ThatchLapaSize',

		(case 
			(select AnswerId from @tbl as temp	
				where temp.QuestionId = 465 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
					when null then 'N'
					when 'False' then 'N'
					when 'true' then 'Y'
		End)  as 'LightningConductor',

		
		(select qa.Answer from @tbl as temp
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 855 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )			
		as 'PropertyBorder',


		(select AnswerId from @tbl as temp
			where temp.QuestionId = 845 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
		    as 'ConstructionYear',

		(select AnswerId from @tbl as temp
			where temp.QuestionId = 849 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
		     as 'SquareMetresOfProperty',

		(select qa.Answer from @tbl as temp
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 846 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
		     as  'NumberOfBathrooms',

		(case 
			(select AnswerId from @tbl as temp	
				where temp.QuestionId = 856 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
					when null then 'N'
					when 'False' then 'N'
					when 'true' then 'Y'
		End)  as 'PropertyUnderConstruction',


		(select qa.Answer from @tbl as temp	
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 847 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )  
		as 'HomeUsage',

		(select qa.Answer from @tbl as temp	
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 853 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )  
		as 'BusinessConducted',

		(case 
			(select AnswerId from @tbl as temp	
				where temp.QuestionId = 854 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
				when 'False' then 'N'
				when 'true' then 'Y'
		End)   as 'Commune',

		(select qa.Answer from @tbl as temp	
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 852 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )  
		as 'PeriodPropertyIsUnoccupied',

		(select qa.Answer from @tbl as temp	
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 843 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
		 as 'Ownership',

		(case 
			(select AnswerId from @tbl as temp	
				where temp.QuestionId = 448 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
				when 'False' then 'N'
				when 'true' then 'Y'
		End)   as 'Security-GatedCommunity',

		(case 
			(select AnswerId from @tbl as temp	
				where temp.QuestionId = 450 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
				when 'False' then 'N'
				when 'true' then 'Y'
		End)   as 'Security-SecurityComplex',

		(case 
			(select AnswerId from @tbl as temp	
				where temp.QuestionId = 436 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
				when 'False' then 'N'
				when 'true' then 'Y'
		End)   as 'Security-BurglarBars',

		(case 
			(select AnswerId from @tbl as temp	
				where temp.QuestionId = 468 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
				when 'False' then 'N'
				when 'true' then 'Y'
		End)  as 'Security-SecurityGates',

		(case 
			(select AnswerId from @tbl as temp	
				where temp.QuestionId = 454 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
				when 'False' then 'N'
				when 'true' then 'Y'
		End)  as 'Security-AlarmWithArmedResponse',

		(select qa.Answer from @tbl as temp	
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 859 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
		 as 'VAPS-AccidentalDamage',

		(case 
			(select AnswerId from @tbl as temp	
				where temp.QuestionId = 472 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
				when 'False' then 'N'
				when 'true' then 'Y'
		End)  as 'VAPS-SubsidenceAndLandslip',

		(select qa.Answer from @tbl as temp	
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 858 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
		 as 'Contents-Excess',

		(select AnswerId from @tbl as temp	
				where temp.QuestionId = 102 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
		 as 'SumInsured',

		(select AnswerId from @tbl as temp	
				where temp.QuestionId = 155 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
		 as 'DiscretionaryDiscount',

		 cast(round(0,2) as numeric(36,2)) as 'SASRIA',
		 cast(round(0,2) as numeric(36,2)) as 'PremiumRisk',

		 cast(round(0,2) as numeric(36,2)) as 'PremiumSubsidenceAndLandslip',

		 cast(round(QteI.Premium,2) as numeric(36,2))  as 'PremiumTotal'

from Party as P
join Lead as L on l.PartyId = p.Id
join Channel as Ch on ch.Id = p.ChannelId
join ProposalHeader as Prh on Prh.PartyId = p.Id 
join ProposalDefinition as prd on Prd.ProposalHeaderId = Prh.Id
join QuoteHeader as Qh on Qh.ProposalHeaderId = Prh.Id
join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
join QuoteItem as QteI on QteI.QuoteId = Qute.Id
join CoverDefinition as covD on covD.Id = QteI.CoverDefinitionId
left join Individual as Ind on P.Id = Ind.PartyId
left join PolicyHeader as Ph on Ph.QuoteId = Qute.Id
left join md.PolicyStatus as PhS on Ph.PolicyStatusId = PhS.Id
join QuoteAcceptedLeadActivity as qala on qala.QuoteId = Qute.Id
join LeadActivity as la on la.Id = qala.LeadActivityId
where Qute.Id is not null
and L.IsDeleted = 0
and Qute.IsDeleted = 0
and prd.IsDeleted = 0
and prd.CoverId = 78
and QteI.CoverDefinitionId in (select id from CoverDefinition where CoverId = 78)
and qute.CreatedAt  BETWEEN @StartDate AND @EndDate
and la.ActivityTypeId = 5
and Qh.IsDeleted = 0

