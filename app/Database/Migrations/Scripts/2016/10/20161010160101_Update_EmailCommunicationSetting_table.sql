﻿BEGIN TRANSACTION
	--Create temp tables
	CREATE TABLE #DropTempTable(script nvarchar(max));

	-- insert drop constraint for channel fk
	INSERT INTO #DropTempTable
	SELECT 
	N'
	  ALTER TABLE ' + QUOTENAME(s.name) + N'.'
	  + QUOTENAME(p.name) + N' DROP CONSTRAINT '
	  + QUOTENAME(f.name) + ';'
	FROM 
	   sys.foreign_keys AS f
	INNER JOIN 
	   sys.foreign_key_columns AS fc 
		  ON f.OBJECT_ID = fc.constraint_object_id
	INNER JOIN 
	   sys.tables t 
		  ON t.OBJECT_ID = fc.referenced_object_id
	INNER JOIN 
	   sys.tables p 
		  ON p.OBJECT_ID = fc.parent_object_id
	INNER JOIN sys.schemas AS s 
		ON p.[schema_id] = s.[schema_id]
	WHERE 
	   OBJECT_NAME (f.referenced_object_id) = 'EmailCommunicationSetting' OR OBJECT_NAME (f.parent_object_id) = 'EmailCommunicationSetting'
	
	DECLARE @MyCursor CURSOR, @script nvarchar(max)
	SET @MyCursor = CURSOR FOR select script from #DropTempTable      
	OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @script

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC (@script);
			FETCH NEXT FROM @MyCursor INTO @script  
		END
		
	CLOSE @MyCursor 
	DEALLOCATE @MyCursor;

	--create temp table
	CREATE TABLE [dbo].[TMP_EmailCommunicationSetting](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Host] [nvarchar](255) NULL,
		[Port] [int] NULL,
		[UseSSL] [bit] NULL,
		[UseDefaultCredentials] [bit] NULL,
		[UserName] [nvarchar](255) NULL,
		[Password] [nvarchar](255) NULL,
		[DefaultFrom] [nvarchar](255) NULL,
		[DefaultBCC] [nvarchar](255) NULL,
		[DefaultContactNumber] [nvarchar](255) NULL,
		[SubAccountID] [nvarchar](255) NULL,
		[IsDeleted] [bit] NULL,
		[ChannelId] [int] NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[TMP_EmailCommunicationSetting]  WITH CHECK ADD  CONSTRAINT [FK_EmailCommunicationSetting_Channel_ChannelId] FOREIGN KEY([ChannelId])
	REFERENCES [dbo].[Channel] ([Id])

	ALTER TABLE [dbo].[TMP_EmailCommunicationSetting] CHECK CONSTRAINT [FK_EmailCommunicationSetting_Channel_ChannelId]

	--insert rows
	IF EXISTS(SELECT * FROM dbo.EmailCommunicationSetting)
		EXEC('INSERT INTO [dbo].[TMP_EmailCommunicationSetting](Host, Port, UseSSL, UseDefaultCredentials, UserName, Password, DefaultFrom, DefaultBCC, DefaultContactNumber, SubAccountID, IsDeleted, ChannelId)
		SELECT Host, Port, UseSSL, UseDefaultCredentials, UserName, Password, DefaultFrom, DefaultBCC, DefaultContactNumber, SubAccountID, 0, ChannelId FROM [dbo].[EmailCommunicationSetting]');
	
	--drop old SalesFNI table 
	DROP table EmailCommunicationSetting;

	--rename temp SalesFNI table to channel
	EXECUTE sp_rename N'dbo.TMP_EmailCommunicationSetting', N'EmailCommunicationSetting';

	-- Drop temp tables
	drop table #DropTempTable;
 
COMMIT TRANSACTION;