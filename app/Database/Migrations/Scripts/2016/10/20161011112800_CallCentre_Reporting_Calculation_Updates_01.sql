﻿if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics
	end
go

create procedure Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics
(
	@CampaignIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
			as 
			(
				select distinct
					[User].Id,
					Campaign.Id,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				from Campaign
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					Campaign.Id in (select * from fn_StringListToTable(@CampaignIds)) and
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
					UserAuthorisationGroup.AuthorisationGroupId = 1
				group by
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName
				)
			select
				Agent,
				Campaign,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
								select
									case when isnull(count(LeadActivity.Id), 0) = 0 then 1
									else cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0)) end
								from LeadActivity
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsDeleted = 0 and
									Quote.IsDeleted = 0
							)
						)

						* 100

						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from Quote
							inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.QuoteId = Quote.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join LeadActivity on LeadActivity.Id = QuoteAcceptedLeadActivity.LeadActivityId
						where
							LeadActivity.UserId = Agents_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote
			from
				Agents_CTE
			order by
				Campaign,
				Agent
		end
	else
		begin
			;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
			as 
			(
				select distinct
					[User].Id,
					Campaign.Id,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				from Campaign
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
					UserAuthorisationGroup.AuthorisationGroupId = 1
				group by
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName
				)
			select
				Agent,
				Campaign,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					where
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
					where
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
								where
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
								select
									case when isnull(count(LeadActivity.Id), 0) = 0 then 1
									else cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0)) end
								from LeadActivity
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								where
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsDeleted = 0 and
									Quote.IsDeleted = 0
							)
						)

						* 100

						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from Quote
							inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.QuoteId = Quote.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join LeadActivity on LeadActivity.Id = QuoteAcceptedLeadActivity.LeadActivityId
						where
							LeadActivity.UserId = Agents_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote
			from
				Agents_CTE
			order by
				Campaign,
				Agent
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics
	end
go

create procedure Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics
(
	@CampaignIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign)
			as 
			(
				select distinct
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
				where
					LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
					QuoteHeader.IsDeleted = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name
				)
			select
				Insurer,
				Product,
				Campaign,
				(
					select
						isnull(count(LeadActivity.Id), 0)
					from LeadActivity							
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					select
						isnull(count(LeadActivity.Id), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
								select
									case when isnull(count(LeadActivity.Id), 0) = 0 then 1
									else cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0)) end
								from LeadActivity							
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsDeleted = 0 and
									Quote.IsDeleted = 0
							)
						)

						* 100

						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						cast(isnull(avg(QuoteItem.Premium), 0) as numeric(18, 2))
					from Quote
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.QuoteId = Quote.Id
						inner join LeadActivity on LeadActivity.Id = QuoteAcceptedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) AveragePremiumPerAcceptedQuote,
				(
					select
						cast(isnull(avg(QuoteItem.Premium), 0) as numeric(18, 2))
					from QuoteHeader
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) AveragePremiumPerQuote
			from
				Insurers_CTE
			order by
				Campaign,
				Insurer,
				Product
		end
	else
		begin
			;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign)
			as 
			(
				select distinct
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
				where
					QuoteHeader.IsDeleted = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name
				)
			select
				Insurer,
				Product,
				Campaign,
				(
					select
						isnull(count(LeadActivity.Id), 0)
					from LeadActivity							
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					select
						isnull(count(LeadActivity.Id), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									Organization.PartyId = Insurers_CTE.InsurerId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
								select
									case when isnull(count(LeadActivity.Id), 0) = 0 then 1
									else cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0)) end
								from LeadActivity							
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									Organization.PartyId = Insurers_CTE.InsurerId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsDeleted = 0 and
									Quote.IsDeleted = 0
							)
						)

						* 100

						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						cast(isnull(avg(QuoteItem.Premium), 0) as numeric(18, 2))
					from Quote
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.QuoteId = Quote.Id
						inner join LeadActivity on LeadActivity.Id = QuoteAcceptedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) AveragePremiumPerAcceptedQuote,
				(
					select
						cast(isnull(avg(QuoteItem.Premium), 0) as numeric(18, 2))
					from QuoteHeader
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) AveragePremiumPerQuote
			from
				Insurers_CTE
			order by
				Campaign,
				Insurer,
				Product
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_LeadManagement_GetAgentLeadStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_LeadManagement_GetAgentLeadStatistics
	end
go

create procedure Report_CallCentre_LeadManagement_GetAgentLeadStatistics
(
	@CampaignIDs varchar(255),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
			as 
			(
				select distinct
					[User].Id,
					Campaign.Id,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				from Campaign
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					Campaign.Id in (select * from fn_StringListToTable(@CampaignIds)) and
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo
				group by
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName,
					Campaign.Name
				)
			select
				Agent,
				Campaign,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId
				) Received,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (5, 6, 7, 8, 9, 10, 11, 12, 13, 14)
				) Contacted,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (15, 16, 17, 18, 19, 20, 21, 22)
				) Uncontactable,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (7, 10, 9, 11, 20, 21)
				) Pending,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (5)
				) Completed,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (2)
				) Unactioned
			from
				Agents_CTE
			order by
				Agent asc,
				Campaign Asc
		end
	else
		begin
			;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
			as 
			(
				select distinct
					[User].Id,
					Campaign.Id,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				from Campaign
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo
				group by
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName,
					Campaign.Name
				)
			select
				Agent,
				Campaign,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId
				) Received,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (5, 6, 7, 8, 9, 10, 11, 12, 13, 14)
				) Contacted,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (15, 16, 17, 18, 19, 20, 21, 22)
				) Uncontactable,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (7, 10, 9, 11, 20, 21)
				) Pending,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (5)
				) Completed,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (2)
				) Unactioned
			from
				Agents_CTE
			order by
				Agent asc,
				Campaign Asc
		end
go