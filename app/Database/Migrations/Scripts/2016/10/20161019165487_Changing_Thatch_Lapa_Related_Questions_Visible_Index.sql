﻿declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode = 'KP')
	begin
declare @ProductId int
select @ProductId = id from Product where ProductCode = 'KPIPERS2'

--House Owners

declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 44)

UPDATE dbo.QuestionDefinition SET VisibleIndex=24 , QuestionGroupId = 2 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=1056

UPDATE dbo.QuestionDefinition SET VisibleIndex=25 , QuestionGroupId = 2 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=4

UPDATE dbo.QuestionDefinition SET VisibleIndex=25 ,QuestionGroupId = 2 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=16

	end
else
	begin
		return
    end