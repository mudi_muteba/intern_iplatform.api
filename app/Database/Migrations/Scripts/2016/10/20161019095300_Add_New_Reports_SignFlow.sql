
if not exists(select * from Report where Name = 'Debit order instruction')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex,  ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('Debit order instruction', 'Debit order instruction', 'Debit Order Instruction.trdx', 0, 0, 1, 5, 3, 3, 3)
	end
go

go
if not exists(select * from Report where Name = 'Record Of Advice')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex,  ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('Record Of Advice', 'Record Of Advice', 'Record Of Advice.trdx', 0, 0, 1, 6,  3, 3, 3)
	end
go

go
if not exists(select * from Report where Name = 'Broker Note')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('Broker Note', 'Broker Note', 'Broker''s Note.trdx', 0, 0, 1, 7,  3, 3, 3)
	end
go

go
if not exists(select * from Report where Name = 'POPI document')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('POPI document', 'POPI document', 'POPI_DOC.trdx', 0, 0, 1, 8, 3, 3, 3)
	end
go

go
if not exists(select * from Report where Name = 'SLA document')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('SLA document', 'SLA document', 'SLA.trdx', 0, 0, 1, 9,  3, 3, 3)
	end
go
