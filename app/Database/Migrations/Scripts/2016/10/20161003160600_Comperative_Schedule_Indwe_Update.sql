﻿declare @reportchannelid int;

 set @reportchannelid = (select ChannelId from ReportChannel
where Code = 'INDWE')

--indwe work number
update ReportChannelLayout
set Value='0860 13 13 14'
where ReportChannelId=@reportchannelid and Id=269

--Website
update ReportChannelLayout
set Value='http://www.indwe.co.za'
where ReportChannelId=@reportchannelid and Id=272

--VAT Number
update ReportChannelLayout
set Value='4290/149/09/7'
where ReportChannelId=@reportchannelid and Id=274

--Registration Number
update ReportChannelLayout
set Value='1994/004436/07'
where ReportChannelId=@reportchannelid and Id=273


--Statutory Disclosure by Indwe

update ReportChannelLayout
set Value='<ol>
			<li>Indwe is an Authorised Financial Services Provider (no 3425), licensed for Short Term Insurance: Personal Lines and Short Term Insurance: Commercial Lines.</li>
			<li>Indwe has a Conflict of Interest Management Policy in place and the COI statement is available on www.indwe.co.za.</li>
			<li>Indwe has written agreements with these insurers which is available on request.</li>
			<li>This is a comparative document and is only an indication of premium per quote given. The premium may be adjusted once the final underwriting is completed by the chosen insurer.</li>
			<li>The premium and fees shown above are inclusive of VAT.</li>
		  </ol>'
where ReportChannelId=@reportchannelid and Id=276

