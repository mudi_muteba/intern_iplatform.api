﻿if not exists(select * from Report where Name = 'Comparative Quote')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 1, 3, 'Comparative Quote', 'Comparative Quote', 'ComparativeQuote.trdx', 0, 0, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 1,
			ReportFormatId = 3,
			Name = 'Comparative Quote',
			[Description] = 'Comparative Quote',
			SourceFile = 'ComparativeQuote.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'Comparative Quote'
	end
go

if not exists(select * from Report where Name = 'Agent Performance')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 2, 3, 'Agent Performance', 'Agent Performance', 'AgentPerformance.trdx',  1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Agent Performance',
			[Description] = 'Agent Performance',
			SourceFile = 'AgentPerformance.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1
		where
			Name = 'Agent Performance'
	end
go

if not exists(select * from Report where Name = 'Insurer Quote Breakdown')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile,  IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 2, 3, 'Insurer Quote Breakdown', 'Insurer Quote Breakdown', 'InsurerQuoteBreakdown.trdx',  1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Insurer Quote Breakdown',
			[Description] = 'Insurer Quote Breakdown',
			SourceFile = 'InsurerQuoteBreakdown.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1
		where
			Name = 'Insurer Quote Breakdown'
	end
go

if not exists(select * from Report where Name = 'Lead Management')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (2, 2, 3, 'Lead Management', 'Lead Management', 'LeadManagement.trdx', 1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 2,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Lead Management',
			[Description] = 'Lead Management',
			SourceFile = 'LeadManagement.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1
		where
			Name = 'Lead Management'
	end
go

if not exists(select * from Report where Name = 'Target & Sales Management')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (2, 2, 3, 'Target & Sales Management', 'Target & Sales Management', 'TargetSalesManagement.trdx', 1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 2,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Target & Sales Management',
			[Description] = 'Target & Sales Management',
			SourceFile = 'TargetSalesManagement.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 0
		where
			Name = 'Target & Sales Management'
	end
go

if not exists(select * from Report where Name = 'Debit order instruction')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('Debit order instruction', 'Debit order instruction', 'Debit Order Instruction.trdx', 0, 0, 1, 5, 3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'Debit order instruction',
			[Description] = 'Debit order instruction',
			SourceFile = 'Debit Order Instruction.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'Debit order instruction'
	end
go

if not exists(select * from Report where Name = 'Record Of Advice')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex,  ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('Record Of Advice', 'Record Of Advice', 'Record Of Advice.trdx', 0, 0, 1, 6, 3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'Record Of Advice',
			[Description] = 'Record Of Advice',
			SourceFile = 'Record Of Advice.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'Record Of Advice'
	end
go

if not exists(select * from Report where Name = 'Broker Note')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex,  ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('Broker Note', 'Broker Note', 'Broker''s Note.trdx', 0, 0, 1, 7, 3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'Broker Note',
			[Description] = 'Broker Note',
			SourceFile = 'Broker''s Note.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'Broker Note'
	end
go

if not exists(select * from Report where Name = 'POPI document')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('POPI document', 'POPI document', 'POPI_DOC.trdx', 0, 0, 1, 8, 3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'POPI document',
			[Description] = 'POPI document',
			SourceFile = 'POPI_DOC.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'POPI document'
	end
go

if not exists(select * from Report where Name = 'SLA document')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('SLA document', 'SLA document', 'SLA.trdx', 0, 0, 1, 9,  3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'SLA document',
			[Description] = 'SLA document',
			SourceFile = 'SLA.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'SLA document'
	end
go


------------
--Parameters
------------

delete from ReportParam
dbcc checkident ('ReportParam', reseed, 0)

declare @ReportId int

if exists(select * from Report where Name = 'Comparative Quote')
	begin
		select @ReportId = Id from Report  where Name = 'Comparative Quote'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Proposal', 'ProposalHeaderId', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Quotes', 'QuoteIds', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Agent Performance')
	begin
		select @ReportId = Id from Report  where Name = 'Agent Performance'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Insurer Quote Breakdown')
	begin
		select @ReportId = Id from Report  where Name = 'Insurer Quote Breakdown'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Lead Management')
	begin
		select @ReportId = Id from Report  where Name = 'Lead Management'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Target & Sales Management')
	begin
		select @ReportId = Id from Report  where Name = 'Target & Sales Management'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Debit order instruction')
	begin
		select @ReportId = Id from Report  where Name = 'Debit order instruction'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Party', 'PartyId', '', 2, 0, 0)
	end
go