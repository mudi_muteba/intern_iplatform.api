﻿DECLARE @ChannelId int;

SELECT @ChannelId = Id FROM Channel WHERE IsDefault = 1 AND IsDeleted <> 1;
if @ChannelId >0
BEGIN
	Update Campaign Set ChannelId = @ChannelId WHERE ChannelId is null;
END
