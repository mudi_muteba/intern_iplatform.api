﻿DECLARE @cnt INT = 1023, @cnttotal int = 1179, @Required int, @DisplayName varchar(500), @ProductId int;

SELECT @ProductId = Id FROM Product WHERE ProductCode = 'VIRGIN';
IF @ProductId> 0
BEGIN
	WHILE @cnt < @cnttotal
	BEGIN
	   SELECT @Required =  [Required], @DisplayName = DisplayName FROM md.ClaimsQuestionDefinition WHERE Id = @cnt;
	   INSERT INTO ProductClaimsQuestionDefinition (ProductId, ClaimsQuestionDefinitionId, [Required], DisplayName, IsDeleted) VALUES
	   (@ProductId, @cnt, @Required, @DisplayName, 0);
	   SET @cnt = @cnt + 1;
	END
END