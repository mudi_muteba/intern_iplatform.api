﻿declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode = 'KP')
	begin

declare @parentQuestionDefinitionId int
declare @childQuestionDefinitionId int

/*Hide and show for Thatch Lapa related questions on buildings proposal  */
select @parentQuestionDefinitionId = id from questionDefinition where questionId = 1056 and CoverDefinitionId = 197

select @childQuestionDefinitionId = id from questionDefinition where questionId = 4  and CoverDefinitionId = 197
update QuestionDefinition set ParentId = @parentQuestionDefinitionId from QuestionDefinition 
inner join Product on Product.Id=58
WHERE CoverDefinitionId=197 and QuestionDefinition.QuestionId= 4

select @childQuestionDefinitionId = id from questionDefinition where questionId = 16  and CoverDefinitionId = 197
update QuestionDefinition set ParentId = @parentQuestionDefinitionId from QuestionDefinition 
inner join Product on Product.Id=58
WHERE CoverDefinitionId=197 and QuestionDefinition.QuestionId=16

	end
else
	begin
		return
    end