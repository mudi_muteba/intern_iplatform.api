﻿IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'BaseFolder' AND Object_ID = Object_ID(N'EmailCommunicationSetting'))
BEGIN
   Alter table EmailCommunicationSetting drop column BaseFolder;
END

IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'UseInsurerTemplate' AND Object_ID = Object_ID(N'EmailCommunicationSetting'))
BEGIN
   Alter table EmailCommunicationSetting drop column UseInsurerTemplate;
END