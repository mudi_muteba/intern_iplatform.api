﻿DECLARE @id INT
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverId INT
DECLARE @CoverName VARCHAR(50)
DECLARE @ProductCode VARCHAR(50)

------'Extended Warranty'---------

SET @CoverId = 375 -- 375 is MOTOR WARRANTY, 220 Motor External
SET @CoverName = N'Extended Warranty'
SET @ProductCode = N'MOTORWARRANTY'

SET @productid = (select top 1 ID from  Product where ProductCode = @ProductCode)

if (@productid IS NULL)
Return 

SET @id = @productid
 
declare @CoverDefinitionId int
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId) 
IF (@coverDefId IS NULL)
Return

SET @CoverDefinitionId = @coverDefId

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2)
              end

if not exists(select * from QuestionDefinition where QuestionId=1184 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Extended Warranty Vehicle Category', @CoverDefinitionId, 1184, NULL, 1, 13, 0, 1, 1,
               N'Extended Warranty Vehicle Category', N'' , N'', 0, NULL, 8)
              end

---Remove Unused Questions
UPDATE QuestionDefinition
SET IsDeleted = 1
WHERE QuestionId IN (69, 1162)
AND CoverDefinitionId = @CoverDefinitionId


----------- N'Pre-Owned Warranty-----------
SET @CoverName = N'Pre-Owned Warranty'
SET @ProductCode = N'PREOWNEDWARRANTY'

SET @productid = (select top 1 ID from  Product where ProductCode = @ProductCode)

if (@productid IS NULL)
Return 

SET @id = @productid

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId) 
IF (@coverDefId IS NULL)
Return

SET @CoverDefinitionId = @coverDefId

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2)
              end

if not exists(select * from QuestionDefinition where QuestionId=1185 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Pre Owned Warranty Vehicle Category', @CoverDefinitionId, 1185, NULL, 1, 13, 0, 1, 1,
               N'Pre Owned Warranty Vehicle Category', N'' , N'', 0, NULL, 8)
              end
			  





