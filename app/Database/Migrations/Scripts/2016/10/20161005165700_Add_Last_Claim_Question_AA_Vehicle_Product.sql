﻿DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT
 
SET @id = 11 --AA
SET @Name = N'AA Combined'
 
SET @productid = ( SELECT ID FROM dbo.Product where  NAme= @Name AND ProductOwnerID = @id)

if (@productid IS NULL)
Return


SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = 218) -- Vehicle


IF (@coverDefId IS NULL)
Return

SET @CoverDefinitionId = @coverDefId

if not exists(select * from QuestionDefinition where QuestionId=149 and CoverDefinitionId = @CoverDefinitionId)
	begin
	INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
	[DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver Last Accident Claim', @CoverDefinitionId, 149, NULL, 1, 38, 1, 1, 0,
	N'{Title} {Name} would you please provide me with the number of years since the main drivers last accident claim?', N'566' , N'', 0, 5)
	Print('Added')
	end

Print('Done')