﻿DECLARE @ChannelId int, @ProductId int;

SELECT @ChannelId = Id FROM Channel WHERE Code = 'KP' AND IsDeleted <> 1

--KPIPERS2 (Kingprice)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'KPIPERS2' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/ServiceURL','https://secure.kingprice.co.za/ServicesUAT/PartnerService.svc'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/ChannelEndpoint','https://secure.kingprice.co.za/ServicesUAT/PartnerService.svc'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/SecurityEndpoint','https://secure.kingprice.co.za/IdentityServer/issue/wstrust/mixed/username'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/SecurityDestination','https://secure.kingprice.co.za/ServicesUAT/'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/subject','UPLOADING LEAD TO KINGPRICE'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/template','LeadUploadTemplate'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/InsurerCode','KPI'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/ProductCode','KPIPERS2'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/UserName','iPlatform_APIPartner'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/Password','h8kPZp3RMU4f'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/Token','API-9717E8459DCE4149A77ED02DA6CC245D'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/AgentCode','IPLATFORM'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/BrokerCode','54CDABBE-314F-45C4-9189-A67B00A241A0'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/CompanyCode','King Price Insurance Company Ltd (iPlat)')

--SAME Settings for UAT 
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue])
SELECT [ChannelId],[ProductId],[IsDeleted],'UAT',[SettingName],[SettingValue] FROM SettingsQuoteUpload WHERE ChannelId = @ChannelId AND Environment = 'Dev'


