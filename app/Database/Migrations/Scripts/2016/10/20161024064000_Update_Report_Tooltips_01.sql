﻿update Report
set
	[Description] = 'This report measures the no. of quotes an agent has done compared to their sales and gives a conversion ratio'
where
	Name = 'Agent Performance'

update Report
set
	[Description] = 'This reports on the number of quotes done per insurer per product and advises on the average premium quoted and the average premium per sale'
where
	Name = 'Insurer Quote Breakdown'