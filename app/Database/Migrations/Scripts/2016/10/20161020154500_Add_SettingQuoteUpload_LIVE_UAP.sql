﻿DECLARE @ChannelId int, @ProductId int;

SELECT @ChannelId = Id FROM Channel WHERE Code = 'UAP' AND IsDeleted <> 1

--SAME Settings for UAT 
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue])
SELECT [ChannelId],[ProductId],[IsDeleted],'Live',[SettingName],[SettingValue] FROM SettingsQuoteUpload WHERE ChannelId = @ChannelId AND Environment = 'Dev'

--KingPrice2
Update SettingsQuoteUpload Set SettingValue = 'http://195.202.66.179/STPWS/Command.svc' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'IDS/Upload/ServiceURL';