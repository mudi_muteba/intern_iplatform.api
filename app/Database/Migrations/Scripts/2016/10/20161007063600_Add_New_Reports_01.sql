﻿update Report
set
	IsVisibleInReportViewer = 0,
	IsVisibleInScheduler = 0,
	ReportCategoryId = 1,
	IsActive = 1 
where
	IsActive is null or
	IsVisibleInReportViewer is null or
	IsVisibleInScheduler is null
go

if not exists(select * from Report where Name = 'Agent Performance')
	begin
		insert into Report (ReportCategoryId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 'Agent Performance', 'Agent Performance', 'AgentPerformance.trdx', 1, 1, 1)
	end
go

if not exists(select * from Report where Name = 'Insurer Quote Breakdown')
	begin
		insert into Report (ReportCategoryId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 'Insurer Quote Breakdown', 'Insurer Quote Breakdown', 'InsurerQuoteBreakdown.trdx', 1, 1, 1)
	end
go

if not exists(select * from Report where Name = 'Lead Management')
	begin
		insert into Report (ReportCategoryId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (2, 'Lead Management', 'Lead Management', 'LeadManagement.trdx', 1, 1, 1)
	end
go

if not exists(select * from Report where Name = 'Target & Sales Management')
	begin
		insert into Report (ReportCategoryId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (2, 'Target & Sales Management', 'Target & Sales Management', 'TargetSalesManagement.trdx', 1, 1, 1)
	end
go