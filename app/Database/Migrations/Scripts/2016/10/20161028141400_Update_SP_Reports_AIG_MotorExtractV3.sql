if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_MotorExtract') and type in (N'P', N'PC'))
begin
	drop procedure Reports_AIG_MotorExtract
end
go

create procedure Reports_AIG_MotorExtract
(
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as
-- ISSUES : VehicleNumber
-- Datadot questionID 9 or 231 ?? (question not present on screen)
-- Immobiliser questionID 80, 245 or 337 ?? (question not present on screen)

--Solution for big table 
	/*(select pqa.Answer from QuestionDefinition as qd
	join ProposalQuestionAnswer as pqa on pqa.QuestionDefinitionId = qd.Id
	where qd.QuestionId = 99 and pqa.ProposalDefinitionId = prd.Id
	-- and qd.CoverDefinition in ()
	) as 'xx',*/

-- As per requirment filter date represent : Quote create date


--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
--set @StartDate = '2016-06-09'
--set @EndDate = '2016-09-22'

DECLARE @tbl TABLE(QuestionId int, QuoteId int, AnswerId nvarchar(255), PartyId int, Created datetime, ProposalHeaderId int, ProposalDefinitionId int)
DECLARE @tblExtra TABLE(PartyId int, AssetId int, Description nvarchar(100), Value decimal(18,2), AccessoryTypeId int)

insert into @tbl
select qd.QuestionId, qte.Id as QuoteId,pqa.Answer as AnswerId, pd.PartyId, pd.Created, pd.ProposalHeaderId, pd.Id from ProposalDefinition as pd
				join ProposalQuestionAnswer as pqa on pqa.ProposalDefinitionId = pd.Id
				join QuestionDefinition as qd on qd.Id = pqa.QuestionDefinitionId
				join md.Question as q on q.Id = qd.QuestionId
				join ProposalHeader as ph on ph.Id = pd.ProposalHeaderId
				join QuoteHeader as qh on qh.ProposalHeaderId = ph.Id
				join Quote as qte on qte.QuoteHeaderId = qh.Id			
				where 
				pd.CoverId = 218 and pd.IsDeleted = 0 and qte.IsDeleted =0 and qd.id = pqa.QuestionDefinitionId and qd.QuestionId in (
				1005,9,69,73,87,142,143,210,226,227,231,223,224,245,145,230,232,234,261,296,519,867,866,871,1003,1004,1010,1160,
				873,249,41,248,862,300,135,91,154,864,863,47,90,89,786,310,255,874,97, 99, 95, 96, 94, 870, 298) --and pqa.Answer is not null

insert into @tblExtra
select ast.PartyId, ast.Id as AssetId, astEx.Description, astEx.Value, astEx.AccessoryTypeId from AssetExtras as astEx
join Asset as ast on ast.Id = astEx.AssetId
where astex.IsDeleted <> 1

--select * from @tbl 
--select * from @tblExtra

Select 

	CASE 
		WHEN Prh.Source IS NULL THEN ('Call center')   
		ELSE Prh.Source                                     
    END as Channel, 

	CASE
		WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y'
		ELSE 'N'
	END as 'QuoteExpired',

	CASE 
		WHEN PhS.Id IS NULL THEN ('N')   
		WHEN PhS.Id = 1 THEN ('N')
		WHEN PhS.Id = 2 THEN ('N')
		WHEN PhS.Id = 3 THEN ('Y')                                         
	END AS 'Quote-Bound',

	case when Qute.AcceptedOn IS NULL Then '' else CONVERT(VARCHAR(10),Qute.AcceptedOn ,111) end as 'QuoteBindDate',

	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'Date of Quote',
	
	case when (select cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2)) + cast(round (Qute.fees ,2) as numeric(36,2)) from QuoteItem as Qi		
				join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
				where qi.Id = QteI.Id and cd.CoverId in (218, 17, 45, 142, 373, 372, 258) 
	) is null
	THEN (0) 
	ELSE (select cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2)) + cast(round (Qute.fees ,2) as numeric(36,2)) from QuoteItem as Qi		
			join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
			where qi.Id = QteI.Id and cd.CoverId in (218, 17, 45, 142, 373, 372, 258) )                                          
	END 
	as 'QuoteTotalPremium',

 	CASE WHEN (select Premium from QuoteItem as Qi		
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
		where 
		cd.CoverId = 218 and qi.Id = QteI.Id
	 ) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId = 218 and qi.Id = QteI.Id)                                          
     END AS 'QuoteMotorPremium',
	 
	 CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId in (17,45) and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select  cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId in (17,45) and qi.Id = QteI.Id)                                          
     END AS 'QuoteHomePremium',

	CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId = 142 and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId = 142 and qi.Id = QteI.Id)                                          
     END AS 'QuoteFuneralPremium',	

	CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId in (373, 372, 258) and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where 
		cd.CoverId in (373, 372, 258) and qi.Id = QteI.Id)                                          
     END as 'QuoteOtherPremium',

	(cast(round(Qute.fees,2) as numeric(36,2))) as 'QuoteFees',
	(cast(round(QteI.Sasria,2) as numeric(36,2))) as 'QuoteSasria',

	(select top 1 CONVERT(VARCHAR(10),InceptionDate ,111) from PaymentDetails where PartyId = p.Id and QuoteId = qute.Id order by Id desc) as 'QuoteIncepptionDate',
		
	ph.PolicyNo as 'QuotePolicyNumber',
	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'Quote-CreateDate',
	CONVERT(VARCHAR(5),Qute.CreatedAt,108) as 'Quote-CreateTime',

	qute.Id as 'QuoteID',
	Ind.ExternalReference as 'VMSAID',

	ast.AssetNo as 'VehicleNumber',

	(select AnswerId from @tbl as temp	
	where temp.QuestionId = 89 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'NightAddress-Suburb',

	(select AnswerId from @tbl as temp	
	where temp.QuestionId = 90 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'NightAddress-PostCode',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 47 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'NightAddress-Province',
	
	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 863 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'NightAddress-Storage',

	(select AnswerId from @tbl where QuestionId = 135 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DayAddress-Suburb',

	(select AnswerId from @tbl where QuestionId = 91 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DayAddress-PostCode',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 154 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DayAddress-Province',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 864 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DayAddress-Storage',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 786 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleType',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 874 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'MotorExcess',
		

	(select AnswerId from @tbl where QuestionId = 97 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'RegistrationNumber',
		
	(select AnswerId from @tbl where QuestionId = 99 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleYear',

	(select AnswerId from @tbl where QuestionId = 95 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleMake',
	(select AnswerId from @tbl where QuestionId = 96 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleModel',
	(select AnswerId from @tbl where QuestionId = 94 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleMMCode',

	(select AnswerId from @tbl where QuestionId = 310 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)as 'VINNumber',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 227 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'Colour',

	case (select AnswerId from @tbl where QuestionId = 143 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) 						
	when 519 then 'Y' else 'N'			
	end  as 'MetallicPaint',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 230 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'CoverType',

	(case 
		(select  AnswerId from @tbl where QuestionId = 255 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Modified',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 867 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleStatus',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 226 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'ClassOfUse',

	(case 
		(select  AnswerId from @tbl where QuestionId = 232 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Delivery',
	
	Case (select  AnswerId from @tbl where QuestionId = 870 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)				
		when null then 'N' 	
		when 'false' then 'N'
		when 'true' then 'Y'	
	end  as 'TrackingDevice', 

	Case (select  AnswerId from @tbl where QuestionId = 298 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)				
		when null then 'N' 	
		when 'false' then 'N'
		when 'true' then 'Y'	
	end  as 'DiscountedTrackingDevice',	

	(case 
		(select AnswerId from @tbl where QuestionId = 245 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Immobiliser',

	(case 
		(select AnswerId from @tbl where QuestionId = 871 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'AntiHijack',

	(case 
		(select AnswerId from @tbl where QuestionId = 231 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'DataDot',
	
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 1 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 1 and AssetId = ast.Id) end as 'Acc-SoundEquipment',	
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 2 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 2 and AssetId = ast.Id) end as 'Acc-MagWheels',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 3 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 3 and AssetId = ast.Id) end as 'Acc-SunRoof',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 4 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 4 and AssetId = ast.Id) end as 'Acc-XenonLights',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 5 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 5 and AssetId = ast.Id) end as 'Acc-TowBar',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 6 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 6 and AssetId = ast.Id) end as 'Acc-BodyKit',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 7 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 7 and AssetId = ast.Id) end as 'Acc-AntiSmashAndGrab',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 8 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 8 and AssetId = ast.Id) end as 'Acc-Other',
	
	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 1005 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VAP-CarHire',

	(case 
		(select AnswerId from @tbl where QuestionId = 1004 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-TyreAndRim',

	(case 
		(select  AnswerId from @tbl where QuestionId = 1003 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-ScratchAndDent',

	(case 
		(select AnswerId from @tbl where QuestionId = 873 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-CreditShortfall',

	Ind.IdentityNo as 'DriverIDNumber',
	(select name from Md.Title where Id = Ind.TitleId) as 'DriverTitle',
	Ind.FirstName as 'DriverFirstName', 
	Ind.Surname as 'DriverSurname', 

	(select name from Md.Gender where Id = Ind.GenderId) as 'DriverGender',

	CASE WHEN 
		(select name from Md.MaritalStatus where Id = Ind.MaritalStatusId) is null
	THEN ''
	ELSE 
		(select name from Md.MaritalStatus where Id = Ind.MaritalStatusId)                                          
	END as 'DriverMaritalStatus',

	CONVERT(VARCHAR(10),Ind.DateOfBirth ,111) as 'DriverDateOfBirth',
	occ.Name as 'DriverOccupation',

	'' as 'DriverRecentCoverIndicator',

    '' as 'DriverUninterruptedCover',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 249 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DriverLicenseType',

	(select  AnswerId from @tbl where QuestionId = 41 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DriverLicenseDate',

	(case 
		(select AnswerId from @tbl where QuestionId = 248 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'DriverLicenseEndorsed',

	(case 
		(select AnswerId from @tbl where QuestionId = 224 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Automatic',

	(case 
		(select AnswerId from @tbl where QuestionId = 261 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Paraplegic',

	(case 
		(select AnswerId from @tbl where QuestionId = 223 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Amputee',

	(case 
		(select AnswerId from @tbl where QuestionId = 234 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Glasses',

	(case 
		(select AnswerId from @tbl where QuestionId = 862 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Financed',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 300 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	 as 'ValuationMethod',

	ast.SumInsured as 'SumInsured', 
	'' as 'SASRIA',	
	p.Id as 'PartyId'

from Party as P
join Lead as L on l.PartyId = p.Id
join Channel as Ch on ch.Id = p.ChannelId
left join ProposalHeader as Prh on Prh.PartyId = p.Id
left join ProposalDefinition as prd on Prd.ProposalHeaderId = Prh.Id
left join QuoteHeader as Qh on qh.ProposalHeaderId = Prh.Id
left join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
left join PolicyHeader as Ph on Ph.QuoteId = Qute.Id
left join md.PolicyStatus as PhS on Ph.PolicyStatusId = PhS.Id
left join QuoteItem as QteI on QteI.QuoteId = Qute.Id
left join ContactDetail as cd on cd.Id = P.ContactDetailId
left join Individual as Ind on P.Id = Ind.PartyId
left join Occupation as Occ on Occ.Id = Ind.OccupationId
left join Asset as ast on ast.Id = prd.AssetId
left join AssetVehicle as astV on astV.AssetId = ast.Id
join QuoteAcceptedLeadActivity as qala on qala.QuoteId = Qute.Id
join LeadActivity as la on la.Id = qala.LeadActivityId
where Qute.Id is not null
and L.IsDeleted = 0
and Qute.IsDeleted = 0
and prd.IsDeleted = 0
and ast.IsDeleted = 0
and prd.CoverId = 218
and qute.CreatedAt  BETWEEN @StartDate AND @EndDate
and la.ActivityTypeId = 5
and Qh.IsDeleted = 0
and QteI.CoverDefinitionId in (select Id from CoverDefinition where CoverId = 218)
order by p.DisplayName