﻿declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode = 'UAP')
	begin

UPDATE [dbo].[QuestionDefinition]
SET DefaultValue='20 000 000'
WHERE QuestionId=172 and CoverDefinitionId = 96;

UPDATE [dbo].[QuestionDefinition]
SET DefaultValue='5 000 000'
WHERE QuestionId=174 and CoverDefinitionId = 96;

UPDATE [dbo].[QuestionDefinition]
SET DefaultValue='30 000'
WHERE QuestionId=175 and CoverDefinitionId = 96;

UPDATE [dbo].[QuestionDefinition]
SET DefaultValue='30 000'
WHERE QuestionId=176 and CoverDefinitionId = 96;

UPDATE [dbo].[QuestionDefinition]
SET DefaultValue='30 000'
WHERE QuestionId=177 and CoverDefinitionId = 96;

UPDATE [dbo].[QuestionDefinition]
SET DefaultValue='30 000'
WHERE QuestionId=110 and CoverDefinitionId = 96;

	end
else
	begin
		return
    end