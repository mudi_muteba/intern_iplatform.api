﻿DECLARE @ChannelId int
SELECT Top 1 @ChannelId =  Id FROM Channel WHERE  Isdefault = 1

INSERT INTO ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode) VALUES ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent',@ChannelId,0,'MW')

INSERT INTO [dbo].[ChannelEventTask] ([TaskName],[ChannelEventId],[IsDeleted]) VALUES (2, (SELECT TOP 1 Id FROM ChannelEvent WHERE ProductCode = 'MW'  AND ChannelId = @ChannelId), 0)
