﻿update Report
set
	IsVisibleInReportViewer = 0,
	IsVisibleInScheduler = 0,
	ReportCategoryId = 1,
	IsActive = 1 ,
	VisibleIndex = 0--,
	--Icon = 'fa fa-question-circle-o'
where
	IsActive is null or
	IsVisibleInReportViewer is null or
	IsVisibleInScheduler is null or
	VisibleIndex is null --or
	--Icon is null or
	--Icon = ''
go