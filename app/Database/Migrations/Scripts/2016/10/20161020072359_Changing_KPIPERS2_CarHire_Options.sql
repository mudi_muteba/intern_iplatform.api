﻿DECLARE @CoverDefinitionId INT
DECLARE @id INT
DECLARE @QuestionDefintionId int

SET @id  = (SELECT TOP 1 ID FROM Product WHERE ProductCode = 'KPIPERS2')
SET @CoverDefinitionId  = (SELECT TOP 1 ID FROM [dbo].[CoverDefinition] WHERE ProductId = @id AND coverid=218)

SET @QuestionDefintionId = (Select id FROM dbo.QuestionDefinition WHERE CoverDefinitionId=222 AND questionid=1070)

UPDATE dbo.ProposalQuestionAnswer SET Answer='11291' WHERE QuestionDefinitionId=8930 AND Answer IN ('11293','11294','11295','11296')