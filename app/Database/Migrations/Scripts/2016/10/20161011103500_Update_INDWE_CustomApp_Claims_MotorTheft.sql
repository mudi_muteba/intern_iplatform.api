﻿Declare @ProductId int;

SELECT @ProductId = Id FROM Product WHERE ProductCode = 'INDWE'

if(@ProductId> 0)
BEGIN
	Update ProductClaimsQuestionDefinition Set Isdeleted = 1 WHERE ClaimsQuestionDefinitionId = 166 AND ProductId = @ProductId;
END

SET @ProductId = 0;
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'CUSTOMAPP'

if(@ProductId> 0)
BEGIN
	Update ProductClaimsQuestionDefinition Set Isdeleted = 1 WHERE ClaimsQuestionDefinitionId = 166 AND ProductId = @ProductId;
END
