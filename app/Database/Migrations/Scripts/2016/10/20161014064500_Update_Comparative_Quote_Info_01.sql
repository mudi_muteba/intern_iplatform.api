﻿update Report
set
	Name = 'Comparative Quote',
	[Description] = 'Comparative Quote',
	SourceFile = 'ComparativeQuote.trdx'
where
	Name = 'Detailed Comparative Quote Report'
go

update ReportChannelLayout
set
	[Value] = '<br><div style="font-family: Calibri; text-align: justify;"><div><div style="padding: 5px; background-color: #c0c0c0; font-weight: bold;">IMPORTANT - PLEASE NOTE</div><div style=" padding-top: 5px;"> The quote is valid for 30 days from the date of this quote and may be subject to confirmation of claims history.<br> <br>Concealment of material facts, whether specifically requested by the Company or not, will void quote without notification and although the quote forms the basis of cover, issuing of the policy is subject to a duly completed application form when applicable.</div></div> <br><div><div><div style="padding: 5px; background-color: #c0c0c0; font-weight: bold;">1) To be borne by the Insured in respect of all Accidental Damage Claims</div><div style="padding-left: 10px;"><ul><li>2.50% of vehicle Value</li><li>Minimum Amount Ksh15,000</li><li>Max. Ksh100,000</li></ul></div></div><div><div style="padding: 5px; background-color: #c0c0c0; font-weight: bold;">2) To be borne by the Insured in respect of all claims resulting from Successful Theft</div><div><div style="padding-left: 10px; padding-top: 5px;"><div><div style="text-decoration: underline;">Vehicle fitted with tracking devices</div><div style="padding-left: 10px;"><ul><li>2.50% of vehicle Value</li><li>Min Ksh20,000</li></ul></div></div><div><div style="text-decoration: underline;">Vehicle fitted with anti-theft devices</div><div style="padding-left: 10px;"><ul><li>10% of vehicle Value</li><li>Min Ksh20,000</li></ul></div></div><div><div style="text-decoration: underline;">Vehicle fitted with no anti-theft devices</div><div style="padding-left: 10px;"><ul><li>20% of vehicle Value</li><li>Min Ksh20,000</li></ul></div></div></div></div></div> <br><div><div style="padding: 5px; background-color: #c0c0c0; font-weight: bold;">3) To be borne by the insured in respect of claims resulting from Third Party Property Damage</div><div style="padding-left: 10px; padding-top: 5px;"><div><div style="text-decoration: underline;">Third Party Property Damage</div><div style="padding-left: 10px;"><ul><li>Ksh7,500</li></ul></div></div></div></div> <br><div><div style="padding: 5px; background-color: #c0c0c0; font-weight: bold;">Addendum</div><div style="padding-left: 10px; padding-top: 5px;"><div><ul><li><span style="text-decoration: underline;"><span style="text-decoration: none;">A) </span>Whilst the vehicle is being driven by any person under 21 years of age</span><ul><li>Ksh5,500</li></ul></li><li><span style="text-decoration: underline;"><span style="text-decoration: none;">B) </span>Whilst the vehicle is being driven by any person who has not held a full driving license to drive such a vehicle for at least 12 months</span><ul><li>Ksh5,500</li></ul></li></ul></div></div><div> The amount to be borne by the insured shall be the cumulative total of the excess in one of 1-3 and (a) and/or (b) where applicable. <br /> <br />It is further understood and agreed that this amount is in addition to any other amount for which the Insured may be responsible under the Terms of this Policy.</div></div> <br><div><div style="padding: 5px; background-color: #c0c0c0; font-weight: bold;">DECLARATION</div><div style="padding-top: 5px;"> You declare that to your knowledge the answers and particulars given in this proposal are true and complete and that you have not withheld any material information and that the vehicle(s) described is/are in good condition. You further agree that this proposal and declaration shall be the basis of the quotation between yourself and UAP Insurance Company Limited whose policy will be applicable to this insurance, you agree to accept.</div></div></div></div>'
where
	ReportChannelLayout.Id in
	(
		select top 1
			ReportChannelLayout.Id
		from Report
			inner join ReportChannel on ReportChannel.ReportId = Report.Id
			inner join ReportChannelLayout on ReportChannelLayout.ReportChannelId = ReportChannel.Id
			inner join ReportLayout on ReportLayout.Id = ReportChannelLayout.ReportLayoutId
			inner join ReportSection on ReportSection.Id = ReportLayout.ReportSectionId
		where
			Report.Name = 'Comparative Quote' and
			ReportChannel.ChannelId = 3 and
			ReportSection.Name = 'QuoteFooter'
	)
go