﻿
declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode = 'UAP')
	begin

declare @ProductId int
select @ProductId = id from Product where ProductCode = 'MUL'

--Motor

declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 218)

UPDATE dbo.QuestionDefinition SET IsDeleted = 1  WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=47


declare @QuestionDefinationId int
set @QuestionDefinationId = (select id from QuestionDefinition where CoverDefinitionId=@CoverDefinitionId AND QuestionId=47 )

UPDATE dbo.ProposalQuestionAnswer SET IsDeleted = 1  WHERE QuestionDefinitionId = @QuestionDefinationId

if not exists(select Id from [dbo].[QuestionDefinition] where QuestionId = 1182 and DisplayName = 'County')
	begin

INSERT [dbo].[QuestionDefinition] ( [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern] ,[GroupIndex], [QuestionDefinitionGroupTypeId],[IsDeleted],[Hide]) VALUES (0, N'County', 96, 1182, NULL, 1, 2, 1, 1, 0, N'In which county is this vehicle mainly operated in?', N'', N'',1,2,0,0)

	end

if not exists(select Id from [md].[Question] where Id = 1182 and Name = 'UAP County')
	begin

insert into md.question
(Id,Name, QuestionTypeId, QuestionGroupId)
values(1182,'UAPCounty', 3, 1) -- General Information

	end

if not exists(select Id from [md].[QuestionAnswer]  where QuestionId = 1182)

	begin

INSERT INTO md.QuestionAnswer (Id,Name, Answer, QuestionId, VisibleIndex)
VALUES	(11812,'UAPCounty-Baringo', 'Baringo', 1182, 0),
		(11813,'UAPCounty-Bomet', 'Bomet', 1182, 1),
		(11814,'UAPCounty-Bungoma', 'Bungoma', 1182, 2),
		(11767,'UAPCounty-Busia', 'Busia', 1182, 3),		
		(11768,'UAPCounty-Embu', 'Embu', 1182, 4),
		(11769,'UAPCounty-Elgeyomarakwet', 'Elgeyomarakwet', 1182, 5),
		(11811,'UAPCounty-Garissa', 'Garissa', 1182, 6),
		(11771,'UAPCounty-HomaBay', 'HomaBay', 1182, 7),
		(11772,'UAPCounty-Isiolo', 'Isiolo', 1182, 8),
		(11773,'UAPCounty-Kajiado', 'Kajiado', 1182, 9),
		(11774,'UAPCounty-Kakamega', 'Kakamega', 1182, 10),
		(11775,'UAPCounty-Kericho', 'Kericho', 1182, 11),
		(11776,'UAPCounty-Kiambu', 'Kiambu', 1182, 12),
		(11777,'UAPCounty-Kirinyaga', 'Kirinyaga', 1182, 13),
		(11778,'UAPCounty-Kisii', 'Kisii', 1182, 14),
		(11779,'UAPCounty-Kisumu', 'Kisumu', 1182, 15),
		(11780,'UAPCounty-Kitui', 'Kitui', 1182, 16),
		(11781,'UAPCounty-Kilifi', 'Kilifi', 1182, 17),
		(11782,'UAPCounty-Kwale', 'Kwale', 1182, 18),
		(11783,'UAPCounty-Lamu', 'Lamu', 1182, 19),
		(11784,'UAPCounty-Laikipia', 'Laikipia', 1182, 20),
		(11785,'UAPCounty-Machakos', 'Machakos', 1182, 21),
		(11786,'UAPCounty-Marsabit', 'Marsabit', 1182, 22),
		(11787,'UAPCounty-Makueni', 'Makueni', 1182, 23),
		(11788,'UAPCounty-Mandera', 'Mandera', 1182, 24),
		(11789,'UAPCounty-Meru', 'Meru', 1182, 25),
		(11790,'UAPCounty-Mombasa', 'Mombasa', 1182, 26),
		(11791,'UAPCounty-Migori', 'Migori', 1182, 27),
		(11792,'UAPCounty-Muranga', 'Muranga', 1182, 28),
		(11793,'UAPCounty-Nairobi', 'Nairobi', 1182, 29),
		(11794,'UAPCounty-Nakuru', 'Nakuru', 1182, 30),
		(11795,'UAPCounty-Nandi', 'Nandi', 1182, 31),
		(11796,'UAPCounty-Narok', 'Narok', 1182, 32),
		(11797,'UAPCounty-Nyamira', 'Nyamira', 1182, 33),
		(11798,'UAPCounty-Nyandarua', 'Nyandarua', 1182, 34),
		(11799,'UAPCounty-Nyeri', 'Nyeri', 1182, 35),
		(11800,'UAPCounty-Samburu', 'Samburu', 1182, 36),
		(11801,'UAPCounty-Siaya', 'Siaya', 1182, 37),
		(11803,'UAPCounty-TaitaTaveta', 'TaitaTaveta', 1182, 39),
		(11804,'UAPCounty-TransNzoia', 'TransNzoia', 1182, 40),
		(11805,'UAPCounty-Turkana', 'Turkana', 1182, 41),
		(11806,'UAPCounty-TharakaNithi', 'TharakaNithi', 1182, 42),
		(11807,'UAPCounty-UasinGishu', 'UasinGishu', 1182, 43),
		(11808,'UAPCounty-Vihiga', 'Vihiga', 1182, 44),
		(11809,'UAPCounty-Wajir', 'Wajir', 1182, 45),
		(11810,'UAPCounty-WestPokot', 'WestPokot', 1182, 46)
		
		end
		  

	end
else
	begin
		return
    end