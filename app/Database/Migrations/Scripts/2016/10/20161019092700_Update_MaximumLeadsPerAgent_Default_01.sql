﻿if exists(select * from sys.columns where Name = N'MaximumLeadsPerAgent' and object_id = object_id(N'Campaign'))
	begin
		alter table Campaign
			add constraint DF_Campaign_MaximumLeadsPerAgent
			default 5 for MaximumLeadsPerAgent

		update Campaign set MaximumLeadsPerAgent = 5
	end