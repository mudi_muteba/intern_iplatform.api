﻿declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode = 'KP')
	begin

declare @ProductId int
select @ProductId = id from Product where ProductCode = 'KPIPERS2'

--All Risk

declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 17)

UPDATE dbo.QuestionDefinition SET RequiredForQuote = 0 ,IsDeleted = 1 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=128

	end
else
	begin
		return
    end