﻿DECLARE @id INT
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverId INT
DECLARE @CoverName VARCHAR(50)
DECLARE @ProductCode VARCHAR(50)

SET @CoverId = 375 -- 375 is MOTOR WARRANTY
SET @CoverName = N'Pre-Owned Warranty'
SET @ProductCode = N'PREOWNEDWARRANTY'


SET @productid = (select top 1 ID from  Product where ProductCode = @ProductCode)

if (@productid IS NULL)
BEGIN

INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          RiskItemTypeId ,
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          @CoverName , -- Name - nvarchar(255)
          6 , -- ProductOwnerId - int
          6 , -- ProductProviderId - int
          13 , -- ProductTypeId - int
          N'123456' , -- AgencyNumber - nvarchar(255)
		  @ProductCode , -- ProductCode - nvarchar(255)
          CAST(0x00009CD400000000 AS DateTime) , -- StartDate - datetime
          CAST(0x00009CD400000000 AS DateTime) , -- EndDate - datetime
          0 , -- RiskItemTypeId - int
          0 , -- IsDeleted - bit
          N'aug.png' , -- ImageName - nvarchar(1024)
          NULL , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )

		SET @id = SCOPE_IDENTITY()
 END
 ELSE
 BEGIN
	SET @id = @productid
 END
 
 
declare @CoverDefinitionId int

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId) 

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, @CoverName, @id, @CoverId, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 1, 0, 0,
               N'I''d like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 5, 4, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Vehicle Registration Number', @CoverDefinitionId, 97, NULL, 1, 1, 1, 1, 0,
               N'What is the registration number for this vehicle?', N'' , N'', 0, NULL, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'Year Of Manufacture', N'' , N'^[0-9]+$', 5, 4, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Vehicle Make', @CoverDefinitionId, 95, NULL, 1, 3, 1, 1, 0,
               N'{Title} {Name} what is the make of the vehicle?', N'' , N'', 5, 4, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Vehicle Model', @CoverDefinitionId, 96, NULL, 1, 4, 1, 1, 0,
               N'{Title} {Name} what is the model of the vehicle?', N'' , N'', 5, 4, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Vehicle MM Code', @CoverDefinitionId, 94, NULL, 1, 5, 1, 1, 0,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'', 5, 4, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'VIN Number', @CoverDefinitionId, 1010, NULL, 1, 7, 0, 0, 0,
               N'VIN Number', N'' , N'', 0, NULL, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Engine Number', @CoverDefinitionId, 1013, NULL, 1, 8, 0, 0, 0,
               N'Engine Number', N'' , N'^[0-9]+$', 0, NULL, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=1163 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Motor Mileage', @CoverDefinitionId, 1163, NULL, 1, 140, 1, 1, 0,
               N'Vehicle Mileage', N'' , N'^[0-9]+$', 0, NULL, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 12, 1, 1, 0,
               N'Sum Insured', N'' , N'^[0-9]+$', 0, NULL, 8)
              end
			  			  
----------------Add Extended Warranty--------------------------------

SET @CoverId = 375 -- 375 is MOTOR WARRANTY, 220 Motor External
SET @CoverName = N'Extended Warranty'
SET @ProductCode = N'MOTORWARRANTY'

SET @productid = (select top 1 ID from  Product where ProductCode = @ProductCode)

if (@productid IS NULL)
BEGIN
INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          RiskItemTypeId ,
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          @CoverName , -- Name - nvarchar(255)
          6 , -- ProductOwnerId - int
          6 , -- ProductProviderId - int
          13 , -- ProductTypeId - int
          N'123456' , -- AgencyNumber - nvarchar(255)
          @ProductCode , -- ProductCode - nvarchar(255)
          CAST(0x00009CD400000000 AS DateTime) , -- StartDate - datetime
          CAST(0x00009CD400000000 AS DateTime) , -- EndDate - datetime
          0 , -- RiskItemTypeId - int
          0 , -- IsDeleted - bit
          N'aug.png' , -- ImageName - nvarchar(1024)
          NULL , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )

		SET @id = SCOPE_IDENTITY()
 END
 ELSE
 BEGIN
	SET @id = @productid
 END

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId) 

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, @CoverName, @id, @CoverId, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 1, 0, 0,
               N'I''d like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 5, 4, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Vehicle Registration Number', @CoverDefinitionId, 97, NULL, 1, 1, 1, 1, 0,
               N'What is the registration number for this vehicle?', N'' , N'', 0, NULL, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'Year Of Manufacture', N'' , N'^[0-9]+$', 5, 4, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Vehicle Make', @CoverDefinitionId, 95, NULL, 1, 3, 1, 1, 0,
               N'{Title} {Name} what is the make of the vehicle?', N'' , N'', 5, 4, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Vehicle Model', @CoverDefinitionId, 96, NULL, 1, 4, 1, 1, 0,
               N'{Title} {Name} what is the model of the vehicle?', N'' , N'', 5, 4, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Vehicle MM Code', @CoverDefinitionId, 94, NULL, 1, 5, 1, 1, 0,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'', 5, 4, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'VIN Number', @CoverDefinitionId, 1010, NULL, 1, 7, 0, 0, 0,
               N'VIN Number', N'' , N'', 0, NULL, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Engine Number', @CoverDefinitionId, 1013, NULL, 1, 8, 0, 0, 0,
               N'Engine Number', N'' , N'^[0-9]+$', 0, NULL, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=1021 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Engine CC Size', @CoverDefinitionId, 1021, NULL, 1, 10, 1, 1, 0,
               N'Engine CC Size', N'' , N'', 0, NULL, 8)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [QuestionGroupId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 12, 1, 1, 0,
               N'Sum Insured', N'' , N'^[0-9]+$', 0, NULL, 8)
              end


UPDATE dbo.Product
SET Name = @CoverName
where id = @id

UPDATE [dbo].[CoverDefinition]
SET DisplayName = @CoverName
where ProductId = @id
and CoverId = @CoverId


