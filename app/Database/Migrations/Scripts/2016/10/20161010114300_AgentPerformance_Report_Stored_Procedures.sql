﻿if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformance_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformance_Header
	end
go

create procedure Report_CallCentre_AgentPerformance_Header
(
	@CampaignID int
)
as
	select top 1 Code Insurer, '' Color from Channel where IsDefault = 1
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics
	end
go

create procedure Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics
(
	@CampaignIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
			as 
			(
				select distinct
					[User].Id AgentId,
					Campaign.Id CampaignId,
					Individual.Surname + ', ' + Individual.FirstName Agent,
					Campaign.Name Campaign
				from Campaign
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					Campaign.Id in (select * from fn_StringListToTable(@CampaignIds)) and
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
					UserAuthorisationGroup.AuthorisationGroupId = 1
				group by
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName,
					Campaign.Name
				)
			select
				Agent,
				Campaign,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
				) Quotes,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
								where
									LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo
							)

							/

							(
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								where
									LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo
							)
						)

						* 100

						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						isnull(cast(avg(QuoteItem.Premium) as numeric(18, 2)), 0)
					from QuoteHeader
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.QuoteId = Quote.Id
						inner join LeadActivity on LeadActivity.Id = QuoteAcceptedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						LeadActivity.UserId = Agents_CTE.AgentId and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) AveragePremiumPerAcceptedQuote
			from
				Agents_CTE
			order by
				Campaign,
				Agent
		end
	else
		begin
			;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
			as 
			(
				select distinct
					[User].Id AgentId,
					Campaign.Id CampaignId,
					Individual.Surname + ', ' + Individual.FirstName Agent,
					Campaign.Name Campaign
				from Campaign
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo
				group by
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName,
					Campaign.Name
				)
			select
				Agent,
				Campaign,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
					where
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
				) Quotes,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
					where
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
								where
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo
							)

							/

							(
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								where
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo
							)
						)

						* 100

						as numeric(18, 2)
					)
				) Closing,
				(
					select
						isnull(cast(avg(QuoteItem.Premium) as numeric(18, 2)), 0)
					from QuoteHeader
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.QuoteId = Quote.Id
						inner join LeadActivity on LeadActivity.Id = QuoteAcceptedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					where
						LeadActivity.UserId = Agents_CTE.AgentId and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) AveragePremiumPerAcceptedQuote
			from
				Agents_CTE
			order by
				Campaign Asc,
				Agent asc
		end
go