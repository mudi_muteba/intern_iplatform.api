﻿DECLARE @ProductId int, @ChannelId int;

SELECT @ChannelId = Id FROM Channel WHERE Code = 'INDWE' AND IsDeleted <> 1;
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'CENTND' AND IsDeleted <> 1;
if @ChannelId > 0 AND @ProductId > 0 
BEGIN

	Update SettingsQuoteUpload set SettingValue = 'PP75' WHERE SettingName = 'SAU/Upload/BrokerCode' AND ChannelId = @ChannelId AND ProductId = @ProductId;
	Update SettingsQuoteUpload set SettingValue = 'PP75' WHERE SettingName = 'SAU/Upload/fspCode' AND ChannelId = @ChannelId AND ProductId = @ProductId;

END