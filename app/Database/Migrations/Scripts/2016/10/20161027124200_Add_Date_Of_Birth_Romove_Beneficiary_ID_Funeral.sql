﻿--insert benefitiary date of birth
if not exists(select * from QuestionDefinition where CoverDefinitionId=149 and QuestionId = 1173)
BEGIN
insert into QuestionDefinition(MasterId,DisplayName,CoverDefinitionId,QuestionId,ParentId,QuestionDefinitionTypeId,VisibleIndex,
							   RequiredForQuote,RatingFactor,[ReadOnly],ToolTip,DefaultValue,RegexPattern,GroupIndex,QuestionDefinitionGroupTypeId,
							   IsDeleted,Hide,LinkQuestionId,QuestionGroupId)	
			values(0,'Beneficiary Date Of Birth',149,1173,NULL,1,10,0,0,0,'Please confirm Beneficiary date of birth',NULL,'^[0-9]+$',0,NULL,0,0,NULL,NULL)
END
go

--remove benefitiary id number on funeral proposal
if exists (select * from QuestionDefinition where CoverDefinitionId=149 and QuestionId=833)
begin
update QuestionDefinition
SET IsDeleted = 1
,RequiredForQuote = 0
,RatingFactor = 0
,Hide=1
where CoverDefinitionId=149 and QuestionId=833
end
go

--add date of birth record in proposalquestion answer table
declare @QuestionDefinitionId int
declare @QuestionDefinitionId_IDNUMBER int

select @QuestionDefinitionId = Id from QuestionDefinition where QuestionId = 1173 and CoverDefinitionId = 149
select @QuestionDefinitionId_IDNUMBER = Id from QuestionDefinition where QuestionId = 833 and CoverDefinitionId = 149

INSERT INTO ProposalQuestionAnswer (ProposalDefinitionId, QuestionDefinitionId,Answer,QuestionTypeId)
SELECT PD.Id, @QuestionDefinitionId, 
CASE WHEN LEN(PQA.Answer) = 13 
             THEN convert(date,SUBSTRING(PQA.Answer, 0, 7),102) 
             ELSE ''
END, 2 FROM ProposalDefinition PD 
left join ProposalQuestionAnswer PQA on PQA.ProposalDefinitionId = PD.Id
where PD.ProductId = 47 and PD.CoverId = 142 and PQA.QuestionDefinitionId = @QuestionDefinitionId_IDNUMBER


--update date value set to first computer time to ''
update ProposalQuestionAnswer set Answer = '' where QuestionDefinitionId = @QuestionDefinitionId and Answer = '1900-01-01'