﻿DECLARE @ChannelId int, @ProductId int;

SELECT @ChannelId = Id FROM Channel WHERE Code = 'KP' AND IsDeleted <> 1

--SAME Settings for UAT 
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue])
SELECT [ChannelId],[ProductId],[IsDeleted],'Live',[SettingName],[SettingValue] FROM SettingsQuoteUpload WHERE ChannelId = @ChannelId AND Environment = 'Dev'


--KingPrice2
Update SettingsQuoteUpload Set SettingValue = 'https://secure.kingprice.co.za/Services/PartnerService.svc' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Kingpriceobv2/Upload/ServiceURL';
Update SettingsQuoteUpload Set SettingValue = 'https://secure.kingprice.co.za/Services/PartnerService.svc' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Kingpriceobv2/Upload/ChannelEndpoint';
Update SettingsQuoteUpload Set SettingValue = 'https://secure.kingprice.co.za/IdentityServer/issue/wstrust/mixed/username' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Kingpriceobv2/Upload/SecurityEndpoint';
Update SettingsQuoteUpload Set SettingValue = 'https://secure.kingprice.co.za/Services/' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Kingpriceobv2/Upload/SecurityDestination';