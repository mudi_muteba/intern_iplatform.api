﻿--CREATE MISSING TABLES
if not exists (select * from information_schema.tables where table_name = 'ReportSection')
	begin
		CREATE TABLE [dbo].[ReportSection](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[ReportId] [int] NOT NULL,
			[Name] [nvarchar](50) NOT NULL,
			[Label] [nvarchar](50) NOT NULL,
			[IsVisible] [bit] NOT NULL,
		 CONSTRAINT [PK_ReportSection] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]


		ALTER TABLE [dbo].[ReportSection] ADD  CONSTRAINT [DF_ReportSection_IsVisible]  DEFAULT ((1)) FOR [IsVisible]
	end
go

if not exists (select * from information_schema.tables where table_name = 'ReportLayout')
	begin
		CREATE TABLE [dbo].[ReportLayout](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[ReportId] [int] NOT NULL,
			[ReportSectionId] [int] NOT NULL,
			[Name] [nvarchar](50) NOT NULL,
			[Label] [nvarchar](50) NOT NULL,
			[Value] [nvarchar](max) NULL,
			[IsVisible] [bit] NOT NULL,
		 CONSTRAINT [PK_ReportLayout] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[ReportLayout] ADD  CONSTRAINT [DF_ReportLayout_IsVisible]  DEFAULT ((1)) FOR [IsVisible]
	end
go

if not exists (select * from information_schema.tables where table_name = 'ReportChannel')
	begin
		CREATE TABLE [dbo].[ReportChannel](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[ReportId] [bigint] NOT NULL,
			[ChannelId] [int] NOT NULL,
			[Code] [nvarchar](10) NOT NULL,
			[IsActive] [bit] NOT NULL,
			[UserId] [int] NOT NULL,
			[CreatedAt] [datetime] NOT NULL,
			[ModifiedAt] [datetime] NOT NULL,
			[IsDeleted] [bit] NOT NULL,
		 CONSTRAINT [PK_ReportChannel] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[ReportChannel] ADD  CONSTRAINT [DF_ReportChannel_IsActive]  DEFAULT ((1)) FOR [IsActive]
		ALTER TABLE [dbo].[ReportChannel] ADD  CONSTRAINT [DF_ReportChannel_CreatedAt]  DEFAULT (getutcdate()) FOR [CreatedAt]
		ALTER TABLE [dbo].[ReportChannel] ADD  CONSTRAINT [DF_ReportChannel_ModifiedAt]  DEFAULT (getutcdate()) FOR [ModifiedAt]
		ALTER TABLE [dbo].[ReportChannel] ADD  CONSTRAINT [DF_ReportChannel_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]

		ALTER TABLE [dbo].[ReportChannel]  WITH CHECK ADD  CONSTRAINT [FK_ReportChannel_Channel] FOREIGN KEY([ChannelId]) REFERENCES [dbo].[Channel] ([Id])
		ALTER TABLE [dbo].[ReportChannel] CHECK CONSTRAINT [FK_ReportChannel_Channel]

		ALTER TABLE [dbo].[ReportChannel]  WITH CHECK ADD  CONSTRAINT [FK_ReportChannel_Report] FOREIGN KEY([ReportId]) REFERENCES [dbo].[Report] ([Id])
		ALTER TABLE [dbo].[ReportChannel] CHECK CONSTRAINT [FK_ReportChannel_Report]
	end
go

if not exists (select * from information_schema.tables where table_name = 'ReportChannelLayout')
	begin
		CREATE TABLE [dbo].[ReportChannelLayout](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[ReportChannelId] [int] NOT NULL,
			[ReportLayoutId] [int] NOT NULL,
			[Label] [nvarchar](50) NOT NULL,
			[Value] [nvarchar](255) NOT NULL,
			[IsVisible] [bit] NOT NULL,
			[UserId] [int] NULL,
			[CreatedAt] [datetime] NOT NULL,
			[ModifiedAt] [datetime] NOT NULL,
			[IsDeleted] [bit] NOT NULL,
		 CONSTRAINT [PK_ReportChannelLayout] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[ReportChannelLayout] ADD  CONSTRAINT [DF_ReportChannelLayout_IsVisible]  DEFAULT ((1)) FOR [IsVisible]
		ALTER TABLE [dbo].[ReportChannelLayout] ADD  CONSTRAINT [DF_ReportChannelLayout_CreatedAt]  DEFAULT (getutcdate()) FOR [CreatedAt]
		ALTER TABLE [dbo].[ReportChannelLayout] ADD  CONSTRAINT [DF_ReportChannelLayout_ModifiedAt]  DEFAULT (getutcdate()) FOR [ModifiedAt]
		ALTER TABLE [dbo].[ReportChannelLayout] ADD  CONSTRAINT [DF_ReportChannelLayout_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
		ALTER TABLE [dbo].[ReportChannelLayout]  WITH CHECK ADD  CONSTRAINT [FK_ReportChannelLayout_ReportChannel] FOREIGN KEY([ReportChannelId])
		REFERENCES [dbo].[ReportChannel] ([Id])
		ALTER TABLE [dbo].[ReportChannelLayout] CHECK CONSTRAINT [FK_ReportChannelLayout_ReportChannel]
		ALTER TABLE [dbo].[ReportChannelLayout]  WITH CHECK ADD  CONSTRAINT [FK_ReportChannelLayout_ReportLayout] FOREIGN KEY([ReportLayoutId])
		REFERENCES [dbo].[ReportLayout] ([Id])
		ALTER TABLE [dbo].[ReportChannelLayout] CHECK CONSTRAINT [FK_ReportChannelLayout_ReportLayout]
	end
go

--UPDATE STORED PROCEDURES
if exists (select * from sys.objects where object_id = object_id(N'Report_Layout_ByChannelId') and type in (N'P', N'PC'))
	begin
		drop procedure Report_Layout_ByChannelId
	end
go

create procedure Report_Layout_ByChannelId
(
	@ChannelId int,
	@ReportName varchar(50)
)
as
	select
		ReportSection.Name SectionName,
		ReportSection.Label SectionLabel,
		ReportSection.IsVisible SectionVisible,
		ReportLayout.Name FieldName,
		ReportChannelLayout.Label FieldLabel,
		ReportChannelLayout.[Value] FieldValue,
		ReportChannelLayout.IsVisible FieldVisible
	from Report
		inner join ReportChannel on ReportChannel.ReportId = Report.Id
		inner join ReportChannelLayout on ReportChannelLayout.ReportChannelId = ReportChannel.Id
		inner join ReportLayout on ReportLayout.Id = ReportChannelLayout.ReportLayoutId
		inner join ReportSection on ReportSection.Id = ReportLayout.ReportSectionId
	where
		ReportChannel.ChannelId = @ChannelId and
		Report.Name = @ReportName
	order by
		ReportChannelLayout.Id
go

if exists (select * from sys.objects where object_id = object_id(N'Report_Layout') and type in (N'P', N'PC'))
	begin
		drop procedure Report_Layout
	end
go

create procedure Report_Layout
(
	@ReportName varchar(50)
)
as
	declare @ChannelId int

	set @ChannelId =
	(
		select top 1
			ReportChannel.ChannelId
		from Report
			inner join ReportChannel on ReportChannel.ReportId = Report.Id
			inner join Channel on Channel.Id = ReportChannel.ChannelId
		where
			Channel.IsDefault = 1 and
			Report.Name = @ReportName
	)

	select @ChannelId

	if isnull(@ChannelId, 0) > 0
		begin
			exec Report_Layout_ByChannelId @ChannelId, @ReportName
		end
	else
		begin
			select
				ReportSection.Name SectionName,
				ReportSection.Label SectionLabel,
				ReportSection.IsVisible SectionVisible,
				ReportLayout.Name FieldName,
				ReportLayout.Label FieldLabel,
				ReportLayout.[Value] FieldValue,
				ReportLayout.IsVisible FieldVisible
			from Report
				inner join ReportLayout on ReportLayout.ReportId = Report.Id
				inner join ReportSection on ReportSection.Id = ReportLayout.ReportSectionId
			where
				Report.Name = @ReportName
		end
go

--ENSURE MAX LENGTH IS SET ON VALUE FIELD OF LAYOUT TABLES
alter table [ReportLayout]
	alter column [Value] varchar(max)

alter table ReportChannel
	alter column [Code] varchar(255)

alter table [ReportChannelLayout]
	alter column [Value] varchar(max)

--CLEAR REPORT SETTINGS
delete from ReportChannelLayout
delete from ReportChannel
delete from ReportSection
delete from ReportLayout
--delete from Report
go

--RESEED IDENTITY COLUMNS
dbcc checkident ('ReportChannelLayout', reseed, 0)
dbcc checkident ('ReportChannel', reseed, 0)
dbcc checkident ('ReportSection', reseed, 0)
dbcc checkident ('ReportLayout', reseed, 0)
--dbcc checkident ('Report', reseed, 0)

go

set nocount on

--VARIABLES
declare
	@ComparativeQuoteId int,
	@ChannelCursor cursor,
	@CurrentDate datetime,
	@CursorChannelId int,
	@CursorChannelCode varchar(255),	
	@ReportChannelLayoutId int,
	@QuoteHeaderId int,
	@QuoteHeaderName varchar(255),
	@QuoteHeaderLabel varchar(255),	
	@QuoteFooterId int,
	@QuoteFooterName varchar(255),
	@QuoteFooterLabel varchar(255),
	@ClientHeaderId int,
	@ClientHeaderName varchar(255),
	@ClientHeaderLabel varchar(255),
	@CompanyHeaderId int,
	@CompanyHeaderName varchar(255),
	@CompanyHeaderLabel varchar(255),
	@QuoteSummaryId int,
	@QuoteSummaryName varchar(255),
	@QuoteSummaryLabel varchar(255),
	@CostFooterId int,
	@CostFooterName varchar(255),
	@CostFooterLabel varchar(255),

	--DEFAULT QUOTE HEADER LABELS
	@DefaultQuoteCodeLabel varchar(255),
	@DefaultQuoteTitleLabel varchar(255),
	@DefaultQuoteSummaryLabel varchar(255),
	@DefaultQuoteAssetsLabel varchar(255),
	@DefaultQuoteDateLabel varchar(255),
	@DefaultQuoteExpiresLabel varchar(255),
	@DefaultQuoteNumberLabel varchar(255),
	@DefaultQuoteAdditionalExcessLabel varchar(255),
	@DefaultQuoteProductDescriptionLabel varchar(255),

	--DEFAULT CLIENT HEADER LABELS
	@DefaultClientHeaderLabel varchar(255),
	@DefaultClientGeneralNameLabel varchar(255),
	@DefaultClientGeneralIdentityLabel varchar(255),
	@DefaultClientAddressPhysicalLabel varchar(255),
	@DefaultClientAddressPostalLabel varchar(255),
	@DefaultClientContactWorkLabel varchar(255),
	@DefaultClientContactHomeLabel varchar(255),
	@DefaultClientContactCellLabel varchar(255),
	@DefaultClientContactFaxLabel varchar(255),
	@DefaultClientContactEmailLabel varchar(255),

	--DEFAULT COMPANY HEADER LABELS
	@DefaultCompanyHeaderLabel varchar(255),
	@DefaultCompanyGeneralNameLabel varchar(255),
	@DefaultCompanyAddressPhysicalLabel varchar(255),
	@DefaultCompanyAddressPostalLabel varchar(255),
	@DefaultCompanyContactWorkLabel varchar(255),
	@DefaultCompanyContactFaxLabel varchar(255),
	@DefaultCompanyContactEmailLabel varchar(255),
	@DefaultCompanyContactWebsiteLabel varchar(255),
	@DefaultCompanyAuthRegistrationLabel varchar(255),
	@DefaultCompanyAuthTaxLabel varchar(255),
	@DefaultCompanyAuthLicenseLabel  varchar(255),
	@DefaultCompanyStatutoryDisclosureLabel varchar(255),

	--DEFAULT COMPANY HEADER VALUES
	@CompanyHeaderValue varchar(255),
	@CompanyGeneralNameValue varchar(255),
	@CompanyAddressPhysicalValue varchar(255),
	@CompanyAddressPostalValue varchar(255),
	@CompanyContactWorkValue varchar(255),
	@CompanyContactFaxValue varchar(255),
	@CompanyContactEmailValue varchar(255),
	@CompanyContactWebsiteValue varchar(255),
	@CompanyAuthRegistrationValue varchar(255),
	@CompanyAuthTaxValue varchar(255),
	@CompanyAuthLicenseValue  varchar(255),
	@CompanyStatutoryDisclosureValue varchar(max),

	--DEFAULT COMPANY FOOTER VALUES
	@DefaultCompanyFooterValue varchar(max),

	--REPORT LAYOUT PRIMARY KEYS
	@ReportLayoutQuoteCodeId int,
	@ReportLayoutQuoteTitleId int,
	@ReportLayoutQuoteSummaryId int,
	@ReportLayoutQuoteAssetsId int,
	@ReportLayoutQuoteDateId int,
	@ReportLayoutQuoteExpiresId int,
	@ReportLayoutQuoteNumberId int,
	@ReportLayoutQuoteAdditionalExcessId int,
	@ReportLayoutQuoteProductDescriptionId int,
	@ReportLayoutClientHeaderId int,
	@ReportLayoutClientGeneralNameId int,
	@ReportLayoutClientGeneralIdentityId int,
	@ReportLayoutClientAddressPhysicalId int,
	@ReportLayoutClientAddressPostalId int,
	@ReportLayoutClientContactWorkId int,
	@ReportLayoutClientContactHomeId int,
	@ReportLayoutClientContactCellId int,
	@ReportLayoutClientContactFaxId int,
	@ReportLayoutClientContactEmailId int,
	@ReportLayoutCompanyHeaderId int,
	@ReportLayoutCompanyGeneralNameId int,
	@ReportLayoutCompanyAddressPhysicalId int,
	@ReportLayouCompanyAddressPostalId int,
	@ReportLayoutCompanyContactWorkId int,
	@ReportLayoutCompanyContactFaxId int,
	@ReportLayoutCompanyContactEmailId int,
	@ReportLayoutCompanyContactWebsiteId int,
	@ReportLayoutCompanyAuthRegistrationId int,
	@ReportLayoutCompanyAuthTaxId int,
	@ReportLayoutCompanyAuthLicenseId int,
	@ReportLayoutCompanyStatutoryDisclosureId int,
	@ReportLayoutQuoteFooterId int,
	@ReportLayoutQuoteSummaryFeesId int,
	@ReportLayoutQuoteSummaryTotalId int,
	@ReportLayoutCostFooterId int,

	--SCOPE IDENTITY
	@ScopeIdentity int

--CREATE COMPARATIVE QUOTE IF IT DOESN'T EXIST
select @ComparativeQuoteId = Id from Report where Name = 'Comparative Quote'

if(isnull(@ComparativeQuoteId, 0) = 0)
	begin
		print 'Comparative Quote not found, creating...'

		insert into Report (Name, Description, SourceFile, IsDeleted, IsVisibleInReportViewer)
		values ('Comparative Quote', 'Comparative Quote', 'ComparativeQuote.trdx', 0, 0)

		set @ScopeIdentity = scope_identity()
		set @ComparativeQuoteId = @ScopeIdentity

		print 'Comparative Quote created with Id ' + cast(@ScopeIdentity as varchar) + '...'
	end

--CREATE COMPARATIVE QUOTE REPORT SECTIONS
insert into ReportSection (ReportId, Name, Label) values (@ComparativeQuoteId, 'QuoteHeader', 'Quote Header')
	set @ScopeIdentity = scope_identity()
	select @QuoteHeaderId = @ScopeIdentity, @QuoteHeaderName = Name, @QuoteHeaderLabel = Label from ReportSection where Id = @ScopeIdentity

insert into ReportSection (ReportId, Name, Label) values (@ComparativeQuoteId, 'QuoteFooter', 'Quote Footer')
	set @ScopeIdentity = scope_identity()
	select @QuoteFooterId = @ScopeIdentity, @QuoteFooterName = Name, @QuoteFooterLabel = Label from ReportSection where Id = @ScopeIdentity

insert into ReportSection (ReportId, Name, Label) values (@ComparativeQuoteId, 'ClientHeader', 'Client Header')
	set @ScopeIdentity = scope_identity()
	select @ClientHeaderId = @ScopeIdentity, @ClientHeaderName = Name, @ClientHeaderLabel = Label from ReportSection where Id = @ScopeIdentity

insert into ReportSection (ReportId, Name, Label) values (@ComparativeQuoteId, 'CompanyHeader', 'Company Header')
	set @ScopeIdentity = scope_identity()
	select @CompanyHeaderId = @ScopeIdentity, @CompanyHeaderName = Name, @CompanyHeaderLabel = Label from ReportSection where Id = @ScopeIdentity

insert into ReportSection (ReportId, Name, Label) values (@ComparativeQuoteId, 'QuoteSummary', 'Quote Summary')
	set @ScopeIdentity = scope_identity()
	select @QuoteSummaryId = @ScopeIdentity, @QuoteSummaryName = Name, @QuoteSummaryLabel = Label from ReportSection where Id = @ScopeIdentity

insert into ReportSection (ReportId, Name, Label) values (@ComparativeQuoteId, 'CostFooter', 'Cost Footer')
	set @ScopeIdentity = scope_identity()
	select @CostFooterId = @ScopeIdentity, @CostFooterName = Name, @CostFooterLabel = Label from ReportSection where Id = @ScopeIdentity

--SET CURRENT DATE
set @CurrentDate = getdate()

--SET DEFAULT QUOTE HEADER LABELS
select
	@DefaultQuoteCodeLabel = 'Quote Code',
	@DefaultQuoteTitleLabel = 'Comparative Quotation',
	@DefaultQuoteSummaryLabel = 'Quote Summary',
	@DefaultQuoteAssetsLabel = 'Summary of Risk Items Quoted',
	@DefaultQuoteDateLabel = 'Current Date',
	@DefaultQuoteExpiresLabel = 'Quote Expiration Date',
	@DefaultQuoteNumberLabel = 'Quote Refrence No.',
	@DefaultQuoteAdditionalExcessLabel = 'Applicable Excess Structures',
	@DefaultQuoteProductDescriptionLabel = 'Product Description'

--SET DEFAULT CLIENT HEADER FIELD LABELS
select
	@DefaultClientHeaderLabel = 'Details of Client',
	@DefaultClientGeneralNameLabel = 'Client',
	@DefaultClientGeneralIdentityLabel = 'Identity No.',
	@DefaultClientAddressPhysicalLabel = 'Physical Address',
	@DefaultClientAddressPostalLabel = 'Postal Address',
	@DefaultClientContactWorkLabel = 'Work No.',
	@DefaultClientContactHomeLabel = 'Home No.',
	@DefaultClientContactCellLabel = 'Cell No.',
	@DefaultClientContactFaxLabel = 'Fax No.',
	@DefaultClientContactEmailLabel = 'Email Address'

--SET DEFAULT COMPANY HEADER FIELD LABELS
select
	@DefaultCompanyHeaderLabel = 'Details of Intermediary',
	@DefaultCompanyGeneralNameLabel = 'Company',
	@DefaultCompanyAddressPhysicalLabel = 'Physical Address',
	@DefaultCompanyAddressPostalLabel = 'Postal Address',
	@DefaultCompanyContactWorkLabel = 'Work No.',
	@DefaultCompanyContactFaxLabel = 'Fax No.',
	@DefaultCompanyContactEmailLabel = 'Email Address',
	@DefaultCompanyContactWebsiteLabel = 'Website',
	@DefaultCompanyAuthRegistrationLabel = 'Registration No.',
	@DefaultCompanyAuthTaxLabel = 'Tax No.',
	@DefaultCompanyAuthLicenseLabel = 'Authorised Financial Services Provider',
	@DefaultCompanyStatutoryDisclosureLabel = 'Statutory Disclosure'

--SET DEFAULT COMPANY QUOTE FOOTER VALUE
select
	@DefaultCompanyFooterValue = ''

--CONFIGURE REPORT LAYOUT -- QUOTE HEADER LABELS
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @QuoteHeaderId, 'QuoteCode', @DefaultQuoteCodeLabel, 'IP', 0)
set @ReportLayoutQuoteCodeId = scope_identity()
	
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @QuoteHeaderId, 'QuoteTitle', @DefaultQuoteTitleLabel, '', 1)
set @ReportLayoutQuoteTitleId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @QuoteHeaderId, 'QuoteSummary', @DefaultQuoteSummaryLabel, '', 1)
set @ReportLayoutQuoteSummaryId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @QuoteHeaderId, 'QuoteAssets', @DefaultQuoteAssetsLabel, '', 1)
set @ReportLayoutQuoteAssetsId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @QuoteHeaderId, 'QuoteDate', @DefaultQuoteDateLabel, '', 1)
set @ReportLayoutQuoteDateId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @QuoteHeaderId, 'QuoteExpires', @DefaultQuoteExpiresLabel, '', 1)
set @ReportLayoutQuoteExpiresId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @QuoteHeaderId, 'QuoteNumber', @DefaultQuoteNumberLabel, '', 1)
set @ReportLayoutQuoteNumberId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @QuoteHeaderId, 'QuoteAdditionalExcess', @DefaultQuoteAdditionalExcessLabel, '', 1)
set @ReportLayoutQuoteAdditionalExcessId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @QuoteHeaderId, 'QuoteProductDescription', @DefaultQuoteProductDescriptionLabel, '', 1)
set @ReportLayoutQuoteProductDescriptionId = scope_identity()

--CONFIGURE REPORT LAYOUT -- CLIENT HEADER LABELS
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @ClientHeaderId, 'ClientHeader', @DefaultClientHeaderLabel, '', 1)
set @ReportLayoutClientHeaderId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @ClientHeaderId, 'ClientGeneralName', @DefaultClientGeneralNameLabel, '', 1)
set @ReportLayoutClientGeneralNameId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @ClientHeaderId, 'ClientGeneralIdentity', @DefaultClientGeneralIdentityLabel, '', 1)
set @ReportLayoutClientGeneralIdentityId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @ClientHeaderId, 'ClientAddressPhysical', @DefaultClientAddressPhysicalLabel, '', 1)
set @ReportLayoutClientAddressPhysicalId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @ClientHeaderId, 'ClientAddressPostal', @DefaultClientAddressPostalLabel, '', 1)
set @ReportLayoutClientAddressPostalId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @ClientHeaderId, 'ClientContactWork', @DefaultClientContactWorkLabel, '', 1)
set @ReportLayoutClientContactWorkId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @ClientHeaderId, 'ClientContactHome', @DefaultClientContactHomeLabel, '', 1)
set @ReportLayoutClientContactHomeId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @ClientHeaderId, 'ClientContactCell', @DefaultClientContactCellLabel, '', 1)
set @ReportLayoutClientContactCellId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @ClientHeaderId, 'ClientContactFax', @DefaultClientContactFaxLabel, '', 1)
set @ReportLayoutClientContactFaxId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @ClientHeaderId, 'ClientContactEmail', @DefaultClientContactEmailLabel, '', 1)
set @ReportLayoutClientContactEmailId = scope_identity()

--CONFIGURE REPORT LAYOUT -- COMPANY HEADER VALUES
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CompanyHeaderId, 'CompanyHeader', @DefaultCompanyHeaderLabel, '', 1)
set @ReportLayoutCompanyHeaderId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CompanyHeaderId, 'CompanyGeneralName', @DefaultCompanyGeneralNameLabel, '', 1)
set @ReportLayoutCompanyGeneralNameId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CompanyHeaderId, 'CompanyAddressPhysical', @DefaultCompanyAddressPhysicalLabel, '', 1)
set @ReportLayoutCompanyAddressPhysicalId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CompanyHeaderId, 'CompanyAddressPostal', @DefaultCompanyAddressPostalLabel, '', 1)
set @ReportLayouCompanyAddressPostalId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CompanyHeaderId, 'CompanyContactWork', @DefaultCompanyContactWorkLabel, '', 1)
set @ReportLayoutCompanyContactWorkId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CompanyHeaderId, 'CompanyContactFax', @DefaultCompanyContactFaxLabel, '', 1)
set @ReportLayoutCompanyContactFaxId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CompanyHeaderId, 'CompanyContactEmail', @DefaultCompanyContactEmailLabel, '', 1)
set @ReportLayoutCompanyContactEmailId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CompanyHeaderId, 'CompanyContactWebsite', @DefaultCompanyContactWebsiteLabel, '', 1)
set @ReportLayoutCompanyContactWebsiteId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CompanyHeaderId, 'CompanyAuthRegistration', @DefaultCompanyAuthRegistrationLabel, '', 1)
set @ReportLayoutCompanyAuthRegistrationId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CompanyHeaderId, 'CompanyAuthTax', @DefaultCompanyAuthTaxLabel, '', 1)
set @ReportLayoutCompanyAuthTaxId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CompanyHeaderId, 'CompanyAuthLicense', @DefaultCompanyAuthLicenseLabel, '', 1)
set @ReportLayoutCompanyAuthLicenseId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CompanyHeaderId, 'CompanyStatutoryDisclosure', @DefaultCompanyStatutoryDisclosureLabel, '', 1)
set @ReportLayoutCompanyStatutoryDisclosureId = scope_identity()

--CONFIGURE REPORT LAYOUT -- QUOTE FOOTER LABELS
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @QuoteFooterId, 'QuoteFooter', '', '', 1)
set @ReportLayoutQuoteFooterId = scope_identity()

--CONFIGURE REPORT LAYOUT -- QUOTE SUMMARY LABELS
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @QuoteSummaryId, 'QuoteSummaryFees', '', '', 0)
set @ReportLayoutQuoteSummaryFeesId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @QuoteSummaryId, 'QuoteSummaryTotal', '', '', 0)
set @ReportLayoutQuoteSummaryTotalId = scope_identity()

--CONFIGURE REPORT LAYOUT -- COST FOOTER VALUES
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@ComparativeQuoteId, @CostFooterId, 'CostFooter', '', '', 1)
set @ReportLayoutCostFooterId = scope_identity()

--CREATE CURSOR TO LOOP THROUGH AVAILABLE CHANNELS
set @ChannelCursor = cursor for
	select Id, Code from Channel

open @ChannelCursor 

fetch next from @ChannelCursor into @CursorChannelId, @CursorChannelCode
	while @@fetch_status = 0
		begin
			print 'Setting up reports for ' + @CursorChannelCode

			--ADD REPORT TO DEFAULT CHANNEL
			insert into ReportChannel (ChannelId, ReportId, Code, CreatedAt, ModifiedAt, UserId) values (@CursorChannelId, @ComparativeQuoteId, @CursorChannelCode, @CurrentDate, @CurrentDate, 1)
			set @ReportChannelLayoutId  = scope_identity()

			--SET FIELD VALUES BASE ON CHANNEL CODE
			if(upper(@CursorChannelCode) = 'INDWE')
				begin
					--OVERRIDE LABELS
					select
						@DefaultCompanyAuthTaxLabel = 'VAT No.',
						@DefaultQuoteProductDescriptionLabel = 'Product Information',
						@DefaultCompanyStatutoryDisclosureLabel = 'Statutory Disclosure by Indwe'

					--OVERRIDE VALUES
					select
						@CompanyHeaderValue = 'Indwe Risk Services',
						@CompanyGeneralNameValue = 'Indwe Risk Services',
						@CompanyAddressPhysicalValue = 'Pamodzi House | 5 Willowbrook Close | Melrose North | 2196',
						@CompanyAddressPostalValue = ' ',
						@CompanyContactWorkValue = '0860 13 13 14',
						@CompanyContactFaxValue = '011 388 3617 ',
						@CompanyContactEmailValue = 'quotes@indwerisk.co.za',
						@CompanyContactWebsiteValue = 'http://www.indwe.co.za',
						@CompanyAuthRegistrationValue = '1994/004436/07',
						@CompanyAuthTaxValue = ' ',
						@CompanyAuthLicenseValue = '3425',
						@CompanyStatutoryDisclosureValue = 
							'<ol>
								<li>Indwe is an Authorised Financial Services Provider (no 3425), licensed for Short Term Insurance: Personal Lines and Short Term Insurance: Commercial Lines.</li>
								<li>Indwe has a Conflict of Interest Management Policy in place and the COI statement is available on www.indwe.co.za.</li>
								<li>This is a comparative document and is only an indication of premium per quote given. The premium may be adjusted once the final underwriting is completed by the chosen insurer.</li>
								<li>The premium and fees shown above are inclusive of VAT.</li>
							</ol>						'
				end
			else if(upper(@CursorChannelCode) = 'IP')
				begin
					--OVERRIDE VALUES
					select
						@CompanyHeaderValue = 'iPlatform',
						@CompanyGeneralNameValue = 'iPlatform',
						@CompanyAddressPhysicalValue = '1st Floor Block A, 28 on Sloane Office Park, 28 Sloane Street, Bryanston, South Africa',
						@CompanyAddressPostalValue = '',
						@CompanyContactWorkValue = '0861 988 888',
						@CompanyContactFaxValue = '0866 100 147',
						@CompanyContactEmailValue = 'quotes@iplatform.co.za',
						@CompanyContactWebsiteValue = 'http://www.iplatform.co.za',
						@CompanyAuthRegistrationValue = '1999/006725/07',
						@CompanyAuthTaxValue = '',
						@CompanyAuthLicenseValue = '',
						@CompanyStatutoryDisclosureValue = ''
				end
			else if(upper(@CursorChannelCode) = 'KP')
				begin
					--OVERRIDE VALUES
					select
						@CompanyHeaderValue = 'King Price Insurance Company Ltd.',
						@CompanyGeneralNameValue = 'King Price Insurance Company Ltd.',
						@CompanyAddressPhysicalValue = 'Menlyn Corporate Park, Block A, 3rd Floor, Corner of Garsfontein Road & Corobay Avenue, Waterkloof Glen X11, Pretoria, 0181',
						@CompanyAddressPostalValue = 'PO Box 284, Menlyn, 0063',
						@CompanyContactWorkValue = '0860 00 55 00',
						@CompanyContactFaxValue = '',
						@CompanyContactEmailValue = 'BrokerCC@kingprice.co.za',
						@CompanyContactWebsiteValue = 'http://www.kingprice.co.za',
						@CompanyAuthRegistrationValue = '2009/012496/06',
						@CompanyAuthTaxValue = '4710259724',
						@CompanyAuthLicenseValue = '43862',
						@CompanyStatutoryDisclosureValue = ''
				end
			else if(upper(@CursorChannelCode) = 'PSG')
				begin
					--OVERRIDE VALUES
					select
						@CompanyHeaderValue = 'PSG Wealth Financial Planning',
						@CompanyGeneralNameValue = 'PSG Wealth Financial Planning',
						@CompanyAddressPhysicalValue = 'Block B, Corporate 66 Office Park, 269 Vonwillich Avenue, Centurion, 0175',
						@CompanyAddressPostalValue = 'Block B, Corporate 66 Office Park, 269 Vonwillich Avenue, Centurion, 0175',
						@CompanyContactWorkValue = '0860 774 566',
						@CompanyContactFaxValue = '',
						@CompanyContactEmailValue = 'ipquotes@psg.co.za',
						@CompanyContactWebsiteValue = 'http://www.psg.co.za',
						@CompanyAuthRegistrationValue = '1999/006725/07',
						@CompanyAuthTaxValue = '4230182216',
						@CompanyAuthLicenseValue = '728',
						@CompanyStatutoryDisclosureValue = ''
				end
			else if(upper(@CursorChannelCode) = 'UAP')
				begin
					--OVERRIDE LABELS
					select
						@DefaultCompanyAuthTaxLabel = 'Pin No.',
						@DefaultCompanyFooterValue = '<br><div style="font-family: Calibri; text-align: justify;"><div><div style="padding: 5px; background-color: #c0c0c0; font-weight: bold;">IMPORTANT - PLEASE NOTE</div><div style=" padding-top: 5px;"> The quote is valid for 30 days from the date of this quote and may be subject to confirmation of claims history.<br> <br>Concealment of material facts, whether specifically requested by the Company or not, will void quote without notification and although the quote forms the basis of cover, issuing of the policy is subject to a duly completed application form when applicable.</div></div> <br><div><div><div style="padding: 5px; background-color: #c0c0c0; font-weight: bold;">1) To be borne by the Insured in respect of all Accidental Damage Claims</div><div style="padding-left: 10px;"><ul><li>2.50% of vehicle Value</li><li>Minimum Amount Ksh15,000</li><li>Max. Ksh100,000</li></ul></div></div><div><div style="padding: 5px; background-color: #c0c0c0; font-weight: bold;">2) To be borne by the Insured in respect of all claims resulting from Successful Theft</div><div><div style="padding-left: 10px; padding-top: 5px;"><div><div style="text-decoration: underline;">Vehicle fitted with tracking devices</div><div style="padding-left: 10px;"><ul><li>2.50% of vehicle Value</li><li>Min Ksh20,000</li></ul></div></div><div><div style="text-decoration: underline;">Vehicle fitted with anti-theft devices</div><div style="padding-left: 10px;"><ul><li>10% of vehicle Value</li><li>Min Ksh20,000</li></ul></div></div><div><div style="text-decoration: underline;">Vehicle fitted with no anti-theft devices</div><div style="padding-left: 10px;"><ul><li>20% of vehicle Value</li><li>Min Ksh20,000</li></ul></div></div></div></div></div> <br><div><div style="padding: 5px; background-color: #c0c0c0; font-weight: bold;">3) To be borne by the insured in respect of claims resulting from Third Party Property Damage</div><div style="padding-left: 10px; padding-top: 5px;"><div><div style="text-decoration: underline;">Third Party Property Damage</div><div style="padding-left: 10px;"><ul><li>Ksh7,500</li></ul></div></div></div></div> <br><div><div style="padding: 5px; background-color: #c0c0c0; font-weight: bold;">Addendum</div><div style="padding-left: 10px; padding-top: 5px;"><div><ul><li><span style="text-decoration: underline;"><span style="text-decoration: none;">A) </span>Whilst the vehicle is being driven by any person under 21 years of age</span><ul><li>Ksh5,500</li></ul></li><li><span style="text-decoration: underline;"><span style="text-decoration: none;">B) </span>Whilst the vehicle is being driven by any person who has not held a full driving license to drive such a vehicle for at least 12 months</span><ul><li>Ksh5,500</li></ul></li></ul></div></div><div> The amount to be borne by the insured shall be the cumulative total of the excess in one of 1-3 and (a) and/or (b) where applicable. <br /> <br />It is further understood and agreed that this amount is in addition to any other amount for which the Insured may be responsible under the Terms of this Policy.</div></div> <br><div><div style="padding: 5px; background-color: #c0c0c0; font-weight: bold;">DECLARATION</div><div style="padding-top: 5px;"> You declare that to your knowledge the answers and particulars given in this proposal are true and complete and that you have not withheld any material information and that the vehicle(s) described is/are in good condition. You further agree that this proposal and declaration shall be the basis of the quotation between yourself and UAP Insurance Company Limited whose policy will be applicable to this insurance, you agree to accept.</div></div></div></div>'

					--OVERRIDE VALUES
					select
						@CompanyHeaderValue = 'UAP Insurance Company Limited',
						@CompanyGeneralNameValue = 'UAP Insurance Company Limited',
						@CompanyAddressPhysicalValue = 'Bishop Gardens Towers, Bishop Ropad, Nairobi',
						@CompanyAddressPostalValue = 'P.O. Box 43013-00100, Nairobi, Kenya',
						@CompanyContactWorkValue = '+254 20 285 0000',
						@CompanyContactFaxValue = '+254 20 285 0000',
						@CompanyContactEmailValue = 'easydirect@uap-group.com',
						@CompanyContactWebsiteValue = 'http://www.uap-group.com | http://www.easydirect.co.ke',
						@CompanyAuthRegistrationValue = 'C.17074',
						@CompanyAuthTaxValue = 'P000609359J',
						@CompanyAuthLicenseValue = 'IRA01/030/0',
						@CompanyStatutoryDisclosureValue = ''
				end
			else
				begin
					--OVERRIDE VALUES
					select
						@CompanyHeaderValue = 'iPlatform',
						@CompanyGeneralNameValue = 'iPlatform',
						@CompanyAddressPhysicalValue = '1st Floor Block A, 28 on Sloane Office Park, 28 Sloane Street, Bryanston, South Africa',
						@CompanyAddressPostalValue = '',
						@CompanyContactWorkValue = '0861 988 888',
						@CompanyContactFaxValue = '0866 100 147',
						@CompanyContactEmailValue = 'quotes@iplatform.co.za',
						@CompanyContactWebsiteValue = 'http://www.iplatform.co.za',
						@CompanyAuthRegistrationValue = '1999/006725/07',
						@CompanyAuthTaxValue = '',
						@CompanyAuthLicenseValue = '',
						@CompanyStatutoryDisclosureValue = ''
				end

			--CONFIGURE REPORT LAYOUT -- QUOTE HEADER LABELS
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteCodeId, @DefaultQuoteCodeLabel, @CursorChannelCode, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteTitleId, @DefaultQuoteTitleLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteSummaryId, @DefaultQuoteSummaryLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteAssetsId, @DefaultQuoteAssetsLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteDateId, @DefaultQuoteDateLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteExpiresId, @DefaultQuoteExpiresLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteNumberId, @DefaultQuoteNumberLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteAdditionalExcessId, @DefaultQuoteAdditionalExcessLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteProductDescriptionId, @DefaultQuoteProductDescriptionLabel, '', 1)

			--CONFIGURE REPORT LAYOUT -- CLIENT HEADER LABELS
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientHeaderId, @DefaultClientHeaderLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientGeneralNameId, @DefaultClientGeneralNameLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientGeneralIdentityId, @DefaultClientGeneralIdentityLabel, '', 0)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientAddressPhysicalId, @DefaultClientAddressPhysicalLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientAddressPostalId, @DefaultClientAddressPostalLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientContactWorkId, @DefaultClientContactWorkLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientContactHomeId, @DefaultClientContactHomeLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientContactCellId, @DefaultClientContactCellLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientContactFaxId, @DefaultClientContactFaxLabel, '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientContactEmailId, @DefaultClientContactEmailLabel, '', 1)

			--CONFIGURE REPORT LAYOUT -- COMPANY HEADER VALUES
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyHeaderId, @DefaultCompanyHeaderLabel, @CompanyHeaderValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyGeneralNameId, @DefaultCompanyGeneralNameLabel, @CompanyGeneralNameValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyAddressPhysicalId, @DefaultCompanyAddressPhysicalLabel, @CompanyAddressPhysicalValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayouCompanyAddressPostalId, @DefaultCompanyAddressPostalLabel, @CompanyAddressPostalValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyContactWorkId, @DefaultCompanyContactWorkLabel, @CompanyContactWorkValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyContactFaxId, @DefaultCompanyContactFaxLabel, ' ', 0)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyContactEmailId, @DefaultCompanyContactEmailLabel, @CompanyContactEmailValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyContactWebsiteId, @DefaultCompanyContactWebsiteLabel, @CompanyContactWebsiteValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyAuthRegistrationId, @DefaultCompanyAuthRegistrationLabel, @CompanyAuthRegistrationValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyAuthTaxId, @DefaultCompanyAuthTaxLabel, @CompanyAuthTaxValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyAuthLicenseId, @DefaultCompanyAuthLicenseLabel, @CompanyAuthLicenseValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyStatutoryDisclosureId, @DefaultCompanyStatutoryDisclosureLabel, @CompanyStatutoryDisclosureValue, 1)

			--QUOTE FOOTER VALUES
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteFooterId, '', @DefaultCompanyFooterValue, 1)

			--QUOTE SUMMARY VALUES
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteSummaryFeesId, '', '', 0)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteSummaryTotalId, '', '', 0)

			--COST FOOTER VALUES
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCostFooterId, '', '', 1)

			fetch next from @ChannelCursor into @CursorChannelId, @CursorChannelCode
		end

close @ChannelCursor
deallocate @ChannelCursor

print 'Done...'

set nocount off


go