﻿DECLARE @ChannelId int, @PSGChannelId int;

SELECT @ChannelId = Id FROM Channel WHERE Code = 'PSGCC' AND IsDeleted <> 1
SELECT @PSGChannelId = Id FROM Channel WHERE Code = 'PSG' AND IsDeleted <> 1

if @ChannelId > 0 AND @PSGChannelId > 0
BEGIN
	if not exists (SELECT * FROM ChannelEvent WHERE ChannelId = @ChannelId)
	BEGIN
		INSERT INTO ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode)
		SELECT EventName, @ChannelId, IsDeleted, ProductCode FROM ChannelEvent WHERE ChannelId = @PSGChannelId AND IsDeleted = 0
	End
	if not exists (SELECT * FROM ChannelEventTask WHERE ChannelEventId in (SELECT Id FROM ChannelEvent WHERE Id = @ChannelId))
	BEGIN
		INSERT INTO ChannelEventTask (TaskName, ChannelEventId, IsDeleted)
		SELECT 2,  Id, 0 FROM ChannelEvent WHERE ChannelId = @ChannelId AND IsDeleted <> 1
	END
	if not exists (SELECT * FROM EmailCommunicationSetting WHERE ChannelId = @ChannelId)
	BEGIN
		INSERT INTO EmailCommunicationSetting ([Host],[Port],[UseSSL],[UseDefaultCredentials],[Username],[Password],[DefaultFrom],[DefaultBCC],[DefaultContactNumber],[ChannelId], [subAccountID])
			VALUES('smtp.gmail.com', '587', 'true', 'false', 'iplatform@iplatform.co.za', 'p8nynyocRLhoCeN2wdoN8A', 'iPlatform<iplatform@platform.co.za>', '','0860 774 566 (use option 3)', @ChannelId, 'QuoteAcceptance')
	END
END