﻿DECLARE @ChannelId int, @ProductId int;

SELECT @ChannelId = Id FROM Channel WHERE Code = 'UAP' AND IsDeleted <> 1

--IWYZEMOTORC (IDS)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'IWYZEMOTORC' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/ServiceURL','http://195.202.66.179/STPWS/Command.svc'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/subject','UPLOADING LEAD TO IDS'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/template','LeadUploadTemplate')

--IWYZEMOTORTPF (IDS)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'IWYZEMOTORTPF' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/ServiceURL','http://195.202.66.179/STPWS/Command.svc'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/subject','UPLOADING LEAD TO IDS'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/template','LeadUploadTemplate')

--IWYZEMOTORTO (IDS)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'IWYZEMOTORTO' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/ServiceURL','http://195.202.66.179/STPWS/Command.svc'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/subject','UPLOADING LEAD TO IDS'),
(@ChannelId, @ProductId,0, 'Dev','IDS/Upload/template','LeadUploadTemplate')

--SAME Settings for UAT 
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue])
SELECT [ChannelId],[ProductId],[IsDeleted],'UAT',[SettingName],[SettingValue] FROM SettingsQuoteUpload WHERE ChannelId = @ChannelId AND Environment = 'Dev'

--SAME Settings for Staging 
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue])
SELECT [ChannelId],[ProductId],[IsDeleted],'Staging',[SettingName],[SettingValue] FROM SettingsQuoteUpload WHERE ChannelId = @ChannelId AND Environment = 'Dev'