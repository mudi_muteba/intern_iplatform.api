﻿update Report
set
	[Description] = 'This report tracks the leads loaded per agent and groups the outcomes depending on the action attached to the lead. I.E. Pending \ Uncontactable \ Unactioned'
where
	Name = 'Lead Management'