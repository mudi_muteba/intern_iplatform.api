﻿INSERT INTO ChannelSetting (ChannelId,  ChannelSettingTypeId, ChannelSettingName, ChannelSettingValue, IsDeleted) VALUES
(1,  1, 'InsurerCode', 'AIG', 0),
(2,  1, 'InsurerCode', 'PSG', 0),
(3,  1, 'InsurerCode', 'UAP', 0),
(4,  1, 'InsurerCode', 'HOL', 0),
(5,  1, 'InsurerCode', 'KP', 0),
(7,  1, 'InsurerCode', 'IP', 0),
(8,  1, 'InsurerCode', 'INDWE', 0),
(9,  1, 'InsurerCode', 'BIDVEST', 0),
(10,  1, 'InsurerCode', 'PSG', 0),
(11,  1, 'InsurerCode', 'TELESURE', 0),
(12,  1, 'InsurerCode', 'MIWAY', 0),
(13,  1, 'InsurerCode', 'BOWER', 0)