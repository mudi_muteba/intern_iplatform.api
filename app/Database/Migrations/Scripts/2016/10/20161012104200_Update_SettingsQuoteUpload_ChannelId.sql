﻿--remove reference 
if exists(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_SettingsQuoteUpload_Channel_ChannelId')
BEGIN 
	ALTER TABLE [dbo].[SettingsQuoteUpload] DROP CONSTRAINT [FK_SettingsQuoteUpload_Channel_ChannelId];
END

--drop column
ALTER TABLE [dbo].[SettingsQuoteUpload] drop COLUMN ChannelId;
--recreate column
ALTER TABLE [dbo].[SettingsQuoteUpload] Add ChannelId int not null;
-- add constraint
ALTER TABLE [dbo].[SettingsQuoteUpload] WITH CHECK ADD CONSTRAINT [FK_SettingsQuoteUpload_Channel_ChannelId] FOREIGN KEY([ChannelId]) REFERENCES [dbo].[Channel] ([Id]);
-- check constraint
ALTER TABLE [dbo].[SettingsQuoteUpload] CHECK CONSTRAINT [FK_SettingsQuoteUpload_Channel_ChannelId]