﻿DECLARE @ChannelId int, @ProductId int;

SET @ChannelId = 13;

--SAME Settings for UAT 
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue])
SELECT [ChannelId],[ProductId],[IsDeleted],'Live',[SettingName],[SettingValue] FROM SettingsQuoteUpload WHERE ChannelId = @ChannelId AND Environment = 'Dev'

--Telesure
Update SettingsQuoteUpload Set SettingValue = 'https://api.telesure.co.za/quickquotes/APIService.asmx' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Telesureag/Upload/ServiceURL';
Update SettingsQuoteUpload Set SettingValue = 'http://rate.iplatform.co.za/' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Telesureag/Upload/iRateUrl';
Update SettingsQuoteUpload Set SettingValue = 'https://api.telesure.co.za/quickquotes/APIService.asmx' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Telesureffw/Upload/ServiceURL';
Update SettingsQuoteUpload Set SettingValue = 'http://rate.iplatform.co.za/' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Telesureffw/Upload/iRateUrl';
Update SettingsQuoteUpload Set SettingValue = 'https://api.telesure.co.za/quickquotes/APIService.asmx' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Telesureunity/Upload/ServiceURL';
Update SettingsQuoteUpload Set SettingValue = 'http://rate.iplatform.co.za/' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Telesureunity/Upload/iRateUrl';

--Oakhurst
Update SettingsQuoteUpload Set SettingValue = 'http://196.212.18.130/OaksureWS/service.asmx' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Oakhurst/Upload/ServiceURL';

--KingPrice2
Update SettingsQuoteUpload Set SettingValue = 'https://secure.kingprice.co.za/Services/PartnerService.svc' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Kingpriceobv2/Upload/ServiceURL';
Update SettingsQuoteUpload Set SettingValue = 'https://secure.kingprice.co.za/Services/PartnerService.svc' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Kingpriceobv2/Upload/ChannelEndpoint';
Update SettingsQuoteUpload Set SettingValue = 'https://secure.kingprice.co.za/IdentityServer/issue/wstrust/mixed/username' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Kingpriceobv2/Upload/SecurityEndpoint';
Update SettingsQuoteUpload Set SettingValue = 'https://secure.kingprice.co.za/Services/' WHERE ChannelId = @ChannelId AND Environment = 'Live' AND SettingName = 'Kingpriceobv2/Upload/SecurityDestination';