﻿if not exists(select * from Channel where Id=13)
begin
insert into Channel(Id,SystemId,IsActive,ActivatedOn,IsDefault,IsDeleted,CurrencyId,[DateFormat],Name,LanguageId,PasswordStrengthEnabled,Code,CountryId)
values (13,'0D52E817-651A-4939-99AB-611FDCDA8F59',1,'2016-10-25 14:00:00.000',0,0,1,'dd MMM yyyy','BOWER',2,0,'BOWER',197)
end

DECLARE @Channel int, @PartyId int, @OrgId int;
SELECT @Channel = Id FROM Channel WHERE Code = 'BOWER';
IF @Channel > 0
BEGIN
	INSERT INTO PARTY (MasterId, PartyTypeId, ContactDetailId, DisplayName, DateCreated, DateUpdated, IsDeleted, ChannelId, ExternalReference, Audit)
	VALUES (0, 2, null, 'BOWER', GETDATE(), null, 0, @Channel, null, null);

	SET @PartyId = 0
	SELECT @PartyId = @@IDENTITY;
	INSERT INTO Organization (PartyId, Code, RegisteredName, TradingName, TradingSince, [Description], RegNo, FspNo, VatNo, ShortTermInsurers,LongTermInsurers, TemporaryFSP, CaptureLeadTransferComment)
	VALUES (@PartyId, 'BOWER', 'Bower Insurance Brokers (Pty) Ltd', 'Bower Insurance Brokers', '1996-01-01', '','','', '', 0, 0, 0,0 )

	Update Channel SET OrganizationId = @PartyId  WHERE Id = @Channel;

	SET @Channel = 0
END
