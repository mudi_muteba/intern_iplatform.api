﻿DECLARE @ChannelId int, @ProductId int;

SELECT @ChannelId = Id FROM Channel WHERE Code = 'IP' AND IsDeleted <> 1

--OAKHURST (Oakhurst)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'OAKHURST' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/ServiceURL','http://apps.softsure.co.za/OaksureUATWS/Service.asmx'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/User','PSG00001'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/Password','C0v3rM3'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/AgentCode','PSGD'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/AgentName','PSGD'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/AgentTelephone','0861988888'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/BrokerCode','PSGD'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/SubBrokerCode','PSGD'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/SubPartnerCode','PSGD'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/SubPartnerName','PSGD'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/SchemeName','LITE'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/subject','UPLOADING LEAD TO OAKHURST'),
(@ChannelId, @ProductId,0, 'Dev','Oakhurst/Upload/template','LeadUploadTemplate')

--AUGPRD (Telesure)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'AUGPRD' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/ServiceURL','https://preview.telesure.co.za/api/quickquotes/apiservice.asmx'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/User','ams'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/Password','@m$'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/iRateUrl','http://psg-uat-rate.iplatform.co.za/'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/subject','UPLOADING LEAD TO TELESURE'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/template','LeadUploadTemplate'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/InsurerCode','AUG'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/ProductCode','AUGPRD'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/BrokerCode','PSGDIR'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/CompanyCode','01'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/UwCompanyCode','01'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/UwProductCode','11'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/AuthCode','12'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/Token','13'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/SchemeCode','9'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/AgentCode','MAPSG'),
(@ChannelId, @ProductId,0, 'Dev','Telesureag/Upload/SubscriberCode','PSA')

--FIR (Telesure)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'FIR' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/ServiceURL','https://preview.telesure.co.za/api/quickquotes/apiservice.asmx'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/User','ams'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/Password','@m$'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/iRateUrl','http://psg-uat-rate.iplatform.co.za/'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/subject','UPLOADING LEAD TO TELESURE'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/template','LeadUploadTemplate'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/InsurerCode','FFW'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/ProductCode','FIR'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/BrokerCode','FPSGDIR'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/CompanyCode','15'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/UwCompanyCode','15'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/UwProductCode','64'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/AuthCode','65'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/Token','66'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/SchemeCode','2'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/AgentCode','MFPSG'),
(@ChannelId, @ProductId,0, 'Dev','Telesureffw/Upload/SubscriberCode','PSF')

--UNI (Telesure)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'UNI' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/ServiceURL','https://preview.telesure.co.za/api/quickquotes/apiservice.asmx'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/User','ams'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/Password','@m$'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/iRateUrl','http://psg-uat-rate.iplatform.co.za/'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/subject','UPLOADING LEAD TO TELESURE'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/template','LeadUploadTemplate'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/InsurerCode','UNITY'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/ProductCode','UNI'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/BrokerCode','UPSGDIR'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/CompanyCode','13'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/UwCompanyCode','02'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/UwProductCode','14'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/AuthCode','15'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/Token','16'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/SchemeCode','12'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/AgentCode','MUPSG'),
(@ChannelId, @ProductId,0, 'Dev','Telesureunity/Upload/SubscriberCode','PSU')

--KPIPERS2 (Kingprice)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'KPIPERS2' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/ServiceURL','https://secure.kingprice.co.za/ServicesUAT/PartnerService.svc'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/ChannelEndpoint','https://secure.kingprice.co.za/ServicesUAT/PartnerService.svc'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/SecurityEndpoint','https://secure.kingprice.co.za/IdentityServer/issue/wstrust/mixed/username'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/SecurityDestination','https://secure.kingprice.co.za/ServicesUAT/'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/subject','UPLOADING LEAD TO KINGPRICE'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/template','LeadUploadTemplate'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/InsurerCode','KPI'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/ProductCode','KPIPERS2'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/UserName','iPlatform_APIPartner'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/Password','h8kPZp3RMU4f'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/Token','API-9717E8459DCE4149A77ED02DA6CC245D'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/AgentCode','IPLATFORM'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/BrokerCode','FC34F169-84AF-4A34-9321-A60B00C164CC'),
(@ChannelId, @ProductId,0, 'Dev','Kingpriceobv2/Upload/CompanyCode','Psg Wealth Financial Planning (Pty) Ltd  [Iplat]')

--CENTND (SAU)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'CENTND' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/ServiceURL','http://services.sauonline.co.za/blackbox/quote.asmx'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/subject','UPLOADING LEAD TO SAU'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/template','LeadUploadTemplate'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/UserName','CIMS'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/Password','C!m5@SAU'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/BrokerCode','MC12'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/fspCode','728'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/AppName','IPLATFORM'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/CompanyCode','Cardinal')

--CENTRD (SAU)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'CENTRD' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/ServiceURL','http://services.sauonline.co.za/blackbox/quote.asmx'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/subject','UPLOADING LEAD TO SAU'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/template','LeadUploadTemplate'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/UserName','CIMS'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/Password','C!m5@SAU'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/BrokerCode','MC12'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/fspCode','728'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/AppName','IPLATFORM'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/CompanyCode','Cardinal')

--SNTMND (SAU)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'SNTMND' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/ServiceURL','http://services.sauonline.co.za/blackbox/quote.asmx'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/subject','UPLOADING LEAD TO SAU'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/template','LeadUploadTemplate'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/UserName','CIMS'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/Password','C!m5@SAU'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/BrokerCode','MC12'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/fspCode','728'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/AppName','IPLATFORM'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/CompanyCode','Cardinal')

--SNTMRD (SAU)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'SNTMRD' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/ServiceURL','http://services.sauonline.co.za/blackbox/quote.asmx'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/subject','UPLOADING LEAD TO SAU'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/template','LeadUploadTemplate'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/UserName','CIMS'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/Password','C!m5@SAU'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/BrokerCode','MC12'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/fspCode','728'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/AppName','IPLATFORM'),
(@ChannelId, @ProductId,0, 'Dev','SAU/Upload/CompanyCode','Cardinal')

--MW (MiWay)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'MW' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/ServiceURL','http://197.97.185.243/api/uat'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/subject','UPLOADING LEAD TO MiWay'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/template','LeadUploadTemplate'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/AgentFullName','BIDVEST'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/AgentId','34011419'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/UserId','BIDVEST'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/ProductId','RB'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/SiteSequence','2'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/SourceSystemId','CARDINAL'),
(@ChannelId, @ProductId,0, 'Dev','MiWay/Upload/ApiKey','d293d2ViYTp3b3d3ZWJhNzg5dWF0')

--AAC (AA)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'AAC' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/ServiceURL','http://no_url_for_now'),
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/subject','UPLOADING LEAD TO AA'),
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/template','LeadUploadTemplate')

--AA (AA)
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'AA' AND IsDeleted <> 1
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES 
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/ServiceURL','http://no_url_for_now'),
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/ErrorEmailAddress','monitoring@iplatform.co.za'),
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/RetryDelayIncrement','5'),
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/MaxRetries','2'),
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/subject','UPLOADING LEAD TO AA'),
(@ChannelId, @ProductId,0, 'Dev','AA/Upload/template','LeadUploadTemplate')

--SAME Settings for UAT 
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue])
SELECT [ChannelId],[ProductId],[IsDeleted],'UAT',[SettingName],[SettingValue] FROM SettingsQuoteUpload WHERE ChannelId = @ChannelId AND Environment = 'Dev'

--SAME Settings for STAGING 
INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue])
SELECT [ChannelId],[ProductId],[IsDeleted],'Staging',[SettingName],[SettingValue] FROM SettingsQuoteUpload WHERE ChannelId = @ChannelId AND Environment = 'Dev'

