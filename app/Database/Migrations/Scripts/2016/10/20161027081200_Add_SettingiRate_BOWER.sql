﻿DECLARE @ProductId int

SELECT @ProductId = Id FROM Product WHERE ProductCode = 'KPIPERS2';

If @ProductId > 0 
BEGIN
	if Exists(SELECT * FROM SettingsiRate WHERE ProductCode = 'KPIPERS2' AND ChannelId = '0D52E817-651A-4939-99AB-611FDCDA8F59')
		BEGIN
			Update SettingsiRate set ProductId = @ProductId WHERE ProductCode = 'KPIPERS2' AND ChannelId = '0D52E817-651A-4939-99AB-611FDCDA8F59'
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[SettingsiRate]([ProductId],[Password],[AgentCode],[BrokerCode],[AuthCode],[SchemeCode],[Token],[ChannelId],[Environment],[UserId],[SubscriberCode],[CompanyCode],[UwCompanyCode],[UwProductCode],[IsDeleted],[ProductCode]) VALUES
			(@ProductId, 'h8kPZp3RMU4f', 'IPLATFORM', 'E2CBB7A1-19E7-4100-B861-A69D01044DBF', NULL, NULL, 'API-9717E8459DCE4149A77ED02DA6CC245D', '0D52E817-651A-4939-99AB-611FDCDA8F59', '', 'iPlatform_APIPartner', NULL, 'Bower Insurance Brokers (Pty) Ltd [Iplat]', NULL, NULL, 0, 'KPIPERS2');
		END
	SET @ProductId = 0;
END

SELECT @ProductId = Id FROM Product WHERE ProductCode = 'AUGPRD';

If @ProductId > 0 
BEGIN
	if Exists(SELECT * FROM SettingsiRate WHERE ProductCode = 'AUGPRD' AND ChannelId = '0D52E817-651A-4939-99AB-611FDCDA8F59')
		BEGIN
			Update SettingsiRate set ProductId = @ProductId WHERE ProductCode = 'AUGPRD' AND ChannelId = '0D52E817-651A-4939-99AB-611FDCDA8F59'
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[SettingsiRate]([ProductId],[Password],[AgentCode],[BrokerCode],[AuthCode],[SchemeCode],[Token],[ChannelId],[Environment],[UserId],[SubscriberCode],[CompanyCode],[UwCompanyCode],[UwProductCode],[IsDeleted],[ProductCode]) VALUES
			(@ProductId, '!ndW3', 'BRAIP', 'MJ1560', '12', '9', '13', '0D52E817-651A-4939-99AB-611FDCDA8F59', '', 'Indwe', 'IWA', '01', '01', '11', 0, 'AUGPRD');
		END
	SET @ProductId = 0;
END

SELECT @ProductId = Id FROM Product WHERE ProductCode = 'FIR';
If @ProductId > 0 
BEGIN
	if Exists(SELECT * FROM SettingsiRate WHERE ProductCode = 'FIR' AND ChannelId = '0D52E817-651A-4939-99AB-611FDCDA8F59')
		BEGIN
			Update SettingsiRate set ProductId = @ProductId WHERE ProductCode = 'FIR' AND ChannelId = '0D52E817-651A-4939-99AB-611FDCDA8F59'
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[SettingsiRate]([ProductId],[Password],[AgentCode],[BrokerCode],[AuthCode],[SchemeCode],[Token],[ChannelId],[Environment],[UserId],[SubscriberCode],[CompanyCode],[UwCompanyCode],[UwProductCode],[IsDeleted],[ProductCode]) VALUES
			(@ProductId, '!ndW3', 'BRFIP', 'U2566', '65', '2', '66', '0D52E817-651A-4939-99AB-611FDCDA8F59', '', 'Indwe', 'IWF', '15', '15', '64', 0, 'FIR');
		END
	SET @ProductId = 0;
END


SELECT @ProductId = Id FROM Product WHERE ProductCode = 'UNI';
If @ProductId > 0 
BEGIN
	if Exists(SELECT * FROM SettingsiRate WHERE ProductCode = 'UNI' AND ChannelId = '0D52E817-651A-4939-99AB-611FDCDA8F59')
		BEGIN
			Update SettingsiRate set ProductId = @ProductId WHERE ProductCode = 'UNI' AND ChannelId = '0D52E817-651A-4939-99AB-611FDCDA8F59'
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[SettingsiRate]([ProductId],[Password],[AgentCode],[BrokerCode],[AuthCode],[SchemeCode],[Token],[ChannelId],[Environment],[UserId],[SubscriberCode],[CompanyCode],[UwCompanyCode],[UwProductCode],[IsDeleted],[ProductCode]) VALUES
			(@ProductId, '!ndW3', 'BRUIP', 'FFW0930', '15', '12', '16', '0D52E817-651A-4939-99AB-611FDCDA8F59', '', 'Indwe', 'IWU', '13', '02', '14', 0, 'UNI');
		END
	SET @ProductId = 0;
END

SELECT @ProductId = Id FROM Product WHERE ProductCode = 'VIRS';
If @ProductId > 0 
BEGIN
	if Exists(SELECT * FROM SettingsiRate WHERE ProductCode = 'VIRS' AND ChannelId = '0D52E817-651A-4939-99AB-611FDCDA8F59')
		BEGIN
			Update SettingsiRate set ProductId = @ProductId WHERE ProductCode = 'VIRS' AND ChannelId = '0D52E817-651A-4939-99AB-611FDCDA8F59'
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[SettingsiRate]([ProductId],[Password],[AgentCode],[BrokerCode],[AuthCode],[SchemeCode],[Token],[ChannelId],[Environment],[UserId],[SubscriberCode],[CompanyCode],[UwCompanyCode],[UwProductCode],[IsDeleted],[ProductCode]) VALUES
			(@ProductId, '!ndW3', 'BRVIP', 'VIR050', '12', '20', '13', '0D52E817-651A-4939-99AB-611FDCDA8F59', '', 'Indwe', 'IWV', '34', '01', '11', 0, 'VIRS');
		END
	SET @ProductId = 0;
END

SELECT @ProductId = Id FROM Product WHERE ProductCode = 'CENTND';
If @ProductId > 0 
BEGIN
	if Exists(SELECT * FROM SettingsiRate WHERE ProductCode = 'SAUMASTER' AND ChannelId = '0D52E817-651A-4939-99AB-611FDCDA8F59')
		BEGIN
			Update SettingsiRate set ProductId = @ProductId WHERE ProductCode = 'SAUMASTER' AND ChannelId = '0D52E817-651A-4939-99AB-611FDCDA8F59'
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[SettingsiRate]([ProductId],[Password],[AgentCode],[BrokerCode],[AuthCode],[SchemeCode],[Token],[ChannelId],[Environment],[UserId],[SubscriberCode],[CompanyCode],[UwCompanyCode],[UwProductCode],[IsDeleted],[ProductCode]) VALUES
			(@ProductId, 'C!m5@SAU', 'BI75', 'BI75', NULL, NULL, NULL, '0D52E817-651A-4939-99AB-611FDCDA8F59', 'CIMS', 'CIMS', NULL, NULL, NULL, NULL, 0, 'SAUMASTER');
		END
	SET @ProductId = 0;
END
