﻿DECLARE @OrgId int, @INDWEOrgId int, @ClaimsTypeId int, @ClaimsTypeCategoryId int, @VisibleIndex int,  @MyCursor CURSOR;

SELECT @OrgId = PartyId FROM Organization WHERE Code = 'CUSTOMAPP'
SELECT @INDWEOrgId = PartyId FROM Organization WHERE Code = 'INDWE'

SET @MyCursor = CURSOR FOR select ClaimsTypeId, ClaimsTypeCategoryId, VisibleIndex FROM OrganizationClaimTypeCategory WHERE OrganizationId = @INDWEOrgId;  
OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @ClaimsTypeId, @ClaimsTypeCategoryId, @VisibleIndex

	WHILE @@FETCH_STATUS = 0
	BEGIN
			INSERT INTO OrganizationClaimTypeCategory (ClaimsTypeId, ClaimsTypeCategoryId, VisibleIndex, IsDeleted, OrganizationId) 
			VALUES (@ClaimsTypeId, @ClaimsTypeCategoryId, @VisibleIndex, 0, @OrgId)

		FETCH NEXT FROM @MyCursor INTO @ClaimsTypeId, @ClaimsTypeCategoryId, @VisibleIndex
	END
		
CLOSE @MyCursor 
DEALLOCATE @MyCursor;
