﻿DECLARE @PartyId int;

INSERT INTO Party (MasterId, PartyTypeId, DisplayName, ChannelId) VALUES (0, 2, 'CustomApp',1);

SELECT @PartyId = SCOPE_IDENTITY();

INSERT INTO Organization (PartyId, Code ,RegisteredName, TradingName, TradingSince, [Description], FspNo)
VALUES (@PartyId, 'CUSTOMAPP','CustomApp', 'CUSTOMAPP', '2006-01-01', 'CustomApp build mobile applications', '0000');

INSERT INTO Product (MasterId, Name,ProductOwnerId, ProductProviderId, ProductTypeId, AgencyNumber, ProductCode, ImageName, QuoteExpiration)
VALUES (0, 'CUSTOMAPP', @PartyId, @PartyId, 7, '123456', 'CUSTOMAPP', 'customapp.png', 30);
