﻿
 WITH CTE AS (
  SELECT ChannelId, UserId, IsDeleted, 
     ROW_NUMBER() OVER(PARTITION BY ChannelId, UserId ORDER BY UserId) AS [row_number]
  FROM UserChannel
)
UPDATE CTE set IsDeleted = 1 WHERE [row_number] > 1

IF EXISTS (SELECT TOP 1 Id FROM Channel WHERE IsDefault = 1)
BEGIN
	UPDATE UserChannel SET isDefault = 0 WHERE IsDeleted = 0;

	UPDATE UserChannel
	SET
	 IsDefault = 1
	WHERE ChannelId = (select top 1 Id from Channel where IsDefault = 1) 
	 AND UserId in (select Id from [User] where [User].Id = UserChannel.UserId)
	 AND IsDeleted = 0;
 END
