﻿Alter Table OrganizationClaimTypeExclusion Alter column OrganizationId int null;
Alter Table OrganizationClaimTypeCategory Alter column OrganizationId int null;

--exclude AIG claims type
INSERT INTO OrganizationClaimTypeExclusion (ClaimsTypeId, IsDeleted) VALUES
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0);

-- MOTOR
INSERT INTO OrganizationClaimTypeCategory (ClaimsTypeId, ClaimsTypeCategoryId, VisibleIndex, IsDeleted) VALUES
(1, 2, 1, 0),
(2, 2, 2, 0),
(15, 2, 3, 0),
(12, 2, 4, 0),
(13, 2, 5, 0),
(14, 2, 6, 0),
(22, 2, 7, 0),
(23, 2, 8, 0),
(24, 2, 9, 0),
(25, 2, 10, 0),
(26, 2, 11, 0),
(27, 2, 12, 0),
(42, 2, 13, 0),
(43, 2, 14, 0);
-- Building
INSERT INTO OrganizationClaimTypeCategory (ClaimsTypeId, ClaimsTypeCategoryId, VisibleIndex, IsDeleted) VALUES
(8, 3, 1, 0),
(20, 3, 2, 0),
(21, 3, 3, 0),
(29, 3, 4, 0),
(38, 3, 5, 0),
(39, 3, 6, 0),
(40, 3, 7, 0);
-- All 
INSERT INTO OrganizationClaimTypeCategory (ClaimsTypeId, ClaimsTypeCategoryId, VisibleIndex, IsDeleted) VALUES
(3, 1, 1, 0),
(4, 1, 2, 0),
(5, 1, 3, 0),
(7, 1, 4, 0),
(18, 1, 5, 0),
(19, 1, 6, 0),
(9, 1, 7, 0),
(10, 1, 8, 0),
(11, 1, 9, 0),
(41, 1, 10, 0);
-- Other 
INSERT INTO OrganizationClaimTypeCategory (ClaimsTypeId, ClaimsTypeCategoryId, VisibleIndex, IsDeleted) VALUES
(6, 4, 1, 0),
(16, 4, 2, 0),
(17, 4, 3, 0),
(28, 4, 4, 0),
(30, 4, 5, 0),
(31, 4, 6, 0),
(32, 4, 7, 0),
(33, 4, 8, 0),
(34, 4, 9, 0),
(35, 4, 10, 0),
(36, 4, 11, 0),
(37, 4, 12, 0);