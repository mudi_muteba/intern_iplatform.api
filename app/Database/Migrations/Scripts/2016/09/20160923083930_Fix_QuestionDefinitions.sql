﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')

DECLARE @CoverID INT
SET @CoverID=(select ID from md.Cover where name ='Motor')

DECLARE @CoverDefid INT
SET @CoverDefid=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId=@CoverID)

update [QuestionDefinition] Set [QuestionDefinitionTypeId] = 1, [RequiredForQuote]=0 where questionid=1180 and [QuestionDefinitionTypeId]=0 and CoverDefinitionId = @CoverDefid
update [QuestionDefinition] Set [QuestionDefinitionTypeId] = 1, [RequiredForQuote]=0 where questionid=1181 and [QuestionDefinitionTypeId]=0 and CoverDefinitionId = @CoverDefid
