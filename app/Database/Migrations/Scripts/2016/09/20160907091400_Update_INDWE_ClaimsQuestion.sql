DECLARE @OrgId int, @ClaimTypeId int, @MyCursor CURSOR;

SELECT @OrgId = PartyId FROM Organization WHERE Code = 'INDWE'

SET @MyCursor = CURSOR FOR select Id FROM md.ClaimsType WHERE Id not in(2,1,42,43,24,37,29,36,39,8,32,30);  
OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @ClaimTypeId

	WHILE @@FETCH_STATUS = 0
	BEGIN

		if not exists(SELECT * FROM OrganizationClaimTypeExclusion WHERE OrganizationId = @OrgId AND ClaimsTypeId = @ClaimTypeId)
		BEGIN
			INSERT INTO OrganizationClaimTypeExclusion (ClaimsTypeId, OrganizationId, IsDeleted) VALUES (@ClaimTypeId, @OrgId, 0)
		END

		FETCH NEXT FROM @MyCursor INTO @ClaimTypeId
	END
		
CLOSE @MyCursor 
DEALLOCATE @MyCursor;

DECLARE @prdClaimQuestionId int, @parentprdClaimQuestionId int, @ProductId int;
SELECT Top 1 @ProductId = Id FROM Product WHERE ProductCode = 'INDWE'

--Motor Accident 3
SELECT @parentprdClaimQuestionId = pc.Id FROM ProductClaimsQuestionDefinition pc 
INNER JOIN md.ClaimsQuestionDefinition c on pc.ClaimsQuestionDefinitionId = c.Id 
WHERE c.ClaimsTypeId = 2  and c.ClaimsQuestionId = 54 AND pc.ProductId = @ProductId

SELECT @prdClaimQuestionId = pc.Id FROM ProductClaimsQuestionDefinition pc
INNER JOIN md.ClaimsQuestionDefinition c on pc.ClaimsQuestionDefinitionId = c.Id
WHERE c.ClaimsTypeId = 2 AND c.ClaimsQuestionId = 55 AND pc.ProductId = @ProductId

if ( @parentprdClaimQuestionId> 0 and @prdClaimQuestionId > 0)
Begin
	Update ProductClaimsQuestionDefinition Set ParentProductClaimQuestionDefinitionId = @parentprdClaimQuestionId WHERE Id = @prdClaimQuestionId;
	SET @prdClaimQuestionId = 0;
	SET @parentprdClaimQuestionId = 0 
END

Update ProductClaimsQuestionDefinition Set DisplayName = 'Contact Number' WHERE ClaimsQuestionDefinitionId in (SELECT Id FROM md.ClaimsQuestionDefinition c WHERE c.ClaimsTypeId = 2 and c.ClaimsQuestionId = 19) AND ProductId = @ProductId;
--Motor Theft 1
Update ProductClaimsQuestionDefinition Set DisplayName = 'When And Where Did The Accident Occur?' WHERE ClaimsQuestionDefinitionId in (SELECT Id FROM md.ClaimsQuestionDefinition c WHERE c.ClaimsTypeId = 1 and c.ClaimsQuestionId = 23) AND ProductId = @ProductId;
--Hijack 
if not exists(SELECT * FROM ProductClaimsQuestionDefinition pc WHERE pc.ClaimsQuestionDefinitionId = 1056 AND pc.ProductId = @ProductId)
BEGIN
	INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1056,1,0,1,'Who was the driver at the time of loss?',0,36);
END
if not exists(SELECT * FROM ProductClaimsQuestionDefinition pc WHERE pc.ClaimsQuestionDefinitionId = 1057 AND pc.ProductId = @ProductId)
BEGIN
	INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1057,1,0,1,'Was anyone injured at time of incident?',1,36);
END
--Motor Damage 43
Update ProductClaimsQuestionDefinition Set FirstPhase = 0 , SecondPhase = 1 WHERE ClaimsQuestionDefinitionId IN (SELECT Id FROM md.ClaimsQuestionDefinition WHERE ClaimsTypeId = 43) AND ProductId = @ProductId;
--windscreen 24
Update ProductClaimsQuestionDefinition Set FirstPhase = 0 , SecondPhase = 1 WHERE ClaimsQuestionDefinitionId IN (SELECT Id FROM md.ClaimsQuestionDefinition WHERE ClaimsTypeId = 24) AND ProductId = @ProductId;
--Theft | Burglary	37
Update ProductClaimsQuestionDefinition Set FirstPhase = 0 , SecondPhase = 1 WHERE ClaimsQuestionDefinitionId IN (SELECT Id FROM md.ClaimsQuestionDefinition WHERE ClaimsTypeId = 37) AND ProductId = @ProductId;
--Geyser 29
Update ProductClaimsQuestionDefinition Set FirstPhase = 0 , SecondPhase = 1 WHERE ClaimsQuestionDefinitionId IN (SELECT Id FROM md.ClaimsQuestionDefinition WHERE ClaimsTypeId = 29) AND ProductId = @ProductId;
Update ProductClaimsQuestionDefinition Set DisplayName = 'Risk Address' WHERE ClaimsQuestionDefinitionId in (SELECT Id FROM md.ClaimsQuestionDefinition c WHERE c.ClaimsTypeId = 29 and c.ClaimsQuestionId = 18) AND ProductId = @ProductId;
--Accidental Damage 36
Update ProductClaimsQuestionDefinition Set FirstPhase = 0 , SecondPhase = 1 WHERE ClaimsQuestionDefinitionId IN (SELECT Id FROM md.ClaimsQuestionDefinition WHERE ClaimsTypeId = 36) AND ProductId = @ProductId;
Update ProductClaimsQuestionDefinition Set IsDeleted = 1 WHERE ClaimsQuestionDefinitionId in (SELECT Id FROM md.ClaimsQuestionDefinition c WHERE c.ClaimsTypeId = 36 and c.ClaimsQuestionId = 231) AND ProductId = @ProductId;
Update ProductClaimsQuestionDefinition Set IsDeleted = 1 WHERE ClaimsQuestionDefinitionId in (SELECT Id FROM md.ClaimsQuestionDefinition c WHERE c.ClaimsTypeId = 36 and c.ClaimsQuestionId = 287) AND ProductId = @ProductId;
--Keys  & locks	39
Update ProductClaimsQuestionDefinition Set FirstPhase = 0 , SecondPhase = 1 WHERE ClaimsQuestionDefinitionId IN (SELECT Id FROM md.ClaimsQuestionDefinition WHERE ClaimsTypeId = 39) AND ProductId = @ProductId;
--Damaged | Lost Item	8
Update ProductClaimsQuestionDefinition Set FirstPhase = 0 , SecondPhase = 1 WHERE ClaimsQuestionDefinitionId IN (SELECT Id FROM md.ClaimsQuestionDefinition WHERE ClaimsTypeId = 8) AND ProductId = @ProductId;
--Jewellery	32
Update ProductClaimsQuestionDefinition Set FirstPhase = 0 , SecondPhase = 1 WHERE ClaimsQuestionDefinitionId IN (SELECT Id FROM md.ClaimsQuestionDefinition WHERE ClaimsTypeId = 32) AND ProductId = @ProductId;

SELECT @parentprdClaimQuestionId = pc.Id FROM ProductClaimsQuestionDefinition pc 
INNER JOIN md.ClaimsQuestionDefinition c on pc.ClaimsQuestionDefinitionId = c.Id 
WHERE c.ClaimsTypeId = 32  and c.ClaimsQuestionId = 447 AND pc.ProductId = @ProductId;

SELECT @prdClaimQuestionId = pc.Id FROM ProductClaimsQuestionDefinition pc
INNER JOIN md.ClaimsQuestionDefinition c on pc.ClaimsQuestionDefinitionId = c.Id
WHERE c.ClaimsTypeId = 32 AND c.ClaimsQuestionId = 110 AND pc.ProductId = @ProductId;

if ( @parentprdClaimQuestionId> 0 and @prdClaimQuestionId > 0)
Begin
	Update ProductClaimsQuestionDefinition Set ParentProductClaimQuestionDefinitionId = @parentprdClaimQuestionId WHERE Id = @prdClaimQuestionId;
	SET @prdClaimQuestionId = 0;
	SET @parentprdClaimQuestionId = 0 
END

--Cellphone	30
Update ProductClaimsQuestionDefinition Set FirstPhase = 0 , SecondPhase = 1 WHERE ClaimsQuestionDefinitionId IN (SELECT Id FROM md.ClaimsQuestionDefinition WHERE ClaimsTypeId = 30) AND ProductId = @ProductId;
Update ProductClaimsQuestionDefinition Set DisplayName = 'Date of Incident' WHERE ClaimsQuestionDefinitionId in (SELECT Id FROM md.ClaimsQuestionDefinition c WHERE c.ClaimsTypeId = 30 and c.ClaimsQuestionId = 98) AND ProductId = @ProductId;
Update ProductClaimsQuestionDefinition Set DisplayName = 'Time of Incident' WHERE ClaimsQuestionDefinitionId in (SELECT Id FROM md.ClaimsQuestionDefinition c WHERE c.ClaimsTypeId = 30 and c.ClaimsQuestionId = 99) AND ProductId = @ProductId;
Update ProductClaimsQuestionDefinition Set DisplayName = 'Cell Phone ITC Number?' WHERE ClaimsQuestionDefinitionId in (SELECT Id FROM md.ClaimsQuestionDefinition c WHERE c.ClaimsTypeId = 30 and c.ClaimsQuestionId = 422) AND ProductId = @ProductId;
Update ProductClaimsQuestionDefinition Set DisplayName = 'What is the IMEI number?' WHERE ClaimsQuestionDefinitionId in (SELECT Id FROM md.ClaimsQuestionDefinition c WHERE c.ClaimsTypeId = 30 and c.ClaimsQuestionId = 421) AND ProductId = @ProductId;
