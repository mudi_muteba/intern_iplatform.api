﻿begin tran

declare @channelId int, @userId int;
SELECT top 1 @userId = Id  FROM [User] WHERE UserName = 'root@iplatform.co.za';

set @channelId  = 9

IF EXISTS(select id from [dbo].[Channel] where id = @channelId)
UPDATE [dbo].[Channel] set Code = 'BIDVEST',Name = 'Bidvest',SystemId = 'B93B292A-76D0-4B6D-A4AC-17130290C89A' where Id = @channelId
ELSE
INSERT INTO  Channel (Id, SystemId, IsActive, ActivatedOn, DeactivatedOn, IsDefault, IsDeleted, CurrencyId, DateFormat, Name,Code, LanguageId, PasswordStrengthEnabled) VALUES
           (@channelId,'B93B292A-76D0-4B6D-A4AC-17130290C89A', 1, GETDATE(), null, 0,0,1,'dd MMM yyyy','Bidvest','BIDVEST', 2,0);

if NOT EXISTS(SELECT * FROM UserAuthorisationGroup WHERE UserId = @userId AND ChannelId = @channelId AND IsDeleted = 0)
BEGIN
	INSERT INTO [dbo].[UserChannel] ([CreatedAt],[ModifiedAt],[UserId],[ChannelId],[IsDeleted],[IsDefault]) VALUES (getdate(),getdate(),@userId, @channelId, 0,0 );
END

if NOT EXISTS(SELECT * FROM UserAuthorisationGroup WHERE UserId = @userId AND ChannelId = @channelId AND IsDeleted = 0 AND AuthorisationGroupId = 5)
BEGIN
	INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, UserId, ChannelId, IsDeleted) VALUES (5,@userId, @channelId, 0 );
END

GO

commit 