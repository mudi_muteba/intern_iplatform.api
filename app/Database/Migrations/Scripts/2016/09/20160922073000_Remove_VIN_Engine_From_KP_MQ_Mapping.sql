﻿DECLARE @idMul INT
DECLARE @CoverDefinitionIdMul INT


SET @idMul  = (SELECT TOP 1 ID FROM Product WHERE ProductCode = 'MUL')
SET @CoverDefinitionIdMul  = (SELECT TOP 1 ID FROM [dbo].[CoverDefinition] WHERE ProductId = @idMul)


DECLARE @CoverDefinitionId INT
DECLARE @id INT


SET @id  = (SELECT TOP 1 ID FROM Product WHERE ProductCode = 'KPIPERS2')
SET @CoverDefinitionId  = (SELECT TOP 1 ID FROM [dbo].[CoverDefinition] WHERE ProductId = @id )

--SELECT mq.id, ParentQuestion.*, ChildQuestion.*
DELETE mq
 FROM
  (SELECT Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @CoverDefinitionIdMul) AS ParentQuestion
 INNER JOIN 
  (SELECT Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @CoverDefinitionId AND Child.QuestionId in (1010, 1013)) AS ChildQuestion
   ON ParentQuestion.Id != ChildQuestion.Id

 
 INNER JOIN
 MapQuestionDefinition mq
 ON ParentQuestion.Id =  mq.ParentId
 AND ChildQuestion.id = mq.ChildId

