﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='KPIPERS2')

DECLARE @CoverID INT
SET @CoverID=(select ID from md.Cover where name ='Motor')

DECLARE @CoverDefid INT
SET @CoverDefid=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId=@CoverID)

if not exists(select * from QuestionDefinition where QuestionId=64 and CoverDefinitionId = @CoverDefid)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Selected Excess',@CoverDefid,64,NULL,1,9,1,1,0,'A selected excess will reduce a clients premium.',N'',N'',0,NULL,0)
		END