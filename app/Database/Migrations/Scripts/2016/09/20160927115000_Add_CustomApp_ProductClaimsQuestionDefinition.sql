﻿Update ProductClaimsQuestionDefinition SET DisplayName = 'Accident Date' WHERE ProductId = 59 AND ClaimsQuestionDefinitionId = 98
Update ProductClaimsQuestionDefinition SET DisplayName = 'Accident Time' WHERE ProductId = 59 AND ClaimsQuestionDefinitionId = 99
Update ProductClaimsQuestionDefinition SET DisplayName = 'Incident Description', IsDeleted = 1 WHERE ProductId = 59 AND ClaimsQuestionDefinitionId = 1033
Update ProductClaimsQuestionDefinition SET DisplayName = 'Theft Date' WHERE ProductId = 59 AND ClaimsQuestionDefinitionId = 185
Update ProductClaimsQuestionDefinition SET DisplayName = 'Theft Time' WHERE ProductId = 59 AND ClaimsQuestionDefinitionId = 186

Declare @ProductId int, @IndweProductId int;

SELECT Top 1 @ProductId = Id FROM Product WHERE ProductCode = 'CUSTOMAPP'
SELECT Top 1 @IndweProductId = Id FROM Product WHERE ProductCode = 'INDWE'

if (@ProductId > 0)
BEGIN
	Declare @MyCursor CURSOR, @ClaimsQuestionDefinitionId int, @Required int, 
	@FirstPhase int, @SecondPhase int, @DisplayName varchar(max), @VisibleIndex int,
	@ClaimsQuestionGroupId int, @ParentProductClaimQuestionDefinitionId int;

	SET @MyCursor = CURSOR FOR SELECT ClaimsQuestionDefinitionId, [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId, ParentProductClaimQuestionDefinitionId 
	FROM ProductClaimsQuestionDefinition WHERE ProductId = 59 AND IsDeleted <> 1;  

	OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @ClaimsQuestionDefinitionId,@Required, @FirstPhase,
	@SecondPhase, @DisplayName, @VisibleIndex, @ClaimsQuestionGroupId, @ParentProductClaimQuestionDefinitionId;

	WHILE @@FETCH_STATUS = 0
	BEGIN

		INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId,ParentProductClaimQuestionDefinitionId) 
		VALUES (@ProductId, 0, @ClaimsQuestionDefinitionId, @Required, @FirstPhase, @SecondPhase, @DisplayName, @VisibleIndex, @ClaimsQuestionGroupId, @ParentProductClaimQuestionDefinitionId)

		FETCH NEXT FROM @MyCursor INTO @ClaimsQuestionDefinitionId, @Required, @FirstPhase,@SecondPhase, 
		@DisplayName, @VisibleIndex, @ClaimsQuestionGroupId, @ParentProductClaimQuestionDefinitionId

	END
END