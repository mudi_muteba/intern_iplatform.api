﻿begin tran

declare @channelId int, @userId int;
SELECT top 1 @userId = Id  FROM [User] WHERE UserName = 'root@iplatform.co.za'
set @channelId  = 8

IF EXISTS(select id from [dbo].[Channel] where id = @channelId)
UPDATE [dbo].[Channel] set Code = 'INDWE',Name = 'Indwe',SystemId = '6FA72902-BCFB-4CC7-9F55-E5140D8D9CB6' where Id = @channelId
ELSE
INSERT INTO  Channel (Id, SystemId, IsActive, ActivatedOn, DeactivatedOn, IsDefault, IsDeleted, CurrencyId, DateFormat, Name,Code, LanguageId, PasswordStrengthEnabled) VALUES
           (@channelId,'6FA72902-BCFB-4CC7-9F55-E5140D8D9CB6', 1, GETDATE(), null, 0,0,1,'dd MMM yyyy','Indwe','INDWE', 2,0);

if NOT EXISTS(SELECT * FROM UserAuthorisationGroup WHERE UserId = @userId AND ChannelId = @channelId AND IsDeleted = 0)
BEGIN
	INSERT INTO [dbo].[UserChannel] ([CreatedAt],[ModifiedAt],[UserId],[ChannelId],[IsDeleted],[IsDefault]) VALUES (getdate(),getdate(),@userId, @channelId, 0,0 );
END

if NOT EXISTS(SELECT * FROM UserAuthorisationGroup WHERE UserId = @userId AND ChannelId = @channelId AND IsDeleted = 0 AND AuthorisationGroupId = 5)
BEGIN
	INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, UserId, ChannelId, IsDeleted) VALUES (5,@userId, @channelId, 0 );
END


GO
commit 