if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_MotorExtract') and type in (N'P', N'PC'))
begin
	drop procedure Reports_AIG_MotorExtract
end
go

create procedure Reports_AIG_MotorExtract
(
--@PartyId int
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as

----declare @PartyId int
--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
----set @PartyId = 1268 
--set @StartDate = '2016-06-09'
--set @EndDate = '2016-09-22'

DECLARE @tbl TABLE(QuestionId int, QuoteId int, AnswerId nvarchar(255), PartyId int, Created datetime, ProposalHeaderId int, ProposalDefinitionId int)
DECLARE @tblExtra TABLE(PartyId int, AssetId int, Description nvarchar(100), Value decimal(18,2), AccessoryTypeId int)

insert into @tbl
select qd.QuestionId, qte.Id as QuoteId,pqa.Answer as AnswerId, pd.PartyId, pd.Created, pd.ProposalHeaderId, pd.Id from ProposalDefinition as pd
				join ProposalQuestionAnswer as pqa on pqa.ProposalDefinitionId = pd.Id
				join QuestionDefinition as qd on qd.Id = pqa.QuestionDefinitionId
				join md.Question as q on q.Id = qd.QuestionId
				join ProposalHeader as ph on ph.Id = pd.ProposalHeaderId
				join QuoteHeader as qh on qh.ProposalHeaderId = ph.Id
				join Quote as qte on qte.QuoteHeaderId = qh.Id			
				where --pd.PartyId = @PartyId and 
				pd.CoverId = 218 and pd.IsDeleted = 0 and qte.IsDeleted =0 and qd.id = pqa.QuestionDefinitionId and qd.QuestionId in (
				1005,9,69,73,87,142,143,210,226,227,231,223,224,245,145,230,232,234,261,296,519,867,866,871,1003,1004,1010,1160,
				873,249,41,248,862,300,135,91,154,864,863,47,90,89,786,310,255) --and pqa.Answer is not null

insert into @tblExtra
select ast.PartyId, ast.Id as AssetId, astEx.Description, astEx.Value, astEx.AccessoryTypeId from AssetExtras as astEx
join Asset as ast on ast.Id = astEx.AssetId
--where ast.PartyId = @PartyId

--select * from @tbl where QuestionId in (862) and PartyId = 1268  
--select * from @tblExtra

Select 
	(select [user].UserName from LeadActivity as la
	join [user] on [user].Id = la.UserId
	where la.Id = prh.LeadActivityId
	) as AgentName,
	CASE 
		WHEN Prh.Source IS NULL THEN ('Call center')   
		ELSE Prh.Source                                     
    END as Channel, 
	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'Date of Quote',
	CASE
		WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y'
		ELSE 'N'
	END as 'QuoteExpired',

	CASE 
	WHEN PhS.Id IS NULL THEN ('N')   
	WHEN PhS.Id = 1 THEN ('N')
	WHEN PhS.Id = 2 THEN ('N')
	WHEN PhS.Id = 3 THEN ('Y')                                         
	END AS 'Quote-Bound',

	case when Qute.AcceptedOn IS NULL Then '' else CONVERT(VARCHAR(10),Qute.AcceptedOn ,111) end as 'QuoteBindDate',

	(select cast(round (Premium ,2) as numeric(36,2)) from QuoteItem as Qi		
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
		where qi.Id = QteI.Id and cd.CoverId in (218, 17, 45, 142, 373, 372, 258) 
	) as 'QuoteTotalPremium',

 	CASE WHEN (select Premium from QuoteItem as Qi		
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
		where --QuoteId = Qute.Id and 
		cd.CoverId = 218 and qi.Id = QteI.Id
	 ) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId = 218 and qi.Id = QteI.Id)                                          
     END AS 'QuoteMotorPremium',
	 
	 CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (17,45) and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select  cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (17,45) and qi.Id = QteI.Id)                                          
     END AS 'QuoteHomePremium',

	CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId = 142 and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId = 142 and qi.Id = QteI.Id)                                          
     END AS 'QuoteFuneralPremium',	

	CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (373, 372, 258) and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (373, 372, 258) and qi.Id = QteI.Id)                                          
     END as 'QuoteOtherPremium',

	(select top 1 CONVERT(VARCHAR(10),InceptionDate ,111) from PaymentDetails where PartyId = p.Id and QuoteId = qute.Id order by Id desc) as 'QuoteIncepptionDate',
	ph.PolicyNo as 'QuotePolicyNumber',
	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'Quote-CreateDate',
	CONVERT(VARCHAR(10),Qute.CreatedAt,108) as 'Quote-CreateTime',
	l.Id as 'LeadText',

	ast.AssetNo as 'VehicleNumber',

	(select AnswerId from @tbl as temp	
	where temp.QuestionId = 89 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'NightAddress-Suburb',

	(select AnswerId from @tbl as temp	
	where temp.QuestionId = 90 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'NightAddress-PostCode',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 47 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'NightAddress-Province',
	

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 863 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'NightAddress-Storage',

	(select AnswerId from @tbl where QuestionId = 135 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DayAddress-Suburb',

	(select AnswerId from @tbl where QuestionId = 91 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DayAddress-PostCode',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 154 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DayAddress-Province',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 864 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DayAddress-Storage',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 786 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleType',

	QteI.ExcessBasic as 'MotorExcess',
	astV.VehicleRegistrationNumber as 'RegistrationNumber',
	astV.VehicleMake as 'VehicleMake',
	astV.VehicleModel as 'VehicleModel',
	astV.VehicleMMCode as 'VehicleMMCode',

	(select AnswerId from @tbl where QuestionId = 310 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)as 'VINNumber',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 227 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'Colour',

	case (select AnswerId from @tbl where QuestionId = 143 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) 						
	when 519 then 'Y' else 'N'			
	end  as 'MetallicPaint',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 230 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'CoverType',

	(case 
		(select  AnswerId from @tbl where QuestionId = 255 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Modified',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 867 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleStatus',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 226 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'ClassOfUse',

	(case 
		(select  AnswerId from @tbl where QuestionId = 232 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Delivery',

	Case (select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 296 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)				
		when null then 'N' else 'Y'			
	end  as 'TrackingDevice',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 296 and QuoteId = Qute.Id	and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id
	) as 'TrackingDeviceType',
	

	'' as 'DiscountedTrackingDevice',	

	(case 
		(select AnswerId from @tbl where QuestionId = 245 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Immobiliser',

	(case 
		(select AnswerId from @tbl where QuestionId = 871 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'AntiHijack',

	(case 
		(select AnswerId from @tbl where QuestionId = 231 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'DataDot',
	
	(select sum(value) from @tblExtra where AccessoryTypeId = 1 and AssetId = ast.Id) as 'Acc-SoundEquipment',
	(select sum(value) from @tblExtra where AccessoryTypeId = 2 and AssetId = ast.Id) as 'Acc-MagWheels',
	(select sum(value) from @tblExtra where AccessoryTypeId = 3 and AssetId = ast.Id) as 'Acc-SunRoof',
	(select sum(value) from @tblExtra where AccessoryTypeId = 4 and AssetId = ast.Id) as 'Acc-XenonLights',
	(select sum(value) from @tblExtra where AccessoryTypeId = 5 and AssetId = ast.Id) as 'Acc-TowBar',
	(select sum(value) from @tblExtra where AccessoryTypeId = 6 and AssetId = ast.Id) as 'Acc-BodyKit',
	(select sum(value) from @tblExtra where AccessoryTypeId = 7 and AssetId = ast.Id) as 'Acc-AntiSmashAndGrab',
	(select sum(value) from @tblExtra where AccessoryTypeId = 8 and AssetId = ast.Id) as 'Acc-Other',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 1005 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VAP-CarHire',

	(case 
		(select AnswerId from @tbl where QuestionId = 1004 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-TyreAndRim',

	(case 
		(select  AnswerId from @tbl where QuestionId = 1003 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-ScratchAndDent',

	(case 
		(select AnswerId from @tbl where QuestionId = 873 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-CreditShortfall',

	Ind.IdentityNo as 'DriverIDNumber',
	T.Name as 'DriverTitle',
	Ind.FirstName as 'DriverFirstName', 
	Ind.Surname as 'DriverSurname', 

	G.Name as 'DriverGender',
	Ms.Name as 'DriverMaritalStatus',
	Ind.DateOfBirth as 'DriverDateOfBirth',
	occ.Name as 'DriverOccupation',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 249 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DriverLicenseType',

	(select  AnswerId from @tbl where QuestionId = 41 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DriverLicenseDate',

	(case 
		(select AnswerId from @tbl where QuestionId = 248 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'DriverLicenseEndorsed',


	(case 
		(select AnswerId from @tbl where QuestionId = 224 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Automatic',

	(case 
		(select AnswerId from @tbl where QuestionId = 261 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Paraplegic',


	(case 
		(select AnswerId from @tbl where QuestionId = 223 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Amputee',

	(case 
		(select AnswerId from @tbl where QuestionId = 234 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Glasses',

	(case 
		(select AnswerId from @tbl where QuestionId = 862 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Financed',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 300 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	 as 'ValuationMethod',

	ast.SumInsured as 'SumInsured', 
	cast(round(QteI.Premium,2) as numeric(36,2))   as 'TariffPremiumTotalPremium', p.Id as 'PartyId'

from Party as P
join Lead as L on l.PartyId = p.Id
join Channel as Ch on ch.Id = p.ChannelId
left join ProposalHeader as Prh on Prh.PartyId = p.Id
left join ProposalDefinition as prd on Prd.ProposalHeaderId = Prh.Id
left join QuoteHeader as Qh on qh.ProposalHeaderId = Prh.Id
left join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
left join PolicyHeader as Ph on Ph.QuoteId = Qute.Id
left join md.PolicyStatus as PhS on Ph.PolicyStatusId = PhS.Id
left join QuoteItem as QteI on QteI.QuoteId = Qute.Id
left join ContactDetail as cd on cd.Id = P.ContactDetailId
left join Individual as Ind on P.Id = Ind.PartyId
left join md.Title as T on T.Id = Ind.TitleId
left join md.Gender as G on G.Id = Ind.GenderId
left join md.MaritalStatus as Ms on Ms.Id = Ind.MaritalStatusId
left join Occupation as Occ on Occ.Id = Ind.OccupationId
left join Asset as ast on ast.Id = prd.AssetId
left join AssetVehicle as astV on astV.AssetId = ast.Id
join QuoteAcceptedLeadActivity as qala on qala.QuoteId = Qute.Id
join LeadActivity as la on la.Id = qala.LeadActivityId
where Qute.Id is not null
and L.IsDeleted = 0
and Qute.IsDeleted = 0
--and p.Id = 2256
and prd.IsDeleted = 0
and ast.IsDeleted = 0
and prd.CoverId = 218
--and ph.DateEffective >= @StartDate
--and ph.DateRenewal <= @EndDate
and la.DateCreated >= @StartDate
and la.DateUpdated <= @EndDate
and la.ActivityTypeId = 5
and Qh.IsDeleted = 0
--and prh.IsQuoted = 1
and QteI.CoverDefinitionId in (select Id from CoverDefinition where CoverId = 218)
order by p.DisplayName

--Select * from QuoteItem where QuoteId in (5632,5633,
--5634)

--select * from CoverDefinition where CoverId = 218