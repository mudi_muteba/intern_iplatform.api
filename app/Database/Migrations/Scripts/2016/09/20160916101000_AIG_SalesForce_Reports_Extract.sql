﻿if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_SalesForceIntegrationExtract') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_AIG_SalesForceIntegrationExtract
	end
go

create procedure Reports_AIG_SalesForceIntegrationExtract
(
--@PartyId int
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
AS
BEGIN
	select CreatedOn,PartyId,'"'+JsonData+'"' as JSON,'"' +Response+'"' as Response,IsSuccess,[Status] from SalesForceIntegrationLog
	where CreatedOn BETWEEN  @StartDate and @EndDate
END
GO

if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_SalesForceIntegrationLogSummeryExtract') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_AIG_SalesForceIntegrationLogSummeryExtract
	end
go

create procedure Reports_AIG_SalesForceIntegrationLogSummeryExtract
(
--@PartyId int
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
AS
BEGIN

if exists (select  * from tempdb.dbo.sysobjects o where o.xtype in ('U') and o.id = object_id(N'tempdb..#temp'))
drop table #temp;
if exists (select  * from tempdb.dbo.sysobjects o where o.xtype in ('U') and o.id = object_id(N'tempdb..#errors'))
drop table #errors;
WITH CTE AS(
   SELECT 
       RN = ROW_NUMBER()OVER(PARTITION BY PartyId,IsSuccess ORDER BY PartyId,IsSuccess),
	   *
   FROM dbo.SalesForceIntegrationLog
   where CreatedOn > @StartDate
)
select PartyId,IsSuccess,MAX(RN) as [count] into #temp FROM CTE WHERE RN > 0 group by PartyId,IsSuccess
order by PartyId,IsSuccess

select b.PartyId,a.count as [Succsess],b.count [Failed] into #errors from #temp a right join #temp b 
on a.PartyId = b.PartyId 
and a.IsSuccess <> b.IsSuccess
where 
b.IsSuccess = 0
and (a.IsSuccess is null or a.IsSuccess = 1)

--select * from #errors
update #errors set Succsess = 0 where Succsess is null

select '1','Total' as [Description] ,count(*) as [Total] from SalesForceIntegrationLog where CreatedOn > @StartDate
union
select '2','Successful',count(*) as [Success] from SalesForceIntegrationLog where IsSuccess = 1 and CreatedOn > @StartDate 
union
select '3','Failed',count(*) as [Fail] from SalesForceIntegrationLog where IsSuccess = 0 and CreatedOn > @StartDate 
union 
select '4','Retries',count(Failed) as [Fail] from #errors where Failed <= Succsess
union 
select '5','Failures not resent', count(Failed) as [Fail] from #errors where Failed > Succsess
union
select '6','ERROR: Retry Response',count(*) as [Fail] from SalesForceIntegrationLog where IsSuccess = 0 and Response like 'AUTH ERROR retry your request%' and CreatedOn > @StartDate 
union
select '7','ERROR: Authentication Failures Response',count(*) as [Fail] from SalesForceIntegrationLog where IsSuccess = 0 and Response like 'AUTH ERROR authentication failure%' and CreatedOn > @StartDate 
union
select '8','ERROR: Other Failures',count(*) as [Fail] from SalesForceIntegrationLog where IsSuccess = 0 
and Response not like 'AUTH ERROR retry your reques%'
and Response not like 'AUTH ERROR authentication failure%'
and CreatedOn > @StartDate 
END

--exec Reports_AIG_SalesForceIntegrationLogSummeryExtract '2016-07-07 10:03:17.000','2016-07-15 10:03:17.000'
--exec Reports_AIG_SalesForceIntegrationExtract '2016-07-07 10:03:17.000','2016-07-15 10:03:17.000'