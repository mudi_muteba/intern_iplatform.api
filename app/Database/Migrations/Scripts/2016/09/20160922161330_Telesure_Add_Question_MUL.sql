﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')

DECLARE @CoverID INT
SET @CoverID=(select ID from md.Cover where name ='Motor')

DECLARE @CoverDefid INT
SET @CoverDefid=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId=@CoverID)

if not exists(select * from QuestionDefinition where QuestionId=1180 and CoverDefinitionId = @CoverDefid)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Overnight Area Type',@CoverDefid,1180,NULL,0,4,1,1,0,'Overnight Area Type',N'',N'',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where QuestionId=1181 and CoverDefinitionId = @CoverDefid)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Work Area Type',@CoverDefid,1181,NULL,0,102,1,1,0,'Work Area Type',N'',N'',0,NULL,0)
		END

if not exists (select *from QuestionDefinition where QuestionId = 132 and CoverDefinitionId = @CoverDefid)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Hail Cover',@CoverDefid,132,NULL,1,27,0,1,0,'Hail Cover','','',0,NULL,0)
		end

update [QuestionDefinition] Set VisibleIndex = 5 where questionid=139 and visibleindex=4 and CoverDefinitionId = @CoverDefid
update [QuestionDefinition] Set VisibleIndex = 6 where questionid=135 and visibleindex=5 and CoverDefinitionId = @CoverDefid
update [QuestionDefinition] Set VisibleIndex = 101 where questionid=91 and visibleindex=6 and CoverDefinitionId = @CoverDefid