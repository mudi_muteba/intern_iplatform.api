﻿---------------------------
--SET DEFAULT USER CHANNELS
---------------------------
update UserChannel
set
	IsDefault = 1
where
	ChannelId = (select top 1 Id from Channel where IsDefault = 1) and
	UserId in (select Id from [User] where [User].Id = UserChannel.UserId)



---------------------
--SETUP REPORT TABLES
---------------------
if not exists (select * from information_schema.tables where table_name = 'ReportChannel')
	begin
		CREATE TABLE [dbo].[ReportChannel](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[ReportId] [bigint] NOT NULL,
			[ChannelId] [int] NOT NULL,
			[Code] [nvarchar](10) NOT NULL,
			[IsActive] [bit] NOT NULL,
			[UserId] [int] NOT NULL,
			[CreatedAt] [datetime] NOT NULL,
			[ModifiedAt] [datetime] NOT NULL,
			[IsDeleted] [bit] NOT NULL,
		 CONSTRAINT [PK_ReportChannel] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[ReportChannel] ADD  CONSTRAINT [DF_ReportChannel_IsActive]  DEFAULT ((1)) FOR [IsActive]
		ALTER TABLE [dbo].[ReportChannel] ADD  CONSTRAINT [DF_ReportChannel_CreatedAt]  DEFAULT (getutcdate()) FOR [CreatedAt]
		ALTER TABLE [dbo].[ReportChannel] ADD  CONSTRAINT [DF_ReportChannel_ModifiedAt]  DEFAULT (getutcdate()) FOR [ModifiedAt]
		ALTER TABLE [dbo].[ReportChannel] ADD  CONSTRAINT [DF_ReportChannel_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]

		ALTER TABLE [dbo].[ReportChannel]  WITH CHECK ADD  CONSTRAINT [FK_ReportChannel_Channel] FOREIGN KEY([ChannelId]) REFERENCES [dbo].[Channel] ([Id])
		ALTER TABLE [dbo].[ReportChannel] CHECK CONSTRAINT [FK_ReportChannel_Channel]

		ALTER TABLE [dbo].[ReportChannel]  WITH CHECK ADD  CONSTRAINT [FK_ReportChannel_Report] FOREIGN KEY([ReportId]) REFERENCES [dbo].[Report] ([Id])
		ALTER TABLE [dbo].[ReportChannel] CHECK CONSTRAINT [FK_ReportChannel_Report]
	end
go

if not exists (select * from information_schema.tables where table_name = 'ReportSection')
	begin
		CREATE TABLE [dbo].[ReportSection](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[ReportId] [int] NOT NULL,
			[Name] [nvarchar](50) NOT NULL,
			[Label] [nvarchar](50) NOT NULL,
			[IsVisible] [bit] NOT NULL,
		 CONSTRAINT [PK_ReportSection] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]


		ALTER TABLE [dbo].[ReportSection] ADD  CONSTRAINT [DF_ReportSection_IsVisible]  DEFAULT ((1)) FOR [IsVisible]
	end
go

if not exists (select * from information_schema.tables where table_name = 'ReportLayout')
	begin
		CREATE TABLE [dbo].[ReportLayout](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[ReportId] [int] NOT NULL,
			[ReportSectionId] [int] NOT NULL,
			[Name] [nvarchar](50) NOT NULL,
			[Label] [nvarchar](50) NOT NULL,
			[Value] [nvarchar](max) NULL,
			[IsVisible] [bit] NOT NULL,
		 CONSTRAINT [PK_ReportLayout] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[ReportLayout] ADD  CONSTRAINT [DF_ReportLayout_IsVisible]  DEFAULT ((1)) FOR [IsVisible]
	end
go

if not exists (select * from information_schema.tables where table_name = 'ReportPartyLayout')
	begin
		CREATE TABLE [dbo].[ReportPartyLayout](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[PartyId] [int] NOT NULL,
			[ReportLayoutId] [int] NOT NULL,
			[Label] [nvarchar](50) NOT NULL,
			[Value] [nvarchar](max) NULL,
			[IsVisible] [bit] NOT NULL,
		 CONSTRAINT [PK_ReportPartyLayout] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[ReportPartyLayout] ADD  CONSTRAINT [DF_Table_1_Visible]  DEFAULT ((1)) FOR [IsVisible]
	end
go

if not exists (select * from information_schema.tables where table_name = 'ReportChannelLayout')
	begin
		CREATE TABLE [dbo].[ReportChannelLayout](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[ReportChannelId] [int] NOT NULL,
			[ReportLayoutId] [int] NOT NULL,
			[Label] [nvarchar](50) NOT NULL,
			[Value] [nvarchar](255) NOT NULL,
			[IsVisible] [bit] NOT NULL,
			[UserId] [int] NULL,
			[CreatedAt] [datetime] NOT NULL,
			[ModifiedAt] [datetime] NOT NULL,
			[IsDeleted] [bit] NOT NULL,
		 CONSTRAINT [PK_ReportChannelLayout] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[ReportChannelLayout] ADD  CONSTRAINT [DF_ReportChannelLayout_IsVisible]  DEFAULT ((1)) FOR [IsVisible]
		ALTER TABLE [dbo].[ReportChannelLayout] ADD  CONSTRAINT [DF_ReportChannelLayout_CreatedAt]  DEFAULT (getutcdate()) FOR [CreatedAt]
		ALTER TABLE [dbo].[ReportChannelLayout] ADD  CONSTRAINT [DF_ReportChannelLayout_ModifiedAt]  DEFAULT (getutcdate()) FOR [ModifiedAt]
		ALTER TABLE [dbo].[ReportChannelLayout] ADD  CONSTRAINT [DF_ReportChannelLayout_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
		ALTER TABLE [dbo].[ReportChannelLayout]  WITH CHECK ADD  CONSTRAINT [FK_ReportChannelLayout_ReportChannel] FOREIGN KEY([ReportChannelId])
		REFERENCES [dbo].[ReportChannel] ([Id])
		ALTER TABLE [dbo].[ReportChannelLayout] CHECK CONSTRAINT [FK_ReportChannelLayout_ReportChannel]
		ALTER TABLE [dbo].[ReportChannelLayout]  WITH CHECK ADD  CONSTRAINT [FK_ReportChannelLayout_ReportLayout] FOREIGN KEY([ReportLayoutId])
		REFERENCES [dbo].[ReportLayout] ([Id])
		ALTER TABLE [dbo].[ReportChannelLayout] CHECK CONSTRAINT [FK_ReportChannelLayout_ReportLayout]
	end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_Layout_ByChannelId') and type in (N'P', N'PC'))
	begin
		drop procedure Report_Layout_ByChannelId
	end
go


create procedure Report_Layout_ByChannelId
(
	@ChannelId int,
	@ReportName varchar(50)
)
as
	select
		ReportSection.Name SectionName,
		ReportSection.Label SectionLabel,
		ReportSection.IsVisible SectionVisible,
		ReportLayout.Name FieldName,
		ReportChannelLayout.Label FieldLabel,
		ReportChannelLayout.[Value] FieldValue,
		ReportChannelLayout.IsVisible FieldVisible
	from Report
		inner join ReportChannel on ReportChannel.ReportId = Report.Id
		inner join ReportChannelLayout on ReportChannelLayout.ReportChannelId = ReportChannel.Id
		inner join ReportLayout on ReportLayout.Id = ReportChannelLayout.ReportLayoutId
		inner join ReportSection on ReportSection.Id = ReportLayout.ReportSectionId
	where
		ReportChannel.ChannelId = @ChannelId and
		Report.Name = @ReportName
	order by
		ReportChannelLayout.Id
go


--CLEAR REPORT SETTINGS
delete from ReportChannelLayout
delete from ReportChannel
delete from ReportSection
delete from ReportLayout
delete from Report
go


--RESEED IDENTITY COLUMNS
dbcc checkident ('ReportChannelLayout', reseed, 0)
dbcc checkident ('ReportChannel', reseed, 0)
dbcc checkident ('ReportSection', reseed, 0)
dbcc checkident ('ReportLayout', reseed, 0)
dbcc checkident ('Report', reseed, 0)

go

set nocount on
--VARIABLES
declare
	@DetailedComparativeQuoteId int,
	@ChannelCursor cursor,
	@CurrentDate datetime,
	@CursorChannelId int,
	@CursorChannelCode varchar(255),	
	@ReportChannelLayoutId int,
	@QuoteHeaderId int,
	@QuoteHeaderName varchar(255),
	@QuoteHeaderLabel varchar(255),	
	@QuoteFooterId int,
	@QuoteFooterName varchar(255),
	@QuoteFooterLabel varchar(255),
	@ClientHeaderId int,
	@ClientHeaderName varchar(255),
	@ClientHeaderLabel varchar(255),
	@CompanyHeaderId int,
	@CompanyHeaderName varchar(255),
	@CompanyHeaderLabel varchar(255),
	@QuoteSummaryId int,
	@QuoteSummaryName varchar(255),
	@QuoteSummaryLabel varchar(255),
	@CostFooterId int,
	@CostFooterName varchar(255),
	@CostFooterLabel varchar(255),

	--DEFAULT COMPANY LABELS
	@DefaultCompanyHeaderLabel varchar(255),
	@DefaultCompanyGeneralNameLabel varchar(255),
	@DefaultCompanyAddressPhysicalLabel varchar(255),
	@DefaultCompanyAddressPostalLabel varchar(255),
	@DefaultCompanyContactWorkLabel varchar(255),
	@DefaultCompanyContactFaxLabel varchar(255),
	@DefaultCompanyContactEmailLabel varchar(255),
	@DefaultCompanyContactWebsiteLabel varchar(255),
	@DefaultCompanyAuthRegistrationLabel varchar(255),
	@DefaultCompanyAuthTaxLabel varchar(255),
	@DefaultCompanyAuthLicenseLabel  varchar(255),

	--DEFAULT COMPANY VALUES
	@CompanyHeaderValue varchar(255),
	@CompanyGeneralNameValue varchar(255),
	@CompanyAddressPhysicalValue varchar(255),
	@CompanyAddressPostalValue varchar(255),
	@CompanyContactWorkValue varchar(255),
	@CompanyContactFaxValue varchar(255),
	@CompanyContactEmailValue varchar(255),
	@CompanyContactWebsiteValue varchar(255),
	@CompanyAuthRegistrationValue varchar(255),
	@CompanyAuthTaxValue varchar(255),
	@CompanyAuthLicenseValue  varchar(255),

	--SCOPE IDENTITY
	@ScopeIdentity int

--CREATE DETAILED COMPARATIVE QUOTE IF IT DOESN'T EXIST
select @DetailedComparativeQuoteId = Id from Report where Name = 'Detailed Comparative Quote Report'

if(isnull(@DetailedComparativeQuoteId, 0) = 0)
	begin
		print 'Detailed Comparative Quote not found, creating...'

		insert into Report (Name, Description, SourceFile, IsDeleted, IsVisibleInReportViewer)
		values ('Detailed Comparative Quote Report', 'Detailed Comparative Quote Report', 'DetailedComparativeQuote.trdx', 0, 0)

		set @ScopeIdentity = scope_identity()
		set @DetailedComparativeQuoteId = @ScopeIdentity

		print 'Detailed Comparative Quote created with Id ' + cast(@ScopeIdentity as varchar) + '...'
	end

	
--CREATE DETAILED COMPRATIVE QUOTE REPORT SECTIONS
insert into ReportSection (ReportId, Name, Label) values (@DetailedComparativeQuoteId, 'QuoteHeader', 'Quote Header')
	set @ScopeIdentity = scope_identity()
	select @QuoteHeaderId = @ScopeIdentity, @QuoteHeaderName = Name, @QuoteHeaderLabel = Label from ReportSection where Id = @ScopeIdentity

insert into ReportSection (ReportId, Name, Label) values (@DetailedComparativeQuoteId, 'QuoteFooter', 'Quote Footer')
	set @ScopeIdentity = scope_identity()
	select @QuoteFooterId = @ScopeIdentity, @QuoteFooterName = Name, @QuoteFooterLabel = Label from ReportSection where Id = @ScopeIdentity

insert into ReportSection (ReportId, Name, Label) values (@DetailedComparativeQuoteId, 'ClientHeader', 'Client Header')
	set @ScopeIdentity = scope_identity()
	select @ClientHeaderId = @ScopeIdentity, @ClientHeaderName = Name, @ClientHeaderLabel = Label from ReportSection where Id = @ScopeIdentity

insert into ReportSection (ReportId, Name, Label) values (@DetailedComparativeQuoteId, 'CompanyHeader', 'Company Header')
	set @ScopeIdentity = scope_identity()
	select @CompanyHeaderId = @ScopeIdentity, @CompanyHeaderName = Name, @CompanyHeaderLabel = Label from ReportSection where Id = @ScopeIdentity

insert into ReportSection (ReportId, Name, Label) values (@DetailedComparativeQuoteId, 'QuoteSummary', 'Quote Summary')
	set @ScopeIdentity = scope_identity()
	select @QuoteSummaryId = @ScopeIdentity, @QuoteSummaryName = Name, @QuoteSummaryLabel = Label from ReportSection where Id = @ScopeIdentity

insert into ReportSection (ReportId, Name, Label) values (@DetailedComparativeQuoteId, 'CostFooter', 'Cost Footer')
	set @ScopeIdentity = scope_identity()
	select @CostFooterId = @ScopeIdentity, @CostFooterName = Name, @CostFooterLabel = Label from ReportSection where Id = @ScopeIdentity


--SET CURRENT DATE
set @CurrentDate = getdate()


--CREATE DEFAUTL REPORT LAYOUT
declare
	@ReportLayoutQuoteCodeId varchar(255),
	@ReportLayoutQuoteTitleId varchar(255),
	@ReportLayoutQuoteSummaryId varchar(255),
	@ReportLayoutQuoteAssetsId varchar(255),
	@ReportLayoutQuoteDateId varchar(255),
	@ReportLayoutQuoteExpiresId varchar(255),
	@ReportLayoutQuoteNumberId varchar(255),
	@ReportLayoutClientHeaderId varchar(255),
	@ReportLayoutClientGeneralNameId varchar(255),
	@ReportLayoutClientGeneralIdentityId varchar(255),
	@ReportLayoutClientAddressPhysicalId varchar(255),
	@ReportLayoutClientAddressPostalId varchar(255),
	@ReportLayoutClientContactWorkId varchar(255),
	@ReportLayoutClientContactHomeId varchar(255),
	@ReportLayoutClientContactCellId varchar(255),
	@ReportLayoutClientContactFaxId varchar(255),
	@ReportLayoutClientContactEmailId varchar(255),
	@ReportLayoutCompanyHeaderId varchar(255),
	@ReportCompanyGeneralNameId varchar(255),
	@ReportLayoutCompanyAddressPhysicalId varchar(255),
	@ReportLayouCompanyAddressPostalId varchar(255),
	@ReportLayoutCompanyContactWorkId varchar(255),
	@ReportLayoutCompanyContactFaxId varchar(255),
	@ReportLayoutCompanyContactEmailId varchar(255),
	@ReportLayoutCompanyContactWebsiteId varchar(255),
	@ReportLayoutCompanyAuthRegistrationId varchar(255),
	@ReportLayoutCompanyAuthTaxId varchar(255),
	@ReportLayoutCompanyAuthLicenseId varchar(255),
	@ReportLayoutQuoteFooterId varchar(255),
	@ReportLayoutQuoteSummaryFeesId varchar(255),
	@ReportLayoutQuoteSummaryTotalId varchar(255),
	@ReportLayoutCostFooterId varchar(255)


--SET DEFAULT FIELD LABELS
select
	@DefaultCompanyHeaderLabel = 'Details of Intermediary',
	@DefaultCompanyGeneralNameLabel = 'Company',
	@DefaultCompanyAddressPhysicalLabel = 'Physical Address',
	@DefaultCompanyAddressPostalLabel = 'Postal Address',
	@DefaultCompanyContactWorkLabel = 'Work No.',
	@DefaultCompanyContactFaxLabel = 'Fax No.',
	@DefaultCompanyContactEmailLabel = 'Email Address',
	@DefaultCompanyContactWebsiteLabel = 'Website',
	@DefaultCompanyAuthRegistrationLabel = 'Registration No.',
	@DefaultCompanyAuthTaxLabel = 'Tax No.',
	@DefaultCompanyAuthLicenseLabel = 'Authorised Financial Services Provider'

--CONFIGURE REPORT LAYOUT -- QUOTE HEADER LABELS
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @QuoteHeaderId, 'QuoteCode', 'Quote Code', 'IP', 0)
set @ReportLayoutQuoteCodeId = scope_identity()
	
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @QuoteHeaderId, 'QuoteTitle', 'COMPARATIVE QUOTATION', '', 1)
set @ReportLayoutQuoteTitleId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @QuoteHeaderId, 'QuoteSummary', 'QUOTE SUMMARY', '', 1)
set @ReportLayoutQuoteSummaryId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @QuoteHeaderId, 'QuoteAssets', 'ASSETS', '', 1)
set @ReportLayoutQuoteAssetsId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @QuoteHeaderId, 'QuoteDate', 'Date:', '', 1)
set @ReportLayoutQuoteDateId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @QuoteHeaderId, 'QuoteExpires', 'Expires:', '', 1)
set @ReportLayoutQuoteExpiresId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @QuoteHeaderId, 'QuoteNumber', 'Reference No:', '', 1)
set @ReportLayoutQuoteNumberId = scope_identity()


--CONFIGURE REPORT LAYOUT -- CLIENT HEADER LABELS
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @ClientHeaderId, 'ClientHeader', 'DETAILS OF CLIENT', '', 1)
set @ReportLayoutClientHeaderId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @ClientHeaderId, 'ClientGeneralName', 'Client', '', 1)
set @ReportLayoutClientGeneralNameId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @ClientHeaderId, 'ClientGeneralIdentity', 'Identity No.', '', 1)
set @ReportLayoutClientGeneralIdentityId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @ClientHeaderId, 'ClientAddressPhysical', 'Physical Address', '', 1)
set @ReportLayoutClientAddressPhysicalId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @ClientHeaderId, 'ClientAddressPostal', 'Postal Address', '', 1)
set @ReportLayoutClientAddressPostalId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @ClientHeaderId, 'ClientContactWork', 'Work No.', '', 1)
set @ReportLayoutClientContactWorkId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @ClientHeaderId, 'ClientContactHome', 'Home No.', '', 1)
set @ReportLayoutClientContactHomeId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @ClientHeaderId, 'ClientContactCell', 'Cell No.', '', 1)
set @ReportLayoutClientContactCellId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @ClientHeaderId, 'ClientContactFax', 'Fax No.', '', 1)
set @ReportLayoutClientContactFaxId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @ClientHeaderId, 'ClientContactEmail', 'Email Address', '', 1)
set @ReportLayoutClientContactEmailId = scope_identity()


--CONFIGURE REPORT LAYOUT -- COMPANY HEADER VALUES
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @CompanyHeaderId, 'CompanyHeader', @DefaultCompanyHeaderLabel, '', 1)
set @ReportLayoutCompanyHeaderId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @CompanyHeaderId, 'CompanyGeneralName', @DefaultCompanyGeneralNameLabel, '', 1)
set @ReportCompanyGeneralNameId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @CompanyHeaderId, 'CompanyAddressPhysical', @DefaultCompanyAddressPhysicalLabel, '', 1)
set @ReportLayoutCompanyAddressPhysicalId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @CompanyHeaderId, 'CompanyAddressPostal', @DefaultCompanyAddressPostalLabel, '', 1)
set @ReportLayouCompanyAddressPostalId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @CompanyHeaderId, 'CompanyContactWork', @DefaultCompanyContactWorkLabel, '', 1)
set @ReportLayoutCompanyContactWorkId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @CompanyHeaderId, 'CompanyContactFax', @DefaultCompanyContactFaxLabel, '', 1)
set @ReportLayoutCompanyContactFaxId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @CompanyHeaderId, 'CompanyContactEmail', @DefaultCompanyContactEmailLabel, '', 1)
set @ReportLayoutCompanyContactEmailId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @CompanyHeaderId, 'CompanyContactWebsite', @DefaultCompanyContactWebsiteLabel, '', 1)
set @ReportLayoutCompanyContactWebsiteId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @CompanyHeaderId, 'CompanyAuthRegistration', @DefaultCompanyAuthRegistrationLabel, '', 1)
set @ReportLayoutCompanyAuthRegistrationId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @CompanyHeaderId, 'CompanyAuthTax', @DefaultCompanyAuthTaxLabel, '', 1)
set @ReportLayoutCompanyAuthTaxId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @CompanyHeaderId, 'CompanyAuthLicense', @DefaultCompanyAuthLicenseLabel, '', 1)
set @ReportLayoutCompanyAuthLicenseId = scope_identity()


--CONFIGURE REPORT LAYOUT -- QUOTE FOOTER LABELS
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @QuoteFooterId, 'QuoteFooter', '', '', 1)
set @ReportLayoutQuoteFooterId = scope_identity()


--CONFIGURE REPORT LAYOUT -- QUOTE SUMMARY LABELS
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @QuoteSummaryId, 'QuoteSummaryFees', '', '', 0)
set @ReportLayoutQuoteSummaryFeesId = scope_identity()

insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @QuoteSummaryId, 'QuoteSummaryTotal', '', '', 0)
set @ReportLayoutQuoteSummaryTotalId = scope_identity()


--CONFIGURE REPORT LAYOUT -- COST FOOTER VALUES
insert into ReportLayout (ReportId, ReportSectionId, Name, Label, [Value], IsVisible) values (@DetailedComparativeQuoteId, @CostFooterId, 'CostFooter', '', '', 1)
set @ReportLayoutCostFooterId = scope_identity()


--CREATE CURSOR TO LOOP THROUGH AVAILABLE CHANNELS
set @ChannelCursor = cursor for
	select Id, Code from Channel

open @ChannelCursor 

fetch next from @ChannelCursor into @CursorChannelId, @CursorChannelCode
	while @@fetch_status = 0
		begin
			print 'Setting up reports for ' + @CursorChannelCode

			--ADD REPORT TO DEFAULT CHANNEL
			insert into ReportChannel (ChannelId, ReportId, Code, CreatedAt, ModifiedAt, UserId) values (@CursorChannelId, @DetailedComparativeQuoteId, @CursorChannelCode, @CurrentDate, @CurrentDate, 1)
			set @ReportChannelLayoutId  = scope_identity()


			--CONFIGURE REPORT LAYOUT -- QUOTE HEADER LABELS
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteCodeId, 'Quote Code', @CursorChannelCode, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteTitleId, 'COMPARATIVE QUOTATION', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteSummaryId, 'QUOTE SUMMARY', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteAssetsId, 'ASSETS', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteDateId, 'Date:', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteExpiresId, 'Expires:', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteNumberId, 'Quote Reference:', '', 1)


			--CONFIGURE REPORT LAYOUT -- CLIENT HEADER LABELS
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientHeaderId, 'DETAILS OF CLIENT', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientGeneralNameId, 'Client', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientGeneralIdentityId, 'Identity No.', '', 0)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientAddressPhysicalId, 'Physical Address', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientAddressPostalId, 'Postal Address', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientContactWorkId, 'Work No.', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientContactHomeId, 'Home No.', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientContactCellId, 'Cell No.', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientContactFaxId, 'Fax No.', '', 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutClientContactEmailId, 'Email Address', '', 1)


			--SET FIELD VALUES BASE ON CHANNEL CODE
			if(upper(@CursorChannelCode) = 'IP')
				begin
					select
						@CompanyHeaderValue = 'iPlatform',
						@CompanyGeneralNameValue = 'iPlatform',
						@CompanyAddressPhysicalValue = '1st Floor Block A, 28 on Sloane Office Park, 28 Sloane Street, Bryanston, South Africa',
						@CompanyAddressPostalValue = '',
						@CompanyContactWorkValue = '0861 988 888',
						@CompanyContactFaxValue = '0866 100 147',
						@CompanyContactEmailValue = 'quotes@iplatform.co.za',
						@CompanyContactWebsiteValue = 'http://www.iplatform.co.za',
						@CompanyAuthRegistrationValue = '1999/006725/07',
						@CompanyAuthTaxValue = '',
						@CompanyAuthLicenseValue = ''
				end
			else if(upper(@CursorChannelCode) = 'KP')
				begin
					select
						@CompanyHeaderValue = 'King Price Insurance Company Ltd.',
						@CompanyGeneralNameValue = 'King Price Insurance Company Ltd.',
						@CompanyAddressPhysicalValue = 'Menlyn Corporate Park, Block A, 3rd Floor, Corner of Garsfontein Road & Corobay Avenue, Waterkloof Glen X11, Pretoria, 0181',
						@CompanyAddressPostalValue = 'PO Box 284, Menlyn, 0063',
						@CompanyContactWorkValue = '0860 00 55 00',
						@CompanyContactFaxValue = '',
						@CompanyContactEmailValue = 'BrokerCC@kingprice.co.za',
						@CompanyContactWebsiteValue = 'http://www.kingprice.co.za',
						@CompanyAuthRegistrationValue = '2009/012496/06',
						@CompanyAuthTaxValue = '4710259724',
						@CompanyAuthLicenseValue = '43862'
				end
			else if(upper(@CursorChannelCode) = 'PSG')
				begin
					select
						@CompanyHeaderValue = 'PSG Wealth Financial Planning',
						@CompanyGeneralNameValue = 'PSG Wealth Financial Planning',
						@CompanyAddressPhysicalValue = 'Block B, Corporate 66 Office Park, 269 Vonwillich Avenue, Centurion, 0175',
						@CompanyAddressPostalValue = 'Block B, Corporate 66 Office Park, 269 Vonwillich Avenue, Centurion, 0175',
						@CompanyContactWorkValue = '0860 774 566',
						@CompanyContactFaxValue = '',
						@CompanyContactEmailValue = 'ipquotes@psg.co.za',
						@CompanyContactWebsiteValue = 'http://www.psg.co.za',
						@CompanyAuthRegistrationValue = '1999/006725/07',
						@CompanyAuthTaxValue = '4230182216',
						@CompanyAuthLicenseValue = '728'
				end
			else if(upper(@CursorChannelCode) = 'UAP')
				begin
					select
						@CompanyHeaderValue = 'Lifecycle Management (Pty) Ltd',
						@CompanyGeneralNameValue = 'Lifecycle Management (Pty) Ltd',
						@CompanyAddressPhysicalValue = '2 Park Lane, Umhlanga Ridge, 4319',
						@CompanyAddressPostalValue = 'PO Box 3025, Prestondale, Kwa-Zulu Natal, 4021',
						@CompanyContactWorkValue = '0861 862273',
						@CompanyContactFaxValue = '',
						@CompanyContactEmailValue = 'needsomehelp@virginmoneyinsurance.co.za',
						@CompanyContactWebsiteValue = 'http://www.virginmoneyinsurance.co.za',
						@CompanyAuthRegistrationValue = '2014/075034/07',
						@CompanyAuthTaxValue = '4070270881',
						@CompanyAuthLicenseValue = 'No 7146'
				end
			else
				begin
					select
						@CompanyHeaderValue = 'iPlatform',
						@CompanyGeneralNameValue = 'iPlatform',
						@CompanyAddressPhysicalValue = '1st Floor Block A, 28 on Sloane Office Park, 28 Sloane Street, Bryanston, South Africa',
						@CompanyAddressPostalValue = '',
						@CompanyContactWorkValue = '0861 988 888',
						@CompanyContactFaxValue = '0866 100 147',
						@CompanyContactEmailValue = 'quotes@iplatform.co.za',
						@CompanyContactWebsiteValue = 'http://www.iplatform.co.za',
						@CompanyAuthRegistrationValue = '1999/006725/07',
						@CompanyAuthTaxValue = '',
						@CompanyAuthLicenseValue = ''
				end


			--CONFIGURE REPORT LAYOUT -- COMPANY HEADER VALUES
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyHeaderId, 'Company', @CompanyHeaderValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportCompanyGeneralNameId, 'Company', @CompanyGeneralNameValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyAddressPhysicalId, 'Physical Address', @CompanyAddressPhysicalValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayouCompanyAddressPostalId, 'Postal Address', @CompanyAddressPostalValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyContactWorkId, 'Work No.', @CompanyContactWorkValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyContactFaxId, 'Fax No.', ' ', 0)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyContactEmailId, 'Email Address', @CompanyContactEmailValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyContactWebsiteId, 'Website', @CompanyContactWebsiteValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyAuthRegistrationId, 'Registration No.', @CompanyAuthRegistrationValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyAuthTaxId, 'Tax No.', @CompanyAuthTaxValue, 1)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCompanyAuthLicenseId, 'Authorised Financial Services Provider', @CompanyAuthLicenseValue, 1)


			--CONFIGURE REPORT LAYOUT -- QUOTE FOOTER LABELS
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteFooterId, '', '', 1)


			--CONFIGURE REPORT LAYOUT -- QUOTE SUMMARY LABELS
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteSummaryFeesId, '', '', 0)
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutQuoteSummaryTotalId, '', '', 0)


			--CONFIGURE REPORT LAYOUT -- COST FOOTER VALUES
			insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, [Value], IsVisible) values (@ReportChannelLayoutId, @ReportLayoutCostFooterId, '', '', 1)

			fetch next from @ChannelCursor into @CursorChannelId, @CursorChannelCode
		end

close @ChannelCursor
deallocate @ChannelCursor

print 'Done...'

set nocount off


go