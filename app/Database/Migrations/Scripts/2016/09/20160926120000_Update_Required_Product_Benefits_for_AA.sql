﻿--AA Insurance

----AA Product 

declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode = 'AIG')
	begin
		return
	end
else
	begin
declare @pId int

set @pId = (select Id from [dbo].[Product] where productCode = 'AA')

declare @CoverId int

set @CoverId = (select Id from [md].[Cover] where Code = 'MOTOR')

declare @CoverDef int

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Private Type Vehicle', N'', 0, 0, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Use of vehicle', N'Domestic, Professional, Full Business', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Comprehensive Cover', N'If requested ', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'New for Old', N'Included', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Windscreen', N'Comprehensive cover only', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Fire and theft Cover', N'If requested', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Cover', N'If requested', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Riot and Strike outside RSA', N'Not Included', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Tracking device to be covered', N'Not Included', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Towing and Storage', N'If insurer appoints', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Sound System [not factory fitted]', N'If requested', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Hail Damage', N'Comprehensive cover only', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Canopy of the pick-up', N'If requested', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Car Hire', N'If requested', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Emergency Hotel Expenses', N'If insurer appoints', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Credit Shortfall ', N'If requested', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Medical Costs', N'5,000', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Liability', N'5,000,000', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party fire and explosion Liability', N'5,000,000', 1, 19, 0)


--Contents

set @pId = (select Id from [dbo].[Product] where productCode = 'AA')

set @CoverId = (select Id from [md].[Cover] where Code = 'CONTENTS')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Automatic anniversary/renewal sum insured increase', N'Yes ', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Accidental Damage extension cover', N'Yes', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Documents limitation', N'Not Included', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Deterioration of Food', N'Included', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Washing stolen at your home', N'No', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Garden furniture stolen at your home', N'Included', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Damage to Garden', N'Not covered', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Guests belongings stolen at your home', N'Included', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Personal documents/coins/stamps - Loss of', N'Included', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Locks and Keys - Lost/damaged', N'Included', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Remote control units', N'Included ', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Loss of water by leakage', N'Not covered', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Credit or Bank Cards - fraudulent use', N'Included', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Hole in one / bowling full house', N'Included', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Insured and Spouce death - fire or a break-in', N'Included', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Your domestics belongings - stolen following break-in', N'Not covered', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Medical Expenses', N'Not covered', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Veterinary Fees', N'Included', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Rent to live elsewhere', N'Included', 1, 20, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Storage costs for contents after damage', N'Included', 1, 21, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Belongings in a removal truck', N'Included ', 1, 22, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Accidental breakage of mirrors and galss', N'Included ', 1, 23, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Accidental breakage of television set', N'Included ', 1, 24, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Fire brigade charges', N'Included', 1, 25, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Security Guards', N'Included ', 1, 26, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Public Liability', N'Included ', 1, 27, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Tenants Liability', N'Included', 1, 28, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Liability to Domestic Employees', N'Included', 1, 29, 0)


--All Risk

set @pId = (select Id from [dbo].[Product] where productCode = 'AA')

set @CoverId = (select Id from [md].[Cover] where Code = 'ALL_RISK')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes ', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Riot and Strike outside RSA', N'Not Included', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Unspecified Items as defined', N'Yes ', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'   * If yes ~ Cover for Jewellery, Clothing and Personal items', N'Yes', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'   * If yes ~ Limit any one item', N'R3,500 ', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'   * If yes ~ Cover for money and netiable instruments', N'Covered up to R5000 under the contents section.', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Sperfified Items [If yes refer to list of items]', N'Yes - e.g. Portable electronic equipment such a GPSs and MP3 players, Cellphone, Prescription spectacles and Bicycles. ', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Items in a Bank Vault', N'Included', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Bicycles', N'If specified', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Wheelchairs and its accessories', N'If specified', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Prescription Glasses', N'If specified', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Contact Lenses', N'If specified', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Cellular phones', N'If specified', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Stamp Collection ', N'If specified', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Coin Collection ', N'If specified', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Transport of groceries and household ods', N'Not included', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'All Risks cover for caravan contents', N'If specified', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'All Risks cover for car radios', N'If specified', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Borehole and swimming pool equipment', N'If specified ', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Items stolen from the cabin of a vehicle limit ', N'Not covered ', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Incident Limit : Items stolen from the locked boot of a vehicle ', N'No Limit', 1, 19, 0)



--Building

set @pId = (select Id from [dbo].[Product] where productCode = 'AA')

set @CoverId = (select Id from [md].[Cover] where Code = 'BUILDING')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes ', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'If yes Home building definition to be checked', N'Covered', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Thatch roof home', N'Subject to approval ', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Swimming Pool covered', N'Yes', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Swimming Pool equipment covered', N'Covered', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Borehole pump/equipment', N'Yes', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Tennis court covered', N'Yes', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Damage to Gardens', N'Not Included', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Automatic anniversary/renewal sum insured increase', N'Yes', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Loss of water by leakage', N'Not Included', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Rent ', N'Included ', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Removal of fallen trees', N'Included ', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Professional Fees', N'Included', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Glass and Sanitaryware', N'Included', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Power supply', N'Included', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Aerials', N'Included ', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Fire brigade charges', N'Included ', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Security Guards', N'Included', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Public Liability as a Home Owner', N'Included', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Liability to Domestic Employees', N'Inclded', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Subsidence, heave and landslip', N'Included ', 1, 20, 0)

end