if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_LeadExtract') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_AIG_LeadExtract
	end
go

create procedure Reports_AIG_LeadExtract
(
	--@PartyId int,
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as

----declare @PartyId int
--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
----set @PartyId = 1955 --3412
--set @StartDate = '2016-04-01'
--set @EndDate = '2016-09-30'

Select 
	(select [user].UserName from LeadActivity as la
	join [user] on [user].Id = la.UserId
	where la.Id = prh.LeadActivityId
	) as AgentName,

	CASE 
		WHEN Prh.Source IS NULL THEN ('Call center')   
		ELSE Prh.Source                                     
    END as Channel, 

	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'DateOfQuote',

	CASE
		WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y'
		ELSE 'N'
	END as 'QuoteExpired',

	CASE 
		WHEN PhS.Id IS NULL THEN ('N')   
		WHEN PhS.Id = 1 THEN ('N')
		WHEN PhS.Id = 2 THEN ('N')
		WHEN PhS.Id = 3 THEN ('Y')                                         
    END AS 'QuoteBound',
	
	case when Qute.AcceptedOn IS NULL Then '' else CONVERT(VARCHAR(10),Qute.AcceptedOn ,111) end as 'QuoteBindDate',

	(select cast(round (Premium ,2) as numeric(36,2)) from QuoteItem as Qi		
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
		where qi.Id = QteI.Id and cd.CoverId in (218, 17, 45, 142, 373, 372, 258) 
	) as 'QuoteTotalPremium',

 	CASE WHEN (select Premium from QuoteItem as Qi		
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
		where --QuoteId = Qute.Id and 
		cd.CoverId = 218 and qi.Id = QteI.Id
	 ) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId = 218 and qi.Id = QteI.Id)                                          
     END AS 'QuoteMotorPremium',
	 
	 CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (17,45) and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select  cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (17,45) and qi.Id = QteI.Id)                                          
     END AS 'QuoteHomePremium',

	CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId = 142 and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId = 142 and qi.Id = QteI.Id)                                          
     END AS 'QuoteFuneralPremium',	

	CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (373, 372, 258) and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (373, 372, 258) and qi.Id = QteI.Id)                                          
     END as 'QuoteOtherPremium',

	(select top 1 CONVERT(VARCHAR(10),InceptionDate ,111) from PaymentDetails where PartyId = p.Id and QuoteId = qute.Id order by Id desc) as 'QuoteIncepptionDate',

	ph.PolicyNo as 'QuotePolicyNumber',
	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'QuoteCreateDate',
	CONVERT(VARCHAR(10),Qute.CreatedAt,108) as 'QuoteCreateTime',
	l.Id as 'LeadText',
	Ind.IdentityNo as 'IDNumber', 
	T.Name as 'Title',
	Ind.FirstName as 'FirstName', 
	'' as 'Initials', 
	Ind.Surname as 'Surname',
	CONVERT(VARCHAR(10),Ind.DateOfBirth ,111) as 'DateOfBirth',
	DATEDIFF(hour,Ind.DateOfBirth,GETDATE())/8766 AS Age, 
	G.Name as 'Gender', 
	Ms.Name as 'MaritalStatus', 
	Qute.ITCScore as 'ITCCreditScore',
	Qute.ExtraITCScore  as 'ITCCreditScoreBand',
	occ.Name as 'Occupation', 
	Lang.Name as 'HomeLanguage', 
	'English' as 'PreferredLanguage',
	ind.ExternalReference as 'ExternalReference',
	cd.Email as 'EmailAddress', 
	cd.Cell as 'CellPhoneNumber', 
	cd.Home as 'HomePhoneNumber', 
	cd.Work as 'WorkPhoneNumber',

	CASE 
		WHEN ind.AnyJudgements is null THEN ('N') 
		WHEN ind.AnyJudgements = 0 THEN ('N')   
		WHEN ind.AnyJudgements = 1 THEN ('Y')                                       
    END AS 'DeclarationJudgements',

	CASE 
		WHEN ind.UnderAdministrationOrDebtReview is null THEN ('N') 
		WHEN ind.UnderAdministrationOrDebtReview = 0 THEN ('N')   
		WHEN ind.UnderAdministrationOrDebtReview = 1 THEN ('Y')                                       
    END AS 'DeclarationDebtReview',

	CASE 
		WHEN ind.BeenSequestratedOrLiquidated is null THEN ('N') 
		WHEN ind.BeenSequestratedOrLiquidated = 0 THEN ('N')   
		WHEN ind.BeenSequestratedOrLiquidated = 1 THEN ('Y')                                       
    END AS 'DeclarationSequestration',

	Bnkd.BankAccHolder as 'AccountHolderName', 
	Bnkd.AccountNo as 'AccountNumber', 
	Bnkd.TypeAccount as 'AccountType', 
	Bnkd.Bank as 'BankName', 
	Bnkd.BankBranch as 'BranchName', 
	Bnkd.BankBranchCode as 'BranchCode',

	(select top 1 ci.Answer from LossHistory as lh
	join md.CurrentlyInsured as ci on ci.Id = lh.CurrentlyInsuredId
	where lh.PartyId = p.Id and lh.IsDeleted = 0 order by lh.Id desc
	) AS 'CurrentlyInsured',	

	(select top 1 up.Answer from LossHistory as lh
	join md.UninterruptedPolicy as up on up.Id = lh.UninterruptedPolicyId
	where lh.PartyId = p.Id and lh.IsDeleted = 0 order by lh.Id desc
	) AS 'UninterruptedCover',

	(select case 
		WHEN (select top 1 InsurerCancel from LossHistory where PartyId = p.id and IsDeleted = 0 order by LossHistory.Id desc) = 1  THEN 'Y'
		WHEN (select top 1 InsurerCancel from LossHistory where PartyId = p.id and IsDeleted = 0 order by LossHistory.Id desc) = 0 Then ('N')		
	end )as 'CancelledInsurance',

	p.Id as 'PartyId'


from Party as P
join Lead as L on l.PartyId = p.Id
join Channel as Ch on ch.Id = p.ChannelId
left join ProposalHeader as Prh on Prh.PartyId = p.Id
left join ProposalDefinition as prd on Prd.ProposalHeaderId = Prh.Id
left join QuoteHeader as Qh on qh.ProposalHeaderId = Prh.Id
left join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
left join QuoteItem as QteI on QteI.QuoteId = Qute.Id
left join PolicyHeader as Ph on Ph.QuoteId = Qute.Id
left join md.PolicyStatus as PhS on Ph.PolicyStatusId = PhS.Id
left join CoverDefinition as cod on QteI.CoverDefinitionId = cod.Id
left join md.Cover as cov on cod.CoverId = cov.Id
left join ContactDetail as cd on cd.Id = P.ContactDetailId
left join Individual as Ind on P.Id = Ind.PartyId
left join md.Title as T on T.Id = Ind.TitleId
left join md.Gender as G on G.Id = Ind.GenderId
left join md.MaritalStatus as Ms on Ms.Id = Ind.MaritalStatusId
left join Occupation as Occ on Occ.Id = Ind.OccupationId
left join Md.Language as Lang on Lang.Id = Ind.LanguageId
left join BankDetails as Bnkd on P.Id = Bnkd.PartyId
join QuoteAcceptedLeadActivity as qala on qala.QuoteId = Qute.Id
join LeadActivity as la on la.Id = qala.LeadActivityId
where Qute.Id is not null
--and ph.DateEffective >= @StartDate
--and ph.DateRenewal <= @EndDate
and la.DateCreated >= @StartDate
and la.DateUpdated <= @EndDate
and la.ActivityTypeId = 5
and L.IsDeleted = 0
and Qute.IsDeleted = 0
and QteI.IsDeleted = 0
--and Prh.IsQuoted = 1
and Prh.IsDeleted = 0
and prd.IsDeleted = 0
and Qh.IsDeleted = 0
--and p.Id = 3412
order by AgentName