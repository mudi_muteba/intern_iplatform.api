﻿if object_id('dbo.ReportParameter', 'u') is not null
  drop table dbo.ReportParameter;

if object_id('dbo.ReportParameterType', 'u') is not null
  drop table dbo.ReportParameterType; 

if object_id('dbo.ReportPartyLayout', 'u') is not null
  drop table dbo.ReportPartyLayout; 

if object_id('dbo.ReportUser', 'u') is not null
  drop table dbo.ReportUser; 

if exists (select * from sys.columns where Name = N'CategoryId' and object_id = object_id(N'dbo.Report'))
	alter table dbo.Report drop
		constraint FK_Report_ReportCategory_CategoryId,
		column CategoryId

if exists (select * from sys.columns where Name = N'ReportScheduleId' and object_id = object_id(N'dbo.Report'))
	alter table dbo.Report drop
		constraint FKBE40EFCCE0F6D630,
		column ReportScheduleId