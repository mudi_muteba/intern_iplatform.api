﻿UPDATE qd
	Set VisibleIndex = 0
FROM
	QuestionDefinition qd
	INNER JOIN CoverDefinition cd
		ON cd.Id = qd.CoverDefinitionId
	INNER JOIN Product p
		ON p.Id = cd.ProductId
		AND p.ProductCode = 'VIRGIN'
		AND cd.CoverId = 78
		AND qd.QuestionId = 394