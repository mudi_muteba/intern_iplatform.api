﻿DECLARE @OrgId int, @ClaimTypeId int, @MyCursor CURSOR;

SELECT @OrgId = PartyId FROM Organization WHERE Code = 'CUSTOMAPP'

SET @MyCursor = CURSOR FOR select Id FROM md.ClaimsType WHERE Id not in(2,1,42,43,24,37,29,36,39,8,32,30);  
OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @ClaimTypeId

	WHILE @@FETCH_STATUS = 0
	BEGIN

		if not exists(SELECT * FROM OrganizationClaimTypeExclusion WHERE OrganizationId = @OrgId AND ClaimsTypeId = @ClaimTypeId)
		BEGIN
			INSERT INTO OrganizationClaimTypeExclusion (ClaimsTypeId, OrganizationId, IsDeleted) VALUES (@ClaimTypeId, @OrgId, 0)
		END

		FETCH NEXT FROM @MyCursor INTO @ClaimTypeId
	END
		
CLOSE @MyCursor 
DEALLOCATE @MyCursor;