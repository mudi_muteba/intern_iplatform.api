﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')

DECLARE @CoverID INT
SET @CoverID=(select ID from md.Cover where name ='Contents')

DECLARE @CoverDefid INT
SET @CoverDefid=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId=@CoverID)

if not exists(select * from QuestionDefinition where QuestionId=10 and CoverDefinitionId = @CoverDefid And IsDeleted = 0)
	BEGIN
	INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
	VALUES(0,'Burglar Alarm Linked To 24 Hr Armed Response',@CoverDefid,10,NULL,1,7,0,1,0,'Burglar Alarm Linked To 24 Hr Armed Response',N'',N'',0,NULL,0)
	END

 
