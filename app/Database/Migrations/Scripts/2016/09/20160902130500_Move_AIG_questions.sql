﻿IF EXISTS(select id from [dbo].[Channel] where Code = 'AIG' AND IsDefault = 1)
	BEGIN

-- skip this it is breaking on AIG


update md.Question set QuestionGroupId = 1 where Id = 847
update md.Question set QuestionGroupId = 1 where Id = 843
update md.Question set QuestionGroupId = 2 where Id = 786

update QuestionDefinition set VisibleIndex = 100 where QuestionId = 847
update QuestionDefinition set VisibleIndex = 101 where QuestionId = 843


update md.Question set QuestionGroupId = 1 where Id in
(
420,
396,
398,
386,
416,
402
)

update QuestionDefinition set VisibleIndex = 102 where QuestionId = 420
update QuestionDefinition set VisibleIndex = 103 where QuestionId = 396
update QuestionDefinition set VisibleIndex = 104 where QuestionId = 398
update QuestionDefinition set VisibleIndex = 105 where QuestionId = 386
update QuestionDefinition set VisibleIndex = 106 where QuestionId = 416
update QuestionDefinition set VisibleIndex = 107 where QuestionId = 402

update QuestionDefinition set VisibleIndex = 1000 where QuestionId = 394

update QuestionDefinition set DisplayName = 'Does your home boarder on vacant land, river or sea?' where QuestionId = 855
update QuestionDefinition set DisplayName = 'Is the property currently under construction?' where QuestionId = 856
update QuestionDefinition set DisplayName = 'Are there more than 2 unrelated families living in the main house?' where QuestionId = 854
update QuestionDefinition set DisplayName = 'Is the property undergoing construction or renovations at present?' where QuestionId = 856
update QuestionDefinition set DisplayName = 'Is your home financed or paid off?' where QuestionId = 843

update md.Question set QuestionGroupId = 1 where Id in
(
448,
450,
436,
468,
454)

update QuestionDefinition set VisibleIndex = 102 where QuestionId = 448
update QuestionDefinition set VisibleIndex = 103 where QuestionId = 450
update QuestionDefinition set VisibleIndex = 104 where QuestionId = 436
update QuestionDefinition set VisibleIndex = 103 where QuestionId = 468
update QuestionDefinition set VisibleIndex = 104 where QuestionId = 454


update QuestionDefinition set DisplayName = 'Suburb (Day)' where DisplayName like '%Suburb (Work)%' and CoverDefinitionId = 145
update QuestionDefinition set DisplayName = 'Postal Code (Day)' where DisplayName like '%Postal Code (Work)%' and CoverDefinitionId = 145
update QuestionDefinition set DisplayName = 'Province (Day)' where DisplayName like '%Province (Work)%' and CoverDefinitionId = 145
update QuestionDefinition set DisplayName = 'Has your license ever been endorsed or suspended?' where DisplayName like '%Has License been endorsed%' and CoverDefinitionId = 145
update QuestionDefinition set DisplayName = 'Does the license restrict you to driving an automatic car?' where DisplayName like '% with an automatic transmission%' and CoverDefinitionId = 145
update QuestionDefinition set DisplayName = 'Does the license restrict you to wear glasses while driving?' where DisplayName like '%Is the driver limited to driving the vehicle with glasses%' and CoverDefinitionId = 145


update QuestionDefinition set VisibleIndex = 0	 where id = 2628
update QuestionDefinition set VisibleIndex = 1	 where id = 2397
update QuestionDefinition set VisibleIndex = 2	 where id = 2383
update QuestionDefinition set VisibleIndex = 3	 where id = 2377
update QuestionDefinition set VisibleIndex = 4	 where id = 2378
update QuestionDefinition set VisibleIndex = 5	 where id = 2375
update QuestionDefinition set VisibleIndex = 6	 where id = 2431
update QuestionDefinition set VisibleIndex = 7	 where id = 2376
update QuestionDefinition set VisibleIndex = 8	 where id = 2374
update QuestionDefinition set VisibleIndex = 9	 where id = 2439
update QuestionDefinition set VisibleIndex = 10 where id = 	2394
update QuestionDefinition set VisibleIndex = 11 where id = 	2419
update QuestionDefinition set VisibleIndex = 12 where id = 	2421
update QuestionDefinition set VisibleIndex = 13 where id = 	2718
update QuestionDefinition set VisibleIndex = 14 where id = 	2393
update QuestionDefinition set VisibleIndex = 15 where id = 	2399
update QuestionDefinition set VisibleIndex = 0	 where id = 2379
update QuestionDefinition set VisibleIndex = 1	 where id = 2386
update QuestionDefinition set VisibleIndex = 2	 where id = 2384
update QuestionDefinition set VisibleIndex = 3	 where id = 2385
update QuestionDefinition set VisibleIndex = 4	 where id = 2369
update QuestionDefinition set VisibleIndex = 5	 where id = 2367
update QuestionDefinition set VisibleIndex = 6	 where id = 2417
update QuestionDefinition set VisibleIndex = 7	 where id = 2402
update QuestionDefinition set VisibleIndex = 8	 where id = 2415
update QuestionDefinition set VisibleIndex = 9	 where id = 2834
update QuestionDefinition set VisibleIndex = 10 where id = 	2414
update QuestionDefinition set VisibleIndex = 11 where id = 	2391
update QuestionDefinition set VisibleIndex = 12 where id = 	2427
update QuestionDefinition set VisibleIndex = 13 where id = 	2390
update QuestionDefinition set VisibleIndex = 14 where id = 	2401

END