﻿declare @DefaultChannelCode varchar(20)

select top 1 @DefaultChannelCode = Code from Channel where IsDefault = 1

if (@DefaultChannelCode <> 'AIG')
	begin
		return
	end
else
	begin
		if not exists(select * from Report where Name = 'Agent Quote Performance Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Agent Quote Performance Report', 'Agent Quote Performance Report', 'AgentQuotePerformance.trdx')
			end

		if not exists(select * from Report where Name = 'Agent Quote Performance Report - PSG')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Agent Quote Performance Report - PSG', 'Agent Quote Performance Report - PSG', 'AgentQuotePerformance_PSG.trdx')
			end

		if not exists(select * from Report where Name = 'Campaign Performance Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Campaign Performance Report', 'Campaign Performance Report', 'CampaignQuotePerformance.trdx')
			end

		if not exists(select * from Report where Name = 'Lead Status Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Lead Status Report', 'Lead Status Report', 'LeadStatus.trdx')
			end

		if not exists(select * from Report where Name = 'Comparative Quote Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Comparative Quote Report', 'Comparative Quote Report', 'ComparativeQuote.trdx')
			end

		if not exists(select * from Report where Name = 'Funeral Quote Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Funeral Quote Report', 'Funeral Quote Report', 'FuneralQuote.trdx')
			end

		if not exists(select * from Report where Name = 'Quote Schedule Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Quote Schedule Report', 'Quote Schedule Report', 'QuoteSchedule.trdx')
			end

		if not exists(select * from Report where Name = 'Detailed Comparative Quote Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Detailed Comparative Quote Report', 'Detailed Comparative Quote Report', 'DetailedComparativeQuote.trdx')
			end

		--CLEAR REPORT SETTINGS
		delete from ReportChannelLayout
		delete from ReportChannel
		delete from ReportSection
		delete from ReportLayout

		--RESEED IDENTITY COLUMNS
		dbcc checkident ('ReportChannelLayout', reseed, 0)
		dbcc checkident ('ReportChannel', reseed, 0)
		dbcc checkident ('ReportSection', reseed, 0)
		dbcc checkident ('ReportLayout', reseed, 0)

		--DECLARE SCOPE IDENTITY VARIABLE
		declare @scope_identity int

		--SETUP COMPARATIVE QUOTE REPORT
		insert into ReportChannel (ChannelId, ReportId, Code, CreatedAt, ModifiedAt, UserId) values (1, (select top 1 Id from Report where Name = 'Comparative Quote Report'), 'vrgnmny', getdate(), getdate(), 1)

		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'QuoteHeader', 'Quote Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'QuoteFooter', 'Quote Footer')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'ClientHeader', 'Client Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'CompanyHeader', 'Company Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'QuoteSummary', 'Quote Summary')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'CostFooter', 'Cost Footer')

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteTitle', 'QUOTATION', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'QUOTATION', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteSummary', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteAssets', 'ASSETS', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'ASSETS', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteDate', 'Date:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Date:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteExpires', 'Expires:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Expires:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteNumber', 'Quote No:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Quote No:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientHeader', 'DETAILS OF CLIENT', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'DETAILS OF CLIENT', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralName', 'Client', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Client', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralIdentity', 'Identity No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Identity No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Physical Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Postal Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Work No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactHome', 'Home No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Home No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactCell', 'Cell No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Cell No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Fax No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Email Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyHeader', 'DETAILS OF INTERMEDIARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'DETAILS OF INTERMEDIARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyGeneralName', 'Company', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Company', 'Lifecycle Management (Pty) Ltd', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Physical Address', '2 Park Lane, Umhlanga Ridge, 4319', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Postal Address', 'PO Box 3025, Prestondale, Kwa-Zulu Natal, 4021', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Work No.', '0861 862273', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Fax No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Email Address', 'needsomehelp@virginmoneyinsurance.co.za', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWebsite', 'Website', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Website', 'http://www.virginmoneyinsurance.co.za', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthRegistration', 'Registration No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Registration No.', '2014/075034/07', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthTax', 'Tax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'VAT No.', '4070270881', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthLicense', 'Authorised Financial Services Provider', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Authorised Financial Services Provider', 'No 7146', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteFooter'), 'QuoteFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, '', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryFees', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryTotal', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CostFooter'), 'CostFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, '', '', 1)


		--SETUP FUNERAL QUOTE REPORT
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'QuoteHeader', 'Quote Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'QuoteFooter', 'Quote Footer')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'ClientHeader', 'Client Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'CompanyHeader', 'Company Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'QuoteSummary', 'Quote Summary')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'CostFooter', 'Cost Footer')

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteTitle', 'QUOTATION', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'QUOTATION', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteSummary', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteAssets', 'ASSETS', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'ASSETS', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteDate', 'Date:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Date:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteExpires', 'Expires:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Expires:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteNumber', 'Quote No:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Quote No:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientHeader', 'DETAILS OF CLIENT', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'DETAILS OF CLIENT', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralName', 'Client', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Client', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralIdentity', 'Identity No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Identity No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Physical Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Postal Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Work No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactHome', 'Home No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Home No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactCell', 'Cell No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Cell No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Fax No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Email Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyHeader', 'DETAILS OF INTERMEDIARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'DETAILS OF INTERMEDIARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyGeneralName', 'Company', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Company', 'Lifecycle Management (Pty) Ltd', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Physical Address', '2 Park Lane, Umhlanga Ridge, 4319', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Postal Address', 'PO Box 3025, Prestondale, Kwa-Zulu Natal, 4021', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Work No.', '0861 862273', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Fax No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Email Address', 'needsomehelp@virginmoneyinsurance.co.za', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWebsite', 'Website', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Website', 'http://www.virginmoneyinsurance.co.za', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthRegistration', 'Registration No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Registration No.', '2014/075034/07', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthTax', 'Tax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Tax No.', '4070270881', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthLicense', 'Authorised Financial Services Provider', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, 'Authorised Financial Services Provider', 'No 7146', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteFooter'), 'QuoteFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, '', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryFees', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryTotal', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CostFooter'), 'CostFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (1, @scope_identity, '', '', 1)
	end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_Layout') and type in (N'P', N'PC'))
	begin
		drop procedure Report_Layout
	end
go

create procedure Report_Layout
(
	@ReportName varchar(50)
)
as
	declare @ChannelId int

	set @ChannelId =
	(
		select top 1
			ReportChannel.ChannelId
		from Report
			inner join ReportChannel on ReportChannel.ReportId = Report.Id
			inner join Channel on Channel.Id = ReportChannel.ChannelId
		where
			Channel.IsDefault = 1 and
			Report.Name = @ReportName
	)

	if isnull(@ChannelId, 0) > 0
		begin
			exec Report_Layout_ByChannelId @ChannelId, @ReportName
		end
	else
		begin
			select
				ReportSection.Name SectionName,
				ReportSection.Label SectionLabel,
				ReportSection.IsVisible SectionVisible,
				ReportLayout.Name FieldName,
				ReportLayout.Label FieldLabel,
				ReportLayout.[Value] FieldValue,
				ReportLayout.IsVisible FieldVisible
			from Report
				inner join ReportLayout on ReportLayout.ReportId = Report.Id
				inner join ReportSection on ReportSection.Id = ReportLayout.ReportSectionId
			where
				Report.Name = @ReportName
		end
go

