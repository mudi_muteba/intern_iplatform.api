﻿DECLARE @PartyId int;

INSERT INTO Party (MasterId, PartyTypeId, DisplayName, ChannelId) VALUES (0, 2, 'INDWE Risk Services',1);

SELECT @PartyId = SCOPE_IDENTITY();

INSERT INTO Organization (PartyId, Code ,RegisteredName, TradingName, TradingSince, [Description], FspNo)
VALUES (@PartyId, 'INDWE','INDWE Risk Services', 'INDWE', '2006-01-01', 'Indwe Risk Services (Pty) Ltd is a leading personal, business and specialist risk and insurance advisory business. Indwe came into existence in 2006 – the product of a merger between Thebe Risk Services and Prestasi Brokers, both of which had a formidable history in insurance', '3425');

INSERT INTO Product (MasterId, Name,ProductOwnerId, ProductProviderId, ProductTypeId, AgencyNumber, ProductCode, ImageName, QuoteExpiration)
VALUES (0, 'INDWE', @PartyId, @PartyId, 7, '123456', 'INDWE', 'indwe.png', 30);


