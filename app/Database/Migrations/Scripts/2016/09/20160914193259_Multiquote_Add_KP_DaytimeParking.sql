﻿declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = 218
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = 58 and CoverId = 218   
   
   INSERT into MapQuestionDefinition(ParentId,ChildId)
    Select T2.Id, t4.Id
  From (select Parent.Id from QuestionDefinition Parent where Parent.CoverDefinitionId = @ParentCoverDefinitionId
 and Parent.QuestionId = 86
) T2
   left Join (select Child.Id from QuestionDefinition Child where Child.CoverDefinitionId = @ChildCoverDefinitionId
 and Child.QuestionId = 1030
) T4
   on T2.Id != t4.Id