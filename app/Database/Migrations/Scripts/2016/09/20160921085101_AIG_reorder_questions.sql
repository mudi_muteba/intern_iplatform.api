﻿--contents


update QuestionDefinition set VisibleIndex = 0	 where QuestionId = 88		and CoverDefinitionId = 146
update QuestionDefinition set VisibleIndex = 1	 where QuestionId = 89		and CoverDefinitionId = 146
update QuestionDefinition set VisibleIndex = 2	 where QuestionId = 90		and CoverDefinitionId = 146
update QuestionDefinition set VisibleIndex = 3	 where QuestionId = 47		and CoverDefinitionId = 146
update QuestionDefinition set VisibleIndex = 4	 where QuestionId = 847	and CoverDefinitionId = 146
update QuestionDefinition set VisibleIndex = 5	 where QuestionId = 843	and CoverDefinitionId = 146
update QuestionDefinition set VisibleIndex = 6	 where QuestionId = 848	and CoverDefinitionId = 146
update QuestionDefinition set VisibleIndex = 7	 where QuestionId = 448	and CoverDefinitionId = 146
update QuestionDefinition set VisibleIndex = 8	 where QuestionId = 450	and CoverDefinitionId = 146
update QuestionDefinition set VisibleIndex = 9	 where QuestionId = 468	and CoverDefinitionId = 146
update QuestionDefinition set VisibleIndex = 10	 where QuestionId = 454	and CoverDefinitionId = 146
update QuestionDefinition set VisibleIndex = 11	 where QuestionId = 436	and CoverDefinitionId = 146


--buildings
update QuestionDefinition set VisibleIndex = 0	 where QuestionId = 88    and CoverDefinitionId = 147
update QuestionDefinition set VisibleIndex = 1	 where QuestionId = 89    and CoverDefinitionId = 147
update QuestionDefinition set VisibleIndex = 2	 where QuestionId = 90    and CoverDefinitionId = 147
update QuestionDefinition set VisibleIndex = 3	 where QuestionId = 47    and CoverDefinitionId = 147
update QuestionDefinition set VisibleIndex = 4	 where QuestionId = 847  and CoverDefinitionId = 147
update QuestionDefinition set VisibleIndex = 5	 where QuestionId = 843  and CoverDefinitionId = 147
update QuestionDefinition set VisibleIndex = 6	 where QuestionId = 848  and CoverDefinitionId = 147
update QuestionDefinition set VisibleIndex = 7	 where QuestionId = 420  and CoverDefinitionId = 147
update QuestionDefinition set VisibleIndex = 8	 where QuestionId = 396  and CoverDefinitionId = 147
update QuestionDefinition set VisibleIndex = 9	 where QuestionId = 398  and CoverDefinitionId = 147
update QuestionDefinition set VisibleIndex = 10	 where QuestionId = 386  and CoverDefinitionId = 147
update QuestionDefinition set VisibleIndex = 11	 where QuestionId = 416  and CoverDefinitionId = 147
update QuestionDefinition set VisibleIndex = 12	 where QuestionId = 402  and CoverDefinitionId = 147

--move excess question to finance group 
update md.Question set QuestionGroupId = 7 where id = 874

update QuestionDefinition set VisibleIndex = 0	 where QuestionId = 862	 and CoverDefinitionId = 145
update QuestionDefinition set VisibleIndex = 1	 where QuestionId = 873	 and CoverDefinitionId = 145
update QuestionDefinition set VisibleIndex = 2	 where QuestionId = 300	 and CoverDefinitionId = 145
update QuestionDefinition set VisibleIndex = 3	 where QuestionId = 102	 and CoverDefinitionId = 145
update QuestionDefinition set VisibleIndex = 4	 where QuestionId = 874	 and CoverDefinitionId = 145



