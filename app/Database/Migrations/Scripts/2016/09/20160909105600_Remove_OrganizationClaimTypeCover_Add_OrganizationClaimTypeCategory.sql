﻿
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'OrganizationClaimTypeCover')
BEGIN
  drop table OrganizationClaimTypeCover
END

Declare @OrgId int;

SELECT @OrgId = PartyId FROM Organization WHERE Code = 'INDWE';

IF (@OrgId > 0)
BEGIN
	INSERT INTO OrganizationClaimTypeCategory (OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) Values 
	(@OrgId, 2, 2, 1,0),
	(@OrgId, 2, 1, 2,0),
	(@OrgId, 2, 42, 3,0),
	(@OrgId, 2, 43, 4,0),
	(@OrgId, 2, 24, 5,0),

	(@OrgId, 1, 8, 1,0),
	(@OrgId, 1, 30, 2,0),
	(@OrgId, 1, 32, 3,0),
	(@OrgId, 1, 39, 4,0),

	(@OrgId, 3, 37, 1,0),
	(@OrgId, 3, 29, 2,0),
	(@OrgId, 3, 36, 3,0);
END