﻿
DECLARE @Partyid INT

SET  @Partyid= (SELECT TOP 1 Id From [dbo].[Party] WHERE DisplayName =  N'MiWay')

If (@Partyid IS NULL)
BEGIN

	INSERT [dbo].[Party] ([MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) 
	VALUES (0, 2, NULL, N'MiWay', NULL, NULL)

	SET @Partyid = IDENT_CURRENT('Party')
END

IF((SELECT  1 FROM [dbo].[Organization]  WHERE Code = 'MW' AND  RegisteredName =  N'MiWay Insurance Limited') IS NULL )
BEGIN

INSERT [dbo].[Organization] ([PartyId], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo])
 VALUES (@Partyid,  N'MW', N'MiWay Insurance Limited', N'MiWay', '01/JAN/2007', '', N'2007/026289/06', N'33970', N'')

--address
INSERT [dbo].[Address] ([PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) 
VALUES (@Partyid, 0, N'MiWay Insurance', N'Samrand Business Park', N'48 Sterling Road', N'Kosmosdal Ext 12', N'', N'Centurion', N'0157', CAST(-25.922191 AS Decimal(9, 6)), CAST(28.146177999999964 AS Decimal(9, 6)), 4, 1, 1, 1, '01/JAN/1970', NULL)

END

DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT


SET @id = (SELECT TOP 1 PartyId From [Organization] where TradingName = 'MiWay')
if (@id IS NULL)
Return

SET @Name = N'MIWHEELS'

 
SET @productid = ( SELECT ID FROM dbo.Product where  NAme= @Name AND ProductOwnerID = @id)

if (@productid IS NULL)
BEGIN
--Hollard
INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          RiskItemTypeId ,
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          @Name , -- Name - nvarchar(255)
          @id , -- ProductOwnerId - int
          @id , -- ProductProviderId - int
          7 , -- ProductTypeId - int
          N'123456' , -- AgencyNumber - nvarchar(255)
          N'MW' , -- ProductCode - nvarchar(255)
          CAST(0x00009CD400000000 AS DateTime) , -- StartDate - datetime
          CAST(0x00009CD400000000 AS DateTime) , -- EndDate - datetime
          0 , -- RiskItemTypeId - int
          0 , -- IsDeleted - bit
          N'mw.png' , -- ImageName - nvarchar(1024)
          NULL , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )

		SET @id = SCOPE_IDENTITY()
 END
 ELSE
 BEGIN
	SET @id = @productid
 END


declare @CoverDefinitionId int
--motor

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = 218) -- 218 is MOTOR

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'Motor', @id, 218, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 1, 0, 0,
               N'I''d like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Registration Number', @CoverDefinitionId, 97, NULL, 1, 1, 1, 1, 0,
               N'What is the registration number for this vehicle?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'Year Of Manufacture', N'' , N'^[0-9]+$', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Make', @CoverDefinitionId, 95, NULL, 1, 3, 1, 1, 0,
               N'{Title} {Name} what is the make of the vehicle?', N'' , N'', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Model', @CoverDefinitionId, 96, NULL, 1, 4, 1, 1, 0,
               N'{Title} {Name} what is the model of the vehicle?', N'' , N'', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle MM Code', @CoverDefinitionId, 94, NULL, 1, 5, 1, 1, 0,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=69 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Type', @CoverDefinitionId, 69, NULL, 1, 6, 1, 1, 0,
               N'Vehicle Type', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'VIN Number', @CoverDefinitionId, 1010, NULL, 1, 7, 1, 1, 0,
               N'VIN Number', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Engine Number', @CoverDefinitionId, 1013, NULL, 1, 8, 1, 1, 0,
               N'Engine Number', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=142 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Colour', @CoverDefinitionId, 142, NULL, 1, 9, 1, 1, 0,
               N'Vehicle Colour', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=143 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Paint Type', @CoverDefinitionId, 143, NULL, 1, 14, 0, 1, 0,
               N'Vehicle Paint Type', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1095 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Class Of Use', @CoverDefinitionId, 1095, NULL, 1, 7, 1, 1, 0,
               N'Class Of Use', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1012 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Delivery Taken', @CoverDefinitionId, 1012, NULL, 1, 12, 0, 1, 0,
               N'Delivery Taken', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=867 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Status', @CoverDefinitionId, 867, NULL, 1, 13, 1, 1, 0,
               N'Vehicle Status', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=7 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Modified/Imported', @CoverDefinitionId, 7, NULL, 1, 10, 0, 1, 0,
               N'Modified/Imported', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1016 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Engine Modified Description', @CoverDefinitionId, 1016, NULL, 1, 11, 0, 0, 0,
               N'Engine Modified Description', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=85 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Overnight Parking', @CoverDefinitionId, 85, NULL, 1, 0, 1, 1, 0,
               N'Vehicle Overnight Parking', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=86 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Daytime Parking', @CoverDefinitionId, 86, NULL, 1, 1, 1, 1, 0,
               N'Vehicle Daytime Parking', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1089 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Anti Hijack', @CoverDefinitionId, 1089, NULL, 1, 2, 0, 1, 0,
               N'Anti Hijack', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1090 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Early Warning Tracker', @CoverDefinitionId, 1090, NULL, 1, 3, 0, 1, 0,
               N'Early Warning Tracker', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=87 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Tracking Device', @CoverDefinitionId, 87, NULL, 1, 4, 1, 1, 0,
               N'Tracking Device', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1092 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Tracking Device Contact Number', @CoverDefinitionId, 1092, NULL, 1, 5, 0, 0, 0,
               N'Tracking Device Contract Number', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=80 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Immobiliser', @CoverDefinitionId, 80, NULL, 1, 7, 1, 1, 0,
               N'Vehicle Immobiliser', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=9 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle DataDot', @CoverDefinitionId, 9, NULL, 1, 9, 0, 1, 0,
               N'Vehicle DataDot', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1096 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Valuation Method', @CoverDefinitionId, 1096, NULL, 1, 44, 1, 1, 0,
               N'Valuation Method', N'' , N'', 0, NULL)
              end
--if not exists(select * from QuestionDefinition where QuestionId=105 and CoverDefinitionId = @CoverDefinitionId)
--              begin
--              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
--              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver Age', @CoverDefinitionId, 105, NULL, 1, 0, 1, 1, 0,
--               N'Main Driver Age', N'' , N'', 0, NULL)
--              end
if not exists(select * from QuestionDefinition where QuestionId=72 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver Marital Status', @CoverDefinitionId, 72, NULL, 1, 1, 1, 1, 0,
               N'Main Driver Marital Status', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=70 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver Gender', @CoverDefinitionId, 70, NULL, 1, 2, 1, 1, 0,
               N'Main Driver Gender', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=60 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Relationship to the Insured', @CoverDefinitionId, 60, NULL, 1, 3, 1, 1, 0,
               N'Relationship to the Insured', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=862 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Financed', @CoverDefinitionId, 862, NULL, 1, 0, 0, 1, 0,
               N'Financed', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1086 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Finance House', @CoverDefinitionId, 1086, NULL, 1, 1, 0, 1, 0,
               N'Finance House', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1087 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Finance House Other', @CoverDefinitionId, 1087, NULL, 1, 2, 0, 0, 0,
               N'Finance House Other', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1088 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Finance Agreement Number', @CoverDefinitionId, 1088, NULL, 1, 3, 0, 0, 0,
               N'Finance Agreement Number', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 4, 1, 1, 0,
               N'Sum Insured', N'' , N'^[0-9]+$', 0, NULL)
              end



