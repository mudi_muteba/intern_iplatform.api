﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT
 
SET @id = 4 --hollard
SET @Name = N'EASY'
 
SET @productid = ( SELECT ID FROM dbo.Product where  NAme= @Name AND ProductOwnerID = @id)

if (@productid IS NULL)
BEGIN
--Hollard
INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          RiskItemTypeId ,
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          @Name , -- Name - nvarchar(255)
          @id , -- ProductOwnerId - int
          @id , -- ProductProviderId - int
          7 , -- ProductTypeId - int
          N'123456' , -- AgencyNumber - nvarchar(255)
          N'HOLEASY' , -- ProductCode - nvarchar(255)
          CAST(0x00009CD400000000 AS DateTime) , -- StartDate - datetime
          CAST(0x00009CD400000000 AS DateTime) , -- EndDate - datetime
          0 , -- RiskItemTypeId - int
          0 , -- IsDeleted - bit
          N'hol.png' , -- ImageName - nvarchar(1024)
          NULL , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )

		SET @id = SCOPE_IDENTITY()
 END
 ELSE
 BEGIN
	SET @id = @productid
 END

--motor

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = 218) -- 218 is MOTOR

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'Motor', @id, 218, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 0, 0,
               N'I''d like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=139 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Daytime address', @CoverDefinitionId, 139, NULL, 1, 10, 0, 0, 0,
               N'Daytime address', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=86 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Day Parking', @CoverDefinitionId, 86, NULL, 1, 20, 0, 0, 0,
               N'Vehicle Day Parking', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=140 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Overnight Address', @CoverDefinitionId, 140, NULL, 1, 30, 0, 0, 0,
               N'{Title} {Name} would you please provide me with the overnight address?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=85 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Overnight Parking', @CoverDefinitionId, 85, NULL, 1, 40, 0, 0, 0,
               N'Vehicle Overnight Parking', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Registration Number', @CoverDefinitionId, 97, NULL, 1, 50, 0, 0, 0,
               N'What is the registration number for this vehicle?', N'' , N'^.{0,10}$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 60, 0, 0, 0,
               N'Year Of Manufacture', N'' , N'^[0-9]+$', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 70, 0, 0, 0,
               N'{Title} {Name} what is the make of the vehicle?', N'' , N'', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 80, 0, 0, 0,
               N'{Title} {Name} what is the model of the vehicle?', N'' , N'', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 90, 0, 0, 0,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'VIN Number', @CoverDefinitionId, 1010, NULL, 1, 100, 0, 0, 0,
               N'VIN Number', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Engine Number', @CoverDefinitionId, 1013, NULL, 1, 110, 0, 0, 0,
               N'Engine Number', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=142 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Colour', @CoverDefinitionId, 142, NULL, 1, 120, 0, 0, 0,
               N'Vehicle Colour', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=143 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Paint Type', @CoverDefinitionId, 143, NULL, 1, 130, 0, 0, 0,
               N'Vehicle Paint Type', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=151 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Motor Mileage', @CoverDefinitionId, 151, NULL, 1, 140, 0, 0, 0,
               N'Motor Mileage', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=7 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Modified/Imported', @CoverDefinitionId, 7, NULL, 1, 150, 0, 0, 0,
               N'Has this vehicle been modified or imported?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1100 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Scratch And Dent', @CoverDefinitionId, 1100, NULL, 1, 160, 0, 1, 0,
               N'Scratch And Dent', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1101 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Tyre And Rim', @CoverDefinitionId, 1101, NULL, 1, 170, 0, 1, 0,
               N'Tyre And Rim', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1118 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Code', @CoverDefinitionId, 1118, NULL, 1, 180, 0, 0, 0,
               N'Vehicle Code', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1119 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Type', @CoverDefinitionId, 1119, NULL, 1, 190, 0, 0, 0,
               N'Vehicle Type', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1154 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Class Of Use', @CoverDefinitionId, 1154, NULL, 1, 7, 0, 0, 0,
               N'Vehicle Use', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=80 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Immobiliser', @CoverDefinitionId, 80, NULL, 1, 210, 0, 0, 0,
               N'Does the vehicle have a factory fitted immobiliser?', N'250' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=87 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Tracking Device', @CoverDefinitionId, 87, NULL, 1, 220, 0, 0, 0,
               N'Is there a tracking device installed in this vehicle?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1099 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Car Hire Group', @CoverDefinitionId, 1099, NULL, 1, 21, 0, 0, 0,
               N'Car Hire Group', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=77 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Car Hire', @CoverDefinitionId, 77, NULL, 1, 240, 0, 0, 0,
               N'Car Hire', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=100 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Registered Owner ID Number', @CoverDefinitionId, 100, NULL, 1, 250, 0, 0, 0,
               N'What is the ID Number for the registered owner of this vehicle?', N'' , N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=60 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Relationship to the Insured', @CoverDefinitionId, 60, NULL, 1, 260, 0, 0, 0,
               N'What is the drivers relationship to the insured?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1110 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Insured Type', @CoverDefinitionId, 1110, NULL, 1, 270, 0, 0, 0,
               N'Insured Type', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1111 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Insurable Interest', @CoverDefinitionId, 1111, NULL, 1, 280, 0, 0, 0,
               N'Insurable Interest', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=114 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Current Insurance Period (In Years)', @CoverDefinitionId, 114, NULL, 1, 290, 0, 0, 0,
               N'How long has the client been at his current insurance company?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=39 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Previously Insured', @CoverDefinitionId, 39, NULL, 1, 300, 0, 1, 0,
               N'Has this vehicle been insured before?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 310, 0, 0, 0,
               N'Sum Insured', N'' , N'^[0-9]+$', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=104 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver ID Number', @CoverDefinitionId, 104, NULL, 1, 315, 0, 0, 0,
               N'What is the ID Number of the main driver of this vehicle?', N'' , N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=144 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver First Name', @CoverDefinitionId, 144, NULL, 1, 320, 0, 0, 0,
               N'Main Driver First Name', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=145 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver Surname', @CoverDefinitionId, 145, NULL, 1, 330, 0, 0, 0,
               N'{Title} {Name} would you please provide me with the main drivers surname?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=146 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver Title', @CoverDefinitionId, 146, NULL, 1, 340, 0, 0, 0,
               N'{Title} {Name} would you please provide me with the main drivers title?', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=42 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver Date of Birth', @CoverDefinitionId, 42, NULL, 1, 360, 0, 0, 0,
               N'What is the Date of Birth of the main driver of this vehicle?', N'' , N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=70 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver Gender', @CoverDefinitionId, 70, NULL, 1, 370, 0, 0, 0,
               N'What is the Gender of the main driver of this vehicle?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=41 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Drivers Licence First Issued Date', @CoverDefinitionId, 41, NULL, 1, 380, 0, 0, 0,
               N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'' , N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=73 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Drivers Licence Code', @CoverDefinitionId, 73, NULL, 1, 390, 0, 0, 0,
               N'Drivers Licence Code', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=72 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Driver Marital Status', @CoverDefinitionId, 72, NULL, 1, 400, 0, 0, 0,
               N'Driver Marital Status', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1121 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Driver Employment Status', @CoverDefinitionId, 1121, NULL, 1, 410, 0, 0, 0,
               N'Driver Employment Status', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1122 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Driver Occupation', @CoverDefinitionId, 1122, NULL, 1, 420, 0, 0, 0,
               N'Driver Occupation', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1123 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Time Traveling', @CoverDefinitionId, 1123, NULL, 1, 430, 0, 0, 0,
               N'Time Traveling', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1103 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'License Endorsed', @CoverDefinitionId, 1103, NULL, 1, 440, 0, 1, 0,
               N'License Endorsed', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1098 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Motor Excess', @CoverDefinitionId, 1098, NULL, 1, 450, 0, 0, 0,
               N'Motor Excess', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=44 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 months', @CoverDefinitionId, 44, NULL, 1, 460, 0, 0, 0,
               N'How many claims have the client registered in the past 12 months?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=45 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 to 24 months', @CoverDefinitionId, 45, NULL, 1, 470, 0, 0, 0,
               N'How many claims have the client registered in the past 12 to 24 months?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=46 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 24 to 36 months', @CoverDefinitionId, 46, NULL, 1, 480, 0, 0, 0,
               N'How many claims have the client registered in the past 24 to 36 months?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1159 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Excess Profile', @CoverDefinitionId, 1159, NULL, 1, 490, 0, 0, 0,
               N'Excess Profile', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=862 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Financed', @CoverDefinitionId, 862, NULL, 1, 500, 0, 0, 0,
               N'', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1106 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Finance Amount', @CoverDefinitionId, 1106, NULL, 1, 510, 0, 0, 0,
               N'Finance Amount', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1107 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Finance House', @CoverDefinitionId, 1107, NULL, 1, 520, 0, 0, 0,
               N'Finance House', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1108 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Finance Term', @CoverDefinitionId, 1108, NULL, 1, 530, 0, 0, 0,
               N'Finance Term', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1109 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Monthly Installments', @CoverDefinitionId, 1109, NULL, 1, 540, 0, 0, 0,
               N'Monthly Installments', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1113 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'New List Price', @CoverDefinitionId, 1113, NULL, 1, 550, 0, 0, 0,
               N'New List Price', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1115 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Residual', @CoverDefinitionId, 1115, NULL, 1, 560, 0, 0, 0,
               N'Residual', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1116 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Retail Plus Adjustment', @CoverDefinitionId, 1116, NULL, 1, 570, 0, 0, 0,
               N'Retail Plus Adjustment', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1117 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Retail Plus Percentage', @CoverDefinitionId, 1117, NULL, 1, 580, 0, 0, 0,
               N'Retail Plus Percentage', N'' , N'^[0-9]+$', 0, NULL)
              end

-------------------------------------------BUILDINGS-----------------------------------------------------------------------


SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = 44) -- House Owners 44

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'Building', @id, 44, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Address', @CoverDefinitionId, 88, NULL, 1, 0, 0, 1, 0,
               N'What is the address where the item is located?', N'' , N'^[0-9]+$', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 10, 0, 1, 0,
               N'What is the suburb name where the property is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 20, 0, 1, 0,
               N'What is the postal code where the property is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 30, 0, 1, 0,
               N'What is the province  where the property is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=21 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Access Controlled Suburb', @CoverDefinitionId, 21, NULL, 1, 40, 0, 1, 0,
               N'Access Controlled Suburb', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1125 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Building Contents Excess', @CoverDefinitionId, 1125, NULL, 1, 50, 0, 0, 0,
               N'Building Contents Excess', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1127 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Roof Construction', @CoverDefinitionId, 1127, NULL, 1, 60, 0, 0, 0,
               N'Roof Construction', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1128 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Wall Construction', @CoverDefinitionId, 1128, NULL, 1, 70, 0, 0, 0,
               N'Wall Construction', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1129 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Residence Type', @CoverDefinitionId, 1129, NULL, 1, 80, 0, 0, 0,
               N'Residence Type', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1130 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Business Nature', @CoverDefinitionId, 1130, NULL, 1, 90, 0, 0, 0,
               N'Business Nature', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1131 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Dwelling Type', @CoverDefinitionId, 1131, NULL, 1, 100, 0, 0, 0,
               N'Dwelling Type', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1132 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Rooms', @CoverDefinitionId, 1132, NULL, 1, 110, 0, 0, 0,
               N'Rooms', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1133 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Land', @CoverDefinitionId, 1133, NULL, 1, 120, 0, 0, 0,
               N'Adjacent Use Land', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1134 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Settlement', @CoverDefinitionId, 1134, NULL, 1, 130, 0, 0, 0,
               N'Adjacent Use Settlement', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1135 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Agriculture', @CoverDefinitionId, 1135, NULL, 1, 140, 0, 0, 0,
               N'Adjacent Use Agriculture', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1136 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Farm', @CoverDefinitionId, 1136, NULL, 1, 150, 0, 0, 0,
               N'Adjacent Use Farm', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1137 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Residence', @CoverDefinitionId, 1137, NULL, 1, 160, 0, 0, 0,
               N'Adjacent Use Residence', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1138 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Office', @CoverDefinitionId, 1138, NULL, 1, 170, 0, 0, 0,
               N'Adjacent Use Office', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1139 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Retail', @CoverDefinitionId, 1139, NULL, 1, 180, 0, 0, 0,
               N'Adjacent Use Retail', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1140 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use School', @CoverDefinitionId, 1140, NULL, 1, 190, 0, 0, 0,
               N'Adjacent Use School', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Near Water Body', @CoverDefinitionId, 1141, NULL, 1, 200, 0, 0, 0,
               N'Near Water Body', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1142 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Electric Geysers', @CoverDefinitionId, 1142, NULL, 1, 210, 0, 0, 0,
               N'Electric Geysers', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1143 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Heat Pump Geysers', @CoverDefinitionId, 1143, NULL, 1, 220, 0, 0, 0,
               N'Heat Pump Geysers', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1144 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Solar Geysers', @CoverDefinitionId, 1144, NULL, 1, 230, 0, 0, 0,
               N'Solar Geysers', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1145 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Gas Geysers', @CoverDefinitionId, 1145, NULL, 1, 240, 0, 0, 0,
               N'Gas Geysers', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1146 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Renewable Energy Sum Insured', @CoverDefinitionId, 1146, NULL, 1, 250, 0, 0, 0,
               N'Renewable Energy Sum Insured', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1147 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Geysers Sum Insured', @CoverDefinitionId, 1147, NULL, 1, 260, 0, 0, 0,
               N'Geysers Sum Insured', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1155 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Rented Building', @CoverDefinitionId, 1155, NULL, 1, 270, 0, 0, 0,
               N'Rented Building', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1156 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Commune', @CoverDefinitionId, 1156, NULL, 1, 280, 0, 0, 0,
               N'Commune', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1157 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Premises Use', @CoverDefinitionId, 1157, NULL, 1, 290, 0, 0, 0,
               N'Premises Use', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1158 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Title Holder', @CoverDefinitionId, 1158, NULL, 1, 300, 0, 0, 0,
               N'Title Holder', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=4 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Lightning Conductor', @CoverDefinitionId, 4, NULL, 1, 310, 0, 1, 0,
               N'Lightning Conductor', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=16 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Thatch Safe', @CoverDefinitionId, 16, NULL, 1, 320, 0, 0, 0,
               N'Is the thatch treated with thatch safe?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=2 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Plot', @CoverDefinitionId, 2, NULL, 1, 330, 0, 0, 0,
               N'Is the property a plot?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=179 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Alarm', @CoverDefinitionId, 179, NULL, 1, 340, 0, 1, 0,
               N'Does the home have an Alarm?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=184 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Is the building unoccupied', @CoverDefinitionId, 184, NULL, 1, 350, 0, 1, 0,
               N'Number of days the building is unoccupied', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=118 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Power Surge  Protection', @CoverDefinitionId, 118, NULL, 1, 360, 0, 0, 0,
               N'Power Surge Protection', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=185 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Full time security guards', @CoverDefinitionId, 185, NULL, 1, 370, 0, 0, 0,
               N'Full time security guards', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=44 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 months', @CoverDefinitionId, 44, NULL, 1, 380, 0, 0, 0,
               N'How many claims have the client registered in the past 12 months?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=45 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 to 24 months', @CoverDefinitionId, 45, NULL, 1, 390, 0, 0, 0,
               N'How many claims have the client registered in the past 12 to 24 months?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=46 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 24 to 36 months', @CoverDefinitionId, 46, NULL, 1, 400, 0, 0, 0,
               N'How many claims have the client registered in the past 24 to 36 months?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=114 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Current Insurance Period (In Years)', @CoverDefinitionId, 114, NULL, 1, 410, 0, 0, 0,
               N'How long has the client been at his current insurance company?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 420, 0, 1, 0,
               N'What is the sum insured of the property?', N'' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1097 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Perimeter Wall', @CoverDefinitionId, 1097, NULL, 1, 430, 0, 0, 0,
               N'What is the perimeter wall made of?', N'' , N'', 0, NULL)
              end

--------------------------CONTENTS------------------------------------------------------------


SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = 78) -- Household Contents 78

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'Household Contents', @id, 78, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END
if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Address', @CoverDefinitionId, 88, NULL, 1, 0, 0, 1, 0,
               N'What is the address where the item is located?', N'' , N'^[0-9]+$', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 10, 0, 1, 0,
               N'What is the suburb name where the property is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 20, 0, 1, 0,
               N'What is the postal code where the property is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 30, 0, 1, 0,
               N'What is the province  where the property is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=21 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Access Controlled Suburb', @CoverDefinitionId, 21, NULL, 1, 40, 0, 1, 0,
               N'Access Controlled Suburb', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1125 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Building Contents Excess', @CoverDefinitionId, 1125, NULL, 1, 50, 0, 0, 0,
               N'Building Contents Excess', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1127 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Roof Construction', @CoverDefinitionId, 1127, NULL, 1, 60, 0, 0, 0,
               N'Roof Construction', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1128 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Wall Construction', @CoverDefinitionId, 1128, NULL, 1, 70, 0, 0, 0,
               N'Wall Construction', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1129 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Residence Type', @CoverDefinitionId, 1129, NULL, 1, 80, 0, 0, 0,
               N'Residence Type', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1130 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Business Nature', @CoverDefinitionId, 1130, NULL, 1, 90, 0, 0, 0,
               N'Business Nature', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1131 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Dwelling Type', @CoverDefinitionId, 1131, NULL, 1, 100, 0, 0, 0,
               N'Dwelling Type', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1132 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Rooms', @CoverDefinitionId, 1132, NULL, 1, 110, 0, 0, 0,
               N'Rooms', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1133 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Land', @CoverDefinitionId, 1133, NULL, 1, 120, 0, 0, 0,
               N'Adjacent Use Land', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1134 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Settlement', @CoverDefinitionId, 1134, NULL, 1, 130, 0, 0, 0,
               N'Adjacent Use Settlement', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1135 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Agriculture', @CoverDefinitionId, 1135, NULL, 1, 140, 0, 0, 0,
               N'Adjacent Use Agriculture', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1136 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Farm', @CoverDefinitionId, 1136, NULL, 1, 150, 0, 0, 0,
               N'Adjacent Use Farm', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1137 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Residence', @CoverDefinitionId, 1137, NULL, 1, 160, 0, 0, 0,
               N'Adjacent Use Residence', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1138 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Office', @CoverDefinitionId, 1138, NULL, 1, 170, 0, 0, 0,
               N'Adjacent Use Office', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1139 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use Retail', @CoverDefinitionId, 1139, NULL, 1, 180, 0, 0, 0,
               N'Adjacent Use Retail', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1140 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Adjacent Use School', @CoverDefinitionId, 1140, NULL, 1, 190, 0, 0, 0,
               N'Adjacent Use School', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Near Water Body', @CoverDefinitionId, 1141, NULL, 1, 200, 0, 0, 0,
               N'Near Water Body', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1142 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Electric Geysers', @CoverDefinitionId, 1142, NULL, 1, 210, 0, 0, 0,
               N'Electric Geysers', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1143 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Heat Pump Geysers', @CoverDefinitionId, 1143, NULL, 1, 220, 0, 0, 0,
               N'Heat Pump Geysers', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1144 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Solar Geysers', @CoverDefinitionId, 1144, NULL, 1, 230, 0, 0, 0,
               N'Solar Geysers', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1145 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Gas Geysers', @CoverDefinitionId, 1145, NULL, 1, 240, 0, 0, 0,
               N'Gas Geysers', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1155 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Rented Building', @CoverDefinitionId, 1155, NULL, 1, 250, 0, 0, 0,
               N'Rented Building', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1156 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Commune', @CoverDefinitionId, 1156, NULL, 1, 260, 0, 0, 0,
               N'Commune', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1157 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Premises Use', @CoverDefinitionId, 1157, NULL, 1, 270, 0, 0, 0,
               N'Premises Use', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=4 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Lightning Conductor', @CoverDefinitionId, 4, NULL, 1, 280, 0, 1, 0,
               N'Lightning Conductor', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=16 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Thatch Safe', @CoverDefinitionId, 16, NULL, 1, 290, 0, 0, 0,
               N'Is the thatch treated with thatch safe?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=2 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Plot', @CoverDefinitionId, 2, NULL, 1, 300, 0, 0, 0,
               N'Is the property a plot?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=179 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Alarm', @CoverDefinitionId, 179, NULL, 1, 310, 0, 1, 0,
               N'Does the home have an Alarm?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=11 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Burglar Bars', @CoverDefinitionId, 11, NULL, 1, 320, 0, 0, 0,
               N'Does the house where the content is located have burglar bars?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=15 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Security Gates', @CoverDefinitionId, 15, NULL, 1, 330, 0, 0, 0,
               N'Security Gates', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=18 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Electric Fence', @CoverDefinitionId, 18, NULL, 1, 340, 0, 0, 0,
               N'Electric Fence', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=184 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Is the building unoccupied', @CoverDefinitionId, 184, NULL, 1, 350, 0, 1, 0,
               N'Number of days the building is unoccupied', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=118 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Power Surge  Protection', @CoverDefinitionId, 118, NULL, 1, 360, 0, 0, 0,
               N'Power Surge Protection', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=185 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Full time security guards', @CoverDefinitionId, 185, NULL, 1, 370, 0, 0, 0,
               N'Full time security guards', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1126 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Tv Limit', @CoverDefinitionId, 1126, NULL, 1, 380, 0, 0, 0,
               N'Tv Limit', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=44 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 months', @CoverDefinitionId, 44, NULL, 1, 390, 0, 0, 0,
               N'How many claims have the client registered in the past 12 months?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=45 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 to 24 months', @CoverDefinitionId, 45, NULL, 1, 400, 0, 0, 0,
               N'How many claims have the client registered in the past 12 to 24 months?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=46 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 24 to 36 months', @CoverDefinitionId, 46, NULL, 1, 410, 0, 0, 0,
               N'How many claims have the client registered in the past 24 to 36 months?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=114 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Current Insurance Period (In Years)', @CoverDefinitionId, 114, NULL, 1, 420, 0, 0, 0,
               N'How long has the client been at his current insurance company?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 430, 0, 1, 0,
               N'What is the sum insured of the property?', N'' , N'^[0-9\.]+$', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=1097 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Perimeter Wall', @CoverDefinitionId, 1097, NULL, 1, 440, 0, 0, 0,
               N'What is the perimeter wall made of?', N'' , N'', 0, NULL)
              end
