Update ProductClaimsQuestionDefinition SET DisplayName = 'Accident Date' WHERE ProductId = 59 AND ClaimsQuestionDefinitionId = 98
Update ProductClaimsQuestionDefinition SET DisplayName = 'Accident Time' WHERE ProductId = 59 AND ClaimsQuestionDefinitionId = 99
Update ProductClaimsQuestionDefinition SET DisplayName = 'Incident Description', IsDeleted = 1 WHERE ProductId = 59 AND ClaimsQuestionDefinitionId = 1033
Update ProductClaimsQuestionDefinition SET DisplayName = 'Theft Date' WHERE ProductId = 59 AND ClaimsQuestionDefinitionId = 185
Update ProductClaimsQuestionDefinition SET DisplayName = 'Theft Time' WHERE ProductId = 59 AND ClaimsQuestionDefinitionId = 186