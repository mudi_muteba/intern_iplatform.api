﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')

DECLARE @CoverID INT
SET @CoverID=(select ID from md.Cover where name ='Building')

DECLARE @CoverDefid INT
SET @CoverDefid=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId=@CoverID)

if not exists(select * from QuestionDefinition where QuestionId=40 and CoverDefinitionId = @CoverDefid)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Occupation Date',@CoverDefid,40,NULL,1,13,1,1,0,'When was the house occupied by the insured?',N'',N'',0,NULL,0)
		END