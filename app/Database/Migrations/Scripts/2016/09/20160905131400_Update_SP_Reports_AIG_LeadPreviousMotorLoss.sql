if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_LeadPreviousMotorLoss') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_AIG_LeadPreviousMotorLoss
	end
go
Create procedure Reports_AIG_LeadPreviousMotorLoss
--(
--	--@PartyId int
--)
as
	SELECT ROW_NUMBER() OVER(ORDER BY mlh.id DESC) AS Row, mlh.PartyId,'Auto' as [Type], mlh.DateOfLoss, mtl.Answer as MotorLossType, mca.Answer as MotorLossClaim from MotorLossHistory as mlh
	join md.MotorTypeOfLoss as mtl on mtl.id = mlh.MotorTypeOfLossId
	join md.MotorClaimAmount as mca on mca.Id = mlh.MotorClaimAmountId
	where --PartyId = @PartyId and 
	mlh.IsDeleted = 0