if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_AllRiskExtract') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_AIG_AllRiskExtract
	end
go

create procedure Reports_AIG_AllRiskExtract
(
--@PartyId int
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as

----declare @PartyId int
--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
----set @PartyId = 1268 
--set @StartDate = '2016-06-09'
--set @EndDate = '2016-09-22'

DECLARE @tbl TABLE(QuestionId int, QuoteId int, AnswerId nvarchar(255), PartyId int, Created datetime, ProposalHeaderId int, ProposalDefinitionId int)

insert into @tbl
select qd.QuestionId, qte.Id as QuoteId,pqa.Answer as AnswerId, pd.PartyId, pd.Created, pd.ProposalHeaderId, pd.Id from ProposalDefinition as pd
				join ProposalQuestionAnswer as pqa on pqa.ProposalDefinitionId = pd.Id
				join QuestionDefinition as qd on qd.Id = pqa.QuestionDefinitionId
				join md.Question as q on q.Id = qd.QuestionId
				join ProposalHeader as ph on ph.Id = pd.ProposalHeaderId
				join QuoteHeader as qh on qh.ProposalHeaderId = ph.Id
				join Quote as qte on qte.QuoteHeaderId = qh.Id			
				where --pd.PartyId = @PartyId and 
				pd.CoverId = 17 and pd.IsDeleted = 0 and qte.IsDeleted =0 and qd.id = pqa.QuestionDefinitionId and qd.QuestionId in (102,155,490,492,494,496,500,89,90,47)


Select --qute.Id, p.id as 'partyId',
	(select [user].UserName from LeadActivity as la
	join [user] on [user].Id = la.UserId
	where la.Id = prh.LeadActivityId
	) as AgentName,

	CASE 
		WHEN Prh.Source IS NULL THEN ('Call center')   
		ELSE Prh.Source                                     
    END as Channel, 

	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'Date of Quote',
	CASE
		WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y'
		ELSE 'N'
	END as 'QuoteExpired',

	CASE 
	WHEN PhS.Id IS NULL THEN ('N')   
	WHEN PhS.Id = 1 THEN ('N')
	WHEN PhS.Id = 2 THEN ('N')
	WHEN PhS.Id = 3 THEN ('Y')                                         
	END AS 'Quote-Bound',

	case when Qute.AcceptedOn IS NULL Then '' else CONVERT(VARCHAR(10),Qute.AcceptedOn ,111) end as 'QuoteBindDate',

	(select cast(round (Premium ,2) as numeric(36,2)) from QuoteItem as Qi		
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
		where qi.Id = QteI.Id and cd.CoverId in (218, 17, 45, 142, 373, 372, 258) 
	) as 'QuoteTotalPremium',

 	CASE WHEN (select Premium from QuoteItem as Qi		
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId		
		where --QuoteId = Qute.Id and 
		cd.CoverId = 218 and qi.Id = QteI.Id
	 ) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId = 218 and qi.Id = QteI.Id)                                          
     END AS 'QuoteMotorPremium',
	 
	 CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (17,45) and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select  cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (17,45) and qi.Id = QteI.Id)                                          
     END AS 'QuoteHomePremium',

	CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId = 142 and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId = 142 and qi.Id = QteI.Id)                                          
     END AS 'QuoteFuneralPremium',	

	CASE WHEN (select sum(Premium) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (373, 372, 258) and qi.Id = QteI.Id) IS NULL 
	 THEN (0)   
     ELSE (select cast(round(Premium,2) as numeric(36,2)) from QuoteItem as Qi
		join CoverDefinition as Cd on cd.Id = qi.CoverDefinitionId
		where --QuoteId = Qute.Id and 
		cd.CoverId in (373, 372, 258) and qi.Id = QteI.Id)                                          
     END as 'QuoteOtherPremium',

	(select top 1 CONVERT(VARCHAR(10),InceptionDate ,111) from PaymentDetails where PartyId = p.Id and QuoteId = qute.Id order by Id desc) as 'QuoteIncepptionDate',
	ph.PolicyNo as 'QuotePolicyNumber',
	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'Quote-CreateDate',
	CONVERT(VARCHAR(10),Qute.CreatedAt,108) as 'Quote-CreateTime',
	l.Id as 'LeadText',

	'' as 'PropertyNumber',
	a.AssetNo as 'ItemNumber',

	(select  AnswerId from @tbl where QuestionId = 89 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)  as 'Address-Suburb',
	(select  AnswerId from @tbl where QuestionId = 90 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)  as 'Address-PostCode',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 47 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'Address-Province',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 500 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'Item-Category',

	(select AnswerId from @tbl where QuestionId = 490 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)  as 'Item-Description',

	(case 
		(select  AnswerId from @tbl where QuestionId = 492 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Item-Jewellery-Safe',

	(case 
		(select  AnswerId from @tbl where QuestionId = 494 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End)  as 'Item-ReceiptsForItems',

	(select  AnswerId from @tbl where QuestionId = 496 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'Item-SerialNumber',

	(select  AnswerId from @tbl where QuestionId = 155 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)  as 'Item-Discount',

	(select  AnswerId from @tbl where QuestionId = 102 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)  as 'Item-SumInsured'

from Party as P
join Lead as L on l.PartyId = p.Id
join Channel as Ch on ch.Id = p.ChannelId
left join ProposalHeader as Prh on Prh.PartyId = p.Id
left join ProposalDefinition as prd on Prd.ProposalHeaderId = Prh.Id
left join QuoteHeader as Qh on qh.ProposalHeaderId = Prh.Id
left join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
left join PolicyHeader as Ph on Ph.QuoteId = Qute.Id
left join md.PolicyStatus as PhS on Ph.PolicyStatusId = PhS.Id
left join QuoteItem as QteI on QteI.QuoteId = Qute.Id
INNER JOIN Asset a on a.Id = prd.AssetId
join QuoteAcceptedLeadActivity as qala on qala.QuoteId = Qute.Id
join LeadActivity as la on la.Id = qala.LeadActivityId
where Qute.Id is not null
and L.IsDeleted = 0
and Qute.IsDeleted = 0
--and p.Id = 2284
and prd.IsDeleted = 0
--and ast.IsDeleted = 0
and prd.CoverId = 17
--and ph.DateEffective >= @StartDate
--and ph.DateRenewal <= @EndDate
and la.DateCreated >= @StartDate
and la.DateUpdated <= @EndDate
and la.ActivityTypeId = 5
and Qh.IsDeleted = 0
--and prh.IsQuoted = 1
and QteI.CoverDefinitionId in (select Id from CoverDefinition where CoverId = 17)
--AND a.AssetNo = qi.AssetNumber
order by p.DisplayName

