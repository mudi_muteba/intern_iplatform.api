if exists (select * from sys.objects where object_id = object_id('dbo.AigReportAllRiskExtract') and type in (N'U'))
begin
 drop TABLE dbo.AigReportAllRiskExtract
end
go

if exists (select * from sys.objects where object_id = object_id('dbo.AigReportBuildingExtract') and type in (N'U'))
begin
 drop TABLE dbo.AigReportBuildingExtract
end
go

if exists (select * from sys.objects where object_id = object_id('dbo.AigReportContentExtract') and type in (N'U'))
begin
 drop TABLE dbo.AigReportContentExtract
end
go

if exists (select * from sys.objects where object_id = object_id('dbo.AigReportDisasterCashExtract') and type in (N'U'))
begin
 drop TABLE dbo.AigReportDisasterCashExtract
end
go

if exists (select * from sys.objects where object_id = object_id('dbo.AigReportFuneralExtract') and type in (N'U'))
begin
 drop TABLE dbo.AigReportFuneralExtract
end
go

if exists (select * from sys.objects where object_id = object_id('dbo.AigReportIdentityTheftExtract') and type in (N'U'))
begin
 drop TABLE dbo.AigReportIdentityTheftExtract
end
go

if exists (select * from sys.objects where object_id = object_id('dbo.AigReportLeadExtract') and type in (N'U'))
begin
 drop TABLE dbo.AigReportLeadExtract
end
go

if exists (select * from sys.objects where object_id = object_id('dbo.AigReportLeadQuoteHomePreviousLossExtract') and type in (N'U'))
begin
 drop TABLE dbo.AigReportLeadQuoteHomePreviousLossExtract
end
go

if exists (select * from sys.objects where object_id = object_id('dbo.AigReportLeadQuoteMotorPreviousLossExtract') and type in (N'U'))
begin
 drop TABLE dbo.AigReportLeadQuoteMotorPreviousLossExtract
end
go

if exists (select * from sys.objects where object_id = object_id('dbo.AigReportMotorExtract') and type in (N'U'))
begin
 drop TABLE dbo.AigReportMotorExtract
end
go

if exists (select * from sys.objects where object_id = object_id('dbo.AigReportPersonalLegalLiabilityExtract') and type in (N'U'))
begin
 drop TABLE dbo.AigReportPersonalLegalLiabilityExtract
end
go
