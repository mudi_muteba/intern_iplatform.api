﻿declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode <> 'AIG')
	begin
		return
	end
else
	begin
		--CLEAR REPORT SETTINGS
		delete from ReportChannelLayout
		delete from ReportChannel
		delete from ReportSection
		delete from ReportLayout

		--RESEED IDENTITY COLUMNS
		dbcc checkident ('ReportChannelLayout', reseed, 0)
		dbcc checkident ('ReportChannel', reseed, 0)
		dbcc checkident ('ReportSection', reseed, 0)
		dbcc checkident ('ReportLayout', reseed, 0)

		--DECLARE SCOPE IDENTITY VARIABLE
		declare @scope_identity int

		--SETUP COMPARATIVE QUOTE REPORT
		insert into ReportChannel (ChannelId, ReportId, Code, CreatedAt, ModifiedAt, UserId) values (@DefaultChannelId, (select top 1 Id from Report where Name = 'Comparative Quote Report'), 'vrgnmny', getdate(), getdate(), 1)
		set @ScopeComparativeQuoteReportChannelId = SCOPE_IDENTITY()

		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'QuoteHeader', 'Quote Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'QuoteFooter', 'Quote Footer')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'ClientHeader', 'Client Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'CompanyHeader', 'Company Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'QuoteSummary', 'Quote Summary')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'CostFooter', 'Cost Footer')

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteTitle', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteSummary', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteAssets', 'ASSETS', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'ASSETS', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteDate', 'Date:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Date:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteExpires', 'Expires:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Expires:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteNumber', 'Quote No:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Quote No:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientHeader', 'DETAILS OF CLIENT', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'DETAILS OF CLIENT', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralName', 'Client', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Client', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralIdentity', 'Identity No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Identity No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Physical Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Postal Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Work No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactHome', 'Home No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Home No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactCell', 'Cell No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Cell No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Fax No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Email Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyHeader', 'DETAILS OF INTERMEDIARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'DETAILS OF INTERMEDIARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyGeneralName', 'Company', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Company', 'Lifecycle Management (Pty) Ltd', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Physical Address', '2 Park Lane, Umhlanga Ridge, 4319', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Postal Address', 'PO Box 3025, Prestondale, Kwa-Zulu Natal, 4021', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Work No.', '0861 50 60 70', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Fax No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Email Address', 'needsomehelp@virginmoneyinsurance.co.za', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWebsite', 'Website', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Website', 'http://www.virginmoneyinsurance.co.za', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthRegistration', 'Registration No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Registration No.', '2014/075034/07', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthTax', 'Vat No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Vat No.', '4070270881', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthLicense', 'Authorised Financial Services Provider', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Authorised Financial Services Provider', 'No 7146', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteFooter'), 'QuoteFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, '', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryFees', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryTotal', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CostFooter'), 'CostFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, '', '', 1)


		--SETUP FUNERAL QUOTE REPORT
		insert into ReportChannel (ChannelId, ReportId, Code, CreatedAt, ModifiedAt, UserId) values (@DefaultChannelId, (select top 1 Id from Report where Name = 'Funeral Quote Report'), 'vrgnmny', getdate(), getdate(), 1)
		set @ScopeFuneralQuoteReportChannelId = scope_identity()

		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'QuoteHeader', 'Quote Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'QuoteFooter', 'Quote Footer')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'ClientHeader', 'Client Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'CompanyHeader', 'Company Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'QuoteSummary', 'Quote Summary')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'CostFooter', 'Cost Footer')

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteTitle', 'QUOTATION', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'QUOTATION', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteSummary', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteAssets', 'ASSETS', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'ASSETS', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteDate', 'Date:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Date:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteExpires', 'Expires:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Expires:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteNumber', 'Quote No:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Quote No:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientHeader', 'DETAILS OF CLIENT', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'DETAILS OF CLIENT', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralName', 'Client', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Client', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralIdentity', 'Identity No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Identity No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Physical Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Postal Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Work No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactHome', 'Home No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Home No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactCell', 'Cell No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Cell No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Fax No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Email Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyHeader', 'DETAILS OF INTERMEDIARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'DETAILS OF INTERMEDIARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyGeneralName', 'Company', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Company', 'Lifecycle Management (Pty) Ltd', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Physical Address', '2 Park Lane, Umhlanga Ridge, 4319', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Postal Address', 'PO Box 3025, Prestondale, Kwa-Zulu Natal, 4021', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Work No.', '0861 50 60 70', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Fax No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Email Address', 'needsomehelp@virginmoneyinsurance.co.za', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWebsite', 'Website', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Website', 'http://www.virginmoneyinsurance.co.za', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthRegistration', 'Registration No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Registration No.', '2014/075034/07', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthTax', 'Vat No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Vat No.', '4070270881', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthLicense', 'Authorised Financial Services Provider', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Authorised Financial Services Provider', 'No 7146', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteFooter'), 'QuoteFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, '', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryFees', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryTotal', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CostFooter'), 'CostFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, '', '', 1)


		--SETUP QUOTE SCHEDULE REPORT
		insert into ReportChannel (ChannelId, ReportId, Code, CreatedAt, ModifiedAt, UserId) values (@DefaultChannelId, (select top 1 Id from Report where Name = 'Quote Schedule Report'), 'vrgnmny', getdate(), getdate(), 1)
		set @ScopeQuoteScheduleReportChannelId = scope_identity()

		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), 'QuoteHeader', 'Quote Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), 'QuoteFooter', 'Quote Footer')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), 'ClientHeader', 'Client Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), 'CompanyHeader', 'Company Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), 'QuoteSummary', 'Quote Summary')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), 'CostFooter', 'Cost Footer')

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteTitle', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteSummary', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteAssets', 'ASSETS', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'ASSETS', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteDate', 'Date:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Date:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteExpires', 'Expires:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Expires:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteNumber', 'Quote No:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Quote No:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientHeader', 'DETAILS OF CLIENT', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'DETAILS OF CLIENT', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralName', 'Client', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Client', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralIdentity', 'Identity No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Identity No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Physical Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Postal Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Work No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactHome', 'Home No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Home No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactCell', 'Cell No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Cell No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Fax No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Email Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyHeader', 'DETAILS OF INTERMEDIARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'DETAILS OF INTERMEDIARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyGeneralName', 'Company', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Company', 'Lifecycle Management (Pty) Ltd', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Physical Address', '2 Park Lane, Umhlanga Ridge, 4319', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Postal Address', 'PO Box 3025, Prestondale, Kwa-Zulu Natal, 4021', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Work No.', '0861 50 60 70', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Fax No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Email Address', 'needsomehelp@virginmoneyinsurance.co.za', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWebsite', 'Website', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Website', 'http://www.virginmoneyinsurance.co.za', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthRegistration', 'Registration No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Registration No.', '2014/075034/07', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthTax', 'Vat No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Vat No.', '4070270881', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthLicense', 'Authorised Financial Services Provider', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Authorised Financial Services Provider', 'No 7146', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteFooter'), 'QuoteFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, '', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryFees', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryTotal', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CostFooter'), 'CostFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, '', '', 1)
	end
go


-------motor cycle stored procedure-----------
if exists (select * from sys.objects where object_id = object_id(N'Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_Motocycle') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_Motocycle
	end
go


create procedure [dbo].[Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_Motocycle]
(
	@ProposalHeaderId int,
	@CoverId int,
	@AssetId int
) --EXEC [Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_Motocycle] 4588,218,5037
as 
select top 1
	(select md.QuestionAnswer.Answer from md.QuestionAnswer
	 where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
	 ) [Description],
	'' Value,
		(
			select top 1
							md.QuestionAnswer.Answer
						from ProposalHeader
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							inner join Product on Product.Id = ProposalDefinition.ProductId
							inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
							inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
							inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
							inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
							inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
							inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
							inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
						where
							ProposalHeader.Id = @ProposalHeaderId and
							Asset.Id = @AssetId and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							md.QuestionGroup.Name = 'Finance Information' and
							md.QuestionType.Name in ('DropDown') and
							md.Question.Name = 'AIG -Valuation Method'
		)
 Actual
from ProposalHeader
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join Asset on Asset.Id = ProposalDefinition.AssetId
	inner join Product on Product.Id = ProposalDefinition.ProductId
	inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
	inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
	inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
	inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
	inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
	inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
where
	ProposalHeader.Id = @ProposalHeaderId and
	Asset.Id = @AssetId and
	ProposalHeader.IsDeleted = 0 and
	ProposalDefinition.IsDeleted = 0 and
	Asset.IsDeleted = 0 and
	md.QuestionGroup.Name = 'Risk Information' and
	md.QuestionType.Name in ('DropDown') and
	md.Question.Name = 'AIG -Cover Type'
union all
select
	QuestionDefinition.DisplayName [Description],
	case
		when md.QuestionType.Name = 'DropDown'
			then
				case
					when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
					then ''
				else
					'Included in premium' --'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
				end
			else
				case
					when ProposalQuestionAnswer.Answer = 'true'
						then 'Included in premium'
					when ProposalQuestionAnswer.Answer = 'false'
						then ''
					else 
						'' --ProposalQuestionAnswer.Answer
					end
	end Value,
	case
		when md.QuestionType.Name = 'DropDown'
			then
				case
					when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
					then 'No'
				else
					'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
				end
			else
				case
					when ProposalQuestionAnswer.Answer = 'true'
						then 'Yes'
					when ProposalQuestionAnswer.Answer = 'false'
						then 'No'
					else 
						'No' --ProposalQuestionAnswer.Answer
					end
	end Actual
from ProposalHeader
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join Asset on Asset.Id = ProposalDefinition.AssetId
	inner join Product on Product.Id = ProposalDefinition.ProductId
	inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
	inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
	inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
	inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
	inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
	inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
where
	ProposalHeader.Id = @ProposalHeaderId and
	Asset.Id = @AssetId and
	ProposalHeader.IsDeleted = 0 and
	ProposalDefinition.IsDeleted = 0 and
	Asset.IsDeleted = 0 and
	md.QuestionGroup.Name = 'Additional Options' and
	md.QuestionType.Name in ('XXX') --('Checkbox', 'DropDown') exclude Car Hire and Tire and rim scratch and dent FOR MOTOR cover Trailer and Caravan
	union
		select 'Motor Excess', '', (select top 1
				md.QuestionAnswer.Answer
				from ProposalHeader
					inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
					inner join Asset on Asset.Id = ProposalDefinition.AssetId
					inner join Product on Product.Id = ProposalDefinition.ProductId
					inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
					inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
					inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
					inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
					inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
					inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
					inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
				where
					ProposalHeader.Id = @ProposalHeaderId and
					Asset.Id = @AssetId and
					ProposalHeader.IsDeleted = 0 and
					ProposalDefinition.IsDeleted = 0 and
					Asset.IsDeleted = 0 and
				    md.QuestionType.Name in ('DropDown') and
					md.Question.Name = 'AIG - Motor Voluntary Excess')	
go


-------Reports_QuoteSchedule_ValueAddedProducts_ByAsset---------



--added motor cycle condition
if exists (select * from sys.objects where object_id = object_id(N'Reports_QuoteSchedule_ValueAddedProducts_ByAsset') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_QuoteSchedule_ValueAddedProducts_ByAsset
	end
go


create  procedure [dbo].[Reports_QuoteSchedule_ValueAddedProducts_ByAsset]
(
	@ProposalHeaderId int,
	@CoverId int,
	@AssetId int
)
as 
	if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) not in ('Building', 'Contents', 'Motor')
		return
	
	if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) in ('Motor')
	begin
			declare @typeOfVehicle nvarchar(1000)
			
			--Find Type Of Vehicle answer
			exec [Reports_select_question_answer]  @ProposalHeaderId,@CoverId,@AssetId, 'AIG - Motor -Type Of Vehicle',@result = @typeOfVehicle output
			print @typeOfVehicle
			if (@typeOfVehicle = 3340) -- If motor
				begin
					exec [Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_Car] @ProposalHeaderId,@CoverId,@AssetId
				end
			else if(@typeOfVehicle = 3344) --motor cycle
				begin
				exec [Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_Motocycle] @ProposalHeaderId,@CoverId,@AssetId
			end
	    	  else
				begin
					exec [Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_Trailer_Caravan] @ProposalHeaderId,@CoverId,@AssetId
				end
	end
	else if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) in ('Building')
		begin
			select
				--QuestionDefinition.DisplayName [Description],
				case
					when md.Question.Id in (857) 
						then
							'Voluntary Additional Excess'
					else
						QuestionDefinition.DisplayName 
				end [Description],
				case
					when md.QuestionType.Name = 'DropDown'
						then
							case
								when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
								then ''
							else
								'Included in premium' --'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
							end
						else
							case
								when ProposalQuestionAnswer.Answer = 'true'
									then 'Included in premium'
								when ProposalQuestionAnswer.Answer = 'false'
									then ''
								else 
									'' --ProposalQuestionAnswer.Answer
								end
				end Value,
				case
					when md.QuestionType.Name = 'DropDown'
						then
							case
								when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
								then 'No'
							else
								'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
							end
						else
							case
								when ProposalQuestionAnswer.Answer = 'true'
									then 'Yes'
								when ProposalQuestionAnswer.Answer = 'false'
									then 'No'
								else 
									'No' --ProposalQuestionAnswer.Answer
								end
				end Actual
			from ProposalHeader
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId
				inner join Product on Product.Id = ProposalDefinition.ProductId
				inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
				inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
				inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
				inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
				inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
				inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
			where
				ProposalHeader.Id = @ProposalHeaderId and
				Asset.Id = @AssetId and
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				QuestionDefinition.IsDeleted = 0 and
				ProposalQuestionAnswer.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				md.Question.Id not in (404) and --Don't display Lightning Conductor entry
				(
				--md.QuestionGroup.Name = 'Risk Information' or
				md.QuestionGroup.Name = 'Additional Options') and
				md.QuestionType.Name in ('Checkbox', 'DropDown')
				--and md.Question.Id not in (858)
				--and
				--(
				--	isnumeric(ProposalQuestionAnswer.Answer) > 0 or
				--	ProposalQuestionAnswer.Answer = 'true'
				--)
				and md.Question.Id not in (858) --AIG_-_Contents_Additional_Excess
		union 
			select 'Basic Excess', '', 'R500'		
		end
	else
		begin
			select
				--QuestionDefinition.DisplayName [Description],
				case
					when md.Question.Id in (858) 
						then
							'Voluntary Additional Excess' 
					else
					QuestionDefinition.DisplayName 
				end 
				[Description],
				case
					when md.QuestionType.Name = 'DropDown'
						then
							case
								when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
								then ''
							else
								'Included in premium' --'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
							end
						else
							case
								when ProposalQuestionAnswer.Answer = 'true'
									then 'Included in premium'
								when ProposalQuestionAnswer.Answer = 'false'
									then ''
								else 
									'' --ProposalQuestionAnswer.Answer
								end
				end Value,
				case
					when md.QuestionType.Name = 'DropDown'
						then
							case
								when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
								then 'No'
							else
								'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
							end
						else
							case
								when ProposalQuestionAnswer.Answer = 'true'
									then 'Yes'
								when ProposalQuestionAnswer.Answer = 'false'
									then 'No'
								else 
									'No' --ProposalQuestionAnswer.Answer
								end
				end Actual
			from ProposalHeader
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId
				inner join Product on Product.Id = ProposalDefinition.ProductId
				inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
				inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
				inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
				inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
				inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
				inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
			where
				ProposalHeader.Id = @ProposalHeaderId and
				Asset.Id = @AssetId and
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				QuestionDefinition.IsDeleted = 0 and
				ProposalQuestionAnswer.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				(
				--md.QuestionGroup.Name = 'Risk Information' or
				md.QuestionGroup.Name = 'Additional Options') and
				md.QuestionType.Name in ('Checkbox', 'DropDown') and
				md.Cover.Id = @CoverId
				--and md.Question.Id not in (858)
				--and
				--(
				--	isnumeric(ProposalQuestionAnswer.Answer) > 0 or
				--	ProposalQuestionAnswer.Answer = 'true'
				--)
		union 
			select 'Basic Excess', '', 'R500'		
		end
go


