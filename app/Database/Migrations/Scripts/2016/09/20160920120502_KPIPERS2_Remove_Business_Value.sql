﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='KPIPERS2')

DECLARE @CoverID INT
SET @CoverID=(select ID from md.Cover where name ='CONTENTS')

DECLARE @CoverDefid INT
SET @CoverDefid=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId=@CoverID)

if exists(select * from QuestionDefinition where QuestionId=1060 and CoverDefinitionId = @CoverDefid and isdeleted<>1)
		begin
		UPDATE dbo.QuestionDefinition SET IsDeleted = 1 WHERE QuestionId=1060 and CoverDefinitionId = @CoverDefid and isdeleted<>1
		END