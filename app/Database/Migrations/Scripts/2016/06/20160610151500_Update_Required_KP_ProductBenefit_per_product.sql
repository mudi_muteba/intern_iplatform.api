﻿--King Price Insurance Company Ltd

----Personal Insurance Quick Quote 
declare @pId int

set @pId = (select Id from [dbo].[Product] where productCode = 'KPIPERSQQ')

declare @CoverId int

set @CoverId = (select Id from [md].[Cover] where Code = 'MOTOR')

declare @CoverDef int

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Private Type Vehicle', N'', 0, 0, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes ', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Use of vehicle', N'Private/Business', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Comprehensive Cover', N'Yes ', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'New for Old', N'', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Windscreen', N'Comprehensive only', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Fire and theft Cover', N'If requested', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Cover', N'If requested', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Riot and Strike outside RSA', N'NO', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Tracking device to be covered', N'NO', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Towing and Storage', N'With a Valid Claim', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Sound System [not factory fitted]', N'If requested', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Hail Damage', N'Comprehensive only', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Canopy of the pick-up', N'If requested', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Car Hire', N'If requested', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Emergency Hotel Expenses', N'Emergency assist', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Credit Shortfall ', N'If requested', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Medical Costs', N'1500', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Liability', N'3,000,000', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party fire and explosion Liability', N'3,000,000', 1, 19, 0)


--Contents

set @pId = (select Id from [dbo].[Product] where productCode = 'KPIPERSQQ')

set @CoverId = (select Id from [md].[Cover] where Code = 'CONTENTS')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes ', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Automatic anniversary/renewal sum insured increase', N'Yes ', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Accidental Damage extension cover', N'No', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Documents limitation', N'Bonus Benifts', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Deterioration of Food', N'Bonus Benifts', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Washing stolen at your home', N'Bonus Benifts', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Garden furniture stolen at your home', N'Bonus Benifts', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Damage to Garden', N'No', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Guests belongings stolen at your home', N'Bonus Benifts', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Personal documents/coins/stamps - Loss of', N'Not Covered', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Locks and Keys - Lost/damaged', N'Included', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Remote control units', N'Not Covered', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Loss of water by leakage', N'Not Covered', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Credit or Bank Cards - fraudulent use', N'Not Covered', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Hole in one / bowling full house', N'Bonus Benifts', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Insured and Spouce death - fire or a break-in', N'Not Covered', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Your domestics belongings - stolen following break-in', N'Bonus Benifts', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Medical Expenses', N'Included', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Veterinary Fees', N'Bonus Benifts', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Rent to live elsewhere', N'Included max 10% of sum insured', 1, 20, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Storage costs for contents after damage', N'Not Covered', 1, 21, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Belongings in a removal truck', N'only in an event of fire or vehicle accident', 1, 22, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Accidental breakage of mirrors and galss', N'included', 1, 23, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Accidental breakage of television set', N'included', 1, 24, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Fire brigade charges', N'Included', 1, 25, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Security Guards', N'Not Covered', 1, 26, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Public Liability', N'', 1, 27, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Tenants Liability', N'Included', 1, 28, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Liability to Domestic Employees', N'Included', 1, 29, 0)


--All Risk

set @pId = (select Id from [dbo].[Product] where productCode = 'KPIPERSQQ')

set @CoverId = (select Id from [md].[Cover] where Code = 'ALL_RISK')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes ', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Riot and Strike outside RSA', N'No', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Unspecified Items as defined', N'Yes ', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'   * If yes ~ Cover for Jewellery, Clothing and Personal items', N'Yes ', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'   * If yes ~ Limit any one item', N'', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'   * If yes ~ Cover for money and netiable instruments', N'Not Covered', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Sperfified Items [If yes refer to list of items]', N'Yes ', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Items in a Bank Vault', N'Not Covered', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Bicycles', N'If specified', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Wheelchairs and its accessories', N'If specified', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Prescription Glasses', N'If specified', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Contact Lenses', N'If specified', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Cellular phones', N'If specified', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Stamp Collection ', N'Not Covered', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Coin Collection ', N'Not Covered', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Transport of groceries and household ods', N'not if stolen in loading bay', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'All Risks cover for caravan contents', N'Has to  be specified under Caravan Section', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'All Risks cover for car radios', N'Not Covered', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Borehole and swimming pool equipment', N'Covered under Buildings', 1, 19, 0)



--Building

set @pId = (select Id from [dbo].[Product] where productCode = 'KPIPERSQQ')

set @CoverId = (select Id from [md].[Cover] where Code = 'BUILDING')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes ', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'If yes Home building definition to be checked', N'Covered', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Thatch roof home', N'Yes/Underwriting ', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Swimming Pool covered', N'Yes', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Swimming Pool equipment covered', N'Yes', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Borehole pump/equipment', N'Yes', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Tennis court covered', N'Yes', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Damage to Gardens', N'Not Included', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Automatic anniversary/renewal sum insured increase', N'Yes', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Loss of water by leakage', N'Not Covered', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Rent ', N'Included', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Removal of fallen trees', N'Yes', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Professional Fees', N'Included', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Glass and Sanitaryware', N'Included', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Power supply', N'Included', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Aerials', N'Included', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Fire brigade charges', N'Included', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Security Guards', N'Not Covered', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Public Liability as a Home Owner', N'Included', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Liability to Domestic Employees', N'Inclded', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Subsidence, heave and landslip', N'Included', 1, 20, 0)

