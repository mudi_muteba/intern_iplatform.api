﻿IF NOT Exists (select * from [dbo].[AuthorisationGroupPoint] where AuthorisationPointId = 22 AND [AuthorisationGroupId] = 1 AND [IsDeleted] = 0)
BEGIN
	INSERT INTO [dbo].[AuthorisationGroupPoint]([AuthorisationPointId],[AuthorisationGroupId],[IsDeleted])VALUES(22,1,0);
END

IF NOT Exists (select * from [dbo].[AuthorisationGroupPoint] where AuthorisationPointId = 22 AND [AuthorisationGroupId] = 2 AND [IsDeleted] = 0)
BEGIN
	INSERT INTO [dbo].[AuthorisationGroupPoint]([AuthorisationPointId],[AuthorisationGroupId],[IsDeleted])VALUES(22,2,0);
END