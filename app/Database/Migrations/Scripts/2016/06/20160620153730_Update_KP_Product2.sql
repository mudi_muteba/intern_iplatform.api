﻿declare @ProductId int
select @ProductId = id from Product where ProductCode = 'KPIPERS2'

declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 218)

if not exists(select * from QuestionDefinition where DisplayName = 'Main Driver Date of Birth' and CoverDefinitionId = @CoverDefinitionId)
		begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Main Driver Date of Birth',@CoverDefinitionId,42,NULL,1,0,1,1,0,'Main Driver Date of Birth','','',0,NULL,0)
		END
if not exists(select * from QuestionDefinition where DisplayName = 'Main Driver First Name' and CoverDefinitionId = @CoverDefinitionId)
		begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Main Driver First Name',@CoverDefinitionId,144,NULL,1,0,1,1,0,'Main Driver First Name','','',0,NULL,0)
		end
if not exists(select * from QuestionDefinition where DisplayName = 'Main Driver Surname' and CoverDefinitionId = @CoverDefinitionId)
		begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Main Driver Surname',@CoverDefinitionId,145,NULL,1,0,1,1,0,'Main Driver Surname','','',0,NULL,0)
		end
if not exists(select * from QuestionDefinition where DisplayName = 'Main Driver Title' and CoverDefinitionId = @CoverDefinitionId)
		begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Main Driver Title',@CoverDefinitionId,146,NULL,1,0,1,1,0,'Main Driver Title','','',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where DisplayName = 'Main Driver Card Date Valid From' and CoverDefinitionId = @CoverDefinitionId)
		begin
			INSERT INTO dbo.QuestionDefinition ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Main Driver Card Date Valid From' , -- DisplayName - nvarchar(255)
          @CoverDefinitionId , -- CoverDefinitionId - int
          147 , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          36 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the main drivers license card valid from date?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )
END

if not exists(select * from QuestionDefinition where DisplayName = 'Main Driver Card Date Valid To' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Main Driver Card Date Valid To' , -- DisplayName - nvarchar(255)
          @CoverDefinitionId , -- CoverDefinitionId - int
          148 , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          37 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the main drivers license card valid to date?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )
end

if not exists(select * from QuestionDefinition where DisplayName = 'Main Driver Last Accident Claim' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Main Driver Last Accident Claim' , -- DisplayName - nvarchar(255)
          @CoverDefinitionId , -- CoverDefinitionId - int
          149 , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          38 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the number of years since the main drivers last accident claim?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )
end

if not exists(select * from QuestionDefinition where DisplayName = 'Main Driver Last Theft Claim' and CoverDefinitionId = @CoverDefinitionId)
		BEGIN		
	INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Main Driver Last Theft Claim' , -- DisplayName - nvarchar(255)
          @CoverDefinitionId , -- CoverDefinitionId - int
          150 , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          39 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the number of years since the main drivers last theft claim?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )
END

if not exists(select * from QuestionDefinition where DisplayName = 'Drivers Licence First Issued Date' and CoverDefinitionId = @CoverDefinitionId)
		BEGIN	
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], 
		[DefaultValue], [RegexPattern]) VALUES (0, N'Drivers Licence First Issued Date', @CoverDefinitionId, 41, NULL, 1, 7, 1, 1, 0, 
		N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
		END

if not exists(select * from QuestionDefinition where DisplayName = 'Drivers Licence Type' and CoverDefinitionId = @CoverDefinitionId)
		BEGIN
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], 
		[DefaultValue], [RegexPattern]) VALUES (0, N'Drivers Licence Type', @CoverDefinitionId, 73, NULL, 1, 8, 1, 1, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
		END

if not exists(select * from QuestionDefinition where DisplayName = 'Current Insurance Period' and CoverDefinitionId = @CoverDefinitionId)
		BEGIN
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], 
		[DefaultValue], [RegexPattern]) VALUES (0, N'Current Insurance Period', @CoverDefinitionId, 114, NULL, 1, 28, 1, 1, 0, N'How long has the client been at his current insurance company?', N'', N'')
		END

if not exists(select * from QuestionDefinition where DisplayName = 'Marital Status' and CoverDefinitionId = @CoverDefinitionId)
		BEGIN
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], 
		[DefaultValue], [RegexPattern]) VALUES (0, N'Marital Status', @CoverDefinitionId, 72, NULL, 1, 6, 0, 1, 0, N'What is the marital status of this client?', N'', N'')
		END

if not exists(select * from QuestionDefinition where QuestionId = 60 and CoverDefinitionId = @coverDefinitionId)
     begin
          INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
               VALUES(0,'Relationship to insured',@coverDefinitionId,60,NULL,1,0,0,0,0,'Relationship to insured','','',0,NULL,0)
     END

if not exists(select * from QuestionDefinition where DisplayName = 'Main Driver Gender' and CoverDefinitionId = @CoverDefinitionId)
		begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Main Driver Gender',@CoverDefinitionId,70,NULL,1,0,1,1,0,'Main Driver Gender','','',0,NULL,0)
		END
        
if not exists(select * from QuestionDefinition where questionId = 39 and CoverDefinitionId = @coverDefinitionId)
     begin
          INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
               VALUES(0,'Previously Insured',@coverDefinitionId,39,NULL,1,25,0,0,0,'Has this vehicle been insured before?','','',0,NULL,0)
     END

if not exists(select * from QuestionDefinition where questionId = 27 and CoverDefinitionId = @coverDefinitionId)
     begin
          INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
               VALUES(0,'License Endorsed',@coverDefinitionId,27,NULL,1,12,0,0,0,'License Endorsed','','',0,NULL,0)
     END


set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 78)

DELETE FROM dbo.ProposalQuestionAnswer WHERE QuestionDefinitionId in (select id from QuestionDefinition where questionId = 57 and CoverDefinitionId = @coverDefinitionId)

DELETE FROM QuestionDefinition where questionId = 57 and CoverDefinitionId = @coverDefinitionId

if not exists(select * from QuestionDefinition where questionId = 57 and CoverDefinitionId = @coverDefinitionId)
     begin
	INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
	VALUES(0,'Unoccupied',@coverDefinitionId,57,NULL,1,14,1,1,0,'How often is the house unoccupied?','0','',0,NULL,0)
	end

if not exists(select * from QuestionDefinition where DisplayName = 'Asset' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex]) 
		VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 1, 1, 0, N'I''d like to gather some information about the asset you would like to insure.', N'', N'^[0-9]+$', 1)
		END



if not exists(select * from QuestionDefinition where DisplayName = 'Postal Code' and CoverDefinitionId = @CoverDefinitionId)
		BEGIN
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
		[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
		END

if not exists(select * from QuestionDefinition where DisplayName = 'Suburb' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
		[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
		END

if not exists (select * from QuestionDefinition  where CoverDefinitionId = @coverDefinitionId and questionId = 47)
begin
	INSERT INTO [dbo].[QuestionDefinition]
			   ([MasterId]
			   ,[DisplayName]
			   ,[CoverDefinitionId]
			   ,[QuestionId]
			   ,[ParentId]
			   ,[QuestionDefinitionTypeId]
			   ,[VisibleIndex]
			   ,[RequiredForQuote]
			   ,[RatingFactor]
			   ,[ReadOnly]
			   ,[ToolTip]
			   ,[DefaultValue]
			   ,[RegexPattern]
			   ,[GroupIndex]
			   ,[QuestionDefinitionGroupTypeId])
		 VALUES
			   (0
			   ,'Province'
			   ,@coverDefinitionId
			   ,47
			   ,null
			   ,1
			   ,3
			   ,1
			   ,1
			   ,0
			   ,'In which province is the risk item is located?'
			   ,''
			   ,''
			   ,0
			   ,2)
end

UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=141 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=88 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=90 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=89 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=47 AND CoverDefinitionId=@CoverDefinitionId



set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 44)

if not exists(select * from QuestionDefinition where DisplayName = 'Asset' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex]) 
		VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 1, 1, 0, N'I''d like to gather some information about the asset you would like to insure.', N'', N'^[0-9]+$', 1)
		END

if not exists(select * from QuestionDefinition where DisplayName = 'Postal Code' and CoverDefinitionId = @CoverDefinitionId)
		BEGIN
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
		[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
		END

if not exists(select * from QuestionDefinition where DisplayName = 'Suburb' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
		[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
		END

if not exists (select * from QuestionDefinition  where CoverDefinitionId = @coverDefinitionId and questionId = 47)
begin
	INSERT INTO [dbo].[QuestionDefinition]
			   ([MasterId]
			   ,[DisplayName]
			   ,[CoverDefinitionId]
			   ,[QuestionId]
			   ,[ParentId]
			   ,[QuestionDefinitionTypeId]
			   ,[VisibleIndex]
			   ,[RequiredForQuote]
			   ,[RatingFactor]
			   ,[ReadOnly]
			   ,[ToolTip]
			   ,[DefaultValue]
			   ,[RegexPattern]
			   ,[GroupIndex]
			   ,[QuestionDefinitionGroupTypeId])
		 VALUES
			   (0
			   ,'Province'
			   ,@coverDefinitionId
			   ,47
			   ,null
			   ,1
			   ,3
			   ,1
			   ,1
			   ,0
			   ,'In which province is the risk item is located?'
			   ,''
			   ,''
			   ,0
			   ,2)
end

UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=141 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=88 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=90 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=89 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=47 AND CoverDefinitionId=@CoverDefinitionId


set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 17)

if not exists(select * from QuestionDefinition where DisplayName = 'Postal Code' and CoverDefinitionId = @CoverDefinitionId)
		BEGIN
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
		[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
		END

if not exists(select * from QuestionDefinition where DisplayName = 'Suburb' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
		[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
		END

if not exists (select * from QuestionDefinition  where CoverDefinitionId = @coverDefinitionId and questionId = 47)
begin
	INSERT INTO [dbo].[QuestionDefinition]
			   ([MasterId]
			   ,[DisplayName]
			   ,[CoverDefinitionId]
			   ,[QuestionId]
			   ,[ParentId]
			   ,[QuestionDefinitionTypeId]
			   ,[VisibleIndex]
			   ,[RequiredForQuote]
			   ,[RatingFactor]
			   ,[ReadOnly]
			   ,[ToolTip]
			   ,[DefaultValue]
			   ,[RegexPattern]
			   ,[GroupIndex]
			   ,[QuestionDefinitionGroupTypeId])
		 VALUES
			   (0
			   ,'Province'
			   ,@coverDefinitionId
			   ,47
			   ,null
			   ,1
			   ,3
			   ,1
			   ,1
			   ,0
			   ,'In which province is the risk item is located?'
			   ,''
			   ,''
			   ,0
			   ,2)
end

UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=141 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=88 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=90 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=89 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=1, QuestionDefinitionGroupTypeId=2 WHERE QuestionId=47 AND CoverDefinitionId=@CoverDefinitionId