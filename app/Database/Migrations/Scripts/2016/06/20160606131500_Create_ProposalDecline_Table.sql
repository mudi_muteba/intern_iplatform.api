﻿IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[ProposalDecline]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProposalDecline](
	[Id] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[PartyId] [int] NOT NULL,
	[RiskItemDeclined] [nvarchar](255) NULL,
	[DeclinedReason] [nvarchar](255) NULL,
	[TimeStamp] [datetime] NULL,
	[IsDeleted] [bit] NULL,
) 
END