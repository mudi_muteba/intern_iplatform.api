﻿if exists (select * from sys.objects where object_id = object_id(N'Reports_FuneralQuote_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_FuneralQuote_Header
	end
go

create procedure Reports_FuneralQuote_Header
(
	@ProposalHeaderId int
)
as
	select top 1
		Individual.FirstName + ' ' + Individual.Surname MainMemberName,
		isnull(Agent.FirstName + ' ' + Agent.Surname, '') SalesConsultant,
		DATEADD(d, 1, EOMONTH(current_timestamp)) StartDate,
		md.Gender.Name MainMemberGender,
		Individual.DateOfBirth MainMemberDateOfBirth,
		Occupation.Name MainMemberOccupation,
		(
			select
				isnull(AdditionalMembers.Initials + ' ' + AdditionalMembers.Surname, '')
			from AdditionalMembers
			where
				AdditionalMembers.ProposalDefinitionId = ProposalDefinition.Id and
				AdditionalMembers.MemberRelationshipId = 1 and
				AdditionalMembers.IsDeleted != 1
		) SpouseName,
		(
			select
				isnull(md.Gender.Name, '')
			from AdditionalMembers
				inner join md.Gender on md.Gender.Id = AdditionalMembers.GenderId
			where
				AdditionalMembers.ProposalDefinitionId = ProposalDefinition.Id and
				AdditionalMembers.MemberRelationshipId = 1
				and AdditionalMembers.IsDeleted != 1
		) SpouseGender,
		cast
		(
			(
				select
					isnull(cast(left(AdditionalMembers.IdNumber, 6) as date), '')
				from AdditionalMembers
				where
					AdditionalMembers.ProposalDefinitionId = ProposalDefinition.Id and
					AdditionalMembers.MemberRelationshipId = 1 AND
					AdditionalMembers.IsDeleted != 1
			)
		as varchar) SpouseDateOfBirth ,
		ProposalDefinition.SumInsured FuneralBenefit,
		ProposalDefinition.SumInsured AccidentalDeathBenefit,
		6000.00 GroceryBenefit,
		5000.00 MemorialBenefit,
		
		0.00 ChildrenPremium,
		8.21 ExtendedFamilyAdditionalChildrenPremium,
		QuoteItem.Premium TotalMonthlyPremium,
		(
			select top 1
				Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code
			from [Address]
			where
				[Address].PartyId = Party.Id and
				[Address].IsDefault = 1 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address')
		) DefaultPhysical,
		(
			select top 1
				Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code
			from [Address]
			where
				[Address].PartyId = Party.Id and
				[Address].IsDefault = 1 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address')
		) DefaultPostal,
		(
			select top 1
				Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code
			from [Address]
			where
				[Address].PartyId = Party.Id and
				[Address].IsDefault = 0 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address')
		) NoDefaultPhysical,
		(
			select top 1
				Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code
			from [Address]
			where
				[Address].PartyId = Party.Id and
				[Address].IsDefault = 0 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address')
		) NoDefaultPostal,
		30 QuoteExpiration,
		Quote.InsurerReference QuoteNo,
		Individual.FirstName FirstName,
		Individual.Surname Surname,
	    md.QuestionAnswer.Answer Package
	from ProposalHeader
	inner join Party on Party.Id = ProposalHeader.PartyId
		inner join Individual on Individual.PartyId = Party.Id
		inner join md.Gender on md.Gender.Id = Individual.GenderId
		inner join LeadActivity on LeadActivity.Id = ProposalHeader.LeadActivityId
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = Proposalheader.Id
		inner join Product on Product.Id = ProposalDefinition.ProductId
		inner join QuoteHeader on QuoteHeader.ProposalHeaderId = ProposalHeader.Id
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		left join Occupation on Occupation.Id = Individual.OccupationId
		left join Party AgentParty on AgentParty.Id = LeadActivity.UserId
		left join Individual Agent on Agent.PartyId = AgentParty.Id


		inner join ProposalDefinition PD on PD.ProposalHeaderId = ProposalHeader.Id
        inner join ProposalQuestionAnswer PQA on PQA.ProposalDefinitionId = PD.Id and QuestionDefinitionId=2216
        inner join md.QuestionAnswer on md.QuestionAnswer.Id=PQA.Answer
	where
		Proposalheader.Id = @ProposalHeaderId
	order by
		Quote.Id desc

	--exec [dbo].[Reports_FuneralQuote_Header] 6835