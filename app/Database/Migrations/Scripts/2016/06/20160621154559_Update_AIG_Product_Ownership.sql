﻿DECLARE @ProductID INT
SET @ProductID=(SELECT top 1 ID FROM dbo.Product WHERE ProductCode='Virgin')

DECLARE @ContentsCoverID INT
SET @ContentsCoverID=(select top 1 ID from md.Cover where name ='Contents')

DECLARE @CoverDefContentsid INT
SET @CoverDefContentsid=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId=@ContentsCoverID)

if not exists(select * from QuestionDefinition where QuestionId=843 and CoverDefinitionId = @CoverDefContentsid and IsDeleted=0)
	BEGIN
	INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
	[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern],IsDeleted) VALUES (0, N'Ownership', @CoverDefContentsid, 843, NULL, 1, 16, 1, 1, 0, N'', N'', N'',0)
	END