﻿--PolicyAccident
Update PolicyAccident Set PolicyItemId = Id;
ALTER TABLE PolicyAccident DROP CONSTRAINT PK_PolicyAccident;
Alter table PolicyAccident drop column  Id;
Alter table PolicyAccident Add Id int identity(1,1);
Alter table PolicyAccident ADD CONSTRAINT PK_PolicyAccident PRIMARY KEY (Id);

--PolicyCoverage
Update PolicyCoverage Set PolicyItemId = Id;
ALTER TABLE PolicyCoverage DROP CONSTRAINT PK_PolicyCoverage;
Alter table PolicyCoverage drop column  Id;
Alter table PolicyCoverage Add Id int identity(1,1);
Alter table PolicyCoverage ADD CONSTRAINT PK_PolicyCoverage PRIMARY KEY (Id);

--PolicyAllRisk
Update PolicyAllRisk Set PolicyItemId = Id;
ALTER TABLE PolicyAllRisk DROP CONSTRAINT PK_PolicyAllRisk; 
Alter table PolicyAllRisk drop column  Id;
Alter table PolicyAllRisk Add Id int identity(1,1);
Alter table PolicyAllRisk ADD CONSTRAINT PK_PolicyAllRisk PRIMARY KEY (Id);

--PolicyAllRisk
Update PolicyBuilding Set PolicyItemId = Id;
ALTER TABLE PolicyBuilding DROP CONSTRAINT PK_PolicyBuilding;
Alter table PolicyBuilding drop column  Id;
Alter table PolicyBuilding Add Id int identity(1,1);
Alter table PolicyBuilding ADD CONSTRAINT PK_PolicyBuilding PRIMARY KEY (Id);

--PolicyContent
Update PolicyContent Set PolicyItemId = Id;
ALTER TABLE PolicyContent DROP CONSTRAINT PK_PolicyContent;
Alter table PolicyContent drop column  Id;
Alter table PolicyContent Add Id int identity(1,1);
Alter table PolicyContent ADD CONSTRAINT PK_PolicyContent PRIMARY KEY (Id);

--PolicyFinance
Update PolicyFinance Set PolicyItemId = Id;
ALTER TABLE PolicyFinance DROP CONSTRAINT PK_PolicyFinance;
Alter table PolicyFinance drop column  Id;
Alter table PolicyFinance Add Id int identity(1,1);
Alter table PolicyFinance ADD CONSTRAINT PK_PolicyFinance PRIMARY KEY (Id);

--PolicyFuneral
Update PolicyFuneral Set PolicyItemId = Id;
ALTER TABLE PolicyFuneral DROP CONSTRAINT PK_PolicyFuneral;
Alter table PolicyFuneral drop column  Id;
Alter table PolicyFuneral Add Id int identity(1,1);
Alter table PolicyFuneral ADD CONSTRAINT PK_PolicyFuneral PRIMARY KEY (Id);

--PolicyLiability
Update PolicyLiability Set PolicyItemId = Id;
ALTER TABLE PolicyLiability DROP CONSTRAINT PK_PolicyLiability;
Alter table PolicyLiability drop column  Id;
Alter table PolicyLiability Add Id int identity(1,1);
Alter table PolicyLiability ADD CONSTRAINT PK_PolicyLiability PRIMARY KEY (Id);

--PolicyPersonalVehicle
Update PolicyPersonalVehicle Set PolicyItemId = Id;
ALTER TABLE PolicyPersonalVehicle DROP CONSTRAINT PK_PolicyPersonalVehicle;
Alter table PolicyPersonalVehicle drop column  Id;
Alter table PolicyPersonalVehicle Add Id int identity(1,1);
Alter table PolicyPersonalVehicle ADD CONSTRAINT PK_PolicyPersonalVehicle PRIMARY KEY (Id);

--PolicySpecial
Update PolicySpecial Set PolicyItemId = Id;
ALTER TABLE PolicySpecial DROP CONSTRAINT PK_PolicySpecial;
Alter table PolicySpecial drop column  Id;
Alter table PolicySpecial Add Id int identity(1,1);
Alter table PolicySpecial ADD CONSTRAINT PK_PolicySpecial PRIMARY KEY (Id);

--PolicyWatercraft
Update PolicyWatercraft Set PolicyItemId = Id;
ALTER TABLE PolicyWatercraft DROP CONSTRAINT PK_PolicyWatercraft;
Alter table PolicyWatercraft drop column  Id;
Alter table PolicyWatercraft Add Id int identity(1,1);
Alter table PolicyWatercraft ADD CONSTRAINT PK_PolicyWatercraft PRIMARY KEY (Id);