﻿declare @ProductId int
select @ProductId = id from Product where ProductCode = 'KPIPERS2'

--Contents
declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 78)

if not exists(select * from QuestionDefinition where DisplayName = 'Thatch Safe' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Thatch Safe',@CoverDefinitionId,16,NULL,1,16,0,1,0,'Is the thatch treated with thatch safe?','','',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where DisplayName = 'Occupation Date' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Occupation Date',@CoverDefinitionId,40,NULL,1,13,1,1,0,'When was the house occupied by the insured?','','',0,NULL,0)
		END

UPDATE dbo.QuestionDefinition SET DefaultValue='' WHERE DefaultValue='0' AND CoverDefinitionId=@CoverDefinitionId AND QuestionId IN 
(1035,1037,1036,130,116,56,55,1039,1040,1041,1042,43,1054,1055,1043,179,21,1062,18,1064,1065,1066,1067,15,1068,6,129,1069,1052,1053,1063,57)

if not exists(select * from QuestionDefinition where DisplayName = 'Is the building unoccupied' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Is the building unoccupied',@CoverDefinitionId,184,NULL,1,14,0,1,0,'Number of days the building is unoccupied','','',0,NULL,0)
		END

DELETE FROM dbo.ProposalQuestionAnswer WHERE QuestionDefinitionId = (SELECT id FROM dbo.QuestionDefinition WHERE QuestionId=57 AND CoverDefinitionId=@CoverDefinitionId)
DELETE FROM dbo.QuestionDefinition WHERE QuestionId=57 AND CoverDefinitionId=@CoverDefinitionId


--Motor
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 218)

UPDATE dbo.QuestionDefinition SET DefaultValue='' WHERE DefaultValue='0' AND CoverDefinitionId=@CoverDefinitionId AND QuestionId IN 
(1010,1015,53,153,151,100,152,104,1030,1031,1032)

if not exists(select * from QuestionDefinition where DisplayName = 'Vehicle Extras Value' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Vehicle Extras Value',@CoverDefinitionId,107,NULL,1,1,0,1,0,'What is the value of the any non-factory extras?',N'',N'^[0-9\.]+$',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where QuestionId=1070 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Car Hire',@CoverDefinitionId,1070,NULL,1,21,0,1,0,'Car Hire',N'',N'',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where QuestionId=1077 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'King Cab',@CoverDefinitionId,1077,NULL,1,31,0,1,0,'King Cab',N'',N'',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where QuestionId=1078 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Credit Shortfall',@CoverDefinitionId,1078,NULL,1,43,0,1,0,'Credit Shortfall',N'',N'',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where QuestionId=1071 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Credit Shortfall Ballon Payment',@CoverDefinitionId,1071,NULL,1,44,0,1,0,'Credit Shortfall Ballon Payment',N'',N'^[0-9\.]+$',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where QuestionId=1072 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Credit Shortfall Deposit',@CoverDefinitionId,1072,NULL,1,45,0,1,0,'Credit Shortfall Deposit',N'',N'^[0-9\.]+$',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where QuestionId=1073 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Credit Shortfall Finance Term',@CoverDefinitionId,1073,NULL,1,46,0,1,0,'Credit Shortfall Finance Term',N'',N'^[0-9\.]+$',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where QuestionId=1074 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Credit Shortfall Interest',@CoverDefinitionId,1074,NULL,1,47,0,1,0,'Credit Shortfall Interest',N'',N'^[0-9\.]+$',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where QuestionId=1075 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Credit Shortfall Loan Start Date',@CoverDefinitionId,1075,NULL,1,48,0,1,0,'Credit Shortfall Loan Start Date',N'',N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where QuestionId=1076 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Credit Shortfall Loan Amount',@CoverDefinitionId,1076,NULL,1,49,0,1,0,'Credit Shortfall Loan Amount',N'',N'^[0-9\.]+$',0,NULL,0)
		END



--All Risks
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 17)
UPDATE dbo.QuestionDefinition SET QuestionDefinitionGroupTypeId=6 WHERE QuestionDefinitionGroupTypeId=2 AND QuestionId=141 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=2 WHERE GroupIndex=1 AND QuestionId=141 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET DisplayName='Asset' WHERE DisplayName='Risk Item' AND QuestionId=141 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=2,QuestionDefinitionGroupTypeId=6 WHERE QuestionId=1033 AND CoverDefinitionId=@CoverDefinitionId
UPDATE dbo.QuestionDefinition SET GroupIndex=2,QuestionDefinitionGroupTypeId=6 WHERE QuestionId=127 AND CoverDefinitionId=@CoverDefinitionId


if not exists(select * from QuestionDefinition where DisplayName = 'Serial/IMEI Number' and CoverDefinitionId = @CoverDefinitionId)
		BEGIN
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], 
		[DefaultValue], [RegexPattern]) VALUES (0, N'Serial/IMEI Number', @CoverDefinitionId, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'^[0-9\.]+$')
		END
UPDATE dbo.QuestionDefinition SET GroupIndex=2,QuestionDefinitionGroupTypeId=6 WHERE QuestionId=128 AND CoverDefinitionId=@CoverDefinitionId

--House Owners
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 44)

UPDATE dbo.QuestionDefinition SET DefaultValue='' WHERE DefaultValue='0' AND CoverDefinitionId=@CoverDefinitionId AND QuestionId IN 
(1035,130,1036,1037,36,4,57,116,56,55,1038,1039,1040,1041,1042,1043,43,1054,1055,1056,1057,1058)

if not exists(select * from QuestionDefinition where DisplayName = 'Thatch Safe' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Thatch Safe',@CoverDefinitionId,16,NULL,1,16,0,1,0,'Is the thatch treated with thatch safe?','','',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where DisplayName = 'Is the building unoccupied' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Is the building unoccupied',@CoverDefinitionId,184,NULL,1,14,0,1,0,'Number of days the building is unoccupied','','',0,NULL,0)
		END
