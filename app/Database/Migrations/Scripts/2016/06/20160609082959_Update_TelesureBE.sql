﻿declare @ProductId int
select @ProductId = id from Product where ProductCode = 'AGBRK'

declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 218)

if not exists(select * from QuestionDefinition where DisplayName = 'Financed' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
		[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern],IsDeleted) VALUES (0, N'Financed', @CoverDefinitionId, 862, NULL, 1, 10, 0, 1, 0, N'', N'', N'^[0-9]+$',0)
		END
        
if not exists(select * from QuestionDefinition where DisplayName = 'Second Hand' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Second Hand',@CoverDefinitionId,1019,NULL,1,27,0,1,0,'Second Hand','','',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where DisplayName = 'Hail Cover' and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Hail Cover',@CoverDefinitionId,132,NULL,1,27,0,1,0,'Hail Cover','','',0,NULL,0)
		END
        
UPDATE dbo.QuestionDefinition SET DefaultValue=NULL WHERE QuestionId=1015 AND CoverDefinitionId=@CoverDefinitionId

if not exists(select * from QuestionDefinition where QuestionId = 1079 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'License Endorsed Date',@CoverDefinitionId,1079,NULL,1,13,0,1,0,'License Endorsed Date','','',0,NULL,0)
		END

if not exists(select * from QuestionDefinition where QuestionId = 1080 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'License Endorsment Reason',@CoverDefinitionId,1080,NULL,1,14,0,1,0,'License Endorsment Reason','','',0,NULL,0)
		END

if exists(select * from QuestionDefinition where QuestionId = 1027 and CoverDefinitionId = @CoverDefinitionId)
		BEGIN
        DELETE FROM dbo.ProposalQuestionAnswer WHERE QuestionDefinitionId = (SELECT id FROM dbo.QuestionDefinition WHERE QuestionId=1027 AND CoverDefinitionId=@CoverDefinitionId)
		DELETE FROM dbo.QuestionDefinition where QuestionId = 1027 and CoverDefinitionId = @CoverDefinitionId
		END

if NOT exists(select * from QuestionDefinition where QuestionId = 73 and CoverDefinitionId = @CoverDefinitionId)
		BEGIN
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
		VALUES(0,'Drivers Licence Type',@CoverDefinitionId,73,NULL,1,16,1,1,0,'What is the drivers licence type of the main driver for this vehicle?','','',0,NULL,0)
		END
