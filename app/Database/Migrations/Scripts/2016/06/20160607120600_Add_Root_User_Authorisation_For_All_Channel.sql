﻿

DECLARE @MyCursor CURSOR, @ChannelId int, @UserId int

SELECT top 1 @UserId = Id FROM [user] WHERE UserName = 'root@iplatform.co.za'

SET @MyCursor = CURSOR FOR select Id from dbo.Channel      

OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @ChannelId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		 
		IF NOT EXISTS ( SELECT * FROM UserChannel WHERE UserId = @UserId AND ChannelId = @ChannelId)
		BEGIN
			INSERT INTO [dbo].[UserChannel]([CreatedAt],[ModifiedAt],[UserId],[ChannelId],[IsDeleted]) VALUES(GETDATE(),GETDATE(),@UserId,@ChannelId,0)
		END

		IF NOT EXISTS ( SELECT * FROM UserAuthorisationGroup WHERE UserId = @UserId AND ChannelId = @ChannelId)
		BEGIN
			INSERT INTO [dbo].[UserAuthorisationGroup]([AuthorisationGroupId],[UserId],[ChannelId],[IsDeleted])VALUES (5, @UserId, @ChannelId,0)
		END
		FETCH NEXT FROM @MyCursor INTO @ChannelId  
	END

CLOSE @MyCursor 
DEALLOCATE @MyCursor;
