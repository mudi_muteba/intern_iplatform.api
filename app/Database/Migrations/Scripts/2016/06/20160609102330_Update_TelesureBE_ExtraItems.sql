﻿declare @ProductId int
select @ProductId = id from Product where ProductCode = 'AGBRK'

declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 218)

if not exists(select * from QuestionDefinition where questionid=27 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
		[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern],IsDeleted) VALUES (0, N'License Endorsed', @CoverDefinitionId, 27, NULL, 1, 12, 0, 1, 0, N'License Endorsed', N'', N'',0)
		END

if not exists(select * from QuestionDefinition where questionid=72 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
		[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern],IsDeleted) VALUES (0, N'Main Driver Marital Status', @CoverDefinitionId, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'',0)
		END