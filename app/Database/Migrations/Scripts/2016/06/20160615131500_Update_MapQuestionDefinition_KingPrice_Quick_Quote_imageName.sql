﻿declare @image varchar (10)
set @image = (select ImageName from [dbo].[Product] where productCode = 'KPIPERS')

update [dbo].[Product]  set ImageName = @image
where Name = 'Personal Insurance Quick Quote'
AND ImageName is null