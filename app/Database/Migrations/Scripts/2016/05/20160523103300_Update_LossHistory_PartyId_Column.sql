﻿IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[LossHistory]') AND type in (N'U'))
BEGIN
ALTER TABLE [dbo].[LossHistory] ALTER COLUMN [PartyId] [int] NULL
END