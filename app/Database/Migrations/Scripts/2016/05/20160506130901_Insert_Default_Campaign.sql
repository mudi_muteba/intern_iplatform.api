﻿IF NOT Exists (select id from Campaign where DefaultCampaign = 1)
BEGIN
	INSERT [dbo].[Campaign] ([Name], [Reference], [StartDate], [EndDate], [DateCreated], [DateUpdated], [DefaultCampaign], [IsDeleted], [IsUsed], [ChannelId], [IsDisabled], [Audit]) 
	VALUES (N'DefaultC', N'ss', CAST(N'2011-01-01 00:00:00.000' AS DateTime), CAST(N'2026-01-01 00:00:00.000' AS DateTime), CAST(N'2011-01-01 00:00:00.000' AS DateTime), CAST(N'2011-01-01 00:00:00.000' AS DateTime), 1, 0, 1, 1, 0, NULL)
END
