﻿Delete from Settingsirate WHERE ProductId in (1,2,34,46,47,54,55,56) and Password = "Password";

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'ProductCode' AND Object_ID = Object_ID(N'Settingsirate'))
BEGIN
    ALTER TABLE Settingsirate ADD ProductCode NVarchar(255) NULL;
end

IF EXISTS(SELECT 1 FROM sys.views 
    WHERE Name = 'vw_rating_configuration')
BEGIN
    DROP VIEW [dbo].[vw_rating_configuration];
END

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_rating_configuration]
AS
select
	   r.Id as Id
	  ,c.Id as ChannelId 
	  ,r.ChannelId as ChannelSystemId
	  ,r.ProductId
	  ,[Password] as RatingPassword
      ,[AgentCode] as RatingAgentCode
      ,[BrokerCode] as RatingBrokerCode
      ,[AuthCode] as RatingAuthCode
      ,[SchemeCode] as RatingSchemeCode
      ,[Token] as RatingToken
      ,r.[Environment] as RatingEnvironment
      ,[UserId] as RatingUserId
      ,r.[SubscriberCode] as RatingSubscriberCode
      ,[CompanyCode] as RatingCompanyCode
      ,[UwCompanyCode] as RatingUwCompanyCode
      ,[UwProductCode] as RatingUwProductCode
	  ,[CheckRequired] as ITCCheckRequired
      ,p.[SubscriberCode] as ITCSubscriberCode
      ,[BranchNumber] as ITCBranchNumber
      ,[BatchNumber] as ITCBatchNumber
      ,[SecurityCode] as ITCSecurityCode
      ,[EnquirerContactName] as ITCEnquirerContactName
      ,[EnquirerContactPhoneNo] as ITCEnquirerContactPhoneNo
      ,p.[Environment] as ITCEnvironment
	  ,o.Code As InsurerCode
	  ,r.ProductCode as ProductCode
	  ,r.IsDeleted
from SettingsiRate r
	inner join Channel c
		on r.ChannelId = c.SystemId
	inner join Product product
		on r.ProductId = product.Id
	inner join Organization o
		on product.ProductOwnerId = o.PartyId
	left join SettingsiPerson p
		on r.ChannelId = p.ChannelId
where r.IsDeleted = 0
	and c.IsActive = 1

GO

INSERT INTO [dbo].[SettingsiRate]([ProductId],[Password],[AgentCode],[ChannelId],[UserId],[SubscriberCode],[IsDeleted],[ProductCode],[Environment] )
     VALUES (47, 'I2P0L1A5T2F0O1R5M', 'IPLATFORM', '2A2339E6-D556-428E-BD0B-9E2ECF1715A5', '','', 0,'VMSAFuneral','');
INSERT INTO [dbo].[SettingsiRate]([ProductId],[Password],[AgentCode],[ChannelId],[UserId],[SubscriberCode],[IsDeleted],[ProductCode],[Environment])
     VALUES (46, 'I2P0L1A5T2F0O1R5M', 'IPLATFORM', '2A2339E6-D556-428E-BD0B-9E2ECF1715A5', 'VMSA_WEB','V2M0S1A6W2E0B16', 0,'VMSA','');
INSERT INTO [dbo].[SettingsiRate]([ProductId],[Password],[AgentCode],[ChannelId],[UserId],[SubscriberCode],[IsDeleted],[ProductCode],[Environment])
     VALUES (47, 'I2P0L1A5T2F0O1R5M', 'IPLATFORM', '60506D0E-0A60-48F3-9CD4-728555E1B3AF', '','', 0,'VMSAFuneral','');
INSERT INTO [dbo].[SettingsiRate]([ProductId],[Password],[AgentCode],[ChannelId],[UserId],[SubscriberCode],[IsDeleted],[ProductCode],[Environment])
     VALUES (46, 'I2P0L1A5T2F0O1R5M', 'IPLATFORM', '60506D0E-0A60-48F3-9CD4-728555E1B3AF', 'VMSA_WEB','V2M0S1A6W2E0B16', 0,'VMSA','');
INSERT INTO [dbo].[SettingsiRate]([ProductId],[Password],[AgentCode],[ChannelId],[UserId],[SubscriberCode],[IsDeleted],[ProductCode],[Environment])
     VALUES (47, 'I2P0L1A5T2F0O1R5M', 'IPLATFORM', '8168BBF8-468E-4628-9AF8-2A074654557D', '','', 0,'VMSAFuneral','');
INSERT INTO [dbo].[SettingsiRate]([ProductId],[Password],[AgentCode],[ChannelId],[UserId],[SubscriberCode],[IsDeleted],[ProductCode],[Environment])
     VALUES (46, 'I2P0L1A5T2F0O1R5M', 'IPLATFORM', '8168BBF8-468E-4628-9AF8-2A074654557D', 'VMSA_WEB','V2M0S1A6W2E0B16', 0,'VMSA','');

