﻿--Oakhurst Insurance Company Limited

-- Dotsure Motor
declare @pId int

set @pId = (select Id from [dbo].[Product] where productCode = 'DOTSURE')

declare @CoverId int

set @CoverId = (select Id from [md].[Cover] where Code = 'MOTOR')

declare @CoverDef int

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Private Type Vehicle', N'', 0, 0, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Use of vehicle', N'Business or Private use', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Comprehensive Cover', N'If requested', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'New for Old', N'Not included', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Windscreen', N'Comprehensive only', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Fire and theft Cover', N'If requested', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Cover', N'If requested', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Riot and Strike outside RSA', N'No', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Tracking device to be covered', N'Not included', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Towing and Storage', N'Yes - insurer supplier', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Sound System [not factory fitted]', N'If requested', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Hail Damage', N'Yes', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Canopy of the pick-up', N'If requested', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Car Hire', N'Not included', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Emergency Hotel Expenses', N'Yes (ld Club benefits)', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Credit Shortfall ', N'Not included', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Medical Costs', N'Not included', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Liability', N'R 1,000,000', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party fire and explosion Liability', N'R 250,000', 1, 19, 0)

-- Oakhurst Motor

set @pId = (select Id from [dbo].[Product] where productCode = 'OAKHURST')


set @CoverId = (select Id from [md].[Cover] where Code = 'MOTOR')


set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Use of vehicle', N'Business or Private use', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Comprehensive Cover', N'If requested', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'New for Old', N'Not included', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Windscreen', N'Comprehensive only', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Fire and theft Cover', N'If requested', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Cover', N'If requested', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Riot and Strike outside RSA', N'No', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Tracking device to be covered', N'Not included', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Towing and Storage', N'Yes - insurer supplier', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Sound System [not factory fitted]', N'If requested', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Hail Damage', N'Yes', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Canopy of the pick-up', N'If requested', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Car Hire', N'Not included', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Emergency Hotel Expenses', N'Yes (ld Club benefits)', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Credit Shortfall ', N'Not included', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Medical Costs', N'Not included', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Liability', N'', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party fire and explosion Liability', N'', 1, 19, 0)


