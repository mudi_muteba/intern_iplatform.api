﻿--Renasa Insurance Company Limited

--Motor
declare @pId int

set @pId = (select Id from [dbo].[Product] where productCode = 'RENPRD')

declare @CoverId int

set @CoverId = (select Id from [md].[Cover] where Code = 'MOTOR')

declare @CoverDef int

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Private Type Vehicle', N'', 0, 0, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Included', N'Yes', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Use of vehicle', N'Private / Business', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Comprehensive Cover', N'If requested', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'New for Old', N'Included', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Windscreen', N'Comprehensive only', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Third Party Fire and theft Cover', N'If requested', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Third Party Cover', N'If requested', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Riot and Strike outside RSA', N'No', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Tracking device to be covered', N'Not Included', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Towing and Storage', N'Included', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Sound System [not factory fitted]', N'If requested', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Hail Damage', N'Included', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Canopy of the pick-up', N'Included', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Car Hire', N'No', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Emergency Hotel Expenses', N'Included', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Credit Shortfall ', N'Included', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Medical Costs', N'Included', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Third Party Liability', N'Schedule limit', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 43, N'Third Party fire and explosion Liability', N'Schedule limit', 1, 19, 0)

--Home Contents
set @pId = (select Id from [dbo].[Product] where productCode = 'RENPRD')

set @CoverId = (select Id from [md].[Cover] where Code = 'CONTENTS')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Included', N'Yes', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Automatic anniversary/renewal sum insured increase', N'Yes', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Accidental Damage extension cover', N'Must be nominated', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Documents limitation', N'Included', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Deterioration of Food', N'Included', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Washing stolen at your home', N'Included', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Garden furniture stolen at your home', N'Included', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Damage to Garden', N'Included', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Guests belongings stolen at your home', N'Included', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Personal documents/coins/stamps - Loss of', N'Included', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Locks and Keys - Lost/damaged', N'Included', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Remote control units', N'Included', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Loss of water by leakage', N'Included', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Credit or Bank Cards - fraudulent use', N'Not included', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Hole in one / bowling full house', N'Not included', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Insured and Spouce death - fire or a break-in', N'Included', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Your domestics belongings - stolen following break-in', N'Included', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Medical Expenses', N'Included', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Veterinary Fees', N'Included', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Rent to live elsewhere', N'Included', 1, 20, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Storage costs for contents after damage', N'Included', 1, 21, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Belongings in a removal truck', N'Included', 1, 22, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Accidental breakage of mirrors and galss', N'Included', 1, 23, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Accidental breakage of television set', N'Included', 1, 24, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Fire brigade charges', N'Included', 1, 25, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Security Guards', N'Included', 1, 26, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Public Liability', N'Not included', 1, 27, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Tenants Liability', N'Not Included', 1, 28, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Liability to Domestic Employees', N'Included', 1, 29, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Accidental Damage extension cover', N'If nominated', 1, 30, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 44, N'Limited Cover Option', N'Inlcuded', 1, 31, 0)

--Personal All Risks
set @pId = (select Id from [dbo].[Product] where productCode = 'RENPRD')

set @CoverId = (select Id from [md].[Cover] where Code = 'ALL_RISK')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Included', N'Yes', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Riot and Strike outside RSA', N'No', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Unspecified Items as defined', N'Yes', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'   * If yes ~ Cover for Jewellery, Clothing and Personal items', N'No', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'   * If yes ~ Limit any one item', N'No', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'   * If yes ~ Cover for money and netiable instruments', N'No', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Sperfified Items [If yes refer to list of items]', N'Yes', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Items in a Bank Vault', N'Not Included', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Bicycles', N'If specified', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Wheelchairs and its accessories', N'If specified', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Prescription Glasses', N'If specified', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Contact Lenses', N'If specified', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Cellular phones', N'If specified', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Stamp Collection ', N'If stated to be covered', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Coin Collection ', N'If stated to be covered', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Transport of groceries and household ods', N'Not Included', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'All Risks cover for caravan contents', N'If stated to be covered', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'All Risks cover for car radios', N'If specified', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Borehole and swimming pool equipment', N'If specified', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Items stolen from the cabin of a vehicle limit', N'Subject to locked boot/concealment', 1, 20, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 46, N'Incident Limit : Items stolen from the locked boot of a vehicle ', N'Limit as per item insured', 1, 21, 0)

--HOME BUILDINGS
set @pId = (select Id from [dbo].[Product] where productCode = 'RENPRD')

set @CoverId = (select Id from [md].[Cover] where Code = 'BUILDING')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Included', N'Yes', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'If yes Home building definition to be checked', N'Covered', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Thatch roof home', N'If nominated/rated as such', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Swimming Pool covered', N'Yes', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Swimming Pool equipment covered', N'Fixed equipment only', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Borehole pump/equipment', N'Yes', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Tennis court covered', N'Yes', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Damage to Gardens', N'Included', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Automatic anniversary/renewal sum insured increase', N'No', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Loss of water by leakage', N'Not Included', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Rent ', N'Included', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Removal of fallen trees', N'Included', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Professional Fees', N'Included', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Glass and Sanitaryware', N'Included', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Power supply', N'Included', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Aerials', N'Included', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Fire brigade charges', N'Included', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Security Guards', N'Included', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Public Liability as a Home Owner', N'Not included', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Liability to Domestic Employees', N'Not included', 1, 20, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Accidental Damage to the dwelling', N'Included', 1, 21, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Essential Alterations', N'Included', 1, 22, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Subsidence, heave and landslip', N'Included', 1, 23, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'PERSONAL ACCIDENT', N'', 1, 24, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Included', N'Yes', 1, 25, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Disappearance', N'Included', 1, 26, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Exposure', N'Included', 1, 27, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Phychological Treatment following violent act', N'Included', 1, 28, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Life support', N'Included', 1, 29, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Repatriation', N'Included', 1, 30, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Bereavement Expenses', N'Included', 1, 31, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Mobility', N'Included', 1, 32, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( 45, N'Trauma', N'Not Included', 1, 33, 0)

