﻿--Create Product
DECLARE @insurerID INT
SET @insurerID = (SELECT TOP 1 PartyId FROM dbo.Organization WHERE code = 'KPI' ORDER BY PartyId)

if not exists (select *from [dbo].[Product] where ProductOwnerId = 28 and ProductCode = 'KPIPERS2' )
INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          --RiskItemTypeId , removed doesn't seem to be used
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          N'Personal Insurance' , -- Name - nvarchar(255)
          @insurerID , -- ProductOwnerId - int
          @insurerID , -- ProductProviderId - int
          7 , -- ProductTypeId - int
          N'' , -- AgencyNumber - nvarchar(255)
          N'KPIPERS2' , -- ProductCode - nvarchar(255)
          '2009-12-03 00:00:00.000' , -- StartDate - datetime
          '2013-09-25 00:00:00.000' , -- EndDate - datetime
          --NULL , -- RiskItemTypeId - int removed doesn't seem to be used
          0 , -- IsDeleted - bit
          N'kpi.png' , -- ImageName - nvarchar(1024)
          N'' , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )
DECLARE @productID INT
SET @productID = (select id from [dbo].[Product] where ProductCode = 'KPIPERS2')

--Motor
if not exists(select * from CoverDefinition where ProductId = @productID AND CoverId = 218)
   begin
	INSERT INTO dbo.CoverDefinition
			( MasterId ,
			  DisplayName ,
			  ProductId ,
			  CoverId ,
			  ParentId ,
			  CoverDefinitionTypeId ,
			  VisibleIndex ,
			  IsDeleted
			)
	VALUES  ( 0 , -- MasterId - int
			  N'Motor' , -- DisplayName - nvarchar(255)
			  @productID , -- ProductId - int
			  218 , -- CoverId - int
			  Null , -- ParentId - int
			  1 , -- CoverDefinitionTypeId - int
			  0 , -- VisibleIndex - int
			  0  -- IsDeleted - bit
			)
	END

declare @pId int

set @pId = (select Id from [dbo].[Product] where productCode = 'KPIPERS')

declare @CoverId int

set @CoverId = (select Id from [md].[Cover] where Code = 'MOTOR')

declare @CoverDef int

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)
  
DECLARE @coverdefID INT
SET @coverdefID = @CoverDef

 if not exists (select *from QuestionDefinition where QuestionId = 99 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @coverdefID, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')

 if not exists (select *from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Make', @coverdefID, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

 if not exists (select *from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @coverdefID, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

 if not exists (select *from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @coverdefID, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

 if not exists (select *from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @coverdefID, 97, NULL, 1, 5, 1, 1, 0, N'What is the registration number for this vehicle?', N'', N'^.{0,10}$')

 if not exists (select *from QuestionDefinition where QuestionId = 102 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Sum Insured', @coverdefID, 102, NULL, 1, 12, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')

 if not exists (select *from QuestionDefinition where QuestionId = 139 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ( [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex])
VALUES (0, N'Work Address', @coverdefID, 139, NULL, 1, 4, 1, 1, 0, N'{Title} {Name} would you please provide me with your work address?', N'', N'', 2)

if not exists (select *from QuestionDefinition where QuestionId = 140 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex]) 
VALUES (0, N'Overnight Address', @coverdefID, 140, NULL, 1, 0, 1, 1, 0, N'{Title} {Name} would you please provide me with the overnight address?', N'', N'', 1)

 if not exists (select *from QuestionDefinition where QuestionId = 141 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex]) 
VALUES (0, N'Asset', @coverdefID, 141, NULL, 1, 0, 1, 1, 0, N'I''d like to gather some information about the asset you would like to insure.', N'', N'^[0-9]+$', 1)

 if not exists (select *from QuestionDefinition where QuestionId = 142 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Vehicle Colour',@coverdefID,142,NULL,1,26,1,1,0,'Vehicle Colour','','',0,NULL,0)

 if not exists (select *from QuestionDefinition where QuestionId = 1010 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'VIN Number',@coverdefID,1010,NULL,1,22,0,0,0,'VIN Number','0','',0,NULL,0)

 if not exists (select *from QuestionDefinition where QuestionId = 1013 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Engine Number', @coverdefID, 1013, NULL, 1, 25, 0, 1, 0, N'Engine Number', N'', N'')

 if not exists (select *from QuestionDefinition where QuestionId = 1019 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Second Hand',@coverdefID,1019,NULL,1,27,1,1,0,'Second Hand','','',0,NULL,0)

 if not exists (select *from QuestionDefinition where QuestionId = 103 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Finance Company', @coverdefID, 103, NULL, 1, 27, 1, 1, 0, N'At which institution is the vehicle financed?', N'', N'')

 if not exists (select *from QuestionDefinition where QuestionId = 132 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Hail Cover',@coverdefID,132,NULL,1,27,0,1,0,'Hail Cover','','',0,NULL,0)

 if not exists (select *from QuestionDefinition where QuestionId = 1015 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Engine Modified',@coverdefID,1015,NULL,1,20,1,1,0,'Engine Modified','0','',0,NULL,0)

 if not exists (select *from QuestionDefinition where QuestionId = 53 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Type Of Cover',@coverdefID,53,NULL,1,7,1,1,0,'Type Of Cover','0','',0,NULL,0)

 if not exists (select *from QuestionDefinition where QuestionId = 153 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Vehicle Condition',@coverdefID,153,NULL,1,42,1,1,0,'Vehicle Condition','0','',0,NULL,0)

 if not exists (select *from QuestionDefinition where QuestionId = 151 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Vehicle Mileage',@coverdefID,151,NULL,1,40,1,1,0,'Vehicle Mileage','0','',0,NULL,0)

 if not exists (select *from QuestionDefinition where QuestionId = 100 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Registered Owner ID Number',@coverdefID,100,NULL,1,13,1,1,0,'What is the ID Number for the registered owner of this vehicle?','0','',0,NULL,0)

 if not exists (select *from QuestionDefinition where QuestionId = 152 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Vehicle Monthly Mileage',@coverdefID,152,NULL,1,41,1,1,0,'Vehicle Monthly Mileage','0','',0,NULL,0)

 if not exists (select *from QuestionDefinition where QuestionId = 104 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Main Driver ID Number',@coverdefID,104,NULL,1,5,1,1,0,'What is the ID Number for the main driver of this vehicle?','0','([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])',0,NULL,0)

--Parking Method

if not exists (select *from QuestionDefinition where QuestionId = 1030 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Daytime Parking Method',@coverdefID,1030,NULL,1,17,1,1,0,'Where is this vehicle parked during the day?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1031 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Overnight Parking Method',@coverdefID,1031,NULL,1,9,1,1,0,'Where will this vehicle be parked over night?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 110 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Radio Value', @coverdefID, 110, NULL, 1, 18, 1, 1, 0, N'What is the radio value for this vehicle?', N'', N'^[0-9\.]+$')


--Type of Use

if not exists (select *from QuestionDefinition where QuestionId = 1032 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Class of Use',@coverdefID,1032,NULL,1,6,1,1,0,'What is the intended class of use for this vehicle?','0','',0,NULL,0)

-- All Risks

if not exists(select * from CoverDefinition where ProductId = @productID AND CoverId = 17)
   begin
	INSERT INTO dbo.CoverDefinition
			( MasterId ,
			  DisplayName ,
			  ProductId ,
			  CoverId ,
			  ParentId ,
			  CoverDefinitionTypeId ,
			  VisibleIndex ,
			  IsDeleted
			)
	VALUES  ( 0 , -- MasterId - int
			  N'All Risks' , -- DisplayName - nvarchar(255)
			  @productID , -- ProductId - int
			  17 , -- CoverId - int
			  Null , -- ParentId - int
			  1 , -- CoverDefinitionTypeId - int
			  3 , -- VisibleIndex - int
			  0  -- IsDeleted - bit
			)
	END

set @pId = (select Id from [dbo].[Product] where productCode = 'KPIPERS')

set @CoverId = (select Id from [md].[Cover] where Code = 'ALL_RISK')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)

SET @coverdefID = @CoverDef

if not exists (select *from QuestionDefinition where QuestionId = 102 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Sum Insured', @coverdefID, 102, NULL, 1, 12, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')

if not exists (select *from QuestionDefinition where QuestionId = 88 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ( [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex])
VALUES (0, N'Risk Address', @coverdefID, 88, NULL, 1, 0, 1, 1, 0, N'What is the address where the item is located?', N'', N'', 2)

if not exists (select *from QuestionDefinition where QuestionId = 127 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ( [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex])
VALUES (0, N'Description', @coverdefID, 127, NULL, 1, 2, 1, 1, 0, N'Enter a description of the item', N'', N'', 2)

if not exists (select *from QuestionDefinition where QuestionId = 141 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ( [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex])
VALUES (0, N'Risk Item', @coverdefID, 141, NULL, 1, 0, 1, 1, 0, N'Which risk item would you like to insure?', N'', N'', 2)

--All Risk Category

if not exists (select *from QuestionDefinition where QuestionId = 1033 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'All Risk Category',@coverdefID,1033,NULL,1,0,1,1,0,'What category does the item fall under?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 43 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'No Claim Bonus',@coverdefID,43,NULL,1,15,0,1,0,'How many years have this client been claim free?','0','',0,NULL,0)

--House Owners
if not exists(select * from CoverDefinition where ProductId = @productID AND CoverId = 44)
   begin
	INSERT INTO dbo.CoverDefinition
			( MasterId ,
			  DisplayName ,
			  ProductId ,
			  CoverId ,
			  ParentId ,
			  CoverDefinitionTypeId ,
			  VisibleIndex ,
			  IsDeleted
			)
	VALUES  ( 0 , -- MasterId - int
			  N'House Owners' , -- DisplayName - nvarchar(255)
			  @productID , -- ProductId - int
			  44 , -- CoverId - int
			  Null , -- ParentId - int
			  1 , -- CoverDefinitionTypeId - int
			  2 , -- VisibleIndex - int
			  0  -- IsDeleted - bit
			)
	END
	
set @pId = (select Id from [dbo].[Product] where productCode = 'KPIPERS')

set @CoverId = (select Id from [md].[Cover] where Code = 'BUILDING')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)

SET @coverdefID = @CoverDef

if not exists (select *from QuestionDefinition where QuestionId = 102 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Sum Insured', @coverdefID, 102, NULL, 1, 12, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')

if not exists (select *from QuestionDefinition where QuestionId = 88 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ( [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex])
VALUES (0, N'Risk Address', @coverdefID, 88, NULL, 1, 0, 1, 1, 0, N'What is the address where the item is located?', N'', N'', 2)

if not exists (select *from QuestionDefinition where QuestionId = 103 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Finance Company', @coverdefID, 103, NULL, 1, 27, 1, 1, 0, N'At which institution is the item financed?', N'', N'')

--Alteration

if not exists (select *from QuestionDefinition where QuestionId = 1034 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Alteration', @coverdefID, 1034, NULL, 1, 22, 1, 1, 0, N'Alteration', N'', N'')

--Residence use

if not exists (select *from QuestionDefinition where QuestionId = 1035 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Building Usage',@coverdefID,1035,NULL,1,6,1,1,0,'What is this residence used for?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 130 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Construction Site',@coverdefID,130,NULL,1,23,1,1,0,'Is there construction work near the item?','0','',0,NULL,0)

--Informal Settlement

if not exists (select *from QuestionDefinition where QuestionId = 1036 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Informal Settlement',@coverdefID,1036,NULL,1,23,1,1,0,'Informal Settlement','0','',0,NULL,0)

--Commune

if not exists (select *from QuestionDefinition where QuestionId = 1037 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Commune',@coverdefID,1037,NULL,1,24,1,1,0,'Commune','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 36 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Subsidence and Landslip',@coverdefID,36,NULL,1,13,1,1,0,'Does this client want to add subsidence and landslip?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 4 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Lightning Conductor',@coverdefID,4,NULL,1,25,1,1,0,'Lightning Conductor','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 57 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Unoccupied',@coverdefID,57,NULL,1,14,1,1,0,'How often is the house unoccupied?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 116 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Perimeter Wall',@coverdefID,116,NULL,1,15,1,1,0,'Perimeter Wall','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 56 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Roof Construction',@coverdefID,56,NULL,1,11,1,1,0,'What is the roof construction of the property?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 55 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Wall Construction',@coverdefID,55,NULL,1,12,1,1,0,'What is the wall construction of the property?','0','',0,NULL,0)

--Floor Type

if not exists (select *from QuestionDefinition where QuestionId = 1038 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Floor Type',@coverdefID,1038,NULL,1,14,1,1,0,'Floor Type','0','',0,NULL,0)

--Construction Year

if not exists (select *from QuestionDefinition where QuestionId = 1039 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Construction Year',@coverdefID,1039,NULL,1,8,1,1,0,'Construction Year','0','',0,NULL,0)

--Home Area

if not exists (select *from QuestionDefinition where QuestionId = 1040 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Home Area',@coverdefID,1040,NULL,1,15,1,1,0,'Home Area','0','',0,NULL,0)

--Type Of Residence

if not exists (select *from QuestionDefinition where QuestionId = 1041 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Home Type',@coverdefID,1041,NULL,1,6,1,1,0,'Home Type','0','',0,NULL,0)

--Plans Approved

if not exists (select *from QuestionDefinition where QuestionId = 1042 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Plans Approved',@coverdefID,1042,NULL,1,16,1,1,0,'Plans Approved','0','',0,NULL,0)

--Rented Building

if not exists (select *from QuestionDefinition where QuestionId = 1043 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Rented Building',@coverdefID,1043,NULL,1,17,1,1,0,'Rented Building','0','',0,NULL,0)

--Boreholes

if not exists (select *from QuestionDefinition where QuestionId = 1044 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Number Of Boreholes',@coverdefID,1044,NULL,1,20,1,1,0,'Number Of Boreholes','0','^[0-9\.]+$',0,NULL,0)

--Gate Motors

if not exists (select *from QuestionDefinition where QuestionId = 1045 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Number Of Gate Motors',@coverdefID,1045,NULL,1,21,1,1,0,'Number Of Gate Motors','0','^[0-9\.]+$',0,NULL,0)

--Intercoms

if not exists (select *from QuestionDefinition where QuestionId = 1046 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Number Of Intercoms',@coverdefID,1046,NULL,1,22,1,1,0,'Number Of Intercoms','0','^[0-9\.]+$',0,NULL,0)

--Non Solar Geysers

if not exists (select *from QuestionDefinition where QuestionId = 1047 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Number Of Non Solar Geysers',@coverdefID,1047,NULL,1,23,1,1,0,'Number Of Non Solar Geysers','0','^[0-9\.]+$',0,NULL,0)

--Solar Geysers

if not exists (select *from QuestionDefinition where QuestionId = 1048 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Number Of Solar Geysers',@coverdefID,1048,NULL,1,24,1,1,0,'Number Of Solar Geysers','0','^[0-9\.]+$',0,NULL,0)

--Satellite Dishes

if not exists (select *from QuestionDefinition where QuestionId = 1049 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Number Of Satellite Dishes',@coverdefID,1049,NULL,1,25,1,1,0,'Number Of Satellite Dishes','0','^[0-9\.]+$',0,NULL,0)

--Solar Panels

if not exists (select *from QuestionDefinition where QuestionId = 1050 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Number Of Solar Panels',@coverdefID,1050,NULL,1,26,1,1,0,'Number Of Solar Panels','0','^[0-9\.]+$',0,NULL,0)

--Swimming Pools

if not exists (select *from QuestionDefinition where QuestionId = 1051 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Number Of Swimming Pools',@coverdefID,1051,NULL,1,27,1,1,0,'Number Of Swimming Pools','0','^[0-9\.]+$',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 43 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'No Claim Bonus',@coverdefID,43,NULL,1,15,0,1,0,'How many years have this client been claim free?','0','',0,NULL,0)

--Property Owned Claim

if not exists (select *from QuestionDefinition where QuestionId = 1054 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Property Owned Claim',@coverdefID,1054,NULL,1,16,1,1,0,'Property Owned Claim','0','',0,NULL,0)

--Property Stolen Claim

if not exists (select *from QuestionDefinition where QuestionId = 1055 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Property Stolen Claim',@coverdefID,1055,NULL,1,17,1,1,0,'Property Stolen Claim','0','',0,NULL,0)

--thatch

if not exists (select *from QuestionDefinition where QuestionId = 1056 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Thatch Lapa',@coverdefID,1056,NULL,1,25,1,1,0,'Thatch Lapa','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1057 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Thatch Lapa Within 10 Meters',@coverdefID,1057,NULL,1,26,1,1,0,'Thatch Lapa Within 10 Meters','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1058 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Thatch Lapa Treated',@coverdefID,1058,NULL,1,27,1,1,0,'Thatch Lapa Treated','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1059 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Thatch Coverage',@coverdefID,1059,NULL,1,28,1,1,0,'Thatch Coverage','0','',0,NULL,0)


--Household Contents
if not exists(select * from CoverDefinition where ProductId = @productID AND CoverId = 78)
   begin
	INSERT INTO dbo.CoverDefinition
			( MasterId ,
			  DisplayName ,
			  ProductId ,
			  CoverId ,
			  ParentId ,
			  CoverDefinitionTypeId ,
			  VisibleIndex ,
			  IsDeleted
			)
	VALUES  ( 0 , -- MasterId - int
			  N'Household Contents' , -- DisplayName - nvarchar(255)
			  @productID , -- ProductId - int
			  78 , -- CoverId - int
			  Null , -- ParentId - int
			  1 , -- CoverDefinitionTypeId - int
			  1 , -- VisibleIndex - int
			  0  -- IsDeleted - bit
			)
	END
	
set @pId = (select Id from [dbo].[Product] where productCode = 'KPIPERS')

set @CoverId = (select Id from [md].[Cover] where Code = 'CONTENTS')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)

SET @coverdefID = @CoverDef

if not exists (select *from QuestionDefinition where QuestionId = 102 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], 
[ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Sum Insured', @coverdefID, 102, NULL, 1, 12, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')

if not exists (select *from QuestionDefinition where QuestionId = 88 and CoverDefinitionId = @CoverDef)

INSERT [dbo].[QuestionDefinition] ( [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex])
VALUES (0, N'Risk Address', @coverdefID, 88, NULL, 1, 0, 1, 1, 0, N'What is the address where the item is located?', N'', N'', 2)

if not exists (select *from QuestionDefinition where QuestionId = 1035 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Building Usage',@coverdefID,1035,NULL,1,6,1,1,0,'What is this residence used for?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1037 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Commune',@coverdefID,1037,NULL,1,24,1,1,0,'Commune','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1036 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Informal Settlement',@coverdefID,1036,NULL,1,23,1,1,0,'Informal Settlement','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 130 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Construction Site',@coverdefID,130,NULL,1,23,1,1,0,'Is there construction work near the item?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 57 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Unoccupied',@coverdefID,57,NULL,1,14,1,1,0,'How often is the house unoccupied?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 116 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Perimeter Wall',@coverdefID,116,NULL,1,15,1,1,0,'Perimeter Wall','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 56 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Roof Construction',@coverdefID,56,NULL,1,11,1,1,0,'What is the roof construction of the property?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 55 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Wall Construction',@coverdefID,55,NULL,1,12,1,1,0,'What is the wall construction of the property?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1039 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Construction Year',@coverdefID,1039,NULL,1,8,1,1,0,'Construction Year','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1040 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Home Area',@coverdefID,1040,NULL,1,15,1,1,0,'Home Area','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1041 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Home Type',@coverdefID,1041,NULL,1,6,1,1,0,'Home Type','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1042 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Plans Approved',@coverdefID,1042,NULL,1,16,1,1,0,'Plans Approved','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 43 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'No Claim Bonus',@coverdefID,43,NULL,1,15,0,1,0,'How many years have this client been claim free?','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1054 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Property Owned Claim',@coverdefID,1054,NULL,1,16,1,1,0,'Property Owned Claim','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1055 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Property Stolen Claim',@coverdefID,1055,NULL,1,17,1,1,0,'Property Stolen Claim','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1043 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Rented Building',@coverdefID,1043,NULL,1,17,1,1,0,'Rented Building','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 179 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Alarm',@coverdefID,179,NULL,1,6,1,1,0,'Does the home have an Alarm?','0','',0,NULL,0)

--Alarm Response

if not exists (select *from QuestionDefinition where QuestionId = 1052 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Alarm Response',@coverdefID,1052,NULL,1,7,1,1,0,'Alarm Response','0','',0,NULL,0)

--Burglar Bars

if not exists (select *from QuestionDefinition where QuestionId = 1052 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Alarm Response',@coverdefID,1052,NULL,1,8,1,1,0,'Alarm Response','0','',0,NULL,0)

--Business Insured Value

if not exists (select *from QuestionDefinition where QuestionId = 1060 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Business Insured Value',@coverdefID,1060,NULL,1,9,1,1,0,'Business Insured Value','0','^[0-9\.]+$',0,NULL,0)

--Garden Insured Value

if not exists (select *from QuestionDefinition where QuestionId = 1061 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Garden Insured Value',@coverdefID,1061,NULL,1,10,1,1,0,'Garden Insured Value','0','^[0-9\.]+$',0,NULL,0)

--Garden Insured Value

if not exists (select *from QuestionDefinition where QuestionId = 1061 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Garden Insured Value',@coverdefID,1061,NULL,1,10,1,1,0,'Garden Insured Value','0','^[0-9\.]+$',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 21 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Access Controlled Suburb',@coverdefID,21,NULL,1,10,1,1,0,'Access Controlled Suburb','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1062 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Domestic Worker',@coverdefID,1062,NULL,1,11,1,1,0,'Domestic Worker','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 18 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Electric Fence',@coverdefID,18,NULL,1,12,1,1,0,'Electric Fence','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1063 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Electric Gate',@coverdefID,1063,NULL,1,13,1,1,0,'Electric Gate','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1064 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Facebrick',@coverdefID,1064,NULL,1,14,1,1,0,'Facebrick','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1063 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Electric Gate',@coverdefID,1063,NULL,1,15,1,1,0,'Electric Gate','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1065 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Infrared Beams',@coverdefID,1065,NULL,1,16,1,1,0,'Infrared Beams','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1066 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Intruder Proof Glass',@coverdefID,1066,NULL,1,17,1,1,0,'Intruder Proof Glass','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1067 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Neighbours',@coverdefID,1067,NULL,1,19,1,1,0,'Neighbours','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 15 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Security Gates',@coverdefID,15,NULL,1,20,1,1,0,'Security Gates','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1068 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Dogs',@coverdefID,1068,NULL,1,21,1,1,0,'Dogs','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 6 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Holiday Home',@coverdefID,6,NULL,1,22,1,1,0,'Holiday Home','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 129 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Near Open Field',@coverdefID,129,NULL,1,23,1,1,0,'Near Open Field','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 1069 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Unoccupied By Day',@coverdefID,1069,NULL,1,23,1,1,0,'Unoccupied By Day','0','',0,NULL,0)

if not exists (select *from QuestionDefinition where QuestionId = 57 and CoverDefinitionId = @CoverDef)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Unoccupied',@coverdefID,57,NULL,1,22,1,1,0,'How often is the house unoccupied?','0','',0,NULL,0)