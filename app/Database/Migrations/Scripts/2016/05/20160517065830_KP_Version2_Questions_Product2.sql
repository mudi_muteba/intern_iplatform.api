﻿DECLARE @productid int
SET @productid = (SELECT id FROM dbo.Product WHERE ProductCode='KPIPERS2')

DECLARE @coverdefid INT
SET @coverdefid = (SELECT id FROM dbo.CoverDefinition WHERE ProductId=@productid AND CoverId=78)

--alarm and burglar bars
DELETE FROM dbo.QuestionDefinition WHERE CoverDefinitionId=@coverdefid AND QuestionId=1052

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Alarm Response',@coverdefID,1052,NULL,1,7,1,1,0,'Alarm Response','0','',0,NULL,0)

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Burglar Bars',@coverdefID,1053,NULL,1,8,1,1,0,'Burglar Bars','0','',0,NULL,0)

--garden insured value
DELETE FROM dbo.QuestionDefinition WHERE CoverDefinitionId=@coverdefid AND QuestionId=1061

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Garden Insured Value',@coverdefID,1061,NULL,1,10,1,1,0,'Garden Insured Value','0','^[0-9\.]+$',0,NULL,0)

--electric gate
DELETE FROM dbo.QuestionDefinition WHERE CoverDefinitionId=@coverdefid AND QuestionId=1063

INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
VALUES(0,'Electric Gate',@coverdefID,1063,NULL,1,15,1,1,0,'Electric Gate','0','',0,NULL,0)

