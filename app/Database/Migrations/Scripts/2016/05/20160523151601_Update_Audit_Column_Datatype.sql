﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Audit]'))
BEGIN
Alter table [Audit] Alter column Detail varchar(max);
Alter table [Audit] rebuild;
END
