﻿IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_PolicyAccident]') AND xtype = 'PK')
ALTER TABLE PolicyAccident DROP CONSTRAINT PK_PolicyAccident
GO
if exists(select * from sys.columns where Name = N'Id' and Object_ID = Object_ID(N'PolicyAccident'))
Alter table PolicyAccident drop column  Id
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_PolicyAllRisk]') AND xtype = 'PK')
ALTER TABLE PolicyAllRisk DROP CONSTRAINT PK_PolicyAllRisk
GO
IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'PolicyAllRisk'))
Alter table PolicyAllRisk drop column  Id
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_PolicyBuilding]') AND xtype = 'PK')
ALTER TABLE PolicyBuilding DROP CONSTRAINT PK_PolicyBuilding
GO
IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'PolicyBuilding'))
Alter table PolicyBuilding drop column  Id
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_PolicyContent]') AND xtype = 'PK')
ALTER TABLE PolicyContent DROP CONSTRAINT PK_PolicyContent
GO
IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'PolicyContent'))
Alter table PolicyContent drop column  Id
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_PolicyCoverage]') AND xtype = 'PK')
ALTER TABLE PolicyCoverage DROP CONSTRAINT PK_PolicyCoverage
GO
IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'PolicyCoverage'))
Alter table PolicyCoverage drop column  Id
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_PolicyFinance]') AND xtype = 'PK')
ALTER TABLE PolicyFinance DROP CONSTRAINT PK_PolicyFinance
GO
IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'PolicyFinance'))
Alter table PolicyFinance drop column  Id
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_PolicyLiability]') AND xtype = 'PK')
ALTER TABLE PolicyLiability DROP CONSTRAINT PK_PolicyLiability
GO
IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'PolicyLiability'))
Alter table PolicyLiability drop column  Id
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_PolicyPersonalVehicle]') AND xtype = 'PK')
ALTER TABLE PolicyPersonalVehicle DROP CONSTRAINT PK_PolicyPersonalVehicle
GO
IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'PolicyPersonalVehicle'))
Alter table PolicyPersonalVehicle drop column  Id
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_PolicySpecial]') AND xtype = 'PK')
ALTER TABLE PolicySpecial DROP CONSTRAINT PK_PolicySpecial
GO
IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'PolicySpecial'))
Alter table PolicySpecial drop column  Id
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_PolicyWatercraft]') AND xtype = 'PK')
ALTER TABLE PolicyWatercraft DROP CONSTRAINT PK_PolicyWatercraft
GO
IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'PolicyWatercraft'))
Alter table PolicyWatercraft drop column  Id
GO

