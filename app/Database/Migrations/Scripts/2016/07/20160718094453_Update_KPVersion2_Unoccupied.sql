﻿declare @ProductId int
select @ProductId = id from Product where ProductCode = 'KPIPERS2'

--Contents
declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 78)

UPDATE dbo.QuestionDefinition SET Displayname='How many consecutive days will the building be unoccupied in a year?' WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId = 184

--Buildings
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 44)

UPDATE dbo.QuestionDefinition SET Displayname='How many consecutive days will the building be unoccupied in a year?' WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId = 184

DELETE FROM dbo.ProposalQuestionAnswer WHERE QuestionDefinitionId = (SELECT id FROM dbo.QuestionDefinition WHERE QuestionId=57 AND CoverDefinitionId=@CoverDefinitionId)
DELETE FROM dbo.QuestionDefinition WHERE QuestionId=57 AND CoverDefinitionId=@CoverDefinitionId

