﻿if exists (select * from sys.objects where object_id = object_id(N'SalesForceStatusLog_SP'))
	begin
		drop procedure SalesForceStatusLog_SP
	end
go

create procedure SalesForceStatusLog_SP
(
	@date nvarchar(50)
)
as


if exists (select  * from tempdb.dbo.sysobjects o where o.xtype in ('U') and o.id = object_id(N'tempdb..#temp'))
drop table #temp;
if exists (select  * from tempdb.dbo.sysobjects o where o.xtype in ('U') and o.id = object_id(N'tempdb..#errors'))
drop table #errors;
WITH CTE AS(
   SELECT 
       RN = ROW_NUMBER()OVER(PARTITION BY PartyId,IsSuccess ORDER BY PartyId,IsSuccess),
	   *
   FROM dbo.SalesForceIntegrationLog
   where CreatedOn > @date
)
select PartyId,IsSuccess,MAX(RN) as [count] into #temp FROM CTE WHERE RN > 0 group by PartyId,IsSuccess
order by PartyId,IsSuccess

select b.PartyId,a.count as [Succsess],b.count [Failed] into #errors from #temp a right join #temp b 
on a.PartyId = b.PartyId 
and a.IsSuccess <> b.IsSuccess
where 
b.IsSuccess = 0
and (a.IsSuccess is null or a.IsSuccess = 1)

--select * from #errors
update #errors set Succsess = 0 where Succsess is null

select '1','Total' ,count(*) as [Total] from SalesForceIntegrationLog where CreatedOn > @date
union
select '2','Successful',count(*) as [Success] from SalesForceIntegrationLog where IsSuccess = 1 and CreatedOn > @date 
union
select '3','Failed',count(*) as [Fail] from SalesForceIntegrationLog where IsSuccess = 0 and CreatedOn > @date 
union 
select '4','Retries', sum(Failed) from #errors where Failed <= Succsess
union 
select '5','Failures not resent', sum(Failed) from #errors where Failed > Succsess
union
select '6','ERROR: Retry Response',count(*) as [Fail] from SalesForceIntegrationLog where IsSuccess = 0 and Response like 'AUTH ERROR retry your request%' and CreatedOn > @date 
union
select '7','ERROR: Authentication Failures Response',count(*) as [Fail] from SalesForceIntegrationLog where IsSuccess = 0 and Response like 'AUTH ERROR authentication failure%' and CreatedOn > @date 
union
select '8','ERROR: Other Failures',count(*) as [Fail] from SalesForceIntegrationLog where IsSuccess = 0 
and Response not like 'AUTH ERROR retry your reques%'
and Response not like 'AUTH ERROR authentication failure%'
and CreatedOn > @date 



