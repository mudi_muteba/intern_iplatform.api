﻿ALTER procedure [dbo].[Reports_QuoteSchedule_Assets_ByCover]
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@CoverId int
)
as
if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) in ('AIG Assist', 'VMI Assist')
		begin
			;with Assets_CTE (AssetId, [Description], Premium, SumInsured)
			as
				(
					select distinct
						0,
						'VMI Assist',
						cast(QuoteItem.Premium as numeric(18, 2)),
						0.00
					from ProposalHeader
						left join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						left join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						left join QuoteItem on QuoteItem.QuoteId = Quote.Id
						left join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						left join Asset on Asset.Id = ProposalDefinition.AssetId
						left join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					where
						ProposalHeader.IsDeleted = 0 and
						ProposalDefinition.IsDeleted = 0 and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0 and
						Asset.IsDeleted = 0 and
						ProposalHeader.Id = @ProposalHeaderId and
						Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
						ProposalDefinition.CoverId = @CoverId and
						CoverDefinition.CoverId = @CoverId
				)
			select distinct * from Assets_CTE order by [Description] asc 
		end
	else if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) in ('Disaster Cash', 'Identity Theft', 'Personal Legal Liability')
		begin
			;with Assets_CTE (AssetId, [Description], Premium, SumInsured)
			as
				(
					select distinct
						Asset.Id,
						'Standard',
						cast(QuoteItem.Premium as numeric(18, 2)),
						cast(ProposalDefinition.SumInsured as numeric(18, 2))
					from ProposalHeader
						inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					where
						ProposalHeader.IsDeleted = 0 and
						ProposalDefinition.IsDeleted = 0 and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0 and
						Asset.IsDeleted = 0 and
						ProposalHeader.Id = @ProposalHeaderId and
						Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
						ProposalDefinition.CoverId = @CoverId and
						CoverDefinition.CoverId = @CoverId
				)
			select distinct * from Assets_CTE order by [Description] asc 
		end
	else if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) in ('All Risk')
		begin
			select distinct
				Asset.Id AssetId,
				(
					(
						select
							md.QuestionAnswer.Answer
						from md.questionanswer
						where
							md.QuestionAnswer.id in
							(
								select
									pqa.Answer
								from ProposalQuestionAnswer pqa
									inner join ProposalDefinition pd on pd.Id = pqa.ProposalDefinitionId
									inner join QuestionDefinition qd on qd.Id = pqa.QuestionDefinitionId
									inner join md.Question q on q.Id = qd.QuestionId
									inner join md.QuestionAnswer qa on qa.QuestionId = q.Id
									inner join Asset a on a.Id = pd.AssetId
								where
									pqa.ProposalDefinitionId = ProposalDefinition.Id and
									pqa.IsDeleted = 0 and
									pqa.QuestionTypeId = 3 and
									a.Id = Asset.Id and
									q.Id in (500)
							)
					)
					+ '- ' + 
					(
						select
							pqa.Answer
						from ProposalQuestionAnswer pqa
							inner join ProposalDefinition pd on pd.Id = pqa.ProposalDefinitionId
							inner join QuestionDefinition qd on qd.Id = pqa.QuestionDefinitionId
							inner join md.Question q on q.Id = qd.QuestionId
							inner join Asset a on a.Id = pd.AssetId
						where
							pqa.ProposalDefinitionId = ProposalDefinition.Id and
							pqa.IsDeleted = 0 and
							pqa.QuestionTypeId = 4 and
							a.Id = Asset.Id and
							q.Id in (490)
					)
				) [Description],
				cast(QuoteItem.Premium as numeric(18, 2)) Premium,
				cast(ProposalDefinition.SumInsured as numeric(18, 2)) SumInsured,
				ProposalDefinition.Id
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsDeleted = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = @ProposalHeaderId and
				Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
				ProposalDefinition.CoverId = @CoverId and
				CoverDefinition.CoverId = @CoverId
			order by
				[Description] asc
		end
	else if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) in ('Motor')
		begin
			select distinct
				Asset.Id AssetId,
				Asset.[Description],
				cast(QuoteItem.Premium as numeric(18, 2)) Premium,
				--case when
				--(
				--	select
				--		count(md.QuestionAnswer.Answer)
				--	from ProposalHeader
				--		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				--		inner join Asset on Asset.Id = ProposalDefinition.AssetId
				--		inner join Product on Product.Id = ProposalDefinition.ProductId
				--		inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
				--		inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
				--		inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
				--		inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
				--		inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
				--		inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
				--		inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
				--	where
				--		ProposalHeader.Id = @ProposalHeaderId and
				--		Asset.Id = Asset.Id and
				--		ProposalHeader.IsDeleted = 0 and
				--		ProposalDefinition.IsDeleted = 0 and
				--		Asset.IsDeleted = 0 and
				--		md.QuestionGroup.Name = 'General Information' and
				--		md.QuestionType.Name in ('DropDown') and
				--		md.Question.Name = 'AIG - Motor -Type Of Vehicle' and
				--		md.QuestionAnswer.Answer in
				--		(
				--			'Light Commercial Vehicles',
				--			'Private Sedan',
				--			'Private Hatch',
				--			'Quad Bikes',
				--			'Motorcycle (on road)',
				--			'Motorcycle (off road)',
				--			'Trailer',
				--			'Caravan'
				--		)
				--) > 0
				--	then 0
				--else
				--	cast(ProposalDefinition.SumInsured as numeric(18, 2))
				--end SumInsured
				cast(0 as numeric(18, 2)) SumInsured
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsDeleted = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = @ProposalHeaderId and
				Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
				ProposalDefinition.CoverId = @CoverId and
				CoverDefinition.CoverId = @CoverId
			order by
				Asset.[Description] asc
		end
	else
		begin
			select distinct
				Asset.Id AssetId,
				Asset.[Description],
				cast(QuoteItem.Premium as numeric(18, 2)) Premium,
				cast(ProposalDefinition.SumInsured as numeric(18, 2)) SumInsured
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsDeleted = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = @ProposalHeaderId and
				Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
				ProposalDefinition.CoverId = @CoverId and
				CoverDefinition.CoverId = @CoverId
			order by
				Asset.[Description] asc
		end
go

exec Reports_QuoteSchedule_Assets_ByCover 2286, 4778, 44