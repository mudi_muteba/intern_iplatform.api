﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='Virgin')

DECLARE @ContentsCoverID INT
SET @ContentsCoverID=(select ID from md.Cover where name ='Contents')

DECLARE @CoverDefContentsid INT
SET @CoverDefContentsid=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId=@ContentsCoverID)

if not exists(select * from QuestionDefinition where QuestionId=856 and CoverDefinitionId = @CoverDefContentsid)
		begin
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
		[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern],IsDeleted) VALUES (0, N'Property Under Construction', @CoverDefContentsid, 856, NULL, 1, 14, 0, 1, 0, N'', N'', N'',0)
		end