﻿
  if not exists(select Id from [md].[Question] where Id = 1081 and Name = 'Im a dummy question')
  begin

  INSERT [md].[Question] ([Id], [Name], [QuestionGroupId] , [QuestionTypeId]) VALUES ( 1081, N'Im a dummy question', 3 , 4 )
  
  end

if not exists (select Id from [dbo].[QuestionDefinition] where QuestionId = 1081 and CoverDefinitionId = 99 )
begin

INSERT [dbo].[QuestionDefinition] ([MasterId], [QuestionId], [DisplayName], [CoverDefinitionId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote],  [RatingFactor], [ReadOnly], [ToolTip], [IsDeleted], [Hide]) VALUES (0, 1081, N'Im a dummy question', 99, 1, 1, 0, 0, 0, N'This a dummy question for mapping MultiQoute Questions', 0, 1)

end

if not exists (select Id from [dbo].[QuestionDefinition] where QuestionId = 1081 and CoverDefinitionId = 98 )
begin

INSERT [dbo].[QuestionDefinition] ([MasterId], [QuestionId], [DisplayName], [CoverDefinitionId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [IsDeleted], [Hide]) VALUES (0, 1081, N'Im a dummy question', 98, 1, 2, 0, 0, 0, N'This a dummy question for mapping MultiQoute Questions', 0, 1)

end

if not exists (select Id from [dbo].[QuestionDefinition] where QuestionId = 1081 and CoverDefinitionId = 97 )
begin

INSERT [dbo].[QuestionDefinition] ([MasterId], [QuestionId], [DisplayName], [CoverDefinitionId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [IsDeleted], [Hide]) VALUES (0, 1081, N'Im a dummy question', 97, 1, 3, 0, 0, 0, N'This a dummy question for mapping MultiQoute Questions', 0, 1)

end

if not exists (select Id from [dbo].[QuestionDefinition] where QuestionId = 1081 and CoverDefinitionId = 96 )
begin

INSERT [dbo].[QuestionDefinition] ([MasterId], [QuestionId], [DisplayName], [CoverDefinitionId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [IsDeleted], [Hide]) VALUES (0, 1081, N'Im a dummy question', 96, 1, 4, 0, 0, 0, N'This a dummy question for mapping MultiQoute Questions', 0, 1)

end

--Null ToolTip values makes creating a proposal throw an exception object reference not set to an instance exception

 update  [QuestionDefinition] set [ToolTip]= '' WHERE [ToolTip] IS NULL


 alter table [QuestionDefinition]  alter column [ToolTip] nvarchar (255) not null