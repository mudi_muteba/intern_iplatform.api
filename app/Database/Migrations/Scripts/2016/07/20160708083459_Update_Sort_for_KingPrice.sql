﻿declare @ProductId int
select @ProductId = id from Product where ProductCode = 'KPIPERS2'

--House Owners
declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 44)

UPDATE dbo.QuestionDefinition SET VisibleIndex=30 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=1044

UPDATE dbo.QuestionDefinition SET VisibleIndex=31 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=1045

UPDATE dbo.QuestionDefinition SET VisibleIndex=32 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=1046

UPDATE dbo.QuestionDefinition SET VisibleIndex=33 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=1047

UPDATE dbo.QuestionDefinition SET VisibleIndex=34 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=1048

UPDATE dbo.QuestionDefinition SET VisibleIndex=35 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=1049

UPDATE dbo.QuestionDefinition SET VisibleIndex=36 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=1050

UPDATE dbo.QuestionDefinition SET VisibleIndex=37 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=1051
GO
