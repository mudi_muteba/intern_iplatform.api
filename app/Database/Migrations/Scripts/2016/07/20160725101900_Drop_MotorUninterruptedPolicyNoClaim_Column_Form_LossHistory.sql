﻿IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'MotorUninterruptedPolicyNoClaim' AND Object_ID = Object_ID(N'LossHistory'))
	ALTER TABLE [LossHistory] DROP COLUMN [MotorUninterruptedPolicyNoClaim]