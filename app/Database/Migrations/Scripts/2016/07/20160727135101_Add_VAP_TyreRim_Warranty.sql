﻿/**Product **/
-- reset product identity 
declare @currentProductId int
select top 1 @currentProductId = id  from Product order by id desc
DBCC CHECKIDENT ( Product,RESEED ,@currentProductId)

declare @Owner int
declare @ProductType int

select top 1 @Owner = PartyID from dbo.Organization where TradingName = 'Auto & General'
select top 1 @ProductType = id from md.ProductType where Name = 'VAP'

IF NOT EXISTS(select id from [dbo].[Product] where ProductCode = 'MOTORWARRANTY' and ProductOwnerId = @Owner)
BEGIN
	SET IDENTITY_INSERT [dbo].[Product] ON
	INSERT [dbo].[Product] ([Id],[MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) 
	VALUES (61, 0, N'Motor Warranty', @Owner, @Owner, @ProductType, N'123456', N'MOTORWARRANTY', CAST(0x00009CD400000000 AS DateTime), NULL)
	Print 'Insert Product'
	SET IDENTITY_INSERT [dbo].[Product] OFF
END

GO

/**CoverDefinition **/

declare @CoverId int
set @CoverId = 375

declare @ProductId int
declare @CoverDefTypeId int

select top 1 @ProductId = p.id from Product p
inner join Organization o on o.PartyId = p.ProductOwnerId where p.name = 'Motor Warranty' and o.TradingName = 'Auto & General' 

print  @ProductId
select top 1 @CoverDefTypeId = id from [md].[CoverDefinitionType] where name = 'Add On'

IF NOT EXISTS(select id from [dbo].[CoverDefinition] where CoverDefinitionTypeId = @CoverDefTypeId and ProductId = @ProductId and CoverId = @CoverId)
BEGIN
	INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Band A', @ProductId, @CoverId, NULL, @CoverDefTypeId, 0)
	Print 'Insert CoverDefinition'
END

GO

/**QuestionDefinition  - add question to multiQuote**/

declare @ProductId int
select @ProductId = id from Product where ProductCode = 'MUL'

--Motor
declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 218)

if not exists(select * from QuestionDefinition where QuestionId=1161 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted],QuestionGroupId)
		VALUES(0,'Motor Warranty',@CoverDefinitionId,1161,NULL,1,0,0,1,0,'Would you like Motor Warranty cover',N'',N'',0,NULL,0,8)
		END

GO

/**QuestionDefinition Warranty Product**/


declare @CoverId int
set @CoverId = 375
declare @ProductId int
declare @CoverDefinitionId int

select top 1 @ProductId = p.id from Product p
inner join Organization o on o.PartyId = p.ProductOwnerId where p.name = 'Motor Warranty' and o.TradingName = 'Auto & General' 

select @CoverDefinitionId =id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId
print @CoverDefinitionId

-- QuestionDefinition - standard motor questions
IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 141 and CoverDefinitionId = @CoverDefinitionId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [QuestionGroupId]) VALUES ( 0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 1, 1, 1, 0, N'', N'', N'',8)

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefinitionId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [QuestionGroupId]) VALUES ( 0, N'Make', @CoverDefinitionId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'',8)

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefinitionId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [QuestionGroupId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'',8)

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefinitionId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [QuestionGroupId]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$',8)

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefinitionId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [QuestionGroupId]) VALUES (0, N'Vehicle Registration Number', @CoverDefinitionId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$',8)

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefinitionId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [QuestionGroupId]) VALUES (0, N'Vehicle Type', @CoverDefinitionId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'',8)

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 99 and CoverDefinitionId = @CoverDefinitionId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [QuestionGroupId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$',8)

--Warranty Period - 1162
if not exists(select * from QuestionDefinition where QuestionId=1162 and CoverDefinitionId = @CoverDefinitionId)
begin
	INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted],QuestionGroupId)
	VALUES(0,'Motor Warranty Period (In Years)',@CoverDefinitionId,1162,NULL,1,0,0,1,0,'Motor Warranty Period in years',N'',N'',0,NULL,0,8)
END

--Vehicle Mileage - 1163
if not exists(select * from QuestionDefinition where QuestionId=1163 and CoverDefinitionId = @CoverDefinitionId)
begin
	INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted],QuestionGroupId)
	VALUES(0,'Vehicle Mileage',@CoverDefinitionId,1163,NULL,1,0,0,1,0,'What is the current vehicle mileage?',N'',N'',0,NULL,0,8)
END

print 'insert QuestionDefinition'

GO

/**visible Index **/

update qd set VisibleIndex = 1
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Motor Warranty' and cd.CoverId = 375
and qd.QuestionId = 141 -- Asset

update qd set VisibleIndex = 2
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Motor Warranty' and cd.CoverId = 375
and qd.QuestionId = 97 -- Vehicle Registration Number

update qd set VisibleIndex = 3
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Motor Warranty' and cd.CoverId = 375
and qd.QuestionId = 99 --Year Of Manufacture

update qd set VisibleIndex = 4
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Motor Warranty' and cd.CoverId = 375
and qd.QuestionId = 95 --Make

update qd set VisibleIndex = 5
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Motor Warranty' and cd.CoverId = 375
and qd.QuestionId = 96 -- Model


update qd set VisibleIndex = 6
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Motor Warranty' and cd.CoverId = 375
and qd.QuestionId = 69 --Vehicle Type

update qd set VisibleIndex = 7
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Motor Warranty' and cd.CoverId = 375
and qd.QuestionId = 94 -- MM Code

update qd set VisibleIndex = 8
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Motor Warranty' and cd.CoverId = 375
and qd.QuestionId = 1162 -- Warranty Period


update qd set VisibleIndex = 9
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Motor Warranty' and cd.CoverId = 375
and qd.QuestionId = 1163 -- Vehicle Mileage

GO
/**MapQuestionDefinition **/

declare @ProductId int
declare @CoverDefinitionId int

select @ProductId = id from Product where ProductCode = 'MUL'

select @CoverDefinitionId =id from CoverDefinition where ProductId = @ProductId and CoverId = 218

declare @ParentQuestionDefinitionId int
select @ParentQuestionDefinitionId = id 
from QuestionDefinition where QuestionId=1161 and CoverDefinitionId = @CoverDefinitionId

insert into MapQuestionDefinition (ParentId,ChildId) 
select @ParentQuestionDefinitionId as [ParentId],qd.Id as [ChildId] from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Motor Warranty' and cd.CoverId = 375
and qd.QuestionId not in (
95,
96,
94,
97,
69,
99,
141)

GO


/** ADD Tyre & Rim to Multi Quote  **/


update qd set QuestionGroupId = 8
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Tyre & Rim' and cd.CoverId = 371

GO

declare @ProductId int
select @ProductId = id from Product where ProductCode = 'MUL'

--Motor
declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 218)

if not exists(select * from QuestionDefinition where QuestionId=1160 and CoverDefinitionId = @CoverDefinitionId)
		begin
		INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted],QuestionGroupId)
		VALUES(0,'Tyre & Rim',@CoverDefinitionId,1160,NULL,1,0,0,1,0,'Would you like Tyre & Rim cover',N'',N'',0,NULL,0,16)
		END

GO

declare @ProductId int
select @ProductId = id from Product where ProductCode = 'MUL'

--Motor
declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 218)

declare @ParentQuestionDefinitionId int
select @ParentQuestionDefinitionId = id from QuestionDefinition where QuestionId=1160 and CoverDefinitionId = @CoverDefinitionId


insert into MapQuestionDefinition (ParentId,ChildId) 
select @ParentQuestionDefinitionId as [ParentId],qd.Id as [ChildId] from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Tyre & Rim' and cd.CoverId = 371
and qd.QuestionId not in (
95,
96,
94,
97,
69,
99,
141)

update qd set VisibleIndex = 1
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Tyre & Rim' and cd.CoverId = 371
and qd.QuestionId = 141 -- Asset

update qd set VisibleIndex = 2
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Tyre & Rim' and cd.CoverId = 371
and qd.QuestionId = 97 -- Vehicle Registration Number

update qd set VisibleIndex = 3
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Tyre & Rim' and cd.CoverId = 371
and qd.QuestionId = 99 --Year Of Manufacture

update qd set VisibleIndex = 4
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Tyre & Rim' and cd.CoverId = 371
and qd.QuestionId = 95 --Make

update qd set VisibleIndex = 5
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Tyre & Rim' and cd.CoverId = 371
and qd.QuestionId = 96 -- Model


update qd set VisibleIndex = 6
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Tyre & Rim' and cd.CoverId = 371
and qd.QuestionId = 69 --Vehicle Type

update qd set VisibleIndex = 7
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Tyre & Rim' and cd.CoverId = 371
and qd.QuestionId = 94 -- MM Code


update qd set VisibleIndex = 8
from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Tyre & Rim' and cd.CoverId = 371
and qd.QuestionId = 138 --Band

select qd.DisplayName,qd.QuestionId,qd.VisibleIndex from Product p
inner join CoverDefinition cd on cd.ProductId = p.id
inner join QuestionDefinition qd on qd.CoverDefinitionId = cd.id
where p.Name = 'Tyre & Rim' and cd.CoverId = 371
order by qd.VisibleIndex 
--rollback

GO
/*Delete duplicate and empty MapQuestionDefinition*/

WITH CTE AS(
   SELECT ParentId, ChildId,
       RN = ROW_NUMBER()OVER(PARTITION BY ParentId,ChildId ORDER BY ParentId,ChildId)
   FROM dbo.MapQuestionDefinition
)
delete FROM CTE WHERE RN > 1

delete from MapQuestionDefinition where ParentId is null or ChildId is null

