﻿
if exists (select * from sys.objects where object_id = object_id(N'Reports_QuoteSchedule_ValueAddedProducts_ByAsset_tc_suminsured') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_QuoteSchedule_ValueAddedProducts_ByAsset_tc_suminsured
	end
go

Create procedure [dbo].[Reports_QuoteSchedule_ValueAddedProducts_ByAsset_tc_suminsured]
(
	@ProposalHeaderId int,
	@CoverId int,
	@AssetId int,
	@result nvarchar(1000) OUTPUT  
)
as
			select top 1
						@result = ProposalQuestionAnswer.Answer
					from ProposalHeader
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join Asset on Asset.Id = ProposalDefinition.AssetId
						inner join Product on Product.Id = ProposalDefinition.ProductId
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
						inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
						inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
					where
						ProposalHeader.Id = 6845 and
						Asset.Id = 1056 and
						ProposalHeader.IsDeleted = 0 and
						ProposalDefinition.IsDeleted = 0 and
						Asset.IsDeleted = 0 and
						md.QuestionGroup.Name = 'Finance Information' and
						md.QuestionType.Name in ('Textbox') and
						md.Question.Name = 'Sum Insured'

go

if exists (select * from sys.objects where object_id = object_id(N'Reports_QuoteSchedule_ValueAddedProducts_ByAsset_motor_valuation_method') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_QuoteSchedule_ValueAddedProducts_ByAsset_motor_valuation_method
	end
go


Create procedure [dbo].[Reports_QuoteSchedule_ValueAddedProducts_ByAsset_motor_valuation_method]
(
	@ProposalHeaderId int,
	@CoverId int,
	@AssetId int,
	@result nvarchar(1000) OUTPUT  
)
as
			select top 1
						@result = md.QuestionAnswer.Answer
					from ProposalHeader
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join Asset on Asset.Id = ProposalDefinition.AssetId
						inner join Product on Product.Id = ProposalDefinition.ProductId
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
						inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
						inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalHeader.Id = @ProposalHeaderId and
						Asset.Id = @AssetId and
						ProposalHeader.IsDeleted = 0 and
						ProposalDefinition.IsDeleted = 0 and
						Asset.IsDeleted = 0 and
						md.QuestionGroup.Name = 'Finance Information' and
						md.QuestionType.Name in ('DropDown') and
						md.Question.Name = 'AIG -Valuation Method'
--select @result

go
if exists (select * from sys.objects where object_id = object_id(N'Reports_select_question_answer') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_select_question_answer
	end
go

Create procedure [dbo].[Reports_select_question_answer]
(
	@ProposalHeaderId int,
	@CoverId int,
	@AssetId int,
	@QuestionName nvarchar(100),
	@result nvarchar(1000) OUTPUT  
)
as

select top 1 
@result = ProposalQuestionAnswer.Answer
			from ProposalHeader
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId
				inner join Product on Product.Id = ProposalDefinition.ProductId
				inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
				inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
				inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
				inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
				inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
				inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
			where
				ProposalHeader.Id = @ProposalHeaderId and
				Asset.Id = @AssetId and
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				md.Question.Name = @QuestionName
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_QuoteSchedule_ValueAddedProducts_ByAsset') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_QuoteSchedule_ValueAddedProducts_ByAsset
	end
go
Create procedure [dbo].[Reports_QuoteSchedule_ValueAddedProducts_ByAsset]
(
	@ProposalHeaderId int,
	@CoverId int,
	@AssetId int
)
as
	if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) not in ('Building', 'Contents', 'Motor')
		return
	if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) in ('Motor')
		begin
		declare @actual nvarchar(1000)
		declare @typeOfVehicle nvarchar(1000)
		
		exec [Reports_select_question_answer]  @ProposalHeaderId,@CoverId,@AssetId, 'AIG - Motor -Type Of Vehicle',@result = @typeOfVehicle output


		IF @typeOfVehicle = '3343' OR @typeOfVehicle = '3346' 
		BEGIN
			exec [Reports_QuoteSchedule_ValueAddedProducts_ByAsset_tc_suminsured] @ProposalHeaderId,@CoverId,@AssetId,@result = @actual output 
		END
		ELSE
			exec [Reports_QuoteSchedule_ValueAddedProducts_ByAsset_motor_valuation_method] @ProposalHeaderId,@CoverId,@AssetId,@result =@actual output

			select top 1
				(select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) [Description],
				'' Value,
				(
					 @actual
				) Actual
			from ProposalHeader
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId
				inner join Product on Product.Id = ProposalDefinition.ProductId
				inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
				inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
				inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
				inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
				inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
				inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
			where
				ProposalHeader.Id = @ProposalHeaderId and
				Asset.Id = @AssetId and
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				md.QuestionGroup.Name = 'Risk Information' and
				md.QuestionType.Name in ('DropDown') and
				md.Question.Name = 'AIG -Cover Type'
			union all
			select
				QuestionDefinition.DisplayName [Description],
				case
					when md.QuestionType.Name = 'DropDown'
						then
							case
								when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
								then ''
							else
								'Included in premium' --'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
							end
						else
							case
								when ProposalQuestionAnswer.Answer = 'true'
									then 'Included in premium'
								when ProposalQuestionAnswer.Answer = 'false'
									then ''
								else 
									'' --ProposalQuestionAnswer.Answer
								end
				end Value,
				case
					when md.QuestionType.Name = 'DropDown'
						then
							case
								when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
								then 'No'
							else
								'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
							end
						else
							case
								when ProposalQuestionAnswer.Answer = 'true'
									then 'Yes'
								when ProposalQuestionAnswer.Answer = 'false'
									then 'No'
								else 
									'No' --ProposalQuestionAnswer.Answer
								end
				end Actual
			from ProposalHeader
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId
				inner join Product on Product.Id = ProposalDefinition.ProductId
				inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
				inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
				inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
				inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
				inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
				inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
			where
				ProposalHeader.Id = @ProposalHeaderId and
				Asset.Id = @AssetId and
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				md.QuestionGroup.Name = 'Additional Options' and
				md.QuestionType.Name in ('Checkbox', 'DropDown')
		end
	else
		begin
			select
				--QuestionDefinition.DisplayName [Description],
				case
					when md.Question.Id in (858) 
						then
							'Voluntary Additional Excess' 
					else
					QuestionDefinition.DisplayName 
				end 
				[Description],
				case
					when md.QuestionType.Name = 'DropDown'
						then
							case
								when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
								then ''
							else
								'Included in premium' --'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
							end
						else
							case
								when ProposalQuestionAnswer.Answer = 'true'
									then 'Included in premium'
								when ProposalQuestionAnswer.Answer = 'false'
									then ''
								else 
									'' --ProposalQuestionAnswer.Answer
								end
				end Value,
				case
					when md.QuestionType.Name = 'DropDown'
						then
							case
								when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
								then 'No'
							else
								'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
							end
						else
							case
								when ProposalQuestionAnswer.Answer = 'true'
									then 'Yes'
								when ProposalQuestionAnswer.Answer = 'false'
									then 'No'
								else 
									'No' --ProposalQuestionAnswer.Answer
								end
				end Actual
			from ProposalHeader
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId
				inner join Product on Product.Id = ProposalDefinition.ProductId
				inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
				inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
				inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
				inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
				inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
				inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
			where
				ProposalHeader.Id = @ProposalHeaderId and
				Asset.Id = @AssetId and
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				Asset.IsDeleted = 0 and(
				--md.QuestionGroup.Name = 'Risk Information' or
				md.QuestionGroup.Name = 'Additional Options') and
				md.QuestionType.Name in ('Checkbox', 'DropDown')
				--and md.Question.Id not in (858)
				--and
				--(
				--	isnumeric(ProposalQuestionAnswer.Answer) > 0 or
				--	ProposalQuestionAnswer.Answer = 'true'
				--)
		union 
			select 'Basic Excess','','R500'		
		end
		--exec [Reports_QuoteSchedule_ValueAddedProducts_ByAsset] 6845,218,1056
		go