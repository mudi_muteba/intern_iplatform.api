﻿Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_Car Accident:What Happened' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_CO REG Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Insured Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Insured Company Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Insured Person''s Age' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Insured Surname' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Insured Initials' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Insured ID Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_VAT Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Insured Ocupation/Business' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Physical Address' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Home Telephone Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_E-mail' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Insured Fax' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Relationship Of Person To The Insured' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_Scan Vehicle Licence' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Vehicle Tare/MASS' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Vehicle Registration Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Vehicle Kilometers Completed' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Vehicle Date Purchase' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Vehicle Price Paid' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_If Vehicle Subject To HP/Lease: State Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_If Vehicle Subject To HP/Lease: Account Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_If Vehicle Subject To HP/Lease: Contact Person' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_If Vehicle Submect To HP/Lease: Contact Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_In Whose Name Is The Vehicle Registered?' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Damage To Own Vehicle' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_Is The Vehicle Drivable Or Not' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_If Not, Please Give Advise Current Location Of Vehicle' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_Driver''s Full Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Driver''s ID Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_Scan Driver Licence' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_Driver''s Licence Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_Driver''s Licence Date Of First Issue' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_Driver''s Licence Expiry Date (PrDP)' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_Driver''s Licence Code' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_Driver''s Licence' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_For What Purpose Is The Vehicle Being Used' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Was He/She Driving With Your Permission' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Was He/She In Your Employ' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Is He/She The Owner Of Another Vehicle?' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_If Yes, Give Insured Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_If Yes, Give Insured Policy Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Details Of Any Convictions For Motor Offences: Also Note Pending Cases' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Has Licence Ever Been Endorsed' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Passenger 1: Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Passenger 1: Injury' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Passenger 1: Address' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Passenger 2: Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Passenger 2: Injury' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Passenger 2: Address' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Passenger 3: Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Passenger 3: Injury' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Passenger 3: Address' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Passengers: For What Purpose Were They Carried' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Passenger: Are They Employees?' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party 1: Vehicle Registration Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party 1: Vehicle Make' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party 1: Vehicle Owner Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party 1: Owner Address' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party 1: Insurance Details' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party 1: Damages' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party 2: Vehicle Registration Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party 2: Vehicle Make' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party 2: Vehicle Owner Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party 2: Owner Address' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party 2: Insurance Details' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party 2: Damages' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Damage: Other Party Property Other Than Vehicles' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party: Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party: Address Of Owner' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party: Damage Details' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party: Personal Injuries (Other Than Insured Vehicle)' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party: Name Of Injuried' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party: Relationship To Accident EG Driver, Passenger' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party: Injury Details' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Other Party: Name Of Hospital If Applicable' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Witness 1: Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Witness 1: Address' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Witness 1: Phone Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Witness 2: Name' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Witness 2: Address' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Witness 2: Phone Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_Accident/Theft Date' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Accident_Accident/Theft Time' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Speed Before Accident' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Speed At Moment Of Impact' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Accident Weather Conditions' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Visability' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Road Surface' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Width Of Road' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Which Vehicle Lights Were On?' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Where The Street Lights On?' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Did You Brake?' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Did The Vehicle Skid?' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Were Any Warnings Given By You? (Hooting, Indicator etc)' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Did The Police Attend The Scene' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Police Station Reference Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Was The Driver/Person Tested For Drugs Or Alcohol?' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Where Were You Coming From' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Where Were You Going To' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Sketch Of Accident: Please Show Clearly The Point Of Impact And Indicate The Direction Of Travel By Arrows' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Give Details Of Any Road Safety Signs Or Warning Signs In The Vicinity Of Scene Of Accident' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Declaration: We Hereby Declare The Forgoing Particulars To Be True In Every Respect' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Scan Vehicle Licence Disc' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Vehicle Identification No. (VIN)' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Vehicle Chassis Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Vehicle Engine Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Accident_Two x Repair Quotation' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_If Write-Off: Original Registration Paper' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Accident_Proof Of Extras On Vehicle' AND ClaimsTypeId =2

DECLARE @id int,@VisibleIndex int;
SELECT @id = MAX(Id) + 1  FROM [md].[ClaimsQuestionDefinition];
SELECT @VisibleIndex = MAX(VisibleIndex) + 1  FROM [md].[ClaimsQuestionDefinition] WHERE ClaimsTypeId=2;

IF NOT (EXISTS (SELECT * FROM [md].[ClaimsQuestionDefinition] WHERE Name = 'Motor_Accident_Repair Quotation 1 from an Approved Panelbeater' AND  ClaimsTypeId = 2))
BEGIN
	INSERT INTO [md].[ClaimsQuestionDefinition]([Id],[Name],[ClaimsTypeId],[VisibleIndex], [DisplayName],[Required],[FirstPhase],[SecondPhase],[ClaimsQuestionId],[ClaimsQuestionGroupId])
	VALUES(@id ,'Motor_Accident_Repair Quotation 1 from an Approved Panelbeater',2,@VisibleIndex,'Repair Quotation 1 from an Approved Panelbeater',0,0,1,129,24)
END

SELECT @id = MAX(Id) + 1  FROM [md].[ClaimsQuestionDefinition];
SELECT @VisibleIndex = MAX(VisibleIndex) + 1  FROM [md].[ClaimsQuestionDefinition] WHERE ClaimsTypeId=2;

IF NOT (EXISTS (SELECT * FROM [md].[ClaimsQuestionDefinition] WHERE Name = 'Motor_Accident_Repair Quotation 2 from an Approved Panelbeater' AND  ClaimsTypeId = 2))
BEGIN
INSERT INTO [md].[ClaimsQuestionDefinition]([Id],[Name],[ClaimsTypeId],[VisibleIndex], [DisplayName],[Required],[FirstPhase],[SecondPhase],[ClaimsQuestionId],[ClaimsQuestionGroupId])
VALUES(@id ,'Motor_Accident_Repair Quotation 2 from an Approved Panelbeater',2,@VisibleIndex,'Repair Quotation 2 from an Approved Panelbeater',0,0,1,129,24)
END


Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Theft_Attempted Theft/Parts Stolen: What Happened' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Insurer' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Policy Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_CO REG Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Insured Name' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Insured Company Name' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Insured Surname' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Insured Initials' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Insured ID Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_VAT Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Insured Ocupation/Business' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Physical Address' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Home Telephone Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Business Telephone Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_E-mail' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Insured Fax' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Relationship Of Person To The Insured' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Injury/Illness When And Where Did The Accident Occur Or Illness Commence?' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Theft_Scan Vehicle Licence' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Vehicle Make' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Vehicle Model' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Vehicle Registration Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Vehicle Kilometers Completed' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_If Vehicle Subject To HP/Lease: State Name' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_If Vehicle Subject To HP/Lease: Account Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_If Vehicle Subject To HP/Lease: Contact Person' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_If Vehicle Submect To HP/Lease: Contact Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_In Whose Name Is The Vehicle Registered?' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Theft_Driver''s Full Name' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Driver''s Address' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Driver''s Occupation' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Driver''s ID Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_For What Purpose Is The Vehicle Being Used' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Was He/She Driving With Your Permission' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Was He/She In Your Employ' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Details Of Any Convictions For Motor Offences: Also Note Pending Cases' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Theft_Accident/Theft Date' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =1, SecondPhase =0 WHERE Name = 'Motor Theft_Accident/Theft Time' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Police Station Reference Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Declaration: We Hereby Declare The Forgoing Particulars To Be True In Every Respect' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Scan Vehicle Licence Disc' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Vehicle Identification No. (VIN)' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Vehicle Engine Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Vehicle Exterior Colour' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Vehicle Interior Colour' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Copy Of Registration Certificate' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Was The Vehicle Locked?' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_If Not, Give A Reason' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Details Of Stolen Accessories (Please Attach Invoice)' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Invoice for Stolen Accessories' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  0, FirstPhase =0, SecondPhase =0 WHERE Name = 'Motor Theft_Are These Items Seprately Insured?' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Anti-Theft/Vehicle Recovery Device Details: Make' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Details Of Window Markings' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Details Of Scratches, Dents, Defects On Vehicle' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Details Of Other Features Which Would Assist In Identification' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Driver''s License' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_ID Number' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Two x Change Of Ownership Paper' AND ClaimsTypeId =1
Update  md.ClaimsQuestionDefinition SET Required =  1, FirstPhase =0, SecondPhase =1 WHERE Name = 'Motor Theft_Spare Keys' AND ClaimsTypeId =1

Update  md.ClaimsQuestionDefinition SET ClaimsQuestionGroupId =  13 WHERE Name = 'Motor Accident_Scan Vehicle Licence Disc' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET ClaimsQuestionGroupId =  13 WHERE Name = 'Motor Accident_Vehicle Identification No. (VIN)' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET ClaimsQuestionGroupId =  13 WHERE Name = 'Motor Accident_Vehicle Chassis Number' AND ClaimsTypeId =2
Update  md.ClaimsQuestionDefinition SET ClaimsQuestionGroupId =  13 WHERE Name = 'Motor Accident_Vehicle Engine Number' AND ClaimsTypeId =2
