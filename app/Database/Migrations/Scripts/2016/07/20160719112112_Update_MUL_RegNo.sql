﻿declare @ProductId int
select @ProductId = id from Product where ProductCode = 'MUL'

--Motor
declare @CoverDefinitionId int
set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 218)

--Vehicle Registration Number
UPDATE dbo.QuestionDefinition SET RequiredForQuote=0, RatingFactor=0 WHERE RequiredForQuote=1 AND CoverDefinitionId=@CoverDefinitionId AND QuestionId = 97
