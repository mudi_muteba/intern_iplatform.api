﻿--Auto & General Product

----Auto General 
declare @pId int

set @pId = (select Id from [dbo].[Product] where productCode = 'AUGPRD')

declare @CoverId int

set @CoverId = (select Id from [md].[Cover] where Code = 'MOTOR')

declare @CoverDef int

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Private Type Vehicle', N'', 0, 0, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes/No ', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Use of vehicle', N'Private/Business', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Comprehensive Cover', N'If requested ', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'New for Old', N' Bettercar Option', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Windscreen', N'Comprehensive only', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Fire and theft Cover', N'If requested', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Cover', N'If requested', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Riot and Strike outside RSA', N'NO', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Tracking device to be covered', N'not Included', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Towing and Storage', N'If insurer appoints', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Sound System [not factory fitted]', N'If requested', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Hail Damage', N'If requested', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Canopy of the pick-up', N'Included if inspection done ', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Car Hire', N'If requested', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Emergency Hotel Expenses', N'R500/1 night accomodation ', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Credit Shortfall ', N'If requested', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Medical Costs', N'500', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party Liability', N'1,000,000', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Third Party fire and explosion Liability', N'3,000', 1, 19, 0)


--Contents

set @pId = (select Id from [dbo].[Product] where productCode = 'AUGPRD')

set @CoverId = (select Id from [md].[Cover] where Code = 'CONTENTS')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes/No ', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Automatic anniversary/renewal sum insured increase', N'Yes ', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Accidental Damage extension cover', N'for TV and sanitary ware', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Documents limitation', N'Not Included', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Deterioration of Food', N'2000', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Washing stolen at your home', N'3500', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Garden furniture stolen at your home', N'3500', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Damage to Garden', N'Not Included', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Guests belongings stolen at your home', N'5200', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Personal documents/coins/stamps - Loss of', N'4000', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Locks and Keys - Lost/damaged', N'2650', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Remote control units', N'under keys and lockes ', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Loss of water by leakage', N'Not Included', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Credit or Bank Cards - fraudulent use', N'4000', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Hole in one / bowling full house', N'2500', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Insured and Spouce death - fire or a break-in', N'15 000', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Your domestics belongings - stolen following break-in', N'5250', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Medical Expenses', N'3300', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Veterinary Fees', N'3300', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Rent to live elsewhere', N'20% of sum insured', 1, 20, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Storage costs for contents after damage', N'Included', 1, 21, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Belongings in a removal truck', N'sum insured ', 1, 22, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Accidental breakage of mirrors and galss', N'items sum insured ', 1, 23, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Accidental breakage of television set', N'items sum insured ', 1, 24, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Fire brigade charges', N'cost of charges', 1, 25, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Security Guards', N'Included ', 1, 26, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Public Liability', N'as householder R1 200 000 ', 1, 27, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Tenants Liability', N'1,200,000', 1, 28, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Liability to Domestic Employees', N'120,000', 1, 29, 0)


--All Risk

set @pId = (select Id from [dbo].[Product] where productCode = 'AUGPRD')

set @CoverId = (select Id from [md].[Cover] where Code = 'ALL_RISK')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes/No ', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Riot and Strike outside RSA', N'Not Included', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Unspecified Items as defined', N'Yes/No ', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'   * If yes ~ Cover for Jewellery, Clothing and Personal items', N'Yes/No ', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'   * If yes ~ Limit any one item', N'R4,000 ', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'   * If yes ~ Cover for money and netiable instruments', N'Not Covered', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Sperfified Items [If yes refer to list of items]', N'Yes/No ', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Items in a Bank Vault', N'Not included', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Bicycles', N'If specified', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Wheelchairs and its accessories', N'If specified', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Prescription Glasses', N'If specified', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Contact Lenses', N'If specified', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Cellular phones', N'If specified', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Stamp Collection ', N'Not included', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Coin Collection ', N'Not included', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Transport of groceries and household ods', N'not included', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'All Risks cover for caravan contents', N'If specified', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'All Risks cover for car radios', N'If specified', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Borehole and swimming pool equipment', N'If specified ', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Items stolen from the cabin of a vehicle limit ', N'5,500 ', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Incident Limit : Items stolen from the locked boot of a vehicle ', N'24,000', 1, 19, 0)



--Building

set @pId = (select Id from [dbo].[Product] where productCode = 'AUGPRD')

set @CoverId = (select Id from [md].[Cover] where Code = 'BUILDING')

set @CoverDef = (select Id from [dbo].[CoverDefinition] where productId = @pID and CoverId = @CoverId)


INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Included', N'Yes/No ', 1, 1, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'If yes Home building definition to be checked', N'', 1, 2, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Thatch roof home', N'Yes ', 1, 3, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Swimming Pool covered', N'Yes', 1, 4, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Swimming Pool equipment covered', N'under specified', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Borehole pump/equipment', N'under specified', 1, 5, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Tennis court covered', N'Yes', 1, 6, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Damage to Gardens', N'Not Included', 1, 7, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Automatic anniversary/renewal sum insured increase', N'Yes', 1, 8, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Loss of water by leakage', N'Not Included', 1, 9, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Rent ', N'20% of sum insured ', 1, 10, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Removal of fallen trees', N'Included ', 1, 11, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Professional Fees', N'Included', 1, 12, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Glass and Sanitaryware', N'Included', 1, 13, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Power supply', N'Included', 1, 14, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Aerials', N'Included', 1, 15, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Fire brigade charges', N'cost of charges ', 1, 16, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Security Guards', N'Included', 1, 17, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Public Liability as a Home Owner', N'Included', 1, 18, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Liability to Domestic Employees', N'Inclded', 1, 19, 0)

INSERT [dbo].[ProductBenefit] ( [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @CoverDef, N'Subsidence, heave and landslip', N'if specified', 1, 20, 0)

