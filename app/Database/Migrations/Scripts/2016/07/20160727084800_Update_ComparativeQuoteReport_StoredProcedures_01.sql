﻿if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Header
	end
go

create procedure Reports_DetailedComparativeQuote_Header
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@Code varchar(50)
)
as
	select distinct top 1
		Client.DisplayName Client,
		(
			select top 1
				case when [Address].IsComplex = 1
					then dbo.ToCamelCase(ltrim(rtrim([Address].Complex)) + ' ' + ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' + ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim([Address].Code)))
					else dbo.ToCamelCase(ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' +  ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim( [Address].Code)))
				end
			from [Address]
			where
				[Address].PartyId = Client.Id and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address') and
				[Address].IsDeleted = 0
		) PhysicalAddress,
		(
			select top 1
				case when [Address].IsComplex = 1
					then dbo.ToCamelCase(ltrim(rtrim([Address].Complex)) + ' ' + ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' + ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim([Address].Code)))
					else dbo.ToCamelCase(ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' +  ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim( [Address].Code)))
				end
			from [Address]
			where
				[Address].PartyId = Client.Id and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address') and
				[Address].IsDeleted = 0
		) PostalAddress,

		ClientContactDetail.Work WorkNo,
		ClientContactDetail.Home HomeNo,
		ClientContactDetail.Fax FaxNo,
		ClientContactDetail.Cell CellNo,
		ClientContactDetail.Email EmailAddress,

		case
			when len(ltrim(rtrim(ClientContact.IdentityNo))) = 0 then ClientContact.PassportNo
			else ClientContact.IdentityNo
		end IDNumber,

		Quote.QuoteHeaderId,
		Quote.InsurerReference QuoteNo,
		Product.QuoteExpiration,
		Quote.Fees,
		(
			select
				Sum(QuoteItem.Sasria)
			from QuoteItem
			where
				QuoteItem.IsDeleted = 0 and
				QuoteItem.QuoteId = Quote.Id and
				QuoteItem.Sasria is not null and
				QuoteItem.Sasria > 0
		) SASRIA,
		(
			select
				Sum(QuoteItem.Premium) + Sum(QuoteItem.Sasria) + Quote.Fees
			from QuoteItem
			where
				QuoteItem.IsDeleted = 0 and
				QuoteItem.QuoteId = Quote.Id and
				QuoteItem.Premium is not null and
				QuoteItem.Premium > 0
		) Total,
		(
			select top 1
				QuoteEMailBody
			from Organization
			where
				Code = @Code
		) QuoteEMailBody,
		(
			select top 1
				dbo.ToCamelCase(Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code)
			from [Address]
			where
				[Address].PartyId = Client.Id and
				[Address].IsDefault = 1 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address')
		) DefaultPhysical,
		(
			select top 1
				dbo.ToCamelCase(Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code)
			from [Address]
			where
				[Address].PartyId = Client.Id and
				[Address].IsDefault = 1 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address')
		) DefaultPostal,
		(
			select top 1
				dbo.ToCamelCase(Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code)
			from [Address]
			where
				[Address].PartyId = Client.Id and
				[Address].IsDefault = 0 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address')
		) NoDefaultPhysical,
		(
			select top 1
				dbo.ToCamelCase(Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code)
			from [Address]
			where
				[Address].PartyId = Client.Id and
				[Address].IsDefault = 0 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address')
		) NoDefaultPostal,
		Individual.FirstName,
		Individual.Surname
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join Product on Product.Id = Quote.ProductId
		inner join LeadActivity on LeadActivity.Id = ProposalHeader.LeadActivityId
		inner join Party Client on Client.Id = LeadActivity.PartyId
		inner join Individual on Individual.PartyId = Client.Id
		--inner join Party [Owner] on [Owner].Id = Product.ProductOwnerId
		--inner join Party [Provider] on [Provider].Id = Product.ProductProviderId
		--inner join Organization OwnerOrg on OwnerOrg.PartyId = [Owner].Id
		--inner join Organization ProviderOrg on ProviderOrg.PartyId = [Provider].Id
		left join [Address] ClientAddress on ClientAddress.PartyId = Client.Id
		left join Contact ClientContact on ClientContact.PartyId = Client.Id
		left join ContactDetail ClientContactDetail on ClientContactDetail.Id = Client.ContactDetailId
	where
		ProposalHeader.Id = @ProposalHeaderId and
		QuoteHeader.IsDeleted = 0 and
		Quote.IsDeleted = 0 and
		Product.IsDeleted = 0 and
		LeadActivity.IsDeleted = 0 and
		Client.IsDeleted = 0 and
		ClientAddress.IsDeleted = 0 and
		ClientContactDetail.IsDeleted = 0 and
		Quote.Id in (select * from fn_StringListToTable(@QuoteIds))
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Summary') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Summary
	end
go

create procedure Reports_DetailedComparativeQuote_Summary
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50)
)
as
	select
		Organization.TradingName Insurer,
		Organization.Code Code,
		Product.Name Product,
		Sum(QuoteItem.ExcessBasic) BasicExcess,
		Sum(QuoteItem.Premium) Premium,
		Sum(QuoteItem.SASRIA) SASRIA,
		Sum(Quote.Fees) Fees,
		Sum(cast((QuoteItem.Premium) as numeric(18, 2))) Total
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		--inner join QuoteState on QuoteState.QuoteId = Quote.Id
		--inner join QuoteStateEntry on QuoteStateEntry.QuoteStateId = QuoteState.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		QuoteHeader.IsDeleted = 0 and
		Quote.IsDeleted = 0 and
		--QuoteState.Errors = 0 and
		QuoteItem.IsDeleted = 0 and
		CoverDefinition.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		QuoteItem.Premium > 0
	group by
		Organization.Code,
		Organization.TradingName,
		Product.Name
	order by
		Premium desc,
		Organization.TradingName asc
go


if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Covers') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Covers
	end
go

create procedure Reports_DetailedComparativeQuote_Covers
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50)
)
as
	;with Covers_CTE (CoverId, [Description])
	as 
	(
		select distinct
			md.Cover.Id CoverId,
			case
				when md.Cover.Name = 'AIG Assist'
				then 'VMI Assist'
			else
				md.Cover.Name
			end Description
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
		where
			ProposalHeader.IsDeleted = 0 and
			QuoteHeader.IsDeleted = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			CoverDefinition.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			Quote.Id in (select * from fn_StringListToTable(@QuoteIds))
		)
	select
		CoverId,
		[Description]
	from
		Covers_CTE
	order by
		[Description] asc
go


if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Assets_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_Assets_ByCover
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@CoverId int
)
as
	select distinct
		dbo.ToCamelCase(Asset.[Description]) [Description],
		QuoteItem.SumInsured Value
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsDeleted = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Benefits_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Benefits_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_Benefits_ByCover
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@CoverId int
)
as
	declare
		@cols as nvarchar(max),
		@query as nvarchar(max)

	set @cols = 
		stuff
		(
			(
				select distinct
					',' + quotename(Organization.Code)
				from ProposalHeader
					inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
					inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
				where
					ProposalHeader.IsDeleted = 0 and
					ProposalDefinition.IsDeleted = 0 and
					QuoteHeader.IsDeleted = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0 and
					Asset.IsDeleted = 0 and
					ProposalHeader.Id = @ProposalHeaderId and
					ProposalDefinition.CoverId = @CoverId and
					CoverDefinition.CoverId = @CoverId and
					Quote.Id in
					(
						select * from fn_StringListToTable(@QuoteIds)
					)
			for xml path(''), type)
		.value('.', 'nvarchar(max)' ), 1, 1, '')

	set @query =
	'
		select
			Description,
			' + @cols + '
		from
		(
			select
				Upper(Asset.Description) Description,
				Organization.Code Insurer
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join Product on Product.Id = Quote.ProductId
				inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsDeleted = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
				ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				Quote.Id in
				(
					select * from fn_StringListToTable(''' + @QuoteIds + ''')
				)
		) x
		pivot 
		(
			max(Insurer)
			for Insurer in (' + @cols + ')
		) p

		union all

		select
			''Premium'' Description,
			' + @cols + '
		from
		(
			select
				Organization.Code Insurer,
				cast(QuoteItem.Premium as varchar) Premium
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join Product on Product.Id = Quote.ProductId
				inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsDeleted = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
				ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				Quote.Id in
				(
					select * from fn_StringListToTable(''' + @QuoteIds + ''')
				)
		) x
		pivot 
		(
			max(Premium)
			for Insurer in (' + @cols + ')
		) p

		union all

		select
			''Basic Excess'' Description,
			' + @cols + '
		from
		(
			select
				Organization.Code Insurer,
				cast(QuoteItem.ExcessBasic as varchar) ExcessBasic
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join Product on Product.Id = Quote.ProductId
				inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsDeleted = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
				ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				Quote.Id in
				(
					select * from fn_StringListToTable(''' + @QuoteIds + ''')
				)
		) x
		pivot 
		(
			max(ExcessBasic)
			for Insurer in (' + @cols + ')
		) p

		union all

		select
			''SASRIA'' Description,
			' + @cols + '
		from
		(
			select
				Organization.Code Insurer,
				cast(QuoteItem.Sasria as varchar) SASRIA
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join Product on Product.Id = Quote.ProductId
				inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsDeleted = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
				ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				Quote.Id in
				(
					select * from fn_StringListToTable(''' + @QuoteIds + ''')
				)
		) x
		pivot 
		(
			max(SASRIA)
			for Insurer in (' + @cols + ')
		) p

		union all

		select
			''Total Payment'' Description,
			' + @cols + '
		from
		(
			select
				Organization.Code Insurer,
				cast((QuoteItem.Premium + QuoteItem.Sasria) as varchar) TotalPayment
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join Product on Product.Id = Quote.ProductId
				inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsDeleted = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
				ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				Quote.Id in
				(
					select * from fn_StringListToTable(''' + @QuoteIds + ''')
				)
		) x
		pivot 
		(
			max(TotalPayment)
			for Insurer in (' + @cols + ')
		) p

		union all

		select
			Description,
			' + @cols + '
		from
		(
			select
				Organization.Code Insurer,
				ProductBenefit.Name Description,
				ProductBenefit.Value BenefitValue
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join Product on Product.Id = Quote.ProductId
				inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
			where
				ProductBenefit.Name is not null and
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsDeleted = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
				ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				Quote.Id in
				(
					select * from fn_StringListToTable(''' + @QuoteIds + ''')
				)
		) x
		pivot 
		(
			max(BenefitValue)
			for Insurer in (' + @cols + ')
		) p
	'

	execute(@query);
go


if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Organizations') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Organizations
	end
go

create procedure Reports_DetailedComparativeQuote_Organizations
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@CoverId int
)
as
	;with Covers_CTE (OrganizationId, TradingName, Code, CoverDefinitionIds)
	as 
	(
		select distinct
			Organization.PartyId,
			Organization.TradingName,
			Organization.Code,
			(
				stuff
				(
					(
						select
							',' + cast(CoverDefinition.Id as nvarchar)
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization o on o.PartyId = Product.ProductOwnerId
						where
							ProposalHeader.IsDeleted = 0 and
							QuoteHeader.IsDeleted = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							CoverDefinition.IsDeleted = 0 and
							ProposalHeader.Id = @ProposalHeaderId and
							Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
							o.PartyId = Organization.PartyId and
							md.Cover.Id = @CoverId
						for xml path(''), type
					).value('.', 'NVARCHAR(MAX)'), 1, 1, '')
			)
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			ProposalHeader.IsDeleted = 0 and
			QuoteHeader.IsDeleted = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			CoverDefinition.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
			md.Cover.Id = @CoverId
		)
	select
		OrganizationId,
		TradingName,
		Code,
		CoverDefinitionIds
	from
		Covers_CTE
	order by
		TradingName asc
go


if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_AdditionalExcess_Categories_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_AdditionalExcess_Categories_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_AdditionalExcess_Categories_ByCover
(
	@ProposalHeaderId int,
	@CoverDefinitionId int
)
as
	select
		ProductAdditionalExcess.Category,
		'Not Used' NotUsed
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
		inner join CoverDefinition on CoverDefinition.CoverId = md.Cover.Id
		inner join ProductAdditionalExcess on ProductAdditionalExcess.CoverDefinitionId = CoverDefinition.Id
	where
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.IsDeleted = 0 and
		CoverDefinition.Id = @CoverDefinitionId
	group by
		ProductAdditionalExcess.Category
go


if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_AdditionalExcess_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_AdditionalExcess_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_AdditionalExcess_ByCover
(
	@ProposalHeaderId int,
	@CoverDefinitionId int
)
as
	select
		ProductAdditionalExcess.Id,
		ProductAdditionalExcess.[Index] 'Index',
		ProductAdditionalExcess.Category,
		ProductAdditionalExcess.[Description] 'Description',
		ProductAdditionalExcess.ActualExcess,
		ProductAdditionalExcess.MinExcess,
		ProductAdditionalExcess.MaxExcess,
		ProductAdditionalExcess.Percentage,
		ProductAdditionalExcess.IsPercentageOfClaim,
		ProductAdditionalExcess.IsPercentageOfItem,
		ProductAdditionalExcess.IsPercentageOfSumInsured
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
		inner join CoverDefinition on CoverDefinition.CoverId = md.Cover.Id
		inner join ProductAdditionalExcess on ProductAdditionalExcess.CoverDefinitionId = CoverDefinition.Id
	where
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.IsDeleted = 0 and
		CoverDefinition.Id = @CoverDefinitionId
	order by
		ProductAdditionalExcess.[Index] asc
go


if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_ProductAddendums') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_ProductAddendums
	end
go


create procedure Reports_DetailedComparativeQuote_ProductAddendums
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50)
)
as
	select distinct
		Organization.TradingName Insurer,
		Product.Name Product,
		Product.QuoteExpiration QuoteValidFor,
		'<ol>' +
		stuff
		(
			(
				select distinct
					'<li>' + pd.[Description] + '</li>'
				from ProposalHeader ph
					inner join QuoteHeader qh on qh.ProposalHeaderId = ph.Id
					inner join Quote q on q.QuoteHeaderId = qh.Id
					inner join Product p  on p.Id = q.ProductId
					inner join ProductDescription pd on pd.ProductId = p.Id
				where
					p.Id = Product.Id
				--order by
				--	pd.[Index] asc
				for xml path(''), type
			).value('.', 'NVARCHAR(MAX)'), 1, 0, ''
		)
		+ '</ol>' Addendum
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsDeleted = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		Quote.Id in (select * from fn_StringListToTable(@QuoteIds))
	order by
		Organization.TradingName
go