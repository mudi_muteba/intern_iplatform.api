﻿delete from ProductAdditionalExcess
dbcc checkident ('ProductAdditionalExcess', reseed, 0)

--KingPrice (Motor), Product Codes: 'KPIPERS', 'KPIPERS2'
declare
	@KingPrice_KPIPERS_Motor_CoverDefinitionId int,
	@KingPrice_KPIPERS2_Motor_CoverDefinitionId int
	
select
	@KingPrice_KPIPERS_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS') and
	Cover.Name in ('Motor')

select
	@KingPrice_KPIPERS2_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS2') and
	Cover.Name in ('Motor')

if	isnull(@KingPrice_KPIPERS_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 0, 'General', 'Basic Excess', 4500, 1500, 27500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 0, 'General', 'Basic Excess', 4500, 1500, 27500, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 1, 'General', 'Specified Ratio', 750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 1, 'General', 'Specified Ratio', 750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 2, 'General', 'Window Replacement (Excl. Panoramic Glass)', 750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 2, 'General', 'Window Replacement (Excl. Panoramic Glass)', 750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 3, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'Is younger than 25 years OR has had their driver’s licence for less than 2 years', 4500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 3, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'Is younger than 25 years OR has had their driver’s licence for less than 2 years', 4500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 4, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'If the incident occurred outside of SA and the car is not drivable', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 4, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'If the incident occurred outside of SA and the car is not drivable', 7500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 5, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'When the incident occurs in the first 3 months of cover', 3500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 5, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'When the incident occurs in the first 3 months of cover', 3500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 6, 'Trailer & Caravans', 'Caravan or caravan contents', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 6, 'Trailer & Caravans', 'Caravan or caravan contents', 2500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 7, 'Trailer & Caravans', 'Caravan or trailer contents', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 7, 'Trailer & Caravans', 'Caravan or trailer contents', 2500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 8, 'Trailer & Caravans', 'Liability to other parties', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 8, 'Trailer & Caravans', 'Liability to other parties', 0, 0, 0, 0, 0, 0, 0, 0)
	end


--KingPrice (Building), Product Codes: 'KPIPERS', 'KPIPERS2'
declare
	@KingPrice_KPIPERS_Building_CoverDefinitionId int,
	@KingPrice_KPIPERS2_Building_CoverDefinitionId int
	
select
	@KingPrice_KPIPERS_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS') and
	Cover.Name in ('Building')

select
	@KingPrice_KPIPERS2_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS2') and
	Cover.Name in ('Building')

if	isnull(@KingPrice_KPIPERS_Building_CoverDefinitionId, 0) > 0 and
	isnull(@KingPrice_KPIPERS2_Building_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Building_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Building_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)
	end

--KingPrice (Contents), Product Codes: 'KPIPERS', 'KPIPERS2'
declare
	@KingPrice_KPIPERS_Contents_CoverDefinitionId int,
	@KingPrice_KPIPERS2_Contents_CoverDefinitionId int
	
select
	@KingPrice_KPIPERS_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS') and
	Cover.Name in ('Contents')

select
	@KingPrice_KPIPERS2_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS2') and
	Cover.Name in ('Contents')

if	isnull(@KingPrice_KPIPERS_Contents_CoverDefinitionId, 0) > 0 and
	isnull(@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 1, 'Additional Cover Excess', 'Food that has deteriorated', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 1, 'Additional Cover Excess', 'Food that has deteriorated', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 2, 'Additional Cover Excess', 'Washing stolen from the line', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 2, 'Additional Cover Excess', 'Washing stolen from the line', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 3, 'Additional Cover Excess', 'Guests belongings', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 3, 'Additional Cover Excess', 'Guests belongings', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 4, 'Additional Cover Excess', 'Locks & keys', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 4, 'Additional Cover Excess', 'Locks & keys', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 5, 'Additional Cover Excess', 'Hole in one in golf/full house in bowling', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 5, 'Additional Cover Excess', 'Hole in one in golf/full house in bowling', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 6, 'Additional Cover Excess', 'Domestic employee’s belongings', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 6, 'Additional Cover Excess', 'Domestic employee’s belongings', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 7, 'Additional Cover Excess', 'Rent to live elsewhere', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 7, 'Additional Cover Excess', 'Rent to live elsewhere', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 8, 'Additional Cover Excess', 'Garden & leisure equipment', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 8, 'Additional Cover Excess', 'Garden & leisure equipment', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 9, 'Additional Cover Excess', 'Veterinary expense', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 9, 'Additional Cover Excess', 'Veterinary expense', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 10, 'Portable Posessions', 'Unspecified items (per item)', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 10, 'Portable Posessions', 'Specified items', 500, 0, 0, 5, 0, 0, 0, 0)
	end


--SA Underwriters (Motor), Product Codes: 'CENTND', 'CENTRD', 'SNTMND', 'SNTMRD'
declare
	@SAUnderwriters_CENTND_Motor_CoverDefinitionId int,
	@SAUnderwriters_CENTRD_Motor_CoverDefinitionId int,
	@SAUnderwriters_SNTMND_Motor_CoverDefinitionId int,
	@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId int
	
select
	@SAUnderwriters_CENTND_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('CENTND') and
	Cover.Name in ('Motor')

select
	@SAUnderwriters_CENTRD_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('CENTRD') and
	Cover.Name in ('Motor')

select
	@SAUnderwriters_SNTMND_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('SNTMND') and
	Cover.Name in ('Motor')

select
	@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('SNTMRD') and
	Cover.Name in ('Motor')

if	isnull(@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
	end