﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='KPIPERS2')

DECLARE @CoverID INT
SET @CoverID=(select ID from md.Cover where name ='Motor')

DECLARE @CoverDefid INT
SET @CoverDefid=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId=@CoverID)

UPDATE dbo.QuestionDefinition SET ToolTip=N'Percentage value of Balloon payment. Note: 35% to be captured as 0.35' WHERE QuestionId=1071 AND CoverDefinitionId=@CoverDefid

 
