﻿
if exists(select * from sys.columns where Name = N'Status' and Object_ID = Object_ID(N'IndividualUploadDetail'))
begin
	alter table IndividualUploadDetail drop column Status
end

if exists(select * from sys.columns where Name = N'Status' and Object_ID = Object_ID(N'IndividualUploadHeader'))
begin
	alter table IndividualUploadHeader drop column Status
end