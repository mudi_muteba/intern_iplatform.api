﻿Update qd
set hide = 1,
	RequiredForQuote = 0,
	RatingFactor = 0
From
QuestionDefinition qd 
INNER JOIN [CoverDefinition] cd 
on qd.CoverDefinitionId = cd.Id
and cd.CoverId = 62 
and cd.ProductId = 27
and qd.QuestionId = 94

