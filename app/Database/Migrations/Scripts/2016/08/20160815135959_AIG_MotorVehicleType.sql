﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='Virgin')

DECLARE @ContentsCoverID INT
SET @ContentsCoverID=(select ID from md.Cover where name ='Motor')

DECLARE @CoverDefid INT
SET @CoverDefid=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId=@ContentsCoverID)

DECLARE @QuestionDefID INT
set @QuestionDefID = (SELECT ID FROM dbo.QuestionDefinition WHERE QuestionId=786 AND IsDeleted=0 AND CoverDefinitionId=@CoverDefid)

--update all text answers 

update  dbo.ProposalQuestionAnswer
set Answer = null
where
QuestionDefinitionId=@QuestionDefID
and
 1 = CASE WHEN patindex('%[^0-9]%', Answer) > 0 THEN 1
               WHEN LEN(Answer) > 4 THEN 1
               WHEN convert(bigint,Answer) > 2147483647 THEN 1
               ELSE 0 END

--car
UPDATE dbo.ProposalQuestionAnswer SET answer = 3340 WHERE Answer IN (3341,3342) AND QuestionDefinitionId=@QuestionDefID

--motorcycle
UPDATE dbo.ProposalQuestionAnswer SET answer = 3344 WHERE Answer IN (3345,3347) AND QuestionDefinitionId=@QuestionDefID

--remove md
DELETE FROM md.QuestionAnswer WHERE QuestionId = 786 AND name = 'AIG - Motor -Type Of Vehicle - Private Sedan'
DELETE FROM md.QuestionAnswer WHERE QuestionId = 786 AND name = 'AIG - Motor -Type Of Vehicle - Light Commercial Vehicles'
DELETE FROM md.QuestionAnswer WHERE QuestionId = 786 AND name = 'AIG - Motor -Type Of Vehicle - Motorcycle (off road)'
DELETE FROM md.QuestionAnswer WHERE QuestionId = 786 AND name = 'AIG - Motor -Type Of Vehicle - Quad Bikes'