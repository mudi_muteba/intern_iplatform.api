﻿  if exists(select * from sys.columns where Name = N'ClientCode' and Object_ID = Object_ID(N'md.LossHistoryQuestion'))
begin
	alter table md.LossHistoryQuestion drop column ClientCode
end