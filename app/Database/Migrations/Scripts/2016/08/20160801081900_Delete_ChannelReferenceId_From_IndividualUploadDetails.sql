﻿if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_IndividualUploadDetail_CampaignReference_CampaignReferenceId]') AND parent_object_id = OBJECT_ID('IndividualUploadDetail'))
alter table IndividualUploadDetail  drop constraint FK_IndividualUploadDetail_CampaignReference_CampaignReferenceId


if exists(select * from sys.columns where Name = N'CampaignReferenceId' and Object_ID = Object_ID(N'IndividualUploadDetail'))
begin
	alter table IndividualUploadDetail drop column CampaignReferenceId
end