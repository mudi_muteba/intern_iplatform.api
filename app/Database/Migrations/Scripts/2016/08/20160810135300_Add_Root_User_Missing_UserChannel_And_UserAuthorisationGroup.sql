﻿
declare @Userid int,@MyCursor CURSOR, @channelId int;

SELECT @Userid = Id FROM [User] WHERE UserName = 'root@iplatform.co.za';

SET @MyCursor = CURSOR FOR select Id from Channel WHERE IsDeleted = 0;     
OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @channelId

	WHILE @@FETCH_STATUS = 0
	BEGIN

		IF NOT EXISTS(SELECT Id FROM UserChannel WHERE UserId = @Userid AND ChannelId = @channelId AND IsDeleted = 0)
		BEGIN
			Insert INTO UserChannel ([CreatedAt],[ModifiedAt],[UserId],[ChannelId],[IsDeleted], [IsDefault])VALUES (GETDATE(), GETDATE(), @Userid, @channelId, 0, 0)
		END
		IF NOT EXISTS(SELECT Id FROM UserAuthorisationGroup  WHERE UserId = @Userid AND ChannelId = @channelId AND IsDeleted = 0)

		BEGIN
			Insert INTO UserAuthorisationGroup ([AuthorisationGroupId],[UserId],[ChannelId],[IsDeleted]) VALUES (5, @Userid, @channelId, 0)
		END

		FETCH NEXT FROM @MyCursor INTO @channelId  
	END
		
CLOSE @MyCursor 
DEALLOCATE @MyCursor;
