﻿begin tran

UPDATE [dbo].[Channel] set SystemId = NEWID() 
GO

SET IDENTITY_INSERT [dbo].[Channel] ON
GO
IF EXISTS(select id from [dbo].[Channel] where id = 1)
UPDATE [dbo].[Channel] set Code = 'AIG',Name = 'AIG',SystemId = '2A2339E6-D556-428E-BD0B-9E2ECF1715A5' where Id = 1
ELSE
INSERT INTO  Channel (Id, SystemId, IsActive, ActivatedOn, DeactivatedOn, IsDefault, IsDeleted, CurrencyId, DateFormat, Name,Code, LanguageId, PasswordStrengthEnabled) VALUES
           (1,'2A2339E6-D556-428E-BD0B-9E2ECF1715A5', 1, GETDATE(), null, 0,0,1,'dd MMM yyyy','AIG','AIG', 2,0);

IF EXISTS(select id from [dbo].[Channel] where id = 2)
UPDATE [dbo].[Channel] set Code = 'PSG',Name = 'PSG',SystemId = '3D411609-46BC-45ED-906F-28A28436B185' where Id = 2
ELSE
INSERT INTO  Channel (Id, SystemId, IsActive, ActivatedOn, DeactivatedOn, IsDefault, IsDeleted, CurrencyId, DateFormat, Name,Code, LanguageId, PasswordStrengthEnabled) VALUES
           (2,'3D411609-46BC-45ED-906F-28A28436B185', 1, GETDATE(), null, 0,0,1,'dd MMM yyyy','PSG','PSG', 2,0);

IF EXISTS(select id from [dbo].[Channel] where id = 3)
UPDATE [dbo].[Channel] set Code = 'UAP',Name = 'UAP',SystemId = 'FF37CEC8-D49C-4E63-87BA-6CE45BDE32D1' where Id = 3
ELSE
INSERT INTO  Channel (Id, SystemId, IsActive, ActivatedOn, DeactivatedOn, IsDefault, IsDeleted, CurrencyId, DateFormat, Name,Code, LanguageId, PasswordStrengthEnabled) VALUES
           (3,'FF37CEC8-D49C-4E63-87BA-6CE45BDE32D1', 1, GETDATE(), null, 0,0,1,'dd MMM yyyy','UAP','UAP', 2,0);

IF EXISTS(select id from [dbo].[Channel] where id = 4)
UPDATE [dbo].[Channel] set Code = 'HOL',Name = 'HOL',SystemId = '8ED30FB8-EB0E-4AE8-B935-2A4864BB48CE' where Id = 4
ELSE
INSERT INTO  Channel (Id, SystemId, IsActive, ActivatedOn, DeactivatedOn, IsDefault, IsDeleted, CurrencyId, DateFormat, Name,Code, LanguageId, PasswordStrengthEnabled) VALUES
           (4,'8ED30FB8-EB0E-4AE8-B935-2A4864BB48CE', 1, GETDATE(), null, 0,0,1,'dd MMM yyyy','Hollard','HOL', 2,0);

SET IDENTITY_INSERT [dbo].[Channel] OFF
GO

declare @wrongId int

select @wrongId = Id from [dbo].[Channel] where (Code = 'AIG' or Name = 'AIG') and Id <> 1
update UserAuthorisationGroup set ChannelId = 1 where ChannelId = @wrongId
update UserChannel set ChannelId = 1 where ChannelId = @wrongId
update ChannelEvent set ChannelId = 1 where ChannelId = @wrongId

select @wrongId = Id from [dbo].[Channel] where (Code = 'PSG' or Name = 'PSG') and Id <> 2
update UserAuthorisationGroup set ChannelId = 2 where ChannelId = @wrongId
update UserChannel set ChannelId = 2 where ChannelId = @wrongId
update ChannelEvent set ChannelId = 2 where ChannelId = @wrongId

GO

select * from Channel
select * from ChannelEvent
delete from UserAuthorisationGroup where ChannelId > 5
delete from UserChannel where ChannelId > 5

delete from Channel where id > 5
DBCC CHECKIDENT ( Channel,RESEED ,4)

commit