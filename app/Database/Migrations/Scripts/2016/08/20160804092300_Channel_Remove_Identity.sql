﻿	BEGIN TRANSACTION;

	--Create temp tables
	CREATE TABLE #DropTempTable(script nvarchar(max));
	CREATE TABLE #CreateTempTable(script nvarchar(max));

	-- insert create constraint for channel fk
	INSERT INTO #CreateTempTable
	SELECT 
	N'
	  ALTER TABLE ' + QUOTENAME(s.name) + N'.'
	  + QUOTENAME(p.name) + N' WITH CHECK ADD CONSTRAINT '
	  + QUOTENAME(f.name) + 'FOREIGN KEY([ChannelId]) REFERENCES [dbo].[Channel] ([Id]);'
	FROM 
	   sys.foreign_keys AS f
	INNER JOIN 
	   sys.foreign_key_columns AS fc 
		  ON f.OBJECT_ID = fc.constraint_object_id
	INNER JOIN 
	   sys.tables t 
		  ON t.OBJECT_ID = fc.referenced_object_id
	INNER JOIN 
	   sys.tables p 
		  ON p.OBJECT_ID = fc.parent_object_id
	INNER JOIN sys.schemas AS s 
		ON p.[schema_id] = s.[schema_id]
	WHERE 
	   OBJECT_NAME (f.referenced_object_id) = 'Channel';

   -- insert drop constraint for channel fk
	INSERT INTO #DropTempTable
	SELECT 
	N'
	  ALTER TABLE ' + QUOTENAME(s.name) + N'.'
	  + QUOTENAME(p.name) + N' DROP CONSTRAINT '
	  + QUOTENAME(f.name) + ';'
	FROM 
	   sys.foreign_keys AS f
	INNER JOIN 
	   sys.foreign_key_columns AS fc 
		  ON f.OBJECT_ID = fc.constraint_object_id
	INNER JOIN 
	   sys.tables t 
		  ON t.OBJECT_ID = fc.referenced_object_id
	INNER JOIN 
	   sys.tables p 
		  ON p.OBJECT_ID = fc.parent_object_id
	INNER JOIN sys.schemas AS s 
		ON p.[schema_id] = s.[schema_id]
	WHERE 
	   OBJECT_NAME (f.referenced_object_id) = 'Channel'

   -- drop channel fk contraints
	DECLARE @MyCursor CURSOR, @script nvarchar(max)
	SET @MyCursor = CURSOR FOR select script from #DropTempTable      
	OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @script

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC (@script);
			FETCH NEXT FROM @MyCursor INTO @script  
		END
		
	CLOSE @MyCursor 
	DEALLOCATE @MyCursor;

	--Create tempchannel table
	CREATE TABLE dbo.Tmp_Channel 
   ( 
	[Id] [int] NOT NULL,
	[SystemId] [uniqueidentifier] NOT NULL,
	[IsActive] [bit] NOT NULL  ,-- DEFAULT ((0)),
	[ActivatedOn] [datetime] NULL,
	[DeactivatedOn] [datetime] NULL,
	[IsDefault] [bit] NOT NULL  , -- DEFAULT ((0)),
	[IsDeleted] [bit] NULL   , --DEFAULT ((0)),
	[CurrencyId] [int] NULL,
	[DateFormat] [nvarchar](255) NULL,
	[Name] [varchar](100) NULL,
	[LanguageId] [int] NULL,
	[PasswordStrengthEnabled] [bit] NULL,
	[Code] [nvarchar](255) NULL,
   )  ON [PRIMARY] 
	
	--insert channel data to tempchannel
	IF EXISTS(SELECT * FROM dbo.Channel) 
		EXEC('INSERT INTO dbo.Tmp_Channel ([Id],SystemId,[IsActive],[ActivatedOn],[DeactivatedOn],[IsDefault],[IsDeleted],[CurrencyId],[DateFormat],[Name],[LanguageId],[PasswordStrengthEnabled],[Code]) 
		  SELECT [Id],[SystemId],[IsActive],[ActivatedOn],[DeactivatedOn],[IsDefault],[IsDeleted],[CurrencyId],[DateFormat],[Name],[LanguageId],[PasswordStrengthEnabled],[Code] FROM dbo.Channel WITH (HOLDLOCK TABLOCKX)') 

	-- drop channel constraints
	if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[DF_Channel_IsDeleted]') AND parent_object_id = OBJECT_ID('Channel'))
	ALTER TABLE [dbo].[Channel] DROP CONSTRAINT [DF_Channel_IsDeleted];

	if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[DF_Settings_IsActive]') AND parent_object_id = OBJECT_ID('Channel'))
	ALTER TABLE [dbo].[Channel] DROP CONSTRAINT [DF_Settings_IsActive];

	if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[DF_Settings_IsDefault]') AND parent_object_id = OBJECT_ID('Channel'))
	ALTER TABLE [dbo].[Channel] DROP CONSTRAINT [DF_Settings_IsDefault];


	if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[UC_Channel]') AND parent_object_id = OBJECT_ID('Channel'))
	ALTER TABLE [dbo].[Channel] DROP CONSTRAINT [UC_Channel];

	if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[PK_Settings]') AND parent_object_id = OBJECT_ID('Channel'))
	ALTER TABLE [dbo].[Channel] DROP CONSTRAINT [PK_Settings];

	--drop old channel table 
	DROP TABLE dbo.Channel;

	--rename temp channel table to channel
	EXECUTE sp_rename N'dbo.Tmp_Channel', N'Channel';

	--add primary key constraint
	ALTER TABLE [dbo].[Channel] ADD  CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	ALTER TABLE [dbo].[Channel] ADD  CONSTRAINT [UC_Channel] UNIQUE NONCLUSTERED 
	(
		[SystemId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	-- add channel constraints
	ALTER TABLE [dbo].[Channel] ADD  CONSTRAINT [DF_Channel_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
	ALTER TABLE [dbo].[Channel] ADD  CONSTRAINT [DF_Settings_IsActive]  DEFAULT ((0)) FOR [IsActive]
	ALTER TABLE [dbo].[Channel] ADD  CONSTRAINT [DF_Settings_IsDefault]  DEFAULT ((0)) FOR [IsDefault]

	-- create channel fk constraints
	SET @MyCursor = CURSOR FOR select script from #CreateTempTable      
	OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @script

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC(@script);
			FETCH NEXT FROM @MyCursor INTO @script 
		END
	CLOSE @MyCursor 
	DEALLOCATE @MyCursor;

	-- Drop temp tables
	drop table #DropTempTable;
	drop table #CreateTempTable;

	COMMIT TRANSACTION;