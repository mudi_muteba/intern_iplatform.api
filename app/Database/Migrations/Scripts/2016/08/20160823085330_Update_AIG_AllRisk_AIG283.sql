﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='VIRGIN')

DECLARE @CoverID INT
SET @CoverID=(select ID from md.Cover where name ='All Risk')

DECLARE @CoverDefid INT
SET @CoverDefid=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId=@CoverID)

UPDATE dbo.QuestionDefinition SET DisplayName=N'Is this item kept in a locked safe', ToolTip=N'Is this item kept in a locked safe' WHERE QuestionId=492 AND CoverDefinitionId=@CoverDefid
 
