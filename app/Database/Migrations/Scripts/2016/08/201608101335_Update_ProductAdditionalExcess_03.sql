﻿if not exists (select * from sys.all_columns c join sys.tables t on t.object_id = c.object_id join sys.schemas s on s.schema_id = t.schema_id where t.name = 'ProductAdditionalExcess' and c.name = 'ShouldDisplayExcessValues')
	begin
		alter table [ProductAdditionalExcess] add ShouldDisplayExcessValues bit default 1
	end
go

if not exists (select * from sys.all_columns c join sys.tables t on t.object_id = c.object_id join sys.schemas s on s.schema_id = t.schema_id where t.name = 'ProductAdditionalExcess' and c.name = 'ShouldApplySelectedExcess')
	begin
		alter table [ProductAdditionalExcess] add ShouldApplySelectedExcess bit default 0
	end
go

if not exists (select * from sys.all_columns c join sys.tables t on t.object_id = c.object_id join sys.schemas s on s.schema_id = t.schema_id join sys.default_constraints d on c.default_object_id = d.object_id where t.name = 'ProductAdditionalExcess' and c.name = 'ShouldDisplayExcessValues')
	begin
		alter table [ProductAdditionalExcess] add constraint df_ProductAdditionalExcess_ShouldDisplayExcessValues default 1 for ShouldDisplayExcessValues
	end
go

if not exists (select * from sys.all_columns c join sys.tables t on t.object_id = c.object_id join sys.schemas s on s.schema_id = t.schema_id join sys.default_constraints d on c.default_object_id = d.object_id where t.name = 'ProductAdditionalExcess' and c.name = 'ShouldApplySelectedExcess')
	begin		
		alter table [ProductAdditionalExcess] add constraint df_ProductAdditionalExcess_ShouldApplySelectedExcess default 0 for ShouldApplySelectedExcess
	end
go

alter table [ProductAdditionalExcess] alter column [Category] nvarchar(4000)
go

alter table [ProductAdditionalExcess] alter column [Description] nvarchar(4000)
go

delete from ProductAdditionalExcess
dbcc checkident ('ProductAdditionalExcess', reseed, 0)
go

--KingPrice (Motor), Product Codes: 'KPIPERS', 'KPIPERS2'
declare
	@KingPrice_KPIPERS_Motor_CoverDefinitionId int,
	@KingPrice_KPIPERS2_Motor_CoverDefinitionId int

select
	@KingPrice_KPIPERS_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS') and
	Cover.Name in ('Motor')

select
	@KingPrice_KPIPERS2_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS2') and
	Cover.Name in ('Motor')

if	isnull(@KingPrice_KPIPERS_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 0, 'General', 'Basic Excess', 4500, 1500, 27500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 0, 'General', 'Basic Excess', 4500, 1500, 27500, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 1, 'General', 'Specified Ratio', 750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 1, 'General', 'Specified Ratio', 750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 2, 'General', 'Window Replacement (Excl. Panoramic Glass)', 750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 2, 'General', 'Window Replacement (Excl. Panoramic Glass)', 750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 3, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'Is younger than 25 years OR has had their driver’s licence for less than 2 years', 4500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 3, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'Is younger than 25 years OR has had their driver’s licence for less than 2 years', 4500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 4, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'If the incident occurred outside of SA and the car is not drivable', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 4, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'If the incident occurred outside of SA and the car is not drivable', 7500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 5, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'When the incident occurs in the first 3 months of cover', 3500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 5, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'When the incident occurs in the first 3 months of cover', 3500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 6, 'Trailer & Caravans', 'Caravan or caravan contents', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 6, 'Trailer & Caravans', 'Caravan or caravan contents', 2500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 7, 'Trailer & Caravans', 'Caravan or trailer contents', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 7, 'Trailer & Caravans', 'Caravan or trailer contents', 2500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 8, 'Trailer & Caravans', 'Liability to other parties', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 8, 'Trailer & Caravans', 'Liability to other parties', 0, 0, 0, 0, 0, 0, 0, 0)
	end


--KingPrice (Building), Product Codes: 'KPIPERS', 'KPIPERS2'
declare
	@KingPrice_KPIPERS_Building_CoverDefinitionId int,
	@KingPrice_KPIPERS2_Building_CoverDefinitionId int
	
select
	@KingPrice_KPIPERS_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS') and
	Cover.Name in ('Building')

select
	@KingPrice_KPIPERS2_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS2') and
	Cover.Name in ('Building')

if	isnull(@KingPrice_KPIPERS_Building_CoverDefinitionId, 0) > 0 and
	isnull(@KingPrice_KPIPERS2_Building_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Building_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Building_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)
	end

--KingPrice (Contents), Product Codes: 'KPIPERS', 'KPIPERS2'
declare
	@KingPrice_KPIPERS_Contents_CoverDefinitionId int,
	@KingPrice_KPIPERS2_Contents_CoverDefinitionId int
	
select
	@KingPrice_KPIPERS_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS') and
	Cover.Name in ('Contents')

select
	@KingPrice_KPIPERS2_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS2') and
	Cover.Name in ('Contents')

if	isnull(@KingPrice_KPIPERS_Contents_CoverDefinitionId, 0) > 0 and
	isnull(@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 1, 'Additional Cover Excess', 'Food that has deteriorated', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 1, 'Additional Cover Excess', 'Food that has deteriorated', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 2, 'Additional Cover Excess', 'Washing stolen from the line', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 2, 'Additional Cover Excess', 'Washing stolen from the line', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 3, 'Additional Cover Excess', 'Guests belongings', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 3, 'Additional Cover Excess', 'Guests belongings', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 4, 'Additional Cover Excess', 'Locks & keys', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 4, 'Additional Cover Excess', 'Locks & keys', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 5, 'Additional Cover Excess', 'Hole in one in golf/full house in bowling', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 5, 'Additional Cover Excess', 'Hole in one in golf/full house in bowling', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 6, 'Additional Cover Excess', 'Domestic employee’s belongings', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 6, 'Additional Cover Excess', 'Domestic employee’s belongings', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 7, 'Additional Cover Excess', 'Rent to live elsewhere', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 7, 'Additional Cover Excess', 'Rent to live elsewhere', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 8, 'Additional Cover Excess', 'Garden & leisure equipment', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 8, 'Additional Cover Excess', 'Garden & leisure equipment', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 9, 'Additional Cover Excess', 'Veterinary expense', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 9, 'Additional Cover Excess', 'Veterinary expense', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 10, 'Portable Posessions', 'Unspecified items (per item)', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 10, 'Portable Posessions', 'Specified items', 500, 0, 0, 5, 0, 0, 0, 0)
	end


--SA Underwriters (Motor), Product Codes: 'CENTND', 'CENTRD', 'SNTMND', 'SNTMRD'
declare
	@SAUnderwriters_CENTND_Motor_CoverDefinitionId int,
	@SAUnderwriters_CENTRD_Motor_CoverDefinitionId int,
	@SAUnderwriters_SNTMND_Motor_CoverDefinitionId int,
	@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId int
	
select
	@SAUnderwriters_CENTND_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('CENTND') and
	Cover.Name in ('Motor')

select
	@SAUnderwriters_CENTRD_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('CENTRD') and
	Cover.Name in ('Motor')

select
	@SAUnderwriters_SNTMND_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('SNTMND') and
	Cover.Name in ('Motor')

select
	@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('SNTMRD') and
	Cover.Name in ('Motor')

if	isnull(@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
	end


--Telesure (Motor), Product Codes: 'AUGPRD', 'FIR', 'UNI' (Auto & General, First For Women, Unity)
declare
	@Telesure_AUGPRD_Motor_CoverDefinitionId int,
	@Telesure_FIR_Motor_CoverDefinitionId int,
	@Telesure_UNI_Motor_CoverDefinitionId int
	
select
	@Telesure_AUGPRD_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AUGPRD') and
	Cover.Name in ('Motor')

select
	@Telesure_FIR_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('FIR') and
	Cover.Name in ('Motor')

select
	@Telesure_UNI_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('UNI') and
	Cover.Name in ('Motor')

if	isnull(@Telesure_AUGPRD_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_FIR_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_UNI_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 0, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In Low Risk Suburbs', 'Normal Use', 3200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 0, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In Low Risk Suburbs', 'Normal Use', 3200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 0, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In Low Risk Suburbs', 'Normal Use', 3200, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 1, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In Low Risk Suburbs', 'Business Use', 4050, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 1, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In Low Risk Suburbs', 'Business Use', 4050, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 1, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In Low Risk Suburbs', 'Business Use', 4050, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 2, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In Low Risk Suburbs', 'Retired', 3200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 2, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In Low Risk Suburbs', 'Retired', 3200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 2, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In Low Risk Suburbs', 'Retired', 3200, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 3, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In High Risk Suburbs', 'Normal Use', 4050, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 3, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In High Risk Suburbs', 'Normal Use', 4050, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 3, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In High Risk Suburbs', 'Normal Use', 4050, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 4, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In High Risk Suburbs', 'Business Use', 4850, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 4, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In High Risk Suburbs', 'Business Use', 4850, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 4, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In High Risk Suburbs', 'Business Use', 4850, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 5, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In High Risk Suburbs', 'Retired', 3200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 5, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In High Risk Suburbs', 'Retired', 3200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 5, 'Accident (AF) - CAT 1 to 24: Value Up To R32,000 In High Risk Suburbs', 'Retired', 3200, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 6, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In Low Risk Suburbs', 'Normal Use', 4150, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 6, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In Low Risk Suburbs', 'Normal Use', 4150, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 6, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In Low Risk Suburbs', 'Normal Use', 4150, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 7, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In Low Risk Suburbs', 'Business Use', 4950, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 7, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In Low Risk Suburbs', 'Business Use', 4950, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 7, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In Low Risk Suburbs', 'Business Use', 4950, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 8, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In Low Risk Suburbs', 'Retired', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 8, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In Low Risk Suburbs', 'Retired', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 8, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In Low Risk Suburbs', 'Retired', 3300, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 9, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In High Risk Suburbs', 'Normal Use', 5000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 9, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In High Risk Suburbs', 'Normal Use', 5000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 9, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In High Risk Suburbs', 'Normal Use', 5000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 10, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In High Risk Suburbs', 'Business Use', 5800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 10, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In High Risk Suburbs', 'Business Use', 5800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 10, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In High Risk Suburbs', 'Business Use', 5800, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 11, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In High Risk Suburbs', 'Retired', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 11, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In High Risk Suburbs', 'Retired', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 11, 'Accident (AF) - CAT 25 to 67: Value of R32,001 to R95,500 In High Risk Suburbs', 'Retired', 3300, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 12, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In Low Risk Suburbs', 'Normal Use', 4800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 12, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In Low Risk Suburbs', 'Normal Use', 4800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 12, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In Low Risk Suburbs', 'Normal Use', 4800, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 13, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In Low Risk Suburbs', 'Business Use', 5600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 13, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In Low Risk Suburbs', 'Business Use', 5600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 13, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In Low Risk Suburbs', 'Business Use', 5600, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 14, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In Low Risk Suburbs', 'Retired', 3350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 14, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In Low Risk Suburbs', 'Retired', 3350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 14, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In Low Risk Suburbs', 'Retired', 3350, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 15, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In High Risk Suburbs', 'Normal Use', 5000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 15, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In High Risk Suburbs', 'Normal Use', 5000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 15, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In High Risk Suburbs', 'Normal Use', 5000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 16, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In High Risk Suburbs', 'Business Use', 5800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 16, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In High Risk Suburbs', 'Business Use', 5800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 16, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In High Risk Suburbs', 'Business Use', 5800, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 17, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In High Risk Suburbs', 'Retired', 3350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 17, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In High Risk Suburbs', 'Retired', 3350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 17, 'Accident (AF) - CAT 68 to 94: Value of R95 501 to R136 000 In High Risk Suburbs', 'Retired', 3350, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 18, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In Low Risk Suburbs', 'Normal Use', 5000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 18, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In Low Risk Suburbs', 'Normal Use', 5000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 18, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In Low Risk Suburbs', 'Normal Use', 5000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 19, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In Low Risk Suburbs', 'Business Use', 5600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 19, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In Low Risk Suburbs', 'Business Use', 5600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 19, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In Low Risk Suburbs', 'Business Use', 5600, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 20, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In Low Risk Suburbs', 'Retired', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 20, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In Low Risk Suburbs', 'Retired', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 20, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In Low Risk Suburbs', 'Retired', 4000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 21, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In High Risk Suburbs', 'Normal Use', 5600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 21, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In High Risk Suburbs', 'Normal Use', 5600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 21, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In High Risk Suburbs', 'Normal Use', 5600, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 22, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In High Risk Suburbs', 'Business Use', 5800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 22, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In High Risk Suburbs', 'Business Use', 5800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 22, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In High Risk Suburbs', 'Business Use', 5800, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 23, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In High Risk Suburbs', 'Retired', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 23, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In High Risk Suburbs', 'Retired', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 23, 'Accident (AF) - CAT 95 to 178: Value of R136 001 to R299 500 In High Risk Suburbs', 'Retired', 4000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 24, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In Low Risk Suburbs', 'Normal Use', 5800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 24, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In Low Risk Suburbs', 'Normal Use', 5800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 24, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In Low Risk Suburbs', 'Normal Use', 5800, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 25, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In Low Risk Suburbs', 'Business Use', 6250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 25, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In Low Risk Suburbs', 'Business Use', 6250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 25, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In Low Risk Suburbs', 'Business Use', 6250, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 26, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In Low Risk Suburbs', 'Retired', 4700, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 26, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In Low Risk Suburbs', 'Retired', 4700, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 26, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In Low Risk Suburbs', 'Retired', 4700, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 27, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In High Risk Suburbs', 'Normal Use', 6250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 27, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In High Risk Suburbs', 'Normal Use', 6250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 27, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In High Risk Suburbs', 'Normal Use', 6250, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 28, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In High Risk Suburbs', 'Business Use', 6250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 28, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In High Risk Suburbs', 'Business Use', 6250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 28, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In High Risk Suburbs', 'Business Use', 6250, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 29, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In High Risk Suburbs', 'Retired', 4700, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 29, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In High Risk Suburbs', 'Retired', 4700, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 29, 'Accident (AF) - CAT 179 to 198: Value of R299 501 to R450 000 In High Risk Suburbs', 'Retired', 4700, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 30, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In Low Risk Suburbs', 'Normal Use', 8050, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 30, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In Low Risk Suburbs', 'Normal Use', 8050, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 30, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In Low Risk Suburbs', 'Normal Use', 8050, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 31, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In Low Risk Suburbs', 'Business Use', 8650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 31, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In Low Risk Suburbs', 'Business Use', 8650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 31, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In Low Risk Suburbs', 'Business Use', 8650, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 32, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In Low Risk Suburbs', 'Retired', 6350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 32, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In Low Risk Suburbs', 'Retired', 6350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 32, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In Low Risk Suburbs', 'Retired', 6350, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 33, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In High Risk Suburbs', 'Normal Use', 8700, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 33, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In High Risk Suburbs', 'Normal Use', 8700, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 33, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In High Risk Suburbs', 'Normal Use', 8700, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 34, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In High Risk Suburbs', 'Business Use', 8700, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 34, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In High Risk Suburbs', 'Business Use', 8700, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 34, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In High Risk Suburbs', 'Business Use', 8700, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 35, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In High Risk Suburbs', 'Retired', 6500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 35, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In High Risk Suburbs', 'Retired', 6500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 35, 'Accident (AF) - CAT 199 to 218: Value of R450 001 to R650 000 In High Risk Suburbs', 'Retired', 6500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 36, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In Low Risk Suburbs', 'Normal Use', 10350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 36, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In Low Risk Suburbs', 'Normal Use', 10350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 36, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In Low Risk Suburbs', 'Normal Use', 10350, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 37, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In Low Risk Suburbs', 'Business Use', 11050, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 37, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In Low Risk Suburbs', 'Business Use', 11050, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 37, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In Low Risk Suburbs', 'Business Use', 11050, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 38, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In Low Risk Suburbs', 'Retired', 8250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 38, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In Low Risk Suburbs', 'Retired', 8250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 38, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In Low Risk Suburbs', 'Retired', 8250, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 39, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In High Risk Suburbs', 'Normal Use', 11250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 39, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In High Risk Suburbs', 'Normal Use', 11250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 39, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In High Risk Suburbs', 'Normal Use', 11250, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 40, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In High Risk Suburbs', 'Business Use', 11250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 40, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In High Risk Suburbs', 'Business Use', 11250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 40, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In High Risk Suburbs', 'Business Use', 11250, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 41, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In High Risk Suburbs', 'Retired', 8350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 41, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In High Risk Suburbs', 'Retired', 8350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 41, 'Accident (AF) - CAT 219 to 238: Value of R650 001 to R850 000 In High Risk Suburbs', 'Retired', 8350, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 42, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In Low Risk Suburbs', 'Normal Use', 12450, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 42, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In Low Risk Suburbs', 'Normal Use', 12450, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 42, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In Low Risk Suburbs', 'Normal Use', 12450, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 43, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In Low Risk Suburbs', 'Business Use', 13350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 43, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In Low Risk Suburbs', 'Business Use', 13350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 43, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In Low Risk Suburbs', 'Business Use', 13350, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 44, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In Low Risk Suburbs', 'Retired', 9950, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 44, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In Low Risk Suburbs', 'Retired', 9950, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 44, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In Low Risk Suburbs', 'Retired', 9950, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 45, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In High Risk Suburbs', 'Normal Use', 13500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 45, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In High Risk Suburbs', 'Normal Use', 13500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 45, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In High Risk Suburbs', 'Normal Use', 13500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 46, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In High Risk Suburbs', 'Business Use', 13500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 46, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In High Risk Suburbs', 'Business Use', 13500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 46, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In High Risk Suburbs', 'Business Use', 13500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 47, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In High Risk Suburbs', 'Retired', 10050, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 47, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In High Risk Suburbs', 'Retired', 10050, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 47, 'Accident (AF) - CAT 239 to 258: Value of R850 001 to R1 050 000 In High Risk Suburbs', 'Retired', 10050, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 48, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In Low Risk Suburbs', 'Normal Use', 13750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 48, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In Low Risk Suburbs', 'Normal Use', 13750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 48, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In Low Risk Suburbs', 'Normal Use', 13750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 49, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In Low Risk Suburbs', 'Business Use', 14750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 49, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In Low Risk Suburbs', 'Business Use', 14750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 49, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In Low Risk Suburbs', 'Business Use', 14750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 50, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In Low Risk Suburbs', 'Retired', 11000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 50, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In Low Risk Suburbs', 'Retired', 11000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 50, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In Low Risk Suburbs', 'Retired', 11000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 51, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In High Risk Suburbs', 'Normal Use', 15000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 51, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In High Risk Suburbs', 'Normal Use', 15000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 51, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In High Risk Suburbs', 'Normal Use', 15000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 52, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In High Risk Suburbs', 'Business Use', 15000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 52, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In High Risk Suburbs', 'Business Use', 15000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 52, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In High Risk Suburbs', 'Business Use', 15000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 53, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In High Risk Suburbs', 'Retired', 11250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 53, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In High Risk Suburbs', 'Retired', 11250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 53, 'Accident (AF) - CAT 259 to 278: Value of R1 050 001 to R1 250 000 In High Risk Suburbs', 'Retired', 11250, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 54, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In Low Risk Suburbs', 'Normal Use', 15750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 54, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In Low Risk Suburbs', 'Normal Use', 15750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 54, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In Low Risk Suburbs', 'Normal Use', 15750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 55, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In Low Risk Suburbs', 'Business Use', 16750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 55, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In Low Risk Suburbs', 'Business Use', 16750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 55, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In Low Risk Suburbs', 'Business Use', 16750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 56, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In Low Risk Suburbs', 'Retired', 12500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 56, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In Low Risk Suburbs', 'Retired', 12500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 56, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In Low Risk Suburbs', 'Retired', 12500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 57, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In High Risk Suburbs', 'Normal Use', 17000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 57, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In High Risk Suburbs', 'Normal Use', 17000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 57, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In High Risk Suburbs', 'Normal Use', 17000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 58, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In High Risk Suburbs', 'Business Use', 17000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 58, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In High Risk Suburbs', 'Business Use', 17000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 58, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In High Risk Suburbs', 'Business Use', 17000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 59, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In High Risk Suburbs', 'Retired', 12750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 59, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In High Risk Suburbs', 'Retired', 12750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 59, 'Accident (AF) - CAT 279 to 298: Value of R1 250 001 to R1 450 000 In High Risk Suburbs', 'Retired', 12750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 60, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In Low Risk Suburbs', 'Normal Use', 17000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 60, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In Low Risk Suburbs', 'Normal Use', 17000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 60, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In Low Risk Suburbs', 'Normal Use', 17000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 61, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In Low Risk Suburbs', 'Business Use', 18000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 61, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In Low Risk Suburbs', 'Business Use', 18000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 61, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In Low Risk Suburbs', 'Business Use', 18000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 62, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In Low Risk Suburbs', 'Retired', 13250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 62, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In Low Risk Suburbs', 'Retired', 13250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 62, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In Low Risk Suburbs', 'Retired', 13250, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 63, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In High Risk Suburbs', 'Normal Use', 18250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 63, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In High Risk Suburbs', 'Normal Use', 18250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 63, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In High Risk Suburbs', 'Normal Use', 18250, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 64, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In High Risk Suburbs', 'Business Use', 18250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 64, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In High Risk Suburbs', 'Business Use', 18250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 64, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In High Risk Suburbs', 'Business Use', 18250, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 65, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In High Risk Suburbs', 'Retired', 13500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 65, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In High Risk Suburbs', 'Retired', 13500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 65, 'Accident (AF) - CAT 299 to 318: Value of R1 450 001 to R1 650 000 In High Risk Suburbs', 'Retired', 13500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 66, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In Low Risk Suburbs', 'Normal Use', 18500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 66, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In Low Risk Suburbs', 'Normal Use', 18500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 66, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In Low Risk Suburbs', 'Normal Use', 18500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 67, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In Low Risk Suburbs', 'Business Use', 19500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 67, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In Low Risk Suburbs', 'Business Use', 19500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 67, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In Low Risk Suburbs', 'Business Use', 19500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 68, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In Low Risk Suburbs', 'Retired', 14500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 68, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In Low Risk Suburbs', 'Retired', 14500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 68, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In Low Risk Suburbs', 'Retired', 14500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 69, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In High Risk Suburbs', 'Normal Use', 20000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 69, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In High Risk Suburbs', 'Normal Use', 20000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 69, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In High Risk Suburbs', 'Normal Use', 20000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 70, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In High Risk Suburbs', 'Business Use', 20000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 70, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In High Risk Suburbs', 'Business Use', 20000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 70, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In High Risk Suburbs', 'Business Use', 20000, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 71, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In High Risk Suburbs', 'Retired', 15000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 71, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In High Risk Suburbs', 'Retired', 15000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 71, 'Accident (AF) - CAT 319 to 320: Value of R1 650 001 And Above In High Risk Suburbs', 'Retired', 15000, 0, 0, 0, 0, 0, 0, 0)
		
		
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 72, 'Theft (AF)', 'Normal Use', 0, 3500, 0, 7.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 72, 'Theft (AF)', 'Normal Use', 0, 3500, 0, 7.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 72, 'Theft (AF)', 'Normal Use', 0, 3500, 0, 7.5, 0, 0, 1, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 73, 'Theft (AF)', 'Business Use', 0, 4000, 0, 7.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 73, 'Theft (AF)', 'Business Use', 0, 4000, 0, 7.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 73, 'Theft (AF)', 'Business Use', 0, 4000, 0, 7.5, 0, 0, 1, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 74, 'Theft (AF)', 'Retired', 0, 2950, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 74, 'Theft (AF)', 'Retired', 0, 2950, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 74, 'Theft (AF)', 'Retired', 0, 2950, 0, 5, 0, 0, 1, 0)



		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 75, 'Windscreen Damage', 'Repairs', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 75, 'Windscreen Damage', 'Repairs', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 75, 'Windscreen Damage', 'Repairs', 0, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 75, 'Windscreen Damage', 'Replacement', 800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 75, 'Windscreen Damage', 'Replacement', 800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 75, 'Windscreen Damage', 'Replacement', 800, 0, 0, 0, 0, 0, 0, 0)



		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 76, 'Additional Excess', 'Outside South African Borders (XK, MTx)', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 76, 'Additional Excess', 'Outside South African Borders (XK, MTx)', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 76, 'Additional Excess', 'Outside South African Borders (XK, MTx)', 7500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 77, 'Additional Excess', 'Regular driver under 25 (TRATM)', 2200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 77, 'Additional Excess', 'Regular driver under 25 (TRATM)', 2200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 77, 'Additional Excess', 'Regular driver under 25 (TRATM)', 2200, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 78, 'Additional Excess', 'Not Regular Driver under 25 (XA, MT)', 5400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 78, 'Additional Excess', 'Not Regular Driver under 25 (XA, MT)', 5400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 78, 'Additional Excess', 'Not Regular Driver under 25 (XA, MT)', 5400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 79, 'Additional Excess', 'Not Regular Driver over 25 (XN, MT)', 3600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 79, 'Additional Excess', 'Not Regular Driver over 25 (XN, MT)', 3600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 79, 'Additional Excess', 'Not Regular Driver over 25 (XN, MT)', 3600, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 80, 'Additional Excess', 'Learners License (XL,MT)', 2550, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 80, 'Additional Excess', 'Learners License (XL,MT)', 2550, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 80, 'Additional Excess', 'Learners License (XL,MT)', 2550, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 81, 'Additional Excess', 'License less that 2 years (XO, MT)', 2550, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 81, 'Additional Excess', 'License less that 2 years (XO, MT)', 2550, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 81, 'Additional Excess', 'License less that 2 years (XO, MT)', 2550, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 82, 'Additional Excess', 'Policy Age less than 6 months', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 82, 'Additional Excess', 'Policy Age less than 6 months', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 82, 'Additional Excess', 'Policy Age less than 6 months', 2500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 83, 'Additional Excess', 'Policy Age > 6 months <= 12 months', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 83, 'Additional Excess', 'Policy Age > 6 months <= 12 months', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 83, 'Additional Excess', 'Policy Age > 6 months <= 12 months', 0, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 84, 'Additional Excess', 'Night time excess', 2200, 0, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 85, 'Third Party (Fire & Theft)', 'Basic Excess', 1700, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 85, 'Third Party (Fire & Theft)', 'Basic Excess', 1700, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 85, 'Third Party (Fire & Theft)', 'Basic Excess', 1700, 0, 0, 0, 0, 0, 0, 0)

		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 86, 'Third Party (Fire & Theft)', 'Windscreen Excess (If Option Taken) (XW, MT)', 800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 86, 'Third Party (Fire & Theft)', 'Windscreen Excess (If Option Taken) (XW, MT)', 800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 86, 'Third Party (Fire & Theft)', 'Windscreen Excess (If Option Taken) (XW, MT)', 800, 0, 0, 0, 0, 0, 0, 0)

		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 87, 'Third Party (Fire & Theft) - Additional Excess', 'Not Regular Driver (XM, MTB)', 3600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 87, 'Third Party (Fire & Theft) - Additional Excess', 'Not Regular Driver (XM, MTB)', 3600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 87, 'Third Party (Fire & Theft) - Additional Excess', 'Not Regular Driver (XM, MTB)', 3600, 0, 0, 0, 0, 0, 0, 0)

		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 88, 'Third Party Only', 'BASIC EXCESS (XB, MTC)', 1700, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 88, 'Third Party Only', 'BASIC EXCESS (XB, MTC)', 1700, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 88, 'Third Party Only', 'BASIC EXCESS (XB, MTC)', 1700, 0, 0, 0, 0, 0, 0, 0)

		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 89, 'Third Party Only - Additional Excess', 'Not Regular Driver (XM, MTC)', 3600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 89, 'Third Party Only - Additional Excess', 'Not Regular Driver (XM, MTC)', 3600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 89, 'Third Party Only - Additional Excess', 'Not Regular Driver (XM, MTC)', 3600, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 90, 'Motorbikes - Basic Excess (XF)', 'Up to 125cc', 0, 2500, 0, 5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 90, 'Motorbikes - Basic Excess (XF)', 'Up to 125cc', 0, 2500, 0, 5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 90, 'Motorbikes - Basic Excess (XF)', 'Up to 125cc', 0, 2500, 0, 5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 91, 'Motorbikes - Additional Excess', 'Learners License (XL, MB)', 2650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 91, 'Motorbikes - Additional Excess', 'Learners License (XL, MB)', 2650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 91, 'Motorbikes - Additional Excess', 'Learners License (XL, MB)', 2650, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 92, 'Motorbikes - Additional Excess', 'License less that 2 years (XO, MB)', 2650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 92, 'Motorbikes - Additional Excess', 'License less that 2 years (XO, MB)', 2650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 92, 'Motorbikes - Additional Excess', 'License less that 2 years (XO, MB)', 2650, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 93, 'Motorbikes - Additional Excess', 'Not Regular Rider (XN, MB)', 3750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 93, 'Motorbikes - Additional Excess', 'Not Regular Rider (XN, MB)', 3750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 93, 'Motorbikes - Additional Excess', 'Not Regular Rider (XN, MB)', 3750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 94, 'Motorbikes - Additional Excess', 'Rider under 25 and Engine > 125cc (XD, MB)', 2300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 94, 'Motorbikes - Additional Excess', 'Rider under 25 and Engine > 125cc (XD, MB)', 2300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 94, 'Motorbikes - Additional Excess', 'Rider under 25 and Engine > 125cc (XD, MB)', 2300, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 95, 'Motorbikes - Additional Excess', 'Outside South African Borders (XK, BMx)', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 95, 'Motorbikes - Additional Excess', 'Outside South African Borders (XK, BMx)', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 95, 'Motorbikes - Additional Excess', 'Outside South African Borders (XK, BMx)', 7500, 0, 0, 0, 0, 0, 0, 0)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 96, 'Caravan - Basic Excess', 'Caravan (XH, CV)', 0, 2700, 0, 5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 96, 'Caravan - Basic Excess', 'Caravan (XH, CV)', 0, 2700, 0, 5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 96, 'Caravan - Basic Excess', 'Caravan (XH, CV)', 0, 2700, 0, 5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 97, 'Caravan - Basic Excess', 'Contents (XC, CV)', 0, 1750, 0, 5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 97, 'Caravan - Basic Excess', 'Contents (XC, CV)', 0, 1750, 0, 5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 97, 'Caravan - Basic Excess', 'Contents (XC, CV)', 0, 1750, 0, 5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 98, 'Caravan - Basic Excess', 'Outside South African Borders (XK, CVx)', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 98, 'Caravan - Basic Excess', 'Outside South African Borders (XK, CVx)', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 98, 'Caravan - Basic Excess', 'Outside South African Borders (XK, CVx)', 7500, 0, 0, 0, 0, 0, 0, 0)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 99, 'Trailers', 'Basic Excess', 0, 950, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 99, 'Trailers', 'Basic Excess', 0, 950, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 99, 'Trailers', 'Basic Excess', 0, 950, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 100, 'Trailers', 'Outside South African Borders (XK, TRx)', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 100, 'Trailers', 'Outside South African Borders (XK, TRx)', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 100, 'Trailers', 'Outside South African Borders (XK, TRx)', 7500, 0, 0, 0, 0, 0, 0, 0)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 101, 'Watercraft', 'Accident & Theft Excess (AF)', 0, 3000, 0, 5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 101, 'Watercraft', 'Accident & Theft Excess (AF)', 0, 3000, 0, 5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 101, 'Watercraft', 'Accident & Theft Excess (AF)', 0, 3000, 0, 5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 102, 'Watercraft', 'Outside South African Borders (XK, TRx)', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 102, 'Watercraft', 'Outside South African Borders (XK, TRx)', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 102, 'Watercraft', 'Outside South African Borders (XK, TRx)', 7500, 0, 0, 0, 0, 0, 0, 0)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 103, 'Vehicle Sound System', 'Basic Excess (XR, MT)', 990, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 103, 'Vehicle Sound System', 'Basic Excess (XR, MT)', 990, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 103, 'Vehicle Sound System', 'Basic Excess (XR, MT)', 990, 0, 0, 0, 0, 0, 0, 0)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 104, 'Golf Carts', 'Accident & Theft Excess (AF)', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 104, 'Golf Carts', 'Accident & Theft Excess (AF)', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 104, 'Golf Carts', 'Accident & Theft Excess (AF)', 0, 2500, 0, 10, 1, 0, 0, 0)
	end



--Oakhurst (Motor), Product Codes: 'OAKHURST', 'OAKHURST ASPIRE'
declare
	@Oakhurst_OAKHURST_Motor_CoverDefinitionId int,
	@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId int
	
select
	@Oakhurst_OAKHURST_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('OAKHURST') and
	Cover.Name in ('Motor')

select
	@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('OAKHURST ASPIRE') and
	Cover.Name in ('Motor')

if	isnull(@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 0, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R0 and the claim is below R5000', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 0, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R0 and the claim is below R5000', 0, 0, 0, 0, 0, 0, 0, 0, 1)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 0, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R0 and the claim is R5000 or more', 2500, 0, 20000, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 0, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R0 and the claim is R5000 or more', 2500, 0, 20000, 0, 0, 0, 0, 0, 1)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 0, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R2500 or more and the claim is R5000 or more', 2500, 0, 20000, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 0, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R2500 or more and the claim is R5000 or more', 2500, 0, 20000, 0, 0, 0, 0, 0, 1)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 0, 'Accessories', 'Windscreen and specified non-standard accessories', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 0, 'Accessories', 'Windscreen and specified non-standard accessories', 0, 500, 0, 20, 1, 0, 0, 0)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 0, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where no 3rd party vehicle is involved or if you claim in the 1st 6 months from the policy start date', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 0, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where no 3rd party vehicle is involved or if you claim in the 1st 6 months from the policy start date', 0, 0, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 0, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where the driver at the time of the loss is under 30 years old or the driver at the time of the loss has had his/her license for less than 2 years', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 0, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where the driver at the time of the loss is under 30 years old or the driver at the time of the loss has had his/her license for less than 2 years', 0, 0, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 0, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents occurring between 10pm and 4am or failure to report an incident to the South African Police Service within 24 hours of the incident occurring.', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 0, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents occurring between 10pm and 4am or failure to report an incident to the South African Police Service within 24 hours of the incident occurring.', 0, 0, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 0, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where we have not received your Vehicle Inspection Certificate or where your policy requires you to have a tracking device other than a Smart-Box fitted to your vehicle and it is not fitted or fully functional, with the exception of theft or hi-jack claims which will not be covered at all. After 3 months if the Smart-Box is required and not fitted. Replace above VIC or tracking device additional excess.)', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 0, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where we have not received your Vehicle Inspection Certificate or where your policy requires you to have a tracking device other than a Smart-Box fitted to your vehicle and it is not fitted or fully functional, with the exception of theft or hi-jack claims which will not be covered at all. After 3 months if the Smart-Box is required and not fitted. Replace above VIC or tracking device additional excess.)', 0, 0, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 0, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where we have not received your Vehicle Inspection Certificate or where your policy requires you to have an Oakhurst Smart-Box fitted to your vehicle and it is not fitted, with the exception of theft or hi-jack claims, in which event you will not be covered at all. After 3 months if the Smart-Box is required and not fitted. Replace above VIC or tracking device additional excess.)', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 0, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where we have not received your Vehicle Inspection Certificate or where your policy requires you to have an Oakhurst Smart-Box fitted to your vehicle and it is not fitted, with the exception of theft or hi-jack claims, in which event you will not be covered at all. After 3 months if the Smart-Box is required and not fitted. Replace above VIC or tracking device additional excess.)', 0, 0, 0, 0, 0, 0, 0, 0, 0)
	end