﻿begin tran

SET IDENTITY_INSERT [dbo].[Channel] ON
GO

declare @channelId int
set @channelId  = 5

IF EXISTS(select id from [dbo].[Channel] where id = @channelId)
UPDATE [dbo].[Channel] set Code = 'KP',Name = 'KingPrice',SystemId = 'CBFED7DA-CD47-4B24-8A48-D917C81B671C' where Id = @channelId
ELSE
INSERT INTO  Channel (Id, SystemId, IsActive, ActivatedOn, DeactivatedOn, IsDefault, IsDeleted, CurrencyId, DateFormat, Name,Code, LanguageId, PasswordStrengthEnabled) VALUES
           (@channelId,'CBFED7DA-CD47-4B24-8A48-D917C81B671C', 1, GETDATE(), null, 0,0,1,'dd MMM yyyy','KingPrice','KP', 2,0);

GO
declare @channelId int
set @channelId  = 6

IF EXISTS(select id from [dbo].[Channel] where id = @channelId)
UPDATE [dbo].[Channel] set Code = 'UAG',Name = 'Telesure',SystemId = '05C3A3FF-9299-41F8-AAFE-8EDD20E0319A' where Id = @channelId
ELSE
INSERT INTO  Channel (Id, SystemId, IsActive, ActivatedOn, DeactivatedOn, IsDefault, IsDeleted, CurrencyId, DateFormat, Name,Code, LanguageId, PasswordStrengthEnabled) VALUES
           (@channelId,'05C3A3FF-9299-41F8-AAFE-8EDD20E0319A', 1, GETDATE(), null, 0,0,1,'dd MMM yyyy','Telesure','UAG', 2,0);

SET IDENTITY_INSERT [dbo].[Channel] OFF
GO

commit 