﻿if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_IndividualUploadHeader_CampaignReference_CampaignReferenceId]') AND parent_object_id = OBJECT_ID('IndividualUploadHeader'))
alter table IndividualUploadHeader  drop constraint FK_IndividualUploadHeader_CampaignReference_CampaignReferenceId


if exists(select * from sys.columns where Name = N'CampaignReferenceId' and Object_ID = Object_ID(N'IndividualUploadHeader'))
begin
	alter table IndividualUploadHeader drop column CampaignReferenceId
end