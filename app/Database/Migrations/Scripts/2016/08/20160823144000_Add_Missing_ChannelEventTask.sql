﻿declare @ChannelEventId int,@MyCursor CURSOR;

SET @MyCursor = CURSOR FOR select Id FROM ChannelEvent WHERE IsDeleted = 0;     
OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @ChannelEventId

	WHILE @@FETCH_STATUS = 0
	BEGIN

		IF NOT EXISTS(SELECT Id FROM ChannelEventTask WHERE ChannelEventId = @ChannelEventId AND IsDeleted = 0)
		BEGIN
			INSERT ChannelEventTask (TaskName, ChannelEventId, IsDeleted) VALUES (2, @ChannelEventId, 0)
		END

		FETCH NEXT FROM @MyCursor INTO @ChannelEventId
	END
		
CLOSE @MyCursor 
DEALLOCATE @MyCursor;