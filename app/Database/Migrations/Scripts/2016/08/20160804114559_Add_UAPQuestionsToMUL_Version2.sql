﻿IF EXISTS(select id from [dbo].[Channel] where Code = 'UAP' AND IsDefault = 1)
	BEGIN

		declare @ProductId int
		select @ProductId = id from Product where ProductCode = 'MUL'

		--Motor
		declare @CoverDefinitionId int
		set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 218)

		if not exists(select * from QuestionDefinition where QuestionId=1006 and CoverDefinitionId = @CoverDefinitionId And IsDeleted = 0)
				begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Riot Strike And Civil Commotion',@CoverDefinitionId,1006,NULL,1,290,0,1,0,'Riot Strike And Civil Commotion',N'',N'',0,NULL,0)
				END

		if not exists(select * from QuestionDefinition where QuestionId=1148 and CoverDefinitionId = @CoverDefinitionId And IsDeleted = 0)
				begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Courtesy Car',@CoverDefinitionId,1148,NULL,1,260,0,1,0,'Courtesy Car',N'',N'',0,NULL,0)
				END

		if not exists(select * from QuestionDefinition where QuestionId=1151 and CoverDefinitionId = @CoverDefinitionId And IsDeleted = 0)
				begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Fatal PA',@CoverDefinitionId,1151,NULL,1,300,0,1,0,'Fatal PA',N'',N'',0,NULL,0)
				END

		if not exists(select * from QuestionDefinition where QuestionId=1153 and CoverDefinitionId = @CoverDefinitionId And IsDeleted = 0)
				begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'AA Rescue',@CoverDefinitionId,1153,NULL,1,320,0,1,0,'AA Rescue',N'',N'',0,NULL,0)
				END

		if not exists(select * from QuestionDefinition where QuestionId=24 and CoverDefinitionId = @CoverDefinitionId And IsDeleted = 0)
				begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Excess Protector',@CoverDefinitionId,24,NULL,1,330,0,1,0,'Excess Protector',N'',N'',0,NULL,0)
				END

		if not exists(select * from QuestionDefinition where QuestionId=1149 and CoverDefinitionId = @CoverDefinitionId And IsDeleted = 0)
				begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Forced ATM Withdrawal',@CoverDefinitionId,1149,NULL,1,270,0,1,0,'Forced ATM Withdrawal',N'',N'^[0-9\.]+$',0,NULL,0)
				END

		if not exists(select * from QuestionDefinition where QuestionId=1150 and CoverDefinitionId = @CoverDefinitionId And IsDeleted = 0)
				begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Loss Of Personal Effects',@CoverDefinitionId,1150,NULL,1,280,0,1,0,'Loss Of Personal Effects',N'',N'^[0-9\.]+$',0,NULL,0)
				END

		if not exists(select * from QuestionDefinition where QuestionId=1152 and CoverDefinitionId = @CoverDefinitionId And IsDeleted = 0)
				begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Out Of Station Accommodation',@CoverDefinitionId,1152,NULL,1,310,0,1,0,'Out Of Station Accommodation',N'',N'^[0-9\.]+$',0,NULL,0)
				END

	END
