﻿begin tran

SET IDENTITY_INSERT [dbo].[Channel] ON
GO

declare @channelId int
set @channelId  = 7

IF EXISTS(select id from [dbo].[Channel] where id = @channelId)
UPDATE [dbo].[Channel] set Code = 'IP',Name = 'IP',SystemId = '585C68BD-DFFA-496C-B9F4-A880C1D21926' where Id = @channelId
ELSE
INSERT INTO  Channel (Id, SystemId, IsActive, ActivatedOn, DeactivatedOn, IsDefault, IsDeleted, CurrencyId, DateFormat, Name,Code, LanguageId, PasswordStrengthEnabled) VALUES
           (@channelId,'585C68BD-DFFA-496C-B9F4-A880C1D21926', 1, GETDATE(), null, 0,0,1,'dd MMM yyyy','IP','IP', 2,0);

GO

commit 