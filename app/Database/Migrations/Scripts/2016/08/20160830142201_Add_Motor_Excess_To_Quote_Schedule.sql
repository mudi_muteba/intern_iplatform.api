﻿if exists (select * from sys.objects where object_id = object_id(N'Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_car') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_car
	end
go

Create procedure [dbo].[Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_car]
(
	@ProposalHeaderId int,
	@CoverId int,
	@AssetId int
)
as 
select top 1
				(select md.QuestionAnswer.Answer from md.QuestionAnswer
				 where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
				 ) [Description],
				'' Value,			
					(
						select top 1
							md.QuestionAnswer.Answer
						from ProposalHeader
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							inner join Product on Product.Id = ProposalDefinition.ProductId
							inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
							inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
							inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
							inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
							inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
							inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
							inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
						where
							ProposalHeader.Id = @ProposalHeaderId and
							Asset.Id = @AssetId and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							md.QuestionGroup.Name = 'Finance Information' and
							md.QuestionType.Name in ('DropDown') and
							md.Question.Name = 'AIG -Valuation Method'
					)
				 Actual
			from ProposalHeader
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId
				inner join Product on Product.Id = ProposalDefinition.ProductId
				inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
				inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
				inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
				inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
				inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
				inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
			where
				ProposalHeader.Id = 4099 and
				Asset.Id = 3627 and
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				--md.QuestionGroup.Name = 'Risk Information' and
				md.QuestionType.Name in ('DropDown') and
				md.Question.Name ='AIG - Motor -Type Of Vehicle'
			union all
			select
				QuestionDefinition.DisplayName [Description],
				case
					when md.QuestionType.Name = 'DropDown'
						then
							case
								when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
								then ''
							else
								'Included in premium' --'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
							end
						else
							case
								when ProposalQuestionAnswer.Answer = 'true'
									then 'Included in premium'
								when ProposalQuestionAnswer.Answer = 'false'
									then ''
								else 
									'' --ProposalQuestionAnswer.Answer
								end
				end Value,
				case
					when md.QuestionType.Name = 'DropDown'
						then
							case
								when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
								then 'No'
							else
								'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
							end
						else
							case
								when ProposalQuestionAnswer.Answer = 'true'
									then 'Yes'
								when ProposalQuestionAnswer.Answer = 'false'
									then 'No'
								else 
									'No' --ProposalQuestionAnswer.Answer
								end
				end Actual
			from ProposalHeader
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId
				inner join Product on Product.Id = ProposalDefinition.ProductId
				inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
				inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
				inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
				inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
				inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
				inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
			where
				ProposalHeader.Id = @ProposalHeaderId and
				Asset.Id = @AssetId and
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				md.QuestionGroup.Name = 'Additional Options' and
				md.QuestionType.Name in ('Checkbox', 'DropDown')
				union 
			select 'Motor Excess', '', (select top 1
						md.QuestionAnswer.Answer
						from ProposalHeader
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							inner join Product on Product.Id = ProposalDefinition.ProductId
							inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
							inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
							inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
							inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
							inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
							inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
							inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
						where
							ProposalHeader.Id = @ProposalHeaderId and
							Asset.Id = @AssetId and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
						    md.QuestionType.Name in ('DropDown') and
							md.Question.Name = 'AIG - Motor Voluntary Excess')	
	
GO

if exists (select * from sys.objects where object_id = object_id(N'Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_Trailer_Caravan') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_Trailer_Caravan
	end
go

Create procedure [dbo].[Reports_QuoteSchedule_ValueAddedProducts_ByAsset_Motor_Trailer_Caravan]
(
	@ProposalHeaderId int,
	@CoverId int,
	@AssetId int
)
as 
select top 1
	(select md.QuestionAnswer.Answer from md.QuestionAnswer
	 where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
	 ) [Description],
	'' Value,
		(
			select top 1
				ltrim(rtrim(ProposalQuestionAnswer.Answer))
			from ProposalHeader
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId
				inner join Product on Product.Id = ProposalDefinition.ProductId
				inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
				inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
				inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
				inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
				inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
				inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
			where
				ProposalHeader.Id = @ProposalHeaderId and
				Asset.Id = @AssetId and
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				md.QuestionGroup.Name = 'Finance Information' and
				md.QuestionType.Name in ('Textbox') and
				md.Question.Name = 'Sum Insured'
		)
 Actual
from ProposalHeader
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join Asset on Asset.Id = ProposalDefinition.AssetId
	inner join Product on Product.Id = ProposalDefinition.ProductId
	inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
	inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
	inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
	inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
	inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
	inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
where
	ProposalHeader.Id = @ProposalHeaderId and
	Asset.Id = @AssetId and
	ProposalHeader.IsDeleted = 0 and
	ProposalDefinition.IsDeleted = 0 and
	Asset.IsDeleted = 0 and
	md.QuestionGroup.Name = 'Risk Information' and
	md.QuestionType.Name in ('DropDown') and
	md.Question.Name = 'AIG -Cover Type'
union all
select
	QuestionDefinition.DisplayName [Description],
	case
		when md.QuestionType.Name = 'DropDown'
			then
				case
					when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
					then ''
				else
					'Included in premium' --'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
				end
			else
				case
					when ProposalQuestionAnswer.Answer = 'true'
						then 'Included in premium'
					when ProposalQuestionAnswer.Answer = 'false'
						then ''
					else 
						'' --ProposalQuestionAnswer.Answer
					end
	end Value,
	case
		when md.QuestionType.Name = 'DropDown'
			then
				case
					when (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer) in ('n', 'no', 'none', '', null)
					then 'No'
				else
					'Yes, ' + (select md.QuestionAnswer.Answer from md.QuestionAnswer where md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer)
				end
			else
				case
					when ProposalQuestionAnswer.Answer = 'true'
						then 'Yes'
					when ProposalQuestionAnswer.Answer = 'false'
						then 'No'
					else 
						'No' --ProposalQuestionAnswer.Answer
					end
	end Actual
from ProposalHeader
	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	inner join Asset on Asset.Id = ProposalDefinition.AssetId
	inner join Product on Product.Id = ProposalDefinition.ProductId
	inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
	inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
	inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
	inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
	inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
	inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
where
	ProposalHeader.Id = @ProposalHeaderId and
	Asset.Id = @AssetId and
	ProposalHeader.IsDeleted = 0 and
	ProposalDefinition.IsDeleted = 0 and
	Asset.IsDeleted = 0 and
	md.QuestionGroup.Name = 'Additional Options' and
	md.QuestionType.Name in ('XXX') --('Checkbox', 'DropDown') exclude Car Hire and Tire and rim scratch and dent FOR MOTOR cover Trailer and Caravan
	union
		select 'Motor Excess', '', (select top 1
				md.QuestionAnswer.Answer
				from ProposalHeader
					inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
					inner join Asset on Asset.Id = ProposalDefinition.AssetId
					inner join Product on Product.Id = ProposalDefinition.ProductId
					inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
					inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
					inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
					inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
					inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
					inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
					inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
				where
					ProposalHeader.Id = @ProposalHeaderId and
					Asset.Id = @AssetId and
					ProposalHeader.IsDeleted = 0 and
					ProposalDefinition.IsDeleted = 0 and
					Asset.IsDeleted = 0 and
				    md.QuestionType.Name in ('DropDown') and
					md.Question.Name = 'AIG - Motor Voluntary Excess')	

	go
