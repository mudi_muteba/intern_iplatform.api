﻿if exists (select * from sys.objects where object_id = object_id(N'Set_Channel_Default_SP'))
	begin
		drop procedure Set_Channel_Default_SP
	end
go

create procedure Set_Channel_Default_SP
(
	@channelCode nvarchar(10)
)
as

update Channel set IsDefault = 0 
update Channel set IsDefault = 1 where Code = @channelCode


declare @channelId int
select @channelId = id from Channel where IsDefault = 1


update UserChannel   set ChannelId = @channelId
update Party   set ChannelId = @channelId
--update EscalationPlan set ChannelId = @channelId
update PersonLookupSetting   set ChannelId = @channelId
update UserAuthorisationGroup   set ChannelId = @channelId
update ChannelEvent   set ChannelId = @channelId
update VehicleGuideSetting   set ChannelId = @channelId
update IndividualUploadHeader   set ChannelId = @channelId
update IndividualUploadDetail   set ChannelId = @channelId
update Campaign   set ChannelId = @channelId
update JsonDataStore   set ChannelId = @channelId
update OrganizationPolicyServiceSetting   set ChannelId = @channelId
update ClaimsHeader   set ChannelId = @channelId
--update ReportChannel   set ChannelId = @channelId
update SalesStructure   set ChannelId = @channelId
