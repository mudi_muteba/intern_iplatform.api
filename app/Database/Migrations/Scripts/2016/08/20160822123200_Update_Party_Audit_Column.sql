﻿if exists(select * from sys.columns where Name = N'Audit' and Object_ID = Object_ID(N'Party'))
begin
	UPDATE Party
	SET [Audit] = NULL
	WHERE [Audit] LIKE '%System.Linq.Enumerable+WhereSelectEnumerableIterator%'
end