﻿update QuestionDefinition
set
	RequiredForQuote = 0
where
	QuestionId = 141 and
	(
		select
			Organization.Code
		from CoverDefinition
			inner join Product on Product.Id = CoverDefinition.ProductId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
			inner join Party on Party.Id = Organization.PartyId
		where
			CoverDefinition.Id = QuestionDefinition.CoverDefinitionId
	) in ('SAU', 'KPI', 'UNITY', 'HOL', 'FFW', 'KPIPERS', 'AUG') and
	(
		select
			CoverDefinition.DisplayName
		from CoverDefinition
		where
			CoverDefinition.Id = QuestionDefinition.CoverDefinitionId
	) in ('Motor', 'Household Contents', 'House Owners') and
	(
		select db_name()
	) in ('iplatform_psguat', 'iplatform_psglive', 'iplatform_kingprice_uat', 'iplatform_kingprice', 'demo_iplatform', 'ibroker')