﻿  DECLARE @InsurerCode nvarchar(255)
  
  --<=====================================================================================================================================================================================================
  SET @InsurerCode = N'AIG'
  if not exists(SELECT * from [LossHistoryQuestionDefinition] where ClientCode = @InsurerCode)
  BEGIN   
  --AIG Previous Insurance Questions
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		'Currently Insured',1,@InsurerCode,1,1,1,0,N'Are you currently insured?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Insurance Duration',2,@InsurerCode,2,1,1,0,N'How long have you had uninterrupted insurance for?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Previous Insurance Cancelled',3,@InsurerCode,3,0,0,0,N'Have any of your previous insurers cancelled your policy?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Had Comprehensive Insurance',4,@InsurerCode,4,0,0,0,N'Have you ever had comprehensive insurance before?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Comprehensive Insurance Duration with no claims',5,@InsurerCode,5,0,0,0,N'How long have you had uninterrupted comprehensive insurance without any clams?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Past Three Years Claim Loss History for Policyholder/Co-PolicyHolder',6,@InsurerCode,6,0,0,0,N'Have you or anyone you intend covering under this policy had any incidents, claims or losses in the past three years?',N'',N'',1,1
	  )
--AIG BUILDING CLAIMS
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of loss',7,@InsurerCode,7,1,1,0,N'Type of loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',8,@InsurerCode,8,1,1,0,N'Date of Loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',9,@InsurerCode,9,1,1,0,N'Claim Amount',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Location',9,@InsurerCode,9,1,1,0,N'Claim Location',N'',N'',3,2
	  )

--AIG MOTOR CLAIMS
      INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of Loss',10,@InsurerCode,10,1,1,0,N'Type of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',11,@InsurerCode,11,1,1,0,N'Date of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',12,@InsurerCode,12,1,1,0,N'Claim Amount',N'',N'',3,3
	  )
  END
  
  
  --<=====================================================================================================================================================================================================
  --PSG Previous Insurance Questions
  SET @InsurerCode = N'PSG'
  if not exists(SELECT * from [LossHistoryQuestionDefinition] where ClientCode = @InsurerCode)
  BEGIN   
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		'Currently Insured',1,@InsurerCode,1,1,1,0,N'Are you currently insured?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Insurance Duration',2,@InsurerCode,2,1,1,0,N'How long have you had uninterrupted insurance for?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Previous Insurance Cancelled',3,@InsurerCode,3,0,0,0,N'Have any of your previous insurers cancelled your policy?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Had Comprehensive Insurance',4,@InsurerCode,4,0,0,0,N'Have you ever had comprehensive insurance before?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Comprehensive Insurance Duration with no claims',5,@InsurerCode,5,0,0,0,N'How long have you had uninterrupted comprehensive insurance without any clams?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Past Three Years Claim Loss History for Policyholder/Co-PolicyHolder',6,@InsurerCode,6,0,0,0,N'Have you or anyone you intend covering under this policy had any incidents, claims or losses in the past three years?',N'',N'',1,1
	  )

	  --BUILDING CLAIMS
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of loss',7,@InsurerCode,7,1,1,0,N'Type of loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',8,@InsurerCode,8,1,1,0,N'Date of Loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',9,@InsurerCode,9,1,1,0,N'Claim Amount',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Location',9,@InsurerCode,9,1,1,0,N'Claim Location',N'',N'',3,2
	  )

--MOTOR CLAIMS
      INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of Loss',10,@InsurerCode,10,1,1,0,N'Type of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',11,@InsurerCode,11,1,1,0,N'Date of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',12,@InsurerCode,12,1,1,0,N'Claim Amount',N'',N'',3,3
	  )
  END

  --<=====================================================================================================================================================================================================
  --UAP Previous Insurance Questions

  SET @InsurerCode = N'UAP'
  if not exists(SELECT * from [LossHistoryQuestionDefinition] where ClientCode = @InsurerCode)
  BEGIN   
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		'Currently Insured',1,@InsurerCode,1,1,1,0,N'Are you currently insured?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Insurance Duration',2,@InsurerCode,2,1,1,0,N'How long have you had uninterrupted insurance for?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Previous Insurance Cancelled',3,@InsurerCode,3,0,0,0,N'Have any of your previous insurers cancelled your policy?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Had Comprehensive Insurance',4,@InsurerCode,4,0,0,0,N'Have you ever had comprehensive insurance before?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Comprehensive Insurance Duration with no claims',5,@InsurerCode,5,0,0,0,N'How long have you had uninterrupted comprehensive insurance without any clams?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Past Three Years Claim Loss History for Policyholder/Co-PolicyHolder',6,@InsurerCode,6,0,0,0,N'Have you or anyone you intend covering under this policy had any incidents, claims or losses in the past three years?',N'',N'',1,1
	  )
	  
	  --BUILDING CLAIMS
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of loss',7,@InsurerCode,7,1,1,0,N'Type of loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',8,@InsurerCode,8,1,1,0,N'Date of Loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',9,@InsurerCode,9,1,1,0,N'Claim Amount',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Location',9,@InsurerCode,9,1,1,0,N'Claim Location',N'',N'',3,2
	  )

--MOTOR CLAIMS
      INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of Loss',10,@InsurerCode,10,1,1,0,N'Type of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',11,@InsurerCode,11,1,1,0,N'Date of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',12,@InsurerCode,12,1,1,0,N'Claim Amount',N'',N'',3,3
	  )
  END

--<=====================================================================================================================================================================================================
  SET @InsurerCode = N'Hollard'

  if not exists(SELECT * from [LossHistoryQuestionDefinition] where ClientCode = @InsurerCode)
  BEGIN   
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		'Currently Insured',1,@InsurerCode,1,1,1,0,N'Are you currently insured?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Insurance Duration',2,@InsurerCode,2,1,1,0,N'How long have you had uninterrupted insurance for?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Previous Insurance Cancelled',3,@InsurerCode,3,0,0,0,N'Have any of your previous insurers cancelled your policy?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Had Comprehensive Insurance',4,@InsurerCode,4,0,0,0,N'Have you ever had comprehensive insurance before?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Comprehensive Insurance Duration with no claims',5,@InsurerCode,5,0,0,0,N'How long have you had uninterrupted comprehensive insurance without any clams?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Past Three Years Claim Loss History for Policyholder/Co-PolicyHolder',6,@InsurerCode,6,0,0,0,N'Have you or anyone you intend covering under this policy had any incidents, claims or losses in the past three years?',N'',N'',1,1
	  )
	  
	  --BUILDING CLAIMS
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of loss',7,@InsurerCode,7,1,1,0,N'Type of loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',8,@InsurerCode,8,1,1,0,N'Date of Loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',9,@InsurerCode,9,1,1,0,N'Claim Amount',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Location',9,@InsurerCode,9,1,1,0,N'Claim Location',N'',N'',3,2
	  )

--MOTOR CLAIMS
      INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of Loss',10,@InsurerCode,10,1,1,0,N'Type of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',11,@InsurerCode,11,1,1,0,N'Date of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',12,@InsurerCode,12,1,1,0,N'Claim Amount',N'',N'',3,3
	  )
  END

  --<=====================================================================================================================================================================================================
  SET @InsurerCode = N'KingPrice'

  if not exists(SELECT * from [LossHistoryQuestionDefinition] where ClientCode = @InsurerCode)
  BEGIN   
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		'Currently Insured',1,@InsurerCode,1,1,1,0,N'Are you currently insured?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Insurance Duration',2,@InsurerCode,2,1,1,0,N'How long have you had uninterrupted insurance for?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Previous Insurance Cancelled',3,@InsurerCode,3,0,0,0,N'Have any of your previous insurers cancelled your policy?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Had Comprehensive Insurance',4,@InsurerCode,4,0,0,0,N'Have you ever had comprehensive insurance before?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Comprehensive Insurance Duration with no claims',5,@InsurerCode,5,0,0,0,N'How long have you had uninterrupted comprehensive insurance without any clams?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Past Three Years Claim Loss History for Policyholder/Co-PolicyHolder',6,@InsurerCode,6,0,0,0,N'Have you or anyone you intend covering under this policy had any incidents, claims or losses in the past three years?',N'',N'',1,1
	  )
	  
	  --BUILDING CLAIMS
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of loss',7,@InsurerCode,7,1,1,0,N'Type of loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',8,@InsurerCode,8,1,1,0,N'Date of Loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',9,@InsurerCode,9,1,1,0,N'Claim Amount',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Location',9,@InsurerCode,9,1,1,0,N'Claim Location',N'',N'',3,2
	  )

--MOTOR CLAIMS
      INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of Loss',10,@InsurerCode,10,1,1,0,N'Type of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',11,@InsurerCode,11,1,1,0,N'Date of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',12,@InsurerCode,12,1,1,0,N'Claim Amount',N'',N'',3,3
	  )
  END

  --<=====================================================================================================================================================================================================
  SET @InsurerCode = N'Telesure'

  if not exists(SELECT * from [LossHistoryQuestionDefinition] where ClientCode = @InsurerCode)
  BEGIN   
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		'Currently Insured',1,@InsurerCode,1,1,1,0,N'Are you currently insured?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Insurance Duration',2,@InsurerCode,2,1,1,0,N'How long have you had uninterrupted insurance for?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Previous Insurance Cancelled',3,@InsurerCode,3,0,0,0,N'Have any of your previous insurers cancelled your policy?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Had Comprehensive Insurance',4,@InsurerCode,4,0,0,0,N'Have you ever had comprehensive insurance before?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Comprehensive Insurance Duration with no claims',5,@InsurerCode,5,0,0,0,N'How long have you had uninterrupted comprehensive insurance without any clams?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Past Three Years Claim Loss History for Policyholder/Co-PolicyHolder',6,@InsurerCode,6,0,0,0,N'Have you or anyone you intend covering under this policy had any incidents, claims or losses in the past three years?',N'',N'',1,1
	  )

	  --Telesure MOTOR CLAIMS
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of loss',7,@InsurerCode,7,1,1,0,N'Type of loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',8,@InsurerCode,8,1,1,0,N'Date of Loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',9,@InsurerCode,9,1,1,0,N'Claim Amount',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Location',9,@InsurerCode,9,1,1,0,N'Claim Location',N'',N'',3,2
	  )

--Telesure MOTOR CLAIMS
      INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of Loss',10,@InsurerCode,10,1,1,0,N'Type of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',11,@InsurerCode,11,1,1,0,N'Date of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',12,@InsurerCode,12,1,1,0,N'Claim Amount',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Anyone Injured',13,@InsurerCode,13,1,1,0,N'Anyone Injured',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Injury Severity',14,@InsurerCode,14,1,1,0,N'Injury Severity',N'',N'',3,3
	  )
  END

  --<=====================================================================================================================================================================================================
  SET @InsurerCode = N'IP'

  if not exists(SELECT * from [LossHistoryQuestionDefinition] where ClientCode = @InsurerCode)
  BEGIN   
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		'Currently Insured',1,@InsurerCode,1,1,1,0,N'Are you currently insured?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Insurance Duration',2,@InsurerCode,2,1,1,0,N'How long have you had uninterrupted insurance for?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Previous Insurance Cancelled',3,@InsurerCode,3,0,0,0,N'Have any of your previous insurers cancelled your policy?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Had Comprehensive Insurance',4,@InsurerCode,4,0,0,0,N'Have you ever had comprehensive insurance before?',N'',N'',1,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Uninterrupted Comprehensive Insurance Duration with no claims',5,@InsurerCode,5,0,0,0,N'How long have you had uninterrupted comprehensive insurance without any clams?',N'',N'',3,1
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Past Three Years Claim Loss History for Policyholder/Co-PolicyHolder',6,@InsurerCode,6,0,0,0,N'Have you or anyone you intend covering under this policy had any incidents, claims or losses in the past three years?',N'',N'',1,1
	  )
	  
	  --BUILDING CLAIMS
	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of loss',7,@InsurerCode,7,1,1,0,N'Type of loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',8,@InsurerCode,8,1,1,0,N'Date of Loss',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',9,@InsurerCode,9,1,1,0,N'Claim Amount',N'',N'',3,2
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Location',9,@InsurerCode,9,1,1,0,N'Claim Location',N'',N'',3,2
	  )

--MOTOR CLAIMS
      INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Type of Loss',10,@InsurerCode,10,1,1,0,N'Type of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Date of Loss',11,@InsurerCode,11,1,1,0,N'Date of Loss',N'',N'',3,3
	  )

	  INSERT INTO [dbo].[LossHistoryQuestionDefinition]
	  (
		[DisplayName],[LossHistoryQuestionDefinitionId],[ClientCode],[VisibleIndex],[RequierdForQuote],[RatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[LossHistoryQuestionGroupId],[QuestionTypeId]
	  )
	  VALUES
	  (
		N'Claim Amount',12,@InsurerCode,12,1,1,0,N'Claim Amount',N'',N'',3,3
	  )
  END