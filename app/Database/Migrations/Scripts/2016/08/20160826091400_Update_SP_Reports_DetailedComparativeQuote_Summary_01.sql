﻿if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Summary') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Summary
	end
go

create procedure Reports_DetailedComparativeQuote_Summary
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50)
)
as
	select
		Organization.TradingName Insurer,
		Organization.Code Code,
		Product.Name Product,
		Sum(QuoteItem.ExcessBasic) BasicExcess,
		Sum(QuoteItem.Premium) Premium,
		Sum(QuoteItem.SASRIA) SASRIA,
		Sum(Quote.Fees) Fees,
		Sum(cast((QuoteItem.Premium) as numeric(18, 2))) Total
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		--inner join QuoteState on QuoteState.QuoteId = Quote.Id
		--inner join QuoteStateEntry on QuoteStateEntry.QuoteStateId = QuoteState.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		QuoteHeader.IsDeleted = 0 and
		Quote.IsDeleted = 0 and
		Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
		--QuoteState.Errors = 0 and
		QuoteItem.IsDeleted = 0 and
		CoverDefinition.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		QuoteItem.Premium > 0
	group by
		Organization.Code,
		Organization.TradingName,
		Product.Name
	order by
		Premium desc,
		Organization.TradingName asc
go