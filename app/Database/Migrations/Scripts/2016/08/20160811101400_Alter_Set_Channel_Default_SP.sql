﻿if exists (select * from sys.objects where object_id = object_id(N'Set_Channel_Default_SP') and type in (N'P', N'PC'))
BEGIN
DROP procedure [Set_Channel_Default_SP]
END
GO
CREATE PROC [dbo].[Set_Channel_Default_SP]
(
	@channelCode nvarchar(10)
)
as
--DECLARE @channelCode nvarchar(10);
--SET @channelCode = 'AIG'
BEGIN TRY
	BEGIN TRANSACTION
		update Channel set IsDefault = 0 
		update Channel set IsDefault = 1 where Code = @channelCode

		declare @channelId int
		select @channelId = id 
		from Channel 
		where IsDefault = 1

		
		update Party   set ChannelId = @channelId
		--update EscalationPlan   set ChannelId = @channelId
		update PersonLookupSetting   set ChannelId = @channelId
		update UserAuthorisationGroup   set ChannelId = @channelId
		update ChannelEvent   set ChannelId = @channelId
		update VehicleGuideSetting   set ChannelId = @channelId
		update IndividualUploadHeader   set ChannelId = @channelId
		update IndividualUploadDetail   set ChannelId = @channelId
		update Campaign   set ChannelId = @channelId
		update JsonDataStore   set ChannelId = @channelId
		update OrganizationPolicyServiceSetting   set ChannelId = @channelId
		update ClaimsHeader   set ChannelId = @channelId
		--update ReportChannel   set ChannelId = @channelId
		update SalesStructure   set ChannelId = @channelId

		--update UserChannel set ChannelId = @channelId

		DECLARE @id int;
		SET @id = (SELECT TOP (1) Id
					FROM UserChannel
					WHERE ChannelId = @channelId)  

		UPDATE UserChannel 
		SET IsDefault = 1,
		ChannelId = @channelId
		WHERE Id = @id

	COMMIT TRANSACTION

	PRINT 'CHANNEL UPDATE SUCCESSFULL'

END TRY
BEGIN CATCH
IF(@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRANSACTION
		PRINT 'UNABLE TO UPDATE, ALL REVERTED'
	END
	SELECT 
		ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
END CATCH
