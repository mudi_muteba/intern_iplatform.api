﻿if exists (select * from Channel where IsDefault = 1 and Code = 'UAP')
begin

if not exists (select Id from md.MethodOfPayment where Id = 3)
	begin
		INSERT [md].[MethodOfPayment] ([Id], [Name]) VALUES ( 3, N'MPesa')
	end

if not exists (select Id from md.MethodOfPayment where Id = 4)
	begin
		INSERT [md].[MethodOfPayment] ([Id], [Name]) VALUES ( 4, N'Credit Card')
	end

end