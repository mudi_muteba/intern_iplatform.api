﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='Virgin')

DECLARE @ContentsCoverID INT
SET @ContentsCoverID=(select ID from md.Cover where name ='Motor')

DECLARE @CoverDefid INT
SET @CoverDefid=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId=@ContentsCoverID)

DECLARE @QuestionDefID INT
set @QuestionDefID = (SELECT ID FROM dbo.QuestionDefinition WHERE QuestionId=229 AND IsDeleted=0 AND CoverDefinitionId=@CoverDefid)

DELETE FROM dbo.ProposalQuestionAnswer WHERE QuestionDefinitionId=@QuestionDefID

DELETE FROM dbo.QuestionDefinition WHERE id = @QuestionDefID
