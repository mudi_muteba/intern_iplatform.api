﻿IF EXISTS(select id from [dbo].[Channel] where Code = 'UAP' AND IsDefault = 1)
	BEGIN

		declare @ProductId int
		select @ProductId = id from Product where ProductCode = 'MUL'

		--Motor
		declare @CoverDefinitionId int
		set @CoverDefinitionId = (select id from CoverDefinition where ProductId = @ProductId and CoverId = 218)

		if not exists(select * from QuestionDefinition where QuestionId=1171 and CoverDefinitionId = @CoverDefinitionId And IsDeleted = 0)
				begin
				INSERT INTO [dbo].[QuestionDefinition]([MasterId],[DisplayName],[CoverDefinitionId],[QuestionId],[ParentId],[QuestionDefinitionTypeId],[VisibleIndex],[RequiredForQuote],[RatingFactor],[ReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[GroupIndex],[QuestionDefinitionGroupTypeId],[IsDeleted])
				VALUES(0,'Political Violence and Terrorism Risks',@CoverDefinitionId,1171,NULL,1,320,0,1,0,'Political Violence and Terrorism Risks',N'',N'^[0-9\.]+$',0,NULL,0)
				END

	END
