﻿declare
	@UserId int,
	@Username varchar(255),
	@Identity int

declare user_cursor cursor for
(
	select
		u.Id,
		u.UserName
	from [User] u
	where
		u.Id not in (select ui.UserId from UserIndividual ui where ui.UserId = u.Id)
)
open user_cursor  
fetch next from user_cursor into @UserId, @Username

while @@fetch_status = 0  
	begin
		insert into Party (PartyTypeId, DisplayName, IsDeleted, ChannelId) values (1, @Username, 0, (select top 1 Id from Channel where IsDefault = 1))
		set @Identity = scope_identity()

		insert into Individual (PartyId, FirstName, Surname) values (@Identity, @Username, @Username)
		insert into UserIndividual (UserId, IndividualId, CreatedAt, ModifiedAt, IsDeleted) values (@UserId, @Identity, getdate(), getdate(), 0)

		fetch next from user_cursor into @UserId, @Username
	end

close user_cursor  
deallocate user_cursor 