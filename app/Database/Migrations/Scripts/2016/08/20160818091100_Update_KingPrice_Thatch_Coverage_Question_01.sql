﻿update QuestionDefinition
set
	ToolTip = 'The amount of thatch that covers the lapa''s roof in percentage, i.e: 75'
where
	DisplayName = 'Thatch Coverage'