﻿Update SettingsiRate Set ChannelSystemId = ChannelId;
GO
Alter table SettingsiRate drop column channelId;
GO
Alter table SettingsiRate add ChannelId int;
GO
UPDATE SettingsiRate 
SET
	ChannelId = (SELECT
					c.Id
				 FROM Channel c
				 WHERE c.SystemId = ChannelSystemId);
GO
