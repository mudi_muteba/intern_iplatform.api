﻿DECLARE @chanid INT
DECLARE @Name VARCHAR(50)
DECLARE @Code VARCHAR(50)

DECLARE @chanEvent INT
DECLARE @rootUserid INT

SET @rootUserid = ( SELECT TOP 1 ID FROM [User] where UserName like '%root@iplatform.co.za%')

SET @Code = N'JP'
SET @Name = N'Just Perfect'

 
SET @chanid = ( SELECT TOP 1 ID FROM dbo.Channel where  Code like @Code)

if (@chanid IS NULL)
BEGIN
INSERT Into Channel (
 [Id]
 ,[SystemId]
 ,[IsActive]
 ,[ActivatedOn]
 ,[DeactivatedOn]
 ,[IsDefault]
 ,[IsDeleted]
 ,[CurrencyId]
 ,[DateFormat]
 ,[Name]
 ,[LanguageId]
 ,[PasswordStrengthEnabled]
 ,[Code]
 ,[CountryId])
 Values (
 17,
 'F4479DD7-87EE-4890-B812-03249CD915C8',
 1,
 GETDATE(),
 Null,
 0,
 0,
 1,
 'dd MMM yyyy',
 @Name,
 2,
 0,
 @Code,
 197
)
SET @chanid = SCOPE_IDENTITY()
END


DECLARE @userChan INT
 
SET @userChan = ( SELECT TOP 1 ID FROM [UserChannel] where UserId = @rootUserid AND ChannelId = @chanid) -- 760 is Root
SELECT @rootUserid
if (@userChan IS NULL)
BEGIN
INSERT INTO [UserChannel] (CreatedAt, ModifiedAt, UserId, ChannelId, IsDeleted, IsDefault)
VALUES( GETDATE(), GETDATE(), @rootUserid, 14, 0, NULL)
SET @userChan = SCOPE_IDENTITY()
END

SELECT * FROM Channel

SELECT   * FROM [UserChannel] where UserId = @rootUserid AND ChannelId = @chanid
 SELECT   * FROM [UserAuthorisationGroup] where UserId = @rootUserid AND ChannelId = @chanid
 SELECT   * FROM [ChannelEventTask] where ChannelEventId = @chanEvent
  SELECT   * FROM [ChannelEvent] where ProductCode like 'AAC' and ChannelId = @chanid
   SELECT   * FROM dbo.Channel where  Code like @Code

RETURN

DECLARE @userAuth INT
 
SET @userAuth = ( SELECT TOP 1 ID FROM [UserAuthorisationGroup] where UserId = @rootUserid AND ChannelId = @chanid) -- 760 is Root


if (@userAuth IS NULL)
BEGIN
INSERT INTO [UserAuthorisationGroup] (AuthorisationGroupId, UserId, ChannelId, IsDeleted)
VALUES( 5, @rootUserid, @chanid, 0)
SET @userAuth = SCOPE_IDENTITY()
END