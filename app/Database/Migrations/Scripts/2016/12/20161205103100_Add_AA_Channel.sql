﻿DECLARE @PartyId int, @ContactDetailId INT, @count int;

--AA
SELECT @count = count(*) FROM Organization o INNER JOIN Party p on p.Id = o.PartyId WHERE Code = 'AA' AND p.IsDeleted != 0;

if @count > 1
BEGIN 
	SELECT top 1 @PartyId = PartyId FROM Organization WHERE Code = 'AA' order by PartyId desc;
	Update Party Set IsDeleted = 1 WHERE Id = @PartyId;
END

SELECT @count = count(*) FROM Organization o INNER JOIN Party p on p.Id = o.PartyId WHERE Code = 'AA' AND p.IsDeleted != 0;

if @count = 1
BEGIN
	SELECT @PartyId = o.PartyId FROM Organization o INNER JOIN Party p on p.Id = o.PartyId WHERE Code = 'AA' AND p.IsDeleted != 0;
END

if not exists(select * from Channel where Id = 16)
begin
insert into Channel(Id,SystemId,IsActive,ActivatedOn,IsDefault,IsDeleted,CurrencyId,[DateFormat],Name,LanguageId,PasswordStrengthEnabled,Code,CountryId,OrganizationId)
values (16,'7A584438-D490-4C07-A353-DBB4C9299AA4',1,'2016-10-14 11:37:00.000',0,0,1,'dd MMM yyyy','AA',2,0,'AA',197, @PartyId)
end