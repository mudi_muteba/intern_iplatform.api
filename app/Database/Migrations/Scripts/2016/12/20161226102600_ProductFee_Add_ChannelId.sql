SET ANSI_NULLS OFF
DECLARE @countChannel int, @countProductFee int, @ChannelId int

SELECT @countChannel = count(*) FROM [dbo].[Channel] WHERE Isdeleted <> 1

IF @countChannel > 1
BEGIN

	SELECT @countProductFee = count(*) FROM ProductFee where ChannelId = null and IsDeleted <> 1

	if @countProductFee> 0
	BEGIN
		DECLARE MY_CURSOR CURSOR 
		LOCAL STATIC READ_ONLY FORWARD_ONLY
		FOR 
		SELECT [Id] FROM [dbo].[Channel]

		OPEN MY_CURSOR
		FETCH NEXT FROM MY_CURSOR INTO @ChannelId
		WHILE @@FETCH_STATUS = 0
		BEGIN 
			INSERT INTO ProductFee (MasterId,ProductId, ProductFeeTypeId, PaymentPlanId, BrokerageId,Value,MinValue,MaxValue,IsOnceOff,IsPercentage,AllowRefund,AllowProRata,IsDeleted, ChannelId)
			SELECT MasterId,ProductId,ProductFeeTypeId, PaymentPlanId, BrokerageId,Value,MinValue,MaxValue,IsOnceOff,IsPercentage,AllowRefund,AllowProRata,IsDeleted, @ChannelId FROM ProductFee where ChannelId = null and IsDeleted <> 1
			FETCH NEXT FROM MY_CURSOR INTO @ChannelId
		END
		CLOSE MY_CURSOR
		DEALLOCATE MY_CURSOR
	END
END

