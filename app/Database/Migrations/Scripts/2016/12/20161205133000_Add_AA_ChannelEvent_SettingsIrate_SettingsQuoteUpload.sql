﻿DECLARE @ChannelId int, @SystemId varchar(50), @ChannelEventId int, @ProductId int;

SELECT @ChannelId = Id,@SystemId = SystemId FROM Channel WHERE Code = 'AA' AND IsDeleted != 1

If @ChannelId > 0
BEGIN
	--Insert ChannelEvent
	INSERT INTO ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode, ChannelReferenceId) Values ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', @ChannelId, 0, 'AAC', null);

	SELECT @ChannelEventId = @@IDENTITY;
	--Insert ChannelEventTask
	INSERT INTO ChannelEventTask (TaskName, ChannelEventId, IsDeleted) Values ('2', @ChannelEventId, 0)
	--Get ProductCode
	SELECT top 1 @ProductId = Id FROM Product WHERE ProductCode = 'AAC' and IsDeleted != 1;

	if @ProductId > 1
	BEGIN
		INSERT INTO SettingsiRate (ProductId, [Password], ChannelId, Userid, CompanyCode,  ProductCode, Environment) Values (@ProductId, 'MRC_PASS',@SystemId, 'CIMS_USER', 'CIMS', 'AAC', 'Live');

		INSERT INTO SettingsQuoteUpload (SettingName, SettingValue,Environment, ChannelId, ProductId, IsDeleted) VALUES
		('AA/Upload/ServiceURL', 'http://no_url_for_now', 'Live',@ChannelId, @ProductId, 0 ),
		('AA/Upload/AcceptanceEmailAddress', 'monitoring@iplatform.co.za', 'Live',@ChannelId, @ProductId, 0 ),
		('AA/Upload/ErrorEmailAddress', 'monitoring@iplatform.co.za', 'Live',@ChannelId, @ProductId, 0 ),
		('AA/Upload/RetryDelayIncrement', '5', 'Live',@ChannelId, @ProductId, 0 ),
		('AA/Upload/MaxRetries', '5', 'Live',@ChannelId, @ProductId, 0 ),
		('AA/Upload/subject', 'UPLOADING LEAD TO AA', 'Live',@ChannelId, @ProductId, 0 ),
		('AA/Upload/template', 'LeadUploadTemplate', 'Live',@ChannelId, @ProductId, 0 );
	END
END

