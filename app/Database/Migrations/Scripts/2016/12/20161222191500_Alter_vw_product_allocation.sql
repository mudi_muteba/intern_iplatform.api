﻿
/****** Object:  View [dbo].[vw_product_allocation]    Script Date: 12/22/2016 9:11:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_product_allocation]
AS
SELECT p.Id, p.ProductCode, p.Name, p.StartDate, 
     p.EndDate, c.Id AS ChannelId, 
     pp.PartyId AS ProductProviderId, 
     pp.TradingName AS ProductProviderTradingName,
      po.PartyId AS ProductOwnerId, 
     po.TradingName AS ProductOwnerTradingName, 0 as IsDeleted, p.IsSelectedExcess, p.IsVoluntaryExcess
FROM dbo.SettingsiRate AS irs INNER JOIN
     dbo.Channel AS c ON 
     irs.ChannelId = c.Id AND 
     c.IsDeleted = 0 INNER JOIN
     dbo.Product AS p ON irs.ProductId = p.Id AND 
     p.IsDeleted = 0 INNER JOIN
     dbo.Organization AS pp ON 
     p.ProductProviderId = pp.PartyId INNER JOIN
     dbo.Party AS ppp ON pp.PartyId = ppp.Id AND 
     ppp.IsDeleted = 0 INNER JOIN
     dbo.Organization AS po ON 
     p.ProductOwnerId = po.PartyId INNER JOIN
     dbo.Party AS pop ON po.PartyId = pop.Id AND 
     pop.IsDeleted = 0
	 
union all
select p.Id, p.ProductCode, p.Name, p.StartDate, 
     p.EndDate, c.Id AS ChannelId, 
     0 AS ProductProviderId, 
     '' AS ProductProviderTradingName,
     0 AS ProductOwnerId, 
     '' AS ProductOwnerTradingName, 0 as IsDeleted, p.IsSelectedExcess, p.IsVoluntaryExcess
from product p
	cross join Channel c
where ProductCode = 'MUL'

GO

