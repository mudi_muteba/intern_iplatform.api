﻿if exists(select * from sys.columns where Name = N'Environment' and Object_ID = Object_ID(N'SettingsiPerson'))
begin
	alter table SettingsiPerson drop column Environment;
end