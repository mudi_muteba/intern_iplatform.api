﻿if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics
	end
go

create procedure Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics
(
	@CampaignIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Agents_CTE (ChannelId, AgentId, CampaignId, Channel, Agent, Campaign)
			as 
			(
				select distinct
					Channel.Id,
					[User].Id,
					Campaign.Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				from Campaign
					inner join Channel on Channel.Id = Campaign.ChannelId
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					Campaign.Id in (select * from fn_StringListToTable(@CampaignIds)) and
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
					UserAuthorisationGroup.AuthorisationGroupId = 1
				group by
					Channel.Id,
					Channel.Name,
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName
				)
			select
				Channel,
				Agent,
				Campaign,
				(
					select
						isnull(count(distinct(LeadActivity.LeadId)), 0)
					from LeadActivity
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
				) Leads,
				(
					select
						isnull(count(distinct(QuoteHeader.Id)), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					isnull(
						cast(
								(
									select
										cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric(18,2))
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
										inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
										inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									where
										LeadActivity.CampaignId = Agents_CTE.CampaignId and
										LeadActivity.UserId = Agents_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo and
										QuoteHeader.IsRerated = 0 and
										Quote.IsDeleted = 0
								)
					
								/
					
								(
									select
										nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
									from LeadActivity
									where
										LeadActivity.CampaignId = Agents_CTE.CampaignId and
										LeadActivity.UserId = Agents_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo
								)

							as numeric(18,2)
						)
					, 0)
				) QuotesPerLead,
				(
					select
						isnull(count(Quote.Id), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) SecondLevelUnderwriting,
				(
					select
						isnull(count(Quote.Id), 0)
					from LeadActivity
						inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) Sales,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(Quote.Id), 0) as numeric( 18, 2))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) Closing,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(Quote.Id), 0) as numeric( 18, 2))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
									select
										nullif(cast(isnull(count(Quote.Id), 0) as numeric( 18, 2)), 0)
									from LeadActivity
										inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
										inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									where
										LeadActivity.CampaignId = Agents_CTE.CampaignId and
										LeadActivity.UserId = Agents_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo and
										Quote.IsDeleted = 0
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerClosing,
				(
					isnull(
						cast
						(
							
							(
								1
						
								-

								(
									( 
										select
											cast(isnull(count(Quote.Id), 0) as numeric( 18, 2))
										from LeadActivity
											inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
										where
											LeadActivity.CampaignId = Agents_CTE.CampaignId and
											LeadActivity.UserId = Agents_CTE.AgentId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0
									)

									/

									(
											select
												nullif(cast(isnull(count(Quote.Id), 0) as numeric( 18, 2)), 0)
											from LeadActivity
												inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
												inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
											where
												LeadActivity.CampaignId = Agents_CTE.CampaignId and
												LeadActivity.UserId = Agents_CTE.AgentId and
												LeadActivity.DateUpdated >= @DateFrom and
												LeadActivity.DateUpdated <= @DateTo and
												Quote.IsDeleted = 0
									)
								)
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerDropOff,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from Quote
							inner join SoldLeadActivity on SoldLeadActivity.QuoteId = Quote.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join LeadActivity on LeadActivity.Id = SoldLeadActivity.LeadActivityId
						where
							LeadActivity.UserId = Agents_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote
			from
				Agents_CTE
			order by
				Campaign,
				Agent
		end
	else
		begin
			;with Agents_CTE (ChannelId, AgentId, CampaignId, Channel, Agent, Campaign)
			as 
			(
				select distinct
					Channel.Id,
					[User].Id,
					Campaign.Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				from Campaign
					inner join Channel on Channel.Id = Campaign.ChannelId
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
					UserAuthorisationGroup.AuthorisationGroupId = 1
				group by
					Channel.Id,
					Channel.Name,
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName
				)
			select
				Channel,
				Agent,
				Campaign,
				(
					select
						isnull(count(distinct(LeadActivity.LeadId)), 0)
					from LeadActivity
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
				) Leads,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					isnull(
						cast(
								(
									select
										cast(isnull(count(distinct(Quote.Id)), 0) as numeric(18,2))
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
										inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
										inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									where
										LeadActivity.CampaignId = Agents_CTE.CampaignId and
										LeadActivity.UserId = Agents_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo and
										QuoteHeader.IsRerated = 0 and
										Quote.IsDeleted = 0
								)
					
								/
					
								(
									select
										nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
									from LeadActivity
									where
										LeadActivity.CampaignId = Agents_CTE.CampaignId and
										LeadActivity.UserId = Agents_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo
								)

							as numeric(18,2)
						)
					, 0)
				) QuotesPerLead,
				(
					select
						isnull(count(Quote.Id), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) SecondLevelUnderwriting,
				(
					select
						isnull(count(Quote.Id), 0)
					from LeadActivity
						inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) Sales,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(Quote.Id), 0) as numeric( 18, 2))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) Closing,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(Quote.Id), 0) as numeric( 18, 2))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
									select
										nullif(cast(isnull(count(Quote.Id), 0) as numeric( 18, 2)), 0)
									from LeadActivity
										inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
										inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									where
										LeadActivity.CampaignId = Agents_CTE.CampaignId and
										LeadActivity.UserId = Agents_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo and
										Quote.IsDeleted = 0
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerClosing,
				(
					isnull(
						cast
						(
							
							(
								1
						
								-

								(
									( 
										select
											cast(isnull(count(Quote.Id), 0) as numeric( 18, 2))
										from LeadActivity
											inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
										where
											LeadActivity.CampaignId = Agents_CTE.CampaignId and
											LeadActivity.UserId = Agents_CTE.AgentId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0
									)

									/

									(
											select
												nullif(cast(isnull(count(Quote.Id), 0) as numeric( 18, 2)), 0)
											from LeadActivity
												inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
												inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
											where
												LeadActivity.CampaignId = Agents_CTE.CampaignId and
												LeadActivity.UserId = Agents_CTE.AgentId and
												LeadActivity.DateUpdated >= @DateFrom and
												LeadActivity.DateUpdated <= @DateTo and
												Quote.IsDeleted = 0
									)
								)
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerDropOff,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from Quote
							inner join SoldLeadActivity on SoldLeadActivity.QuoteId = Quote.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join LeadActivity on LeadActivity.Id = SoldLeadActivity.LeadActivityId
						where
							LeadActivity.UserId = Agents_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote
			from
				Agents_CTE
			order by
				Campaign,
				Agent
		end
go