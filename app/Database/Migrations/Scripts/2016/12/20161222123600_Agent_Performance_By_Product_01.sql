﻿-----------
--AIG Check
-----------
declare @AIGChannelCode varchar(20)

select top 1
	@AIGChannelCode = Code
from Channel
where
	IsDefault = 1

if @AIGChannelCode = 'AIG'
	begin
		set noexec on
	end
go

set nocount on
go

---------
--Reports
---------

if not exists(select * from Report where Name = 'Comparative Quote')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 1, 3, 'Comparative Quote', 'Comparative Quote', 'ComparativeQuote.trdx',  0, 0, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 1,
			ReportFormatId = 3,
			Name = 'Comparative Quote',
			[Description] = 'Comparative Quote',
			SourceFile = 'ComparativeQuote.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'Comparative Quote'
	end
go

if not exists(select * from Report where Name = 'Agent Performance')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 2, 3, 'Agent Performance', 'Agent Performance', 'AgentPerformance.trdx',  1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Agent Performance',
			[Description] = 'This report measures the no. of quotes an agent has done compared to their sales and gives a conversion ratio',
			SourceFile = 'AgentPerformance.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1
		where
			Name = 'Agent Performance'
	end
go

if not exists(select * from Report where Name = 'Agent Performance By Product')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 2, 3, 'Agent Performance By Product', 'This report measures the no. of quotes an agent has done compared to their sales and gives a conversion ratio by product', 'AgentPerformanceByProduct.trdx',  1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Agent Performance By Product',
			[Description] = 'This report measures the no. of quotes an agent has done compared to their sales and gives a conversion ratio by product',
			SourceFile = 'AgentPerformanceByProduct.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1
		where
			Name = 'Agent Performance By Product'
	end
go

if not exists(select * from Report where Name = 'Insurer Quote Breakdown')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile,  IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 2, 3, 'Insurer Quote Breakdown', 'Insurer Quote Breakdown', 'InsurerQuoteBreakdown.trdx',  1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Insurer Quote Breakdown',
			[Description] = 'This reports on the number of quotes done per insurer per product and advises on the average premium quoted and the average premium per sale',
			SourceFile = 'InsurerQuoteBreakdown.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1
		where
			Name = 'Insurer Quote Breakdown'
	end
go

if not exists(select * from Report where Name = 'Lead Management')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile,  IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (2, 2, 3, 'Lead Management', 'Lead Management', 'LeadManagement.trdx', 1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 2,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Lead Management',
			[Description] = 'Lead Management',
			SourceFile = 'LeadManagement.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1
		where
			Name = 'Lead Management'
	end
go

if not exists(select * from Report where Name = 'Target & Sales Management')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile,  IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (2, 2, 3, 'Target & Sales Management', 'Target & Sales Management', 'TargetSalesManagement.trdx',  1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 2,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Target & Sales Management',
			[Description] = 'Target & Sales Management',
			SourceFile = 'TargetSalesManagement.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 0
		where
			Name = 'Target & Sales Management'
	end
go

if not exists(select * from Report where Name = 'Debit order instruction')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('Debit order instruction', 'Debit order instruction', 'Debit Order Instruction.trdx', 0, 0, 1, 5,  3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'Debit order instruction',
			[Description] = 'Debit order instruction',
			SourceFile = 'Debit Order Instruction.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'Debit order instruction'
	end
go

if not exists(select * from Report where Name = 'Record Of Advice')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('Record Of Advice', 'Record Of Advice', 'Record Of Advice.trdx', 0, 0, 1, 6,  3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'Record Of Advice',
			[Description] = 'Record Of Advice',
			SourceFile = 'Record Of Advice.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'Record Of Advice'
	end
go

if not exists(select * from Report where Name = 'Broker Note')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex,  ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('Broker Note', 'Broker Note', 'Broker''s Note.trdx', 0, 0, 1, 7,  3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'Broker Note',
			[Description] = 'Broker Note',
			SourceFile = 'Broker''s Note.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'Broker Note'
	end
go

if not exists(select * from Report where Name = 'POPI document')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex,  ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('POPI document', 'POPI document', 'POPI_DOC.trdx', 0, 0, 1, 8,  3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'POPI document',
			[Description] = 'POPI document',
			SourceFile = 'POPI_DOC.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'POPI document'
	end
go

if not exists(select * from Report where Name = 'SLA document')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('SLA document', 'SLA document', 'SLA.trdx', 0, 0, 1, 9,  3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'SLA document',
			[Description] = 'SLA document',
			SourceFile = 'SLA.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'SLA document'
	end
go

if not exists(select * from Report where Name = 'Insurer Data')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile,  IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 2, 3, 'Insurer Data', 'Insurer Data', 'InsurerData.trdx',  1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Insurer Data',
			[Description] = 'Insurer Data',
			SourceFile = 'InsurerData.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1
		where
			Name = 'Insurer Data'
	end
go

------------
--Parameters
------------

delete from ReportParam
dbcc checkident ('ReportParam', reseed, 0)

declare @ReportId int

if exists(select * from Report where Name = 'Comparative Quote')
	begin
		select @ReportId = Id from Report  where Name = 'Comparative Quote'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Channel Id', 'ChannelId', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Organization Code', 'OrganizationCode', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Proposal', 'ProposalHeaderId', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Quotes', 'QuoteIds', '', 2, 1, 1)
	end

if exists(select * from Report where Name = 'Agent Performance')
	begin
		select @ReportId = Id from Report  where Name = 'Agent Performance'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Agent Performance By Product')
	begin
		select @ReportId = Id from Report  where Name = 'Agent Performance By Product'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Insurer Quote Breakdown')
	begin
		select @ReportId = Id from Report  where Name = 'Insurer Quote Breakdown'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Lead Management')
	begin
		select @ReportId = Id from Report  where Name = 'Lead Management'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Target & Sales Management')
	begin
		select @ReportId = Id from Report  where Name = 'Target & Sales Management'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Debit order instruction')
	begin
		select @ReportId = Id from Report  where Name = 'Debit order instruction'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Party', 'PartyId', '', 2, 0, 0)
	end

if exists(select * from Report where Name = 'Record Of Advice')
	begin
		select @ReportId = Id from Report  where Name = 'Record Of Advice'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Party', 'PartyId', '', 2, 0, 0)
	end

if exists(select * from Report where Name = 'SLA document')
	begin
		select @ReportId = Id from Report  where Name = 'SLA document'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Party', 'PartyId', '', 2, 0, 0)
	end

if exists(select * from Report where Name = 'Broker Note')
	begin
		select @ReportId = Id from Report  where Name = 'Broker Note'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Insurer Code', 'InsurerCode', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Party', 'PartyId', '', 2, 0, 0)
	end

if exists(select * from Report where Name = 'Insurer Data')
	begin
		select @ReportId = Id from Report  where Name = 'Insurer Data'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Products', 'ProductIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformanceByProduct_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformanceByProduct_Header
	end
go

create procedure Report_CallCentre_AgentPerformanceByProduct_Header
(
	@CampaignID int
)
as
	select top 1 Code Insurer, '' Color from Channel where IsDefault = 1
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformanceByProduct_GetAgentQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformanceByProduct_GetAgentQuoteStatistics
	end
go

create procedure Report_CallCentre_AgentPerformanceByProduct_GetAgentQuoteStatistics
(
	@CampaignIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Insurers_CTE (InsurerId, CampaignId, Insurer, ProductId, Product, Campaign, ChannelId, AgentId, Channel, Agent)
			as 
			(
				select distinct
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Id,
					Product.Name,
					Campaign.Name,
					Channel.Id,
					[User].Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
					inner join Channel on Channel.Id = Campaign.ChannelId
					inner join [User] on [User].Id = LeadActivity.UserId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
					UserAuthorisationGroup.AuthorisationGroupId = 1 and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Id,
					Product.Name,
					Campaign.Name,
					Channel.Id,
					[User].Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName
				)
			select
				Channel,
				Campaign,
				Agent,
				Insurer,
				Product,
				(
					select
						isnull(count(distinct(LeadActivity.LeadId)), 0)
					from LeadActivity
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
				) Leads,
				(
					select
						isnull(count(distinct(QuoteHeader.Id)), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.Id = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					isnull(
						cast(
								(
									select
										isnull(count(distinct(QuoteHeader.Id)), 0)
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
										inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
										inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
										inner join Product on Product.Id = Quote.ProductId
										inner join Organization on Organization.PartyId = Product.ProductOwnerId
									where
										LeadActivity.CampaignId = Insurers_CTE.CampaignId and
										LeadActivity.UserId = Insurers_CTE.AgentId and
										Organization.PartyId = Insurers_CTE.InsurerId and
										Product.Id = Insurers_CTE.ProductId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo and
										QuoteHeader.IsRerated = 0 and
										Quote.IsDeleted = 0
								)
					
								/
					
								(
									select
										nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
									from LeadActivity
									where
										LeadActivity.CampaignId = Insurers_CTE.CampaignId and
										LeadActivity.UserId = Insurers_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo
								)

							as numeric(18,2)
						)
					, 0)
				) QuotesPerLead,
				(
					select
						isnull(count(Quote.Id), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.Id = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) SecondLevelUnderwriting,
				(
					select
						isnull(count(Quote.Id), 0)
					from LeadActivity
						inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.Id = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) Sales,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(Quote.Id), 0) as numeric( 18, 2))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) Closing,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(Quote.Id), 0) as numeric( 18, 2))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
									select
										nullif(cast(isnull(count(Quote.Id), 0) as numeric( 18, 2)), 0)
									from LeadActivity
										inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
										inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
										inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
										inner join Product on Product.Id = Quote.ProductId
										inner join Organization on Organization.PartyId = Product.ProductOwnerId
									where
										LeadActivity.CampaignId = Insurers_CTE.CampaignId and
										LeadActivity.UserId = Insurers_CTE.AgentId and
										Organization.PartyId = Insurers_CTE.InsurerId and
										Product.Id = Insurers_CTE.ProductId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo and
										Quote.IsDeleted = 0
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerClosing,
				(
					isnull(
						cast
						(
							
							(
								1
						
								-

								(
									( 
										select
											cast(isnull(count(Quote.Id), 0) as numeric( 18, 2))
										from LeadActivity
											inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
											inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
										where
											LeadActivity.CampaignId = Insurers_CTE.CampaignId and
											LeadActivity.UserId = Insurers_CTE.AgentId and
											Organization.PartyId = Insurers_CTE.InsurerId and
											Product.Id = Insurers_CTE.ProductId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0
									)

									/

									(
											select
												nullif(cast(isnull(count(Quote.Id), 0) as numeric( 18, 2)), 0)
											from LeadActivity
												inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
												inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
												inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
												inner join Product on Product.Id = Quote.ProductId
												inner join Organization on Organization.PartyId = Product.ProductOwnerId
											where
												LeadActivity.CampaignId = Insurers_CTE.CampaignId and
												LeadActivity.UserId = Insurers_CTE.AgentId and
												Organization.PartyId = Insurers_CTE.InsurerId and
												Product.Id = Insurers_CTE.ProductId and
												LeadActivity.DateUpdated >= @DateFrom and
												LeadActivity.DateUpdated <= @DateTo and
												Quote.IsDeleted = 0
									)
								)
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerDropOff,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from Quote
							inner join SoldLeadActivity on SoldLeadActivity.QuoteId = Quote.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join LeadActivity on LeadActivity.Id = SoldLeadActivity.LeadActivityId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							LeadActivity.UserId = Insurers_CTE.AgentId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.Id = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote
			from
				Insurers_CTE
			order by
				Channel,
				Campaign,
				Agent,
				Insurer,
				Product
		end
	else
		begin
			;with Insurers_CTE (InsurerId, CampaignId, Insurer, ProductId, Product, Campaign, ChannelId, AgentId, Channel, Agent)
			as 
			(
				select distinct
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Id,
					Product.Name,
					Campaign.Name,
					Channel.Id,
					[User].Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
					inner join Channel on Channel.Id = Campaign.ChannelId
					inner join [User] on [User].Id = LeadActivity.UserId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					UserAuthorisationGroup.AuthorisationGroupId = 1 and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Id,
					Product.Name,
					Campaign.Name,
					Channel.Id,
					[User].Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName
				)
			select
				Channel,
				Campaign,
				Agent,
				Insurer,
				Product,
				(
					select
						isnull(count(distinct(LeadActivity.LeadId)), 0)
					from LeadActivity
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
				) Leads,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.Id = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					isnull(
						cast(
								(
									select
										cast(isnull(count(distinct(Quote.Id)), 0) as numeric(18, 2))
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
										inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
										inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
										inner join Product on Product.Id = Quote.ProductId
										inner join Organization on Organization.PartyId = Product.ProductOwnerId
									where
										LeadActivity.CampaignId = Insurers_CTE.CampaignId and
										LeadActivity.UserId = Insurers_CTE.AgentId and
										Organization.PartyId = Insurers_CTE.InsurerId and
										Product.Id = Insurers_CTE.ProductId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo and
										QuoteHeader.IsRerated = 0 and
										Quote.IsDeleted = 0
								)
					
								/
					
								(
									select
										nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
									from LeadActivity
									where
										LeadActivity.CampaignId = Insurers_CTE.CampaignId and
										LeadActivity.UserId = Insurers_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo
								)

							as numeric(18,2)
						)
					, 0)
				) QuotesPerLead,
				(
					select
						isnull(count(Quote.Id), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.Id = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) SecondLevelUnderwriting,
				(
					select
						isnull(count(Quote.Id), 0)
					from LeadActivity
						inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.Id = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) Sales,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(Quote.Id), 0) as numeric( 18, 2))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) Closing,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(Quote.Id), 0) as numeric( 18, 2))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(Quote.Id), 0) as numeric( 18, 2)), 0)
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerClosing,
				(
					isnull(
						cast
						(
							
							(
								1
						
								-

								(
									( 
										select
											cast(isnull(count(Quote.Id), 0) as numeric( 18, 2))
										from LeadActivity
											inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
											inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
										where
											LeadActivity.CampaignId = Insurers_CTE.CampaignId and
											LeadActivity.UserId = Insurers_CTE.AgentId and
											Organization.PartyId = Insurers_CTE.InsurerId and
											Product.Id = Insurers_CTE.ProductId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0
									)

									/

									(
											select
												nullif(cast(isnull(count(Quote.Id), 0) as numeric( 18, 2)), 0)
											from LeadActivity
												inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
												inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
												inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
												inner join Product on Product.Id = Quote.ProductId
												inner join Organization on Organization.PartyId = Product.ProductOwnerId
											where
												LeadActivity.CampaignId = Insurers_CTE.CampaignId and
												LeadActivity.UserId = Insurers_CTE.AgentId and
												Organization.PartyId = Insurers_CTE.InsurerId and
												Product.Id = Insurers_CTE.ProductId and
												LeadActivity.DateUpdated >= @DateFrom and
												LeadActivity.DateUpdated <= @DateTo and
												Quote.IsDeleted = 0
									)
								)
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerDropOff,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from Quote
							inner join SoldLeadActivity on SoldLeadActivity.QuoteId = Quote.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join LeadActivity on LeadActivity.Id = SoldLeadActivity.LeadActivityId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							LeadActivity.UserId = Insurers_CTE.AgentId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.Id = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote
			from
				Insurers_CTE
			order by
				Channel,
				Campaign,
				Agent,
				Insurer,
				Product
		end
go

set nocount off
go

set noexec off
go