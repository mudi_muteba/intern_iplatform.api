﻿ALTER VIEW [dbo].[vw_rating_configuration]
AS
select
	   r.Id as Id
	  ,c.Id as ChannelId 
	  ,r.ChannelId as ChannelSystemId
	  ,r.ProductId
	  ,[Password] as RatingPassword
      ,[AgentCode] as RatingAgentCode
      ,[BrokerCode] as RatingBrokerCode
      ,[AuthCode] as RatingAuthCode
      ,[SchemeCode] as RatingSchemeCode
      ,[Token] as RatingToken
      ,r.[Environment] as RatingEnvironment
      ,[UserId] as RatingUserId
      ,r.[SubscriberCode] as RatingSubscriberCode
      ,[CompanyCode] as RatingCompanyCode
      ,[UwCompanyCode] as RatingUwCompanyCode
      ,[UwProductCode] as RatingUwProductCode
	  ,[CheckRequired] as ITCCheckRequired
      ,p.[SubscriberCode] as ITCSubscriberCode
      ,[BranchNumber] as ITCBranchNumber
      ,[BatchNumber] as ITCBatchNumber
      ,[SecurityCode] as ITCSecurityCode
      ,[EnquirerContactName] as ITCEnquirerContactName
      ,[EnquirerContactPhoneNo] as ITCEnquirerContactPhoneNo
      ,p.[Environment] as ITCEnvironment
	  ,o.Code As InsurerCode
	  ,r.ProductCode as ProductCode
	  ,r.IsDeleted
from SettingsiRate r
	inner join Channel c
		on r.ChannelId = c.SystemId
	inner join Product product
		on r.ProductId = product.Id
	inner join Organization o
		on product.ProductOwnerId = o.PartyId
	left join SettingsiPerson p
		on c.Id = p.ChannelId
where r.IsDeleted = 0
	and c.IsActive = 1

Go

