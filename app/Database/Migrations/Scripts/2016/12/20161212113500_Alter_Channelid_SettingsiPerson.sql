﻿Alter table SettingsiPerson Add ChannelSystemId UniqueIdentifier;
Go
Update SettingsiPerson Set ChannelSystemId = ChannelId;
go
Alter table SettingsiPerson drop column channelId
go
Alter table SettingsiPerson Add ChannelId int
go
Update SettingsiPerson Set ChannelId = ( Select Id FROM Channel WHERE SystemId = ChannelSystemId)
go
