﻿if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Covers') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Covers
	end
go

create procedure Reports_DetailedComparativeQuote_Covers
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50)
)
as
	;with Covers_CTE (CoverId, [Description])
	as 
	(
		select distinct
			md.Cover.Id CoverId,
			case
				when md.Cover.Name = 'AIG Assist'
				then 'VMI Assist'
			else
				md.Cover.Name
			end Description
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
			inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
		where
			ProposalHeader.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			CoverDefinition.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			Quote.Id in (select * from fn_StringListToTable(@QuoteIds))
		)
	select
		CoverId,
		[Description]
	from
		Covers_CTE
	order by
		[Description] asc
go


if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Assets_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_Assets_ByCover
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@CoverId int
)
as
	select distinct
		dbo.ToCamelCase(Asset.[Description]) [Description],
		QuoteItem.SumInsured Value
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Benefits_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Benefits_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_Benefits_ByCover
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@CoverId int
)
as
	declare
		@cols as nvarchar(max),
		@query as nvarchar(max)

	set @cols = 
		stuff
		(
			(
				select distinct
					',' + quotename(Organization.Code)
				from ProposalHeader
					inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
					inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
				where
					ProposalHeader.IsDeleted = 0 and
					ProposalDefinition.IsDeleted = 0 and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0 and
					Asset.IsDeleted = 0 and
					ProposalHeader.Id = @ProposalHeaderId and
					ProposalDefinition.CoverId = @CoverId and
					CoverDefinition.CoverId = @CoverId and
					Quote.Id in
					(
						select * from fn_StringListToTable(@QuoteIds)
					)
			for xml path(''), type)
		.value('.', 'nvarchar(max)' ), 1, 1, '')

	set @query =
	'
		select
			Description,
			' + @cols + '
		from
		(
			select
				Upper(Asset.Description) Description,
				Organization.Code Insurer
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join Product on Product.Id = Quote.ProductId
				inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
				ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				Quote.Id in
				(
					select * from fn_StringListToTable(''' + @QuoteIds + ''')
				)
		) x
		pivot 
		(
			max(Insurer)
			for Insurer in (' + @cols + ')
		) p

		union all

		select
			''Premium'' Description,
			' + @cols + '
		from
		(
			select
				Organization.Code Insurer,
				cast(QuoteItem.Premium as varchar) Premium
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join Product on Product.Id = Quote.ProductId
				inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
				ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				Quote.Id in
				(
					select * from fn_StringListToTable(''' + @QuoteIds + ''')
				)
		) x
		pivot 
		(
			max(Premium)
			for Insurer in (' + @cols + ')
		) p

		union all

		select
			''Basic Excess'' Description,
			' + @cols + '
		from
		(
			select
				Organization.Code Insurer,
				cast(QuoteItem.ExcessBasic as varchar) ExcessBasic
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join Product on Product.Id = Quote.ProductId
				inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
				ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				Quote.Id in
				(
					select * from fn_StringListToTable(''' + @QuoteIds + ''')
				)
		) x
		pivot 
		(
			max(ExcessBasic)
			for Insurer in (' + @cols + ')
		) p

		union all

		select
			''SASRIA'' Description,
			' + @cols + '
		from
		(
			select
				Organization.Code Insurer,
				cast(QuoteItem.Sasria as varchar) SASRIA
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join Product on Product.Id = Quote.ProductId
				inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
				ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				Quote.Id in
				(
					select * from fn_StringListToTable(''' + @QuoteIds + ''')
				)
		) x
		pivot 
		(
			max(SASRIA)
			for Insurer in (' + @cols + ')
		) p

		union all

		select
			''Total Payment'' Description,
			' + @cols + '
		from
		(
			select
				Organization.Code Insurer,
				cast((QuoteItem.Premium + QuoteItem.Sasria) as varchar) TotalPayment
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join Product on Product.Id = Quote.ProductId
				inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
				ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				Quote.Id in
				(
					select * from fn_StringListToTable(''' + @QuoteIds + ''')
				)
		) x
		pivot 
		(
			max(TotalPayment)
			for Insurer in (' + @cols + ')
		) p

		union all

		select
			Description,
			' + @cols + '
		from
		(
			select
				Organization.Code Insurer,
				ProductBenefit.Name Description,
				ProductBenefit.Value BenefitValue
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join Product on Product.Id = Quote.ProductId
				inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
			where
				ProductBenefit.Name is not null and
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
				ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
				Quote.Id in
				(
					select * from fn_StringListToTable(''' + @QuoteIds + ''')
				)
		) x
		pivot 
		(
			max(BenefitValue)
			for Insurer in (' + @cols + ')
		) p
	'

	execute(@query);
go