/*adjusting table column size,to accomodate the car hire tooltip size*/	
declare @tooltipSize int;
set @tooltipSize = (SELECT  character_maximum_length FROM information_schema.columns WHERE table_name = 'QuestionDefinition' AND column_name = 'ToolTip')

if (@tooltipSize<1000)
begin
ALTER TABLE [QuestionDefinition] ALTER COLUMN [ToolTip] ntext
end
