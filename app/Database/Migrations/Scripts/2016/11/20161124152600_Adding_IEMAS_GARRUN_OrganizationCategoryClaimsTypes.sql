

DECLARE @OrgId int;
SELECT @OrgId = PartyId FROM Organization WHERE Code  = 'IEMAS'
if @OrgId > 0
BEGIN
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,2, 2, 1, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,2, 1, 2, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,2, 14, 3, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,2, 36, 4, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,2, 24, 5, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,1, 8, 1, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,1, 30, 2, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,1, 32, 3, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,1, 39, 4, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,3, 37, 1, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,3, 29, 2, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,3, 36, 3, 0);
END
SET @OrgId = 0;
SELECT @OrgId = PartyId FROM Organization WHERE Code  = 'GARRUN'
if @OrgId > 0
BEGIN
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,2, 2, 1, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,2, 1, 2, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,2, 14, 3, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,2, 24, 5, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,1, 8, 1, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,1, 30, 2, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,1, 32, 3, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,1, 39, 4, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,3, 37, 1, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,3, 29, 2, 0);
	INSERT INTO OrganizationClaimTypeCategory(OrganizationId, ClaimsTypeCategoryId, ClaimsTypeId, VisibleIndex, IsDeleted) VALUES (@OrgId,3, 36, 3, 0);

END

DECLARE @ClaimTypeId int, @MyCursor CURSOR;

SELECT @OrgId = PartyId FROM Organization WHERE Code = 'IEMAS'

--delete previous exclusion list
DELETE FROM OrganizationClaimTypeExclusion WHERE OrganizationId = @OrgId;

SET @MyCursor = CURSOR FOR select Id FROM md.ClaimsType WHERE Id not in(2,1,14,36,24,8,30,32,39,37,29,36);  
OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @ClaimTypeId

	WHILE @@FETCH_STATUS = 0
	BEGIN

		if not exists(SELECT * FROM OrganizationClaimTypeExclusion WHERE OrganizationId = @OrgId AND ClaimsTypeId = @ClaimTypeId)
		BEGIN
			INSERT INTO OrganizationClaimTypeExclusion (ClaimsTypeId, OrganizationId, IsDeleted) VALUES (@ClaimTypeId, @OrgId, 0)
		END

		FETCH NEXT FROM @MyCursor INTO @ClaimTypeId
	END
		
CLOSE @MyCursor 
DEALLOCATE @MyCursor;

SELECT @OrgId = PartyId FROM Organization WHERE Code = 'GARRUN'

--delete previous exclusion list
DELETE FROM OrganizationClaimTypeExclusion WHERE OrganizationId = @OrgId;

SET @MyCursor = CURSOR FOR select Id FROM md.ClaimsType WHERE Id not in(2,1,14,24,8,30,32,39,37,29,36);  
OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @ClaimTypeId

	WHILE @@FETCH_STATUS = 0
	BEGIN

		if not exists(SELECT * FROM OrganizationClaimTypeExclusion WHERE OrganizationId = @OrgId AND ClaimsTypeId = @ClaimTypeId)
		BEGIN
			INSERT INTO OrganizationClaimTypeExclusion (ClaimsTypeId, OrganizationId, IsDeleted) VALUES (@ClaimTypeId, @OrgId, 0)
		END

		FETCH NEXT FROM @MyCursor INTO @ClaimTypeId
	END
		
CLOSE @MyCursor 
DEALLOCATE @MyCursor;









