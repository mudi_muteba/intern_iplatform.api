if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_BuildingExtract') and type in (N'P', N'PC'))
begin
	drop procedure Reports_AIG_BuildingExtract
end
go

create procedure Reports_AIG_BuildingExtract
(
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as


--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
--set @StartDate = '2016-08-01'
--set @EndDate = '2016-11-09'


DECLARE @tbl TABLE(ProposalDefinitionId int, PartyId int, LeadId int, ProposalHeaderId int, CoverId int, 
Channel nvarchar(max), QuoteExpired nvarchar(max), QuoteBound nvarchar(max), QuoteBindDate nvarchar(max), DateOfQuote nvarchar(max), 
QuoteTotalPremium decimal(38,2), QuoteMotorPremium decimal(38,2), QuoteHomePremium decimal(38,2), QuoteFuneralPremium decimal(38,2), QuoteOtherPremium decimal(38,2),
QuoteFees decimal(38,2), QuoteSasria decimal(38,2), QuoteIncepptionDate nvarchar(max), QuotePolicyNumber nvarchar(max), QuoteCreateDate nvarchar(max), QuoteCreateTime nvarchar(max), QuoteID int, VMSAID nvarchar(max), 
PropertyNumber int, CoverType nvarchar(max),
--AddressSuburb nvarchar(max), AddressPostCode nvarchar(max), AddressProvince nvarchar(max),
SASRIA decimal(38,2),
PremiumRisk decimal(38,2),
PremiumSubsidenceAndLandslip decimal(38,2),
PremiumWaterMachinery decimal(38,2),
PremiumTotal decimal(38,2))


insert into @tbl
SELECT 

prd.Id, p.Id, l.id , Prh.id , Prd.coverid,

CASE WHEN Prh.Source IS NULL THEN ('Call center') ELSE Prh.Source END as Channel,

CASE WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y' ELSE 'N' END as 'QuoteExpired',

(CASE 
 (SELECT COALESCE((select top 1 PolicyStatusId from PolicyHeader where PartyId = p.id and QuoteId = qute.Id and PolicyStatusId = 3), 0))
  when 3 then 'Y'
  when 2 then 'N'
  when 1 then 'N'
  when 0 then 'N'
End  )as 'QuoteBound',

	(CASE 
		(SELECT COALESCE((select top 1 PolicyStatusId from PolicyHeader where PartyId = p.id and QuoteId = qute.Id and PolicyStatusId = 3), 0))
			when 3 then CONVERT(VARCHAR(10),Qute.AcceptedOn ,111)
			when 2 then ''
			when 1 then ''
			when 0 then ''
	End  )as 'QuoteBindDate',

'' as 'DateOfQuote',

	case when (select sum(cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2))) + cast(round (Qute.Fees ,2) as numeric(36,2))  from QuoteItem 	
				where QuoteId = Qute.id) is null
	THEN (0) 
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2))) + cast(round (Qute.Fees ,2) as numeric(36,2))  from QuoteItem 	
				where QuoteId = Qute.id)                                          
	END 
	as 'QuoteTotalPremium',

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 218)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 218))                                          
	END AS 'QuoteMotorPremium',
	 
	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 44)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 44))                                          
	END AS 'QuoteHomePremium',

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 142)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 142))                                          
	END AS 'QuoteFuneralPremium',	

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId not in(218,78,142))) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId not in(218,78,142)))                                          
	END as 'QuoteOtherPremium',

	(cast(round(Qute.fees,2) as numeric(36,2))) as 'QuoteFees',

	(select sum(cast(round (Sasria ,2) as numeric(36,2)))  from QuoteItem 		
				where QuoteId = Qute.id) as 'QuoteSasria',

	(select top 1 CONVERT(VARCHAR(10),DateEffective ,111) from PolicyHeader where PartyId = p.Id and QuoteId = qute.Id and PolicyStatusId = 3) as 'QuoteIncepptionDate',

	(select COALESCE((select top 1 PolicyNo from PolicyHeader where PartyId = p.Id and QuoteId = qute.Id and PolicyStatusId = 3),null)) as 'QuotePolicyNumber',

	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'QuoteCreateDate',
	CONVERT(VARCHAR(5),Qute.CreatedAt,108) as 'QuoteCreateTime', 

	qute.Id as 'QuoteID',
	Ind.ExternalReference as 'VMSAID',	

	(select AssetNo from Asset where id = prd.AssetId) as 'PropertyNumber',
		
	(select DisplayName from CoverDefinition where CoverId =  prd.CoverId and ProductId = 46) as 'CoverType',	

	/*(select line1
				from Address as [add]
				join AssetAddress as astAdd on astAdd.AddressId = [add].Id				
				join ProposalDefinition as pr on pr.AssetId = astAdd.AssetId
				where astAdd.AssetId = prd.AssetId
				and pr.Id = prd.Id
				and [add].IsDeleted = 0) as 'AddressSuburb',

		(select [add].Code
				from Address as [add]
				join AssetAddress as astAdd on astAdd.AddressId = [add].Id				
				join ProposalDefinition as pr on pr.AssetId = astAdd.AssetId
				where astAdd.AssetId = prd.AssetId
				and pr.Id = prd.Id
				and [add].IsDeleted = 0) as 'AddressPostCode',

		(select sp.Name
				from Address as [add]
				join AssetAddress as astAdd on astAdd.AddressId = [add].Id
				join md.StateProvince as sp on sp.Id = [add].StateProvinceId
				join ProposalDefinition as pr on pr.AssetId = astAdd.AssetId
				where astAdd.AssetId = prd.AssetId
				and pr.Id = prd.Id
				and [add].IsDeleted = 0) as 'AddressProvince',*/

		(select total from QuoteItemBreakDown where QuoteItemId = qi.Id and Name = 'Sasria') 
		 as 'SASRIA',

		(select total from QuoteItemBreakDown where QuoteItemId = qi.Id and Name = 'Standard') 
		as 'PremiumRisk', 
		
		(select total from QuoteItemBreakDown where QuoteItemId = qi.Id and Name = 'Subsidence And Landslip') 
		 as 'PremiumSubsidenceAndLandslip',		

		(select total from QuoteItemBreakDown where QuoteItemId = qi.Id and Name = 'Water Pumping Machinery') 
		 as 'PremiumWaterMachinery',

		(select sum(cast(round (Total ,2) as numeric(36,2))) from QuoteItemBreakDown where QuoteItemId = qi.Id and Name in ('Standard', 'Water Pumping Machinery', 'Subsidence And Landslip', 'Accidental Damage'))
		 as 'PremiumTotal' 
	
--select * 
from ProposalHeader as Prh
join ProposalDefinition as prd on prd.ProposalHeaderId = Prh.Id and prd.CoverId = 44
join QuoteHeader as Qh on qh.ProposalHeaderId = Prh.Id
join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
JOIN Party as P on p.Id = Prh.PartyId
join Lead as L on l.PartyId = p.Id
left join Individual as Ind on P.Id = Ind.PartyId
join QuoteItem as qi on qi.QuoteId = qute.Id and qi.IsDeleted = 0
left join QuoteItemBreakDown as qibd on qibd.QuoteItemId = qi.Id and qibd.IsDeleted = 0
join CoverDefinition as covD on covD.Id = qi.CoverDefinitionId
inner join Asset as ast on ast.AssetNo = qi.AssetNumber and ast.Id = prd.AssetId
where ((CONVERT(VARCHAR(10),qute.CreatedAt ,121))  >= @StartDate AND (CONVERT(VARCHAR(10),qute.CreatedAt ,121)) <= @EndDate)
and L.IsDeleted = 0
and Qute.IsDeleted = 0
and Prh.IsDeleted = 0
and Qh.IsRerated = 0
and prd.IsDeleted = 0
and prd.CoverId in (44)
and covD.CoverId = 44 

group by Prh.Source, prd.Id , p.id, l.id,prh.id,prd.coverid,qute.createdat,qute.id,qute.acceptedon, qute.Fees, qh.CreatedAt, ind.ExternalReference, prd.AssetId, qi.Id

SELECT Channel  ,QuoteExpired, QuoteBound, QuoteBindDate, QuoteTotalPremium, QuoteMotorPremium, QuoteHomePremium, QuoteFuneralPremium, QuoteOtherPremium,QuoteFees,
QuoteSasria, QuoteIncepptionDate, QuotePolicyNumber, QuoteCreateDate, QuoteCreateTime, QuoteID, VMSAID, PropertyNumber, CoverType, 


--AddressSuburb, AddressPostCode, AddressProvince,

	(SELECT pa.Answer FROM ProposalQuestionAnswer pa
	JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
	WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 89)
	as 'AddressSuburb', 


	(SELECT pa.Answer FROM ProposalQuestionAnswer pa
	JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
	WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 90)
	as 'AddressPostCode', 


	(SELECT qa.Answer FROM ProposalQuestionAnswer pa
	JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
	JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
	WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 47)
	as 'AddressProvince',

(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 848)
		  as 'HomeType',		

		(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 410)
		 as 'RoofConstruction',

		 (SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 428)
		 as 'WallConstruction',

		(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 850)
		  as 'ThatchLapa',

		  (case 
			(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 851)
					when 'False' then 'N'
					when 'true' then 'Y'
		End)   as 'ThatchLapaSize',
		

		''  as 'LightningConductor',


		(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 855)
		as 'PropertyBorder',


		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 845)
		as 'ConstructionYear',

		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 849)
		as 'SquareMetresOfProperty',

		(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 846)
		  as 'NumberOfBathrooms',

		(case (SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 856)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End) as 'PropertyUnderConstruction',


		(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 847)
		     as 'HomeUsage',

		(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 853)
		     as 'BusinessConducted',

			 		(case (SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 854)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)   as 'Commune',

		(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 852)
		     as 'PeriodPropertyIsUnoccupied',

		case (SELECT isnumeric(pa.Answer) FROM ProposalQuestionAnswer pa
		JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
		WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 843)
		when 0 then (SELECT pa.Answer FROM ProposalQuestionAnswer pa
		JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
		WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 843)
		when 1 then ((SELECT qa.Answer FROM ProposalQuestionAnswer pa
		JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
		JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
		WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 843))
		End as 'Ownership',

		(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 844)
		as 'MortgageBank',

		(case (SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 396)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)   as 'Security-GatedCommunity',

	   (case (SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 398)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)   as 'Security-SecurityComplex',

	   (case (SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 386)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)  as 'Security-BurglarBars',

		(case (SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 416)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)  as 'Security-SecurityGates',

		(case (SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 402)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)  as 'Security-AlarmWithArmedResponse',

		(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 860)
		as 'VAPS-WaterPumpingMachinery',

		(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 859)
		as 'VAPS-AccidentalDamage',

		(case 
			(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 420 )
				when 'False' then 'N'
				when 'true' then 'Y'
		End)  as 'VAPS-SubsidenceAndLandslip',

		(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 857)
		as 'Buildings-Additional-Excess',

		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 102)
		as 'SumInsured',

		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 155)
		as 'DiscretionaryDiscount',


SASRIA, PremiumRisk ,PremiumSubsidenceAndLandslip ,PremiumWaterMachinery ,PremiumTotal

FROM @tbl as t