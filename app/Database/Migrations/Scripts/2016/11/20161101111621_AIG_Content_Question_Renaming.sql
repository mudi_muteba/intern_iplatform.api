﻿--Risk Address
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=88)
begin
update QuestionDefinition
set DisplayName = 'What’s the address of the home you want to insure?' 
where CoverDefinitionId = 146 and QuestionId=88
end

--Usage
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=847)
begin
update QuestionDefinition
set DisplayName = 'Is this your primary home or Holiday home?' 
where CoverDefinitionId = 146 and QuestionId=847
end

--Is your home financed or paid off?
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=843)
begin
update QuestionDefinition
set DisplayName = 'Do you own or are you renting the property?' 
where CoverDefinitionId = 146 and QuestionId=843
end

--home type
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=848)
begin
update QuestionDefinition
set DisplayName = 'Is your home a free standing house or in a Complex with access control, Small holding or Plot?',
	Tooltip = 'If plot or small holding: refer to the underwriter' 
where CoverDefinitionId = 146 and QuestionId=848
end

--subsidence & landslip
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=472)
begin
update questiondefinition
set displayname = 'do you want to include subsidence and landslip cover?' 
where coverdefinitionid = 146 and questionid=472
end

--Burglar bars in front of all opening windows
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=436)
begin
update QuestionDefinition
set DisplayName = 'Do you have burglar bars in front of all opening windows?' 
where CoverDefinitionId = 146 and QuestionId=436
end

--Security gates in front of all opening doors
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=468)
begin
update QuestionDefinition
set DisplayName = 'Do you have security gates in front of all opening doors?' 
where CoverDefinitionId = 146 and QuestionId=468
end

--Linked Alarm with armed response
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=454)
begin
update QuestionDefinition
set DisplayName = 'Do you have an alarm linked to armed response?' 
where CoverDefinitionId = 146 and QuestionId=454
end

--Roof Construction
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=462)
begin
update QuestionDefinition
set DisplayName = 'What type of roof do you have?' 
where CoverDefinitionId = 146 and QuestionId=462
end

--Wall Construction
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=480)
begin
update QuestionDefinition
set DisplayName = 'Are the walls of your house made of brick or cement?' 
where CoverDefinitionId = 146 and QuestionId=480
end

--Thatch Lapa
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=850)
begin
update QuestionDefinition
set DisplayName = 'Do you have a thatched lapa?Is it Attached or 5 metres or closer to the main house?' 
where CoverDefinitionId = 146 and QuestionId=850
end

--Thatch Lapa size > 25% of the area of the main building
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=851)
begin
update QuestionDefinition
set DisplayName = 'Is the thatched lapa more than 25% of the area of the main building?' 
where CoverDefinitionId = 146 and QuestionId=851
end

--Construction Year
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=845)
begin
update QuestionDefinition
set DisplayName = 'Approximately when was your home built?' 
where CoverDefinitionId = 146 and QuestionId=845
end

--Square Metres Of Property
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=849)
begin
update QuestionDefinition
set DisplayName = 'What is the size of the property in square meters?',
	ToolTip = 'This is the building size, excluding the land' 
where CoverDefinitionId = 146 and QuestionId=849
end

--Number Of Bathrooms
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=846)
begin
update QuestionDefinition
set DisplayName = 'How many bathrooms are there on the property?' 
where CoverDefinitionId = 146 and QuestionId=846
end

--Period Property Is Unoccupied
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=852)
begin
update QuestionDefinition
set DisplayName = 'How many days a year is your Home unoccupied?' 
where CoverDefinitionId = 146 and QuestionId=852
end

--Business Conducted
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=853)
begin
update QuestionDefinition
set DisplayName = 'Is business conducted on the property?If yes: What type of business is conducted on the property?' 
where CoverDefinitionId = 146 and QuestionId=853
end

--Accidental Damage
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=859)
begin
update QuestionDefinition
set DisplayName = 'Do you want to include accidental damage?' ,ToolTip='If yes: select limit'
where CoverDefinitionId = 146 and QuestionId=859
end


--Sum Insured
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=102)
begin
update QuestionDefinition
set DisplayName = 'What is the value that you would like to insure your contents for?' 
where CoverDefinitionId = 146 and QuestionId=102
end

--Exclude Theft Cover
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=394)
begin
update QuestionDefinition
set DisplayName = 'What is the value that you would like to insure your contents for?' 
where CoverDefinitionId = 146 and QuestionId=394
end

--Does your home boarder on vacant land, river or sea?
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=855)
begin
update QuestionDefinition
set DisplayName = 'Does your home border on vacant land, river or sea?If property’s border is sea/river or stream: Is the river or sea less than 300 metres from your property?' 
where CoverDefinitionId = 146 and QuestionId=855
end


--Contents Excess
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=858)
begin
update QuestionDefinition
set ToolTip='If yes: select limit'
where CoverDefinitionId = 146 and QuestionId=858
end

