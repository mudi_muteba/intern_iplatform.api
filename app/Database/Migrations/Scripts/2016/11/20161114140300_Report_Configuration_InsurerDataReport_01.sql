﻿if not exists(select * from Report where Name = 'Comparative Quote')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 1, 3, 'Comparative Quote', 'Comparative Quote', 'ComparativeQuote.trdx', 0, 0, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 1,
			ReportFormatId = 3,
			Name = 'Comparative Quote',
			[Description] = 'Comparative Quote',
			SourceFile = 'ComparativeQuote.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'Comparative Quote'
	end
go

if not exists(select * from Report where Name = 'Agent Performance')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 2, 3, 'Agent Performance', 'Agent Performance', 'AgentPerformance.trdx',  1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Agent Performance',
			[Description] = 'Agent Performance',
			SourceFile = 'AgentPerformance.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1
		where
			Name = 'Agent Performance'
	end
go

if not exists(select * from Report where Name = 'Insurer Quote Breakdown')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile,  IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 2, 3, 'Insurer Quote Breakdown', 'Insurer Quote Breakdown', 'InsurerQuoteBreakdown.trdx',  1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Insurer Quote Breakdown',
			[Description] = 'Insurer Quote Breakdown',
			SourceFile = 'InsurerQuoteBreakdown.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1
		where
			Name = 'Insurer Quote Breakdown'
	end
go

if not exists(select * from Report where Name = 'Lead Management')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile,  IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (2, 2, 3, 'Lead Management', 'Lead Management', 'LeadManagement.trdx',  1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 2,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Lead Management',
			[Description] = 'Lead Management',
			SourceFile = 'LeadManagement.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1
		where
			Name = 'Lead Management'
	end
go

if not exists(select * from Report where Name = 'Target & Sales Management')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile,  IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (2, 2, 3, 'Target & Sales Management', 'Target & Sales Management', 'TargetSalesManagement.trdx', 1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 2,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Target & Sales Management',
			[Description] = 'Target & Sales Management',
			SourceFile = 'TargetSalesManagement.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 0
		where
			Name = 'Target & Sales Management'
	end
go

if not exists(select * from Report where Name = 'Debit order instruction')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('Debit order instruction', 'Debit order instruction', 'Debit Order Instruction.trdx', 0, 0, 1, 5, 3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'Debit order instruction',
			[Description] = 'Debit order instruction',
			SourceFile = 'Debit Order Instruction.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'Debit order instruction'
	end
go

if not exists(select * from Report where Name = 'Record Of Advice')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex,  ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('Record Of Advice', 'Record Of Advice', 'Record Of Advice.trdx', 0, 0, 1, 6, 3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'Record Of Advice',
			[Description] = 'Record Of Advice',
			SourceFile = 'Record Of Advice.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'Record Of Advice'
	end
go

if not exists(select * from Report where Name = 'Broker Note')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('Broker Note', 'Broker Note', 'Broker''s Note.trdx', 0, 0, 1, 7,  3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'Broker Note',
			[Description] = 'Broker Note',
			SourceFile = 'Broker''s Note.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'Broker Note'
	end
go

if not exists(select * from Report where Name = 'POPI document')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('POPI document', 'POPI document', 'POPI_DOC.trdx', 0, 0, 1, 8, 3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'POPI document',
			[Description] = 'POPI document',
			SourceFile = 'POPI_DOC.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'POPI document'
	end
go

if not exists(select * from Report where Name = 'SLA document')
	begin
		insert into Report (Name, Description, SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, VisibleIndex, ReportCategoryId, ReportTypeId, ReportFormatId)
		values ('SLA document', 'SLA document', 'SLA.trdx', 0, 0, 1, 9, 3, 3, 3)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 3,
			ReportTypeId = 3,
			ReportFormatId = 3,
			Name = 'SLA document',
			[Description] = 'SLA document',
			SourceFile = 'SLA.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1
		where
			Name = 'SLA document'
	end
go

if not exists(select * from Report where Name = 'Insurer Data')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile,  IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 2, 3, 'Insurer Data', 'Insurer Data', 'InsurerData.trdx',  1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Insurer Data',
			[Description] = 'Insurer Data',
			SourceFile = 'InsurerData.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1
		where
			Name = 'Insurer Data'
	end
go

------------
--Parameters
------------

delete from ReportParam
dbcc checkident ('ReportParam', reseed, 0)

declare @ReportId int

if exists(select * from Report where Name = 'Comparative Quote')
	begin
		select @ReportId = Id from Report  where Name = 'Comparative Quote'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Channel Id', 'ChannelId', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Organization Code', 'OrganizationCode', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Proposal', 'ProposalHeaderId', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Quotes', 'QuoteIds', '', 2, 1, 1)
	end

if exists(select * from Report where Name = 'Agent Performance')
	begin
		select @ReportId = Id from Report  where Name = 'Agent Performance'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Insurer Quote Breakdown')
	begin
		select @ReportId = Id from Report  where Name = 'Insurer Quote Breakdown'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Lead Management')
	begin
		select @ReportId = Id from Report  where Name = 'Lead Management'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Target & Sales Management')
	begin
		select @ReportId = Id from Report  where Name = 'Target & Sales Management'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists(select * from Report where Name = 'Debit order instruction')
	begin
		select @ReportId = Id from Report  where Name = 'Debit order instruction'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Party', 'PartyId', '', 2, 0, 0)
	end

if exists(select * from Report where Name = 'Record Of Advice')
	begin
		select @ReportId = Id from Report  where Name = 'Record Of Advice'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Party', 'PartyId', '', 2, 0, 0)
	end

if exists(select * from Report where Name = 'SLA document')
	begin
		select @ReportId = Id from Report  where Name = 'SLA document'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Party', 'PartyId', '', 2, 0, 0)
	end

if exists(select * from Report where Name = 'Broker Note')
	begin
		select @ReportId = Id from Report  where Name = 'Broker Note'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Insurer Code', 'InsurerCode', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Party', 'PartyId', '', 2, 0, 0)
	end

if exists(select * from Report where Name = 'Insurer Data')
	begin
		select @ReportId = Id from Report  where Name = 'Insurer Data'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Campaigns', 'CampaignIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Products', 'ProductIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_Header
	end
go

create procedure Report_CallCentre_InsurerData_Header
(
	@CampaignID int
)
as
	select top 1 Code Insurer, '' Color from Channel where IsDefault = 1
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetInsurerQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetInsurerQuoteStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetInsurerQuoteStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign)
			as 
			(
				select distinct
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
				where
					LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
					Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
					QuoteHeader.IsDeleted = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0 --and
					--(
					--	Organization.RegisteredName like '%Auto & General%' or 
					--	Organization.RegisteredName like '%First for Women%' or 
					--	Organization.RegisteredName like '%Unity Insu%'
					--)
				group by
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name
				)
			select
				Insurer,
				Product,
				Campaign,
				(
					select
						isnull(count(LeadActivity.Id), 0)
					from LeadActivity							
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					select
						isnull(count(LeadActivity.Id), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteHeader.IsDeleted = 0
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteHeader.IsDeleted = 0
							)
							/
							(
								select
									case when isnull(count(LeadActivity.Id), 0) = 0 then 1
									else cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0)) end
								from LeadActivity							
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsDeleted = 0 and
									Quote.IsDeleted = 0
							)
						)
						* 100
						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						cast(isnull(avg(QuoteItem.Premium), 0) as numeric(18, 2))
					from Quote
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.QuoteId = Quote.Id
						inner join LeadActivity on LeadActivity.Id = QuoteAcceptedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0 and
						QuoteHeader.IsDeleted = 0
				) AveragePremiumPerAcceptedQuote,
				(
					select
						cast(isnull(avg(QuoteItem.Premium), 0) as numeric(18, 2))
					from QuoteHeader
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0 and
						QuoteHeader.IsDeleted = 0
				) AveragePremiumPerQuote
			from
				Insurers_CTE
			order by
				Campaign,
				Insurer,
				Product
		end
	else
		begin
			;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign)
			as 
			(
				select distinct
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
				where
					QuoteHeader.IsDeleted = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name
				)
			select
				Insurer,
				Product,
				Campaign,
				(
					select
						isnull(count(LeadActivity.Id), 0)
					from LeadActivity							
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					select
						isnull(count(LeadActivity.Id), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteHeader.IsDeleted = 0
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									Organization.PartyId = Insurers_CTE.InsurerId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteHeader.IsDeleted = 0
							)
							/
							(
								select
									case when isnull(count(LeadActivity.Id), 0) = 0 then 1
									else cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0)) end
								from LeadActivity							
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									Organization.PartyId = Insurers_CTE.InsurerId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsDeleted = 0 and
									Quote.IsDeleted = 0
							)
						)
						* 100
						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						cast(isnull(avg(QuoteItem.Premium), 0) as numeric(18, 2))
					from Quote
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.QuoteId = Quote.Id
						inner join LeadActivity on LeadActivity.Id = QuoteAcceptedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0 and
						QuoteHeader.IsDeleted = 0
				) AveragePremiumPerAcceptedQuote,
				(
					select
						cast(isnull(avg(QuoteItem.Premium), 0) as numeric(18, 2))
					from QuoteHeader
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) AveragePremiumPerQuote
			from
				Insurers_CTE
			order by
				Campaign,
				Insurer,
				Product
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetAgeStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetAgeStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetAgeStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @AgeRangeTotal numeric(18, 2)
	declare @AgeRange18To20 numeric(18, 2)
	declare @AgeRange21To25 numeric(18, 2)
	declare @AgeRange26To30 numeric(18, 2)
	declare @AgeRange31To35 numeric(18, 2)
	declare @AgeRange36To40 numeric(18, 2)
	declare @AgeRange41To45 numeric(18, 2)
	declare @AgeRange46To50 numeric(18, 2)
	declare @AgeRange51To55 numeric(18, 2)
	declare @AgeRange56Plus numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@AgeRangeTotal = 
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo
						),
						@AgeRange18To20 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 18 and 20
						),
						@AgeRange21To25 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 21 and 25
						),
						@AgeRange26To30 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 26 and 30
						),
						@AgeRange31To35 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 31 and 35
						),
						@AgeRange36To40 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 36 and 40
						),
						@AgeRange41To45 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 41 and 45
						),
						@AgeRange46To50 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 46 and 50
						),
						@AgeRange51To55 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 51 and 55
						),
						@AgeRange56Plus =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) > 55
						)
									
					select '18-20' Label, '18-20' Category, @AgeRange18To20 Total, (@AgeRange18To20 / @AgeRangeTotal * 100) [Percentage] union all
					select '21-25' Label, '21-25' Category, @AgeRange21To25 Total, (@AgeRange21To25 / @AgeRangeTotal * 100) [Percentage] union all
					select '26-30' Label, '26-30' Category, @AgeRange26To30 Total, (@AgeRange26To30 / @AgeRangeTotal * 100) [Percentage] union all
					select '31-35' Label, '31-35' Category, @AgeRange31To35 Total, (@AgeRange31To35 / @AgeRangeTotal * 100) [Percentage] union all
					select '36-40' Label, '36-40' Category, @AgeRange36To40 Total, (@AgeRange36To40 / @AgeRangeTotal * 100) [Percentage] union all
					select '41-45' Label, '41-45' Category, @AgeRange41To45 Total, (@AgeRange41To45 / @AgeRangeTotal * 100) [Percentage] union all
					select '46-50' Label, '46-50' Category, @AgeRange46To50 Total, (@AgeRange46To50 / @AgeRangeTotal * 100) [Percentage] union all
					select '51-55' Label, '51-55' Category, @AgeRange51To55 Total, (@AgeRange51To55 / @AgeRangeTotal * 100) [Percentage] union all
					select '56+' Label, '56+' Category, @AgeRange56Plus Total, (@AgeRange56Plus / @AgeRangeTotal * 100) [Percentage]
				end
			else
				begin
					select
						@AgeRangeTotal = 
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo
						),
						@AgeRange18To20 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 18 and 20
						),
						@AgeRange21To25 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 21 and 25
						),
						@AgeRange26To30 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 26 and 30
						),
						@AgeRange31To35 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 31 and 35
						),
						@AgeRange36To40 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 36 and 40
						),
						@AgeRange41To45 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 41 and 45
						),
						@AgeRange46To50 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 46 and 50
						),
						@AgeRange51To55 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 51 and 55
						),
						@AgeRange56Plus =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) > 55
						)
									
					select '18-20' Label, '18-20' Category, @AgeRange18To20 Total, (@AgeRange18To20 / @AgeRangeTotal * 100) [Percentage] union all
					select '21-25' Label, '21-25' Category, @AgeRange21To25 Total, (@AgeRange21To25 / @AgeRangeTotal * 100) [Percentage] union all
					select '26-30' Label, '26-30' Category, @AgeRange26To30 Total, (@AgeRange26To30 / @AgeRangeTotal * 100) [Percentage] union all
					select '31-35' Label, '31-35' Category, @AgeRange31To35 Total, (@AgeRange31To35 / @AgeRangeTotal * 100) [Percentage] union all
					select '36-40' Label, '36-40' Category, @AgeRange36To40 Total, (@AgeRange36To40 / @AgeRangeTotal * 100) [Percentage] union all
					select '41-45' Label, '41-45' Category, @AgeRange41To45 Total, (@AgeRange41To45 / @AgeRangeTotal * 100) [Percentage] union all
					select '46-50' Label, '46-50' Category, @AgeRange46To50 Total, (@AgeRange46To50 / @AgeRangeTotal * 100) [Percentage] union all
					select '51-55' Label, '51-55' Category, @AgeRange51To55 Total, (@AgeRange51To55 / @AgeRangeTotal * 100) [Percentage] union all
					select '56+' Label, '56+' Category, @AgeRange56Plus Total, (@AgeRange56Plus / @AgeRangeTotal * 100) [Percentage]
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@AgeRangeTotal = 
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo
						),
						@AgeRange18To20 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 18 and 20
						),
						@AgeRange21To25 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 21 and 25
						),
						@AgeRange26To30 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 26 and 30
						),
						@AgeRange31To35 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 31 and 35
						),
						@AgeRange36To40 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 36 and 40
						),
						@AgeRange41To45 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 41 and 45
						),
						@AgeRange46To50 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 46 and 50
						),
						@AgeRange51To55 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 51 and 55
						),
						@AgeRange56Plus =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) > 55
						)
									
					select '18-20' Label, '18-20' Category, @AgeRange18To20 Total, (@AgeRange18To20 / @AgeRangeTotal * 100) [Percentage] union all
					select '21-25' Label, '21-25' Category, @AgeRange21To25 Total, (@AgeRange21To25 / @AgeRangeTotal * 100) [Percentage] union all
					select '26-30' Label, '26-30' Category, @AgeRange26To30 Total, (@AgeRange26To30 / @AgeRangeTotal * 100) [Percentage] union all
					select '31-35' Label, '31-35' Category, @AgeRange31To35 Total, (@AgeRange31To35 / @AgeRangeTotal * 100) [Percentage] union all
					select '36-40' Label, '36-40' Category, @AgeRange36To40 Total, (@AgeRange36To40 / @AgeRangeTotal * 100) [Percentage] union all
					select '41-45' Label, '41-45' Category, @AgeRange41To45 Total, (@AgeRange41To45 / @AgeRangeTotal * 100) [Percentage] union all
					select '46-50' Label, '46-50' Category, @AgeRange46To50 Total, (@AgeRange46To50 / @AgeRangeTotal * 100) [Percentage] union all
					select '51-55' Label, '51-55' Category, @AgeRange51To55 Total, (@AgeRange51To55 / @AgeRangeTotal * 100) [Percentage] union all
					select '56+' Label, '56+' Category, @AgeRange56Plus Total, (@AgeRange56Plus / @AgeRangeTotal * 100) [Percentage]
				end
			else
				begin
					select
						@AgeRangeTotal = 
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo
						),
						@AgeRange18To20 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 18 and 20
						),
						@AgeRange21To25 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 21 and 25
						),
						@AgeRange26To30 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 26 and 30
						),
						@AgeRange31To35 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 31 and 35
						),
						@AgeRange36To40 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 36 and 40
						),
						@AgeRange41To45 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 41 and 45
						),
						@AgeRange46To50 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 46 and 50
						),
						@AgeRange51To55 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 51 and 55
						),
						@AgeRange56Plus =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								QuoteHeader.IsDeleted = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) > 55
						)
									
					select '18-20' Label, '18-20' Category, @AgeRange18To20 Total, (@AgeRange18To20 / @AgeRangeTotal * 100) [Percentage] union all
					select '21-25' Label, '21-25' Category, @AgeRange21To25 Total, (@AgeRange21To25 / @AgeRangeTotal * 100) [Percentage] union all
					select '26-30' Label, '26-30' Category, @AgeRange26To30 Total, (@AgeRange26To30 / @AgeRangeTotal * 100) [Percentage] union all
					select '31-35' Label, '31-35' Category, @AgeRange31To35 Total, (@AgeRange31To35 / @AgeRangeTotal * 100) [Percentage] union all
					select '36-40' Label, '36-40' Category, @AgeRange36To40 Total, (@AgeRange36To40 / @AgeRangeTotal * 100) [Percentage] union all
					select '41-45' Label, '41-45' Category, @AgeRange41To45 Total, (@AgeRange41To45 / @AgeRangeTotal * 100) [Percentage] union all
					select '46-50' Label, '46-50' Category, @AgeRange46To50 Total, (@AgeRange46To50 / @AgeRangeTotal * 100) [Percentage] union all
					select '51-55' Label, '51-55' Category, @AgeRange51To55 Total, (@AgeRange51To55 / @AgeRangeTotal * 100) [Percentage] union all
					select '56+' Label, '56+' Category, @AgeRange56Plus Total, (@AgeRange56Plus / @AgeRangeTotal * 100) [Percentage]
				end
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetClassUseStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetClassUseStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetClassUseStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer	
				end	
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetCoverTypeStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetCoverTypeStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetCoverTypeStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2))  Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer	
				end	
		end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetGenderStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetGenderStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetGenderStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = isnull(count(md.Gender.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.Gender.Name Label,
						md.Gender.Name Category,
						cast(count(md.Gender.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.Gender.Id,
						md.Gender.Name
				end
			else
				begin
					select
						@Total = isnull(count(md.Gender.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.Gender.Name Label,
						md.Gender.Name Category,
						cast(count(md.Gender.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.Gender.Id,
						md.Gender.Name
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = isnull(count(md.Gender.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.Gender.Name Label,
						md.Gender.Name Category,
						cast(count(md.Gender.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.Gender.Id,
						md.Gender.Name
				end
			else
				begin
					select
						@Total = isnull(count(md.Gender.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.Gender.Name Label,
						md.Gender.Name Category,
						cast(count(md.Gender.Id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.Gender.Id,
						md.Gender.Name
				end
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetGeneralStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetGeneralStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetGeneralStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @AverageNoClaimBonus numeric(18, 2)
	declare @AverageSumInsured numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@AverageNoClaimBonus = avg(cast(isnull(md.QuestionAnswer.Answer, 0) as numeric(18, 2)))
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 43 and --No Claim Bonus
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1 and
						isnumeric(md.QuestionAnswer.Answer) = 1

					select
						@AverageSumInsured = avg(cast(isnull(ProposalQuestionAnswer.Answer, 0) as numeric(18, 2)))
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 102 and --Sum Insured
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured
					union all
					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured
				end
			else
				begin
					select
						@AverageNoClaimBonus = avg(cast(isnull(md.QuestionAnswer.Answer, 0) as numeric(18, 2)))
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 43 and --No Claim Bonus
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1 and
						isnumeric(md.QuestionAnswer.Answer) = 1

					select
						@AverageSumInsured = avg(cast(isnull(ProposalQuestionAnswer.Answer, 0) as numeric(18, 2)))
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 102 and --Sum Insured
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured
					union all
					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@AverageNoClaimBonus = avg(cast(isnull(md.QuestionAnswer.Answer, 0) as numeric(18, 2)))
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 43 and --No Claim Bonus
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1 and
						isnumeric(md.QuestionAnswer.Answer) = 1

					select
						@AverageSumInsured = avg(cast(isnull(ProposalQuestionAnswer.Answer, 0) as numeric(18, 2)))
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 102 and --Sum Insured
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured
					union all
					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured
				end
			else
				begin
					select
						@AverageNoClaimBonus = avg(cast(isnull(md.QuestionAnswer.Answer, 0) as numeric(18, 2)))
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 43 and --No Claim Bonus
						QuoteHeader.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1 and
						isnumeric(md.QuestionAnswer.Answer) = 1

					select
						@AverageSumInsured = avg(cast(isnull(ProposalQuestionAnswer.Answer, 0) as numeric(18, 2)))
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 102 and --Sum Insured
						QuoteHeader.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured
					union all
					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured
				end
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetInsuranceTypeStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetInsuranceTypeStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetInsuranceTypeStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.LossHistoryQuestionAnswer.Id)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Label,
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Category,
						cast(count(md.LossHistoryQuestionAnswer.Id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.LossHistoryQuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.LossHistoryQuestionAnswer.Id)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Label,
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Category,
						cast(count(md.LossHistoryQuestionAnswer.Id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.LossHistoryQuestionAnswer.Answer
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.LossHistoryQuestionAnswer.Id)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Label,
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Category,
						cast(count(md.LossHistoryQuestionAnswer.Id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.LossHistoryQuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.LossHistoryQuestionAnswer.Id)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Label,
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Category,
						cast(count(md.LossHistoryQuestionAnswer.Id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.LossHistoryQuestionAnswer.Answer
				end
		end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetMaritalStatusStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetMaritalStatusStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetMaritalStatusStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = isnull(count(md.MaritalStatus.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.MaritalStatus.Name Label,
						md.MaritalStatus.Name Category,
						cast(count(md.MaritalStatus.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.MaritalStatus.Id,
						md.MaritalStatus.Name
				end
			else
				begin
					select
						@Total = isnull(count(md.MaritalStatus.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.MaritalStatus.Name Label,
						md.MaritalStatus.Name Category,
						cast(count(md.MaritalStatus.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.MaritalStatus.Id,
						md.MaritalStatus.Name
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = isnull(count(md.MaritalStatus.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.MaritalStatus.Name Label,
						md.MaritalStatus.Name Category,
						cast(count(md.MaritalStatus.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.MaritalStatus.Id,
						md.MaritalStatus.Name
				end
			else
				begin
					select
						@Total = isnull(count(md.MaritalStatus.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.MaritalStatus.Name Label,
						md.MaritalStatus.Name Category,
						cast(count(md.MaritalStatus.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.MaritalStatus.Id,
						md.MaritalStatus.Name
				end
		end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetProvinceStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetProvinceStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetProvinceStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = isnull(count([Address].Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.StateProvince.Name Label,
						md.StateProvince.Name Category,
						cast(count(md.StateProvince.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.StateProvince.Id,
						md.StateProvince.Name
				end
			else
				begin
					select
						@Total = isnull(count([Address].Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.StateProvince.Name Label,
						md.StateProvince.Name Category,
						cast(count(md.StateProvince.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.StateProvince.Id,
						md.StateProvince.Name
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = isnull(count([Address].Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.StateProvince.Name Label,
						md.StateProvince.Name Category,
						cast(count(md.StateProvince.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.StateProvince.Id,
						md.StateProvince.Name
				end
			else
				begin
					select
						@Total = isnull(count([Address].Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.StateProvince.Name Label,
						md.StateProvince.Name Category,
						cast(count(md.StateProvince.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.StateProvince.Id,
						md.StateProvince.Name
				end
		end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetVehicleColourStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetVehicleColourStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetVehicleColourStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
			else
				begin
					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
			else
				begin
					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer	
				end	
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetVehicleMakeStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetVehicleMakeStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetVehicleMakeStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(ProposalQuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null

					select
						ProposalQuestionAnswer.Answer Label,
						ProposalQuestionAnswer.Answer Category,
						cast(count(ProposalQuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null
					group by
						ProposalQuestionAnswer.Answer
				end
			else
				begin
					select
						ProposalQuestionAnswer.Answer Label,
						ProposalQuestionAnswer.Answer Category,
						cast(count(ProposalQuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null
					group by
						ProposalQuestionAnswer.Answer
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						ProposalQuestionAnswer.Answer Label,
						ProposalQuestionAnswer.Answer Category,
						cast(count(ProposalQuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null
					group by
						ProposalQuestionAnswer.Answer
				end
			else
				begin
					select
						ProposalQuestionAnswer.Answer Label,
						ProposalQuestionAnswer.Answer Category,
						cast(count(ProposalQuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null
					group by
						ProposalQuestionAnswer.Answer
				end	
		end
go