if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics
	end
go

create procedure Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics
(
	@CampaignIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
			as 
			(
				select distinct
					[User].Id,
					Campaign.Id,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				from Campaign
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					Campaign.Id in (select * from fn_StringListToTable(@CampaignIds)) and
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
					UserAuthorisationGroup.AuthorisationGroupId = 1
				group by
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName
				)
			select
				Agent,
				Campaign,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) SecondLevelUnderwriting,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
								select
									case when isnull(count(LeadActivity.Id), 0) = 0 then 1
									else cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0)) end
								from LeadActivity
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsDeleted = 0 and
									Quote.IsDeleted = 0
							)
						)

						* 100

						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from Quote
							inner join SoldLeadActivity on SoldLeadActivity.QuoteId = Quote.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join LeadActivity on LeadActivity.Id = SoldLeadActivity.LeadActivityId
						where
							LeadActivity.UserId = Agents_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote
			from
				Agents_CTE
			order by
				Campaign,
				Agent
		end
	else
		begin
			;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
			as 
			(
				select distinct
					[User].Id,
					Campaign.Id,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				from Campaign
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
					UserAuthorisationGroup.AuthorisationGroupId = 1
				group by
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName
				)
			select
				Agent,
				Campaign,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) SecondLevelUnderwriting,
				(
					select
						isnull(count(LeadActivity.ActivityTypeId), 0)
					from LeadActivity
						inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0
							)

							/

							(
								select
									case when isnull(count(LeadActivity.Id), 0) = 0 then 1
									else cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0)) end
								from LeadActivity
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsDeleted = 0 and
									Quote.IsDeleted = 0
							)
						)

						* 100

						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from Quote
							inner join SoldLeadActivity on SoldLeadActivity.QuoteId = Quote.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join LeadActivity on LeadActivity.Id = SoldLeadActivity.LeadActivityId
						where
							LeadActivity.UserId = Agents_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote
			from
				Agents_CTE
			order by
				Campaign,
				Agent
		end
go