﻿--Main Driver Gender
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=70)
begin
update QuestionDefinition
set DisplayName = 'What is the gender of the regular driver?' 
where CoverDefinitionId = 145 and QuestionId=70
end

--Main Driver ID Number
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=104)
begin
update QuestionDefinition
set DisplayName = 'What is the ID number of the regular driver?' 
where CoverDefinitionId = 145 and QuestionId=104
end

--Main Driver Title
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=146)
begin
update QuestionDefinition
set DisplayName = 'What is the title of the regular driver?' 
where CoverDefinitionId = 145 and QuestionId=146
end

--Does the license restrict you to driving an automatic car?
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=224)
begin
update QuestionDefinition
set DisplayName = 'Does the license restrict the main driver to driving an automatic car?' 
where CoverDefinitionId = 145 and QuestionId=224
end

--Class Of Use
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=226)
begin
update QuestionDefinition
set DisplayName = 'What will the car be used for on a day to day basis? Private, Professional or Full Business?' ,ToolTip = 'Professional: Occasional business trips (Less than 10 times)
Full Business: Frequent business trips'
where CoverDefinitionId = 145 and QuestionId=226
end

--Color
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=227)
begin
update QuestionDefinition
set DisplayName = 'What is the colour of your car?' 
where CoverDefinitionId = 145 and QuestionId=227
end

--Cover Type
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=230)
begin
update QuestionDefinition
set DisplayName = 'What type of a cover would you like us to arrange for you today? Comprehensive, Total loss or Third Party, Fire and Theft?' 
where CoverDefinitionId = 145 and QuestionId=230
end

--License Type
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=249)
begin
update QuestionDefinition
set DisplayName = 'What is the license type of regular driver?' 
where CoverDefinitionId = 145 and QuestionId=249
end

--Date License Obtained
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=41)
begin
update QuestionDefinition
set DisplayName = 'When did you first receive your license?' 
where CoverDefinitionId = 145 and QuestionId=41
end

--Financed
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=862)
begin
update QuestionDefinition
set DisplayName = 'Is the car financed?',ToolTip='If the car is financed Offer Credit Shortfall  and explain the benefits' 
where CoverDefinitionId = 145 and QuestionId=862	
end

--Overnight Address
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=140)
begin
update QuestionDefinition
set DisplayName = 'Where do you park your car overnight?' 
where CoverDefinitionId = 145 and QuestionId=140	
end

--Stored At Night
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=863)
begin
update QuestionDefinition
set DisplayName = 'Is your car parked in a garage overnight?
Or is it parked Behind Closed Gates, Enclosed Locked, Garage, Parked in Street, Unsecured Parking, Other?' 
where CoverDefinitionId = 145 and QuestionId=863
end

--Daytime Address
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=139)
begin
update QuestionDefinition
set DisplayName = 'Where is your car parked during the day?' 
where CoverDefinitionId = 145 and QuestionId=139
end

--Stored During Day
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=864)
begin
update QuestionDefinition
set DisplayName = 'Is your car parked in a garage during the day?
Or is it parked Behind Closed Gates, Enclosed Locked, Garage, Parked in Street, Unsecured Parking, Other?' 
where CoverDefinitionId = 145 and QuestionId=864
end

--Vehicle Registration Number
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=97)
begin
update QuestionDefinition
set DisplayName = 'What is the registration number?'
where CoverDefinitionId = 145 and QuestionId=97
end

--Type Of Vehicle
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=786)
begin
update QuestionDefinition
set DisplayName = 'What is the vehicle type you would like to insure? Is it a Car, Motorcycle, Caravan, Trailer?'
where CoverDefinitionId = 145 and QuestionId=786
end

--Year Of Manufacture
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=99)
begin
update QuestionDefinition
set DisplayName = 'What is the year of manufacture?'
where CoverDefinitionId = 145 and QuestionId=99
end

--Vehicle Make
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=95)
begin
update QuestionDefinition
set DisplayName = 'What is the make and model of the car?'
where CoverDefinitionId = 145 and QuestionId=95
end

--Make of Trailer / Caravan
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=294)
begin
update QuestionDefinition
set DisplayName = 'What is the make of the trailer / Caravan?'
where CoverDefinitionId = 145 and QuestionId=294
end

--Vin Number
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=310)
begin
update QuestionDefinition
set DisplayName = 'What is your Vin Number? (New cars only)?',ToolTip = 'This is only a requirement if the vehicle is still at the dealership'
where CoverDefinitionId = 145 and QuestionId=310
end

--Has the vehicle been modified to enhance performance
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=255)
begin
update QuestionDefinition
set DisplayName = 'Have you modified your car in any way to make it go faster?'
where CoverDefinitionId = 145 and QuestionId=255
end

--Vehicle Status
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=867)
begin
update QuestionDefinition
set DisplayName = 'Is the car/motorcycle/caravan/trailer new or used?'
where CoverDefinitionId = 145 and QuestionId=867
end

--Would you like a discounted tracking device to be fitted
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=298)
begin
update QuestionDefinition
set DisplayName = 'VMSA has partnered with Altech Netstar and we can arrange a discounted tracking device for you, would you like us to arrange this?'
where CoverDefinitionId = 145 and QuestionId=298
end

--Vehicle Extras Value
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=107)
begin
update QuestionDefinition
set DisplayName = 'Do you have any accessories or extras like a sunroof or a sound system or anything else fitted in your car?'
where CoverDefinitionId = 145 and QuestionId=107
end

/*adjusting table column size,to accomodate the car hire tooltip size*/	
declare @tooltipSize int;
set @tooltipSize = (SELECT  character_maximum_length FROM information_schema.columns WHERE table_name = 'QuestionDefinition' AND column_name = 'ToolTip')

if (@tooltipSize<650)
begin
ALTER TABLE [QuestionDefinition] ALTER COLUMN [ToolTip] nvarchar(650)
end



--Car Hire	
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=1005)
begin
update QuestionDefinition
set ToolTip = 'A valuable benefit that you get with Virgin Money insurance is that our car hire option can be used for up to 30 days. What this means for you, is that, if your car is in an accident or stolen, we’ll hire out a like for like vehicle for you to get around in.
- This means you’ll get a rental car in the same category as your own car – so you don’t have to settle for less.  That’s not all; we’ll also provide you with a rental car if your car breaks down or goes in for a regular service. That’s a great benefit, wouldn’t you agree?
This unique car hire option is for as little as R125 a month shall I include this in your quote'
where CoverDefinitionId = 145 and QuestionId=1005
end

--Marital Status	
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=251)
begin
update QuestionDefinition
set ToolTip = 'What is the marital status of the regular driver?'
where CoverDefinitionId = 145 and QuestionId=251
end

--Tracking device / Early warning	
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=870)
begin
update QuestionDefinition
set ToolTip = 'Does your vehicle have a tracking device?'
where CoverDefinitionId = 145 and QuestionId=870
end


--Scratch And Dent	
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=1003)
begin
update QuestionDefinition
set DisplayName = 'Would you like to include Tyre and Rim to your cover?',ToolTip=''
where CoverDefinitionId = 145 and QuestionId=1003
end

--Tyre And Rim	
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=1004)
begin
update QuestionDefinition
set DisplayName = 'Would you like to include Tyre and Rim to your cover?',ToolTip=''
where CoverDefinitionId = 145 and QuestionId=1004
end

