if exists (select * from sys.objects where object_id = object_id('dbo.ProposalQuestionAnswer') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='ProposalQuestionAnswerIndex' AND object_id = OBJECT_ID('ProposalQuestionAnswer'))
		begin
			drop INDEX ProposalQuestionAnswerIndex ON ProposalQuestionAnswer
			CREATE INDEX ProposalQuestionAnswerIndex
			ON ProposalQuestionAnswer (QuestionDefinitionId, ProposalDefinitionId)
		end

	if not exists (select * FROM sys.indexes WHERE name='ProposalQuestionAnswerIndex' AND object_id = OBJECT_ID('ProposalQuestionAnswer'))
		begin			
			CREATE INDEX ProposalQuestionAnswerIndex
			ON ProposalQuestionAnswer (QuestionDefinitionId, ProposalDefinitionId)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.QuestionDefinition') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='QuestionDefinitionIndex' AND object_id = OBJECT_ID('QuestionDefinition'))
		begin
			drop INDEX QuestionDefinitionIndex ON QuestionDefinition
			CREATE INDEX QuestionDefinitionIndex
			ON QuestionDefinition (Id)
		end

	if not exists (select * FROM sys.indexes WHERE name='QuestionDefinitionIndex' AND object_id = OBJECT_ID('QuestionDefinition'))
		begin			
			CREATE INDEX QuestionDefinitionIndex
			ON QuestionDefinition (Id)
		end
end
go