﻿--Scratch And Dent	
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=1003)
begin
update QuestionDefinition
set DisplayName = 'Would you like to include Scratch and Dent to your cover?',ToolTip=''
where CoverDefinitionId = 145 and QuestionId=1003
end

--Tyre And Rim	
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=1004)
begin
update QuestionDefinition
set DisplayName = 'Would you like to include Tyre and Rim to your cover?',ToolTip=''
where CoverDefinitionId = 145 and QuestionId=1004
end


--Tracking device / Early warning	
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=870)
begin
update QuestionDefinition
set DisplayName = 'Does your vehicle have a tracking device?'
where CoverDefinitionId = 145 and QuestionId=870
end

--Main Driver First Name	
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=144)
begin
update QuestionDefinition
set DisplayName = 'What is the full name of the regular driver?',ToolTip=''
where CoverDefinitionId = 145 and QuestionId=144
end

--Main Driver surname Name	
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=145)
begin
update QuestionDefinition
set DisplayName = 'What is the surname of the regular driver?',ToolTip=''
where CoverDefinitionId = 145 and QuestionId=145
end

--Marital Status	
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=251)
begin
update QuestionDefinition
set DisplayName = 'What is the marital status of the regular driver?'
where CoverDefinitionId = 145 and QuestionId=251
end

--Does the license restrict you to wear glasses while driving?
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=234)
begin
update QuestionDefinition
set DisplayName = 'Does the driver have any restrictions to wear glasses while driving?'
where CoverDefinitionId = 145 and QuestionId=234
end

--Does the license restrict the main driver to driving an automatic car?
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=224)
begin
update QuestionDefinition
set DisplayName = 'Does the license restrict the main driver to driving an automatic car?'
where CoverDefinitionId = 145 and QuestionId=224
end

--------CONTENT
--Exclude Theft Cover
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=394)
begin
update QuestionDefinition
set DisplayName = 'Exclude Theft Cover' 
where CoverDefinitionId = 146 and QuestionId=394
end


-------Building
--Does your home boarder on vacant land, river or sea
if exists (select * from QuestionDefinition where CoverDefinitionId = 147 and QuestionId=855)
begin
update QuestionDefinition
set DisplayName = 'Does your home border on vacant land, river or sea?If property’s border is sea/river or stream: Is the river or sea less than 300 metres from your property?' 
where CoverDefinitionId = 147 and QuestionId=855
end

--additional excess
if exists (select * from QuestionDefinition where CoverDefinitionId = 147 and QuestionId=857)
begin
update QuestionDefinition
set ToolTip = 'If yes: select limit'
where CoverDefinitionId = 147 and QuestionId=857
end

--Accidental Damage
if exists(select * from QuestionDefinition where CoverDefinitionId = 147 and QuestionId=859)
begin
update QuestionDefinition
set ToolTip = 'If yes: select limit' 
where CoverDefinitionId = 147 and QuestionId=859
end