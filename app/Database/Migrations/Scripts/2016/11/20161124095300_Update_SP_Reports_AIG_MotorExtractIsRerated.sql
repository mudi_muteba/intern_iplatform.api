if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_MotorExtract') and type in (N'P', N'PC'))
begin
	drop procedure Reports_AIG_MotorExtract
end
go

create procedure Reports_AIG_MotorExtract
(
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as


--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
--set @StartDate = '2016-10-01'
--set @EndDate = '2016-11-30'

DECLARE @tblExtra TABLE(PartyId int, AssetId int, Description nvarchar(100), Value decimal(18,2), AccessoryTypeId int)

insert into @tblExtra
select ast.PartyId, ast.Id as AssetId, astEx.Description, astEx.Value, astEx.AccessoryTypeId from AssetExtras as astEx
join Asset as ast on ast.Id = astEx.AssetId
where astex.IsDeleted <> 1

DECLARE @tbl TABLE(ProposalDefinitionId int, PartyId int, LeadId int, ProposalHeaderId int, CoverId int, Channel nvarchar(max), QuoteExpired nvarchar(max), QuoteBound nvarchar(max), QuoteBindDate nvarchar(max), DateOfQuote nvarchar(max), 
QuoteTotalPremium decimal(38,2), QuoteMotorPremium decimal(38,2), QuoteHomePremium decimal(38,2), QuoteFuneralPremium decimal(38,2), QuoteOtherPremium decimal(38,2),
QuoteFees decimal(38,2), QuoteSasria decimal(38,2), QuoteIncepptionDate nvarchar(max), QuotePolicyNumber nvarchar(max), QuoteCreateDate nvarchar(max), QuoteCreateTime nvarchar(max), QuoteID int, VMSAID nvarchar(max), VehicleNumber int,
AccSoundEquipment decimal(38,2),AccMagWheels decimal(38,2),AccSunRoof decimal(38,2),AccXenonLights decimal(38,2),AccTowBar decimal(38,2),AccBodyKit decimal(38,2),AccAntiSmashAndGrab decimal(38,2),AccOther decimal(38,2),
DriverIDNumber nvarchar(max),DriverTitle nvarchar(max),DriverFirstName nvarchar(max),DriverSurname nvarchar(max),
DriverGender nvarchar(max),DriverMaritalStatus nvarchar(max),DriverDateOfBirth nvarchar(max),DriverOccupation nvarchar(max),
DriverRecentCoverIndicator nvarchar(max), DriverUninterruptedCover nvarchar(max),
SumInsured decimal(38,2), SASRIA decimal(38,2))

insert into @tbl
SELECT 

prd.Id, p.Id, l.id , Prh.id , Prd.coverid,

CASE WHEN Prh.Source IS NULL THEN ('Call center') ELSE Prh.Source END as Channel,

CASE WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y' ELSE 'N' END as 'QuoteExpired',

(CASE 
 (SELECT COALESCE((select top 1 PolicyStatusId from PolicyHeader where PartyId = p.id and QuoteId = qute.Id and PolicyStatusId = 3), 0))
  when 3 then 'Y'
  when 2 then 'N'
  when 1 then 'N'
  when 0 then 'N'
End  )as 'QuoteBound',

	(CASE 
		(SELECT COALESCE((select top 1 PolicyStatusId from PolicyHeader where PartyId = p.id and QuoteId = qute.Id and PolicyStatusId = 3), 0))
			when 3 then CONVERT(VARCHAR(10),Qute.AcceptedOn ,111)
			when 2 then ''
			when 1 then ''
			when 0 then ''
	End  )as 'QuoteBindDate',

'' as 'DateOfQuote',

	case when (select sum(cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2))) + cast(round (Qute.Fees ,2) as numeric(36,2))  from QuoteItem 	
				where QuoteId = Qute.id) is null
	THEN (0) 
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2))) + cast(round (Qute.Fees ,2) as numeric(36,2))  from QuoteItem 	
				where QuoteId = Qute.id)                                          
	END 
	as 'QuoteTotalPremium',

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 218)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 218))                                          
	END AS 'QuoteMotorPremium',
	 
	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 78)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 78))                                          
	END AS 'QuoteHomePremium',

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 142)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 142))                                          
	END AS 'QuoteFuneralPremium',	

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId not in(218,78,142))) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId not in(218,78,142)))                                          
	END as 'QuoteOtherPremium',

	(cast(round(Qute.fees,2) as numeric(36,2))) as 'QuoteFees',

	(select sum(cast(round (Sasria ,2) as numeric(36,2)))  from QuoteItem 		
				where QuoteId = Qute.id) as 'QuoteSasria',

	(select top 1 CONVERT(VARCHAR(10),DateEffective ,111) from PolicyHeader where PartyId = p.Id and QuoteId = qute.Id and PolicyStatusId = 3) as 'QuoteIncepptionDate',

	(select COALESCE((select top 1 PolicyNo from PolicyHeader where PartyId = p.Id and QuoteId = qute.Id and PolicyStatusId = 3),null)) as 'QuotePolicyNumber',

	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'QuoteCreateDate',
	CONVERT(VARCHAR(5),Qute.CreatedAt,108) as 'QuoteCreateTime', 

	qute.Id as 'QuoteID',
	Ind.ExternalReference as 'VMSAID',	

	(select COALESCE((select AssetNo from Asset where id = prd.AssetId),null)) as 'VehicleNumber',

	case when (select sum(value) from @tblExtra where AccessoryTypeId = 1 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 1 and AssetId = ast.Id) end as 'AccSoundEquipment',	
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 2 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 2 and AssetId = ast.Id) end as 'AccMagWheels',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 3 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 3 and AssetId = ast.Id) end as 'AccSunRoof',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 4 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 4 and AssetId = ast.Id) end as 'AccXenonLights',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 5 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 5 and AssetId = ast.Id) end as 'AccTowBar',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 6 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 6 and AssetId = ast.Id) end as 'AccBodyKit',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 7 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 7 and AssetId = ast.Id) end as 'AccAntiSmashAndGrab',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 8 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 8 and AssetId = ast.Id) end as 'AccOther',

		Ind.IdentityNo as 'DriverIDNumber',
	(select name from Md.Title where Id = Ind.TitleId) as 'DriverTitle',
	Ind.FirstName as 'DriverFirstName', 
	Ind.Surname as 'DriverSurname',

	(select name from Md.Gender where Id = Ind.GenderId) as 'DriverGender',

	CASE WHEN 
		(select name from Md.MaritalStatus where Id = Ind.MaritalStatusId) is null
	THEN ''
	ELSE 
		(select name from Md.MaritalStatus where Id = Ind.MaritalStatusId)                                          
	END as 'DriverMaritalStatus',

	CONVERT(VARCHAR(10),Ind.DateOfBirth ,111) as 'DriverDateOfBirth',
	occ.Name as 'DriverOccupation',

	(select top 1 ci.Answer 
	from LossHistory as lh
	join md.CurrentlyInsured as ci on ci.Id = lh.CurrentlyInsuredId
	where PartyId = p.Id  and IsDeleted = 0 order by lh.Id desc) as 'DriverRecentCoverIndicator',

    (select top 1 up.Answer 
	from LossHistory as lh
	join md.UninterruptedPolicy as up on up.Id = lh.UninterruptedPolicyId
	where PartyId = p.Id  and IsDeleted = 0 order by lh.Id desc) as 'DriverUninterruptedCover',

	(select SumInsured from Asset where id = prd.AssetId) as 'SumInsured', 

	(select total from QuoteItemBreakDown where QuoteItemId = qi.Id and Name = 'Sasria')  as 'SASRIA'
	

from ProposalHeader as Prh
join ProposalDefinition as prd on prd.ProposalHeaderId = Prh.Id and prd.CoverId = 218
join QuoteHeader as Qh on qh.ProposalHeaderId = Prh.Id
join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
JOIN Party as P on p.Id = Prh.PartyId
join Lead as L on l.PartyId = p.Id
left join Individual as Ind on P.Id = Ind.PartyId
left join Occupation as Occ on Occ.Id = Ind.OccupationId
join QuoteItem as qi on qi.QuoteId = qute.Id and qi.IsDeleted = 0
left join QuoteItemBreakDown as qibd on qibd.QuoteItemId = qi.Id and qibd.IsDeleted = 0
join CoverDefinition as covD on covD.Id = qi.CoverDefinitionId
inner join Asset as ast on ast.AssetNo = qi.AssetNumber and ast.Id = prd.AssetId

where qute.CreatedAt  BETWEEN @StartDate AND @EndDate 
and L.IsDeleted = 0
and Qute.IsDeleted = 0
and Prh.IsDeleted = 0
and Qh.IsRerated = 0
and prd.IsDeleted = 0
and prd.CoverId in (218)
and covD.CoverId = 218

--and p.Id = 3730

group by Prh.Source, prd.Id , p.id, l.id,prh.id,prd.coverid,qute.createdat,qute.id,qute.acceptedon, qute.Fees, qh.CreatedAt, ind.ExternalReference, prd.AssetId, qi.Id, ast.Id, Ind.IdentityNo, Ind.TitleId, Ind.FirstName, Ind.Surname,
Ind.GenderId, Ind.MaritalStatusId, Ind.DateOfBirth, Occ.Name


SELECT  Channel  ,QuoteExpired, QuoteBound, QuoteBindDate,  '' as 'DateOfQuote', QuoteTotalPremium, QuoteMotorPremium, QuoteHomePremium, QuoteFuneralPremium, QuoteOtherPremium,QuoteFees,
QuoteSasria, QuoteIncepptionDate, QuotePolicyNumber, QuoteCreateDate, QuoteCreateTime, QuoteID, VMSAID, VehicleNumber, 


(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 89) as 'NightAddress-Suburb',

(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 90) as 'NightAddress-PostCode',

(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 47) as 'NightAddress-Province',

	(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 863) as 'NightAddress-Storage',

	(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 135) as 'DayAddress-Suburb',

	(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 91) as 'DayAddress-PostCode',

	(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 154) as 'DayAddress-Province',

	(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 864) as 'DayAddress-Storage',

(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 786) as 'VehicleType',

	(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 874) as 'MotorExcess',
	
	(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 97) as 'RegistrationNumber',
		
	(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 99) as 'VehicleYear',

	(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 95) as 'VehicleMake',

	(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 96) as 'VehicleModel',

	(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 94) as 'VehicleMMCode',

	(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 310)as 'VINNumber',

	(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 227) as 'Colour',

	/*case(SELECT pa.Answer FROM ProposalQuestionAnswer pa
		JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
		WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 143)
		when 519 then 'Y' else 'N'			
	end*/ 'Y'  as 'MetallicPaint',

	(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 230) as 'CoverType',

	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 255)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Modified',

	(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 867) as 'VehicleStatus',

	(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 226) as 'ClassOfUse',

(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 232)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Delivery',
	
	Case (SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 870)				
		when null then 'N' 	
		when 'false' then 'N'
		when 'true' then 'Y'	
	end  as 'TrackingDevice', 


	Case (SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 298)				
		when null then 'N' 	
		when 'false' then 'N'
		when 'true' then 'Y'	
	end  as 'DiscountedTrackingDevice',	

	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 245)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Immobiliser',

	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 871)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'AntiHijack',

	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 231)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'DataDot',




	AccSoundEquipment ,AccMagWheels ,AccSunRoof ,AccXenonLights ,AccTowBar ,AccBodyKit ,AccAntiSmashAndGrab ,AccOther,



	(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 1005) as 'VAP-CarHire',

	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 1004)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-TyreAndRim',

	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 1003)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-ScratchAndDent',

	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 873)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-CreditShortfall',

	DriverIDNumber, DriverTitle,DriverFirstName, DriverSurname, DriverGender,DriverMaritalStatus,DriverDateOfBirth,DriverOccupation,DriverRecentCoverIndicator, DriverUninterruptedCover,

	(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 249) as 'DriverLicenseType',

	(SELECT REPLACE(CONVERT(VARCHAR(10),pa.Answer ,111), '-', '/') FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 41) as 'DriverLicenseDate',

	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 248)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'DriverLicenseEndorsed',

	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 224)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Automatic',

	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 261)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Paraplegic',


	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 223)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Amputee',

	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 234)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Glasses',

	(case 
		(SELECT pa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 862)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Financed',

	(SELECT qa.Answer FROM ProposalQuestionAnswer pa
JOIN QuestionDefinition qd on qd.Id = pa.QuestionDefinitionId
JOIN md.QuestionAnswer qa on qa.Id = TRY_PARSE(pa.Answer AS int)
WHERE pa.ProposalDefinitionId = t.ProposalDefinitionId AND qd.QuestionId = 300)	 as 'ValuationMethod',

SumInsured, SASRIA, PartyId

FROM @tbl as t
