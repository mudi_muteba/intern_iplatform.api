delete from ProductAdditionalExcess
dbcc checkident ('ProductAdditionalExcess', reseed, 0)

--KingPrice (Motor), Product Codes: 'KPIPERS', 'KPIPERS2'
declare
	@KingPrice_KPIPERS_Motor_CoverDefinitionId int,
	@KingPrice_KPIPERS2_Motor_CoverDefinitionId int
	
select
	@KingPrice_KPIPERS_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS') and
	Cover.Name in ('Motor')

select
	@KingPrice_KPIPERS2_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS2') and
	Cover.Name in ('Motor')

if	isnull(@KingPrice_KPIPERS_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 0, 'General', 'Basic Excess', 4500, 1500, 27500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 0, 'General', 'Basic Excess', 4500, 1500, 27500, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 1, 'General', 'Specified Ratio', 750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 1, 'General', 'Specified Ratio', 750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 2, 'General', 'Window Replacement (Excl. Panoramic Glass)', 750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 2, 'General', 'Window Replacement (Excl. Panoramic Glass)', 750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 3, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'Is younger than 25 years OR has had their driver�s licence for less than 2 years', 4500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 3, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'Is younger than 25 years OR has had their driver�s licence for less than 2 years', 4500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 4, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'If the incident occurred outside of SA and the car is not drivable', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 4, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'If the incident occurred outside of SA and the car is not drivable', 7500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 5, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'When the incident occurs in the first 3 months of cover', 3500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 5, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'When the incident occurs in the first 3 months of cover', 3500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 6, 'Trailer & Caravans', 'Caravan or caravan contents', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 6, 'Trailer & Caravans', 'Caravan or caravan contents', 2500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 7, 'Trailer & Caravans', 'Caravan or trailer contents', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 7, 'Trailer & Caravans', 'Caravan or trailer contents', 2500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 8, 'Trailer & Caravans', 'Liability to other parties', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 8, 'Trailer & Caravans', 'Liability to other parties', 0, 0, 0, 0, 0, 0, 0, 0)
	end


--KingPrice (Building), Product Codes: 'KPIPERS', 'KPIPERS2'
declare
	@KingPrice_KPIPERS_Building_CoverDefinitionId int,
	@KingPrice_KPIPERS2_Building_CoverDefinitionId int
	
select
	@KingPrice_KPIPERS_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS') and
	Cover.Name in ('Building')

select
	@KingPrice_KPIPERS2_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS2') and
	Cover.Name in ('Building')

if	isnull(@KingPrice_KPIPERS_Building_CoverDefinitionId, 0) > 0 and
	isnull(@KingPrice_KPIPERS2_Building_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Building_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Building_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)
	end

--KingPrice (Contents), Product Codes: 'KPIPERS', 'KPIPERS2'
declare
	@KingPrice_KPIPERS_Contents_CoverDefinitionId int,
	@KingPrice_KPIPERS2_Contents_CoverDefinitionId int
	
select
	@KingPrice_KPIPERS_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS') and
	Cover.Name in ('Contents')

select
	@KingPrice_KPIPERS2_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS2') and
	Cover.Name in ('Contents')

if	isnull(@KingPrice_KPIPERS_Contents_CoverDefinitionId, 0) > 0 and
	isnull(@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 1, 'Additional Cover Excess', 'Food that has deteriorated', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 1, 'Additional Cover Excess', 'Food that has deteriorated', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 2, 'Additional Cover Excess', 'Washing stolen from the line', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 2, 'Additional Cover Excess', 'Washing stolen from the line', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 3, 'Additional Cover Excess', 'Guests belongings', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 3, 'Additional Cover Excess', 'Guests belongings', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 4, 'Additional Cover Excess', 'Locks & keys', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 4, 'Additional Cover Excess', 'Locks & keys', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 5, 'Additional Cover Excess', 'Hole in one in golf/full house in bowling', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 5, 'Additional Cover Excess', 'Hole in one in golf/full house in bowling', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 6, 'Additional Cover Excess', 'Domestic employee�s belongings', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 6, 'Additional Cover Excess', 'Domestic employee�s belongings', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 7, 'Additional Cover Excess', 'Rent to live elsewhere', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 7, 'Additional Cover Excess', 'Rent to live elsewhere', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 8, 'Additional Cover Excess', 'Garden & leisure equipment', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 8, 'Additional Cover Excess', 'Garden & leisure equipment', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 9, 'Additional Cover Excess', 'Veterinary expense', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 9, 'Additional Cover Excess', 'Veterinary expense', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 10, 'Portable Posessions', 'Unspecified items (per item)', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 10, 'Portable Posessions', 'Specified items', 500, 0, 0, 5, 0, 0, 0, 0)
	end


--SA Underwriters (Motor), Product Codes: 'CENTND', 'CENTRD', 'SNTMND', 'SNTMRD'
declare
	@SAUnderwriters_CENTND_Motor_CoverDefinitionId int,
	@SAUnderwriters_CENTRD_Motor_CoverDefinitionId int,
	@SAUnderwriters_SNTMND_Motor_CoverDefinitionId int,
	@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId int
	
select
	@SAUnderwriters_CENTND_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('CENTND') and
	Cover.Name in ('Motor')

select
	@SAUnderwriters_CENTRD_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('CENTRD') and
	Cover.Name in ('Motor')

select
	@SAUnderwriters_SNTMND_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('SNTMND') and
	Cover.Name in ('Motor')

select
	@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('SNTMRD') and
	Cover.Name in ('Motor')

if	isnull(@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
	end

	--Telesure (Content), Product Codes: 'AUGPRD', 'FIR', 'UNI' (Auto & General, First For Women, Unity)
declare
	@Telesure_AUGPRD_Contents_CoverDefinitionId int,
	@Telesure_FIR_Contents_CoverDefinitionId int,
	@Telesure_UNI_Contents_CoverDefinitionId int
	
select
	@Telesure_AUGPRD_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AUGPRD') and
	Cover.Name in ('Contents')

select
	@Telesure_FIR_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('FIR') and
	Cover.Name in ('Contents')

select
	@Telesure_UNI_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('UNI') and
	Cover.Name in ('Contents')

if	isnull(@Telesure_AUGPRD_Contents_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_FIR_Contents_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_UNI_Contents_CoverDefinitionId, 0) > 0
	begin
		--NonMotorExcess New
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 0, 'House Contents', 'Basic Excess(5% or Excess Displayed)', 1600, 0, 9000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 0, 'House Contents', 'Additional Excess - Burglary', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 0, 'House Contents', 'Additional Excess Lightning',800, 0, 0, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 1, 'House Contents', 'Basic Excess', 1600, 0, 9000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 1, 'House Contents', 'Additional Excess - Burglary', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 1, 'House Contents', 'Additional Excess Lightning',800, 0, 0, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 2, 'House Contents', 'Basic Excess', 1600, 0, 9000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 2, 'House Contents', 'Additional Excess - Burglary', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 2, 'House Contents', 'Additional Excess Lightning',800, 0, 0, 0, 0, 0, 0, 0)

		--min manx sheet
				--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 3, 'HOUSE CONTENTS HOUSE / COTTAGE (H)','sum insured', 0, 50000, 7400000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 4,  'HOUSE CONTENTS HOUSE / COTTAGE (H)','sum insured', 0, 50000, 7400000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 5, 'HOUSE CONTENTS HOUSE / COTTAGE (H)','sum insured', 0, 50000, 7400000, 0, 0, 0, 0, 0)
		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 6, 'HOUSE CONTENTS FLAT ABOVE GROUND (F)','sum insured', 0, 50000, 4750000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 7, 'HOUSE CONTENTS FLAT ABOVE GROUND (F)','sum insured', 0, 50000, 4750000, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 8, 'HOUSE CONTENTS FLAT ABOVE GROUND (F)','sum insured', 0, 50000, 4750000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId,9, 'HOUSE CONTENTS FLAT GROUND (G)', 'sum insured', 0, 50000, 3400000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 10, 'HOUSE CONTENTS FLAT GROUND (G)', 'sum insured',0, 50000, 3400000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 11, 'HOUSE CONTENTS FLAT GROUND (G)', 'sum insured',0, 50000, 3400000, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 12, 'HOUSE CONTENTS TOWN HOUSE / CLUSTER (T)','sum insured', 0, 50000, 7400000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 13, 'HOUSE CONTENTS TOWN HOUSE / CLUSTER (T)','sum insured', 0, 50000, 7400000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 14, 'HOUSE CONTENTS TOWN HOUSE / CLUSTER (T)','sum insured', 0, 50000, 7400000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 15, 'HOUSE CONTENTS PARK HOME (P)','sum insured', 0, 50000, 450000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 16, 'HOUSE CONTENTS PARK HOME (P)','sum insured', 0, 50000, 450000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 17, 'HOUSE CONTENTS PARK HOME (P)','sum insured', 0, 50000, 450000, 0, 0, 0, 0, 0)
	end


--Telesure (All Risk), Product Codes: 'AUGPRD', 'FIR', 'UNI' (Auto & General, First For Women, Unity)
declare
	@Telesure_AUGPRD_AllRisk_CoverDefinitionId int,
	@Telesure_FIR_AllRisk_CoverDefinitionId int,
	@Telesure_UNI_AllRisk_CoverDefinitionId int
	
select
	@Telesure_AUGPRD_AllRisk_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AUGPRD') and
	Cover.Name in ('All Risk')

select
	@Telesure_FIR_AllRisk_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('FIR') and
	Cover.Name in ('All Risk')

select
	@Telesure_UNI_AllRisk_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('UNI') and
	Cover.Name in ('All Risk')

if	isnull(@Telesure_AUGPRD_AllRisk_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_FIR_AllRisk_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_UNI_AllRisk_CoverDefinitionId, 0) > 0
	begin
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_AllRisk_CoverDefinitionId, 0, 'All Risks', 'Basic Excess (Except Cell Phone) - Supported', 850, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_AllRisk_CoverDefinitionId, 0, 'All Risks', 'Basic Excess (Except Cell Phone) - Unsupported', 1200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_AllRisk_CoverDefinitionId, 0, 'All Risks', 'Basic Excess - Cell phone (5% or Excess Displayed)', 1350, 0, 9000, 0, 0, 0, 0, 0)
			--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_AllRisk_CoverDefinitionId, 1, 'All Risks', 'Basic Excess (Except Cell Phone) - Supported', 850, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_AllRisk_CoverDefinitionId, 1, 'All Risks', 'Basic Excess (Except Cell Phone) - Unsupported', 1200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_AllRisk_CoverDefinitionId, 1, 'All Risks', 'Basic Excess - Cell phone (5% or Excess Displayed)', 1350, 0, 9000, 0, 0, 0, 0, 0)
			--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_AllRisk_CoverDefinitionId, 2, 'All Risks', 'Basic Excess (Except Cell Phone) - Supported', 850, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_AllRisk_CoverDefinitionId, 2, 'All Risks', 'Basic Excess (Except Cell Phone) - Unsupported', 1200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_AllRisk_CoverDefinitionId, 2, 'All Risks', 'Basic Excess - Cell phone (5% or Excess Displayed)', 1350, 0, 9000, 0, 0, 0, 0, 0)
		
		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 3, 'PORTABLE POSSESSIONS:  SUPPORTED', 'UNSPECIFIED (AU) per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 3, 'PORTABLE POSSESSIONS:  SUPPORTED', 'UNSPECIFIED (AU) per policy', 0, 5000, 40000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 4, 'PORTABLE POSSESSIONS:  SUPPORTED', 'UNSPECIFIED (AU) per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 4, 'PORTABLE POSSESSIONS:  SUPPORTED', 'UNSPECIFIED (AU) per policy', 0, 5000, 40000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 5, 'PORTABLE POSSESSIONS:  SUPPORTED', 'UNSPECIFIED (AU) per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 5, 'PORTABLE POSSESSIONS:  SUPPORTED', 'UNSPECIFIED (AU) per policy', 0, 5000, 40000, 0, 0, 0, 0, 0)

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 6, 'PORTABLE POSSESSIONS:  SUPPORTED', 'SPECIFIED (AJ) per item', 0, 4000, 176000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 6, 'PORTABLE POSSESSIONS:  SUPPORTED', 'SPECIFIED (AJ) Maximum per policy', 600000, 0, 0, 50, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 7, 'PORTABLE POSSESSIONS:  SUPPORTED', 'SPECIFIED (AJ) per item', 0, 4000, 176000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 7, 'PORTABLE POSSESSIONS:  SUPPORTED', 'SPECIFIED (AJ) Maximum per policy', 600000, 0, 0, 50, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 8, 'PORTABLE POSSESSIONS:  SUPPORTED', 'SPECIFIED (AJ) per item', 0, 4000, 176000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 8, 'PORTABLE POSSESSIONS:  SUPPORTED', 'SPECIFIED (AJ) Maximum per policy', 600000, 0, 0, 50, 0, 0, 0, 0)
		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 9, 'LOCKED BOOT', 'MAXIMUM INDEMNITY (Brokers)', 0, 0, 24000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 9, 'LOCKED CABIN', 'MAXIMUM INDEMNITY (Brokers)', 0, 0, 5500, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 10, 'LOCKED BOOT', 'MAXIMUM INDEMNITY (Brokers)', 0, 0, 24000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 10, 'LOCKED CABIN', 'MAXIMUM INDEMNITY (Brokers)', 0, 0, 5500, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 11, 'LOCKED BOOT', 'MAXIMUM INDEMNITY (Brokers)', 0, 0, 24000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 11, 'LOCKED CABIN', 'MAXIMUM INDEMNITY (Brokers)', 0, 0, 5500, 0, 0, 0, 0, 0)

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 12, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'UNSPECIFIED (BU) per policy', 0, 5000, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 12, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'UNSPECIFIED (BU)Maximum per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 13, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'UNSPECIFIED (BU) per policy', 0, 5000, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 13, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'UNSPECIFIED (BU)Maximum per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 14, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'UNSPECIFIED (BU) per policy', 0, 5000, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 14, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'UNSPECIFIED (BU)Maximum per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 15, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'SPECIFIED (BJ) per item', 0, 4000, 70000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 15, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'SPECIFIED (BJ) Maximum per policy', 0, 0, 78000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 16, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'SPECIFIED (BJ) per item', 0, 4000, 70000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 16, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'SPECIFIED (BJ) Maximum per policy', 0, 0, 78000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 17, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'SPECIFIED (BJ) per item', 0, 4000, 70000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 17, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'SPECIFIED (BJ) Maximum per policy', 0, 0, 78000, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 18, 'SPECTACLES / CONTACT LENSES (CL/BL)', 'per item', 0, 1500, 14000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 18, 'SPECTACLES / CONTACT LENSES (CL/BL)', 'Maximum per policy', 0, 0, 41500, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 19, 'SPECTACLES / CONTACT LENSES (CL/BL)', 'per item', 0, 1500, 14000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 19, 'SPECTACLES / CONTACT LENSES (CL/BL)', 'Maximum per policy', 0, 0, 41500, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 20, 'SPECTACLES / CONTACT LENSES (CL/BL)', 'per item', 0, 1500, 14000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 19, 'SPECTACLES / CONTACT LENSES (CL/BL)', 'Maximum per policy', 0, 0, 41500, 0, 0, 0, 0, 0)
		

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 21, 'CELL PHONES (CP/BP)', 'per item', 0,1700, 24200, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 21, 'CELL PHONES (CP/BP)', 'Maximum per policy', 0, 0, 60000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 22, 'CELL PHONES (CP/BP)', 'per item', 0,1700, 24200, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 22, 'CELL PHONES (CP/BP)', 'Maximum per policy', 0, 0, 60000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 23, 'CELL PHONES (CP/BP)', 'per item', 0,1700, 24200, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 23, 'CELL PHONES (CP/BP)', 'Maximum per policy', 0, 0, 60000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 24, 'BICYCLES (PC/BC)', 'per item', 0, 1400, 146500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 24, 'BICYCLES (PC/BC)', 'Maximum per policy', 0, 0, 220000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 25, 'BICYCLES (PC/BC)', 'per item', 0, 1400, 146500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 25, 'BICYCLES (PC/BC)', 'Maximum per policy', 0, 0, 220000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 26, 'BICYCLES (PC/BC)', 'per item', 0, 1400, 146500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 26, 'BICYCLES (PC/BC)', 'Maximum per policy', 0, 0, 220000, 0, 0, 0, 0, 0)
		
				--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 27, 'SWIMMING POOL (SP/SB)', 'Minimum per policy', 0, 2500, 25000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 28, 'SWIMMING POOL (SP/SB)', 'Minimum per policy', 0, 2500, 25000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 29, 'SWIMMING POOL (SP/SB)', 'Minimum per policy', 0, 2500, 25000, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 30, 'STANDALONE CELL PHONES', 'per item', 0, 1500, 20000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 30, 'STANDALONE CELL PHONES', 'Maximum per policy', 0, 0, 48000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 31, 'STANDALONE CELL PHONES', 'per item', 0, 1500, 20000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 31, 'STANDALONE CELL PHONES', 'Maximum per policy', 0, 0, 48000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 32, 'STANDALONE CELL PHONES', 'per item', 0, 1500, 20000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId,32, 'STANDALONE CELL PHONES', 'Maximum per policy', 0, 0, 48000, 0, 0, 0, 0, 0)
	end


--Telesure (Building), Product Codes: 'AUGPRD', 'FIR', 'UNI' (Auto & General, First For Women, Unity)
declare
	@Telesure_AUGPRD_Building_CoverDefinitionId int,
	@Telesure_FIR_Building_CoverDefinitionId int,
	@Telesure_UNI_Building_CoverDefinitionId int
	
select
	@Telesure_AUGPRD_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AUGPRD') and
	Cover.Name in ('Building')

select
	@Telesure_FIR_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('FIR') and
	Cover.Name in ('Building')

select
	@Telesure_UNI_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('UNI') and
	Cover.Name in ('Building')

if	isnull(@Telesure_AUGPRD_Building_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_FIR_Building_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_UNI_Building_CoverDefinitionId, 0) > 0
	begin
				--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 0, 'Building', 'Basic Excess(5% or Excess Displayed)', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 0, 'Building', 'Basic Excess - subsidence and landslip(5% or Excess Displayed)', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 0,'Building', 'Liability as home owner', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 0, 'Building','Additional Excess (5% or Excess Displayed)- burst water pipes (not attached to Geysers)', 1000, 0, 10000, 0, 0, 0, 0, 0)
				--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 1, 'Building', 'Basic Excess(5% or Excess Displayed)', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 1, 'Building', 'Basic Excess - subsidence and landslip(5% or Excess Displayed)', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 1,'Building', 'Liability as home owner', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 1, 'Building','Additional Excess (5% or Excess Displayed)- burst water pipes (not attached to Geysers)', 1000, 0, 10000, 0, 0, 0, 0, 0)
				--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 2, 'Building', 'Basic Excess(5% or Excess Displayed)', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 2, 'Building', 'Basic Excess - subsidence and landslip(5% or Excess Displayed)', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 2,'Building', 'Liability as home owner', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 2, 'Building','Additional Excess (5% or Excess Displayed)- burst water pipes (not attached to Geysers)', 1000, 0, 10000, 0, 0, 0, 0, 0)

				--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 3, 'Geyser and Geyser Related Building damage', 'Basic Excess (5% or Excess Displayed)', 1650, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 3, 'Geyser and Geyser Related Building damage', 'Basic Excess(5% or Excess Displayed) - consequential damage - not accompanied with geyser claim(No Max)', 1000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 3,'Geyser and Geyser Related Building damage', 'buy up options', 10000, 0, 0, 0, 0, 0, 0, 0)
				--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 4, 'Geyser and Geyser Related Building damage', 'Basic Excess (5% or Excess Displayed)', 1650, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId,4, 'Geyser and Geyser Related Building damage','Basic Excess(5% or Excess Displayed) - consequential damage - not accompanied with geyser claim(No Max)',1000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId,4,'Geyser and Geyser Related Building damage', 'buy up options', 20000, 0, 0, 0, 0, 0, 0, 0)
				--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 5, 'Geyser and Geyser Related Building damage', 'Basic Excess (5% or Excess Displayed)', 1650, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId,5, 'Geyser and Geyser Related Building damage', 'Basic Excess(5% or Excess Displayed) - consequential damage - not accompanied with geyser claim(No Max)',1000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId,5,'Geyser and Geyser Related Building damage', 'buy up options', 30000, 0, 0, 0, 0, 0, 0, 0)

				--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 6, 'Additional Cover', 'Additional Cover Excess buildings', 450, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 6, 'Additional Cover', 'Additional Cover Excess contents', 550, 0, 0, 0, 0, 0, 0, 0)		
				--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 7, 'Additional Cover', 'Additional Cover Excess buildings', 450, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 7, 'Additional Cover', 'Additional Cover Excess contents', 550, 0, 0, 0, 0, 0, 0, 0)		
				--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 8, 'Additional Cover', 'Additional Cover Excess buildings', 450, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 8, 'Additional Cover', 'Additional Cover Excess contents', 550, 0, 0, 0, 0, 0, 0, 0)


	--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 9, 'Building HOUSE / COTTAGE (H)','Sum Insured' ,0, 330000, 33500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 9,'Building HOUSE / COTTAGE (H)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 10, 'Building HOUSE / COTTAGE (H)','Sum Insured', 0, 330000, 33500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 10,'Building HOUSE / COTTAGE (H)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 11, 'Building HOUSE / COTTAGE (H)','Sum Insured', 0, 330000, 33500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 11,'Building HOUSE / COTTAGE (H)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 12, 'Building FLAT ABOVE GROUND (F)', 'sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 12,'Building FLAT ABOVE GROUND (F)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 13,'Building FLAT ABOVE GROUND (F)', 'sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 13,'Building FLAT ABOVE GROUND (F)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 14,'Building FLAT ABOVE GROUND (F)', 'sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 14,'Building FLAT ABOVE GROUND (F)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 15,'Building FLAT GROUND (G)','sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 15, 'Building FLAT GROUND (G)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 16,'Building FLAT GROUND (G)','sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 16,'Building FLAT GROUND (G)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 17,'Building FLAT GROUND (G)','sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 17, 'Building FLAT GROUND (G)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 18,'Building TOWN HOUSE','sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 18,'Building TOWN HOUSE Maximum','sum insured(Thatch)', 0, 0,8250000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 19,'Building TOWN HOUSE','sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 19,'Building TOWN HOUSE Maximum','sum insured(Thatch)', 0, 0,8250000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 20,'Building TOWN HOUSE','sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 20,'Building TOWN HOUSE Maximum','sum insured(Thatch)', 0, 0,8250000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 21, 'Building CLUSTER','sum insured', 0, 360000,33500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 21,'Building CLUSTER',' Maximum sum insured(Thatch)', 0, 0,8250000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 22, 'Building CLUSTER','sum insured', 0, 360000,33500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 22,'Building CLUSTER',' Maximum sum insured(Thatch)', 0, 0,8250000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 23, 'Building CLUSTER','sum insured', 0, 360000,33500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 23,'Building CLUSTER',' Maximum sum insured(Thatch)', 0, 0,8250000, 0, 0, 0, 0, 0)

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 24, 'Building RDP Houses', 'sum insured', 0, 150000, 8250000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 24, 'Building RDP Houses', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 25, 'Building RDP Houses', 'sum insured', 0, 150000, 8250000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 25, 'Building RDP Houses', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 26, 'Building RDP Houses', 'sum insured', 0, 150000, 8250000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 26, 'Building RDP Houses', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 27, 'Building PARK HOME (P)', 'sum insured', 0,200000, 1350000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 27, 'Building PARK HOME (P)', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 28, 'Building PARK HOME (P)', 'sum insured', 0,200000, 1350000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 28, 'Building PARK HOME (P)', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 29, 'Building PARK HOME (P)', 'sum insured', 0,200000, 1350000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 29, 'Building PARK HOME (P)', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)
		
		
		--HH ADDITIONAL COVER:  under building Deterioration of food
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 30, 'ADDITIONAL COVER(House & Home)', 'Deterioration of food', 2200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 31,'ADDITIONAL COVER(House & Home)', 'Deterioration of food', 2200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 32,'ADDITIONAL COVER(House & Home)', 'Deterioration of food', 2200, 0, 0, 0, 0, 0, 0, 0)

		--Washing and Garden Furniture
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 33, 'ADDITIONAL COVER(House & Home)','Washing and Garden Furniture', 3500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 34,  'ADDITIONAL COVER(House & Home)','Washing and Garden Furniture', 3500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 35,  'ADDITIONAL COVER(House & Home)','Washing and Garden Furniture',3500, 0, 0, 0, 0, 0, 0, 0)
		
		--Guests property
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 36, 'ADDITIONAL COVER(House & Home)','Guests property', 5200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 37,  'ADDITIONAL COVER(House & Home)','Guests property', 5200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 38,  'ADDITIONAL COVER(House & Home)','Guests property', 5200, 0, 0, 0, 0, 0, 0, 0)
		
		--Money - stolen from risk address
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 39, 'ADDITIONAL COVER(House & Home)','Money - stolen from risk address', 2000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 40,  'ADDITIONAL COVER(House & Home)','Money - stolen from risk address', 2000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 41,  'ADDITIONAL COVER(House & Home)','Money - stolen from risk address', 2000, 0, 0, 0, 0, 0, 0, 0)
		
		--Documents, coins, stamps
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 42, 'ADDITIONAL COVER(House & Home)','Documents, coins, stamps', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 43,  'ADDITIONAL COVER(House & Home)','Documents, coins, stamps', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 44,  'ADDITIONAL COVER(House & Home)','Documents, coins, stamps', 4000, 0, 0, 0, 0, 0, 0, 0)
		
		--Locks & Keys
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 45, 'ADDITIONAL COVER(House & Home)','Locks & Keys', 2650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 46,  'ADDITIONAL COVER(House & Home)','Locks & Keys', 2650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 47,  'ADDITIONAL COVER(House & Home)','Locks & Keys', 2650, 0, 0, 0, 0, 0, 0, 0)
		
		--'Fraudulent use of bank / credit cards'
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 48, 'ADDITIONAL COVER(House & Home)','Fraudulent use of bank / credit cards', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 49,  'ADDITIONAL COVER(House & Home)','Fraudulent use of bank / credit cards', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 50,  'ADDITIONAL COVER(House & Home)','Fraudulent use of bank / credit cards', 4000, 0, 0, 0, 0, 0, 0, 0)
		
		--Hole-in-one
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 51, 'ADDITIONAL COVER(House & Home)','Hole-in-one', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 52,  'ADDITIONAL COVER(House & Home)','Hole-in-one', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 53,  'ADDITIONAL COVER(House & Home)','Hole-in-one', 2500, 0, 0, 0, 0, 0, 0, 0)
		
		--Insured & spouse (death) fire / break in
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 54, 'ADDITIONAL COVER(House & Home)','Insured & spouse (death) fire / break in', 15000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 55,  'ADDITIONAL COVER(House & Home)','Insured & spouse (death) fire / break in', 15000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 56,  'ADDITIONAL COVER(House & Home)','Insured & spouse (death) fire / break in', 15000, 0, 0, 0, 0, 0, 0, 0)
		
		--Domestics belongins - theft
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 57, 'ADDITIONAL COVER(House & Home)','Domestics belongins - theft', 5250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 58,  'ADDITIONAL COVER(House & Home)','Domestics belongins - theft', 5250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 59,  'ADDITIONAL COVER(House & Home)','Domestics belongins - theft',5250, 0, 0, 0, 0, 0, 0, 0)
		
		--Medical expenses (defect / pet)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 60, 'ADDITIONAL COVER(House & Home)','Medical expenses (defect / pet)', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 61,  'ADDITIONAL COVER(House & Home)','Medical expenses (defect / pet)', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 62,  'ADDITIONAL COVER(House & Home)','Medical expenses (defect / pet)', 3300, 0, 0, 0, 0, 0, 0, 0)
		
		--Veterinary costs due to road accident
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 63, 'ADDITIONAL COVER(House & Home)','Veterinary costs due to road accident', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 64,  'ADDITIONAL COVER(House & Home)','Veterinary costs due to road accident', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 65,  'ADDITIONAL COVER(House & Home)','Veterinary costs due to road accident', 3300, 0, 0, 0, 0, 0, 0, 0)
		
		--Loss of rent
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 66, 'Loss of rent','20% of sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 67,  'Loss of rent','20% of sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 68,  'Loss of rent','20% of sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		--Transit
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 69, 'Transit','Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 70,  'Transit','Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 71,  'Transit','Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		--Mirrors / glass as part of furniture
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 72, 'Mirrors / glass as part of furniture','Items sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 73,  'Mirrors / glass as part of furniture','Items sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 74,  'Mirrors / glass as part of furniture','Items sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		--Accidental breakage - TV
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 75, 'Accidental breakage - TV','Items sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 76,  'COVER Accidental breakage - TV','Items sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 77,  'Accidental breakage - TV','Items sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		--Fire brigade charges
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 78, 'Fire brigade charges','Cost of Charges', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 79,  'Fire brigade charges','Cost of Charges', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 80,  'Fire brigade charges','Cost of Charges', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		
		--Public Liability as a Householder
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 81, 'ADDITIONAL COVER(House & Home)','Public Liability as a Householder', 1200000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 82,  'ADDITIONAL COVER(House & Home)','Public Liability as a Householder', 1200000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 83,  'ADDITIONAL COVER(House & Home)','Public Liability as a Householder', 1200000, 0, 0, 0, 0, 0, 0, 0)
		
		--Tenant's Liability as a Householder
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 84, 'ADDITIONAL COVER(House & Home)','Tenant is Liability as a Householder', 1200000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 85,  'ADDITIONAL COVER(House & Home)','Tenant is Liability as a Householder', 1200000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 86,  'ADDITIONAL COVER(House & Home)','Tenant is Liability as a Householder', 1200000, 0, 0, 0, 0, 0, 0, 0)
		
		--Liability to Domestic Employees
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 87, 'ADDITIONAL COVER(House & Home)','Liability to Domestic Employees', 120000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 88,  'ADDITIONAL COVER(House & Home)','Liability to Domestic Employees', 120000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 89,  'ADDITIONAL COVER(House & Home)','Liability to Domestic Employees', 120000, 0, 0, 0, 0, 0, 0, 0)		
	
		
		--Loss of rent
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 90, 'Building(Loss of rent)','20% of Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 91,  'Building(Loss of rent)','20% of Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 92,  'Building(Loss of rent)','20% of Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		--Public authority - professional fees
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 93, 'Public authority - professional fees','20% of Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 94,  'Public authority - professional fees','20% of Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 95,  'Public authority - professional fees','20% of Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		--Accidental damage - glass / sanitaryware
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 96, 'Accidental damage - glass / sanitaryware','Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 97,  'Accidental damage - glass / sanitaryware','Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 98,  'Accidental damage - glass / sanitaryware','Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		--Accidental damage - public supply / mains
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 99, 'Accidental damage - public supply / mains','Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 100,  'Accidental damage - public supply / mains','Sum Insured',0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 101,  'Accidental damage - public supply / mains','Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		--Aerials / masts / dish - accident & theft damage
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 102, 'Aerials / masts / dish - accident & theft damage','Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 103,  'Aerials / masts / dish - accident & theft damage','Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 105,  'Aerials / masts / dish - accident & theft damage','Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		--Fire brigade charges
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 105,'Building(Fire brigade charges Costs: demolish / remove debris)','Cost of Charges Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 106,'Building(Fire brigade charges Costs: demolish / remove debris)','Cost of Charges Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 107,'Building(Fire brigade charges Costs: demolish / remove debris)','Cost of Charges Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		--Rent to live elsewhere
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 108, 'Rent to live elsewhere','20% of Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 109,  'Rent to live elsewhere','20% of Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 110,  'Rent to live elsewhere','20% of Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
	end
	
	
	
--Telesure (Motor), Product Codes: 'AUGPRD', 'FIR', 'UNI' (Auto & General, First For Women, Unity)
declare
	@Telesure_AUGPRD_Motor_CoverDefinitionId int,
	@Telesure_FIR_Motor_CoverDefinitionId int,
	@Telesure_UNI_Motor_CoverDefinitionId int
	
select
	@Telesure_AUGPRD_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AUGPRD') and
	Cover.Name in ('Motor')

select
	@Telesure_FIR_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('FIR') and
	Cover.Name in ('Motor')

select
	@Telesure_UNI_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('UNI') and
	Cover.Name in ('Motor')

if	isnull(@Telesure_AUGPRD_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_FIR_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_UNI_Motor_CoverDefinitionId, 0) > 0
	begin
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 0, 'COMPREHENSIVE VEHICLE', 'sum insured', 0,18500, 3300000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 0, 'COMPREHENSIVE VEHICLE', 'Maximum Sum Insured (younger than 21)', 0, 0, 302500, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 1, 'COMPREHENSIVE VEHICLE', 'sum insured', 0,18500, 3300000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 1, 'COMPREHENSIVE VEHICLE', 'Maximum Sum Insured (younger than 21)', 0, 0, 302500, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 2, 'COMPREHENSIVE VEHICLE', 'sum insured', 0,18500, 3300000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 2, 'COMPREHENSIVE VEHICLE', 'Maximum Sum Insured (younger than 21)', 0, 0, 302500, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 3, 'MOTORBIKES', 'sum insured', 0, 5000,500000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 4, 'MOTORBIKES', 'sum insured', 0, 5000,500000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 5, 'MOTORBIKES', 'sum insured', 5000, 0, 0, 0, 0, 0, 0, 0)
		
		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 6, 'AGE RESTRICTIONS 16 - 17 years (legal)', '50cc  - 125cc', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 6, 'AGE RESTRICTIONS 18 - 24 years', 'up to 500cc', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 6,'AGE RESTRICTIONS 25 & older', '500cc +', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 7, 'AGE RESTRICTIONS 16 - 17 years (legal)', '50cc  - 125cc', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 7, 'AGE RESTRICTIONS 18 - 24 years', 'up to 500cc', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 7,'AGE RESTRICTIONS 25 & older', '500cc +', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 8, 'AGE RESTRICTIONS 16 - 17 years (legal)', '50cc  - 125cc', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 8, 'AGE RESTRICTIONS 18 - 24 years', 'up to 500cc', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 8, 'AGE RESTRICTIONS 25 & older', '500cc +', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		--AUGPRD3
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 9, 'CARAVAN', 'sum insured', 0, 15000, 500000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 10, 'CARAVAN', 'sum insured', 0, 15000, 500000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 11, 'CARAVAN', 'sum insured', 0, 15000, 500000, 0, 0, 0, 0, 0)
		
		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 12, 'CARAVAN CONTENTS', 'Minimum sum insured - Contents', 0, 6000, 80000, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 13, 'CARAVAN CONTENTS', 'Minimum sum insured - Contents', 0, 6000, 80000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 14, 'CARAVAN CONTENTS', 'Minimum sum insured - Contents', 0, 6000, 80000, 0, 0, 0, 0, 0)
				--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 15, 'TRAILERS', 'sum insured', 0,2400, 275000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 16, 'TRAILERS', 'sum insured', 0,2400, 275000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 17, 'TRAILERS', 'sum insured', 0,2400, 275000, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 18, 'WATERCRAFT', 'sum insured', 0,6000, 1000000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 19, 'WATERCRAFT', 'sum insured', 0,6000, 1000000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 20, 'WATERCRAFT', 'sum insured', 0,6000, 1000000, 0, 0, 0, 0, 0)

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 21, 'VEHICLE SOUND SYSTEM', 'sum insured', 0,1500,33000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 22, 'VEHICLE SOUND SYSTEM', 'sum insured', 0,1500,33000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 23, 'VEHICLE SOUND SYSTEM', 'sum insured', 0,1500,33000, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 24, 'GOLF CARTS (Personal Lines)', 'sum insured', 0,12000, 120000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 25, 'GOLF CARTS (Personal Lines)', 'sum insured', 0,12000, 120000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 26, 'GOLF CARTS (Personal Lines)', 'sum insured', 0,12000, 120000, 0, 0, 0, 0, 0)

				--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 27, 'GOLF CARTS (Business Insurance)', 'sum insured', 0, 12000, 200000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 28, 'GOLF CARTS (Business Insurance)', 'sum insured', 0, 12000, 200000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 29, 'GOLF CARTS (Business Insurance)', 'sum insured', 0, 12000, 200000, 0, 0, 0, 0, 0)
end
--Telesure END



--Oakhurst (Motor), Product Codes: 'OAKHURST', 'OAKHURST ASPIRE'
declare
	@Oakhurst_OAKHURST_Motor_CoverDefinitionId int,
	@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId int
	
select
	@Oakhurst_OAKHURST_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('OAKHURST') and
	Cover.Name in ('Motor')

select
	@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('OAKHURST ASPIRE') and
	Cover.Name in ('Motor')

if	isnull(@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 105, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R0 and the claim is below R5000', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 105, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R0 and the claim is below R5000', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 105, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R0 and the claim is R5000 or more', 2500, 0, 20000, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 105, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R0 and the claim is R5000 or more', 2500, 0, 20000, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 105, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R2500 or more and the claim is R5000 or more', 2500, 0, 20000, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 105, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R2500 or more and the claim is R5000 or more', 2500, 0, 20000, 0, 0, 0, 0, 0, 1)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 106, 'Accessories', 'Windscreen and specified non-standard accessories', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 106, 'Accessories', 'Windscreen and specified non-standard accessories', 0, 500, 0, 20, 1, 0, 0, 0)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where no 3rd party vehicle is involved or if you claim in the 1st 6 months from the policy start date', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where no 3rd party vehicle is involved or if you claim in the 1st 6 months from the policy start date', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where the driver at the time of the loss is under 30 years old or the driver at the time of the loss has had his/her license for less than 2 years', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where the driver at the time of the loss is under 30 years old or the driver at the time of the loss has had his/her license for less than 2 years', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents occurring between 10pm and 4am or failure to report an incident to the South African Police Service within 24 hours of the incident occurring.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents occurring between 10pm and 4am or failure to report an incident to the South African Police Service within 24 hours of the incident occurring.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where we have not received your Vehicle Inspection Certificate or where your policy requires you to have a tracking device other than a Smart-Box fitted to your vehicle and it is not fitted or fully functional, with the exception of theft or hi-jack claims which will not be covered at all. After 3 months if the Smart-Box is required and not fitted. Replace above VIC or tracking device additional excess.)', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where we have not received your Vehicle Inspection Certificate or where your policy requires you to have a tracking device other than a Smart-Box fitted to your vehicle and it is not fitted or fully functional, with the exception of theft or hi-jack claims which will not be covered at all. After 3 months if the Smart-Box is required and not fitted. Replace above VIC or tracking device additional excess.)', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where we have not received your Vehicle Inspection Certificate or where your policy requires you to have an Oakhurst Smart-Box fitted to your vehicle and it is not fitted, with the exception of theft or hi-jack claims, in which event you will not be covered at all. After 3 months if the Smart-Box is required and not fitted. Replace above VIC or tracking device additional excess.)', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where we have not received your Vehicle Inspection Certificate or where your policy requires you to have an Oakhurst Smart-Box fitted to your vehicle and it is not fitted, with the exception of theft or hi-jack claims, in which event you will not be covered at all. After 3 months if the Smart-Box is required and not fitted. Replace above VIC or tracking device additional excess.)', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
	end



--AA Excess (Motor), Product Codes: 'AA'
declare
	@AA_AA_Motor_CoverDefinitionId int
	
select
	@AA_AA_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AAC') and
	Cover.Name in ('Motor')

if	isnull(@AA_AA_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 108, 'Motor Vehicles', 'Basic excess - Additional excess may apply', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 109, 'Motorbikes', 'Flat excess - Additional excess as per motor vehicles', 3000, 3000, 3000, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 110, 'Caravans', 'Flat excess', 2500, 2500, 2500, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 111, 'Trailers', 'Flat excess', 2500, 2500, 2500, 0, 0, 0, 0, 0, 0)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 112, 'Accessories', 'Specified radio', 750, 750, 750, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 112, 'Accessories ', 'Windscreen replacement (Excluding panoramic glass)', 1500, 1500, 1500, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 112, 'Accessories', 'Vehicle glass (Excluding panoramic glass)', 1500, 1500, 1500, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 112, 'Accessories ', 'Panoramic glass', 1500, 1500, 1500, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 113, 'Additional Excess', 'If the incident driver is not the regular driver and is younger than 25 years old', 3000, 3000, 3000, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 113, 'Additional Excess', 'If the incident driver has a drivers licence for less than 2 years', 3000, 3000, 3000, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 113, 'Additional Excess', 'If the incident occurred outside of South Africa and the vehicle is not drivable', 7500, 7500, 7500, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 113, 'Additional Excess', 'With regards to a car claim that occurs within the first 6 months of cover', 7500, 7500, 7500, 0, 0, 0, 0, 0, 0)
	end


--AA Excess (All Risk), Product Codes: 'AA'
declare
	@AA_AA_AllRisk_CoverDefinitionId int
	
select
	@AA_AA_AllRisk_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AAC') and
	Cover.Name in ('All Risk')

if	isnull(@AA_AA_AllRisk_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_AllRisk_CoverDefinitionId, 114, 'General', 'Basic excess as per selected excess', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_AllRisk_CoverDefinitionId, 115, 'Factory Fitted Radio', 'If unspecified basic vehicle excess applies', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_AllRisk_CoverDefinitionId, 116, 'Portable Possessions', 'Specified (Flat excess)', 500, 500, 500, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_AllRisk_CoverDefinitionId, 116, 'Portable Possessions', 'Unspecified (Flat excess)', 500, 500, 500, 0, 0, 0, 0, 0, 0)
	end


--AA Excess (Contents), Product Codes: 'AA'
declare
	@AA_AA_Contents_CoverDefinitionId int
	
select
	@AA_AA_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AAC') and
	Cover.Name in ('Contents')

if	isnull(@AA_AA_Contents_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Contents_CoverDefinitionId, 117, 'Home Contents', 'Minimum R1,500 or maximum R4,500', 0, 1500, 4500, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Contents_CoverDefinitionId, 117, 'Additional Contents Cover', 'No excess payable with the exception of garden furniture', 0, 0, 0, 0, 0, 0, 0, 0, 0)
	end


--AA Excess (Building), Product Codes: 'AA'
declare
	@AA_AA_Building_CoverDefinitionId int
	
select
	@AA_AA_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AAC') and
	Cover.Name in ('Building')

if	isnull(@AA_AA_Building_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Building_CoverDefinitionId, 118, 'Buildings', 'Minimum R1,500 or maximum R4,500', 0, 1500, 4500, 0, 0, 0, 0, 0, 0)
	end