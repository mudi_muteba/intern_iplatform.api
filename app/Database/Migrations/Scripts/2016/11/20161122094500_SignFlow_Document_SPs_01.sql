if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_BrokerNote') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_BrokerNote
	end
go

create procedure Reports_SignFlow_BrokerNote
(	
	@Id int,
	@InsurerCode nvarchar
)
as

-- To be implemented later
select top 1
	ISNULL(ind.FirstName,'') + ' ' + ISNULL(ind.Surname,'')  as 'InsuredName',
	 (select top 1 RegisteredName from Organization where Code = @InsurerCode)  as 'Appointee'
from Party as p
join Individual as ind on ind.PartyId = p.Id
where p.Id = @Id

go

if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_CompanyDetails') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_CompanyDetails
	end
go

create procedure Reports_SignFlow_CompanyDetails
(	
	@Code nvarchar(10)	
)
as

select cd.cell, cd.Work, [add].Description 
from Channel as c
join Party as p on p.Id = c.OrganizationId
join ContactDetail as cd on cd.Id = p.ContactDetailId
join [Address] as [add] on [add].PartyId = p.Id
where c.Code = @Code
and [add].IsDefault = 1

go

if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_DebitOrder') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_DebitOrder
	end
go

create procedure Reports_SignFlow_DebitOrder
(	
	@Id int
	
)
as

select top 1
	''   as 'InsuredName',
	''   as 'InsuredId',
	cd.Work  as 'ContactWork',
	cd.Cell as 'ContactMobile',
	cd.Home       as 'ContactHome',
	ad.Description     as 'PostalAddress',
	bk.BankAccHolder     as 'BankDetailAccountName',
	bk.Bank    as 'BankDetailsBankName',
	bk.BankBranch    as 'BankDetailBranchName',
	bk.BankBranchCode as 'BankDetailBranchCode',
	bk.AccountNo    as 'BankDetailAccountNumber',
	bk.TypeAccount  as 'BankDetailAccountType'
from Party as p
join ContactDetail as cd on cd.Id = p.ContactDetailId
join [Address] as ad on ad.PartyId = p.Id
left join BankDetails as bk on bk.PartyId = p.Id
where p.Id = @Id

go

if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_PopiDoc') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_PopiDoc
	end
go

create procedure Reports_SignFlow_PopiDoc
(	
	@Id int
	
)
as

-- To be implemented later
SELECT top 1 * FROM Individual

go


if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_RecordOfAdvice') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_RecordOfAdvice
	end
go

create procedure Reports_SignFlow_RecordOfAdvice
(	
	@Id int
	
)
as

-- To be implemented later
SELECT cov.Name, SumInsured, Description from ProposalDefinition as pd
join md.Cover as cov on cov.Id = pd.CoverId
where pd.IsDeleted <> 1
and pd.PartyId = @Id



go

if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_RecordOfAdviceContact') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_RecordOfAdviceContact
	end
go

create procedure Reports_SignFlow_RecordOfAdviceContact
(	
	@Id int
	
)
as

select top 1
	ISNULL(ind.FirstName,'') + ' ' + ISNULL(ind.Surname,'')  as 'InsuredName',
	ind.IdentityNo   as 'InsuredId',
	''   as 'Broker',
	occup.Name   as 'Occupation',
	cd.Email   as 'EmailAddress',
	cd.Work  as 'ContactWork',
	cd.Cell as 'ContactMobile',
	cd.Home as 'ContactHome',
	ad.Description as 'PostalAddress',
	ad.Description as 'PhysicalAddress'
from Party as p
join ContactDetail as cd on cd.Id = p.ContactDetailId
join [Address] as ad on ad.PartyId = p.Id
join Individual as ind on ind.PartyId = p.Id
join Occupation as occup on occup.Id = ind.OccupationId
where p.Id = @Id


go


if exists (select * from sys.objects where object_id = object_id(N'Reports_SignFlow_Sla') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_SignFlow_Sla
	end
go

create procedure Reports_SignFlow_Sla
(	
	@Id int,
	@InsurerCode nvarchar
)
as

select top 1
	(select top 1 RegisteredName from Organization where Code = @InsurerCode)   as 'BrokerName',
	ISNULL(ind.FirstName,'') + ' ' + ISNULL(ind.Surname,'')  as 'ClientName',
	ind.IdentityNo  as 'ClientIndentityNo',
	ad.Description     as 'PostalAddress'	
from Party as p
join [Address] as ad on ad.PartyId = p.Id
join Individual as ind on ind.PartyId = p.Id
where p.Id = @Id

go