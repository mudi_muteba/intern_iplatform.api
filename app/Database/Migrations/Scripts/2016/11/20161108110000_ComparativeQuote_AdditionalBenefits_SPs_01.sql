﻿if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_AdditionalBenefits') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_AdditionalBenefits
	end
go

create procedure Reports_DetailedComparativeQuote_AdditionalBenefits
(
	@ProposalHeaderId int,
	@QuestionIds varchar(255)
)
as
	select
		md.Question.Name,
		ProposalQuestionAnswer.Answer
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
		inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
		inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
	where
		md.Question.Id in (select * from fn_StringListToTable(@QuestionIds)) and
		ProposalHeader.Id = @ProposalHeaderId
	order by
		md.Question.name asc
go