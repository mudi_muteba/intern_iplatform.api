if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_MotorExtract') and type in (N'P', N'PC'))
begin
	drop procedure Reports_AIG_MotorExtract
end
go

create procedure Reports_AIG_MotorExtract
(
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as

--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
--set @StartDate = '2016-08-01'
--set @EndDate = '2016-11-30'

DECLARE @tbl TABLE(QuestionId int, QuoteId int, AnswerId nvarchar(255), PartyId int, Created datetime, ProposalHeaderId int, ProposalDefinitionId int, QuestionDefDisplayName nvarchar(255))
DECLARE @tblExtra TABLE(PartyId int, AssetId int, Description nvarchar(100), Value decimal(18,2), AccessoryTypeId int)

insert into @tbl
select qd.QuestionId, qte.Id as QuoteId,pqa.Answer as AnswerId, pd.PartyId, pd.Created, pd.ProposalHeaderId, pd.Id, qd.DisplayName from ProposalDefinition as pd
				join ProposalQuestionAnswer as pqa on pqa.ProposalDefinitionId = pd.Id
				join QuestionDefinition as qd on qd.Id = pqa.QuestionDefinitionId
				join md.Question as q on q.Id = qd.QuestionId
				join ProposalHeader as ph on ph.Id = pd.ProposalHeaderId
				join QuoteHeader as qh on qh.ProposalHeaderId = ph.Id
				join Quote as qte on qte.QuoteHeaderId = qh.Id	
				join CoverDefinition as covd on pd.CoverId = covd.CoverId		
				where covd.ProductId = 46 and
				pd.CoverId = 218 and pd.IsDeleted = 0 and qte.IsDeleted =0 and qd.id = pqa.QuestionDefinitionId

insert into @tblExtra
select ast.PartyId, ast.Id as AssetId, astEx.Description, astEx.Value, astEx.AccessoryTypeId from AssetExtras as astEx
join Asset as ast on ast.Id = astEx.AssetId
where astex.IsDeleted <> 1

Select

	--prd.id as 'ProposalDefinitionId', p.id as 'PartyId', l.id as 'LeadID', Prh.id  as 'ProposalHeaderId', Prd.coverid,
	
	CASE 
		WHEN Prh.Source IS NULL THEN ('Call center')   
		ELSE Prh.Source                                     
    END as Channel, 

	CASE
		WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y'
		ELSE 'N'
	END as 'QuoteExpired',

	(CASE 
		(SELECT COALESCE((select top 1 PolicyStatusId from PolicyHeader where PartyId = p.id and QuoteId = qute.Id and PolicyStatusId = 3), 0))
			when 3 then 'Y'
			when 2 then 'N'
			when 1 then 'N'
			when 0 then 'N'
	End  )as 'QuoteBound',

	case when Qute.AcceptedOn IS NULL Then '' else CONVERT(VARCHAR(10),Qute.AcceptedOn ,111) end as 'QuoteBindDate',

	'' as 'DateOfQuote',

	case when (select sum(cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2))) + cast(round (Qute.Fees ,2) as numeric(36,2))  from QuoteItem 	
				where QuoteId = Qute.id) is null
	THEN (0) 
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2))) + cast(round (Qute.Fees ,2) as numeric(36,2))  from QuoteItem 	
				where QuoteId = Qute.id)                                          
	END 
	as 'QuoteTotalPremium',

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 218)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 218))                                          
	END AS 'QuoteMotorPremium',
	 
	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 78)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 78))                                          
	END AS 'QuoteHomePremium',

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 142)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 142))                                          
	END AS 'QuoteFuneralPremium',	

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId not in(218,78,142))) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId not in(218,78,142)))                                          
	END as 'QuoteOtherPremium',
	 
	(cast(round(Qute.fees,2) as numeric(36,2))) as 'QuoteFees',

	(select sum(cast(round (Sasria ,2) as numeric(36,2)))  from QuoteItem 		
				where QuoteId = Qute.id) as 'QuoteSasria',

	(CONVERT(VARCHAR(10),Qh.CreatedAt ,111) ) as 'QuoteIncepptionDate',	

	(select COALESCE((select top 1 PolicyNo from PolicyHeader where PartyId = p.Id and QuoteId = qute.Id and PolicyStatusId = 3),null)) as 'QuotePolicyNumber',

	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'QuoteCreateDate',
	CONVERT(VARCHAR(5),Qute.CreatedAt,108) as 'QuoteCreateTime', 

	qute.Id as 'QuoteID',
	Ind.ExternalReference as 'VMSAID',	

	(select COALESCE((select AssetNo from Asset where id = prd.AssetId),null)) as 'VehicleNumber',	

	(select AnswerId from @tbl as temp	
	where temp.QuestionId = 89 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'NightAddress-Suburb',

	(select AnswerId from @tbl as temp	
	where temp.QuestionId = 90 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'NightAddress-PostCode',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 47 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'NightAddress-Province',
	
	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 863 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'NightAddress-Storage',

	(select AnswerId from @tbl where QuestionId = 135 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DayAddress-Suburb',

	(select AnswerId from @tbl where QuestionId = 91 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DayAddress-PostCode',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 154 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DayAddress-Province',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 864 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DayAddress-Storage',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 786 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleType',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 874 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'MotorExcess',
	
	(select AnswerId from @tbl where QuestionId = 97 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'RegistrationNumber',
		
	(select AnswerId from @tbl where QuestionId = 99 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleYear',

	(select AnswerId from @tbl where QuestionId = 95 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleMake',
	(select AnswerId from @tbl where QuestionId = 96 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleModel',
	(select AnswerId from @tbl where QuestionId = 94 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleMMCode',

	(select AnswerId from @tbl where QuestionId = 310 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)as 'VINNumber',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 227 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'Colour',

	case (select AnswerId from @tbl where QuestionId = 143 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) 						
	when 519 then 'Y' else 'N'			
	end  as 'MetallicPaint',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 230 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'CoverType',

	(case 
		(select  AnswerId from @tbl where QuestionId = 255 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Modified',

	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 867 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VehicleStatus',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 226 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'ClassOfUse',

	(case 
		(select  AnswerId from @tbl where QuestionId = 232 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Delivery',
	
	Case (select  AnswerId from @tbl where QuestionId = 870 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)				
		when null then 'N' 	
		when 'false' then 'N'
		when 'true' then 'Y'	
	end  as 'TrackingDevice', 


	Case (select  AnswerId from @tbl where QuestionId = 298 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)				
		when null then 'N' 	
		when 'false' then 'N'
		when 'true' then 'Y'	
	end  as 'DiscountedTrackingDevice',	

	(case 
		(select AnswerId from @tbl where QuestionId = 245 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Immobiliser',

	(case 
		(select AnswerId from @tbl where QuestionId = 871 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'AntiHijack',

	(case 
		(select AnswerId from @tbl where QuestionId = 231 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'DataDot',
	
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 1 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 1 and AssetId = ast.Id) end as 'Acc-SoundEquipment',	
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 2 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 2 and AssetId = ast.Id) end as 'Acc-MagWheels',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 3 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 3 and AssetId = ast.Id) end as 'Acc-SunRoof',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 4 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 4 and AssetId = ast.Id) end as 'Acc-XenonLights',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 5 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 5 and AssetId = ast.Id) end as 'Acc-TowBar',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 6 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 6 and AssetId = ast.Id) end as 'Acc-BodyKit',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 7 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 7 and AssetId = ast.Id) end as 'Acc-AntiSmashAndGrab',
	case when (select sum(value) from @tblExtra where AccessoryTypeId = 8 and AssetId = ast.Id) is null then (0) else (select sum(value) from @tblExtra where AccessoryTypeId = 8 and AssetId = ast.Id) end as 'Acc-Other',
	
	(select qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 1005 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'VAP-CarHire',

	(case 
		(select AnswerId from @tbl where QuestionId = 1004 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-TyreAndRim',

	(case 
		(select  AnswerId from @tbl where QuestionId = 1003 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-ScratchAndDent',

	(case 
		(select AnswerId from @tbl where QuestionId = 873 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-CreditShortfall',

	Ind.IdentityNo as 'DriverIDNumber',
	(select name from Md.Title where Id = Ind.TitleId) as 'DriverTitle',
	Ind.FirstName as 'DriverFirstName', 
	Ind.Surname as 'DriverSurname', 

	(select name from Md.Gender where Id = Ind.GenderId) as 'DriverGender',

	CASE WHEN 
		(select name from Md.MaritalStatus where Id = Ind.MaritalStatusId) is null
	THEN ''
	ELSE 
		(select name from Md.MaritalStatus where Id = Ind.MaritalStatusId)                                          
	END as 'DriverMaritalStatus',

	CONVERT(VARCHAR(10),Ind.DateOfBirth ,111) as 'DriverDateOfBirth',
	occ.Name as 'DriverOccupation',

	(select top 1 ci.Answer 
	from LossHistory as lh
	join md.CurrentlyInsured as ci on ci.Id = lh.CurrentlyInsuredId
	where PartyId = p.Id  and IsDeleted = 0 order by lh.Id desc) as 'DriverRecentCoverIndicator',

    (select top 1 up.Answer 
	from LossHistory as lh
	join md.UninterruptedPolicy as up on up.Id = lh.UninterruptedPolicyId
	where PartyId = p.Id  and IsDeleted = 0 order by lh.Id desc) as 'DriverUninterruptedCover',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 249 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DriverLicenseType',

	(select  AnswerId from @tbl where QuestionId = 41 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id) as 'DriverLicenseDate',

	(case 
		(select AnswerId from @tbl where QuestionId = 248 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'DriverLicenseEndorsed',


	(case 
		(select AnswerId from @tbl where QuestionId = 224 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Automatic',

	(case 
		(select AnswerId from @tbl where QuestionId = 261 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Paraplegic',


	(case 
		(select AnswerId from @tbl where QuestionId = 223 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Amputee',

	(case 
		(select AnswerId from @tbl where QuestionId = 234 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Glasses',

	(case 
		(select AnswerId from @tbl where QuestionId = 862 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Financed',

	(select  qa.Answer from @tbl as temp
	join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
	where temp.QuestionId = 300 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)	 as 'ValuationMethod',

	(select SumInsured from Asset where id = prd.AssetId) as 'SumInsured', 

	(select sum(cast(round (CalculatedSasria ,2) as numeric(36,2)))  from QuoteItem 		
				where QuoteId = Qute.id) as 'SASRIA',
	p.Id as 'PartyId'	

from ProposalHeader as Prh
join ProposalDefinition as prd on prd.ProposalHeaderId = Prh.Id and prd.CoverId = 218
join QuoteHeader as Qh on qh.ProposalHeaderId = Prh.Id
join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
JOIN Party as P on p.Id = Prh.PartyId
join Lead as L on l.PartyId = p.Id
left join Individual as Ind on P.Id = Ind.PartyId
left join Occupation as Occ on Occ.Id = Ind.OccupationId
left join Asset as ast on ast.Id = prd.AssetId
left join AssetVehicle as astV on astV.AssetId = ast.Id
where qute.CreatedAt  BETWEEN @StartDate AND @EndDate 
and L.IsDeleted = 0
and Qute.IsDeleted = 0
and Prh.IsDeleted = 0
and Qh.IsDeleted = 0
and prd.IsDeleted = 0
and prd.CoverId in (218)
