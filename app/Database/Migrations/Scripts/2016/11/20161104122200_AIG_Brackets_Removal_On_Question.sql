﻿--Sum Insured
if exists(select * from QuestionDefinition where CoverDefinitionId = 147 and QuestionId=102)
begin
update QuestionDefinition
set DisplayName = 'What is the value that you would like to insure your building for?' 
where CoverDefinitionId = 147 and QuestionId=102
end


--Sum Insured
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=102)
begin
update QuestionDefinition
set DisplayName = 'What is the value that you would like to insure your contents for?' 
where CoverDefinitionId = 146 and QuestionId=102
end