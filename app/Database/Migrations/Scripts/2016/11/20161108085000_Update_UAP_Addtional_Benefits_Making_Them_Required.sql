declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode = 'UAP')
	begin

UPDATE [dbo].[QuestionDefinition]
SET DefaultValue='', RequiredForQuote = 1
WHERE QuestionId=1149 and CoverDefinitionId = 96;

UPDATE [dbo].[QuestionDefinition]
SET DefaultValue='', RequiredForQuote = 1
WHERE QuestionId=1150 and CoverDefinitionId = 96;

UPDATE [dbo].[QuestionDefinition]
SET DefaultValue='',RequiredForQuote = 1
WHERE QuestionId=1152 and CoverDefinitionId = 96;

	end
else
	begin
		return
    end