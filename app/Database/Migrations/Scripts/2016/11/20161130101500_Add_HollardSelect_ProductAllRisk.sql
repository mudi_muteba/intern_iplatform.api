﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT
 
SET @id = 4 --hollard
SET @Name = N'Hollard Select'
 

SET @ProductId = (select TOP 1 ID from product where ProductCode = 'HOLSEL')


if (@productid IS NULL)
BEGIN
--Hollard
INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          RiskItemTypeId ,
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          @Name , -- Name - nvarchar(255)
          @id , -- ProductOwnerId - int
          @id , -- ProductProviderId - int
          7 , -- ProductTypeId - int
          N'123456' , -- AgencyNumber - nvarchar(255)
          N'HOLSEL' , -- ProductCode - nvarchar(255)
          CAST(0x00009CD400000000 AS DateTime) , -- StartDate - datetime
          CAST(0x00009CD400000000 AS DateTime) , -- EndDate - datetime
          0 , -- RiskItemTypeId - int
          0 , -- IsDeleted - bit
          N'hol.png' , -- ImageName - nvarchar(1024)
          NULL , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )

		SET @id = SCOPE_IDENTITY()
 END
 ELSE
 BEGIN
	SET @id = @productid
 END

--motor

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = 17)
-- 218 is MOTOR
--78 Household Contents
--44 House Owners
--17 All Risks


IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'All Risks', @id, 17, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=127 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Desciption', @CoverDefinitionId, 127, NULL, 1, 2, 1, 1, 0,
               N'Enter a description of the item', N'' , N'', 0, 6)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=128 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Serial/IMEI Number', @CoverDefinitionId, 128, NULL, 1, 3, 1, 1, 0,
               N'What is the serial/imei number of the item?', N'' , N'', 0, 6)
              end
if not exists(select * from QuestionDefinition where QuestionId=74 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Voluntary Excess', @CoverDefinitionId, 74, NULL, 1, 20, 0, 1, 0,
               N'Does this client want a voluntary excess to reduce their premium?', N'201' , N'', 0, NULL)
             end
if not exists(select * from QuestionDefinition where QuestionId=44 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 months', @CoverDefinitionId, 44, NULL, 1, 24, 0, 0, 0,
               N'How many claims have the client registered in the past 12 months?', N'' , N'', 0, NULL)
             end
if not exists(select * from QuestionDefinition where QuestionId=45 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 to 24 months', @CoverDefinitionId, 45, NULL, 1, 25, 0, 0, 0,
               N'How many claims have the client registered in the past 12 to 24 months?', N'' , N'', 0, NULL)
             end
if not exists(select * from QuestionDefinition where QuestionId=46 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 24 to 36 months', @CoverDefinitionId, 46, NULL, 1, 26, 0, 0, 0,
               N'How many claims have the client registered in the past 24 to 36 months?', N'' , N'', 0, NULL)
             end

-------------------- Map to multi----------------------------------------------------
declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int


SET @ProductId = (select TOP 1 ID from product where ProductCode = 'HOLSEL')

if (@ProductId is null)
return

--MOTOR MAP to multi
select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = 17
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = 17   

Print('Adding Mapping to Map question Def Parent: ' + CAST(@ParentCoverDefinitionId AS VARCHAR)  + ' Child: ' + CAST(@ChildCoverDefinitionId AS VARCHAR))

INSERT into MapQuestionDefinition(ParentId,ChildId, IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
 FROM 
  (SELECT Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
 LEFT JOIN 
  (SELECT Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 74) AS ChildQuestion
   ON ParentQuestion.Id != ChildQuestion.Id

INSERT into MapQuestionDefinition(ParentId,ChildId, IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
 FROM 
  (SELECT Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
 LEFT JOIN 
  (SELECT Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 44) AS ChildQuestion
   ON ParentQuestion.Id != ChildQuestion.Id

INSERT into MapQuestionDefinition(ParentId,ChildId, IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
 FROM 
  (SELECT Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
 LEFT JOIN 
  (SELECT Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 45) AS ChildQuestion
   ON ParentQuestion.Id != ChildQuestion.Id

INSERT into MapQuestionDefinition(ParentId,ChildId, IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
 FROM 
  (SELECT Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
 LEFT JOIN 
  (SELECT Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 46) AS ChildQuestion
   ON ParentQuestion.Id != ChildQuestion.Id

Print('Done adding mappings ')

