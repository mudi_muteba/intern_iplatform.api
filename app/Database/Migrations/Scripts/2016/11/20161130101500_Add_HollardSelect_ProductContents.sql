﻿Print('Starting Select Contents ')
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT
 
SET @id = 4 --hollard
SET @Name = N'Hollard Select'
 

SET @ProductId = (select TOP 1 ID from product where ProductCode = 'HOLSEL')


if (@productid IS NULL)
BEGIN
--Hollard
INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          RiskItemTypeId ,
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          @Name , -- Name - nvarchar(255)
          @id , -- ProductOwnerId - int
          @id , -- ProductProviderId - int
          7 , -- ProductTypeId - int
          N'123456' , -- AgencyNumber - nvarchar(255)
          N'HOLSEL' , -- ProductCode - nvarchar(255)
          CAST(0x00009CD400000000 AS DateTime) , -- StartDate - datetime
          CAST(0x00009CD400000000 AS DateTime) , -- EndDate - datetime
          0 , -- RiskItemTypeId - int
          0 , -- IsDeleted - bit
          N'hol.png' , -- ImageName - nvarchar(1024)
          NULL , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )

		SET @id = SCOPE_IDENTITY()
		Print('Added New product Select ')
 END
 ELSE
 BEGIN
	SET @id = @productid
 END

--motor

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = 78)
-- 218 is MOTOR
--78 Household Contents
--44 House Owners
--17 All Risks


IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N' Household Contents', @id, 78, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
Print('Added New cover def for Select Contents ')
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END




if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address of the property where the goods are kept?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured of the contents?', N'' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 2, 1, 1, 0,
               N'What is the suburb where the contents is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=14 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Secure Complex', @CoverDefinitionId, 14, NULL, 1, 4, 0, 0, 0,
               N'Is the house where the content is located in a security complex?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=49 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Type Of Residence', @CoverDefinitionId, 49, NULL, 1, 5, 1, 1, 0,
               N'What is the type of the residence where the contents is located?', N'62' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=82 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Burglar Alarm Type', @CoverDefinitionId, 82, NULL, 1, 6, 1, 1, 0,
               N'What type of alarm does the house have where the contents is located?', N'260' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=64 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Selected Excess', @CoverDefinitionId, 64, NULL, 1, 7, 1, 1, 0,
               N'A selected excess will reduce a clients premium.', N'152' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=55 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Wall Construction', @CoverDefinitionId, 55, NULL, 1, 11, 0, 1, 0,
               N'What is the wall construction of this property?', N'95' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=56 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Roof Construction', @CoverDefinitionId, 56, NULL, 1, 12, 0, 1, 0,
               N'What is the roof contruction of this property?', N'97' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=16 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Thatch Safe', @CoverDefinitionId, 16, NULL, 1, 13, 0, 0, 0,
               N'Does this residence have thatch safe?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=57 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Unoccupied', @CoverDefinitionId, 57, NULL, 1, 13, 1, 1, 0,
               N'How often is the house unoccupied?', N'101' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=15 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Security Gates', @CoverDefinitionId, 15, NULL, 1, 14, 0, 0, 0,
               N'Does the house where the content is located have security gates?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=11 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Burglar Bars', @CoverDefinitionId, 11, NULL, 1, 15, 0, 0, 0,
               N'Does the house where the content is located have burglar bars?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=18 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Electric Fence', @CoverDefinitionId, 18, NULL, 1, 20, 0, 0, 0,
               N'Does the house where the content is located have an electric fence surrounding the property?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=74 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Voluntary Excess', @CoverDefinitionId, 74, NULL, 1, 20, 0, 1, 0,
               N'Does this client want a voluntary excess to reduce their premium?', N'201' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=76 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Accidental Damage', @CoverDefinitionId, 76, NULL, 1, 21, 0, 1, 0,
               N'Does this client want to add accidental damage?', N'228' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=27 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Any Construction Work Nearby', @CoverDefinitionId, 27, NULL, 1, 22, 0, 0, 0,
               N'Is there construction work near the item?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=36 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Subsidence and Landslip', @CoverDefinitionId, 36, NULL, 1, 22, 0, 0, 0,
               N'Does this client want to add subsidence and landslip?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=19 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Exclude Theft', @CoverDefinitionId, 19, NULL, 1, 24, 0, 0, 0,
               N'Does the client want to exlude Theft from cover?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=129 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Near Open Ground', @CoverDefinitionId, 129, NULL, 1, 26, 0, 0, 0,
               N'Is there open ground near the item?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=131 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Property Border', @CoverDefinitionId, 131, NULL, 1, 28, 1, 1, 0,
               N'Do any of these border the item?', N'463' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=52 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Situation of Residence', @CoverDefinitionId, 52, NULL, 1, 7, 0, 1, 0,
               N'Where is this residence situated?', N'80' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1053 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Burglar Bars', @CoverDefinitionId, 1053, NULL, 1, 8, 1, 1, 0,
               N'Burglar Bars', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=21 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Access Controlled Suburb', @CoverDefinitionId, 21, NULL, 1, 10, 1, 1, 0,
               N'Access Controlled Suburb', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=116 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Perimeter Wall', @CoverDefinitionId, 116, NULL, 1, 15, 1, 1, 0,
               N'Perimeter Wall', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=43 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claim Free Group', @CoverDefinitionId, 43, NULL, 1, 19, 0, 1, 0,
               N'What is the CFG for the contents?', N'1' , N'', 0, NULL)
              end

-- Map to multiquote
if not exists(select * from QuestionDefinition where QuestionId=190 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Power Surge Cover', @CoverDefinitionId, 190, NULL, 1, 0, 0, 0, 0,
               N'Do you want accidental damage cover for power surges?', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=4 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Lightning Conductor', @CoverDefinitionId, 4, NULL, 1, 310, 0, 1, 0,
               N'Lightning Conductor', N'' , N'', 0, NULL)
              end

Print('Added all the questions Select contents ')
--return--run once
--------------------UN Map FROM KP multi----------------------------------------------------


declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int


SET @ProductId = (select TOP 1 ID from product where ProductCode = 'KPIPERS2')

if (@ProductId is null)
return

--MOTOR MAP to multi
select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = 78
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = 78   


UPDATE MapQuestionDefinition
SET IsDeleted = 1
WHere id in ( 
SELECT mq.id FROM 
MapQuestionDefinition mq
INNER JOIN QuestionDefinition qd
ON qd.Id = mq.ChildId
AND qd.CoverDefinitionId = @ChildCoverDefinitionId
AND qd.QuestionId IN (21, 116)
)


Print('Removed KP questions Content ')

-------------Map to multiquote Map Question Definition---------------------------------------------------------------


SET @ProductId = (select TOP 1 ID from product where ProductCode = 'HOLSEL')

if (@ProductId is null)
return

select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = 78   


Print('Adding Mapping to Map question Def Parent: ' + CAST(@ParentCoverDefinitionId AS VARCHAR)  + ' Child: ' + CAST(@ChildCoverDefinitionId AS VARCHAR))

INSERT into MapQuestionDefinition(ParentId,ChildId, IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
 FROM 
  (SELECT Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
 LEFT JOIN 
  (SELECT Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 190) AS ChildQuestion
   ON ParentQuestion.Id != ChildQuestion.Id



INSERT into MapQuestionDefinition(ParentId,ChildId, IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
 FROM 
  (SELECT Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
 LEFT JOIN 
  (SELECT Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 4) AS ChildQuestion
   ON ParentQuestion.Id != ChildQuestion.Id

Print('Done adding mappings ')
----- ADD to multiquote   ----------------------------------------------------


Print('Adding new questions to multiquote ')


SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = 27 AND CoverId = 78)
if (@coverDefId is null)
return


if not exists(select * from QuestionDefinition where QuestionId=21 and CoverDefinitionId = @coverDefId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Access Controlled Suburb', @coverDefId, 21, NULL, 1, 10, 1, 1, 0,
               N'Access Controlled Suburb', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=116 and CoverDefinitionId = @coverDefId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Perimeter Wall', @coverDefId, 116, NULL, 1, 15, 1, 1, 0,
               N'Perimeter Wall', N'' , N'', 0, NULL)
              end

Print('Done and Done ')

