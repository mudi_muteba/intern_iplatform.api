--BUILDING
--Thatch Lapa
if exists(select * from QuestionDefinition where CoverDefinitionId = 147 and QuestionId=850)
begin
update QuestionDefinition
set DisplayName = 'Do you have a thatched lapa? If yes: Is it Attached or 5 metres or closer to the main house?' 
where CoverDefinitionId = 147 and QuestionId=850
end


--Business Conducted
if exists(select * from QuestionDefinition where CoverDefinitionId = 147 and QuestionId=853)
begin
update QuestionDefinition
set DisplayName = 'Is business conducted on the property? If yes: What type of business is conducted on the property?' 
where CoverDefinitionId = 147 and QuestionId=853
end


-------Building
--Does your home boarder on vacant land, river or sea
if exists (select * from QuestionDefinition where CoverDefinitionId = 147 and QuestionId=855)
begin
update QuestionDefinition
set DisplayName = 'Does your home border on vacant land, river or sea? If property''s border is sea/river or stream: Is the river or sea less than 300 metres from your property?' 
where CoverDefinitionId = 147 and QuestionId=855
end

--CONTENTS
--Does your home boarder on vacant land, river or sea?
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=855)
begin
update QuestionDefinition
set DisplayName = 'Does your home border on vacant land, river or sea? If property''s border is sea/river or stream: Is the river or sea less than 300 metres from your property?' 
where CoverDefinitionId = 146 and QuestionId=855
end

--Thatch Lapa
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=850)
begin
update QuestionDefinition
set DisplayName = 'Do you have a thatched lapa? If yes: Is it Attached or 5 metres or closer to the main house?' 
where CoverDefinitionId = 146 and QuestionId=850
end

--Business Conducted
if exists (select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=853)
begin
update QuestionDefinition
set DisplayName = 'Is business conducted on the property? If yes: What type of business is conducted on the property?' 
where CoverDefinitionId = 146 and QuestionId=853
end

