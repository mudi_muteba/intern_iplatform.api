declare @id int

select @id = id from QuestionDefinition where questionId = 840 and CoverDefinitionId = 149


SELECT @id

update QuestionDefinition set IsDeleted = 1 where id = @id
update ProposalQuestionAnswer set IsDeleted = 1 where QuestionDefinitionId = @id

