﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT
 
SET @id = 4 --hollard
SET @Name = N'Hollard Select'
 

SET @ProductId = (select TOP 1 ID from product where ProductCode = 'HOLSEL')


if (@productid IS NULL)
BEGIN
--Hollard
INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          RiskItemTypeId ,
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          @Name , -- Name - nvarchar(255)
          @id , -- ProductOwnerId - int
          @id , -- ProductProviderId - int
          7 , -- ProductTypeId - int
          N'123456' , -- AgencyNumber - nvarchar(255)
          N'HOLSEL' , -- ProductCode - nvarchar(255)
          CAST(0x00009CD400000000 AS DateTime) , -- StartDate - datetime
          CAST(0x00009CD400000000 AS DateTime) , -- EndDate - datetime
          0 , -- RiskItemTypeId - int
          0 , -- IsDeleted - bit
          N'hol.png' , -- ImageName - nvarchar(1024)
          NULL , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )

		SET @id = SCOPE_IDENTITY()
 END
 ELSE
 BEGIN
	SET @id = @productid
 END

--motor

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = 218) -- 218 is MOTOR

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'Motor', @id, 218, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 1, 0,
               N'I''d like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the suburb and town name where you live?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'I''d like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', N'' , N'^[0-9]+$', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 3, 1, 1, 0,
               N'Thank you, please confirm your street postal code', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 5, 1, 1, 1,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=72 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Marital Status', @CoverDefinitionId, 72, NULL, 1, 6, 0, 1, 0,
               N'What is the marital status of this client?', N'178' , N'', 0, 5)
              end
if not exists(select * from QuestionDefinition where QuestionId=41 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Drivers Licence First Issued Date', @CoverDefinitionId, 41, NULL, 1, 7, 1, 1, 0,
               N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'' , N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$', 0, 5)
              end
if not exists(select * from QuestionDefinition where QuestionId=48 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Class of Use', @CoverDefinitionId, 48, NULL, 1, 7, 1, 1, 0,
               N'{Title} {Name} is this vehicle mainly for private or business use?', N'54' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=53 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Type of Cover', @CoverDefinitionId, 53, NULL, 1, 8, 1, 1, 0,
               N'Would you like full comprehensive cover on this vehicle?', N'89' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=87 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Tracking Device', @CoverDefinitionId, 87, NULL, 1, 8, 1, 1, 0,
               N'Thank you, does the vehicle have a tracking device installed?', N'303' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=73 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Drivers Licence Type', @CoverDefinitionId, 73, NULL, 1, 8, 1, 1, 0,
               N'What is the drivers licence type of the main driver for this vehicle?', N'190' , N'', 0, 5)
              end
if not exists(select * from QuestionDefinition where QuestionId=80 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Immobiliser', @CoverDefinitionId, 80, NULL, 1, 9, 0, 1, 0,
               N'Does the vehicle have a factory fitted immobiliser?', N'250' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=85 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Overnight Parking', @CoverDefinitionId, 85, NULL, 1, 9, 1, 1, 0,
               N'Where is the vehicle parked overnight?', N'278' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=7 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Modified/Imported', @CoverDefinitionId, 7, NULL, 1, 10, 0, 0, 0,
               N'Was this vehicle imported or modified in any way?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=8 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Gear Lock', @CoverDefinitionId, 8, NULL, 1, 11, 0, 0, 0,
               N'Does the vehicle have a gearlock?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=9 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Data Dot', @CoverDefinitionId, 9, NULL, 1, 12, 0, 0, 0,
               N'Does the vehicle have data dot?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 12, 1, 1, 0,
               N'What is the sum insured of the vehicle?', N'' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=70 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Gender', @CoverDefinitionId, 70, NULL, 1, 13, 0, 1, 0,
               N'What is the gender of the main driver for this vehicle?', N'' , N'', 0, 5)
              end
if not exists(select * from QuestionDefinition where QuestionId=107 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Extras Value', @CoverDefinitionId, 107, NULL, 1, 17, 0, 1, 0,
               N'What is the value of the any non-factory extras?', N'' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=86 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Daytime Parking', @CoverDefinitionId, 86, NULL, 1, 17, 1, 1, 0,
               N'Where is the vehicle parked during the day?', N'290' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=110 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Radio Value', @CoverDefinitionId, 110, NULL, 1, 18, 0, 1, 0,
               N'What is the radio value for this vehicle?', N'' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=43 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'No Claim Bonus', @CoverDefinitionId, 43, NULL, 1, 19, 0, 1, 0,
               N'How many years has this client been accident free?', N'1' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=77 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Car Hire', @CoverDefinitionId, 77, NULL, 1, 20, 0, 1, 0,
               N'Does the client require car hire?', N'237' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=24 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Excess Buster', @CoverDefinitionId, 24, NULL, 1, 21, 0, 0, 0,
               N'Does the client want to waive the excess?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=42 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver Date of Birth', @CoverDefinitionId, 42, NULL, 1, 21, 1, 1, 0,
               N'What is the Date of Birth of the main driver of this vehicle?', N'' , N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$', 0, 5)
              end
if not exists(select * from QuestionDefinition where QuestionId=74 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Voluntary Excess', @CoverDefinitionId, 74, NULL, 1, 21, 0, 1, 0,
               N'A voluntary excess will reduce a clients premium.', N'201' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=64 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Selected Excess', @CoverDefinitionId, 64, NULL, 1, 24, 1, 1, 0,
               N'A selected excess will reduce a clients premium.', N'152' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=44 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 months', @CoverDefinitionId, 44, NULL, 1, 29, 1, 1, 0,
               N'How many claims have the client registered in the past 12 months?', N'12' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=45 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 to 24 months', @CoverDefinitionId, 45, NULL, 1, 30, 1, 1, 0,
               N'How many claims have the client registered in the past 12 to 24 months?', N'23' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=46 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 24 to 36 months', @CoverDefinitionId, 46, NULL, 1, 31, 1, 1, 0,
               N'How many claims have the client registered in the past 24 to 36 months?', N'34' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=142 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Colour', @CoverDefinitionId, 142, NULL, 1, 32, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the colour of the vehicle?', N'515' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=143 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Paint Type', @CoverDefinitionId, 143, NULL, 1, 33, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the type of paint on the vehicle?', N'521' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=65 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Valuation Method', @CoverDefinitionId, 65, NULL, 1, 43, 1, 1, 0,
               N'Please select the valuation method?', N'160' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Registration Number', @CoverDefinitionId, 97, NULL, 1, 1, 0, 0, 0,
               N'Can you please provide me with the license plate number?', N'' , N'^.{0,10}$', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 3, 1, 1, 0,
               N'{Title} {Name} what is the make and model of the vehicle?', N'' , N'', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 4, 1, 1, 0,
               N'What is the model of this vehicle?', N'' , N'', 5, 4)
              end
if not exists(select * from QuestionDefinition where QuestionId=30 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'4x4 Cover', @CoverDefinitionId, 30, NULL, 1, 24, 0, 0, 0,
               N'Would the client want to add 4x4 cover?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1099 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Car Hire Group', @CoverDefinitionId, 1099, NULL, 1, 21, 1, 0, 0,
               N'Car Hire Group', N'' , N'', 0, NULL)
              end



--return--run once
--------------------MOTOR MAP to multi----------------------------------------------------


declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int


SET @ProductId = (select TOP 1 ID from product where ProductCode = 'HOLSEL')

if (@ProductId is null)
return

--MOTOR MAP to multi
select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = 218
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = 218   



INSERT into MapQuestionDefinition(ParentId,ChildId) SELECT ParentQuestion.Id, ChildQuestion.Id
 FROM 
  (SELECT Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId =1081) AS ParentQuestion
 LEFT JOIN 
  (SELECT Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 1099) AS ChildQuestion
   ON ParentQuestion.Id != ChildQuestion.Id