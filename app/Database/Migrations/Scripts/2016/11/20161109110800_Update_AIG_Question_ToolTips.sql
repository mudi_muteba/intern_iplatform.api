--Main Driver Date of Birth
if exists (select * from QuestionDefinition where CoverDefinitionId =145 and QuestionId=42)
begin
update QuestionDefinition
set DisplayName = 'Regular Driver Date of Birth?'
where CoverDefinitionId = 145 and QuestionId=42
end
go

--Removing tooltips on cars
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId not in (310,226,862,1005))
begin
update QuestionDefinition
set ToolTip = ''
where CoverDefinitionId = 145 and QuestionId not in (310,226,862,1005)
end
go

--Removing tooltips on Building
if exists (select * from QuestionDefinition where QuestionId not in (848,849,844,859,857) and CoverDefinitionId=147)
begin
update QuestionDefinition
set ToolTip = ''
where QuestionId not in (848,849,844,859,857) and CoverDefinitionId=147
end
go

--Removing tooltips on Contents
if exists (select * from QuestionDefinition where QuestionId not in (848,849,858,859) and CoverDefinitionId=146)
begin
update QuestionDefinition
set ToolTip = ''
where QuestionId not in (848,849,858,859) and CoverDefinitionId=146
end
go