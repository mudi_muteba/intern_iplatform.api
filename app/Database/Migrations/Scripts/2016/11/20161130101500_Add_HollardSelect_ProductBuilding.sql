﻿Print(' Start Buildings ')
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT
 
SET @id = 4 --hollard
SET @Name = N'Hollard Select'
 

SET @ProductId = (select TOP 1 ID from product where ProductCode = 'HOLSEL')


if (@productid IS NULL)
BEGIN
--Hollard
INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          RiskItemTypeId ,
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          @Name , -- Name - nvarchar(255)
          @id , -- ProductOwnerId - int
          @id , -- ProductProviderId - int
          7 , -- ProductTypeId - int
          N'123456' , -- AgencyNumber - nvarchar(255)
          N'HOLSEL' , -- ProductCode - nvarchar(255)
          CAST(0x00009CD400000000 AS DateTime) , -- StartDate - datetime
          CAST(0x00009CD400000000 AS DateTime) , -- EndDate - datetime
          0 , -- RiskItemTypeId - int
          0 , -- IsDeleted - bit
          N'hol.png' , -- ImageName - nvarchar(1024)
          NULL , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )

		SET @id = SCOPE_IDENTITY()
 END
 ELSE
 BEGIN
	SET @id = @productid
 END

--motor

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = 44)
-- 218 is MOTOR
--78 Household Contents
--44 House Owners
--17 All Risks


IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'House Owners', @id, 44, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

Print(' Start Questions ')
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 2, 1, 1, 0,
               N'What is the sum insured of the property?', N'' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 3, 1, 1, 0,
               N'What is the postal code where the property is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 4, 1, 1, 0,
               N'What is the suburb name where the property is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=56 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Roof Construction', @CoverDefinitionId, 56, NULL, 1, 11, 1, 1, 0,
               N'What is the roof construction of the property?', N'97' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=55 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Wall Construction', @CoverDefinitionId, 55, NULL, 1, 12, 1, 1, 0,
               N'What is the wall construction of the property?', N'95' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=36 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Subsidence and Landslip', @CoverDefinitionId, 36, NULL, 1, 13, 0, 0, 0,
               N'Does this client want to add subsidence and landslip?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=57 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Unoccupied', @CoverDefinitionId, 57, NULL, 1, 14, 1, 1, 0,
               N'How often is the Property unoccupied?', N'101' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=43 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'No Claim Bonus', @CoverDefinitionId, 43, NULL, 1, 15, 1, 1, 0,
               N'How many years have this client been claim free?', N'1' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=64 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Selected Excess', @CoverDefinitionId, 64, NULL, 1, 16, 1, 1, 0,
               N'A selected excess will reduce a clients premium.', N'152' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=74 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Voluntary Excess', @CoverDefinitionId, 74, NULL, 1, 17, 1, 1, 0,
               N'Does this client want a voluntary excess to reduce their premium?', N'201' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=190 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Power Surge Cover', @CoverDefinitionId, 190, NULL, 1, 0, 0, 0, 0,
               N'Do you want accidental damage cover for power surges?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1035 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Building Usage', @CoverDefinitionId, 1035, NULL, 1, 6, 1, 1, 0,
               N'What is this residence used for?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=16 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Thatch Safe', @CoverDefinitionId, 16, NULL, 1, 16, 0, 1, 0,
               N'Is the thatch treated with thatch safe?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1037 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Commune', @CoverDefinitionId, 1037, NULL, 1, 24, 1, 1, 0,
               N'Commune', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=4 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Lightning Conductor', @CoverDefinitionId, 4, NULL, 1, 25, 1, 1, 0,
               N'Lightning Conductor', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1047 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Number Of Non Solar Geysers', @CoverDefinitionId, 1047, NULL, 1, 33, 1, 1, 0,
               N'Number Of Non Solar Geysers', N'0' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1048 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Number Of Solar Geysers', @CoverDefinitionId, 1048, NULL, 1, 34, 1, 1, 0,
               N'Number Of Solar Geysers', N'0' , N'^[0-9\.]+$', 0, NULL)
              end
-- Map To MultiQuote
if not exists(select * from QuestionDefinition where QuestionId=1189 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Solar Geyser Amount Value', @CoverDefinitionId, 1189, NULL, 1, 40, 1, 0, 0,
               N'Solar Geyser Amount Value', N'0' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1188 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Generator Amount Value', @CoverDefinitionId, 1188, NULL, 1, 41, 1, 0, 0,
               N'Generator Amount Value', N'0' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1187 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Solar Panel Amount Value', @CoverDefinitionId, 1187, NULL, 1, 42, 1, 0, 0,
               N'Solar Panel Amount Value', N'0' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1186 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Machinery Amount Value', @CoverDefinitionId, 1186, NULL, 1, 43, 1, 0, 0,
               N'Machinery Amount Value', N'0' , N'^[0-9\.]+$', 0, NULL)
              end



--return--run once
--------------------UN Map FROM KP multi----------------------------------------------------

Print(' un map from mq ')
declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int


SET @ProductId = (select TOP 1 ID from product where ProductCode = 'KPIPERS2')

if (@ProductId is null)
return

--MOTOR MAP to multi
select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = 44
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = 44   


UPDATE MapQuestionDefinition
SET IsDeleted = 1
WHere id in ( 
SELECT mq.id FROM 
MapQuestionDefinition mq
INNER JOIN QuestionDefinition qd
ON qd.Id = mq.ChildId
AND qd.CoverDefinitionId = @ChildCoverDefinitionId
AND qd.QuestionId IN (16) -- Thatch Safe
)

-------------Map to multiquote Map Question Definition---------------------------------------------------------------


SET @ProductId = (select TOP 1 ID from product where ProductCode = 'HOLSEL')

if (@ProductId is null)
return

select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = 44   


Print('Adding Mapping to Map question Def Parent: ' + CAST(@ParentCoverDefinitionId AS VARCHAR)  + ' Child: ' + CAST(@ChildCoverDefinitionId AS VARCHAR))

INSERT into MapQuestionDefinition(ParentId,ChildId, IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
 FROM 
  (SELECT Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
 LEFT JOIN 
  (SELECT Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 1186) AS ChildQuestion
   ON ParentQuestion.Id != ChildQuestion.Id

INSERT into MapQuestionDefinition(ParentId,ChildId, IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
 FROM 
  (SELECT Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
 LEFT JOIN 
  (SELECT Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 1187) AS ChildQuestion
   ON ParentQuestion.Id != ChildQuestion.Id


INSERT into MapQuestionDefinition(ParentId,ChildId, IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
 FROM 
  (SELECT Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
 LEFT JOIN 
  (SELECT Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 1188) AS ChildQuestion
   ON ParentQuestion.Id != ChildQuestion.Id

INSERT into MapQuestionDefinition(ParentId,ChildId, IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
 FROM 
  (SELECT Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
 LEFT JOIN 
  (SELECT Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 1189) AS ChildQuestion
   ON ParentQuestion.Id != ChildQuestion.Id


Print('Done adding mappings ')


----- ADD to multiquote   ----------------------------------------------------

Print(' Add to MQ product ')
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = 27 AND CoverId = 44)
if (@coverDefId is null)
return

if not exists(select * from QuestionDefinition where QuestionId=16 and CoverDefinitionId = @coverDefId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Thatch Safe', @coverDefId, 16, NULL, 1, 16, 0, 1, 0,
               N'Is the thatch treated with thatch safe?', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=1190 and CoverDefinitionId = @coverDefId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Power Surge Sum Insured', @coverDefId, 1190, NULL, 1, 0, 0, 0, 0,
               N'What is the sum insured for Power Surge damage?', N'' , N'', 0, NULL)
              end

Print(' Remove From MQ product ')

if exists (select * from QuestionDefinition where QuestionId=191 and CoverDefinitionId = @coverDefId)
BEGIN
	UPDATE QuestionDefinition
		SET Hide = 1
	WHERE 
		QuestionId=191 	and 
		CoverDefinitionId = @coverDefId
END


Print(' Done ')