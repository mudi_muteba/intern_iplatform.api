﻿
declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode = 'UAP')
	begin

	UPDATE md.question SET  QuestionTypeId = 3 WHERE Id = 1149
	
	UPDATE md.question SET  QuestionTypeId = 3 WHERE Id =1150
	
	UPDATE md.question SET  QuestionTypeId = 3 WHERE Id =1152
	

if not exists(select Id from [md].[QuestionAnswer]  where QuestionId = 1149)

	begin

	INSERT INTO md.QuestionAnswer (Id,Name, Answer, QuestionId, VisibleIndex)
	VALUES	(11859,'Forced ATM Withdrwal -Value 0', '0', 1149, 0),
		(11860,'Forced ATM Withdrwal -Value 20 000', '20 000', 1149, 1)	
		
	end
	
	if not exists(select Id from [md].[QuestionAnswer]  where QuestionId = 1149)

	begin

	INSERT INTO md.QuestionAnswer (Id,Name, Answer, QuestionId, VisibleIndex)
	VALUES	(11861,'Loss Of Personal Effects -Value 0', '0', 1150, 0),
		(11862,'Loss Of Personal Effects -Value 10 000', '10 000', 1150, 1)	
		
	end
	
if not exists(select Id from [md].[QuestionAnswer]  where QuestionId = 1150)

	begin

	INSERT INTO md.QuestionAnswer (Id,Name, Answer, QuestionId, VisibleIndex)
	VALUES	(11863,'Out Of Station Accommodation -Value 0', '0', 1152, 0),
		(11864,'Out Of Station Accommodation -Value 5 000', '5 000', 1152, 1)	
		
	end
		  

	end
else
	begin
		return
    end