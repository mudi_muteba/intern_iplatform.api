﻿if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics
	end
go

create procedure Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics
(
	@CampaignIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign)
			as 
			(
				select distinct
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
				where
					LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
					QuoteHeader.IsDeleted = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0 --and
					--(
					--	Organization.RegisteredName like '%Auto & General%' or 
					--	Organization.RegisteredName like '%First for Women%' or 
					--	Organization.RegisteredName like '%Unity Insu%'
					--)
				group by
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name
				)
			select
				Insurer,
				Product,
				Campaign,
				(
					select
						isnull(count(LeadActivity.Id), 0)
					from LeadActivity							
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					select
						isnull(count(LeadActivity.Id), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteHeader.IsDeleted = 0
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteHeader.IsDeleted = 0
							)
							/
							(
								select
									case when isnull(count(LeadActivity.Id), 0) = 0 then 1
									else cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0)) end
								from LeadActivity							
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsDeleted = 0 and
									Quote.IsDeleted = 0
							)
						)
						* 100
						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						cast(isnull(avg(QuoteItem.Premium), 0) as numeric(18, 2))
					from Quote
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.QuoteId = Quote.Id
						inner join LeadActivity on LeadActivity.Id = QuoteAcceptedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0 and
						QuoteHeader.IsDeleted = 0
				) AveragePremiumPerAcceptedQuote,
				(
					select
						cast(isnull(avg(QuoteItem.Premium), 0) as numeric(18, 2))
					from QuoteHeader
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0 and
						QuoteHeader.IsDeleted = 0
				) AveragePremiumPerQuote
			from
				Insurers_CTE
			order by
				Campaign,
				Insurer,
				Product
		end
	else
		begin
			;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign)
			as 
			(
				select distinct
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
				where
					QuoteHeader.IsDeleted = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name
				)
			select
				Insurer,
				Product,
				Campaign,
				(
					select
						isnull(count(LeadActivity.Id), 0)
					from LeadActivity							
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					select
						isnull(count(LeadActivity.Id), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteHeader.IsDeleted = 0
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									Organization.PartyId = Insurers_CTE.InsurerId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteHeader.IsDeleted = 0
							)
							/
							(
								select
									case when isnull(count(LeadActivity.Id), 0) = 0 then 1
									else cast(isnull(count(LeadActivity.Id), 0) as numeric(18, 0)) end
								from LeadActivity							
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
								where
									Organization.PartyId = Insurers_CTE.InsurerId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsDeleted = 0 and
									Quote.IsDeleted = 0
							)
						)
						* 100
						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						cast(isnull(avg(QuoteItem.Premium), 0) as numeric(18, 2))
					from Quote
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.QuoteId = Quote.Id
						inner join LeadActivity on LeadActivity.Id = QuoteAcceptedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0 and
						QuoteHeader.IsDeleted = 0
				) AveragePremiumPerAcceptedQuote,
				(
					select
						cast(isnull(avg(QuoteItem.Premium), 0) as numeric(18, 2))
					from QuoteHeader
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						Organization.PartyId = Insurers_CTE.InsurerId and
						QuoteHeader.IsDeleted = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) AveragePremiumPerQuote
			from
				Insurers_CTE
			order by
				Campaign,
				Insurer,
				Product
		end
go