if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_BuildingExtract') and type in (N'P', N'PC'))
begin
	drop procedure Reports_AIG_BuildingExtract
end
go

create procedure Reports_AIG_BuildingExtract
(
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as

--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
--set @StartDate = '2015-08-01'
--set @EndDate = '2016-11-30'

DECLARE @tbl TABLE(QuestionId int, QuoteId int, AnswerId nvarchar(255), PartyId int, Created datetime, ProposalHeaderId int, ProposalDefinitionId int, QuestionDefDisplayName nvarchar(255))

insert into @tbl
select qd.QuestionId, qte.Id as QuoteId,pqa.Answer as AnswerId, pd.PartyId, pd.Created, pd.ProposalHeaderId, pd.Id, qd.DisplayName from ProposalDefinition as pd
				join ProposalQuestionAnswer as pqa on pqa.ProposalDefinitionId = pd.Id
				join QuestionDefinition as qd on qd.Id = pqa.QuestionDefinitionId
				join md.Question as q on q.Id = qd.QuestionId
				join ProposalHeader as ph on ph.Id = pd.ProposalHeaderId
				join QuoteHeader as qh on qh.ProposalHeaderId = ph.Id
				join Quote as qte on qte.QuoteHeaderId = qh.Id	
				join CoverDefinition as covd on pd.CoverId = covd.CoverId		
				where covd.ProductId = 46 and
				pd.CoverId = 44 and pd.IsDeleted = 0 and qte.IsDeleted =0 and qd.id = pqa.QuestionDefinitionId
Select

	--prd.id as 'ProposalDefinitionId', p.id as 'PartyId', l.id as 'LeadID', Prh.id  as 'ProposalHeaderId', Prd.coverid,
	
	CASE 
		WHEN Prh.Source IS NULL THEN ('Call center')   
		ELSE Prh.Source                                     
    END as Channel, 

	CASE
		WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y'
		ELSE 'N'
	END as 'QuoteExpired',

		(CASE 
		(SELECT COALESCE((select top 1 PolicyStatusId from PolicyHeader where PartyId = p.id and QuoteId = qute.Id and PolicyStatusId = 3), 0))
			when 3 then 'Y'
			when 2 then 'N'
			when 1 then 'N'
			when 0 then 'N'
	End  )as 'QuoteBound',

	case when Qute.AcceptedOn IS NULL Then '' else CONVERT(VARCHAR(10),Qute.AcceptedOn ,111) end as 'QuoteBindDate',

	case when (select sum(cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2))) + cast(round (Qute.Fees ,2) as numeric(36,2))  from QuoteItem 	
				where QuoteId = Qute.id) is null
	THEN (0) 
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2))) + cast(round (Qute.Fees ,2) as numeric(36,2))  from QuoteItem 	
				where QuoteId = Qute.id)                                          
	END 
	as 'QuoteTotalPremium',

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 218)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 218))                                          
	END AS 'QuoteMotorPremium',
	 
	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 78)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 78))                                          
	END AS 'QuoteHomePremium',

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 142)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 142))                                          
	END AS 'QuoteFuneralPremium',	

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId not in(218,78,142))) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId not in(218,78,142)))                                          
	END as 'QuoteOtherPremium',
	 
	(cast(round(Qute.fees,2) as numeric(36,2))) as 'QuoteFees',

	(select sum(cast(round (Sasria ,2) as numeric(36,2)))  from QuoteItem 		
				where QuoteId = Qute.id) as 'QuoteSasria',

	(CONVERT(VARCHAR(10),Qh.CreatedAt ,111) ) as 'QuoteIncepptionDate',	

	(select top 1 PolicyNo from PolicyHeader where PartyId = p.Id and QuoteId = qute.Id and PolicyStatusId = 3) as 'QuotePolicyNumber',

	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'QuoteCreateDate',
	CONVERT(VARCHAR(5),Qute.CreatedAt,108) as 'QuoteCreateTime', 

	qute.Id as 'QuoteID',
	Ind.ExternalReference as 'VMSAID',

	(select AssetNo from Asset where id = prd.AssetId) as 'PropertyNumber',
		
	(select DisplayName from CoverDefinition where CoverId =  prd.CoverId and ProductId = 46) as 'CoverType',	

	(select line1
				from Address as [add]
				join AssetAddress as astAdd on astAdd.AddressId = [add].Id				
				join ProposalDefinition as pr on pr.AssetId = astAdd.AssetId
				where astAdd.AssetId = prd.AssetId
				and pr.Id = prd.Id
				and [add].IsDeleted = 0) as 'Address-Suburb',

		(select [add].Code
				from Address as [add]
				join AssetAddress as astAdd on astAdd.AddressId = [add].Id				
				join ProposalDefinition as pr on pr.AssetId = astAdd.AssetId
				where astAdd.AssetId = prd.AssetId
				and pr.Id = prd.Id
				and [add].IsDeleted = 0) as 'Address-PostCode',

		(select sp.Name
				from Address as [add]
				join AssetAddress as astAdd on astAdd.AddressId = [add].Id
				join md.StateProvince as sp on sp.Id = [add].StateProvinceId
				join ProposalDefinition as pr on pr.AssetId = astAdd.AssetId
				where astAdd.AssetId = prd.AssetId
				and pr.Id = prd.Id
				and [add].IsDeleted = 0) as 'Address-Province',

		(select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 848 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		  as 'HomeType',		

		(select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 410 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		 as 'RoofConstruction',

		 (select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 428 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		 as 'WallConstruction',

		(select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 850 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		  as 'ThatchLapa',

		(case 
			(select  AnswerId from @tbl as temp				
				where temp.QuestionId = 851 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
					when 'False' then 'N'
					when 'true' then 'Y'
		End)   as 'ThatchLapaSize',
		

		''  as 'LightningConductor',


		(select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 855 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		as 'PropertyBorder',


		(select AnswerId from @tbl as temp		
		where temp.QuestionId = 845 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		as 'ConstructionYear',

		(select AnswerId from @tbl as temp		
		where temp.QuestionId = 849 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		as 'SquareMetresOfProperty',

		(select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 846 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		  as 'NumberOfBathrooms',

		(case (select  AnswerId from @tbl as temp				
				where temp.QuestionId = 856 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End) as 'PropertyUnderConstruction',


		(select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 847 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		     as 'HomeUsage',

		(select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 853 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		     as 'BusinessConducted',

		(case (select AnswerId from @tbl as temp		
		where temp.QuestionId = 854 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)   as 'Commune',

		(select  qa.Answer from @tbl as temp
		join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
		where temp.QuestionId = 852 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		     as 'PeriodPropertyIsUnoccupied',

		case (select top 1 isnumeric(AnswerId) from @tbl as temp		
		where temp.QuestionId = 843 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
			when 0 then (select AnswerId from @tbl as temp		
		where temp.QuestionId = 843 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
			when 1 then ((select  qa.Answer from @tbl as temp
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 843 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id))
		End as 'Ownership',

		(select  qa.Answer from @tbl as temp
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 844 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		as 'MortgageBank',

		(case (select AnswerId from @tbl as temp		
		where temp.QuestionId = 396 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)   as 'Security-GatedCommunity',

	   (case (select AnswerId from @tbl as temp		
		where temp.QuestionId = 398 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)   as 'Security-SecurityComplex',

	   (case (select AnswerId from @tbl as temp		
		where temp.QuestionId = 386 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)  as 'Security-BurglarBars',

		(case (select AnswerId from @tbl as temp	
		where temp.QuestionId = 416 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)  as 'Security-SecurityGates',

		(case (select AnswerId from @tbl as temp	
		where temp.QuestionId = 402 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)  as 'Security-AlarmWithArmedResponse',

		(select  qa.Answer from @tbl as temp
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 860 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		as 'VAPS-WaterPumpingMachinery',

		(select  qa.Answer from @tbl as temp
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 859 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		as 'VAPS-AccidentalDamage',

		(case 
			(select AnswerId from @tbl as temp	
				where temp.QuestionId = 420 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id )
				when 'False' then 'N'
				when 'true' then 'Y'
		End)  as 'VAPS-SubsidenceAndLandslip',

		(select  qa.Answer from @tbl as temp
			join md.QuestionAnswer as qa on temp.AnswerId = qa.Id
			where temp.QuestionId = 857 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		as 'Buildings-Additional-Excess',

		(select AnswerId from @tbl as temp	
			where temp.QuestionId = 102 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		as 'SumInsured',

		(select AnswerId from @tbl as temp	
			where temp.QuestionId = 155 and QuoteId = Qute.Id and PartyId = p.Id and ProposalHeaderId= prh.id and ProposalDefinitionId =prd.Id)
		as 'DiscretionaryDiscount',

		 (select sum(cast(round (CalculatedSasria ,2) as numeric(36,2)))  from QuoteItem 		
				where QuoteId = Qute.id) as 'SASRIA',
				 
		(SELECT top 1 cast(round (t.Col.value('(PremiumRisk)[1]','varchar(max)') ,2) as numeric(36,2))
			FROM
			(
				SELECT 
				cast(qute.ExternalRatingResult as xml) request
			) tbl 
				cross apply request.nodes('/QRaterAssemblyRs/QRaterRs/Policy/Buildings') as t(Col)
				where t.Col.value('(@Id)[1]','varchar(max)') in (prd.Id)
		)	as 'PremiumRisk', 
		
		(SELECT top 1 cast(round (t.Col.value('(PremiumSubsidenceAndLandslip)[1]','varchar(max)') ,2) as numeric(36,2)) 
			FROM
			(
				SELECT 
				cast(qute.ExternalRatingResult as xml) request
			) tbl 
				cross apply request.nodes('/QRaterAssemblyRs/QRaterRs/Policy/Buildings') as t(Col)
				where t.Col.value('(@Id)[1]','varchar(max)') in (prd.Id)
		)	as 'PremiumSubsidenceAndLandslip',
		
		(SELECT top 1 cast(round (t.Col.value('(PremiumWaterMachinery)[1]','varchar(max)') ,2) as numeric(36,2))
			FROM
			(
				SELECT 
				cast(qute.ExternalRatingResult as xml) request
			) tbl 
				cross apply request.nodes('/QRaterAssemblyRs/QRaterRs/Policy/Buildings') as t(Col)
				where t.Col.value('(@Id)[1]','varchar(max)') in (prd.Id)
		)	as 'PremiumWaterMachinery',

		(SELECT top 1 cast(round (t.Col.value('(PremiumTotal)[1]','varchar(max)') ,2) as numeric(36,2))
			FROM
			(
				SELECT 
				cast(qute.ExternalRatingResult as xml) request
			) tbl 
				cross apply request.nodes('/QRaterAssemblyRs/QRaterRs/Policy/Buildings') as t(Col)
				where t.Col.value('(@Id)[1]','varchar(max)') in (prd.Id)
		)	as 'PremiumTotal'

from ProposalHeader as Prh
join ProposalDefinition as prd on prd.ProposalHeaderId = Prh.Id and prd.CoverId = 44
join QuoteHeader as Qh on qh.ProposalHeaderId = Prh.Id
join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
JOIN Party as P on p.Id = Prh.PartyId
join Lead as L on l.PartyId = p.Id
left join Individual as Ind on P.Id = Ind.PartyId
where qute.CreatedAt  BETWEEN @StartDate AND @EndDate 
and L.IsDeleted = 0
and Qute.IsDeleted = 0
and Prh.IsDeleted = 0
and Qh.IsDeleted = 0
and prd.IsDeleted = 0
and prd.CoverId in (44)

