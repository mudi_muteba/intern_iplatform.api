--Car Hire	
if exists (select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=1005)
begin
update QuestionDefinition
set ToolTip = 'A valuable benefit that you get with Virgin Money insurance is that our car hire option can be used for up to 30 days. What this means for you, 
			  is that, if your car is in an accident or stolen, we''ll hire out a like for like vehicle for you to get around in.
			- This means you''ll get a rental car in the same category as your own car - so you don''t have to settle for less.  That''s not all; 
			 we''ll also provide you with a rental car if your car breaks down or goes in for a regular service. That''s a great benefit, wouldn''t you agree?
			This unique car hire option is for as little as R125 a month shall I include this in your quote?'
where CoverDefinitionId = 145 and QuestionId=1005
end