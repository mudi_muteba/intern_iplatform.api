﻿DECLARE @ChannelIdTelesure INT, @MotorWarrantyProductId INT, @PreOwnedWarrantyProductId INT
SELECT TOP 1 @ChannelIdTelesure =  Id FROM Channel WHERE  SystemId = '7BC71B67-AD3F-42A9-9979-F231F73820EB' -- TELESURE
SELECT Top 1 @PreOwnedWarrantyProductId = Id FROM Product WHERE ProductCode = 'PREOWNEDWARRANTY';
SELECT Top 1 @MotorWarrantyProductId = Id FROM Product WHERE ProductCode = 'MOTORWARRANTY';
SELECT @ChannelIdTelesure, @PreOwnedWarrantyProductId, @MotorWarrantyProductId


INSERT INTO SettingsQuoteUpload(SettingName, SettingValue, Environment, IsDeleted, ProductId, ChannelID) VALUES
--DEV
('IDS/Upload/ServiceURL',					'http://localhost/STPWS/Command.svc',		    'Dev',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/AcceptanceEmailAddress',		'monitoring@iplatform.co.za',					'Dev',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/ErrorEmailAddress',			'monitoring@iplatform.co.za',					'Dev',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/RetryDelayIncrement',			'5',											'Dev',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/MaxRetries',					'2',											'Dev',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/subject',						'UPLOADING LEAD TO IDS',						'Dev',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/template',						'LeadUploadTemplate',							'Dev',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/ServiceURL',					'http://localhots/STPWS/Command.svc',			'Dev',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/AcceptanceEmailAddress',		'monitoring@iplatform.co.za',					'Dev',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/ErrorEmailAddress',			'monitoring@iplatform.co.za',					'Dev',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/RetryDelayIncrement',			'5',											'Dev',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/MaxRetries',					'2',											'Dev',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/subject',						'UPLOADING LEAD TO IDS',						'Dev',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/template',						'LeadUploadTemplate',							'Dev',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
--Staging
('IDS/Upload/ServiceURL',					'http://localhost/STPWS/Command.svc',		    'Staging',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/AcceptanceEmailAddress',		'monitoring@iplatform.co.za',					'Staging',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/ErrorEmailAddress',			'monitoring@iplatform.co.za',					'Staging',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/RetryDelayIncrement',			'5',											'Staging',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/MaxRetries',					'2',											'Staging',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/subject',						'UPLOADING LEAD TO IDS',						'Staging',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/template',						'LeadUploadTemplate',							'Staging',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/ServiceURL',					'http://localhots/STPWS/Command.svc',			'Staging',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/AcceptanceEmailAddress',		'monitoring@iplatform.co.za',					'Staging',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/ErrorEmailAddress',			'monitoring@iplatform.co.za',					'Staging',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/RetryDelayIncrement',			'5',											'Staging',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/MaxRetries',					'2',											'Staging',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/subject',						'UPLOADING LEAD TO IDS',						'Staging',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/template',						'LeadUploadTemplate',							'Staging',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
--UAT
('IDS/Upload/ServiceURL',					'http://localhost/STPWS/Command.svc',		    'UAT',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/AcceptanceEmailAddress',		'monitoring@iplatform.co.za',					'UAT',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/ErrorEmailAddress',			'monitoring@iplatform.co.za',					'UAT',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/RetryDelayIncrement',			'5',											'UAT',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/MaxRetries',					'2',											'UAT',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/subject',						'UPLOADING LEAD TO IDS',						'UAT',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/template',						'LeadUploadTemplate',							'UAT',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/ServiceURL',					'http://localhots/STPWS/Command.svc',			'UAT',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/AcceptanceEmailAddress',		'monitoring@iplatform.co.za',					'UAT',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/ErrorEmailAddress',			'monitoring@iplatform.co.za',					'UAT',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/RetryDelayIncrement',			'5',											'UAT',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/MaxRetries',					'2',											'UAT',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/subject',						'UPLOADING LEAD TO IDS',						'UAT',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/template',						'LeadUploadTemplate',							'UAT',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
--Live
('IDS/Upload/ServiceURL',					'http://localhost/STPWS/Command.svc',		    'Live',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/AcceptanceEmailAddress',		'monitoring@iplatform.co.za',					'Live',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/ErrorEmailAddress',			'monitoring@iplatform.co.za',					'Live',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/RetryDelayIncrement',			'5',											'Live',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/MaxRetries',					'2',											'Live',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/subject',						'UPLOADING LEAD TO IDS',						'Live',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/template',						'LeadUploadTemplate',							'Live',	0,	@MotorWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/ServiceURL',					'http://localhots/STPWS/Command.svc',			'Live',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/AcceptanceEmailAddress',		'monitoring@iplatform.co.za',					'Live',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/ErrorEmailAddress',			'monitoring@iplatform.co.za',					'Live',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/RetryDelayIncrement',			'5',											'Live',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/MaxRetries',					'2',											'Live',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/subject',						'UPLOADING LEAD TO IDS',						'Live',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure),
('IDS/Upload/template',						'LeadUploadTemplate',							'Live',	0,	@PreOwnedWarrantyProductId,	 @ChannelIdTelesure);


INSERT INTO ChannelEvent(EventName, ChannelId, IsDeleted, ProductCode)
VALUES ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', @ChannelIdTelesure, 0, 'MOTORWARRANTY');

DECLARE @MotorWarantyEvent INT;
SELECT TOP 1 @MotorWarantyEvent = SCOPE_IDENTITY();



INSERT INTO ChannelEvent(EventName, ChannelId, IsDeleted, ProductCode)
VALUES ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', @ChannelIdTelesure, 0, 'PREOWNEDWARRANTY');

DECLARE @PreOwnedWarantyEvent INT;
SELECT TOP 1 @PreOwnedWarantyEvent = SCOPE_IDENTITY();

INSERT INTO ChannelEventTask(TaskName, ChannelEventId, IsDeleted)
VALUES	(2, @MotorWarantyEvent, 0),
		(2, @PreOwnedWarantyEvent, 0);