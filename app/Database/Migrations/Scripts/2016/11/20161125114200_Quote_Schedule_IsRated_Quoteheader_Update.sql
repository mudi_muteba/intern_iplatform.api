if exists (select * from sys.objects where object_id = object_id(N'Reports_QuoteSchedule_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_QuoteSchedule_Header
	end
go

create procedure Reports_QuoteSchedule_Header
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@Code varchar(50)
)
as
	select distinct top 1
		Client.DisplayName Client,

		(
			select top 1
				case when [Address].IsComplex = 1
					then ltrim(rtrim([Address].Complex)) + ' ' + ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' + ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim([Address].Code))
					else ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' +  ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim( [Address].Code))
				end
			from [Address]
			where
				[Address].PartyId = Client.Id and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address') and
				[Address].IsDeleted = 0
		) PhysicalAddress,
		(
			select top 1
				case when [Address].IsComplex = 1
					then ltrim(rtrim([Address].Complex)) + ' ' + ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' + ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim([Address].Code))
					else ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' +  ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim( [Address].Code))
				end
			from [Address]
			where
				[Address].PartyId = Client.Id and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address') and
				[Address].IsDeleted = 0
		) PostalAddress,

		ClientContactDetail.Work WorkNo,
		ClientContactDetail.Home HomeNo,
		ClientContactDetail.Fax FaxNo,
		ClientContactDetail.Cell CellNo,
		ClientContactDetail.Email EmailAddress,

		case
			when len(ltrim(rtrim(ClientContact.IdentityNo))) = 0 then ClientContact.PassportNo
			else ClientContact.IdentityNo
		end IDNumber,

		Quote.QuoteHeaderId,
		Quote.InsurerReference QuoteNo,
		Product.QuoteExpiration,
		Quote.Fees,
		(
			select
				Sum(QuoteItem.Sasria)
			from QuoteItem
			where
				QuoteItem.IsDeleted = 0 and
				QuoteItem.QuoteId = Quote.Id and
				QuoteItem.Sasria is not null and
				QuoteItem.Sasria > 0
		) SASRIA,
		(
			select
				Sum(QuoteItem.Premium) + Sum(QuoteItem.Sasria) + Quote.Fees
			from QuoteItem
			where
				QuoteItem.IsDeleted = 0 and
				QuoteItem.QuoteId = Quote.Id and
				QuoteItem.Premium is not null and
				QuoteItem.Premium > 0
		) Total,
		(
			select top 1
				QuoteEMailBody
			from Organization
			where
				Code = @Code
		) QuoteEMailBody,
		(
			select top 1
				Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code
			from [Address]
			where
				[Address].PartyId = Client.Id and
				[Address].IsDefault = 1 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address')
		) DefaultPhysical,
		(
			select top 1
				Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code
			from [Address]
			where
				[Address].PartyId = Client.Id and
				[Address].IsDefault = 1 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address')
		) DefaultPostal,
		(
			select top 1
				Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code
			from [Address]
			where
				[Address].PartyId = Client.Id and
				[Address].IsDefault = 0 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address')
		) NoDefaultPhysical,
		(
			select top 1
				Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code
			from [Address]
			where
				[Address].PartyId = Client.Id and
				[Address].IsDefault = 0 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address')
		) NoDefaultPostal,
		Individual.FirstName,
		Individual.Surname
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join Product on Product.Id = Quote.ProductId
		inner join LeadActivity on LeadActivity.Id = ProposalHeader.LeadActivityId
		inner join Party Client on Client.Id = LeadActivity.PartyId
		inner join Individual on Individual.PartyId = Client.Id
		--inner join Party [Owner] on [Owner].Id = Product.ProductOwnerId
		--inner join Party [Provider] on [Provider].Id = Product.ProductProviderId
		--inner join Organization OwnerOrg on OwnerOrg.PartyId = [Owner].Id
		--inner join Organization ProviderOrg on ProviderOrg.PartyId = [Provider].Id
		left join [Address] ClientAddress on ClientAddress.PartyId = Client.Id
		left join Contact ClientContact on ClientContact.PartyId = Client.Id
		left join ContactDetail ClientContactDetail on ClientContactDetail.Id = Client.ContactDetailId
	where
		ProposalHeader.Id = @ProposalHeaderId and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		Product.IsDeleted = 0 and
		LeadActivity.IsDeleted = 0 and
		Client.IsDeleted = 0 and
		ClientAddress.IsDeleted = 0 and
		ClientContactDetail.IsDeleted = 0 and
		Quote.Id in (select * from fn_StringListToTable(@QuoteIds))
go

--exec Reports_QuoteSchedule_Header 890, '2556', 'VRGNMNY'




if exists (select * from sys.objects where object_id = object_id(N'Reports_QuoteSchedule_Covers') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_QuoteSchedule_Covers
	end
go

create procedure Reports_QuoteSchedule_Covers
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50)
)
as
;with Covers_CTE (CoverId, [Description])
	as (
		select distinct
			md.Cover.Id CoverId,
			case
				when md.Cover.Name = 'AIG Assist'
				then 'VMI Assist'
			else
				md.Cover.Name
			end Description
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
		where
			ProposalHeader.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			CoverDefinition.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			Quote.Id in (select * from fn_StringListToTable(@QuoteIds))
		)
	select
		CoverId,
		[Description]
	from
		Covers_CTE
	order by
		[Description] asc
go

--exec Reports_QuoteSchedule_Covers 890, '2556'




if exists (select * from sys.objects where object_id = object_id(N'Reports_QuoteSchedule_Totals') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_QuoteSchedule_Totals
	end
go

create procedure Reports_QuoteSchedule_Totals
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50)
)
as
	select
		Quote.Fees,
		(
			select
				Sum(QuoteItem.Sasria)
			from QuoteItem
			where
				QuoteItem.IsDeleted = 0 and
				QuoteItem.QuoteId = Quote.Id and
				QuoteItem.Sasria is not null and
				QuoteItem.Sasria > 0
		) SASRIA,
		(
			select
				Sum(QuoteItem.Premium)
			from QuoteItem
			where
				QuoteItem.IsDeleted = 0 and
				QuoteItem.QuoteId = Quote.Id and
				QuoteItem.Premium is not null and
				QuoteItem.Premium > 0
		) MonthlyPremium,
		(
			select
				Sum(QuoteItem.Premium) + Sum(QuoteItem.Sasria) + Quote.Fees
			from QuoteItem
			where
				QuoteItem.IsDeleted = 0 and
				QuoteItem.QuoteId = Quote.Id and
				QuoteItem.Premium is not null and
				QuoteItem.Premium > 0
		) FirstPremium
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	where
		ProposalHeader.Id = @ProposalHeaderId and
		QuoteHeader.IsRerated = 0 and
		Quote.Id in (select * from fn_StringListToTable(@QuoteIds))
go

--exec Reports_QuoteSchedule_Totals 890, '2556'





	
if exists (select * from sys.objects where object_id = object_id(N'Reports_QuoteSchedule_Assets_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_QuoteSchedule_Assets_ByCover
	end
go

create procedure Reports_QuoteSchedule_Assets_ByCover
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50),
	@CoverId int
)
as
if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) in ('AIG Assist', 'VMI Assist')
		begin
			;with Assets_CTE (AssetId, [Description], Premium, SumInsured)
			as
				(
					select distinct
						0,
						'VMI Assist',
						cast(QuoteItem.Premium as numeric(18, 2)),
						0.00
					from ProposalHeader
						left join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						left join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						left join QuoteItem on QuoteItem.QuoteId = Quote.Id
						left join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						left join Asset on Asset.Id = ProposalDefinition.AssetId
						left join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					where
						ProposalHeader.IsDeleted = 0 and
						ProposalDefinition.IsDeleted = 0 and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0 and
						Asset.IsDeleted = 0 and
						ProposalHeader.Id = @ProposalHeaderId and
						Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
						ProposalDefinition.CoverId = @CoverId and
						CoverDefinition.CoverId = @CoverId
				)
			select distinct * from Assets_CTE order by [Description] asc 
		end
	else if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) in ('Disaster Cash', 'Identity Theft', 'Personal Legal Liability')
		begin
			;with Assets_CTE (AssetId, [Description], Premium, SumInsured)
			as
				(
					select distinct
						Asset.Id,
						'Standard',
						cast(QuoteItem.Premium as numeric(18, 2)),
						cast(ProposalDefinition.SumInsured as numeric(18, 2))
					from ProposalHeader
						inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					where
						ProposalHeader.IsDeleted = 0 and
						ProposalDefinition.IsDeleted = 0 and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0 and
						Asset.IsDeleted = 0 and
						ProposalHeader.Id = @ProposalHeaderId and
						Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
						ProposalDefinition.CoverId = @CoverId and
						CoverDefinition.CoverId = @CoverId
				)
			select distinct * from Assets_CTE order by [Description] asc 
		end
	else if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) in ('All Risk')
		begin
			select distinct
				Asset.Id AssetId,
				(
					(
						select
							md.QuestionAnswer.Answer
						from md.questionanswer
						where
							md.QuestionAnswer.id in
							(
								select
									pqa.Answer
								from ProposalQuestionAnswer pqa
									inner join ProposalDefinition pd on pd.Id = pqa.ProposalDefinitionId
									inner join QuestionDefinition qd on qd.Id = pqa.QuestionDefinitionId
									inner join md.Question q on q.Id = qd.QuestionId
									inner join md.QuestionAnswer qa on qa.QuestionId = q.Id
									inner join Asset a on a.Id = pd.AssetId
								where
									pqa.ProposalDefinitionId = ProposalDefinition.Id and
									pqa.IsDeleted = 0 and
									pqa.QuestionTypeId = 3 and
									a.Id = Asset.Id and
									q.Id in (500)
							)
					)
					+ '- ' + 
					(
						select
							pqa.Answer
						from ProposalQuestionAnswer pqa
							inner join ProposalDefinition pd on pd.Id = pqa.ProposalDefinitionId
							inner join QuestionDefinition qd on qd.Id = pqa.QuestionDefinitionId
							inner join md.Question q on q.Id = qd.QuestionId
							inner join Asset a on a.Id = pd.AssetId
						where
							pqa.ProposalDefinitionId = ProposalDefinition.Id and
							pqa.IsDeleted = 0 and
							pqa.QuestionTypeId = 4 and
							a.Id = Asset.Id and
							q.Id in (490)
					)
				) [Description],
				cast(QuoteItem.Premium as numeric(18, 2)) Premium,
				cast(ProposalDefinition.SumInsured as numeric(18, 2)) SumInsured,
				ProposalDefinition.Id
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = @ProposalHeaderId and
				Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
				ProposalDefinition.CoverId = @CoverId and
				CoverDefinition.CoverId = @CoverId
			order by
				[Description] asc
		end
	else if (select md.Cover.Name from md.Cover where md.Cover.Id = @CoverId) in ('Motor')
		begin
			select distinct
				Asset.Id AssetId,
				Asset.[Description],
				cast(QuoteItem.Premium as numeric(18, 2)) Premium,
				--case when
				--(
				--	select
				--		count(md.QuestionAnswer.Answer)
				--	from ProposalHeader
				--		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				--		inner join Asset on Asset.Id = ProposalDefinition.AssetId
				--		inner join Product on Product.Id = ProposalDefinition.ProductId
				--		inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
				--		inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
				--		inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
				--		inner join md.Question on md.Question.Id = QuestionDefinition.QuestionId
				--		inner join md.QuestionType on md.QuestionType.Id = ProposalQuestionAnswer.QuestionTypeId
				--		inner join md.QuestionGroup on md.QuestionGroup.Id = Question.QuestionGroupId
				--		inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
				--	where
				--		ProposalHeader.Id = @ProposalHeaderId and
				--		Asset.Id = Asset.Id and
				--		ProposalHeader.IsDeleted = 0 and
				--		ProposalDefinition.IsDeleted = 0 and
				--		Asset.IsDeleted = 0 and
				--		md.QuestionGroup.Name = 'General Information' and
				--		md.QuestionType.Name in ('DropDown') and
				--		md.Question.Name = 'AIG - Motor -Type Of Vehicle' and
				--		md.QuestionAnswer.Answer in
				--		(
				--			'Light Commercial Vehicles',
				--			'Private Sedan',
				--			'Private Hatch',
				--			'Quad Bikes',
				--			'Motorcycle (on road)',
				--			'Motorcycle (off road)',
				--			'Trailer',
				--			'Caravan'
				--		)
				--) > 0
				--	then 0
				--else
				--	cast(ProposalDefinition.SumInsured as numeric(18, 2))
				--end SumInsured
				cast(0 as numeric(18, 2)) SumInsured
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = @ProposalHeaderId and
				Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
				ProposalDefinition.CoverId = @CoverId and
				CoverDefinition.CoverId = @CoverId
			order by
				Asset.[Description] asc
		end
	else
		begin
			select distinct
				Asset.Id AssetId,
				Asset.[Description],
				cast(QuoteItem.Premium as numeric(18, 2)) Premium,
				cast(ProposalDefinition.SumInsured as numeric(18, 2)) SumInsured
			from ProposalHeader
				inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
				inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			where
				ProposalHeader.IsDeleted = 0 and
				ProposalDefinition.IsDeleted = 0 and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0 and
				Asset.IsDeleted = 0 and
				ProposalHeader.Id = @ProposalHeaderId and
				Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
				ProposalDefinition.CoverId = @CoverId and
				CoverDefinition.CoverId = @CoverId
			order by
				Asset.[Description] asc
		end
go

--exec Reports_QuoteSchedule_Assets_ByCover 2286, 4778, 44


---report layout data went missing

declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode <> 'AIG')
	begin
		return
	end
else
	begin
		if not exists(select * from Report where Name = 'Agent Quote Performance Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Agent Quote Performance Report', 'Agent Quote Performance Report', 'AgentQuotePerformance.trdx')
			end

		if not exists(select * from Report where Name = 'Agent Quote Performance Report - PSG')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Agent Quote Performance Report - PSG', 'Agent Quote Performance Report - PSG', 'AgentQuotePerformance_PSG.trdx')
			end

		if not exists(select * from Report where Name = 'Campaign Performance Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Campaign Performance Report', 'Campaign Performance Report', 'CampaignQuotePerformance.trdx')
			end

		if not exists(select * from Report where Name = 'Lead Status Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Lead Status Report', 'Lead Status Report', 'LeadStatus.trdx')
			end

		if not exists(select * from Report where Name = 'Comparative Quote Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Comparative Quote Report', 'Comparative Quote Report', 'ComparativeQuote.trdx')
			end

		if not exists(select * from Report where Name = 'Funeral Quote Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Funeral Quote Report', 'Funeral Quote Report', 'FuneralQuote.trdx')
			end

		if not exists(select * from Report where Name = 'Quote Schedule Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Quote Schedule Report', 'Quote Schedule Report', 'QuoteSchedule.trdx')
			end

		if not exists(select * from Report where Name = 'Detailed Comparative Quote Report')
			begin
				insert into Report (Name, [Description], SourceFile)
				values ('Detailed Comparative Quote Report', 'Detailed Comparative Quote Report', 'DetailedComparativeQuote.trdx')
			end

		--CLEAR REPORT SETTINGS
		delete from ReportChannelLayout
		delete from ReportChannel
		delete from ReportSection
		delete from ReportLayout

		--RESEED IDENTITY COLUMNS
		dbcc checkident ('ReportChannelLayout', reseed, 0)
		dbcc checkident ('ReportChannel', reseed, 0)
		dbcc checkident ('ReportSection', reseed, 0)
		dbcc checkident ('ReportLayout', reseed, 0)

		--DECLARE SCOPE IDENTITY VARIABLE
		declare @scope_identity int

		--SETUP COMPARATIVE QUOTE REPORT
		insert into ReportChannel (ChannelId, ReportId, Code, CreatedAt, ModifiedAt, UserId) values (@DefaultChannelId, (select top 1 Id from Report where Name = 'Comparative Quote Report'), 'vrgnmny', getdate(), getdate(), 1)
		set @ScopeComparativeQuoteReportChannelId = SCOPE_IDENTITY()

		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'QuoteHeader', 'Quote Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'QuoteFooter', 'Quote Footer')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'ClientHeader', 'Client Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'CompanyHeader', 'Company Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'QuoteSummary', 'Quote Summary')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), 'CostFooter', 'Cost Footer')

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteTitle', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteSummary', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteAssets', 'ASSETS', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'ASSETS', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteDate', 'Date:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Date:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteExpires', 'Expires:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Expires:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteNumber', 'Quote No:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Quote No:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientHeader', 'DETAILS OF CLIENT', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'DETAILS OF CLIENT', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralName', 'Client', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Client', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralIdentity', 'Identity No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Identity No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Physical Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Postal Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Work No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactHome', 'Home No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Home No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactCell', 'Cell No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Cell No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Fax No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Email Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyHeader', 'DETAILS OF INTERMEDIARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'DETAILS OF INTERMEDIARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyGeneralName', 'Company', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Company', 'Lifecycle Management (Pty) Ltd', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Physical Address', '2 Park Lane, Umhlanga Ridge, 4319', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Postal Address', 'PO Box 3025, Prestondale, Kwa-Zulu Natal, 4021', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Work No.', '0861 50 60 70', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Fax No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Email Address', 'needsomehelp@virginmoneyinsurance.co.za', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWebsite', 'Website', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Website', 'http://www.virginmoneyinsurance.co.za', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthRegistration', 'Registration No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Registration No.', '2014/075034/07', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthTax', 'Tax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'VAT No.', '4070270881', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthLicense', 'Authorised Financial Services Provider', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, 'Authorised Financial Services Provider', 'No 7146', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteFooter'), 'QuoteFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, '', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryFees', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryTotal', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Comparative Quote Report'), (select top 1 Id from ReportSection where Name = 'CostFooter'), 'CostFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeComparativeQuoteReportChannelId, @scope_identity, '', '', 1)


		--SETUP FUNERAL QUOTE REPORT
		insert into ReportChannel (ChannelId, ReportId, Code, CreatedAt, ModifiedAt, UserId) values (@DefaultChannelId, (select top 1 Id from Report where Name = 'Funeral Quote Report'), 'vrgnmny', getdate(), getdate(), 1)
		set @ScopeFuneralQuoteReportChannelId = scope_identity()

		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'QuoteHeader', 'Quote Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'QuoteFooter', 'Quote Footer')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'ClientHeader', 'Client Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'CompanyHeader', 'Company Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'QuoteSummary', 'Quote Summary')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), 'CostFooter', 'Cost Footer')

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteTitle', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteSummary', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteAssets', 'ASSETS', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'ASSETS', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteDate', 'Date:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Date:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteExpires', 'Expires:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Expires:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteNumber', 'Quote No:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Quote No:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientHeader', 'DETAILS OF CLIENT', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'DETAILS OF CLIENT', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralName', 'Client', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Client', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralIdentity', 'Identity No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Identity No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Physical Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Postal Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Work No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactHome', 'Home No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Home No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactCell', 'Cell No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Cell No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Fax No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Email Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyHeader', 'DETAILS OF INTERMEDIARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'DETAILS OF INTERMEDIARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyGeneralName', 'Company', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Company', 'Lifecycle Management (Pty) Ltd', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Physical Address', '2 Park Lane, Umhlanga Ridge, 4319', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Postal Address', 'PO Box 3025, Prestondale, Kwa-Zulu Natal, 4021', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Work No.', '0861 50 60 70', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Fax No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Email Address', 'needsomehelp@virginmoneyinsurance.co.za', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWebsite', 'Website', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Website', 'http://www.virginmoneyinsurance.co.za', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthRegistration', 'Registration No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Registration No.', '2014/075034/07', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthTax', 'Tax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Tax No.', '4070270881', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthLicense', 'Authorised Financial Services Provider', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, 'Authorised Financial Services Provider', 'No 7146', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteFooter'), 'QuoteFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, '', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryFees', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryTotal', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Funeral Quote Report'), (select top 1 Id from ReportSection where Name = 'CostFooter'), 'CostFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeFuneralQuoteReportChannelId, @scope_identity, '', '', 1)


		--SETUP QUOTE SCHEDULE REPORT
		insert into ReportChannel (ChannelId, ReportId, Code, CreatedAt, ModifiedAt, UserId) values (@DefaultChannelId, (select top 1 Id from Report where Name = 'Quote Schedule Report'), 'vrgnmny', getdate(), getdate(), 1)
		set @ScopeQuoteScheduleReportChannelId = scope_identity()

		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), 'QuoteHeader', 'Quote Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), 'QuoteFooter', 'Quote Footer')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), 'ClientHeader', 'Client Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), 'CompanyHeader', 'Company Header')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), 'QuoteSummary', 'Quote Summary')
		insert into ReportSection (ReportId, Name, Label) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), 'CostFooter', 'Cost Footer')

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteTitle', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteSummary', 'QUOTE SUMMARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'QUOTE SUMMARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteAssets', 'ASSETS', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'ASSETS', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteDate', 'Date:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Date:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteExpires', 'Expires:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Expires:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteHeader'), 'QuoteNumber', 'Quote No:', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Quote No:', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientHeader', 'DETAILS OF CLIENT', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'DETAILS OF CLIENT', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralName', 'Client', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Client', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientGeneralIdentity', 'Identity No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Identity No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Physical Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Postal Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Work No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactHome', 'Home No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Home No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactCell', 'Cell No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Cell No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Fax No.', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'ClientHeader'), 'ClientContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Email Address', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyHeader', 'DETAILS OF INTERMEDIARY', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'DETAILS OF INTERMEDIARY', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyGeneralName', 'Company', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Company', 'Lifecycle Management (Pty) Ltd', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPhysical', 'Physical Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Physical Address', '2 Park Lane, Umhlanga Ridge, 4319', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAddressPostal', 'Postal Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Postal Address', 'PO Box 3025, Prestondale, Kwa-Zulu Natal, 4021', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWork', 'Work No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Work No.', '0861 50 60 70', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactFax', 'Fax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Fax No.', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactEmail', 'Email Address', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Email Address', 'needsomehelp@virginmoneyinsurance.co.za', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyContactWebsite', 'Website', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Website', 'http://www.virginmoneyinsurance.co.za', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthRegistration', 'Registration No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Registration No.', '2014/075034/07', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthTax', 'Tax No.', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Tax No.', '4070270881', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CompanyHeader'), 'CompanyAuthLicense', 'Authorised Financial Services Provider', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, 'Authorised Financial Services Provider', 'No 7146', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteFooter'), 'QuoteFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, '', '', 1)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryFees', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'QuoteSummary'), 'QuoteSummaryTotal', '', '', 0)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, '', '', 0)

		insert into ReportLayout (ReportId, ReportSectionId, Name, Label, Value, IsVisible) values ((select top 1 Id from Report where Name = 'Quote Schedule Report'), (select top 1 Id from ReportSection where Name = 'CostFooter'), 'CostFooter', '', '', 1)
		set @scope_identity = scope_identity()
		insert into ReportChannelLayout (ReportChannelId, ReportLayoutId, Label, Value, IsVisible) values (@ScopeQuoteScheduleReportChannelId, @scope_identity, '', '', 1)
	end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_Layout') and type in (N'P', N'PC'))
	begin
		drop procedure Report_Layout
	end
go

create procedure Report_Layout
(
	@ReportName varchar(50)
)
as
	declare @ChannelId int

	set @ChannelId =
	(
		select top 1
			ReportChannel.ChannelId
		from Report
			inner join ReportChannel on ReportChannel.ReportId = Report.Id
			inner join Channel on Channel.Id = ReportChannel.ChannelId
		where
			Channel.IsDefault = 1 and
			Report.Name = @ReportName
	)

	if isnull(@ChannelId, 0) > 0
		begin
			exec Report_Layout_ByChannelId @ChannelId, @ReportName
		end
	else
		begin
			select
				ReportSection.Name SectionName,
				ReportSection.Label SectionLabel,
				ReportSection.IsVisible SectionVisible,
				ReportLayout.Name FieldName,
				ReportLayout.Label FieldLabel,
				ReportLayout.[Value] FieldValue,
				ReportLayout.IsVisible FieldVisible
			from Report
				inner join ReportLayout on ReportLayout.ReportId = Report.Id
				inner join ReportSection on ReportSection.Id = ReportLayout.ReportSectionId
			where
				Report.Name = @ReportName
		end
go