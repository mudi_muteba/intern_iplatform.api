﻿DECLARE @PartyId int, @ContactDetailId INT;

--IEMAS
SELECT top 1 @PartyId = PartyId FROM Organization WHERE Code = 'IEMAS';

if @PartyId > 0
BEGIN
	--IEMAS
	INSERT INTO ContactDetail (Work, Direct, Cell, Home, Fax, Email, Website, IsDeleted) Values ('0126747074',  '0126747074', null, null, '0126747043', 'info@iemas.co.za', 'https://www.iemasfinancialservices.co.za/', 0)

	SELECT @ContactDetailId = SCOPE_IDENTITY();
	Update Party Set ContactDetailId = @ContactDetailId WHERE id  = @PartyId

	Update Organization 
	SET RegisteredName = 'IEMAS Financial Services (Co-Operative) Limited', TradingName = 'IEMAS Financial Services',
	TradingSince = '1940-01-01', [Description] = 'Iemas is a co-operative that offers financial services.We provide affordable and competitive financial solutions to our members across South Africa. As a caring partner, we advise and support our members to ensure that they receive products suited to their individual needs.A co-operative is member-owned. We are not about making big profits for shareholders, but creating value for our members. This is what gives us our unique character and influences our core values.',
	FspNo = '15815'
	WHERE PartyId = @PartyId


	INSERT INTO [Address](PartyId, [MasterId], [Description],  Line1, Line2, Line3, Line4, Code, Longitude, Latitude, StateProvinceId, AddressTypeId, IsDefault, DateFrom, IsDeleted )
	VALUES (@PartyId,0, '1249 Embankment Road, Centurion, ZWARTKOP EXT 7, 0051', '1249 Embankment Road', 'Iemas Park', 'Centurion','ZWARTKOP EXT 7' ,'0051','-25,852897','28,183327','4' ,'1', 1, '1940-01-01', 0)

	DECLARE @ProductId INT;
	SET @ProductId = 0;
	SELECT  top 1 @ProductId = Id FROM Product WHERE ProductCode = 'IEMAS' AND Id = @ProductId;

	IF NOT EXISTS(SELECT  * FROM Product WHERE ProductCode = 'IEMAS' AND Id = @ProductId)
	BEGIN
		INSERT INTO Product (MasterId, Name,ProductOwnerId, ProductProviderId, ProductTypeId, AgencyNumber, ProductCode, ImageName, QuoteExpiration)
		VALUES (0, 'IEMAS', @PartyId, @PartyId, 7, '123456', 'IEMAS', 'iemas.png', 30);
	END
	ELSE
	BEGIN
		Update Product SET Name = 'IEMAS Product', ImageName = 'iemas.png'  WHERE Id = @ProductId;
	END
END
ELSE
BEGIN 
	INSERT INTO ContactDetail (Work, Direct, Cell, Home, Fax, Email, Website, IsDeleted) Values ('0126747074',  '0126747074', null, null, '0126747043', 'info@iemas.co.za', 'https://www.iemasfinancialservices.co.za/', 0)

	SELECT @ContactDetailId = SCOPE_IDENTITY();

	INSERT INTO Party (MasterId, PartyTypeId, DisplayName, ChannelId, ContactDetailId, IsDeleted) VALUES (0, 2, 'Iemas Financial Services',1, @ContactDetailId, 0);

	SELECT @PartyId = SCOPE_IDENTITY();

	INSERT INTO Organization (PartyId, Code ,RegisteredName, TradingName, TradingSince, [Description], FspNo)
	VALUES (@PartyId, 'IEMAS','IEMAS Financial Services (Co-Operative) Limited', 'IEMAS Financial Services', '1940-01-01', 'Iemas is a co-operative that offers financial services.We provide affordable and competitive financial solutions to our members across South Africa. As a caring partner, we advise and support our members to ensure that they receive products suited to their individual needs.A co-operative is member-owned. We are not about making big profits for shareholders, but creating value for our members. This is what gives us our unique character and influences our core values.', '15815');

	INSERT INTO [Address](PartyId, [MasterId], [Description],  Line1, Line2, Line3, Line4, Code, Longitude, Latitude, StateProvinceId, AddressTypeId, IsDefault, DateFrom, IsDeleted )
	VALUES (@PartyId,0, '1249 Embankment Road, Centurion, ZWARTKOP EXT 7, 0051', '1249 Embankment Road', 'Iemas Park', 'Centurion','ZWARTKOP EXT 7' ,'0051','-25,852897','28,183327','4' ,'1', 1, '1940-01-01', 0)

	INSERT INTO Product (MasterId, Name,ProductOwnerId, ProductProviderId, ProductTypeId, AgencyNumber, ProductCode, ImageName, QuoteExpiration)
	VALUES (0, 'IEMAS', @PartyId, @PartyId, 7, '123456', 'IEMAS', 'iemas.png', 30);
END

SET @ContactDetailId = 0;
SET @PartyId = 0;

--GARRUN
INSERT INTO ContactDetail (Work, Direct, Cell, Home, Fax, Email, Website, IsDeleted) Values ('0116945000',  '0116945000', null, null, null, 'info@garrun-group.co.za', 'http://www.garrun-group.co.za/', 0)

SELECT @ContactDetailId = SCOPE_IDENTITY();

INSERT INTO Party (MasterId, PartyTypeId, DisplayName, ChannelId, ContactDetailId, IsDeleted) VALUES (0, 2, 'Garrun Group',1, @ContactDetailId, 0);

SELECT @PartyId = SCOPE_IDENTITY();

INSERT INTO Organization (PartyId, Code ,RegisteredName, TradingName, TradingSince, [Description], FspNo)
VALUES (@PartyId, 'GARRUN','Garrun Group Investment Holdings', 'Garrun Group Investment Holdings', '1960-01-01', 'The Garrun Group is an established financial services provider in the South African short-term insurance industry with a committed and experienced leadership team.We provide well-designed insurance products that are formulated to protect the assets of our broad range of clients. Our comprehensive product offering includes domestic, commercial, agricultural as well as specialised insurance products such as marine, aviation, wildlife and many more.', '00000');

INSERT INTO [Address](PartyId, [MasterId], [Description],  Line1, Line2, Line3, Line4, Code, Longitude, Latitude, StateProvinceId, AddressTypeId, IsDefault, DateFrom, IsDeleted )
VALUES (@PartyId,0, '33 Central Street, Johannesburg, Houghton Estate, 2198', '33 Central Street', '', 'Johannesburg','Houghton Estate' ,'2198','-26.16091','28.058019999999942','4' ,'1', 1, '1960-01-01', 0)

INSERT INTO Product (MasterId, Name,ProductOwnerId, ProductProviderId, ProductTypeId, AgencyNumber, ProductCode, ImageName, QuoteExpiration)
VALUES (0, 'GARRUN', @PartyId, @PartyId, 7, '123456', 'GARRUN', 'garrun.png', 30);