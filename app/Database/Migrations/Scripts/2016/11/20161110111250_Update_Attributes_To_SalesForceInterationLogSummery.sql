
if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_SalesForceIntegrationLogSummeryExtract') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_AIG_SalesForceIntegrationLogSummeryExtract
	end
go

create procedure Reports_AIG_SalesForceIntegrationLogSummeryExtract
(  
--@PartyId int
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
AS
BEGIN

if exists (select  * from tempdb.dbo.sysobjects o where o.xtype in ('U') and o.id = object_id(N'tempdb..#temp'))
drop table #temp;
if exists (select  * from tempdb.dbo.sysobjects o where o.xtype in ('U') and o.id = object_id(N'tempdb..#errors'))
drop table #errors;
WITH CTE AS(
   SELECT 
       RN = ROW_NUMBER()OVER(PARTITION BY PartyId,IsSuccess ORDER BY PartyId,IsSuccess),
	   *
   FROM dbo.SalesForceIntegrationLog
   where CreatedOn BETWEEN  @StartDate and @EndDate
)
select PartyId,IsSuccess,MAX(RN) as [count] into #temp FROM CTE WHERE RN > 0 group by PartyId,IsSuccess
order by PartyId,IsSuccess

select b.PartyId,a.count as [Succsess],b.count [Failed] into #errors from #temp a right join #temp b 
on a.PartyId = b.PartyId 
and a.IsSuccess <> b.IsSuccess
where 
b.IsSuccess = 0
and (a.IsSuccess is null or a.IsSuccess = 1)

--select * from #errors
update #errors set Succsess = 0 where Succsess is null

select 'Total' ,count(*) as [Total] from SalesForceIntegrationLog where CreatedOn BETWEEN  @StartDate and @EndDate
union
select 'Successful',count(*) as [Success] from SalesForceIntegrationLog where IsSuccess = 1 and CreatedOn BETWEEN  @StartDate and @EndDate
union
select 'Failed',count(*) as [Fail] from SalesForceIntegrationLog where IsSuccess = 0 and CreatedOn BETWEEN  @StartDate and @EndDate
union 
select 'Retries', count(Failed) from #errors where Failed <= Succsess
union 
select 'Failures not resent', count(Failed) from #errors where Failed > Succsess
union
select 'ERROR: Retry Response',count(*) as [Fail] from SalesForceIntegrationLog where IsSuccess = 0 and Response like 'AUTH ERROR retry your request%' and CreatedOn BETWEEN  @StartDate and @EndDate
union
select 'ERROR: Authentication Failures Response',count(*) as [Fail] from SalesForceIntegrationLog where IsSuccess = 0 and Response like 'AUTH ERROR authentication failure%' and CreatedOn BETWEEN  @StartDate and @EndDate
union
select 'ERROR: Other Failures',count(*) as [Fail] from SalesForceIntegrationLog where IsSuccess = 0 
and Response not like 'AUTH ERROR retry your reques%'
and Response not like 'AUTH ERROR authentication failure%'
and CreatedOn BETWEEN  @StartDate and @EndDate
UNION
select 'Items processed late by the work flow', count(*) from SalesForceEntry
	 SFE with (nolock)
left join SalesForceIntegrationLog 
	SFIL with (nolock) 
on SFIL.PartyId = SFE.PartyId and SFE.Status = SFIL.Status
where SFE.Suspended != 1 and SFIL.Status is not null and DATEDIFF(minute,SFE.ModifiedOn,SFIL.CreatedOn) > 60 and SFIL.CreatedOn BETWEEN  @StartDate and @EndDate

union
select 'Items not processed by the work flow',count(*) from SalesForceEntry SFE with (nolock)
left join SalesForceIntegrationLog SFIL with (nolock) 
on SFIL.PartyId = SFE.PartyId and SFE.Status = SFIL.Status
where SFE.Suspended != 1
and SFIL.Status is null and SFE.CreatedOn BETWEEN  @StartDate and @EndDate
END

--exec Reports_AIG_SalesForceIntegrationLogSummeryExtract '2016-10-19 7:39:03.000','2016-10-20 7:39:03.000'
