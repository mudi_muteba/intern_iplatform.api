if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_LeadExtract') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_AIG_LeadExtract
	end
go

create procedure Reports_AIG_LeadExtract
(
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as


--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
--set @StartDate = '2015-08-01'
--set @EndDate = '2016-11-30'

Select
	
	CASE 
		WHEN Prh.Source IS NULL THEN ('Call center')   
		ELSE Prh.Source                                     
    END as Channel, 

	CASE
		WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y'
		ELSE 'N'
	END as 'QuoteExpired',
	
	(CASE 
		(SELECT COALESCE((select top 1 PolicyStatusId from PolicyHeader where PartyId = p.id and QuoteId = qute.Id and PolicyStatusId = 3), 0))
			when 3 then 'Y'
			when 2 then 'N'
			when 1 then 'N'
			when 0 then 'N'
	End  )as 'QuoteBound',
	
	case when Qute.AcceptedOn IS NULL Then '' else CONVERT(VARCHAR(10),Qute.AcceptedOn ,111) end as 'QuoteBindDate',

	case when (select sum(cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2))) + cast(round (Qute.Fees ,2) as numeric(36,2))  from QuoteItem 	
				where QuoteId = Qute.id) is null
	THEN (0) 
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2))) + cast(round (Qute.Fees ,2) as numeric(36,2))  from QuoteItem 	
				where QuoteId = Qute.id)                                          
	END 
	as 'QuoteTotalPremium',

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 218)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 218))                                          
	END AS 'QuoteMotorPremium',
	 
	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 78)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 78))                                          
	END AS 'QuoteHomePremium',

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 142)) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId = 142))                                          
	END AS 'QuoteFuneralPremium',	

	CASE WHEN (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId not in(218,78,142))) IS NULL 
	THEN (0)   
	ELSE (select sum(cast(round (Premium ,2) as numeric(36,2)))  from QuoteItem 		
			where QuoteId = Qute.id and CoverDefinitionId in (select id from CoverDefinition where CoverId not in(218,78,142)))                                          
	END as 'QuoteOtherPremium',
	 
	(cast(round(Qute.fees,2) as numeric(36,2))) as 'QuoteFees',

	(select sum(cast(round (Sasria ,2) as numeric(36,2)))  from QuoteItem 		
				where QuoteId = Qute.id) as 'QuoteSasria',	

	(CONVERT(VARCHAR(10),Qh.CreatedAt ,111) ) as 'QuoteIncepptionDate',

	(select top 1 PolicyNo from PolicyHeader where PartyId = p.Id and QuoteId = qute.Id and PolicyStatusId = 3) as 'QuotePolicyNumber',

	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'QuoteCreateDate',
	CONVERT(VARCHAR(5),Qute.CreatedAt,108) as 'QuoteCreateTime', 

	qute.Id as 'QuoteID',
	Ind.ExternalReference as 'VMSAID',

	Ind.IdentityNo as 'IDNumber', 
	(select name from Md.Title where Id = Ind.TitleId) as 'Title',
	Ind.FirstName as 'FirstName', 
	'' as 'Initials', 
	Ind.Surname as 'Surname',
	CONVERT(VARCHAR(10),Ind.DateOfBirth ,111) as 'DateOfBirth',
	DATEDIFF(hour,Ind.DateOfBirth,GETDATE())/8766 AS Age, 
	(select name from Md.Gender where Id = Ind.GenderId) as 'Gender', 

	CASE WHEN 
		(select name from Md.MaritalStatus where Id = Ind.MaritalStatusId) is null
	THEN ''
	ELSE 
		(select name from Md.MaritalStatus where Id = Ind.MaritalStatusId)                                          
	END as 'MaritalStatus',

	CASE WHEN 
		(Qute.ITCScore) is null
	THEN ''
	ELSE 
		(Qute.ITCScore)                                          
	END as 'ITCCreditScoreBand',

	CASE WHEN 
		(occ.Name) is null
	THEN ''
	ELSE 
		(occ.Name)                                          
	END as 'Occupation',

	CASE WHEN 
		(select name from Md.Language where Id = Ind.LanguageId) is null
	THEN ''
	ELSE 
		(select name from Md.Language where Id = Ind.LanguageId)                                          
	END as 'HomeLanguage',
	
	'English' as 'PreferredLanguage',

	cd.Email as 'EmailAddress', 

	cd.Cell as 'CellPhoneNumber', 
	cd.Home as 'HomePhoneNumber', 
	cd.Work as 'WorkPhoneNumber',
	
	(SELECT
		replace(concat(CASE WHEN Email = 1 THEN 'Email ' END ,
					   CASE WHEN Telephone = 1 THEN ' Telephone ' END,
					   CASE WHEN SMS = 1 THEN ' SMS ' END,
					   CASE WHEN Post = 1 THEN ' Post' END), '  ', '/')
	 FROM PartyCorrespondencePreference where PartyId = p.Id and CorrespondenceTypeId = 1 )as 'CommPreferencePolicy',


	 (SELECT
		replace(concat(CASE WHEN Email = 1 THEN 'Email ' END ,
					   CASE WHEN Telephone = 1 THEN ' Telephone ' END,
					   CASE WHEN SMS = 1 THEN ' SMS ' END,
					   CASE WHEN Post = 1 THEN ' Post' END), '  ', '/')
	 FROM PartyCorrespondencePreference where PartyId = p.Id and CorrespondenceTypeId = 3 )as 'CommPreferenceMarketing',

	CASE 
		WHEN ind.AnyJudgements is null THEN ('N') 
		WHEN ind.AnyJudgements = 0 THEN ('N')   
		WHEN ind.AnyJudgements = 1 THEN ('Y')                                       
    END AS 'DeclarationJudgements',

	CASE 
		WHEN ind.UnderAdministrationOrDebtReview is null THEN ('N') 
		WHEN ind.UnderAdministrationOrDebtReview = 0 THEN ('N')   
		WHEN ind.UnderAdministrationOrDebtReview = 1 THEN ('Y')                                       
    END AS 'DeclarationDebtReview',

	CASE 
		WHEN ind.BeenSequestratedOrLiquidated is null THEN ('N') 
		WHEN ind.BeenSequestratedOrLiquidated = 0 THEN ('N')   
		WHEN ind.BeenSequestratedOrLiquidated = 1 THEN ('Y')                                       
    END AS 'DeclarationSequestration',


	(select top 1 BankAccHolder from BankDetails where PartyId = p.Id order by Id desc) as 'AccountHolderName', 
	(select top 1 AccountNo from BankDetails where PartyId = p.Id order by Id desc) as 'AccountNumber', 
	(select top 1 TypeAccount from BankDetails where PartyId = p.Id order by Id desc) as 'AccountType', 
	(select top 1 Bank from BankDetails where PartyId = p.Id order by Id desc) as 'BankName', 
	(select top 1 BankBranch from BankDetails where PartyId = p.Id order by Id desc) as 'BranchName', 
	(select top 1 BankBranchCode from BankDetails where PartyId = p.Id order by Id desc) as 'BranchCode',

	(select top 1 ci.Answer from LossHistory as lh
	join md.CurrentlyInsured as ci on ci.Id = lh.CurrentlyInsuredId
	where lh.PartyId = p.Id and lh.IsDeleted = 0 and lh.ContactId = 0 order by lh.Id asc
	) AS 'CurrentlyInsured',	

	(select top 1 up.Answer from LossHistory as lh
	join md.UninterruptedPolicy as up on up.Id = lh.UninterruptedPolicyId
	where lh.PartyId = p.Id and lh.IsDeleted = 0 and lh.ContactId = 0 order by lh.Id asc
	) AS 'UninterruptedCover',

	(select case 
		WHEN (select top 1 InsurerCancel from LossHistory where PartyId = p.id and IsDeleted = 0 and LossHistory.ContactId = 0 order by LossHistory.Id asc) = 1  THEN 'Y'
		WHEN (select top 1 InsurerCancel from LossHistory where PartyId = p.id and IsDeleted = 0 and LossHistory.ContactId = 0 order by LossHistory.Id asc) = 0 Then ('N')		
	end )as 'CancelledInsurance',

	p.Id as 'PartyId'

from ProposalHeader as Prh
join QuoteHeader as Qh on qh.ProposalHeaderId = Prh.Id
join Quote as Qute on Qute.QuoteHeaderId = Qh.Id
JOIN Party as P on p.Id = Prh.PartyId
join Lead as L on l.PartyId = p.Id
join LeadActivity as la on la.LeadId = l.Id
left join ContactDetail as cd on cd.Id = P.ContactDetailId
left join Individual as Ind on P.Id = Ind.PartyId
left join Occupation as Occ on Occ.Id = Ind.OccupationId
where la.DateCreated BETWEEN @StartDate AND @EndDate
and la.ActivityTypeId = 1 
and L.IsDeleted = 0
and Qute.IsDeleted = 0
and Prh.IsDeleted = 0
and Qh.IsDeleted = 0