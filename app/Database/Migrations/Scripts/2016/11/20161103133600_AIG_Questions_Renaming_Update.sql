﻿--Is your property in a gated community
if exists(select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=448)
begin
update QuestionDefinition
set DisplayName = 'Is your property in a gated community?' 
where CoverDefinitionId = 146 and QuestionId=448
end


if exists(select * from QuestionDefinition where CoverDefinitionId = 147 and QuestionId=396)
begin
update QuestionDefinition
set DisplayName = 'Is your property in a gated community?' 
where CoverDefinitionId = 147 and QuestionId=396
end

--Is the property in a security complex
if exists(select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=450)
begin
update QuestionDefinition
set DisplayName = 'Is the property in a security complex?' 
where CoverDefinitionId = 146 and QuestionId=450
end

if exists(select * from QuestionDefinition where CoverDefinitionId = 147 and QuestionId=398)
begin
update QuestionDefinition
set DisplayName = 'Is the property in a security complex?' 
where CoverDefinitionId = 147 and QuestionId=398
end

--Do you want to include subsidence and landslip cover
if exists(select * from QuestionDefinition where CoverDefinitionId = 146 and QuestionId=472)
begin
update QuestionDefinition
set DisplayName = 'Do you want to include subsidence and landslip cover?' 
where CoverDefinitionId = 146 and QuestionId=472
end

--What is your Vin Number? (New cars only)?
if exists(select * from QuestionDefinition where CoverDefinitionId = 145 and QuestionId=310)
begin
update QuestionDefinition
set DisplayName = 'What is your Vin Number? (New cars only)' 
where CoverDefinitionId = 145 and QuestionId=310
end

