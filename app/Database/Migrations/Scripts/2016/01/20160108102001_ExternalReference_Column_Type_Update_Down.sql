﻿drop index [Party_External_Reference] on [dbo].[Party]
go

alter table Party
alter column ExternalReference nvarchar(40) --nvarchar can't be converted to uniqueidentifier after the fact
go

create nonclustered index [Party_External_Reference] on [dbo].[Party]
(
	[ExternalReference] ASC
) with (pad_index = off, statistics_norecompute = off, sort_in_tempdb = off, drop_existing = off, online = off, allow_row_locks = on, allow_page_locks = on) on [primary]
go