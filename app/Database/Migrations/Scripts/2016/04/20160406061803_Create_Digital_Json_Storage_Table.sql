﻿IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[JsonDataStore]') AND type in (N'U'))
BEGIN
create table [dbo].[JsonDataStore](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[SessionId] [nvarchar](80) NOT NULL,
	[ChannelID] [int] NOT NULL,
	[PartyId] [nvarchar](20) NOT NULL,
	[Section] [nvarchar](50) NULL,
	[SavePoint] [int] NOT NULL,
	[Store] [nvarchar](max) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[IsDeleted] [bit] NULL
)
END