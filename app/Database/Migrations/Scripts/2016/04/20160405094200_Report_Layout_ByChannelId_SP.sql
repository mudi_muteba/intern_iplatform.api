﻿if exists (select * from sys.objects where object_id = object_id(N'Report_Layout_ByChannelId') and type in (N'P', N'PC'))
	begin
		drop procedure Report_Layout_ByChannelId
	end
go

create procedure Report_Layout_ByChannelId
(
	@ChannelId int,
	@ReportName varchar(50)
)
as
	select
		ReportSection.Name SectionName,
		ReportSection.Label SectionLabel,
		ReportSection.IsVisible SectionVisible,
		ReportLayout.Name FieldName,
		ReportChannelLayout.Label FieldLabel,
		ReportChannelLayout.Value FieldValue,
		ReportChannelLayout.IsVisible FieldVisible
	from Report
		inner join ReportChannel on ReportChannel.ReportId = Report.Id
		inner join Channel on Channel.Id = ReportChannel.ChannelId
		inner join ReportLayout on ReportLayout.ReportId = Report.Id
		inner join ReportSection on ReportSection.Id = ReportLayout.ReportSectionId
		inner join ReportChannelLayout on ReportChannelLayout.ReportLayoutId = ReportLayout.Id
	where
		Channel.Id = @ChannelId and
		Report.Name = @ReportName
go
