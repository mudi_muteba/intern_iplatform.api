﻿IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[LossHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LossHistory](
	[Id] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[PartyId] [int] NOT NULL,
	[CurrentlyInsuredId] [int] NULL,
	[UninterruptedPolicyId] [int] NULL,
	[InsurerCancel] [bit] NULL,
	[PreviousComprehensiveInsurance] [bit] NULL,
	[MotorUninterruptedPolicyNoClaimId] [int] NULL,
	[InterruptedPolicyClaim] [bit] NOT NULL DEFAULT ((0)),
	[IsDeleted] [bit] NULL,
) 
END