﻿
    create table Lead (
        Id INT IDENTITY NOT NULL,
       PartyId INT NOT NULL,
       primary key (Id)
    )

    create table LeadActivity (
        Id INT IDENTITY NOT NULL,
       LeadId INT NOT NULL,
	   [Priority] INT NOT NULL default(5),
       Selected BIT NOT NULL default(0),
	   Proposal  NVARCHAR(MAX) NOT NULL default(''),
       CampaignId INT null,
       LeadStatusId INT NOT NULL,
       DateCreated DATETIME NOT NULL default(getdate()),
       DateUpdated DATETIME NOT NULL default(getdate()),
       primary key (Id)
    )

    create table Campaign (
        Id INT IDENTITY NOT NULL,
       Name NVARCHAR(255) NOT NULL,
       Reference NVARCHAR(255) NOT NULL,
       CampaignSourceId INT NOT NULL,
       StartDate DATETIME  NOT NULL default(getdate()),
       EndDate DATETIME null,
       DateCreated DATETIME NOT NULL default(getdate()),
       DateUpdated DATETIME NOT NULL default(getdate()),
       primary key (Id)
    )

	-- Master Data --
    create table md.LeadStatus (
        Id INT IDENTITY NOT NULL,
		Name NVARCHAR(255) NOT NULL UNIQUE,
		VisibleIndex INT NOT NULL
		primary key (Id)
    )

	insert into md.LeadStatus
	values ('New', 0),
		   ('Open', 1),
		   ('Hold', 2),
		   ('Sold', 3),
		   ('Lost', 4),
		   ('Dead', 5)

    create table md.CampaignSource (
        Id INT IDENTITY NOT NULL,
		Name NVARCHAR(255) NOT NULL UNIQUE,
		primary key (Id)
    )

 	insert into md.CampaignSource
	values ('Email'),
		   ('Phone'),
		   ('SMS'),
		   ('Radio'),
		   ('Print'),
		   ('Website'),
		   ('Other')

	-- Constraints --
	alter table Lead 
        add constraint FK_Lead_Party
        foreign key (PartyId) 
        references Party

    alter table LeadActivity 
        add constraint FK_LeadActivity_Lead
        foreign key (LeadId) 
        references Lead

    alter table LeadActivity 
        add constraint FK_LeadActivity_Campaign
        foreign key (CampaignId) 
        references Campaign

    alter table LeadActivity 
        add constraint FK_LeadActivity_LeadStatus
        foreign key (LeadStatusId) 
        references md.LeadStatus

	alter table Campaign
		add constraint FK_Campaign_CampaignSource
		foreign key (CampaignSourceId)
		references md.CampaignSource

