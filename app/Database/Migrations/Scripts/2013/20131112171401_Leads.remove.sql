﻿
    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Lead_Party]') AND parent_object_id = OBJECT_ID('Lead'))
alter table Lead  drop constraint FK_Lead_Party

    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_LeadActivity_Lead]') AND parent_object_id = OBJECT_ID('LeadActivity'))
alter table LeadActivity  drop constraint FK_LeadActivity_Lead

    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_LeadActivity_Campaign]') AND parent_object_id = OBJECT_ID('LeadActivity'))
alter table LeadActivity  drop constraint FK_LeadActivity_Campaign

    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_LeadActivity_LeadStatus]') AND parent_object_id = OBJECT_ID('LeadActivity'))
alter table LeadActivity  drop constraint FK_LeadActivity_LeadStatus

    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Campaign_CampaignSource]') AND parent_object_id = OBJECT_ID('Campaign'))
alter table Campaign  drop constraint FK_Campaign_CampaignSource

if exists (select * from dbo.sysobjects where id = object_id(N'Lead') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table Lead
if exists (select * from dbo.sysobjects where id = object_id(N'Campaign') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table Campaign
if exists (select * from dbo.sysobjects where id = object_id(N'LeadActivity') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table LeadActivity

if exists (select * from dbo.sysobjects where id = object_id(N'md.LeadStatus') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table md.LeadStatus
if exists (select * from dbo.sysobjects where id = object_id(N'md.CampaignSource') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table md.CampaignSource