﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_AdditionalBenefits') and type in (N'P', N'PC'))
	begin
		drop procedure [dbo].[Reports_DetailedComparativeQuote_AdditionalBenefits]
	end
go

CREATE procedure [dbo].[Reports_DetailedComparativeQuote_AdditionalBenefits]
(
	@ProposalHeaderId int,
	@QuestionIds varchar(max)
)
as
		create table #AdditionalBenefits
		(
			Name varchar(max),
			Answer varchar(max)
		)


INSERT INTO #AdditionalBenefits (Name, Answer)
	select
		md.Question.Name,
		case
			when ProposalQuestionAnswer.Answer = 'True' then 'Yes'
			else ProposalQuestionAnswer.Answer
		end Answer
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
		inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
		inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
		inner join md.QuestionGroup on md.QuestionGroup.Id = md.Question.QuestionGroupId
	where
		md.Question.Id not in (select * from fn_StringListToTable(@QuestionIds)) and
		ProposalHeader.Id = @ProposalHeaderId and
		md.QuestionGroup.Name = 'Additional Options' and
		ProposalQuestionAnswer.Answer <> 'False' and
		ProposalQuestionAnswer.Answer <> '0' and
		ProposalQuestionAnswer.Answer <> '';

INSERT INTO #AdditionalBenefits (Name, Answer) 
	select
		md.Question.Name,
		md.QuestionAnswer.Answer
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
		inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
		inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
		inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
	where
		md.Question.Id in (select * from fn_StringListToTable(@QuestionIds)) and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalQuestionAnswer.Answer LIKE '%[^0-9]%' and
		md.QuestionAnswer.Answer <> '0' and
		md.QuestionAnswer.Answer <> '';

SELECT DISTINCT Name, Answer FROM #AdditionalBenefits Order by Name ASC;

Drop table #AdditionalBenefits;

GO