﻿

IF (OBJECT_ID('FK_ReportSchedule_Individual_OwnerId', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE ReportSchedule DROP CONSTRAINT FK_ReportSchedule_Individual_OwnerId
END

IF (OBJECT_ID('FK_ReportScheduleRecipient_Individual_RecipientId', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE ReportScheduleRecipient DROP CONSTRAINT FK_ReportScheduleRecipient_Individual_RecipientId
END