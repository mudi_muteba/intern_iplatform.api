﻿DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'ZURPERS'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @Name)

if (@productid IS NULL)
	BEGIN
		PRINT('OUT Of scope to create the product')
	RETURN
 END
 
--------------------224	'Motorcycle'-----------------------
SET @CoverID = 224				

SET @CoverName = 'Motorcycle'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END
--Overnight Address
if not exists(select * from QuestionDefinition where QuestionId=140 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Overnight Address', @CoverDefinitionId, 140, NULL, 1, 0, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the overnight address?', N'' , N'', 1, 2)
              end

			  
--Suburb
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 0, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the suburb and town name where you live?', N'' , N'', 1, 2)
              end
			  
			  
--Province
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 0, 1, 1, 0,
               N'In which province is this motocycle mainly operated in?', N'' , N'', 1, 2)
              end
			  
			  
--Postal Code
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 0, 1, 1, 0,
               N'Thank you, please confirm your street postal code', N'', N'', 1, 2)
              end
  
			  
 --work address
if not exists(select * from questiondefinition where questionid=139 and coverdefinitionid = @coverdefinitionid)
            begin
            insert [dbo].[questiondefinition] ([masterid], [displayname], [coverdefinitionid], [questionid], [parentid], [questiondefinitiontypeid], [visibleindex], [requiredforquote], [ratingfactor], [readonly], [tooltip],
            [defaultvalue], [regexpattern], [groupindex], [questiondefinitiongrouptypeid]) values (0, N'Work Address', @coverdefinitionid, 139, null, 1, 0, 1, 1, 0,
            N'{Title} {Name} would you please provide me with your work address?', N'' , N'', 1, 2)
            end

			  
--Suburb(Work)
if not exists(select * from QuestionDefinition where QuestionId=135 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb (Work)', @CoverDefinitionId, 135, NULL, 1, 0, 1, 1, 0,
              N'{Title} {Name} would you please provide me with the suburb and town name where you work?', N'' , N'', 1, 2)
              end
			  
			  
--Province(Work)
if not exists(select * from QuestionDefinition where QuestionId=154 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Province (Work)', @CoverDefinitionId, 154, NULL, 1, 0, 1, 1, 0,
             N'In which province is this motocycle mainly operated in?', N'' , N'', 1, 2)
             end
			  
			  
--Postal Code(Work)
if not exists(select * from QuestionDefinition where QuestionId=91 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code (Work)', @CoverDefinitionId, 91, NULL, 1, 0, 1, 1, 0,
              N'{Title} {Name} would you also please provide me with the suburb and town name where you work?', N'', N'', 1, 2)
              end
			  
			  
			  
--Overnight Area Type
if not exists(select * from QuestionDefinition where QuestionId=1180 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Overnight Area TypePostal Code (Work)', @CoverDefinitionId, 1180, NULL, 1, 0, 1, 1, 0,
             N'Overnight Area Type', N'', N'', 1, 2)
             end
			  
--Work Area Type
if not exists(select * from QuestionDefinition where QuestionId=1181 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Work Area Type', @CoverDefinitionId, 1181, NULL, 1, 0, 1, 1, 0,
             N'Work Area Type', N'', N'', 1, 2)
             end
--Asset
if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Asset', @CoverDefinitionId,141, NULL, 1, 0, 1, 1, 0,
               'I would like to gather some information about the asset you would like to insure.', N'', N'', 1, 2)
              end 
--Year Of Manufacture
if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 0, 1, 1, 0,
               N'I would like to gather some information about the motorcycle you would like to insure. Firstly what is the year of your motorcycle was first registered in?', N'' , N'', 1, 2)
              end
			  
--Make			  
if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 1, 1, 1, 0,
               N'{Title} {Name} what is the make and model of the motorcycle?', N'' , N'', 1, 2)
              end
--Model			  
if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 1, 1, 1, 0,
               N'What is the model of this motocycle?', N'0' , N'^[0-9\.]+$', 0, NULL)
              end
--MM Code
if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 2, 1, 1, 0,
               N'What is the Mead and McGrouther code for this motocycle?', N'' , N'', 1, 2)
              end
--Vehicle Type
if not exists(select * from QuestionDefinition where QuestionId=69 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Type', @CoverDefinitionId, 69, NULL, 1, 0, 1, 1, 0,
               N'What type of motocycle is the motocycle in question?', N'' , N'', 1, 2)
              end

--Vehicle Registration Number			  
if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Registration Number', @CoverDefinitionId, 97, NULL, 1, 0, 1, 1, 0,
               N'Can you please provide me with the license plate number?', N'' , N'', 1, 2)
              end
			  
--Class of Use			  
if not exists(select * from QuestionDefinition where QuestionId=1506 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) 
			  VALUES (0, N'Class of Use', @CoverDefinitionId, 1506, NULL, 1, 0, 1, 1, 0,N'What is the intended class of use for this motocycle?', N'' , N'', 1, 2)			  
			end			   
			   
 --Imported Modified			  
 if not exists(select * from QuestionDefinition where QuestionId=7 and CoverDefinitionId = @CoverDefinitionId)
               begin
               INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
               [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES
			   (0, N'Modified/Imported', @CoverDefinitionId, 7, NULL, 1, 0, 1, 1, 0, N'Was this vehicle imported or modified in any way?', N'' , N'', 1, 2)
               end
			

--Cover Type			  
if not exists(select * from QuestionDefinition where QuestionId=53 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Type of Cover', @CoverDefinitionId, 53, NULL, 1, 0, 1, 1, 0,
             N'Would you like full comprehensive cover on this motocycle?', N'' , N'', 1, 2)
             end
			 
--No Claim Bonus			  
if not exists(select * from QuestionDefinition where QuestionId=43 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'No Claim Bonus', @CoverDefinitionId, 43, NULL, 1, 0, 1, 1, 0,
             N'How many years has this client been accident free?', N'' , N'', 1, 2)
             end
			  
			  
--Vehicle Tracking Device			  			  
if not exists(select * from QuestionDefinition where QuestionId=87 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Tracking Device', @CoverDefinitionId, 87, NULL, 1, 0, 1, 1, 0,
             N'Thank you, does the vehicle have a tracking device installed?', N'' , N'', 1, 2)
             end
			  
--Secure Complex			  			  
if not exists(select * from QuestionDefinition where QuestionId=14 and CoverDefinitionId = @CoverDefinitionId)
            begin
            INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
            [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Secure Complex', @CoverDefinitionId, 14, NULL, 1, 0, 1, 1, 0,
             N'Is the motocycle parked in a security complex?', N'' , N'', 1, 2)
             end
			  
-- --Drivers For This Vehicle		
if not exists(select * from QuestionDefinition where QuestionId=63 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Driver for this vehicle', @CoverDefinitionId, 63, NULL, 1, 0, 1, 1, 0,
             N'Who will be the main driver for this vehicle?', N'' , N'', 1, 2)
             end
			  	  
