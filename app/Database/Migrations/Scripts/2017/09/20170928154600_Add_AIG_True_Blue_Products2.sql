﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'Domestic Armour CC'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  Name= @Name)

SELECT @productid
if (@productid IS NULL)
BEGIN
INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          RiskItemTypeId ,
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          @Name , -- Name - nvarchar(255)
          29 , -- ProductOwnerId - int
          29 , -- ProductProviderId - int
          7 , -- ProductTypeId - int
          N'123456' , -- AgencyNumber - nvarchar(255)
          N'AIGDTB' , -- ProductCode - nvarchar(255)
          CAST(0x00009CD400000000 AS DateTime) , -- StartDate - datetime
          NULL , -- EndDate - datetime
          0 , -- RiskItemTypeId - int
          0 , -- IsDeleted - bit
          N'aig.png' , -- ImageName - nvarchar(1024)
          NULL , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )

		SET @productid = SCOPE_IDENTITY()
 END




--------------------224	MotorCycle-----------------------
SET @CoverID = 224	 			

SET @CoverName = 'Motorcycle'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=140 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Overnight Address', @CoverDefinitionId, 140, NULL, 1, 0, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the overnight address?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 1, 0,
               N'Id like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 5, 4, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=862 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Financed', @CoverDefinitionId, 862, NULL, 1, 0, 0, 1, 0,
               N'Financed', N'' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Vehicle Registration Number', @CoverDefinitionId, 97, NULL, 1, 1, 0, 0, 0,
               N'Can you please provide me with the license plate number?', N'' , N'^.{0,10}$', 5, 4, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the suburb and town name where you live?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 2, 1, 1, 0,
               N'In which province is this vehicle mainly operated in?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'Id like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', N'' , N'^[0-9]+$', 5, 4, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 3, 1, 1, 0,
               N'{Title} {Name} what is the make and model of the vehicle?', N'' , N'', 5, 4, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 3, 1, 1, 0,
               N'Thank you, please confirm your street postal code', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 4, 1, 1, 0,
               N'What is the model of this vehicle?', N'' , N'', 5, 4, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 5, 1, 1, 1,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', 5, 4, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=139 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Work Address', @CoverDefinitionId, 139, NULL, 1, 5, 1, 1, 0,
               N'{Title} {Name} would you please provide me with your work address?', N'' , N'', 3, 3, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=104 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Main Driver ID Number', @CoverDefinitionId, 104, NULL, 1, 5, 1, 1, 0,
               N'What is the ID Number for the main driver of this vehicle?', N'' , N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])', 0, 5, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=72 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Marital Status', @CoverDefinitionId, 72, NULL, 1, 6, 0, 1, 0,
               N'What is the marital status of this client?', N'178' , N'', 0, 5, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=135 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb (Work)', @CoverDefinitionId, 135, NULL, 1, 6, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the suburb and town name where you work?', N'' , N'', 3, 3, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=41 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Drivers Licence First Issued Date', @CoverDefinitionId, 41, NULL, 1, 7, 1, 1, 0,
               N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'' , N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$', 0, 5, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=73 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Drivers Licence Type', @CoverDefinitionId, 73, NULL, 1, 8, 1, 1, 0,
               N'What is the drivers licence type of the main driver for this vehicle?', N'190' , N'', 0, 5, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 12, 1, 1, 0,
               N'What is the sum insured of the vehicle?', N'' , N'^[0-9\.]+$', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=70 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Gender', @CoverDefinitionId, 70, NULL, 1, 13, 0, 1, 0,
               N'What is the gender of the main driver for this vehicle?', N'' , N'', 0, 5, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=43 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'No Claim Bonus', @CoverDefinitionId, 43, NULL, 1, 19, 0, 1, 0,
               N'How many years has this client been accident free?', N'' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=42 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Main Driver Date of Birth', @CoverDefinitionId, 42, NULL, 1, 21, 1, 1, 0,
               N'What is the Date of Birth of the main driver of this vehicle?', N'' , N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$', 0, 5, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=60 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Relationship to the Insured', @CoverDefinitionId, 60, NULL, 1, 23, 1, 1, 0,
               N'What is the drivers relationship to the insured?', N'134' , N'', 0, 5, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=64 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Selected Excess', @CoverDefinitionId, 64, NULL, 1, 24, 1, 1, 0,
               N'A selected excess will reduce a clients premium.', N'152' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=103 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Finance House', @CoverDefinitionId, 103, NULL, 1, 27, 0, 1, 0,
               N'At which institution is the vehicle financed?', N'' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=91 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Postal Code (Work)', @CoverDefinitionId, 91, NULL, 1, 101, 1, 1, 0,
               N'{Title} {Name} would you also please provide me with the suburb and town name where you work?', N'' , N'', 3, 3, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=1507 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Class of Use', @CoverDefinitionId, 1507, NULL, 1, 7, 0, 0, 0,
               N'{Title} {Name} is this vehicle mainly for private or business use?', N'54' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=1500 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Type of Cover', @CoverDefinitionId, 1500, NULL, 1, 11, 0, 1, 0,
               N'What type of cover is required for this vehicle?', N'' , N'', NULL, NULL, 0)
              end

			
---MultiquoteQuestions

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverID
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverID   
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 1507 -- Aig True Blue Class Of Use Motorcyle

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id

SET @NewQuestionId = 1500 -- Aig True Blue Type Of Cover

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id



--------------------258	Personal Legal Liability-----------------------
SET @CoverID = 258			

SET @CoverName = 'Personal Liability'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'0' , N'^[0-9\.]+$', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2, 0)
              END              
if not exists(select * from QuestionDefinition where QuestionId=1508 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Personal Liability Limit', @CoverDefinitionId, 1508, NULL, 1, 3, 0, 0, 0,
               N'Personal Liability Limit', N'' , N'', 1, 2, 0)
              end




---Map To Multiquote

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverID
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverID   
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 1508 -- AIG TB Personal Liability

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id



--------------------257	Personal Accident-----------------------
SET @CoverID = 257			

SET @CoverName = 'Personal Accident'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'0' , N'^[0-9\.]+$', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2, 0)
              END     
if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 1, 0, 0,
               N'I''d like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 5, 4, 0)
              end

---Map To Multiquote

--NA

--------------------353 MultiQuoteWatercraft-----------------------
SET @CoverID = 353	 			

SET @CoverName = 'Watercraft'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = 27 AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, 27, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2)
              end

if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2)
              end
              
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'0' , N'^[0-9\.]+$', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2)
              END

if not exists(select * from QuestionDefinition where QuestionId=1485 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Watercraft Type', @CoverDefinitionId, 1485, NULL, 1, 32, 0, 0, 0,
               N'What is the type of the watercraft?', N'' , N'', 0, NULL)
              END       
			         
if not exists(select * from QuestionDefinition where QuestionId=1081 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Im a dummy question', @CoverDefinitionId, 1081, NULL, 1, 4, 0, 0, 0,
               N'This a dummy question for mapping MultiQoute Questions', N'NULL' , N'NULL', 0, NULL, 1)
              end


----------------------End of MultiQuoteWatercraft---------------------------------------


--------------------353 Watercraft-----------------------
SET @CoverID = 353	 			

SET @CoverName = 'Watercraft'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productId AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2)
              end

if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2)
              end
              
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'0' , N'^[0-9\.]+$', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2)
              END

if not exists(select * from QuestionDefinition where QuestionId=1485 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Watercraft Type', @CoverDefinitionId, 1485, NULL, 1, 32, 0, 0, 0,
               N'What is the type of the watercraft?', N'' , N'', 0, NULL)
              END       

if not exists(select * from QuestionDefinition where QuestionId=1509 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Watercraft Horse Power', @CoverDefinitionId, 1509, NULL, 1, 35, 0, 0, 0,
               N'What is the horse power of the watercraft?', N'' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=1510 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Submerged Objects', @CoverDefinitionId, 1510, NULL, 1, 36, 0, 0, 0,
               N'Submerged Objects', N'' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=1511 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Liability to Third Party', @CoverDefinitionId, 1511, NULL, 1, 37, 0, 0, 0,
               N'Liability to Third Party', N'' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=1512 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Watercraft Speeds', @CoverDefinitionId, 1512, NULL, 1, 38, 0, 0, 0,
               N'Watercraft Speeds', N'' , N'', 0, NULL, 0)
              end
			     

---Map To Multiquote

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverID
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverID   
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 1509 -- 

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


SET @NewQuestionId = 1510 -- 

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


SET @NewQuestionId = 1511 -- 

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


SET @NewQuestionId = 1512 -- 

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


----------------------End of Watercraft---------------------------------------




--------------------62	Caravan or Trailer-----------------------
SET @CoverID = 62		

SET @CoverName = 'Caravan or Trailer'

--MULTIQUOTE
if not exists(select * from QuestionDefinition where QuestionId=1081 and CoverDefinitionId = 200)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Im a dummy question', 200, 1081, NULL, 1, 4, 0, 0, 0,
               N'This a dummy question for mapping MultiQoute Questions', N'NULL' , N'NULL', 0, NULL, 1)
              end

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 0, 0, 0, 1,
               N'What is the year of manufacture for this vehicle?', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 0, 1,
               N'Id like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 1, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 1,
               N'What is the sum insured of the vehicle?', N'' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=139 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Address', @CoverDefinitionId, 139, NULL, 1, 1, 0, 0, 1,
               N'{Title} {Name} would you please provide me with your address?', N'' , N'', 2, NULL, 1)
              end
if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 1, 0, 0, 1,
               N'What is the make of this vehicle?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 2, 0, 0, 1,
               N'What is the model of this vehicle?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 3, 0, 0, 1,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', 0, NULL, 0)
              END
if not exists(select * from QuestionDefinition where QuestionId=1030 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Parking Method', @CoverDefinitionId, 1030, NULL, 1, 17, 1, 1, 1,
               N'Where is this vehicle parked during the day?', N'' , N'', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=103 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Finance House', @CoverDefinitionId, 103, NULL, 1, 27, 0, 1, 0,
               N'At which institution is the vehicle financed?', N'' , N'', 0, NULL, 0)
              END              
if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Vehicle Registration Number', @CoverDefinitionId, 97, NULL, 1, 1, 0, 0, 0,
               N'Can you please provide me with the license plate number?', N'' , N'^.{0,10}$', 5, 4, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'VIN Number', @CoverDefinitionId, 1010, NULL, 1, 4, 0, 0, 0,
               N'VIN Number', N'' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=53 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Type of Cover', @CoverDefinitionId, 53, NULL, 1, 8, 1, 1, 0,
               N'Would you like full comprehensive cover on this vehicle?', N'89' , N'', 0, NULL, 0)
              end


---Map To Multiquote

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverID
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverID   
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 103 -- 

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


SET @NewQuestionId = 97 -- 

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


SET @NewQuestionId = 1010 -- 

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


SET @NewQuestionId = 53 -- 

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


----------------------End of Caravan Trailer ---------------------------------------
