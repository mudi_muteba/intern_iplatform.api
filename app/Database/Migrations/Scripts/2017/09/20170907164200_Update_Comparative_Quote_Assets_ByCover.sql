﻿ALTER procedure [dbo].[Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding]
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	select distinct
		ltrim(rtrim(dbo.ToCamelCase(Asset.[Description]))) [Description],
		cast(QuoteItem.SumInsured as numeric(18, 2)) [Value]
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber OR Asset.Id = ProposalDefinition.AssetId
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
	group by
		Asset.[Description],
		QuoteItem.SumInsured
GO

ALTER procedure [dbo].[Reports_DetailedComparativeQuote_Assets_ByCover_ForContents]
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	select distinct
		ltrim(rtrim(dbo.ToCamelCase(Asset.[Description]))) [Description],
		cast(QuoteItem.SumInsured as numeric(18, 2)) [Value]
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber OR Asset.Id = ProposalDefinition.AssetId
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		QuoteItem.AssetNumber = ProposalDefinition.Id and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
	group by
		Asset.[Description],
		QuoteItem.SumInsured
GO

ALTER procedure [dbo].[Reports_DetailedComparativeQuote_Assets_ByCover_ForPersonalLegalLiability]
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	select distinct
		ltrim(rtrim(dbo.ToCamelCase(Asset.[Description]))) [Description],
		cast(QuoteItem.SumInsured as numeric(18, 2)) [Value]
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber OR Asset.Id = ProposalDefinition.AssetId
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
	group by
		Asset.[Description],
		QuoteItem.SumInsured
GO

ALTER procedure [dbo].[Reports_DetailedComparativeQuote_Assets_ByCover_ForPersonalAccident]
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	select distinct
		ltrim(rtrim(dbo.ToCamelCase(Asset.[Description]))) [Description],
		cast(QuoteItem.SumInsured as numeric(18, 2)) [Value]
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber OR Asset.Id = ProposalDefinition.AssetId
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
	group by
		Asset.[Description],
		QuoteItem.SumInsured
GO

ALTER procedure [dbo].[Reports_DetailedComparativeQuote_Assets_ByCover_ForOther]
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	select distinct
		ltrim(rtrim(dbo.ToCamelCase(Asset.[Description]))) [Description],
		cast(QuoteItem.SumInsured as numeric(18, 2)) [Value]
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber OR Asset.Id = ProposalDefinition.AssetId
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
	group by
		Asset.[Description],
		QuoteItem.SumInsured
GO
