﻿IF OBJECT_ID('tempdb..#productCodes') IS NOT NULL
    DROP TABLE #productCodes

DECLARE @paCoverId INT = (SELECT Id FROM md.Cover WHERE Code = 'PERSONAL_ACCIDENT');

CREATE TABLE #productCodes (Code [NVARCHAR](255) COLLATE DATABASE_DEFAULT,
							CoverDefinitionId INT,
							ProductId INT);

DECLARE @DummyQuestionId INT = (SELECT TOP 1 qed.Id FROM QuestionDefinition qed
								JOIN CoverDefinition cd ON cd.Id = qed.CoverDefinitionId AND qed.QuestionId = 1081
								JOIN Product pd ON pd.Id = cd.ProductId AND pd.ProductCode = 'MUL');

SELECT * 
	INTO #MapQuesionDefinitionTemp
	FROM QuestionDefinition
	WHERE Id IS NULL

INSERT INTO #productCodes(Code) VALUES (N'ZURPERS')

UPDATE pProduct
	SET ProductId = p.Id
	FROM #productCodes pProduct
	JOIN Product p ON p.ProductCode = pProduct.Code COLLATE DATABASE_DEFAULT

UPDATE pCover
	SET CoverDefinitionId = c.Id
	FROM #productCodes pCover
	JOIN CoverDefinition c ON c.ProductId = pCover.ProductId
								AND c.CoverId = @paCoverId;

INSERT INTO CoverDefinition([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	SELECT 0, N'Personal Accident', pd.ProductId, @paCoverId, NULL, 1, 0, 0
	FROM #productCodes pd
	WHERE pd.CoverDefinitionId IS NULL

UPDATE pCover
	SET CoverDefinitionId = c.Id
	FROM #productCodes pCover
	JOIN CoverDefinition c ON c.ProductId = pCover.ProductId
								AND c.CoverId = @paCoverId;

IF OBJECT_ID('tempdb..#QDtemp') IS NOT NULL
    DROP TABLE #QDtemp

SELECT * 
	INTO #QDtemp
	FROM QuestionDefinition
	WHERE Id IS NULL

ALTER TABLE #QDtemp DROP COLUMN Id;
ALTER TABLE #QDtemp ADD ID INT NULL;
ALTER TABLE #QDtemp ALTER COLUMN CoverDefinitionId INT NULL;

INSERT INTO #QDtemp ([MasterId],  [DisplayName],      											[CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [ToolTip])
VALUES				(0,           N'Occupation',	  											NULL,				   1490,	     NULL,       1,                          1,              1,                  1,              0,          N'',            N'',            2,            NULL,							0, 		N'Occupation'),
					(0,           N'Permanent Disability Sum Insured',							NULL,				   1492,	     NULL,       1,                          2,              1,                  1,              0,          N'',            N'',            2,            NULL,							0, 		N'Permannet Disability Sum Insured'),
					(0,           N'Temporary Disability Sum Insured',							NULL,				   1493,	     NULL,       1,                          3,              1,                  1,              0,          N'',            N'',            2,            NULL,							0, 		N'Temporary Disability Sum Insured'),
					(0,           N'Medical Expenses Sum Insured',								NULL,				   1494,	     NULL,       1,                          4,              1,                  1,              0,          N'',            N'',            2,            NULL,							0, 		N'Medical Expenses Sum Insured'),
					(0,           N'Total Temporary Disability Weeks Covered',					NULL,				   1495,	     NULL,       1,                          5,              1,                  1,              0,          N'',            N'',            2,            NULL,							0, 		N'Total Temporary Disability Weeks Covered'),
					(0,           N'Total Temporary Disability Percentage of Weekly Earnings',	NULL,				   1496,	     NULL,       1,                          6,              1,                  1,              0,          N'',            N'',            2,            NULL,							0, 		N'Total Temporary Disability Percentage of Weekly Earnings'),
					(0,           N'Death Benefit Annual',										NULL,				   1497,	     NULL,       1,                          7,              1,                  1,              0,          N'',            N'',            2,            NULL,							0, 		N'Death Benefit Annual'),
					(0,			  N'Risk Address',	  											NULL,				   	88,			 NULL,		 1,							 0,				 1,					 1,				 0,			 N'',			 N'^[0-9]+$',	 1,			   2,							    0,		N'What is the risk address'),
					(0,			  N'Suburb',		  											NULL,				   	89,			 NULL,		 1,							 1,				 1,					 1,				 0,			 N'',			 N'',			 1,			   2,								0,		N'What is the suburb name?'),
					(0,			  N'Postal Code',	  											NULL,				   	90,			 NULL,		 1,							 1,				 1,					 1,				 0,			 N'',			 N'',			 1,			   2,								0,		N'What is the postal code?'),
					(0,			  N'Sum Insured',     											NULL,				   	102,		 NULL,		 1,							 1,				 1,					 1,				 0,			 N'',			 N'^[0-9\.]+$',  0,			   NULL,							0,		N'What is the sum insured for this cover?')


IF OBJECT_ID('tempdb..#QDInsert') IS NOT NULL
    DROP TABLE #QDInsert

SELECT p.CoverDefinitionId, q.Id, q.[MasterId],  q.[DisplayName], q.[QuestionId], q.[ParentId], q.[QuestionDefinitionTypeId], q.[VisibleIndex], q.[RequiredForQuote], q.[RatingFactor], q.[ReadOnly], q.[ToolTip], q.[DefaultValue], q.[RegexPattern], q.[GroupIndex], q.[QuestionDefinitionGroupTypeId]
	INTO #QDInsert
	FROM #productCodes p
	CROSS JOIN #QDtemp q

UPDATE pQD
	SET Id = qd.Id
	FROM #QDInsert pQD
	JOIN QuestionDefinition qd ON qd.QuestionId = pQD.QuestionId
								AND qd.CoverDefinitionId = pQD.CoverDefinitionId

DELETE #QDInsert WHERE Id IS NOT NULL

INSERT INTO QuestionDefinition([MasterId],
							   [DisplayName],
							   [CoverDefinitionId],
							   [QuestionId],
							   [ParentId],
							   [QuestionDefinitionTypeId],
							   [VisibleIndex],
							   [RequiredForQuote],
							   [RatingFactor],
							   [ReadOnly],
							   [ToolTip],
							   [DefaultValue],
							   [RegexPattern],
							   [GroupIndex],
							   [QuestionDefinitionGroupTypeId])
					    SELECT [MasterId],
							   [DisplayName],
							   [CoverDefinitionId],
							   [QuestionId],
							   [ParentId],
							   [QuestionDefinitionTypeId],
							   [VisibleIndex],
							   [RequiredForQuote],
							   [RatingFactor],
							   [ReadOnly],
							   [ToolTip],
							   [DefaultValue],
							   [RegexPattern],
							   [GroupIndex],
							   [QuestionDefinitionGroupTypeId]
						  FROM #QDInsert
