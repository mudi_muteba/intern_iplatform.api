-----------MOTORSERVICE-----------
DECLARE @productId INT, @ProductCode VARCHAR(50), @CoverDefinitionId INT
SET @ProductCode = 'MOTORSERVICE'

SET @ProductCode =(SELECT TOP 1 ID FROM dbo.Product  WHERE ProductCode = @ProductCode)
SET @CoverDefinitionId = (SELECT TOP 1 ID FROM dbo.CoverDefinition WHERE ProductId = @ProductCode AND CoverId = 378)

UPDATE dbo.QuestionDefinition
SET Hide = 1
WHERE CoverDefinitionId = @CoverDefinitionId 
	AND QuestionId = 141

SELECT * FROM QuestionDefinition
WHERE CoverDefinitionId = @CoverDefinitionId 
	AND QuestionId = 141

-----------MOTORWAR-----------
SET @ProductCode = 'MOTORWAR'

SET @ProductCode =(SELECT TOP 1 ID FROM dbo.Product  WHERE ProductCode = @ProductCode)
SET @CoverDefinitionId = ( SELECT TOP 1 ID FROM dbo.CoverDefinition WHERE ProductId = @ProductCode AND CoverId = 375)

UPDATE dbo.QuestionDefinition
SET Hide = 1
WHERE CoverDefinitionId = @CoverDefinitionId 
	AND QuestionId = 141


SELECT * FROM QuestionDefinition
WHERE CoverDefinitionId = @CoverDefinitionId 
	AND QuestionId = 141