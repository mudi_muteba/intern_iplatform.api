﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Benefits_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure [dbo].[Reports_DetailedComparativeQuote_Benefits_ByCover]
	end
go

CREATE procedure [dbo].[Reports_DetailedComparativeQuote_Benefits_ByCover]
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	create table #Breakdown
	(
		Col_1 varchar(255),
		Col_2 varchar(255),
		Col_3 varchar(255),
		Col_4 varchar(255)
	)
	if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Building'))
		begin
			print 'Building'
			exec Reports_DetailedComparativeQuote_Benefits_ByCover_ForBuilding @ProposalHeaderId, @QuoteIds, @CoverId
		end
	else if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Contents'))
		begin
			print 'Contents'
			exec Reports_DetailedComparativeQuote_Benefits_ByCover_ForContents @ProposalHeaderId, @QuoteIds, @CoverId
		end
	else if(@CoverId in (select md.Cover.Id from md.Cover where md.Cover.Name <> 'Buidling' and md.Cover.Name <> 'Contents'))
		begin
			print 'Other'
			exec Reports_DetailedComparativeQuote_Benefits_ByCover_ForNonBuilding @ProposalHeaderId, @QuoteIds, @CoverId
		end
	select * from #Breakdown
	drop table #Breakdown