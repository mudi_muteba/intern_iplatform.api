﻿/****** Object:  StoredProcedure [dbo].[Report_CallCentre_InsurerQuoteBreakdown_Header]    Script Date: 9/4/2017 10:39:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover_ForAllRisk') and type in (N'P', N'PC'))
	begin
		drop procedure [dbo].[Reports_DetailedComparativeQuote_Assets_ByCover_ForAllRisk]
	end
go

CREATE procedure [dbo].[Reports_DetailedComparativeQuote_Assets_ByCover_ForAllRisk]
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	
SELECT DISTINCT ltrim(rtrim(dbo.ToCamelCase(a.[Description]))) [Description],
		cast(QI.SumInsured as numeric(18, 2)) [Value]
FROM QuoteItem QI
INNER JOIN Quote q on q.Id = QI.QuoteId AND q.IsDeleted != 1
INNER JOIN CoverDefinition cd on cd.Id = QI.CoverDefinitionId
INNER JOIN Product p on p.Id = cd.ProductId
INNER JOIN Organization o on o.PartyId = p.ProductOwnerId
INNER JOIN Asset a on a.AssetNo = QI.AssetNumber
WHERE q.Id in (select * from fn_StringListToTable(@QuoteIds))
AND cd.CoverId = @CoverId AND QI.IsDeleted != 1 
group by
		a.[Description],
		QI.SumInsured
