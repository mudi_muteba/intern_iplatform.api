﻿/****** Object:  StoredProcedure [dbo].[Report_CallCentre_InsurerQuoteBreakdown_Header]    Script Date: 9/4/2017 10:39:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure [dbo].[Reports_DetailedComparativeQuote_Assets_ByCover]
	end
go

CREATE procedure [dbo].[Reports_DetailedComparativeQuote_Assets_ByCover]
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Building'))
		begin
			print 'Building'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding @ProposalHeaderId, @QuoteIds, @CoverId
		end
	else if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Contents'))
		begin
			print 'Contents'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForContents @ProposalHeaderId, @QuoteIds, @CoverId
		end
	else if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Personal Legal Liability'))
		begin
			print 'Personal Legal Liability'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForPersonalLegalLiability @ProposalHeaderId, @QuoteIds, @CoverId
		end
	else if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Personal Accident'))
		begin
			print 'Personal Accident'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForPersonalAccident @ProposalHeaderId, @QuoteIds, @CoverId
		end
	else if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'All Risk'))
		begin
			print 'All Risk'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForAllRisk @ProposalHeaderId, @QuoteIds, @CoverId
		end
	else if(@CoverId in (select md.Cover.Id from md.Cover where md.Cover.Name not in ('Building', 'Contents', 'Personal Legal Liability','Personal Accident', 'All Risk')))
		begin
			print 'Other'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForOther  @ProposalHeaderId, @QuoteIds, @CoverId
		end
GO