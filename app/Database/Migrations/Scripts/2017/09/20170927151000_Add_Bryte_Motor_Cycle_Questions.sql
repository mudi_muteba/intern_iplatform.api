﻿DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'MUL'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @Name)

if (@productid IS NULL)
	BEGIN
		PRINT('OUT Of scope to create the product')
	RETURN
 END
 
--------------------224	'Motorcycle'-----------------------
SET @CoverID = 224				

SET @CoverName = 'Motorcycle'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END
--Secure Complex			  			  
if not exists(select * from QuestionDefinition where QuestionId=14 and CoverDefinitionId = @CoverDefinitionId)
            begin
            INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
            [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Secure Complex', @CoverDefinitionId, 14, NULL, 1, 0, 1, 1, 0,
             N'Is the motocycle parked in a security complex?', N'' , N'', 1, 2)
             end
			  
-- --Drivers For This Vehicle		
if not exists(select * from QuestionDefinition where QuestionId=63 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Driver for this vehicle', @CoverDefinitionId, 63, NULL, 1, 0, 1, 1, 0,
             N'Who will be the main driver for this vehicle?', N'' , N'', 1, 2)
             end
			  	  
