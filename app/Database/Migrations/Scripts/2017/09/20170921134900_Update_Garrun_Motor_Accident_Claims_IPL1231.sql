﻿DECLARE @ProductId int;
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'GARRUN';

Update ProductClaimsQuestionDefinition Set Required = 0 WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 43
Update ProductClaimsQuestionDefinition Set Required = 0 WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 84
Update ProductClaimsQuestionDefinition Set Required = 0 WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 85
Update ProductClaimsQuestionDefinition Set Required = 0 WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 86
Update ProductClaimsQuestionDefinition Set Required = 0 WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 87
Update ProductClaimsQuestionDefinition Set Required = 0 WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 88
