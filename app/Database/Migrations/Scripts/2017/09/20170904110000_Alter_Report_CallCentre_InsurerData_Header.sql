﻿
/****** Object:  StoredProcedure [dbo].[Report_CallCentre_InsurerData_Header]    Script Date: 9/4/2017 10:59:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_Header') and type in (N'P', N'PC'))
	begin
		drop procedure [dbo].[Report_CallCentre_InsurerData_Header]
	end
go


CREATE procedure [dbo].[Report_CallCentre_InsurerData_Header]
(
	@CampaignIds varchar(max),
	@CampaignID int
)
as
	declare @IpChannelId int
	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')
	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id in (select * from fn_StringListToTable(@CampaignIds)) and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) = 1)
		begin
			exec Report_CallCentre_InsurerData_Header_WhenChannelExists @CampaignId
		end
	else
		begin
			exec Report_CallCentre_InsurerData_Header_WhenChannelDoesNotExist @IpChannelId
		end

GO