﻿
/****** Object:  StoredProcedure [dbo].[Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithoutInsurerIds]    Script Date: 9/4/2017 12:17:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithoutInsurerIds') and type in (N'P', N'PC'))
	begin
		drop procedure [dbo].[Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithoutInsurerIds]
	end
go

CREATE procedure [dbo].[Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithoutInsurerIds]
(
	@DateFrom datetime,
	@DateTo datetime
)
as
	;with Summary_CTE (ChannelId, AgentId, Channel, Agent)
	as 
	(
		select distinct
			Channel.Id,
			[User].Id,
			Channel.Name,
			Individual.Surname + ', ' + Individual.FirstName
		from QuoteHeader
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
			inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
			inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			inner join Channel on Channel.Id = Campaign.ChannelId
			inner join [User] on [User].Id = LeadActivity.UserId
			inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
			inner join UserIndividual on UserIndividual.UserId = [User].Id
			inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		where
			--Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
			--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			[User].IsActive = 1
		group by
			Channel.Id,
			[User].Id,
			Channel.Name,
			Individual.Surname + ', ' + Individual.FirstName
		)
	select
		Channel BrokerName,
		Agent,
		(
			select
				isnull(count(distinct(LeadActivity.LeadId)), 0)
			from LeadActivity
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo
		) LeadsCount,
		(
			select
				isnull(count(distinct(LeadActivity.Id)), 0)
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) NoOfQuotes,
		(
			select
				isnull(count(distinct(Quote.Id)), 0)
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Quote.Id in
				(
					select
						QuoteId
					from QuoteUploadLog
					where
						QuoteUploadLog.QuoteId = Quote.Id and
						QuoteUploadLog.IsDeleted = 0
				) and
				--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) NoOfQuotesUploaded,
		(
			isnull(
				cast
				(
					(
						select
							cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
						from LeadActivity
							inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
						where
							Quote.Id in
							(
								select
									QuoteId
								from QuoteUploadLog
								where
									QuoteUploadLog.QuoteId = Quote.Id and
									QuoteUploadLog.IsDeleted = 0
							) and
							--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
							Campaign.ChannelId = Summary_CTE.ChannelId and
							LeadActivity.UserId = Summary_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)
					/
					(
						select
							nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
						from LeadActivity
							inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
						where
							--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
							Campaign.ChannelId = Summary_CTE.ChannelId and
							LeadActivity.UserId = Summary_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)
					* 100
					as numeric( 18, 2)
				)
			, 0)
		) Closing,
		(
			select
				cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2))
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			where
				Quote.Id in
				(
					select
						QuoteId
					from QuoteUploadLog
					where
						QuoteUploadLog.QuoteId = Quote.Id and
						QuoteUploadLog.IsDeleted = 0
				) and
				--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
				Campaign.ChannelId = Summary_CTE.ChannelId and
				LeadActivity.UserId = Summary_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) LeadsSPV
	from
		Summary_CTE
	order by
		BrokerName,
		Agent
GO