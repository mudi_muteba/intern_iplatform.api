﻿IF EXISTS 
(
	SELECT * 
	FROM sys.objects AS O
	WHERE O.[type] = 'P'
		AND O.[object_id] = OBJECT_ID('dbo.DataView_LeadImport')
)
BEGIN
	DROP PROCEDURE dbo.DataView_LeadImport;
END
GO
CREATE PROCEDURE [dbo].[DataView_LeadImport]
(
	@ChannelId AS INTEGER,
	@DateFrom AS DATETIME,
	@DateTo AS DATETIME
)
AS
BEGIN	

	IF OBJECT_ID('tempdb..#CreatedLeads') IS NOT NULL DROP TABLE #CreatedLeads;
	IF OBJECT_ID('tempdb..#SalesDetails') IS NOT NULL DROP TABLE #SalesDetails;
	IF OBJECT_ID('tempdb..#ImportedLeads') IS NOT NULL DROP TABLE #ImportedLeads;
	IF OBJECT_ID('tempdb..#AllocatedAgent') IS NOT NULL DROP TABLE #AllocatedAgent;
	IF OBJECT_ID('tempdb..#QuotedLeads') IS NOT NULL DROP TABLE #QuotedLeads;
	IF OBJECT_ID('tempdb..#AcceptedQuotes') IS NOT NULL DROP TABLE #AcceptedQuotes;

	--Get all leads created for the channel
	SELECT
		C.Name AS Channel,
		C2.Name AS Campaign,
		P.DateCreated AS DateLeadImported,
		L.Id AS LeadId,
		I.FirstName + ' ' + I.Surname AS Client,
		I.IdentityNo AS IDNumber,
		CD.Cell AS CellNumber
	INTO #CreatedLeads
	FROM dbo.Channel AS C WITH (NOLOCK)
	INNER JOIN dbo.Campaign AS C2 WITH (NOLOCK)
		ON C2.ChannelId = C.Id
		AND C2.IsDeleted = 0
	INNER JOIN dbo.CampaignLeadBucket AS CLB WITH (NOLOCK)
		ON CLB.CampaignId = C2.Id
		AND CLB.IsDeleted = 0
	INNER JOIN Lead AS L WITH (NOLOCK)
		ON L.Id = CLB.LeadId
		AND L.IsDeleted = 0
	INNER JOIN dbo.Party AS P WITH (NOLOCK)
		ON P.Id = L.PartyId
		AND P.DateCreated BETWEEN @DateFrom AND @DateTo
		AND P.IsDeleted = 0
	INNER JOIN dbo.Individual AS I WITH (NOLOCK)
		ON I.PartyId = P.Id	
	INNER JOIN dbo.ContactDetail AS CD WITH (NOLOCK)
		ON CD.Id = P.ContactDetailId
		AND CD.IsDeleted = 0
	WHERE C.Id = @ChannelId
	GROUP BY CLB.LeadId,
		C.Name,
		C2.Name,
		P.DateCreated,
		L.Id,
		I.FirstName,
		I.Surname,
		I.IdentityNo,
		CD.Cell;

	--Get all SalesDetail
	SELECT
		SD.LeadId,
		SD.SalesStructureId,
		SD.AgentId,
		SD.AssetId,
		ROW_NUMBER() OVER (PARTITION BY SD.LeadId ORDER BY SD.Id) AS RowNumber
	INTO #SalesDetails
	FROM dbo.SalesDetails AS SD WITH (NOLOCK)
	WHERE SD.LeadId IN
		(
			SELECT
				CL.LeadId
			FROM #CreatedLeads AS CL
		)
		AND SD.IsDeleted = 0

	--Get all imported leads 
	SELECT
		LA.LeadId,
		IT.Name AS LeadSystem,
		ST.Name AS LeadSource,
		SF.Firstname + ' ' + SF.Lastname AS LeadPassedBy,
		A.[Description] AS Asset
	INTO #ImportedLeads
	FROM dbo.LeadActivity AS LA WITH (NOLOCK)
	INNER JOIN dbo.ImportedLeadActivity AS ILA WITH (NOLOCK)
		ON ILA.LeadActivityId = LA.Id
		AND ILA.Existing = 0
	INNER JOIN md.ImportType AS IT WITH (NOLOCK)
		ON IT.Id = ILA.ImportTypeId
	INNER JOIN #SalesDetails AS SD WITH (NOLOCK)
		ON SD.LeadId = LA.LeadId
		AND SD.RowNumber = 1
	INNER JOIN dbo.SalesStructure AS SS WITH (NOLOCK)
		ON SS.Id = SD.SalesStructureId
		AND ss.IsDeleted = 0
	INNER JOIN dbo.SalesTag AS ST WITH (NOLOCK)
		ON ST.Id = SS.BranchSalesTagId
		AND ST.IsDeleted = 0
	INNER JOIN dbo.SalesFNI AS SF WITH (NOLOCK)
		ON SF.Id = SD.AgentId
		AND SF.IsDeleted = 0
	INNER JOIN dbo.Asset AS A WITH (NOLOCK)
		ON A.Id = SD.AssetId
		AND A.IsDeleted = 0
	WHERE LA.ActivityTypeId = 2 --Imported
		AND LA.LeadId IN
		(
			SELECT 
				CL.LeadId
			FROM #CreatedLeads AS CL
		)
		AND LA.IsDeleted = 0
	GROUP BY
		LA.LeadId,
		IT.Name,
		ST.Name,
		SF.Firstname,
		SF.Lastname,
		A.[Description]

	--Get allocated agent
	SELECT
		CLB.LeadId,
		I.FirstName + ' ' + I.Surname AS AgentName,
		ROW_NUMBER() OVER(PARTITION BY CLB.LeadId ORDER BY CLB.CreatedOn DESC) AS RowNumber
	INTO #AllocatedAgent
	FROM dbo.CampaignLeadBucket AS CLB WITH (NOLOCK)
	INNER JOIN dbo.[User] AS U WITH (NOLOCK)
		ON U.Id = CLB.AgentId
		AND U.IsDeleted = 0
	INNER JOIN dbo.UserIndividual AS UI WITH (NOLOCK)
		ON UI.UserId = U.Id
		AND UI.IsDeleted = 0
	INNER JOIN dbo.Individual AS I WITH (NOLOCK)
		ON I.PartyId = UI.IndividualId	
	WHERE CLB.LeadCallCentreCodeId IN (2, 32) --Lead Allocated, Lead Reallocated
		AND CLB.LeadId IN
		(
			SELECT
				CL.LeadId
			FROM #CreatedLeads AS CL
		)
		AND CLB.IsDeleted = 0;

	--Get all quoted leads
	SELECT DISTINCT
		LA.LeadId,
		1 AS Quoted
	INTO #QuotedLeads
	FROM dbo.LeadActivity AS LA WITH (NOLOCK)
	WHERE LA.ActivityTypeId = 4 --Quote Requested
		AND LA.LeadId IN
		(
			SELECT 
				CL.LeadId
			FROM #CreatedLeads AS CL
		)
		AND LA.IsDeleted = 0;

	--Get all sold leads
	SELECT
		LA.LeadId,
		LA.DateCreated AS DateOfSale,
		P.Name AS Product,
		PH.PolicyNo,
		Q.Fees + SUM(QI.Premium) + SUM(QI.Sasria) AS SoldPolicyPremium,
		1 AS Sale
	INTO #AcceptedQuotes
	FROM dbo.LeadActivity AS LA WITH (NOLOCK)
	INNER JOIN dbo.QuoteAcceptedLeadActivity AS QALA WITH (NOLOCK)
		ON QALA.LeadActivityId = LA.Id
	INNER JOIN dbo.Quote AS Q WITH (NOLOCK)
		ON Q.Id = QALA.QuoteId
		AND Q.IsDeleted = 0	
	INNER JOIN dbo.QuoteHeader AS QH WITH (NOLOCK)
		ON QH.Id = Q.QuoteHeaderId
		AND QH.IsRerated = 0
		AND QH.IsDeleted = 0	
	INNER JOIN dbo.Product AS P WITH (NOLOCK)
		ON P.Id = Q.ProductId	
		AND P.IsDeleted = 0
	INNER JOIN dbo.PolicyHeader AS PH WITH (NOLOCK)
		ON PH.QuoteId = Q.Id
		AND PH.IsDeleted = 0	
	INNER JOIN dbo.QuoteItem AS QI WITH (NOLOCK)
		ON QI.QuoteId = Q.Id
		AND QI.IsDeleted = 0	
	WHERE LA.ActivityTypeId = 5 --Quote Accepted
		AND LA.LeadId IN
		(
			SELECT 
				CL.LeadId
			FROM #CreatedLeads AS CL
		)
		AND LA.IsDeleted = 0
	GROUP BY LA.LeadId,
		LA.DateCreated,
		P.Name,
		PH.PolicyNo,
		Q.Fees;

	SELECT
		IL.LeadSystem,
		CL.Channel,
		CL.Campaign,
		IL.LeadSource,
		IL.LeadPassedBy,
		AA.AgentName,
		CL.DateLeadImported,
		AQ.DateOfSale,
		CL.LeadId AS LeadReference,
		AQ.Product,
		AQ.PolicyNo,
		CL.Client,
		CL.IDNumber,
		CL.CellNumber,
		AQ.SoldPolicyPremium,
		IL.Asset,
		IL.LeadSystem AS LeadType, -- This will be the campaign linked to the lead import
		QL.Quoted,
		AQ.Sale
	FROM #CreatedLeads AS CL
	LEFT JOIN #ImportedLeads AS IL
		ON IL.LeadId = CL.LeadId
	LEFT JOIN #QuotedLeads AS QL
		ON QL.LeadId = CL.LeadId
	LEFT JOIN #AcceptedQuotes AS AQ
		ON AQ.LeadId = CL.LeadId
	LEFT JOIN #AllocatedAgent AS AA
		ON AA.LeadId = CL.LeadId
		AND AA.RowNumber = 1
	ORDER BY CL.LeadId DESC

END