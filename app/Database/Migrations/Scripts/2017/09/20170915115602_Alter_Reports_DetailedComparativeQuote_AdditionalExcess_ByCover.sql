﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_AdditionalExcess_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure [dbo].[Reports_DetailedComparativeQuote_AdditionalExcess_ByCover]
	end
go

CREATE procedure [dbo].[Reports_DetailedComparativeQuote_AdditionalExcess_ByCover]
(
	@ProposalHeaderId int,
	@CoverDefinitionId int
)
as
	select
	DISTINCT
		ProductAdditionalExcess.Id,
		ProductAdditionalExcess.[Index] 'Index',
		ProductAdditionalExcess.Category,
		ProductAdditionalExcess.[Description] 'Description',
		ProductAdditionalExcess.ActualExcess,
		ProductAdditionalExcess.MinExcess,
		ProductAdditionalExcess.MaxExcess,
		ProductAdditionalExcess.Percentage,
		ProductAdditionalExcess.IsPercentageOfClaim,
		ProductAdditionalExcess.IsPercentageOfItem,
		ProductAdditionalExcess.IsPercentageOfSumInsured,
		ProductAdditionalExcess.ShouldApplySelectedExcess,
		ProductAdditionalExcess.ShouldDisplayExcessValues
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
		inner join CoverDefinition on CoverDefinition.CoverId = md.Cover.Id
		inner join ProductAdditionalExcess on ProductAdditionalExcess.CoverDefinitionId = CoverDefinition.Id
	where
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.IsDeleted = 0 and
		CoverDefinition.Id = @CoverDefinitionId
	order by
		ProductAdditionalExcess.[Index] asc

GO