﻿
Declare @productid int, @id int, @coverDefId int

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode = 'MW')

if (@productid IS NULL)
BEGIN

Return
 END
 ELSE
 BEGIN
	SET @id = @productid
 END


declare @CoverDefinitionId int
--motor

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = 218) -- 218 is MOTOR

IF (@coverDefId IS NULL)
BEGIN
return
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=234 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Limited to Glasses', @CoverDefinitionId, 234, NULL, 1, 19, 0, 0, 0,
               N'Does the driver have any restrictions to wear glasses while driving?', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=1426 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Car Hire Duration', @CoverDefinitionId, 1426, NULL, 1, 20, 0, 1, 0,
               N'Does the client require car hire?', N'12060' , N'', 0, NULL)
              end

---Map To Multiquote

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = 218
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = 218   



if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = 234 )
INSERT into MapQuestionDefinition(ParentId,ChildId) SELECT ParentQuestion.Id, ChildQuestion.Id
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId =1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 234) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = 1426 )
INSERT into MapQuestionDefinition(ParentId,ChildId) SELECT ParentQuestion.Id, ChildQuestion.Id
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId =1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 1426) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id   
