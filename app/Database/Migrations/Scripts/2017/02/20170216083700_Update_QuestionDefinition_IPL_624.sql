Update QuestionDefinition Set DisplayName = 'Group B - Manual - Ford Figo AMB/Polo Vivo/Similar (30 Days)', Tooltip = 'Group B - Manual - Ford Figo AMB/Polo Vivo/Similar (30 Days)' WHERE QuestionId = 2009
Update QuestionDefinition Set DisplayName = 'Standard Flat excess of R3500 (Santam and Zurich)' , Tooltip = 'Standard Flat excess of R3500 (Santam and Zurich)' WHERE QuestionId = 2010
Update QuestionDefinition Set DisplayName = 'Standard Flat excess of R3000 (Zurich)' , Tooltip = 'Standard Flat excess of R3000 (Zurich)' WHERE QuestionId = 2011
Update QuestionDefinition Set DisplayName = 'XSSure Premium' , Tooltip = 'XSSure Premium' WHERE QuestionId = 2012

--Add Xsure Organization
DECLARE @ContactDetailId int, @PartyId int;

INSERT INTO ContactDetail (Work, Direct, Cell, Home, Fax, Email, Website, IsDeleted) Values ('0860018140', null, null, null, '0865876630', 'info@xssure.co.za', 'http://xssure.co.za/', 0)

SELECT @ContactDetailId = SCOPE_IDENTITY();

INSERT INTO Party (MasterId, PartyTypeId, DisplayName, ChannelId, ContactDetailId, IsDeleted) VALUES (0, 2, 'X''Sure(Pty) Ltd',7, @ContactDetailId, 0);

SELECT @PartyId = SCOPE_IDENTITY();

INSERT INTO Organization (PartyId, Code ,RegisteredName, TradingName, TradingSince, [Description], FspNo)
VALUES (@PartyId, 'XSURE','X''Sure(Pty) Ltd', 'X''Sure', '1940-01-01', 'X�S Sure (Pty) Ltd underwrites and markets a variety of innovative short-term insurance add-on products both independently and in association with a variety of product and financial service providers. The following value added motor vehicle products can be tailored to suit specific requirements', '');

INSERT INTO [Address](PartyId, [MasterId], [Description],  Line1, Line2, Line3, Line4, Code, Longitude, Latitude, StateProvinceId, AddressTypeId, IsDefault, DateFrom, IsDeleted )
VALUES (@PartyId,0, '187 Gouws Avenue, Centurion, CENTURION RES COUNTR, 0197', '187 Gouws Avenue', '', 'Centurion','CENTURION RES COUNTR' ,'0197','-25,846995','28,130136','5' ,'1', 1, '1940-01-01', 0)

--Add Xsure Product

INSERT INTO Product(MasterId, Name, ProductOwnerId, ProductProviderId, ProductTypeId, AgencyNumber, ProductCode, StartDate, EndDate, RiskItemTypeId, IsDeleted, ImageName, Audit, QuoteExpiration, IsSelectedExcess, IsVoluntaryExcess )
VALUES (0, 'XSure', @PartyId, @PartyId, 13, '123456', 'XSURE', '2017-02-14 00:00:00.000', '2022-02-14 00:00:00.000', null, 0, 'xsure.png', null, 30, 0,0)
