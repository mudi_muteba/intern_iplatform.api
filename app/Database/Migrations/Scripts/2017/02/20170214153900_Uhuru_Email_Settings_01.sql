declare @ChannelID int

select top 1
	@ChannelID = Id
from Channel
where
	Code = 'UHU'

if isnull(@ChannelID, 0) > 0
	begin
		if not exists (select ChannelId from EmailCommunicationSetting where ChannelId = @ChannelID)
			begin
				insert into EmailCommunicationSetting (Host, Port, UseSSL, UseDefaultCredentials, UserName, [Password], DefaultFrom, DefaultBCC, DefaultContactNumber, SubAccountId, IsDeleted, ChannelId)
				values
					(
						'hound.aserv.co.za',
						'465',
						1,
						0,
						'lungileziqubu0@gmail.com',
						'0787323650',
						'iPlatform<iplatform@platform.co.za>',
						'',
						'0746499670',
						'QuoteAcceptance',
						0,
						@ChannelID
					)
			end
		else
			begin
				update EmailCommunicationSetting
				set
					Host = 'hound.aserv.co.za',
					Port = '465',
					UseSSL = 1,
					UseDefaultCredentials = 0,
					UserName = 'lungileziqubu0@gmail.com',
					[Password] = '0787323650',
					DefaultFrom = 'sifisok@ufsbrokers.co.za',
					DefaultBCC = '',
					DefaultContactNumber = '0746499670',
					SubAccountId = 'QuoteAcceptance',
					IsDeleted = 0
				where
					ChannelId = @ChannelID
			end
	end
go