Declare @CoverDefId int, @ProductId int;

-- Create Customer Loyalty Consultants Organization
DECLARE @ContactDetailId int, @PartyId int;
INSERT INTO ContactDetail (Work, Direct, Cell, Home, Fax, Email, Website, IsDeleted) Values ('0861 22 22 52', null, null, null, '086 750 3245', 'info@customerloyalty.co.za', 'http://psglifestyle.lmsystem.co.za/', 0)
SELECT @ContactDetailId = SCOPE_IDENTITY();
INSERT INTO Party (MasterId, PartyTypeId, DisplayName, ChannelId, ContactDetailId, IsDeleted) VALUES (0, 2, 'Customer Loyalty Consultants',7, @ContactDetailId, 0);
SELECT @PartyId = SCOPE_IDENTITY();
INSERT INTO Organization (PartyId, Code ,RegisteredName, TradingName, TradingSince, [Description], FspNo)
VALUES (@PartyId, 'CLC','Realcheck CC', 'Customer Loyalty Consultants', '1940-01-01', 'Customer Loyalty Consultants was founded in 1998 by Karla Hunt to provide consulting services on customer loyalty programmes for many diverse market segments including medical aid, motor and insurance.', '26908');
INSERT INTO [Address](PartyId, [MasterId], [Description],  Line1, Line2, Line3, Line4, Code, Longitude, Latitude, StateProvinceId, AddressTypeId, IsDefault, DateFrom, IsDeleted )
VALUES (@PartyId,0, 'Building 23, 5 Bauhinia Street, Pretoria, CENTURION, 0046', '5 Bauhinia Street', '', 'Pretoria','CENTURION' ,'0046','-25,8811331','28,18305','5' ,'1', 1, '1940-01-01', 0)
--Add Rodel Assist Product
Update Product Set Name = 'Rodel Assist - CLC',  ProductCode = 'RODELASSIST', ImageName = 'clc.png'WHERE ProductCode = 'AUTOVAP';

-- Create Car Gage Hire Organization
INSERT INTO ContactDetail (Work, Direct, Cell, Home, Fax, Email, Website, IsDeleted) Values ('011 282 3400', null, null, null, '011 783 1026', 'info@gagecarhire.co.za', 'http://psglifestyle.lmsystem.co.za/', 0)
SELECT @ContactDetailId = SCOPE_IDENTITY();
INSERT INTO Party (MasterId, PartyTypeId, DisplayName, ChannelId, ContactDetailId, IsDeleted) VALUES (0, 2, 'Gage Car Hire',7, @ContactDetailId, 0);
SELECT @PartyId = SCOPE_IDENTITY();
INSERT INTO Organization (PartyId, Code ,RegisteredName, TradingName, TradingSince, [Description], FspNo)
VALUES (@PartyId, 'GCH','Gage Car Hire', 'Gage Car Hire', '1940-01-01', 'Gage Car Hire � Insurance replacement specialist', '');
INSERT INTO [Address](PartyId, [MasterId], [Description],  Line1, Line2, Line3, Line4, Code, Longitude, Latitude, StateProvinceId, AddressTypeId, IsDefault, DateFrom, IsDeleted )
VALUES (@PartyId,0, '1st Floor Tropicana house, 1 Palmboon Place Athol, Sandton, ATHOLL, 2196', '1st Floor Tropicana house', '1 Palmboon Place', 'Sandton','ATHOLL' ,'2196','-26,1165512','28,0669078999999','5' ,'1', 1, '1940-01-01', 0)
--Add Gage Car Hire Product
INSERT INTO Product(MasterId, Name, ProductOwnerId, ProductProviderId, ProductTypeId, AgencyNumber, ProductCode, StartDate, EndDate, RiskItemTypeId, IsDeleted, ImageName, Audit, QuoteExpiration, IsSelectedExcess, IsVoluntaryExcess )
VALUES (0, 'Rodel Personal Lines 30 Day', @PartyId, @PartyId, 13, '123456', 'RPL30DAY', '2017-02-14 00:00:00.000', '2022-02-14 00:00:00.000', null, 0, 'gch.png', null, 30, 0,0)


---- Cover Definition
--Rodel Assist Cover
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'RODELASSIST';
if @ProductId >0
BEGIN
	SELECT @CoverDefId = Id FROM Coverdefinition WHERE CoverId = 377;
	Update Coverdefinition Set DisplayName = 'Rodel Assist', @ProductId = @ProductId WHERE Id = @CoverDefId;
	Update QuestionDefinition Set CoverDefinitionId = @CoverDefId WHERE QuestionId = 2008
END

--Car Gage Hire Cover
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'RPL30DAY';
if @ProductId >0
BEGIN
	INSERT INTO CoverDefinition(MasterId, DisplayName, ProductId, CoverId, ParentId, CoverDefinitionTypeId, VisibleIndex, IsDeleted)
	Values(0, 'Group B - Manual - Ford Figo AMB/Polo Vivo/Similar (30 Days)', @ProductId, 377, null, 4, 0,0);
	SELECT @CoverDefId = SCOPE_IDENTITY();
	Update QuestionDefinition Set CoverDefinitionId = @CoverDefId WHERE QuestionId = 2009
END

-- XSure
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'XSURE';
if @ProductId >0
BEGIN
	INSERT INTO CoverDefinition(MasterId, DisplayName, ProductId, CoverId, ParentId, CoverDefinitionTypeId, VisibleIndex, IsDeleted)
	Values(0, 'Standard Flat excess of R3500 (Santam and Zurich)', @ProductId, 377, null, 4, 0,0);
	SELECT @CoverDefId = SCOPE_IDENTITY();
	Update QuestionDefinition Set CoverDefinitionId = @CoverDefId WHERE QuestionId = 2010

	INSERT INTO CoverDefinition(MasterId, DisplayName, ProductId, CoverId, ParentId, CoverDefinitionTypeId, VisibleIndex, IsDeleted)
	Values(0, 'Standard Flat excess of R3000 (Zurich)', @ProductId, 377, null, 4, 0,0);
	SELECT @CoverDefId = SCOPE_IDENTITY();
	Update QuestionDefinition Set CoverDefinitionId = @CoverDefId WHERE QuestionId = 2011

	INSERT INTO CoverDefinition(MasterId, DisplayName, ProductId, CoverId, ParentId, CoverDefinitionTypeId, VisibleIndex, IsDeleted)
	Values(0, 'XSSure Premium', @ProductId, 377, null, 4, 0,0);
	SELECT @CoverDefId = SCOPE_IDENTITY();
	Update QuestionDefinition Set CoverDefinitionId = @CoverDefId WHERE QuestionId = 2012
END

-- make those questions not compulsory
Update QuestionDefinition set RequiredForQuote = 0 WHERE Questionid in (2008,2009,2010,2011,2012)
