﻿DECLARE @Id int;

IF NOT EXISTS(SELECT * FROM ChannelEvent WHERE ChannelId = 20 AND ProductCode = 'CENTND' AND EventName = 'ExternalQuoteAcceptedWithUnderwritingEnabledEvent')
BEGIN
	INSERT INTO ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode) VALUES ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', 20,0, 'CENTND');
	SELECT @Id = SCOPE_IDENTITY()
	INSERT INTO ChannelEventTask (TaskName, ChannelEventId, IsDeleted) VALUES  (2, @Id, 0);
END

IF NOT EXISTS(SELECT * FROM ChannelEvent WHERE ChannelId = 20 AND ProductCode = 'CENTRD' AND EventName = 'ExternalQuoteAcceptedWithUnderwritingEnabledEvent')
BEGIN
INSERT INTO ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode) VALUES ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', 20,0, 'CENTRD');
SELECT @Id = SCOPE_IDENTITY()
INSERT INTO ChannelEventTask (TaskName, ChannelEventId, IsDeleted) VALUES  (2, @Id, 0);
END

IF NOT EXISTS(SELECT * FROM ChannelEvent WHERE ChannelId = 20 AND ProductCode = 'SNTMND' AND EventName = 'ExternalQuoteAcceptedWithUnderwritingEnabledEvent')
BEGIN
INSERT INTO ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode) VALUES ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', 20,0, 'SNTMND');
SELECT @Id = SCOPE_IDENTITY()
INSERT INTO ChannelEventTask (TaskName, ChannelEventId, IsDeleted) VALUES  (2, @Id, 0);
END

IF NOT EXISTS(SELECT * FROM ChannelEvent WHERE ChannelId = 20 AND ProductCode = 'SNTMRD' AND EventName = 'ExternalQuoteAcceptedWithUnderwritingEnabledEvent')
BEGIN
INSERT INTO ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode) VALUES ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', 20,0, 'SNTMRD');
SELECT @Id = SCOPE_IDENTITY()
INSERT INTO ChannelEventTask (TaskName, ChannelEventId, IsDeleted) VALUES  (2, @Id, 0);
END

IF NOT EXISTS(SELECT * FROM ChannelEvent WHERE ChannelId = 20 AND ProductCode = 'KPIPERS2' AND EventName = 'ExternalQuoteAcceptedWithUnderwritingEnabledEvent')
BEGIN
INSERT INTO ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode) VALUES ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', 20,0, 'KPIPERS2');
SELECT @Id = SCOPE_IDENTITY()
INSERT INTO ChannelEventTask (TaskName, ChannelEventId, IsDeleted) VALUES  (2, @Id, 0);
END

IF NOT EXISTS(SELECT * FROM ChannelEvent WHERE ChannelId = 20 AND ProductCode = 'AAC' AND EventName = 'ExternalQuoteAcceptedWithUnderwritingEnabledEvent')
BEGIN
	INSERT INTO ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode) VALUES ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', 20,0, 'AAC');
	SELECT @Id = SCOPE_IDENTITY()
	INSERT INTO ChannelEventTask (TaskName, ChannelEventId, IsDeleted) VALUES  (2, @Id, 0);
END