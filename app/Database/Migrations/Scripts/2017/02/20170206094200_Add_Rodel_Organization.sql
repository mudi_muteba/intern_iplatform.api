Update Channel Set ExternalReference = Code
Update [User] Set ExternalReference = UserName

--Add Rodel Organization
DECLARE @ContactDetailId int, @ChannelId int, @PartyId int;

SELECT @ChannelId = Id FROM Channel WHERE Code = 'RODEL' AND IsDeleted != 1


INSERT INTO ContactDetail (Work, Direct, Cell, Home, Fax, Email, Website, IsDeleted) Values ('0313037722', null, null, null, '0866247017', 'info@rodel.co.za', 'https://www.rodel.co.za/', 0)

SELECT @ContactDetailId = SCOPE_IDENTITY();

INSERT INTO Party (MasterId, PartyTypeId, DisplayName, ChannelId, ContactDetailId, IsDeleted) VALUES (0, 2, 'Rodel Financial Services (Pty) Ltd',@ChannelId, @ContactDetailId, 0);

SELECT @PartyId = SCOPE_IDENTITY();

INSERT INTO Organization (PartyId, Code ,RegisteredName, TradingName, TradingSince, [Description], FspNo)
VALUES (@PartyId, 'RODEL','Rodel Financial Services (Pty) Ltd', 'Rodel Financial Services', '1940-01-01', 'Rodel has been in the bridging finance industry since 2000 and we still love what we do. We strive each day to create innovative solutions to help our clients� access their money locked in a property deal. After more than 85,000 transactions we have learned the hard way that success is a marathon not a sprint, and only achieved through consistently doing your best each day.', '');

INSERT INTO [Address](PartyId, [MasterId], [Description],  Line1, Line2, Line3, Line4, Code, Longitude, Latitude, StateProvinceId, AddressTypeId, IsDefault, DateFrom, IsDeleted )
VALUES (@PartyId,0, 'The Manor House, 14 Nuttall Gardens, Morningside, Durban, 4001', 'The Manor House', '14 Nuttall Gardens', 'Berea','MORNINGSIDE' ,'4001','-29,8272268','31,0128466','5' ,'1', 1, '1940-01-01', 0)


Update Channel Set OrganizationId = @PartyId WHERE Id = @ChannelId;
