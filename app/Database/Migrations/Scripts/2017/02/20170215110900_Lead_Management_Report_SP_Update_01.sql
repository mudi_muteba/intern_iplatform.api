if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_LeadManagement_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_LeadManagement_Header
	end
go

create procedure Report_CallCentre_LeadManagement_Header
(
	@CampaignID int
)
as
	select top 1 Code Insurer, '' Color from Channel where IsDefault = 1
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_LeadManagement_GetAgentLeadStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_LeadManagement_GetAgentLeadStatistics
	end
go

create procedure Report_CallCentre_LeadManagement_GetAgentLeadStatistics
(
	@CampaignIDs varchar(255),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
			as 
			(
				select distinct
					[User].Id,
					Campaign.Id,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				from Campaign
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					Campaign.Id in (select * from fn_StringListToTable(@CampaignIds)) and
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo
				group by
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName,
					Campaign.Name
				)
			select
				Agent,
				Campaign,
				(
					select
						isnull(count(distinct(CampaignLeadBucket.LeadId)), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						CampaignLeadBucket.IsDeleted = 0 and
						AgentId = Agents_CTE.AgentId
				) Received,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						CampaignLeadBucket.IsDeleted = 0 and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 23, 24, 25, 26, 27, 28, 29, 30, 31)
				) Contacted,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						CampaignLeadBucket.IsDeleted = 0 and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (15, 16, 17, 18, 19, 20, 21, 22)
				) Uncontactable,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						CampaignLeadBucket.IsDeleted = 0 and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (7, 10, 9, 11, 20, 21)
				) Pending,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						CampaignLeadBucket.IsDeleted = 0 and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (5)
				) Completed,
				(
					select
						isnull(count(distinct(CampaignLeadBucket.LeadId)), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (2)
				) Unactioned
			from
				Agents_CTE
			order by
				Agent asc,
				Campaign Asc
		end
	else
		begin
			;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
			as 
			(
				select distinct
					[User].Id,
					Campaign.Id,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				from Campaign
					inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
					inner join [User] on [User].Id = CampaignLeadBucket.AgentId
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo
				group by
					Campaign.Id,
					Campaign.Name,
					[User].Id,
					Individual.PartyID,
					Individual.Surname,
					Individual.FirstName,
					Campaign.Name
				)
			select
				Agent,
				Campaign,
				(
					select
						isnull(count(distinct(CampaignLeadBucket.LeadId)), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						CampaignLeadBucket.IsDeleted = 0 and
						AgentId = Agents_CTE.AgentId
				) Received,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						CampaignLeadBucket.IsDeleted = 0 and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 23, 24, 25, 26, 27, 28, 29, 30, 31)
				) Contacted,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						CampaignLeadBucket.IsDeleted = 0 and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (15, 16, 17, 18, 19, 20, 21, 22)
				) Uncontactable,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						CampaignLeadBucket.IsDeleted = 0 and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (7, 10, 9, 11, 20, 21)
				) Pending,
				(
					select
						isnull(count(CampaignLeadBucket.Id), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						CampaignLeadBucket.IsDeleted = 0 and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (5)
				) Completed,
				(
					select
						isnull(count(distinct(CampaignLeadBucket.LeadId)), 0)
					from CampaignLeadBucket
					where
						CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
						CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
						CampaignLeadBucket.IsDeleted = 0 and
						AgentId = Agents_CTE.AgentId and
						LeadCallCentreCodeId in (2)
				) Unactioned
			from
				Agents_CTE
			order by
				Agent asc,
				Campaign Asc
		end
go