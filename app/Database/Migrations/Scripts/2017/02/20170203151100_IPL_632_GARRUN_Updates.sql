Declare @ProductId int, @OrgId int;

SELECT Top 1 @ProductId = Id FROM Product WHERE ProductCode = 'GARRUN' AND Isdeleted != 1
SELECT Top 1 @OrgId = o.PartyId FROM Organization o INNER JOIN Party p on p.Id = o.partyId WHERE Code = 'GARRUN' AND p.IsDeleted != 1

IF (@ProductId > 0 AND @OrgId > 0)
BEGIN
	--1. Update Id Number(Supporting Document) to Copy ID Document in Motor Theft
	Update ProductClaimsQuestionDefinition Set DisplayName = 'Copy ID Document' WHERE Id in(
	SELECT pcqd.Id FROM ProductClaimsQuestionDefinition pcqd
			INNER JOIN md.ClaimsQuestionDefinition cqd on cqd.id = pcqd.ClaimsQuestionDefinitionId
			INNER JOIN md.ClaimsType ct on ct.id = cqd.ClaimsTypeId
			INNER JOIN md.ClaimsQuestion cq on cq.Id = cqd.ClaimsQuestionId
			INNER JOIN md.ClaimsQuestionType cqt on cqt.Id = cq.ClaimsQuestionTypeId
			WHERE ProductId = @ProductId
			AND cqd.Id = 204
			AND pcqd.IsDeleted != 1)

	--2. Remove SAPS Reference Number (Supporting Document) in Motor Theft
	Update ProductClaimsQuestionDefinition Set IsDeleted = 1 WHERE Id in(
	SELECT pcqd.Id FROM ProductClaimsQuestionDefinition pcqd
			INNER JOIN md.ClaimsQuestionDefinition cqd on cqd.id = pcqd.ClaimsQuestionDefinitionId
			INNER JOIN md.ClaimsType ct on ct.id = cqd.ClaimsTypeId
			INNER JOIN md.ClaimsQuestion cq on cq.Id = cqd.ClaimsQuestionId
			INNER JOIN md.ClaimsQuestionType cqt on cqt.Id = cq.ClaimsQuestionTypeId
			WHERE ProductId = @ProductId 
			AND cqd.Id = 205
			AND pcqd.IsDeleted != 1)

	Update OrganizationClaimTypeCategory Set CategoryDisplayName = 'Personal Items' WHERE OrganizationId = @OrgId AND ClaimsTypeCategoryId = 1
	Update OrganizationClaimTypeCategory Set CategoryDisplayName = 'Content & Building' WHERE OrganizationId = @OrgId AND ClaimsTypeCategoryId = 3
	Update OrganizationClaimTypeCategory Set ClaimTypeDisplayName = 'Home Burgled' WHERE OrganizationId = @OrgId AND ClaimsTypeId = 37
END


