﻿if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Summary') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Summary
	end
go

create procedure Reports_DetailedComparativeQuote_Summary
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50)
)
as
	;with summary_CTE(ProposalHeaderId, QuoteHeaderId, OrganizationId, ProductId, QuoteId)
	as
	(
		select distinct
			ProposalHeader.Id,
			QuoteHeader.Id, 
			Organization.PartyId,
			Product.Id,
			Quote.Id
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
			inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			ProposalHeader.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
			Quote.IsDeleted = 0 and
			CoverDefinition.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId
		group by
			ProposalHeader.Id,
			QuoteHeader.Id, 
			Organization.PartyId,
			Product.Id,
			Quote.Id
	)
	select
		(
			select
				Organization.TradingName
			from Organization
			where
				Organization.PartyId = summary_CTE.OrganizationId
		) Insurer,
		(
			select
				Organization.Code
			from Organization
			where
				Organization.PartyId = summary_CTE.OrganizationId
		) Code,
		(
			select
				Product.[Name]
			from Product
			where
				Product.Id = summary_CTE.ProductId
		) Product,
		(
			select
				sum(cast((QuoteItem.ExcessBasic) as numeric(18, 2)))
			from Quote
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			where
				Quote.Id = summary_CTE.QuoteId
		) BasicExcess,
		(
			select
				sum(cast((QuoteItem.Premium) as numeric(18, 2)))
			from Quote
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			where
				Quote.Id = summary_CTE.QuoteId
		) Premium,
		(
			select
				sum(cast((QuoteItem.SASRIA) as numeric(18, 2)))
			from Quote
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			where
				Quote.Id = summary_CTE.QuoteId
		) SASRIA,
		(
			select
				sum(cast((Quote.Fees) as numeric(18, 2)))
			from Quote
			where
				Quote.Id = summary_CTE.QuoteId
		) Fees,
		(
			(
				select
					sum(cast((QuoteItem.Premium) as numeric(18, 2))) + sum(cast((QuoteItem.SASRIA) as numeric(18, 2)))
				from Quote
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				where
					Quote.Id = summary_CTE.QuoteId
			)

			+

			(
				select
					sum(cast((Quote.Fees) as numeric(18, 2)))
				from Quote
				where
					Quote.Id = summary_CTE.QuoteId
			)
		) Total,
		(
			select
				cast(isnull(sum(Commission), 0) as numeric(18, 2))
			from
			(
				select
					case
						when md.Cover.name = 'All Risk' then sum(cast((QuoteItem.Premium * cast(20 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
						when md.Cover.name = 'Building' then sum(cast((QuoteItem.Premium * cast(20 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
						when md.Cover.name = 'Caravan Or Trailer' then sum(cast((QuoteItem.Premium * cast(12.5 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
						when md.Cover.name = 'Contents' then sum(cast((QuoteItem.Premium * cast(20 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
						when md.Cover.name = 'Motor' then sum(cast((QuoteItem.Premium * cast(12.5 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
						when md.Cover.name = 'Personal Legal Liability' then sum(cast((QuoteItem.Premium * cast(20 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
						else 0
					end Commission
				from Quote
					inner join Product on Product.Id = Quote.ProductId
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId			
				where
					Quote.Id = summary_CTE.QuoteId and
					Product.Id = summary_CTE.ProductId
				group by
					quote.id,
					md.Cover.name
			) x
		) Commission
	from summary_CTE


	--select
	--	Organization.TradingName Insurer,
	--	Organization.Code Code,
	--	Product.Name, -- + ' ' + md.Cover.name Product,
	--	sum(QuoteItem.ExcessBasic) BasicExcess,
	--	sum(QuoteItem.Premium) Premium,
	--	sum(QuoteItem.SASRIA) SASRIA,
	--	sum(Quote.Fees) Fees,
	--	sum(cast((QuoteItem.Premium) as numeric(18, 2))) Total,
	--	case
	--		when md.Cover.name = 'All Risk' then sum(cast((QuoteItem.Premium * cast(20 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
	--		when md.Cover.name = 'Building' then sum(cast((QuoteItem.Premium * cast(20 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
	--		when md.Cover.name = 'Caravan Or Trailer' then sum(cast((QuoteItem.Premium * cast(12.5 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
	--		when md.Cover.name = 'Contents' then sum(cast((QuoteItem.Premium * cast(20 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
	--		when md.Cover.name = 'Motor' then sum(cast((QuoteItem.Premium * cast(12.5 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
	--		when md.Cover.name = 'Personal Legal Liability' then sum(cast((QuoteItem.Premium * cast(20 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
	--		else 0
	--	end Commission
	--from ProposalHeader
	--	inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
	--	inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
	--	inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
	--	inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
	--	inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
	--	inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
	--	inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
	--	inner join Product on Product.Id = Quote.ProductId
	--	inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
	--	inner join Organization on Organization.PartyId = Product.ProductOwnerId
	--where
	--	ProposalHeader.IsDeleted = 0 and
	--	QuoteHeader.IsRerated = 0 and
	--	Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
	--	Quote.IsDeleted = 0 and
	--	--QuoteState.Errors = 0 and
	--	QuoteItem.IsDeleted = 0 and
	--	CoverDefinition.IsDeleted = 0 and
	--	ProposalHeader.Id = @ProposalHeaderId and
	--	QuoteItem.Premium > 0
	--group by
	--	Organization.Code,
	--	Organization.TradingName,
	--	Product.Name,
	--	md.Cover.Name
	--order by
	--	Premium desc,
	--	Organization.TradingName asc
go