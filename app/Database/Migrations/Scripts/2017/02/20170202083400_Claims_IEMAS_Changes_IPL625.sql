
Declare @ProductId int;

SELECT Top 1 @ProductId = Id FROM Product WHERE ProductCode = 'IEMAS' AND Isdeleted != 1

IF (@ProductId > 0)
BEGIN

--1 Update Question Description to Where did the incident occur?
Update ProductClaimsQuestionDefinition Set DisplayName = 'Where did the incident occur?' WHERE Id IN(
	SELECT pcqd.Id FROM ProductClaimsQuestionDefinition pcqd
		INNER JOIN md.ClaimsQuestionDefinition cqd on cqd.id = pcqd.ClaimsQuestionDefinitionId
		INNER JOIN md.ClaimsType ct on ct.id = cqd.ClaimsTypeId
		WHERE ProductId = @ProductId AND ct.id =  1 AND ClaimsQuestionDefinitionId  = 165);

--2 Remove duplicate questions for Accidental damage
	DELETE FROM ProductClaimsQuestionDefinition WHERE Id in(
		SELECT pcqd.id FROM ProductClaimsQuestionDefinition pcqd
		INNER JOIN md.ClaimsQuestionDefinition cqd on cqd.id = pcqd.ClaimsQuestionDefinitionId
		INNER JOIN md.ClaimsType ct on ct.id = cqd.ClaimsTypeId
		WHERE ProductId = @ProductId AND ct.id = 36)

	INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,960, 1, 0, 1, 'Date of Incident', 1, 36);
	INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,961, 1, 0, 1, 'Time of Incident', 2, 36);
	INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,962, 1, 0, 1, 'Full Description of what happened', 3, 36);
	INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,963, 1, 0, 1, 'Where did it happen', 4, 36);
	INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,964, 1, 0, 1, 'What caused the damage', 5, 36);
	INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1044, 1, 0, 1, 'Description of Item damaged', 1, 24);

--3 Update all questions Type FromGallery to not required
	Update ProductClaimsQuestionDefinition Set [Required] = 0 WHERE Id in (SELECT pcqd.Id FROM ProductClaimsQuestionDefinition pcqd
		INNER JOIN md.ClaimsQuestionDefinition cqd on cqd.id = pcqd.ClaimsQuestionDefinitionId
		INNER JOIN md.ClaimsType ct on ct.id = cqd.ClaimsTypeId
		INNER JOIN md.ClaimsQuestion cq on cq.Id = cqd.ClaimsQuestionId
		INNER JOIN md.ClaimsQuestionType cqt on cqt.Id = cq.ClaimsQuestionTypeId
		WHERE ProductId = @ProductId 
		AND cq.ClaimsQuestionTypeId in (9)
		AND pcqd.IsDeleted != 1)

END
