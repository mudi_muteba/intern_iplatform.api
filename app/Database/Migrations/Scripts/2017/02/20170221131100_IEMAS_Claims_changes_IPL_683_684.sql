﻿DEClARE @ProductId int
SELECT @ProductId = Id FROM PRODUCT WHERE ProductCode = 'IEMAS' AND IsDeleted != 1;
IF @ProductId > 0
BEGIN
	INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) 
	VALUES 
	(@ProductId, 0,1180, 0, 1, 0, 'Driver''s Date of Birth', 4, 14),
	(@ProductId, 0,49, 0, 1, 0, 'Driver''s Licence Number', 5, 14),
	(@ProductId, 0,50, 0, 1, 0, 'Driver''s Licence Date Of First Issue', 6, 14),
	(@ProductId, 0,52, 0, 1, 0, 'Driver''s Licence Code', 7, 14),
	(@ProductId, 0,1181, 0, 1, 0, 'Driver''s License From Date', 8, 14),
	(@ProductId, 0,51, 0, 1, 0, 'Driver''s License Expiry Date', 9, 14);

	Update ProductClaimsQuestionDefinition Set ClaimsQuestionDefinitionId = 962 WHERE ClaimsQuestionDefinitionId in (964, 1044) AND ProductId = @ProductId;
END