﻿DECLARE @ProductId INT;
SET @ProductId = (SELECT Id FROM Product WHERE ProductCode = 'ZURPERS');

DECLARE @CoverId INT;
SET @CoverId = (SELECT Id FROM md.Cover WHERE Code = 'MOTOR');

DECLARE @CoverDefinitionId INT;
SET @CoverDefinitionId = (SELECT Id FROM CoverDefinition WHERE ProductId = @ProductId AND CoverId = @CoverId);

DECLARE @ExcessWaiverQuestionId INT = 24;

UPDATE QuestionDefinition SET DisplayName = 'Excess Waiver' WHERE QuestionId = @ExcessWaiverQuestionId AND CoverDefinitionId = @CoverDefinitionId;

UPDATE SettingsiRate SET AgentCode = '1616' WHERE ProductCode = 'ZURPERS';