DECLARE @OrgId int, @ProductId int, @CoverDefinitionId int;
SELECT @OrgId = PartyId FROM Organization WHERE Code = 'RODEL';

--Product vaps
INSERT INTO Product(MasterId, Name, ProductOwnerId, ProductProviderId, ProductTypeId, AgencyNumber, ProductCode, StartDate, EndDate, RiskItemTypeId, IsDeleted, ImageName, Audit, QuoteExpiration, IsSelectedExcess, IsVoluntaryExcess )
VALUES (0, 'Automatic Vaps', @OrgId, @OrgId, 13, '123456', 'AUTOVAP', '2017-02-14 00:00:00.000', '2022-02-14 00:00:00.000', null, 0, 'rodel.png', null, 30, 0,0)
SELECT @ProductId = SCOPE_IDENTITY();

INSERT INTO CoverDefinition(MasterId, DisplayName, ProductId, CoverId, ParentId, CoverDefinitionTypeId, VisibleIndex, IsDeleted)
Values(0, 'Automatic Vaps', @ProductId, 377, null, 4, 0,0);
SELECT @CoverDefinitionId = SCOPE_IDENTITY();

INSERT QuestionDefinition (MasterId, DisplayName, CoverDefinitionId, QuestionId, ParentId, QuestionDefinitionTypeId, VisibleIndex, RequiredForQuote, RatingFactor, [ReadOnly], ToolTip, DefaultValue, RegexPattern) 
VALUES 
(0, N'Rodel Assist', @CoverDefinitionId, 2008, NULL, 1, 1, 1, 1, 0, N'Rodel Assist', N'', N''),
(0, N'Group B�Manual-Ford Figo A and B, Polo Vivo, Similar 30', @CoverDefinitionId, 2009, NULL, 1, 1, 1, 1, 0, N'Group B � Manual - Ford Figo A and B, Polo Vivo, Similar 30', N'', N''),
(0, N'Zurich�R3500', @CoverDefinitionId, 2010, NULL, 1, 1, 1, 1, 0, N'Zurich�R3500', N'', N''),
(0, N'Zurich�R3000', @CoverDefinitionId, 2011, NULL, 1, 1, 1, 1, 0, N'Zurich-R3000', N'', N''),
(0, N'AIG-5% min R2500 max R7500', @CoverDefinitionId, 2012, NULL, 1, 1, 1, 1, 0, N'AIG�5% min R2500 max R7500', N'', N'')
