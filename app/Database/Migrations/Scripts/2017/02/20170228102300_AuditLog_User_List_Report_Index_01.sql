﻿if not exists (select * from sys.indexes where name = 'UserListReport_NonClusteredIndex_20170228_100905' AND object_id = object_id('AuditLog'))
    begin
		create nonclustered index [UserListReport_NonClusteredIndex_20170228_100905] on [AuditLog]
		(
			[EntityId] asc,
			[AuditEntryType] asc,
			[RequestDate] asc
		)
		with
		(
			pad_index = off,
			statistics_norecompute = off,
			sort_in_tempdb = off,
			drop_existing = off,
			online = off,
			allow_row_locks = on,
			allow_page_locks = on
		)
    end
go