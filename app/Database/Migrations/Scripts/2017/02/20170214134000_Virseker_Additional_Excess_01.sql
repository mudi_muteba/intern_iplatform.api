delete from ProductAdditionalExcess
dbcc checkident ('ProductAdditionalExcess', reseed, 0)

--KingPrice (Motor), Product Codes: 'KPIPERS', 'KPIPERS2'
declare
	@KingPrice_KPIPERS_Motor_CoverDefinitionId int,
	@KingPrice_KPIPERS2_Motor_CoverDefinitionId int
	
select
	@KingPrice_KPIPERS_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS') and
	Cover.Name in ('Motor')

select
	@KingPrice_KPIPERS2_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS2') and
	Cover.Name in ('Motor')

if	isnull(@KingPrice_KPIPERS_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 0, 'General', 'Basic Excess', 4500, 1500, 27500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 0, 'General', 'Basic Excess', 4500, 1500, 27500, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 1, 'General', 'Specified Ratio', 750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 1, 'General', 'Specified Ratio', 750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 2, 'General', 'Window Replacement (Excl. Panoramic Glass)', 750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 2, 'General', 'Window Replacement (Excl. Panoramic Glass)', 750, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 3, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'Is younger than 25 years OR has had their driver�s licence for less than 2 years', 4500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 3, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'Is younger than 25 years OR has had their driver�s licence for less than 2 years', 4500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 4, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'If the incident occurred outside of SA and the car is not drivable', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 4, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'If the incident occurred outside of SA and the car is not drivable', 7500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 5, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'When the incident occurs in the first 3 months of cover', 3500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 5, 'Additional Excess of the Incident Driver (Excl. Legal Spouse)', 'When the incident occurs in the first 3 months of cover', 3500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 6, 'Trailer & Caravans', 'Caravan or caravan contents', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 6, 'Trailer & Caravans', 'Caravan or caravan contents', 2500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 7, 'Trailer & Caravans', 'Caravan or trailer contents', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 7, 'Trailer & Caravans', 'Caravan or trailer contents', 2500, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Motor_CoverDefinitionId, 8, 'Trailer & Caravans', 'Liability to other parties', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Motor_CoverDefinitionId, 8, 'Trailer & Caravans', 'Liability to other parties', 0, 0, 0, 0, 0, 0, 0, 0)
	end


--KingPrice (Building), Product Codes: 'KPIPERS', 'KPIPERS2'
declare
	@KingPrice_KPIPERS_Building_CoverDefinitionId int,
	@KingPrice_KPIPERS2_Building_CoverDefinitionId int
	
select
	@KingPrice_KPIPERS_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS') and
	Cover.Name in ('Building')

select
	@KingPrice_KPIPERS2_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS2') and
	Cover.Name in ('Building')

if	isnull(@KingPrice_KPIPERS_Building_CoverDefinitionId, 0) > 0 and
	isnull(@KingPrice_KPIPERS2_Building_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Building_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Building_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)
	end

--KingPrice (Contents), Product Codes: 'KPIPERS', 'KPIPERS2'
declare
	@KingPrice_KPIPERS_Contents_CoverDefinitionId int,
	@KingPrice_KPIPERS2_Contents_CoverDefinitionId int
	
select
	@KingPrice_KPIPERS_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS') and
	Cover.Name in ('Contents')

select
	@KingPrice_KPIPERS2_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('KPIPERS2') and
	Cover.Name in ('Contents')

if	isnull(@KingPrice_KPIPERS_Contents_CoverDefinitionId, 0) > 0 and
	isnull(@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 0, 'General', 'Basic Excess', 0, 1500, 4500, 5, 0, 1, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 1, 'Additional Cover Excess', 'Food that has deteriorated', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 1, 'Additional Cover Excess', 'Food that has deteriorated', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 2, 'Additional Cover Excess', 'Washing stolen from the line', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 2, 'Additional Cover Excess', 'Washing stolen from the line', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 3, 'Additional Cover Excess', 'Guests belongings', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 3, 'Additional Cover Excess', 'Guests belongings', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 4, 'Additional Cover Excess', 'Locks & keys', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 4, 'Additional Cover Excess', 'Locks & keys', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 5, 'Additional Cover Excess', 'Hole in one in golf/full house in bowling', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 5, 'Additional Cover Excess', 'Hole in one in golf/full house in bowling', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 6, 'Additional Cover Excess', 'Domestic employee�s belongings', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 6, 'Additional Cover Excess', 'Domestic employee�s belongings', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 7, 'Additional Cover Excess', 'Rent to live elsewhere', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 7, 'Additional Cover Excess', 'Rent to live elsewhere', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 8, 'Additional Cover Excess', 'Garden & leisure equipment', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 8, 'Additional Cover Excess', 'Garden & leisure equipment', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 9, 'Additional Cover Excess', 'Veterinary expense', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 9, 'Additional Cover Excess', 'Veterinary expense', 400, 0, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS_Contents_CoverDefinitionId, 10, 'Portable Posessions', 'Unspecified items (per item)', 400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@KingPrice_KPIPERS2_Contents_CoverDefinitionId, 10, 'Portable Posessions', 'Specified items', 500, 0, 0, 5, 0, 0, 0, 0)
	end


--SA Underwriters (Motor), Product Codes: 'CENTND', 'CENTRD', 'SNTMND', 'SNTMRD'
declare
	@SAUnderwriters_CENTND_Motor_CoverDefinitionId int,
	@SAUnderwriters_CENTRD_Motor_CoverDefinitionId int,
	@SAUnderwriters_SNTMND_Motor_CoverDefinitionId int,
	@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId int
	
select
	@SAUnderwriters_CENTND_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('CENTND') and
	Cover.Name in ('Motor')

select
	@SAUnderwriters_CENTRD_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('CENTRD') and
	Cover.Name in ('Motor')

select
	@SAUnderwriters_SNTMND_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('SNTMND') and
	Cover.Name in ('Motor')

select
	@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('SNTMRD') and
	Cover.Name in ('Motor')

if	isnull(@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 0, 'Standard', 'Accident and Other Claims : Private Use', 0, 2000, 0, 3.5, 0, 0, 1, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 1, 'Standard', 'Accident and Other Claims: Business Use', 0, 4000, 0, 5, 0, 0, 1, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 2, 'Standard', 'Theft / Hijack Claims: For vehicles without approved tracking device protection', 0, 2500, 0, 10, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 3, 'Standard', 'Theft / Hi-Jacking: For all vehicles without approved tracking device protection. LDV''s of 2500cc and over, and all Minibusses and Panelvans of 2000cc and over', 0, 5000, 0, 10, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 4, 'Standard', ' Windscreen / Fixed Glass', 0, 500, 0, 20, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 5, 'Additional', 'If the driver of the vehicle at the time of loss is under 30 and is noted as a driver on the policy', 0, 3000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 6, 'Additional', 'If the driver of the vehicle at the time of loss has a licence other than Code 08/ B / EB', 0, 3000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 7, 'Additional', 'If the claim is within the first 12 months of inception of the policy where the driver had no or interrupted previous insurance at inception', 0, 3000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 8, 'Additional', 'If the claims occur within 90 days of inception of the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 9, 'Additional', 'If the driver of the vehicle is under 25 and is noted as a driver on the policy', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 10, 'Additional', 'If there is no third party involved or the third party cannot be traced due to lack of information', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 11, 'Additional', 'If this is the second claim against the policy within a 12-month period', 0, 5000, 0, 7.5, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 12, 'Additional', 'If the vehicle remains uninspected after the allowed 24 hours. This will apply to all accident claims. There will be no cover for theft, hi-jacking or windscreen claims', 0, 5000, 0, 20, 1, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTND_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_CENTRD_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMND_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@SAUnderwriters_SNTMRD_Motor_CoverDefinitionId, 13, 'Additional', 'Theft / Hi-Jacking / Attempted theft within grace period. There is a s even day grace period for the approved tracking device to be installed. During this time the penalty excess will apply for all theft and hi-jack cover if the device is not fitted', 0, 5000, 0, 20, 1, 0, 0, 0)
	end

--Telesure (Content), Product Codes: 'AUGPRD', 'FIR', 'UNI', 'VIRS' (Auto & General, First For Women, Unity, Virseker)
declare
	@Telesure_AUGPRD_Contents_CoverDefinitionId int,
	@Telesure_FIR_Contents_CoverDefinitionId int,
	@Telesure_UNI_Contents_CoverDefinitionId int,
	@Telesure_VIRS_Contents_CoverDefinitionId int
	
select
	@Telesure_AUGPRD_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AUGPRD') and
	Cover.Name in ('Contents')

select
	@Telesure_FIR_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('FIR') and
	Cover.Name in ('Contents')

select
	@Telesure_UNI_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('UNI') and
	Cover.Name in ('Contents')

select
	@Telesure_VIRS_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('VIRS') and
	Cover.Name in ('Contents')

if	isnull(@Telesure_AUGPRD_Contents_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_FIR_Contents_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_UNI_Contents_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_VIRS_Contents_CoverDefinitionId, 0) > 0
	begin
		--Household Contents - Basic & Additional Excess

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 0, 'House Contents', 'Basic Excess(5% or Excess Displayed)', 1600, 0, 9000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 1, 'House Contents', 'Additional Excess - Burglary', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 2, 'House Contents', 'Additional Excess Lightning',800, 0, 0, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 0, 'House Contents', 'Basic Excess', 1600, 0, 9000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 1, 'House Contents', 'Additional Excess - Burglary', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 2, 'House Contents', 'Additional Excess Lightning',800, 0, 0, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 0, 'House Contents', 'Basic Excess', 1600, 0, 9000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 1, 'House Contents', 'Additional Excess - Burglary', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 2, 'House Contents', 'Additional Excess Lightning',800, 0, 0, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 0, 'Huisinhoud', 'Basiese Bybetaling', 1725, 0, 9750, 5, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 1, 'Huisinhoud', 'Addisionele Bybetaling - Diefstal', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 2, 'Huisinhoud', 'Addisionele Bybetaling - Weerlig/Kragstuwing', 850, 0, 0, 0, 0, 0, 0, 0)

		--Household Contents - Min/Max Sheets

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 3, 'HOUSE CONTENTS HOUSE / COTTAGE (H)','sum insured', 0, 50000, 7400000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 3,  'HOUSE CONTENTS HOUSE / COTTAGE (H)','sum insured', 0, 50000, 7400000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 3, 'HOUSE CONTENTS HOUSE / COTTAGE (H)','sum insured', 0, 50000, 7400000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 3, 'HUISINHOUD - HUIS/KOTHUIS','Versekerde Bedrag', 0, 50000, 7500000, 0, 0, 0, 0, 0)
		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 4, 'HOUSE CONTENTS FLAT ABOVE GROUND (F)','sum insured', 0, 50000, 4750000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 4, 'HOUSE CONTENTS FLAT ABOVE GROUND (F)','sum insured', 0, 50000, 4750000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 4, 'HOUSE CONTENTS FLAT ABOVE GROUND (F)','sum insured', 0, 50000, 4750000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 4, 'HUISINHOUD - WOONSTEL BO GROND VLOER (F)','Versekerde Bedrag', 0, 50000, 4750000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 5, 'HOUSE CONTENTS FLAT GROUND (G)', 'sum insured', 0, 50000, 3400000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 5, 'HOUSE CONTENTS FLAT GROUND (G)', 'sum insured',0, 50000, 3400000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 5, 'HOUSE CONTENTS FLAT GROUND (G)', 'sum insured',0, 50000, 3400000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 5, 'HUISINHOUD - WOONSTEL GROND VLOER (G)','Versekerde Bedrag', 0, 50000, 3500000, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 6, 'HOUSE CONTENTS TOWN HOUSE / CLUSTER (T)','sum insured', 0, 50000, 7400000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 6, 'HOUSE CONTENTS TOWN HOUSE / CLUSTER (T)','sum insured', 0, 50000, 7400000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 6, 'HOUSE CONTENTS TOWN HOUSE / CLUSTER (T)','sum insured', 0, 50000, 7400000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 6, 'HUISINHOUD - MEENTHUIS/KLUSTER (T)','Versekerde Bedrag', 0, 50000, 7500000, 0, 0, 0, 0, 0)

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 7, 'HOUSE CONTENTS PARK HOME (P)','sum insured', 0, 50000, 450000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 7, 'HOUSE CONTENTS PARK HOME (P)','sum insured', 0, 50000, 450000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 7, 'HOUSE CONTENTS PARK HOME (P)','sum insured', 0, 50000, 450000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 7, 'HUISINHOUD - PARKEER WONING (P)','Versekerde Bedrag', 0, 50000, 500000, 0, 0, 0, 0, 0)

		--Household Contents - Additional Coverage Excesses
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 8, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER', 'Deterioration of food', 2200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 9, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Washing and Garden Furniture', 3500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 10, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Guests property', 5200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 11, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Money - stolen from risk address', 2000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 12, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Documents, coins, stamps', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 13, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Locks & Keys', 2650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 14, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Fraudulent use of bank / credit cards', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 15, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Hole-in-one', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 16, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Insured & spouse (death) fire / break in', 15000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 17, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Domestics belongins - theft', 5250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 18, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Medical expenses (defect / pet)', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 19, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Veterinary costs due to road accident', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 20, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Loss of rent - 20% of sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 21, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Transit - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 22, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Mirrors / glass as part of furniture - Items sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 23, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Accidental breakage - TV - Items sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 24, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Fire brigade charges - Cost of Charges', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 25, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Public Liability as a Householder', 1200000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 26, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Tenant is Liability as a Householder', 1200000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Contents_CoverDefinitionId, 27, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Liability to Domestic Employees', 120000, 0, 0, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 8, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER', 'Deterioration of food', 2200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 9, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Washing and Garden Furniture', 3500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 10, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Guests property', 5200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 11, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Money - stolen from risk address', 2000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 12, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Documents, coins, stamps', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 13, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Locks & Keys', 2650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 14, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Fraudulent use of bank / credit cards', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 15, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Hole-in-one', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 16, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Insured & spouse (death) fire / break in', 15000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 17, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Domestics belongins - theft', 5250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 18, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Medical expenses (defect / pet)', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 19, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Veterinary costs due to road accident', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 20, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Loss of rent - 20% of sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 21, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Transit - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 22, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Mirrors / glass as part of furniture - Items sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 23, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Accidental breakage - TV - Items sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 24, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Fire brigade charges - Cost of Charges', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 25, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Public Liability as a Householder', 1200000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 26, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Tenant is Liability as a Householder', 1200000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Contents_CoverDefinitionId, 27, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Liability to Domestic Employees', 120000, 0, 0, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 8, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER', 'Deterioration of food', 2200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 9, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Washing and Garden Furniture', 3500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 10, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Guests property', 5200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 11, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Money - stolen from risk address', 2000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 12, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Documents, coins, stamps', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 13, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Locks & Keys', 2650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 14, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Fraudulent use of bank / credit cards', 4000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 15, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Hole-in-one', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 16, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Insured & spouse (death) fire / break in', 15000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 17, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Domestics belongins - theft', 5250, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 18, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Medical expenses (defect / pet)', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 19, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Veterinary costs due to road accident', 3300, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 20, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Loss of rent - 20% of sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 21, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Transit - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 22, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Mirrors / glass as part of furniture - Items sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 23, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Accidental breakage - TV - Items sum insured', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 24, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Fire brigade charges - Cost of Charges', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 25, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Public Liability as a Householder', 1200000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 26, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Tenant is Liability as a Householder', 1200000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Contents_CoverDefinitionId, 27, 'HOUSEHOLD CONTENTS - ADDITIONAL COVER','Liability to Domestic Employees', 120000, 0, 0, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 8, 'HUISINHOUD - ADDISIONELE DEKKING', '1. Voedsel wat bederf', 2200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 9, 'HUISINHOUD - ADDISIONELE DEKKING', '2. Gesteelde wasgoed en tuinmeubels', 3850, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 10, 'HUISINHOUD - ADDISIONELE DEKKING', '3. Gaste se besittings', 5750, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 11, 'HUISINHOUD - ADDISIONELE DEKKING', '4. Geld uit u huis gesteel', 2200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 12, 'HUISINHOUD - ADDISIONELE DEKKING', '5. Persoonlike dokumente, munte en se�ls', 4400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 13, 'HUISINHOUD - ADDISIONELE DEKKING', '6. Slotte en sleutels', 3000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 14, 'HUISINHOUD - ADDISIONELE DEKKING', '7. Krediet- en bankkaarte', 4400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 15, 'HUISINHOUD - ADDISIONELE DEKKING', '8. ''n Gholfkolhou of ''n rolbalvoltal', 4400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 16, 'HUISINHOUD - ADDISIONELE DEKKING', '9. U en/of u wederhelf se dood', 16500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 17, 'HUISINHOUD - ADDISIONELE DEKKING', '10. U huishoudelike werker se besittings', 6000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 18, 'HUISINHOUD - ADDISIONELE DEKKING', '11. Mediese Uitgawes', 3650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 19, 'HUISINHOUD - ADDISIONELE DEKKING', '12. Veeartskoste', 3650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 20, 'HUISINHOUD - ADDISIONELE DEKKING', '13. Huur om elders te bly', 0, 0, 20, 0, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess, ShouldDisplayExcessValues)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 21, 'HUISINHOUD - ADDISIONELE DEKKING', '14. Transito - Versekerde Waarde', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess, ShouldDisplayExcessValues)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 22, 'HUISINHOUD - ADDISIONELE DEKKING', '15. Breek van spie�ls en glas - Items se versekerde waarde', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess, ShouldDisplayExcessValues)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 23, 'HUISINHOUD - ADDISIONELE DEKKING', '16. Breek van televisiestel - Items se versekerde waarde', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess, ShouldDisplayExcessValues)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 24, 'HUISINHOUD - ADDISIONELE DEKKING', '17. Brandweerkoste - Koste van rekening', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 25, 'HUISINHOUD - ADDISIONELE DEKKING', 'Aanspreeklikheid as ''n huiseienaar', 0, 0, 1200000, 0, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 26, 'HUISINHOUD - ADDISIONELE DEKKING', 'Huurder se aanspreeklikheid as ''n huiseienaar', 0, 0, 1200000, 0, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 27, 'HUISINHOUD - ADDISIONELE DEKKING', 'Aanspreeklikheid teenoor huishoudelike werkers', 0, 0, 120000, 0, 0, 0, 1, 0)

		--Household Contents - Additional Coverage Excesses

		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Contents_CoverDefinitionId, 28, 'HUISINHOUD - ADDISIONELE DEKKING BYBETALINGS', 'ADDISIONELE DEKKING (Buiten No. 8)', 0, 0, 525, 0, 0, 0, 1, 0)
	end


--Telesure (All Risk), Product Codes: 'AUGPRD', 'FIR', 'UNI', 'VIRS' (Auto & General, First For Women, Unity, Virseker)
declare
	@Telesure_AUGPRD_AllRisk_CoverDefinitionId int,
	@Telesure_FIR_AllRisk_CoverDefinitionId int,
	@Telesure_UNI_AllRisk_CoverDefinitionId int,
	@Telesure_VIRS_AllRisk_CoverDefinitionId int
	
select
	@Telesure_AUGPRD_AllRisk_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AUGPRD') and
	Cover.Name in ('All Risk')

select
	@Telesure_FIR_AllRisk_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('FIR') and
	Cover.Name in ('All Risk')

select
	@Telesure_UNI_AllRisk_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('UNI') and
	Cover.Name in ('All Risk')

select
	@Telesure_VIRS_AllRisk_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('VIRS') and
	Cover.Name in ('All Risk')

if	isnull(@Telesure_AUGPRD_AllRisk_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_FIR_AllRisk_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_UNI_AllRisk_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_VIRS_AllRisk_CoverDefinitionId, 0) > 0
	begin
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_AllRisk_CoverDefinitionId, 0, 'All Risks', 'Basic Excess (Except Cell Phone) - Supported', 850, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_AllRisk_CoverDefinitionId, 1, 'All Risks', 'Basic Excess (Except Cell Phone) - Unsupported', 1200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_AllRisk_CoverDefinitionId, 2, 'All Risks', 'Basic Excess - Cell phone (5% or Excess Displayed)', 1350, 0, 9000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_AllRisk_CoverDefinitionId, 0, 'All Risks', 'Basic Excess (Except Cell Phone) - Supported', 850, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_AllRisk_CoverDefinitionId, 1, 'All Risks', 'Basic Excess (Except Cell Phone) - Unsupported', 1200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_AllRisk_CoverDefinitionId, 2, 'All Risks', 'Basic Excess - Cell phone (5% or Excess Displayed)', 1350, 0, 9000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_AllRisk_CoverDefinitionId, 0, 'All Risks', 'Basic Excess (Except Cell Phone) - Supported', 850, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_AllRisk_CoverDefinitionId, 1, 'All Risks', 'Basic Excess (Except Cell Phone) - Unsupported', 1200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_AllRisk_CoverDefinitionId, 2, 'All Risks', 'Basic Excess - Cell phone (5% or Excess Displayed)', 1350, 0, 9000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 0, 'ALLE RISIKO', 'BASIESE BYBETALING (BUITEN VIR SELFOON) -- Ondersteun', 850, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 1, 'ALLE RISIKO', 'BASIESE BYBETALING (BUITEN VIR SELFOON) -- Nie ondersteun', 1200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 2, 'ALLE RISIKO', 'BASIESE BYBETALING - SELFOON (5% of Vertoonde Bybetaling)', 1450, 0, 9750, 0, 0, 0, 0, 0)
		
		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 3, 'PORTABLE POSSESSIONS:  SUPPORTED', 'UNSPECIFIED (AU) per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 4, 'PORTABLE POSSESSIONS:  SUPPORTED', 'UNSPECIFIED (AU) per policy', 0, 5000, 40000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 3, 'PORTABLE POSSESSIONS:  SUPPORTED', 'UNSPECIFIED (AU) per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 4, 'PORTABLE POSSESSIONS:  SUPPORTED', 'UNSPECIFIED (AU) per policy', 0, 5000, 40000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 3, 'PORTABLE POSSESSIONS:  SUPPORTED', 'UNSPECIFIED (AU) per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 4, 'PORTABLE POSSESSIONS:  SUPPORTED', 'UNSPECIFIED (AU) per policy', 0, 5000, 40000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 3, 'DRAAGBARE BESITTINGS: ONDERSTEUN', 'ONGESPESIFISEER (AU) - Per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 4, 'DRAAGBARE BESITTINGS: ONDERSTEUN', 'ONGESPESIFISEER (AU) - Per polis', 0, 5500, 44000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 5, 'PORTABLE POSSESSIONS:  SUPPORTED', 'SPECIFIED (AJ) per item', 0, 4000, 176000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 6, 'PORTABLE POSSESSIONS:  SUPPORTED', 'SPECIFIED (AJ) Maximum per policy', 600000, 0, 0, 50, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 5, 'PORTABLE POSSESSIONS:  SUPPORTED', 'SPECIFIED (AJ) per item', 0, 4000, 176000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 6, 'PORTABLE POSSESSIONS:  SUPPORTED', 'SPECIFIED (AJ) Maximum per policy', 600000, 0, 0, 50, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 5, 'PORTABLE POSSESSIONS:  SUPPORTED', 'SPECIFIED (AJ) per item', 0, 4000, 176000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 6, 'PORTABLE POSSESSIONS:  SUPPORTED', 'SPECIFIED (AJ) Maximum per policy', 600000, 0, 0, 50, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 5, 'DRAAGBARE BESITTINGS: ONDERSTEUN', 'GESPESIFISEER (AJ) per item', 0, 4000, 176000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 6, 'DRAAGBARE BESITTINGS: ONDERSTEUN', 'GESPESIFISEER (AJ) per polis', 660000, 0, 0, 50, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 7, 'LOCKED BOOT', 'MAXIMUM INDEMNITY (Brokers)', 0, 0, 24000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 8, 'LOCKED CABIN', 'MAXIMUM INDEMNITY (Brokers)', 0, 0, 5500, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 7, 'LOCKED BOOT', 'MAXIMUM INDEMNITY (Brokers)', 0, 0, 24000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 8, 'LOCKED CABIN', 'MAXIMUM INDEMNITY (Brokers)', 0, 0, 5500, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 7, 'LOCKED BOOT', 'MAXIMUM INDEMNITY (Brokers)', 0, 0, 24000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 8, 'LOCKED CABIN', 'MAXIMUM INDEMNITY (Brokers)', 0, 0, 5500, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 7, 'GESLOTE KATTEBAK', 'MAKSIMUM VRYWARING', 0, 0, 26500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 8, 'GESLOTE BINNERUIM', 'MAKSIMUM VRYWARING', 0, 0, 6000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 9, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'UNSPECIFIED (BU) per policy', 0, 5000, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 10, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'UNSPECIFIED (BU)Maximum per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 9, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'UNSPECIFIED (BU) per policy', 0, 5000, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 10, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'UNSPECIFIED (BU)Maximum per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 9, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'UNSPECIFIED (BU) per policy', 0, 5000, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 10, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'UNSPECIFIED (BU)Maximum per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 9, 'DRAAGBARE BESITTINGS: NIE ONDERSTEUN', 'ONGESPESIFISEER (BU) per item', 0, 0, 4000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 10, 'DRAAGBARE BESITTINGS: NIE ONDERSTEUN', 'ONGESPESIFISEER (BU) per polis', 0, 5500, 16500, 0, 0, 0, 0, 0)
		

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 11, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'SPECIFIED (BJ) per item', 0, 4000, 70000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 12, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'SPECIFIED (BJ) Maximum per policy', 0, 0, 78000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 11, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'SPECIFIED (BJ) per item', 0, 4000, 70000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 12, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'SPECIFIED (BJ) Maximum per policy', 0, 0, 78000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 11, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'SPECIFIED (BJ) per item', 0, 4000, 70000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 12, 'PORTABLE POSSESSIONS:UNSUPPORTED', 'SPECIFIED (BJ) Maximum per policy', 0, 0, 78000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 11, 'DRAAGBARE BESITTINGS: NIE ONDERSTEUN', 'GESPESIFISEERD (BJ) per item', 0, 0, 4000, 77000, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 12, 'DRAAGBARE BESITTINGS: NIE ONDERSTEUN', 'GESPESIFISEERD (BJ) per polis', 0, 0, 85750, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 13, 'SPECTACLES / CONTACT LENSES (CL/BL)', 'per item', 0, 1500, 14000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 14, 'SPECTACLES / CONTACT LENSES (CL/BL)', 'Maximum per policy', 0, 0, 41500, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 13, 'SPECTACLES / CONTACT LENSES (CL/BL)', 'per item', 0, 1500, 14000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 14, 'SPECTACLES / CONTACT LENSES (CL/BL)', 'Maximum per policy', 0, 0, 41500, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 13, 'SPECTACLES / CONTACT LENSES (CL/BL)', 'per item', 0, 1500, 14000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 14, 'SPECTACLES / CONTACT LENSES (CL/BL)', 'Maximum per policy', 0, 0, 41500, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 13, 'VOORSKRIFBRILLE / KONTAKLENSE (CL/BL)', 'Per item', 0, 1500, 15500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 14, 'VOORSKRIFBRILLE / KONTAKLENSE (CL/BL)', 'Per polis', 0, 0, 45650, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 15, 'CELL PHONES (CP/BP)', 'per item', 0,1700, 24200, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 16, 'CELL PHONES (CP/BP)', 'Maximum per policy', 0, 0, 60000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 15, 'CELL PHONES (CP/BP)', 'per item', 0,1700, 24200, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 16, 'CELL PHONES (CP/BP)', 'Maximum per policy', 0, 0, 60000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 15, 'CELL PHONES (CP/BP)', 'per item', 0,1700, 24200, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 16, 'CELL PHONES (CP/BP)', 'Maximum per policy', 0, 0, 60000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 15, 'SELFONE (CP/BP)', 'Per item', 0, 1700, 26650, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 16, 'SELFONE (CP/BP)', 'Per polis', 0, 0, 66000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 17, 'BICYCLES (PC/BC)', 'per item', 0, 1400, 146500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 18, 'BICYCLES (PC/BC)', 'Maximum per policy', 0, 0, 220000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 17, 'BICYCLES (PC/BC)', 'per item', 0, 1400, 146500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 18, 'BICYCLES (PC/BC)', 'Maximum per policy', 0, 0, 220000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 17, 'BICYCLES (PC/BC)', 'per item', 0, 1400, 146500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 18, 'BICYCLES (PC/BC)', 'Maximum per policy', 0, 0, 220000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 17, 'FIETSE (PC/BC)', 'Per item', 0, 1400, 160000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 18, 'FIETSE (PC/BC)', 'Per polis', 0, 0, 240000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 19, 'SWIMMING POOL (SP/SB)', 'Minimum per policy', 0, 2500, 25000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_ALLRISK_CoverDefinitionId, 19, 'SWIMMING POOL (SP/SB)', 'Minimum per policy', 0, 2500, 25000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_ALLRISK_CoverDefinitionId, 19, 'SWIMMING POOL (SP/SB)', 'Minimum per policy', 0, 2500, 25000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 19, 'BOORGAT- EN SWEMBADTOERUSTING (SP/SB)', 'Per polis', 0, 2500, 27500, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 20, 'STANDALONE CELL PHONES', 'per item', 0, 1500, 20000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 21, 'STANDALONE CELL PHONES', 'Maximum per policy', 0, 0, 48000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 20, 'STANDALONE CELL PHONES', 'per item', 0, 1500, 20000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 21, 'STANDALONE CELL PHONES', 'Maximum per policy', 0, 0, 48000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 20, 'STANDALONE CELL PHONES', 'per item', 0, 1500, 20000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_ALLRISK_CoverDefinitionId, 21, 'STANDALONE CELL PHONES', 'Maximum per policy', 0, 0, 48000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 20, 'SELFOON ALLEEN ****', 'Per item', 0, 1500, 22000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_AllRisk_CoverDefinitionId, 21, 'SELFOON ALLEEN ****', 'Per polis', 0, 0, 52750, 0, 0, 0, 0, 0)
	end


--Telesure (Building), Product Codes: 'AUGPRD', 'FIR', 'UNI', 'VIRS' (Auto & General, First For Women, Unity, Virseker)
declare
	@Telesure_AUGPRD_Building_CoverDefinitionId int,
	@Telesure_FIR_Building_CoverDefinitionId int,
	@Telesure_UNI_Building_CoverDefinitionId int,
	@Telesure_VIRS_Building_CoverDefinitionId int
	
select
	@Telesure_AUGPRD_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AUGPRD') and
	Cover.Name in ('Building')

select
	@Telesure_FIR_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('FIR') and
	Cover.Name in ('Building')

select
	@Telesure_UNI_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('UNI') and
	Cover.Name in ('Building')

select
	@Telesure_VIRS_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('VIRS') and
	Cover.Name in ('Building')

if	isnull(@Telesure_AUGPRD_Building_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_FIR_Building_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_UNI_Building_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_VIRS_Building_CoverDefinitionId, 0) > 0
	begin
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 0, 'Building', 'Basic Excess(5% or Excess Displayed)', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 1, 'Building', 'Basic Excess - subsidence and landslip(5% or Excess Displayed)', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 2,'Building', 'Liability as home owner', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 3, 'Building','Additional Excess (5% or Excess Displayed)- burst water pipes (not attached to Geysers)', 1000, 0, 10000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 0, 'Building', 'Basic Excess(5% or Excess Displayed)', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 1, 'Building', 'Basic Excess - subsidence and landslip(5% or Excess Displayed)', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 2,'Building', 'Liability as home owner', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 3, 'Building','Additional Excess (5% or Excess Displayed)- burst water pipes (not attached to Geysers)', 1000, 0, 10000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 0, 'Building', 'Basic Excess(5% or Excess Displayed)', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 1, 'Building', 'Basic Excess - subsidence and landslip(5% or Excess Displayed)', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 2,'Building', 'Liability as home owner', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 3, 'Building','Additional Excess (5% or Excess Displayed)- burst water pipes (not attached to Geysers)', 1000, 0, 10000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 0, 'GEBOUE BYBETALINGS', 'Basiese Bybetaling (Insluitende ineensakking, deining- of grondverskuiwing) - 5% of Vertoonde Bybetaling', 1500, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 1, 'GEBOUE BYBETALINGS', 'Addisionele Bybetaling (Gebarste waterpype wat nie aan die warmwatertoestel gekoppel is nie) - 5% of Vertoonde Bybetaling', 1000, 0, 10000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 2, 'GEBOUE BYBETALINGS', 'Addisionele dekking bybetalings geboue', 500, 0, 0, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 4, 'Geyser and Geyser Related Building damage', 'Basic Excess (5% or Excess Displayed)', 1650, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 5, 'Geyser and Geyser Related Building damage', 'Basic Excess(5% or Excess Displayed) - consequential damage - not accompanied with geyser claim(No Max)', 1000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 6,'Geyser and Geyser Related Building damage', 'buy up options', 10000, 0, 0, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 4, 'Geyser and Geyser Related Building damage', 'Basic Excess (5% or Excess Displayed)', 1650, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 5, 'Geyser and Geyser Related Building damage','Basic Excess(5% or Excess Displayed) - consequential damage - not accompanied with geyser claim(No Max)',1000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 6,'Geyser and Geyser Related Building damage', 'buy up options', 20000, 0, 0, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 4, 'Geyser and Geyser Related Building damage', 'Basic Excess (5% or Excess Displayed)', 1650, 0, 15000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 5, 'Geyser and Geyser Related Building damage', 'Basic Excess(5% or Excess Displayed) - consequential damage - not accompanied with geyser claim(No Max)',1000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 6, 'Geyser and Geyser Related Building damage', 'buy up options', 30000, 0, 0, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 3, 'WARMWATERTOESTEL- EN VERWANTE GEBOUE SKADE', 'Basiese Bybetaling - 5% of Vertoonde Bybetaling)', 1650, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 4, 'WARMWATERTOESTEL- EN VERWANTE GEBOUE SKADE', 'Maksimum Vrywaring (Uitsluitend die spesifieke warmwatertoestel koste)', 0, 0, 16500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 5, 'WARMWATERTOESTEL- EN VERWANTE GEBOUE SKADE', 'Basiese Bybetaling - Gevolglike skade (Nie ingesluit by warmwatertoestel eis) - 5% of Vertoonde Bybetaling - Geen Maksimum', 1100, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 6, 'WARMWATERTOESTEL- EN VERWANTE GEBOUE SKADE', 'Op Koop opsies', 0, 10000, 30000, 0, 0, 0, 0, 0)

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 7, 'BUILDING ADDITIONAL COVER', '1. Loss of rent - 20% of Sum Insured', 0, 0, 0, 20, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 8, 'BUILDING ADDITIONAL COVER', '2. Public authority - professional fees  - 20% of Sum Insured', 0, 0, 0, 20, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 9, 'BUILDING ADDITIONAL COVER', '3. Accidental damage - glass / sanitaryware - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 10, 'BUILDING ADDITIONAL COVER', '4.  Accidental damage - public supply / mains - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 11, 'BUILDING ADDITIONAL COVER', '5.  Aerials / masts / dish - accident & theft damage - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 12, 'BUILDING ADDITIONAL COVER', '6.  Fire brigade charges - Cost of Charges', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 13, 'BUILDING ADDITIONAL COVER', 'Costs:  demolish / remove debris - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 14, 'BUILDING ADDITIONAL COVER', '7. Rent to live elsewhere - 20% of Sum Insured', 0, 0, 0, 20, 0, 0, 1, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 7, 'BUILDING ADDITIONAL COVER', '1. Loss of rent - 20% of Sum Insured', 0, 0, 0, 20, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 8, 'BUILDING ADDITIONAL COVER', '2. Public authority - professional fees  - 20% of Sum Insured', 0, 0, 0, 20, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 9, 'BUILDING ADDITIONAL COVER', '3. Accidental damage - glass / sanitaryware - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 10, 'BUILDING ADDITIONAL COVER', '4.  Accidental damage - public supply / mains - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 11, 'BUILDING ADDITIONAL COVER', '5.  Aerials / masts / dish - accident & theft damage - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 12, 'BUILDING ADDITIONAL COVER', '6.  Fire brigade charges - Cost of Charges', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 13, 'BUILDING ADDITIONAL COVER', 'Costs:  demolish / remove debris - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 14, 'BUILDING ADDITIONAL COVER', '7. Rent to live elsewhere - 20% of Sum Insured', 0, 0, 0, 20, 0, 0, 1, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 7, 'BUILDING ADDITIONAL COVER', '1. Loss of rent - 20% of Sum Insured', 0, 0, 0, 20, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 8, 'BUILDING ADDITIONAL COVER', '2. Public authority - professional fees  - 20% of Sum Insured', 0, 0, 0, 20, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 9, 'BUILDING ADDITIONAL COVER', '3. Accidental damage - glass / sanitaryware - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 10, 'BUILDING ADDITIONAL COVER', '4.  Accidental damage - public supply / mains - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 11, 'BUILDING ADDITIONAL COVER', '5.  Aerials / masts / dish - accident & theft damage - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 12, 'BUILDING ADDITIONAL COVER', '6.  Fire brigade charges - Cost of Charges', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 13, 'BUILDING ADDITIONAL COVER', 'Costs:  demolish / remove debris - Sum Insured', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 14, 'BUILDING ADDITIONAL COVER', '7. Rent to live elsewhere - 20% of Sum Insured', 0, 0, 0, 20, 0, 0, 1, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 7, 'GEBOUE ADDISIONELE DEKKING', '1. Verlies van huurinkomste - 20% van Versekerde Bedrag', 0, 0, 0, 20, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 8, 'GEBOUE ADDISIONELE DEKKING', '2. Professionele fooie - 20% van Versekerde Bedrag', 0, 0, 0, 20, 0, 0, 1, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 9, 'GEBOUE ADDISIONELE DEKKING', '3. Toevallige skade - glas- en badkamertoerusting - Versekerde Bedrag', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 10, 'GEBOUE ADDISIONELE DEKKING', '4. Toevallige skade - elektrisiteitsvoorsiening - Versekerde Bedrag', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 11, 'GEBOUE ADDISIONELE DEKKING', '5. Toevallige skade & diefstal - antennas / maste / sattelietskottels - Versekerde Bedrag', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 12, 'GEBOUE ADDISIONELE DEKKING', '6. Brandweerkoste - Koste', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 13, 'GEBOUE ADDISIONELE DEKKING', 'Slopingskoste - Vesekerde Bedrag', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 14, 'GEBOUE ADDISIONELE DEKKING', '7. Huuronkoste om elders te bly - 20% van Versekerde Bedrag', 0, 0, 0, 20, 0, 0, 1, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 15, 'Additional Cover', 'Additional Cover Excess buildings', 450, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Building_CoverDefinitionId, 16, 'Additional Cover', 'Additional Cover Excess contents', 550, 0, 0, 0, 0, 0, 0, 0)		
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 15, 'Additional Cover', 'Additional Cover Excess buildings', 450, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Building_CoverDefinitionId, 16, 'Additional Cover', 'Additional Cover Excess contents', 550, 0, 0, 0, 0, 0, 0, 0)		
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 15, 'Additional Cover', 'Additional Cover Excess buildings', 450, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Building_CoverDefinitionId, 16, 'Additional Cover', 'Additional Cover Excess contents', 550, 0, 0, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 17, 'Building HOUSE / COTTAGE (H)','Sum Insured' ,0, 330000, 33500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 18,'Building HOUSE / COTTAGE (H)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 17, 'Building HOUSE / COTTAGE (H)','Sum Insured', 0, 330000, 33500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 18, 'Building HOUSE / COTTAGE (H)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 17, 'Building HOUSE / COTTAGE (H)','Sum Insured', 0, 330000, 33500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 18, 'Building HOUSE / COTTAGE (H)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 15, 'HUIS / KOTHUIS (H)','Versekerde bedrag', 0, 360000, 37000000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 16, 'HUIS / KOTHUIS (H)', 'Maksimum versekerde bedrag (Grasdak)', 0, 0, 9000000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 19, 'Building FLAT ABOVE GROUND (F)', 'sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 20, 'Building FLAT ABOVE GROUND (F)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 19, 'Building FLAT ABOVE GROUND (F)', 'sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 20, 'Building FLAT ABOVE GROUND (F)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 19, 'Building FLAT ABOVE GROUND (F)', 'sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 20, 'Building FLAT ABOVE GROUND (F)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 17,'WOONSTEL BO GROND VLOER (F)', 'Versekerde bedrag', 0, 220000, 18000000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 18,'WOONSTEL BO GROND VLOER (F)',' Maksimum versekerde bedrag (Grasdak)', 0, 0, 9000000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 21, 'Building FLAT GROUND (G)','sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 22, 'Building FLAT GROUND (G)',' Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 21, 'Building FLAT GROUND (G)','sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 22, 'Building FLAT GROUND (G)', 'Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 21, 'Building FLAT GROUND (G)', 'sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 22, 'Building FLAT GROUND (G)', 'Maximum Sum Insured (Thatch)', 0, 0, 8250000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 19, 'WOONSTEL GROND VLOER (G)', 'Versekerde bedrag', 0, 220000, 18000000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 20, 'WOONSTEL GROND VLOER (G)', 'Maksimum versekerde bedrag (Grasdak)', 0, 0, 9000000, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 23,'Building TOWN HOUSE','sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 24,'Building TOWN HOUSE Maximum','sum insured(Thatch)', 0, 0,8250000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 23,'Building TOWN HOUSE','sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 24,'Building TOWN HOUSE Maximum','sum insured(Thatch)', 0, 0,8250000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 23,'Building TOWN HOUSE','sum insured', 0, 200000, 16500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 24,'Building TOWN HOUSE Maximum','sum insured(Thatch)', 0, 0,8250000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 21, 'MEENTHUIS (T)', 'Versekerde bedrag', 0, 200000, 18000000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 22, 'MEENTHUIS (T)', 'Maksimum versekerde bedrag (Grasdak)', 0, 0, 9000000, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 25, 'Building CLUSTER','sum insured', 0, 360000,33500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 26, 'Building CLUSTER',' Maximum sum insured(Thatch)', 0, 0,8250000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 25, 'Building CLUSTER','sum insured', 0, 360000,33500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 26, 'Building CLUSTER',' Maximum sum insured(Thatch)', 0, 0,8250000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 25, 'Building CLUSTER','sum insured', 0, 360000,33500000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 26, 'Building CLUSTER', 'Maximum sum insured(Thatch)', 0, 0,8250000, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 23, 'KLUSTER', 'Versekerde bedrag', 0, 400000, 37000000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 24, 'KLUSTER', 'Maksimum versekerde bedrag (Grasdak)', 0, 0, 9000000, 0, 0, 0, 0, 0)

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 27, 'Building RDP Houses', 'sum insured', 0, 150000, 8250000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 28, 'Building RDP Houses', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 27, 'Building RDP Houses', 'sum insured', 0, 150000, 8250000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 28, 'Building RDP Houses', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 27, 'Building RDP Houses', 'sum insured', 0, 150000, 8250000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 28, 'Building RDP Houses', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 25, 'HOP HUIS', 'Versekerde bedrag', 0, 165000, 37000000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 26, 'HOP HUIS', 'Maksimum versekerde bedrag (Grasdak)', 0, 0, 0, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 29, 'Building PARK HOME (P)', 'sum insured', 0,200000, 1350000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_BUILDING_CoverDefinitionId, 30, 'Building PARK HOME (P)', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 29, 'Building PARK HOME (P)', 'sum insured', 0,200000, 1350000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_BUILDING_CoverDefinitionId, 30, 'Building PARK HOME (P)', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 29, 'Building PARK HOME (P)', 'sum insured', 0,200000, 1350000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_BUILDING_CoverDefinitionId, 30, 'Building PARK HOME (P)', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 27, 'PARKEER WONING (P)', 'sum insured', 0,200000, 1350000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Building_CoverDefinitionId, 28, 'PARKEER WONING (P)', 'Maximum sum insured(Thatch)', 0, 0, 0, 0, 0, 0, 0, 0)
	end
	
	
	
--Telesure (Motor), Product Codes: 'AUGPRD', 'FIR', 'UNI', 'VIRS' (Auto & General, First For Women, Unity, Virseker)
declare
	@Telesure_AUGPRD_Motor_CoverDefinitionId int,
	@Telesure_FIR_Motor_CoverDefinitionId int,
	@Telesure_UNI_Motor_CoverDefinitionId int,
	@Telesure_VIRS_Motor_CoverDefinitionId int
	
select
	@Telesure_AUGPRD_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AUGPRD') and
	Cover.Name in ('Motor')

select
	@Telesure_FIR_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('FIR') and
	Cover.Name in ('Motor')

select
	@Telesure_UNI_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('UNI') and
	Cover.Name in ('Motor')

select
	@Telesure_VIRS_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('VIRS') and
	Cover.Name in ('Motor')

if	isnull(@Telesure_AUGPRD_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_FIR_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_UNI_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@Telesure_VIRS_Motor_CoverDefinitionId, 0) > 0
	begin
		--VIRS
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 0, 'OMVATTENDE DEKKING', 'Versekerde bedrag', 0, 18500, 3300000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 1, 'OMVATTENDE DEKKING', 'Maksimum versekerde bedrag - jonger as 21', 0, 0, 302500, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 2, 'DIEFSTAL BYBETALING', 'Normale Gebruik', 0, 3750, 0, 7.5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 3, 'DIEFSTAL BYBETALING', 'Besigheidsgebruik', 0, 4250, 0, 7.5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 4, 'DIEFSTAL BYBETALING', 'Afgetree', 0, 3150, 0, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 5, 'WINDSKERM BYBETALING', 'Herstel', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 6, 'WINDSKERM BYBETALING', 'Vervang', 850, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 7, 'ADDISIONELE BYBETALINGS', 'Buite Suid-Afrikaanse grense', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 8, 'ADDISIONELE BYBETALINGS', 'Gereelde Bestuurder jonger as 25', 2200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 9, 'ADDISIONELE BYBETALINGS', 'Nie-gereelde bestuurder jonger as 25', 5400, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 10, 'ADDISIONELE BYBETALINGS', 'Nie-gereelde bestuurder ouer as 25', 3600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 11, 'ADDISIONELE BYBETALINGS', 'Leerlinglisensie', 2550, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 12, 'ADDISIONELE BYBETALINGS', 'Lisensie korter as 2 jaar', 2550, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 13, 'ADDISIONELE BYBETALINGS', 'Polis minder as 6 maande oud', 2500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 14, 'ADDISIONELE BYBETALINGS', 'Polis Ouderdom > 6 maande <= 12 maande', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 15, 'ADDISIONELE BYBETALINGS', 'Aand bybetaling (as ongeluk tussen 23:00 - 05:00 plaasvind)', 2200, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 16, 'VEREISTES', 'Lae Risiko - Besigtig', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 17, 'VEREISTES', 'Ho� Risiko - Uitbreiding / Voertuig (of dubbele diefstal)', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 18, 'DERDE PARTY, BRAND EN DIEFSTAL', 'Basiese bybetaling', 1800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 19, 'DERDE PARTY, BRAND EN DIEFSTAL', 'Windskerm Bybetaling (indien opsie opgeneem is)', 850, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 20, 'DERDE PARTY, BRAND EN DIEFSTAL', 'Addisionele bybetaling - Nie-gereelde bestuurder', 3600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 21, 'DERDE PARTY, BRAND EN DIEFSTAL', 'Diefstal - Sien Diefstal', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 22, 'DERDE PARTY ALLEENLIK', 'Basiese bybetaling', 1800, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 23, 'DERDE PARTY ALLEENLIK', 'Addisionele bybetaling - Nie-gereelde bestuurder', 3600, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 24, 'MOTORFIETSE', 'Versekerde bedrag', 5000, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 25, 'MOTORFIETSE - OURDERDOM BEPERKINGS', '16 - 17 jaar (wettig) - 50cc - 125cc', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 26, 'MOTORFIETSE - OURDERDOM BEPERKINGS', '18 - 24 jaar - tot en met 500cc', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 27, 'MOTORFIETSE - OURDERDOM BEPERKINGS', '25 & ouer - 500cc +', 0, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 28, 'MOTORFIETS BYBETALINGS', 'Basiese bybetaling - Tot en met 125cc', 0, 2650, 0, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 29, 'MOTORFIETS BYBETALINGS', 'Basiese bybetaling - Tot en met 125cc', 0, 3850, 0, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 30, 'MOTORFIETS BYBETALINGS', 'Addisionele bybetaling - Leerlinglisensie', 2650, 0, 0, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 31, 'MOTORFIETS BYBETALINGS', 'Addisionele bybetaling - Lisensie korter as 2 jaar', 2650, 0, 0, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 32, 'MOTORFIETS BYBETALINGS', 'Addisionele bybetaling - Nie-gereelde bestuurder', 3750, 0, 0, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 33, 'MOTORFIETS BYBETALINGS', 'Addisionele bybetaling - Bestuurder onder 25 en Enjin > 125cc', 2300, 0, 0, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 34, 'MOTORFIETS BYBETALINGS', 'Addisionele bybetaling - Buite Suid-Afrikaanse Grense', 7500, 0, 0, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 35, 'KARAVAAN', 'Versekerde bedrag', 0, 15000, 550000, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 36, 'KARAVAAN INHOUD', 'Versekerde bedrag', 0, 6000, 90000, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 37, 'KARAVAAN BYBETALING', 'Basiese bybetaling - Karavaan', 0, 2850, 0, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 38, 'KARAVAAN BYBETALING', 'Basiese bybetaling - Inhoud', 0, 1850, 0, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 39, 'KARAVAAN BYBETALING', 'Basiese bybetaling - Buite Suid-Afrikaanse grense', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 40, 'SLEEPWAENS', 'Versekerde bedrag', 0, 2400, 300000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 41, 'SLEEPWA BYBETALINGS', 'Basiese bybetaling', 0, 950, 0, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 42, 'SLEEPWA BYBETALINGS', 'Buite Suid-Afrikaanse grense', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 43, 'WATERVAARTUIG', 'Versekerde bedrag', 0, 6000, 1000000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 44, 'WATERVAARTUIG BYBETALINGS', 'Ongeluk & diefstal bybetalings', 0, 3000, 0, 5, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 45, 'WATERVAARTUIG BYBETALINGS', 'Buite Suid-Afrikaanse grense', 7500, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 46, 'VOERTUIG KLANKSTELSEL', 'Versekerde Bedrag', 0, 1500, 33000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 47, 'VOERTUIG KLANKSTELSEL BYBETALING', 'Basiese bybetaling', 1050, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 48, 'GHOLFKARRETJIE (Persoonlike Lyne)', 'Versekerde Bedrag', 0, 12000, 132000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 49, 'GHOLFKARRETJIE (Besigheidsversekering)', 'Versekerde Bedrag', 0, 12000, 225000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 50, 'GHOLFKARRETJIE BYBETALING', 'Ongeluk & diefstal bybetaling', 0, 2500, 0, 10, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 51, 'HERSTELLING- EN AANVANGSFOOIE - A&G / A&G IBC', 'Johannesburg', 370, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 52, 'HERSTELLING- EN AANVANGSFOOIE - A&G / A&G IBC', 'Pretoria', 350, 0, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_VIRS_Motor_CoverDefinitionId, 53, 'HERSTELLING- EN AANVANGSFOOIE - A&G / A&G IBC', 'Res van die land', 270, 0, 0, 0, 0, 0, 0, 0)


		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 0, 'COMPREHENSIVE VEHICLE', 'sum insured', 0,18500, 3300000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 1, 'COMPREHENSIVE VEHICLE', 'Maximum Sum Insured (younger than 21)', 0, 0, 302500, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 0, 'COMPREHENSIVE VEHICLE', 'sum insured', 0,18500, 3300000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 1, 'COMPREHENSIVE VEHICLE', 'Maximum Sum Insured (younger than 21)', 0, 0, 302500, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 0, 'COMPREHENSIVE VEHICLE', 'sum insured', 0,18500, 3300000, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 1, 'COMPREHENSIVE VEHICLE', 'Maximum Sum Insured (younger than 21)', 0, 0, 302500, 0, 0, 0, 0, 0)
		

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 2, 'MOTORBIKES', 'sum insured', 0, 5000,500000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 2, 'MOTORBIKES', 'sum insured', 0, 5000,500000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 2, 'MOTORBIKES', 'sum insured', 5000, 0, 0, 0, 0, 0, 0, 0)
		
		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 3, 'AGE RESTRICTIONS 16 - 17 years (legal)', '50cc  - 125cc', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 4, 'AGE RESTRICTIONS 18 - 24 years', 'up to 500cc', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 5,'AGE RESTRICTIONS 25 & older', '500cc +', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 3, 'AGE RESTRICTIONS 16 - 17 years (legal)', '50cc  - 125cc', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 4, 'AGE RESTRICTIONS 18 - 24 years', 'up to 500cc', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 5,'AGE RESTRICTIONS 25 & older', '500cc +', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 3, 'AGE RESTRICTIONS 16 - 17 years (legal)', '50cc  - 125cc', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 4, 'AGE RESTRICTIONS 18 - 24 years', 'up to 500cc', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 5, 'AGE RESTRICTIONS 25 & older', '500cc +', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		
		--AUGPRD3
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 6, 'CARAVAN', 'sum insured', 0, 15000, 500000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 6, 'CARAVAN', 'sum insured', 0, 15000, 500000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 6, 'CARAVAN', 'sum insured', 0, 15000, 500000, 0, 0, 0, 0, 0)
		
		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 7, 'CARAVAN CONTENTS', 'Minimum sum insured - Contents', 0, 6000, 80000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 7, 'CARAVAN CONTENTS', 'Minimum sum insured - Contents', 0, 6000, 80000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 7, 'CARAVAN CONTENTS', 'Minimum sum insured - Contents', 0, 6000, 80000, 0, 0, 0, 0, 0)
		
		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 8, 'TRAILERS', 'sum insured', 0,2400, 275000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 8, 'TRAILERS', 'sum insured', 0,2400, 275000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 8, 'TRAILERS', 'sum insured', 0,2400, 275000, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 9, 'WATERCRAFT', 'sum insured', 0,6000, 1000000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 9, 'WATERCRAFT', 'sum insured', 0,6000, 1000000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 9, 'WATERCRAFT', 'sum insured', 0,6000, 1000000, 0, 0, 0, 0, 0)

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 10, 'VEHICLE SOUND SYSTEM', 'sum insured', 0,1500,33000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 10, 'VEHICLE SOUND SYSTEM', 'sum insured', 0,1500,33000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 10, 'VEHICLE SOUND SYSTEM', 'sum insured', 0,1500,33000, 0, 0, 0, 0, 0)

		
		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 11, 'GOLF CARTS (Personal Lines)', 'sum insured', 0,12000, 120000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 11, 'GOLF CARTS (Personal Lines)', 'sum insured', 0,12000, 120000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 11, 'GOLF CARTS (Personal Lines)', 'sum insured', 0,12000, 120000, 0, 0, 0, 0, 0)

		--AUGPRD
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_AUGPRD_Motor_CoverDefinitionId, 12, 'GOLF CARTS (Business Insurance)', 'sum insured', 0, 12000, 200000, 0, 0, 0, 0, 0)
		--FIR
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_FIR_Motor_CoverDefinitionId, 12, 'GOLF CARTS (Business Insurance)', 'sum insured', 0, 12000, 200000, 0, 0, 0, 0, 0)
		--UNI
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Telesure_UNI_Motor_CoverDefinitionId, 12, 'GOLF CARTS (Business Insurance)', 'sum insured', 0, 12000, 200000, 0, 0, 0, 0, 0)
end
--Telesure END



--Oakhurst (Motor), Product Codes: 'OAKHURST', 'OAKHURST ASPIRE'
declare
	@Oakhurst_OAKHURST_Motor_CoverDefinitionId int,
	@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId int
	
select
	@Oakhurst_OAKHURST_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('OAKHURST') and
	Cover.Name in ('Motor')

select
	@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('OAKHURST ASPIRE') and
	Cover.Name in ('Motor')

if	isnull(@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 0) > 0 and
	isnull(@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 105, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R0 and the claim is below R5000', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 105, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R0 and the claim is below R5000', 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 105, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R0 and the claim is R5000 or more', 2500, 0, 20000, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 105, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R0 and the claim is R5000 or more', 2500, 0, 20000, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 105, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R2500 or more and the claim is R5000 or more', 2500, 0, 20000, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 105, 'Flat excess for external brokers and 10% for internal panel', 'Client''s selected excess is R2500 or more and the claim is R5000 or more', 2500, 0, 20000, 0, 0, 0, 0, 0, 1)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 106, 'Accessories', 'Windscreen and specified non-standard accessories', 0, 500, 0, 20, 1, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 106, 'Accessories', 'Windscreen and specified non-standard accessories', 0, 500, 0, 20, 1, 0, 0, 0)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where no 3rd party vehicle is involved or if you claim in the 1st 6 months from the policy start date', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where no 3rd party vehicle is involved or if you claim in the 1st 6 months from the policy start date', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where the driver at the time of the loss is under 30 years old or the driver at the time of the loss has had his/her license for less than 2 years', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where the driver at the time of the loss is under 30 years old or the driver at the time of the loss has had his/her license for less than 2 years', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents occurring between 10pm and 4am or failure to report an incident to the South African Police Service within 24 hours of the incident occurring.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents occurring between 10pm and 4am or failure to report an incident to the South African Police Service within 24 hours of the incident occurring.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where we have not received your Vehicle Inspection Certificate or where your policy requires you to have a tracking device other than a Smart-Box fitted to your vehicle and it is not fitted or fully functional, with the exception of theft or hi-jack claims which will not be covered at all. After 3 months if the Smart-Box is required and not fitted. Replace above VIC or tracking device additional excess.)', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where we have not received your Vehicle Inspection Certificate or where your policy requires you to have a tracking device other than a Smart-Box fitted to your vehicle and it is not fitted or fully functional, with the exception of theft or hi-jack claims which will not be covered at all. After 3 months if the Smart-Box is required and not fitted. Replace above VIC or tracking device additional excess.)', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURST_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where we have not received your Vehicle Inspection Certificate or where your policy requires you to have an Oakhurst Smart-Box fitted to your vehicle and it is not fitted, with the exception of theft or hi-jack claims, in which event you will not be covered at all. After 3 months if the Smart-Box is required and not fitted. Replace above VIC or tracking device additional excess.)', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldDisplayExcessValues, ShouldApplySelectedExcess)
		values (@Oakhurst_OAKHURSTASPIRE_Motor_CoverDefinitionId, 107, 'Additional excesses equal to your basic excess payble in addition to your basic excess (Cumulative)', 'Incidents where we have not received your Vehicle Inspection Certificate or where your policy requires you to have an Oakhurst Smart-Box fitted to your vehicle and it is not fitted, with the exception of theft or hi-jack claims, in which event you will not be covered at all. After 3 months if the Smart-Box is required and not fitted. Replace above VIC or tracking device additional excess.)', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
	end

	


--AA Excess (Motor), Product Codes: 'AA'
declare
	@AA_AA_Motor_CoverDefinitionId int
	
select
	@AA_AA_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AAC') and
	Cover.Name in ('Motor')

if	isnull(@AA_AA_Motor_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 108, 'Motor Vehicles', 'Basic excess - Additional excess may apply', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 109, 'Motorbikes', 'Flat excess - Additional excess as per motor vehicles', 3000, 3000, 3000, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 110, 'Caravans', 'Flat excess', 2500, 2500, 2500, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 111, 'Trailers', 'Flat excess', 2500, 2500, 2500, 0, 0, 0, 0, 0, 0)


		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 112, 'Accessories', 'Specified radio', 750, 750, 750, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 112, 'Accessories ', 'Windscreen replacement (Excluding panoramic glass)', 1500, 1500, 1500, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 112, 'Accessories', 'Vehicle glass (Excluding panoramic glass)', 1500, 1500, 1500, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 112, 'Accessories ', 'Panoramic glass', 1500, 1500, 1500, 0, 0, 0, 0, 0, 0)

		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 113, 'Additional Excess', 'If the incident driver is not the regular driver and is younger than 25 years old', 3000, 3000, 3000, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 113, 'Additional Excess', 'If the incident driver has a drivers licence for less than 2 years', 3000, 3000, 3000, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 113, 'Additional Excess', 'If the incident occurred outside of South Africa and the vehicle is not drivable', 7500, 7500, 7500, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Motor_CoverDefinitionId, 113, 'Additional Excess', 'With regards to a car claim that occurs within the first 6 months of cover', 7500, 7500, 7500, 0, 0, 0, 0, 0, 0)

	end


--AA Excess (All Risk), Product Codes: 'AA'
declare
	@AA_AA_AllRisk_CoverDefinitionId int
	
select
	@AA_AA_AllRisk_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AAC') and
	Cover.Name in ('All Risk')

if	isnull(@AA_AA_AllRisk_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_AllRisk_CoverDefinitionId, 114, 'General', 'Basic excess as per selected excess', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_AllRisk_CoverDefinitionId, 115, 'Factory Fitted Radio', 'If unspecified basic vehicle excess applies', 0, 0, 0, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_AllRisk_CoverDefinitionId, 116, 'Portable Possessions', 'Specified (Flat excess)', 500, 500, 500, 0, 0, 0, 0, 0, 0)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_AllRisk_CoverDefinitionId, 116, 'Portable Possessions', 'Unspecified (Flat excess)', 500, 500, 500, 0, 0, 0, 0, 0, 0)
	end


--AA Excess (Contents), Product Codes: 'AA'
declare
	@AA_AA_Contents_CoverDefinitionId int
	
select
	@AA_AA_Contents_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AAC') and
	Cover.Name in ('Contents')

if	isnull(@AA_AA_Contents_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Contents_CoverDefinitionId, 117, 'Home Contents', 'Minimum R1,500 or maximum R4,500', 0, 1500, 4500, 0, 0, 0, 0, 0, 0)
		
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Contents_CoverDefinitionId, 117, 'Additional Contents Cover', 'No excess payable with the exception of garden furniture', 0, 0, 0, 0, 0, 0, 0, 0, 0)
	end


--AA Excess (Building), Product Codes: 'AA'
declare
	@AA_AA_Building_CoverDefinitionId int
	
select
	@AA_AA_Building_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('AAC') and
	Cover.Name in ('Building')

if	isnull(@AA_AA_Building_CoverDefinitionId, 0) > 0
	begin
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@AA_AA_Building_CoverDefinitionId, 118, 'Buildings', 'Minimum R1,500 or maximum R4,500', 0, 1500, 4500, 0, 0, 0, 0, 0, 0)
	end


	--Domestic Amoour -- aig
	--reference:extracted from aig-460 pdf paage 13-14
	declare
	@Domestic_Amour_Motor_CoverDefinitionId int
	
select
	@Domestic_Amour_Motor_CoverDefinitionId = CoverDefinition.Id
from CoverDefinition
	inner join Product on Product.Id = CoverDefinition.ProductId
	inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId
where
	Product.ProductCode in ('DOMESTIC_ARMOUR') and
	Cover.Name in ('Motor')
	
--select @Domestic_Amour_Motor_CoverDefinitionId
	
if	isnull(@Domestic_Amour_Motor_CoverDefinitionId, 0) > 0
	begin
		--Motor Cars 
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Domestic_Amour_Motor_CoverDefinitionId, 119, 'Motor Cars', 'Basic (5% of claim)', 0, 2000 , 7500, 0, 0, 0, 0, 0, 0)
		
		--If vehicle is driven by or is in the charge or custody of a person under 25 years of age
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Domestic_Amour_Motor_CoverDefinitionId, 119, 'Motor Cars', 'If vehicle is driven by or is in the charge or custody of a person under 25 years of age(5% of claim)', 0, 2000 , 7500, 0, 0, 0, 0, 0, 0)


		--If the insured vehicle is damaged whilst being driven and no other vehicle is damaged in the same incident that gave rise to such damage 
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Domestic_Amour_Motor_CoverDefinitionId, 119, 'Motor Cars', ' If the insured vehicle is damaged whilst being driven and no other vehicle is damaged in the same incident that gave rise to such damage ', 0, 2000 , 0, 0, 0, 0, 0, 0, 0)

		--THEFT - if the vehicle is fitted with an approved immobiliser
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Domestic_Amour_Motor_CoverDefinitionId, 119, 'Motor Cars', 'THEFT - if the vehicle is fitted with an approved immobiliser(5% of claim)', 0, 2000 , 7500, 0, 0, 0, 0, 0, 0)

		--THEFT - if the vehicle DOES NOT have an approved Immobiliser
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Domestic_Amour_Motor_CoverDefinitionId, 119, 'Motor Cars', 'THEFT - if the vehicle DOES NOT have an approved Immobiliser(10% of claim)', 0, 2000 , 0, 0, 0, 0, 0, 0, 0)

		--a light commercial vehicle with a gross vehicle mass of less than 3 500 kg and the insured value is more than R 500 000(10% of claim)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Domestic_Amour_Motor_CoverDefinitionId, 119, 'Motor Cars', 'a light commercial vehicle with a gross vehicle mass of less than 3 500 kg and the insured value is more than R 500 000(10% of claim)', 0, 2500 , 0, 0, 0, 0, 0, 0, 0)

		-- a vehicle with a seating capacity of 9 or 10 (for example minibus type vehicles) including the driver and the insured value is more than R 250 000 (10% of claim)
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Domestic_Amour_Motor_CoverDefinitionId, 119, 'Motor Cars', ' a vehicle with a seating capacity of 9 or 10 (for example minibus type vehicles) including the driver and the insured value is more than R 250 000(10% of claim)', 0, 2500 , 0, 0, 0, 0, 0, 0, 0)

		--Windscreen 
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Domestic_Amour_Motor_CoverDefinitionId, 119, 'Motor Cars', 'Windscreen 25% of claim', 0, 0 , 0, 25, 0, 0, 0, 0, 0)

		--Losses occurring within 30 days of commencement unless there was uninterrupted previous insurance Additional
		insert into ProductAdditionalExcess (CoverDefinitionId, [Index], Category, [Description], ActualExcess, MinExcess, MaxExcess, Percentage, IsPercentageOfClaim, IsPercentageOfItem, IsPercentageOfSumInsured, IsDeleted, ShouldApplySelectedExcess)
		values (@Domestic_Amour_Motor_CoverDefinitionId, 119, 'Motor Cars', 'Losses occurring within 30 days of commencement unless there was uninterrupted previous insurance Additional(10% pf claim)', 0, 2000 , 0, 0, 0, 0, 0, 0, 0)
	end
