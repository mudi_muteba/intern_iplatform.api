﻿DECLARE @Partyid INT

SET  @Partyid= (SELECT TOP 1 Id From [dbo].[Party] WHERE DisplayName =  N'Bryte')

If (@Partyid IS NULL)
BEGIN

	INSERT [dbo].[Party] ([MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated])
	VALUES (0, 2, NULL, N'Bryte', NULL, NULL)

	SET @Partyid = IDENT_CURRENT('Party')
END

IF((SELECT  1 FROM [dbo].[Organization]  WHERE Code = 'ZUR' AND  RegisteredName =  N'Bryte Insurance Company Limited') IS NULL )
BEGIN

INSERT [dbo].[Organization] ([PartyId], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo])
 VALUES (@Partyid,  N'ZUR', N'Bryte Insurance Company Limited', N'Bryte', '01/JAN/1965', '', N'1965/006764/06', N'17703', N'')

--address
INSERT [dbo].[Address] ([PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) 
VALUES (@Partyid, 0, N'Bryte Insurance Company Limited', N'15 Marshall Street', N'', N'Ferreirasdorp', N'', N'Johannesburg', N'2001', NULL, NULL, 4, 1, 1, 1, '01/JAN/1970', NULL)

END

DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @contentsCoverId INT;
DECLARE @multiquoteProductId INT = 27;


SET @id = (SELECT TOP 1 PartyId From [Organization] where TradingName = 'Bryte')
if (@id IS NULL)
Return

SET @Name = N'Bryte ZBox'

 
SET @productid = ( SELECT ID FROM dbo.Product where  Name = @Name AND ProductOwnerID = @id)
SET @contentsCoverId = (SELECT ID from [md].[Cover] WHERE Code = 'CONTENTS')

if (@productid IS NULL)
BEGIN
--Zurich
INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          RiskItemTypeId ,
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          @Name , -- Name - nvarchar(255)
          @id , -- ProductOwnerId - int
          @id , -- ProductProviderId - int
          7 , -- ProductTypeId - int
          N'123456' , -- AgencyNumber - nvarchar(255)
          N'ZURPERS' , -- ProductCode - nvarchar(255)
          CAST(0x00009CD400000000 AS DateTime) , -- StartDate - datetime
          CAST(0x00009CD400000000 AS DateTime) , -- EndDate - datetime
          0 , -- RiskItemTypeId - int
          0 , -- IsDeleted - bit
          N'bryte.png' , -- ImageName - nvarchar(1024)
          NULL , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )

		SET @id = SCOPE_IDENTITY()
		SET @productid = @id;
 END
 ELSE
 BEGIN
	SET @id = @productid
 END


DECLARE @CoverDefinitionId INT; --content
DECLARE @MultiQuoteDummyQuestionId INT;

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @contentsCoverId)

SELECT @MultiQuoteDummyQuestionId = ID FROM QuestionDefinition 
WHERE CoverDefinitionId = 97 AND QuestionId = 1081;

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'Content', @id, @contentsCoverId, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=36 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Subsidence and Landslip', @CoverDefinitionId, 36, NULL, 1, 22, 0, 0, 0,
               N'Does this client want to add subsidence and landslip?', N'' , N'', 0, NULL)
			   end

if not exists(select * from QuestionDefinition where QuestionId=76 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Accidental Damage', @CoverDefinitionId, 76, NULL, 1, 21, 0, 1, 0,
               N'Does this client want to add accidental damage?', N'0' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=1 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Pensioner', @CoverDefinitionId, 1, NULL, 1, 22, 0, 0, 0,
               N'Is the client a pensioner?', N'' , N'', 0, NULL)
			   
			   DECLARE @PensionerId INT;
			   SET @PensionerId = SCOPE_IDENTITY();
			   
			   INSERT INTO MapQuestionDefinition(ParentId, ChildId, IsDeleted)
			   VALUES(@MultiQuoteDummyQuestionId, @PensionerId, 0);
			   
			  end

if not exists(select * from QuestionDefinition where QuestionId=19 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Exclude Theft', @CoverDefinitionId, 19, NULL, 1, 24, 0, 0, 0,
               N'Does the client want to exlude Theft from cover?', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=51 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Property Use', @CoverDefinitionId, 51, NULL, 1, 24, 0, 0, 0,
               N'What is this porperty used for?', N'' , N'', 0, NULL)
			   
			   DECLARE @PropertyUseId INT;
			   SET @PropertyUseId = SCOPE_IDENTITY();
			   
			   INSERT INTO MapQuestionDefinition(ParentId, ChildId, IsDeleted)
			   VALUES(@MultiQuoteDummyQuestionId, @PropertyUseId, 0);
			   
              end

if not exists(select * from QuestionDefinition where QuestionId=130 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Construction nearby', @CoverDefinitionId, 130, NULL, 1, 24, 0, 0, 0,
               N'Is there construction work near the property?', N'' , N'', 0, NULL)
			   
			   DECLARE @ConstructionNearbyId INT;
			   SET @ConstructionNearbyId = SCOPE_IDENTITY();
			   
			   INSERT INTO MapQuestionDefinition(ParentId, ChildId, IsDeleted)
			   VALUES(@MultiQuoteDummyQuestionId, @ConstructionNearbyId, 0);
			   
              end

if not exists(select * from QuestionDefinition where QuestionId=50 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Residence Type', @CoverDefinitionId, 50, NULL, 1, 24, 0, 0, 0,
               N'What type of residence is the property?', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=57 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Unoccupied', @CoverDefinitionId, 57, NULL, 1, 13, 1, 1, 0,
               N'How often is the house unoccupied?', N'101' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=43 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claim Free Group', @CoverDefinitionId, 43, NULL, 1, 19, 0, 1, 0,
               N'What is the CFG for the contents?', N'1' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=100 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Registered Owner ID Number', @CoverDefinitionId, 100, NULL, 1, 250, 0, 0, 0,
               N'What is the ID Number for the registered owner of this property?', N'' , N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])', 0, NULL)
              
			   DECLARE @RegOwnerId INT;
			   SET @RegOwnerId = SCOPE_IDENTITY();
			   
			   INSERT INTO MapQuestionDefinition(ParentId, ChildId, IsDeleted)
			   VALUES(@MultiQuoteDummyQuestionId, @RegOwnerId, 0);
			  
			  end

if not exists(select * from QuestionDefinition where QuestionId=55 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Wall Construction', @CoverDefinitionId, 55, NULL, 1, 11, 0, 1, 0,
               N'What is the wall construction of this property?', N'95' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=56 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Roof Construction', @CoverDefinitionId, 56, NULL, 1, 12, 0, 1, 0,
               N'What is the roof contruction of this property?', N'97' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=11 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Burglar Bars', @CoverDefinitionId, 11, NULL, 1, 15, 0, 0, 0,
               N'Does the house where the content is located have burglar bars?', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=15 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Security Gates', @CoverDefinitionId, 15, NULL, 1, 14, 0, 0, 0,
               N'Does the house where the content is located have security gates?', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=10 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Burglar Alarm Linked To 24 Hr Armed Response', @CoverDefinitionId, 10, NULL, 1, 7, 0, 1, 0,
               N'Burglar Alarm Linked To 24 Hr Armed Response', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=82 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Burglar Alarm Type', @CoverDefinitionId, 82, NULL, 1, 6, 1, 1, 0,
               N'What type of alarm does the house have where the contents is located?', N'260' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=49 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Type Of Residence', @CoverDefinitionId, 49, NULL, 1, 5, 1, 1, 0,
               N'What is the type of the residence where the contents is located?', N'62' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured of the contents?', N'' , N'^[0-9\.]+$', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=14 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Secure Complex', @CoverDefinitionId, 14, NULL, 1, 4, 0, 0, 0,
               N'Is the house where the content is located in a security complex?', N'' , N'', 0, NULL)
              end

-----------///////// Zurich Specific Questions

DECLARE @EnableZurichAssistId INT = 1191;

if not exists(select * from QuestionDefinition where QuestionId=@EnableZurichAssistId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Enable Zurich Assist', @CoverDefinitionId, @EnableZurichAssistId, NULL, 1, 1, 1, 1, 0,
               N'Enable Zurich Assist', N'' , N'', 0, NULL)
              end

DECLARE @QuestionDef_EnableZurichAssistId INT;
SELECT @QuestionDef_EnableZurichAssistId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @EnableZurichAssistId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_EnableZurichAssistId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_EnableZurichAssistId);
END

DECLARE @MarketAdjustmentAmountId INT = 1196;
			  
if not exists(select * from QuestionDefinition where QuestionId=@MarketAdjustmentAmountId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Market Adjustment Amount', @CoverDefinitionId, @MarketAdjustmentAmountId, NULL, 1, 6, 1, 1, 0,
               N'Market Adjustment Amount', N'' , N'^[0-9]+$', 0, NULL)
              end

DECLARE @QuestionDef_MarketAdjustmentAmountId INT;
SELECT @QuestionDef_MarketAdjustmentAmountId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @MarketAdjustmentAmountId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_MarketAdjustmentAmountId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_MarketAdjustmentAmountId);
END

DECLARE @SubjectiveAdjustmentAmountId INT = 1197;
			  
if not exists(select * from QuestionDefinition where QuestionId=@SubjectiveAdjustmentAmountId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Subjective Adjustment Amount', @CoverDefinitionId, @SubjectiveAdjustmentAmountId, NULL, 1, 7, 1, 1, 0,
               N'Subjective Adjustment Amount', N'' , N'^[0-9]+$', 0, NULL)
              end

DECLARE @QuestionDef_SubjectiveAdjustmentAmountId INT;
SELECT @QuestionDef_SubjectiveAdjustmentAmountId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @SubjectiveAdjustmentAmountId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_SubjectiveAdjustmentAmountId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_SubjectiveAdjustmentAmountId);
END

DECLARE @WaterLeakageCoverIndicatorId INT = 1206;

if not exists(select * from QuestionDefinition where QuestionId=@WaterLeakageCoverIndicatorId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Water Leakage Cover', @CoverDefinitionId, @WaterLeakageCoverIndicatorId, NULL, 1, 1, 1, 1, 0,
               N'Water Leakage Cover', N'' , N'', 0, NULL)
              end

DECLARE @QuestionDef_WaterLeakageCoverIndicatorId INT;
SELECT @QuestionDef_WaterLeakageCoverIndicatorId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @WaterLeakageCoverIndicatorId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_WaterLeakageCoverIndicatorId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_WaterLeakageCoverIndicatorId);
END

DECLARE @FloodRiskIndicatorId INT = 1201;

if not exists(select * from QuestionDefinition where QuestionId=@FloodRiskIndicatorId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Flood Risk', @CoverDefinitionId, @FloodRiskIndicatorId, NULL, 1, 1, 1, 1, 0,
               N'Flood Risk', N'' , N'', 0, NULL)
              end

DECLARE @QuestionDef_FloodRiskIndicatorId INT;
SELECT @QuestionDef_FloodRiskIndicatorId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @FloodRiskIndicatorId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_FloodRiskIndicatorId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_FloodRiskIndicatorId);
END

DECLARE @PowerSurgeArrestedSumInsuredId INT = 1199;
			  
if not exists(select * from QuestionDefinition where QuestionId=@PowerSurgeArrestedSumInsuredId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Power surge arrested sum insured', @CoverDefinitionId, @PowerSurgeArrestedSumInsuredId, NULL, 1, 6, 1, 1, 0,
               N'Power surge arrested sum insured', N'' , N'^[0-9]+$', 0, NULL)
              end

DECLARE @QuestionDef_PowerSurgeArrestedSumInsuredId INT;
SELECT @QuestionDef_PowerSurgeArrestedSumInsuredId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @PowerSurgeArrestedSumInsuredId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_PowerSurgeArrestedSumInsuredId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_PowerSurgeArrestedSumInsuredId);
END

DECLARE @ElectricalOrMechanicalBreakdownAppliancesSumInsured INT = 1207;
			  
if not exists(select * from QuestionDefinition where QuestionId=@ElectricalOrMechanicalBreakdownAppliancesSumInsured and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Electrical or Mechanical Appliance Breakdown Sum Insured', @CoverDefinitionId, @ElectricalOrMechanicalBreakdownAppliancesSumInsured, NULL, 1, 6, 1, 1, 0,
               N'Electrical or Mechanical Appliance Breakdown Sum Insured', N'' , N'^[0-9]+$', 0, NULL)
              end

DECLARE @QuestionDef_ElectricalOrMechanicalBreakdownAppliancesSumInsured INT;
SELECT @QuestionDef_ElectricalOrMechanicalBreakdownAppliancesSumInsured = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @ElectricalOrMechanicalBreakdownAppliancesSumInsured;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_ElectricalOrMechanicalBreakdownAppliancesSumInsured)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_ElectricalOrMechanicalBreakdownAppliancesSumInsured);
END

DECLARE @FurnishedBuildingIndicatorId INT = 1208;

if not exists(select * from QuestionDefinition where QuestionId=@FurnishedBuildingIndicatorId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Furnished property', @CoverDefinitionId, @FurnishedBuildingIndicatorId, NULL, 1, 1, 1, 1, 0,
               N'Is this property furnished', N'' , N'', 0, NULL)
              end

DECLARE @QuestionDef_FurnishedBuildingIndicatorId INT;
SELECT @QuestionDef_FurnishedBuildingIndicatorId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @FloodRiskIndicatorId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_FurnishedBuildingIndicatorId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_FurnishedBuildingIndicatorId);
END

DECLARE @VacantDwellingIndicatorId INT = 1204;

if not exists(select * from QuestionDefinition where QuestionId=@VacantDwellingIndicatorId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vacant Dwelling', @CoverDefinitionId, @VacantDwellingIndicatorId, NULL, 1, 1, 1, 1, 0,
               N'Is this property vacant', N'' , N'', 0, NULL)
              end

DECLARE @QuestionDef_VacantDwellingIndicatorId INT;
SELECT @QuestionDef_VacantDwellingIndicatorId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @VacantDwellingIndicatorId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_VacantDwellingIndicatorId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_VacantDwellingIndicatorId);
END

DECLARE @ExtendedHolidayIndicatorId INT = 1205;

if not exists(select * from QuestionDefinition where QuestionId=@ExtendedHolidayIndicatorId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Extended Holiday', @CoverDefinitionId, @ExtendedHolidayIndicatorId, NULL, 1, 1, 1, 1, 0,
               N'Extended Holiday', N'' , N'', 0, NULL)
              end

DECLARE @QuestionDef_ExtendedHolidayIndicatorId INT;
SELECT @QuestionDef_ExtendedHolidayIndicatorId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @ExtendedHolidayIndicatorId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_ExtendedHolidayIndicatorId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_ExtendedHolidayIndicatorId);
END