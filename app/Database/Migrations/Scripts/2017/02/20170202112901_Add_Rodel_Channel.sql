begin tran

	declare @code nvarchar(50)
	declare @Name nvarchar(50)
	declare @Usergroupid int
	declare @channelId int

	set @code = 'Rodel'
	set @Name = 'Rodel'
	set @Usergroupid = 5
	select @channelId = max(Id) + 1 from Channel
	--select max(Id) + 1 from Channel

	if not exists(select * from Channel where Code = @code)
		begin

			insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
			values (@channelId, 'F7B4029D-C1DF-41E6-B8BE-69216A5524AA', 197, 1, 2, null, @name, @code, 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)

			declare @UserId int
			select @UserId = Id from [User] where UserName = 'root@iplatform.co.za'
			exec sp_executesql N'INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3); select SCOPE_IDENTITY()',N'@p0 int,@p1 bit,@p2 int,@p3 int',@p0=@Usergroupid,@p1=0,@p2=@UserId,@p3=@channelId
			exec sp_executesql N'INSERT INTO UserChannel (CreatedAt, ModifiedAt, IsDefault, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3, @p4, @p5); select SCOPE_IDENTITY()',N'@p0 datetime,@p1 datetime,@p2 bit,@p3 bit,@p4 int,@p5 int',@p0='2016-11-04 11:01:55',@p1='2016-11-04 11:01:55',@p2=0,@p3=0,@p4=@UserId,@p5=@channelId
			select @UserId = Id from [User] where UserName = 'admin@iplatform.co.za'
			exec sp_executesql N'INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3); select SCOPE_IDENTITY()',N'@p0 int,@p1 bit,@p2 int,@p3 int',@p0=@Usergroupid,@p1=0,@p2=@UserId,@p3=@channelId
			exec sp_executesql N'INSERT INTO UserChannel (CreatedAt, ModifiedAt, IsDefault, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3, @p4, @p5); select SCOPE_IDENTITY()',N'@p0 datetime,@p1 datetime,@p2 bit,@p3 bit,@p4 int,@p5 int',@p0='2016-11-04 11:01:55',@p1='2016-11-04 11:01:55',@p2=0,@p3=0,@p4=@UserId,@p5=@channelId


		end
	go


commit