﻿
DECLARE @Partyid INT

SET  @Partyid= (SELECT TOP 1 Id From [dbo].[Party] WHERE DisplayName =  N'Bryte')

If (@Partyid IS NULL)
BEGIN

	INSERT [dbo].[Party] ([MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated])
	VALUES (0, 2, NULL, N'Bryte', NULL, NULL)

	SET @Partyid = IDENT_CURRENT('Party')
END

IF((SELECT  1 FROM [dbo].[Organization]  WHERE Code = 'ZUR' AND  RegisteredName =  N'Bryte Insurance Company Limited') IS NULL )
BEGIN

INSERT [dbo].[Organization] ([PartyId], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo])
 VALUES (@Partyid,  N'ZUR', N'Bryte Insurance Company Limited', N'Bryte', '01/JAN/1965', '', N'1965/006764/06', N'17703', N'')

--address
INSERT [dbo].[Address] ([PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) 
VALUES (@Partyid, 0, N'Bryte Insurance Company Limited', N'15 Marshall Street', N'', N'Ferreirasdorp', N'', N'Johannesburg', N'2001', NULL, NULL, 4, 1, 1, 1, '01/JAN/1970', NULL)

END

DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @motorCoverId INT = 218;
DECLARE @multiquoteProductId INT = 27;


SET @id = (SELECT TOP 1 PartyId From [Organization] where TradingName = 'Bryte')
if (@id IS NULL)
Return

SET @Name = N'Bryte ZBox'

 
SET @productid = ( SELECT ID FROM dbo.Product where  Name = @Name AND ProductOwnerID = @id)

if (@productid IS NULL)
BEGIN
--Zurich
INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          RiskItemTypeId ,
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          @Name , -- Name - nvarchar(255)
          @id , -- ProductOwnerId - int
          @id , -- ProductProviderId - int
          7 , -- ProductTypeId - int
          N'123456' , -- AgencyNumber - nvarchar(255)
          N'ZURPERS' , -- ProductCode - nvarchar(255)
          CAST(0x00009CD400000000 AS DateTime) , -- StartDate - datetime
          CAST(0x00009CD400000000 AS DateTime) , -- EndDate - datetime
          0 , -- RiskItemTypeId - int
          0 , -- IsDeleted - bit
          N'bryte.png' , -- ImageName - nvarchar(1024)
          NULL , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )

		SET @id = SCOPE_IDENTITY()
		SET @productid = @id;
 END
 ELSE
 BEGIN
	SET @id = @productid
 END


DECLARE @CoverDefinitionId INT; --motor
DECLARE @MultiQuoteDummyQuestionId INT;

SELECT @MultiQuoteDummyQuestionId = ID FROM QuestionDefinition 
WHERE CoverDefinitionId = 96 AND QuestionId = 1081;

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @motorCoverId)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'Motor', @id, @motorCoverId, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 70, 0, 0, 0,
               N'{Title} {Name} what is the make of the vehicle?', N'' , N'', 5, 4)
              end
			  

if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 80, 0, 0, 0,
               N'{Title} {Name} what is the model of the vehicle?', N'' , N'', 5, 4)
              end

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 1, 0,
               N'I''d like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 5, 4)
              end

if not exists(select * from QuestionDefinition where QuestionId=77 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Car Hire', @CoverDefinitionId, 77, NULL, 1, 20, 0, 1, 0,
               N'Does the client require car hire?', N'237' , N'', 0, NULL)
			   end

if not exists(select * from QuestionDefinition where QuestionId=48 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Class of Use', @CoverDefinitionId, 48, NULL, 1, 7, 1, 1, 0,
               N'{Title} {Name} is this vehicle mainly for private or business use?', N'54' , N'', 0, NULL)
			   end

if not exists(select * from QuestionDefinition where QuestionId=80 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Immobiliser', @CoverDefinitionId, 80, NULL, 1, 210, 0, 0, 0,
               N'Does the vehicle have a factory fitted immobiliser?', N'250' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=7 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Modified/Imported', @CoverDefinitionId, 7, NULL, 1, 10, 0, 1, 0,
               N'Modified/Imported', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Registration Number', @CoverDefinitionId, 97, NULL, 1, 1, 1, 1, 0,
               N'What is the registration number for this vehicle?', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'VIN Number', @CoverDefinitionId, 1010, NULL, 1, 4, 0, 0, 0,
               N'VIN Number', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=74 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Voluntary Excess', @CoverDefinitionId, 74, NULL, 1, 21, 0, 1, 0,
               N'A voluntary excess will reduce a clients premium.', N'201' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=24 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Excess Buster', @CoverDefinitionId, 24, NULL, 1, 21, 0, 0, 0,
               N'Does the client want to waive the excess?', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'I''d like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', N'' , N'^[0-9]+$', 5, 4)
              end

if not exists(select * from QuestionDefinition where QuestionId=72 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Marital Status', @CoverDefinitionId, 72, NULL, 1, 6, 0, 1, 0,
               N'What is the marital status of this client?', N'178' , N'', 0, 5)
              end

if not exists(select * from QuestionDefinition where QuestionId=70 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver Gender', @CoverDefinitionId, 70, NULL, 1, 2, 1, 1, 0,
               N'Main Driver Gender', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=42 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver Date of Birth', @CoverDefinitionId, 42, NULL, 1, 21, 1, 1, 0,
               N'What is the Date of Birth of the main driver of this vehicle?', N'' , N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$', 0, 5)
              end

if not exists(select * from QuestionDefinition where QuestionId=104 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Main Driver ID Number', @CoverDefinitionId, 104, NULL, 1, 315, 0, 0, 0,
               N'What is the ID Number of the main driver of this vehicle?', N'' , N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=100 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Registered Owner ID Number', @CoverDefinitionId, 100, NULL, 1, 250, 0, 0, 0,
               N'What is the ID Number for the registered owner of this vehicle?', N'' , N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=87 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Tracking Device', @CoverDefinitionId, 87, NULL, 1, 4, 1, 1, 0,
               N'Tracking Device', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=14 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Secure Complex', @CoverDefinitionId, 14, NULL, 1, 9, 1, 1, 0,
               N'Is the vehicle parked in a security complex?', N'' , N'', 0, NULL)
			   
			   DECLARE @SecureComplexId INT;
			   SET @SecureComplexId  = SCOPE_IDENTITY();
			   
			   INSERT INTO MapQuestionDefinition(ParentId, ChildId, IsDeleted)
			   VALUES(@MultiQuoteDummyQuestionId, @SecureComplexId, 0);
			   
              end

if not exists(select * from QuestionDefinition where QuestionId=85 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Overnight Parking', @CoverDefinitionId, 85, NULL, 1, 0, 1, 1, 0,
               N'Vehicle Overnight Parking', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=43 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'No Claim Bonus', @CoverDefinitionId, 43, NULL, 1, 19, 0, 1, 0,
               N'How many years has this client been accident free?', N'1' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=53 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Type of Cover', @CoverDefinitionId, 53, NULL, 1, 8, 1, 1, 0,
               N'Would you like full comprehensive cover on this vehicle?', N'89' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 5, 1, 1, 1,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', 5, 4)
              end

if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 4, 1, 1, 0,
               N'Sum Insured', N'' , N'^[0-9]+$', 0, NULL)
              end

-----------///////// Zurich Specific Questions

DECLARE @EnableZurichAssistId INT = 1191;

if not exists(select * from QuestionDefinition where QuestionId=@EnableZurichAssistId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Enable Zurich/Bryte Assist', @CoverDefinitionId, @EnableZurichAssistId, NULL, 1, 1, 1, 1, 0,
               N'Enable Zurich/Bryte Assist', N'' , N'', 0, NULL)
              end

DECLARE @QuestionDef_EnableZurichAssistId INT;
SELECT @QuestionDef_EnableZurichAssistId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @EnableZurichAssistId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_EnableZurichAssistId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_EnableZurichAssistId);
END

DECLARE @SumInsuredIsAgreedValueId INT = 1192;

if not exists(select * from QuestionDefinition where QuestionId=@SumInsuredIsAgreedValueId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured Is Agreed Value', @CoverDefinitionId, @SumInsuredIsAgreedValueId, NULL, 1, 2, 1, 1, 0,
               N'Sum Insured Is Agreed Value', N'' , N'', 1, NULL)
              end

DECLARE @QuestionDef_SumInsuredIsAgreedValueId INT;
SELECT @QuestionDef_SumInsuredIsAgreedValueId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @SumInsuredIsAgreedValueId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_SumInsuredIsAgreedValueId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_SumInsuredIsAgreedValueId);
END

DECLARE @WreckageRemovalSumInsuredId INT = 1193;

if not exists(select * from QuestionDefinition where QuestionId=@WreckageRemovalSumInsuredId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Wreckage removal Sum Insured', @CoverDefinitionId, @WreckageRemovalSumInsuredId, NULL, 1, 3, 1, 1, 0,
               N'Wreckage removal sum insured', N'0' , N'^[0-9]+$', 0, NULL)
              end

DECLARE @QuestionDef_WreckageRemovalSumInsuredId INT;
SELECT @QuestionDef_WreckageRemovalSumInsuredId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @WreckageRemovalSumInsuredId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_WreckageRemovalSumInsuredId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_WreckageRemovalSumInsuredId);
END

DECLARE @LossOfKeysSumInsuredId INT = 1194;
			  
if not exists(select * from QuestionDefinition where QuestionId=@LossOfKeysSumInsuredId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Loss Of Keys Sum Insured', @CoverDefinitionId, @LossOfKeysSumInsuredId, NULL, 1, 4, 1, 1, 0,
               N'Loss Of Keys Sum Insured', N'0' , N'^[0-9]+$', 0, NULL)
              end

DECLARE @QuestionDef_LossOfKeysSumInsuredId INT;
SELECT @QuestionDef_LossOfKeysSumInsuredId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @LossOfKeysSumInsuredId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_LossOfKeysSumInsuredId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_LossOfKeysSumInsuredId);
END

DECLARE @ThirdPartyLiabilityLimitsId INT = 1195;
			  
if not exists(select * from QuestionDefinition where QuestionId=@ThirdPartyLiabilityLimitsId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Third Party Liability Limits', @CoverDefinitionId, @ThirdPartyLiabilityLimitsId, NULL, 1, 5, 1, 1, 0,
               N'Third Party Liability Limits', N'0' , N'^[0-9]+$', 0, NULL)
              end

DECLARE @QuestionDef_ThirdPartyLiabilityLimitsId INT;
SELECT @QuestionDef_ThirdPartyLiabilityLimitsId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @ThirdPartyLiabilityLimitsId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_ThirdPartyLiabilityLimitsId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_ThirdPartyLiabilityLimitsId);
END

DECLARE @MarketAdjustmentAmountId INT = 1196;
			  
if not exists(select * from QuestionDefinition where QuestionId=@MarketAdjustmentAmountId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Market Adjustment Amount', @CoverDefinitionId, @MarketAdjustmentAmountId, NULL, 1, 6, 1, 1, 0,
               N'Market Adjustment Amount', N'0' , N'^[0-9]+$', 0, NULL)
              end

DECLARE @QuestionDef_MarketAdjustmentAmountId INT;
SELECT @QuestionDef_MarketAdjustmentAmountId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @MarketAdjustmentAmountId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_MarketAdjustmentAmountId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_MarketAdjustmentAmountId);
END

DECLARE @SubjectiveAdjustmentAmountId INT = 1197;
			  
if not exists(select * from QuestionDefinition where QuestionId=@SubjectiveAdjustmentAmountId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Subjective Adjustment Amount', @CoverDefinitionId, @SubjectiveAdjustmentAmountId, NULL, 1, 7, 1, 1, 0,
               N'Subjective Adjustment Amount', N'0' , N'^[0-9]+$', 0, NULL)
              end

DECLARE @QuestionDef_SubjectiveAdjustmentAmountId INT;
SELECT @QuestionDef_SubjectiveAdjustmentAmountId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @SubjectiveAdjustmentAmountId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_SubjectiveAdjustmentAmountId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_SubjectiveAdjustmentAmountId);
END
	
DECLARE @CreditShortfallSumInsuredId INT = 1198;
			  
if not exists(select * from QuestionDefinition where QuestionId=@CreditShortfallSumInsuredId and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Credit Shortfall Sum Insured', @CoverDefinitionId, @CreditShortfallSumInsuredId, NULL, 1, 8, 1, 1, 0,
               N'Credit Shortfall Sum Insured', N'0' , N'^[0-9]+$', 0, NULL)
              end

DECLARE @QuestionDef_CreditShortfallSumInsuredId INT;
SELECT @QuestionDef_CreditShortfallSumInsuredId = Id 
FROM QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefinitionId
AND QuestionId = @CreditShortfallSumInsuredId;

IF NOT EXISTS
		(SELECT * FROM MapQuestionDefinition 
		WHERE ParentId = @MultiQuoteDummyQuestionId
		AND ChildId = @QuestionDef_CreditShortfallSumInsuredId)
BEGIN
		INSERT INTO MapQuestionDefinition(ParentId, ChildId)
		VALUES(@MultiQuoteDummyQuestionId, @QuestionDef_CreditShortfallSumInsuredId);
END

------------------//////IPL Quote Settings Upload
DECLARE @ChannelGuid_IPL UNIQUEIDENTIFIER;
SET @ChannelGuid_IPL = '585C68BD-DFFA-496C-B9F4-A880C1D21926';
DECLARE @ChannelId_IPL INT;

IF EXISTS(SELECT Id FROM Channel WHERE SystemId = @ChannelGuid_IPL)
BEGIN
	SELECT @ChannelId_IPL = Id FROM Channel WHERE SystemId = @ChannelGuid_IPL;
	--PRINT @ChannelId_IPL;
	
	INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES
	(@ChannelId_IPL, @ProductId,0, 'Dev','IDS/Upload/ServiceURL','http://196.29.140.159/STPWS/Command.svc'),
	(@ChannelId_IPL, @ProductId,0, 'Dev','IDS/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
	(@ChannelId_IPL, @ProductId,0, 'Dev','IDS/Upload/RetryDelayIncrement','5'),
	(@ChannelId_IPL, @ProductId,0, 'Dev','IDS/Upload/MaxRetries','2'),
	(@ChannelId_IPL, @ProductId,0, 'Dev','IDS/Upload/subject','UPLOADING LEAD TO IDS - Bryte/Zurich'),
	(@ChannelId_IPL, @ProductId,0, 'Dev','IDS/Upload/template','LeadUploadTemplate')
	
	INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES
	(@ChannelId_IPL, @ProductId,0, 'UAT','IDS/Upload/ServiceURL','http://196.29.140.159/STPWS/Command.svc'),
	(@ChannelId_IPL, @ProductId,0, 'UAT','IDS/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
	(@ChannelId_IPL, @ProductId,0, 'UAT','IDS/Upload/RetryDelayIncrement','5'),
	(@ChannelId_IPL, @ProductId,0, 'UAT','IDS/Upload/MaxRetries','2'),
	(@ChannelId_IPL, @ProductId,0, 'UAT','IDS/Upload/subject','UPLOADING LEAD TO IDS - Bryte/Zurich'),
	(@ChannelId_IPL, @ProductId,0, 'UAT','IDS/Upload/template','LeadUploadTemplate')
	
	INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES
	(@ChannelId_IPL, @ProductId,0, 'Staging','IDS/Upload/ServiceURL','http://196.29.140.159/STPWS/Command.svc'),
	(@ChannelId_IPL, @ProductId,0, 'Staging','IDS/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
	(@ChannelId_IPL, @ProductId,0, 'Staging','IDS/Upload/RetryDelayIncrement','5'),
	(@ChannelId_IPL, @ProductId,0, 'Staging','IDS/Upload/MaxRetries','2'),
	(@ChannelId_IPL, @ProductId,0, 'Staging','IDS/Upload/subject','UPLOADING LEAD TO IDS - Bryte/Zurich'),
	(@ChannelId_IPL, @ProductId,0, 'Staging','IDS/Upload/template','LeadUploadTemplate')
	
	INSERT INTO ChannelEvent(EventName, ChannelId, IsDeleted, ProductCode)
					  VALUES('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', @ChannelId_IPL, 0, 'ZURPERS');
	
	DECLARE @ChannelEventId_IPL INT;
	SELECT @ChannelEventId_IPL = SCOPE_IDENTITY();
	
	INSERT INTO ChannelEventTask(TaskName, ChannelEventId, IsDeleted)
						  VALUES(2, @ChannelEventId_IPL, 0);
						  
	IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @ChannelGuid_IPL)
	BEGIN
		INSERT INTO SettingsiRate
			  ([ProductId]
			  ,[Password]
			  ,[AgentCode]
			  ,[UserId]
			  ,[IsDeleted]
			  ,[ProductCode]
			  ,[ChannelSystemId]
			  ,[ChannelId]
			  ,[Environment])
	
		VALUES(@productId,
			   'b1rdOfpr3y',
			   43,
			   'Cardinal',
			   0,
			   'ZURPERS',
			   @ChannelGuid_IPL,
			   @ChannelId_IPL,
			   '')
	END
END

DECLARE @ChannelGuid_AdLease UNIQUEIDENTIFIER;
SET @ChannelGuid_AdLease = 'E7EFCBB1-1DEF-4A03-ACBE-2AAEDA2C29D8';
DECLARE @ChannelId_AdLease INT;

IF EXISTS(SELECT Id FROM Channel WHERE SystemId = @ChannelGuid_AdLease)
BEGIN
	SELECT @ChannelId_AdLease = Id FROM Channel WHERE SystemId = @ChannelGuid_AdLease;
	
	IF NOT EXISTS(SELECT ID FROM SettingsQuoteUpload WHERE [ChannelId] = @ChannelId_AdLease AND [ProductId] = @ProductId AND [Environment] = 'Dev')
	BEGIN
		INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES
		(@ChannelId_AdLease, @ProductId,0, 'Dev','IDS/Upload/ServiceURL','http://196.29.140.159/STPWS/Command.svc'),
		(@ChannelId_AdLease, @ProductId,0, 'Dev','IDS/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
		(@ChannelId_AdLease, @ProductId,0, 'Dev','IDS/Upload/RetryDelayIncrement','5'),
		(@ChannelId_AdLease, @ProductId,0, 'Dev','IDS/Upload/MaxRetries','2'),
		(@ChannelId_AdLease, @ProductId,0, 'Dev','IDS/Upload/subject','UPLOADING LEAD TO IDS - Bryte/Zurich'),
		(@ChannelId_AdLease, @ProductId,0, 'Dev','IDS/Upload/template','LeadUploadTemplate')
	END
	
	IF NOT EXISTS(SELECT ID FROM SettingsQuoteUpload WHERE [ChannelId] = @ChannelId_AdLease AND [ProductId] = @ProductId AND [Environment] = 'UAT')
	BEGIN
		INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES
		(@ChannelId_AdLease, @ProductId,0, 'UAT','IDS/Upload/ServiceURL','http://196.29.140.159/STPWS/Command.svc'),
		(@ChannelId_AdLease, @ProductId,0, 'UAT','IDS/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
		(@ChannelId_AdLease, @ProductId,0, 'UAT','IDS/Upload/RetryDelayIncrement','5'),
		(@ChannelId_AdLease, @ProductId,0, 'UAT','IDS/Upload/MaxRetries','2'),
		(@ChannelId_AdLease, @ProductId,0, 'UAT','IDS/Upload/subject','UPLOADING LEAD TO IDS - Bryte/Zurich'),
		(@ChannelId_AdLease, @ProductId,0, 'UAT','IDS/Upload/template','LeadUploadTemplate')
	END
	
	IF NOT EXISTS(SELECT ID FROM SettingsQuoteUpload WHERE [ChannelId] = @ChannelId_AdLease AND [ProductId] = @ProductId AND [Environment] = 'Staging')
	BEGIN
		INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES
		(@ChannelId_AdLease, @ProductId,0, 'Staging','IDS/Upload/ServiceURL','http://196.29.140.159/STPWS/Command.svc'),
		(@ChannelId_AdLease, @ProductId,0, 'Staging','IDS/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
		(@ChannelId_AdLease, @ProductId,0, 'Staging','IDS/Upload/RetryDelayIncrement','5'),
		(@ChannelId_AdLease, @ProductId,0, 'Staging','IDS/Upload/MaxRetries','2'),
		(@ChannelId_AdLease, @ProductId,0, 'Staging','IDS/Upload/subject','UPLOADING LEAD TO IDS - Bryte/Zurich'),
		(@ChannelId_AdLease, @ProductId,0, 'Staging','IDS/Upload/template','LeadUploadTemplate')
	END
	
	IF NOT EXISTS(SELECT Id FROM ChannelEvent 
							WHERE EventName = 'ExternalQuoteAcceptedWithUnderwritingEnabledEvent'
							AND	  ChannelId = @ChannelId_AdLease
							AND	  ProductCode = 'ZURPERS')
	BEGIN
		INSERT INTO ChannelEvent(EventName, ChannelId, IsDeleted, ProductCode)
						  VALUES('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', @ChannelId_AdLease, 0, 'ZURPERS');
		
		DECLARE @ChannelEventId_AdLease INT;
		SELECT @ChannelEventId_AdLease = SCOPE_IDENTITY();
		
		INSERT INTO ChannelEventTask(TaskName, ChannelEventId, IsDeleted)
							  VALUES(2, @ChannelEventId_AdLease, 0);
	END
						  
	IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @ChannelGuid_AdLease)
	BEGIN
		INSERT INTO SettingsiRate
			  ([ProductId]
			  ,[Password]
			  ,[AgentCode]
			  ,[UserId]
			  ,[IsDeleted]
			  ,[ProductCode]
			  ,[ChannelSystemId]
			  ,[ChannelId]
			  ,[Environment])
	
		VALUES(@productId,
			   'b1rdOfpr3y',
			   43,
			   'Cardinal',
			   0,
			   'ZURPERS',
			   @ChannelGuid_AdLease,
			   @ChannelId_AdLease,
			   '')
	END
END

DECLARE @ChannelGuid_JoeClara UNIQUEIDENTIFIER;
SET @ChannelGuid_JoeClara = '5012C888-BB00-474C-B15F-5EB27CCA22CE';
DECLARE @ChannelId_JoeClara INT;

IF EXISTS(SELECT Id FROM Channel WHERE SystemId = @ChannelGuid_JoeClara)
BEGIN
	SELECT @ChannelId_JoeClara = Id FROM Channel WHERE SystemId = @ChannelGuid_JoeClara;
	
	IF NOT EXISTS(SELECT ID FROM SettingsQuoteUpload WHERE [ChannelId] = @ChannelId_JoeClara AND [ProductId] = @ProductId AND [Environment] = 'Dev')
	BEGIN
		INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES
		(@ChannelId_JoeClara, @ProductId,0, 'Dev','IDS/Upload/ServiceURL','http://196.29.140.159/STPWS/Command.svc'),
		(@ChannelId_JoeClara, @ProductId,0, 'Dev','IDS/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
		(@ChannelId_JoeClara, @ProductId,0, 'Dev','IDS/Upload/RetryDelayIncrement','5'),
		(@ChannelId_JoeClara, @ProductId,0, 'Dev','IDS/Upload/MaxRetries','2'),
		(@ChannelId_JoeClara, @ProductId,0, 'Dev','IDS/Upload/subject','UPLOADING LEAD TO IDS - Bryte/Zurich'),
		(@ChannelId_JoeClara, @ProductId,0, 'Dev','IDS/Upload/template','LeadUploadTemplate')
	END
	
	IF NOT EXISTS(SELECT ID FROM SettingsQuoteUpload WHERE [ChannelId] = @ChannelId_JoeClara AND [ProductId] = @ProductId AND [Environment] = 'UAT')
	BEGIN
		INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES
		(@ChannelId_JoeClara, @ProductId,0, 'UAT','IDS/Upload/ServiceURL','http://196.29.140.159/STPWS/Command.svc'),
		(@ChannelId_JoeClara, @ProductId,0, 'UAT','IDS/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
		(@ChannelId_JoeClara, @ProductId,0, 'UAT','IDS/Upload/RetryDelayIncrement','5'),
		(@ChannelId_JoeClara, @ProductId,0, 'UAT','IDS/Upload/MaxRetries','2'),
		(@ChannelId_JoeClara, @ProductId,0, 'UAT','IDS/Upload/subject','UPLOADING LEAD TO IDS - Bryte/Zurich'),
		(@ChannelId_JoeClara, @ProductId,0, 'UAT','IDS/Upload/template','LeadUploadTemplate')
	END
	
	IF NOT EXISTS(SELECT ID FROM SettingsQuoteUpload WHERE [ChannelId] = @ChannelId_JoeClara AND [ProductId] = @ProductId AND [Environment] = 'Staging')
	BEGIN
		INSERT INTO SettingsQuoteUpload ([ChannelId],[ProductId],[IsDeleted],[Environment],[SettingName],[SettingValue]) VALUES
		(@ChannelId_JoeClara, @ProductId,0, 'Staging','IDS/Upload/ServiceURL','http://196.29.140.159/STPWS/Command.svc'),
		(@ChannelId_JoeClara, @ProductId,0, 'Staging','IDS/Upload/AcceptanceEmailAddress','monitoring@iplatform.co.za'),
		(@ChannelId_JoeClara, @ProductId,0, 'Staging','IDS/Upload/RetryDelayIncrement','5'),
		(@ChannelId_JoeClara, @ProductId,0, 'Staging','IDS/Upload/MaxRetries','2'),
		(@ChannelId_JoeClara, @ProductId,0, 'Staging','IDS/Upload/subject','UPLOADING LEAD TO IDS - Bryte/Zurich'),
		(@ChannelId_JoeClara, @ProductId,0, 'Staging','IDS/Upload/template','LeadUploadTemplate')
	END
	
	IF NOT EXISTS(SELECT Id FROM ChannelEvent 
							WHERE EventName = 'ExternalQuoteAcceptedWithUnderwritingEnabledEvent'
							AND	  ChannelId = @ChannelId_JoeClara
							AND	  ProductCode = 'ZURPERS')
	BEGIN
		INSERT INTO ChannelEvent(EventName, ChannelId, IsDeleted, ProductCode)
						  VALUES('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', @ChannelId_JoeClara, 0, 'ZURPERS');
		
		DECLARE @ChannelEventId_JoeClara INT;
		SELECT @ChannelEventId_JoeClara = SCOPE_IDENTITY();
		
		INSERT INTO ChannelEventTask(TaskName, ChannelEventId, IsDeleted)
							  VALUES(2, @ChannelEventId_JoeClara, 0);
    END
						  
	IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @ChannelGuid_JoeClara)
	BEGIN
		INSERT INTO SettingsiRate
			  ([ProductId]
			  ,[Password]
			  ,[AgentCode]
			  ,[UserId]
			  ,[IsDeleted]
			  ,[ProductCode]
			  ,[ChannelSystemId]
			  ,[ChannelId]
			  ,[Environment])
	
		VALUES(@productId,
			   'b1rdOfpr3y',
			   43,
			   'Cardinal',
			   0,
			   'ZURPERS',
			   @ChannelGuid_JoeClara,
			   @ChannelId_JoeClara,
			   '')
	END
END