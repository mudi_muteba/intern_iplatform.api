﻿if not exists(select * from Report where Name = 'User List')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, Info)
		values
			(
				4,
				2,
				3,
				'User List',
				'Provides a detailed list of users across all channels of an iPlatform instance.',
				'UserList.trdx',
				1,
				1,
				1,
				'
				Provides a detailed list of users across all channels of an iPlatform instance.

				<ul>
					<li>
						<strong>Broker Name</strong>
						<ul><li>The name of the Broker or Insurance Company as setup in iPlatform, also known as a Channel.</li></ul>
					</li>
					<li>
						<strong>Role</strong>
						<ul><li>The role of the user in iPlatform related to the Channel.</li></ul>
					</li>
					<li>
						<strong>Agent Name & Surname</strong>
						<ul><li>The full name of the listed user.</li></ul>
					</li>
					<li>
						<strong>Upload Date</strong>
						<ul><li>The e-mail address of the listed user.</li></ul>
					</li>
					<li>
						<strong>Active</strong>
						<ul><li>Whether or not the user was active in the selected date period.</li></ul>
					</li>
				</ul>
				'
			)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 4,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'User List',
			[Description] = 'Provides a detailed list of users across all channels of an iPlatform instance.',
			SourceFile = 'UserList.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1,
			Info = '
				Provides a detailed list of users across all channels of an iPlatform instance.

				<ul>
					<li>
						<strong>Broker Name</strong>
						<ul><li>The name of the Broker or Insurance Company as setup in iPlatform, also known as a Channel.</li></ul>
					</li>
					<li>
						<strong>Role</strong>
						<ul><li>The role of the user in iPlatform related to the Channel.</li></ul>
					</li>
					<li>
						<strong>Agent Name & Surname</strong>
						<ul><li>The full name of the listed user.</li></ul>
					</li>
					<li>
						<strong>Upload Date</strong>
						<ul><li>The e-mail address of the listed user.</li></ul>
					</li>
					<li>
						<strong>Active</strong>
						<ul><li>Whether or not the user was active in the selected date period.</li></ul>
					</li>
				</ul>
				'
		where
			Name = 'Users'
	end
go

declare @ReportId int

if exists(select * from Report where Name = 'User List')
	begin
		select @ReportId = Id from Report  where Name = 'User List'

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Base Url', 'BaseUrl', '', 2, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'Channels', 'ChannelIds', '', 6, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'From', 'DateFrom', '', 3, 1, 1)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler) values (@ReportId, 'To', 'DateTo', '', 3, 1, 1)
	end

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_User_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_User_Header
	end
go

create procedure Report_CallCentre_User_Header
(
	@ChannelId int
)
as
	select top 1 Code Insurer, '' Color from Channel where IsDefault = 1
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_User_GetUserStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_User_GetUserStatistics
	end
go

create procedure Report_CallCentre_User_GetUserStatistics
(
	@ChannelIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@ChannelIds) > 0)
		begin
			select distinct
				Channel.Name [Channel],
				md.AuthorisationGroup.Name [Role],
				dbo.ToCamelCase(ltrim(rtrim(Individual.FirstName + ' ' + Individual.Surname))) [Agent],
				[User].UserName [EMail],
				case when (select count(AuditLog.Id) from AuditLog where AuditLog.EntityId = [User].Id and AuditLog.AuditEntryType = 'UserAuthenticationSuccess' and AuditLog.RequestDate >= @DateFrom and AuditLog.RequestDate <= @DateTo) > 0
					then 'Yes' else 'No'
				End [Active]
			from [User]
				inner join UserIndividual on UserIndividual.UserId = [User].Id
				inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				inner join UserChannel on UserChannel.UserId = [User].Id
				inner join Channel on Channel.Id = UserChannel.ChannelId
				inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
				inner join md.AuthorisationGroup on md.AuthorisationGroup.Id = UserAuthorisationGroup.AuthorisationGroupId
			where
				Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
				[User].IsDeleted = 0 and
				UserIndividual.IsDeleted = 0 and
				UserChannel.IsDeleted = 0 and
				Channel.IsDeleted = 0 and
				UserAuthorisationGroup.IsDeleted = 0
			order by
				Channel.Name,
				md.AuthorisationGroup.Name
		end
	else
		begin
			select distinct
				Channel.Name [Channel],
				md.AuthorisationGroup.Name [Role],
				dbo.ToCamelCase(ltrim(rtrim(Individual.FirstName + ' ' + Individual.Surname))) [Agent],
				[User].UserName [EMail],
				case when (select count(AuditLog.Id) from AuditLog where AuditLog.EntityId = [User].Id and AuditLog.AuditEntryType = 'UserAuthenticationSuccess' and AuditLog.RequestDate >= @DateFrom and AuditLog.RequestDate <= @DateTo) > 0
					then 'Yes' else 'No'
				End [Active]
			from [User]
				inner join UserIndividual on UserIndividual.UserId = [User].Id
				inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				inner join UserChannel on UserChannel.UserId = [User].Id
				inner join Channel on Channel.Id = UserChannel.ChannelId
				inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
				inner join md.AuthorisationGroup on md.AuthorisationGroup.Id = UserAuthorisationGroup.AuthorisationGroupId
			where
				--Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
				[User].IsDeleted = 0 and
				UserIndividual.IsDeleted = 0 and
				UserChannel.IsDeleted = 0 and
				Channel.IsDeleted = 0 and
				UserAuthorisationGroup.IsDeleted = 0
			order by
				Channel.Name,
				md.AuthorisationGroup.Name
		end
go