﻿DECLARE @BuildingCoverDefinitionId INT;

SELECT @BuildingCoverDefinitionId = cd.Id
FROM CoverDefinition cd
INNER JOIN Product pd ON pd.Id = cd.ProductId
INNER JOIN md.Cover mdc ON mdc.Id = cd.CoverId
WHERE pd.ProductCode = 'ZURPERS'
AND mdc.Code = 'BUILDING';

--PRINT  @BuildingCoverDefinitionId

--------------------------------------

DECLARE @Building_MultiQuoteDummyQuestionId INT

SELECT @Building_MultiQuoteDummyQuestionId = qd.Id
FROM QuestionDefinition qd 
INNER JOIN CoverDefinition cd ON cd.Id = qd.CoverDefinitionId
INNER JOIN Product pd ON pd.Id = cd.ProductId
INNER JOIN md.Cover mdc ON mdc.Id = cd.CoverId
WHERE pd.ProductCode = 'MUL'
AND mdc.Code = 'BUILDING'
AND qd.QuestionId = 1081

--PRINT @Building_MultiQuoteDummyQuestionId

------------------------------------

DECLARE @ContentCoverDefinitionId INT;

SELECT @ContentCoverDefinitionId = cd.Id
FROM CoverDefinition cd
INNER JOIN Product pd ON pd.Id = cd.ProductId
INNER JOIN md.Cover mdc ON mdc.Id = cd.CoverId
WHERE pd.ProductCode = 'ZURPERS'
AND mdc.Code = 'CONTENTS';

--PRINT  @ContentCoverDefinitionId

-------------------------------------

DECLARE @Content_MultiQuoteDummyQuestionId INT

SELECT @Content_MultiQuoteDummyQuestionId = qd.Id
FROM QuestionDefinition qd 
INNER JOIN CoverDefinition cd ON cd.Id = qd.CoverDefinitionId
INNER JOIN Product pd ON pd.Id = cd.ProductId
INNER JOIN md.Cover mdc ON mdc.Id = cd.CoverId
WHERE pd.ProductCode = 'MUL'
AND mdc.Code = 'CONTENTS'
AND qd.QuestionId = 1081

--PRINT @Content_MultiQuoteDummyQuestionId

----------------------------------------

IF NOT EXISTS(SELECT * FROM QuestionDefinition WHERE QuestionId=1202 and CoverDefinitionId = @BuildingCoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Satisfactory Geological Report', @BuildingCoverDefinitionId, 1202, NULL, 1, 22, 0, 0, 0,
               N'Satisfactory Geological Report', N'' , N'', 0, NULL)
			   
			   DECLARE @GeoLogicalReportBuildingId INT;
			   SET @GeoLogicalReportBuildingId = SCOPE_IDENTITY();
			   
			   INSERT INTO MapQuestionDefinition(ParentId, ChildId, IsDeleted)
			   VALUES(@Building_MultiQuoteDummyQuestionId, @GeoLogicalReportBuildingId, 0);
			   
			  end

IF NOT EXISTS(SELECT * FROM QuestionDefinition WHERE QuestionId=1202 and CoverDefinitionId = @ContentCoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Satisfactory Geological Report', @ContentCoverDefinitionId, 1202, NULL, 1, 22, 0, 0, 0,
               N'Satisfactory Geological Report', N'' , N'', 0, NULL)
			   
			   DECLARE @GeoLogicalReportContentId INT;
			   SET @GeoLogicalReportContentId = SCOPE_IDENTITY();
			   
			   INSERT INTO MapQuestionDefinition(ParentId, ChildId, IsDeleted)
			   VALUES(@Content_MultiQuoteDummyQuestionId, @GeoLogicalReportContentId, 0);
			   
			  end

UPDATE QuestionDefinition SET IsDeleted = 1, Hide = 1 WHERE QuestionId = 36 AND CoverDefinitionId = @ContentCoverDefinitionId
