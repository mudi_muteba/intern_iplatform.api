﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'Domestic Armour CC'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  Name= @Name)

SELECT @productid
if (@productid IS NULL)
BEGIN
-- Out of scope
RETURN
 END


------------------House Owners-----------------------
SET @CoverID = 44 			 

SET @CoverName = 'House Owners'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()

END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

--Type of Residence
UPDATE dbo.QuestionDefinition
SET QuestionId = 1555
WHERE
CoverDefinitionId =  @CoverDefinitionId
AND QuestionId = 49


------------------Motorcycle 224-----------------------
SET @CoverID = 224 			 

SET @CoverName = 'Motorcycle'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()

END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

UPDATE QuestionDefinition
SET Hide = 1,
	RequiredForQuote = 0,
	RatingFactor= 0
WHERE QuestionId IN (105,60)
And CoverDefinitionId = @CoverDefinitionId


------------------Watercraft-----------------------
SET @CoverID = 353 			 

SET @CoverName = 'Watercraft'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()

END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

UPDATE QuestionDefinition
SET Hide = 1,
	RequiredForQuote = 0,
	RatingFactor= 0
WHERE QuestionId = 42
And CoverDefinitionId = @CoverDefinitionId