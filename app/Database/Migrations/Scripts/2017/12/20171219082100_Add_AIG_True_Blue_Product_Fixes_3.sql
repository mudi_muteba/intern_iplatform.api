﻿DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'Domestic Armour CC'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  Name= @Name)

SELECT @productid
if (@productid IS NULL)
BEGIN
-- Out of scope
RETURN
 END

------------------AIG Assist-----------------------
SET @CoverID = 374 			 

SET @CoverName = 'AIG Assist'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()

END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'Risk Address', N'' , N'', 0, NULL)
              end


if not exists(select * from ProductAutomaticCovers where ProductId=@productid and CoverDefinitionId = @CoverDefinitionId)
BEGIN	
INSERT INTO dbo.ProductAutomaticCovers(    Description,    IsCompulsary,    IsDeleted,    CoverId,    ProductId,    CoverDefinitionId)
VALUES
(   N'', 
    0, -- IsCompulsary - bit
    0, -- IsDeleted - bit
	44,    -- CoverId - int
    @productid,    -- ProductId - int
    @CoverDefinitionId     -- CoverDefinitionId - int
    )

INSERT INTO dbo.ProductAutomaticCovers(    Description,    IsCompulsary,    IsDeleted,    CoverId,    ProductId,    CoverDefinitionId)
VALUES
(   N'', 
    0, -- IsCompulsary - bit
    0, -- IsDeleted - bit
	78,    -- CoverId - int
    @productid,    -- ProductId - int
    @CoverDefinitionId     -- CoverDefinitionId - int
    )
END

------------------Personal Liability-----------------------
SET @CoverID = 258 			 

SET @CoverName = 'Personal Liability'

SET @CoverDefinitionId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

if not exists(select * from ProductAutomaticCovers where ProductId=@productid and CoverDefinitionId = @CoverDefinitionId)
BEGIN
    INSERT INTO dbo.ProductAutomaticCovers(    Description,    IsCompulsary,    IsDeleted,    CoverId,    ProductId,    CoverDefinitionId)
	VALUES
	(   N'', 
		0, -- IsCompulsary - bit
		0, -- IsDeleted - bit
		NULL,    -- CoverId - int
		@productid,    -- ProductId - int
		@CoverDefinitionId     -- CoverDefinitionId - int
		)
END


------------------Motor-----------------------
SET @CoverID = 218 			 

SET @CoverName = 'Motor'

SET @CoverDefinitionId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

UPDATE dbo.QuestionDefinition
SET RatingFactor= 1,
RequiredForQuote = 1
WHERE questionId IN ( 1535, 1500)
AND CoverDefinitionId = @CoverDefinitionId


------------------Household Contents-----------------------
SET @CoverID = 78 			 

SET @CoverName = 'Household Contents'

SET @CoverDefinitionId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

UPDATE dbo.QuestionDefinition
SET RatingFactor= 1,
RequiredForQuote = 1
WHERE questionId IN ( 1527)
AND CoverDefinitionId = @CoverDefinitionId

UPDATE dbo.QuestionDefinition
SET ReadOnly= 1
WHERE questionId IN ( 89,90,47)
AND CoverDefinitionId = @CoverDefinitionId


------------------House Owners-----------------------
SET @CoverID = 44 			 

SET @CoverName = 'House Owners'

SET @CoverDefinitionId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

if not exists(select * from QuestionDefinition where QuestionId=76 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Accidental Damage', @CoverDefinitionId, 76, NULL, 1, 15, 1, 1, 0,
               N'Does the client want to include accidental damage cover?', N'' , N'', NULL, NULL, NULL)
              end


UPDATE dbo.QuestionDefinition
SET ReadOnly= 1
WHERE questionId IN ( 89,90,47)
AND CoverDefinitionId = @CoverDefinitionId


