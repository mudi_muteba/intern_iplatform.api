﻿DECLARE @ChannelId int, @ProductId int, @PolicyBindingId int, @QuestionId int, @CoverId int, @CoverDefinitionId int;
SET @CoverId = 381;
SELECT @ChannelId = Id FROM Channel WHERE Code = 'AIG TB';
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'AIGDTB';
SELECT @CoverDefinitionId = Id FROM CoverDefinition WHERE ProductId = @ProductId AND CoverId = @CoverId;
SELECT TOP 1 @PolicyBindingId = Id FROM PolicyBinding WHERE ChannelId = @ChannelId AND CoverDefinitionId = @CoverDefinitionId;

if(@PolicyBindingId > 0)
BEGIN
	INSERT INTO PolicyBindingQuestion (Name, VisibleIndex, ToolTip, Reference, IsDeleted, QuestionTypeId) VALUES
	('Payment Terms', 13, 'Payment Terms', 13, 0, 3)

	SELECT @QuestionId = SCOPE_IDENTITY();

	INSERT INTO PolicyBindingQuestionAnswer (Name, Answer, VisibleIndex, Reference, Isdeleted, PolicyBindingQuestionId) VALUES
	('Monthly', 'Monthly', 1, 14, 0, @QuestionId),
	('Yearly', 'Yearly', 1, 15, 0, @QuestionId)

	INSERT INTO PolicyBindingQuestionDefinition (Name, VisibleIndex, IsProposalQuestion, IsDeleted, PolicyBindingQuestionGroupId, PolicyBindingQuestionId, QuestionId, PolicyBindingId) VALUES
	('Payment Terms', 13, 0,0, 5,@QuestionId, 8, @PolicyBindingId)
END

