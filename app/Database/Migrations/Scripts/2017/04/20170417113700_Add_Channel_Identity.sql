﻿	BEGIN TRANSACTION;

	if exists (select * from dbo.sysobjects where id = object_id(N'tmp_Channel')) drop table dbo.tmp_Channel

	--Create temp tables
	CREATE TABLE #DropTempTable(script nvarchar(max));
	CREATE TABLE #CreateTempTable(script nvarchar(max));

	-- insert create constraint for channel fk
	INSERT INTO #CreateTempTable
	SELECT 
	N'
	  ALTER TABLE ' + QUOTENAME(s.name) + N'.'
	  + QUOTENAME(p.name) + N' WITH CHECK ADD CONSTRAINT '
	  + QUOTENAME(f.name) + 'FOREIGN KEY([ChannelId]) REFERENCES [dbo].[Channel] ([Id]);'
	FROM 
	   sys.foreign_keys AS f
	INNER JOIN 
	   sys.foreign_key_columns AS fc 
		  ON f.OBJECT_ID = fc.constraint_object_id
	INNER JOIN 
	   sys.tables t 
		  ON t.OBJECT_ID = fc.referenced_object_id
	INNER JOIN 
	   sys.tables p 
		  ON p.OBJECT_ID = fc.parent_object_id
	INNER JOIN sys.schemas AS s 
		ON p.[schema_id] = s.[schema_id]

	WHERE 
	   OBJECT_NAME (f.referenced_object_id) = 'Channel'
	  AND f.name not in  ('FK_Channel_Channel_ParentChannelIdId', 'FK_Channel_Channel_ParentChannelId');

   -- insert drop constraint for channel fk
	INSERT INTO #DropTempTable
	SELECT 
	N'
	  ALTER TABLE ' + QUOTENAME(s.name) + N'.'
	  + QUOTENAME(p.name) + N' DROP CONSTRAINT '
	  + QUOTENAME(f.name) + ';'
	FROM 
	   sys.foreign_keys AS f
	INNER JOIN 
	   sys.foreign_key_columns AS fc 
		  ON f.OBJECT_ID = fc.constraint_object_id
	INNER JOIN 
	   sys.tables t 
		  ON t.OBJECT_ID = fc.referenced_object_id
	INNER JOIN 
	   sys.tables p 
		  ON p.OBJECT_ID = fc.parent_object_id
	INNER JOIN sys.schemas AS s 
		ON p.[schema_id] = s.[schema_id]
	WHERE 
	   OBJECT_NAME (f.referenced_object_id) = 'Channel'

	-- insert table constraints
	INSERT INTO #DropTempTable
	SELECT 
		N'
		  ALTER TABLE ' + SCHEMA_NAME(schema_id) + N'.'
		  + OBJECT_NAME(parent_object_id) + N' DROP CONSTRAINT '
		  + OBJECT_NAME(OBJECT_ID) + ';'

	FROM sys.objects
	WHERE type_desc LIKE '%CONSTRAINT' AND OBJECT_NAME(parent_object_id) = 'Channel' AND type in ('UQ', 'D')

   -- drop channel fk contraints
	DECLARE @MyCursor CURSOR, @script nvarchar(max)
	SET @MyCursor = CURSOR FOR select script from #DropTempTable      
	OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @script

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC (@script);
			FETCH NEXT FROM @MyCursor INTO @script  
		END
		
	CLOSE @MyCursor 
	DEALLOCATE @MyCursor;


	CREATE TABLE [dbo].[Tmp_Channel](
		[Id] [int] NOT NULL IDENTITY(1, 1),
		[SystemId] [uniqueidentifier] NOT NULL,
		[IsActive] [bit] NOT NULL,
		[ActivatedOn] [datetime] NULL,
		[DeactivatedOn] [datetime] NULL,
		[IsDefault] [bit] NOT NULL,
		[IsDeleted] [bit] NULL,
		[CurrencyId] [int] NULL,
		[DateFormat] [nvarchar](255) NULL,
		[Name] [varchar](100) NULL,
		[LanguageId] [int] NULL,
		[PasswordStrengthEnabled] [bit] NULL,
		[Code] [nvarchar](255) NULL,
		[CountryId] [int] NOT NULL,
		[OrganizationId] [int] NULL,
		[ExternalReference] [nvarchar](255) NULL,
		[RequestBroker] [bit] NULL,
		[RequestAccountExecutive] [bit] NULL,
		)
	
	--insert channel data to tempchannel
	IF EXISTS(SELECT * FROM dbo.Channel)
	BEGIN
	SET IDENTITY_INSERT dbo.Tmp_Channel ON

	INSERT INTO Tmp_Channel ([Id],[SystemId],[IsActive],[ActivatedOn],[DeactivatedOn],[IsDefault],[IsDeleted],[CurrencyId],[DateFormat],[Name],[LanguageId],[PasswordStrengthEnabled],[Code],[CountryId],[OrganizationId],[ExternalReference],[RequestBroker],[RequestAccountExecutive])
	SELECT [Id],[SystemId],[IsActive],[ActivatedOn],[DeactivatedOn],[IsDefault],[IsDeleted],[CurrencyId],[DateFormat],[Name],[LanguageId],[PasswordStrengthEnabled],[Code],[CountryId],[OrganizationId],[ExternalReference],[RequestBroker],[RequestAccountExecutive]
	FROM [dbo].[Channel]

	SET IDENTITY_INSERT dbo.Tmp_Channel OFF
	END

	--drop old channel table 
	DROP TABLE dbo.Channel;

	--rename temp channel table to channel
	EXECUTE sp_rename N'dbo.Tmp_Channel', N'Channel';

	--add primary key constraint
	ALTER TABLE [dbo].[Channel] ADD  CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	ALTER TABLE [dbo].[Channel] ADD  CONSTRAINT [UC_Channel] UNIQUE NONCLUSTERED 
	(
		[SystemId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	-- add channel constraints
	ALTER TABLE [dbo].[Channel] ADD  CONSTRAINT [DF_Channel_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
	ALTER TABLE [dbo].[Channel] ADD  CONSTRAINT [DF_Settings_IsActive]  DEFAULT ((0)) FOR [IsActive]
	ALTER TABLE [dbo].[Channel] ADD  CONSTRAINT [DF_Settings_IsDefault]  DEFAULT ((0)) FOR [IsDefault]
	ALTER TABLE [dbo].[Channel] ADD  CONSTRAINT [DF_Channel_CountryId]  DEFAULT ((197)) FOR [CountryId]

	-- create channel fk constraints
	SET @MyCursor = CURSOR FOR select script from #CreateTempTable      
	OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @script

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC(@script);
			FETCH NEXT FROM @MyCursor INTO @script 
		END
	CLOSE @MyCursor 
	DEALLOCATE @MyCursor;

	ALTER TABLE [dbo].[Channel]  ADD ParentChannelId INT NULL, CONSTRAINT [FK_Channel_Channel_ParentChannelId] FOREIGN KEY([ParentChannelId]) REFERENCES [dbo].[Channel] ([Id])

	-- Drop temp tables
	drop table #DropTempTable;
	drop table #CreateTempTable;
	if exists (select * from dbo.sysobjects where id = object_id(N'tmp_Channel')) drop table dbo.tmp_Channel
	COMMIT TRANSACTION;