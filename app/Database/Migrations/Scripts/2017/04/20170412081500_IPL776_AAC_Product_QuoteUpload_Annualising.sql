﻿DECLARE @Id int, @QuoteId int, @ActualPremium float, @ProductId int
SELECT @ProductId = Id FROM Product WHERE ProductCode = 'AAC' AND IsDeleted <> 1

if(@ProductId > 0)
BEGIN

	DECLARE db_cursor CURSOR FOR  
	SELECT ql.id, ql.QuoteId , (SELECT SUM(premium) FROM QuoteItem WHERE QuoteId = ql.QuoteId) as ActualPremium FROM QuoteUploadLog ql
	inner join Quote q on q.Id = ql.QuoteId
	WHERE ql.Premium = (SELECT SUM(premium)*12 FROM QuoteItem WHERE QuoteId = ql.QuoteId)
	AND ql.ProductId = @ProductId

	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @Id, @QuoteId,  @ActualPremium  

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
 
	 update QuoteUploadLog set Premium = @ActualPremium WHERE Id = @Id AND QuoteId = @QuoteId

		   FETCH NEXT FROM db_cursor INTO @Id, @QuoteId,  @ActualPremium   
	END   

	CLOSE db_cursor   
	DEALLOCATE db_cursor
END