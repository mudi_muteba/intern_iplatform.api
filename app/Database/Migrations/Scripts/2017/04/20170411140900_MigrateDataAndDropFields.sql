﻿IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'MapVapQuestionDefinitionCoverHeader')
BEGIN
	UPDATE MapVapQuestionDefinitionCover
	SET
		CoverId = T.CoverId,
		MapVapQuestionDefinitionId = T.MapVapQuestionDefinitionId,
		MapVapQuestionDefinitionCoverHeaderId = null
	FROM
	(
		SELECT MVQDC.Id,
			MVQDC.CoverId,
			MVQDCH.MapVapQuestionDefinitionId
		FROM MapVapQuestionDefinitionCover AS MVQDC
		INNER JOIN MapVapQuestionDefinitionCoverHeader AS MVQDCH
			ON MVQDC.MapVapQuestionDefinitionCoverHeaderId = MVQDCH.Id
	) AS T
	WHERE MapVapQuestionDefinitionCover.Id = T.Id
	
	ALTER TABLE MapVapQuestionDefinitionCover DROP CONSTRAINT FK_MapVapQuestionDefinitionCover_MapVapQuestionDefinitionCoverHeader_MapVapQuestionDefinitionCoverHeaderId
	ALTER TABLE MapVapQuestionDefinitionCover DROP COLUMN MapVapQuestionDefinitionCoverHeaderId
	DROP TABLE MapVapQuestionDefinitionCoverHeader
END
