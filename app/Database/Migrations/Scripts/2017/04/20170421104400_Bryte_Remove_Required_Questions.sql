﻿DECLARE @ProductId INT;

SELECT @ProductId = Id FROM Product WHERE ProductCode = 'ZURPERS';

UPDATE q
SET q.RequiredForQuote = 0,
	q.RatingFactor = 0
FROM QuestionDefinition q
INNER JOIN CoverDefinition cd 
ON cd.Id = q.CoverDefinitionId
WHERE cd.ProductId = @ProductId
and q.QuestionId IN (1197, 1196)