﻿
 UPDATE mqd
 SET mqd.IsDeleted =0
FROM MapQuestionDefinition mqd



Declare @productid int, @id int, @coverDefId int,  @ParentCoverDefinitionId int,
 @ChildCoverDefinitionId int, @coverID int, @DummyQuestionDefId int, @qdNumberOccupants INT


SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode = 'KPIPERS2')

if (@productid IS NULL)
BEGIN

Return
 END
 ELSE
 BEGIN
	SET @id = @productid
 END


declare @CoverDefinitionId int




----------------------Contents-------------------
SET @CoverId = 78 -- 78 is Contents

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)

IF (@coverDefId IS NULL)
BEGIN
return
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=1427 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Building Occupants', @CoverDefinitionId, 1427, NULL, 1, 11, 0, 0, 0,
               N'Building Occupants', N'11649' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=44 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 months', @CoverDefinitionId, 44, NULL, 1, 29, 1, 1, 0,
               N'How many claims have the client registered in the past 12 months?', N'12' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=45 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 to 24 months', @CoverDefinitionId, 45, NULL, 1, 30, 1, 1, 0,
               N'How many claims have the client registered in the past 12 to 24 months?', N'23' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=46 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 24 to 36 months', @CoverDefinitionId, 46, NULL, 1, 31, 1, 1, 0,
               N'How many claims have the client registered in the past 24 to 36 months?', N'34' , N'', 0, NULL)
              end



--REMOVE Number of occupants question
if  exists(SELECT TOP 1 ID FROM [dbo].[QuestionDefinition] where QuestionId=1438 and CoverDefinitionId = @CoverDefinitionId)
BEGIN

    UPDATE [dbo].[QuestionDefinition] 
    SET IsDeleted = 1 
    WHERE QuestionId=1438 and CoverDefinitionId = @CoverDefinitionId

    SET @qdNumberOccupants = (SELECT TOP 1 ID FROM [dbo].[QuestionDefinition] where QuestionId=1438 and CoverDefinitionId = @CoverDefinitionId)

    UPDATE [dbo].MapQuestionDefinition 
    SET IsDeleted = 1 
    WHERE childId = @qdNumberOccupants 

END

---Map To Multiquote


select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverId
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId 
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)


if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = 1427  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId) SELECT ParentQuestion.Id, ChildQuestion.Id
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 1427) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id

----------------------Motor-------------------
SET @CoverId = 218 -- 218 is Motor

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)

IF (@coverDefId IS NULL)
BEGIN
return
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


---Map To Multiquote


select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverId
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId 
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = 1012  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId) SELECT ParentQuestion.Id, ChildQuestion.Id
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 1012) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id




----------------------Building-------------------
SET @CoverId = 44 -- 44 is Building

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)

IF (@coverDefId IS NULL)
BEGIN
return
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=1427 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Building Occupants', @CoverDefinitionId, 1427, NULL, 1, 11, 0, 0, 0,
               N'Building Occupants', N'11649' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=44 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 months', @CoverDefinitionId, 44, NULL, 1, 29, 1, 1, 0,
               N'How many claims have the client registered in the past 12 months?', N'12' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=45 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 12 to 24 months', @CoverDefinitionId, 45, NULL, 1, 30, 1, 1, 0,
               N'How many claims have the client registered in the past 12 to 24 months?', N'23' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=46 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claims in the past 24 to 36 months', @CoverDefinitionId, 46, NULL, 1, 31, 1, 1, 0,
               N'How many claims have the client registered in the past 24 to 36 months?', N'34' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=114 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Current Insurance Period (In Years)', @CoverDefinitionId, 114, NULL, 1, 28, 1, 1, 0,
               N'How long has the client been insured?', N'' , N'', 0, NULL)
              end


--REMOVE Number of occupants question
if  exists(SELECT TOP 1 ID FROM [dbo].[QuestionDefinition] where QuestionId=1438 and CoverDefinitionId = @CoverDefinitionId)
BEGIN

    UPDATE [dbo].[QuestionDefinition] 
    SET IsDeleted = 1 
    WHERE QuestionId=1438 and CoverDefinitionId = @CoverDefinitionId

    SET @qdNumberOccupants = (SELECT TOP 1 ID FROM [dbo].[QuestionDefinition] where QuestionId=1438 and CoverDefinitionId = @CoverDefinitionId)

    UPDATE [dbo].MapQuestionDefinition 
    SET IsDeleted = 1 
    WHERE childId = @qdNumberOccupants 

END

SELECT * FROM [dbo].[QuestionDefinition] where QuestionId=1438 and CoverDefinitionId = 197


---Map To Multiquote


select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverId
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId 
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = 1427  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId) SELECT ParentQuestion.Id, ChildQuestion.Id
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 1427) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = 44  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId) SELECT ParentQuestion.Id, ChildQuestion.Id
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 44) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = 45  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId) SELECT ParentQuestion.Id, ChildQuestion.Id
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 45) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = 46  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId) SELECT ParentQuestion.Id, ChildQuestion.Id
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 46) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = 114 and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId) SELECT ParentQuestion.Id, ChildQuestion.Id
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = 114) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


---FIX ParentQuestions for KP Buildings

DECLARE @dummyId INT = (SELECT  TOP 1 ID  FROM QuestionDefinition where CoverDefinitionId = 98
and QuestionId = 1081)

--House Owners
Update mqd
Set ParentId = @dummyId
--SELECT mqd.ParentId,  cqd.* 
FROM MapQuestionDefinition mqd
INNER JOIN  QuestionDefinition cqd on cqd.Id = mqd.ChildId
where CoverDefinitionId = 197
AND QuestionId in (
1434,
1435,
1436,
1437
)


Declare @thatchParentId Int = (Select Top 1 Id from QuestionDefinition q where q.QuestionId = 1056 and q.CoverDefinitionId = 197)

Update q 
SET ParentId = @thatchParentId
, VisibleIndex = 29
, GroupIndex = 0
--SELECT * 
FROM 
QuestionDefinition q where q.QuestionId in (1435) and q.CoverDefinitionId = 197


Update q 
SET ParentId = @thatchParentId
, VisibleIndex = 30
, GroupIndex = 0
--SELECT * 
FROM 
QuestionDefinition q where q.QuestionId in (1436) and q.CoverDefinitionId = 197


