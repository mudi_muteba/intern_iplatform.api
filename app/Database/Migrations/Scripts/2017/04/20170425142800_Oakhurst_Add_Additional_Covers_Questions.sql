﻿

DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT,
  @ParentCoverDefinitionId int, @ChildCoverDefinitionId int, @DummyQuestionDefId int, @NewQuestionId int
DECLARE @coverDefId INT
DECLARE @CoverId INT

/*
OAKHURST
OAKHURST ASPIRE
OAKHURST ELITE
OAKHURST ELEGANCE
*/

SET @Name = 'LITEAA'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode = @Name )

if (@productid IS NULL)
BEGIN
    --Oakhurst Product does not exist so now we must first create it (out of scope)
 Return
 END
 ELSE
 BEGIN
	SET @id = @productid
 END

--1. Add Extra Questions to the Oakhurst Motor.
--   Add Contents Cover + Questions
--   Add Building Cover + Questions
--   Add All Risk Cover + Questions 
--1.1. Add new Oakhurst mapped questions 
--2. Remove Motor Mapped Multiquote questions from KP
--3. Add Multiquote Questions

declare @CoverDefinitionId int

----------------------Motor-------------------
SET @CoverId = 218 -- 218 is Motor

SET @coverDefId = ( SELECT TOP 1 ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)

IF (@coverDefId IS NULL)
BEGIN

-- THIS SHOULD EXIST ALREADY
 RETURN

END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=1019 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Second Hand', @CoverDefinitionId, 1019, NULL, 1, 27, 1, 1, 0,
               N'Is the vehicle second hand or new', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1452 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Vehicle Use', @CoverDefinitionId, 1452, NULL, 1, 7, 1, 1, 0,
               N'{Title} {Name} What is this vehicle mainly used for?', N'54' , N'', 0, NULL)
              end



--------------- Map To Multiquote  Motor

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverId
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId 
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 1452 -- Oakhurst Vehicle Use

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id



----------------------Contents-------------------
SET @CoverId = 78 -- 78 is Contents

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'Household Contents', @id, @CoverId, NULL, 1, 0, 0)
SET @CoverDefinitionId = SCOPE_IDENTITY()
 
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the contents is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=1450 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Alarm Type', @CoverDefinitionId, 1450, NULL, 1, 1, 0, 0, 0,
               N'What type of alarm is installed?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=40 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Occupation Date', @CoverDefinitionId, 40, NULL, 1, 13, 1, 1, 0,
               N'When was the house occupied by the insured?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=129 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Near Open Ground', @CoverDefinitionId, 129, NULL, 1, 26, 0, 0, 0,
               N'Is there open ground near the item?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=55 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Wall Construction', @CoverDefinitionId, 55, NULL, 1, 11, 0, 1, 0,
               N'What is the wall construction of this property?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=56 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Roof Construction', @CoverDefinitionId, 56, NULL, 1, 12, 0, 1, 0,
               N'What is the roof contruction of this property?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=57 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Unoccupied', @CoverDefinitionId, 57, NULL, 1, 13, 1, 1, 0,
               N'How often is the house unoccupied?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=15 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Security Gates', @CoverDefinitionId, 15, NULL, 1, 14, 0, 0, 0,
               N'Does the house where the content is located have security gates?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=43 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Claim Free Group', @CoverDefinitionId, 43, NULL, 1, 19, 0, 1, 0,
               N'What is the CFG for the contents?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1451 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Home Use', @CoverDefinitionId, 1451, NULL, 1, 5, 0, 0, 0,
               N'What is the residence used for?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1043 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Rented Building', @CoverDefinitionId, 1043, NULL, 1, 17, 0, 0, 0,
               N'Is this residence rented out?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1069 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Unoccupied By Day', @CoverDefinitionId, 1069, NULL, 1, 23, 0, 0, 0,
               N'Unoccupied By Day', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1036 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Informal Settlement', @CoverDefinitionId, 1036, NULL, 1, 23, 0, 0, 0,
               N'Informal Settlement', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1037 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Commune', @CoverDefinitionId, 1037, NULL, 1, 24, 0, 0, 0,
               N'Commune', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=11 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Burglar Bars', @CoverDefinitionId, 11, NULL, 1, 5, 0, 0, 0,
               N'Does the house where the content is located have burglar bars?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured of the contents?', N'' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=49 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Type Of Residence', @CoverDefinitionId, 49, NULL, 1, 2, 1, 1, 0,
               N'What is the type of the residence where the contents is located?', N'62' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=52 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Situation of Residence', @CoverDefinitionId, 52, NULL, 1, 3, 0, 1, 0,
               N'Where is this residence situated?', N'80' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=18 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Electric Fence', @CoverDefinitionId, 18, NULL, 1, 9, 0, 0, 0,
               N'Does the house where the content is located have an electric fence surrounding the property?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 5, 1, 1, 0,
               N'In which province is the property is located?', N'' , N'', 1, 2)
              end

--------------- Map To Multiquote Contents

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverId
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId 
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 1450 --Oakhurst Alarm Type

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


SET @NewQuestionId = 1451 -- Oakhurst Home Use

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


----------------------Building-------------------
SET @CoverId = 44 -- 44 is Building

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'House Owners', @id, @CoverId, NULL, 1, 0, 0)
 SET @CoverDefinitionId = SCOPE_IDENTITY()

END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the contents is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=55 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Wall Construction', @CoverDefinitionId, 55, NULL, 1, 11, 0, 1, 0,
               N'What is the wall construction of this property?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=56 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Roof Construction', @CoverDefinitionId, 56, NULL, 1, 12, 0, 1, 0,
               N'What is the roof contruction of this property?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured of the contents?', N'' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=36 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Subsidence and Landslip', @CoverDefinitionId, 36, NULL, 1, 13, 0, 0, 0,
               N'Does this client want to add subsidence and landslip?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 5, 1, 1, 0,
               N'In which province is the property is located?', N'' , N'', 1, 2)
              end


----------------------All Risk-------------------
SET @CoverId = 17 -- 17 is All Risk

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'All Risks', @id, @CoverId, NULL, 1, 0, 0)
 SET @CoverDefinitionId = SCOPE_IDENTITY()

END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the contents is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=126 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'All Risk Category', @CoverDefinitionId, 126, NULL, 1, 1, 1, 1, 0,
               N'What category does the item fall under?', N'' , N'', 0, 6)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 5, 1, 1, 0,
               N'What is the sum insured of the item?', N'' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 5, 1, 1, 0,
               N'In which province is the property is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=127 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Description', @CoverDefinitionId, 127, NULL, 1, 2, 1, 1, 0,
               N'Enter a description of the item', N'' , N'', 0, 6)
              end


----- ONLY DO THIS FOR ONE OF THE THE OAKHURST COVERS

-------------- Add to Multiquote Product

SET @id = 27 -- Multiquote Product

--MOTOR
SET @CoverId = 218 -- 218 is Motor

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)

IF (@coverDefId IS NULL)
BEGIN
--NO MULTIQUOTE MOTOR means big problems
Return 


END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=1019 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Second Hand', @CoverDefinitionId, 1019, NULL, 1, 27, 1, 1, 0,
               N'Is the vehicle second hand or new', N'' , N'', 0, NULL)
              end


--------------- Remove Map To Multiquote MOTOR

SET @ProductId = 58 -- King Price's Questions we must remove from the multiquote mappin

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverId
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId 
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 1019 

if exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
    Delete FROM MapQuestionDefinition
    WHERE id in (
    SELECT TOP 1 mq.ID  FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId
    )

--Contents
SET @CoverId = 78 -- 78 is Contents

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)

IF (@coverDefId IS NULL)
BEGIN

 --NO MULTIQUOTE CONTENTS means big problems
 Return
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=1043 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Rented Building', @CoverDefinitionId, 1043, NULL, 1, 17, 0, 0, 0,
               N'Is this residence rented out?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1069 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Unoccupied By Day', @CoverDefinitionId, 1069, NULL, 1, 23, 0, 0, 0,
               N'Unoccupied By Day', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1036 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Informal Settlement', @CoverDefinitionId, 1036, NULL, 1, 23, 0, 0, 0,
               N'Informal Settlement', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1037 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Commune', @CoverDefinitionId, 1037, NULL, 1, 24, 0, 0, 0,
               N'Commune', N'' , N'', 0, NULL)
              end


--------------- Remove Map To Multiquote Contents
select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverId
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId 
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 1043
if exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
    Delete FROM MapQuestionDefinition
    WHERE id in (
    SELECT TOP 1 mq.ID  FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId
    )

SET @NewQuestionId = 1069
if exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
    Delete FROM MapQuestionDefinition
    WHERE id in (
    SELECT TOP 1 mq.ID  FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId
    )

SET @NewQuestionId = 1036
if exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
    Delete FROM MapQuestionDefinition
    WHERE id in (
    SELECT TOP 1 mq.ID  FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId
    )

SET @NewQuestionId = 1037 
if exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
    Delete FROM MapQuestionDefinition
    WHERE id in (
    SELECT TOP 1 mq.ID  FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId
    )

    PRINT('DONE')

