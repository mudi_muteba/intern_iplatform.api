﻿if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Covers') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Covers
	end
go

create procedure Reports_DetailedComparativeQuote_Covers
(
	@ProposalHeaderId int,
	@QuoteIds varchar(50)
)
as
	;with Covers_CTE (CoverId, [Description])
	as 
	(
		select distinct
			md.Cover.Id CoverId,
			case
				when md.Cover.Name = 'AIG Assist'
				then 'VMI Assist'
			else
				md.Cover.Name
			end Description
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
			inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
		where
			ProposalHeader.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			CoverDefinition.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			md.Cover.name <> 'Value Added Products' and
			Quote.Id in (select * from fn_StringListToTable(@QuoteIds))
		)
	select
		CoverId,
		[Description]
	from
		Covers_CTE
	order by
		[Description] asc
go