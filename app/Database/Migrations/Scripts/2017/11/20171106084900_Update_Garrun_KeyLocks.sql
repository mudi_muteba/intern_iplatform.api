Declare @ProductId int
SELECT Top 1 @ProductId = Id FROM Product WHERE ProductCode = 'GARRUN'
if exists (SELECT Top 1 Id FROM Product WHERE ProductCode = 'GARRUN')
BEGIN

	if exists (select * from ProductClaimsQuestionDefinition where ProductId = @ProductId and ClaimsQuestionGroupId = 19 and ClaimsQuestionDefinitionId = 1045)
	BEGIN
		update ProductClaimsQuestionDefinition set IsDeleted = 1 where ProductId = @ProductId and ClaimsQuestionGroupId = 19 and ClaimsQuestionDefinitionId = 1045
	END

END