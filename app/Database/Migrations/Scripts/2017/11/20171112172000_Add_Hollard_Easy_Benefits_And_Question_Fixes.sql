﻿
DECLARE @productId INT, @CoverDefinitionID INT

SET @productId = (Select TOP 1 ID from Product where ProductCode = 'HOLEASYO')
SET @CoverDefinitionID = (SELECT  TOP 1 ID FROM CoverDefinition where ProductID = @productId and CoverId = 218)

UPDATE QuestionDefinition
SET Hide = 1
WHERE QuestionId in (
1099,
77,
1111,
1124)
and CoverDefinitionId = @CoverDefinitionID



UPDATE MapQuestionDefinition 
SET IsDeleted = 0
where ChildId IN (
SELECT ID FROM QuestionDefinition where CoverDefinitionId = @CoverDefinitionID and (QuestionId = 1124 OR QuestionId = 1111)
)


UPDATE QuestionDefinition
 SET Hide = 1
 WHERE  QuestionId = 1081  and CoverDefinitionId in (
SELECT ID FROM CoverDefinition where ProductId = 27 and CoverId = 373)



UPDATE
QuestionDefinition
 SET Hide = 1
 WHERE  QuestionId = 141  and
  CoverDefinitionId in (SELECT ID FROM CoverDefinition where ProductId = 27 and CoverId = 257)


UPDATE
QuestionDefinition
 SET RequiredForQuote = 0,
 	RatingFactor = 0
 WHERE  QuestionId IN ( 1461,1462, 141)  and
  CoverDefinitionId in (SELECT ID FROM CoverDefinition where ProductId = 27 and CoverId = 257)
  
  GO

DECLARE @defaultconstraint sysname

SELECT @defaultconstraint = NAME 
FROM sys.default_constraints 
WHERE parent_object_id = object_ID('dbo.ProductBenefit')
and parent_column_id = 4
select @defaultconstraint
-- declare a "DROP" statement to drop that default constraint
DECLARE @DropStmt NVARCHAR(500)

SET @DropStmt = 'ALTER TABLE dbo.ProductBenefit DROP CONSTRAINT ' + @defaultconstraint

-- drop the constraint
EXEC(@DropStmt)

ALTER TABLE ProductBenefit
ALTER COLUMN Value nvarchar(max);

GO
ALTER TABLE [dbo].[ProductBenefit] ADD  DEFAULT ('') FOR [Value]

GO

DECLARE @productId INT, @CoverDefinitionID INT


  --ORANGE Benefits
	SET @productId = (Select TOP 1 ID from Product where ProductCode = 'HOLEASYO')
	SET @CoverDefinitionID = (SELECT  TOP 1 ID FROM CoverDefinition where ProductID = @productId and CoverId = 218) --MOTOR
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Repair', 'Sum insured', 1,0,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Indemnification', 'Sum insured', 1,1,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Hail damage', 'Sum insured', 1,2,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Keys and remote control units', 'Total cost', 1,3,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Transit cover', 'Sum insured', 1,4,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Emergency services, clean-up and removal of wreckage', 'R10 000', 1,5,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Emergency repair after an accident', 'R5 000', 1,6,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Pre-approved transport, towing and storage', 'Total cost', 1,7,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Unapproved transport, towing and storage', 'R2 000', 1,8,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Windscreen and glass repair - pre-approved', 'R3 000', 1,9,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Windscreen and glass replacement - pre-approved (excluding hail damage)', 'Total cost', 1,10,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Windscreen and glass replacement - pre-approved (hail damage)', 'Total cost', 1,11,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Windscreen and glass - unapproved', 'R3 000', 1,12,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Transfer cover when buying a new car', 'No cover', 1,13,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Car modifications for disability', 'R50 000', 1,14,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Third party liability', 'R5 000 000', 1,15,0)

	SET @CoverDefinitionID = (SELECT  TOP 1 ID FROM CoverDefinition where ProductID = @productId and CoverId = 257) --Personal Accident
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Death benefit', 'R20 000 per person older than 6 years and R10 000 per child up to 6 years old', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Permanent disability benefit', 'R150 000 per person', 1,1,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Medical expenses', 'Actual expenses up to a maximum of R20 000 per person', 1,2,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Return of a body', 'R75 000 per person', 1,3,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Disappearance benefit', 'R20 000 per person older than 6 years and R10 000 per child up to 6 years old', 1,4,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Emergency transportation costs', 'R100 000 per person, maximum of R500 000 per incident', 1,5,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Life support machinery', 'R100 000 per person, maximum of R100 000 per incident', 1,6,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Mobility cover', 'R150 000 per person', 1,7,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Rehabilitation costs', 'R150 000 per person', 1,8,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Return of an injured person ', 'R150 000 per person', 1,9,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Search and rescue', 'R100 000 per person, up to a maximum of R500 000 per incident and up to a maximum of R500 000 in any 12 month period', 1,10,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Trauma counselling', 'R1 000 per trauma counselling visit, up to a maximum of R10 000 per person per incident and up to a maximum of R100 000 in any 12 month period', 1,11,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Trauma and HIV assistance helpline', 'Unlimited access', 1,12,0)


	SET @CoverDefinitionID = (SELECT  TOP 1 ID FROM CoverDefinition where ProductID = @productId and CoverId = 373) --Identity Theft
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Legal costs - identity theft', 'R30 000', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Lost income', 'R750', 1,1,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Payments to creditors', 'R2 500', 1,2,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Other costs ', 'R2 500', 1,3,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Assistance benefits - identity, document and crime', 'Hotel accommodation up to a maximum of R1000 in any 12 months period (in the case of home invasion); R100 pre-paid cell phone airtime (in the case of cell phone theft); debit card pre-loaded with R500 (in the case of bank card theft); locksmith expenses up to a maximum of R1 000 per year; free hired car up to a maximum of 48 hours (in the case of car theft)', 1,4,0)


	SET @CoverDefinitionID = (SELECT  TOP 1 ID FROM CoverDefinition where ProductID = @productId and CoverId = 179) --Legal
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Legal advice helpline', 'Refer to your policy terms and conditions. A waiting period of 30 days after the start of your cover applies', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Pothole damage recovery', 'Refer to your policy terms and conditions', 1,1,0)


	SET @CoverDefinitionID = (SELECT  TOP 1 ID FROM CoverDefinition where ProductID = @productId and CoverId = 258) --Personal Legal Liability
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Comprehensive legal liability', 'R5 000 000 overall limit', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Director''s and officer''s liability', 'R1 000 000', 1,1,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Domestic employee''s liability', 'R1 000 000', 1,2,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Security companies and garden services', 'R1 000 000', 1,3,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Tenant''s liability', 'R2 000 000', 1,4,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Wrongful arrest', 'R100 000', 1,5,0)




	--Purple Benefits
		SET @productId = (Select TOP 1 ID from Product where ProductCode = 'HOLEASYP')
	SET @CoverDefinitionID = (SELECT  TOP 1 ID FROM CoverDefinition where ProductID = @productId and CoverId = 218) --MOTOR
  
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Repair', 'Sum insured', 1,0,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Indemnification', 'Sum insured', 1,1,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Hail damage', 'Sum insured', 1,2,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Keys and remote control units', 'Total cost', 1,3,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Transit cover', 'Sum insured', 1,4,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Emergency services, clean-up and removal of wreckage', 'R25 000', 1,5,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Emergency repair after an accident', 'R10 000', 1,6,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Pre-approved transport, towing and storage', 'Total cost', 1,7,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Unapproved transport, towing and storage', 'R2 000', 1,8,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Windscreen and glass repair - pre-approved', 'R3 000', 1,9,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Windscreen and glass replacement - pre-approved (excluding hail damage)', 'Total cost', 1,10,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Windscreen and glass replacement - pre-approved (hail damage)', 'Total cost', 1,11,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Windscreen and glass - unapproved', 'R3 000', 1,12,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Transfer cover when buying a new car', 'Sum insured of your car listed above', 1,13,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Car modifications for disability', 'R100 000', 1,14,0)
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Third party liability', 'R10 000 000', 1,15,0)


	SET @CoverDefinitionID = (SELECT  TOP 1 ID FROM CoverDefinition where ProductID = @productId and CoverId = 257) --Personal Accident
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Death benefit', 'R100 000 per person 14 years and older, R30 000 per child 6 years or older, but younger than 14 years, and R10 000 per child younger than 6 years', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Permanent disability benefit', 'R250 000 per person', 1,1,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Medical expenses', 'Actual expenses up to a maximum of R50 000 per person', 1,2,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Return of a body', 'R75 000 per person', 1,3,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Disappearance benefit', 'R100 000 per person 14 years and older, R30 000 per child 6 years or older, but younger than 14 years, and R10 000 per child younger than 6 years', 1,4,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Emergency transportation costs', 'R100 000 per person, maximum of R500 000 per incident', 1,5,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Life support machinery', 'R100 000 per person, maximum of R100 000 per incident', 1,6,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Mobility cover', 'R150 000 per person', 1,7,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Rehabilitation costs', 'R150 000 per person', 1,8,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Return of an injured person ', 'R150 000 per person', 1,9,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Search and rescue', 'R100 000 per person, maximum of R500 000 per incident and up to maximum of R500 000 in any 12 month period', 1,10,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Trauma counselling', 'R1 000 per trauma counselling visit, up to a maximum of R10 000 per person per incident and up to a maximum of R100 000 in any 12 month period', 1,11,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Trauma and HIV assistance helpline', 'Unlimited access', 1,12,0)


	SET @CoverDefinitionID = (SELECT  TOP 1 ID FROM CoverDefinition where ProductID = @productId and CoverId = 373) --Identity Theft
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Legal costs - identity theft', 'R30 000', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Lost income', 'R750', 1,1,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Payments to creditors', 'R2 500', 1,2,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Other costs ', 'R2 500', 1,3,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Assistance benefits - identity, document and crime', 'Hotel accommodation up to a maximum of R1000 in any 12 months period (in the case of home invasion); R100 pre-paid cell phone airtime (in the case of cell phone theft); debit card pre-loaded with R500 (in the case of bank card theft); locksmith expenses up to a maximum of R1 000 per year; free hired car up to a maximum of 48 hours (in the case of car theft)', 1,4,0)


	SET @CoverDefinitionID = (SELECT  TOP 1 ID FROM CoverDefinition where ProductID = @productId and CoverId = 179) --Legal
	Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Legal advice helpline', 'Refer to your policy terms and conditions. A waiting period of 30 days after the start of your cover applies', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Pothole damage recovery', 'Refer to your policy terms and conditions', 1,1,0)


	SET @CoverDefinitionID = (SELECT  TOP 1 ID FROM CoverDefinition where ProductID = @productId and CoverId = 258) --Personal Legal Liability
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Comprehensive legal liability', 'R20 000 000 overall limit', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Director''s and officer''s liability', 'R1 000 000', 1,1,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Domestic employee''s liability', 'R1 000 000', 1,2,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Security companies and garden services', 'R1 000 000', 1,3,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Tenant''s liability', 'R2 000 000', 1,4,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Wrongful arrest', 'R100 000', 1,5,0)




