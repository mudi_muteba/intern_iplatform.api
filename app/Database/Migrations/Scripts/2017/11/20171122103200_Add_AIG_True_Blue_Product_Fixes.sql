﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'Domestic Armour CC'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  Name= @Name)

SELECT @productid
if (@productid IS NULL)
BEGIN
-- Out of scope
RETURN
 END


------------------House Owners-----------------------
SET @CoverID = 44 			 

SET @CoverName = 'House Owners'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()

END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=103 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Finance House', @CoverDefinitionId, 103, NULL, 1, 27, 0, 0, 0,
               N'At which institution is the asset financed?', N'' , N'', 0, NULL, 0)
              end


------------------Motorcycle 224-----------------------
SET @CoverID = 224 			 

SET @CoverName = 'Motorcycle'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()

END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

UPDATE QuestionDefinition
SET Hide = 1
WHERE QuestionId = 1537
And CoverDefinitionId = @CoverDefinitionId


------------------Watercraft-----------------------
SET @CoverID = 353 			 

SET @CoverName = 'Watercraft'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()

END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END
if not exists(select * from QuestionDefinition where QuestionId=1552 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Engine Year Of Manufacture', @CoverDefinitionId, 1552, NULL, 1, 44, 0, 0, 0,
               N'What is the year the engine was manufactured?', N'' , N'^[0-9]+$', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=1551 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Engine Make and Model', @CoverDefinitionId, 1551, NULL, 1, 45, 0, 0, 0,
               N'{Title} {Name} what is the make and model of the engine?', N'' , N'', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=1554 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Hull Value', @CoverDefinitionId, 1554, NULL, 1, 40, 0, 0, 0,
               N'What is the value of the hull?', N'' , N'^[0-9]+$', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=1553 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Hull Material', @CoverDefinitionId, 1553, NULL, 1, 41, 0, 0, 0,
               N'{Title} {Name} what is the hull made of?', N'' , N'', 0, NULL, 0)
              end



------------------Personal Accident-----------------------
SET @CoverID = 257 			 

SET @CoverName = 'Personal Accident'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()

END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END



UPDATE QuestionDefinition
SET Hide = 0,
	QuestionGroupId = 2
WHERE QuestionId IN (1537,146,60)
And CoverDefinitionId = @CoverDefinitionId


UPDATE QuestionDefinition
SET Hide = 1,
	RequiredForQuote = 0
WHERE QuestionId IN (141)
And CoverDefinitionId = @CoverDefinitionId
