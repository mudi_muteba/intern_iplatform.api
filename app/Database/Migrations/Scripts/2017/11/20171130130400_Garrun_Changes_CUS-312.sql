Declare @ProductId int;

SELECT @ProductId = Id FROM Product WHERE ProductCode = 'GARRUN'

INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) 
VALUES (@ProductId, 0,1220, 0, 0, 1, 'Other Party 1:Scan Driver Licence', 1, 16),
(@ProductId, 0,1221, 0, 0, 1, 'Other Party 2:Scan Driver Licence', 8, 16);

Update ProductClaimsQuestionDefinition Set IsDeleted = 1 WHERE ClaimsQuestionDefinitionId = 52 AND ProductId = @ProductId AND VisibleIndex = 3 AND ClaimsQuestionGroupId = 14;

Update ProductClaimsQuestionDefinition Set VisibleIndex = 2 WHERE ClaimsQuestionDefinitionId = 71 AND ProductId = @ProductId AND VisibleIndex = 1;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 3 WHERE ClaimsQuestionDefinitionId = 72 AND ProductId = @ProductId AND VisibleIndex = 2;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 4 WHERE ClaimsQuestionDefinitionId = 73 AND ProductId = @ProductId AND VisibleIndex = 3;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 5 WHERE ClaimsQuestionDefinitionId = 74 AND ProductId = @ProductId AND VisibleIndex = 4;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 6 WHERE ClaimsQuestionDefinitionId = 75 AND ProductId = @ProductId AND VisibleIndex = 5;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 7 WHERE ClaimsQuestionDefinitionId = 76 AND ProductId = @ProductId AND VisibleIndex = 6;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 9 WHERE ClaimsQuestionDefinitionId = 77 AND ProductId = @ProductId AND VisibleIndex = 7;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 10 WHERE ClaimsQuestionDefinitionId = 78 AND ProductId = @ProductId AND VisibleIndex = 8;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 11 WHERE ClaimsQuestionDefinitionId = 79 AND ProductId = @ProductId AND VisibleIndex = 9;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 12 WHERE ClaimsQuestionDefinitionId = 80 AND ProductId = @ProductId AND VisibleIndex = 10;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 13 WHERE ClaimsQuestionDefinitionId = 81 AND ProductId = @ProductId AND VisibleIndex = 11;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 14 WHERE ClaimsQuestionDefinitionId = 82 AND ProductId = @ProductId AND VisibleIndex = 12;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 15 WHERE ClaimsQuestionDefinitionId = 83 AND ProductId = @ProductId AND VisibleIndex = 13;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 16 WHERE ClaimsQuestionDefinitionId = 84 AND ProductId = @ProductId AND VisibleIndex = 14;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 17 WHERE ClaimsQuestionDefinitionId = 85 AND ProductId = @ProductId AND VisibleIndex = 15;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 18 WHERE ClaimsQuestionDefinitionId = 86 AND ProductId = @ProductId AND VisibleIndex = 16;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 19 WHERE ClaimsQuestionDefinitionId = 87 AND ProductId = @ProductId AND VisibleIndex = 17;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 20 WHERE ClaimsQuestionDefinitionId = 88 AND ProductId = @ProductId AND VisibleIndex = 18;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 21 WHERE ClaimsQuestionDefinitionId = 90 AND ProductId = @ProductId AND VisibleIndex = 19;
Update ProductClaimsQuestionDefinition Set VisibleIndex = 22 WHERE ClaimsQuestionDefinitionId = 91 AND ProductId = @ProductId AND VisibleIndex = 20;