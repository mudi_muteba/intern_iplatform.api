Declare @ProductId int
SELECT Top 1 @ProductId = Id FROM Product WHERE ProductCode = 'GARRUN'
if exists (SELECT Top 1 Id FROM Product WHERE ProductCode = 'GARRUN')
BEGIN

	if exists (select * from ProductClaimsQuestionDefinition where ProductId = @ProductId and ClaimsQuestionGroupId = 14 and ClaimsQuestionDefinitionId = 49)
	BEGIN
		update ProductClaimsQuestionDefinition set DisplayName = 'Drivers License' where ProductId = @ProductId and ClaimsQuestionGroupId = 14 and ClaimsQuestionDefinitionId = 49
	END

END