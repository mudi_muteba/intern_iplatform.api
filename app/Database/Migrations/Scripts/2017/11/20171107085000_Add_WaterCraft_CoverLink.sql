﻿DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @CoverDefinitionId INT, @CoverID INT ,@CoverName VARCHAR(50)

 
SET @Name = N'MUL'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @Name)

if (@productid IS NULL)
	BEGIN
		PRINT('OUT Of scope to create the product')
	RETURN
 END
 		  	  
----------------------'watercraft'---------------------------------------
select TOP 1 @CoverDefinitionId=Id from CoverDefinition where ProductId=@productid AND CoverId=353

declare @coverlink int
SET @coverlink = (select Id from CoverLink where ChannelId = 7 and CoverDefinitionId=@CoverDefinitionId)

if (@coverlink IS NULL)
	BEGIN		
	insert into CoverLink(ChannelId,VisibleIndex,IsDeleted,CoverDefinitionId)
	values (7,0,0,@CoverDefinitionId)
end