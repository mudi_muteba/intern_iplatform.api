﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'Domestic Armour CC'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  Name= @Name)

SELECT @productid
if (@productid IS NULL)
BEGIN
--out of scope
RETURN
END



--------------------Caravan or Trailer 62-----------------------
SET @CoverID = 62	 			

SET @CoverName = 'Caravan or Trailer'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


UPDATE  QuestionDefinition
SET
QuestionId = 866 --AIG Caravan Type Of Cover
WHERE QuestionId =  53 -- Type Of Cover
AND CoverDefinitionId = @CoverDefinitionId


----------------------End of Caravan Trailer ---------------------------------------



--------------------Computer Equipment 380-----------------------
SET @CoverID = 380	 			

SET @CoverName = 'Computer Equipment'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=1524 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Electronic Item Make', @CoverDefinitionId, 1524, NULL, 1, 60, 0, 0, 0,
               N'What is the Electronic Item Make?', N'' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=1525 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Electronic Item Model', @CoverDefinitionId, 1525, NULL, 1, 70, 0, 0, 0,
               N'What is the Electronic Item Model?', N'' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=128 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Serial Number', @CoverDefinitionId, 128, NULL, 1, 30, 0, 0, 0,
               N'What is the Electronic Item Serial/IMEI Number?', N'' , N'', 0, NULL, 0)
              end
			  
----------------------End of Computer Equipment 380 ---------------------------------------


--------------------Household Contents 78-----------------------
SET @CoverID = 78	 			

SET @CoverName = 'Household Contents'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

UPDATE  QuestionDefinition
SET
QuestionId = 1526 -- AIG Accidental Damage
WHERE QuestionId =  76 -- Accidental Damage
AND CoverDefinitionId = @CoverDefinitionId

if not exists(select * from QuestionDefinition where QuestionId=1527 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Claims In Last 7 years', @CoverDefinitionId, 1527, NULL, 1, 0, 0, 0, 0,
               N'How many claims in the last 7 years', N'' , N'^[0-9]+$', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=1521 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Security Installed', @CoverDefinitionId, 1521, NULL, 1, 0, 0, 0, 0,
               N'Security Installed', N'' , N'', 0, NULL, 0)
              end

UPDATE  QuestionDefinition
SET
QuestionId = 1528 -- AIG Roof Construction
WHERE QuestionId =  56 -- Roof Construction
AND CoverDefinitionId = @CoverDefinitionId

UPDATE  QuestionDefinition
SET
QuestionId = 1529 -- AIG Unoccupied
WHERE QuestionId =  57 -- Unoccupied
AND CoverDefinitionId = @CoverDefinitionId

UPDATE  QuestionDefinition
SET
QuestionId = 1530 -- AIG Voluntary Excess
WHERE QuestionId =  74 -- Voluntary Excess
AND CoverDefinitionId = @CoverDefinitionId

UPDATE  QuestionDefinition
SET
QuestionId = 1531 -- AIG Wall
WHERE QuestionId =  55 -- Wall Construction
AND CoverDefinitionId = @CoverDefinitionId

UPDATE  QuestionDefinition
SET hide = 1,
RequiredForQuote = 0,
RatingFactor = 0
WHERE QuestionId in (	21,
						10,
						18,
						11,
						15,
						82,
						43)
AND CoverDefinitionId = @CoverDefinitionId
	  
----------------------End of Household Contents ---------------------------------------



--------------------House Owners 44-----------------------
SET @CoverID = 44	 			

SET @CoverName = 'House Owners'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=1532 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Residence Is', @CoverDefinitionId, 1532, NULL, 1, 0, 0, 0, 0,
               N'Residence Is', N'' , N'', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=49 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Type Of Residence', @CoverDefinitionId, 49, NULL, 1, 9, 0, 0, 0,
               N'What is the type of the residence where the contents is located?', N'' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=52 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Situation of Residence', @CoverDefinitionId, 52, NULL, 1, 16, 0, 0, 0,
               N'Where is this residence situated?', N'' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=36 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Subsidence and Landslip', @CoverDefinitionId, 36, NULL, 1, 22, 0, 0, 0,
               N'Does this client want to add subsidence and landslip?', N'' , N'', 0, NULL, 0)
              end

UPDATE  QuestionDefinition
SET
QuestionId = 1528 -- AIG Roof Construction
WHERE QuestionId =  56 -- Roof Construction
AND CoverDefinitionId = @CoverDefinitionId

UPDATE  QuestionDefinition
SET
QuestionId = 1529 -- AIG Unoccupied
WHERE QuestionId =  57 -- Unoccupied
AND CoverDefinitionId = @CoverDefinitionId

UPDATE  QuestionDefinition
SET
QuestionId = 1531 -- AIG Wall
WHERE QuestionId =  55 -- Wall Construction
AND CoverDefinitionId = @CoverDefinitionId

UPDATE  QuestionDefinition
SET hide = 1,
RequiredForQuote = 0,
RatingFactor = 0
WHERE QuestionId =	190	
AND CoverDefinitionId = @CoverDefinitionId

----------------------End of House Owners ---------------------------------------

--------------------Motor 218-----------------------
SET @CoverID = 218	 			

SET @CoverName = 'Motor'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

UPDATE  QuestionDefinition
SET
QuestionId = 1534 -- Car Hire
WHERE QuestionId =  77 -- Car Hire
AND CoverDefinitionId = @CoverDefinitionId

UPDATE  QuestionDefinition
SET
QuestionId = 1535 -- Class of use
WHERE QuestionId =  1507 -- Class of use
AND CoverDefinitionId = @CoverDefinitionId

UPDATE  QuestionDefinition
SET hide = 1,
RequiredForQuote = 0,
RatingFactor = 0
WHERE QuestionId =	110	
AND CoverDefinitionId = @CoverDefinitionId

--------------------End of Motor 218-----------------------



--------------------Motorcycle -----------------------
SET @CoverID = 224	 			

SET @CoverName = 'Motorcycle'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

UPDATE  QuestionDefinition
SET hide = 1,
RequiredForQuote = 0,
RatingFactor = 0
WHERE QuestionId IN (43	,64)
AND CoverDefinitionId = @CoverDefinitionId


UPDATE  QuestionDefinition
SET
QuestionId = 53 -- Type of Cover
WHERE QuestionId =  1500 -- Type of Cover
AND CoverDefinitionId = @CoverDefinitionId




if not exists(select * from QuestionDefinition where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Engine Number', @CoverDefinitionId, 1013, NULL, 1, 4, 0, 0, 0,
               N'Engine Number', N'' , N'', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=105 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Driver Age', @CoverDefinitionId, 105, NULL, 1, 22, 0, 0, 0,
               N'What is the drivers age', N'' , N'', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=146 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Title', @CoverDefinitionId, 146, NULL, 1, 61, 0, 0, 0,
               N'Title', N'' , N'', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=1537 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Contact ID Number', @CoverDefinitionId, 1537, NULL, 1, 62, 0, 0, 0,
               N'Contact ID Number', N'' , N'', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=1527 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Claims In Last 7 years', @CoverDefinitionId, 1527, NULL, 1, 0, 0, 0, 0,
               N'How many claims in the last 7 years', N'' , N'^[0-9]+$', 0, NULL, 0)
			   END

--------------------End of Motorcycle-----------------------



--------------------Personal Liability -----------------------
SET @CoverID = 258	 			

SET @CoverName = 'Personal Liability'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=1538 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Insured Name', @CoverDefinitionId, 1538, NULL, 1, 4, 0, 0, 0,
               N'Insured Name', N'' , N'', 0, NULL, 0)
              end


--------------------Personal Liability-----------------------


--------------------Personal Accident -----------------------
SET @CoverID = 257	 			

SET @CoverName = 'Personal Accident'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=146 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Title', @CoverDefinitionId, 146, NULL, 1, 10, 0, 0, 0,
               N'Title', N'' , N'', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=1537 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Contact ID Number', @CoverDefinitionId, 1537, NULL, 1, 30, 0, 0, 0,
               N'Contact ID Number', N'' , N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=1173 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Date Of Birth', @CoverDefinitionId, 1173, NULL, 1, 40, 0, 0, 0,
               N'Date Of Birth', N'' , N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$', 0, NULL, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=60 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Relationship to the Insured', @CoverDefinitionId, 60, NULL, 1, 50, 0, 0, 0,
               N'What is the relationship to the insured?', N'' , N'', NULL, NULL, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=1539 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Name', @CoverDefinitionId, 1539, NULL, 1, 20, 0, 0, 0,
               N'Name', N'' , N'', 0, NULL, 0)
              end


--------------------Personal Accident-----------------------



--------------------Watercraft -----------------------
SET @CoverID = 353	 			

SET @CoverName = 'Watercraft'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 0, 1, 1, 0,
               N'What is the year of manufacture for this vessel?', N'' , N'^[0-9]+$', NULL, NULL, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 1, 1, 1, 0,
               N'What is the make of this vessel?', N'' , N'', NULL, NULL, NULL)
              end
			  
if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 2, 1, 1, 0,
               N'What is the model of this vessel?', N'' , N'', NULL, NULL, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=104 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Registered Owner ID Number', @CoverDefinitionId, 104, NULL, 1, 5, 1, 1, 0,
               N'What is the ID Number for the registered owner of this vessel?', N'' , N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])', NULL, NULL, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=42 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Registered Owner Date of Birth', @CoverDefinitionId, 42, NULL, 1, 15, 1, 1, 0,
               N'What is the Date of Birth of the registered owner of this vessel?', N'' , N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$', NULL, NULL, NULL)
              end

-------------------Watercraft----------------------
