﻿DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @CoverDefinitionId INT, @CoverID INT,@NewQuestionId INT,@ParentCoverDefinitionId INT,@ChildCoverDefinitionId INT,@DummyQuestionDefId INT,@CoverName VARCHAR(50)

SET @Name = 'ZURPERS'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @Name)

if (@productid IS NULL)
	BEGIN
		PRINT('OUT Of scope to create the product')
	RETURN
 END
--------------------258	'Personal Legal Liability'-----------------------
SET @CoverID = 258				

SET @CoverDefinitionId = ( SELECT TOP 1 ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

BEGIN


update QuestionDefinition
set RequiredForQuote=1
where QuestionId=1467 and CoverDefinitionId=@CoverDefinitionId
END