﻿DECLARE @Constraint VARCHAR(255),
	@sql AS VARCHAR(MAX);

IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'CreateLeadActivity' 
		AND C.COLUMN_NAME = 'LeadActivityId'
		AND C.DATA_TYPE = 'bigint'
)
BEGIN
	SELECT
		@Constraint = CCU.CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE AS CCU 
	WHERE CCU.TABLE_NAME = 'CreateLeadActivity'
		AND CCU.COLUMN_NAME = 'LeadActivityId'

	SET @sql = 'ALTER TABLE dbo.CreateLeadActivity DROP CONSTRAINT ' + @Constraint
	EXEC sys.sp_sqlexec @sql

	ALTER TABLE dbo.CreateLeadActivity ALTER COLUMN LeadActivityId INT NOT NULL
	
	SET @sql = 'ALTER TABLE dbo.CreateLeadActivity ADD CONSTRAINT ' + @Constraint + ' PRIMARY KEY (LeadActivityId)'
	EXEC sys.sp_sqlexec @sql	
END

IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'DeadLeadActivity' 
		AND C.COLUMN_NAME = 'LeadActivityId'
		AND C.DATA_TYPE = 'bigint'
)
BEGIN
	SELECT
		@Constraint = CCU.CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE AS CCU 
	WHERE CCU.TABLE_NAME = 'DeadLeadActivity'
		AND CCU.COLUMN_NAME = 'LeadActivityId'
		
	SET @sql = 'ALTER TABLE dbo.DeadLeadActivity DROP CONSTRAINT ' + @Constraint
	EXEC sys.sp_sqlexec @sql

	ALTER TABLE dbo.DeadLeadActivity  ALTER COLUMN LeadActivityId INT NOT NULL

	SET @sql = 'ALTER TABLE dbo.DeadLeadActivity ADD CONSTRAINT ' + @Constraint + ' PRIMARY KEY (LeadActivityId)'
	EXEC sys.sp_sqlexec @sql	
END

IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'DelayLeadActivity' 
		AND C.COLUMN_NAME = 'LeadActivityId'
		AND C.DATA_TYPE = 'bigint'
)
BEGIN
	SELECT
		@Constraint = CCU.CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE AS CCU 
	WHERE CCU.TABLE_NAME = 'DelayLeadActivity'
		AND CCU.COLUMN_NAME = 'LeadActivityId'
		
	SET @sql = 'ALTER TABLE dbo.DelayLeadActivity DROP CONSTRAINT ' + @Constraint
	EXEC sys.sp_sqlexec @sql

	ALTER TABLE dbo.DelayLeadActivity ALTER COLUMN LeadActivityId INT NOT NULL
	
	SET @sql = 'ALTER TABLE dbo.DelayLeadActivity ADD CONSTRAINT ' + @Constraint + ' PRIMARY KEY (LeadActivityId)'
	EXEC sys.sp_sqlexec @sql	
END

IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'ImportedLeadActivity' 
		AND C.COLUMN_NAME = 'LeadActivityId'
		AND C.DATA_TYPE = 'bigint'
)
BEGIN
	SELECT
		@Constraint = CCU.CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE AS CCU 
	WHERE CCU.TABLE_NAME = 'ImportedLeadActivity'
		AND CCU.COLUMN_NAME = 'LeadActivityId'
		
	SET @sql = 'ALTER TABLE dbo.ImportedLeadActivity DROP CONSTRAINT ' + @Constraint
	EXEC sys.sp_sqlexec @sql

	ALTER TABLE dbo.ImportedLeadActivity ALTER COLUMN LeadActivityId INT NOT NULL
	
	SET @sql = 'ALTER TABLE dbo.ImportedLeadActivity ADD CONSTRAINT ' + @Constraint + ' PRIMARY KEY (LeadActivityId)'
	EXEC sys.sp_sqlexec @sql
END

IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'LossLeadActivity' 
		AND C.COLUMN_NAME = 'LeadActivityId'
		AND C.DATA_TYPE = 'bigint'
)
BEGIN
	SELECT
		@Constraint = CCU.CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE AS CCU 
	WHERE CCU.TABLE_NAME = 'LossLeadActivity'
		AND CCU.COLUMN_NAME = 'LeadActivityId'
		
	SET @sql = 'ALTER TABLE dbo.LossLeadActivity DROP CONSTRAINT ' + @Constraint
	EXEC sys.sp_sqlexec @sql

	ALTER TABLE dbo.LossLeadActivity ALTER COLUMN LeadActivityId INT NOT NULL

	SET @sql = 'ALTER TABLE dbo.LossLeadActivity ADD CONSTRAINT ' + @Constraint + ' PRIMARY KEY (LeadActivityId)'
	EXEC sys.sp_sqlexec @sql
END

IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'PolicyLeadActivity' 
		AND C.COLUMN_NAME = 'LeadActivityId'
		AND C.DATA_TYPE = 'bigint'
)
BEGIN
	SELECT
		@Constraint = CCU.CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE AS CCU 
	WHERE CCU.TABLE_NAME = 'PolicyLeadActivity'
		AND CCU.COLUMN_NAME = 'LeadActivityId'
		
	SET @sql = 'ALTER TABLE dbo.PolicyLeadActivity DROP CONSTRAINT ' + @Constraint
	EXEC sys.sp_sqlexec @sql

	ALTER TABLE dbo.PolicyLeadActivity ALTER COLUMN LeadActivityId INT NOT NULL

	SET @sql = 'ALTER TABLE dbo.PolicyLeadActivity ADD CONSTRAINT ' + @Constraint + ' PRIMARY KEY (LeadActivityId)'
	EXEC sys.sp_sqlexec @sql
END

IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'ProposalLeadActivity' 
		AND C.COLUMN_NAME = 'LeadActivityId'
		AND C.DATA_TYPE = 'bigint'
)
BEGIN
	SELECT
		@Constraint = CCU.CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE AS CCU 
	WHERE CCU.TABLE_NAME = 'ProposalLeadActivity'
		AND CCU.COLUMN_NAME = 'LeadActivityId'
		
	SET @sql = 'ALTER TABLE dbo.ProposalLeadActivity DROP CONSTRAINT ' + @Constraint
	EXEC sys.sp_sqlexec @sql

	ALTER TABLE dbo.ProposalLeadActivity ALTER COLUMN LeadActivityId INT NOT NULL
	
	SET @sql = 'ALTER TABLE dbo.ProposalLeadActivity ADD CONSTRAINT ' + @Constraint + ' PRIMARY KEY (LeadActivityId)'
	EXEC sys.sp_sqlexec @sql
END

IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'QuoteAcceptedLeadActivity' 
		AND C.COLUMN_NAME = 'LeadActivityId'
		AND C.DATA_TYPE = 'bigint'
)
BEGIN
	SELECT
		@Constraint = CCU.CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE AS CCU 
	WHERE CCU.TABLE_NAME = 'QuoteAcceptedLeadActivity'
		AND CCU.COLUMN_NAME = 'LeadActivityId'
		
	SET @sql = 'ALTER TABLE dbo.QuoteAcceptedLeadActivity DROP CONSTRAINT ' + @Constraint
	EXEC sys.sp_sqlexec @sql

	ALTER TABLE dbo.QuoteAcceptedLeadActivity ALTER COLUMN LeadActivityId INT NOT NULL
	
	SET @sql = 'ALTER TABLE dbo.QuoteAcceptedLeadActivity ADD CONSTRAINT ' + @Constraint + ' PRIMARY KEY (LeadActivityId)'
	EXEC sys.sp_sqlexec @sql
END

IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'QuotedLeadActivity' 
		AND C.COLUMN_NAME = 'LeadActivityId'
		AND C.DATA_TYPE = 'bigint'
)
BEGIN
	SELECT
		@Constraint = CCU.CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE AS CCU 
	WHERE CCU.TABLE_NAME = 'QuotedLeadActivity'
		AND CCU.COLUMN_NAME = 'LeadActivityId'
		
	SET @sql = 'ALTER TABLE dbo.QuotedLeadActivity DROP CONSTRAINT ' + @Constraint
	EXEC sys.sp_sqlexec @sql

	ALTER TABLE dbo.QuotedLeadActivity ALTER COLUMN LeadActivityId INT NOT NULL
	
	SET @sql = 'ALTER TABLE dbo.QuotedLeadActivity ADD CONSTRAINT ' + @Constraint + ' PRIMARY KEY (LeadActivityId)'
	EXEC sys.sp_sqlexec @sql
END

IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'PartyCorrespondence' 
		AND C.COLUMN_NAME = 'To'
		AND C.DATA_TYPE = 'nvarchar'
)
BEGIN
	ALTER TABLE dbo.PartyCorrespondence  ALTER COLUMN [To] INT 
END

IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'PartyCorrespondence' 
		AND C.COLUMN_NAME = 'From'
		AND C.DATA_TYPE = 'nvarchar'
)
BEGIN
	ALTER TABLE dbo.PartyCorrespondence  ALTER COLUMN [From] INT 
END

IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'ProposalHeader' 
		AND C.COLUMN_NAME = 'LeadActivityId'
		AND C.DATA_TYPE = 'int'
)
BEGIN
	ALTER TABLE dbo.ProposalHeader  ALTER COLUMN LeadActivityId BIGINT
END