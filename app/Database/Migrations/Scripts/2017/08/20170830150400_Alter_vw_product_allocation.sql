﻿/****** Object:  View [dbo].[vw_product_allocation]    Script Date: 30-Aug-2017 01:04:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_product_allocation]
AS
SELECT p.Id, p.ProductCode, p.Name, p.StartDate, 
     p.EndDate, c.Id AS ChannelId, 
     pp.PartyId AS ProductProviderId, 
     pp.TradingName AS ProductProviderTradingName,
      po.PartyId AS ProductOwnerId, 
     po.TradingName AS ProductOwnerTradingName, 0 as IsDeleted, p.IsSelectedExcess, p.IsVoluntaryExcess, p.ShowInReporting
FROM dbo.SettingsiRate AS irs INNER JOIN
     dbo.Channel AS c ON 
     irs.ChannelId = c.Id AND 
     c.IsDeleted = 0 INNER JOIN
     dbo.Product AS p ON irs.ProductId = p.Id AND 
     p.IsDeleted = 0 INNER JOIN
     dbo.Organization AS pp ON 
     p.ProductProviderId = pp.PartyId INNER JOIN
     dbo.Party AS ppp ON pp.PartyId = ppp.Id AND 
     ppp.IsDeleted = 0 INNER JOIN
     dbo.Organization AS po ON 
     p.ProductOwnerId = po.PartyId INNER JOIN
     dbo.Party AS pop ON po.PartyId = pop.Id AND 
     pop.IsDeleted = 0
	 
union all
select p.Id, p.ProductCode, p.Name, p.StartDate, 
     p.EndDate, c.Id AS ChannelId, 
     0 AS ProductProviderId, 
     '' AS ProductProviderTradingName,
     0 AS ProductOwnerId, 
     '' AS ProductOwnerTradingName, 0 as IsDeleted, p.IsSelectedExcess, p.IsVoluntaryExcess, p.ShowInReporting
from product p
	cross join Channel c
where ProductCode = 'MUL'

GO


Update Product Set ShowInReporting = 0 WHERE ProductTypeId = 13;
Update Product Set ShowInReporting = 0 WHERE ProductCode IN ('MULPLX','MULMOT','ALLIN','MOTORSERVICE','MOTORWAR','MOTORWAR1','MOTORWAR2','MOTORWAR3','MOTORWAR4','MOTORWAR5','MOTORWAR6','MOTORWAR7','MOTORWAR8','SCRATC','CUSTOMAPP', 'GARRUN')
Update Product Set ShowInReporting = 1 WHERE ShowInReporting is null