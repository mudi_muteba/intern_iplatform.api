﻿DECLARE @ProductCursor CURSOR, @ProductId int;

SET @ProductCursor = CURSOR FOR SELECT Id FROM Product WHERE IsDeleted != 1;  
OPEN @ProductCursor FETCH NEXT FROM @ProductCursor INTO @ProductId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		print 'looking in Product:'+ STR(@ProductId);

		INSERT INTO CoverDefinition([MasterId],[DisplayName],[ProductId],[CoverId],[ParentId],[CoverDefinitionTypeId],[VisibleIndex],[IsDeleted])VALUES
		(0, 'General Policy Binding', @ProductId, 381, NULL, 1,0,0)

		FETCH NEXT FROM @ProductCursor INTO @ProductId
	END
		
CLOSE @ProductCursor 
DEALLOCATE @ProductCursor;