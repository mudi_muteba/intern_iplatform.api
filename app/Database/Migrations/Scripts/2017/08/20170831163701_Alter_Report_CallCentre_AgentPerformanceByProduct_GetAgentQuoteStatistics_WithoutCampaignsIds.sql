﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[Report_CallCentre_AgentPerformanceByProduct_GetAgentQuoteStatistics_WithoutCampaignsIds]
(
	@DateFrom datetime,
	@DateTo datetime
)
as
	;with Insurers_CTE (InsurerId, CampaignId, Insurer, ProductId, Product, Campaign, ChannelId, AgentId, Channel, Agent)
	as 
	(
		select distinct
			Organization.PartyId,
			Campaign.Id,
			Organization.RegisteredName,
			Product.Id,
			Product.Name,
			Campaign.Name,
			Channel.Id,
			[User].Id,
			Channel.Name,
			Individual.Surname + ', ' + Individual.FirstName
		from QuoteHeader
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
			inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
			inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			inner join Channel on Channel.Id = Campaign.ChannelId
			inner join [User] on [User].Id = LeadActivity.UserId
			inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
			inner join UserIndividual on UserIndividual.UserId = [User].Id
			inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		where
			--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			--UserAuthorisationGroup.AuthorisationGroupId = 1 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			[User].IsActive = 1
		group by
			Organization.PartyId,
			Campaign.Id,
			Organization.RegisteredName,
			Product.Id,
			Product.Name,
			Campaign.Name,
			Channel.Id,
			[User].Id,
			Channel.Name,
			Individual.Surname + ', ' + Individual.FirstName
		)
	select
		Channel,
		Campaign,
		Agent,
		Insurer,
		Product,
		(
			select
				isnull(count(distinct(LeadActivity.LeadId)), 0)
			from LeadActivity
			where
				LeadActivity.CampaignId = Insurers_CTE.CampaignId and
				LeadActivity.UserId = Insurers_CTE.AgentId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo
		) Leads,
		(
			select
				isnull(count(distinct(Quote.Id)), 0)
			from LeadActivity
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			where
				LeadActivity.CampaignId = Insurers_CTE.CampaignId and
				LeadActivity.UserId = Insurers_CTE.AgentId and
				Organization.PartyId = Insurers_CTE.InsurerId and
				Product.Id = Insurers_CTE.ProductId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) Quotes,
		(
			isnull(
				cast(
						(
							select
								cast(isnull(count(distinct(Quote.Id)), 0) as numeric(18, 2))
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
								inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
								inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
								inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							where
								LeadActivity.CampaignId = Insurers_CTE.CampaignId and
								LeadActivity.UserId = Insurers_CTE.AgentId and
								Organization.PartyId = Insurers_CTE.InsurerId and
								Product.Id = Insurers_CTE.ProductId and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								QuoteItem.IsDeleted = 0
						)
					
						/
					
						(
							select
								nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
							from LeadActivity
							where
								LeadActivity.CampaignId = Insurers_CTE.CampaignId and
								LeadActivity.UserId = Insurers_CTE.AgentId and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo
						)
					as numeric(18,2)
				)
			, 0)
		) QuotesPerLead,
		(
			select
				isnull(count(distinct(Quote.Id)), 0)
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			where
				LeadActivity.CampaignId = Insurers_CTE.CampaignId and
				LeadActivity.UserId = Insurers_CTE.AgentId and
				Organization.PartyId = Insurers_CTE.InsurerId and
				Product.Id = Insurers_CTE.ProductId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) SecondLevelUnderwriting,
		(
			select
				isnull(count(distinct(Quote.Id)), 0)
			from LeadActivity
				inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			where
				LeadActivity.CampaignId = Insurers_CTE.CampaignId and
				LeadActivity.UserId = Insurers_CTE.AgentId and
				Organization.PartyId = Insurers_CTE.InsurerId and
				Product.Id = Insurers_CTE.ProductId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) Sales,
		(
			isnull(
				cast
				(
					( 
						select
							cast(isnull(count(distinct(Quote.Id)), 0) as numeric( 18, 2))
						from LeadActivity
							inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							LeadActivity.UserId = Insurers_CTE.AgentId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.Id = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)
					/
					(
						select
							nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
						from LeadActivity
							inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join QuoteHeader on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							LeadActivity.UserId = Insurers_CTE.AgentId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.Id = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)
					* 100
					as numeric( 18, 2)
				)
			, 0)
		) Closing,
		(
			isnull(
				cast
				(
					( 
						select
							cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
						from LeadActivity
							inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							LeadActivity.UserId = Insurers_CTE.AgentId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.Id = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)
					/
					(
						select
							nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
						from LeadActivity
							inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							LeadActivity.UserId = Insurers_CTE.AgentId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.Id = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)
					* 100
					as numeric( 18, 2)
				)
			, 0)
		) InsurerClosing,
		(
			isnull(
				cast
				(
							
					(
						1
						
						-
						(
							( 
								select
									cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)
							/
							(
									select
										nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
									from LeadActivity
										inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
										inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
										inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
										inner join Product on Product.Id = Quote.ProductId
										inner join Organization on Organization.PartyId = Product.ProductOwnerId
										inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
										inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
										inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
									where
										LeadActivity.CampaignId = Insurers_CTE.CampaignId and
										LeadActivity.UserId = Insurers_CTE.AgentId and
										Organization.PartyId = Insurers_CTE.InsurerId and
										Product.Id = Insurers_CTE.ProductId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo and
										QuoteHeader.IsRerated = 0 and
										Quote.IsDeleted = 0 and
										QuoteItem.IsDeleted = 0
							)
						)
					)
					* 100
					as numeric( 18, 2)
				)
			, 0)
		) InsurerDropOff,
		(
			select
				cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
			from
			(
				select
					cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
				from Quote
					inner join SoldLeadActivity on SoldLeadActivity.QuoteId = Quote.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join LeadActivity on LeadActivity.Id = SoldLeadActivity.LeadActivityId
					inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
					inner join Product on Product.Id = Quote.ProductId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				where
					LeadActivity.CampaignId = Insurers_CTE.CampaignId and
					LeadActivity.UserId = Insurers_CTE.AgentId and
					Organization.PartyId = Insurers_CTE.InsurerId and
					Product.Id = Insurers_CTE.ProductId and
					LeadActivity.DateUpdated >= @DateFrom and
					LeadActivity.DateUpdated <= @DateTo and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Quote.Id
			) x
		) AveragePremiumPerAcceptedQuote
	from
		Insurers_CTE
	order by
		Channel,
		Campaign,
		Agent,
		Insurer,
		Product
