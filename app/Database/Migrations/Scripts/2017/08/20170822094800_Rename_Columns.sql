﻿IF EXISTS
(
	SELECT *
	FROM INFORMATION_SCHEMA.COLUMNS AS C
	WHERE C.TABLE_NAME = 'SalesStructure' 
		AND (C.COLUMN_NAME = 'SalesTagId' OR C.COLUMN_NAME = 'ParentSalesTagId')
)
BEGIN
	DECLARE @sql AS VARCHAR(MAX);
	SET @sql =
		'UPDATE SalesStructure
		SET
			BranchSalesTagId = SalesTagId,
			CompanySalesTagId = ParentSalesTagId;

		ALTER TABLE dbo.SalesStructure DROP CONSTRAINT FK_SalesStructure_SalesTag_SalesTagId;
		ALTER TABLE dbo.SalesStructure DROP COLUMN SalesTagId;
		ALTER TABLE dbo.SalesStructure DROP CONSTRAINT FK_SalesStructure_SalesTag_ParentSalesTagId;
		ALTER TABLE dbo.SalesStructure DROP COLUMN ParentSalesTagId;';

	EXEC sys.sp_sqlexec @sql;
END