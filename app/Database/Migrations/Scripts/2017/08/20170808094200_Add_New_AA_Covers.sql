﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'AA Combined'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  NAme= @Name)

if (@productid IS NULL)
	BEGIN
		PRINT('OUT Of scope to create the product')
	RETURN
 END


--------------------Household Contents-----------------------
SET @CoverID = 78	

SET @CoverName = 'Household Contents'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=1480 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'AA Home Security', @CoverDefinitionId, 1480, NULL, 1, 6, 1, 1, 0,
               N'Home Security?', N'' , N'', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=1481 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'AA Type Of Cover', @CoverDefinitionId, 1481, NULL, 1, 1, 1, 1, 0,
               N'What is the type of cover required for the contents?', N'' , N'', 0, NULL)
              END

if not exists(select * from QuestionDefinition where QuestionId=101 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'ID Number', @CoverDefinitionId, 101, NULL, 1, 0, 0, 0, 0,
               N'What is the ID Number of the insured?', N'' , N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])', 0, NULL)
              end



---Map To Multiquote

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverId
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId 
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)



SET @NewQuestionId = 1480 -- AA Home Security

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


SET @NewQuestionId = 1481 -- AA Type Of Cover

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


----------------------End of Household contents---------------------------------------






--------------------62	Caravan or Trailer-----------------------
SET @CoverID = 62		

SET @CoverName = 'Caravan or Trailer'

--MULTIQUOTE
if not exists(select * from QuestionDefinition where QuestionId=1081 and CoverDefinitionId = 200)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Im a dummy question', 200, 1081, NULL, 1, 4, 0, 0, 0,
               N'This a dummy question for mapping MultiQoute Questions', N'NULL' , N'NULL', 0, NULL, 1)
              end

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 0, 0, 0, 1,
               N'What is the year of manufacture for this vehicle?', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 0, 1,
               N'Id like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 1, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 1,
               N'What is the sum insured of the vehicle?', N'' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=139 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Address', @CoverDefinitionId, 139, NULL, 1, 1, 0, 0, 1,
               N'{Title} {Name} would you please provide me with your address?', N'' , N'', 2, NULL, 1)
              end
if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 1, 0, 0, 1,
               N'What is the make of this vehicle?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 2, 0, 0, 1,
               N'What is the model of this vehicle?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 3, 0, 0, 1,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=1165 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Contents Value', @CoverDefinitionId, 1165, NULL, 1, 6, 0, 0, 1,
               N'What is the value of the internal contents?', N'' , N'^[0-9]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=1166 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Type', @CoverDefinitionId, 1166, NULL, 1, 7, 0, 0, 1,
               N'Is this a caravan or trailer?', N'' , N'', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=43 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'NCB', @CoverDefinitionId, 44, NULL, 1, 22, 0, 0, 0,
               N'NCB', N'' , N'', 0, NULL)
              end

----------------------End of Caravan Trailer ---------------------------------------


--------------------258	Personal Legal Liability-----------------------
SET @CoverID = 258			

SET @CoverName = 'Personal Liability'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=1489 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'AA Personal Liability Limit', @CoverDefinitionId, 1489, NULL, 1, 12, 0, 0, 1,
               N'AA Personal Liability Limit', N'3000000' , N'^[0-9\.]+$', 0, NULL)
              end


---Map To Multiquote

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverID
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverID   
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 1489 -- Personal Liability

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id

-- HIde Unused Question

UPDATE QuestionDefinition
SET Hide = 1,
RatingFactor = 0,
RequiredForQuote = 0
WHERE
CoverDefinitionId = @CoverDefinitionId AND QuestionId = 1464



----------------------End of Personal Liability ---------------------------------------




--------------------129	Extended Personal Legal Liability-----------------------
SET @CoverID = 129				

SET @CoverName = 'Extended Personal Liability'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'0' , N'^[0-9\.]+$', 0, NULL)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2)
              end
if not exists(select * from QuestionDefinition where QuestionId=1464 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'AA Limit of Indemnity', @CoverDefinitionId, 1464, NULL, 1, 4, 0, 0, 0,
               N'Personal Liability for Combined Insurance at Full Quote', N'' , N'', 0, NULL)
              end


----------------------End of Personal Liability ---------------------------------------


--------------------380 Computer Equipment-----------------------
SET @CoverID = 380 			

SET @CoverName = 'Computer Equipment'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2)
              end

if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2)
              end
              
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'0' , N'^[0-9\.]+$', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2)
              END

if not exists(select * from QuestionDefinition where QuestionId=1484 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Type Of Computer', @CoverDefinitionId, 1484, NULL, 1, 32, 0, 0, 0,
               N'What is the type of the computer equipment', N'' , N'', 0, NULL)
			END           




----------------------End of Computer Equipment---------------------------------------


--------------------353	Watercraft-----------------------
SET @CoverID = 353	 			

SET @CoverName = 'Watercraft'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2)
              end

if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2)
              end
              
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'0' , N'^[0-9\.]+$', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2)
              END

if not exists(select * from QuestionDefinition where QuestionId=1485 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Watercraft Type', @CoverDefinitionId, 1485, NULL, 1, 32, 0, 0, 0,
               N'What is the type of the watercraft?', N'' , N'', 0, NULL)
              END              

----------------------End of Computer Equipment---------------------------------------

--------------------257	Personal Accident-----------------------
SET @CoverID = 257		 			


select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverID
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverID   
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 1463 -- CON Occupation

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id

----------------------End of Personal Accident---------------------------------------




--------------------224	MotorCycle-----------------------
SET @CoverID = 224	 			

SET @CoverName = 'Motorcycle'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=140 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Overnight Address', @CoverDefinitionId, 140, NULL, 1, 0, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the overnight address?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 1, 0,
               N'Id like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 5, 4, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the suburb and town name where you live?', N'' , N'', 1, 2, 1)
              end
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 2, 1, 1, 0,
               N'In which province is this vehicle mainly operated in?', N'' , N'', 1, 2, 1)
              end
if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'Id like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', N'' , N'^[0-9]+$', 5, 4, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 3, 1, 1, 0,
               N'{Title} {Name} what is the make and model of the vehicle?', N'' , N'', 5, 4, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 3, 1, 1, 0,
               N'Thank you, please confirm your street postal code', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 4, 1, 1, 0,
               N'What is the model of this vehicle?', N'' , N'', 5, 4, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 5, 1, 1, 1,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', 5, 4, 1)
              end
if not exists(select * from QuestionDefinition where QuestionId=104 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Main Driver ID Number', @CoverDefinitionId, 104, NULL, 1, 5, 1, 1, 0,
               N'What is the ID Number for the main driver of this vehicle?', N'' , N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])', 0, 5, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=43 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'No Claim Bonus', @CoverDefinitionId, 43, NULL, 1, 19, 0, 1, 0,
               N'How many years has this client been accident free?', N'' , N'', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=1483 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Is Off Road', @CoverDefinitionId, 1483, NULL, 1, 40, 0, 0, 0,
               N'Is this an off road vehicle?', N'' , N'', 0, NULL, 0)
              end


----------------------End of Motorcyle---------------------------------------



