Declare @ProductId int
SELECT Top 1 @ProductId = Id FROM Product WHERE ProductCode = 'GARRUN'
if exists (SELECT Top 1 Id FROM Product WHERE ProductCode = 'GARRUN')
BEGIN

	if exists (select * from ProductClaimsQuestionDefinition where ProductId = @ProductId and ClaimsQuestionGroupId = 16 and ClaimsQuestionDefinitionId = 71)
	BEGIN
		update ProductClaimsQuestionDefinition set Required = 0 where ProductId = @ProductId and ClaimsQuestionGroupId = 16 and ClaimsQuestionDefinitionId = 71
	END

	if exists (select * from ProductClaimsQuestionDefinition where ProductId = @ProductId and ClaimsQuestionGroupId = 16 and ClaimsQuestionDefinitionId = 72)
	BEGIN
		update ProductClaimsQuestionDefinition set Required = 0 where ProductId = @ProductId and ClaimsQuestionGroupId = 16 and ClaimsQuestionDefinitionId = 72
	END

	if exists (select * from ProductClaimsQuestionDefinition where ProductId = @ProductId and ClaimsQuestionGroupId = 16 and ClaimsQuestionDefinitionId = 73)
	BEGIN
		update ProductClaimsQuestionDefinition set Required = 0 where ProductId = @ProductId and ClaimsQuestionGroupId = 16 and ClaimsQuestionDefinitionId = 73
	END

	if exists (select * from ProductClaimsQuestionDefinition where ProductId = @ProductId and ClaimsQuestionGroupId = 16 and ClaimsQuestionDefinitionId = 74)
	BEGIN
		update ProductClaimsQuestionDefinition set Required = 0 where ProductId = @ProductId and ClaimsQuestionGroupId = 16 and ClaimsQuestionDefinitionId = 74
	END

	if exists (select * from ProductClaimsQuestionDefinition where ProductId = @ProductId and ClaimsQuestionGroupId = 16 and ClaimsQuestionDefinitionId = 75)
	BEGIN
		update ProductClaimsQuestionDefinition set Required = 0 where ProductId = @ProductId and ClaimsQuestionGroupId = 16 and ClaimsQuestionDefinitionId = 75
	END

	if exists (select * from ProductClaimsQuestionDefinition where ProductId = @ProductId and ClaimsQuestionGroupId = 16 and ClaimsQuestionDefinitionId = 76)
	BEGIN
		update ProductClaimsQuestionDefinition set Required = 0 where ProductId = @ProductId and ClaimsQuestionGroupId = 16 and ClaimsQuestionDefinitionId = 76
	END

END