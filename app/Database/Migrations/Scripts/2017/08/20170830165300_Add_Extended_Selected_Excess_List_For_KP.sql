
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'MultiQuote'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  NAme= @Name)

if (@productid IS NULL)
	BEGIN
		PRINT('OUT Of scope to create the product')
	RETURN
 END


--------------------Multiquote Motor-----------------------
SET @CoverID = 218	

SET @CoverName = 'Motor'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
	PRINT('OUT Of scope to create the product')
	RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=1491 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Selected Excess', @CoverDefinitionId, 1491, NULL, 1, 24, 1, 1, 0,
               N'A selected excess will reduce a clients premium.', N'12467' , N'', 0, NULL, 0)
              end

UPDATE dbo.QuestionDefinition
SET	
	RatingFactor = 0,
	RequiredForQuote = 0,
	Hide = 1
WHERE 
	QuestionId = 64 -- SELECTED excess
	AND CoverDefinitionId = @CoverDefinitionId  



-- Do the same for the king Price motor product
SET @Name = N'KPIPERS2'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @Name)

if (@productid IS NULL)
	BEGIN
		PRINT('OUT Of scope to create the product')
	RETURN
 END
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
	PRINT('OUT Of scope to create the product')
	RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=1491 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Selected Excess', @CoverDefinitionId, 1491, NULL, 1, 9, 1, 1, 0,
               N'A selected excess will reduce a clients premium.', N'' , N'', 0, NULL, 0)
              end

UPDATE dbo.QuestionDefinition
SET	
	RatingFactor = 0,
	RequiredForQuote = 0,
	Hide = 1
WHERE 
	QuestionId = 64 -- SELECTED excess
	AND CoverDefinitionId = @CoverDefinitionId  


