﻿IF OBJECT_ID('tempdb..#productCodes') IS NOT NULL
    DROP TABLE #productCodes

DECLARE @GeyserCoverId INT = (SELECT Id FROM md.Cover WHERE Code = 'GEYSER');

CREATE TABLE #productCodes (Code [NVARCHAR](255) COLLATE DATABASE_DEFAULT,
							CoverDefinitionId INT,
							ProductId INT);

INSERT INTO #productCodes(Code) VALUES (N'AUGPRD'),
									   (N'BUD'),
									   (N'DIA'),
									   (N'FIR'),
									   (N'UNI'),
									   (N'VIRS'),
									   (N'MUL')

UPDATE pProduct
	SET ProductId = p.Id
	FROM #productCodes pProduct
	JOIN Product p ON p.ProductCode = pProduct.Code COLLATE DATABASE_DEFAULT

UPDATE pCover
	SET CoverDefinitionId = c.Id
	FROM #productCodes pCover
	JOIN CoverDefinition c ON c.ProductId = pCover.ProductId
								AND c.CoverId = @GeyserCoverId;

INSERT INTO CoverDefinition([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	SELECT 0, N'Geyser', pd.ProductId, @GeyserCoverId, NULL, 1, 0, 0
	FROM #productCodes pd
	WHERE pd.CoverDefinitionId IS NULL

UPDATE pCover
	SET CoverDefinitionId = c.Id
	FROM #productCodes pCover
	JOIN CoverDefinition c ON c.ProductId = pCover.ProductId
								AND c.CoverId = @GeyserCoverId;

IF OBJECT_ID('tempdb..#QDtemp') IS NOT NULL
    DROP TABLE #QDtemp

SELECT * 
	INTO #QDtemp
	FROM QuestionDefinition
	WHERE Id IS NULL

ALTER TABLE #QDtemp DROP COLUMN Id;
ALTER TABLE #QDtemp ADD ID INT NULL;
ALTER TABLE #QDtemp ALTER COLUMN CoverDefinitionId INT NULL;

INSERT INTO #QDtemp ([MasterId],  [DisplayName],      [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [ToolTip])
VALUES				(0,           N'Geyser Location', NULL,				   1487,	     NULL,       1,							 1,              1,                  1,              0,          N'',            N'',            2,            NULL,							N'Geyser Location'),
					(0,           N'Heating Source',  NULL,				   1486,	     NULL,       1,                          2,              1,                  1,              0,          N'',            N'',            2,            NULL,							N'Heating Source'),
					(0,			  N'Risk Address',	  NULL,				   88,			 NULL,		 1,							 0,				 1,					 1,				 0,			 N'',			 N'^[0-9]+$',	 1,			   2,							    N'What is the address where the geyser is located?'),
					(0,			  N'Suburb',		  NULL,				   89,			 NULL,		 1,							 4,				 1,					 1,				 0,			 N'',			 N'',			 1,			   2,								N'What is the suburb name where the geyser is located?'),
					(0,			  N'Postal Code',	  NULL,				   90,			 NULL,		 1,							 4,				 1,					 1,				 0,			 N'',			 N'',			 1,			   2,								N'What is the postal code where the geyser is located?'),
					(0,			  N'Sum Insured',     NULL,				   102,			 NULL,		 1,							 1,				 1,					 1,				 1,			 N'',			 N'^[0-9\.]+$',  0,			   NULL,							N'What is the sum insured of the geyser?')


IF OBJECT_ID('tempdb..#QDInsert') IS NOT NULL
    DROP TABLE #QDInsert

SELECT p.CoverDefinitionId, q.Id, q.[MasterId],  q.[DisplayName], q.[QuestionId], q.[ParentId], q.[QuestionDefinitionTypeId], q.[VisibleIndex], q.[RequiredForQuote], q.[RatingFactor], q.[ReadOnly], q.[ToolTip], q.[DefaultValue], q.[RegexPattern], q.[GroupIndex], q.[QuestionDefinitionGroupTypeId]
	INTO #QDInsert
	FROM #productCodes p
	CROSS JOIN #QDtemp q

UPDATE pQD
	SET Id = qd.Id
	FROM #QDInsert pQD
	JOIN QuestionDefinition qd ON qd.QuestionId = pQD.QuestionId
								AND qd.CoverDefinitionId = pQD.CoverDefinitionId

DELETE #QDInsert WHERE Id IS NOT NULL

INSERT INTO QuestionDefinition([MasterId],
							   [DisplayName],
							   [CoverDefinitionId],
							   [QuestionId],
							   [ParentId],
							   [QuestionDefinitionTypeId],
							   [VisibleIndex],
							   [RequiredForQuote],
							   [RatingFactor],
							   [ReadOnly],
							   [ToolTip],
							   [DefaultValue],
							   [RegexPattern],
							   [GroupIndex],
							   [QuestionDefinitionGroupTypeId])
					    SELECT [MasterId],
							   [DisplayName],
							   [CoverDefinitionId],
							   [QuestionId],
							   [ParentId],
							   [QuestionDefinitionTypeId],
							   [VisibleIndex],
							   [RequiredForQuote],
							   [RatingFactor],
							   [ReadOnly],
							   [ToolTip],
							   [DefaultValue],
							   [RegexPattern],
							   [GroupIndex],
							   [QuestionDefinitionGroupTypeId]
						  FROM #QDInsert