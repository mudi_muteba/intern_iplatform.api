Declare @ProductId int, @ProductClaimsQuestionDefinitionId int;

SELECT Top 1 @ProductId = Id FROM Product WHERE ProductCode = 'GARRUN'

if exists (SELECT Top 1 Id FROM Product WHERE ProductCode = 'GARRUN')
BEGIN
-- Motor Accident 2
INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1208, 1, 1, 1, 'I hereby consent that the information submitted herein is true and correct and that I have the necessary authority as the Insured or authorized agent of the insured to provide the above information', 1, 46);

--Motor Theft 1
INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1209, 1, 1, 1, 'I hereby consent that the information submitted herein is true and correct and that I have the necessary authority as the Insured or authorized agent of the insured to provide the above information', 1, 46);

--Hijack 14
INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1210, 1, 1, 1, 'I hereby consent that the information submitted herein is true and correct and that I have the necessary authority as the Insured or authorized agent of the insured to provide the above information', 1, 46);

--Windscreen 24
INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1212, 1, 0, 1, 'I hereby consent that the information submitted herein is true and correct and that I have the necessary authority as the Insured or authorized agent of the insured to provide the above information', 1, 46);

--Theft - 37
INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1213, 1, 0, 1, 'I hereby consent that the information submitted herein is true and correct and that I have the necessary authority as the Insured or authorized agent of the insured to provide the above information', 1, 46);

--geyser - 29
INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1214, 1, 0, 1, 'I hereby consent that the information submitted herein is true and correct and that I have the necessary authority as the Insured or authorized agent of the insured to provide the above information', 1, 46);

--Accidental Damage - 36
INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1211, 1, 0, 1, 'I hereby consent that the information submitted herein is true and correct and that I have the necessary authority as the Insured or authorized agent of the insured to provide the above information', 1, 46);

--keys and lock - 39
INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1216, 1, 0, 1, 'I hereby consent that the information submitted herein is true and correct and that I have the necessary authority as the Insured or authorized agent of the insured to provide the above information', 1, 46);

--damage lost item - 8
INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1219, 1, 0, 1, 'I hereby consent that the information submitted herein is true and correct and that I have the necessary authority as the Insured or authorized agent of the insured to provide the above information', 1, 46);

--jewellery - 32
INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1217, 1, 0, 1, 'I hereby consent that the information submitted herein is true and correct and that I have the necessary authority as the Insured or authorized agent of the insured to provide the above information', 1, 46);

--cellphone 30
INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1218, 1, 0, 1, 'I hereby consent that the information submitted herein is true and correct and that I have the necessary authority as the Insured or authorized agent of the insured to provide the above information', 1, 46);

END