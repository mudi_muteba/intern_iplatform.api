declare
	@ChannelGuid_PSG uniqueidentifier,
	@ChannelGuid_PSGCC uniqueidentifier,
	@ChannelId_PSG int,
	@ChannelId_PSGCC int,
	@OrganizationId int,
	@ProductId int

select
	@ChannelGuid_PSG = '3D411609-46BC-45ED-906F-28A28436B185',
	@ChannelGuid_PSGCC = 'F7A7FDDC-1841-4C18-8CFE-B0F6B45AE7C3'

if exists (select Id from Channel where SystemId = @ChannelGuid_PSGCC)
	begin
		select @ChannelId_PSG = Id from channel where SystemId = @ChannelGuid_PSG
		select @ChannelId_PSGCC = Id from channel where SystemId = @ChannelGuid_PSGCC

		--Quote Upload Settings
		if not exists (select Id from SettingsQuoteUpload where SettingName like '%Telesure%' and ProductId in (6, 9, 10) and Environment = 'Dev' and ChannelId = @ChannelId_PSGCC)
			begin
				insert into SettingsQuoteUpload (SettingName, SettingValue, Environment, IsDeleted, ProductId, ChannelId)
				select
					SettingName,
					case
						--Broker Code
						when SettingName = 'Telesureag/Upload/BrokerCode' then 'PSGCCCIP'
						when SettingName = 'Telesureffw/Upload/BrokerCode' then 'FPSGCCCIP'
						when SettingName = 'Telesureunity/Upload/BrokerCode' then 'UPSGCCCIP'

						--Agent Code
						when SettingName = 'Telesureag/Upload/AgentCode' then 'MAPS1'
						when SettingName = 'Telesureffw/Upload/AgentCode' then 'MFPS1'
						when SettingName = 'Telesureunity/Upload/AgentCode' then 'MUPS1'

						--Row Value
						else SettingValue
					end,
					Environment,
					IsDeleted,
					ProductId,
					@ChannelId_PSGCC
				from SettingsQuoteUpload
				where
					SettingName like '%Telesure%' and
					ProductId in (6, 9, 10) and
					Environment = 'Dev' and
					ChannelId = @ChannelId_PSG
			end

		if not exists (select Id from SettingsQuoteUpload where SettingName like '%Telesure%' and ProductId in (6, 9, 10) and Environment = 'UAT' and ChannelId = @ChannelId_PSGCC)
			begin
				insert into SettingsQuoteUpload (SettingName, SettingValue, Environment, IsDeleted, ProductId, ChannelId)
				select
					SettingName,
					case
						--Broker Code
						when SettingName = 'Telesureag/Upload/BrokerCode' then 'PSGCCCIP'
						when SettingName = 'Telesureffw/Upload/BrokerCode' then 'FPSGCCCIP'
						when SettingName = 'Telesureunity/Upload/BrokerCode' then 'UPSGCCCIP'

						--Agent Code
						when SettingName = 'Telesureag/Upload/AgentCode' then 'MAPS1'
						when SettingName = 'Telesureffw/Upload/AgentCode' then 'MFPS1'
						when SettingName = 'Telesureunity/Upload/AgentCode' then 'MUPS1'

						--Row Value
						else SettingValue
					end,
					Environment,
					IsDeleted,
					ProductId,
					@ChannelId_PSGCC
				from SettingsQuoteUpload
				where
					SettingName like '%Telesure%' and
					ProductId in (6, 9, 10) and
					Environment = 'UAT' and
					ChannelId = @ChannelId_PSG
			end

		if not exists (select Id from SettingsQuoteUpload where SettingName like '%Telesure%' and ProductId in (6, 9, 10) and Environment = 'Live' and ChannelId = @ChannelId_PSGCC)
			begin
				insert into SettingsQuoteUpload (SettingName, SettingValue, Environment, IsDeleted, ProductId, ChannelId)
				select
					SettingName,
					case
						--Broker Code
						when SettingName = 'Telesureag/Upload/BrokerCode' then 'PSGCCCIP'
						when SettingName = 'Telesureffw/Upload/BrokerCode' then 'FPSGCCCIP'
						when SettingName = 'Telesureunity/Upload/BrokerCode' then 'UPSGCCCIP'

						--Agent Code
						when SettingName = 'Telesureag/Upload/AgentCode' then 'MAPS1'
						when SettingName = 'Telesureffw/Upload/AgentCode' then 'MFPS1'
						when SettingName = 'Telesureunity/Upload/AgentCode' then 'MUPS1'

						--Row Value
						else SettingValue
					end,
					Environment,
					IsDeleted,
					ProductId,
					@ChannelId_PSGCC
				from SettingsQuoteUpload
				where
					SettingName like '%Telesure%' and
					ProductId in (6, 9, 10) and
					Environment = 'Live' and
					ChannelId = @ChannelId_PSG
			end

		--Channel Events & Channel Event Tasks
		if not exists (select Id from ChannelEvent where EventName = 'ExternalQuoteAcceptedWithUnderwritingEnabledEvent' and ChannelId = @ChannelId_PSGCC and ProductCode in ('AUGPRD', 'FIR', 'UNI'))
			begin
				declare @ChannelEventId int

				insert into ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode) values ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', @ChannelId_PSGCC, 0, 'AUGPRD')
				select @ChannelEventId = scope_identity()
				insert into ChannelEventTask (TaskName, ChannelEventId, IsDeleted) values (2, @ChannelEventId, 0)

				insert into ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode) values ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', @ChannelId_PSGCC, 0, 'FIR')
				select @ChannelEventId = scope_identity()
				insert into ChannelEventTask (TaskName, ChannelEventId, IsDeleted) values (2, @ChannelEventId, 0)

				insert into ChannelEvent (EventName, ChannelId, IsDeleted, ProductCode) values ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', @ChannelId_PSGCC, 0, 'UNI')
				select @ChannelEventId = scope_identity()
				insert into ChannelEventTask (TaskName, ChannelEventId, IsDeleted) values (2, @ChannelEventId, 0)
			end

		--iRate Settings
		if exists (select Id from SettingsiRate where ProductId in (6, 9, 10) and ChannelSystemId = @ChannelGuid_PSGCC and ChannelId = @ChannelId_PSGCC)
			begin
				delete from SettingsiRate where ProductId in (6, 9, 10) and ChannelSystemId = @ChannelGuid_PSGCC and ChannelId = @ChannelId_PSGCC
			end

		insert into SettingsiRate (ProductId, [Password], AgentCode, BrokerCode, AuthCode, SchemeCode, Token, Environment, UserId, SubscriberCode, CompanyCode, UwCompanyCode, UwProductCode, IsDeleted, ProductCode, ChannelSystemId, ChannelId)
		select
			ProductId,
			[Password],
			case
				when AgentCode = 'MAPSG' then 'MAPS1'
				when AgentCode = 'MFPSG' then 'MFPS1'
				when AgentCode = 'MUPSG' then 'MUPS1'
				else AgentCode
			end,
			case
				when BrokerCode = 'PSGDIR' then 'PSGCCCIP'
				when BrokerCode = 'FPSGDIR' then 'FPSGCCCIP'
				when BrokerCode = 'UPSGDIR' then 'UPSGCCCIP'
				else BrokerCode
			end,
			AuthCode,
			SchemeCode,
			Token,
			Environment,
			UserId,
			SubscriberCode,
			CompanyCode,
			UwCompanyCode,
			UwProductCode,
			IsDeleted,
			ProductCode,
			@ChannelGuid_PSGCC,
			@ChannelId_PSGCC
		from SettingsiRate
		where
			ProductId in (6, 9, 10) and
			ChannelSystemId = @ChannelGuid_PSG and
			ChannelId = @ChannelId_PSG
	end
go