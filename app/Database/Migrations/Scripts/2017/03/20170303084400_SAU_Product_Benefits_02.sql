declare
	--SAU: CENTND
	@CoverDefinitionID_CENTND_AllRisk int,
	@CoverDefinitionID_CENTND_Building int,
	@CoverDefinitionID_CENTND_Contents int,
	@CoverDefinitionID_CENTND_Motor int,

	--SAU: CENTRD
	@CoverDefinitionID_CENTRD_AllRisk int,
	@CoverDefinitionID_CENTRD_Building int,
	@CoverDefinitionID_CENTRD_Contents int,
	@CoverDefinitionID_CENTRD_Motor int,

	--SAU: SNTMND
	@CoverDefinitionID_SNTMND_AllRisk int,
	@CoverDefinitionID_SNTMND_Building int,
	@CoverDefinitionID_SNTMND_Contents int,
	@CoverDefinitionID_SNTMND_Motor int,

	--SAU: SNTMRD
	@CoverDefinitionID_SNTMRD_AllRisk int,
	@CoverDefinitionID_SNTMRD_Building int,
	@CoverDefinitionID_SNTMRD_Contents int,
	@CoverDefinitionID_SNTMRD_Motor int

select @CoverDefinitionID_CENTND_AllRisk = CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('CENTND') and Cover.Name in ('All Risk')
select @CoverDefinitionID_CENTND_Building = CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('CENTND') and Cover.Name in ('Building')
select @CoverDefinitionID_CENTND_Contents = CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('CENTND') and Cover.Name in ('Contents')
select @CoverDefinitionID_CENTND_Motor = CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('CENTND') and Cover.Name in ('Motor')

select @CoverDefinitionID_CENTRD_AllRisk = CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where	Product.ProductCode in ('CENTRD') and Cover.Name in ('All Risk')
select @CoverDefinitionID_CENTRD_Building = CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where	Product.ProductCode in ('CENTRD') and Cover.Name in ('Building')
select @CoverDefinitionID_CENTRD_Contents = CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where	Product.ProductCode in ('CENTRD') and Cover.Name in ('Contents')
select @CoverDefinitionID_CENTRD_Motor = CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where	Product.ProductCode in ('CENTRD') and Cover.Name in ('Motor')

select @CoverDefinitionID_SNTMND_AllRisk = CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where	Product.ProductCode in ('SNTMND') and Cover.Name in ('All Risk')
select @CoverDefinitionID_SNTMND_Building = CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where	Product.ProductCode in ('SNTMND') and Cover.Name in ('Building')
select @CoverDefinitionID_SNTMND_Contents = CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where	Product.ProductCode in ('SNTMND') and Cover.Name in ('Contents')
select @CoverDefinitionID_SNTMND_Motor = CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where	Product.ProductCode in ('SNTMND') and Cover.Name in ('Motor')

select @CoverDefinitionID_SNTMRD_AllRisk = CoverDefinition.Id from CoverDefinition	inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('SNTMRD') and Cover.Name in ('All Risk')
select @CoverDefinitionID_SNTMRD_Building = CoverDefinition.Id from CoverDefinition	inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('SNTMRD') and Cover.Name in ('Building')
select @CoverDefinitionID_SNTMRD_Contents = CoverDefinition.Id from CoverDefinition	inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('SNTMRD') and Cover.Name in ('Contents')
select @CoverDefinitionID_SNTMRD_Motor = CoverDefinition.Id from CoverDefinition	inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('SNTMRD') and Cover.Name in ('Motor')

delete from ProductBenefit where CoverDefinitionId in
(
	--SAU: CENTND
	@CoverDefinitionID_CENTND_AllRisk,
	@CoverDefinitionID_CENTND_Building,
	@CoverDefinitionID_CENTND_Contents,
	@CoverDefinitionID_CENTND_Motor,

	--SAU: CENTRD
	@CoverDefinitionID_CENTRD_AllRisk,
	@CoverDefinitionID_CENTRD_Building,
	@CoverDefinitionID_CENTRD_Contents,
	@CoverDefinitionID_CENTRD_Motor,

	--SAU: SNTMND
	@CoverDefinitionID_SNTMND_AllRisk,
	@CoverDefinitionID_SNTMND_Building,
	@CoverDefinitionID_SNTMND_Contents,
	@CoverDefinitionID_SNTMND_Motor,

	--SAU: SNTMRD
	@CoverDefinitionID_SNTMRD_AllRisk,
	@CoverDefinitionID_SNTMRD_Building,
	@CoverDefinitionID_SNTMRD_Contents,
	@CoverDefinitionID_SNTMRD_Motor
)

--Product Benefits - All Risk: CENTND
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Riot and Strike outside RSA', 'No', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Unspecified Items as defined', 'Yes- R5000 Max', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, '* If yes ~ Cover for Jewellery, Clothing and Personal items', 'Yes', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, '* If yes ~ Limit any one item', 'No', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, '* If yes ~ Cover for money and negotiable instruments', 'No', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Specified Items [If yes refer to list of items]', 'Yes', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Items in a Bank Vault', 'No', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Bicycles', 'If Specified', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Wheelchairs and its accessories', 'If Specified', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Prescription Glasses', 'If Specified', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Contact Lenses', 'If Specified', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Cellular phones', 'If Specified', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Stamp Collection', 'If Specified', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Coin Collection', 'If Specified', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Transport of groceries and household goods', 'No', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'All Risks cover for caravan contents', 'If Specified', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'All Risks cover for car radios', 'If Specified', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Borehole and swimming pool equipment', 'No', 1, 18, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Items stolen from the cabin of a vehicle limit', 'If Specified', 1, 19, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_AllRisk, 'Incident Limit : Items stolen from the locked boot of a vehicle', 'If Specified', 1, 20, 0)


--Product Benefits - All Risk: CENTRD
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Riot and Strike outside RSA', 'No', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Unspecified Items as defined', 'Yes- R5000 Max', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, '* If yes ~ Cover for Jewellery, Clothing and Personal items', 'Yes', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, '* If yes ~ Limit any one item', 'No', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, '* If yes ~ Cover for money and negotiable instruments', 'No', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Specified Items [If yes refer to list of items]', 'Yes', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Items in a Bank Vault', 'No', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Bicycles', 'If Specified', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Wheelchairs and its accessories', 'If Specified', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Prescription Glasses', 'If Specified', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Contact Lenses', 'If Specified', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Cellular phones', 'If Specified', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Stamp Collection', 'If Specified', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Coin Collection', 'If Specified', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Transport of groceries and household goods', 'No', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'All Risks cover for caravan contents', 'If Specified', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'All Risks cover for car radios', 'If Specified', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Borehole and swimming pool equipment', 'No', 1, 18, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Items stolen from the cabin of a vehicle limit', 'If Specified', 1, 19, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_AllRisk, 'Incident Limit : Items stolen from the locked boot of a vehicle', 'If Specified', 1, 20, 0)


--Product Benefits - All Risk: SNTMND
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Riot and Strike outside RSA', 'No', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Unspecified Items as defined', 'Yes- R5000 Max', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, '* If yes ~ Cover for Jewellery, Clothing and Personal items', 'Yes', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, '* If yes ~ Limit any one item', 'No', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, '* If yes ~ Cover for money and negotiable instruments', 'No', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Specified Items [If yes refer to list of items]', 'Yes', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Items in a Bank Vault', 'No', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Bicycles', 'If Specified', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Wheelchairs and its accessories', 'If Specified', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Prescription Glasses', 'If Specified', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Contact Lenses', 'If Specified', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Cellular phones', 'If Specified', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Stamp Collection', 'If Specified', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Coin Collection', 'If Specified', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Transport of groceries and household goods', 'No', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'All Risks cover for caravan contents', 'If Specified', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'All Risks cover for car radios', 'If Specified', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Borehole and swimming pool equipment', 'No', 1, 18, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Items stolen from the cabin of a vehicle limit', 'If Specified', 1, 19, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_AllRisk, 'Incident Limit : Items stolen from the locked boot of a vehicle', 'If Specified', 1, 20, 0)


--Product Benefits - All Risk: SNTMRD
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Riot and Strike outside RSA', 'No', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Unspecified Items as defined', 'Yes- R5000 Max', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, '* If yes ~ Cover for Jewellery, Clothing and Personal items', 'Yes', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, '* If yes ~ Limit any one item', 'No', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, '* If yes ~ Cover for money and negotiable instruments', 'No', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Specified Items [If yes refer to list of items]', 'Yes', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Items in a Bank Vault', 'No', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Bicycles', 'If Specified', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Wheelchairs and its accessories', 'If Specified', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Prescription Glasses', 'If Specified', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Contact Lenses', 'If Specified', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Cellular phones', 'If Specified', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Stamp Collection', 'If Specified', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Coin Collection', 'If Specified', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Transport of groceries and household goods', 'No', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'All Risks cover for caravan contents', 'If Specified', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'All Risks cover for car radios', 'If Specified', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Borehole and swimming pool equipment', 'No', 1, 18, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Items stolen from the cabin of a vehicle limit', 'If Specified', 1, 19, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_AllRisk, 'Incident Limit : Items stolen from the locked boot of a vehicle', 'If Specified', 1, 20, 0)


--Product Benefits - Building: CENTND
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'If yes Home building definition to be checked', 'Covered', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Thatch roof home', 'Referral Basis', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Swimming Pool covered', 'Yes', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Swimming Pool equipment covered', 'Yes', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Borehole pump/equipment', 'Yes', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Tennis court covered', 'Yes', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Damage to Gardens', 'No', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Automatic anniversary/renewal sum insured increase', 'No', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Loss of water by leakage', 'No', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Rent', 'Yes', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Removal of fallen trees', 'No', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Professional Fees', 'Yes', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Glass and Sanitaryware', 'Yes', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Power supply', 'Yes', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Aerials', 'Yes', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Fire brigade charges', 'Yes', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Security Guards', 'Yes', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Public Liability as a Home Owner', 'Yes', 1, 18, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Liability to Domestic Employees', 'Yes', 1, 19, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Building, 'Subsidence, heave and landslip', 'No', 1, 20, 0)


--Product Benefits - Building: CENTRD
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'If yes Home building definition to be checked', 'Covered', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Thatch roof home', 'Referral Basis', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Swimming Pool covered', 'Yes', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Swimming Pool equipment covered', 'Yes', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Borehole pump/equipment', 'Yes', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Tennis court covered', 'Yes', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Damage to Gardens', 'No', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Automatic anniversary/renewal sum insured increase', 'No', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Loss of water by leakage', 'No', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Rent', 'Yes', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Removal of fallen trees', 'No', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Professional Fees', 'Yes', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Glass and Sanitaryware', 'Yes', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Power supply', 'Yes', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Aerials', 'Yes', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Fire brigade charges', 'Yes', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Security Guards', 'Yes', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Public Liability as a Home Owner', 'Yes', 1, 18, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Liability to Domestic Employees', 'Yes', 1, 19, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Building, 'Subsidence, heave and landslip', 'No', 1, 20, 0)


--Product Benefits - Building: SNTMND
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'If yes Home building definition to be checked', 'Covered', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Thatch roof home', 'Referral Basis', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Swimming Pool covered', 'Yes', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Swimming Pool equipment covered', 'Yes', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Borehole pump/equipment', 'Yes', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Tennis court covered', 'Yes', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Damage to Gardens', 'No', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Automatic anniversary/renewal sum insured increase', 'No', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Loss of water by leakage', 'No', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Rent', 'Yes', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Removal of fallen trees', 'No', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Professional Fees', 'Yes', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Glass and Sanitaryware', 'Yes', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Power supply', 'Yes', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Aerials', 'Yes', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Fire brigade charges', 'Yes', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Security Guards', 'Yes', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Public Liability as a Home Owner', 'Yes', 1, 18, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Liability to Domestic Employees', 'Yes', 1, 19, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Building, 'Subsidence, heave and landslip', 'No', 1, 20, 0)


--Product Benefits - Building: SNTMRD
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'If yes Home building definition to be checked', 'Covered', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Thatch roof home', 'Referral Basis', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Swimming Pool covered', 'Yes', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Swimming Pool equipment covered', 'Yes', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Borehole pump/equipment', 'Yes', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Tennis court covered', 'Yes', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Damage to Gardens', 'No', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Automatic anniversary/renewal sum insured increase', 'No', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Loss of water by leakage', 'No', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Rent', 'Yes', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Removal of fallen trees', 'No', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Professional Fees', 'Yes', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Glass and Sanitaryware', 'Yes', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Power supply', 'Yes', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Aerials', 'Yes', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Fire brigade charges', 'Yes', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Security Guards', 'Yes', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Public Liability as a Home Owner', 'Yes', 1, 18, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Liability to Domestic Employees', 'Yes', 1, 19, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Building, 'Subsidence, heave and landslip', 'No', 1, 20, 0)


--Product Benefits - Contents: CENTND
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Automatic anniversary/renewal sum insured increase', 'No', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Accidental Damage extension cover', 'Yes', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Documents limitation', 'No', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Deterioration of Food', 'Included', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Washing stolen at your home', 'Included', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Garden furniture stolen at your home', 'Included', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Damage to Garden', 'Not Included', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Guests belongings stolen at your home', 'Included', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Personal documents/coins/stamps - Loss of', 'Included', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Locks and Keys - Lost/damaged', 'Included', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Remote control units', 'Not Included', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Loss of water by leakage', 'Not Included', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Credit or Bank Cards - fraudulent use', 'Included', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Hole in one / bowling full house', 'Included', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Insured and Spouce death - fire or a break-in', 'Not Included', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Your domestics belongings - stolen following break-in', 'Included', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Medical Expenses', 'Not Included', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Veterinary Fees', 'Not Included', 1, 18, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Rent to live elsewhere', 'Included', 1, 19, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Storage costs for contents after damage', 'Included', 1, 20, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Belongings in a removal truck', 'Included', 1, 21, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Accidental breakage of mirrors and galss', 'Not Included', 1, 22, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Accidental breakage of television set', 'Not Included', 1, 23, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Fire brigade charges', 'Included', 1, 24, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Security Guards', 'Included', 1, 25, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Public Liability', '', 1, 26, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Tenants Liability', '', 1, 27, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Contents, 'Liability to Domestic Employees', '', 1, 28, 0)


--Product Benefits - Contents: CENTRD
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Automatic anniversary/renewal sum insured increase', 'No', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Accidental Damage extension cover', 'Yes', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Documents limitation', 'No', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Deterioration of Food', 'Included', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Washing stolen at your home', 'Included', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Garden furniture stolen at your home', 'Included', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Damage to Garden', 'Not Included', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Guests belongings stolen at your home', 'Included', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Personal documents/coins/stamps - Loss of', 'Included', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Locks and Keys - Lost/damaged', 'Included', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Remote control units', 'Not Included', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Loss of water by leakage', 'Not Included', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Credit or Bank Cards - fraudulent use', 'Included', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Hole in one / bowling full house', 'Included', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Insured and Spouce death - fire or a break-in', 'Not Included', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Your domestics belongings - stolen following break-in', 'Included', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Medical Expenses', 'Not Included', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Veterinary Fees', 'Not Included', 1, 18, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Rent to live elsewhere', 'Included', 1, 19, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Storage costs for contents after damage', 'Included', 1, 20, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Belongings in a removal truck', 'Included', 1, 21, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Accidental breakage of mirrors and galss', 'Not Included', 1, 22, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Accidental breakage of television set', 'Not Included', 1, 23, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Fire brigade charges', 'Included', 1, 24, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Security Guards', 'Included', 1, 25, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Public Liability', '', 1, 26, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Tenants Liability', '', 1, 27, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Contents, 'Liability to Domestic Employees', '', 1, 28, 0)


--Product Benefits - Contents: SNTMND
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Automatic anniversary/renewal sum insured increase', 'No', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Accidental Damage extension cover', 'Yes', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Documents limitation', 'No', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Deterioration of Food', 'Included', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Washing stolen at your home', 'Included', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Garden furniture stolen at your home', 'Included', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Damage to Garden', 'Not Included', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Guests belongings stolen at your home', 'Included', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Personal documents/coins/stamps - Loss of', 'Included', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Locks and Keys - Lost/damaged', 'Included', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Remote control units', 'Not Included', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Loss of water by leakage', 'Not Included', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Credit or Bank Cards - fraudulent use', 'Included', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Hole in one / bowling full house', 'Included', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Insured and Spouce death - fire or a break-in', 'Not Included', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Your domestics belongings - stolen following break-in', 'Included', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Medical Expenses', 'Not Included', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Veterinary Fees', 'Not Included', 1, 18, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Rent to live elsewhere', 'Included', 1, 19, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Storage costs for contents after damage', 'Included', 1, 20, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Belongings in a removal truck', 'Included', 1, 21, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Accidental breakage of mirrors and galss', 'Not Included', 1, 22, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Accidental breakage of television set', 'Not Included', 1, 23, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Fire brigade charges', 'Included', 1, 24, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Security Guards', 'Included', 1, 25, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Public Liability', '', 1, 26, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Tenants Liability', '', 1, 27, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Contents, 'Liability to Domestic Employees', '', 1, 28, 0)


--Product Benefits - Contents: SNTMRD
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Automatic anniversary/renewal sum insured increase', 'No', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Accidental Damage extension cover', 'Yes', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Documents limitation', 'No', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Deterioration of Food', 'Included', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Washing stolen at your home', 'Included', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Garden furniture stolen at your home', 'Included', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Damage to Garden', 'Not Included', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Guests belongings stolen at your home', 'Included', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Personal documents/coins/stamps - Loss of', 'Included', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Locks and Keys - Lost/damaged', 'Included', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Remote control units', 'Not Included', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Loss of water by leakage', 'Not Included', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Credit or Bank Cards - fraudulent use', 'Included', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Hole in one / bowling full house', 'Included', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Insured and Spouce death - fire or a break-in', 'Not Included', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Your domestics belongings - stolen following break-in', 'Included', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Medical Expenses', 'Not Included', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Veterinary Fees', 'Not Included', 1, 18, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Rent to live elsewhere', 'Included', 1, 19, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Storage costs for contents after damage', 'Included', 1, 20, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Belongings in a removal truck', 'Included', 1, 21, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Accidental breakage of mirrors and galss', 'Not Included', 1, 22, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Accidental breakage of television set', 'Not Included', 1, 23, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Fire brigade charges', 'Included', 1, 24, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Security Guards', 'Included', 1, 25, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Public Liability', '', 1, 26, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Tenants Liability', '', 1, 27, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Contents, 'Liability to Domestic Employees', '', 1, 28, 0)


--Product Benefits - Motor: CENTND
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Use of vehicle', 'Business & Private', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Comprehensive Cover', 'Yes', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'New for Old', 'No', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Windscreen', 'Yes', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Third Party Fire and theft Cover', 'Yes', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Third Party Cover', 'Yes', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Riot and Strike outside RSA', 'No', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Tracking device to be covered', 'Not Included', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Towing and Storage', 'Yes if 0800 002 883 was called', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Sound System [not factory fitted]', 'If Specified', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Hail Damage', 'Yes (Carport min. requirement)', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Canopy of the pick-up', 'If Specified', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Car Hire', 'Yes (14; 30 & 60 day option)', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Emergency Hotel Expenses', 'Yes', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Credit Shortfall', 'No', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Medical Costs', 'No', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Third Party Liability', 'R1000000', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTND_Motor, 'Third Party fire and explosion Liability', 'No', 1, 18, 0)


--Product Benefits - Motor: CENTRD
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Use of vehicle', 'Business & Private', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Comprehensive Cover', 'Yes', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'New for Old', 'No', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Windscreen', 'Yes', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Third Party Fire and theft Cover', 'Yes', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Third Party Cover', 'Yes', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Riot and Strike outside RSA', 'No', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Tracking device to be covered', 'Not Included', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Towing and Storage', 'Yes if 0800 002 883 was called', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Sound System [not factory fitted]', 'If Specified', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Hail Damage', 'Yes (Carport min. requirement)', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Canopy of the pick-up', 'If Specified', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Car Hire', 'Yes (14; 30 & 60 day option)', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Emergency Hotel Expenses', 'Yes', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Credit Shortfall', 'No', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Medical Costs', 'No', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Third Party Liability', 'R1000000', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_CENTRD_Motor, 'Third Party fire and explosion Liability', 'No', 1, 18, 0)


--Product Benefits - Motor: SNTMND
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Use of vehicle', 'Business & Private', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Comprehensive Cover', 'Yes', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'New for Old', 'No', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Windscreen', 'Yes', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Third Party Fire and theft Cover', 'Yes', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Third Party Cover', 'Yes', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Riot and Strike outside RSA', 'No', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Tracking device to be covered', 'Not Included', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Towing and Storage', 'Yes if 0800 002 883 was called', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Sound System [not factory fitted]', 'If Specified', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Hail Damage', 'Yes (Carport min. requirement)', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Canopy of the pick-up', 'If Specified', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Car Hire', 'Yes (14; 30 & 60 day option)', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Emergency Hotel Expenses', 'Yes', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Credit Shortfall', 'No', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Medical Costs', 'No', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Third Party Liability', 'R1000000', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMND_Motor, 'Third Party fire and explosion Liability', 'No', 1, 18, 0)


--Product Benefits - Motor: SNTMRD
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Included', 'Yes', 1, 0, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Use of vehicle', 'Business & Private', 1, 1, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Comprehensive Cover', 'Yes', 1, 2, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'New for Old', 'No', 1, 3, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Windscreen', 'Yes', 1, 4, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Third Party Fire and theft Cover', 'Yes', 1, 5, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Third Party Cover', 'Yes', 1, 6, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Riot and Strike outside RSA', 'No', 1, 7, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Tracking device to be covered', 'Not Included', 1, 8, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Towing and Storage', 'Yes if 0800 002 883 was called', 1, 9, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Sound System [not factory fitted]', 'If Specified', 1, 10, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Hail Damage', 'Yes (Carport min. requirement)', 1, 11, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Canopy of the pick-up', 'If Specified', 1, 12, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Car Hire', 'Yes (14; 30 & 60 day option)', 1, 13, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Emergency Hotel Expenses', 'Yes', 1, 14, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Credit Shortfall', 'No', 1, 15, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Medical Costs', 'No', 1, 16, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Third Party Liability', 'R1000000', 1, 17, 0)
insert into ProductBenefit (CoverDefinitionId, Name, [Value], ShowToClient, VisibleIndex, IsDeleted) values (@CoverDefinitionID_SNTMRD_Motor, 'Third Party fire and explosion Liability', 'No', 1, 18, 0)