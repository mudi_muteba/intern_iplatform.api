﻿DECLARE @ProductId INT;
SET @ProductId = (SELECT Id FROM Product WHERE ProductCode = 'DOMESTIC_ARMOUR');
PRINT @ProductId;

DECLARE @MultiQuoteDummyQuestionId INT;

SELECT @MultiQuoteDummyQuestionId = ID FROM QuestionDefinition 
WHERE CoverDefinitionId = 99 AND QuestionId = 1081;
PRINT @MultiQuoteDummyQuestionId;

DECLARE @CoverId INT;
SET @CoverId = (SELECT Id FROM md.Cover WHERE Code = 'ALL_RISK');
PRINT @CoverId;

DECLARE @CoverDefinitionId INT;
SET @CoverDefinitionId = (SELECT Id FROM CoverDefinition WHERE ProductId = @ProductId AND CoverId = @CoverId);
PRINT @CoverDefinitionId

if not exists(select * from QuestionDefinition where QuestionId=1441 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'All Risk Category', @CoverDefinitionId, 1441, NULL, 1, 1, 1, 1, 0,
               N'What category does the item fall under?', N'' , N'', 0, 6)

			  DECLARE @AllRiskId INT;
			  SET @AllRiskId  = SCOPE_IDENTITY();
			  
			  INSERT INTO MapQuestionDefinition(ParentId, ChildId, IsDeleted)
			  VALUES(@MultiQuoteDummyQuestionId, @AllRiskId, 0);

			  UPDATE QuestionDefinition SET IsDeleted = 1, Hide = 1 WHERE CoverDefinitionId = @CoverDefinitionId AND QuestionId = 126
              end