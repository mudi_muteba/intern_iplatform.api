﻿SET ANSI_PADDING ON
GO

DECLARE @Partyid INT
SET @Partyid = (SELECT partyid FROM dbo.Organization WHERE code = 'MOTORV')

--product
INSERT[dbo].[Product]
        ( [MasterId] ,
          [Name] ,
          [ProductOwnerId] ,
          [ProductProviderId] ,
          [ProductTypeId] ,
          [AgencyNumber] ,
          [ProductCode] ,
          [StartDate] ,
          [EndDate]
        )
VALUES( 0 ,
          N'Warranty' ,
          @Partyid ,
          @Partyid ,
          7 ,
          N'123456' ,
          N'MOTORWAR' ,
          CAST(0x00009CD400000000 AS DATETIME) ,
          NULL
        )

DECLARE @Productid INT
SET @Productid = IDENT_CURRENT('Product')

--covers
INSERT[dbo].[CoverDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [ProductId] ,
          [CoverId] ,
          [ParentId] ,
          [CoverDefinitionTypeId] ,
          [VisibleIndex]
        )
VALUES( 0 ,
          N'Motor Warranty' ,
          @Productid ,
          375 ,
          NULL ,
          1 ,
          0
        )

DECLARE @CoverDefSPlanid INT
SET @CoverDefSPlanid = IDENT_CURRENT('CoverDefinition')

--questions
INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Year Of Manufacture' ,
          @CoverDefSPlanid ,
          99 ,
          NULL ,
          1 ,
          0 ,
          1 ,
          1 ,
          0 ,
          N'What is the year of manufacture for this vehicle?' ,
          N'' ,
          N'^[0-9]+$'
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Make' ,
          @CoverDefSPlanid ,
          95 ,
          NULL ,
          1 ,
          1 ,
          1 ,
          1 ,
          0 ,
          N'What is the make of this vehicle?' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Model' ,
          @CoverDefSPlanid ,
          96 ,
          NULL ,
          1 ,
          2 ,
          1 ,
          1 ,
          0 ,
          N'What is the model of this vehicle?' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'MM Code' ,
          @CoverDefSPlanid ,
          94 ,
          NULL ,
          1 ,
          4 ,
          1 ,
          1 ,
          1 ,
          N'What is the Mead and McGrouther code for this vehicle?' ,
          N'' ,
          N'^[0-9]+$'
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Sum Insured' ,
          @CoverDefSPlanid ,
          102 ,
          NULL ,
          1 ,
          4 ,
          1 ,
          1 ,
          0 ,
          N'What is the sum insured of the vehicle?' ,
          N'' ,
          N'^[0-9\.]+$'
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Asset' ,
          @CoverDefSPlanid ,
          141 ,
          NULL ,
          1 ,
          11 ,
          0 ,
          0 ,
          0 ,
          N'Asset' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Body Type' ,
          @CoverDefSPlanid ,
          1401 ,
          NULL ,
          1 ,
          13 ,
          1 ,
          1 ,
          0 ,
          N'Body Type' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Fuel Type' ,
          @CoverDefSPlanid ,
          1402 ,
          NULL ,
          1 ,
          14 ,
          1 ,
          1 ,
          0 ,
          N'Fuel Type' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'All Wheel Drive' ,
          @CoverDefSPlanid ,
          1404 ,
          NULL ,
          1 ,
          16 ,
          1 ,
          1 ,
          0 ,
          N'All Wheel Drive' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Under Factory Warranty' ,
          @CoverDefSPlanid ,
          1405 ,
          NULL ,
          1 ,
          17 ,
          1 ,
          1 ,
          0 ,
          N'Under Factory Warranty' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Odometer (km)' ,
          @CoverDefSPlanid ,
          1406 ,
          NULL ,
          1 ,
          18 ,
          1 ,
          1 ,
          0 ,
          N'Odometer (km)' ,
          N'' ,
          N'^[0-9\.]+$'
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Vehicle Use' ,
          @CoverDefSPlanid ,
          1407 ,
          NULL ,
          1 ,
          21 ,
          1 ,
          1 ,
          0 ,
          N'Vehicle Use' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Full Service History' ,
          @CoverDefSPlanid ,
          1410 ,
          NULL ,
          1 ,
          21 ,
          1 ,
          1 ,
          0 ,
          N'Full Service History' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Previous Breakdown' ,
          @CoverDefSPlanid ,
          1422 ,
          NULL ,
          1 ,
          28 ,
          1 ,
          1 ,
          0 ,
          N'Previous Breakdown' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Additional Services' ,
          @CoverDefSPlanid ,
          1413 ,
          NULL ,
          1 ,
          24 ,
          1 ,
          1 ,
          0 ,
          N'Additional Services' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Cover Type' ,
          @CoverDefSPlanid ,
          1431 ,
          NULL ,
          1 ,
          25 ,
          1 ,
          1 ,
          0 ,
          N'Cover Type' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Limited Scratch And Dent' ,
          @CoverDefSPlanid ,
          1415 ,
          NULL ,
          1 ,
          26 ,
          1 ,
          1 ,
          0 ,
          N'Limited Scratch And Dent' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Better Car' ,
          @CoverDefSPlanid ,
          1423 ,
          NULL ,
          1 ,
          27 ,
          1 ,
          1 ,
          0 ,
          N'Better Car' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Cover Boost' ,
          @CoverDefSPlanid ,
          1425 ,
          NULL ,
          1 ,
          21 ,
          1 ,
          1 ,
          0 ,
          N'Cover Boost' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Limited Service Plan' ,
          @CoverDefSPlanid ,
          1424 ,
          NULL ,
          1 ,
          22 ,
          1 ,
          1 ,
          0 ,
          N'Limited Service Plan' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Tyre Protect' ,
          @CoverDefSPlanid ,
          1419 ,
          NULL ,
          1 ,
          23 ,
          1 ,
          1 ,
          0 ,
          N'Tyre Protect' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Vehicle High Performance' ,
          @CoverDefSPlanid ,
          1430 ,
          NULL ,
          1 ,
          24 ,
          1 ,
          1 ,
          0 ,
          N'Vehicle High Performance' ,
          N'' ,
          N''
        )

INSERT[dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES( 0 ,
          N'Mechanical Warranty Last Covered' ,
          @CoverDefSPlanid ,
          1421 ,
          NULL ,
          1 ,
          25 ,
          1 ,
          1 ,
          0 ,
          N'Mechanical Warranty Last Covered' ,
          N'' ,
          N''
        )

DECLARE @SystemId UNIQUEIDENTIFIER;
DECLARE @channelId INT;

SET @SystemId = 'F7B4029D-C1DF-41E6-B8BE-69216A5524AA'
SELECT @channelId = Id FROM Channel WHERE SystemId = @SystemId;


IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @SystemId AND ChannelId = @channelId)
		BEGIN
			INSERT INTO SettingsiRate
				  ([ProductId]
				  ,[Password]
				  ,[AgentCode]
				  ,[UserId]
				  ,[IsDeleted]
				  ,[ProductCode]
				  ,[ChannelSystemId]
				  ,[ChannelId]
				  ,[Environment])
	
			VALUES(@productId,
				   'I2P0L1A6T2F0O1R6M',
				   'IPLATFORM',
				   '',
				   0,
				   'MOTORWAR',
				   @SystemId,
				   @channelId,
				   '')
	END