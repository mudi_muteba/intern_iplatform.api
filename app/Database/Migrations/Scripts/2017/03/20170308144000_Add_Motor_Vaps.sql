﻿SET ANSI_PADDING ON
GO

--party
INSERT  [dbo].[Party]
        ( [MasterId] ,
          [PartyTypeId] ,
          [ContactDetailId] ,
          [DisplayName] ,
          [DateCreated] ,
          [DateUpdated]
        )
VALUES  ( 0 ,
          2 ,
          NULL ,
          N'MotorVaps' ,
          NULL ,
          NULL
        )

DECLARE @Partyid INT
SET @Partyid = IDENT_CURRENT('Party')

--organization
INSERT  [dbo].[Organization]
        ( PartyId ,
          [Code] ,
          [RegisteredName] ,
          [TradingName] ,
          [TradingSince] ,
          [Description] ,
          [RegNo] ,
          [FspNo] ,
          [VatNo]
        )
VALUES  ( @Partyid ,
          N'MOTORV' ,
          N'MotorVaps' ,
          N'' ,
          '01/JUN/2012' ,
          '' ,
          N'' ,
          N'45790' ,
          N''
        )

--address
INSERT  [dbo].[Address]
        ( [PartyId] ,
          [MasterId] ,
          [Description] ,
          [Complex] ,
          [Line1] ,
          [Line2] ,
          [Line3] ,
          [Line4] ,
          [Code] ,
          [Latitude] ,
          [Longitude] ,
          [StateProvinceId] ,
          [AddressTypeId] ,
          [IsDefault] ,
          [IsComplex] ,
          [DateFrom] ,
          [DateTo]
        )
VALUES  ( @Partyid ,
          0 ,
          N'Head Office' ,
          N'Suite 201' ,
          N'Tokai Village Centre' ,
          N'' ,
          N'Tokai' ,
          N'Cape Town' ,
          N'7945' ,
          CAST(-25.789952 AS DECIMAL(9, 6)) ,
          CAST(28.278506 AS DECIMAL(9, 6)) ,
          4 ,
          1 ,
          1 ,
          1 ,
          '01/JUN/2012' ,
          NULL
        )

--product
INSERT  [dbo].[Product]
        ( [MasterId] ,
          [Name] ,
          [ProductOwnerId] ,
          [ProductProviderId] ,
          [ProductTypeId] ,
          [AgencyNumber] ,
          [ProductCode] ,
          [StartDate] ,
          [EndDate]
        )
VALUES  ( 0 ,
          N'Service Plan' ,
          @Partyid ,
          @Partyid ,
          7 ,
          N'123456' ,
          N'MOTORSERVICE' ,
          CAST(0x00009CD400000000 AS DATETIME) ,
          NULL
        )

DECLARE @Productid INT
SET @Productid = IDENT_CURRENT('Product')

--covers
INSERT  [dbo].[CoverDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [ProductId] ,
          [CoverId] ,
          [ParentId] ,
          [CoverDefinitionTypeId] ,
          [VisibleIndex]
        )
VALUES  ( 0 ,
          N'Service Plan' ,
          @Productid ,
          378 ,
          NULL ,
          1 ,
          0
        )

DECLARE @CoverDefSPlanid INT
SET @CoverDefSPlanid = IDENT_CURRENT('CoverDefinition')

--questions
INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Year Of Manufacture' ,
          @CoverDefSPlanid ,
          99 ,
          NULL ,
          1 ,
          0 ,
          1 ,
          1 ,
          0 ,
          N'What is the year of manufacture for this vehicle?' ,
          N'' ,
          N'^[0-9]+$'
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Make' ,
          @CoverDefSPlanid ,
          95 ,
          NULL ,
          1 ,
          1 ,
          1 ,
          1 ,
          0 ,
          N'What is the make of this vehicle?' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Model' ,
          @CoverDefSPlanid ,
          96 ,
          NULL ,
          1 ,
          2 ,
          1 ,
          1 ,
          0 ,
          N'What is the model of this vehicle?' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'MM Code' ,
          @CoverDefSPlanid ,
          94 ,
          NULL ,
          1 ,
          4 ,
          1 ,
          1 ,
          1 ,
          N'What is the Mead and McGrouther code for this vehicle?' ,
          N'' ,
          N'^[0-9]+$'
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Sum Insured' ,
          @CoverDefSPlanid ,
          102 ,
          NULL ,
          1 ,
          4 ,
          1 ,
          1 ,
          0 ,
          N'What is the sum insured of the vehicle?' ,
          N'' ,
          N'^[0-9\.]+$'
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Asset' ,
          @CoverDefSPlanid ,
          141 ,
          NULL ,
          1 ,
          11 ,
          0 ,
          0 ,
          0 ,
          N'Asset' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Engine Capacity (cc)' ,
          @CoverDefSPlanid ,
          1400 ,
          NULL ,
          1 ,
          12 ,
          1 ,
          1 ,
          0 ,
          N'Engine Capacity (cc)' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Body Type' ,
          @CoverDefSPlanid ,
          1401 ,
          NULL ,
          1 ,
          13 ,
          1 ,
          1 ,
          0 ,
          N'Body Type' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Fuel Type' ,
          @CoverDefSPlanid ,
          1402 ,
          NULL ,
          1 ,
          14 ,
          1 ,
          1 ,
          0 ,
          N'Fuel Type' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Turbo Or Super Charged' ,
          @CoverDefSPlanid ,
          1403 ,
          NULL ,
          1 ,
          15 ,
          1 ,
          1 ,
          0 ,
          N'Turbo Or Super Charged' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'All Wheel Drive' ,
          @CoverDefSPlanid ,
          1404 ,
          NULL ,
          1 ,
          16 ,
          1 ,
          1 ,
          0 ,
          N'All Wheel Drive' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Under Factory Warranty' ,
          @CoverDefSPlanid ,
          1405 ,
          NULL ,
          1 ,
          17 ,
          1 ,
          1 ,
          0 ,
          N'Under Factory Warranty' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Odometer (km)' ,
          @CoverDefSPlanid ,
          1406 ,
          NULL ,
          1 ,
          18 ,
          1 ,
          1 ,
          0 ,
          N'Odometer (km)' ,
          N'' ,
          N'^[0-9\.]+$'
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Vehicle Use' ,
          @CoverDefSPlanid ,
          1407 ,
          NULL ,
          1 ,
          21 ,
          1 ,
          1 ,
          0 ,
          N'Vehicle Use' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N' Service Annually Or Intervals' ,
          @CoverDefSPlanid ,
          1408 ,
          NULL ,
          1 ,
          19 ,
          1 ,
          1 ,
          0 ,
          N' Service Annually Or Intervals' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Service Intervals (km)' ,
          @CoverDefSPlanid ,
          1409 ,
          NULL ,
          1 ,
          20 ,
          1 ,
          1 ,
          0 ,
          N'Service Intervals (km)' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Full Service History' ,
          @CoverDefSPlanid ,
          1410 ,
          NULL ,
          1 ,
          21 ,
          1 ,
          1 ,
          0 ,
          N'Full Service History' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Last Serviced (km)' ,
          @CoverDefSPlanid ,
          1411 ,
          NULL ,
          1 ,
          22 ,
          1 ,
          1 ,
          0 ,
          N'Last Serviced (km)' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Average Distance Per Year (km)' ,
          @CoverDefSPlanid ,
          1412 ,
          NULL ,
          1 ,
          28 ,
          1 ,
          1 ,
          0 ,
          N'Average Distance Per Year (km)' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Additional Services' ,
          @CoverDefSPlanid ,
          1413 ,
          NULL ,
          1 ,
          24 ,
          1 ,
          1 ,
          0 ,
          N'Additional Services' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Plan Duration (months)' ,
          @CoverDefSPlanid ,
          1414 ,
          NULL ,
          1 ,
          25 ,
          1 ,
          1 ,
          0 ,
          N'Plan Duration (months)' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Limited Scratch And Dent' ,
          @CoverDefSPlanid ,
          1415 ,
          NULL ,
          1 ,
          26 ,
          1 ,
          1 ,
          0 ,
          N'Limited Scratch And Dent' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Cambelt Service Extension' ,
          @CoverDefSPlanid ,
          1416 ,
          NULL ,
          1 ,
          27 ,
          1 ,
          1 ,
          0 ,
          N'Cambelt Service Extension' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Premier Extension' ,
          @CoverDefSPlanid ,
          1417 ,
          NULL ,
          1 ,
          21 ,
          1 ,
          1 ,
          0 ,
          N'Premier Extension' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Service Plan Extra' ,
          @CoverDefSPlanid ,
          1418 ,
          NULL ,
          1 ,
          22 ,
          1 ,
          1 ,
          0 ,
          N'Service Plan Extra' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Tyre Protect' ,
          @CoverDefSPlanid ,
          1419 ,
          NULL ,
          1 ,
          23 ,
          1 ,
          1 ,
          0 ,
          N'Tyre Protect' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Major Component Cover' ,
          @CoverDefSPlanid ,
          1420 ,
          NULL ,
          1 ,
          24 ,
          1 ,
          1 ,
          0 ,
          N'Major Component Cover' ,
          N'' ,
          N''
        )