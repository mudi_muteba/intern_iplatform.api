﻿DECLARE @Partyid INT

SET  @Partyid= (SELECT TOP 1 Id From [dbo].[Party] WHERE DisplayName =  N'Bryte')

If (@Partyid IS NULL)
BEGIN

	INSERT [dbo].[Party] ([MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated])
	VALUES (0, 2, NULL, N'Bryte', NULL, NULL)

	SET @Partyid = IDENT_CURRENT('Party')
END

IF((SELECT  1 FROM [dbo].[Organization]  WHERE Code = 'ZUR' AND  RegisteredName =  N'Bryte Insurance Company Limited') IS NULL )
BEGIN

INSERT [dbo].[Organization] ([PartyId], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo])
 VALUES (@Partyid,  N'ZUR', N'Bryte Insurance Company Limited', N'Bryte', '01/JAN/1965', '', N'1965/006764/06', N'17703', N'')

--address
INSERT [dbo].[Address] ([PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) 
VALUES (@Partyid, 0, N'Bryte Insurance Company Limited', N'15 Marshall Street', N'', N'Ferreirasdorp', N'', N'Johannesburg', N'2001', NULL, NULL, 4, 1, 1, 1, '01/JAN/1970', NULL)

END

DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @allriskCoverId INT = 17;
DECLARE @multiquoteProductId INT = 27;


SET @id = (SELECT TOP 1 PartyId From [Organization] where TradingName = 'Bryte')
if (@id IS NULL)
Return

SET @Name = N'Bryte ZBox'

 
SET @productid = ( SELECT ID FROM dbo.Product where  Name = @Name AND ProductOwnerID = @id)
SET @allriskCoverId = (SELECT ID from [md].[Cover] WHERE Code = 'ALL_RISK')

if (@productid IS NULL)
BEGIN
--Zurich
INSERT INTO dbo.Product
        ( MasterId ,
          Name ,
          ProductOwnerId ,
          ProductProviderId ,
          ProductTypeId ,
          AgencyNumber ,
          ProductCode ,
          StartDate ,
          EndDate ,
          RiskItemTypeId ,
          IsDeleted ,
          ImageName ,
          Audit ,
          QuoteExpiration
        )
VALUES  ( 0 , -- MasterId - int
          @Name , -- Name - nvarchar(255)
          @id , -- ProductOwnerId - int
          @id , -- ProductProviderId - int
          7 , -- ProductTypeId - int
          N'123456' , -- AgencyNumber - nvarchar(255)
          N'ZURPERS' , -- ProductCode - nvarchar(255)
          CAST(0x00009CD400000000 AS DateTime) , -- StartDate - datetime
          CAST(0x00009CD400000000 AS DateTime) , -- EndDate - datetime
          0 , -- RiskItemTypeId - int
          0 , -- IsDeleted - bit
          N'bryte.png' , -- ImageName - nvarchar(1024)
          NULL , -- Audit - nvarchar(max)
          30  -- QuoteExpiration - int
        )

		SET @id = SCOPE_IDENTITY()
		SET @productid = @id;
 END
 ELSE
 BEGIN
	SET @id = @productid
 END


DECLARE @CoverDefinitionId INT;
DECLARE @MultiQuoteDummyQuestionId INT = 1081;

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @allriskCoverId)


SELECT @MultiQuoteDummyQuestionId = ID FROM QuestionDefinition 
WHERE CoverDefinitionId = 99 AND QuestionId = 1081;

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
VALUES (0, N'All Risk', @id, @allriskCoverId, NULL, 1, 0, 0)
 
SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=126 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'All Risk Category', @CoverDefinitionId, 126, NULL, 1, 1, 1, 1, 0,
               N'What category does the item fall under?', N'' , N'', 0, 6)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 5, 1, 1, 0,
               N'What is the sum insured of the item?', N'' , N'^[0-9\.]+$', 0, NULL)
              end

if not exists(select * from QuestionDefinition where QuestionId=1 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Pensioner', @CoverDefinitionId, 1, NULL, 1, 3, 1, 1, 0,
               N'Is the client a pensioner?', N'' , N'', 0, NULL)
			  end

if not exists(select * from QuestionDefinition where QuestionId=127 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Desciption', @CoverDefinitionId, 127, NULL, 1, 2, 1, 1, 0,
               N'Enter a description of the item', N'' , N'', 0, 6)
              end

