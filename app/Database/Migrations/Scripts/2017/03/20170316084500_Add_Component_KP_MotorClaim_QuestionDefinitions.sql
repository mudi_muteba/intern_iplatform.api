﻿
IF OBJECT_ID(N'dbo.ComponentQuestionDefinition', N'U') IS NOT NULL
BEGIN 
		
	Insert Into [dbo].[ComponentQuestionDefinition]
	( 
		[DisplayName],[ChannelId],[ComponentQuestionId],[ComponentQuestionGroupId],[ComponentTypeId],[VisibleIndex],[IsRequieredForQuote],[IsRatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[IsDeleted]     
	)
	VALUES(
	'Claimed For Incident',5,1,3,2,1,1,1,0,'','','',0
	)
	Insert Into [dbo].[ComponentQuestionDefinition]
	( 
		[DisplayName],[ChannelId],[ComponentQuestionId],[ComponentQuestionGroupId],[ComponentTypeId],[VisibleIndex],[IsRequieredForQuote],[IsRatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[IsDeleted]     
	)
	VALUES(
	'Loss Value',5,2,3,2,2,1,1,0,'','','',0
	)
	Insert Into [dbo].[ComponentQuestionDefinition]
	( 
		[DisplayName],[ChannelId],[ComponentQuestionId],[ComponentQuestionGroupId],[ComponentTypeId],[VisibleIndex],[IsRequieredForQuote],[IsRatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[IsDeleted]     
	)
	VALUES(
	'Incident Type',5,3,3,2,3,1,1,0,'','','',0
	)
	Insert Into [dbo].[ComponentQuestionDefinition]
	( 
		[DisplayName],[ChannelId],[ComponentQuestionId],[ComponentQuestionGroupId],[ComponentTypeId],[VisibleIndex],[IsRequieredForQuote],[IsRatingFactor],[IsReadOnly],[ToolTip],[DefaultValue],[RegexPattern],[IsDeleted]     
	)
	VALUES(
	'Date Of Loss',5,4,3,2,4,1,1,0,'','','',0
	)
		
END
ELSE
BEGIN
	PRINT('TABLE NOT FOUND: dbo.ComponentQuestionDefinition');
END