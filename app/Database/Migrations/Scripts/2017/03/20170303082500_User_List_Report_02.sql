﻿if not exists(select * from Report where Name = 'User List')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, Info)
		values
			(
				4,
				2,
				3,
				'User List',
				'Provides a detailed list of users across all channels of an iPlatform instance.',
				'UserList.trdx',
				1,
				1,
				1,
				'
				Provides a detailed list of users across all channels of an iPlatform instance.

				<ul>
					<li>
						<strong>Broker Name</strong>
						<ul><li>The name of the Broker or Insurance Company as setup in iPlatform, also known as a Channel.</li></ul>
					</li>
					<li>
						<strong>Role</strong>
						<ul><li>The role of the user in iPlatform related to the Channel.</li></ul>
					</li>
					<li>
						<strong>Agent Name & Surname</strong>
						<ul><li>The full name of the listed user.</li></ul>
					</li>
					<li>
						<strong>Agent E-Mail</strong>
						<ul><li>The e-mail address of the listed user.</li></ul>
					</li>
					<li>
						<strong>Active</strong>
						<ul><li>Whether or not the user was active in the selected date period.</li></ul>
					</li>
				</ul>
				'
			)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 4,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'User List',
			[Description] = 'Provides a detailed list of users across all channels of an iPlatform instance.',
			SourceFile = 'UserList.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1,
			Info = '
				Provides a detailed list of users across all channels of an iPlatform instance.

				<ul>
					<li>
						<strong>Broker Name</strong>
						<ul><li>The name of the Broker or Insurance Company as setup in iPlatform, also known as a Channel.</li></ul>
					</li>
					<li>
						<strong>Role</strong>
						<ul><li>The role of the user in iPlatform related to the Channel.</li></ul>
					</li>
					<li>
						<strong>Agent Name & Surname</strong>
						<ul><li>The full name of the listed user.</li></ul>
					</li>
					<li>
						<strong>Agent E-Mail</strong>
						<ul><li>The e-mail address of the listed user.</li></ul>
					</li>
					<li>
						<strong>Active</strong>
						<ul><li>Whether or not the user was active in the selected date period.</li></ul>
					</li>
				</ul>
				'
		where
			Name = 'User List'
	end
go