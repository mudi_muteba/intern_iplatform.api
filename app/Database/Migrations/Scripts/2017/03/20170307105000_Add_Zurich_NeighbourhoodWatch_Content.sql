﻿DECLARE @ProductId INT;
SET @ProductId = (SELECT Id FROM Product WHERE ProductCode = 'ZURPERS');
PRINT @ProductId;

DECLARE @MultiQuoteDummyQuestionId INT;
SELECT @MultiQuoteDummyQuestionId = ID FROM QuestionDefinition 
WHERE CoverDefinitionId = 97 AND QuestionId = 1081;
PRINT @MultiQuoteDummyQuestionId;

DECLARE @CoverId INT;
SET @CoverId = (SELECT Id FROM md.Cover WHERE Code = 'CONTENTS');
PRINT @CoverId;

DECLARE @CoverDefinitionId INT;
SET @CoverDefinitionId = (SELECT Id FROM CoverDefinition WHERE ProductId = @ProductId AND CoverId = @CoverId);
PRINT @CoverDefinitionId

if not exists(select * from QuestionDefinition where QuestionId=1439 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Neighbourhood Watch', @CoverDefinitionId, 1439, NULL, 1, 10, 1, 1, 0,
               N'Does the risk address area have a neighbourhood watch?', N'' , N'', 0, NULL)

			  DECLARE @NeighbourhoodWatchId INT;
			  SET @NeighbourhoodWatchId  = SCOPE_IDENTITY();
			  
			  INSERT INTO MapQuestionDefinition(ParentId, ChildId, IsDeleted)
			  VALUES(@MultiQuoteDummyQuestionId, @NeighbourhoodWatchId, 0);

              end