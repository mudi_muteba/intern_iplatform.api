﻿
	DECLARE @code nvarchar(50)
	DECLARE @Name nvarchar(50)
	DECLARE @Usergroupid int
	DECLARE @channelId int
	DECLARE @productId INT;
	DECLARE @SystemId UNIQUEIDENTIFIER;

	SET @code = 'Cardinal'
	SET @Name = 'Cardinal'
	SET @Usergroupid = 5
	SET @SystemId = '38c63b4c-6682-4108-ac1c-35ae52a3dc30';
	SELECT @productId = Id FROM Product WHERE ProductCode = 'MOTORSERVICE';
	SELECT @channelId = MAX(Id) + 1 FROM Channel

	IF NOT EXISTS (SELECT * FROM Channel WHERE SystemId = @SystemId)
		BEGIN

			insert into Channel (Id, SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, ExternalReference, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
			values (@channelId, @SystemId, 197, 1, 2, null, @code, @name, @code, 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)

			DECLARE @UserId INT
			SELECT @UserId = Id FROM [User] WHERE UserName = 'root@iplatform.co.za'
			EXEC sp_executesql N'INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3); select SCOPE_IDENTITY()',N'@p0 int,@p1 bit,@p2 int,@p3 int',@p0=@Usergroupid,@p1=0,@p2=@UserId,@p3=@channelId
			EXEC sp_executesql N'INSERT INTO UserChannel (CreatedAt, ModifiedAt, IsDefault, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3, @p4, @p5); select SCOPE_IDENTITY()',N'@p0 datetime,@p1 datetime,@p2 bit,@p3 bit,@p4 int,@p5 int',@p0='2016-11-04 11:01:55',@p1='2016-11-04 11:01:55',@p2=0,@p3=0,@p4=@UserId,@p5=@channelId
			
			SELECT @UserId = Id FROM [User] WHERE UserName = 'admin@iplatform.co.za'
			EXEC sp_executesql N'INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3); select SCOPE_IDENTITY()',N'@p0 int,@p1 bit,@p2 int,@p3 int',@p0=@Usergroupid,@p1=0,@p2=@UserId,@p3=@channelId
			EXEC sp_executesql N'INSERT INTO UserChannel (CreatedAt, ModifiedAt, IsDefault, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3, @p4, @p5); select SCOPE_IDENTITY()',N'@p0 datetime,@p1 datetime,@p2 bit,@p3 bit,@p4 int,@p5 int',@p0='2016-11-04 11:01:55',@p1='2016-11-04 11:01:55',@p2=0,@p3=0,@p4=@UserId,@p5=@channelId

		END

		IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @SystemId)
		BEGIN
			INSERT INTO SettingsiRate
				  ([ProductId]
				  ,[Password]
				  ,[AgentCode]
				  ,[UserId]
				  ,[IsDeleted]
				  ,[ProductCode]
				  ,[ChannelSystemId]
				  ,[ChannelId]
				  ,[Environment])
	
			VALUES(@productId,
				   'C2A0R1D5INAL',
				   'CARDINAL',
				   '',
				   0,
				   'MOTORSERVICE',
				   @SystemId,
				   @channelId,
				   '')
	END