﻿DECLARE @ProductId INT;
SET @ProductId = (SELECT Id FROM Product WHERE ProductCode = 'ZURPERS');
--PRINT @ProductId;

DECLARE @MultiQuoteDummyQuestionId INT;
SELECT @MultiQuoteDummyQuestionId = ID FROM QuestionDefinition 
WHERE CoverDefinitionId = 96 AND QuestionId = 1081;
--PRINT @MultiQuoteDummyQuestionId;

DECLARE @CoverId INT;
SET @CoverId = (SELECT Id FROM md.Cover WHERE Code = 'MOTOR');
PRINT @CoverId;

DECLARE @CoverDefinitionId INT;
SET @CoverDefinitionId = (SELECT Id FROM CoverDefinition WHERE ProductId = @ProductId AND CoverId = @CoverId);
--PRINT @CoverDefinitionId

if not exists(select * from QuestionDefinition where QuestionId=1432 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Voluntary Excess', @CoverDefinitionId, 1432, NULL, 1, 21, 0, 1, 0,
               N'A voluntary excess will reduce a clients premium.', N'201' , N'', 0, NULL)

			  DECLARE @VoluntaryExcessId INT;
			  SET @VoluntaryExcessId  = SCOPE_IDENTITY();
			  
			  INSERT INTO MapQuestionDefinition(ParentId, ChildId, IsDeleted)
			  VALUES(@MultiQuoteDummyQuestionId, @VoluntaryExcessId, 0);

              end

 UPDATE QuestionDefinition
 SET Hide = 1, IsDeleted = 1
 WHERE QuestionId IN (1197, 1196);