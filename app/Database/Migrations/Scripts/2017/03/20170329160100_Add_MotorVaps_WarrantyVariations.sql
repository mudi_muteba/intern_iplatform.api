﻿SET ANSI_PADDING ON
GO

DECLARE @Partyid INT
SET @Partyid = (SELECT top 1 partyid FROM dbo.Organization WHERE code = 'MOTORV')

--product
INSERT[dbo].[Product]
        ( [MasterId] ,
          [Name] ,
          [ProductOwnerId] ,
          [ProductProviderId] ,
          [ProductTypeId] ,
          [AgencyNumber] ,
          [ProductCode] ,
          [StartDate] ,
          [EndDate]
        )
VALUES( 0 ,
          N'Warranty' ,
          @Partyid ,
          @Partyid ,
          7 ,
          N'123456' ,
          N'MOTORWAR1' ,
          CAST(0x00009CD400000000 AS DATETIME) ,
          NULL
        )

DECLARE @Productid INT
SET @Productid = IDENT_CURRENT('Product')

--covers
INSERT[dbo].[CoverDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [ProductId] ,
          [CoverId] ,
          [ParentId] ,
          [CoverDefinitionTypeId] ,
          [VisibleIndex]
        )
VALUES( 0 ,
          N'Motor Warranty' ,
          @Productid ,
          375 ,
          NULL ,
          1 ,
          0
        )

DECLARE @SystemId UNIQUEIDENTIFIER;
DECLARE @channelId INT;

SET @SystemId = 'F7B4029D-C1DF-41E6-B8BE-69216A5524AA'
SELECT @channelId = Id FROM Channel WHERE SystemId = @SystemId;


IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @SystemId AND ChannelId = @channelId)
		BEGIN
			INSERT INTO SettingsiRate
				  ([ProductId]
				  ,[Password]
				  ,[AgentCode]
				  ,[UserId]
				  ,[IsDeleted]
				  ,[ProductCode]
				  ,[ChannelSystemId]
				  ,[ChannelId]
				  ,[Environment])
	
			VALUES(@productId,
				   'I2P0L1A6T2F0O1R6M',
				   'IPLATFORM',
				   '',
				   0,
				   'MOTORWAR1',
				   @SystemId,
				   @channelId,
				   '')
	END

--product
INSERT[dbo].[Product]
        ( [MasterId] ,
          [Name] ,
          [ProductOwnerId] ,
          [ProductProviderId] ,
          [ProductTypeId] ,
          [AgencyNumber] ,
          [ProductCode] ,
          [StartDate] ,
          [EndDate]
        )
VALUES( 0 ,
          N'Warranty' ,
          @Partyid ,
          @Partyid ,
          7 ,
          N'123456' ,
          N'MOTORWAR2' ,
          CAST(0x00009CD400000000 AS DATETIME) ,
          NULL
        )

SET @Productid = IDENT_CURRENT('Product')

--covers
INSERT[dbo].[CoverDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [ProductId] ,
          [CoverId] ,
          [ParentId] ,
          [CoverDefinitionTypeId] ,
          [VisibleIndex]
        )
VALUES( 0 ,
          N'Motor Warranty' ,
          @Productid ,
          375 ,
          NULL ,
          1 ,
          0
        )

IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @SystemId AND ChannelId = @channelId)
		BEGIN
			INSERT INTO SettingsiRate
				  ([ProductId]
				  ,[Password]
				  ,[AgentCode]
				  ,[UserId]
				  ,[IsDeleted]
				  ,[ProductCode]
				  ,[ChannelSystemId]
				  ,[ChannelId]
				  ,[Environment])
	
			VALUES(@productId,
				   'I2P0L1A6T2F0O1R6M',
				   'IPLATFORM',
				   '',
				   0,
				   'MOTORWAR2',
				   @SystemId,
				   @channelId,
				   '')
	END

--product
INSERT[dbo].[Product]
        ( [MasterId] ,
          [Name] ,
          [ProductOwnerId] ,
          [ProductProviderId] ,
          [ProductTypeId] ,
          [AgencyNumber] ,
          [ProductCode] ,
          [StartDate] ,
          [EndDate]
        )
VALUES( 0 ,
          N'Warranty' ,
          @Partyid ,
          @Partyid ,
          7 ,
          N'123456' ,
          N'MOTORWAR3' ,
          CAST(0x00009CD400000000 AS DATETIME) ,
          NULL
        )

SET @Productid = IDENT_CURRENT('Product')

--covers
INSERT[dbo].[CoverDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [ProductId] ,
          [CoverId] ,
          [ParentId] ,
          [CoverDefinitionTypeId] ,
          [VisibleIndex]
        )
VALUES( 0 ,
          N'Motor Warranty' ,
          @Productid ,
          375 ,
          NULL ,
          1 ,
          0
        )

IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @SystemId AND ChannelId = @channelId)
		BEGIN
			INSERT INTO SettingsiRate
				  ([ProductId]
				  ,[Password]
				  ,[AgentCode]
				  ,[UserId]
				  ,[IsDeleted]
				  ,[ProductCode]
				  ,[ChannelSystemId]
				  ,[ChannelId]
				  ,[Environment])
	
			VALUES(@productId,
				   'I2P0L1A6T2F0O1R6M',
				   'IPLATFORM',
				   '',
				   0,
				   'MOTORWAR3',
				   @SystemId,
				   @channelId,
				   '')
	END

--product
INSERT[dbo].[Product]
        ( [MasterId] ,
          [Name] ,
          [ProductOwnerId] ,
          [ProductProviderId] ,
          [ProductTypeId] ,
          [AgencyNumber] ,
          [ProductCode] ,
          [StartDate] ,
          [EndDate]
        )
VALUES( 0 ,
          N'Warranty' ,
          @Partyid ,
          @Partyid ,
          7 ,
          N'123456' ,
          N'MOTORWAR4' ,
          CAST(0x00009CD400000000 AS DATETIME) ,
          NULL
        )

SET @Productid = IDENT_CURRENT('Product')

--covers
INSERT[dbo].[CoverDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [ProductId] ,
          [CoverId] ,
          [ParentId] ,
          [CoverDefinitionTypeId] ,
          [VisibleIndex]
        )
VALUES( 0 ,
          N'Motor Warranty' ,
          @Productid ,
          375 ,
          NULL ,
          1 ,
          0
        )

IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @SystemId AND ChannelId = @channelId)
		BEGIN
			INSERT INTO SettingsiRate
				  ([ProductId]
				  ,[Password]
				  ,[AgentCode]
				  ,[UserId]
				  ,[IsDeleted]
				  ,[ProductCode]
				  ,[ChannelSystemId]
				  ,[ChannelId]
				  ,[Environment])
	
			VALUES(@productId,
				   'I2P0L1A6T2F0O1R6M',
				   'IPLATFORM',
				   '',
				   0,
				   'MOTORWAR4',
				   @SystemId,
				   @channelId,
				   '')
	END

--product
INSERT[dbo].[Product]
        ( [MasterId] ,
          [Name] ,
          [ProductOwnerId] ,
          [ProductProviderId] ,
          [ProductTypeId] ,
          [AgencyNumber] ,
          [ProductCode] ,
          [StartDate] ,
          [EndDate]
        )
VALUES( 0 ,
          N'Warranty' ,
          @Partyid ,
          @Partyid ,
          7 ,
          N'123456' ,
          N'MOTORWAR5' ,
          CAST(0x00009CD400000000 AS DATETIME) ,
          NULL
        )

SET @Productid = IDENT_CURRENT('Product')

--covers
INSERT[dbo].[CoverDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [ProductId] ,
          [CoverId] ,
          [ParentId] ,
          [CoverDefinitionTypeId] ,
          [VisibleIndex]
        )
VALUES( 0 ,
          N'Motor Warranty' ,
          @Productid ,
          375 ,
          NULL ,
          1 ,
          0
        )

IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @SystemId AND ChannelId = @channelId)
		BEGIN
			INSERT INTO SettingsiRate
				  ([ProductId]
				  ,[Password]
				  ,[AgentCode]
				  ,[UserId]
				  ,[IsDeleted]
				  ,[ProductCode]
				  ,[ChannelSystemId]
				  ,[ChannelId]
				  ,[Environment])
	
			VALUES(@productId,
				   'I2P0L1A6T2F0O1R6M',
				   'IPLATFORM',
				   '',
				   0,
				   'MOTORWAR5',
				   @SystemId,
				   @channelId,
				   '')
	END

--product
INSERT[dbo].[Product]
        ( [MasterId] ,
          [Name] ,
          [ProductOwnerId] ,
          [ProductProviderId] ,
          [ProductTypeId] ,
          [AgencyNumber] ,
          [ProductCode] ,
          [StartDate] ,
          [EndDate]
        )
VALUES( 0 ,
          N'Warranty' ,
          @Partyid ,
          @Partyid ,
          7 ,
          N'123456' ,
          N'MOTORWAR6' ,
          CAST(0x00009CD400000000 AS DATETIME) ,
          NULL
        )

SET @Productid = IDENT_CURRENT('Product')

--covers
INSERT[dbo].[CoverDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [ProductId] ,
          [CoverId] ,
          [ParentId] ,
          [CoverDefinitionTypeId] ,
          [VisibleIndex]
        )
VALUES( 0 ,
          N'Motor Warranty' ,
          @Productid ,
          375 ,
          NULL ,
          1 ,
          0
        )

IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @SystemId AND ChannelId = @channelId)
		BEGIN
			INSERT INTO SettingsiRate
				  ([ProductId]
				  ,[Password]
				  ,[AgentCode]
				  ,[UserId]
				  ,[IsDeleted]
				  ,[ProductCode]
				  ,[ChannelSystemId]
				  ,[ChannelId]
				  ,[Environment])
	
			VALUES(@productId,
				   'I2P0L1A6T2F0O1R6M',
				   'IPLATFORM',
				   '',
				   0,
				   'MOTORWAR6',
				   @SystemId,
				   @channelId,
				   '')
	END

--product
INSERT[dbo].[Product]
        ( [MasterId] ,
          [Name] ,
          [ProductOwnerId] ,
          [ProductProviderId] ,
          [ProductTypeId] ,
          [AgencyNumber] ,
          [ProductCode] ,
          [StartDate] ,
          [EndDate]
        )
VALUES( 0 ,
          N'Warranty' ,
          @Partyid ,
          @Partyid ,
          7 ,
          N'123456' ,
          N'MOTORWAR7' ,
          CAST(0x00009CD400000000 AS DATETIME) ,
          NULL
        )

SET @Productid = IDENT_CURRENT('Product')

--covers
INSERT[dbo].[CoverDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [ProductId] ,
          [CoverId] ,
          [ParentId] ,
          [CoverDefinitionTypeId] ,
          [VisibleIndex]
        )
VALUES( 0 ,
          N'Motor Warranty' ,
          @Productid ,
          375 ,
          NULL ,
          1 ,
          0
        )

IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @SystemId AND ChannelId = @channelId)
		BEGIN
			INSERT INTO SettingsiRate
				  ([ProductId]
				  ,[Password]
				  ,[AgentCode]
				  ,[UserId]
				  ,[IsDeleted]
				  ,[ProductCode]
				  ,[ChannelSystemId]
				  ,[ChannelId]
				  ,[Environment])
	
			VALUES(@productId,
				   'I2P0L1A6T2F0O1R6M',
				   'IPLATFORM',
				   '',
				   0,
				   'MOTORWAR7',
				   @SystemId,
				   @channelId,
				   '')
	END

--product
INSERT[dbo].[Product]
        ( [MasterId] ,
          [Name] ,
          [ProductOwnerId] ,
          [ProductProviderId] ,
          [ProductTypeId] ,
          [AgencyNumber] ,
          [ProductCode] ,
          [StartDate] ,
          [EndDate]
        )
VALUES( 0 ,
          N'Warranty' ,
          @Partyid ,
          @Partyid ,
          7 ,
          N'123456' ,
          N'MOTORWAR8' ,
          CAST(0x00009CD400000000 AS DATETIME) ,
          NULL
        )

SET @Productid = IDENT_CURRENT('Product')

--covers
INSERT[dbo].[CoverDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [ProductId] ,
          [CoverId] ,
          [ParentId] ,
          [CoverDefinitionTypeId] ,
          [VisibleIndex]
        )
VALUES( 0 ,
          N'Motor Warranty' ,
          @Productid ,
          375 ,
          NULL ,
          1 ,
          0
        )

IF NOT EXISTS (SELECT Id FROM SettingsiRate WHERE [ProductId] = @productId AND [ChannelSystemId] = @SystemId AND ChannelId = @channelId)
		BEGIN
			INSERT INTO SettingsiRate
				  ([ProductId]
				  ,[Password]
				  ,[AgentCode]
				  ,[UserId]
				  ,[IsDeleted]
				  ,[ProductCode]
				  ,[ChannelSystemId]
				  ,[ChannelId]
				  ,[Environment])
	
			VALUES(@productId,
				   'I2P0L1A6T2F0O1R6M',
				   'IPLATFORM',
				   '',
				   0,
				   'MOTORWAR8',
				   @SystemId,
				   @channelId,
				   '')
	END