if object_id('tempdb..#tempDoBTable') is not null
	begin
		drop table #tempDoBTable
	end
go

with cteContactId as
(
SELECT        distinct cra.IdentityNo,  SUBSTRING(cra.IdentityNo,1,2) yearDOB,SUBSTRING(cra.IdentityNo,3,2) monthDOB,SUBSTRING(cra.IdentityNo,5,2) dayDOB
FROM        dbo.Contact cra
WHERE        LEN(CRA.IdentityNo)=13
)
select
	IdentityNo,
	case
            when yearDOB > 16 then '19' + convert(nvarchar(2),yearDOB)
            when yearDOB < 16 then '20' + convert(nvarchar(2),yearDOB)
        end
        + '-' +
        
        case when monthDOB = 01 then '01'
             when monthDOB = 02 then '02'
             when monthDOB = 03 then '03'
             when monthDOB = 04 then '04'
             when monthDOB = 05 then '05'
             when monthDOB = 06 then '06'
             when monthDOB = 07 then '07'
             when monthDOB = 08 then '08'
             when monthDOB = 09 then '09'
             when monthDOB = 10 then '10'
             when monthDOB = 11 then '11'
             when monthDOB = 12 then '12'
        end   + '-' +

        convert(nvarchar(11),dayDOB
    ) NewDateOfBirth into #tempDoBTable
from cteContactId

update Contact
set
	Contact.DateOfBirth = temp.NewDateOfBirth
from
    #tempDoBTable AS temp
    inner join Contact AS Contact on Contact.IdentityNo = temp.IdentityNo

	if object_id('tempdb..#tempDoBTable') is not null
	begin
		drop table #tempDoBTable
	end
go