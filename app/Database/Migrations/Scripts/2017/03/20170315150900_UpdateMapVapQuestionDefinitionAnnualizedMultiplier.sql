﻿IF EXISTS
(
	SELECT *
	FROM MapVapQuestionDefinition AS MVQD WITH (NOLOCK)
	WHERE AnnualizedMultiplier IS NULL
)
BEGIN
	UPDATE MapVapQuestionDefinition
	SET
		AnnualizedMultiplier = 12
	WHERE AnnualizedMultiplier IS NULL
END