﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @CoverId INT


SET @Name = 'ZURPERS'

SET @productid = (SELECT TOP 1 ID FROM dbo.Product where  ProductCode = @Name )

if (@productid IS NULL)
BEGIN
    --ZURPERS Product does not exist so now we must first create it (out of scope)
 Return
 END
 ELSE
 BEGIN
	SET @id = @productid
 END


declare @CoverDefinitionId int

----------------------MOTORCYCLE-------------------
SET @CoverId = 224 -- 224 MOTORCYCLE

SET @CoverDefinitionId = ( SELECT TOP 1 ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)
IF (@CoverDefinitionId IS NULL)
BEGIN
--out of scope
return
END
ELSE
BEGIN
	update QuestionDefinition
set VisibleIndex=5
where QuestionId=1506 and CoverDefinitionId=@CoverDefinitionId --classofuse

END
		

