﻿UPDATE dbo.CoverDefinition
SET CoverId = 179
WHERE ProductId IN (
SELECT ID FROM dbo.Product WHERE ProductCode IN ('HOLEASYO','HOLEASYP'))
AND DisplayName LIKE 'Legal'
