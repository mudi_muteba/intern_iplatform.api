﻿DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @CoverDefinitionId INT, @CoverID INT,@NewQuestionId INT,@ParentCoverDefinitionId INT,@ChildCoverDefinitionId INT,@DummyQuestionDefId INT,@CoverName VARCHAR(50)

 
SET @Name = 'ZURPERS'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @Name)

if (@productid IS NULL)
	BEGIN
		PRINT('OUT Of scope to create the product')
	RETURN
 END
 		  	  
--------------------62	'Caravan Or Trailer'-----------------------
SET @CoverName = 'Caravan Or Trailer'
SET @CoverID = 62				

SET @CoverDefinitionId = ( SELECT TOP 1 ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverId)

IF (@CoverDefinitionId IS not NULL)

BEGIN
--Credit Shortfall Sum Insured			  			  
if not exists(select * from QuestionDefinition where QuestionId=1198 and CoverDefinitionId = @CoverDefinitionId)
            begin
            INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
            [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Credit Shortfall Sum Insured', @CoverDefinitionId, 1198, NULL, 1, 0, 1, 1, 0,
             N'Credit Shortfall Sum Insured', N'' , N'', 1, 2)
end
			  
--Type of Cover	
if not exists(select * from QuestionDefinition where QuestionId=53 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Type of Cover', @CoverDefinitionId, 53, NULL, 1, 0, 1, 1, 0,
             N'What type of cover is required for this vehicle?', N'' , N'', 1, 2)
end

--vehicle
if not exists(select * from QuestionDefinition where QuestionId=87 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Tracking Device', @CoverDefinitionId, 87, NULL, 1, 0, 1, 1, 0,
             N'Does this vehicle have a tracking device installed?', N'' , N'', 1, 2)
end

--Windscreen Cover
if not exists(select * from QuestionDefinition where QuestionId=133 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Windscreen Cover', @CoverDefinitionId, 133, NULL, 1, 0, 1, 1, 0,
             N'Does this vehicle have a tracking device installed?', N'' , N'', 1, 2)
end
			  	  
				  			  
--Suburb
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name?', N'' , N'', 1, 2)
              end
			  
			  
--Province
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 2, 1, 1, 0,
               N'What is the province?', N'' , N'', 1, 2)
              end
			  
			  
--Postal Code
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 3, 1, 1, 0,
               N'What is the postal code?', N'', N'', 1, 2)
              end

--Risk Address
if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the risk address?', N'' , N'', 1, 2)
			   end		

--Modified/Imported
if not exists(select * from QuestionDefinition where QuestionId=7 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Modified/Imported', @CoverDefinitionId, 7, NULL, 1, 0, 1, 1, 0,
             N'Was this vehicle imported or modified in any way?', N'' , N'', 1, 2)
 end

--Secure Complex
if not exists(select * from QuestionDefinition where QuestionId=14 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Secure Complex', @CoverDefinitionId, 14, NULL, 1, 0, 1, 1, 0,
             N'Is the vehicle parked in a security complex?', N'' , N'', 1, 2)
 end


--class of use
if not exists(select * from QuestionDefinition where QuestionId=1506 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Class of Use', @CoverDefinitionId, 1506, NULL, 1, 0, 1, 1, 0,
             N'What is the intended class of use for this vehicle?', N'' , N'', 1, 2)
 end

 --Pensioner
  if not exists(select * from QuestionDefinition where QuestionId=1 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Pensioner', @CoverDefinitionId, 1, NULL, 1, 4, 1, 1, 0,
             N'Is the client a pensioner?', N'' , N'', 1, 2)
 end

---Map To Multiquote
select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverId
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId 
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)



SET @NewQuestionId = 1506 -- classofuse

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id


SET @NewQuestionId = 1 -- classofuse

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id
END