﻿DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @CoverId INT


SET @Name = 'MUL'

SET @productid = (SELECT TOP 1 ID FROM dbo.Product where  ProductCode = @Name )

if (@productid IS NULL)
BEGIN
    --ZURPERS Product does not exist so now we must first create it (out of scope)
 Return
 END
 ELSE
 BEGIN
	SET @id = @productid
 END


declare @CoverDefinitionId int

----------------------MOTORCYCLE-------------------
SET @CoverId = 224 -- 224 MOTORCYCLE

SET @CoverDefinitionId = ( SELECT TOP 1 ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)
IF (@CoverDefinitionId IS NULL)
BEGIN
--out of scope
return
END
ELSE
BEGIN
update QuestionDefinition
set Hide=1
where QuestionId=139 and CoverDefinitionId=@CoverDefinitionId --Work Address

update QuestionDefinition
set Hide=1
where QuestionId=135 and CoverDefinitionId=@CoverDefinitionId --Suburb (Work)

update QuestionDefinition
set Hide=1
where QuestionId=154 and CoverDefinitionId=@CoverDefinitionId --Province (Work)

update QuestionDefinition
set Hide=1
where QuestionId=91 and CoverDefinitionId=@CoverDefinitionId --Postal Code (Work)

update QuestionDefinition
set Hide=1
where QuestionId=1181 and CoverDefinitionId=@CoverDefinitionId --Work Area Type
	

update QuestionDefinition
set Hide=1
where QuestionId=94 and CoverDefinitionId=@CoverDefinitionId --MM Code

update QuestionDefinition
set Hide=1
where QuestionId=1010 and CoverDefinitionId=@CoverDefinitionId --VIN Number

update QuestionDefinition
set Hide=1
where QuestionId=1013 and CoverDefinitionId=@CoverDefinitionId --Engine Number

update QuestionDefinition
set Hide=1
where QuestionId=1021 and CoverDefinitionId=@CoverDefinitionId --Engine CC

update QuestionDefinition
set Hide=1
where QuestionId=1019 and CoverDefinitionId=@CoverDefinitionId --Second Hand

update QuestionDefinition
set Hide=1
where QuestionId=142 and CoverDefinitionId=@CoverDefinitionId --Vehicle Colour

update QuestionDefinition
set Hide=1
where QuestionId=151 and CoverDefinitionId=@CoverDefinitionId --Vehicle Mileage

update QuestionDefinition
set Hide=1
where QuestionId=152 and CoverDefinitionId=@CoverDefinitionId --Vehicle Monthly Mileage


update QuestionDefinition
set Hide=1
where QuestionId=153 and CoverDefinitionId=@CoverDefinitionId --Vehicle Condition

update QuestionDefinition
set Hide=1
where QuestionId=142 and CoverDefinitionId=@CoverDefinitionId --Vehicle Colour

update QuestionDefinition
set Hide=1
where QuestionId=1483 and CoverDefinitionId=@CoverDefinitionId --Is Off Road

update QuestionDefinition
set Hide=1
where QuestionId=80 and CoverDefinitionId=@CoverDefinitionId --Immobiliser

update QuestionDefinition
set Hide=1
where QuestionId=137 and CoverDefinitionId=@CoverDefinitionId --Vehicle Overnight Access Control

update QuestionDefinition
set Hide=1
where QuestionId=86 and CoverDefinitionId=@CoverDefinitionId --Daytime Parking

update QuestionDefinition
set Hide=1
where QuestionId=136 and CoverDefinitionId=@CoverDefinitionId --Vehicle Daytime Access Control

update QuestionDefinition
set Hide=1
where QuestionId=143 and CoverDefinitionId=@CoverDefinitionId --Vehicle Paint Type

update QuestionDefinition
set Hide=1
where QuestionId=100 and CoverDefinitionId=@CoverDefinitionId --Registered Owner ID Number

update QuestionDefinition
set Hide=1
where QuestionId=107 and CoverDefinitionId=@CoverDefinitionId --Vehicle Extras Value

update QuestionDefinition
set Hide=1
where QuestionId=74 and CoverDefinitionId=@CoverDefinitionId --Voluntary Excess

update QuestionDefinition
set Hide=1
where QuestionId=64 and CoverDefinitionId=@CoverDefinitionId --Selected Excess

update QuestionDefinition
set Hide=1
where QuestionId=65 and CoverDefinitionId=@CoverDefinitionId --Valuation Method

update QuestionDefinition
set Hide=1
where QuestionId=104 and CoverDefinitionId=@CoverDefinitionId --Main Driver ID Number


update QuestionDefinition
set Hide=1
where QuestionId=72 and CoverDefinitionId=@CoverDefinitionId --Marital Status


update QuestionDefinition
set Hide=1
where QuestionId=41 and CoverDefinitionId=@CoverDefinitionId --Drivers Licence First Issued Date


update QuestionDefinition
set Hide=1
where QuestionId=73 and CoverDefinitionId=@CoverDefinitionId --Drivers Licence Type


update QuestionDefinition
set Hide=1
where QuestionId=70 and CoverDefinitionId=@CoverDefinitionId --Gender


update QuestionDefinition
set Hide=1
where QuestionId=42 and CoverDefinitionId=@CoverDefinitionId --Main Driver Date of Birth


update QuestionDefinition
set Hide=1
where QuestionId=60 and CoverDefinitionId=@CoverDefinitionId --Relationship to the Insured


update QuestionDefinition
set Hide=1
where QuestionId=37 and CoverDefinitionId=@CoverDefinitionId --Nominated Driver


update QuestionDefinition
set Hide=1
where QuestionId=146 and CoverDefinitionId=@CoverDefinitionId --Main Driver Title


update QuestionDefinition
set Hide=1
where QuestionId=144 and CoverDefinitionId=@CoverDefinitionId --Main Driver First Name


update QuestionDefinition
set Hide=1
where QuestionId=145 and CoverDefinitionId=@CoverDefinitionId --Main Driver Surname


update QuestionDefinition
set Hide=1
where QuestionId=147 and CoverDefinitionId=@CoverDefinitionId --Main Driver Card Date Valid From


update QuestionDefinition
set Hide=1
where QuestionId=148 and CoverDefinitionId=@CoverDefinitionId --Main Driver Card Date Valid To

update QuestionDefinition
set Hide=1
where QuestionId=149 and CoverDefinitionId=@CoverDefinitionId --Main Driver Last Accident Claim

update QuestionDefinition
set Hide=1
where QuestionId=150 and CoverDefinitionId=@CoverDefinitionId --Main Driver Last Theft Claim

update QuestionDefinition
set Hide=1
where QuestionId=48 and CoverDefinitionId=@CoverDefinitionId --Class of use

update QuestionDefinition
set Hide=1
where QuestionId=862 and CoverDefinitionId=@CoverDefinitionId --Financed

update QuestionDefinition
set Hide=1
where QuestionId=39 and CoverDefinitionId=@CoverDefinitionId --Previously Insured

update QuestionDefinition
set Hide=1
where QuestionId=103 and CoverDefinitionId=@CoverDefinitionId --Finance House

update QuestionDefinition
set Hide=1
where QuestionId=114 and CoverDefinitionId=@CoverDefinitionId --Current Insurance Period (In Years)

update QuestionDefinition
set Hide=1
where QuestionId=44 and CoverDefinitionId=@CoverDefinitionId --Claims in the past 12 months

update QuestionDefinition
set Hide=1
where QuestionId=45 and CoverDefinitionId=@CoverDefinitionId --Claims in the past 12 to 24 months

update QuestionDefinition
set Hide=1
where QuestionId=46 and CoverDefinitionId=@CoverDefinitionId --Claims in the past 24 to 36 months

END	


