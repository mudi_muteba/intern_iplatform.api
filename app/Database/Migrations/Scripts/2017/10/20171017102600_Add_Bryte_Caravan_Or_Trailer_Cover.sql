﻿DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @CoverDefinitionId INT, @CoverID INT ,@CoverName VARCHAR(50)

 
SET @Name = N'MUL'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @Name)

if (@productid IS NULL)
	BEGIN
		PRINT('OUT Of scope to create the product')
	RETURN
 END
 		  	  
----------------------Caravan Or Trailer---------------------------------------
select TOP 1 @CoverDefinitionId=Id from CoverDefinition where ProductId=@productid AND CoverId=62
--Adding Caravan Or Trailer cover
declare @coverlink int
SET @coverlink = (select Id from CoverLink where ChannelId = 7 and CoverDefinitionId=@CoverDefinitionId)

if (@coverlink IS NULL)
	BEGIN		
	insert into CoverLink(ChannelId,VisibleIndex,IsDeleted,CoverDefinitionId)
	values (7,0,0,@CoverDefinitionId)



 
--Credit Shortfall Sum Insured			  			  
if not exists(select * from QuestionDefinition where QuestionId=1198 and CoverDefinitionId = @CoverDefinitionId)
            begin
            INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
            [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Credit Shortfall Sum Insured', @CoverDefinitionId, 1198, NULL, 1, 0, 1, 1, 0,
             N'Credit Shortfall Sum Insured', N'' , N'', 1, 2)
end
			  
--Type of Cover	
if not exists(select * from QuestionDefinition where QuestionId=53 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Type of Cover', @CoverDefinitionId, 53, NULL, 1, 0, 1, 1, 0,
             N'What type of cover is required for this vehicle?', N'' , N'', 1, 2)
end

--vehicle
if not exists(select * from QuestionDefinition where QuestionId=87 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Tracking Device', @CoverDefinitionId, 87, NULL, 1, 0, 1, 1, 0,
             N'Does this vehicle have a tracking device installed?', N'' , N'', 1, 2)
end

--Windscreen Cover
if not exists(select * from QuestionDefinition where QuestionId=133 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Windscreen Cover', @CoverDefinitionId, 133, NULL, 1, 0, 1, 1, 0,
             N'Does this vehicle have a tracking device installed?', N'' , N'', 1, 2)
end
			  	  
				  			  
--Suburb
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name?', N'' , N'', 1, 2)
              end
			  
			  
--Province
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 2, 1, 1, 0,
               N'What is the province?', N'' , N'', 1, 2)
              end
			  
			  
--Postal Code
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 3, 1, 1, 0,
               N'What is the postal code?', N'', N'', 1, 2)
              end

--Risk Address
if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the risk address?', N'' , N'', 1, 2)
			   end		

--Modified/Imported
if not exists(select * from QuestionDefinition where QuestionId=7 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Modified/Imported', @CoverDefinitionId, 7, NULL, 1, 0, 1, 1, 0,
             N'Was this vehicle imported or modified in any way?', N'' , N'', 1, 2)
 end

--Secure Complex
if not exists(select * from QuestionDefinition where QuestionId=14 and CoverDefinitionId = @CoverDefinitionId)
             begin
             INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
             [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Secure Complex', @CoverDefinitionId, 14, NULL, 1, 0, 1, 1, 0,
             N'Is the vehicle parked in a security complex?', N'' , N'', 1, 2)
 end


--Work Address not required for carava/trailers
update QuestionDefinition
set RequiredForQuote=0,Hide=1
where QuestionId=139 and CoverDefinitionId=@CoverDefinitionId


--Existing damage
update QuestionDefinition
set RequiredForQuote=0, Hide=1
where QuestionId=1164 and CoverDefinitionId=@CoverDefinitionId


--Contents Value
update QuestionDefinition
set RequiredForQuote=0,Hide=1
where QuestionId=1165 and CoverDefinitionId=@CoverDefinitionId

--Parking Method
update QuestionDefinition
set RequiredForQuote=0,Hide=1
where QuestionId=1030 and CoverDefinitionId=@CoverDefinitionId

--Asset
update QuestionDefinition
set RequiredForQuote=0,Hide=1
where QuestionId=141 and CoverDefinitionId=@CoverDefinitionId

--Make
update QuestionDefinition
set RequiredForQuote=0,Hide=1
where QuestionId=95 and CoverDefinitionId=@CoverDefinitionId

--Model
update QuestionDefinition
set RequiredForQuote=0,Hide=1
where QuestionId=96 and CoverDefinitionId=@CoverDefinitionId


--ClassOfUse
update QuestionDefinition
set RequiredForQuote=0,Hide=1
where QuestionId=1032 and CoverDefinitionId=@CoverDefinitionId


--vehicletype
update QuestionDefinition
set DisplayName='Vehicle Type'
where QuestionId=1166 and CoverDefinitionId=@CoverDefinitionId 

	RETURN
 END