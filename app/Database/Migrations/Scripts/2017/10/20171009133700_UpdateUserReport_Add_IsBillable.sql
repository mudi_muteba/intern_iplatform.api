if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_User_GetUserStatistics_WithChannelIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_User_GetUserStatistics_WithChannelIds
	end
go

Create procedure Report_CallCentre_User_GetUserStatistics_WithChannelIds
(
	@ChannelIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	select distinct
		Channel.Name [Channel],
		md.AuthorisationGroup.Name [Role],
		dbo.ToCamelCase(ltrim(rtrim(Individual.FirstName + ' ' + Individual.Surname))) [Agent],
		[User].UserName [EMail],
		case when (select count(AuditLog.Id) from AuditLog where AuditLog.EntityId = [User].Id and AuditLog.AuditEntryType = 'UserAuthenticationSuccess' and AuditLog.RequestDate >= @DateFrom and AuditLog.RequestDate <= @DateTo) > 0
			then 'Yes' else 'No'
		End [Active],
		CASE [IsBillable]
			WHEN 1 THEN 'Yes'  
			ELSE 'No'
		END  [IsBillable]
	from [User]
		inner join UserIndividual on UserIndividual.UserId = [User].Id
		inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		inner join UserChannel on UserChannel.UserId = [User].Id
		inner join Channel on Channel.Id = UserChannel.ChannelId
		inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
		inner join md.AuthorisationGroup on md.AuthorisationGroup.Id = UserAuthorisationGroup.AuthorisationGroupId
	where
		Channel.Id in (select * from fn_StringListToTable(@ChannelIds)) and
		[User].IsDeleted = 0 and
		UserIndividual.IsDeleted = 0 and
		UserChannel.IsDeleted = 0 and
		Channel.IsDeleted = 0 and
		UserAuthorisationGroup.IsDeleted = 0
	order by
		Channel.Name,
		md.AuthorisationGroup.Name
