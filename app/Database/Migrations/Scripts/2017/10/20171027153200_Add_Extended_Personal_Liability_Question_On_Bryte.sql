﻿DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @CoverDefinitionId INT, @CoverID INT,@NewQuestionId INT,@ParentCoverDefinitionId INT,@ChildCoverDefinitionId INT,@DummyQuestionDefId INT,@CoverName VARCHAR(50)

 
SET @Name = 'ZURPERS'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @Name)

if (@productid IS NULL)
	BEGIN
		PRINT('OUT Of scope to create the product')
	RETURN
 END
 		  	  
--------------------129	'Extended lIABILITY SUM INSURED'-----------------------
SET @CoverName = 'Extended Personal Legal Liability'
SET @CoverID = 129				

SET @CoverDefinitionId = ( SELECT TOP 1 ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverId)

IF (@CoverDefinitionId IS not NULL)

BEGIN
--extended liability Sum Insured			  			  
if not exists(select * from QuestionDefinition where QuestionId=1522 and CoverDefinitionId = @CoverDefinitionId)
            begin
            INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
            [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Extended Liability Sum Insured', @CoverDefinitionId, 1522, NULL, 1, 0, 1, 1, 0,
             N'What Is Extended Liability Sum Insured', N'' , N'', 1, 2)
end

---Map To Multiquote
select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverId
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId 
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 1522 -- extended liability sum insured

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id
END