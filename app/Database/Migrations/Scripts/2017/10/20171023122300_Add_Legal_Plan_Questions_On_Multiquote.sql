﻿DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @CoverDefinitionId INT, @CoverID INT,@NewQuestionId INT,@ParentCoverDefinitionId INT,@ChildCoverDefinitionId INT,@DummyQuestionDefId INT,@CoverName VARCHAR(50)

 
SET @Name = N'MUL'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @Name)

if (@productid IS NULL)
	BEGIN
		PRINT('OUT Of scope to create the product')
	RETURN
 END
 		  	  
--------------------179	'Legal Plan'-----------------------
SET @CoverName = 'Legal Plan'
SET @CoverID = 179				

SET @CoverDefinitionId = ( SELECT TOP 1 ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverId)

IF (@CoverDefinitionId IS NULL)

BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)


SET @CoverDefinitionId = ( SELECT TOP 1 ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverId)
if not exists(select * from QuestionDefinition where QuestionId=1081 and CoverDefinitionId = @CoverDefinitionId)
begin
      INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
      [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Im a dummy question', @CoverDefinitionId, 1081, NULL, 1, 4, 0, 0, 0,
       N'This a dummy question for mapping MultiQoute Questions', N'NULL' , N'NULL', 0, NULL, 1)
end
END