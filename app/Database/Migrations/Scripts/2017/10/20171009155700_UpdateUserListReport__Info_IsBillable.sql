update Report
		set
			Info = '
				Provides a detailed list of users across all channels of an iPlatform instance.

				<ul>
					<li>
						<strong>Broker Name</strong>
						<ul><li>The name of the Broker or Insurance Company as setup in iPlatform, also known as a Channel.</li></ul>
					</li>
					<li>
						<strong>Role</strong>
						<ul><li>The role of the user in iPlatform related to the Channel.</li></ul>
					</li>
					<li>
						<strong>Agent Name & Surname</strong>
						<ul><li>The full name of the listed user.</li></ul>
					</li>
					<li>
						<strong>Agent E-Mail</strong>
						<ul><li>The e-mail address of the listed user.</li></ul>
					</li>
					<li>
						<strong>Active</strong>
						<ul><li>Whether or not the user was active in the selected date period.</li></ul>
					</li>
					<li>
						<strong>Client User</strong>
						<ul><li>Whether or not the user is a client user.</li></ul>
					</li>
				</ul>
				'
		where
			Name = 'User List' and SourceFile = 'UserList.trdx'