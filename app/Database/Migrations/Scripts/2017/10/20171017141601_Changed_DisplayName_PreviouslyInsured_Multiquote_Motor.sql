﻿DECLARE @CoverDefId INT = (SELECT TOP 1 Id FROM dbo.CoverDefinition WHERE ProductId = 27 AND CoverId = 218)

IF @CoverDefId IS NULL RETURN

UPDATE dbo.QuestionDefinition 
SET DisplayName = 'Is your current motor insurance cover comprehensive'
WHERE CoverDefinitionId = @CoverDefId
AND QuestionId = 39

UPDATE dbo.QuestionDefinition 
SET ToolTip = 'How many years has this client been claim free?'
WHERE CoverDefinitionId = @CoverDefId
AND QuestionId = 43

SELECT * FROM dbo.QuestionDefinition 
WHERE CoverDefinitionId = @CoverDefId