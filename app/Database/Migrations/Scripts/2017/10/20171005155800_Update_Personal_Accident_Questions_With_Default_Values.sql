﻿DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverId INT


SET @Name = 'ZURPERS'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode = @Name )

if (@productid IS NULL)
BEGIN
    --ZURPERS Product does not exist so now we must first create it (out of scope)
 Return
 END
 ELSE
 BEGIN
	SET @id = @productid
 END


declare @CoverDefinitionId int

----------------------Personal accident-------------------
SET @CoverId = 257 -- 257 personalAccident

SET @coverDefId = (SELECT TOP 1 ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)

--Death Benefit Annual
update QuestionDefinition
set DefaultValue=0
where QuestionId=1497 and CoverDefinitionId=@coverDefId

--Permanent Disability Sum Insured
update QuestionDefinition
set DefaultValue=0
where QuestionId=1492 and CoverDefinitionId=@coverDefId

--Temporary Disability Sum Insured
update QuestionDefinition
set DefaultValue=0
where QuestionId=1493 and CoverDefinitionId=@coverDefId