﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'EASY'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  Name= @Name)

SELECT @productid
IF @productid IS NOT NULL
BEGIN
UPDATE dbo.Product
SET Name = 'Easy Orange', 
	ProductCode = 'HOLEASYO',
	EndDate = NULL
	WHERE Name LIKE  N'EASY'
END 

SET @Name = N'Easy Orange'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  Name= @Name)

SELECT @productid


--------------------258	Personal Legal Liability-----------------------
SET @CoverID = 258	 			

SET @CoverName = 'Personal Legal Liability'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'0' , N'^[0-9\.]+$', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2, 0)
              end


--------------------257	Personal Accident-----------------------
SET @CoverID = 257	 			

SET @CoverName = 'Personal Accident'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'0' , N'^[0-9\.]+$', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=1505 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Number Of Dependants', @CoverDefinitionId, 1505, NULL, 1, 1, 0, 0, 0,
               N'How many dependants do you have?', N'' , N'', 0, NULL, 0)
              end

---MultiquoteQuestions

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverID
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverID   
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 1505 -- Hollard Dependants

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id






--------------------373	Identity Theft-----------------------
SET @CoverID = 373	 			

SET @CoverName = 'Identity Theft'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'0' , N'^[0-9\.]+$', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2, 0)
              end

--Multiquote
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = 27 AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'0' , N'^[0-9\.]+$', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2, 0)
              END              
if not exists(select * from QuestionDefinition where QuestionId=1081 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Im a dummy question', @CoverDefinitionId, 1081, NULL, 1, 4, 0, 0, 0,
               N'This a dummy question for mapping MultiQoute Questions', N'NULL' , N'NULL', 0, NULL, 0)
              end



--------------------182	Legal-----------------------
SET @CoverID = 182	 			

SET @CoverName = 'Legal'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the address where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'What is the suburb name where the item is located?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 1, 1, 1, 0,
               N'What is the sum insured', N'0' , N'^[0-9\.]+$', 0, NULL, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 2, 1, 1, 0,
               N'What is the postal code where the item is located?', N'' , N'', 1, 2, 0)
              end




IF NOT EXISTS (SELECT * FROM dbo.Product WHERE ProductCode = 'HOLEASYP')
BEGIN
	
	INSERT INTO Product
	SELECT  TOP 1 
	        MasterId ,
	        'Easy Purple' ,
	        ProductOwnerId ,
	        ProductProviderId ,
	        ProductTypeId ,
	        AgencyNumber ,
	        'HOLEASYP' ,
	        StartDate ,
	        NULL ,
	        RiskItemTypeId ,
	        IsDeleted ,
	        ImageName ,
	        Audit ,
	        QuoteExpiration ,
	        IsSelectedExcess ,
	        IsVoluntaryExcess ,
	        ShowInReporting	 FROM dbo.Product WHERE ProductCode = 'HOLEASYO'	
END

DECLARE @purpleProductId INT = (SELECT TOP 1 Id FROM dbo.Product WHERE ProductCode = 'HOLEASYP')

IF @purpleProductId IS NULL RETURN


IF NOT EXISTS (SELECT * FROM dbo.CoverDefinition WHERE ProductId = @purpleProductId)
BEGIN
INSERT INTO CoverDefinition
SELECT 
        MasterId ,
        DisplayName ,
        @purpleProductId ,
        CoverId ,
        ParentId ,
        CoverDefinitionTypeId ,
        VisibleIndex ,
        IsDeleted FROM dbo.CoverDefinition WHERE ProductId = @productid	
END


SELECT * FROM dbo.Product WHERE id IN (@productid, @purpleProductId)
SELECT * FROM dbo.CoverDefinition WHERE ProductId IN (@productid, @purpleProductId)


