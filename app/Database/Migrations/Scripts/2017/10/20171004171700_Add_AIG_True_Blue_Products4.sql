﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'Domestic Armour CC'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  Name= @Name)

SELECT @productid
if (@productid IS NULL)
BEGIN
-- Out of scope
RETURN
 END
 
 UPDATE Product
 SET ShowInReporting = 0
 WHERE id = @productid


------------------Contents-----------------------
SET @CoverID = 78 			

SET @CoverName = 'Household Contents'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--Out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 2, 1, 1, 0,
               N'In which province is this property situated?', N'' , N'', 1, 2, 0)
              END

if not exists(select * from QuestionDefinition where QuestionId=10 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Burglar Alarm Linked To 24 Hr Armed Response', @CoverDefinitionId, 10, NULL, 1, 7, 0, 1, 0,
               N'Burglar Alarm Linked To 24 Hr Armed Response', N'' , N'', 0, NULL, 0)
              END

if not exists(select * from QuestionDefinition where QuestionId=21 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Access Controlled Suburb', @CoverDefinitionId, 21, NULL, 1, 10, 1, 1, 0,
               N'Access Controlled Suburb', N'' , N'', 0, NULL, 0)
              end

              
UPDATE QuestionDefinition
SET GroupIndex = 1
	,QuestionDefinitionGroupTypeId =	2
WHERE QuestionId IN (
	88,
	90,
	89,
	47
	)
	AND CoverDefinitionId = @CoverDefinitionId



UPDATE QuestionDefinition
SET Hide = 0
WHERE CoverDefinitionId = @CoverDefinitionId

-----------------Buildings------------------
SET @CoverID = 44 			

SET @CoverName = 'House Owners'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--Out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 2, 1, 1, 0,
               N'In which province is this property situated?', N'' , N'', 1, 2, 0)
              END
              
UPDATE QuestionDefinition
SET GroupIndex = 1
	,QuestionDefinitionGroupTypeId =	2
WHERE QuestionId IN (
	88,
	90,
	89,
	47
	)
	AND CoverDefinitionId = @CoverDefinitionId



UPDATE QuestionDefinition
SET Hide = 0
WHERE CoverDefinitionId = @CoverDefinitionId



--------------------All Risk---------------------------------------

SET @CoverID = 17 			

SET @CoverName = 'All Risk'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--Out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the suburb and town name where you live?', N'' , N'', 1, 2, 0)
              END
              
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 2, 1, 1, 0,
               N'In which province is this vehicle mainly operated in?', N'' , N'', 1, 2, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Item', @CoverDefinitionId, 141, NULL, 1, 0, 0, 0, 0,
               N'Which risk item would you like to insure?', N'' , N'', 5, 6, 0)
              end


UPDATE QuestionDefinition
SET GroupIndex = 1
	,QuestionDefinitionGroupTypeId =	2
WHERE QuestionId IN (
	88,
	90,
	89,
	47
	)
	AND CoverDefinitionId = @CoverDefinitionId



UPDATE QuestionDefinition
SET Hide = 0
WHERE CoverDefinitionId = @CoverDefinitionId


--------------------Computer Equipment---------------------------------------

SET @CoverID = 380 			

SET @CoverName = 'Computer Equipment'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted)
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0)
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


-----------------Motor------------------
SET @CoverID = 218 			

SET @CoverName = 'Motor'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--Out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END


if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 1, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the suburb and town name where you live?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 2, 1, 1, 0,
               N'In which province is this vehicle mainly operated in?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=135 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb (Work)', @CoverDefinitionId, 135, NULL, 1, 6, 1, 1, 0,
               N'{Title} {Name} would you please provide me with the suburb and town name where you work?', N'' , N'', 3, 3, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=154 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Province (Work)', @CoverDefinitionId, 154, NULL, 1, 100, 1, 1, 0,
               N'Which province is the vehicle during working hours?', N'' , N'', 3, 3, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=91 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Postal Code (Work)', @CoverDefinitionId, 91, NULL, 1, 101, 1, 1, 0,
               N'{Title} {Name} would you also please provide me with the suburb and town name where you work?', N'' , N'', 3, 3, 0)
              end

if not exists(select * from QuestionDefinition where QuestionId=139 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Work Address', @CoverDefinitionId, 139, NULL, 1, 5, 1, 1, 0,
               N'{Title} {Name} would you please provide me with your work address?', N'' , N'', 3, 3, 0)
              end

UPDATE  QuestionDefinition
SET     Hide = 1 ,
        RatingFactor = 0 ,
        RequiredForQuote = 0
WHERE   QuestionId IN ( 88 )
        AND CoverDefinitionId = @CoverDefinitionId


UPDATE  [dbo].[QuestionDefinition]
SET     [QuestionDefinitionTypeId] = 1 ,
        [VisibleIndex] = 3 ,
        [RequiredForQuote] = 1 ,
        [RatingFactor] = 1 ,
        [GroupIndex] = 1 ,
        [QuestionDefinitionGroupTypeId] = 2
WHERE   QuestionId = 90
        AND CoverDefinitionId = @CoverDefinitionId
         


-----------------Caravan------------------
SET @CoverID = 62		

SET @CoverName = 'Caravan or Trailer'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
--Out of scope
RETURN
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Risk Address', @CoverDefinitionId, 88, NULL, 1, 0, 1, 1, 0,
               N'What is the risk address?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=89 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Suburb', @CoverDefinitionId, 89, NULL, 1, 0, 1, 1, 0,
               N'What is the suburb name?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=47 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Province', @CoverDefinitionId, 47, NULL, 1, 0, 1, 1, 0,
               N'What is the province?', N'' , N'', 1, 2, 0)
              end
if not exists(select * from QuestionDefinition where QuestionId=90 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide]) VALUES (0, N'Postal Code', @CoverDefinitionId, 90, NULL, 1, 0, 1, 1, 0,
               N'What is the postal code?', N'' , N'', 1, 2, 0)
              end


UPDATE dbo.QuestionDefinition --Reg
SET VisibleIndex = 1
WHERE QuestionId = 97
AND CoverDefinitionId = @CoverDefinitionId

UPDATE dbo.QuestionDefinition -- Year Of Manufacture
SET VisibleIndex = 2
WHERE QuestionId = 99
AND CoverDefinitionId = @CoverDefinitionId

UPDATE dbo.QuestionDefinition -- Make
SET VisibleIndex =3
WHERE QuestionId = 95
AND CoverDefinitionId = @CoverDefinitionId

UPDATE dbo.QuestionDefinition -- Model
SET VisibleIndex = 4
WHERE QuestionId = 96
AND CoverDefinitionId = @CoverDefinitionId

UPDATE dbo.QuestionDefinition -- MMcode
SET VisibleIndex = 5, Hide = 1 
WHERE QuestionId = 94
AND CoverDefinitionId = @CoverDefinitionId

UPDATE dbo.QuestionDefinition -- VIN
SET VisibleIndex = 6
WHERE QuestionId = 1010
AND CoverDefinitionId = @CoverDefinitionId