﻿if exists (select * from sys.objects where object_id = object_id(N'Report_Layout') and type in (N'P', N'PC'))
	begin
		drop procedure Report_Layout
	end
go

create procedure Report_Layout
(
	@ChannelId int,
	@ReportName varchar(50)
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select count(ReportLayout.Id) from Report inner join ReportLayout on ReportLayout.ReportId = Report.Id where Report.Name = @ReportName and Report.ChannelId = @ChannelId) > 0)
		begin
			select
				ReportLayout.Name,
				ReportLayout.Label,
				ReportLayout.[Value],
				ReportLayout.IsVisible
			from Report
				inner join ReportLayout on ReportLayout.ReportId = Report.Id
			where
				Report.Name = @ReportName and
				Report.ChannelId = @ChannelId
		end
	else
		begin
			select
				ReportLayout.Name,
				ReportLayout.Label,
				ReportLayout.[Value],
				ReportLayout.IsVisible
			from Report
				inner join ReportLayout on ReportLayout.ReportId = Report.Id
			where
				Report.Name = @ReportName and
				Report.ChannelId = @IpChannelId
		end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_ReportList') and type in (N'P', N'PC'))
	begin
		drop procedure Report_ReportList
	end
go

create procedure Report_ReportList
as
	select
		Report.Id,
		Report.ReportCategoryId,
		Report.ReportTypeId,
		Report.ReportFormatId,
		Report.Name,
		Report.[Description],
		Report.SourceFile,
		Report.IsVisibleInReportViewer,
		Report.IsVisibleInScheduler,
		Report.IsActive,
		Report.VisibleIndex,
		Report.IconId,
		Report.Info
	from Report
	where
		Report.IsDeleted = 0 and
		ChannelId = (select top 1 Id from Channel where Code = 'IP')
	order by
		Report.Name
go


if exists (select * from sys.objects where object_id = object_id(N'Report_ReportParams') and type in (N'P', N'PC'))
	begin
		drop procedure Report_ReportParams
	end
go

create procedure Report_ReportParams
(
	@ReportId int
)
as
	select
		[Field],
		DataTypeId,
		VisibleIndex,
		--IconId,
        Name,
        [Description],
        IsVisibleInReportViewer,
        IsVisibleInScheduler
	from ReportParam
	where
		(
			IsDeleted is null or
			IsDeleted = 0
		) and
		ReportId = @ReportId
	order by
		VisibleIndex asc
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformance_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformance_Header
	end
go

create procedure Report_CallCentre_AgentPerformance_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
				inner join Campaign on Campaign.ChannelId = Channel.Id
			where
				Campaign.Id = @CampaignId and
				Campaign.IsDeleted = 0 and
				Channel.IsDeleted = 0
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformanceByProduct_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformanceByProduct_Header
	end
go

create procedure Report_CallCentre_AgentPerformanceByProduct_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
				inner join Campaign on Campaign.ChannelId = Channel.Id
			where
				Campaign.Id = @CampaignId and
				Campaign.IsDeleted = 0 and
				Channel.IsDeleted = 0
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_BrokerQuoteUpload_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_BrokerQuoteUpload_Header
	end
go

create procedure Report_CallCentre_BrokerQuoteUpload_Header
(
	@ChannelId int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Id) from Channel where Id = @ChannelId and IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @ChannelId
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_Header
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
				inner join Campaign on Campaign.ChannelId = Channel.Id
			where
				Campaign.Id = @CampaignId and
				Campaign.IsDeleted = 0 and
				Channel.IsDeleted = 0
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_Header
	end
go

create procedure Report_CallCentre_InsurerData_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
				inner join Campaign on Campaign.ChannelId = Channel.Id
			where
				Campaign.Id = @CampaignId and
				Campaign.IsDeleted = 0 and
				Channel.IsDeleted = 0
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerQuoteBreakdown_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerQuoteBreakdown_Header
	end
go

create procedure Report_CallCentre_InsurerQuoteBreakdown_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
				inner join Campaign on Campaign.ChannelId = Channel.Id
			where
				Campaign.Id = @CampaignId and
				Campaign.IsDeleted = 0 and
				Channel.IsDeleted = 0
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_LeadManagement_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_LeadManagement_Header
	end
go

create procedure Report_CallCentre_LeadManagement_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
				inner join Campaign on Campaign.ChannelId = Channel.Id
			where
				Campaign.Id = @CampaignId and
				Campaign.IsDeleted = 0 and
				Channel.IsDeleted = 0
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_ManagerLevelQuoteUpload_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_ManagerLevelQuoteUpload_Header
	end
go

create procedure Report_CallCentre_ManagerLevelQuoteUpload_Header
(
	@ChannelId int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Id) from Channel where Id = @ChannelId and IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @ChannelId
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go



if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_OutboundRatios_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_OutboundRatios_Header
	end
go

create procedure Report_CallCentre_OutboundRatios_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
				inner join Campaign on Campaign.ChannelId = Channel.Id
			where
				Campaign.Id = @CampaignId and
				Campaign.IsDeleted = 0 and
				Channel.IsDeleted = 0
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_SalesManagement_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_SalesManagement_Header
	end
go

create procedure Report_CallCentre_SalesManagement_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
				inner join Campaign on Campaign.ChannelId = Channel.Id
			where
				Campaign.Id = @CampaignId and
				Campaign.IsDeleted = 0 and
				Channel.IsDeleted = 0
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_User_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_User_Header
	end
go

create procedure Report_CallCentre_User_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			select top 1
				Code
				, ''
			from Channel
				inner join Campaign on Campaign.ChannelId = Channel.Id
			where
				Campaign.Id = @CampaignId and
				Campaign.IsDeleted = 0 and
				Channel.IsDeleted = 0
		end
	else
		begin
			select top 1
				Code
				, ''
			from Channel
			where
				Id = @IpChannelId
		end
go