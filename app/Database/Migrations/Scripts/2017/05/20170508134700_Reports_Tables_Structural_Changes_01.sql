﻿--Drop Constraint on ReportScheduleParam
if object_id('FK_ReportScheduleParam_ReportParam_ReportParamId', 'F') is not null
	begin
		alter table ReportScheduleParam drop constraint FK_ReportScheduleParam_ReportParam_ReportParamId
	end
go


--Delete Existing Reporting Data
if object_id('ReportChannelLayout', 'U') is not null
	begin
		delete from ReportChannelLayout
	end

if object_id('ReportChannel', 'U') is not null
	begin
		delete from ReportChannel
	end

if object_id('ReportLayout', 'U') is not null
	begin
		delete from ReportLayout
	end

if object_id('ReportDocumentDefinition', 'U') is not null
	begin
		delete from ReportDocumentDefinition
	end

if object_id('ReportParam', 'U') is not null
	begin
		delete from ReportParam
	end

if object_id('ReportSection', 'U') is not null
	begin
		delete from ReportSection
	end

if object_id('Report', 'U') is not null
	begin
		delete from Report
	end
go


--Drop Existing Reporting Tables
if object_id('ReportChannelLayout', 'U') is not null
	begin
		drop table ReportChannelLayout
	end

if object_id('ReportChannel', 'U') is not null
	begin
		drop table ReportChannel
	end

if object_id('ReportLayout', 'U') is not null
	begin
		drop table ReportLayout
	end

if object_id('ReportDocumentDefinition', 'U') is not null
	begin
		drop table ReportDocumentDefinition
	end

if object_id('ReportSection', 'U') is not null
	begin
		drop table ReportSection
	end

if object_id('ReportParam', 'U') is not null
	begin
		drop table ReportParam
	end

if object_id('Report', 'U') is not null
	begin
		drop table Report
	end
go


--Create MD Tables Required for FK References
if object_id('[md].[FontAwesomeIcon]', 'U') is null 
	begin
		CREATE TABLE [md].[FontAwesomeIcon](
			[Id] [int] NOT NULL,
			[Name] [nvarchar](255) NULL,
		PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (1, N'fa-500px')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (2, N'fa-address-book')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (3, N'fa-address-book-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (4, N'fa-address-card')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (5, N'fa-address-card-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (6, N'fa-adjust')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (7, N'fa-adn')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (8, N'fa-align-center')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (9, N'fa-align-justify')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (10, N'fa-align-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (11, N'fa-align-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (12, N'fa-amazon')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (13, N'fa-ambulance')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (14, N'fa-american-sign-language-interpreting')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (15, N'fa-anchor')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (16, N'fa-android')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (17, N'fa-angellist')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (18, N'fa-angle-double-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (19, N'fa-angle-double-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (20, N'fa-angle-double-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (21, N'fa-angle-double-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (22, N'fa-angle-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (23, N'fa-angle-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (24, N'fa-angle-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (25, N'fa-angle-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (26, N'fa-apple')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (27, N'fa-archive')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (28, N'fa-area-chart')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (29, N'fa-arrow-circle-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (30, N'fa-arrow-circle-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (31, N'fa-arrow-circle-o-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (32, N'fa-arrow-circle-o-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (33, N'fa-arrow-circle-o-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (34, N'fa-arrow-circle-o-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (35, N'fa-arrow-circle-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (36, N'fa-arrow-circle-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (37, N'fa-arrow-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (38, N'fa-arrow-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (39, N'fa-arrow-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (40, N'fa-arrow-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (41, N'fa-arrows')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (42, N'fa-arrows-alt')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (43, N'fa-arrows-h')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (44, N'fa-arrows-v')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (45, N'fa-assistive-listening-systems')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (46, N'fa-asterisk')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (47, N'fa-at')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (48, N'fa-audio-description')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (49, N'fa-backward')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (50, N'fa-balance-scale')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (51, N'fa-ban')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (52, N'fa-bandcamp')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (53, N'fa-bar-chart')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (54, N'fa-barcode')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (55, N'fa-bars')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (56, N'fa-bath')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (57, N'fa-battery-empty')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (58, N'fa-battery-full')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (59, N'fa-battery-half')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (60, N'fa-battery-quarter')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (61, N'fa-battery-three-quarters')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (62, N'fa-bed')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (63, N'fa-beer')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (64, N'fa-behance')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (65, N'fa-behance-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (66, N'fa-bell')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (67, N'fa-bell-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (68, N'fa-bell-slash')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (69, N'fa-bell-slash-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (70, N'fa-bicycle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (71, N'fa-binoculars')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (72, N'fa-birthday-cake')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (73, N'fa-bitbucket')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (74, N'fa-bitbucket-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (75, N'fa-black-tie')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (76, N'fa-blind')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (77, N'fa-bluetooth')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (78, N'fa-bluetooth-b')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (79, N'fa-bold')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (80, N'fa-bolt')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (81, N'fa-bomb')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (82, N'fa-book')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (83, N'fa-bookmark')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (84, N'fa-bookmark-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (85, N'fa-braille')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (86, N'fa-briefcase')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (87, N'fa-btc')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (88, N'fa-bug')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (89, N'fa-building')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (90, N'fa-building-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (91, N'fa-bullhorn')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (92, N'fa-bullseye')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (93, N'fa-bus')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (94, N'fa-buysellads')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (95, N'fa-calculator')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (96, N'fa-calendar')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (97, N'fa-calendar-check-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (98, N'fa-calendar-minus-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (99, N'fa-calendar-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (100, N'fa-calendar-plus-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (101, N'fa-calendar-times-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (102, N'fa-camera')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (103, N'fa-camera-retro')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (104, N'fa-car')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (105, N'fa-caret-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (106, N'fa-caret-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (107, N'fa-caret-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (108, N'fa-caret-square-o-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (109, N'fa-caret-square-o-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (110, N'fa-caret-square-o-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (111, N'fa-caret-square-o-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (112, N'fa-caret-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (113, N'fa-cart-arrow-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (114, N'fa-cart-plus')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (115, N'fa-cc')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (116, N'fa-cc-amex')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (117, N'fa-cc-diners-club')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (118, N'fa-cc-discover')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (119, N'fa-cc-jcb')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (120, N'fa-cc-mastercard')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (121, N'fa-cc-paypal')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (122, N'fa-cc-stripe')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (123, N'fa-cc-visa')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (124, N'fa-certificate')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (125, N'fa-chain-broken')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (126, N'fa-check')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (127, N'fa-check-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (128, N'fa-check-circle-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (129, N'fa-check-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (130, N'fa-check-square-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (131, N'fa-chevron-circle-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (132, N'fa-chevron-circle-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (133, N'fa-chevron-circle-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (134, N'fa-chevron-circle-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (135, N'fa-chevron-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (136, N'fa-chevron-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (137, N'fa-chevron-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (138, N'fa-chevron-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (139, N'fa-child')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (140, N'fa-chrome')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (141, N'fa-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (142, N'fa-circle-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (143, N'fa-circle-o-notch')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (144, N'fa-circle-thin')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (145, N'fa-clipboard')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (146, N'fa-clock-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (147, N'fa-clone')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (148, N'fa-cloud')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (149, N'fa-cloud-download')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (150, N'fa-cloud-upload')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (151, N'fa-code')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (152, N'fa-code-fork')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (153, N'fa-codepen')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (154, N'fa-codiepie')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (155, N'fa-coffee')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (156, N'fa-cog')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (157, N'fa-cogs')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (158, N'fa-columns')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (159, N'fa-comment')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (160, N'fa-comment-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (161, N'fa-commenting')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (162, N'fa-commenting-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (163, N'fa-comments')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (164, N'fa-comments-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (165, N'fa-compass')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (166, N'fa-compress')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (167, N'fa-connectdevelop')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (168, N'fa-contao')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (169, N'fa-copyright')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (170, N'fa-creative-commons')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (171, N'fa-credit-card')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (172, N'fa-credit-card-alt')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (173, N'fa-crop')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (174, N'fa-crosshairs')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (175, N'fa-css3')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (176, N'fa-cube')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (177, N'fa-cubes')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (178, N'fa-cutlery')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (179, N'fa-dashcube')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (180, N'fa-database')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (181, N'fa-deaf')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (182, N'fa-delicious')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (183, N'fa-desktop')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (184, N'fa-deviantart')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (185, N'fa-diamond')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (186, N'fa-digg')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (187, N'fa-dot-circle-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (188, N'fa-download')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (189, N'fa-dribbble')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (190, N'fa-dropbox')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (191, N'fa-drupal')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (192, N'fa-edge')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (193, N'fa-eercast')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (194, N'fa-eject')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (195, N'fa-ellipsis-h')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (196, N'fa-ellipsis-v')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (197, N'fa-empire')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (198, N'fa-envelope')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (199, N'fa-envelope-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (200, N'fa-envelope-open')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (201, N'fa-envelope-open-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (202, N'fa-envelope-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (203, N'fa-envira')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (204, N'fa-eraser')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (205, N'fa-etsy')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (206, N'fa-eur')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (207, N'fa-exchange')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (208, N'fa-exclamation')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (209, N'fa-exclamation-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (210, N'fa-exclamation-triangle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (211, N'fa-expand')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (212, N'fa-expeditedssl')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (213, N'fa-external-link')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (214, N'fa-external-link-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (215, N'fa-eye')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (216, N'fa-eye-slash')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (217, N'fa-eyedropper')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (218, N'fa-facebook')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (219, N'fa-facebook-official')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (220, N'fa-facebook-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (221, N'fa-fast-backward')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (222, N'fa-fast-forward')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (223, N'fa-fax')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (224, N'fa-female')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (225, N'fa-fighter-jet')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (226, N'fa-file')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (227, N'fa-file-archive-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (228, N'fa-file-audio-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (229, N'fa-file-code-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (230, N'fa-file-excel-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (231, N'fa-file-image-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (232, N'fa-file-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (233, N'fa-file-pdf-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (234, N'fa-file-powerpoint-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (235, N'fa-file-text')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (236, N'fa-file-text-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (237, N'fa-file-video-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (238, N'fa-file-word-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (239, N'fa-files-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (240, N'fa-film')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (241, N'fa-filter')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (242, N'fa-fire')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (243, N'fa-fire-extinguisher')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (244, N'fa-firefox')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (245, N'fa-first-order')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (246, N'fa-flag')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (247, N'fa-flag-checkered')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (248, N'fa-flag-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (249, N'fa-flask')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (250, N'fa-flickr')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (251, N'fa-floppy-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (252, N'fa-folder')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (253, N'fa-folder-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (254, N'fa-folder-open')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (255, N'fa-folder-open-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (256, N'fa-font')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (257, N'fa-font-awesome')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (258, N'fa-fonticons')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (259, N'fa-fort-awesome')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (260, N'fa-forumbee')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (261, N'fa-forward')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (262, N'fa-foursquare')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (263, N'fa-free-code-camp')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (264, N'fa-frown-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (265, N'fa-futbol-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (266, N'fa-gamepad')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (267, N'fa-gavel')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (268, N'fa-gbp')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (269, N'fa-genderless')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (270, N'fa-get-pocket')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (271, N'fa-gg')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (272, N'fa-gg-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (273, N'fa-gift')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (274, N'fa-git')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (275, N'fa-git-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (276, N'fa-github')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (277, N'fa-github-alt')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (278, N'fa-github-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (279, N'fa-gitlab')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (280, N'fa-glass')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (281, N'fa-glide')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (282, N'fa-glide-g')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (283, N'fa-globe')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (284, N'fa-ogle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (285, N'fa-ogle-plus')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (286, N'fa-ogle-plus-official')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (287, N'fa-ogle-plus-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (288, N'fa-ogle-wallet')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (289, N'fa-graduation-cap')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (290, N'fa-gratipay')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (291, N'fa-grav')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (292, N'fa-h-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (293, N'fa-hacker-news')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (294, N'fa-hand-lizard-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (295, N'fa-hand-o-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (296, N'fa-hand-o-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (297, N'fa-hand-o-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (298, N'fa-hand-o-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (299, N'fa-hand-paper-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (300, N'fa-hand-peace-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (301, N'fa-hand-pointer-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (302, N'fa-hand-rock-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (303, N'fa-hand-scissors-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (304, N'fa-hand-spock-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (305, N'fa-handshake-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (306, N'fa-hashtag')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (307, N'fa-hdd-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (308, N'fa-header')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (309, N'fa-headphones')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (310, N'fa-heart')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (311, N'fa-heart-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (312, N'fa-heartbeat')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (313, N'fa-history')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (314, N'fa-home')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (315, N'fa-hospital-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (316, N'fa-hourglass')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (317, N'fa-hourglass-end')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (318, N'fa-hourglass-half')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (319, N'fa-hourglass-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (320, N'fa-hourglass-start')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (321, N'fa-houzz')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (322, N'fa-html5')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (323, N'fa-i-cursor')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (324, N'fa-id-badge')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (325, N'fa-id-card')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (326, N'fa-id-card-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (327, N'fa-ils')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (328, N'fa-imdb')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (329, N'fa-inbox')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (330, N'fa-indent')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (331, N'fa-industry')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (332, N'fa-info')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (333, N'fa-info-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (334, N'fa-inr')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (335, N'fa-instagram')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (336, N'fa-internet-explorer')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (337, N'fa-ioxhost')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (338, N'fa-italic')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (339, N'fa-joomla')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (340, N'fa-jpy')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (341, N'fa-jsfiddle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (342, N'fa-key')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (343, N'fa-keyboard-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (344, N'fa-krw')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (345, N'fa-language')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (346, N'fa-laptop')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (347, N'fa-lastfm')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (348, N'fa-lastfm-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (349, N'fa-leaf')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (350, N'fa-leanpub')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (351, N'fa-lemon-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (352, N'fa-level-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (353, N'fa-level-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (354, N'fa-life-ring')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (355, N'fa-lightbulb-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (356, N'fa-line-chart')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (357, N'fa-link')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (358, N'fa-linkedin')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (359, N'fa-linkedin-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (360, N'fa-linode')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (361, N'fa-linux')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (362, N'fa-list')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (363, N'fa-list-alt')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (364, N'fa-list-ol')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (365, N'fa-list-ul')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (366, N'fa-location-arrow')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (367, N'fa-lock')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (368, N'fa-long-arrow-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (369, N'fa-long-arrow-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (370, N'fa-long-arrow-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (371, N'fa-long-arrow-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (372, N'fa-low-vision')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (373, N'fa-magic')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (374, N'fa-magnet')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (375, N'fa-male')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (376, N'fa-map')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (377, N'fa-map-marker')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (378, N'fa-map-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (379, N'fa-map-pin')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (380, N'fa-map-signs')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (381, N'fa-mars')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (382, N'fa-mars-double')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (383, N'fa-mars-stroke')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (384, N'fa-mars-stroke-h')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (385, N'fa-mars-stroke-v')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (386, N'fa-maxcdn')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (387, N'fa-meanpath')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (388, N'fa-medium')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (389, N'fa-medkit')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (390, N'fa-meetup')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (391, N'fa-meh-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (392, N'fa-mercury')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (393, N'fa-microchip')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (394, N'fa-microphone')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (395, N'fa-microphone-slash')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (396, N'fa-minus')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (397, N'fa-minus-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (398, N'fa-minus-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (399, N'fa-minus-square-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (400, N'fa-mixcloud')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (401, N'fa-mobile')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (402, N'fa-modx')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (403, N'fa-money')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (404, N'fa-moon-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (405, N'fa-motorcycle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (406, N'fa-mouse-pointer')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (407, N'fa-music')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (408, N'fa-neuter')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (409, N'fa-newspaper-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (410, N'fa-object-group')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (411, N'fa-object-ungroup')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (412, N'fa-odnoklassniki')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (413, N'fa-odnoklassniki-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (414, N'fa-opencart')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (415, N'fa-openid')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (416, N'fa-opera')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (417, N'fa-optin-monster')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (418, N'fa-outdent')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (419, N'fa-pagelines')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (420, N'fa-paint-brush')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (421, N'fa-paper-plane')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (422, N'fa-paper-plane-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (423, N'fa-paperclip')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (424, N'fa-paragraph')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (425, N'fa-pause')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (426, N'fa-pause-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (427, N'fa-pause-circle-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (428, N'fa-paw')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (429, N'fa-paypal')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (430, N'fa-pencil')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (431, N'fa-pencil-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (432, N'fa-pencil-square-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (433, N'fa-percent')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (434, N'fa-phone')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (435, N'fa-phone-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (436, N'fa-picture-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (437, N'fa-pie-chart')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (438, N'fa-pied-piper')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (439, N'fa-pied-piper-alt')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (440, N'fa-pied-piper-pp')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (441, N'fa-pinterest')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (442, N'fa-pinterest-p')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (443, N'fa-pinterest-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (444, N'fa-plane')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (445, N'fa-play')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (446, N'fa-play-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (447, N'fa-play-circle-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (448, N'fa-plug')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (449, N'fa-plus')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (450, N'fa-plus-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (451, N'fa-plus-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (452, N'fa-plus-square-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (453, N'fa-podcast')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (454, N'fa-power-off')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (455, N'fa-print')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (456, N'fa-product-hunt')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (457, N'fa-puzzle-piece')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (458, N'fa-qq')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (459, N'fa-qrcode')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (460, N'fa-question')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (461, N'fa-question-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (462, N'fa-question-circle-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (463, N'fa-quora')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (464, N'fa-quote-left')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (465, N'fa-quote-right')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (466, N'fa-random')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (467, N'fa-ravelry')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (468, N'fa-rebel')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (469, N'fa-recycle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (470, N'fa-reddit')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (471, N'fa-reddit-alien')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (472, N'fa-reddit-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (473, N'fa-refresh')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (474, N'fa-registered')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (475, N'fa-renren')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (476, N'fa-repeat')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (477, N'fa-reply')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (478, N'fa-reply-all')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (479, N'fa-retweet')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (480, N'fa-road')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (481, N'fa-rocket')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (482, N'fa-rss')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (483, N'fa-rss-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (484, N'fa-rub')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (485, N'fa-safari')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (486, N'fa-scissors')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (487, N'fa-scribd')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (488, N'fa-search')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (489, N'fa-search-minus')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (490, N'fa-search-plus')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (491, N'fa-sellsy')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (492, N'fa-server')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (493, N'fa-share')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (494, N'fa-share-alt')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (495, N'fa-share-alt-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (496, N'fa-share-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (497, N'fa-share-square-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (498, N'fa-shield')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (499, N'fa-ship')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (500, N'fa-shirtsinbulk')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (501, N'fa-shopping-bag')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (502, N'fa-shopping-basket')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (503, N'fa-shopping-cart')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (504, N'fa-shower')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (505, N'fa-sign-in')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (506, N'fa-sign-language')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (507, N'fa-sign-out')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (508, N'fa-signal')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (509, N'fa-simplybuilt')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (510, N'fa-sitemap')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (511, N'fa-skyatlas')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (512, N'fa-skype')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (513, N'fa-slack')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (514, N'fa-sliders')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (515, N'fa-slideshare')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (516, N'fa-smile-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (517, N'fa-snapchat')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (518, N'fa-snapchat-ghost')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (519, N'fa-snapchat-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (520, N'fa-snowflake-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (521, N'fa-sort')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (522, N'fa-sort-alpha-asc')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (523, N'fa-sort-alpha-desc')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (524, N'fa-sort-amount-asc')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (525, N'fa-sort-amount-desc')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (526, N'fa-sort-asc')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (527, N'fa-sort-desc')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (528, N'fa-sort-numeric-asc')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (529, N'fa-sort-numeric-desc')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (530, N'fa-soundcloud')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (531, N'fa-space-shuttle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (532, N'fa-spinner')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (533, N'fa-spoon')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (534, N'fa-spotify')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (535, N'fa-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (536, N'fa-square-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (537, N'fa-stack-exchange')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (538, N'fa-stack-overflow')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (539, N'fa-star')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (540, N'fa-star-half')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (541, N'fa-star-half-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (542, N'fa-star-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (543, N'fa-steam')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (544, N'fa-steam-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (545, N'fa-step-backward')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (546, N'fa-step-forward')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (547, N'fa-stethoscope')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (548, N'fa-sticky-note')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (549, N'fa-sticky-note-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (550, N'fa-stop')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (551, N'fa-stop-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (552, N'fa-stop-circle-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (553, N'fa-street-view')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (554, N'fa-strikethrough')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (555, N'fa-stumbleupon')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (556, N'fa-stumbleupon-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (557, N'fa-subscript')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (558, N'fa-subway')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (559, N'fa-suitcase')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (560, N'fa-sun-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (561, N'fa-superpowers')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (562, N'fa-superscript')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (563, N'fa-table')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (564, N'fa-tablet')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (565, N'fa-tachometer')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (566, N'fa-tag')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (567, N'fa-tags')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (568, N'fa-tasks')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (569, N'fa-taxi')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (570, N'fa-telegram')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (571, N'fa-television')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (572, N'fa-tencent-weibo')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (573, N'fa-terminal')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (574, N'fa-text-height')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (575, N'fa-text-width')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (576, N'fa-th')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (577, N'fa-th-large')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (578, N'fa-th-list')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (579, N'fa-themeisle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (580, N'fa-thermometer-empty')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (581, N'fa-thermometer-full')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (582, N'fa-thermometer-half')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (583, N'fa-thermometer-quarter')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (584, N'fa-thermometer-three-quarters')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (585, N'fa-thumb-tack')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (586, N'fa-thumbs-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (587, N'fa-thumbs-o-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (588, N'fa-thumbs-o-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (589, N'fa-thumbs-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (590, N'fa-ticket')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (591, N'fa-times')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (592, N'fa-times-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (593, N'fa-times-circle-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (594, N'fa-tint')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (595, N'fa-toggle-off')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (596, N'fa-toggle-on')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (597, N'fa-trademark')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (598, N'fa-train')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (599, N'fa-transgender')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (600, N'fa-transgender-alt')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (601, N'fa-trash')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (602, N'fa-trash-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (603, N'fa-tree')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (604, N'fa-trello')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (605, N'fa-tripadvisor')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (606, N'fa-trophy')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (607, N'fa-truck')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (608, N'fa-try')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (609, N'fa-tty')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (610, N'fa-tumblr')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (611, N'fa-tumblr-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (612, N'fa-twitch')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (613, N'fa-twitter')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (614, N'fa-twitter-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (615, N'fa-umbrella')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (616, N'fa-underline')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (617, N'fa-undo')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (618, N'fa-universal-access')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (619, N'fa-university')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (620, N'fa-unlock')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (621, N'fa-unlock-alt')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (622, N'fa-upload')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (623, N'fa-usb')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (624, N'fa-usd')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (625, N'fa-user')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (626, N'fa-user-circle')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (627, N'fa-user-circle-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (628, N'fa-user-md')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (629, N'fa-user-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (630, N'fa-user-plus')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (631, N'fa-user-secret')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (632, N'fa-user-times')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (633, N'fa-users')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (634, N'fa-venus')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (635, N'fa-venus-double')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (636, N'fa-venus-mars')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (637, N'fa-viacoin')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (638, N'fa-viadeo')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (639, N'fa-viadeo-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (640, N'fa-video-camera')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (641, N'fa-vimeo')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (642, N'fa-vimeo-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (643, N'fa-vine')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (644, N'fa-vk')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (645, N'fa-volume-control-phone')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (646, N'fa-volume-down')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (647, N'fa-volume-off')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (648, N'fa-volume-up')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (649, N'fa-weibo')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (650, N'fa-weixin')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (651, N'fa-whatsapp')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (652, N'fa-wheelchair')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (653, N'fa-wheelchair-alt')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (654, N'fa-wifi')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (655, N'fa-wikipedia-w')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (656, N'fa-window-close')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (657, N'fa-window-close-o')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (658, N'fa-window-maximize')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (659, N'fa-window-minimize')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (660, N'fa-window-restore')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (661, N'fa-windows')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (662, N'fa-wordpress')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (663, N'fa-wpbeginner')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (664, N'fa-wpexplorer')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (665, N'fa-wpforms')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (666, N'fa-wrench')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (667, N'fa-xing')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (668, N'fa-xing-square')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (669, N'fa-y-combinator')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (670, N'fa-yahoo')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (671, N'fa-yelp')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (672, N'fa-yoast')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (673, N'fa-youtube')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (674, N'fa-youtube-play')

		INSERT [md].[FontAwesomeIcon] ([Id], [Name]) VALUES (675, N'fa-youtube-square')
	end
go

--Create New Reporting Tables
if object_id('Report', 'U') is null 
	begin
		CREATE TABLE [dbo].[Report](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[Content] [text] NULL,
			[Info] [text] NULL,
			[ReportCategoryId] [int] NULL,
			[ReportTypeId] [int] NULL,
			[Name] [nvarchar](255) NULL,
			[Description] [nvarchar](255) NULL,
			[SourceFile] [nvarchar](255) NULL,
			[ContentType] [nvarchar](255) NULL,
			[Extension] [nvarchar](255) NULL,
			[IsVisibleInReportViewer] [bit] NULL,
			[IsVisibleInScheduler] [bit] NULL,
			[IsActive] [bit] NULL,
			[VisibleIndex] [int] NULL,
			[IsDeleted] [bit] NULL,
			[ChannelId] [int] NULL,
			[IconId] [int] NULL,
			[ReportFormatId] [int] NULL,
		PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

		ALTER TABLE [dbo].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Report_Channel_ChannelId] FOREIGN KEY([ChannelId])
		REFERENCES [dbo].[Channel] ([Id])

		ALTER TABLE [dbo].[Report] CHECK CONSTRAINT [FK_Report_Channel_ChannelId]

		ALTER TABLE [dbo].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Report_ExportType_ExportTypeId] FOREIGN KEY([ReportFormatId])
		REFERENCES [md].[ExportType] ([Id])

		ALTER TABLE [dbo].[Report] CHECK CONSTRAINT [FK_Report_ExportType_ExportTypeId]

		ALTER TABLE [dbo].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Report_FontAwesomeIcon_IconId] FOREIGN KEY([IconId])
		REFERENCES [md].[FontAwesomeIcon] ([Id])

		ALTER TABLE [dbo].[Report] CHECK CONSTRAINT [FK_Report_FontAwesomeIcon_IconId]
	end

if object_id('ReportDocumentDefinition', 'U') is null 
	begin
		CREATE TABLE [dbo].[ReportDocumentDefinition](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[DocumentDefinitionId] [int] NULL,
			[IsDeleted] [bit] NULL,
			[ReportId] [int] NULL,
		PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[ReportDocumentDefinition]  WITH CHECK ADD  CONSTRAINT [FK_ReportDocumentDefinition_Report_ReportId] FOREIGN KEY([ReportId])
		REFERENCES [dbo].[Report] ([Id])

		ALTER TABLE [dbo].[ReportDocumentDefinition] CHECK CONSTRAINT [FK_ReportDocumentDefinition_Report_ReportId]
	end

if object_id('ReportLayout', 'U') is null 
	begin
		CREATE TABLE [dbo].[ReportLayout](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[Value] [text] NULL,
			[Name] [nvarchar](255) NULL,
			[Label] [nvarchar](255) NULL,
			[IsVisible] [bit] NULL,
			[IsDeleted] [bit] NULL,
			[ReportId] [int] NULL,
		PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

		ALTER TABLE [dbo].[ReportLayout]  WITH CHECK ADD  CONSTRAINT [FK_ReportLayout_Report_ReportId] FOREIGN KEY([ReportId])
		REFERENCES [dbo].[Report] ([Id])

		ALTER TABLE [dbo].[ReportLayout] CHECK CONSTRAINT [FK_ReportLayout_Report_ReportId]
	end

if object_id('ReportParam', 'U') is null 
	begin
		CREATE TABLE [dbo].[ReportParam](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[DataTypeId] [int] NULL,
			[Field] [nvarchar](255) NULL,
			[Name] [nvarchar](255) NULL,
			[Description] [nvarchar](255) NULL,
			[VisibleIndex] [int] NULL,
			[IsVisibleInReportViewer] [bit] NULL,
			[IsVisibleInScheduler] [bit] NULL,
			[IsDeleted] [bit] NULL,
			[ReportId] [int] NULL,
			[IconId] [int] NULL,
		PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[ReportParam]  WITH CHECK ADD  CONSTRAINT [FK_ReportParam_FontAwesomeIcon_IconId] FOREIGN KEY([IconId])
		REFERENCES [md].[FontAwesomeIcon] ([Id])

		ALTER TABLE [dbo].[ReportParam] CHECK CONSTRAINT [FK_ReportParam_FontAwesomeIcon_IconId]

		ALTER TABLE [dbo].[ReportParam]  WITH CHECK ADD  CONSTRAINT [FK_ReportParam_Report_ReportId] FOREIGN KEY([ReportId])
		REFERENCES [dbo].[Report] ([Id])

		ALTER TABLE [dbo].[ReportParam] CHECK CONSTRAINT [FK_ReportParam_Report_ReportId]
	end
go