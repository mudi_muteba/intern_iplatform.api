﻿

DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT,
  @ParentCoverDefinitionId int, @ChildCoverDefinitionId int, @DummyQuestionDefId int, @NewQuestionId int
DECLARE @coverDefId INT
DECLARE @CoverId INT


SET @Name = 'MW'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode = @Name )

if (@productid IS NULL)
BEGIN
    --Miway Product does not exist so now we must first create it (out of scope)
 Return
 END
 ELSE
 BEGIN
	SET @id = @productid
 END

--1. Add Extra Questions to the Miway Motor.
--1.1. Add new Miway mapped questions 
--2. Remove Motor Multiquote questions from Miway

declare @CoverDefinitionId int

----------------------Motor-------------------
SET @CoverId = 218 -- 218 is Motor

SET @coverDefId = ( SELECT TOP 1 ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @id AND CoverId = @CoverId)

IF (@coverDefId IS NULL)
BEGIN

-- THIS SHOULD EXIST ALREADY
 RETURN

END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId
END
if not exists(select * from QuestionDefinition where QuestionId=1458 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Amputee Related Driver Restrictions', @CoverDefinitionId, 1458, NULL, 1, 11, 0, 1, 0,
               N'Amputee Related Driver Restrictions', N'' , N'', 0, 5)
              end


--------------- Map To Multiquote  Motor

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = @CoverId
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId 
SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

SET @NewQuestionId = 1458 

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id

SET @NewQuestionId = 1096 

if not exists(SELECT * FROM MapQuestionDefinition mq INNER Join QuestionDefinition qd on qd.id = mq.ChildId where qd.QuestionId = @NewQuestionId  and mq.ParentId = @DummyQuestionDefId and CoverDefinitionId = @ChildCoverDefinitionId)
INSERT into MapQuestionDefinition(ParentId,ChildId,IsDeleted) SELECT ParentQuestion.Id, ChildQuestion.Id, 0
FROM 
(SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081) AS ParentQuestion
LEFT JOIN 
(SELECT TOP 1 Child.Id FROM QuestionDefinition Child WHERE Child.CoverDefinitionId = @ChildCoverDefinitionId AND Child.QuestionId = @NewQuestionId) AS ChildQuestion
ON ParentQuestion.Id != ChildQuestion.Id



PRINT('DONE')
