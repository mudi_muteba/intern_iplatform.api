﻿if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover_ForPersonalLegalLiability') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForPersonalLegalLiability
	end
go

create procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForPersonalLegalLiability
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	select distinct
		ltrim(rtrim(dbo.ToCamelCase(Asset.[Description]))) [Description],
		cast(QuoteItem.SumInsured as numeric(18, 2)) [Value]
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.Id = ProposalDefinition.AssetId
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
	group by
		Asset.[Description],
		QuoteItem.SumInsured
go


if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover_ForOther') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForOther
	end
go

create procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForOther
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	select distinct
		ltrim(rtrim(dbo.ToCamelCase(Asset.[Description]))) [Description],
		cast(QuoteItem.SumInsured as numeric(18, 2)) [Value]
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber 
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
	group by
		Asset.[Description],
		QuoteItem.SumInsured
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Assets_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_Assets_ByCover
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Building'))
		begin
			print 'Building'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding @ProposalHeaderId, @QuoteIds, @CoverId
		end

	if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Contents'))
		begin
			print 'Contents'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForContents @ProposalHeaderId, @QuoteIds, @CoverId
		end

	if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Personal Legal Liability'))
		begin
			print 'Personal Legal Liability'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForPersonalLegalLiability @ProposalHeaderId, @QuoteIds, @CoverId
		end

	if(@CoverId in (select md.Cover.Id from md.Cover where md.Cover.Name not in ('Building', 'Contents', 'Personal Legal Liability')))
		begin
			print 'Other'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForOther  @ProposalHeaderId, @QuoteIds, @CoverId
		end
go