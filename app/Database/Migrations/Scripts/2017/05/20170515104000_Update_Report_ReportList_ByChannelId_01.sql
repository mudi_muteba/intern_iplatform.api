﻿if exists (select * from sys.objects where object_id = object_id(N'Report_ReportList_ByChannelId') and type in (N'P', N'PC'))
	begin
		drop procedure Report_ReportList_ByChannelId
	end
go

create procedure Report_ReportList_ByChannelId
(
	@ChannelId int
)
as
	select
		Report.Id,
		Report.ReportCategoryId,
		Report.ReportTypeId,
		Report.ReportFormatId,
		Report.Name,
		Report.[Description],
		Report.SourceFile,
		Report.IsVisibleInReportViewer,
		Report.IsVisibleInScheduler,
		Report.IsActive,
		Report.VisibleIndex,
		Report.IconId,
		Report.Info
	from Report
		inner join Channel on Channel.Id = Report.ChannelId
	where
		Channel.Id = @ChannelId and
		Channel.IsDeleted = 0 and
		Report.IsDeleted = 0
	order by
		Report.Name
go
