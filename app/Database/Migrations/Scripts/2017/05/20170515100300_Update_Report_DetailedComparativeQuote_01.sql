﻿--Reports_DetailedComparativeQuote_Header
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Header
	end
go

create procedure Reports_DetailedComparativeQuote_Header
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@Code varchar(50)
)
as
	select distinct top 1
		Client.DisplayName Client,
		(
			select top 1
				case when [Address].IsComplex = 1
					then dbo.ToCamelCase(ltrim(rtrim([Address].Complex)) + ' ' + ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' + ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim([Address].Code)))
					else dbo.ToCamelCase(ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' +  ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim( [Address].Code)))
				end
			from [Address]
			where
				[Address].PartyId = Client.Id and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address') and
				[Address].IsDeleted = 0
		) PhysicalAddress,
		(
			select top 1
				case when [Address].IsComplex = 1
					then dbo.ToCamelCase(ltrim(rtrim([Address].Complex)) + ' ' + ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' + ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim([Address].Code)))
					else dbo.ToCamelCase(ltrim(rtrim([Address].Line1)) + ' ' + ltrim(rtrim([Address].Line2)) + ' ' + ltrim(rtrim([Address].Line3)) + ' ' +  ltrim(rtrim([Address].Line4)) + ' ' + ltrim(rtrim( [Address].Code)))
				end
			from [Address]
			where
				[Address].PartyId = Client.Id and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address') and
				[Address].IsDeleted = 0
		) PostalAddress,

		ClientContactDetail.Work WorkNo,
		ClientContactDetail.Home HomeNo,
		ClientContactDetail.Fax FaxNo,
		ClientContactDetail.Cell CellNo,
		ClientContactDetail.Email EmailAddress,

		case
			when len(ltrim(rtrim(ClientContact.IdentityNo))) = 0 then ClientContact.PassportNo
			else ClientContact.IdentityNo
		end IDNumber,

		Quote.QuoteHeaderId,
		Quote.InsurerReference QuoteNo,
		Product.QuoteExpiration,
		Quote.Fees,
		(
			select
				Sum(QuoteItem.Sasria)
			from QuoteItem
			where
				QuoteItem.IsDeleted = 0 and
				QuoteItem.QuoteId = Quote.Id and
				QuoteItem.Sasria is not null and
				QuoteItem.Sasria > 0
		) SASRIA,
		(
			select
				Sum(QuoteItem.Premium) + Sum(QuoteItem.Sasria) + Quote.Fees
			from QuoteItem
			where
				QuoteItem.IsDeleted = 0 and
				QuoteItem.QuoteId = Quote.Id and
				QuoteItem.Premium is not null and
				QuoteItem.Premium > 0
		) Total,
		(
			select top 1
				QuoteEMailBody
			from Organization
			where
				Code = @Code
		) QuoteEMailBody,
		(
			select top 1
				dbo.ToCamelCase(Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code)
			from [Address]
			where
				[Address].PartyId = Client.Id and
				[Address].IsDefault = 1 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address')
		) DefaultPhysical,
		(
			select top 1
				dbo.ToCamelCase(Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code)
			from [Address]
			where
				[Address].PartyId = Client.Id and
				[Address].IsDefault = 1 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address')
		) DefaultPostal,
		(
			select top 1
				dbo.ToCamelCase(Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code)
			from [Address]
			where
				[Address].PartyId = Client.Id and
				[Address].IsDefault = 0 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address')
		) NoDefaultPhysical,
		(
			select top 1
				dbo.ToCamelCase(Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code)
			from [Address]
			where
				[Address].PartyId = Client.Id and
				[Address].IsDefault = 0 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address')
		) NoDefaultPostal,
		Individual.FirstName,
		Individual.Surname
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join Product on Product.Id = Quote.ProductId
		inner join LeadActivity on LeadActivity.Id = ProposalHeader.LeadActivityId
		inner join Party Client on Client.Id = LeadActivity.PartyId
		inner join Individual on Individual.PartyId = Client.Id
		--inner join Party [Owner] on [Owner].Id = Product.ProductOwnerId
		--inner join Party [Provider] on [Provider].Id = Product.ProductProviderId
		--inner join Organization OwnerOrg on OwnerOrg.PartyId = [Owner].Id
		--inner join Organization ProviderOrg on ProviderOrg.PartyId = [Provider].Id
		left join [Address] ClientAddress on ClientAddress.PartyId = Client.Id
		left join Contact ClientContact on ClientContact.PartyId = Client.Id
		left join ContactDetail ClientContactDetail on ClientContactDetail.Id = Client.ContactDetailId
	where
		ProposalHeader.Id = @ProposalHeaderId and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		Product.IsDeleted = 0 and
		LeadActivity.IsDeleted = 0 and
		Client.IsDeleted = 0 and
		ClientAddress.IsDeleted = 0 and
		ClientContactDetail.IsDeleted = 0 and
		Quote.Id in (select * from fn_StringListToTable(@QuoteIds))
go



--Reports_DetailedComparativeQuote_Summary
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Summary') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Summary
	end
go

create procedure Reports_DetailedComparativeQuote_Summary
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max)
)
as
	;with summary_CTE(ProposalHeaderId, QuoteHeaderId, OrganizationId, ProductId, QuoteId)
	as
	(
		select distinct
			ProposalHeader.Id,
			QuoteHeader.Id, 
			Organization.PartyId,
			Product.Id,
			Quote.Id
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
			inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			ProposalHeader.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
			Quote.IsDeleted = 0 and
			CoverDefinition.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId
		group by
			ProposalHeader.Id,
			QuoteHeader.Id, 
			Organization.PartyId,
			Product.Id,
			Quote.Id
	)
	select
		(
			select
				Organization.TradingName
			from Organization
			where
				Organization.PartyId = summary_CTE.OrganizationId
		) Insurer,
		(
			select
				Organization.Code
			from Organization
			where
				Organization.PartyId = summary_CTE.OrganizationId
		) Code,
		(
			select
				Product.[Name]
			from Product
			where
				Product.Id = summary_CTE.ProductId
		) Product,
		(
			select
				sum(cast((QuoteItem.ExcessBasic) as numeric(18, 2)))
			from Quote
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			where
				Quote.Id = summary_CTE.QuoteId
		) BasicExcess,
		(
			select
				sum(cast((QuoteItem.Premium) as numeric(18, 2)))
			from Quote
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			where
				Quote.Id = summary_CTE.QuoteId
		) Premium,
		(
			select
				sum(cast((QuoteItem.SASRIA) as numeric(18, 2)))
			from Quote
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			where
				Quote.Id = summary_CTE.QuoteId
		) SASRIA,
		(
			select
				sum(cast((Quote.Fees) as numeric(18, 2)))
			from Quote
			where
				Quote.Id = summary_CTE.QuoteId
		) Fees,
		(
			(
				select
					sum(cast((QuoteItem.Premium) as numeric(18, 2))) + sum(cast((QuoteItem.SASRIA) as numeric(18, 2)))
				from Quote
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				where
					Quote.Id = summary_CTE.QuoteId
			)

			+

			(
				select
					sum(cast((Quote.Fees) as numeric(18, 2)))
				from Quote
				where
					Quote.Id = summary_CTE.QuoteId
			)
		) Total,
		(
			select
				cast(isnull(sum(Commission), 0) as numeric(18, 2))
			from
			(
				select
					case
						when md.Cover.name = 'All Risk' then sum(cast((QuoteItem.Premium * cast(20 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
						when md.Cover.name = 'Building' then sum(cast((QuoteItem.Premium * cast(20 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
						when md.Cover.name = 'Caravan Or Trailer' then sum(cast((QuoteItem.Premium * cast(12.5 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
						when md.Cover.name = 'Contents' then sum(cast((QuoteItem.Premium * cast(20 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
						when md.Cover.name = 'Motor' then sum(cast((QuoteItem.Premium * cast(12.5 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
						when md.Cover.name = 'Personal Legal Liability' then sum(cast((QuoteItem.Premium * cast(20 as numeric(18, 2)) / cast(100 as numeric(18, 2))) as numeric(18, 2)))
						else 0
					end Commission
				from Quote
					inner join Product on Product.Id = Quote.ProductId
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId			
				where
					Quote.Id = summary_CTE.QuoteId and
					Product.Id = summary_CTE.ProductId
				group by
					quote.id,
					md.Cover.name
			) x
		) Commission
	from summary_CTE
go



--Reports_DetailedComparativeQuote_Covers
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Covers') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Covers
	end
go

create procedure Reports_DetailedComparativeQuote_Covers
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max)
)
as
	;with Covers_CTE (CoverId, [Description])
	as 
	(
		select distinct
			md.Cover.Id CoverId,
			case
				when md.Cover.Name = 'AIG Assist'
				then 'VMI Assist'
			else
				md.Cover.Name
			end Description
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
			inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
		where
			ProposalHeader.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			CoverDefinition.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			md.Cover.name <> 'Value Added Products' and
			Quote.Id in (select * from fn_StringListToTable(@QuoteIds))
		)
	select
		CoverId,
		[Description]
	from
		Covers_CTE
	order by
		[Description] asc
go



--Reports_DetailedComparativeQuote_Assets_ByCover
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding
	end
go

create procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	select distinct
		ltrim(rtrim(dbo.ToCamelCase(Asset.[Description]))) [Description],
		cast(QuoteItem.SumInsured as numeric(18, 2)) [Value]
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.Id = ProposalDefinition.AssetId
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
	group by
		Asset.[Description],
		QuoteItem.SumInsured
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover_ForNonBuilding') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForNonBuilding
	end
go

create procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForNonBuilding
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	select distinct
		ltrim(rtrim(dbo.ToCamelCase(Asset.[Description]))) [Description],
		cast(QuoteItem.SumInsured as numeric(18, 2)) [Value]
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber 
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
	group by
		Asset.[Description],
		QuoteItem.SumInsured
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Assets_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_Assets_ByCover
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Building'))
		begin
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding @ProposalHeaderId, @QuoteIds, @CoverId
		end
	else
		begin
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForNonBuilding  @ProposalHeaderId, @QuoteIds, @CoverId
		end
go



--Reports_DetailedComparativeQuote_Benefits_ByCover
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Benefits_ByCover_ForBuilding') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Benefits_ByCover_ForBuilding
	end
go

create procedure Reports_DetailedComparativeQuote_Benefits_ByCover_ForBuilding
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	declare
		@Columns as nvarchar(max),
		@DefaultColumns as varchar(max),
		@Query as nvarchar(max),
		@AssetId int

	select @DefaultColumns = ''''' [Description], '''' [col_0], '''' [col_1], '''' [col_2]'

	declare summary_Cursor cursor fast_forward for
		select distinct
			Asset.Id
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
			inner join Asset on Asset.Id = ProposalDefinition.AssetId
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			ProposalHeader.IsDeleted = 0 and
			ProposalDefinition.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			Asset.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			ProposalDefinition.CoverId = @CoverId and
			CoverDefinition.CoverId = @CoverId and
			Quote.Id in
			(
				select * from fn_StringListToTable(@QuoteIds)
			)
		group by
			Asset.Id,
			ProposalHeader.Id

	open summary_Cursor
	fetch next from summary_Cursor into @AssetId

	while @@fetch_status = 0
		begin
			set @Columns =
				stuff
				(
					(
						select distinct
							',' + quotename(Organization.Code)
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = @ProposalHeaderId and
							ProposalDefinition.CoverId = @CoverId and
							CoverDefinition.CoverId = @CoverId and
							Quote.Id in
							(
								select * from fn_StringListToTable(@QuoteIds)
							)
					for xml path(''), type)
				.value('.', 'nvarchar(max)' ), 1, 1, '')

			if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 0
				begin
					select @Columns = @Columns + ', [col_1], [col_2]'
				end

			if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 1
				begin
					select @Columns = @Columns + ', [col_1]'
				end

			set @Query =
				'
					insert into #Breakdown (Col_1, Col_2, Col_3, Col_4)
					select
						' + @DefaultColumns + '

					union all 

					select
						Description,
						' + @Columns + '
					from
					(
						select
							Upper(Asset.Description) Description,
							Organization.Code Insurer
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(Insurer)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Premium'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.Premium as varchar) Premium
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(Premium)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Basic Excess'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.ExcessBasic as varchar) ExcessBasic
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(ExcessBasic)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''SASRIA'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.Sasria as varchar) SASRIA
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where	
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(SASRIA)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Total Payment'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast((QuoteItem.Premium + QuoteItem.Sasria) as varchar) TotalPayment
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(TotalPayment)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							ProductBenefit.Name Description,
							ProductBenefit.Value BenefitValue
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProductBenefit.Name is not null and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(BenefitValue)
						for Insurer in (' + @Columns + ')
					) p
			'

			execute(@query);

			fetch next from summary_Cursor into @AssetId
		end

	close summary_Cursor
	deallocate summary_Cursor
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Benefits_ByCover_ForNonBuilding') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Benefits_ByCover_ForNonBuilding
	end
go

create procedure Reports_DetailedComparativeQuote_Benefits_ByCover_ForNonBuilding
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	declare
		@Columns as nvarchar(max),
		@DefaultColumns as varchar(max),
		@Query as nvarchar(max),
		@AssetId int

	select @DefaultColumns = ''''' [Description], '''' [col_0], '''' [col_1], '''' [col_2]'

	declare summary_Cursor cursor fast_forward for
		select distinct
			Asset.Id
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
			inner join Asset on Asset.Id = ProposalDefinition.AssetId
			--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			ProposalHeader.IsDeleted = 0 and
			ProposalDefinition.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			Asset.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			ProposalDefinition.CoverId = @CoverId and
			CoverDefinition.CoverId = @CoverId and
			Quote.Id in
			(
				select * from fn_StringListToTable(@QuoteIds)
			)
		group by
			Asset.Id

	open summary_Cursor
	fetch next from summary_Cursor into @AssetId

	while @@fetch_status = 0
		begin
			set @Columns =
				stuff
				(
					(
						select distinct
							',' + quotename(Organization.Code)
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
						where
							Asset.Id = @AssetId and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = @ProposalHeaderId and
							ProposalDefinition.CoverId = @CoverId and
							CoverDefinition.CoverId = @CoverId and
							Quote.Id in
							(
								select * from fn_StringListToTable(@QuoteIds)
							)
					for xml path(''), type)
				.value('.', 'nvarchar(max)' ), 1, 1, '')

			if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 0
				begin
					select @Columns = @Columns + ', [col_1], [col_2]'
				end

			if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 1
				begin
					select @Columns = @Columns + ', [col_1]'
				end

			set @Query =
				'
					insert into #Breakdown (Col_1, Col_2, Col_3, Col_4)
					select
						' + @DefaultColumns + '

					union all 

					select
						Description,
						' + @Columns + '
					from
					(
						select
							Upper(Asset.Description) Description,
							Organization.Code Insurer
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(Insurer)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Premium'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.Premium as varchar) Premium
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(Premium)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Basic Excess'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.ExcessBasic as varchar) ExcessBasic
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(ExcessBasic)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''SASRIA'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.Sasria as varchar) SASRIA
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(SASRIA)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Total Payment'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast((QuoteItem.Premium + QuoteItem.Sasria) as varchar) TotalPayment
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(TotalPayment)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							ProductBenefit.Name Description,
							ProductBenefit.Value BenefitValue
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
							ProductBenefit.Name is not null and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(BenefitValue)
						for Insurer in (' + @Columns + ')
					) p
			'

			execute(@query);

			fetch next from summary_Cursor into @AssetId
		end

	close summary_Cursor
	deallocate summary_Cursor
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Benefits_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Benefits_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_Benefits_ByCover
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	create table #Breakdown
	(
		Col_1 varchar(255),
		Col_2 varchar(255),
		Col_3 varchar(255),
		Col_4 varchar(255)
	)

	if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Building'))
		begin
			exec Reports_DetailedComparativeQuote_Benefits_ByCover_ForBuilding @ProposalHeaderId, @QuoteIds, @CoverId
		end
	else
		begin
			exec Reports_DetailedComparativeQuote_Benefits_ByCover_ForNonBuilding @ProposalHeaderId, @QuoteIds, @CoverId
		end

	select * from #Breakdown
	drop table #Breakdown
go



--Reports_DetailedComparativeQuote_Organizations
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Organizations') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Organizations
	end
go

create procedure Reports_DetailedComparativeQuote_Organizations
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	;with Covers_CTE (OrganizationId, TradingName, Code, CoverDefinitionIds)
	as 
	(
		select distinct
			Organization.PartyId,
			Organization.TradingName,
			Organization.Code,
			(
				stuff
				(
					(
						select
							',' + cast(CoverDefinition.Id as nvarchar)
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization o on o.PartyId = Product.ProductOwnerId
						where
							ProposalHeader.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							CoverDefinition.IsDeleted = 0 and
							ProposalHeader.Id = @ProposalHeaderId and
							Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
							o.PartyId = Organization.PartyId and
							md.Cover.Id = @CoverId
						for xml path(''), type
					).value('.', 'NVARCHAR(MAX)'), 1, 1, '')
			)
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			ProposalHeader.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			CoverDefinition.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
			md.Cover.Id = @CoverId and
			CoverDefinition.Id in (select CoverDefinitionId from ProductAdditionalExcess where CoverDefinitionId = CoverDefinition.Id)
		)
	select
		OrganizationId,
		TradingName,
		Code,
		CoverDefinitionIds
	from
		Covers_CTE
	order by
		TradingName asc
go



--Reports_DetailedComparativeQuote_AdditionalExcess_Categories_ByCover
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_AdditionalExcess_Categories_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_AdditionalExcess_Categories_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_AdditionalExcess_Categories_ByCover
(
	@ProposalHeaderId int,
	@CoverDefinitionId int
)
as
	select
		ProductAdditionalExcess.Category,
		'Not Used' NotUsed
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
		inner join CoverDefinition on CoverDefinition.CoverId = md.Cover.Id
		inner join ProductAdditionalExcess on ProductAdditionalExcess.CoverDefinitionId = CoverDefinition.Id
	where
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.IsDeleted = 0 and
		CoverDefinition.Id = @CoverDefinitionId
	group by
		ProductAdditionalExcess.Category
go



--Reports_DetailedComparativeQuote_AdditionalExcess_ByCover
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_AdditionalExcess_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_AdditionalExcess_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_AdditionalExcess_ByCover
(
	@ProposalHeaderId int,
	@CoverDefinitionId int
)
as
	select
		ProductAdditionalExcess.Id,
		ProductAdditionalExcess.[Index] 'Index',
		ProductAdditionalExcess.Category,
		ProductAdditionalExcess.[Description] 'Description',
		ProductAdditionalExcess.ActualExcess,
		ProductAdditionalExcess.MinExcess,
		ProductAdditionalExcess.MaxExcess,
		ProductAdditionalExcess.Percentage,
		ProductAdditionalExcess.IsPercentageOfClaim,
		ProductAdditionalExcess.IsPercentageOfItem,
		ProductAdditionalExcess.IsPercentageOfSumInsured,
		ProductAdditionalExcess.ShouldApplySelectedExcess,
		ProductAdditionalExcess.ShouldDisplayExcessValues
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
		inner join CoverDefinition on CoverDefinition.CoverId = md.Cover.Id
		inner join ProductAdditionalExcess on ProductAdditionalExcess.CoverDefinitionId = CoverDefinition.Id
	where
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.IsDeleted = 0 and
		CoverDefinition.Id = @CoverDefinitionId
	order by
		ProductAdditionalExcess.[Index] asc
go



--Reports_DetailedComparativeQuote_ProductAddendums
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_ProductAddendums') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_ProductAddendums
	end
go

create procedure Reports_DetailedComparativeQuote_ProductAddendums
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max)
)
as
	select distinct
		Organization.TradingName Insurer,
		Product.Name Product,
		Product.QuoteExpiration QuoteValidFor,
		'<ol>' +
		stuff
		(
			(
				select distinct
					'<li>' + pd.[Description] + '</li>'
				from ProposalHeader ph
					inner join QuoteHeader qh on qh.ProposalHeaderId = ph.Id
					inner join Quote q on q.QuoteHeaderId = qh.Id
					inner join Product p  on p.Id = q.ProductId
					inner join ProductDescription pd on pd.ProductId = p.Id
				where
					p.Id = Product.Id
				--order by
				--	pd.[Index] asc
				for xml path(''), type
			).value('.', 'NVARCHAR(MAX)'), 1, 0, ''
		)
		+ '</ol>' Addendum
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.AssetNo = QuoteItem.AssetNumber
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		Quote.Id in (select * from fn_StringListToTable(@QuoteIds))
	order by
		Organization.TradingName
go



--Reports_DetailedComparativeQuote_AdditionalBenefits
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_AdditionalBenefits') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_AdditionalBenefits
	end
go

create procedure Reports_DetailedComparativeQuote_AdditionalBenefits
(
	@ProposalHeaderId int,
	@QuestionIds varchar(max)
)
as
	select
		md.Question.Name,
		md.QuestionAnswer.Answer
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
		inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
		inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
		inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
	where
		md.Question.Id in (select * from fn_StringListToTable(@QuestionIds)) and
		ProposalHeader.Id = @ProposalHeaderId and
		isnumeric(ProposalQuestionAnswer.Answer) = 1 and
		md.QuestionAnswer.Answer <> '0' and
		md.QuestionAnswer.Answer <> ''

	union all

	select
		md.Question.Name,
		case
			when ProposalQuestionAnswer.Answer = 'True' then 'Yes'
			else ProposalQuestionAnswer.Answer
		end Answer
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
		inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
		inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
		inner join md.QuestionGroup on md.QuestionGroup.Id = md.Question.QuestionGroupId
	where
		md.Question.Id not in (select * from fn_StringListToTable(@QuestionIds)) and
		ProposalHeader.Id = @ProposalHeaderId and
		md.QuestionGroup.Name = 'Additional Options' and
		ProposalQuestionAnswer.Answer <> 'False' and
		ProposalQuestionAnswer.Answer <> '0' and
		ProposalQuestionAnswer.Answer <> ''
	order by
		md.Question.name asc
go