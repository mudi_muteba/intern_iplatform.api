﻿IF EXISTS(SELECT TOP 1 * FROM QuoteItem WHERE OriginalPremium IS NULL)
BEGIN
	UPDATE QuoteItem
	SET
		OriginalPremium = Premium
	WHERE OriginalPremium IS NULL
END