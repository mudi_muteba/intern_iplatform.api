﻿DECLARE @constraint VARCHAR(MAX) = '';
 
WITH FKData
AS 
(
	SELECT
		FK.parent_object_id, 
		FKC.parent_column_id, 
		FK.referenced_object_id, 
		FKC.referenced_column_id, 
		FKCount = COUNT(*)
	FROM sys.foreign_keys FK
    INNER JOIN sys.foreign_key_columns FKC 
		ON FKC.constraint_object_id = FK.object_id
    GROUP BY  FK.parent_object_id, FKC.parent_column_id, FK.referenced_object_id, FKC.referenced_column_id
    HAVING COUNT(*) > 1
),
DuplicateFK
AS 
(
	SELECT
		FK.name AS FKName, 
		S1.name AS ParentSchema, 
		T1.name AS ParentTable, 
		C1.name AS ParentColumn, 
		T2.name AS ReferencedTable,
		C2.name AS ReferencedColumn
	FROM sys.foreign_keys FK
	INNER JOIN sys.foreign_key_columns FKC 
		ON FKC.constraint_object_id = FK.object_id
	INNER JOIN FKData F 
		ON FK.parent_object_id = F.parent_object_id
		AND FK.referenced_object_id = F.referenced_object_id
		AND FKC.parent_column_id = F.parent_column_id
		AND FKC.referenced_column_id = F.referenced_column_id
	INNER JOIN sys.tables T1 
		ON F.parent_object_id = T1.object_id
    INNER JOIN sys.columns C1 
		ON F.parent_object_id = C1.object_id
		AND F.parent_column_id = C1.column_id
	INNER JOIN sys.schemas S1 
		ON T1.schema_id = S1.schema_id
	INNER JOIN sys.tables T2 
		ON F.referenced_object_id = T2.object_id
	INNER JOIN sys.columns C2 
		ON F.referenced_object_id = C2.object_id
		AND F.referenced_column_id = C2.column_id
),
dupes
AS 
(
	SELECT
		DuplicateFK.ParentTable, 
		DuplicateFK.ParentColumn, 
		DuplicateFK.ReferencedTable, 
		DuplicateFK.ReferencedColumn, 
		DuplicateFK.FKName,
        DuplicateFK.ParentSchema,
        ROW_NUMBER() OVER (PARTITION BY DuplicateFK.ReferencedTable, DuplicateFK.ReferencedColumn, DuplicateFK.ParentTable ORDER BY DuplicateFK.FKName) rowno
    FROM DuplicateFK
)
SELECT
	@constraint = @constraint + ' ALTER TABLE ' + dupes.ParentSchema + '.' + dupes.ParentTable + ' DROP CONSTRAINT ' + dupes.FKName
FROM dupes
WHERE dupes.rowno > 1;
 
EXEC sys.sp_sqlexec @p1 = @constraint;