﻿declare
	@AG_Motor_CoverDefinitionId int,
	@AG_Contents_CoverDefinitionId int,
	@AG_Building_CoverDefinitionId int,
	@AG_AllRisk_CoverDefinitionId int,
	@VIRS_Motor_CoverDefinitionId int,
	@VIRS_Contents_CoverDefinitionId int,
	@VIRS_Building_CoverDefinitionId int,
	@VIRS_AllRisk_CoverDefinitionId int

select
	@AG_Motor_CoverDefinitionId =  (select top 1 CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('AUGPRD') and Cover.Name in ('Motor')),
	@AG_Contents_CoverDefinitionId = (select top 1 CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('AUGPRD') and Cover.Name in ('Contents')),
	@AG_Building_CoverDefinitionId = (select top 1 CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('AUGPRD') and Cover.Name in ('Building')),
	@AG_AllRisk_CoverDefinitionId = (select top 1 CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('AUGPRD') and Cover.Name in ('All Risk')),
	@VIRS_Motor_CoverDefinitionId = (select top 1 CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('VIRS') and Cover.Name in ('Motor')),
	@VIRS_Contents_CoverDefinitionId = (select top 1 CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('VIRS') and Cover.Name in ('Contents')),
	@VIRS_Building_CoverDefinitionId = (select top 1 CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('VIRS') and Cover.Name in ('Building')),
	@VIRS_AllRisk_CoverDefinitionId = (select top 1 CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('VIRS') and Cover.Name in ('All Risk'))

delete from ProductBenefit
where
	CoverDefinitionId in
	(
		@VIRS_Motor_CoverDefinitionId,
		@VIRS_Contents_CoverDefinitionId,
		@VIRS_Building_CoverDefinitionId,
		@VIRS_AllRisk_CoverDefinitionId
	)

insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted)
select
	@VIRS_Motor_CoverDefinitionId,
	Name,
	Value,
	ShowToClient,
	VisibleIndex,
	IsDeleted
from ProductBenefit
where
	CoverDefinitionId = @AG_Motor_CoverDefinitionId

insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted)
select
	@VIRS_Contents_CoverDefinitionId,
	Name,
	Value,
	ShowToClient,
	VisibleIndex,
	IsDeleted
from ProductBenefit
where
	CoverDefinitionId = @AG_Contents_CoverDefinitionId

insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted)
select
	@VIRS_Building_CoverDefinitionId,
	Name,
	Value,
	ShowToClient,
	VisibleIndex,
	IsDeleted
from ProductBenefit
where
	CoverDefinitionId = @AG_Building_CoverDefinitionId

insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted)
select
	@VIRS_AllRisk_CoverDefinitionId,
	Name,
	Value,
	ShowToClient,
	VisibleIndex,
	IsDeleted
from ProductBenefit
where
	CoverDefinitionId = @AG_AllRisk_CoverDefinitionId