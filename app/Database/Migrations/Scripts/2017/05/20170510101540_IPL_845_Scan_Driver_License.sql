﻿Declare @ProductId int;

SELECT Top 1 @ProductId = Id FROM Product WHERE ProductCode = 'INDWE'

if exists (SELECT Top 1 Id FROM Product WHERE ProductCode = 'INDWE')
BEGIN
	if not exists(SELECT * FROM ProductClaimsQuestionDefinition WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 48)
	BEGIN
		INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,48, 1, 1, 0, 'Scan Driver License', 1, 14);
	END
	if not exists(SELECT * FROM ProductClaimsQuestionDefinition WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 47)
	BEGIN
		INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,47, 1, 1, 0, 'Driver''s ID Number', 4, 14);
	END
	if not exists(SELECT * FROM ProductClaimsQuestionDefinition WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 1180)
	BEGIN
		INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1180, 1, 1, 0, 'Driver''s Date of Birth', 5, 14);
	END
	if not exists(SELECT * FROM ProductClaimsQuestionDefinition WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 52)
	BEGIN
		INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,52, 0, 1, 0, 'Driver''s License Code', 8, 14);
	END
	if not exists(SELECT * FROM ProductClaimsQuestionDefinition WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 1181)
	BEGIN
		INSERT INTO ProductClaimsQuestionDefinition (ProductId, IsDeleted, ClaimsQuestionDefinitionId,  [Required], FirstPhase, SecondPhase, DisplayName, VisibleIndex, ClaimsQuestionGroupId) VALUES (@ProductId, 0,1181, 0, 1, 0, 'Driver''s License From Date', 9, 14);
	END
	Update ProductClaimsQuestionDefinition Set IsDeleted = 1 WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 135
	Update ProductClaimsQuestionDefinition Set DisplayName = 'Driver''s License Expiry Date' WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 51
	Update ProductClaimsQuestionDefinition Set FirstPhase = 1, SecondPhase = 0, VisibleIndex = 4 WHERE ProductId = @ProductId AND ClaimsQuestionDefinitionId = 47
END