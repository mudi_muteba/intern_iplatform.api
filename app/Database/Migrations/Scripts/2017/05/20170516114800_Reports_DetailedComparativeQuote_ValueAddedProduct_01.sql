﻿--Reports_DetailedComparativeQuote_ValueAddedProduct
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_ValueAddedProduct') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_ValueAddedProduct
	end
go

create procedure Reports_DetailedComparativeQuote_ValueAddedProduct
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max)
)
as
	;with vaps_CTE(ProposalHeaderId, QuoteHeaderId, OrganizationId, ProductId, QuoteId)
	as
	(
		select distinct
			ProposalHeader.Id,
			QuoteHeader.Id, 
			Organization.PartyId,
			Product.Id,
			Quote.Id
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
			inner join Asset on Asset.Id = ProposalDefinition.AssetId or Asset.AssetNo = QuoteItem.AssetNumber
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			ProposalHeader.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.Id in (select * from fn_StringListToTable(@QuoteIds)) and
			Quote.IsDeleted = 0 and
			CoverDefinition.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			QuoteItem.IsVap = 1
		group by
			ProposalHeader.Id,
			QuoteHeader.Id, 
			Organization.PartyId,
			Product.Id,
			Quote.Id
	)
	select
		(
			select
				Organization.Code
			from Organization
			where
				Organization.PartyId = vaps_CTE.OrganizationId
		) Code,
		(
			select top 1
				Product.[Name]
			from Product
			where
				Product.Id = vaps_CTE.ProductId
		) Product,
		(
			select			
				coalesce('<li>' + QuoteItem.[Description] + '</li>', '')
			from Quote
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			where
				Quote.Id = vaps_CTE.QuoteId and
				QuoteItem.IsVap = 1
		) ValueAddedProducts,
		(
			select
				sum(cast((QuoteItem.Premium) as numeric(18, 2)))
			from Quote
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			where
				Quote.Id = vaps_CTE.QuoteId and
				QuoteItem.IsVap = 1
		) TotalPremium
	from vaps_CTE
go