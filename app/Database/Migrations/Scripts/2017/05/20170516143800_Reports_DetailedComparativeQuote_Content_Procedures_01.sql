﻿--Reports_DetailedComparativeQuote_Assets_ByCover
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding
	end
go

create procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	select distinct
		ltrim(rtrim(dbo.ToCamelCase(Asset.[Description]))) [Description],
		cast(QuoteItem.SumInsured as numeric(18, 2)) [Value]
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.Id = ProposalDefinition.AssetId
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
	group by
		Asset.[Description],
		QuoteItem.SumInsured
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover_ForContents') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForContents
	end
go

create procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForContents
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	select distinct
		ltrim(rtrim(dbo.ToCamelCase(Asset.[Description]))) [Description],
		cast(QuoteItem.SumInsured as numeric(18, 2)) [Value]
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.Id = ProposalDefinition.AssetId
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
	group by
		Asset.[Description],
		QuoteItem.SumInsured
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover_ForNonBuilding') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForNonBuilding
	end
go

create procedure Reports_DetailedComparativeQuote_Assets_ByCover_ForNonBuilding
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	select distinct
		ltrim(rtrim(dbo.ToCamelCase(Asset.[Description]))) [Description],
		cast(QuoteItem.SumInsured as numeric(18, 2)) [Value]
	from ProposalHeader
		inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
		inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber 
		inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
		inner join Product on Product.Id = Quote.ProductId
		inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
		inner join Organization on Organization.PartyId = Product.ProductOwnerId
	where
		ProposalHeader.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QuoteHeader.IsRerated = 0 and
		Quote.IsDeleted = 0 and
		QuoteItem.IsDeleted = 0 and
		Asset.IsDeleted = 0 and
		ProposalHeader.Id = @ProposalHeaderId and
		ProposalDefinition.CoverId = @CoverId and
		CoverDefinition.CoverId = @CoverId and
		Quote.Id in
		(
			select * from fn_StringListToTable(@QuoteIds)
		)
	group by
		Asset.[Description],
		QuoteItem.SumInsured
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Assets_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Assets_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_Assets_ByCover
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Building'))
		begin
			print 'Building'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding @ProposalHeaderId, @QuoteIds, @CoverId
		end

	if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Contents'))
		begin
			print 'Contents'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForContents @ProposalHeaderId, @QuoteIds, @CoverId
		end

	if(@CoverId in (select md.Cover.Id from md.Cover where md.Cover.Name <> 'Building' and md.Cover.Name <> 'Contents'))
		begin
			print 'Other'
			exec Reports_DetailedComparativeQuote_Assets_ByCover_ForNonBuilding  @ProposalHeaderId, @QuoteIds, @CoverId
		end
go



--Reports_DetailedComparativeQuote_Benefits_ByCover
if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Benefits_ByCover_ForBuilding') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Benefits_ByCover_ForBuilding
	end
go

create procedure Reports_DetailedComparativeQuote_Benefits_ByCover_ForBuilding
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	declare
		@Columns as nvarchar(max),
		@DefaultColumns as varchar(max),
		@Query as nvarchar(max),
		@AssetId int

	select @DefaultColumns = ''''' [Description], '''' [col_0], '''' [col_1], '''' [col_2]'

	declare summary_Cursor cursor fast_forward for
		select distinct
			Asset.Id
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
			inner join Asset on Asset.Id = ProposalDefinition.AssetId
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			ProposalHeader.IsDeleted = 0 and
			ProposalDefinition.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			Asset.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			ProposalDefinition.CoverId = @CoverId and
			CoverDefinition.CoverId = @CoverId and
			Quote.Id in
			(
				select * from fn_StringListToTable(@QuoteIds)
			)
		group by
			Asset.Id,
			ProposalHeader.Id

	open summary_Cursor
	fetch next from summary_Cursor into @AssetId

	while @@fetch_status = 0
		begin
			set @Columns =
				stuff
				(
					(
						select distinct
							',' + quotename(Organization.Code)
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = @ProposalHeaderId and
							ProposalDefinition.CoverId = @CoverId and
							CoverDefinition.CoverId = @CoverId and
							Quote.Id in
							(
								select * from fn_StringListToTable(@QuoteIds)
							)
					for xml path(''), type)
				.value('.', 'nvarchar(max)' ), 1, 1, '')

			if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 0
				begin
					select @Columns = @Columns + ', [col_1], [col_2]'
				end

			if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 1
				begin
					select @Columns = @Columns + ', [col_1]'
				end

			set @Query =
				'
					insert into #Breakdown (Col_1, Col_2, Col_3, Col_4)
					select
						' + @DefaultColumns + '

					union all 

					select
						Description,
						' + @Columns + '
					from
					(
						select
							Upper(Asset.Description) Description,
							Organization.Code Insurer
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(Insurer)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Premium'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.Premium as varchar) Premium
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(Premium)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Basic Excess'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.ExcessBasic as varchar) ExcessBasic
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(ExcessBasic)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''SASRIA'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.Sasria as varchar) SASRIA
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where	
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(SASRIA)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Total Payment'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast((QuoteItem.Premium + QuoteItem.Sasria) as varchar) TotalPayment
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(TotalPayment)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							ProductBenefit.Name Description,
							ProductBenefit.Value BenefitValue
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProductBenefit.Name is not null and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(BenefitValue)
						for Insurer in (' + @Columns + ')
					) p
			'

			execute(@query);

			fetch next from summary_Cursor into @AssetId
		end

	close summary_Cursor
	deallocate summary_Cursor
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Benefits_ByCover_ForContents') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Benefits_ByCover_ForContents
	end
go

create procedure Reports_DetailedComparativeQuote_Benefits_ByCover_ForContents
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	declare
		@Columns as nvarchar(max),
		@DefaultColumns as varchar(max),
		@Query as nvarchar(max),
		@AssetId int

	select @DefaultColumns = ''''' [Description], '''' [col_0], '''' [col_1], '''' [col_2]'

	declare summary_Cursor cursor fast_forward for
		select distinct
			Asset.Id
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
			inner join Asset on Asset.Id = ProposalDefinition.AssetId
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			ProposalHeader.IsDeleted = 0 and
			ProposalDefinition.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			Asset.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			ProposalDefinition.CoverId = @CoverId and
			CoverDefinition.CoverId = @CoverId and
			Quote.Id in
			(
				select * from fn_StringListToTable(@QuoteIds)
			)
		group by
			Asset.Id,
			ProposalHeader.Id

	open summary_Cursor
	fetch next from summary_Cursor into @AssetId

	while @@fetch_status = 0
		begin
			set @Columns =
				stuff
				(
					(
						select distinct
							',' + quotename(Organization.Code)
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = @ProposalHeaderId and
							ProposalDefinition.CoverId = @CoverId and
							CoverDefinition.CoverId = @CoverId and
							Quote.Id in
							(
								select * from fn_StringListToTable(@QuoteIds)
							)
					for xml path(''), type)
				.value('.', 'nvarchar(max)' ), 1, 1, '')

			if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 0
				begin
					select @Columns = @Columns + ', [col_1], [col_2]'
				end

			if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 1
				begin
					select @Columns = @Columns + ', [col_1]'
				end

			set @Query =
				'
					insert into #Breakdown (Col_1, Col_2, Col_3, Col_4)
					select
						' + @DefaultColumns + '

					union all 

					select
						Description,
						' + @Columns + '
					from
					(
						select
							Upper(Asset.Description) Description,
							Organization.Code Insurer
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(Insurer)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Premium'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.Premium as varchar) Premium
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(Premium)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Basic Excess'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.ExcessBasic as varchar) ExcessBasic
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(ExcessBasic)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''SASRIA'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.Sasria as varchar) SASRIA
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where	
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(SASRIA)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Total Payment'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast((QuoteItem.Premium + QuoteItem.Sasria) as varchar) TotalPayment
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(TotalPayment)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							ProductBenefit.Name Description,
							ProductBenefit.Value BenefitValue
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId and Asset.Id = ' + cast(@AssetId as nvarchar) + ' and Asset.IsDeleted = 0
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							ProductBenefit.Name is not null and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(BenefitValue)
						for Insurer in (' + @Columns + ')
					) p
			'

			execute(@query);

			fetch next from summary_Cursor into @AssetId
		end

	close summary_Cursor
	deallocate summary_Cursor
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Benefits_ByCover_ForNonBuilding') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Benefits_ByCover_ForNonBuilding
	end
go

create procedure Reports_DetailedComparativeQuote_Benefits_ByCover_ForNonBuilding
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	declare
		@Columns as nvarchar(max),
		@DefaultColumns as varchar(max),
		@Query as nvarchar(max),
		@AssetId int

	select @DefaultColumns = ''''' [Description], '''' [col_0], '''' [col_1], '''' [col_2]'

	declare summary_Cursor cursor fast_forward for
		select distinct
			Asset.Id
		from ProposalHeader
			inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
			inner join Asset on Asset.Id = ProposalDefinition.AssetId
			--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
		where
			ProposalHeader.IsDeleted = 0 and
			ProposalDefinition.IsDeleted = 0 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0 and
			Asset.IsDeleted = 0 and
			ProposalHeader.Id = @ProposalHeaderId and
			ProposalDefinition.CoverId = @CoverId and
			CoverDefinition.CoverId = @CoverId and
			Quote.Id in
			(
				select * from fn_StringListToTable(@QuoteIds)
			)
		group by
			Asset.Id

	open summary_Cursor
	fetch next from summary_Cursor into @AssetId

	while @@fetch_status = 0
		begin
			set @Columns =
				stuff
				(
					(
						select distinct
							',' + quotename(Organization.Code)
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
						where
							Asset.Id = @AssetId and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = @ProposalHeaderId and
							ProposalDefinition.CoverId = @CoverId and
							CoverDefinition.CoverId = @CoverId and
							Quote.Id in
							(
								select * from fn_StringListToTable(@QuoteIds)
							)
					for xml path(''), type)
				.value('.', 'nvarchar(max)' ), 1, 1, '')

			if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 0
				begin
					select @Columns = @Columns + ', [col_1], [col_2]'
				end

			if (select len(@Columns) - len(replace(@Columns, ',' ,''))) = 1
				begin
					select @Columns = @Columns + ', [col_1]'
				end

			set @Query =
				'
					insert into #Breakdown (Col_1, Col_2, Col_3, Col_4)
					select
						' + @DefaultColumns + '

					union all 

					select
						Description,
						' + @Columns + '
					from
					(
						select
							Upper(Asset.Description) Description,
							Organization.Code Insurer
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(Insurer)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Premium'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.Premium as varchar) Premium
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(Premium)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Basic Excess'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.ExcessBasic as varchar) ExcessBasic
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(ExcessBasic)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''SASRIA'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast(QuoteItem.Sasria as varchar) SASRIA
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(SASRIA)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						''Total Payment'' Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							cast((QuoteItem.Premium + QuoteItem.Sasria) as varchar) TotalPayment
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(TotalPayment)
						for Insurer in (' + @Columns + ')
					) p

					union all

					select
						Description,
						' + @Columns + '
					from
					(
						select
							Organization.Code Insurer,
							ProductBenefit.Name Description,
							ProductBenefit.Value BenefitValue
						from ProposalHeader
							inner join QuoteHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
							inner join Asset on Asset.Id = ProposalDefinition.AssetId
							--inner join Asset on Asset.AssetNo = QuoteItem.AssetNumber
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							left join ProductBenefit on ProductBenefit.CoverDefinitionId = CoverDefinition.Id
						where
							Asset.Id = ' + cast(@AssetId as nvarchar) + ' and
							ProductBenefit.Name is not null and
							ProposalHeader.IsDeleted = 0 and
							ProposalDefinition.IsDeleted = 0 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0 and
							Asset.IsDeleted = 0 and
							ProposalHeader.Id = ' + cast(@ProposalHeaderId as nvarchar) + ' and
							ProposalDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							CoverDefinition.CoverId = ' + cast(@CoverId as nvarchar) + ' and
							Quote.Id in
							(
								select * from fn_StringListToTable(''' + @QuoteIds + ''')
							)
					) x
					pivot 
					(
						max(BenefitValue)
						for Insurer in (' + @Columns + ')
					) p
			'

			execute(@query);

			fetch next from summary_Cursor into @AssetId
		end

	close summary_Cursor
	deallocate summary_Cursor
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_DetailedComparativeQuote_Benefits_ByCover') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_DetailedComparativeQuote_Benefits_ByCover
	end
go

create procedure Reports_DetailedComparativeQuote_Benefits_ByCover
(
	@ProposalHeaderId int,
	@QuoteIds varchar(max),
	@CoverId int
)
as
	create table #Breakdown
	(
		Col_1 varchar(255),
		Col_2 varchar(255),
		Col_3 varchar(255),
		Col_4 varchar(255)
	)

	if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Building'))
		begin
			print 'Building'
			exec Reports_DetailedComparativeQuote_Benefits_ByCover_ForBuilding @ProposalHeaderId, @QuoteIds, @CoverId
		end

	if(@CoverId = (select top 1 md.Cover.Id from md.Cover where md.Cover.Name = 'Contents'))
		begin
			print 'Contents'
			exec Reports_DetailedComparativeQuote_Benefits_ByCover_ForContents @ProposalHeaderId, @QuoteIds, @CoverId
		end

	if(@CoverId in (select md.Cover.Id from md.Cover where md.Cover.Name <> 'Buidling' and md.Cover.Name <> 'Contents'))
		begin
			print 'Other'
			exec Reports_DetailedComparativeQuote_Benefits_ByCover_ForNonBuilding @ProposalHeaderId, @QuoteIds, @CoverId
		end

	select * from #Breakdown
	drop table #Breakdown
go