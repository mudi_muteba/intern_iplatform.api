﻿---Report_CallCentre_InsurerQuoteBreakdown_Header
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerQuoteBreakdown_Header_ChannelExists') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerQuoteBreakdown_Header_ChannelExists
	end
go

create procedure Report_CallCentre_InsurerQuoteBreakdown_Header_ChannelExists
(
	@CampaignID int
)
as
	select top 1
		Code
		, ''
	from Channel
		inner join Campaign on Campaign.ChannelId = Channel.Id
	where
		Campaign.Id = @CampaignId and
		Campaign.IsDeleted = 0 and
		Channel.IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerQuoteBreakdown_Header_ChannelDoesNotExist') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerQuoteBreakdown_Header_ChannelDoesNotExist
	end
go

create procedure Report_CallCentre_InsurerQuoteBreakdown_Header_ChannelDoesNotExist
(
	@ChannelId int
)
as
	select top 1
		Code
		, ''
	from Channel
	where
		Id = @ChannelId and
		IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerQuoteBreakdown_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerQuoteBreakdown_Header
	end
go

create procedure Report_CallCentre_InsurerQuoteBreakdown_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			exec Report_CallCentre_InsurerQuoteBreakdown_Header_ChannelExists @CampaignID
		end
	else
		begin
			exec Report_CallCentre_InsurerQuoteBreakdown_Header_ChannelDoesNotExist @IpChannelId
		end
go



--Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics_WithCampaignIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics_WithCampaignIds
	end
go

create procedure Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics_WithCampaignIds
(
	@CampaignIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign, ProductId)
	as 
	(
		select distinct
			Organization.PartyId,
			Campaign.Id,
			Organization.RegisteredName,
			Product.Name,
			Campaign.Name,
			Product.Id
		from QuoteHeader
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
			inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
			inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			inner join Channel on Channel.Id = Campaign.ChannelId
			inner join [User] on [User].Id = LeadActivity.UserId
			inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
			inner join UserIndividual on UserIndividual.UserId = [User].Id
			inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		where
			LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			--UserAuthorisationGroup.AuthorisationGroupId = 1 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0
		group by
			Campaign.Id,
			Campaign.Name,
			Organization.PartyId,
			Organization.RegisteredName,
			Product.Id,
			Product.Name
		)
	select
		Insurer,
		Product,
		Campaign,
		(
			select
				isnull(count(distinct(Quote.Id)), 0)
			from LeadActivity							
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			where
				LeadActivity.CampaignId = Insurers_CTE.CampaignId and
				Organization.PartyId = Insurers_CTE.InsurerId and
				Product.ID = Insurers_CTE.ProductId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) Quotes,
		(
			select
				isnull(count(distinct(Quote.Id)), 0)
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			where
				exists
				(
					select top 1 QuoteUploadLog.QuoteId from QuoteUploadLog where QuoteUploadLog.QuoteId = Quote.Id order by DateCreated desc
				) and
				LeadActivity.CampaignId = Insurers_CTE.CampaignId and
				Organization.PartyId = Insurers_CTE.InsurerId and
				Product.ID = Insurers_CTE.ProductId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) Sales,
		(
			cast
			(
				(
					( 
						select
							cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric(18, 0))
						from LeadActivity
							inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							exists
							(
								select top 1 QuoteUploadLog.QuoteId from QuoteUploadLog where QuoteUploadLog.QuoteId = Quote.Id order by DateCreated desc
							) and
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.ID = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)

					/

					(
						select
							nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
						from LeadActivity
							inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.ID = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)
				)
				* 100
				as numeric( 18, 2)
			)
		) Closing,
		(
			select
				cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
			from
			(
				select
					cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
				from LeadActivity
					inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
					inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
					inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
					inner join Product on Product.Id = Quote.ProductId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				where
					exists
					(
						select top 1 QuoteUploadLog.QuoteId from QuoteUploadLog where QuoteUploadLog.QuoteId = Quote.Id order by DateCreated desc
					) and
					LeadActivity.CampaignId = Insurers_CTE.CampaignId and
					Organization.PartyId = Insurers_CTE.InsurerId and
					Product.ID = Insurers_CTE.ProductId and
					LeadActivity.DateUpdated >= @DateFrom and
					LeadActivity.DateUpdated <= @DateTo and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Quote.Id
			) x
		) AveragePremiumPerAcceptedQuote,
		(
			select
				cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
			from
			(
				select
					cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Product on Product.Id = Quote.ProductId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				where
					LeadActivity.CampaignId = Insurers_CTE.CampaignId and
					Organization.PartyId = Insurers_CTE.InsurerId and
					Product.ID = Insurers_CTE.ProductId and
					LeadActivity.DateUpdated >= @DateFrom and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Quote.Id
			) x
		) AveragePremiumPerQuote
	from
		Insurers_CTE
	order by
		Campaign,
		Insurer,
		Product
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics_WithoutCampaignIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics_WithoutCampaignIds
	end
go

create procedure Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics_WithoutCampaignIds
(
	@DateFrom datetime,
	@DateTo datetime
)
as
	;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign, ProductId)
	as 
	(
		select distinct
			Organization.PartyId,
			Campaign.Id,
			Organization.RegisteredName,
			Product.Name,
			Campaign.Name,
			Product.Id
		from QuoteHeader
			inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
			inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
			inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
			inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			inner join Product on Product.Id = Quote.ProductId
			inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
			inner join Organization on Organization.PartyId = Product.ProductOwnerId
			inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
			inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
			inner join Campaign on Campaign.Id = LeadActivity.CampaignId
			inner join Channel on Channel.Id = Campaign.ChannelId
			inner join [User] on [User].Id = LeadActivity.UserId
			inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
			inner join UserIndividual on UserIndividual.UserId = [User].Id
			inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		where
			--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
			--UserAuthorisationGroup.AuthorisationGroupId = 1 and
			QuoteHeader.IsRerated = 0 and
			Quote.IsDeleted = 0 and
			QuoteItem.IsDeleted = 0
		group by
			Campaign.Id,
			Campaign.Name,
			Organization.PartyId,
			Organization.RegisteredName,
			Product.Id,
			Product.Name
		)
	select
		Insurer,
		Product,
		Campaign,
		(
			select
				isnull(count(distinct(Quote.Id)), 0)
			from LeadActivity							
				inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
				inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			where
				LeadActivity.CampaignId = Insurers_CTE.CampaignId and
				Organization.PartyId = Insurers_CTE.InsurerId and
				Product.ID = Insurers_CTE.ProductId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) Quotes,
		(
			select
				isnull(count(distinct(Quote.Id)), 0)
			from LeadActivity
				inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
				inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join Product on Product.Id = Quote.ProductId
				inner join Organization on Organization.PartyId = Product.ProductOwnerId
				inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
				inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
				inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
			where
				exists
				(
					select top 1 QuoteUploadLog.QuoteId from QuoteUploadLog where QuoteUploadLog.QuoteId = Quote.Id order by DateCreated desc
				) and
				LeadActivity.CampaignId = Insurers_CTE.CampaignId and
				Organization.PartyId = Insurers_CTE.InsurerId and
				Product.ID = Insurers_CTE.ProductId and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				QuoteHeader.IsRerated = 0 and
				Quote.IsDeleted = 0 and
				QuoteItem.IsDeleted = 0
		) Sales,
		(
			cast
			(
				(
					( 
						select
							cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric(18, 0))
						from LeadActivity
							inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							exists
							(
								select top 1 QuoteUploadLog.QuoteId from QuoteUploadLog where QuoteUploadLog.QuoteId = Quote.Id order by DateCreated desc
							) and
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.ID = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)

					/

					(
						select
							nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
						from LeadActivity
							inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
							inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.ID = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
					)
				)
				* 100
				as numeric( 18, 2)
			)
		) Closing,
		(
			select
				cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
			from
			(
				select
					cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
				from LeadActivity
					inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
					inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
					inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
					inner join Product on Product.Id = Quote.ProductId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				where
					exists
					(
						select top 1 QuoteUploadLog.QuoteId from QuoteUploadLog where QuoteUploadLog.QuoteId = Quote.Id order by DateCreated desc
					) and
					LeadActivity.CampaignId = Insurers_CTE.CampaignId and
					Organization.PartyId = Insurers_CTE.InsurerId and
					Product.ID = Insurers_CTE.ProductId and
					LeadActivity.DateUpdated >= @DateFrom and
					LeadActivity.DateUpdated <= @DateTo and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Quote.Id
			) x
		) AveragePremiumPerAcceptedQuote,
		(
			select
				cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
			from
			(
				select
					cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Product on Product.Id = Quote.ProductId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
				where
					LeadActivity.CampaignId = Insurers_CTE.CampaignId and
					Organization.PartyId = Insurers_CTE.InsurerId and
					Product.ID = Insurers_CTE.ProductId and
					LeadActivity.DateUpdated >= @DateFrom and
					LeadActivity.DateUpdated <= @DateTo and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Quote.Id
			) x
		) AveragePremiumPerQuote
	from
		Insurers_CTE
	order by
		Campaign,
		Insurer,
		Product
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics
	end
go

create procedure Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics
(
	@CampaignIds varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			exec Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics_WithCampaignIds @CampaignIds, @DateFrom, @DateTo
		end
	else
		begin
			exec Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics_WithoutCampaignIds @DateFrom, @DateTo
		end
go