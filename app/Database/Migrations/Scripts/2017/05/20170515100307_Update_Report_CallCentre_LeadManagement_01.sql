﻿--Report_CallCentre_LeadManagement_Header
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_LeadManagement_Header_ChannelExists') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_LeadManagement_Header_ChannelExists
	end
go

create procedure Report_CallCentre_LeadManagement_Header_ChannelExists
(
	@CampaignID int
)
as
	select top 1
		Code
		, ''
	from Channel
		inner join Campaign on Campaign.ChannelId = Channel.Id
	where
		Campaign.Id = @CampaignId and
		Campaign.IsDeleted = 0 and
		Channel.IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_LeadManagement_Header_ChannelDoesNotExist') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_LeadManagement_Header_ChannelDoesNotExist
	end
go

create procedure Report_CallCentre_LeadManagement_Header_ChannelDoesNotExist
(
	@ChannelId int
)
as
	select top 1
		Code
		, ''
	from Channel
	where
		Id = @ChannelId and
		IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_LeadManagement_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_LeadManagement_Header
	end
go

create procedure Report_CallCentre_LeadManagement_Header
(
	@CampaignID int
)
as
	declare @IpChannelId int

	select top 1
		@IpChannelId = Channel.Id
	from Channel
	where
		Channel.Id = (select top 1 Id from Channel where Code = 'IP')

	if ((select top 1 count(Channel.Id) from Channel inner join Campaign on Campaign.ChannelId = Channel.Id where Campaign.Id = @CampaignId and Campaign.IsDeleted = 0 and Channel.IsDeleted = 0) > 0)
		begin
			exec Report_CallCentre_LeadManagement_Header_ChannelExists @CampaignID
		end
	else
		begin
			exec Report_CallCentre_LeadManagement_Header_ChannelDoesNotExist @IpChannelId
		end
go



--Report_CallCentre_LeadManagement_GetAgentLeadStatistics
if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_LeadManagement_GetAgentLeadStatistics_WithCampaignIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_LeadManagement_GetAgentLeadStatistics_WithCampaignIds
	end
go

create procedure Report_CallCentre_LeadManagement_GetAgentLeadStatistics_WithCampaignIds
(
	@CampaignIDs varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
	as 
	(
		select distinct
			[User].Id,
			Campaign.Id,
			Individual.Surname + ', ' + Individual.FirstName,
			Campaign.Name
		from Campaign
			inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
			inner join [User] on [User].Id = CampaignLeadBucket.AgentId
			inner join UserIndividual on UserIndividual.UserId = [User].Id
			inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		where
			Campaign.Id in (select * from fn_StringListToTable(@CampaignIds)) and
			CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo
		group by
			Campaign.Id,
			Campaign.Name,
			[User].Id,
			Individual.PartyID,
			Individual.Surname,
			Individual.FirstName,
			Campaign.Name
		)
	select
		Agent,
		Campaign,
		(
			select
				isnull(count(distinct(CampaignLeadBucket.LeadId)), 0)
			from CampaignLeadBucket
			where
				CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
				CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
				CampaignLeadBucket.IsDeleted = 0 and
				AgentId = Agents_CTE.AgentId
		) Received,
		(
			select
				isnull(count(CampaignLeadBucket.Id), 0)
			from CampaignLeadBucket
			where
				CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
				CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
				CampaignLeadBucket.IsDeleted = 0 and
				AgentId = Agents_CTE.AgentId and
				LeadCallCentreCodeId in (5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 23, 24, 25, 26, 27, 28, 29, 30, 31)
		) Contacted,
		(
			select
				isnull(count(CampaignLeadBucket.Id), 0)
			from CampaignLeadBucket
			where
				CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
				CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
				CampaignLeadBucket.IsDeleted = 0 and
				AgentId = Agents_CTE.AgentId and
				LeadCallCentreCodeId in (15, 16, 17, 18, 19, 20, 21, 22)
		) Uncontactable,
		(
			select
				isnull(count(CampaignLeadBucket.Id), 0)
			from CampaignLeadBucket
			where
				CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
				CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
				CampaignLeadBucket.IsDeleted = 0 and
				AgentId = Agents_CTE.AgentId and
				LeadCallCentreCodeId in (7, 10, 9, 11, 20, 21)
		) Pending,
		(
			select
				isnull(count(CampaignLeadBucket.Id), 0)
			from CampaignLeadBucket
			where
				CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
				CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
				CampaignLeadBucket.IsDeleted = 0 and
				AgentId = Agents_CTE.AgentId and
				LeadCallCentreCodeId in (5)
		) Completed,
		(
			select
				isnull(count(distinct(CampaignLeadBucket.LeadId)), 0)
			from CampaignLeadBucket
			where
				CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
				CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
				AgentId = Agents_CTE.AgentId and
				LeadCallCentreCodeId in (2)
		) Unactioned
	from
		Agents_CTE
	order by
		Agent asc,
		Campaign Asc
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_LeadManagement_GetAgentLeadStatistics_WithoutCampaignIds') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_LeadManagement_GetAgentLeadStatistics_WithoutCampaignIds
	end
go

create procedure Report_CallCentre_LeadManagement_GetAgentLeadStatistics_WithoutCampaignIds
(
	@DateFrom datetime,
	@DateTo datetime
)
as
	;with Agents_CTE (AgentId, CampaignId, Agent, Campaign)
	as 
	(
		select distinct
			[User].Id,
			Campaign.Id,
			Individual.Surname + ', ' + Individual.FirstName,
			Campaign.Name
		from Campaign
			inner join CampaignLeadBucket on CampaignLeadBucket.CampaignId = Campaign.Id
			inner join [User] on [User].Id = CampaignLeadBucket.AgentId
			inner join UserIndividual on UserIndividual.UserId = [User].Id
			inner join Individual on Individual.PartyId = UserIndividual.IndividualId
		where
			CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo
		group by
			Campaign.Id,
			Campaign.Name,
			[User].Id,
			Individual.PartyID,
			Individual.Surname,
			Individual.FirstName,
			Campaign.Name
		)
	select
		Agent,
		Campaign,
		(
			select
				isnull(count(distinct(CampaignLeadBucket.LeadId)), 0)
			from CampaignLeadBucket
			where
				CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
				CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
				CampaignLeadBucket.IsDeleted = 0 and
				AgentId = Agents_CTE.AgentId
		) Received,
		(
			select
				isnull(count(CampaignLeadBucket.Id), 0)
			from CampaignLeadBucket
			where
				CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
				CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
				CampaignLeadBucket.IsDeleted = 0 and
				AgentId = Agents_CTE.AgentId and
				LeadCallCentreCodeId in (5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 23, 24, 25, 26, 27, 28, 29, 30, 31)
		) Contacted,
		(
			select
				isnull(count(CampaignLeadBucket.Id), 0)
			from CampaignLeadBucket
			where
				CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
				CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
				CampaignLeadBucket.IsDeleted = 0 and
				AgentId = Agents_CTE.AgentId and
				LeadCallCentreCodeId in (15, 16, 17, 18, 19, 20, 21, 22)
		) Uncontactable,
		(
			select
				isnull(count(CampaignLeadBucket.Id), 0)
			from CampaignLeadBucket
			where
				CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
				CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
				CampaignLeadBucket.IsDeleted = 0 and
				AgentId = Agents_CTE.AgentId and
				LeadCallCentreCodeId in (7, 10, 9, 11, 20, 21)
		) Pending,
		(
			select
				isnull(count(CampaignLeadBucket.Id), 0)
			from CampaignLeadBucket
			where
				CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
				CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
				CampaignLeadBucket.IsDeleted = 0 and
				AgentId = Agents_CTE.AgentId and
				LeadCallCentreCodeId in (5)
		) Completed,
		(
			select
				isnull(count(distinct(CampaignLeadBucket.LeadId)), 0)
			from CampaignLeadBucket
			where
				CampaignLeadBucket.CampaignId = Agents_CTE.CampaignId and
				CampaignLeadBucket.CreatedOn between @DateFrom and @DateTo and
				CampaignLeadBucket.IsDeleted = 0 and
				AgentId = Agents_CTE.AgentId and
				LeadCallCentreCodeId in (2)
		) Unactioned
	from
		Agents_CTE
	order by
		Agent asc,
		Campaign Asc
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_LeadManagement_GetAgentLeadStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_LeadManagement_GetAgentLeadStatistics
	end
go

create procedure Report_CallCentre_LeadManagement_GetAgentLeadStatistics
(
	@CampaignIDs varchar(max),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			exec Report_CallCentre_LeadManagement_GetAgentLeadStatistics_WithCampaignIds @CampaignIDs, @DateFrom, @DateTo
		end
	else
		begin
			exec Report_CallCentre_LeadManagement_GetAgentLeadStatistics_WithoutCampaignIds @DateFrom, @DateTo
		end
go