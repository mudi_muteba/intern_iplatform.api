﻿
ALTER VIEW dbo.vw_rating_configuration 
AS  
SELECT        r.Id, c.Id AS ChannelId, c.SystemId AS ChannelSystemId, r.ProductId, r.Password AS RatingPassword, r.AgentCode AS RatingAgentCode, r.BrokerCode AS RatingBrokerCode, r.AuthCode AS RatingAuthCode, 
                         r.SchemeCode AS RatingSchemeCode, r.Token AS RatingToken, r.Environment AS RatingEnvironment, r.UserId AS RatingUserId, r.SubscriberCode AS RatingSubscriberCode, r.CompanyCode AS RatingCompanyCode, 
                         r.UwCompanyCode AS RatingUwCompanyCode, r.UwProductCode AS RatingUwProductCode, p.CheckRequired AS ITCCheckRequired, p.SubscriberCode AS ITCSubscriberCode, p.BranchNumber AS ITCBranchNumber, 
                         p.BatchNumber AS ITCBatchNumber, p.SecurityCode AS ITCSecurityCode, p.EnquirerContactName AS ITCEnquirerContactName, p.EnquirerContactPhoneNo AS ITCEnquirerContactPhoneNo, et.[Key] AS ITCEnvironment, 
                         o.Code AS InsurerCode, r.ProductCode, r.IsDeleted
FROM            dbo.SettingsiRate AS r INNER JOIN
                         dbo.Channel AS c ON r.ChannelId = c.Id AND c.IsDeleted = 0 INNER JOIN
                         dbo.Product AS product ON r.ProductId = product.Id AND product.IsDeleted = 0 INNER JOIN
                         dbo.Organization AS o ON product.ProductOwnerId = o.PartyId LEFT OUTER JOIN
                         dbo.SettingsiPerson AS p ON c.Id = p.ChannelId AND p.IsDeleted = 0 LEFT OUTER JOIN
                         md.EnvironmentType AS et ON et.Id = p.EnvironmentTypeId
WHERE        (r.IsDeleted = 0) AND (c.IsActive = 1)
GO