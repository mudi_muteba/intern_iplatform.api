﻿DECLARE @userId int;

SELECT @userId = Id FROM [User] WHERE UserName = 'customapp@iplatform.co.za'
if(@userId > 0)
BEGIN
	INSERT INTO UserAuthorisationGroup VALUES (5, @userId,1,0)
END

--Generic
INSERT INTO OrganizationClaimTypeCategory VALUES 
(44, 2, 15, 0, null, 'Motor', 'Motor Fire and Explosion'),
(45, 2, 16, 0, null, 'Motor', 'Motor Weather and Elements'),
(46, 1, 11, 0, null, 'All', 'Earthquake'),
(47, 1, 12, 0, null, 'All', 'Fire/Explosion'),
(48, 2, 8, 0, null, 'Building', 'Geyser / Pipes and Related Damage'),
(49, 1, 13, 0, null, 'All', 'Subsidence / Landslip'),
(50, 1, 14, 0, null, 'All', 'Weather / Storm'),
(51, 1, 15, 0, null, 'All', 'Other')
