﻿
DECLARE @code nvarchar(50)
DECLARE @Name nvarchar(50)
DECLARE @Usergroupid int
DECLARE @channelId int
DECLARE @productId INT;
DECLARE @SystemId UNIQUEIDENTIFIER;

SET @code = 'SMART'
SET @Name = 'SMARTUK'
SET @Usergroupid = 5
SET @SystemId = '3fd80dae-c596-4323-806b-061233909090';
	

IF NOT EXISTS (SELECT * FROM Channel WHERE SystemId = @SystemId)
	BEGIN
	
		insert into Channel ( SystemId, CountryId, CurrencyId, LanguageId, OrganizationId, ExternalReference, Name, Code, [DateFormat], PasswordStrengthEnabled, ActivatedOn, DeactivatedOn, IsDefault, IsActive, IsDeleted)
		values ( @SystemId, 197, 1, 2, null, @code, @name, @code, 'dd MMM yyyy', 0, getdate(), null, 0, 1, 0)

		SELECT @channelId = Id FROM Channel where code = @code
		PRINT (@channelId)

		DECLARE @UserId INT
		SELECT @UserId = Id FROM [User] WHERE UserName = 'root@iplatform.co.za'
		EXEC sp_executesql N'INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3); select SCOPE_IDENTITY()',N'@p0 int,@p1 bit,@p2 int,@p3 int',@p0=@Usergroupid,@p1=0,@p2=@UserId,@p3=@channelId
		EXEC sp_executesql N'INSERT INTO UserChannel (CreatedAt, ModifiedAt, IsDefault, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3, @p4, @p5); select SCOPE_IDENTITY()',N'@p0 datetime,@p1 datetime,@p2 bit,@p3 bit,@p4 int,@p5 int',@p0='2016-11-04 11:01:55',@p1='2016-11-04 11:01:55',@p2=0,@p3=0,@p4=@UserId,@p5=@channelId
			
		SELECT @UserId = Id FROM [User] WHERE UserName = 'admin@iplatform.co.za'
		EXEC sp_executesql N'INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3); select SCOPE_IDENTITY()',N'@p0 int,@p1 bit,@p2 int,@p3 int',@p0=@Usergroupid,@p1=0,@p2=@UserId,@p3=@channelId
		EXEC sp_executesql N'INSERT INTO UserChannel (CreatedAt, ModifiedAt, IsDefault, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3, @p4, @p5); select SCOPE_IDENTITY()',N'@p0 datetime,@p1 datetime,@p2 bit,@p3 bit,@p4 int,@p5 int',@p0='2016-11-04 11:01:55',@p1='2016-11-04 11:01:55',@p2=0,@p3=0,@p4=@UserId,@p5=@channelId

	END

	
IF (@channelId IS NOT null)
IF NOT EXISTS (SELECT * FROM [PersonLookupSetting] WHERE [ChannelId] = @channelId)
	INSERT [dbo].[PersonLookupSetting] ([IsDeleted], [ChannelId], [CountryId], [ApiKey], [Email], 
		[InvoiceReference], [PCubedEnabled], [PCubedUsername], [PCubedPassword], [IPRSEnabled], [IPRSUsername], 
		[IPRSPassword], [LightStonePropertyEnabled], [LightStonePropertyUserId], [TransunionEnabled], [TransunionFirstName], 
		[TransunionSurname], [TransunionSubscriberCode], [TransunionSecurityCode], [TransUnionEndPoint], [TransUnionGetClaims], 
		[TransUnionGetDrivers], 
		[CompanyInformationUsername], [CompanyInformationPassword])
	VALUES (
		0,
		@channelId, -- channel ID
		225, --Great Britan
		N'3BQmEDTa3FoZRXALMY82y38qXQqrpgcJPlO4kUmXP8Zab9siaNnG1X__41YpN3mn3eK6I72tdLN0DzPpYIRbu6jsmOASwyJnndGxZW9Dmv4QKax6M90FLKtywSkv6Jxf',
		N'iperson-root@iplatform.co.za',
		NULL,
		1,
		N'CardinalDemoNonCPA',
		N'ERdfg45YqPzz',
		0,
		NULL,
		NULL,
		0,
		NULL,
		0,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		0,
		0,
		'0R6gGfopF1T4J7wbbJhoL1aRLkhbEWQhk_GTTBd0',
		NULL)
