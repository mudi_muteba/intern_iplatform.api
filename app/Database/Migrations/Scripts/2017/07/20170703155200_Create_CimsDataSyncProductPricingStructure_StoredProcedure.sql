﻿IF EXISTS 
(
	SELECT * 
	FROM sys.objects AS O
	WHERE O.[type] = 'P'
		AND O.[object_id] = OBJECT_ID('dbo.CimsDataSyncProductPricingStructure')
)
BEGIN
	DROP PROCEDURE dbo.CimsDataSyncProductPricingStructure;
END
GO

CREATE PROC dbo.CimsDataSyncProductPricingStructure
(
	@DatabaseName VARCHAR(128)
)
AS
SET NOCOUNT ON;
DECLARE @rowCount INTEGER;
DECLARE @SQLExec NVARCHAR(MAX);
SET @SQLExec = '';

CREATE TABLE #CoverMap
(
	Cover_iPlat NVARCHAR(255)
	,CoverCode NVARCHAR(6)
	,CoverGUID UNIQUEIDENTIFIER
	,CoverType NVARCHAR(500)
);
CREATE TABLE #ProductPricingStructure
(
	ProductCodeIplatform NVARCHAR(255)
	,ProductId INT DEFAULT 0
	,CoverGUID UNIQUEIDENTIFIER
	,CoverId INT DEFAULT 0
	,StartDate DATETIME
	,EndDate DATETIME
	,ExternalReference NVARCHAR(50)
	,ChannelId INT DEFAULT 0
	,Monthly1YearDefault DECIMAL(18, 5)
	,Monthly1YearMin DECIMAL(18, 5)
	,Monthly1YearMax DECIMAL(18, 5)
	,Monthly2YearDefault DECIMAL(18, 5)
	,Monthly2YearMin DECIMAL(18, 5)
	,Monthly2YearMax DECIMAL(18, 5)
	,Monthly3YearDefault DECIMAL(18, 5)
	,Monthly3YearMin DECIMAL(18, 5)
	,Monthly3YearMax DECIMAL(18, 5)
	,Annual1YearDefault DECIMAL(18, 5)
	,Annual1YearMin DECIMAL(18, 5)
	,Annual1YearMax DECIMAL(18, 5)
	,Annual2YearDefault DECIMAL(18, 5)
	,Annual2YearMin DECIMAL(18, 5)
	,Annual2YearMax DECIMAL(18, 5)
	,Annual3YearDefault DECIMAL(18, 5)
	,Annual3YearMin DECIMAL(18, 5)
	,Annual3YearMax DECIMAL(18, 5)
	,IsDeleted BIT
);
INSERT  #CoverMap (Cover_iPlat, CoverCode, CoverGUID, CoverType)
		SELECT  'BUILDING', 'HOUOWN', '00000000-60C5-4B46-94B1-29D4F8FD284D', 'Houseowners'
		UNION ALL
		SELECT  'ALL RISK', 'ARSPEC', '00000000-222A-4EC7-BE9D-4E11A36701F3', 'All Risks'
		UNION ALL
		SELECT  'PERSONAL LEGAL LIABILITY', 'PERLIA', '00000000-FC59-4F47-BE69-55A702AD9E25', 'Personal Liability'
		UNION ALL
		SELECT  'PERSONAL ACCIDENT', 'PERACC', '00000000-BA38-449D-A51C-564DDC56DFE0', 'Personal Accident'
		UNION ALL
		SELECT  'CONTENTS', 'HOUHLD', '00000000-C84E-4D84-BDB3-6013F0E9D43B', 'Householders'
		UNION ALL
		SELECT  'DISASTER CASH', 'DISCAS', '00000000-B9E7-4564-8B4C-857A2E5A36E4', 'Disaster Cash'
		UNION ALL
		SELECT  'MOTOR', 'MOTORA', '00000000-DBF4-4E3A-8696-9A5443085944', 'Motor'
		UNION ALL
		SELECT  'CARAVAN OR TRAILER', 'CARAVA', '00000000-27E7-4F7C-85DB-A4B2FB368E79', 'Caravan'
		UNION ALL
		SELECT  'FUNERAL BENEFITS', 'FUNBEN', '00000000-D638-4AF4-8167-C870C5503F2B', 'Funeral Benefits'
		UNION ALL
		SELECT  'FUNERAL COSTS ADD DEATH BENEFITS', 'FUNBEN', '00000000-D638-4AF4-8167-C870C5503F2B', 'Funeral Benefits'
		UNION ALL
		SELECT  'IDENTITY THEFT', 'IDTHFT', '00000000-69E3-4DDC-BCB2-D0743180EEC5', 'Identity Theft'
		UNION ALL
		SELECT  'Touch Up', 'VAPSAD', '00000000-F8CE-45DE-B72D-0BD47B4F4E49', 'Scratch and Dent';

SET @SQLExec = 'INSERT #ProductPricingStructure (ProductCodeIplatform, CoverGUID, StartDate, EndDate, ExternalReference, Monthly1YearDefault,
									Monthly1YearMin, Monthly1YearMax, Monthly2YearDefault, Monthly2YearMin, Monthly2YearMax, Monthly3YearDefault,
									Monthly3YearMin, Monthly3YearMax, Annual1YearDefault, Annual1YearMin, Annual1YearMax, Annual2YearDefault, Annual2YearMin,
									Annual2YearMax, Annual3YearDefault, Annual3YearMin, Annual3YearMax, IsDeleted)
SELECT  pm.ProductCodeIplatform, pps.CoverGUID, pps.StartDate, pps.EndDate, COALESCE(br.ExternalReference, b.ExternalReference, '''') ExternalReference,
		pps.Monthly1YearDefault, pps.Monthly1YearMin, pps.Monthly1YearMax, pps.Monthly2YearDefault, pps.Monthly2YearMin, pps.Monthly2YearMax,
		pps.Monthly3YearDefault, pps.Monthly3YearMin, pps.Monthly3YearMax, pps.Annual1YearDefault, pps.Annual1YearMin, pps.Annual1YearMax,
		pps.Annual2YearDefault, pps.Annual2YearMin, pps.Annual2YearMax, pps.Annual3YearDefault, pps.Annual3YearMin, pps.Annual3YearMax, pps.Deleted
FROM    ' + @DatabaseName + '.dbo.ProductPricingStructure pps
		JOIN ' + @DatabaseName + '.dbo.PB_ProductMaster pm ON pm.ProductGUID = pps.ProductGUID
		LEFT JOIN ' + @DatabaseName + '.dbo.EntityDetails b ON b.EntityGUID = pps.BCodeGUID
		LEFT JOIN ' + @DatabaseName + '.dbo.EntityDetails br ON br.EntityGUID = pps.BCodeBranchGUID
WHERE   pps.Deleted = 0;'

--		Get Cims3 Data
EXEC sys.sp_sqlexec @p1 = @SQLExec;

UPDATE  ps
SET     ps.CoverId = c.Id
FROM    #ProductPricingStructure ps
		JOIN #CoverMap cm ON cm.CoverGUID = ps.CoverGUID
		JOIN md.Cover c ON cm.Cover_iPlat = c.Name COLLATE SQL_Latin1_General_CP1_CI_AS;

UPDATE  ps
SET     ps.ChannelId = c.Id
FROM    #ProductPricingStructure ps
		JOIN dbo.Channel c ON c.ExternalReference COLLATE SQL_Latin1_General_CP1_CI_AS = ps.ExternalReference;

UPDATE  ps
SET     ps.ProductId = p.Id
FROM    #ProductPricingStructure ps
		JOIN dbo.Product p ON p.ProductCode COLLATE SQL_Latin1_General_CP1_CI_AS = ps.ProductCodeIplatform;

		 
DELETE  ps
FROM    dbo.ProductPricingStructure ps
WHERE   ps.ProductId IN (SELECT pps.ProductId
							FROM   #ProductPricingStructure pps)
		AND ps.ProductId != 0;


INSERT  dbo.ProductPricingStructure (ProductId, CoverId, StartDate, EndDate, ChannelId, Monthly1YearDefault, Monthly1YearMin, Monthly1YearMax,
										Monthly2YearDefault, Monthly2YearMin, Monthly2YearMax, Monthly3YearDefault, Monthly3YearMin, Monthly3YearMax,
										Annual1YearDefault, Annual1YearMin, Annual1YearMax, Annual2YearDefault, Annual2YearMin, Annual2YearMax, Annual3YearDefault,
										Annual3YearMin, Annual3YearMax)
SELECT  pps.ProductId, pps.CoverId, pps.StartDate, pps.EndDate, pps.ChannelId, pps.Monthly1YearDefault, pps.Monthly1YearMin, pps.Monthly1YearMax,
		pps.Monthly2YearDefault, pps.Monthly2YearMin, pps.Monthly2YearMax, pps.Monthly3YearDefault, pps.Monthly3YearMin, pps.Monthly3YearMax,
		pps.Annual1YearDefault, pps.Annual1YearMin, pps.Annual1YearMax, pps.Annual2YearDefault, pps.Annual2YearMin, pps.Annual2YearMax, pps.Annual3YearDefault,
		pps.Annual3YearMin, pps.Annual3YearMax
FROM    #ProductPricingStructure pps
WHERE   pps.ProductId != 0
		AND pps.CoverId != 0
		AND pps.ChannelId != 0;
				
--Return results
SET @rowCount = @@ROWCOUNT
SELECT 
	ISNULL(@rowCount, 0) AS RowsAffected,
	CASE WHEN ISNULL(@rowCount, 0) > 0
		THEN 'Successfully imported latest product pricing structure.'
		ELSE 'No new records were synced.'
	END AS [Message];

GO