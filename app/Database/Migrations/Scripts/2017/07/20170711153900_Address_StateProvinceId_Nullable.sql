﻿IF EXISTS
(
	SELECT * 
	FROM SYS.COLUMNS AS C
	WHERE C.object_id = OBJECT_ID('dbo.Address') 
		AND C.name = 'StateProvinceId'
		AND C.is_nullable = 0
)
BEGIN
	ALTER TABLE [Address] ALTER COLUMN StateProvinceId INT NULL
END