﻿IF NOT EXISTS (SELECT * FROM CoverDefinition where ProductID = 37 AND CoverId = 17)
INSERT INTO CoverDefinition (MasterId, DisplayName, ProductId, CoverId, ParentId, CoverDefinitionTypeId, VisibleIndex, IsDeleted)
VALUES (0,	'All Risks',	37,	17,	NULL,	1,	0,	0)

IF NOT EXISTS (SELECT * FROM CoverDefinition where ProductID = 37 AND CoverId = 44)
INSERT INTO CoverDefinition (MasterId, DisplayName, ProductId, CoverId, ParentId, CoverDefinitionTypeId, VisibleIndex, IsDeleted)
VALUES (0,	'House Owners',	37,	44,	NULL,	1,	0,	0)

IF NOT EXISTS (SELECT * FROM CoverDefinition where ProductID = 37 AND CoverId = 78)
INSERT INTO CoverDefinition (MasterId, DisplayName, ProductId, CoverId, ParentId, CoverDefinitionTypeId, VisibleIndex, IsDeleted)
VALUES (0,	'Household Contents',	37,	78,	NULL,	1,	0,	0)