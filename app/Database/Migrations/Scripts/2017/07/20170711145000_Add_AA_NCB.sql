﻿DECLARE @MotorCoverDefinitionId INT;

SELECT @MotorCoverDefinitionId = cd.Id
FROM CoverDefinition cd
INNER JOIN Product pd ON pd.Id = cd.ProductId
INNER JOIN md.Cover mdc ON mdc.Id = cd.CoverId
WHERE pd.ProductCode = 'AAC'
AND mdc.Code = 'MOTOR';

DECLARE @ParentCoverDefinitionId INT;
DECLARE @ChildCoverDefinitionId INT;
DECLARE @DummyQuestionDefId INT;
DECLARE @ProductId INT;

SET @ProductId = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode = 'AAC' )

select @ParentCoverDefinitionId = id from CoverDefinition where ProductId = 27 and CoverId = 218
select @ChildCoverDefinitionId = id from CoverDefinition where ProductId = @ProductId and CoverId = 218

SET @DummyQuestionDefId  =  (SELECT TOP 1 Parent.Id FROM QuestionDefinition Parent WHERE Parent.CoverDefinitionId = @ParentCoverDefinitionId AND Parent.QuestionId = 1081)

IF NOT EXISTS(SELECT * FROM QuestionDefinition WHERE QuestionId=1476 and CoverDefinitionId = @MotorCoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'AA NCB', @MotorCoverDefinitionId, 1476, NULL, 1, 22, 1, 1, 0,
               N'AA NCB', N'' , N'', 0, NULL)
			   
			   DECLARE @AANCBId INT;
			   SET @AANCBId = SCOPE_IDENTITY();
			   
			   INSERT INTO MapQuestionDefinition(ParentId, ChildId, IsDeleted)
			   VALUES(@DummyQuestionDefId, @AANCBId, 0);
			   
			  end