﻿DECLARE @ContentCoverDefinitionId INT;

SELECT @ContentCoverDefinitionId = cd.Id
FROM CoverDefinition cd
INNER JOIN Product pd ON pd.Id = cd.ProductId
INNER JOIN md.Cover mdc ON mdc.Id = cd.CoverId
WHERE pd.ProductCode = 'MUL'
AND mdc.Code = 'CONTENTS';

IF NOT EXISTS(SELECT * FROM QuestionDefinition WHERE QuestionId=1477 and CoverDefinitionId = @ContentCoverDefinitionId)
              BEGIN
              
			  INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'When were you last burgled?', @ContentCoverDefinitionId, 1477, NULL, 1, 22, 0, 0, 0,
               N'When were you last burgled?', N'' , N'', 0, NULL)

			   END