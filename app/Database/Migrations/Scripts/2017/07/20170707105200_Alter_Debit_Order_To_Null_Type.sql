﻿declare @value varchar(5);
set @value = (select IS_NULLABLE from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME ='PaymentDetails' and COLUMN_NAME ='DebitOrderDate')
if @value = 'no'
begin
ALTER TABLE PaymentDetails ALTER COLUMN DebitOrderDate Date NULL
end