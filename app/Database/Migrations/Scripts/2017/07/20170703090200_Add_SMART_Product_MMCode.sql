﻿SET ANSI_PADDING ON
GO

DECLARE @CoverDefinitionId INT

SELECT  @CoverDefinitionId = ( SELECT TOP 1
                                        CoverDefinition.Id
                               FROM     CoverDefinition
                                        INNER JOIN Product ON Product.Id = CoverDefinition.ProductId
                                        INNER JOIN md.Cover Cover ON Cover.Id = CoverDefinition.CoverId
                               WHERE    Product.ProductCode IN ( 'SCRATC' )
                                        AND Cover.id IN ( 368 )
                             )
	
--question
INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern] ,
          [GroupIndex] ,
          [QuestionDefinitionGroupTypeId] ,
          [Hide]
        )
VALUES  ( 0 ,
          N'MM Code' ,
          @CoverDefinitionId ,
          94 ,
          NULL ,
          1 ,
          0 ,
          0 ,
          0 ,
          0 ,
          N'' ,
          N'' ,
          N'^[0-9]+$' ,
          NULL ,
          NULL ,
          1
        )