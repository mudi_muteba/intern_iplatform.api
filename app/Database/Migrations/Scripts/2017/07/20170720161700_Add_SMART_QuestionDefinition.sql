﻿DECLARE @ProductId INT,
	@CoverId INT,
	@CoverDefinitionId INT,
	@QuestionId INT;
  
SET @ProductId = 86;
SET @CoverId = 368;
SET @CoverDefinitionId = (SELECT TOP 1 Id FROM CoverDefinition AS CD WITH (NOLOCK) WHERE ProductId = @ProductId AND CoverId = @CoverId);
SET @QuestionId = 1479;

IF (@CoverDefinitionId IS NULL)
BEGIN
	RETURN; -- cant find 
END

IF NOT EXISTS
(
	SELECT QD.* 
	FROM QuestionDefinition AS QD WITH (NOLOCK)
	WHERE QD.QuestionId = @QuestionId 
		AND CoverDefinitionId = @CoverDefinitionId
)
BEGIN

	INSERT QuestionDefinition 
	(
		MasterId,
		DisplayName,
		CoverDefinitionId,
		QuestionId,
		ParentId,
		QuestionDefinitionTypeId,
		VisibleIndex,
		RequiredForQuote,
		RatingFactor,
		[ReadOnly],
		ToolTip,
		DefaultValue,
		RegexPattern,
		GroupIndex,
		QuestionDefinitionGroupTypeId,
		Hide
	) 
	VALUES 
	(
		0,
		N'Ways to pay override value', 
		@CoverDefinitionId, 
		@QuestionId, 
		NULL, 
		1, 
		3, 
		0, 
		0, 
		0,
		N'',
		N'',
		N'',
		1,
		2,
		1
	)

END

