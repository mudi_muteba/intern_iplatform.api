﻿DECLARE @MotorCoverDefinitionId INT;

SELECT @MotorCoverDefinitionId = cd.Id
FROM CoverDefinition cd
INNER JOIN Product pd ON pd.Id = cd.ProductId
INNER JOIN md.Cover mdc ON mdc.Id = cd.CoverId
WHERE pd.ProductCode = 'MUL'
AND mdc.Code = 'MOTOR';

IF NOT EXISTS(SELECT * FROM QuestionDefinition WHERE QuestionId=1021 and CoverDefinitionId = @MotorCoverDefinitionId)
              BEGIN
              
			  INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Engine CC', @MotorCoverDefinitionId, 1021, NULL, 1, 22, 0, 0, 0,
               N'Engine CC', N'' , N'', 0, NULL)

			   END