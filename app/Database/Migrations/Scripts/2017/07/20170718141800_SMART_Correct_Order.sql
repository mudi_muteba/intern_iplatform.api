﻿DECLARE @ProductID AS INT = (SELECT id FROM dbo.Product WHERE ProductCode='SCRATC')
DECLARE @CoverDefID AS INT = (SELECT id FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND CoverId = 368)

UPDATE dbo.QuestionDefinition SET VisibleIndex=0 WHERE CoverDefinitionId=@CoverDefID AND QuestionId=141 --asset

UPDATE dbo.QuestionDefinition SET VisibleIndex=1 WHERE CoverDefinitionId=@CoverDefID AND QuestionId=97 --reg

UPDATE dbo.QuestionDefinition SET VisibleIndex=2 WHERE CoverDefinitionId=@CoverDefID AND QuestionId=95 --make

UPDATE dbo.QuestionDefinition SET VisibleIndex=3 WHERE CoverDefinitionId=@CoverDefID AND QuestionId=96 --model

UPDATE dbo.QuestionDefinition SET VisibleIndex=4 WHERE CoverDefinitionId=@CoverDefID AND QuestionId=142 --colour

UPDATE dbo.QuestionDefinition SET VisibleIndex=5 WHERE CoverDefinitionId=@CoverDefID AND QuestionId=99 --year of manufacture

UPDATE dbo.QuestionDefinition SET VisibleIndex=6 WHERE CoverDefinitionId=@CoverDefID AND QuestionId=98 --first registered

UPDATE dbo.QuestionDefinition SET VisibleIndex=7 WHERE CoverDefinitionId=@CoverDefID AND QuestionId=127 --type of vehicle

UPDATE dbo.QuestionDefinition SET VisibleIndex=8 WHERE CoverDefinitionId=@CoverDefID AND QuestionId=1010 --VIN

UPDATE dbo.QuestionDefinition SET VisibleIndex=9 WHERE CoverDefinitionId=@CoverDefID AND QuestionId=258 --new or used?

UPDATE dbo.QuestionDefinition SET VisibleIndex=0 WHERE CoverDefinitionId=@CoverDefID AND QuestionId=862 --financed or cash?

UPDATE dbo.QuestionDefinition SET VisibleIndex=10 WHERE CoverDefinitionId=@CoverDefID AND QuestionId=1105 --existing damage