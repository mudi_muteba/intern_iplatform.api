﻿DECLARE @ProductId INT,
	@CoverId INT,
	@CoverDefinitionId INT,
	@QuestionId INT;
  
SET @ProductId = 86;
SET @CoverId = 368;
SET @CoverDefinitionId = (SELECT TOP 1 Id FROM CoverDefinition AS CD WITH (NOLOCK) WHERE ProductId = @ProductId AND CoverId = @CoverId);
SET @QuestionId = 1478;

IF (@CoverDefinitionId IS NULL)
BEGIN
	RETURN; -- cant find 
END

IF NOT EXISTS
(
	SELECT QD.* 
	FROM QuestionDefinition AS QD WITH (NOLOCK)
	WHERE QD.QuestionId = @QuestionId 
		AND CoverDefinitionId = @CoverDefinitionId
)
BEGIN

	INSERT QuestionDefinition 
	(
		MasterId,
		DisplayName,
		CoverDefinitionId,
		QuestionId,
		ParentId,
		QuestionDefinitionTypeId,
		VisibleIndex,
		RequiredForQuote,
		RatingFactor,
		[ReadOnly],
		ToolTip,
		DefaultValue,
		RegexPattern,
		GroupIndex,
		QuestionDefinitionGroupTypeId
	) 
	VALUES 
	(
		0,
		N'Ways to pay', 
		@CoverDefinitionId, 
		@QuestionId, 
		NULL, 
		1, 
		2, 
		1, 
		1, 
		0,
		N'',
		N'',
		N'',
		1,
		2
	)

	UPDATE dbo.QuestionDefinition SET VisibleIndex=0 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=862 --Is the car financed?
	UPDATE dbo.QuestionDefinition SET VisibleIndex=1 WHERE CoverDefinitionId=@CoverDefinitionId AND QuestionId=102 --Sum Insured
END

