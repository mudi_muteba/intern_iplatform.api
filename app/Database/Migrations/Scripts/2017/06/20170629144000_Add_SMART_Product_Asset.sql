﻿SET ANSI_PADDING ON
GO

declare
	@CoverDefinitionId int

select
	@CoverDefinitionId =  (select top 1 CoverDefinition.Id from CoverDefinition inner join Product on Product.Id = CoverDefinition.ProductId inner join md.Cover Cover on Cover.Id = CoverDefinition.CoverId where Product.ProductCode in ('SCRATC') and Cover.id in (368))
	
--question
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 1, 0, 0,
               N'I''d like to gather some information about the asset you would like to insure.', N'' , N'^[0-9]+$', 5, 4)