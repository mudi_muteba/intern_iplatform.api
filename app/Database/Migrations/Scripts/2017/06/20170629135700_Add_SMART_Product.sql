﻿SET ANSI_PADDING ON
GO

--party
INSERT  [dbo].[Party]
        ( [MasterId] ,
          [PartyTypeId] ,
          [ContactDetailId] ,
          [DisplayName] ,
          [DateCreated] ,
          [DateUpdated]
        )
VALUES  ( 0 ,
          2 ,
          NULL ,
          N'SMART' ,
          NULL ,
          NULL
        )

DECLARE @Partyid INT
SET @Partyid = IDENT_CURRENT('Party')

--organization
INSERT  [dbo].[Organization]
        ( [partyId] ,
          [Code] ,
          [RegisteredName] ,
          [TradingName] ,
          [TradingSince] ,
          [Description] ,
          [RegNo] ,
          [FspNo] ,
          [VatNo]
        )
VALUES  ( @Partyid ,
          N'SMART' ,
          N'SMART Insurance' ,
          N'SMART Insurance' ,
          '' ,
          '' ,
          N'' ,
          N'' ,
          N''
        )

--address
INSERT  [dbo].[Address]
        ( [PartyId] ,
          [MasterId] ,
          [Description] ,
          [Complex] ,
          [Line1] ,
          [Line2] ,
          [Line3] ,
          [Line4] ,
          [Code] ,
          [Latitude] ,
          [Longitude] ,
          [StateProvinceId] ,
          [AddressTypeId] ,
          [IsDefault] ,
          [IsComplex] ,
          [DateFrom] ,
          [DateTo]
        )
VALUES  ( @Partyid ,
          0 ,
          N'Head Office' ,
          N'' ,
          N'Unit 12 Rawdon Business Park' ,
          N'' ,
          N'Moira' ,
          N'Derbyshire' ,
          N'DE12 6EJ' ,
          CAST(-25.789952 AS DECIMAL(9, 6)) ,
          CAST(28.278506 AS DECIMAL(9, 6)) ,
          4 ,
          1 ,
          1 ,
          1 ,
          NULL ,
          NULL
        )

--product
INSERT  [dbo].[Product]
        ( [MasterId] ,
          [Name] ,
          [ProductOwnerId] ,
          [ProductProviderId] ,
          [ProductTypeId] ,
          [AgencyNumber] ,
          [ProductCode] ,
          [StartDate] ,
          [EndDate]
        )
VALUES  ( 0 ,
          N'Scratch And Dent' ,
          @Partyid ,
          @Partyid ,
          7 ,
          N'' ,
          N'SCRATC' ,
          CAST(0x00009CD400000000 AS DATETIME) ,
          NULL
        )

DECLARE @Productid INT
SET @Productid = IDENT_CURRENT('Product')

--covers
INSERT  [dbo].[CoverDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [ProductId] ,
          [CoverId] ,
          [ParentId] ,
          [CoverDefinitionTypeId] ,
          [VisibleIndex]
        )
VALUES  ( 0 ,
          N'Scratch And Dent' ,
          @Productid ,
          368 ,
          NULL ,
          1 ,
          0
        )

DECLARE @CoverDefMotorid INT
SET @CoverDefMotorid = IDENT_CURRENT('CoverDefinition')

--questions
INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Car Registration' ,
          @CoverDefMotorid ,
          97 ,
          NULL ,
          1 ,
          0 ,
          1 ,
          1 ,
          0 ,
          N'{Title} {Name} would you please provide me with the registration number of the vehicle?' ,
          N'' ,
          N'^[0-9]+$'
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Make' ,
          @CoverDefMotorid ,
          95 ,
          NULL ,
          1 ,
          1 ,
          1 ,
          1 ,
          0 ,
          N'What is the make of this vehicle?' ,
          N'' ,
          N'^[0-9]+$'
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Model' ,
          @CoverDefMotorid ,
          96 ,
          NULL ,
          1 ,
          2 ,
          1 ,
          1 ,
          0 ,
          N'What is the model of this vehicle?' ,
          N'' ,
          N'^[0-9]+$'
        )
		
INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Colour' ,
          @CoverDefMotorid ,
          142 ,
          NULL ,
          1 ,
          3 ,
          1 ,
          1 ,
          0 ,
          N'{Title} {Name} would you please provide me with the colour of the vehicle?' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Year Of Manufacture' ,
          @CoverDefMotorid ,
          99 ,
          NULL ,
          1 ,
          4 ,
          1 ,
          1 ,
          0 ,
          N'What is the year of manufacture for this vehicle?' ,
          N'' ,
          N'^[0-9]'
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'First Registered' ,
          @CoverDefMotorid ,
          98 ,
          NULL ,
          1 ,
          0 ,
          5 ,
          1 ,
          0 ,
          N'What is the year of first registration for this vehicle?' ,
          N'' ,
          N'^[0-9]'
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Body Type' ,
          @CoverDefMotorid ,
          127 ,
          NULL ,
          1 ,
          0 ,
          6 ,
          1 ,
          0 ,
          N'What is the type of this vehicle?' ,
          N'' ,
          N'^[0-9]+$'
        )


INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'VIN' ,
          @CoverDefMotorid ,
          1010 ,
          NULL ,
          1 ,
          0 ,
          7 ,
          1 ,
          0 ,
          N'What is the VIN Number for this vehicle?' ,
          N'' ,
          N'^[0-9]+$'
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Is the car New?' ,
          @CoverDefMotorid ,
          258 ,
          NULL ,
          1 ,
          0 ,
          8 ,
          1 ,
          0 ,
          N'' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Is the car financed?' ,
          @CoverDefMotorid ,
          862 ,
          NULL ,
          1 ,
          0 ,
          9 ,
          1 ,
          0 ,
          N'' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Existing Damage' ,
          @CoverDefMotorid ,
          1105 ,
          NULL ,
          1 ,
          0 ,
          10 ,
          1 ,
          0 ,
          N'' ,
          N'' ,
          N''
        )

INSERT  [dbo].[QuestionDefinition]
        ( [MasterId] ,
          [DisplayName] ,
          [CoverDefinitionId] ,
          [QuestionId] ,
          [ParentId] ,
          [QuestionDefinitionTypeId] ,
          [VisibleIndex] ,
          [RequiredForQuote] ,
          [RatingFactor] ,
          [ReadOnly] ,
          [ToolTip] ,
          [DefaultValue] ,
          [RegexPattern]
        )
VALUES  ( 0 ,
          N'Sum Insured' ,
          @CoverDefMotorid ,
          102 ,
          NULL ,
          1 ,
          4 ,
          11 ,
          1 ,
          0 ,
          N'What is the sum insured of the vehicle?' ,
          N'' ,
          N'^[0-9\.]+$'
        )