--- RENAMING THE KINGPRICE CHANNEL

UPDATE Channel SET Name = 'King Price Head Office' WHERE Id = 5 AND SystemId = 'CBFED7DA-CD47-4B24-8A48-D917C81B671C'

---INSERTING NEW CHANNEL

DECLARE @chanid INT
DECLARE @Name VARCHAR(50)
DECLARE @Code VARCHAR(50)

DECLARE @chanEvent INT
DECLARE @rootUserid INT

SET @rootUserid = ( SELECT TOP 1 ID FROM [User] where UserName like '%root@iplatform.co.za%')

SET @Code = N'KP'
SET @Name = N'YMS'

 
SET @chanid = ( SELECT TOP 1 ID FROM dbo.Channel where  Code like @Code)

if not exists(select * from Channel where Id = 19)
BEGIN
INSERT Into Channel (
 [Id]
 ,[SystemId]
 ,[IsActive]
 ,[ActivatedOn]
 ,[DeactivatedOn]
 ,[IsDefault]
 ,[IsDeleted]
 ,[CurrencyId]
 ,[DateFormat]
 ,[Name]
 ,[LanguageId]
 ,[PasswordStrengthEnabled]
 ,[Code]
 ,[CountryId])
 Values (
 19,
 'F0D21BA3-985B-4818-82AF-6BDCD6068010',
 1,
 GETDATE(),
 Null,
 0,
 0,
 1,
 'dd MMM yyyy',
 @Name,
 2,
 0,
 @Code,
 197
)
SET @chanid = SCOPE_IDENTITY()
END


DECLARE @userChan INT
 
SET @userChan = ( SELECT TOP 1 ID FROM [UserChannel] where UserId = @rootUserid AND ChannelId = @chanid) -- 760 is Root
SELECT @rootUserid
if (@userChan IS NULL)
BEGIN
INSERT INTO [UserChannel] (CreatedAt, ModifiedAt, UserId, ChannelId, IsDeleted, IsDefault)
VALUES( GETDATE(), GETDATE(), @rootUserid, 14, 0, NULL)
SET @userChan = SCOPE_IDENTITY()
END

SELECT * FROM Channel

SELECT   * FROM [UserChannel] where UserId = @rootUserid AND ChannelId = @chanid
 SELECT   * FROM [UserAuthorisationGroup] where UserId = @rootUserid AND ChannelId = @chanid
 SELECT   * FROM [ChannelEventTask] where ChannelEventId = @chanEvent
  SELECT   * FROM [ChannelEvent] where ProductCode like 'KPIPERS2' and ChannelId = @chanid
   SELECT   * FROM dbo.Channel where  Code like @Code

RETURN

DECLARE @userAuth INT
 
SET @userAuth = ( SELECT TOP 1 ID FROM [UserAuthorisationGroup] where UserId = @rootUserid AND ChannelId = @chanid) -- 760 is Root


if (@userAuth IS NULL)
BEGIN
INSERT INTO [UserAuthorisationGroup] (AuthorisationGroupId, UserId, ChannelId, IsDeleted)
VALUES( 5, @rootUserid, @chanid, 0)
SET @userAuth = SCOPE_IDENTITY()
END
