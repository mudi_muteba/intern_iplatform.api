DECLARE @productId int;
SELECT @productId = Id FROM Product WHERE ProductCode = 'KPIPERS2' AND IsDeleted != 1;

if(@productId>0)
BEGIN
	Update SettingsQuoteUpload Set SettingValue = 'https://dev.kingprice.co.za/Services/PartnerService.svc' 
	WHERE Environment != 'Live' and ProductId = @productId and SettingName = 'Kingpriceobv2/Upload/ServiceURL';

	Update SettingsQuoteUpload Set SettingValue = 'https://dev.kingprice.co.za/Services/PartnerService.svc' 
	WHERE Environment != 'Live' and ProductId = @productId and SettingName = 'Kingpriceobv2/Upload/ChannelEndpoint';

	Update SettingsQuoteUpload Set SettingValue = 'https://dev.kingprice.co.za/IdentityServer/issue/wstrust/mixed/username' 
	WHERE Environment != 'Live' and ProductId = @productId and SettingName = 'Kingpriceobv2/Upload/SecurityEndpoint';

	Update SettingsQuoteUpload Set SettingValue = 'https://dev.kingprice.co.za/Services/' 
	WHERE Environment != 'Live' and ProductId = @productId and SettingName = 'Kingpriceobv2/Upload/SecurityDestination';
END