
if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_MotorExtract') and type in (N'P', N'PC'))
begin
	drop procedure Reports_AIG_MotorExtract
end
go

create procedure Reports_AIG_MotorExtract
(
--@PartyId int
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET NOCOUNT ON 

--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
--set @StartDate = '2016-09-01'
--set @EndDate = '2017-01-15'

create table #tempExtra(PartyId int, AssetId int, Description nvarchar(100), Value decimal(18,2), AccessoryTypeId int)
insert into #tempExtra
select ast.PartyId, ast.Id as AssetId, astEx.Description, astEx.Value, astEx.AccessoryTypeId from dbo.AssetExtras as astEx
join dbo.Asset as ast on ast.Id = astEx.AssetId
where astex.IsDeleted <> 1
CREATE INDEX tempExtra ON #tempExtra(PartyId , AssetId ,AccessoryTypeId )
CREATE TABLE #TempTableMotorExtract(
 ProposalDefinitionId int, PartyId int, LeadId int, ProposalHeaderId int, CoverId int, Channel nvarchar(max), QuoteExpired nvarchar(max), QuoteBound nvarchar(max), QuoteBindDate nvarchar(max), DateOfQuote nvarchar(max), 
QuoteTotalPremium decimal(38,2), QuoteMotorPremium decimal(38,2), QuoteHomePremium decimal(38,2), QuoteFuneralPremium decimal(38,2), QuoteOtherPremium decimal(38,2),
QuoteFees decimal(38,2), QuoteSasria decimal(38,2), QuoteIncepptionDate nvarchar(max), QuotePolicyNumber nvarchar(max), QuoteCreateDate nvarchar(max), QuoteCreateTime nvarchar(max), QuoteID int, VMSAID nvarchar(max), VehicleNumber int,
AccSoundEquipment decimal(38,2),AccMagWheels decimal(38,2),AccSunRoof decimal(38,2),AccXenonLights decimal(38,2),AccTowBar decimal(38,2),AccBodyKit decimal(38,2),AccAntiSmashAndGrab decimal(38,2),AccOther decimal(38,2),
DriverIDNumber nvarchar(max),DriverTitle nvarchar(max),DriverFirstName nvarchar(max),DriverSurname nvarchar(max),
DriverGender nvarchar(max),DriverMaritalStatus nvarchar(max),DriverDateOfBirth nvarchar(max),DriverOccupation nvarchar(max),
DriverRecentCoverIndicator nvarchar(max), DriverUninterruptedCover nvarchar(max),
SumInsured decimal(38,2), SASRIA decimal(38,2))
CREATE INDEX testTemp ON #TempTableMotorExtract(ProposalDefinitionId , PartyId , LeadId , ProposalHeaderId , CoverId )
insert into #TempTableMotorExtract
(
 ProposalDefinitionId , PartyId , LeadId , ProposalHeaderId , CoverId , Channel , QuoteExpired , QuoteBound , QuoteBindDate , DateOfQuote , 
QuoteTotalPremium , QuoteMotorPremium , QuoteHomePremium , QuoteFuneralPremium , QuoteOtherPremium ,
QuoteFees , QuoteSasria , QuoteIncepptionDate , QuotePolicyNumber , QuoteCreateDate , QuoteCreateTime , QuoteID , VMSAID , VehicleNumber ,
AccSoundEquipment ,AccMagWheels ,AccSunRoof ,AccXenonLights ,AccTowBar ,AccBodyKit ,AccAntiSmashAndGrab ,AccOther ,
DriverIDNumber ,DriverTitle ,DriverFirstName ,DriverSurname ,
DriverGender ,DriverMaritalStatus ,DriverDateOfBirth ,DriverOccupation ,
DriverRecentCoverIndicator , DriverUninterruptedCover ,
SumInsured , SASRIA ) 
SELECT 
prd.Id, p.Id, l.id , Prh.id , Prd.coverid,
CASE WHEN Prh.Source IS NULL THEN ('Call center') ELSE Prh.Source END as Channel,
CASE WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y' ELSE 'N' END as 'QuoteExpired',
(CASE 
 (SELECT COALESCE((select top 1 PolicyStatusId from dbo.PolicyHeader  WITH (NOLOCK) where PartyId = p.id and QuoteId = qute.Id and PolicyStatusId = 3), 0))
  when 3 then 'Y'
  when 2 then 'N'
  when 1 then 'N'
  when 0 then 'N'
End  )as 'QuoteBound',
	(CASE 
		(SELECT COALESCE((select top 1 PolicyStatusId from dbo.PolicyHeader  WITH (NOLOCK) where PartyId = p.id and QuoteId = qute.Id and PolicyStatusId = 3), 0))
			when 3 then CONVERT(VARCHAR(10),Qute.AcceptedOn ,111)
			when 2 then ''
			when 1 then ''
			when 0 then ''
	End  )as 'QuoteBindDate',
'' as 'DateOfQuote',
	
	(select ISNULL(sum(cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2))) + cast(round (Qute.Fees ,2) as numeric(36,2))  , 0) 
	from dbo.QuoteItem 	 WITH (NOLOCK) 
				where QuoteId = Qute.id) as 'QuoteTotalPremium',
	
	(select ISNULL(sum(cast(round (Premium ,2) as numeric(36,2))),0)
	from dbo.QuoteItem 	 WITH (NOLOCK) 
				where QuoteId = Qute.id and CoverDefinitionId in (select id from dbo.CoverDefinition  WITH (NOLOCK) where CoverId = 218)) as 'QuoteMotorPremium',
	 
	
	(select ISNULL(sum(cast(round (Premium ,2) as numeric(36,2))),0)
	from dbo.QuoteItem 	 WITH (NOLOCK) 
				where QuoteId = Qute.id and CoverDefinitionId in (select id from dbo.CoverDefinition  WITH (NOLOCK) where CoverId = 78)) as 'QuoteHomePremium',
	
	(select ISNULL(sum(cast(round (Premium ,2) as numeric(36,2))),0)
	from dbo.QuoteItem 	 WITH (NOLOCK) 
				where QuoteId = Qute.id and CoverDefinitionId in (select id from dbo.CoverDefinition  WITH (NOLOCK) where CoverId = 142)) as 'QuoteFuneralPremium',
	
	(select ISNULL(sum(cast(round (Premium ,2) as numeric(36,2))),0)
	from dbo.QuoteItem 	 WITH (NOLOCK) 
				where QuoteId = Qute.id and CoverDefinitionId in (select id from dbo.CoverDefinition  WITH (NOLOCK) where CoverId not in(218,78,142)))   as 'QuoteOtherPremium',
	(cast(round(Qute.fees,2) as numeric(36,2))) as 'QuoteFees',
	(select sum(cast(round (Sasria ,2) as numeric(36,2)))  from dbo.QuoteItem 		 WITH (NOLOCK) 
				where QuoteId = Qute.id) as 'QuoteSasria',
	(select top 1 CONVERT(VARCHAR(10),DateEffective ,111) from dbo.PolicyHeader  WITH (NOLOCK) where PartyId = p.Id and QuoteId = qute.Id and PolicyStatusId = 3) as 'QuoteIncepptionDate',
	(select COALESCE((select top 1 PolicyNo from dbo.PolicyHeader  WITH (NOLOCK) where PartyId = p.Id and QuoteId = qute.Id and PolicyStatusId = 3),null)) as 'QuotePolicyNumber',
	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'QuoteCreateDate',
	CONVERT(VARCHAR(5),Qute.CreatedAt,108) as 'QuoteCreateTime', 
	qute.Id as 'QuoteID',
	Ind.ExternalReference as 'VMSAID',	
	(select COALESCE((select AssetNo from dbo.Asset  WITH (NOLOCK) where id = prd.AssetId),null)) as 'VehicleNumber',
	(select COALESCE((select sum(value) from #tempExtra where AccessoryTypeId = 1 and AssetId = ast.Id),0)) as 'AccSoundEquipment',	
	(select COALESCE((select sum(value) from #tempExtra where AccessoryTypeId = 2 and AssetId = ast.Id),0)) as 'AccMagWheels',
	(select COALESCE((select sum(value) from #tempExtra where AccessoryTypeId = 3 and AssetId = ast.Id),0)) as 'AccSunRoof',
	(select COALESCE((select sum(value) from #tempExtra where AccessoryTypeId = 4 and AssetId = ast.Id),0)) as 'AccXenonLights',
	(select COALESCE((select sum(value) from #tempExtra where AccessoryTypeId = 5 and AssetId = ast.Id),0)) as 'AccTowBar',
	(select COALESCE((select sum(value) from #tempExtra where AccessoryTypeId = 6 and AssetId = ast.Id),0)) as 'AccBodyKit',
	(select COALESCE((select sum(value) from #tempExtra where AccessoryTypeId = 7 and AssetId = ast.Id),0)) as 'AccAntiSmashAndGrab',
	(select COALESCE((select sum(value) from #tempExtra where AccessoryTypeId = 8 and AssetId = ast.Id),0)) as 'AccOther',
		Ind.IdentityNo as 'DriverIDNumber',
	(select name from Md.Title  WITH (NOLOCK) where Id = Ind.TitleId) as 'DriverTitle',
	Ind.FirstName as 'DriverFirstName', 
	Ind.Surname as 'DriverSurname',
	(select name from Md.Gender  WITH (NOLOCK) where Id = Ind.GenderId) as 'DriverGender',
	
	(select COALESCE((select name from Md.MaritalStatus  WITH (NOLOCK) where Id = Ind.MaritalStatusId),'')) as 'DriverMaritalStatus',
	CONVERT(VARCHAR(10),Ind.DateOfBirth ,111) as 'DriverDateOfBirth',
	occ.Name as 'DriverOccupation',
	(select top 1 ci.Answer 
	from dbo.LossHistory as lh  WITH (NOLOCK) 
	join md.CurrentlyInsured as ci  WITH (NOLOCK) on ci.Id = lh.CurrentlyInsuredId
	where PartyId = p.Id  and IsDeleted = 0 order by lh.Id desc) as 'DriverRecentCoverIndicator',
    (select top 1 up.Answer 
	from dbo.LossHistory as lh WITH (NOLOCK) 
	join md.UninterruptedPolicy as up  WITH (NOLOCK) on up.Id = lh.UninterruptedPolicyId
	where PartyId = p.Id  and IsDeleted = 0 order by lh.Id desc) as 'DriverUninterruptedCover',
	(select SumInsured from dbo.Asset  WITH (NOLOCK) where id = prd.AssetId) as 'SumInsured', 
	(select total from dbo.QuoteItemBreakDown  WITH (NOLOCK) where QuoteItemId = qi.Id and Name = 'Sasria')  as 'SASRIA'
	
--select *
from dbo.ProposalHeader as Prh  WITH (NOLOCK)
join dbo.ProposalDefinition as prd  WITH (NOLOCK) on prd.ProposalHeaderId = Prh.Id and prd.CoverId = 218
join dbo.QuoteHeader as Qh  WITH (NOLOCK) on qh.ProposalHeaderId = Prh.Id
join dbo.Quote as Qute WITH (NOLOCK)  on Qute.QuoteHeaderId = Qh.Id
join dbo.Party as P  WITH (NOLOCK) on p.Id = Prh.PartyId
join dbo.Lead as L  WITH (NOLOCK) on l.PartyId = p.Id
left join dbo.Individual as Ind  WITH (NOLOCK) on P.Id = Ind.PartyId
left join dbo.Occupation as Occ  WITH (NOLOCK) on Occ.Id = Ind.OccupationId
join dbo.QuoteItem as qi  WITH (NOLOCK) on qi.QuoteId = qute.Id and qi.IsDeleted = 0
left join dbo.QuoteItemBreakDown as qibd  WITH (NOLOCK) on qibd.QuoteItemId = qi.Id and qibd.IsDeleted = 0
join dbo.CoverDefinition as covD  WITH (NOLOCK) on covD.Id = qi.CoverDefinitionId
inner join dbo.Asset as ast WITH (NOLOCK) on ast.AssetNo = qi.AssetNumber and ast.Id = prd.AssetId
where qute.CreatedAt  BETWEEN @StartDate AND @EndDate 
and L.IsDeleted = 0
and Qute.IsDeleted = 0
and Prh.IsDeleted = 0
and Qh.IsRerated = 0
and prd.IsDeleted = 0
and prd.CoverId in (218)
and covD.CoverId = 218 
group by Prh.Source, prd.Id , p.id, l.id,prh.id,prd.coverid,qute.createdat,qute.id,qute.acceptedon, qute.Fees, qh.CreatedAt, ind.ExternalReference, prd.AssetId, qi.Id, ast.Id, Ind.IdentityNo, Ind.TitleId, Ind.FirstName, Ind.Surname,
Ind.GenderId, Ind.MaritalStatusId, Ind.DateOfBirth, Occ.Name 


CREATE TABLE #TempTableQuestionAnswer(Answer nvarchar(max), QuestionId int, ProposalDefinitionId int)

CREATE INDEX TempTableQuestionAnswerIndex ON #TempTableQuestionAnswer(QuestionId , ProposalDefinitionId)


insert into #TempTableQuestionAnswer( Answer , QuestionId, ProposalDefinitionId ) 

SELECT pa.Answer, qd.QuestionId, pa.ProposalDefinitionId from dbo.ProposalQuestionAnswer pa  WITH (NOLOCK) 
join dbo.QuestionDefinition qd  WITH (NOLOCK) on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId in (select ProposalDefinitionId FROM #TempTableMotorExtract)





SELECT   Channel  ,QuoteExpired, QuoteBound, QuoteBindDate,  '' as 'DateOfQuote', QuoteTotalPremium, QuoteMotorPremium, QuoteHomePremium, QuoteFuneralPremium, QuoteOtherPremium,QuoteFees,
QuoteSasria, QuoteIncepptionDate, QuotePolicyNumber, QuoteCreateDate, QuoteCreateTime, QuoteID, VMSAID, VehicleNumber,



(select Answer from #TempTableQuestionAnswer where QuestionId = 89 and ProposalDefinitionId = t.ProposalDefinitionId) as 'NightAddress-Suburb',
(select Answer from #TempTableQuestionAnswer where QuestionId = 90 and ProposalDefinitionId = t.ProposalDefinitionId) as 'NightAddress-PostCode',

(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 47 and ProposalDefinitionId = t.ProposalDefinitionId ) as 'NightAddress-Province',

(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 863 and ProposalDefinitionId = t.ProposalDefinitionId ) as 'NightAddress-Storage',


(select Answer from #TempTableQuestionAnswer where QuestionId = 135 and ProposalDefinitionId = t.ProposalDefinitionId) as 'DayAddress-Suburb',
(select Answer from #TempTableQuestionAnswer where QuestionId = 91 and ProposalDefinitionId = t.ProposalDefinitionId) as 'DayAddress-PostCode',

(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 154 and ProposalDefinitionId = t.ProposalDefinitionId ) as 'DayAddress-Province',

	(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 864 and ProposalDefinitionId = t.ProposalDefinitionId ) as 'DayAddress-Storage',

(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 786 and ProposalDefinitionId = t.ProposalDefinitionId ) as 'VehicleType',

	(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 874 and ProposalDefinitionId = t.ProposalDefinitionId  ) as 'MotorExcess',


(select Answer from #TempTableQuestionAnswer where QuestionId = 97 and ProposalDefinitionId = t.ProposalDefinitionId) as 'RegistrationNumber',
(select Answer from #TempTableQuestionAnswer where QuestionId = 99 and ProposalDefinitionId = t.ProposalDefinitionId) as 'VehicleYear',
(select Answer from #TempTableQuestionAnswer where QuestionId = 95 and ProposalDefinitionId = t.ProposalDefinitionId) as 'VehicleMake',
(select Answer from #TempTableQuestionAnswer where QuestionId = 96 and ProposalDefinitionId = t.ProposalDefinitionId) as 'VehicleModel',
(select Answer from #TempTableQuestionAnswer where QuestionId = 94 and ProposalDefinitionId = t.ProposalDefinitionId) as 'VehicleMMCode',
(select Answer from #TempTableQuestionAnswer where QuestionId = 310 and ProposalDefinitionId = t.ProposalDefinitionId) as 'VINNumber',





(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 227 and ProposalDefinitionId = t.ProposalDefinitionId ) as 'Colour',

 'Y'  as 'MetallicPaint',

	(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 230 and ProposalDefinitionId = t.ProposalDefinitionId ) as 'CoverType',




		(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 255 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Modified',

	(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 867 and ProposalDefinitionId = t.ProposalDefinitionId ) as 'VehicleStatus',

	(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 226 and ProposalDefinitionId = t.ProposalDefinitionId ) as 'ClassOfUse',

(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 232 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Delivery',


	
	Case (select Answer from #TempTableQuestionAnswer where QuestionId = 870 and ProposalDefinitionId = t.ProposalDefinitionId)				
		when null then 'N' 	
		when 'false' then 'N'
		when 'true' then 'Y'	
	end  as 'TrackingDevice', 

	Case (select Answer from #TempTableQuestionAnswer where QuestionId = 298 and ProposalDefinitionId = t.ProposalDefinitionId)				
		when null then 'N' 	
		when 'false' then 'N'
		when 'true' then 'Y'	
	end  as 'DiscountedTrackingDevice',	

	(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 245 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Immobiliser',

	(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 871 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'AntiHijack',

	(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 231 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'DataDot',

	AccSoundEquipment ,AccMagWheels ,AccSunRoof ,AccXenonLights ,AccTowBar ,AccBodyKit ,AccAntiSmashAndGrab ,AccOther,

	(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 1005 and ProposalDefinitionId = t.ProposalDefinitionId) as 'VAP-CarHire',

	(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 1004 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-TyreAndRim',

	(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 1003 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-ScratchAndDent',

	(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 873 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'VAP-CreditShortfall',

	DriverIDNumber, DriverTitle,DriverFirstName, DriverSurname, DriverGender,DriverMaritalStatus,DriverDateOfBirth,DriverOccupation,DriverRecentCoverIndicator, DriverUninterruptedCover,

	(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 249 and ProposalDefinitionId = t.ProposalDefinitionId) as 'DriverLicenseType',

	(select REPLACE(CONVERT(VARCHAR(10),Answer ,111), '-', '/') from #TempTableQuestionAnswer where QuestionId = 41 and ProposalDefinitionId = t.ProposalDefinitionId) as 'DriverLicenseDate',

	(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 248 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'DriverLicenseEndorsed',

	(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 224 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Automatic',

	(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 261 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Paraplegic',

	(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 223 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Amputee',

	(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 234 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Limitation-Glasses',

	(case 
		(select Answer from #TempTableQuestionAnswer where QuestionId = 862 and ProposalDefinitionId = t.ProposalDefinitionId)	
		when 'false' then 'N'
		when 'true' then 'Y'
	End) as 'Financed',

(select qa.Answer from #TempTableQuestionAnswer as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 300 and ProposalDefinitionId = t.ProposalDefinitionId) as 'ValuationMethod',


SumInsured, SASRIA, PartyId


FROM #TempTableMotorExtract as t 
DROP TABLE #TempTableMotorExtract 
DROP table #tempExtra
DROP table #TempTableQuestionAnswer
