if not exists(select * from Report where Name = 'Insurer Data')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (1, 2, 3, 'Insurer Data', 'Displays detailed lead and quote (motor only) related statistical charts and breakdowns by insurer product.', 'InsurerData.trdx', 1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Insurer Data',
			[Description] = 'Displays detailed lead and quote (motor only) related statistical charts and breakdowns by insurer product.',
			SourceFile = 'InsurerData.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1,
			Info = '
				This report uses the default channel of this iPlatform instance and whatever campaigns were selected as criteria inside the selected date range. It lists report data as well as statistical/chart data for captured leads and primarily motor/vehicle related quotes. <br /><br />

				<ul>
					<li>
						<strong>Insurer</strong>
						<ul><li>The name of the insurer</li></ul>
					</li>
					<li>
						<strong>Product</strong>
						<ul><li>The name of the insurer''s product</li></ul>
					</li>
					<li>
						<strong>Campaign</strong>
						<ul><li>The name of the campaign</li></ul>
					</li>
					<li>
						<strong>Quotes</strong>
						<ul><li>The amount of quotes associated with this insurer''s product and associated campaign.</li></ul>
					</li>
					<li>
						<strong>Sales</strong>
						<ul><li>The amount of sales associated with this insurer''s product and associated campaign.</li></ul>
					</li>
					<li>
						<strong>Closing %</strong>
						<ul><li>The percentage based closing ratio (sales ratio) of quotes associated with this insurer''s product and associated campaign.</li></ul>
					</li>
					<li>
						<strong>Avg. Premium Per Sale</strong>
						<ul><li>The average premium amount per sale for insurer''s product and associated campaign.</li></ul>
					</li>
					<li>
						<strong>Avg. Premium Per Quote</strong>
						<ul><li>The average premium amount per sale for insurer''s product and associated campaign.</li></ul>
					</li>
				</ul>

				<strong>Lead & Vehicle Statistical Chart Data:</strong> <br />
				Based on user input on the following screens:<br /> <br />

				<ul>
					<li>
						<strong>Create Lead & Edit Lead</strong>
						<ul>
							<li>Age Statistics</li>
							<li>Gender Statistics</li>
							<li>Insurance Types Statistics (Auto, Home, Both)</li>
							<li>Marital Status Statistics</li>
							<li>Province Statisics</li>
						</ul>
					</li>
					<li>
						<strong>Create Proposal: Motor</strong>
						<ul>
							<li>Class of Use</li>
							<li>Cover Types</li>
							<li>Average No Claim Bonus</li>
							<li>Average Sum Insured</li>
							<li>Vehicle Colors</li>
							<li>Vehicle Makes</li>
						</ul>
					</li>
				</ul>
			'
		where
			Name = 'Insurer Data'
	end
go