declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode = 'IP')
	begin

--Motor

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 96 and QuestionId = 80

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 96 and QuestionId = 85

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 96 and QuestionId = 86

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 96 and QuestionId = 136

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 96 and QuestionId = 149

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 96 and QuestionId = 150

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 96 and QuestionId = 43


	end
else
	begin
		return
    end
