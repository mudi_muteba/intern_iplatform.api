if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformance_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformance_Header
	end
go

create procedure Report_CallCentre_AgentPerformance_Header
(
	@CampaignID int
)
as
	select top 1 Code Insurer, '' Color from Channel where IsDefault = 1
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics
	end
go

create procedure Report_CallCentre_AgentPerformance_GetAgentQuoteStatistics
(
	@CampaignIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Agents_CTE (ChannelId, AgentId, CampaignId, Channel, Agent, Campaign)
			as 
			(
				select distinct
					Channel.Id,
					[User].Id,
					Campaign.Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
					inner join Channel on Channel.Id = Campaign.ChannelId
					inner join [User] on [User].Id = LeadActivity.UserId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
					UserAuthorisationGroup.AuthorisationGroupId = 1 and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Channel.Id,
					[User].Id,
					Campaign.Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				)
			select
				Channel,
				Agent,
				Campaign,
				(
					select
						isnull(count(distinct(LeadActivity.LeadId)), 0)
					from LeadActivity
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
				) Leads,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) Quotes,
				(
					isnull(
						cast(
								(
									select
										cast(isnull(count(distinct(Quote.Id)), 0) as numeric(18, 2))
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
										inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
										inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
										inner join Product on Product.Id = Quote.ProductId
										inner join Organization on Organization.PartyId = Product.ProductOwnerId
										inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
										inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
										inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
									where
										LeadActivity.CampaignId = Agents_CTE.CampaignId and
										LeadActivity.UserId = Agents_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo and
										QuoteHeader.IsRerated = 0 and
										Quote.IsDeleted = 0 and
										QuoteItem.IsDeleted = 0
								)
					
								/
					
								(
									select
										nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
									from LeadActivity
									where
										LeadActivity.CampaignId = Agents_CTE.CampaignId and
										LeadActivity.UserId = Agents_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo
								)

							as numeric(18,2)
						)
					, 0)
				) QuotesPerLead,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) SecondLevelUnderwriting,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) Sales,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) Closing,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerClosing,
				(
					isnull(
						cast
						(
							
							(
								1
						
								-

								(
									( 
										select
											cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
										from LeadActivity
											inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
											inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Agents_CTE.CampaignId and
											LeadActivity.UserId = Agents_CTE.AgentId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0 and
											QuoteItem.IsDeleted = 0
									)

									/

									(
										select
											nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
										from LeadActivity
											inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
											inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Agents_CTE.CampaignId and
											LeadActivity.UserId = Agents_CTE.AgentId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0 and
											QuoteItem.IsDeleted = 0
									)
								)
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerDropOff,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from LeadActivity
							inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Agents_CTE.CampaignId and
							LeadActivity.UserId = Agents_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote --AverageInsurerPremiumPerSale
			from
				Agents_CTE
			order by
				Campaign,
				Agent
		end
	else
		begin
			;with Agents_CTE (ChannelId, AgentId, CampaignId, Channel, Agent, Campaign)
			as 
			(
				select distinct
					Channel.Id,
					[User].Id,
					Campaign.Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
					inner join Channel on Channel.Id = Campaign.ChannelId
					inner join [User] on [User].Id = LeadActivity.UserId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
					UserAuthorisationGroup.AuthorisationGroupId = 1 and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Channel.Id,
					[User].Id,
					Campaign.Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName,
					Campaign.Name
				)
			select
				Channel,
				Agent,
				Campaign,
				(
					select
						isnull(count(distinct(LeadActivity.LeadId)), 0)
					from LeadActivity
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
				) Leads,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) Quotes,
				(
					isnull(
						cast(
								(
									select
										cast(isnull(count(distinct(Quote.Id)), 0) as numeric(18, 2))
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
										inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
										inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
										inner join Product on Product.Id = Quote.ProductId
										inner join Organization on Organization.PartyId = Product.ProductOwnerId
										inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
										inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
										inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
									where
										LeadActivity.CampaignId = Agents_CTE.CampaignId and
										LeadActivity.UserId = Agents_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo and
										QuoteHeader.IsRerated = 0 and
										Quote.IsDeleted = 0 and
										QuoteItem.IsDeleted = 0
								)
					
								/
					
								(
									select
										nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
									from LeadActivity
									where
										LeadActivity.CampaignId = Agents_CTE.CampaignId and
										LeadActivity.UserId = Agents_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo
								)

							as numeric(18,2)
						)
					, 0)
				) QuotesPerLead,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) SecondLevelUnderwriting,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Agents_CTE.CampaignId and
						LeadActivity.UserId = Agents_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) Sales,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) Closing,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Agents_CTE.CampaignId and
									LeadActivity.UserId = Agents_CTE.AgentId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerClosing,
				(
					isnull(
						cast
						(
							
							(
								1
						
								-

								(
									( 
										select
											cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
										from LeadActivity
											inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
											inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Agents_CTE.CampaignId and
											LeadActivity.UserId = Agents_CTE.AgentId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0 and
											QuoteItem.IsDeleted = 0
									)

									/

									(
										select
											nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
										from LeadActivity
											inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
											inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Agents_CTE.CampaignId and
											LeadActivity.UserId = Agents_CTE.AgentId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0 and
											QuoteItem.IsDeleted = 0
									)
								)
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerDropOff,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from LeadActivity
							inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Agents_CTE.CampaignId and
							LeadActivity.UserId = Agents_CTE.AgentId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote --AverageInsurerPremiumPerSale
			from
				Agents_CTE
			order by
				Campaign,
				Agent
		end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformanceByProduct_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformanceByProduct_Header
	end
go

create procedure Report_CallCentre_AgentPerformanceByProduct_Header
(
	@CampaignID int
)
as
	select top 1 Code Insurer, '' Color from Channel where IsDefault = 1
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_AgentPerformanceByProduct_GetAgentQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_AgentPerformanceByProduct_GetAgentQuoteStatistics
	end
go

create procedure Report_CallCentre_AgentPerformanceByProduct_GetAgentQuoteStatistics
(
	@CampaignIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Insurers_CTE (InsurerId, CampaignId, Insurer, ProductId, Product, Campaign, ChannelId, AgentId, Channel, Agent)
			as 
			(
				select distinct
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Id,
					Product.Name,
					Campaign.Name,
					Channel.Id,
					[User].Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
					inner join Channel on Channel.Id = Campaign.ChannelId
					inner join [User] on [User].Id = LeadActivity.UserId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
					UserAuthorisationGroup.AuthorisationGroupId = 1 and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Id,
					Product.Name,
					Campaign.Name,
					Channel.Id,
					[User].Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName
				)
			select
				Channel,
				Campaign,
				Agent,
				Insurer,
				Product,
				(
					select
						isnull(count(distinct(LeadActivity.LeadId)), 0)
					from LeadActivity
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
				) Leads,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.Id = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) Quotes,
				(
					isnull(
						cast(
								(
									select
										cast(isnull(count(distinct(Quote.Id)), 0) as numeric(18, 2))
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
										inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
										inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
										inner join Product on Product.Id = Quote.ProductId
										inner join Organization on Organization.PartyId = Product.ProductOwnerId
										inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
										inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
										inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
									where
										LeadActivity.CampaignId = Insurers_CTE.CampaignId and
										LeadActivity.UserId = Insurers_CTE.AgentId and
										Organization.PartyId = Insurers_CTE.InsurerId and
										Product.Id = Insurers_CTE.ProductId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo and
										QuoteHeader.IsRerated = 0 and
										Quote.IsDeleted = 0 and
										QuoteItem.IsDeleted = 0
								)
					
								/
					
								(
									select
										nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
									from LeadActivity
									where
										LeadActivity.CampaignId = Insurers_CTE.CampaignId and
										LeadActivity.UserId = Insurers_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo
								)

							as numeric(18,2)
						)
					, 0)
				) QuotesPerLead,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.Id = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) SecondLevelUnderwriting,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.Id = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) Sales,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(distinct(Quote.Id)), 0) as numeric( 18, 2))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) Closing,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerClosing,
				(
					isnull(
						cast
						(
							
							(
								1
						
								-

								(
									( 
										select
											cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
										from LeadActivity
											inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
											inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Insurers_CTE.CampaignId and
											LeadActivity.UserId = Insurers_CTE.AgentId and
											Organization.PartyId = Insurers_CTE.InsurerId and
											Product.Id = Insurers_CTE.ProductId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0 and
											QuoteItem.IsDeleted = 0
									)

									/

									(
											select
												nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
											from LeadActivity
												inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
												inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
												inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
												inner join Product on Product.Id = Quote.ProductId
												inner join Organization on Organization.PartyId = Product.ProductOwnerId
												inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
												inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
												inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
											where
												LeadActivity.CampaignId = Insurers_CTE.CampaignId and
												LeadActivity.UserId = Insurers_CTE.AgentId and
												Organization.PartyId = Insurers_CTE.InsurerId and
												Product.Id = Insurers_CTE.ProductId and
												LeadActivity.DateUpdated >= @DateFrom and
												LeadActivity.DateUpdated <= @DateTo and
												Quote.IsDeleted = 0 and
												QuoteItem.IsDeleted = 0
									)
								)
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerDropOff,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from Quote
							inner join SoldLeadActivity on SoldLeadActivity.QuoteId = Quote.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join LeadActivity on LeadActivity.Id = SoldLeadActivity.LeadActivityId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							LeadActivity.UserId = Insurers_CTE.AgentId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.Id = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote
			from
				Insurers_CTE
			order by
				Channel,
				Campaign,
				Agent,
				Insurer,
				Product
		end
	else
		begin
			;with Insurers_CTE (InsurerId, CampaignId, Insurer, ProductId, Product, Campaign, ChannelId, AgentId, Channel, Agent)
			as 
			(
				select distinct
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Id,
					Product.Name,
					Campaign.Name,
					Channel.Id,
					[User].Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
					inner join Channel on Channel.Id = Campaign.ChannelId
					inner join [User] on [User].Id = LeadActivity.UserId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
					UserAuthorisationGroup.AuthorisationGroupId = 1 and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Id,
					Product.Name,
					Campaign.Name,
					Channel.Id,
					[User].Id,
					Channel.Name,
					Individual.Surname + ', ' + Individual.FirstName
				)
			select
				Channel,
				Campaign,
				Agent,
				Insurer,
				Product,
				(
					select
						isnull(count(distinct(LeadActivity.LeadId)), 0)
					from LeadActivity
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
				) Leads,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.Id = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) Quotes,
				(
					isnull(
						cast(
								(
									select
										cast(isnull(count(distinct(Quote.Id)), 0) as numeric(18, 2))
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
										inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
										inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
										inner join Product on Product.Id = Quote.ProductId
										inner join Organization on Organization.PartyId = Product.ProductOwnerId
										inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
										inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
										inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
									where
										LeadActivity.CampaignId = Insurers_CTE.CampaignId and
										LeadActivity.UserId = Insurers_CTE.AgentId and
										Organization.PartyId = Insurers_CTE.InsurerId and
										Product.Id = Insurers_CTE.ProductId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo and
										QuoteHeader.IsRerated = 0 and
										Quote.IsDeleted = 0 and
										QuoteItem.IsDeleted = 0
								)
					
								/
					
								(
									select
										nullif(cast(isnull(count(distinct(LeadActivity.LeadId)), 0) as numeric( 18, 2)), 0)
									from LeadActivity
									where
										LeadActivity.CampaignId = Insurers_CTE.CampaignId and
										LeadActivity.UserId = Insurers_CTE.AgentId and
										LeadActivity.DateUpdated >= @DateFrom and
										LeadActivity.DateUpdated <= @DateTo
								)

							as numeric(18,2)
						)
					, 0)
				) QuotesPerLead,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.Id = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) SecondLevelUnderwriting,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						LeadActivity.UserId = Insurers_CTE.AgentId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.Id = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteItem.IsDeleted = 0
				) Sales,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(distinct(Quote.Id)), 0) as numeric( 18, 2))
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) Closing,
				(
					isnull(
						cast
						(
							( 
								select
									cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
									inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									LeadActivity.UserId = Insurers_CTE.AgentId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.Id = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerClosing,
				(
					isnull(
						cast
						(
							
							(
								1
						
								-

								(
									( 
										select
											cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2))
										from LeadActivity
											inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
											inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Insurers_CTE.CampaignId and
											LeadActivity.UserId = Insurers_CTE.AgentId and
											Organization.PartyId = Insurers_CTE.InsurerId and
											Product.Id = Insurers_CTE.ProductId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0 and
											QuoteItem.IsDeleted = 0
									)

									/

									(
											select
												nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
											from LeadActivity
												inner join QuoteAcceptedLeadActivity on QuoteAcceptedLeadActivity.LeadActivityId = LeadActivity.Id
												inner join Quote on Quote.Id = QuoteAcceptedLeadActivity.QuoteId
												inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
												inner join Product on Product.Id = Quote.ProductId
												inner join Organization on Organization.PartyId = Product.ProductOwnerId
												inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
												inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
												inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
											where
												LeadActivity.CampaignId = Insurers_CTE.CampaignId and
												LeadActivity.UserId = Insurers_CTE.AgentId and
												Organization.PartyId = Insurers_CTE.InsurerId and
												Product.Id = Insurers_CTE.ProductId and
												LeadActivity.DateUpdated >= @DateFrom and
												LeadActivity.DateUpdated <= @DateTo and
												Quote.IsDeleted = 0 and
												QuoteItem.IsDeleted = 0
									)
								)
							)

							* 100

							as numeric( 18, 2)
						)
					, 0)
				) InsurerDropOff,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from Quote
							inner join SoldLeadActivity on SoldLeadActivity.QuoteId = Quote.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join LeadActivity on LeadActivity.Id = SoldLeadActivity.LeadActivityId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							LeadActivity.UserId = Insurers_CTE.AgentId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.Id = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote
			from
				Insurers_CTE
			order by
				Channel,
				Campaign,
				Agent,
				Insurer,
				Product
		end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_Header
	end
go

create procedure Report_CallCentre_InsurerData_Header
(
	@CampaignID int
)
as
	select top 1 Code Insurer, '' Color from Channel where IsDefault = 1
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetCampaignProductsByCoverId') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetCampaignProductsByCoverId
	end
go

create procedure Report_CallCentre_InsurerData_GetCampaignProductsByCoverId
(
	@CampaignIds varchar(50),
	@CoverId int,
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			select distinct
				ProductOwner.PartyId ProductOwnerId,
				ProductOwner.TradingName ProductOwnerName,
				ProductProvider.PartyId ProductProviderId,
				ProductProvider.TradingName ProductProviderName,
				Product.Id ProductId,
				Product.Name ProductName
			from Product
				inner join CoverDefinition on CoverDefinition.ProductId = Product.Id
				inner join Quote on Quote.ProductId = Product.Id
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
				inner join Organization ProductOwner on ProductOwner.PartyId = Product.ProductOwnerId
				inner join Organization ProductProvider on ProductProvider.PartyId = Product.ProductOwnerId
			where
				LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				CoverDefinition.CoverId = @CoverId and
				CoverDefinition.IsDeleted = 0 and
				Product.IsDeleted = 0
			group by
				ProductOwner.PartyId,
				ProductOwner.TradingName,
				ProductProvider.PartyId,
				ProductProvider.TradingName,
				Product.Id,
				Product.Name
		end
	else
		begin
			select distinct
				ProductOwner.PartyId ProductOwnerId,
				ProductOwner.TradingName ProductOwnerName,
				ProductProvider.PartyId ProductProviderId,
				ProductProvider.TradingName ProductProviderName,
				Product.Id ProductId,
				Product.Name ProductName
			from Product
				inner join CoverDefinition on CoverDefinition.ProductId = Product.Id
				inner join Quote on Quote.ProductId = Product.Id
				inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
				inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
				inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
				inner join Organization ProductOwner on ProductOwner.PartyId = Product.ProductOwnerId
				inner join Organization ProductProvider on ProductProvider.PartyId = Product.ProductOwnerId
			where
				--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
				LeadActivity.DateUpdated >= @DateFrom and
				LeadActivity.DateUpdated <= @DateTo and
				CoverDefinition.CoverId = @CoverId and
				CoverDefinition.IsDeleted = 0 and
				Product.IsDeleted = 0
			group by
				ProductOwner.PartyId,
				ProductOwner.TradingName,
				ProductProvider.PartyId,
				ProductProvider.TradingName,
				Product.Id,
				Product.Name
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetInsurerQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetInsurerQuoteStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetInsurerQuoteStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign, ProductId)
					as 
					(
						select distinct
							Organization.PartyId,
							Campaign.Id,
							Organization.RegisteredName,
							Product.Name,
							Campaign.Name,
							Product.Id
						from QuoteHeader
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
							inner join Channel on Channel.Id = Campaign.ChannelId
							inner join [User] on [User].Id = LeadActivity.UserId
							inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
							inner join UserIndividual on UserIndividual.UserId = [User].Id
							inner join Individual on Individual.PartyId = UserIndividual.IndividualId
						where
							LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
							Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
							CoverDefinition.CoverId = 218 and
							UserAuthorisationGroup.AuthorisationGroupId = 1 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Campaign.Id,
							Campaign.Name,
							Organization.PartyId,
							Organization.RegisteredName,
							Product.Id,
							Product.Name
						)
					select
						Insurer,
						Product,
						Campaign,
						(
							select
								isnull(count(distinct(Quote.Id)), 0)
							from LeadActivity							
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
								inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
								inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
								inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							where
								LeadActivity.CampaignId = Insurers_CTE.CampaignId and
								Organization.PartyId = Insurers_CTE.InsurerId and
								Product.ID = Insurers_CTE.ProductId and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) Quotes,
						(
							select
								isnull(count(distinct(Quote.Id)), 0)
							from LeadActivity
								inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
								inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
								inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
								inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
								inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							where
								LeadActivity.CampaignId = Insurers_CTE.CampaignId and
								Organization.PartyId = Insurers_CTE.InsurerId and
								Product.ID = Insurers_CTE.ProductId and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								Quote.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0
						) Sales,
						(
							cast
							(
								(
									( 
										select
											cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric(18, 0))
										from LeadActivity
											inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
											inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Insurers_CTE.CampaignId and
											Organization.PartyId = Insurers_CTE.InsurerId and
											Product.ID = Insurers_CTE.ProductId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0 and
											QuoteHeader.IsRerated = 0
									)

									/

									(
										select
											nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
										from LeadActivity
											inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
											inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
											inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Insurers_CTE.CampaignId and
											Organization.PartyId = Insurers_CTE.InsurerId and
											Product.ID = Insurers_CTE.ProductId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											QuoteHeader.IsRerated = 0 and
											Quote.IsDeleted = 0
									)
								)
								* 100
								as numeric( 18, 2)
							)
						) Closing,
						(
							select
								cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
							from
							(
								select
									cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.ID = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
								group by
									Quote.Id
							) x
						) AveragePremiumPerAcceptedQuote,
						(
							select
								cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
							from
							(
								select
									cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
								from QuoteHeader
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
									inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.ID = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
								group by
									Quote.Id
							) x
						) AveragePremiumPerQuote
					from
						Insurers_CTE
					order by
						Campaign,
						Insurer,
						Product
				end
			else
				begin
					;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign, ProductId)
					as
					(
						select distinct
							Organization.PartyId,
							Campaign.Id,
							Organization.RegisteredName,
							Product.Name,
							Campaign.Name,
							Product.Id
						from QuoteHeader
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
							inner join Channel on Channel.Id = Campaign.ChannelId
							inner join [User] on [User].Id = LeadActivity.UserId
							inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
							inner join UserIndividual on UserIndividual.UserId = [User].Id
							inner join Individual on Individual.PartyId = UserIndividual.IndividualId
						where
							LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
							Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
							UserAuthorisationGroup.AuthorisationGroupId = 1 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Campaign.Id,
							Campaign.Name,
							Organization.PartyId,
							Organization.RegisteredName,
							Product.Id,
							Product.Name
						)
					select
						Insurer,
						Product,
						Campaign,
						(
							select
								isnull(count(distinct(Quote.Id)), 0)
							from LeadActivity							
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
								inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
								inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
								inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							where
								LeadActivity.CampaignId = Insurers_CTE.CampaignId and
								Organization.PartyId = Insurers_CTE.InsurerId and
								Product.ID = Insurers_CTE.ProductId and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) Quotes,
						(
							select
								isnull(count(distinct(Quote.Id)), 0)
							from LeadActivity
								inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
								inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
								inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
								inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
								inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							where
								LeadActivity.CampaignId = Insurers_CTE.CampaignId and
								Organization.PartyId = Insurers_CTE.InsurerId and
								Product.ID = Insurers_CTE.ProductId and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								Quote.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0
						) Sales,
						(
							cast
							(
								(
									( 
										select
											cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric(18, 0))
										from LeadActivity
											inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
											inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Insurers_CTE.CampaignId and
											Organization.PartyId = Insurers_CTE.InsurerId and
											Product.ID = Insurers_CTE.ProductId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0 and
											QuoteHeader.IsRerated = 0
									)

									/

									(
										select
											nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
										from LeadActivity
											inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
											inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
											inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Insurers_CTE.CampaignId and
											Organization.PartyId = Insurers_CTE.InsurerId and
											Product.ID = Insurers_CTE.ProductId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											QuoteHeader.IsRerated = 0 and
											Quote.IsDeleted = 0
									)
								)
								* 100
								as numeric( 18, 2)
							)
						) Closing,
						(
							select
								cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
							from
							(
								select
									cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.ID = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
								group by
									Quote.Id
							) x
						) AveragePremiumPerAcceptedQuote,
						(
							select
								cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
							from
							(
								select
									cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
								from QuoteHeader
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
									inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.ID = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
								group by
									Quote.Id
							) x
						) AveragePremiumPerQuote
					from
						Insurers_CTE
					order by
						Campaign,
						Insurer,
						Product
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign, ProductId)
					as 
					(
						select distinct
							Organization.PartyId,
							Campaign.Id,
							Organization.RegisteredName,
							Product.Name,
							Campaign.Name,
							Product.Id
						from QuoteHeader
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
							inner join Channel on Channel.Id = Campaign.ChannelId
							inner join [User] on [User].Id = LeadActivity.UserId
							inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
							inner join UserIndividual on UserIndividual.UserId = [User].Id
							inner join Individual on Individual.PartyId = UserIndividual.IndividualId
						where
							--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
							Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
							CoverDefinition.CoverId = 218 and
							UserAuthorisationGroup.AuthorisationGroupId = 1 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Campaign.Id,
							Campaign.Name,
							Organization.PartyId,
							Organization.RegisteredName,
							Product.Id,
							Product.Name
						)
					select
						Insurer,
						Product,
						Campaign,
						(
							select
								isnull(count(distinct(Quote.Id)), 0)
							from LeadActivity							
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
								inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
								inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
								inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							where
								LeadActivity.CampaignId = Insurers_CTE.CampaignId and
								Organization.PartyId = Insurers_CTE.InsurerId and
								Product.ID = Insurers_CTE.ProductId and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) Quotes,
						(
							select
								isnull(count(distinct(Quote.Id)), 0)
							from LeadActivity
								inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
								inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
								inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
								inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
								inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							where
								LeadActivity.CampaignId = Insurers_CTE.CampaignId and
								Organization.PartyId = Insurers_CTE.InsurerId and
								Product.ID = Insurers_CTE.ProductId and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								Quote.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0
						) Sales,
						(
							cast
							(
								(
									( 
										select
											cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric(18, 0))
										from LeadActivity
											inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
											inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Insurers_CTE.CampaignId and
											Organization.PartyId = Insurers_CTE.InsurerId and
											Product.ID = Insurers_CTE.ProductId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0 and
											QuoteHeader.IsRerated = 0
									)

									/

									(
										select
											nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
										from LeadActivity
											inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
											inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
											inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Insurers_CTE.CampaignId and
											Organization.PartyId = Insurers_CTE.InsurerId and
											Product.ID = Insurers_CTE.ProductId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											QuoteHeader.IsRerated = 0 and
											Quote.IsDeleted = 0
									)
								)
								* 100
								as numeric( 18, 2)
							)
						) Closing,
						(
							select
								cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
							from
							(
								select
									cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.ID = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
								group by
									Quote.Id
							) x
						) AveragePremiumPerAcceptedQuote,
						(
							select
								cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
							from
							(
								select
									cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
								from QuoteHeader
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
									inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.ID = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
								group by
									Quote.Id
							) x
						) AveragePremiumPerQuote
					from
						Insurers_CTE
					order by
						Campaign,
						Insurer,
						Product
				end
			else
				begin
					;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign, ProductId)
					as 
					(
						select distinct
							Organization.PartyId,
							Campaign.Id,
							Organization.RegisteredName,
							Product.Name,
							Campaign.Name,
							Product.Id
						from QuoteHeader
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							inner join Product on Product.Id = Quote.ProductId
							inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
							inner join Campaign on Campaign.Id = LeadActivity.CampaignId
							inner join Channel on Channel.Id = Campaign.ChannelId
							inner join [User] on [User].Id = LeadActivity.UserId
							inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
							inner join UserIndividual on UserIndividual.UserId = [User].Id
							inner join Individual on Individual.PartyId = UserIndividual.IndividualId
						where
							--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
							Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
							UserAuthorisationGroup.AuthorisationGroupId = 1 and
							QuoteHeader.IsRerated = 0 and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Campaign.Id,
							Campaign.Name,
							Organization.PartyId,
							Organization.RegisteredName,
							Product.Id,
							Product.Name
						)
					select
						Insurer,
						Product,
						Campaign,
						(
							select
								isnull(count(distinct(Quote.Id)), 0)
							from LeadActivity							
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
								inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
								inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
								inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							where
								LeadActivity.CampaignId = Insurers_CTE.CampaignId and
								Organization.PartyId = Insurers_CTE.InsurerId and
								Product.ID = Insurers_CTE.ProductId and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) Quotes,
						(
							select
								isnull(count(distinct(Quote.Id)), 0)
							from LeadActivity
								inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
								inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
								inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
								inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
								inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
							where
								LeadActivity.CampaignId = Insurers_CTE.CampaignId and
								Organization.PartyId = Insurers_CTE.InsurerId and
								Product.ID = Insurers_CTE.ProductId and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								Quote.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0
						) Sales,
						(
							cast
							(
								(
									( 
										select
											cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric(18, 0))
										from LeadActivity
											inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
											inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
											inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Insurers_CTE.CampaignId and
											Organization.PartyId = Insurers_CTE.InsurerId and
											Product.ID = Insurers_CTE.ProductId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											Quote.IsDeleted = 0 and
											QuoteHeader.IsRerated = 0
									)

									/

									(
										select
											nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
										from LeadActivity
											inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
											inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
											inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
											inner join Product on Product.Id = Quote.ProductId
											inner join Organization on Organization.PartyId = Product.ProductOwnerId
											inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
											inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
											inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
										where
											LeadActivity.CampaignId = Insurers_CTE.CampaignId and
											Organization.PartyId = Insurers_CTE.InsurerId and
											Product.ID = Insurers_CTE.ProductId and
											LeadActivity.DateUpdated >= @DateFrom and
											LeadActivity.DateUpdated <= @DateTo and
											QuoteHeader.IsRerated = 0 and
											Quote.IsDeleted = 0
									)
								)
								* 100
								as numeric( 18, 2)
							)
						) Closing,
						(
							select
								cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
							from
							(
								select
									cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.ID = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
								group by
									Quote.Id
							) x
						) AveragePremiumPerAcceptedQuote,
						(
							select
								cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
							from
							(
								select
									cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
								from QuoteHeader
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
									inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.ID = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteItem.IsDeleted = 0
								group by
									Quote.Id
							) x
						) AveragePremiumPerQuote
					from
						Insurers_CTE
					order by
						Campaign,
						Insurer,
						Product
				end
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetAgeStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetAgeStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetAgeStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @AgeRangeTotal numeric(18, 2)
	declare @AgeRange18To20 numeric(18, 2)
	declare @AgeRange21To25 numeric(18, 2)
	declare @AgeRange26To30 numeric(18, 2)
	declare @AgeRange31To35 numeric(18, 2)
	declare @AgeRange36To40 numeric(18, 2)
	declare @AgeRange41To45 numeric(18, 2)
	declare @AgeRange46To50 numeric(18, 2)
	declare @AgeRange51To55 numeric(18, 2)
	declare @AgeRange56Plus numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@AgeRangeTotal = 
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo
						),
						@AgeRange18To20 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 18 and 20
						),
						@AgeRange21To25 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 21 and 25
						),
						@AgeRange26To30 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 26 and 30
						),
						@AgeRange31To35 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 31 and 35
						),
						@AgeRange36To40 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 36 and 40
						),
						@AgeRange41To45 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 41 and 45
						),
						@AgeRange46To50 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 46 and 50
						),
						@AgeRange51To55 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 51 and 55
						),
						@AgeRange56Plus =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) > 55
						)

					if @AgeRangeTotal = 0
						begin
							select @AgeRangeTotal = 1
						end
									
					select '18-20' Label, '18-20' Category, @AgeRange18To20 Total, (@AgeRange18To20 / @AgeRangeTotal * 100) [Percentage] union all
					select '21-25' Label, '21-25' Category, @AgeRange21To25 Total, (@AgeRange21To25 / @AgeRangeTotal * 100) [Percentage] union all
					select '26-30' Label, '26-30' Category, @AgeRange26To30 Total, (@AgeRange26To30 / @AgeRangeTotal * 100) [Percentage] union all
					select '31-35' Label, '31-35' Category, @AgeRange31To35 Total, (@AgeRange31To35 / @AgeRangeTotal * 100) [Percentage] union all
					select '36-40' Label, '36-40' Category, @AgeRange36To40 Total, (@AgeRange36To40 / @AgeRangeTotal * 100) [Percentage] union all
					select '41-45' Label, '41-45' Category, @AgeRange41To45 Total, (@AgeRange41To45 / @AgeRangeTotal * 100) [Percentage] union all
					select '46-50' Label, '46-50' Category, @AgeRange46To50 Total, (@AgeRange46To50 / @AgeRangeTotal * 100) [Percentage] union all
					select '51-55' Label, '51-55' Category, @AgeRange51To55 Total, (@AgeRange51To55 / @AgeRangeTotal * 100) [Percentage] union all
					select '56+' Label, '56+' Category, @AgeRange56Plus Total, (@AgeRange56Plus / @AgeRangeTotal * 100) [Percentage]
				end
			else
				begin
					select
						@AgeRangeTotal = 
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo
						),
						@AgeRange18To20 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 18 and 20
						),
						@AgeRange21To25 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 21 and 25
						),
						@AgeRange26To30 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 26 and 30
						),
						@AgeRange31To35 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 31 and 35
						),
						@AgeRange36To40 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 36 and 40
						),
						@AgeRange41To45 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 41 and 45
						),
						@AgeRange46To50 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 46 and 50
						),
						@AgeRange51To55 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 51 and 55
						),
						@AgeRange56Plus =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) > 55
						)

					if @AgeRangeTotal = 0
						begin
							select @AgeRangeTotal = 1
						end

					select '18-20' Label, '18-20' Category, @AgeRange18To20 Total, (@AgeRange18To20 / @AgeRangeTotal * 100) [Percentage] union all
					select '21-25' Label, '21-25' Category, @AgeRange21To25 Total, (@AgeRange21To25 / @AgeRangeTotal * 100) [Percentage] union all
					select '26-30' Label, '26-30' Category, @AgeRange26To30 Total, (@AgeRange26To30 / @AgeRangeTotal * 100) [Percentage] union all
					select '31-35' Label, '31-35' Category, @AgeRange31To35 Total, (@AgeRange31To35 / @AgeRangeTotal * 100) [Percentage] union all
					select '36-40' Label, '36-40' Category, @AgeRange36To40 Total, (@AgeRange36To40 / @AgeRangeTotal * 100) [Percentage] union all
					select '41-45' Label, '41-45' Category, @AgeRange41To45 Total, (@AgeRange41To45 / @AgeRangeTotal * 100) [Percentage] union all
					select '46-50' Label, '46-50' Category, @AgeRange46To50 Total, (@AgeRange46To50 / @AgeRangeTotal * 100) [Percentage] union all
					select '51-55' Label, '51-55' Category, @AgeRange51To55 Total, (@AgeRange51To55 / @AgeRangeTotal * 100) [Percentage] union all
					select '56+' Label, '56+' Category, @AgeRange56Plus Total, (@AgeRange56Plus / @AgeRangeTotal * 100) [Percentage]
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@AgeRangeTotal = 
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo
						),
						@AgeRange18To20 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 18 and 20
						),
						@AgeRange21To25 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 21 and 25
						),
						@AgeRange26To30 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 26 and 30
						),
						@AgeRange31To35 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 31 and 35
						),
						@AgeRange36To40 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 36 and 40
						),
						@AgeRange41To45 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 41 and 45
						),
						@AgeRange46To50 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 46 and 50
						),
						@AgeRange51To55 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 51 and 55
						),
						@AgeRange56Plus =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) > 55
						)

					if @AgeRangeTotal = 0
						begin
							select @AgeRangeTotal = 1
						end

					select '18-20' Label, '18-20' Category, @AgeRange18To20 Total, (@AgeRange18To20 / @AgeRangeTotal * 100) [Percentage] union all
					select '21-25' Label, '21-25' Category, @AgeRange21To25 Total, (@AgeRange21To25 / @AgeRangeTotal * 100) [Percentage] union all
					select '26-30' Label, '26-30' Category, @AgeRange26To30 Total, (@AgeRange26To30 / @AgeRangeTotal * 100) [Percentage] union all
					select '31-35' Label, '31-35' Category, @AgeRange31To35 Total, (@AgeRange31To35 / @AgeRangeTotal * 100) [Percentage] union all
					select '36-40' Label, '36-40' Category, @AgeRange36To40 Total, (@AgeRange36To40 / @AgeRangeTotal * 100) [Percentage] union all
					select '41-45' Label, '41-45' Category, @AgeRange41To45 Total, (@AgeRange41To45 / @AgeRangeTotal * 100) [Percentage] union all
					select '46-50' Label, '46-50' Category, @AgeRange46To50 Total, (@AgeRange46To50 / @AgeRangeTotal * 100) [Percentage] union all
					select '51-55' Label, '51-55' Category, @AgeRange51To55 Total, (@AgeRange51To55 / @AgeRangeTotal * 100) [Percentage] union all
					select '56+' Label, '56+' Category, @AgeRange56Plus Total, (@AgeRange56Plus / @AgeRangeTotal * 100) [Percentage]
				end
			else
				begin
					select
						@AgeRangeTotal = 
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo
						),
						@AgeRange18To20 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 18 and 20
						),
						@AgeRange21To25 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 21 and 25
						),
						@AgeRange26To30 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 26 and 30
						),
						@AgeRange31To35 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 31 and 35
						),
						@AgeRange36To40 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 36 and 40
						),
						@AgeRange41To45 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 41 and 45
						),
						@AgeRange46To50 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 46 and 50
						),
						@AgeRange51To55 =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) between 51 and 55
						),
						@AgeRange56Plus =
						(
							select
								count(LeadActivity.Id)
							from Lead
								inner join Individual on Individual.PartyId = Lead.PartyId
								inner join md.Gender on md.Gender.Id = Individual.GenderId
								inner join LeadActivity on LeadActivity.LeadId = Lead.Id
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
								inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
								inner join Product on Product.Id = Quote.ProductId
								inner join Organization on Organization.PartyId = Product.ProductOwnerId
							where
								--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
								Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0 and
								LeadActivity.DateUpdated >= @DateFrom and
								LeadActivity.DateUpdated <= @DateTo and
								datediff(year, Individual.DateOfBirth, getdate()) > 55
						)

					if @AgeRangeTotal = 0
						begin
							select @AgeRangeTotal = 1
						end
	
					select '18-20' Label, '18-20' Category, @AgeRange18To20 Total, (@AgeRange18To20 / @AgeRangeTotal * 100) [Percentage] union all
					select '21-25' Label, '21-25' Category, @AgeRange21To25 Total, (@AgeRange21To25 / @AgeRangeTotal * 100) [Percentage] union all
					select '26-30' Label, '26-30' Category, @AgeRange26To30 Total, (@AgeRange26To30 / @AgeRangeTotal * 100) [Percentage] union all
					select '31-35' Label, '31-35' Category, @AgeRange31To35 Total, (@AgeRange31To35 / @AgeRangeTotal * 100) [Percentage] union all
					select '36-40' Label, '36-40' Category, @AgeRange36To40 Total, (@AgeRange36To40 / @AgeRangeTotal * 100) [Percentage] union all
					select '41-45' Label, '41-45' Category, @AgeRange41To45 Total, (@AgeRange41To45 / @AgeRangeTotal * 100) [Percentage] union all
					select '46-50' Label, '46-50' Category, @AgeRange46To50 Total, (@AgeRange46To50 / @AgeRangeTotal * 100) [Percentage] union all
					select '51-55' Label, '51-55' Category, @AgeRange51To55 Total, (@AgeRange51To55 / @AgeRangeTotal * 100) [Percentage] union all
					select '56+' Label, '56+' Category, @AgeRange56Plus Total, (@AgeRange56Plus / @AgeRangeTotal * 100) [Percentage]
				end
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetClassUseStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetClassUseStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetClassUseStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 48 and --Class Of Use
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end	
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetCoverTypeStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetCoverTypeStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetCoverTypeStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 53 and --Type of Cover Motor
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end	
		end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetGenderStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetGenderStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetGenderStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = isnull(count(md.Gender.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.Gender.Name Label,
						md.Gender.Name Category,
						cast(count(md.Gender.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.Gender.Id,
						md.Gender.Name
				end
			else
				begin
					select
						@Total = isnull(count(md.Gender.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.Gender.Name Label,
						md.Gender.Name Category,
						cast(count(md.Gender.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.Gender.Id,
						md.Gender.Name
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = isnull(count(md.Gender.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.Gender.Name Label,
						md.Gender.Name Category,
						cast(count(md.Gender.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.Gender.Id,
						md.Gender.Name
				end
			else
				begin
					select
						@Total = isnull(count(md.Gender.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.Gender.Name Label,
						md.Gender.Name Category,
						cast(count(md.Gender.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.Gender.Id,
						md.Gender.Name
				end
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetGeneralStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetGeneralStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetGeneralStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @AverageNoClaimBonus numeric(18, 2)
	declare @AverageSumInsured numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@AverageNoClaimBonus = isnull(avg(cast(md.QuestionAnswer.Answer as numeric(18, 2))), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 43 and --No Claim Bonus
						QuoteHeader.IsRerated = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1 and
						isnumeric(md.QuestionAnswer.Answer) = 1

					select
						@AverageSumInsured = isnull(avg(cast(ProposalQuestionAnswer.Answer as numeric(18, 2))), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 102 and --Sum Insured
						QuoteHeader.IsRerated = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured

					union all

					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured
				end
			else
				begin
					select
						@AverageNoClaimBonus = isnull(avg(cast(md.QuestionAnswer.Answer as numeric(18, 2))), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 43 and --No Claim Bonus
						QuoteHeader.IsRerated = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1 and
						isnumeric(md.QuestionAnswer.Answer) = 1

					select
						@AverageSumInsured = isnull(avg(cast(ProposalQuestionAnswer.Answer as numeric(18, 2))), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 102 and --Sum Insured
						QuoteHeader.IsRerated = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured

					union all

					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@AverageNoClaimBonus = isnull(avg(cast(md.QuestionAnswer.Answer as numeric(18, 2))), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 43 and --No Claim Bonus
						QuoteHeader.IsRerated = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1 and
						isnumeric(md.QuestionAnswer.Answer) = 1

					select
						@AverageSumInsured = isnull(avg(cast(ProposalQuestionAnswer.Answer as numeric(18, 2))), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 102 and --Sum Insured
						QuoteHeader.IsRerated = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured

					union all

					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured
				end
			else
				begin
					select
						@AverageNoClaimBonus = isnull(avg(cast(md.QuestionAnswer.Answer as numeric(18, 2))), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 43 and --No Claim Bonus
						QuoteHeader.IsRerated = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1 and
						isnumeric(md.QuestionAnswer.Answer) = 1

					select
						@AverageSumInsured = isnull(avg(cast(ProposalQuestionAnswer.Answer as numeric(18, 2))), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 102 and --Sum Insured
						QuoteHeader.IsRerated = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured

					union all

					select
						@AverageNoClaimBonus AverageNoClaimBonus,
						@AverageSumInsured AverageSumInsured
				end
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetInsuranceTypeStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetInsuranceTypeStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetInsuranceTypeStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.LossHistoryQuestionAnswer.Id)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Label,
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Category,
						cast(count(md.LossHistoryQuestionAnswer.Id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.LossHistoryQuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.LossHistoryQuestionAnswer.Id)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Label,
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Category,
						cast(count(md.LossHistoryQuestionAnswer.Id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.LossHistoryQuestionAnswer.Answer
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.LossHistoryQuestionAnswer.Id)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Label,
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Category,
						cast(count(md.LossHistoryQuestionAnswer.Id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.LossHistoryQuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.LossHistoryQuestionAnswer.Id)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Label,
						case
							when md.LossHistoryQuestionAnswer.Answer = 'auto & home-yes' then 'Auto & Home'
							when md.LossHistoryQuestionAnswer.Answer = 'auto-yes' then 'Auto Only'
							when md.LossHistoryQuestionAnswer.Answer = 'home-yes' then 'Home Only'
							when md.LossHistoryQuestionAnswer.Answer = 'no' then 'None'
							else md.LossHistoryQuestionAnswer.Answer
						end Category,
						cast(count(md.LossHistoryQuestionAnswer.Id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join LossHistory on LossHistory.PartyId = Individual.PartyId
						inner join md.LossHistoryQuestionAnswer on md.LossHistoryQuestionAnswer.Id = LossHistory.CurrentlyInsuredId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.LossHistoryQuestionAnswer.Answer
				end
		end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetMaritalStatusStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetMaritalStatusStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetMaritalStatusStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = isnull(count(md.MaritalStatus.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.MaritalStatus.Name Label,
						md.MaritalStatus.Name Category,
						cast(count(md.MaritalStatus.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.MaritalStatus.Id,
						md.MaritalStatus.Name
				end
			else
				begin
					select
						@Total = isnull(count(md.MaritalStatus.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.MaritalStatus.Name Label,
						md.MaritalStatus.Name Category,
						cast(count(md.MaritalStatus.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.MaritalStatus.Id,
						md.MaritalStatus.Name
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = isnull(count(md.MaritalStatus.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.MaritalStatus.Name Label,
						md.MaritalStatus.Name Category,
						cast(count(md.MaritalStatus.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.MaritalStatus.Id,
						md.MaritalStatus.Name
				end
			else
				begin
					select
						@Total = isnull(count(md.MaritalStatus.Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.MaritalStatus.Name Label,
						md.MaritalStatus.Name Category,
						cast(count(md.MaritalStatus.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.MaritalStatus on md.MaritalStatus.Id = Individual.MaritalStatusId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.MaritalStatus.Id,
						md.MaritalStatus.Name
				end
		end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetProvinceStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetProvinceStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetProvinceStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = isnull(count([Address].Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.StateProvince.Name Label,
						md.StateProvince.Name Category,
						cast(count(md.StateProvince.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.StateProvince.Id,
						md.StateProvince.Name
				end
			else
				begin
					select
						@Total = isnull(count([Address].Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.StateProvince.Name Label,
						md.StateProvince.Name Category,
						cast(count(md.StateProvince.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.StateProvince.Id,
						md.StateProvince.Name
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = isnull(count([Address].Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.StateProvince.Name Label,
						md.StateProvince.Name Category,
						cast(count(md.StateProvince.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.StateProvince.Id,
						md.StateProvince.Name
				end
			else
				begin
					select
						@Total = isnull(count([Address].Id), 0)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo

					select
						md.StateProvince.Name Label,
						md.StateProvince.Name Category,
						cast(count(md.StateProvince.id) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join [Address] on [Address].PartyId = Individual.PartyId
						inner join md.StateProvince on md.StateProvince.Id = [Address].StateProvinceId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						[Address].AddressTypeId = 1 and --Physical Address
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo
					group by
						md.StateProvince.Id,
						md.StateProvince.Name
				end
		end
go

if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetVehicleColourStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetVehicleColourStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetVehicleColourStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(md.QuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1

					select
						md.QuestionAnswer.Answer Label,
						md.QuestionAnswer.Answer Category,
						cast(count(md.QuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
						inner join md.QuestionAnswer on md.QuestionAnswer.Id = ProposalQuestionAnswer.Answer
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 142 and --Vehicle Colour
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						isnumeric(ProposalQuestionAnswer.Answer) = 1
					group by
						md.QuestionAnswer.Answer
				end	
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerData_GetVehicleMakeStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerData_GetVehicleMakeStatistics
	end
go

create procedure Report_CallCentre_InsurerData_GetVehicleMakeStatistics
(
	@CampaignIds varchar(50),
	@ProductIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	declare @Total numeric(18, 2)

	if(len(@CampaignIds) > 0)
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(ProposalQuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null

					select
						ProposalQuestionAnswer.Answer Label,
						ProposalQuestionAnswer.Answer Category,
						cast(count(ProposalQuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null
					group by
						ProposalQuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(ProposalQuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null

					select
						ProposalQuestionAnswer.Answer Label,
						ProposalQuestionAnswer.Answer Category,
						cast(count(ProposalQuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null
					group by
						ProposalQuestionAnswer.Answer
				end
		end
	else
		begin
			if(len(@ProductIds) > 0)
				begin
					select
						@Total = count(ProposalQuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null

					select
						ProposalQuestionAnswer.Answer Label,
						ProposalQuestionAnswer.Answer Category,
						cast(count(ProposalQuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select * from fn_StringListToTable(@ProductIds)) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null
					group by
						ProposalQuestionAnswer.Answer
				end
			else
				begin
					select
						@Total = count(ProposalQuestionAnswer.Answer)
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null

					select
						ProposalQuestionAnswer.Answer Label,
						ProposalQuestionAnswer.Answer Category,
						cast(count(ProposalQuestionAnswer.Answer) as numeric(18, 2)) Total,
						isnull(count(Quote.Id), 0) / @Total  * 100 [Percentage]
					from Lead
						inner join Individual on Individual.PartyId = Lead.PartyId
						inner join md.Gender on md.Gender.Id = Individual.GenderId
						inner join LeadActivity on LeadActivity.LeadId = Lead.Id
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join ProposalHeader on ProposalHeader.Id = QuoteHeader.ProposalHeaderId
						inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = ProposalHeader.Id
						inner join md.Cover on md.Cover.Id = ProposalDefinition.CoverId
						inner join ProposalQuestionAnswer on ProposalQuestionAnswer.ProposalDefinitionId = ProposalDefinition.Id
						inner join QuestionDefinition on QuestionDefinition.Id = ProposalQuestionAnswer.QuestionDefinitionId
						inner join md.Question on md.Question.id = QuestionDefinition.QuestionId
					where
						--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
						Product.Id in (select p.Id from Product p inner join CoverDefinition cd on cd.ProductId = p.Id where cd.CoverId = 218 and p.Id = Product.Id) and
						ProposalDefinition.CoverId = 218 and --Motor
						md.Question.Id = 95 and --Vehicle Make
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0 and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						ProposalQuestionAnswer.Answer is not null
					group by
						ProposalQuestionAnswer.Answer
				end	
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerQuoteBreakdown_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerQuoteBreakdown_Header
	end
go

create procedure Report_CallCentre_InsurerQuoteBreakdown_Header
(
	@CampaignID int
)
as
	select top 1 Code Insurer, '' Color from Channel where IsDefault = 1
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics
	end
go

create procedure Report_CallCentre_InsurerQuoteBreakdown_GetInsurerQuoteStatistics
(
	@CampaignIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign, ProductId)
			as 
			(
				select distinct
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name,
					Product.Id
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
					inner join Channel on Channel.Id = Campaign.ChannelId
					inner join [User] on [User].Id = LeadActivity.UserId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
					UserAuthorisationGroup.AuthorisationGroupId = 1 and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Campaign.Id,
					Campaign.Name,
					Organization.PartyId,
					Organization.RegisteredName,
					Product.Id,
					Product.Name
				)
			select
				Insurer,
				Product,
				Campaign,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity							
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.ID = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.ID = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteHeader.IsRerated = 0
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric(18, 0))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.ID = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.ID = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0
							)
						)
						* 100
						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from LeadActivity
							inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.ID = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from QuoteHeader
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.ID = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerQuote
			from
				Insurers_CTE
			order by
				Campaign,
				Insurer,
				Product
		end
	else
		begin
			;with Insurers_CTE (InsurerId, CampaignId, Insurer, Product, Campaign, ProductId)
			as 
			(
				select distinct
					Organization.PartyId,
					Campaign.Id,
					Organization.RegisteredName,
					Product.Name,
					Campaign.Name,
					Product.Id
				from QuoteHeader
					inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
					inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
					inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
					inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					inner join Product on Product.Id = Quote.ProductId
					inner join md.ProductType on md.ProductType.Id = Product.ProductTypeId
					inner join Organization on Organization.PartyId = Product.ProductOwnerId
					inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
					inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
					inner join Campaign on Campaign.Id = LeadActivity.CampaignId
					inner join Channel on Channel.Id = Campaign.ChannelId
					inner join [User] on [User].Id = LeadActivity.UserId
					inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
					inner join UserIndividual on UserIndividual.UserId = [User].Id
					inner join Individual on Individual.PartyId = UserIndividual.IndividualId
				where
					--LeadActivity.CampaignId in (select * from fn_StringListToTable(@CampaignIds)) and
					UserAuthorisationGroup.AuthorisationGroupId = 1 and
					QuoteHeader.IsRerated = 0 and
					Quote.IsDeleted = 0 and
					QuoteItem.IsDeleted = 0
				group by
					Campaign.Id,
					Campaign.Name,
					Organization.PartyId,
					Organization.RegisteredName,
					Product.Id,
					Product.Name
				)
			select
				Insurer,
				Product,
				Campaign,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity							
						inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
						inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.ID = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
				) Quotes,
				(
					select
						isnull(count(distinct(Quote.Id)), 0)
					from LeadActivity
						inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
						inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductOwnerId
						inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
						inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
						inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
					where
						LeadActivity.CampaignId = Insurers_CTE.CampaignId and
						Organization.PartyId = Insurers_CTE.InsurerId and
						Product.ID = Insurers_CTE.ProductId and
						LeadActivity.DateUpdated >= @DateFrom and
						LeadActivity.DateUpdated <= @DateTo and
						Quote.IsDeleted = 0 and
						QuoteHeader.IsRerated = 0
				) Sales,
				(
					cast
					(
						(
							( 
								select
									cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric(18, 0))
								from LeadActivity
									inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
									inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
									inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.ID = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									Quote.IsDeleted = 0 and
									QuoteHeader.IsRerated = 0
							)

							/

							(
								select
									nullif(cast(isnull(count(distinct(QuoteHeader.Id)), 0) as numeric( 18, 2)), 0)
								from LeadActivity
									inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									inner join QuoteHeader on QuoteHeader.Id = QuotedLeadActivity.QuoteHeaderId
									inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
									inner join Product on Product.Id = Quote.ProductId
									inner join Organization on Organization.PartyId = Product.ProductOwnerId
									inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
									inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
									inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
								where
									LeadActivity.CampaignId = Insurers_CTE.CampaignId and
									Organization.PartyId = Insurers_CTE.InsurerId and
									Product.ID = Insurers_CTE.ProductId and
									LeadActivity.DateUpdated >= @DateFrom and
									LeadActivity.DateUpdated <= @DateTo and
									QuoteHeader.IsRerated = 0 and
									Quote.IsDeleted = 0
							)
						)
						* 100
						as numeric( 18, 2)
					)
				) Closing,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from LeadActivity
							inner join SoldLeadActivity on SoldLeadActivity.LeadActivityId = LeadActivity.Id
							inner join Quote on Quote.Id = SoldLeadActivity.QuoteId
							inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.ID = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerAcceptedQuote,
				(
					select
						cast(isnull(avg(TotalPremium), 0) as numeric(18, 2))
					from
					(
						select
							cast(isnull(sum(QuoteItem.Premium), 0) as numeric(18, 2)) as TotalPremium
						from QuoteHeader
							inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
							inner join QuotedLeadActivity on QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							inner join LeadActivity on LeadActivity.Id = QuotedLeadActivity.LeadActivityId
							inner join Product on Product.Id = Quote.ProductId
							inner join Organization on Organization.PartyId = Product.ProductOwnerId
							inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
							inner join CoverDefinition on CoverDefinition.Id = QuoteItem.CoverDefinitionId
							inner join md.Cover on md.Cover.Id = CoverDefinition.CoverId
						where
							LeadActivity.CampaignId = Insurers_CTE.CampaignId and
							Organization.PartyId = Insurers_CTE.InsurerId and
							Product.ID = Insurers_CTE.ProductId and
							LeadActivity.DateUpdated >= @DateFrom and
							LeadActivity.DateUpdated <= @DateTo and
							Quote.IsDeleted = 0 and
							QuoteItem.IsDeleted = 0
						group by
							Quote.Id
					) x
				) AveragePremiumPerQuote
			from
				Insurers_CTE
			order by
				Campaign,
				Insurer,
				Product
		end
go