
update [user] set UserName = 'notinuse_support@ip.co.za' where UserName = 'support@iplatform.co.za'

begin tran



-- Pass@123

declare @channel_id int
declare @user_group_id int
declare @user_email nvarchar(100)
declare @user_name nvarchar(100)
declare @user_surename nvarchar(100)
declare @displayName nvarchar(100)

set @user_email = 'support@iplatform.co.za'
set @user_name = 'Support'
set @user_surename = 'Support'
set @channel_id = 1
set @user_group_id = 5


set @displayName = CONCAT(@user_name,', ', @user_surename)

exec sp_executesql N'INSERT INTO [User] (Password, UserName, SystemId, IsDeleted, CreatedOn, ModifiedOn, IsApproved, IsActive, IsLoggedIn, LastLoggedInDate, LoggedInFromIP, LoggedInFromSystem, RegistrationToken, PasswordResetToken) VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13); select SCOPE_IDENTITY()',N'@p0 nvarchar(max) ,@p1 nvarchar(4000),@p2 uniqueidentifier,@p3 bit,@p4 datetime,@p5 datetime,@p6 bit,@p7 bit,@p8 bit,@p9 datetime,@p10 nvarchar(4000),@p11 nvarchar(4000),@p12 uniqueidentifier,@p13 uniqueidentifier',@p0=N'100:Mf63lBiHJTDz5dERnASryQ/gONk0pa3gD/NoQI0DrN9VODf3jLO7v91ajxMNIlUi6fd6XsTHkEAMs/IW9Lt7bBnhUyh+/x/W0P5yPEpM1f1jF9Nt7KUTDwKnsR6Pc0Mci6Tqk7HZ2ST7D7U5XXprAX9tE9emG87VuSAbqrWLL25A1LCwQBa245BBOlOZSSpSde9w80dU+RHH29yJCE7qzPCZEaXDOucgGI69sqeKAW8d0EMQEl0s0f3EdnXU0NIPRq9KAlomyMzr8/36KjG7JBh9yvoxrmflkjTLEhILG2p7uX9dqP6TfOTxRqFZ3IPnGo5bpyHA8h+jCRFc7L2hXw==:bngPAgZRq5im87E+jVI2RRQbySAIHc712gRBeVIEwnIrc9xUcIom+ZSsKOV6JcaUvM3CsLd5fFIwZZle8bZSCMy1Josf6eefoDn/G3hEyouXljjS5o8PlR2NMklMSieMguVA7qsUUSmuVVW3i1xSJww4kBuknSTz6GnrlhHZsfInrU7iWoL4JDNmAl8fHnoWoYP6JFundT+7w6bm1weGauJ4y+CVgIxvOUXXj9z8akrTG4rJjPRKy6nVELNfBsIYCXpAeH0cActXzGl7bVMI8MQ6FPkQ9U+nVygaEauPOMvekkALwrA3yB2KWSDAL/YYI5N+Op1yd9HTov3rIrDkkA==',@p1=@user_email,@p2='00000000-0000-0000-0000-000000000000',@p3=0,@p4='2016-11-04 11:01:55',@p5='2016-11-04 11:01:55',@p6=1,@p7=1,@p8=0,@p9=NULL,@p10=NULL,@p11=NULL,@p12='00000000-0000-0000-0000-000000000000',@p13=NULL

declare @user_id int
select @user_id = id from [user] where UserName = @user_email

exec sp_executesql N'INSERT INTO UserChannel (CreatedAt, ModifiedAt, IsDefault, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3, @p4, @p5); select SCOPE_IDENTITY()',N'@p0 datetime,@p1 datetime,@p2 bit,@p3 bit,@p4 int,@p5 int',@p0='2016-11-04 11:01:55',@p1='2016-11-04 11:01:55',@p2=0,@p3=0,@p4=@user_id,@p5=@channel_id
exec sp_executesql N'INSERT INTO ContactDetail ([Work], Direct, Cell, Home, Fax, Email, Website, Facebook, Twitter, LinkedIn, Skype, IsDeleted) VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11); select SCOPE_IDENTITY()',N'@p0 nvarchar(4000),@p1 nvarchar(4000),@p2 nvarchar(4000),@p3 nvarchar(4000),@p4 nvarchar(4000),@p5 nvarchar(4000),@p6 nvarchar(4000),@p7 nvarchar(4000),@p8 nvarchar(4000),@p9 nvarchar(4000),@p10 nvarchar(4000),@p11 bit',@p0=NULL,@p1=NULL,@p2=NULL,@p3=NULL,@p4=NULL,@p5=@user_email,@p6=NULL,@p7=NULL,@p8=NULL,@p9=NULL,@p10=NULL,@p11=0

declare @contact_detail_id int
select @contact_detail_id = id from ContactDetail where Email = @user_email

exec sp_executesql N'INSERT INTO Party (DisplayName, ExternalReference, PartyTypeId, DateCreated, DateUpdated, MasterId, IsDeleted, ContactDetailId, ChannelId) VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8); select SCOPE_IDENTITY()',N'@p0 nvarchar(4000),@p1 nvarchar(4000),@p2 int,@p3 datetime,@p4 datetime,@p5 int,@p6 bit,@p7 int,@p8 int',@p0=@displayName ,@p1=NULL,@p2=1,@p3='2016-11-04 11:01:55',@p4='2016-11-04 11:01:55',@p5=0,@p6=0,@p7=@contact_detail_id,@p8=@channel_id

declare @party_id int
select @party_id = id from [party] where DisplayName = @displayName

exec sp_executesql N'INSERT INTO Individual (FirstName, MiddleName, Surname, PreferredName, TitleId, GenderId, DateOfBirth, IdentityNo, PassportNo, ExternalReference, MaritalStatusId, LanguageId, AnyJudgements, AnyJudgementsReason, UnderAdministrationOrDebtReview, UnderAdministrationOrDebtReviewReason, BeenSequestratedOrLiquidated, BeenSequestratedOrLiquidatedReason, InsurancePolicyCancelledOrRenewalRefused, InsurancePolicyCancelledOrRenewalRefusedReason, PinNo, OccupationId, PartyId) VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18, @p19, @p20, @p21, @p22)',N'@p0 nvarchar(4000),@p1 nvarchar(4000),@p2 nvarchar(4000),@p3 nvarchar(4000),@p4 int,@p5 int,@p6 datetime,@p7 nvarchar(4000),@p8 nvarchar(4000),@p9 nvarchar(4000),@p10 int,@p11 int,@p12 bit,@p13 nvarchar(4000),@p14 bit,@p15 nvarchar(4000),@p16 bit,@p17 nvarchar(4000),@p18 bit,@p19 nvarchar(4000),@p20 nvarchar(4000),@p21 int,@p22 int',@p0=@user_name,@p1=NULL,@p2=@user_surename,@p3=NULL,@p4=NULL,@p5=NULL,@p6=NULL,@p7=NULL,@p8=NULL,@p9=NULL,@p10=NULL,@p11=NULL,@p12=0,@p13=NULL,@p14=0,@p15=NULL,@p16=0,@p17=NULL,@p18=0,@p19=NULL,@p20=NULL,@p21=NULL,@p22=@party_id
exec sp_executesql N'INSERT INTO Lead (ExternalReference, LeadStatusId, IsDeleted, PartyId, LeadQualityId) VALUES (@p0, @p1, @p2, @p3, @p4); select SCOPE_IDENTITY()',N'@p0 nvarchar(4000),@p1 int,@p2 bit,@p3 int,@p4 int',@p0=NULL,@p1=1,@p2=0,@p3=@party_id,@p4=NULL
exec sp_executesql N'INSERT INTO UserIndividual (CreatedAt, ModifiedAt, IsDeleted, UserId, IndividualId) VALUES (@p0, @p1, @p2, @p3, @p4); select SCOPE_IDENTITY()',N'@p0 datetime,@p1 datetime,@p2 bit,@p3 int,@p4 int',@p0='2016-11-04 11:01:55',@p1='2016-11-04 11:01:55',@p2=0,@p3=@user_id,@p4=@party_id
exec sp_executesql N'INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, IsDeleted, UserId, ChannelId) VALUES (@p0, @p1, @p2, @p3); select SCOPE_IDENTITY()',N'@p0 int,@p1 bit,@p2 int,@p3 int',@p0=@user_group_id,@p1=0,@p2=@user_id,@p3=@channel_id

commit 

-- select * from [user] order by 1 desc
--update [user] set UserName = 'Dhavendran.Sigamoney1@aig.com' where UserName = 'Dhavendran.Sigamoney@aig.com'



-----------------------



declare @userdId int
select @userdId = id from [user] where UserName = @user_email

-- select * from UserChannel

delete from UserChannel where UserId = @userdId

insert into UserChannel 
select '2016-02-02 00:00:00.000' as CreatedAt, '2016-02-02 00:00:00.000' as ModifiedAt, @userdId as [UserId] , Id as [ChannelId],0,0 
from Channel 
order by Id

update UserChannel set IsDefault = 1 where UserId = @userdId and ChannelId = 7

--UserAuthorisationGroup 

delete from UserAuthorisationGroup where UserId = @userdId

insert into UserAuthorisationGroup 
select 5, @userdId as [UserId] , Id as [ChannelId],0
from Channel 
order by Id

