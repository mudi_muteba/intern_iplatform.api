declare @channelCode nvarchar(10)

SELECT @channelCode = Code FROM Channel WHERE IsDefault = 1 AND IsDeleted <> 1;
if @channelCode = 'AIG'
BEGIN
DELETE FROM [VehicleGuideSetting] where [ChannelId] = 1
INSERT [dbo].[VehicleGuideSetting] ([IsDeleted], [ChannelId], [CountryId], [ApiKey], [Email], [MMBookEnabled], [LightstoneEnabled], [LSA_UserId], [LSA_ContractId], [LSA_CustomerId], [LSA_PackageId], [LSA_UserName], [LSA_Password], [KeAutoKey], [KeAutoSecret], [Evalue8Username], [Evalue8Password], [Evalue8Enabled], [TransUnionAutoEnabled]) VALUES ( 0, 1, 197, N'2kw9a6njlSxqG7wRYYzHIU1m4Hee9UJZ23Y0Z99aSwo', N'iguide-root@iplatform.co.za', 1, 0, N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'VIGeneric', N'testpassword', N'', N'', NULL, NULL, 0, 1)

update UserChannel set IsDefault = 0 
update UserChannel set IsDefault = 1 where ChannelId = 1

END
