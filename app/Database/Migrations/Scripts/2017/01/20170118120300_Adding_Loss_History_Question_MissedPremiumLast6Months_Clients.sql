DECLARE @ClientCode VARCHAR(50)
SET @ClientCode = 'IP'
  
if not exists(select * from [LossHistoryQuestionDefinition] where LossHistoryQuestionDefinitionId=16 and ClientCode = @ClientCode)
BEGIN
	INSERT INTO [dbo].[LossHistoryQuestionDefinition] ([DisplayName] ,[LossHistoryQuestionDefinitionId] ,[ClientCode] ,[VisibleIndex] ,[RequierdForQuote] ,[RatingFactor] ,[IsReadOnly] ,[ToolTip] ,[DefaultValue] ,[RegexPattern] ,[LossHistoryQuestionGroupId] ,[QuestionTypeId]) 
		VALUES ('Missed Premium Last 6 Months' ,16 ,@ClientCode, 7 ,0 ,1 ,0 ,'Have you missed a premium in the last 6 months?','' ,''  ,3 ,1 ) 
	
	Print('Added Missed Premium Last 6 Months Question for client ' + @ClientCode)
END

SET @ClientCode = 'Hollard'
  
if not exists(select * from [LossHistoryQuestionDefinition] where LossHistoryQuestionDefinitionId=16 and ClientCode = @ClientCode)
BEGIN
	INSERT INTO [dbo].[LossHistoryQuestionDefinition] ([DisplayName] ,[LossHistoryQuestionDefinitionId] ,[ClientCode] ,[VisibleIndex] ,[RequierdForQuote] ,[RatingFactor] ,[IsReadOnly] ,[ToolTip] ,[DefaultValue] ,[RegexPattern] ,[LossHistoryQuestionGroupId] ,[QuestionTypeId]) 
		VALUES ('Missed Premium Last 6 Months' ,16 ,@ClientCode, 7 ,0 ,1 ,0 ,'Have you missed a premium in the last 6 months?','' ,''  ,3 ,1 ) 
	
	Print('Added Missed Premium Last 6 Months Question for client ' + @ClientCode)
END

SET @ClientCode = 'KingPrice'
  
if not exists(select * from [LossHistoryQuestionDefinition] where LossHistoryQuestionDefinitionId=16 and ClientCode = @ClientCode)
BEGIN
	INSERT INTO [dbo].[LossHistoryQuestionDefinition] ([DisplayName] ,[LossHistoryQuestionDefinitionId] ,[ClientCode] ,[VisibleIndex] ,[RequierdForQuote] ,[RatingFactor] ,[IsReadOnly] ,[ToolTip] ,[DefaultValue] ,[RegexPattern] ,[LossHistoryQuestionGroupId] ,[QuestionTypeId]) 
		VALUES ('Missed Premium Last 6 Months' ,16 ,@ClientCode, 7 ,0 ,1 ,0 ,'Have you missed a premium in the last 6 months?','' ,''  ,3 ,1 ) 
	
	Print('Added Missed Premium Last 6 Months Question for client ' + @ClientCode)
END

SET @ClientCode = 'PSG'
  
if not exists(select * from [LossHistoryQuestionDefinition] where LossHistoryQuestionDefinitionId=16 and ClientCode = @ClientCode)
BEGIN
	INSERT INTO [dbo].[LossHistoryQuestionDefinition] ([DisplayName] ,[LossHistoryQuestionDefinitionId] ,[ClientCode] ,[VisibleIndex] ,[RequierdForQuote] ,[RatingFactor] ,[IsReadOnly] ,[ToolTip] ,[DefaultValue] ,[RegexPattern] ,[LossHistoryQuestionGroupId] ,[QuestionTypeId]) 
		VALUES ('Missed Premium Last 6 Months' ,16 ,@ClientCode, 7 ,0 ,1 ,0 ,'Have you missed a premium in the last 6 months?','' ,''  ,3 ,1 ) 
	
	Print('Added Missed Premium Last 6 Months Question for client ' + @ClientCode)
END

SET @ClientCode = 'Telesure'
  
if not exists(select * from [LossHistoryQuestionDefinition] where LossHistoryQuestionDefinitionId=16 and ClientCode = @ClientCode)
BEGIN
	INSERT INTO [dbo].[LossHistoryQuestionDefinition] ([DisplayName] ,[LossHistoryQuestionDefinitionId] ,[ClientCode] ,[VisibleIndex] ,[RequierdForQuote] ,[RatingFactor] ,[IsReadOnly] ,[ToolTip] ,[DefaultValue] ,[RegexPattern] ,[LossHistoryQuestionGroupId] ,[QuestionTypeId]) 
		VALUES ('Missed Premium Last 6 Months' ,16 ,@ClientCode, 7 ,0 ,1 ,0 ,'Have you missed a premium in the last 6 months?','' ,''  ,3 ,1 ) 
	
	Print('Added Missed Premium Last 6 Months Question for client ' + @ClientCode)
END


SET @ClientCode = 'UAP'
  
if not exists(select * from [LossHistoryQuestionDefinition] where LossHistoryQuestionDefinitionId=16 and ClientCode = @ClientCode)
BEGIN
	INSERT INTO [dbo].[LossHistoryQuestionDefinition] ([DisplayName] ,[LossHistoryQuestionDefinitionId] ,[ClientCode] ,[VisibleIndex] ,[RequierdForQuote] ,[RatingFactor] ,[IsReadOnly] ,[ToolTip] ,[DefaultValue] ,[RegexPattern] ,[LossHistoryQuestionGroupId] ,[QuestionTypeId]) 
		VALUES ('Missed Premium Last 6 Months' ,16 ,@ClientCode, 7 ,0 ,1 ,0 ,'Have you missed a premium in the last 6 months?','' ,''  ,3 ,1 ) 
	
	Print('Added Missed Premium Last 6 Months Question for client ' + @ClientCode)
END



