Declare @rootid int,@adminid int,@supportid int, @defaultChannelId int;

SELECT @rootid = Id from [User] where username = 'root@iplatform.co.za' AND IsDeleted != 1
SELECT @adminid = Id from [User] where username = 'admin@iplatform.co.za' AND IsDeleted != 1
SELECT @supportid = Id from [User] where username = 'support@iplatform.co.za' AND IsDeleted != 1

SELECT top 1 @defaultChannelId = Id FROM Channel WHERE IsDefault = 1 AND IsDeleted != 1

IF(@rootid> 0)
BEGIN
	IF not exists(select * from UserAuthorisationGroup WHERE ChannelId = @defaultChannelId and AuthorisationGroupId = 6 AND userid = @rootid  AND IsDeleted != 1)
	BEGIN
		INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, UserId, ChannelId, IsDeleted)
		VALUES (6, @rootid,@defaultChannelId, 0 )
	END
	IF not exists(select * from UserAuthorisationGroup WHERE ChannelId = @defaultChannelId and AuthorisationGroupId = 7 AND userid = @rootid AND IsDeleted != 1)
	BEGIN
		INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, UserId, ChannelId, IsDeleted)
		VALUES (7, @rootid,@defaultChannelId, 0 )
	END
END


IF(@adminid> 0)
BEGIN
	IF not exists(select * from UserAuthorisationGroup WHERE ChannelId = @defaultChannelId and AuthorisationGroupId = 6 AND userid = @adminid AND IsDeleted != 1)
	BEGIN
		INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, UserId, ChannelId, IsDeleted)
		VALUES (6, @adminid,@defaultChannelId, 0 )
	END
	IF not exists(select * from UserAuthorisationGroup WHERE ChannelId = @defaultChannelId and AuthorisationGroupId = 7 AND userid = @adminid AND IsDeleted != 1)
	BEGIN
		INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, UserId, ChannelId, IsDeleted)
		VALUES (7, @adminid,@defaultChannelId, 0 )
	END
END

IF(@supportid> 0)
BEGIN
	IF not exists(select * from UserAuthorisationGroup WHERE ChannelId = @defaultChannelId and AuthorisationGroupId = 6 AND userid = @supportid AND IsDeleted != 1)
	BEGIN
		INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, UserId, ChannelId, IsDeleted)
		VALUES (6, @supportid,@defaultChannelId, 0 )
	END
	IF not exists(select * from UserAuthorisationGroup WHERE ChannelId = @defaultChannelId and AuthorisationGroupId = 7 AND userid = @supportid AND IsDeleted != 1)
	BEGIN
		INSERT INTO UserAuthorisationGroup (AuthorisationGroupId, UserId, ChannelId, IsDeleted)
		VALUES (7, @supportid,@defaultChannelId, 0 )
	END
END