if exists (select * from sys.objects where object_id = object_id(N'Reports_FuneralQuote_ExtendedFamily') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_FuneralQuote_ExtendedFamily
	end
go

create procedure Reports_FuneralQuote_ExtendedFamily
(
	@ProposalHeaderId int
)
as
	select distinct
		AM.Initials + ' ' + AM.Surname Name,
		md.Gender.Name Gender,
		cast(AM.DateOfBirth as date) DateOfBirth,
		QIB.Relationship Relationship,
		QIB.Total Premium,
		AM.Initials + ' ' + AM.Surname +', born ' + cast(cast(AM.DateOfBirth as date) as varchar) + ', a ' + LOWER(QIB.Relationship) + ' of the main member.' [Description]
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = Proposalheader.Id
		inner join AdditionalMembers AM on AM.ProposalDefinitionId = ProposalDefinition.Id
		inner join md.Gender on md.Gender.Id = AM.GenderId
		inner join md.MemberRelationship on md.MemberRelationship.Id = AM.MemberRelationshipId
		inner join QuoteHeader QH on QH.ProposalHeaderId = ProposalHeader.Id
		inner join Quote Q on Q.QuoteHeaderId = QH.Id
		inner join QuoteItem QI on QI.QuoteId = Q.Id
		inner join QuoteItemBreakDown QIB on QIB.QuoteItemId = QI.Id AND AM.Id = QIB.AdditionalMembersId
	where
		Proposalheader.Id = @ProposalHeaderId AND
		md.MemberRelationship.Id not in (1, 12, 21) and
		ProposalDefinition.IsDeleted = 0 and
		QH.IsRerated = 0 and
		QH.IsDeleted = 0 and
		Q.IsDeleted = 0 and
		QI.IsDeleted = 0 and
		QIB.IsDeleted = 0 and
		AM.IsDeleted = 0
go