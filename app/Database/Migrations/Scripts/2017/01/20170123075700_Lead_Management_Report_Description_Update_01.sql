alter table [Report]
	alter column [Info] varchar(max)
go

alter table [Report]
	alter column Description varchar(max)
go

if not exists(select * from Report where Name = 'Lead Management')
	begin
		insert into Report (ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile,  IsVisibleInReportViewer, IsVisibleInScheduler, IsActive)
		values (2, 2, 3, 'Lead Management', 'This report displays the performance of call centre agents based on call centre code selections made during calls to assigned leads.', 'LeadManagement.trdx',  1, 1, 1)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 2,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Lead Management',
			[Description] = 'This report displays the performance of call centre agents based on call centre code selections made during calls to assigned leads.',
			SourceFile = 'LeadManagement.trdx',
			IsVisibleInReportViewer = 1,
			IsVisibleInScheduler = 1,
			IsActive = 1,
			Info = '
				This report uses the default channel of this iPlatform instance and whatever campaigns were selected as criteria inside the selected date range. It lists call centre related statistical data for each call centre agent assigned to these campaigns. 
				The statuses are broken down below, toggled by the call centre agents on the lead screen using the "Lead Contacted" and "Lead Not Contacted" menu options.<br /><br />

				<ul>
					<li>
						<strong>Agent</strong>
						<ul><li>A call centre agent assigned to this campaign</li></ul>
					</li>
					<li>
						<strong>Campaign</strong>
						<ul><li>A campaign part of the selected campaigns in the criteria</li></ul>
					</li>
					<li>
						<strong>Received</strong>
						<ul>
							<li>Total amount of leads assigned to a Call Centre Agent via the Call Centre queue (Next Lead Button on Dashboard)</li>
						</ul>
					</li>
					<li>
						<strong>Contacted</strong>
						<ul>
							<li>Sold</li>
							<li>Not Interested</li>
							<li>Call Back Later</li>
							<li>Wrong Number</li>
							<li>Language Barrier</li>
							<li>Requested Product Information</li>
							<li>Unavailable</li>
							<li>Uninsurable</li>
							<li>Not Marketable</li>
							<li>Duplicate Lead</li>

							<li>Outstanding Service Query</li>
							<li>Outstanding Claim</li>
							<li>Claim Rejected</li>
							<li>Existing Coverage</li>
							<li>Presentation</li>
							<li>No Presentation</li>
							<li>Unemployed</li>
							<li>Hung Up</li>
							<li>Duplicate Lead</li>
							<li>Too Expensive</li>
						</ul>
					</li>
					<li>
						<strong>Uncontactable</strong>
						<ul>
							<li>Deceased</li>
							<li>Contact Number Invalid</li>
							<li>Maximum Contact Attempts Reached</li>
							<li>Fax Machine Contact Number</li>
							<li>Similar Campaign</li>
							<li>Answering Machine</li>
							<li>Line Engaged</li>
							<li>Number Does Not Exist</li>
						</ul>
					</li>
					<li>
						<strong>Pending</strong>
						<ul>
							<li>Call Back Later</li>
							<li>Language Barrier</li>
							<li>Requested Product Information</li>
							<li>Unavailable</li>
							<li>Answering Machine</li>
							<li>Line Engaged</li>
						</ul>
					</li>
					<li>
						<strong>Completed</strong>
						<ul>
							<li>Sold (Automatically set once quote is marked as sold)</li>
						</ul>
					</li>
					<li>
						<strong>Unactioned</strong>
						<ul>
							<li>Leads allocated to call centre agent but no action has yet been taken.</li>
						</ul>
					</li>
				</ul>
			'
		where
			Name = 'Lead Management'
	end
go