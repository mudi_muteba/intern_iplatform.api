if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_Header
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_Header
(
	@CampaignID int
)
as
	select top 1 Code Insurer, '' Color from Channel where IsDefault = 1
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_GetUploadStatistics
(
	@CampaignIds varchar(50),
	@InsurerIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			if(len(@InsurerIds) > 0)
				begin
					select
						Organization.TradingName [Insurer],
						Product.Name [Product],
						(
							select top 1
								Individual.Surname + ', ' + Individual.FirstName
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join [User] on [User].Id = LeadActivity.UserId
								inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
								inner join UserIndividual on UserIndividual.UserId = [User].Id
								inner join Individual on Individual.PartyId = UserIndividual.IndividualId
							where
								QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							order by
								LeadActivity.DateUpdated desc
						) Agent,
						(
							select top 1
								Individual.Surname + ', ' + Individual.FirstName
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join Lead on Lead.Id = LeadActivity.LeadId
								inner join Individual on Individual.PartyId = Lead.PartyId
							where
								QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							order by
								LeadActivity.DateUpdated desc
						) Client,
						QuoteUploadLog.InsurerReference [InsurerReference],
						QuoteUploadLog.DateCreated [UploadDate],
						QuoteUploadLog.Premium [PremiumValue]
					from QuoteUploadLog
						inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductProviderId
					where
						(
							select
								LeadActivity.CampaignId
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
							where
								QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						) in (select * from fn_StringListToTable(@CampaignIds)) and
						Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
						QuoteUploadLog.DateCreated >= @DateFrom and
						QuoteUploadLog.DateCreated <= @DateTo and
						QuoteUploadLog.IsDeleted = 0 and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
					order by
						Organization.TradingName,
						Product.Name,
						QuoteUploadLog.DateCreated
				end
			else
				begin
					select
						Organization.TradingName [Insurer],
						Product.Name [Product],
						(
							select top 1
								Individual.Surname + ', ' + Individual.FirstName
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join [User] on [User].Id = LeadActivity.UserId
								inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
								inner join UserIndividual on UserIndividual.UserId = [User].Id
								inner join Individual on Individual.PartyId = UserIndividual.IndividualId
							where
								QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							order by
								LeadActivity.DateUpdated desc
						) Agent,
						(
							select top 1
								Individual.Surname + ', ' + Individual.FirstName
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join Lead on Lead.Id = LeadActivity.LeadId
								inner join Individual on Individual.PartyId = Lead.PartyId
							where
								QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							order by
								LeadActivity.DateUpdated desc
						) Client,
						QuoteUploadLog.InsurerReference [InsurerReference],
						QuoteUploadLog.DateCreated [UploadDate],
						QuoteUploadLog.Premium [PremiumValue]
					from QuoteUploadLog
						inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductProviderId
					where
						(
							select
								LeadActivity.CampaignId
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
							where
								QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						) in (select * from fn_StringListToTable(@CampaignIds)) and
						--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
						QuoteUploadLog.DateCreated >= @DateFrom and
						QuoteUploadLog.DateCreated <= @DateTo and
						QuoteUploadLog.IsDeleted = 0 and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
					order by
						Organization.TradingName,
						Product.Name,
						QuoteUploadLog.DateCreated
				end
		end
	else
		begin
			if(len(@InsurerIds) > 0)
				begin
					select
						Organization.TradingName [Insurer],
						Product.Name [Product],
						(
							select top 1
								Individual.Surname + ', ' + Individual.FirstName
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join [User] on [User].Id = LeadActivity.UserId
								inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
								inner join UserIndividual on UserIndividual.UserId = [User].Id
								inner join Individual on Individual.PartyId = UserIndividual.IndividualId
							where
								QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							order by
								LeadActivity.DateUpdated desc
						) Agent,
						(
							select top 1
								Individual.Surname + ', ' + Individual.FirstName
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join Lead on Lead.Id = LeadActivity.LeadId
								inner join Individual on Individual.PartyId = Lead.PartyId
							where
								QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							order by
								LeadActivity.DateUpdated desc
						) Client,
						QuoteUploadLog.InsurerReference [InsurerReference],
						QuoteUploadLog.DateCreated [UploadDate],
						QuoteUploadLog.Premium [PremiumValue]
					from QuoteUploadLog
						inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductProviderId
					where
						--(
						--	select
						--		LeadActivity.CampaignId
						--	from LeadActivity
						--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						--	where
						--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						--) in (select * from fn_StringListToTable(@CampaignIds)) and
						Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
						QuoteUploadLog.DateCreated >= @DateFrom and
						QuoteUploadLog.DateCreated <= @DateTo and
						QuoteUploadLog.IsDeleted = 0 and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
					order by
						Organization.TradingName,
						Product.Name,
						QuoteUploadLog.DateCreated
				end
			else
				begin
					select
						Organization.TradingName [Insurer],
						Product.Name [Product],
						(
							select top 1
								Individual.Surname + ', ' + Individual.FirstName
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join [User] on [User].Id = LeadActivity.UserId
								inner join UserAuthorisationGroup on UserAuthorisationGroup.UserId = [User].Id
								inner join UserIndividual on UserIndividual.UserId = [User].Id
								inner join Individual on Individual.PartyId = UserIndividual.IndividualId
							where
								QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							order by
								LeadActivity.DateUpdated desc
						) Agent,
						(
							select top 1
								Individual.Surname + ', ' + Individual.FirstName
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								inner join Lead on Lead.Id = LeadActivity.LeadId
								inner join Individual on Individual.PartyId = Lead.PartyId
							where
								QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
							order by
								LeadActivity.DateUpdated desc
						) Client,
						QuoteUploadLog.InsurerReference [InsurerReference],
						QuoteUploadLog.DateCreated [UploadDate],
						QuoteUploadLog.Premium [PremiumValue]
					from QuoteUploadLog
						inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductProviderId
					where
						--(
						--	select
						--		LeadActivity.CampaignId
						--	from LeadActivity
						--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						--	where
						--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						--) in (select * from fn_StringListToTable(@CampaignIds)) and
						--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
						QuoteUploadLog.DateCreated >= @DateFrom and
						QuoteUploadLog.DateCreated <= @DateTo and
						QuoteUploadLog.IsDeleted = 0 and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
					order by
						Organization.TradingName,
						Product.Name,
						QuoteUploadLog.DateCreated
				end
		end
go


if exists (select * from sys.objects where object_id = object_id(N'Report_CallCentre_DetailedInsurerUpload_GetUploadTotals') and type in (N'P', N'PC'))
	begin
		drop procedure Report_CallCentre_DetailedInsurerUpload_GetUploadTotals
	end
go

create procedure Report_CallCentre_DetailedInsurerUpload_GetUploadTotals
(
	@CampaignIds varchar(50),
	@InsurerIds varchar(50),
	@DateFrom datetime,
	@DateTo datetime
)
as
	if(len(@CampaignIds) > 0)
		begin
			if(len(@InsurerIds) > 0)
				begin
					select distinct
						Organization.TradingName [Insurer],
						Product.Name [Product],
						(
							select
								count(Quote.Id)
							from QuoteUploadLog
								inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
								inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
							where
								(
									select
										LeadActivity.CampaignId
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									where
										QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
								) in (select * from fn_StringListToTable(@CampaignIds)) and
								Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
								Insurer.PartyId = Organization.PartyId and
								InsurerProduct.Id = Product.Id and
								QuoteUploadLog.DateCreated >= @DateFrom and
								QuoteUploadLog.DateCreated <= @DateTo and
								QuoteUploadLog.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) [Uploads],
						(
							select
								avg(QuoteUploadLog.Premium)
							from QuoteUploadLog
								inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
								inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
							where
								(
									select
										LeadActivity.CampaignId
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									where
										QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
								) in (select * from fn_StringListToTable(@CampaignIds)) and
								Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
								Insurer.PartyId = Organization.PartyId and
								InsurerProduct.Id = Product.Id and
								QuoteUploadLog.DateCreated >= @DateFrom and
								QuoteUploadLog.DateCreated <= @DateTo and
								QuoteUploadLog.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) [AveragePremiumValue],
						(
							select
								sum(QuoteUploadLog.Premium)
							from QuoteUploadLog
								inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
								inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
							where
								(
									select
										LeadActivity.CampaignId
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									where
										QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
								) in (select * from fn_StringListToTable(@CampaignIds)) and
								Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
								Insurer.PartyId = Organization.PartyId and
								InsurerProduct.Id = Product.Id and
								QuoteUploadLog.DateCreated >= @DateFrom and
								QuoteUploadLog.DateCreated <= @DateTo and
								QuoteUploadLog.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) [TotalPremiumValue]
					from QuoteUploadLog
						inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductProviderId
					where
						(
							select
								LeadActivity.CampaignId
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
							where
								QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						) in (select * from fn_StringListToTable(@CampaignIds)) and
						Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
						QuoteUploadLog.DateCreated >= @DateFrom and
						QuoteUploadLog.DateCreated <= @DateTo and
						QuoteUploadLog.IsDeleted = 0 and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
				end
			else
				begin
					select distinct
						Organization.TradingName [Insurer],
						Product.Name [Product],
						(
							select
								count(Quote.Id)
							from QuoteUploadLog
								inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
								inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
							where
								(
									select
										LeadActivity.CampaignId
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									where
										QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
								) in (select * from fn_StringListToTable(@CampaignIds)) and
								--Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
								Insurer.PartyId = Organization.PartyId and
								InsurerProduct.Id = Product.Id and
								QuoteUploadLog.DateCreated >= @DateFrom and
								QuoteUploadLog.DateCreated <= @DateTo and
								QuoteUploadLog.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) [Uploads],
						(
							select
								avg(QuoteUploadLog.Premium)
							from QuoteUploadLog
								inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
								inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
							where
								(
									select
										LeadActivity.CampaignId
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									where
										QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
								) in (select * from fn_StringListToTable(@CampaignIds)) and
								--Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
								Insurer.PartyId = Organization.PartyId and
								InsurerProduct.Id = Product.Id and
								QuoteUploadLog.DateCreated >= @DateFrom and
								QuoteUploadLog.DateCreated <= @DateTo and
								QuoteUploadLog.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) [AveragePremiumValue],
						(
							select
								sum(QuoteUploadLog.Premium)
							from QuoteUploadLog
								inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
								inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
							where
								(
									select
										LeadActivity.CampaignId
									from LeadActivity
										inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
									where
										QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
								) in (select * from fn_StringListToTable(@CampaignIds)) and
								--Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
								Insurer.PartyId = Organization.PartyId and
								InsurerProduct.Id = Product.Id and
								QuoteUploadLog.DateCreated >= @DateFrom and
								QuoteUploadLog.DateCreated <= @DateTo and
								QuoteUploadLog.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) [TotalPremiumValue]
					from QuoteUploadLog
						inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductProviderId
					where
						(
							select
								LeadActivity.CampaignId
							from LeadActivity
								inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
							where
								QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						) in (select * from fn_StringListToTable(@CampaignIds)) and
						--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
						QuoteUploadLog.DateCreated >= @DateFrom and
						QuoteUploadLog.DateCreated <= @DateTo and
						QuoteUploadLog.IsDeleted = 0 and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
				end
		end
	else
		begin
			if(len(@InsurerIds) > 0)
				begin
					select distinct
						Organization.TradingName [Insurer],
						Product.Name [Product],
						(
							select
								count(Quote.Id)
							from QuoteUploadLog
								inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
								inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
							where
								--(
								--	select
								--		LeadActivity.CampaignId
								--	from LeadActivity
								--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								--	where
								--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
								--) in (select * from fn_StringListToTable(@CampaignIds)) and
								Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
								Insurer.PartyId = Organization.PartyId and
								InsurerProduct.Id = Product.Id and
								QuoteUploadLog.DateCreated >= @DateFrom and
								QuoteUploadLog.DateCreated <= @DateTo and
								QuoteUploadLog.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) [Uploads],
						(
							select
								avg(QuoteUploadLog.Premium)
							from QuoteUploadLog
								inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
								inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
							where
								--(
								--	select
								--		LeadActivity.CampaignId
								--	from LeadActivity
								--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								--	where
								--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
								--) in (select * from fn_StringListToTable(@CampaignIds)) and
								Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
								Insurer.PartyId = Organization.PartyId and
								InsurerProduct.Id = Product.Id and
								QuoteUploadLog.DateCreated >= @DateFrom and
								QuoteUploadLog.DateCreated <= @DateTo and
								QuoteUploadLog.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) [AveragePremiumValue],
						(
							select
								sum(QuoteUploadLog.Premium)
							from QuoteUploadLog
								inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
								inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
							where
								--(
								--	select
								--		LeadActivity.CampaignId
								--	from LeadActivity
								--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								--	where
								--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
								--) in (select * from fn_StringListToTable(@CampaignIds)) and
								Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
								Insurer.PartyId = Organization.PartyId and
								InsurerProduct.Id = Product.Id and
								QuoteUploadLog.DateCreated >= @DateFrom and
								QuoteUploadLog.DateCreated <= @DateTo and
								QuoteUploadLog.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) [TotalPremiumValue]
					from QuoteUploadLog
						inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductProviderId
					where
						--(
						--	select
						--		LeadActivity.CampaignId
						--	from LeadActivity
						--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						--	where
						--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						--) in (select * from fn_StringListToTable(@CampaignIds)) and
						Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
						QuoteUploadLog.DateCreated >= @DateFrom and
						QuoteUploadLog.DateCreated <= @DateTo and
						QuoteUploadLog.IsDeleted = 0 and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
				end
			else
				begin
					select distinct
						Organization.TradingName [Insurer],
						Product.Name [Product],
						(
							select
								count(Quote.Id)
							from QuoteUploadLog
								inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
								inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
							where
								--(
								--	select
								--		LeadActivity.CampaignId
								--	from LeadActivity
								--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								--	where
								--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
								--) in (select * from fn_StringListToTable(@CampaignIds)) and
								--Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
								Insurer.PartyId = Organization.PartyId and
								InsurerProduct.Id = Product.Id and
								QuoteUploadLog.DateCreated >= @DateFrom and
								QuoteUploadLog.DateCreated <= @DateTo and
								QuoteUploadLog.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) [Uploads],
						(
							select
								avg(QuoteUploadLog.Premium)
							from QuoteUploadLog
								inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
								inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
							where
								--(
								--	select
								--		LeadActivity.CampaignId
								--	from LeadActivity
								--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								--	where
								--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
								--) in (select * from fn_StringListToTable(@CampaignIds)) and
								--Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
								Insurer.PartyId = Organization.PartyId and
								InsurerProduct.Id = Product.Id and
								QuoteUploadLog.DateCreated >= @DateFrom and
								QuoteUploadLog.DateCreated <= @DateTo and
								QuoteUploadLog.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) [AveragePremiumValue],
						(
							select
								sum(QuoteUploadLog.Premium)
							from QuoteUploadLog
								inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
								inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
								inner join Product InsurerProduct on InsurerProduct.Id = Quote.ProductId
								inner join Organization Insurer on Insurer.PartyId = InsurerProduct.ProductProviderId
							where
								--(
								--	select
								--		LeadActivity.CampaignId
								--	from LeadActivity
								--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
								--	where
								--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
								--) in (select * from fn_StringListToTable(@CampaignIds)) and
								--Insurer.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
								Insurer.PartyId = Organization.PartyId and
								InsurerProduct.Id = Product.Id and
								QuoteUploadLog.DateCreated >= @DateFrom and
								QuoteUploadLog.DateCreated <= @DateTo and
								QuoteUploadLog.IsDeleted = 0 and
								QuoteHeader.IsRerated = 0 and
								Quote.IsDeleted = 0
						) [TotalPremiumValue]
					from QuoteUploadLog
						inner join Quote on Quote.Id = QuoteUploadLog.QuoteId
						inner join QuoteHeader on QuoteHeader.Id = Quote.QuoteHeaderId
						inner join Product on Product.Id = Quote.ProductId
						inner join Organization on Organization.PartyId = Product.ProductProviderId
					where
						--(
						--	select
						--		LeadActivity.CampaignId
						--	from LeadActivity
						--		inner join QuotedLeadActivity on QuotedLeadActivity.LeadActivityId = LeadActivity.Id
						--	where
						--		QuotedLeadActivity.QuoteHeaderId = QuoteHeader.Id
						--) in (select * from fn_StringListToTable(@CampaignIds)) and
						--Organization.PartyId in (select * from fn_StringListToTable(@InsurerIds)) and
						QuoteUploadLog.DateCreated >= @DateFrom and
						QuoteUploadLog.DateCreated <= @DateTo and
						QuoteUploadLog.IsDeleted = 0 and
						QuoteHeader.IsRerated = 0 and
						Quote.IsDeleted = 0
				end
		end
go