DECLARE @countProductFee int

if exists (select * from information_schema.tables where table_name = 'ProductFee')
	begin
		SELECT @countProductFee = count(*) FROM [dbo].[ProductFee] WHERE BrokerageId = 0
		
		IF @countProductFee > 0
		
		begin
			update ProductFee set BrokerageId = null where BrokerageId = 0
		end
	end
go