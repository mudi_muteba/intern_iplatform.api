if exists (select * from sys.objects where object_id = object_id(N'Reports_FuneralQuote_Header') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_FuneralQuote_Header
	end
go

create procedure [dbo].[Reports_FuneralQuote_Header]
(
	@ProposalHeaderId int
)
as
select top 1
		Individual.FirstName + ' ' + Individual.Surname MainMemberName,
		isnull(Agent.FirstName + ' ' + Agent.Surname, '') SalesConsultant,
		DATEADD(d, 1, EOMONTH(current_timestamp)) StartDate,
		md.Gender.Name MainMemberGender,
		Individual.DateOfBirth MainMemberDateOfBirth,
		Occupation.Name MainMemberOccupation,
		(
			select
				isnull(AdditionalMembers.Initials + ' ' + AdditionalMembers.Surname, '')
			from AdditionalMembers
			where
				AdditionalMembers.ProposalDefinitionId = ProposalDefinition.Id and
				AdditionalMembers.MemberRelationshipId = 1 and
				AdditionalMembers.IsDeleted != 1
		) SpouseName,
		(
			select
				isnull(md.Gender.Name, '')
			from AdditionalMembers
				inner join md.Gender on md.Gender.Id = AdditionalMembers.GenderId
			where
				AdditionalMembers.ProposalDefinitionId = ProposalDefinition.Id and
				AdditionalMembers.MemberRelationshipId = 1
				and AdditionalMembers.IsDeleted != 1
		) SpouseGender,
		cast
		(
			(
				select
					isnull(cast(AdditionalMembers.DateOfBirth as date), '')
				from AdditionalMembers
				where
					AdditionalMembers.ProposalDefinitionId = ProposalDefinition.Id and
					AdditionalMembers.MemberRelationshipId = 1 AND
					AdditionalMembers.IsDeleted != 1
			)
		as varchar) SpouseDateOfBirth ,
		ProposalDefinition.SumInsured FuneralBenefit,
		ProposalDefinition.SumInsured AccidentalDeathBenefit,
		6000.00 GroceryBenefit,
		5000.00 MemorialBenefit,
		
		0.00 ChildrenPremium,
		8.21 ExtendedFamilyAdditionalChildrenPremium,
		QuoteItem.Premium TotalMonthlyPremium,
		(
			select top 1
				Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code
			from [Address]
			where
				[Address].PartyId = Party.Id and
				[Address].IsDefault = 1 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address')
		) DefaultPhysical,
		(
			select top 1
				Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code
			from [Address]
			where
				[Address].PartyId = Party.Id and
				[Address].IsDefault = 1 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address')
		) DefaultPostal,
		(
			select top 1
				Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code
			from [Address]
			where
				[Address].PartyId = Party.Id and
				[Address].IsDefault = 0 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Physical Address')
		) NoDefaultPhysical,
		(
			select top 1
				Complex + ',' + Line1 + ',' + Line2 + ',' + Line3 + ',' + Line4 + ',' + Code
			from [Address]
			where
				[Address].PartyId = Party.Id and
				[Address].IsDefault = 0 and
				AddressTypeId = (select top 1 id from md.AddressType where md.AddressType.Name = 'Postal Address')
		) NoDefaultPostal,
		30 QuoteExpiration,
		Quote.InsurerReference QuoteNo,
		Individual.FirstName FirstName,
		Individual.Surname Surname,
	    md.QuestionAnswer.Answer Package,
		QuoteItemBreakDown.Total TotalBasicPremium
	from ProposalHeader
	inner join Party on Party.Id = ProposalHeader.PartyId
		inner join Individual on Individual.PartyId = Party.Id
		inner join md.Gender on md.Gender.Id = Individual.GenderId
		inner join LeadActivity on LeadActivity.Id = ProposalHeader.LeadActivityId
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = Proposalheader.Id
		inner join Product on Product.Id = ProposalDefinition.ProductId
		inner join QuoteHeader on QuoteHeader.ProposalHeaderId = ProposalHeader.Id
		inner join Quote on Quote.QuoteHeaderId = QuoteHeader.Id
		inner join QuoteItem on QuoteItem.QuoteId = Quote.Id
		inner join QuoteItemBreakDown on QuoteItemBreakDown.QuoteItemId = QuoteItem.Id
		left join Occupation on Occupation.Id = Individual.OccupationId
		left join Party AgentParty on AgentParty.Id = LeadActivity.UserId
		left join Individual Agent on Agent.PartyId = AgentParty.Id
		inner join ProposalDefinition PD on PD.ProposalHeaderId = ProposalHeader.Id
        inner join ProposalQuestionAnswer PQA on PQA.ProposalDefinitionId = PD.Id and QuestionDefinitionId=2216
        inner join md.QuestionAnswer on md.QuestionAnswer.Id=PQA.Answer
	where
		Proposalheader.Id = @ProposalHeaderId and
		QuoteItemBreakDown.IsMainMember = 1 and 
		QuoteHeader.IsDeleted = 0
	order by
		Quote.Id desc
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_FuneralQuote_Children') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_FuneralQuote_Children
	end
go

create procedure Reports_FuneralQuote_Children
(
	@ProposalHeaderId int
)
as
	select
		AdditionalMembers.Initials + ' ' + AdditionalMembers.Surname Name,
		md.Gender.Name Gender,
		cast(AdditionalMembers.DateOfBirth as date) DateOfBirth,
		AdditionalMembers.Initials + ' ' + AdditionalMembers.Surname +', a ' + md.Gender.Name + ' born ' + cast(cast(AdditionalMembers.DateOfBirth as date) as varchar) + ', a child of the main member.' [Description]
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = Proposalheader.Id
		inner join AdditionalMembers on AdditionalMembers.ProposalDefinitionId = ProposalDefinition.Id
		inner join md.MemberRelationship on md.MemberRelationship.Id = AdditionalMembers.MemberRelationshipId
		inner join md.Gender on md.Gender.Id = AdditionalMembers.GenderId
		inner join QuoteHeader QH on QH.ProposalHeaderId = ProposalHeader.Id
		inner join Quote Q on Q.QuoteHeaderId = QH.Id
		inner join QuoteItem QI on QI.QuoteId = Q.Id
		inner join QuoteItemBreakDown QIB on QIB.QuoteItemId = QI.Id AND AdditionalMembers.Id = QIB.AdditionalMembersId
	where
		Proposalheader.Id = @ProposalHeaderId and
		md.MemberRelationship.Id in (12) and
		AdditionalMembers.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QH.IsRerated = 0 and
		QH.IsDeleted = 0 and
		Q.IsDeleted = 0 and
		QI.IsDeleted = 0 and
		QIB.IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_FuneralQuote_ExtendedFamily') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_FuneralQuote_ExtendedFamily
	end
go

create procedure Reports_FuneralQuote_ExtendedFamily
(
	@ProposalHeaderId int
)
as
	select distinct
		AM.Initials + ' ' + AM.Surname Name,
		md.Gender.Name Gender,
		cast(AM.DateOfBirth as date) DateOfBirth,
		QIB.Relationship Relationship,
		QIB.Total Premium,
		AM.Initials + ' ' + AM.Surname +', born ' + cast(cast(AM.DateOfBirth as date) as varchar) + ', a ' + LOWER(QIB.Relationship) + ' of the main member.' [Description]
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = Proposalheader.Id
		inner join AdditionalMembers AM on AM.ProposalDefinitionId = ProposalDefinition.Id
		inner join md.Gender on md.Gender.Id = AM.GenderId
		inner join md.MemberRelationship on md.MemberRelationship.Id = AM.MemberRelationshipId
		inner join QuoteHeader QH on QH.ProposalHeaderId = ProposalHeader.Id
		inner join Quote Q on Q.QuoteHeaderId = QH.Id
		inner join QuoteItem QI on QI.QuoteId = Q.Id
		inner join QuoteItemBreakDown QIB on QIB.QuoteItemId = QI.Id AND AM.Id = QIB.AdditionalMembersId
	where
		Proposalheader.Id = @ProposalHeaderId AND
		md.MemberRelationship.Id not in (1, 12, 21) and
		ProposalDefinition.IsDeleted = 0 and
		QH.IsRerated = 0 and
		QH.IsDeleted = 0 and
		Q.IsDeleted = 0 and
		QI.IsDeleted = 0 and
		QIB.IsDeleted = 0 and
		AM.IsDeleted = 0
go

if exists (select * from sys.objects where object_id = object_id(N'Reports_FuneralQuote_AdditionalChildren') and type in (N'P', N'PC'))
	begin
		drop procedure Reports_FuneralQuote_AdditionalChildren
	end
go

create procedure Reports_FuneralQuote_AdditionalChildren
(
	@ProposalHeaderId int
)
as
select
		AdditionalMembers.Initials + ' ' + AdditionalMembers.Surname Name,
		md.Gender.Name Gender,
		cast(AdditionalMembers.DateOfBirth as date) DateOfBirth,
		0.00 Premium,
		AdditionalMembers.Initials + ' ' + AdditionalMembers.Surname +', born ' + cast(cast(AdditionalMembers.DateOfBirth as date) as varchar) + ', a child of the main member.' [Description]
	from ProposalHeader
		inner join ProposalDefinition on ProposalDefinition.ProposalHeaderId = Proposalheader.Id
		inner join AdditionalMembers on AdditionalMembers.ProposalDefinitionId = ProposalDefinition.Id
		inner join md.MemberRelationship on md.MemberRelationship.Id = AdditionalMembers.MemberRelationshipId
		inner join md.Gender on md.Gender.Id = AdditionalMembers.GenderId
		inner join QuoteHeader QH on QH.ProposalHeaderId = ProposalHeader.Id
		inner join Quote Q on Q.QuoteHeaderId = QH.Id
		inner join QuoteItem QI on QI.QuoteId = Q.Id
		inner join QuoteItemBreakDown QIB on QIB.QuoteItemId = QI.Id AND AdditionalMembers.Id = QIB.AdditionalMembersId
	where
		Proposalheader.Id = @ProposalHeaderId and
		md.MemberRelationship.Id in (21) and
		AdditionalMembers.IsDeleted = 0 and
		ProposalDefinition.IsDeleted = 0 and
		QH.IsRerated = 0 and
		QH.IsDeleted = 0 and
		Q.IsDeleted = 0 and
		QI.IsDeleted = 0 and
		QIB.IsDeleted = 0
go

