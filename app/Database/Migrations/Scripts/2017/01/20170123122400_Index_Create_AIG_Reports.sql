if exists (select * from sys.objects where object_id = object_id('dbo.CoverDefinition') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='CoverDefinitionIdCoverIdIndex' AND object_id = OBJECT_ID('CoverDefinition'))
		begin
			drop INDEX CoverDefinitionIdCoverIdIndex ON CoverDefinition
			CREATE NONCLUSTERED INDEX [CoverDefinitionIdCoverIdIndex] ON [dbo].[CoverDefinition]
			(
				[Id] ASC,
				[CoverId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='CoverDefinitionIdCoverIdIndex' AND object_id = OBJECT_ID('CoverDefinition'))
		begin			
			CREATE NONCLUSTERED INDEX [CoverDefinitionIdCoverIdIndex] ON [dbo].[CoverDefinition]
			(
				[Id] ASC,
				[CoverId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.CoverDefinition') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='CoverDefinitionIdCoverIdCoverDefinitionTypeIdIndex' AND object_id = OBJECT_ID('CoverDefinition'))
		begin
			drop INDEX CoverDefinitionIdCoverIdCoverDefinitionTypeIdIndex ON CoverDefinition
			CREATE NONCLUSTERED  INDEX [CoverDefinitionIdCoverIdCoverDefinitionTypeIdIndex] ON [dbo].[CoverDefinition]
			(
				[Id],
				[CoverId],
				[CoverDefinitionTypeId]
			)WITH (DROP_EXISTING = OFF)
		end

	if not exists (select * FROM sys.indexes WHERE name='CoverDefinitionIdCoverIdCoverDefinitionTypeIdIndex' AND object_id = OBJECT_ID('CoverDefinition'))
		begin			
			CREATE NONCLUSTERED  INDEX [CoverDefinitionIdCoverIdCoverDefinitionTypeIdIndex] ON [dbo].[CoverDefinition]
			(
				[Id],
				[CoverId],
				[CoverDefinitionTypeId]
			)WITH (DROP_EXISTING = OFF)
		end
end
go

if exists (select * from sys.objects where object_id = object_id('dbo.CoverDefinition') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='CoverDefinitionIdCoverIdCoverDefinitionTypeIdIndexNonClustered' AND object_id = OBJECT_ID('CoverDefinition'))
		begin
			drop INDEX CoverDefinitionIdCoverIdCoverDefinitionTypeIdIndexNonClustered ON CoverDefinition
			CREATE NONCLUSTERED INDEX [CoverDefinitionIdCoverIdCoverDefinitionTypeIdIndexNonClustered] ON [dbo].[CoverDefinition]
			(
				[Id] ASC,
				[CoverId] ASC,
				[CoverDefinitionTypeId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='CoverDefinitionIdCoverIdCoverDefinitionTypeIdIndexNonClustered' AND object_id = OBJECT_ID('CoverDefinition'))
		begin			
			CREATE NONCLUSTERED INDEX [CoverDefinitionIdCoverIdCoverDefinitionTypeIdIndexNonClustered] ON [dbo].[CoverDefinition]
			(
				[Id] ASC,
				[CoverId] ASC,
				[CoverDefinitionTypeId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go

if exists (select * from sys.objects where object_id = object_id('dbo.QuoteHeader') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='QuoteHeaderIdIndex' AND object_id = OBJECT_ID('QuoteHeader'))
		begin
			drop INDEX QuoteHeaderIdIndex ON QuoteHeader
			CREATE UNIQUE NONCLUSTERED INDEX [QuoteHeaderIdIndex] ON [dbo].[QuoteHeader]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='QuoteHeaderIdIndex' AND object_id = OBJECT_ID('QuoteHeader'))
		begin			
			CREATE UNIQUE NONCLUSTERED INDEX [QuoteHeaderIdIndex] ON [dbo].[QuoteHeader]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.Lead') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='LeadIdPartyIdIndex' AND object_id = OBJECT_ID('Lead'))
		begin
			drop INDEX LeadIdPartyIdIndex ON Lead
			CREATE NONCLUSTERED INDEX [LeadIdPartyIdIndex] ON [dbo].[Lead]
			(
				[Id] ASC,
				[PartyId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='LeadIdPartyIdIndex' AND object_id = OBJECT_ID('Lead'))
		begin			
			CREATE NONCLUSTERED INDEX [LeadIdPartyIdIndex] ON [dbo].[Lead]
			(
				[Id] ASC,
				[PartyId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.Party') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='PartyIdIndex' AND object_id = OBJECT_ID('Party'))
		begin
			drop INDEX PartyIdIndex ON Party
			CREATE NONCLUSTERED INDEX [PartyIdIndex] ON [dbo].[Party]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='PartyIdIndex' AND object_id = OBJECT_ID('Party'))
		begin			
			CREATE NONCLUSTERED INDEX [PartyIdIndex] ON [dbo].[Party]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.Individual') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='IndividualPartyIdIndex' AND object_id = OBJECT_ID('Individual'))
		begin
			drop INDEX IndividualPartyIdIndex ON Individual
			CREATE NONCLUSTERED INDEX [IndividualPartyIdIndex] ON [dbo].[Individual]
			(
				[PartyId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='IndividualPartyIdIndex' AND object_id = OBJECT_ID('Individual'))
		begin			
			CREATE NONCLUSTERED INDEX [IndividualPartyIdIndex] ON [dbo].[Individual]
			(
				[PartyId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.Occupation') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='OccupationIdIndex' AND object_id = OBJECT_ID('Occupation'))
		begin
			drop INDEX OccupationIdIndex ON Occupation
			CREATE NONCLUSTERED INDEX [OccupationIdIndex] ON [dbo].[Occupation]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='OccupationIdIndex' AND object_id = OBJECT_ID('Occupation'))
		begin			
			CREATE NONCLUSTERED INDEX [OccupationIdIndex] ON [dbo].[Occupation]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.QuoteItem') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='QuoteItemIdUnique' AND object_id = OBJECT_ID('QuoteItem'))
		begin
			drop INDEX QuoteItemIdUnique ON QuoteItem
			CREATE UNIQUE NONCLUSTERED INDEX [QuoteItemIdUnique] ON [dbo].[QuoteItem]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='QuoteItemIdUnique' AND object_id = OBJECT_ID('QuoteItem'))
		begin			
			CREATE UNIQUE NONCLUSTERED INDEX [QuoteItemIdUnique] ON [dbo].[QuoteItem]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.QuoteItem') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='QuoteItemQuoteId' AND object_id = OBJECT_ID('QuoteItem'))
		begin
			drop INDEX QuoteItemQuoteId ON QuoteItem
			CREATE NONCLUSTERED INDEX [QuoteItemQuoteId] ON [dbo].[QuoteItem]
			(
				[QuoteId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='QuoteItemQuoteId' AND object_id = OBJECT_ID('QuoteItem'))
		begin			
			CREATE NONCLUSTERED INDEX [QuoteItemQuoteId] ON [dbo].[QuoteItem]
			(
				[QuoteId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.QuoteItemBreakDown') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='QuoteItemBreakDownIdIndex' AND object_id = OBJECT_ID('QuoteItemBreakDown'))
		begin
			drop INDEX QuoteItemBreakDownIdIndex ON QuoteItemBreakDown
			CREATE NONCLUSTERED INDEX [QuoteItemBreakDownIdIndex] ON [dbo].[QuoteItemBreakDown]
			(
				[Id] ASC,
				[QuoteItemId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='QuoteItemBreakDownIdIndex' AND object_id = OBJECT_ID('QuoteItemBreakDown'))
		begin			
			CREATE NONCLUSTERED INDEX [QuoteItemBreakDownIdIndex] ON [dbo].[QuoteItemBreakDown]
			(
				[Id] ASC,
				[QuoteItemId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.QuoteItemBreakDown') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='QuoteItemBreakDownQuoteItemIdIndex' AND object_id = OBJECT_ID('QuoteItemBreakDown'))
		begin
			drop INDEX QuoteItemBreakDownQuoteItemIdIndex ON QuoteItemBreakDown
			CREATE NONCLUSTERED INDEX [QuoteItemBreakDownQuoteItemIdIndex] ON [dbo].[QuoteItemBreakDown]
			(
				[QuoteItemId] ASC,
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='QuoteItemBreakDownQuoteItemIdIndex' AND object_id = OBJECT_ID('QuoteItemBreakDown'))
		begin			
			CREATE NONCLUSTERED INDEX [QuoteItemBreakDownQuoteItemIdIndex] ON [dbo].[QuoteItemBreakDown]
			(
				[QuoteItemId] ASC,
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.ProposalHeader') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='ProposalHeaderIdIndex' AND object_id = OBJECT_ID('ProposalHeader'))
		begin
			drop INDEX ProposalHeaderIdIndex ON ProposalHeader
			CREATE UNIQUE NONCLUSTERED INDEX [ProposalHeaderIdIndex] ON [dbo].[ProposalHeader]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='ProposalHeaderIdIndex' AND object_id = OBJECT_ID('ProposalHeader'))
		begin			
			CREATE UNIQUE NONCLUSTERED INDEX [ProposalHeaderIdIndex] ON [dbo].[ProposalHeader]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go



if exists (select * from sys.objects where object_id = object_id('dbo.ProposalHeader') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='ProposalHeaderIdPartyIdIndex' AND object_id = OBJECT_ID('ProposalHeader'))
		begin
			drop INDEX ProposalHeaderIdPartyIdIndex ON ProposalHeader
			CREATE NONCLUSTERED INDEX [ProposalHeaderIdPartyIdIndex] ON [dbo].[ProposalHeader]
			(
				[Id] ASC,
				[PartyId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='ProposalHeaderIdPartyIdIndex' AND object_id = OBJECT_ID('ProposalHeader'))
		begin			
			CREATE NONCLUSTERED INDEX [ProposalHeaderIdPartyIdIndex] ON [dbo].[ProposalHeader]
			(
				[Id] ASC,
				[PartyId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go



if exists (select * from sys.objects where object_id = object_id('dbo.Quote') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='QuoteIdQuoteHeaderIdIndex' AND object_id = OBJECT_ID('Quote'))
		begin
			drop INDEX QuoteIdQuoteHeaderIdIndex ON Quote
			CREATE NONCLUSTERED INDEX [QuoteIdQuoteHeaderIdIndex] ON [dbo].[Quote]
			(
				[QuoteHeaderId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='QuoteIdQuoteHeaderIdIndex' AND object_id = OBJECT_ID('Quote'))
		begin			
			CREATE NONCLUSTERED INDEX [QuoteIdQuoteHeaderIdIndex] ON [dbo].[Quote]
			(
				[QuoteHeaderId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go



if exists (select * from sys.objects where object_id = object_id('dbo.Quote') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='QuoteIndex' AND object_id = OBJECT_ID('Quote'))
		begin
			drop INDEX QuoteIndex ON Quote
			CREATE UNIQUE NONCLUSTERED INDEX [QuoteIndex] ON [dbo].[Quote]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='QuoteIndex' AND object_id = OBJECT_ID('Quote'))
		begin			
			CREATE UNIQUE NONCLUSTERED INDEX [QuoteIndex] ON [dbo].[Quote]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.LossHistory') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='LossHistoryIdIndex' AND object_id = OBJECT_ID('LossHistory'))
		begin
			drop INDEX LossHistoryIdIndex ON LossHistory
			CREATE NONCLUSTERED INDEX [LossHistoryIdIndex] ON [dbo].[LossHistory]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='LossHistoryIdIndex' AND object_id = OBJECT_ID('LossHistory'))
		begin			
			CREATE NONCLUSTERED INDEX [LossHistoryIdIndex] ON [dbo].[LossHistory]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.LossHistory') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='LossHistoryPartyIdIndex' AND object_id = OBJECT_ID('LossHistory'))
		begin
			drop INDEX LossHistoryPartyIdIndex ON LossHistory
			CREATE NONCLUSTERED INDEX [LossHistoryPartyIdIndex] ON [dbo].[LossHistory]
			(
				[PartyId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

		end

	if not exists (select * FROM sys.indexes WHERE name='LossHistoryPartyIdIndex' AND object_id = OBJECT_ID('LossHistory'))
		begin			
			CREATE NONCLUSTERED INDEX [LossHistoryPartyIdIndex] ON [dbo].[LossHistory]
			(
				[PartyId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.ProposalDefinition') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='ProposalDefinitionIdUnique' AND object_id = OBJECT_ID('ProposalDefinition'))
		begin
			drop INDEX ProposalDefinitionIdUnique ON ProposalDefinition
			CREATE UNIQUE NONCLUSTERED INDEX [ProposalDefinitionIdUnique] ON [dbo].[ProposalDefinition]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='ProposalDefinitionIdUnique' AND object_id = OBJECT_ID('ProposalDefinition'))
		begin			
			CREATE UNIQUE NONCLUSTERED INDEX [ProposalDefinitionIdUnique] ON [dbo].[ProposalDefinition]
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go


if exists (select * from sys.objects where object_id = object_id('dbo.ProposalDefinition') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='ProposalDefinitionIdProposalHeaderIdPartyIdCoverIdIndex' AND object_id = OBJECT_ID('ProposalDefinition'))
		begin
			drop INDEX ProposalDefinitionIdProposalHeaderIdPartyIdCoverIdIndex ON ProposalDefinition
			CREATE NONCLUSTERED INDEX [ProposalDefinitionIdProposalHeaderIdPartyIdCoverIdIndex] ON [dbo].[ProposalDefinition]
			(
				[ProposalHeaderId] ASC,
				[PartyId] ASC,
				[CoverId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='ProposalDefinitionIdProposalHeaderIdPartyIdCoverIdIndex' AND object_id = OBJECT_ID('ProposalDefinition'))
		begin			
			CREATE NONCLUSTERED INDEX [ProposalDefinitionIdProposalHeaderIdPartyIdCoverIdIndex] ON [dbo].[ProposalDefinition]
			(
				[ProposalHeaderId] ASC,
				[PartyId] ASC,
				[CoverId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go



if exists (select * from sys.objects where object_id = object_id('dbo.Asset') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='AssetIdPartyIdAssetNoIndex' AND object_id = OBJECT_ID('Asset'))
		begin
			drop INDEX AssetIdPartyIdAssetNoIndex ON Asset
			CREATE NONCLUSTERED INDEX [AssetIdPartyIdAssetNoIndex] ON [dbo].[Asset]
			(
				[Id] ASC,
				[PartyId] ASC,
				[AssetNo] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end

	if not exists (select * FROM sys.indexes WHERE name='AssetIdPartyIdAssetNoIndex' AND object_id = OBJECT_ID('Asset'))
		begin			
			CREATE NONCLUSTERED INDEX [AssetIdPartyIdAssetNoIndex] ON [dbo].[Asset]
			(
				[Id] ASC,
				[PartyId] ASC,
				[AssetNo] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
		end
end
go



if exists (select * from sys.objects where object_id = object_id('dbo.Asset') and type in (N'U'))
begin
	if exists (select * FROM sys.indexes WHERE name='AssetIdPartyIdAssetNoNonClustered' AND object_id = OBJECT_ID('Asset'))
		begin
			drop INDEX AssetIdPartyIdAssetNoNonClustered ON Asset
			CREATE NONCLUSTERED  INDEX [AssetIdPartyIdAssetNoNonClustered] ON [dbo].[Asset]
			(
				[Id],
				[PartyId],
				[AssetNo]
			)WITH (DROP_EXISTING = OFF)
		end

	if not exists (select * FROM sys.indexes WHERE name='AssetIdPartyIdAssetNoNonClustered' AND object_id = OBJECT_ID('Asset'))
		begin			
			CREATE NONCLUSTERED  INDEX [AssetIdPartyIdAssetNoNonClustered] ON [dbo].[Asset]
			(
				[Id],
				[PartyId],
				[AssetNo]
			)WITH (DROP_EXISTING = OFF)
		end
end
go