declare
	@DefaultChannelId int,
	@DefaultChannelCode varchar(20),
	@ScopeComparativeQuoteReportChannelId int,
	@ScopeFuneralQuoteReportChannelId int,
	@ScopeQuoteScheduleReportChannelId int

select
	@DefaultChannelId = Id,
	@DefaultChannelCode = Code
from Channel
where
	IsDefault = 1

if (@DefaultChannelCode = 'IP')
	begin

--Contents

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 115

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 50

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 49

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 52

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 44

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 56

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 57

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 55

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 54

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 131

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 43

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 74

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 76

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 82

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 64

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 45

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 97 and QuestionId = 46

--Buildings

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 98 and QuestionId = 115

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 98 and QuestionId = 49

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 98 and QuestionId = 52

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 98 and QuestionId = 50

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 98 and QuestionId = 56

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 98 and QuestionId = 55

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 98 and QuestionId = 57

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 98 and QuestionId = 43

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 98 and QuestionId = 64

update QuestionDefinition set DefaultValue = '' where CoverDefinitionId = 98 and QuestionId = 74


	end
else
	begin
		return
    end
