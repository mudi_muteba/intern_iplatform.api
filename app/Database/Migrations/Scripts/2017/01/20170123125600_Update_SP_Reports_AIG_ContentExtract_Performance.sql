if exists (select * from sys.objects where object_id = object_id(N'Reports_AIG_ContentExtract') and type in (N'P', N'PC'))
begin
	drop procedure Reports_AIG_ContentExtract
end
go

create procedure Reports_AIG_ContentExtract
(
	@StartDate nvarchar(50),
	@EndDate nvarchar(50)
)
as

-- Content

--declare @StartDate nvarchar(50)
--declare @EndDate nvarchar(50)
--set @StartDate = '2016-08-01'
--set @EndDate = '2016-11-30'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET NOCOUNT ON 

CREATE TABLE #TempTableContentExtract(ProposalDefinitionId int, PartyId int, LeadId int, ProposalHeaderId int, CoverId int, 
Channel nvarchar(max), QuoteExpired nvarchar(max), QuoteBound nvarchar(max), QuoteBindDate nvarchar(max), DateOfQuote nvarchar(max), 
QuoteTotalPremium decimal(38,2), QuoteMotorPremium decimal(38,2), QuoteHomePremium decimal(38,2), QuoteFuneralPremium decimal(38,2), QuoteOtherPremium decimal(38,2),
QuoteFees decimal(38,2), QuoteSasria decimal(38,2), QuoteIncepptionDate nvarchar(max), QuotePolicyNumber nvarchar(max), QuoteCreateDate nvarchar(max), QuoteCreateTime nvarchar(max), QuoteID int, VMSAID nvarchar(max), 
PropertyNumber int, CoverType nvarchar(max),
SASRIA decimal(38,2),
PremiumSubsidenceAndLandslip decimal(38,2),
PremiumTotal decimal(38,2), ExternalRatingResult nvarchar(max))



insert into #TempTableContentExtract
SELECT 
prd.Id, p.Id, l.id , Prh.id , Prd.coverid, 
CASE WHEN Prh.Source IS NULL THEN ('Call center') ELSE Prh.Source END as Channel,
CASE WHEN DATEDIFF(day,Qute.CreatedAt,GETDATE()) > 30 Then 'Y' ELSE 'N' END as 'QuoteExpired',
(CASE 
 (SELECT COALESCE((select top 1 PolicyStatusId from PolicyHeader  WITH (NOLOCK) where PartyId = p.id and QuoteId = qute.Id and PolicyStatusId = 3), 0))
  when 3 then 'Y'
  when 2 then 'N'
  when 1 then 'N'
  when 0 then 'N'
End  )as 'QuoteBound',
	(CASE 
		(SELECT COALESCE((select top 1 PolicyStatusId from PolicyHeader  WITH (NOLOCK) where PartyId = p.id and QuoteId = qute.Id and PolicyStatusId = 3), 0))
			when 3 then CONVERT(VARCHAR(10),Qute.AcceptedOn ,111)
			when 2 then ''
			when 1 then ''
			when 0 then ''
	End  )as 'QuoteBindDate',
'' as 'DateOfQuote',	

	(select ISNULL(sum(cast(round (Premium ,2) as numeric(36,2)) + cast(round (Sasria ,2) as numeric(36,2))) + cast(round (Qute.Fees ,2) as numeric(36,2))  , 0) 
	from dbo.QuoteItem 	 WITH (NOLOCK) 
				where QuoteId = Qute.id) as 'QuoteTotalPremium',

	

	(select ISNULL(sum(cast(round (Premium ,2) as numeric(36,2))),0)
	from dbo.QuoteItem 	 WITH (NOLOCK) 
				where QuoteId = Qute.id and CoverDefinitionId in (select id from dbo.CoverDefinition  WITH (NOLOCK) where CoverId = 218)) as 'QuoteMotorPremium',
	 
	

	(select ISNULL(sum(cast(round (Premium ,2) as numeric(36,2))),0)
	from dbo.QuoteItem 	 WITH (NOLOCK) 
				where QuoteId = Qute.id and CoverDefinitionId in (select id from dbo.CoverDefinition  WITH (NOLOCK) where CoverId = 78)) as 'QuoteHomePremium',
	

	(select ISNULL(sum(cast(round (Premium ,2) as numeric(36,2))),0)
	from dbo.QuoteItem 	 WITH (NOLOCK) 
				where QuoteId = Qute.id and CoverDefinitionId in (select id from dbo.CoverDefinition  WITH (NOLOCK) where CoverId = 142)) as 'QuoteFuneralPremium',

	
	(select ISNULL(sum(cast(round (Premium ,2) as numeric(36,2))),0)
	from dbo.QuoteItem 	 WITH (NOLOCK) 
				where QuoteId = Qute.id and CoverDefinitionId in (select id from dbo.CoverDefinition  WITH (NOLOCK) where CoverId not in(218,78,142)))   as 'QuoteOtherPremium',

	(cast(round(Qute.fees,2) as numeric(36,2))) as 'QuoteFees',
	(select sum(cast(round (Sasria ,2) as numeric(36,2)))  from QuoteItem 	 WITH (NOLOCK) 	
				where QuoteId = Qute.id) as 'QuoteSasria',
	(select top 1 CONVERT(VARCHAR(10),DateEffective ,111) from PolicyHeader  WITH (NOLOCK) where PartyId = p.Id and QuoteId = qute.Id and PolicyStatusId = 3) as 'QuoteIncepptionDate',
	(select COALESCE((select top 1 PolicyNo from PolicyHeader WITH (NOLOCK)  where PartyId = p.Id and QuoteId = qute.Id and PolicyStatusId = 3),null)) as 'QuotePolicyNumber',
	CONVERT(VARCHAR(10),Qute.CreatedAt ,111) as 'QuoteCreateDate',
	CONVERT(VARCHAR(5),Qute.CreatedAt,108) as 'QuoteCreateTime', 
	qute.Id as 'QuoteID',
	Ind.ExternalReference as 'VMSAID',	
	(select AssetNo from Asset where id = prd.AssetId) as 'PropertyNumber',
		
	(select DisplayName from CoverDefinition  WITH (NOLOCK) where CoverId =  prd.CoverId and ProductId = 46) as 'CoverType',		
		(select total from QuoteItemBreakDown  WITH (NOLOCK) where QuoteItemId = qi.Id and Name = 'Sasria')  as 'SASRIA',
		
		(select total from QuoteItemBreakDown  WITH (NOLOCK) where QuoteItemId = qi.Id and Name = 'Subsidence And Landslip') as 'PremiumSubsidenceAndLandslip',
		
		(select sum(cast(round (Total ,2) as numeric(36,2))) from QuoteItemBreakDown  WITH (NOLOCK) where QuoteItemId = qi.Id and Name in ('Standard', 'Water Pumping Machinery', 'Subsidence And Landslip', 'Accidental Damage', 'Standard including theft')) as 'PremiumTotal',
		 CAST(qute.ExternalRatingResult AS NVARCHAR(max))
	
from ProposalHeader as Prh WITH (NOLOCK) 
join ProposalDefinition as prd  WITH (NOLOCK) on prd.ProposalHeaderId = Prh.Id and prd.CoverId = 78
join QuoteHeader as Qh  WITH (NOLOCK) on qh.ProposalHeaderId = Prh.Id
join Quote as Qute  WITH (NOLOCK) on Qute.QuoteHeaderId = Qh.Id
JOIN Party as P  WITH (NOLOCK) on p.Id = Prh.PartyId
join Lead as L  WITH (NOLOCK) on l.PartyId = p.Id
left join Individual as Ind  WITH (NOLOCK) on P.Id = Ind.PartyId
join QuoteItem as qi  WITH (NOLOCK) on qi.QuoteId = qute.Id and qi.IsDeleted = 0
left join QuoteItemBreakDown as qibd  WITH (NOLOCK) on qibd.QuoteItemId = qi.Id and qibd.IsDeleted = 0
join CoverDefinition as covD  WITH (NOLOCK) on covD.Id = qi.CoverDefinitionId
inner join Asset as ast  WITH (NOLOCK) on ast.AssetNo = qi.AssetNumber and ast.Id = prd.AssetId
where ((CONVERT(VARCHAR(10),qute.CreatedAt ,121))  >= @StartDate AND (CONVERT(VARCHAR(10),qute.CreatedAt ,121)) <= @EndDate)
and L.IsDeleted = 0
and Qute.IsDeleted = 0
and Prh.IsDeleted = 0
and Qh.IsRerated = 0
and prd.IsDeleted = 0
and prd.CoverId in (78)
and covD.CoverId = 78

group by Prh.Source, prd.Id , p.id, l.id,prh.id,prd.coverid,qute.createdat,qute.id,qute.acceptedon, qute.Fees, qh.CreatedAt, ind.ExternalReference, prd.AssetId, qi.Id, CAST(qute.ExternalRatingResult AS NVARCHAR(max))




CREATE TABLE #TempTableQuestionAnswerContent(Answer nvarchar(max), QuestionId int, ProposalDefinitionId int)

CREATE INDEX TempTableQuestionAnswerContentIndex ON #TempTableQuestionAnswerContent(QuestionId , ProposalDefinitionId)


insert into #TempTableQuestionAnswerContent( Answer , QuestionId, ProposalDefinitionId ) 

SELECT pa.Answer, qd.QuestionId, pa.ProposalDefinitionId from dbo.ProposalQuestionAnswer pa  WITH (NOLOCK) 
join dbo.QuestionDefinition qd  WITH (NOLOCK) on qd.Id = pa.QuestionDefinitionId
WHERE pa.ProposalDefinitionId in (select ProposalDefinitionId FROM #TempTableContentExtract)




SELECT  Channel  ,QuoteExpired, QuoteBound, QuoteBindDate, '' as 'DateOfQuote', QuoteTotalPremium, QuoteMotorPremium, QuoteHomePremium, QuoteFuneralPremium, QuoteOtherPremium,QuoteFees,
QuoteSasria, QuoteIncepptionDate, QuotePolicyNumber, QuoteCreateDate, QuoteCreateTime, QuoteID, VMSAID, PropertyNumber, CoverType, 

		(select Answer from #TempTableQuestionAnswerContent where QuestionId = 89 and ProposalDefinitionId = t.ProposalDefinitionId)
		as 'AddressSuburb', 

		(select Answer from #TempTableQuestionAnswerContent where QuestionId = 90 and ProposalDefinitionId = t.ProposalDefinitionId)
		as 'AddressPostCode', 

(select qa.Answer from #TempTableQuestionAnswerContent as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 47 and ProposalDefinitionId = t.ProposalDefinitionId)
		  as 'AddressProvince',

(select qa.Answer from #TempTableQuestionAnswerContent as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 848 and ProposalDefinitionId = t.ProposalDefinitionId)
		  as 'HomeType',		

		(select qa.Answer from #TempTableQuestionAnswerContent as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 480 and ProposalDefinitionId = t.ProposalDefinitionId)
		 as 'WallConstruction',

		 (select qa.Answer from #TempTableQuestionAnswerContent as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 462 and ProposalDefinitionId = t.ProposalDefinitionId)
		 as 'RoofConstruction',

		(select qa.Answer from #TempTableQuestionAnswerContent as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 850 and ProposalDefinitionId = t.ProposalDefinitionId)
		  as 'ThatchLapa',

		  (case 
			(select Answer from #TempTableQuestionAnswerContent where QuestionId = 394 and ProposalDefinitionId = t.ProposalDefinitionId)
					when null then 'N'
					when 'False' then 'N'
					when 'true' then 'Y'
		End)   as 'ExcludingTheft',

		  (case 
			(select Answer from #TempTableQuestionAnswerContent where QuestionId = 851 and ProposalDefinitionId = t.ProposalDefinitionId)
					when 'False' then 'N'
					when 'true' then 'Y'
		End)   as 'ThatchLapaSize',
		
		(case 
			(select Answer from #TempTableQuestionAnswerContent where QuestionId = 465 and ProposalDefinitionId = t.ProposalDefinitionId)
					when null then 'N'
					when 'False' then 'N'
					when 'true' then 'Y'
		End)  as 'LightningConductor',

		(select qa.Answer from #TempTableQuestionAnswerContent as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 855 and ProposalDefinitionId = t.ProposalDefinitionId)
		as 'PropertyBorder',

		(select Answer from #TempTableQuestionAnswerContent where QuestionId = 845 and ProposalDefinitionId = t.ProposalDefinitionId)
		as 'ConstructionYear',

		(select Answer from #TempTableQuestionAnswerContent where QuestionId = 849 and ProposalDefinitionId = t.ProposalDefinitionId)
		as 'SquareMetresOfProperty',

		(select qa.Answer from #TempTableQuestionAnswerContent as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 846 and ProposalDefinitionId = t.ProposalDefinitionId)
		  as 'NumberOfBathrooms',

		(case (select Answer from #TempTableQuestionAnswerContent where QuestionId = 856 and ProposalDefinitionId = t.ProposalDefinitionId)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End) as 'PropertyUnderConstruction',

		(select qa.Answer from #TempTableQuestionAnswerContent as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 847 and ProposalDefinitionId = t.ProposalDefinitionId)
		     as 'HomeUsage',

		(select qa.Answer from #TempTableQuestionAnswerContent as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 853 and ProposalDefinitionId = t.ProposalDefinitionId)
		     as 'BusinessConducted',

			 		(case (select Answer from #TempTableQuestionAnswerContent where QuestionId = 854 and ProposalDefinitionId = t.ProposalDefinitionId)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)   as 'Commune',

		(select qa.Answer from #TempTableQuestionAnswerContent as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 852 and ProposalDefinitionId = t.ProposalDefinitionId)
		     as 'PeriodPropertyIsUnoccupied',

		case (select isnumeric(Answer) from #TempTableQuestionAnswerContent where QuestionId = 843 and ProposalDefinitionId = t.ProposalDefinitionId) when 0 then 		
		(select Answer from #TempTableQuestionAnswerContent where QuestionId = 843 and ProposalDefinitionId = t.ProposalDefinitionId) when 1 then 
		((select qa.Answer from #TempTableQuestionAnswerContent as tqa
			JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
			where tqa.QuestionId = 843 and ProposalDefinitionId = t.ProposalDefinitionId))
		End as 'Ownership',

		(case (select Answer from #TempTableQuestionAnswerContent where QuestionId = 448 and ProposalDefinitionId = t.ProposalDefinitionId)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)   as 'Security-GatedCommunity',

	   (case (select Answer from #TempTableQuestionAnswerContent where QuestionId = 450 and ProposalDefinitionId = t.ProposalDefinitionId)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)   as 'Security-SecurityComplex',

	   (case (select Answer from #TempTableQuestionAnswerContent where QuestionId = 436 and ProposalDefinitionId = t.ProposalDefinitionId)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)  as 'Security-BurglarBars',

		(case (select Answer from #TempTableQuestionAnswerContent where QuestionId = 468 and ProposalDefinitionId = t.ProposalDefinitionId)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)  as 'Security-SecurityGates',

		(case (select Answer from #TempTableQuestionAnswerContent where QuestionId = 454 and ProposalDefinitionId = t.ProposalDefinitionId)
		    when 'False' then 'N'
			when 'true' then 'Y'
		End)  as 'Security-AlarmWithArmedResponse',

		(select qa.Answer from #TempTableQuestionAnswerContent as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 859 and ProposalDefinitionId = t.ProposalDefinitionId)
		 as 'VAPS-AccidentalDamage',

		(case 
			(select Answer from #TempTableQuestionAnswerContent where QuestionId = 472 and ProposalDefinitionId = t.ProposalDefinitionId)
				when 'False' then 'N'
				when 'true' then 'Y'
		End)  as 'VAPS-SubsidenceAndLandslip',

		(select qa.Answer from #TempTableQuestionAnswerContent as tqa
JOIN md.QuestionAnswer qa  WITH (NOLOCK) on qa.Id = TRY_PARSE(tqa.Answer AS int)
where tqa.QuestionId = 858 and ProposalDefinitionId = t.ProposalDefinitionId)
		 as 'Contents-Excess',

		(select Answer from #TempTableQuestionAnswerContent where QuestionId = 102 and ProposalDefinitionId = t.ProposalDefinitionId)
		 as 'SumInsured',

	(SELECT  cast(round (t.Col.value('(DiscretionaryDiscount)[1]','varchar(max)') ,2) as numeric(36,2))
		FROM
		(
		SELECT 
			cast(ExternalRatingResult as xml) request) tbl 
			cross apply request.nodes('/QRaterAssemblyRs/QRaterRs/Policy/Contents') as t(Col)
			where t.Col.value('(@Id)[1]','varchar(max)') in (ProposalDefinitionId)
			and t.Col.value('(Status/@Description)[1]','varchar(max)') <> 'FAILED'
	)	as 'DiscretionaryDiscount',

SASRIA, 

	(SELECT  cast(round (t.Col.value('(PremiumRisk)[1]','varchar(max)') ,2) as numeric(36,2))
		FROM
		(
		SELECT 
			cast(ExternalRatingResult as xml) request) tbl 
			cross apply request.nodes('/QRaterAssemblyRs/QRaterRs/Policy/Contents') as t(Col)
			where t.Col.value('(@Id)[1]','varchar(max)') in (ProposalDefinitionId)
			and t.Col.value('(Status/@Description)[1]','varchar(max)') <> 'FAILED'
	)	as 'PremiumRisk', 

	(SELECT  cast(round (t.Col.value('(PremiumSubsidenceAndLandslip)[1]','varchar(max)') ,2) as numeric(36,2))
		FROM
		(
		SELECT 
			cast(ExternalRatingResult as xml) request) tbl 
			cross apply request.nodes('/QRaterAssemblyRs/QRaterRs/Policy/Contents') as t(Col)
			where t.Col.value('(@Id)[1]','varchar(max)') in (ProposalDefinitionId)
			and t.Col.value('(Status/@Description)[1]','varchar(max)') <> 'FAILED'
	)	as 'PremiumSubsidenceAndLandslip' ,

	(SELECT  cast(round (t.Col.value('(PremiumTotal)[1]','varchar(max)') ,2) as numeric(36,2))
		FROM
		(
		SELECT 
			cast(ExternalRatingResult as xml) request) tbl 
			cross apply request.nodes('/QRaterAssemblyRs/QRaterRs/Policy/Contents') as t(Col)
			where t.Col.value('(@Id)[1]','varchar(max)') in (ProposalDefinitionId)
			and t.Col.value('(Status/@Description)[1]','varchar(max)') <> 'FAILED'
	)	as 'PremiumTotal' 

FROM #TempTableContentExtract as t

DROP TABLE #TempTableContentExtract 
DROP table #TempTableQuestionAnswerContent