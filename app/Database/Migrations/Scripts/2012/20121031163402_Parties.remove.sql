﻿if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Party_PartyType]') AND parent_object_id = OBJECT_ID('Party'))
alter table Party  drop constraint FK_Party_PartyType

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Relationship_Party]') AND parent_object_id = OBJECT_ID('Relationship'))
alter table Relationship  drop constraint FK_Relationship_Party

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Relationship_ParentParty]') AND parent_object_id = OBJECT_ID('Relationship'))
alter table Relationship  drop constraint FK_Relationship_ParentParty

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Relationship_RelationshipType]') AND parent_object_id = OBJECT_ID('Relationship'))
alter table Relationship  drop constraint FK_Relationship_RelationshipType

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Role_Party]') AND parent_object_id = OBJECT_ID('Role'))
alter table [Role]  drop constraint FK_Role_Party

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Role_RoleType]') AND parent_object_id = OBJECT_ID('Role'))
alter table [Role]  drop constraint FK_Role_RoleType
	
if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[Party_UC]') AND parent_object_id = OBJECT_ID('Party'))
alter table Party  drop constraint Party_UC

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Party_ContactDetail]') AND parent_object_id = OBJECT_ID('Party'))
ALTER TABLE Party DROP CONSTRAINT FK_Party_ContactDetail


if exists (select * from dbo.sysobjects where id = object_id(N'Party') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table Party
if exists (select * from dbo.sysobjects where id = object_id(N'Relationship') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table Relationship
if exists (select * from dbo.sysobjects where id = object_id(N'Role') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [Role]

if exists (select * from dbo.sysobjects where id = object_id(N'md.PartyType') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table md.PartyType
if exists (select * from dbo.sysobjects where id = object_id(N'md.RelationshipType') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table md.RelationshipType
if exists (select * from dbo.sysobjects where id = object_id(N'md.RoleType') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table md.RoleType
