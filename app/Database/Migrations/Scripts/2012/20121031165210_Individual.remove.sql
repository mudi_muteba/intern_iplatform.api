﻿
if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Individual_Party]') AND parent_object_id = OBJECT_ID('Individual'))
alter table Individual  drop constraint FK_Individual_Party

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Individual_Gender]') AND parent_object_id = OBJECT_ID('Individual'))
alter table Individual  drop constraint FK_Individual_Gender

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Individual_Title]') AND parent_object_id = OBJECT_ID('Individual'))
alter table Individual  drop constraint FK_Individual_Title

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Individual_Occupation]') AND parent_object_id = OBJECT_ID('Individual'))
alter table Individual  drop constraint FK_Individual_Occupation

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Individual_HomeLanguage]') AND parent_object_id = OBJECT_ID('Individual'))
alter table Individual  drop constraint FK_Individual_HomeLanguage

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Individual_MaritalStatus]') AND parent_object_id = OBJECT_ID('Individual'))
alter table Individual  drop constraint FK_Individual_MaritalStatus


if exists (select * from dbo.sysobjects where id = object_id(N'Individual') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table Individual
if exists (select * from dbo.sysobjects where id = object_id(N'Occupation') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table Occupation
if exists (select * from dbo.sysobjects where id = object_id(N'md.Gender') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table md.Gender
if exists (select * from dbo.sysobjects where id = object_id(N'md.Title') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table md.Title
if exists (select * from dbo.sysobjects where id = object_id(N'md.Language') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table md.[Language]
if exists (select * from dbo.sysobjects where id = object_id(N'md.MaritalStatus') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table md.MaritalStatus
