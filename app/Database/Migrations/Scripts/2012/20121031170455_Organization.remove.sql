﻿
if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Organization_Party]') AND parent_object_id = OBJECT_ID('Organization'))
	alter table Organization  drop constraint FK_Organization_Party

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[FK_ClientSite_Organization]') AND parent_object_id = OBJECT_ID('ClientSite'))
	alter table ClientSite  drop constraint FK_ClientSite_Organization

if exists (select * from dbo.sysobjects where id = object_id(N'Organization') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table Organization