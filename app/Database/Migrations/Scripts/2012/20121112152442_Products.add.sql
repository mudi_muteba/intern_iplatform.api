﻿
    create table Product (
        Id INT IDENTITY NOT NULL,
		MasterId INT null,
		Name NVARCHAR(255) not null,
		ProductOwnerId INT not null,
		ProductProviderId INT not null,
		ProductTypeId INT not null,
		AgencyNumber NVARCHAR(255) null,
		ProductCode NVARCHAR(255) not null,
		StartDate DATETIME null,
		EndDate DATETIME null,
		primary key (Id)
    )

    create table md.ProductType (
        Id INT IDENTITY NOT NULL,
		Name NVARCHAR(255) NOT NULL UNIQUE,
		primary key (Id)
    )

	-- Table Constraints
    alter table Product 
        add constraint FK_Product_Owner 
        foreign key (ProductOwnerId) 
        references Organization

    alter table Product 
        add constraint FK_Product_Provider 
        foreign key (ProductProviderId) 
        references Organization

    alter table Product 
        add constraint FK_Product_ProductType 
        foreign key (ProductTypeId) 
        references md.ProductType