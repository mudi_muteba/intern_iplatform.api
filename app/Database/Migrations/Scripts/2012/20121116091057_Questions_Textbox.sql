﻿INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES		('Risk Address', 4, 1),
			('Suburb', 4, 1), -- General Information
			('Postal Code', 4, 1),
			('Postal Code (Work)', 4, 1),
			('PAYD Duration', 4, 1),
			('Insured Age', 4, 1),
			('Vehicle MM Code', 4, 2), -- Risk Information
			('Vehicle Make', 4, 2),
			('Vehicle Model', 4, 2),
			('Vehicle Registration Number', 4, 2),
			('Vehicle Age', 4, 2),
			('Year Of Manufacture', 4, 2),
			('Registered Owner ID Number', 4, 2),
			('ID Number', 4, 2),
			('Sum Insured', 4, 7), -- Finance Information
			('Finance House', 4, 7),
			('Main Driver ID Number', 4, 5), -- Driver Information
			('Main Driver Age', 4, 5),
			('Vehicle Extras Description', 4, 4), -- Additional Options
			('Vehicle Extras Value', 4, 4),
			('Radio Make', 4, 4),
			('Radio Model', 4, 4),
			('Radio Value', 4, 4),
			('Tools And Spare Parts Value', 4, 4),
			('Home Industry Sum Insured', 4, 4),
			('NN Accidental Damage Amount', 4, 4),
			('Current Insurance Period', 4, 6)  -- Insurance History
 
			--('Registration Number', 4, 2),
			--('Vehicle Engine Capacity (CC)', 4, 2),
			--('Vehicle Engine Number', 4, 2),
			--('Vehicle VIN Number', 4, 2)
