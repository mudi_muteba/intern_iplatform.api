﻿
  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Contact_Title') BEGIN EXEC('ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [FK_Contact_Title]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_StateProvince_Country') BEGIN EXEC('ALTER TABLE [md].[StateProvince] DROP CONSTRAINT [FK_StateProvince_Country]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK92F34A7D6331F7DC') BEGIN EXEC('ALTER TABLE [dbo].[IdentityUserClaim] DROP CONSTRAINT [FK92F34A7D6331F7DC]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Party_Address') BEGIN EXEC('ALTER TABLE [dbo].[Address] DROP CONSTRAINT [FK_Party_Address]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK25FB07746331F7DC') BEGIN EXEC('ALTER TABLE [dbo].[IdentityUserLogin] DROP CONSTRAINT [FK25FB07746331F7DC]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_PolicyFuneralAddressId_AddressId') BEGIN EXEC('ALTER TABLE [dbo].[PolicyFuneral] DROP CONSTRAINT [FK_PolicyFuneralAddressId_AddressId]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FKE1FC89AB6331F7DC') BEGIN EXEC('ALTER TABLE [dbo].[IdentityUserRole] DROP CONSTRAINT [FKE1FC89AB6331F7DC]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FKE1FC89AB9A3C3A4C') BEGIN EXEC('ALTER TABLE [dbo].[IdentityUserRole] DROP CONSTRAINT [FKE1FC89AB9A3C3A4C]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK6B681666DB2A8D2E') BEGIN EXEC('ALTER TABLE [dbo].[BankDetails] DROP CONSTRAINT [FK6B681666DB2A8D2E]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_PolicyHeaderClaimableItem_PolicyHeaderId_PolicyHeader_Id') BEGIN EXEC('ALTER TABLE [dbo].[PolicyHeaderClaimableItem] DROP CONSTRAINT [FK_PolicyHeaderClaimableItem_PolicyHeaderId_PolicyHeader_Id]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_UserSecurityRole_User') BEGIN EXEC('ALTER TABLE [dbo].[UserSecurityRole] DROP CONSTRAINT [FK_UserSecurityRole_User]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_PolicyHeaderClaimableItem_PolicyItemId_PolicyItem_Id') BEGIN EXEC('ALTER TABLE [dbo].[PolicyHeaderClaimableItem] DROP CONSTRAINT [FK_PolicyHeaderClaimableItem_PolicyItemId_PolicyItem_Id]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_UserSecurityRole_SecurityRole') BEGIN EXEC('ALTER TABLE [dbo].[UserSecurityRole] DROP CONSTRAINT [FK_UserSecurityRole_SecurityRole]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_UserChannel_User') BEGIN EXEC('ALTER TABLE [dbo].[UserChannel] DROP CONSTRAINT [FK_UserChannel_User]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_ProductFee_ProductFeeType') BEGIN EXEC('ALTER TABLE [dbo].[ProductFee] DROP CONSTRAINT [FK_ProductFee_ProductFeeType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_UserChannel_Channel') BEGIN EXEC('ALTER TABLE [dbo].[UserChannel] DROP CONSTRAINT [FK_UserChannel_Channel]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_ProductFee_PaymentPlan') BEGIN EXEC('ALTER TABLE [dbo].[ProductFee] DROP CONSTRAINT [FK_ProductFee_PaymentPlan]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_ProductFee_Product') BEGIN EXEC('ALTER TABLE [dbo].[ProductFee] DROP CONSTRAINT [FK_ProductFee_Product]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_PolicySpecialAddressId_AddressId') BEGIN EXEC('ALTER TABLE [dbo].[PolicySpecial] DROP CONSTRAINT [FK_PolicySpecialAddressId_AddressId]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_InsurersBranches_Organization') BEGIN EXEC('ALTER TABLE [dbo].[InsurersBranches] DROP CONSTRAINT [FK_InsurersBranches_Organization]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_InsurersBranches_InsurersBranchType') BEGIN EXEC('ALTER TABLE [dbo].[InsurersBranches] DROP CONSTRAINT [FK_InsurersBranches_InsurersBranchType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'User_UserAuthorisationGroup') BEGIN EXEC('ALTER TABLE [dbo].[UserAuthorisationGroup] DROP CONSTRAINT [User_UserAuthorisationGroup]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Organization_OrganizationType') BEGIN EXEC('ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [FK_Organization_OrganizationType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Channel_UserAuthorisationGroup') BEGIN EXEC('ALTER TABLE [dbo].[UserAuthorisationGroup] DROP CONSTRAINT [Channel_UserAuthorisationGroup]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Party_PartyType') BEGIN EXEC('ALTER TABLE [dbo].[Party] DROP CONSTRAINT [FK_Party_PartyType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Campaign_CampaingnReference') BEGIN EXEC('ALTER TABLE [dbo].[CampaignReference] DROP CONSTRAINT [Campaign_CampaingnReference]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Relationship_Party') BEGIN EXEC('ALTER TABLE [dbo].[Relationship] DROP CONSTRAINT [FK_Relationship_Party]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Role_Party') BEGIN EXEC('ALTER TABLE [dbo].[Role] DROP CONSTRAINT [FK_Role_Party]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'MembershipApplication') BEGIN EXEC('ALTER TABLE [dbo].[Memberships] DROP CONSTRAINT [MembershipApplication]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Role_RoleType') BEGIN EXEC('ALTER TABLE [dbo].[Role] DROP CONSTRAINT [FK_Role_RoleType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'MembershipUser') BEGIN EXEC('ALTER TABLE [dbo].[Memberships] DROP CONSTRAINT [MembershipUser]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'AuthorisationPointCategory_authorisationPoint') BEGIN EXEC('ALTER TABLE [dbo].[AuthorisationPoint] DROP CONSTRAINT [AuthorisationPointCategory_authorisationPoint]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Relationship_ParentParty') BEGIN EXEC('ALTER TABLE [dbo].[Relationship] DROP CONSTRAINT [FK_Relationship_ParentParty]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'RoleApplication') BEGIN EXEC('ALTER TABLE [dbo].[Roles] DROP CONSTRAINT [RoleApplication]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Product_RiskItemType') BEGIN EXEC('ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Product_RiskItemType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Relationship_RelationshipType') BEGIN EXEC('ALTER TABLE [dbo].[Relationship] DROP CONSTRAINT [FK_Relationship_RelationshipType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Lead_LeadStatus') BEGIN EXEC('ALTER TABLE [dbo].[Lead] DROP CONSTRAINT [FK_Lead_LeadStatus]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'UserApplication') BEGIN EXEC('ALTER TABLE [dbo].[Users] DROP CONSTRAINT [UserApplication]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Party_ContactDetail') BEGIN EXEC('ALTER TABLE [dbo].[Party] DROP CONSTRAINT [FK_Party_ContactDetail]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'UserProfile') BEGIN EXEC('ALTER TABLE [dbo].[Profiles] DROP CONSTRAINT [UserProfile]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'AuthorisationPoint_AuthorisationGroupPoint') BEGIN EXEC('ALTER TABLE [dbo].[AuthorisationGroupPoint] DROP CONSTRAINT [AuthorisationPoint_AuthorisationGroupPoint]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'UsersInRoleRole') BEGIN EXEC('ALTER TABLE [dbo].[UsersInRoles] DROP CONSTRAINT [UsersInRoleRole]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_ProductDescription_Product') BEGIN EXEC('ALTER TABLE [dbo].[ProductDescription] DROP CONSTRAINT [FK_ProductDescription_Product]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'UsersInRoleUser') BEGIN EXEC('ALTER TABLE [dbo].[UsersInRoles] DROP CONSTRAINT [UsersInRoleUser]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Lead_Party') BEGIN EXEC('ALTER TABLE [dbo].[Lead] DROP CONSTRAINT [FK_Lead_Party]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Campaign_PartyCampaign') BEGIN EXEC('ALTER TABLE [dbo].[PartyCampaign] DROP CONSTRAINT [Campaign_PartyCampaign]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_LeadActivity_Lead') BEGIN EXEC('ALTER TABLE [dbo].[LeadActivity] DROP CONSTRAINT [FK_LeadActivity_Lead]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Party_PartyCampaign') BEGIN EXEC('ALTER TABLE [dbo].[PartyCampaign] DROP CONSTRAINT [Party_PartyCampaign]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_AnswerDefinition_Product') BEGIN EXEC('ALTER TABLE [dbo].[AnswerDefinition] DROP CONSTRAINT [FK_AnswerDefinition_Product]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'PolicyHeader_PolicyItem') BEGIN EXEC('ALTER TABLE [dbo].[PolicyItem] DROP CONSTRAINT [PolicyHeader_PolicyItem]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_AnswerDefinition_QuestionAnswer') BEGIN EXEC('ALTER TABLE [dbo].[AnswerDefinition] DROP CONSTRAINT [FK_AnswerDefinition_QuestionAnswer]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'ClaimsHeader_ClaimsItem') BEGIN EXEC('ALTER TABLE [dbo].[ClaimsItem] DROP CONSTRAINT [ClaimsHeader_ClaimsItem]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'PolicyItem_ClaimsItem') BEGIN EXEC('ALTER TABLE [dbo].[ClaimsItem] DROP CONSTRAINT [PolicyItem_ClaimsItem]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'ClaimsItem_ClaimsItemQuestionAnswer') BEGIN EXEC('ALTER TABLE [dbo].[ClaimsItemQuestionAnswer] DROP CONSTRAINT [ClaimsItem_ClaimsItemQuestionAnswer]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Configuration_Party') BEGIN EXEC('ALTER TABLE [dbo].[Configuration] DROP CONSTRAINT [FK_Configuration_Party]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_ClaimsQuestion_QuestionGroup') BEGIN EXEC('ALTER TABLE [md].[ClaimsQuestion] DROP CONSTRAINT [FK_ClaimsQuestion_QuestionGroup]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_ClaimsQuestion_QuestionType') BEGIN EXEC('ALTER TABLE [md].[ClaimsQuestion] DROP CONSTRAINT [FK_ClaimsQuestion_QuestionType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Campaign_LeadImport') BEGIN EXEC('ALTER TABLE [dbo].[LeadImport] DROP CONSTRAINT [Campaign_LeadImport]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Individual_Party') BEGIN EXEC('ALTER TABLE [dbo].[Individual] DROP CONSTRAINT [FK_Individual_Party]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'User_LeadImport') BEGIN EXEC('ALTER TABLE [dbo].[LeadImport] DROP CONSTRAINT [User_LeadImport]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuoteUploadLog_Product') BEGIN EXEC('ALTER TABLE [dbo].[QuoteUploadLog] DROP CONSTRAINT [FK_QuoteUploadLog_Product]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Individual_Gender') BEGIN EXEC('ALTER TABLE [dbo].[Individual] DROP CONSTRAINT [FK_Individual_Gender]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Party_LeadActivity') BEGIN EXEC('ALTER TABLE [dbo].[LeadActivity] DROP CONSTRAINT [Party_LeadActivity]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_ClaimsQuestionAnswer_Question') BEGIN EXEC('ALTER TABLE [md].[ClaimsQuestionAnswer] DROP CONSTRAINT [FK_ClaimsQuestionAnswer_Question]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Individual_Title') BEGIN EXEC('ALTER TABLE [dbo].[Individual] DROP CONSTRAINT [FK_Individual_Title]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'ProposalHeader_ProposalLeadActivity') BEGIN EXEC('ALTER TABLE [dbo].[ProposalLeadActivity] DROP CONSTRAINT [ProposalHeader_ProposalLeadActivity]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Individual_Occupation') BEGIN EXEC('ALTER TABLE [dbo].[Individual] DROP CONSTRAINT [FK_Individual_Occupation]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'PolicyHeader_PolicyLeadActivity') BEGIN EXEC('ALTER TABLE [dbo].[PolicyLeadActivity] DROP CONSTRAINT [PolicyHeader_PolicyLeadActivity]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Note_Individual') BEGIN EXEC('ALTER TABLE [dbo].[Note] DROP CONSTRAINT [FK_Note_Individual]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Individual_HomeLanguage') BEGIN EXEC('ALTER TABLE [dbo].[Individual] DROP CONSTRAINT [FK_Individual_HomeLanguage]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'ClaimsHeader_ClaimsLeadActivity') BEGIN EXEC('ALTER TABLE [dbo].[ClaimsLeadActivity] DROP CONSTRAINT [ClaimsHeader_ClaimsLeadActivity]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Note_Party') BEGIN EXEC('ALTER TABLE [dbo].[Note] DROP CONSTRAINT [FK_Note_Party]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Individual_MaritalStatus') BEGIN EXEC('ALTER TABLE [dbo].[Individual] DROP CONSTRAINT [FK_Individual_MaritalStatus]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'QuoteHeader_QuotedLeadActivity') BEGIN EXEC('ALTER TABLE [dbo].[QuotedLeadActivity] DROP CONSTRAINT [QuoteHeader_QuotedLeadActivity]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_PolicyLeadActivity_PolicyHeader') BEGIN EXEC('ALTER TABLE [dbo].[PolicyLeadActivity] DROP CONSTRAINT [FK_PolicyLeadActivity_PolicyHeader]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Quote_QuoteAcceptedLeadActivity') BEGIN EXEC('ALTER TABLE [dbo].[QuoteAcceptedLeadActivity] DROP CONSTRAINT [Quote_QuoteAcceptedLeadActivity]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Lead_LeadQuality') BEGIN EXEC('ALTER TABLE [dbo].[Lead] DROP CONSTRAINT [FK_Lead_LeadQuality]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Organization_Party') BEGIN EXEC('ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [FK_Organization_Party]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_AuditTrailItem_Party') BEGIN EXEC('ALTER TABLE [dbo].[AuditTrailItem] DROP CONSTRAINT [FK_AuditTrailItem_Party]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_AuditTrailItem_Individual') BEGIN EXEC('ALTER TABLE [dbo].[AuditTrailItem] DROP CONSTRAINT [FK_AuditTrailItem_Individual]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Product_Owner') BEGIN EXEC('ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Product_Owner]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Product_Provider') BEGIN EXEC('ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Product_Provider]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Product_ProductType') BEGIN EXEC('ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Product_ProductType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_LeadActivity_User') BEGIN EXEC('ALTER TABLE [dbo].[LeadActivity] DROP CONSTRAINT [FK_LeadActivity_User]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_UserIndividual_User') BEGIN EXEC('ALTER TABLE [dbo].[UserIndividual] DROP CONSTRAINT [FK_UserIndividual_User]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Proposal_Party') BEGIN EXEC('ALTER TABLE [dbo].[ProposalDefinition] DROP CONSTRAINT [Proposal_Party]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_UserIndividual_Individual') BEGIN EXEC('ALTER TABLE [dbo].[UserIndividual] DROP CONSTRAINT [FK_UserIndividual_Individual]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Proposal_Cover') BEGIN EXEC('ALTER TABLE [dbo].[ProposalDefinition] DROP CONSTRAINT [Proposal_Cover]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_AssetExtras_Asset') BEGIN EXEC('ALTER TABLE [dbo].[AssetExtras] DROP CONSTRAINT [FK_AssetExtras_Asset]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Proposal_Product') BEGIN EXEC('ALTER TABLE [dbo].[ProposalDefinition] DROP CONSTRAINT [Proposal_Product]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_LeadActivity_Campaign') BEGIN EXEC('ALTER TABLE [dbo].[LeadActivity] DROP CONSTRAINT [FK_LeadActivity_Campaign]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_MessageQueue_Clients') BEGIN EXEC('ALTER TABLE [dbo].[MessageClient] DROP CONSTRAINT [FK_MessageQueue_Clients]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'ProposalQuestionAnswer_Proposal') BEGIN EXEC('ALTER TABLE [dbo].[ProposalQuestionAnswer] DROP CONSTRAINT [ProposalQuestionAnswer_Proposal]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_AssetAddress_Asset') BEGIN EXEC('ALTER TABLE [dbo].[AssetAddress] DROP CONSTRAINT [FK_AssetAddress_Asset]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Proposal_ProposalHeader') BEGIN EXEC('ALTER TABLE [dbo].[ProposalDefinition] DROP CONSTRAINT [Proposal_ProposalHeader]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_AssetAddress_Address') BEGIN EXEC('ALTER TABLE [dbo].[AssetAddress] DROP CONSTRAINT [FK_AssetAddress_Address]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_CoverDefinition_Cover') BEGIN EXEC('ALTER TABLE [dbo].[CoverDefinition] DROP CONSTRAINT [FK_CoverDefinition_Cover]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'AssetVehicle_Asset') BEGIN EXEC('ALTER TABLE [dbo].[AssetVehicle] DROP CONSTRAINT [AssetVehicle_Asset]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_CoverDefinition_CoverDefinition') BEGIN EXEC('ALTER TABLE [dbo].[CoverDefinition] DROP CONSTRAINT [FK_CoverDefinition_CoverDefinition]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'ProposalHeader_LeadActivity') BEGIN EXEC('ALTER TABLE [dbo].[ProposalHeader] DROP CONSTRAINT [ProposalHeader_LeadActivity]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_CoverDefinition_Product') BEGIN EXEC('ALTER TABLE [dbo].[CoverDefinition] DROP CONSTRAINT [FK_CoverDefinition_Product]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_ProductBenefit_CoverDefinition') BEGIN EXEC('ALTER TABLE [dbo].[ProductBenefit] DROP CONSTRAINT [FK_ProductBenefit_CoverDefinition]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_CoverDefinition_CoverDefinitionType') BEGIN EXEC('ALTER TABLE [dbo].[CoverDefinition] DROP CONSTRAINT [FK_CoverDefinition_CoverDefinitionType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Campaign_Product') BEGIN EXEC('ALTER TABLE [dbo].[CampaignProduct] DROP CONSTRAINT [FK_Campaign_Product]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_ClientSite_Clients') BEGIN EXEC('ALTER TABLE [dbo].[MessageClient] DROP CONSTRAINT [FK_ClientSite_Clients]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Product_Campaign') BEGIN EXEC('ALTER TABLE [dbo].[CampaignProduct] DROP CONSTRAINT [FK_Product_Campaign]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_ClientSite_Organization') BEGIN EXEC('ALTER TABLE [dbo].[ClientSite] DROP CONSTRAINT [FK_ClientSite_Organization]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'ProposalQuestionAnswer_QuestionDefinition') BEGIN EXEC('ALTER TABLE [dbo].[ProposalQuestionAnswer] DROP CONSTRAINT [ProposalQuestionAnswer_QuestionDefinition]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Asset_Party') BEGIN EXEC('ALTER TABLE [dbo].[Asset] DROP CONSTRAINT [Asset_Party]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_SecondLevelQuestionAnswer_SecondLevelQuestion') BEGIN EXEC('ALTER TABLE [md].[SecondLevelQuestionAnswer] DROP CONSTRAINT [FK_SecondLevelQuestionAnswer_SecondLevelQuestion]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuoteItem_Quote') BEGIN EXEC('ALTER TABLE [dbo].[QuoteItem] DROP CONSTRAINT [FK_QuoteItem_Quote]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'QuestionDefinition_QuestionDefinitionGroupType') BEGIN EXEC('ALTER TABLE [dbo].[QuestionDefinition] DROP CONSTRAINT [QuestionDefinition_QuestionDefinitionGroupType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuoteItem_CoverDefinition') BEGIN EXEC('ALTER TABLE [dbo].[QuoteItem] DROP CONSTRAINT [FK_QuoteItem_CoverDefinition]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuestionAnswer_Question') BEGIN EXEC('ALTER TABLE [md].[QuestionAnswer] DROP CONSTRAINT [FK_QuestionAnswer_Question]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'ProposalQuestionAnswer_QuestionType') BEGIN EXEC('ALTER TABLE [dbo].[ProposalQuestionAnswer] DROP CONSTRAINT [ProposalQuestionAnswer_QuestionType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_SecondLevelQuestion_SecondLevelQuestionGroup') BEGIN EXEC('ALTER TABLE [md].[SecondLevelQuestion] DROP CONSTRAINT [FK_SecondLevelQuestion_SecondLevelQuestionGroup]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Question_QuestionType') BEGIN EXEC('ALTER TABLE [md].[Question] DROP CONSTRAINT [FK_Question_QuestionType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Campaign_Agent') BEGIN EXEC('ALTER TABLE [dbo].[CampaignAgent] DROP CONSTRAINT [FK_Campaign_Agent]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Agent_Campaign') BEGIN EXEC('ALTER TABLE [dbo].[CampaignAgent] DROP CONSTRAINT [FK_Agent_Campaign]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Question_QuestionGroup') BEGIN EXEC('ALTER TABLE [md].[Question] DROP CONSTRAINT [FK_Question_QuestionGroup]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_SecondLevelQuestionDefinition_SecondLevelQuestion') BEGIN EXEC('ALTER TABLE [dbo].[SecondLevelQuestionDefinition] DROP CONSTRAINT [FK_SecondLevelQuestionDefinition_SecondLevelQuestion]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_SecondLevelQuestionDefinition_SecondLevelQuestionDefinition') BEGIN EXEC('ALTER TABLE [dbo].[SecondLevelQuestionDefinition] DROP CONSTRAINT [FK_SecondLevelQuestionDefinition_SecondLevelQuestionDefinition]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_SecondLevelQuestionDefinition_CoverDefinition') BEGIN EXEC('ALTER TABLE [dbo].[SecondLevelQuestionDefinition] DROP CONSTRAINT [FK_SecondLevelQuestionDefinition_CoverDefinition]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_SecondLevelQuestionDefinition_QuestionDefinitionType') BEGIN EXEC('ALTER TABLE [dbo].[SecondLevelQuestionDefinition] DROP CONSTRAINT [FK_SecondLevelQuestionDefinition_QuestionDefinitionType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_SecondLevelQuestion_QuestionType') BEGIN EXEC('ALTER TABLE [md].[SecondLevelQuestion] DROP CONSTRAINT [FK_SecondLevelQuestion_QuestionType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuestionDefinition_Question') BEGIN EXEC('ALTER TABLE [dbo].[QuestionDefinition] DROP CONSTRAINT [FK_QuestionDefinition_Question]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuestionDefinition_QuestionDefinition') BEGIN EXEC('ALTER TABLE [dbo].[QuestionDefinition] DROP CONSTRAINT [FK_QuestionDefinition_QuestionDefinition]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuestionDefinition_CoverDefinition') BEGIN EXEC('ALTER TABLE [dbo].[QuestionDefinition] DROP CONSTRAINT [FK_QuestionDefinition_CoverDefinition]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuestionDefinition_QuestionDefinitionType') BEGIN EXEC('ALTER TABLE [dbo].[QuestionDefinition] DROP CONSTRAINT [FK_QuestionDefinition_QuestionDefinitionType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Campaign_TeamCampaign') BEGIN EXEC('ALTER TABLE [dbo].[TeamCampaign] DROP CONSTRAINT [Campaign_TeamCampaign]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Team_TeamCampaign') BEGIN EXEC('ALTER TABLE [dbo].[TeamCampaign] DROP CONSTRAINT [Team_TeamCampaign]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_TagCampaignMap_Tag') BEGIN EXEC('ALTER TABLE [dbo].[TagCampaignMap] DROP CONSTRAINT [FK_TagCampaignMap_Tag]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'Team_TeamUser') BEGIN EXEC('ALTER TABLE [dbo].[TeamUser] DROP CONSTRAINT [Team_TeamUser]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_TagProductMap_Product') BEGIN EXEC('ALTER TABLE [dbo].[TagProductMap] DROP CONSTRAINT [FK_TagProductMap_Product]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuestionAnswerExclusion_Product') BEGIN EXEC('ALTER TABLE [dbo].[QuestionAnswerExclusion] DROP CONSTRAINT [FK_QuestionAnswerExclusion_Product]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'User_TeamUser') BEGIN EXEC('ALTER TABLE [dbo].[TeamUser] DROP CONSTRAINT [User_TeamUser]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_TagProductMap_Tag') BEGIN EXEC('ALTER TABLE [dbo].[TagProductMap] DROP CONSTRAINT [FK_TagProductMap_Tag]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuestionAnswerExclusion_CoverDefinition') BEGIN EXEC('ALTER TABLE [dbo].[QuestionAnswerExclusion] DROP CONSTRAINT [FK_QuestionAnswerExclusion_CoverDefinition]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuestionAnswerExclusion_QuestionDefinition') BEGIN EXEC('ALTER TABLE [dbo].[QuestionAnswerExclusion] DROP CONSTRAINT [FK_QuestionAnswerExclusion_QuestionDefinition]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuestionAnswerExclusion_QuestionAnswer') BEGIN EXEC('ALTER TABLE [dbo].[QuestionAnswerExclusion] DROP CONSTRAINT [FK_QuestionAnswerExclusion_QuestionAnswer]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'AssetRiskItem_Asset') BEGIN EXEC('ALTER TABLE [dbo].[AssetRiskItem] DROP CONSTRAINT [AssetRiskItem_Asset]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_ClaimsQuestionDefinition_ClaimsQuestionGroup') BEGIN EXEC('ALTER TABLE [md].[ClaimsQuestionDefinition] DROP CONSTRAINT [FK_ClaimsQuestionDefinition_ClaimsQuestionGroup]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_QuoteAcceptedLeadActivity_Quote') BEGIN EXEC('ALTER TABLE [dbo].[QuoteAcceptedLeadActivity] DROP CONSTRAINT [FK_QuoteAcceptedLeadActivity_Quote]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'ProposalDefinition_FuneralMember') BEGIN EXEC('ALTER TABLE [dbo].[FuneralMember] DROP CONSTRAINT [ProposalDefinition_FuneralMember]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Address_AddressType') BEGIN EXEC('ALTER TABLE [dbo].[Address] DROP CONSTRAINT [FK_Address_AddressType]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Contact_Gender') BEGIN EXEC('ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [FK_Contact_Gender]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_ClaimsQuestionDefinition_ClaimsQuestion') BEGIN EXEC('ALTER TABLE [md].[ClaimsQuestionDefinition] DROP CONSTRAINT [FK_ClaimsQuestionDefinition_ClaimsQuestion]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Contact_HomeLanguage') BEGIN EXEC('ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [FK_Contact_HomeLanguage]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Contact_MaritalStatus') BEGIN EXEC('ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [FK_Contact_MaritalStatus]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Address_StateProvince') BEGIN EXEC('ALTER TABLE [dbo].[Address] DROP CONSTRAINT [FK_Address_StateProvince]') END

  IF EXISTS (SELECT name FROM sys.objects WHERE name = 'FK_Contact_Occupation') BEGIN EXEC('ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [FK_Contact_Occupation]') END