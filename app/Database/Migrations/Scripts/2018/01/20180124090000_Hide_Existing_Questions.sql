
declare @ProductCode VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT, @CoverID INT , @CoverName VARCHAR(50)
declare @RerunCreationScript BIT
declare @RerunCreationScriptVersion VARCHAR(50)

SET @RerunCreationScript = 0
Set @RerunCreationScriptVersion = '20180123150000'
--------------------Service Plan-----------------------

SELECT 'The Select Service Plan' As [Section]
SET @ProductCode = N'MOTORSERVICE'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 378			

SET @CoverName = 'The Select Service Plan'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL)
BEGIN

select @coverDefId
Update [dbo].[QuestionDefinition] Set Hide = 1, RequiredForQuote = 0, Ratingfactor = 0 Where CoverDefinitionId = @coverDefId
SET @RerunCreationScript = 1

END

--------------------Tyre & Rim-----------------------

SELECT 'Tyre & Rim' As [Section]
SET @ProductCode = N'TYRERIMVAPS'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 371			

SET @CoverName = 'Tyre & Rim'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL)
BEGIN

select @coverDefId
Update [dbo].[QuestionDefinition] Set Hide = 1, RequiredForQuote = 0, Ratingfactor = 0 Where CoverDefinitionId = @coverDefId
SET @RerunCreationScript = 1

END

--------------------Motor Warranty-----------------------

SELECT 'Motor Warranty' As [Section]
SET @ProductCode = N'MOTORWAR'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 375			

SET @CoverName = 'Motor Warranty'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL)
BEGIN

select @coverDefId
Update [dbo].[QuestionDefinition] Set Hide = 1, RequiredForQuote = 0, Ratingfactor = 0 Where CoverDefinitionId = @coverDefId
SET @RerunCreationScript = 1

END


--------------------Excess Eraser-----------------------

SELECT 'Excess Eraser' As [Section]
SET @ProductCode = N'EXCESSERASER'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 385			

SET @CoverName = 'Excess Eraser'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL)
BEGIN

select @coverDefId
Update [dbo].[QuestionDefinition] Set Hide = 1, RequiredForQuote = 0, Ratingfactor = 0 Where CoverDefinitionId = @coverDefId
SET @RerunCreationScript = 1

END

--------------------Power Train Warranty-----------------------

SELECT 'Power Train Warranty' As [Section]
SET @ProductCode = N'POWERTRAINWAR'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 387			

SET @CoverName = 'Power Train Warranty'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL)
BEGIN

select @coverDefId
Update [dbo].[QuestionDefinition] Set Hide = 1, RequiredForQuote = 0, Ratingfactor = 0 Where CoverDefinitionId = @coverDefId
SET @RerunCreationScript = 1

END

--------------------Cosmetic Plan-----------------------

SELECT 'Cosmetic Plan' As [Section]
SET @ProductCode = N'COSMETICPLAN'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 386			

SET @CoverName = 'Cosmetic Plan'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL)
BEGIN

select @coverDefId
Update [dbo].[QuestionDefinition] Set Hide = 1, RequiredForQuote = 0, Ratingfactor = 0 Where CoverDefinitionId = @coverDefId
SET @RerunCreationScript = 1

END

IF (@RerunCreationScript = 1)
BEGIN
   Select 'Intialising rerun of script ' + @RerunCreationScriptVersion
   Delete From [dbo].[_Version_iBroker] From _Version_iBroker Where [Version] = @RerunCreationScriptVersion 
End