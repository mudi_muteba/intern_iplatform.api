
Update Product Set [Name] = 'The Select Warranty Extender' Where [Name] = 'Warranty' And ProductCode = 'MOTORWAR'
Update Product Set [Name] = 'The Select Warranty Plan 1' Where [Name] = 'Warranty' And ProductCode = 'MOTORWAR1'
Update Product Set [Name] = 'The Select Warranty Plan 2' Where [Name] = 'Warranty' And ProductCode = 'MOTORWAR2'
Update Product Set [Name] = 'The Select Warranty Plan 3' Where [Name] = 'Warranty' And ProductCode = 'MOTORWAR3'
Update Product Set [Name] = 'The Select Warranty Plan 4' Where [Name] = 'Warranty' And ProductCode = 'MOTORWAR4'
Update Product Set [Name] = 'The Select Warranty Plan 5' Where [Name] = 'Warranty' And ProductCode = 'MOTORWAR5'
Update Product Set [Name] = 'The Select Warranty Plan 6' Where [Name] = 'Warranty' And ProductCode = 'MOTORWAR6'
Update Product Set [Name] = 'The Select Warranty Plan 7' Where [Name] = 'Warranty' And ProductCode = 'MOTORWAR7'
Update Product Set [Name] = 'The Select Warranty Plan 8' Where [Name] = 'Warranty' And ProductCode = 'MOTORWAR8'


DECLARE @ProductCode VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

SET @ProductCode = N'MOTORWAR'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 375			

Update  [dbo].[CoverDefinition] Set DisplayName = 'The Select Warranty Extender'  WHERE ProductId = @productid AND CoverId = @CoverID

