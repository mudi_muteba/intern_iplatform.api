if exists (select * from sys.objects where object_id = object_id(N'Reports_ProposalCover_QuestionAnswer') and type in (N'P', N'PC'))
begin
	drop procedure Reports_ProposalCover_QuestionAnswer
end
go

create procedure Reports_ProposalCover_QuestionAnswer
(
	@QuoteId int
)
as

select c.Name as 'CoverName', pd.Description as 'Asset', qd.DisplayName as 'Question', 
	CASE WHEN (pqa.QuestionTypeId = 3)
	THEN (SELECT Answer FROM md.QuestionAnswer WHERE Id = pqa.Answer) 
	ELSE pqa.Answer 
	END AS 'Answer'	
from ProposalQuestionAnswer as pqa
join ProposalDefinition as pd on pd.Id = pqa.ProposalDefinitionId and pd.IsDeleted <> 1
join md.Cover as c on c.Id = pd.CoverId
join QuestionDefinition as qd on qd.Id = pqa.QuestionDefinitionId and qd.IsDeleted <> 1
join ProposalHeader as ph on ph.Id = pd.ProposalHeaderId and ph.IsDeleted <> 1
join QuoteHeader as qh on qh.ProposalHeaderId =  ph.Id and qh.IsDeleted <> 1
join Quote as q on q.QuoteHeaderId = qh.Id and q.IsDeleted <> 1
where q.Id = @QuoteId and qd.DisplayName not like '%dummy question%'
order by qd.QuestionGroupId