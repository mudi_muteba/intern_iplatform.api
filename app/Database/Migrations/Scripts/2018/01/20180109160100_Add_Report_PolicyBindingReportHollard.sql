if not exists(select * from Report where Name = 'Policy Binding Question Answers' and ChannelId = 4)
	begin
		insert into Report (info, ReportCategoryId, ReportTypeId, ReportFormatId, Name, [Description], SourceFile, IsVisibleInReportViewer, IsVisibleInScheduler, IsActive, ChannelId, IsDeleted)
		values ('Policy Binding Question Answers info', 1, 2, 3, 'Policy Binding Question Answers', 'Policy Binding Question Answers', 'PolicyBindingQuestionAnswerReport.trdx', 0, 0, 1,4,0)
	end
else
	begin
		update Report
		set
			ReportCategoryId = 1,
			ReportTypeId = 2,
			ReportFormatId = 3,
			Name = 'Policy Binding Question Answers',
			[Description] = 'Policy Binding Question Answers',
			SourceFile = 'PolicyBindingQuestionAnswersReport.trdx',
			IsVisibleInReportViewer = 0,
			IsVisibleInScheduler = 0,
			IsActive = 1,
			Info = 'Policy Binding Question Answers info',
			IsDeleted = 0			
		where
			Name = 'Policy Binding Question Answers' and
			ChannelId = 4
	end
go

declare @ReportId int

if exists(select * from Report where Name = 'Policy Binding Question Answers' and ChannelId = 4)
	begin
		select @ReportId = Id from Report  where Name = 'Policy Binding Question Answers' and ChannelId = 4

		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler, IsDeleted) values (@ReportId, 'Quote Id', 'QuoteId', '', 2, 0, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler, IsDeleted) values (@ReportId, 'Party Id', 'PartyId', '', 2, 0, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler, IsDeleted) values (@ReportId, 'Channel Id', 'ChannelId', '', 2, 0, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler, IsDeleted) values (@ReportId, 'Current Channel Id', 'CurrentChannelId', '', 2, 0, 0, 0)
		insert into ReportParam (ReportId, Name, Field, [Description], DataTypeId, IsVisibleInReportViewer, IsVisibleInScheduler, IsDeleted) values (@ReportId, 'Agent Id', 'AgentId', '', 2, 0, 0, 0)
	end
go