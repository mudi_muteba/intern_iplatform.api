
DECLARE @id INT
DECLARE @ProductCode VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
declare @ProductOwnerId int
declare @OrganizationCode VARCHAR(50)
declare @ParentID INT


Set @OrganizationCode = N'MOTORV'
SET @ProductOwnerId = ( SELECT TOP 1 PartyId FROM dbo.Organization where  Code= @OrganizationCode)


--------------------Tyre & Rim Extra Products-----------------------
SELECT 'Tyre & Rim' As [Section]
SET @ProductCode = N'TYRERIMVAPSPLANA'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 371 			

SET @CoverName = 'Tyre & Rim Plan 1'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
else
begin
   Update CoverDefinition Set DisplayName = @CoverName WHERE ProductId = @productid AND CoverId = @CoverID
end

SET @ProductCode = N'TYRERIMVAPSPLANB'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 371 			

SET @CoverName = 'Tyre & Rim Plan 2'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
else
begin
   Update CoverDefinition Set DisplayName = @CoverName WHERE ProductId = @productid AND CoverId = @CoverID
end

SET @ProductCode = N'TYRERIMVAPSPLANC'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 371 			

SET @CoverName = 'Tyre & Rim Plan 3'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
else
begin
   Update CoverDefinition Set DisplayName = @CoverName WHERE ProductId = @productid AND CoverId = @CoverID
end

SET @ProductCode = N'TYRERIMVAPSPLAND'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 371 			

SET @CoverName = 'Tyre & Rim Plan 4'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
else
begin
   Update CoverDefinition Set DisplayName = @CoverName WHERE ProductId = @productid AND CoverId = @CoverID
end

SET @ProductCode = N'TYRERIMVAPSPLANE'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 371 			

SET @CoverName = 'Tyre & Rim Plan 5'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
else
begin
   Update CoverDefinition Set DisplayName = @CoverName WHERE ProductId = @productid AND CoverId = @CoverID
end

SET @ProductCode = N'TYRERIMVAPS'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 371 			

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

SET @CoverDefinitionId = @coverDefId

if exists(select * from QuestionDefinition where QuestionId=1591 and CoverDefinitionId = @CoverDefinitionId)
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [Hide] = 1
				  Where QuestionId=1591 and CoverDefinitionId = @CoverDefinitionId
             end
else

SELECT 'Product Info' As [Info Type], * FROM dbo.Product WHERE id IN (@productid)
SELECT 'Cover Definition Info' As [Info Type], * FROM dbo.CoverDefinition WHERE ProductId IN (@productid)

--------------------Cosmetic Plan-----------------------
SELECT 'Cosmetic Plan Extra Products' As [Section]
SET @ProductCode = N'COSMETICPLANBRONZE'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 386 			

SET @CoverName = 'Cosmetic Plan Bronze'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END

SET @ProductCode = N'COSMETICPLANGOLD'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 386 			

SET @CoverName = 'Cosmetic Plan Gold'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END

SET @ProductCode = N'COSMETICPLANPLATINUM'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 386 			

SET @CoverName = 'Cosmetic Plan Platinum'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END

SET @ProductCode = N'COSMETICPLANTITANIUM'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 386 			

SET @CoverName = 'Cosmetic Plan Titanium'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END

SET @ProductCode = N'COSMETICPLAN'

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 386 			

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

SET @CoverDefinitionId = @coverDefId

if exists(select * from QuestionDefinition where QuestionId=1592 and CoverDefinitionId = @CoverDefinitionId)
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [Hide] = 1
				  Where QuestionId=1592 and CoverDefinitionId = @CoverDefinitionId
             end
else
             
			   
SELECT 'Product Info' As [Info Type], * FROM dbo.Product WHERE id IN (@productid)
SELECT 'Cover Definition Info' As [Info Type], * FROM dbo.CoverDefinition WHERE ProductId IN (@productid)		   

SELECT 'Organization' As [Section]
SELECT * From dbo.Organization Where PartyId In (@ProductOwnerId)



