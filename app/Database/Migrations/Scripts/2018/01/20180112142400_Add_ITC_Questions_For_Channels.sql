﻿IF NOT EXISTS (SELECT TOP 1 *  
    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
    WHERE CONSTRAINT_NAME='DF_ItcQuestionDefinition_DateCreated')
BEGIN
ALTER TABLE dbo.ItcQuestionDefinition
	ADD CONSTRAINT DF_ItcQuestionDefinition_DateCreated
	DEFAULT (GETUTCDATE())
	FOR DateCreated;
END

IF NOT EXISTS (SELECT TOP 1 *  
    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
    WHERE CONSTRAINT_NAME='DF_ItcQuestionDefinition_IsDeleted')
BEGIN
ALTER TABLE dbo.ItcQuestionDefinition
	ADD CONSTRAINT DF_ItcQuestionDefinition_IsDeleted
	DEFAULT (0)
	FOR IsDeleted;
END

IF NOT EXISTS (SELECT TOP 1 *  
    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
    WHERE CONSTRAINT_NAME='UQ_ItcQuestionDefinition_Name_ChannelId_DateUpdated_IsDeleted')
BEGIN
ALTER TABLE dbo.ItcQuestionDefinition
	ADD CONSTRAINT UQ_ItcQuestionDefinition_Name_ChannelId_DateUpdated_IsDeleted
	UNIQUE([Name], ChannelId, DateUpdated, IsDeleted);
END

IF NOT EXISTS (SELECT TOP 1 *  
    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
    WHERE CONSTRAINT_NAME='FK_ItcQuestionDefinition_ChannelId_Channel_Id')
BEGIN
ALTER TABLE dbo.ItcQuestionDefinition
	ADD CONSTRAINT FK_ItcQuestionDefinition_ChannelId_Channel_Id
	FOREIGN KEY (ChannelId) 
	REFERENCES dbo.Channel (Id);
END

-- Add Defaults --
INSERT INTO dbo.ItcQuestionDefinition
	([Name], ChannelId, QuestionText)
	VALUES (
		'Default'
		, NULL
		, 'Good day, welcome to ##PLACEHOLDER## and thank you for your interest in our insurance offerings and products. <br />In order for us to give you great service and low competitive pricing we need your permission to perform a quick ITC score. <br />This ITC Score is free of charge and will also return all your latest contact information to make this quoting process a lot quicker for you. <br />Sir/Ma''am do you consent to me performing an ITC check?'
	), (
		'AIG'
		, 1
		,'In order to provide you with the best possible premium I need your permission to screen your application with the risk management companies. <br />Can we proceed?'
	)