﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'Domestic Armour CC'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  Name= @Name)

SELECT @productid
if (@productid IS NULL)
BEGIN
-- Out of scope
RETURN
 END


 --Hide all province, suburb and postal code questions for AIG TRUE BLUE
UPDATE dbo.QuestionDefinition 
SET Hide = 1
WHERE Id IN (
SELECT ID FROM dbo.QuestionDefinition WHERE CoverDefinitionId IN
	(SELECT ID FROM dbo.CoverDefinition WHERE ProductId = @productid)
	AND QuestionId IN (89,90,47, 135,154,91)
)

 --Update all Voluntary Excess wording on questions for AIG TRUE BLUE
UPDATE dbo.QuestionDefinition 
SET DisplayName = 'Excess Structure'
WHERE Id IN (
SELECT Id FROM dbo.QuestionDefinition WHERE CoverDefinitionId IN
	(SELECT ID FROM dbo.CoverDefinition WHERE ProductId = @productid)
	AND QuestionId IN (1501,1513,1530)
)

------------------Motor-----------------------
SET @CoverID = 218 			 

SET @CoverName = 'Motor'

SET @CoverDefinitionId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

--Drivers Licence Mandatory
UPDATE dbo.QuestionDefinition
SET RatingFactor = 1
, RequiredForQuote = 1
WHERE questionId IN ( 73)
AND CoverDefinitionId = @CoverDefinitionId




------------------Household Contents-----------------------
SET @CoverID = 78 			 

SET @CoverName = 'Household Contents'

SET @CoverDefinitionId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

--Contents Installed Mandatory
UPDATE dbo.QuestionDefinition
SET RatingFactor = 1
, RequiredForQuote = 1
WHERE questionId IN ( 1521)
AND CoverDefinitionId = @CoverDefinitionId


