﻿
DECLARE @id INT
DECLARE @Name VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
 
SET @Name = N'Domestic Armour CC'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  Name= @Name)

SELECT @productid
if (@productid IS NULL)
BEGIN
-- Out of scope
RETURN
 END

------------------Household Contents-----------------------
SET @CoverID = 78 			 

SET @CoverName = 'Household Contents'

SET @CoverDefinitionId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

UPDATE dbo.QuestionDefinition
SET QuestionId = 1555
WHERE questionId IN ( 49)
AND CoverDefinitionId = @CoverDefinitionId

UPDATE dbo.QuestionDefinition
SET QuestionId = 1532
WHERE questionId IN ( 50)
AND CoverDefinitionId = @CoverDefinitionId



------------------House Owners-----------------------
SET @CoverID = 44 			 

SET @CoverName = 'House Owners'

SET @CoverDefinitionId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

UPDATE dbo.QuestionDefinition
SET QuestionId = 1526
, Hide = 0
WHERE questionId IN ( 76)
AND CoverDefinitionId = @CoverDefinitionId


------------------Motorcycle-----------------------
SET @CoverID = 151 			 

SET @CoverName = 'Motorcycle'

SET @CoverDefinitionId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

UPDATE dbo.QuestionDefinition
SET QuestionId = 1557
, Hide = 0
WHERE questionId IN ( 53)
AND CoverDefinitionId = @CoverDefinitionId
