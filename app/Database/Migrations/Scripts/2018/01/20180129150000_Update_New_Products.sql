--FCM - This script builds the different Cosmectic Plan and Tyre & Rim Options

DECLARE @ProductCode VARCHAR(50)
declare @ProductOwnerId int
declare @OrganizationCode VARCHAR(50)

Set @OrganizationCode = N'MOTORV'
SET @ProductOwnerId = ( SELECT TOP 1 PartyId FROM dbo.Organization where  Code= @OrganizationCode)

--------------------Tyre & Rim Plan A-----------------------
SELECT 'Tyre & Rim Plan 1' As [Section]
SET @ProductCode = N'TYRERIMVAPSPLANA'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Tyre & Rim Plan 1', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'TYRERIMVAPSPLANA', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 1)
			   end
else
      begin
	       update dbo.Product Set [Name] = 'Tyre & Rim Plan 1', [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 1
		   Where ProductCode = @ProductCode
	  end


	  --------------------Tyre & Rim Plan B-----------------------
SELECT 'Tyre & Rim Plan 2' As [Section]
SET @ProductCode = N'TYRERIMVAPSPLANB'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Tyre & Rim Plan 2', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'TYRERIMVAPSPLANB', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 1)
			   end
else
      begin
	       update dbo.Product Set [Name] = 'Tyre & Rim Plan 2', [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 1
		   Where ProductCode = @ProductCode
	  end

	  	  --------------------Tyre & Rim Plan C-----------------------
SELECT 'Tyre & Rim Plan 3' As [Section]
SET @ProductCode = N'TYRERIMVAPSPLANC'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Tyre & Rim Plan 3', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'TYRERIMVAPSPLANC', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 1)
			   end
else
      begin
	       update dbo.Product Set [Name] = 'Tyre & Rim Plan 3', [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 1
		   Where ProductCode = @ProductCode
	  end

	  	  	  --------------------Tyre & Rim Plan D-----------------------
SELECT 'Tyre & Rim Plan 4' As [Section]
SET @ProductCode = N'TYRERIMVAPSPLAND'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Tyre & Rim Plan 4', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'TYRERIMVAPSPLAND', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 1)
			   end
else
      begin
	       update dbo.Product Set [Name] = 'Tyre & Rim Plan 4', [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 1
		   Where ProductCode = @ProductCode
	  end

SELECT 'Tyre & Rim Plan 5' As [Section]
SET @ProductCode = N'TYRERIMVAPSPLANE'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Tyre & Rim Plan 5', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'TYRERIMVAPSPLANE', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 1)
			   end
else
      begin
	       update dbo.Product Set [Name] = 'Tyre & Rim Plan 5', [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 1
		   Where ProductCode = @ProductCode
	  end
	  	  	  	  --------------------Cosmetic Plan Bronze-----------------------
SELECT 'Cosmetic Plan Bronze' As [Section]
SET @ProductCode = N'COSMETICPLANBRONZE'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Cosmetic Plan Bronze', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'COSMETICPLANBRONZE', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 1)
			   end
else
      begin
	       update dbo.Product Set [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 1
		   Where ProductCode = @ProductCode
	  end

	  	  	  	  	  --------------------Cosmetic Plan Gold-----------------------
SELECT 'Cosmetic Plan Gold' As [Section]
SET @ProductCode = N'COSMETICPLANGOLD'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Cosmetic Plan Gold', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'COSMETICPLANGOLD', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 1)
			   end
else
      begin
	       update dbo.Product Set [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 1
		   Where ProductCode = @ProductCode
	  end

	  --------------------Cosmetic Plan Titanium-----------------------
SELECT 'Cosmetic Plan Titanium' As [Section]
SET @ProductCode = N'COSMETICPLANTITANIUM'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Cosmetic Plan Titanium', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'COSMETICPLANTITANIUM', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 1)
			   end
else
      begin
	       update dbo.Product Set [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 1
		   Where ProductCode = @ProductCode
	  end

	  --------------------Cosmetic Plan Platinum-----------------------
SELECT 'Cosmetic Plan Platinum' As [Section]
SET @ProductCode = N'COSMETICPLANPLATINUM'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Cosmetic Plan Platinum', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'COSMETICPLANPLATINUM', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 1)
			   end
else
      begin
	       update dbo.Product Set [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 1
		   Where ProductCode = @ProductCode
	  end