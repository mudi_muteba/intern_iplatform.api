
if exists (select * from sys.objects where object_id = object_id(N'Reports_PolicyBinding_QuestionAnswer') and type in (N'P', N'PC'))
begin
	drop procedure Reports_PolicyBinding_QuestionAnswer
end
go

create procedure Reports_PolicyBinding_QuestionAnswer
(
	@QuoteId int
)
as

SELECT pbqd.Name as 'Question',
-- Case DropDown (3) / Checkbox (1)
	CASE WHEN (pbq.QuestionTypeId = 1 or pbq.QuestionTypeId = 3) 
	THEN (SELECT Answer FROM PolicyBindingQuestionAnswer WHERE PolicyBindingQuestionId = pbqd.PolicyBindingQuestionId and Id = pba.Answer and IsDeleted <> 1) 
	ELSE pba.Answer 
	END AS 'Answer'
FROM PolicyBindingAnswer AS pba
	join PolicyBindingQuestionDefinition AS pbqd ON pbqd.Id = pba.PolicyBindingQuestionDefinitionId 
	join PolicyBindingQuestion AS pbq ON pbq.Id = pbqd.PolicyBindingQuestionId
	join md.QuestionType AS qt ON qt.Id = pbq.QuestionTypeId
WHERE pba.QuoteId = @QuoteId and pba.IsDeleted <> 1 and pbqd.IsDeleted <> 1 and pbq.IsDeleted <> 1 and qt.Id <> 10



