﻿
DECLARE @productId INT, @CoverDefinitionID INT

SET @productId = (Select TOP 1 ID from Product where ProductCode = 'MW')  
SET @CoverDefinitionID = (SELECT  TOP 1 ID FROM CoverDefinition where ProductID = @productId and CoverId = 218) --MOTOR
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Included', 'Yes', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Use of vehicle', 'As per selected use', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Comprehensive Cover', 'Yes', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'New for Old', 'Not included', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Windscreen', 'Comprehensive only', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Third Party Fire and theft Cover', 'If requested', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Third Party Cover', 'If requested', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Riot and Strike outside RSA', 'No', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Tracking device to be covered', 'No', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Towing and Storage', 'Yes', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Sound System [not factory fitted]', 'If requested', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Hail Damage', 'Yes', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Canopy of the pick-up', 'If requested', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Car Hire', 'If requested', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Emergency Hotel Expenses', 'Not Included', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Credit Shortfall ', 'If requested', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Medical Costs', 'If requested', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Third Party Liability', 'R5,000,000', 1,0,0)
Insert into ProductBenefit (CoverDefinitionId, Name, Value, ShowToClient, VisibleIndex, IsDeleted) VALUES (  @CoverDefinitionID, 'Third Party fire and explosion Liability', 'No', 1,0,0)

