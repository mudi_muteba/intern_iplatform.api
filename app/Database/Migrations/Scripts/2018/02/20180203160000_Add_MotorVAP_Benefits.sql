﻿declare @ProductCode VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT, @CoverID INT

-------------------Service Plan-----------------------

SELECT 'The Select Service Plan' As [Section]
SET @ProductCode = N'MOTORSERVICE'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 378			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN

 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Servicing at the manufacturer', N'If premier extension included', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Spark Plugs', N'Included', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Engine Oil', N'Included', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Air Filter', N'Included', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Oil Filter', N'Included', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Sump Plug', N'Included', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Petrol / Diesel Filter', N'Included', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Pollen Filter', N'Included', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Brake Fluid Top Up', N'Included', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Engine Cleaner', N'Included', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Consumables', N'Included', 1, 10, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Labour', N'Included', 1, 11, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cam Belt', N'If cam belt extension included', 1, 12, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Brake Pads and Brake Discs', N'If extra extension included', 1, 13, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Brake Pad Wear Sensor', N'If extra extension included', 1, 14, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Aircon Re-Gas', N'If extra extension included', 1, 15, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Vehicle Fluids', N'If extra extension included', 1, 16, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Wiper Blades', N'If extra extension included', 1, 17, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Fuses', N'If extra extension included', 1, 18, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diagnostics', N'If extra extension included', 1, 19, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Car Wash', N'If extra extension included', 1, 20, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Shock Absorbers', N'If extra extension included', 1, 21, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Warranty Extender-----------------------

SELECT 'Warranty Extender' As [Section]
SET @ProductCode = N'MOTORWAR'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 375			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN

 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Servicing at the manufacturer', N'If premier extension included', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Spark Plugs', N'Included', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Engine Oil', N'Included', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Air Filter', N'Included', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Oil Filter', N'Included', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Sump Plug', N'Included', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Petrol / Diesel Filter', N'Included', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Pollen Filter', N'Included', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Brake Fluid Top Up', N'Included', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Engine Cleaner', N'Included', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Consumables', N'Included', 1, 10, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Labour', N'Included', 1, 11, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cam Belt', N'If cam belt extension included', 1, 12, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Brake Pads and Brake Discs', N'If extra extension included', 1, 13, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Brake Pad Wear Sensor', N'If extra extension included', 1, 14, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Aircon Re-Gas', N'If extra extension included', 1, 15, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Vehicle Fluids', N'If extra extension included', 1, 16, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Wiper Blades', N'If extra extension included', 1, 17, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Fuses', N'If extra extension included', 1, 18, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diagnostics', N'If extra extension included', 1, 19, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Car Wash', N'If extra extension included', 1, 20, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Shock Absorbers', N'If extra extension included', 1, 21, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Warranty Plan 1-----------------------

SELECT 'Warranty Plan 1' As [Section]
SET @ProductCode = N'MOTORWAR1'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 375			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN

 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Engine', N'R 75 000', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Gearbox', N'R 30 000', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential', N'R 30 000', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential Lock', N'R 25 000', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transfer Box', N'R 30 000', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Turbo Assembly', N'R 20 000', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Management System', N'R 15 000', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electronic Ignition', N'R 10 000', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Air-Conditioner', N'R 10 000', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cooling System', N'R 10 000', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Suspension', N'R10 000', 1, 10, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Wheel Bearings', N'R 10 000', 1, 11, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Braking System', N'R 10 000', 1, 12, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Fuel System', N'R 10 000', 1, 13, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Components', N'R 10 000', 1, 14, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Viscous and Electric Fans', N'R 5 000', 1, 15, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Clutch', N'R 10 000', 1, 16, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Prop Shaft (Drive Shafts)', N'R 10 000', 1, 17, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'CV Joints', N'R 10 000', 1, 18, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Steering Mechanism', N'R 10 000', 1, 19, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Sensors', N'R 10 000', 1, 20, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cylinder Head Gaskets', N'R 10 000', 1, 21, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Drive Pulleys', N'R 10 000', 1, 22, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Motors', N'R 5 000', 1, 23, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Mirrors', N'R 5 000', 1, 24, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Central Locking', N'R 5 000', 1, 25, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'GPS Navigation System', N'R 5 000', 1, 26, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Entertainment System', N'R 5 000', 1, 27, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Phone System', N'R 5 000', 1, 28, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transponder Key', N'R 3 000', 1, 29, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Alarms and Immobiliser', N'R 3 000', 1, 30, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Casings', N'R 3 000', 1, 31, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Winch', N'R 5 000', 1, 32, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Free Wheel Hubs', N'R 5 000', 1, 33, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Emission Control', N'R 3 000', 1, 34, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Strip and Quote', N'R 2 000', 1, 35, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Oils and Consumables Overheating', N'R 500', 1, 36, 0)
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Overfueling', N'R 10 000', 1, 37, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cambelt Failure', N'R 10 000', 1, 38, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Warranty Plan 2-----------------------

SELECT 'Warranty Plan 2' As [Section]
SET @ProductCode = N'MOTORWAR2'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 375			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN
 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Engine', N'R 50 000', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Gearbox', N'R 25 000', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential', N'R 25 000', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential Lock', N'R 20 000', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transfer Box', N'R 25 000', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Turbo Assembly', N'R 15 000', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Management System', N'R 12 500', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electronic Ignition', N'R 7 500', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Air-Conditioner', N'R 7 500', 1, 10, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cooling System', N'R 7 500', 1, 11, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Suspension', N'R 7 500', 1, 12, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Wheel Bearings', N'R 7 500', 1, 13, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Braking System', N'R 7 500', 1, 14, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Fuel System', N'R 7 500', 1, 15, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Components', N'R 7 500', 1, 16, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Viscous and Electric Fans', N'R 3 000', 1, 17, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Clutch', N'R 7 500', 1, 18, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Prop Shaft (Drive Shafts)', N'R  7 500', 1, 19, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'CV Joints', N'R 7 500', 1, 20, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Steering Mechanism', N'R 7 500', 1, 21, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Sensors', N'R 7 500', 1, 22, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cylinder Head Gaskets', N'R 7 500', 1, 23, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Drive Pulleys', N'R 7 500', 1, 24, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Motors', N'R 3 000', 1, 25, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Mirrors', N'R 3 000', 1, 26, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Central Locking', N'R 3 000', 1, 27, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'GPS Navigation System', N'R 3 000', 1, 28, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Entertainment System', N'R 3 000', 1, 29, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Phone System', N'R 3 000', 1, 30, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transponder Key', N'R 2 000', 1, 31, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Alarms and Immobiliser', N'R 2 000', 1, 32, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Casings', N'R 2 000', 1, 33, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Winch', N'R 2 000', 1, 34, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Free Wheel Hubs', N'R 2 000', 1, 35, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Emission Control', N'R 2 000', 1, 36, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Strip and Quote', N'R 1 500', 1, 37, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Oils and Consumables Overheating', N'R 500', 1, 38, 0)
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Overfueling', N'R 7 500', 1, 39, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cambelt Failure', N'R 7 500', 1, 40, 0) 
 
 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Warranty Plan 3-----------------------

SELECT 'Warranty Plan 3' As [Section]
SET @ProductCode = N'MOTORWAR3'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 375			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN

 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Engine', N'R 40 000', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Gearbox', N'R 20 000', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential', N'R 20 000', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential Lock', N'R 15 000', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transfer Box', N'R 20 000', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Turbo Assembly', N'R 10 000', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Management System', N'R 10 000', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electronic Ignition', N'R 5 000', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Air-Conditioner', N'R 5 000', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cooling System', N'R 5 000', 1, 10, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Suspension', N'R 5 000', 1, 11, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Wheel Bearings', N'R 5 000', 1, 12, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Braking System', N'R 5 000', 1, 13, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Fuel System', N'R 5 000', 1, 14, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Components', N'R 5 000', 1, 15, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Viscous and Electric Fans', N'R 2 500', 1, 16, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Clutch', N'R 5 000', 1, 17, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Prop Shaft (Drive Shafts)', N'R 5 000', 1, 18, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'CV Joints', N'R 5 000', 1, 19, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Steering Mechanism', N'R 5 000', 1, 20, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Sensors', N'R 5 000', 1, 21, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cylinder Head Gaskets', N'R 5 000', 1, 22, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Drive Pulleys', N'R 5 000', 1, 23, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Motors', N'R 2 500', 1, 24, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Mirrors', N'R 2 500', 1, 25, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Central Locking', N'R 2 500', 1, 26, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'GPS Navigation System', N'R 2 500', 1, 27, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Entertainment System', N'R 2 500', 1, 28, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Phone System', N'R 2 500', 1, 29, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transponder Key', N'R 1 500', 1, 30, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Alarms and Immobiliser', N'R 1 500', 1, 31, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Casings', N'R 1 500', 1, 32, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Winch', N'R 1 500', 1, 33, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Free Wheel Hubs', N'R 1 500', 1, 34, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Emission Control', N'R 1 500', 1, 35, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Strip and Quote', N'R 1 000', 1, 36, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Oils and Consumables Overheating', N'R 500', 1, 37, 0)
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Overfueling', N'R 5 000', 1, 38, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cambelt Failure', N'R 5 000', 1, 39, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Warranty Plan 4-----------------------

SELECT 'Warranty Plan 4' As [Section]
SET @ProductCode = N'MOTORWAR4'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 375			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Engine', N'R 30 000', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Gearbox', N'R 15 000', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential', N'R 15 000', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential Lock', N'R 10 000', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transfer Box', N'R 15 000', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Turbo Assembly', N'R 8 000', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Management System', N'R 8 000', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electronic Ignition', N'R 4 000', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Air-Conditioner', N'R 4 000', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cooling System', N'R 4 000', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Suspension', N'R 4 000', 1, 10, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Wheel Bearings', N'R 4 000', 1, 11, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Braking System', N'R 4 000', 1, 12, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Fuel System', N'R 4 000', 1, 13, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Components', N'R 4 000', 1, 14, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Viscous and Electric Fans', N'R 2 000', 1, 15, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Clutch', N'R 4 000', 1, 16, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Prop Shaft (Drive Shafts)', N'R 4 000', 1, 17, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'CV Joints', N'R 4 000', 1, 18, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Steering Mechanism', N'R 4 000', 1, 19, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Sensors', N'R 4 000', 1, 20, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cylinder Head Gaskets', N'R 4 000', 1, 21, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Drive Pulleys', N'R 4 000', 1, 22, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Motors', N'R 2 000', 1, 23, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Mirrors', N'R 2 000', 1, 24, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Central Locking', N'R 2 000', 1, 25, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'GPS Navigation System', N'R 2 000', 1, 26, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Entertainment System', N'R 2 000', 1, 27, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Phone System', N'R 2 000', 1, 28, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transponder Key', N'R 1 000', 1, 29, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Alarms and Immobiliser', N'R 1 000', 1, 30, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Casings', N'R 1 000', 1, 31, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Winch', N'R 1 000', 1, 32, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Free Wheel Hubs', N'R 1 000', 1, 33, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Emission Control', N'R 1 000', 1, 34, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Strip and Quote', N'Nil', 1, 35, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Oils and Consumables Overheating', N'R 400 ', 1, 36, 0)
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Overfueling', N'R 4 000', 1, 37, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cambelt Failure', N'R 4 000', 1, 38, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Warranty Plan 5-----------------------

SELECT 'Warranty Plan 5' As [Section]
SET @ProductCode = N'MOTORWAR5'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 375			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN

 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Engine', N'R 20 000', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Gearbox', N'R 10 000', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential', N'R10 000', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential Lock', N'R 7 500', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transfer Box', N'R 10 000', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Turbo Assembly', N'R 6 000', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Management System', N'R 6 000', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electronic Ignition', N'R 3 000', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Air-Conditioner', N'R 3 000', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cooling System', N'R 3 000', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Suspension', N'R 3 000', 1, 10, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Wheel Bearings', N'R 3 000', 1, 11, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Braking System', N'R 3 000', 1, 12, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Fuel System', N'R 3 000', 1, 13, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Components', N'R 3 000', 1, 14, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Viscous and Electric Fans', N'R 1 500', 1, 15, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Clutch', N'R 3 000', 1, 16, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Prop Shaft (Drive Shafts)', N'R 3 000', 1, 17, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'CV Joints', N'R 3 000', 1, 18, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Steering Mechanism', N'R 3 000', 1, 19, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Sensors', N'R 3 000', 1, 20, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cylinder Head Gaskets', N'R 3 000', 1, 21, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Drive Pulleys', N'R 3 000', 1, 22, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Motors', N'R 1 500', 1, 23, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Mirrors', N'R 1 500', 1, 24, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Central Locking', N'R 1 500', 1, 25, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'GPS Navigation System', N'R 1 500', 1, 26, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Entertainment System', N'R 1 500', 1, 27, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Phone System', N'R 1 500', 1, 28, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transponder Key', N'R 750', 1, 29, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Alarms and Immobiliser', N'R 750', 1, 30, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Casings', N'R 750', 1, 31, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Winch', N'R 750', 1, 32, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Free Wheel Hubs', N'R 750', 1, 33, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Emission Control', N'Nil', 1, 34, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Strip and Quote', N'Nil', 1, 35, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Oils and Consumables Overheating', N'R 300', 1, 36, 0)
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Overfueling', N'R 3 000', 1, 37, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cambelt Failure', N'R 3 000', 1, 38, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Warranty Plan 6-----------------------

SELECT 'Warranty Plan 6' As [Section]
SET @ProductCode = N'MOTORWAR6'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 375			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN

 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Engine', N'R 15 000', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Gearbox', N'R 7 500', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential', N'R 7 500', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential Lock', N'R 5 000', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transfer Box', N'R 7 500', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Turbo Assembly', N'R 4 000', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Management System', N'R 4 000', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electronic Ignition', N'R 2 000', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Air-Conditioner', N'R 2 000', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cooling System', N'R 2 000', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Suspension', N'R 2 000', 1, 10, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Wheel Bearings', N'R 2 000', 1, 11, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Braking System', N'R 2 000', 1, 12, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Fuel System', N'R 2 000', 1, 13, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Components', N'R 2 000', 1, 14, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Viscous and Electric Fans', N'R 1 000', 1, 15, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Clutch', N'R 2 000', 1, 16, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Prop Shaft (Drive Shafts)', N'R 2 000', 1, 17, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'CV Joints', N'R 2 000', 1, 18, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Steering Mechanism', N'R 2 000', 1, 19, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Sensors', N'R 2 000', 1, 20, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cylinder Head Gaskets', N'R 2 000', 1, 21, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Drive Pulleys', N'R 2 000', 1, 22, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Motors', N'R 1 000', 1, 23, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Mirrors', N'R 1 000', 1, 24, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Central Locking', N'R 1 000', 1, 25, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'GPS Navigation System', N'R 1 000', 1, 26, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Entertainment System', N'R 1 000', 1, 27, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Phone System', N'R 1 000', 1, 28, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transponder Key', N'Nil', 1, 29, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Alarms and Immobiliser', N'Nil', 1, 30, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Casings', N'Nil', 1, 31, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Winch', N'Nil', 1, 32, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Free Wheel Hubs', N'Nil', 1, 33, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Emission Control', N'Nil', 1, 34, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Strip and Quote', N'Nil', 1, 35, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Oils and Consumables Overheating', N'R 250', 1, 36, 0)
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Overfueling', N'R 2 000', 1, 37, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cambelt Failure', N'R 2 000', 1, 38, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Warranty Plan 7-----------------------

SELECT 'Warranty Plan 7' As [Section]
SET @ProductCode = N'MOTORWAR7'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 375			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN

  INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Engine', N'R 10 000', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Gearbox', N'R 5 000', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential', N'R 5 000', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Diﬀerential Lock', N'R 2 500', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transfer Box', N'R 5 000', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Turbo Assembly', N'R 2 000', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Management System', N'R 2 000', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electronic Ignition', N'R 2 000', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Air-Conditioner', N'R 1 000', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cooling System', N'R 1 000', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Suspension', N'R 1 000', 1, 10, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Wheel Bearings', N'R 1 000', 1, 11, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Braking System', N'R 1 000', 1, 12, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Fuel System', N'R 1 000', 1, 13, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Components', N'R 1 000', 1, 14, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Viscous and Electric Fans', N'R 1 000', 1, 15, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Clutch', N'Nil', 1, 16, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Prop Shaft (Drive Shafts)', N'Nil', 1, 17, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'CV Joints', N'Nil', 1, 18, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Steering Mechanism', N'Nil', 1, 19, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Sensors', N'Nil', 1, 20, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cylinder Head Gaskets', N'Nil', 1, 21, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Drive Pulleys', N'Nil', 1, 22, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Motors', N'Nil', 1, 23, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electric Mirrors', N'Nil', 1, 24, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Central Locking', N'Nil', 1, 25, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'GPS Navigation System', N'Nil', 1, 26, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Entertainment System', N'Nil', 1, 27, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Phone System', N'Nil', 1, 28, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Transponder Key', N'Nil', 1, 29, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Alarms and Immobiliser', N'Nil', 1, 30, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Casings', N'Nil', 1, 31, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Electrical Winch', N'Nil', 1, 32, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Free Wheel Hubs', N'Nil', 1, 33, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Emission Control', N'Nil', 1, 34, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Strip and Quote', N'Nil', 1, 35, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Oils and Consumables Overheating', N'R 200', 1, 36, 0)
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Overfueling', N'Nil', 1, 37, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Cambelt Failure', N'Nil', 1, 38, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END


-------------------Excess Eraser-----------------------

SELECT 'Excess Eraser' As [Section]
SET @ProductCode = N'EXCESSERASER'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 385			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN

 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Excess2500', N'Included if selected', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Excess3500', N'Included if selected', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Excess5000', N'Included if selected', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Excess7500', N'Included if selected', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Excess10000', N'Included if selected', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Excess20000', N'Included if selected', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Excess30000', N'Included if selected', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Optional Sections', N'Included if selected', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Bike Accessories', N'Included if selected', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Track Day Use', N'Included if selected', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Contents & Building', N'Included if selected', 1, 10, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Tyre Rim Plan 1-----------------------

SELECT 'Tyre Rim Plan 1' As [Section]
SET @ProductCode = N'TYRERIMVAPSPLANA'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 371			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN

 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Tyre Cover Limit', N'15000', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Runﬂat Excess', N'10% of Claim', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Tyre Betterment', N'As per betterment table', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Claim Limit', N'2 Claims per 12 payments received', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Cover Limit', N'R10 000', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Repair Excess', N'R750', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Replacement Excess', N'10% of Claim', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Claim Limit', N'2 Claims per 12 payments received', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Plugs Per Year', N'Unlimited', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Wheel Balancing & Tyre Rotation', N'180 days', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Nitrogen', N'180 days', 1, 10, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Tyre Rim Plan 2-----------------------

SELECT 'Tyre Rim Plan 2' As [Section]
SET @ProductCode = N'TYRERIMVAPSPLANB'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 371			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN

 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Tyre Cover Limit', N'10000', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Runﬂat Excess', N'10% of Claim', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Tyre Betterment', N'As per betterment table', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Claim Limit', N'2 Claims per 12 payments received', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Cover Limit', N'5000', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Repair Excess', N'500', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Replacement Excess', N'10% of Claim', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Claim Limit', N'2 Claims per 12 payments received', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Plugs Per Year', N'Unlimited', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Wheel Balancing & Tyre Rotation', N'270 days', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Nitrogen', N'270 days', 1, 10, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Tyre Rim Plan 3-----------------------

SELECT 'Tyre Rim Plan 3' As [Section]
SET @ProductCode = N'TYRERIMVAPSPLANC'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 371			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN

 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Tyre Cover Limit', N'8000', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Runﬂat Excess', N'10% of Claim', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Tyre Betterment', N'As per betterment table', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Claim Limit', N'2 Claims per 12 payments received', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Cover Limit', N'4000', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Repair Excess', N'400', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Replacement Excess', N'10% of Claim', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Claim Limit', N'2 Claims per 12 payments received', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Plugs Per Year', N'3', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Wheel Balancing & Tyre Rotation', N'365 days', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Nitrogen', N'Not Covered', 1, 10, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Tyre Rim Plan 4-----------------------

SELECT 'Tyre Rim Plan 4' As [Section]
SET @ProductCode = N'TYRERIMVAPSPLAND'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 371			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN

 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Tyre Cover Limit', N'5000', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Runﬂat Excess', N'10% of Claim', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Tyre Betterment', N'As per betterment table', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Claim Limit', N'2 Claims per 12 payments received', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Cover Limit', N'2500', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Repair Excess', N'250', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Replacement Excess', N'10% of Claim', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Claim Limit', N'2 Claims per 12 payments received', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Plugs Per Year', N'2', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Wheel Balancing & Tyre Rotation', N'Not Covered', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Nitrogen', N'Not Covered', 1, 10, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END

-------------------Tyre Rim Plan 5-----------------------

SELECT 'Tyre Rim Plan 5' As [Section]
SET @ProductCode = N'TYRERIMVAPSPLANE'
SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
SET @CoverID = 371			
SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS Not NULL And @productid IS Not NULL)
BEGIN
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Tyre Cover Limit', N'3000', 1, 0, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Runﬂat Excess', N'10% of Claim', 1, 1, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Tyre Betterment', N'As per betterment table', 1, 2, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Claim Limit', N'2 Claims per 12 payments received', 1, 3, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Cover Limit', N'1500', 1, 4, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Repair Excess', N'150', 1, 5, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Replacement Excess', N'10% of Claim', 1, 6, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Rim Insurance: Claim Limit', N'2 Claims per 12 payments received', 1, 7, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Plugs Per Year', N'1', 1, 8, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Wheel Balancing & Tyre Rotation', N'Not Covered', 1, 9, 0) 
 INSERT [dbo].[ProductBenefit] ([CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex], [IsDeleted]) VALUES ( @coverDefId, N'Additional Benefits: Nitrogen', N'Not Covered', 1, 10, 0) 

 Select * From [dbo].[ProductBenefit] Where [CoverDefinitionId] = @coverDefId
 SELECT * FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID
END