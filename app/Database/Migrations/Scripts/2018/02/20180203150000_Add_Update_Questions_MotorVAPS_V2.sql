DECLARE @id INT
DECLARE @ProductCode VARCHAR(50)
DECLARE @productid INT
DECLARE @coverDefId INT
DECLARE @CoverDefinitionId INT, @CoverID INT , @CoverName VARCHAR(50)

declare @ParentCoverDefinitionId int
declare @ChildCoverDefinitionId int, @NewQuestionId INT, @DummyQuestionDefId INT
declare @ProductOwnerId int
declare @OrganizationCode VARCHAR(50)
declare @ParentID INT


Set @OrganizationCode = N'MOTORV'
SET @ProductOwnerId = ( SELECT TOP 1 PartyId FROM dbo.Organization where  Code= @OrganizationCode)

 --------------------Service Plan-----------------------

SELECT 'The Select Service Plan' As [Section]
SET @ProductCode = N'MOTORSERVICE'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'The Select Service Plan', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'MOTORSERVICE', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 1)
			   end
else
      begin
	       update dbo.Product Set [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId, [Name] = N'The Select Service Plan',
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 1
		   Where ProductCode = @ProductCode
	  end


SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 378			

SET @CoverName = 'The Select Service Plan'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'A service plan is there to make life a bit easier for  clients when a service is due. It''' + 's  not easy taking the massive lump sum out of the monthly budget to pay for a service for the month. Therefore in exchange for a small monthly payment, when It''' + 's time for the service, Motorvaps will do the big payment to the garage and all you do is drop off and pick up.')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId

	 Update [dbo].[CoverDefinition] Set [DisplayName] = @CoverName, CoverLevelWording = N'A service plan is there to make life a bit easier for  clients when a service is due. It''' + 's not easy taking the massive lump sum out of the monthly budget to pay for a service for the month. Therefore in exchange for a small monthly payment, when It''' + 's time for the service, Motorvaps will do the big payment to the garage and all you do is drop off and pick up.'
	 Where ID = @CoverDefinitionId
END

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 0, 0,
               N'Asset', N'' , N'', 5, 4, 0, 20)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Asset', [ParentId] = null, [VisibleIndex] = 0, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Asset', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 20, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 1, 1, 1, 1,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', 5, 4, 0, 20)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'MM Code', [ParentId] = null, [VisibleIndex] = 1, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 1, 
				  [ToolTip] = N'What is the Mead and McGrouther code for this vehicle?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 0, [QuestionGroupId] = 20, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'What is the year of manufacture for this vehicle?', N'' , N'^[0-9]+$', 5, 4, 0, 20)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Year Of Manufacture', [ParentId] = null, [VisibleIndex] = 1, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the year of manufacture for this vehicle?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 0, [QuestionGroupId] = 20, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 3, 1, 1, 0,
               N'What is the make of this vehicle?', N'' , N'', 5, 4, 0, 20)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Make', [ParentId] = null, [VisibleIndex] = 3, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the make of this vehicle?', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 20, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 4, 1, 1, 0,
               N'What is the model of this vehicle?', N'' , N'', 5, 4, 0, 20)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Model', [ParentId] = null, [VisibleIndex] = 4, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the model of this vehicle?', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 20, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 4, 1, 1, 0,
               N'What is the sum insured of the vehicle?', N'' , N'^[0-9\.]+$', 5, 4, 1, 20)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Sum Insured', [ParentId] = null, [VisibleIndex] = 4, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the sum insured of the vehicle?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 1, [QuestionGroupId] = 20, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1400 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Engine Capacity (cc)', @CoverDefinitionId, 1400, NULL, 1, 5, 1, 1, 0,
               N'Engine Capacity (cc)', N'' , N'', 5, 4, 0, 20)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Engine Capacity (cc)', [ParentId] = null, [VisibleIndex] = 5, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Engine Capacity (cc)', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 20, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=1400 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1401 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Body Type', @CoverDefinitionId, 1401, NULL, 1, 6, 1, 1, 0,
               N'Body Type', N'' , N'', NULL, NULL, 0, 20)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Body Type', [ParentId] = null, [VisibleIndex] = 6, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Body Type', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 20
				  Where QuestionId=1401 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1402 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Fuel Type', @CoverDefinitionId, 1402, NULL, 1, 7, 1, 1, 0,
               N'Fuel Type', N'' , N'', NULL, NULL, 0, 20)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Fuel Type', [ParentId] = null, [VisibleIndex] = 7, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Fuel Type', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 20
				  Where QuestionId=1402 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1574 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Turbo Or Super Charged', @CoverDefinitionId, 1574, NULL, 1, 8, 1, 1, 0,
               N'Turbo Or Super Charged', N'' , N'', NULL, NULL, 0, 20)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Turbo Or Super Charged', [ParentId] = null, [VisibleIndex] = 8, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Turbo Or Super Charged', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 20
				  Where QuestionId=1574 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1575 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Drive Type', @CoverDefinitionId, 1575, NULL, 1, 9, 1, 1, 0,
               N'Drive Type', N'' , N'', NULL, NULL, 0, 20)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Drive Type', [ParentId] = null, [VisibleIndex] = 9, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Drive Type', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 20
				  Where QuestionId=1575 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1576 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Under Factory Warranty', @CoverDefinitionId, 1576, NULL, 1, 10, 1, 1, 0,
               N'By answering Yes you are confirming that this vehicle is under original factory warranty OR within 1500km''s or 30 days of said warranty expiring', N'' , N'', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Under Factory Warranty', [ParentId] = null, [VisibleIndex] = 10, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'By answering Yes you are confirming that this vehicle is under original factory warranty OR within 1500km''s or 30 days of said warranty expirin', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1576 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1406 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Odometer (km)', @CoverDefinitionId, 1406, NULL, 1, 11, 1, 1, 0,
               N'Odometer', N'' , N'^[0-9\.]+$', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Odometer (km)', [ParentId] = null, [VisibleIndex] = 11, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Odometer', [RegexPattern] = N'^[0-9\.]+$',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1406 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1407 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Use Type', @CoverDefinitionId, 1407, NULL, 1, 12, 1, 1, 0,
               N'Private Use: The monthly limit for private use is 3000 km''s. Professional: The monthly limit for professional use is 3500 km''s. Business: There is no monthly limit for business use. Commercial: There is no monthly limit for commercial use.', N'' , N'', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Use Type', [ParentId] = null, [VisibleIndex] = 12, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Private Use: The monthly limit for private use is 3000 km''s. Professional: The monthly limit for professional use is 3500 km''s. Business: There is no monthly limit for business use. Commercial: There is no monthly limit for commercial use.', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1407 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1408 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Service Type', @CoverDefinitionId, 1408, NULL, 1, 13, 1, 1, 0,
               N'Service Type', N'' , N'', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Service Type', [ParentId] = null, [VisibleIndex] = 13, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Service Type', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1408 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1409 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Service Intervals (km)', @CoverDefinitionId, 1409, NULL, 1, 14, 1, 1, 0,
               N'Service Intervals (km)', N'' , N'', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Service Intervals (km)', [ParentId] = null, [VisibleIndex] = 14, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Service Intervals (km)', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1409 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1577 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Full Service History', @CoverDefinitionId, 1577, NULL, 1, 15, 1, 1, 0,
               N'A full service history is a service history on the vehicle which has had all service and maintenance work carried out as per the manufacturers specifications', N'' , N'', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Full Service History', [ParentId] = null, [VisibleIndex] = 15, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'A full service history is a service history on the vehicle which has had all service and maintenance work carried out as per the manufacturers specifications', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1577 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1411 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Last Serviced (km)', @CoverDefinitionId, 1411, NULL, 1, 16, 1, 1, 0,
               N'This is the odometer reading of your vehicle when it was last serviced', N'' , N'^[0-9\.]+$', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Last Serviced (km)', [ParentId] = null, [VisibleIndex] = 16, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'This is the odometer reading of your vehicle when it was last serviced', [RegexPattern] = N'^[0-9\.]+$',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1411 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1412 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Average Distance Per Year (km)', @CoverDefinitionId, 1412, NULL, 1, 17, 1, 1, 0,
               N'Average Distance Per Year (km)', N'' , N'^[0-9\.]+$', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Average Distance Per Year (km)', [ParentId] = null, [VisibleIndex] = 17, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Average Distance Per Year (km)', [RegexPattern] = N'^[0-9\.]+$',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1412 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1414 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Repayment Period', @CoverDefinitionId, 1414, NULL, 1, 18, 1, 1, 0,
               N'Repayment Period', N'' , N'^[0-9\.]+$', NULL, NULL, 0, 18)
			   end 
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Repayment Period', [ParentId] = null, [VisibleIndex] = 18, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Repayment Period', [RegexPattern] = N'^[0-9\.]+$',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1414 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1579 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Cambelt Service Extension', @CoverDefinitionId, 1579, NULL, 1, 19, 1, 1, 0,
               N'Cambelt Service Extension', N'' , N'', NULL, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Cambelt Service Extension', [ParentId] = null, [VisibleIndex] = 19, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Cambelt Service Extension', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=1579 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1580 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Premier Extension', @CoverDefinitionId, 1580, NULL, 1, 20, 1, 1, 0,
               N'Premier Extension', N'' , N'', NULL, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Premier Extension', [ParentId] = null, [VisibleIndex] = 20, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Premier Extension', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=1580 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1418 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Service Plan Extra', @CoverDefinitionId, 1418, NULL, 1, 21, 1, 1, 0,
               N'Service Plan Extra', N'' , N'', NULL, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Service Plan Extra', [ParentId] = null, [VisibleIndex] = 21, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Service Plan Extra', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=1418 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1582 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Admin Fees', @CoverDefinitionId, 1582, NULL, 1, 23, 1, 1, 0,
               N'Admin Fees', N'0' ,  N'^[0-9\.]+$', NULL, NULL, 1, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Admin Fees', [ParentId] = null, [VisibleIndex] = 23, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Admin Fees', [RegexPattern] = N'^[0-9\.]+$',
				  [Hide] = 1, [QuestionGroupId] = 4
				  Where QuestionId=1582 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1583 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Once Off Initiation Fee', @CoverDefinitionId, 1583, NULL, 1, 24, 1, 1, 0,
               N'Once Off Initiation Fee', N'0' ,  N'^[0-9\.]+$', NULL, NULL, 1, 4)
              end
else
			  begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Once Off Initiation Fee', [ParentId] = null, [VisibleIndex] = 24, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Once Off Initiation Fee', [RegexPattern] = N'^[0-9\.]+$',
				  [Hide] = 1, [QuestionGroupId] = 4
				  Where QuestionId=1583 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1588 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Tyre Protect', @CoverDefinitionId, 1588, NULL, 1, 25, 1, 1, 0,
               N'Tyre Protect', N'' ,  N'', NULL, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Tyre Protect', [ParentId] = null, [VisibleIndex] = 25, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Tyre Protect', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=1588 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1589 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Major Component Cover', @CoverDefinitionId, 1589, NULL, 1, 26, 1, 1, 0,
               N'Major Component Cover', N'' ,  N'', NULL, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Major Component Cover', [ParentId] = null, [VisibleIndex] = 26, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Major Component Cover', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=1589 and CoverDefinitionId = @CoverDefinitionId
              end

 if not exists(select * from QuestionDefinition where QuestionId=1590 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Limited Scratch & Dent', @CoverDefinitionId, 1590, NULL, 1, 27, 1, 1, 0,
               N'Limited Scratch & Dent', N'' ,  N'', NULL, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Limited Scratch & Dent', [ParentId] = null, [VisibleIndex] = 27, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Limited Scratch & Dent', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=1590 and CoverDefinitionId = @CoverDefinitionId
              end

SELECT 'Product Info' As [Info Type], * FROM dbo.Product WHERE id IN (@productid)
SELECT 'Cover Definition Info' As [Info Type], * FROM dbo.CoverDefinition WHERE ProductId IN (@productid)

--------------------Tyre & Rim-----------------------
SELECT 'Tyre & Rim' As [Section]
SET @ProductCode = N'TYRERIMVAPS'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Tyre & Rim', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'TYRERIMVAPS', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 0)
			   end
else
      begin
	       update dbo.Product Set [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 0
		   Where ProductCode = @ProductCode
	  end

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 371 			

SET @CoverName = 'Tyre & Rim'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId

	Update [dbo].[CoverDefinition] Set CoverLevelWording = N''
	Where ID = @CoverDefinitionId
END

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 0, 0,
               N'Asset', N'' , N'', 5, 4, 0, Null)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Asset', [ParentId] = null, [VisibleIndex] = 0, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Asset', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = Null, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 0, 1, 1, 1,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', 5, 4, 1, NULL)
			   end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'MM Code', [ParentId] = null, [VisibleIndex] = 0, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 1, 
				  [ToolTip] = N'What is the Mead and McGrouther code for this vehicle?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 1, [QuestionGroupId] = Null, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'Id like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', N'' , N'^[0-9]+$', 5, 4, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Year Of Manufacture', [ParentId] = null, [VisibleIndex] = 2, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Id like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 0, [QuestionGroupId] = Null, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 3, 1, 1, 0,
               N'{Title} {Name} what is the make and model of the vehicle?', N'' , N'', 5, 4, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Make', [ParentId] = null, [VisibleIndex] = 3, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'{Title} {Name} what is the make and model of the vehicle?', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = Null, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 4, 1, 1, 0,
               N'What is the model of this vehicle?', N'' , N'', 5, 4, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Model', [ParentId] = null, [VisibleIndex] = 4, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the model of this vehicle?', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = Null, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 4, 1, 1, 0,
               N'What is the sum insured of the vehicle?', N'' , N'^[0-9\.]+$', 5, 4, 1, 2)
			   end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Model', [ParentId] = null, [VisibleIndex] = 4, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the sum insured of the vehicle?', [RegexPattern] = N'^[0-9\.]+$',
				  [Hide] = 1, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Registration Number', @CoverDefinitionId, 97, NULL, 1, 4, 0, 0, 0,
               N'Can you please provide me with the license plate number?', N'' , N'^.{0,10}$', 5, 4, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Registration Number', [ParentId] = null, [VisibleIndex] = 4, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Can you please provide me with the license plate number?', [RegexPattern] = N'^.{0,10}$',
				  [Hide] = 0, [QuestionGroupId] = Null, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'VIN Number', @CoverDefinitionId, 1010, NULL, 1, 4, 0, 0, 0,
               N'VIN Number', N'' , N'', 5, 4, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'VIN Number', [ParentId] = null, [VisibleIndex] = 4, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'VIN Number', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = Null, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Engine Number', @CoverDefinitionId, 1013, NULL, 1, 4, 0, 0, 0,
               N'Engine Number', N'' , N'', 5, 4, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Engine Number', [ParentId] = null, [VisibleIndex] = 4, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Engine Number', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = Null, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId
              end
			  			
if not exists(select * from QuestionDefinition where QuestionId=1406 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Odometer', @CoverDefinitionId, 1406, NULL, 1, 10, 0, 0, 0,
               N'Odometer', N'' , N'^[0-9]+$', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Odometer', [ParentId] = null, [VisibleIndex] = 10, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Odometer', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=1406 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=1591 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Cover Limit', @CoverDefinitionId, 1591, NULL, 1, 11, 0, 0, 0,
               N'Cover Limit', N'' , N'', NULL, NULL, 1, Null)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Cover Limit', [ParentId] = null, [VisibleIndex] = 11, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Cover Limit', [RegexPattern] = N'',
				  [Hide] = 1, [QuestionGroupId] = Null
				  Where QuestionId=1591 and CoverDefinitionId = @CoverDefinitionId
              end

SELECT 'Product Info' As [Info Type], * FROM dbo.Product WHERE id IN (@productid)
SELECT 'Cover Definition Info' As [Info Type], * FROM dbo.CoverDefinition WHERE ProductId IN (@productid)
			  
--------------------Motor Warranty-----------------------
SELECT 'Motor Warranty' As [Section]
SET @ProductCode = N'MOTORWAR'


if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Motor Warranty', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'MOTORWAR', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 0)
			   end
else
      begin
	       update dbo.Product Set [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 0
		   Where ProductCode Like @ProductCode + '%'
	  end

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 375			

SET @CoverName = 'Motor Warranty'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'Mechanical and Electrical components in the Engine, Gearbox and Differential are costly to replace. Motorvaps make life easier by covering these components in the event of an unforeseen Mechanical or Electrical breakdown in exchange for a small monthly premium. Your cover does not expire and the warranty may be transferred upon private sale of the vehicle. A benefit of the warranty is that it allows you to cancel your benefits at any time with immediate effect as long as it is in written notice. There is no relation between your comprehensive insurance and the warranty; your claims do not affect your other insurance premium or policy. There is no excess when you claim on your warranty. All claims are settled directly with the repairer. All warranty plans include 24 hour roadside assistance.')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId

	 Update [dbo].[CoverDefinition] Set CoverLevelWording = N'Mechanical and Electrical components in the Engine, Gearbox and Differential are costly to replace. Motorvaps make life easier by covering these components in the event of an unforeseen Mechanical or Electrical breakdown in exchange for a small monthly premium. Your cover does not expire and the warranty may be transferred upon private sale of the vehicle. A benefit of the warranty is that it allows you to cancel your benefits at any time with immediate effect as long as it is in written notice. There is no relation between your comprehensive insurance and the warranty; your claims do not affect your other insurance premium or policy. There is no excess when you claim on your warranty. All claims are settled directly with the repairer. All warranty plans include 24 hour roadside assistance.'
	 Where ID = @CoverDefinitionId
END

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 0, 0,
               N'Asset', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Asset', [ParentId] = null, [VisibleIndex] = 0, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Asset', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId
              end

if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 1, 1, 1, 1,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'MM Code', [ParentId] = null, [VisibleIndex] = 1, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 1, 
				  [ToolTip] = N'What is the Mead and McGrouther code for this vehicle?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'What is the year of manufacture for this vehicle?', N'' , N'^[0-9]+$', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Year Of Manufacture', [ParentId] = null, [VisibleIndex] = 2, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the year of manufacture for this vehicle?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 3, 1, 1, 0,
               N'What is the make of this vehicle?', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Make', [ParentId] = null, [VisibleIndex] = 3, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the make of this vehicle?', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 4, 1, 1, 0,
               N'What is the model of this vehicle?', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Model', [ParentId] = null, [VisibleIndex] = 4, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the model of this vehicle?', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Retail Sum Insured', @CoverDefinitionId, 102, NULL, 1, 4, 1, 1, 0,
               N'What is the sum insured of the vehicle?', N'' , N'^[0-9\.]+$', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Retail Sum Insured', [ParentId] = null, [VisibleIndex] = 4, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the sum insured of the vehicle?', [RegexPattern] = N'^[0-9\.]+$',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1401 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Body Type', @CoverDefinitionId, 1401, NULL, 1, 5, 1, 1, 0,
               N'Body Type', N'' , N'', NULL, NULL, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Body Type', [ParentId] = null, [VisibleIndex] = 5, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Body Type', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2
				  Where QuestionId=1401 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1402 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Fuel Type', @CoverDefinitionId, 1402, NULL, 1, 6, 1, 1, 0,
               N'Fuel Type', N'' , N'', NULL, NULL, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Fuel Type', [ParentId] = null, [VisibleIndex] = 6, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Fuel Type', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2
				  Where QuestionId=1402 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1575 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Drive Type', @CoverDefinitionId, 1575, NULL, 1, 7, 1, 1, 0,
               N'Drive Type', N'' , N'', NULL, NULL, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Drive Type', [ParentId] = null, [VisibleIndex] = 7, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Drive Type', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2
				  Where QuestionId=1575 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1576 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Under Factory Warranty', @CoverDefinitionId, 1576, NULL, 1, 11, 1, 1, 0,
               N'By answering Yes you are confirming that this vehicle is under original factory warranty OR within 1500km''s or 30 days of said warranty expiring', N'' , N'', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Under Factory Warranty', [ParentId] = null, [VisibleIndex] = 11, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'By answering Yes you are confirming that this vehicle is under original factory warranty OR within 1500km''s or 30 days of said warranty expiring', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1576 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1584 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Previous Breakdown', @CoverDefinitionId, 1584, NULL, 1, 12, 1, 1, 0,
               N'By answering No you are confirming that this vehicle has not suffered a breakdown or needed mechanical repairs in the previous 6 months', N'' , N'', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Previous Breakdown', [ParentId] = null, [VisibleIndex] = 12, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'By answering No you are confirming that this vehicle has not suffered a breakdown or needed mechanical repairs in the previous 6 months', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1584 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1406 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Odometer (km)', @CoverDefinitionId, 1406, NULL, 1, 10, 1, 1, 0,
               N'Odometer', N'' , N'^[0-9\.]+$', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Odometer (km)', [ParentId] = null, [VisibleIndex] = 10, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Odometer', [RegexPattern] = N'^[0-9\.]+$',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1406 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1407 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Use Type', @CoverDefinitionId, 1407, NULL, 1, 13, 1, 1, 0,
               N'Private Use: The monthly limit for private use is 3000 km''s. Professional: The monthly limit for professional use is 3500 km''s. Business: There is no monthly limit for business use. Commercial: There is no monthly limit for commercial use.', N'' , N'', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Use Type', [ParentId] = null, [VisibleIndex] = 13, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Private Use: The monthly limit for private use is 3000 km''s. Professional: The monthly limit for professional use is 3500 km''s. Business: There is no monthly limit for business use. Commercial: There is no monthly limit for commercial use.', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1407 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1585 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Cover Type', @CoverDefinitionId, 1585, NULL, 1, 13, 1, 1, 0,
               N'Cover Type', N'Cover Type' , N'', NULL, NULL, 1, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Cover Type', [ParentId] = null, [VisibleIndex] = 13, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Cover Type', [RegexPattern] = N'',
				  [Hide] = 1, [QuestionGroupId] = 18
				  Where QuestionId=1585 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1586 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Better Car', @CoverDefinitionId, 1586, NULL, 1, 15, 1, 1, 0,
               N'Better Car', N'' , N'', NULL, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Better Car', [ParentId] = null, [VisibleIndex] = 15, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Better Car', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=1586 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1577 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Full Service History', @CoverDefinitionId, 1577, NULL, 1, 14, 1, 1, 0,
               N'A full service history is a service history on the vehicle which has had all service and maintenance work carried out as per the manufacturers specifications', N'' , N'', NULL, NULL, 0, 18)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Full Service History', [ParentId] = null, [VisibleIndex] = 14, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'A full service history is a service history on the vehicle which has had all service and maintenance work carried out as per the manufacturers specifications', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 18
				  Where QuestionId=1577 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1587 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Cover Boost', @CoverDefinitionId, 1587, NULL, 1, 16, 1, 1, 0,
               N'Cover Boost', N'' , N'', NULL, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Cover Boost', [ParentId] = null, [VisibleIndex] = 16, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Cover Boost', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=1587 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1588 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Tyre Protect', @CoverDefinitionId, 1588, NULL, 1, 17, 1, 1, 0,
               N'Tyre Protect', N'' ,  N'', NULL, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Tyre Protect', [ParentId] = null, [VisibleIndex] = 17, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Tyre Protect', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=1588 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1424 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Limited Service Plan', @CoverDefinitionId, 1424, NULL, 1, 18, 1, 1, 0,
               N'Limited Service Plan', N'' ,  N'', NULL, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Limited Service Plan', [ParentId] = null, [VisibleIndex] = 18, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Limited Service Plan', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=1424 and CoverDefinitionId = @CoverDefinitionId
             end

 if not exists(select * from QuestionDefinition where QuestionId=1590 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Limited Scratch & Dent', @CoverDefinitionId, 1590, NULL, 1, 19, 1, 1, 0,
               N'Limited Scratch & Dent', N'' ,  N'', NULL, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Limited Scratch & Dent', [ParentId] = null, [VisibleIndex] = 19, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Limited Scratch & Dent', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=1590 and CoverDefinitionId = @CoverDefinitionId
             end

SELECT 'Product Info' As [Info Type], * FROM dbo.Product WHERE id IN (@productid)
SELECT 'Cover Definition Info' As [Info Type], * FROM dbo.CoverDefinition WHERE ProductId IN (@productid)


--------------------Excess Eraser-----------------------
SELECT 'Excess Eraser' As [Section]
SET @ProductCode = N'EXCESSERASER'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Excess Eraser', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'EXCESSERASER', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 1)
			   end
else
      begin
	       update dbo.Product Set [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 1
		   Where ProductCode = @ProductCode
	  end

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 385	 			

SET @CoverName = 'Excess Eraser'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId

	Update [dbo].[CoverDefinition] Set CoverLevelWording = N''
	Where ID = @CoverDefinitionId
END

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 0, 0,
               N'Asset', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Asset', [ParentId] = null, [VisibleIndex] = 0, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Asset', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 0, 0, 0, 1,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', 5, 4, 1, 2)
			   end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'MM Code', [ParentId] = null, [VisibleIndex] = 0, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 1, 
				  [ToolTip] = N'What is the Mead and McGrouther code for this vehicle?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'Id like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', N'' , N'^[0-9]+$', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Year Of Manufacture', [ParentId] = null, [VisibleIndex] = 2, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Id like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 3, 1, 1, 0,
               N'{Title} {Name} what is the make and model of the vehicle?', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Make', [ParentId] = null, [VisibleIndex] = 3, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'{Title} {Name} what is the make and model of the vehicle?', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 4, 1, 1, 0,
               N'What is the model of this vehicle?', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Model', [ParentId] = null, [VisibleIndex] = 4, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the model of this vehicle?', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 4, 0, 0, 0,
               N'What is the sum insured of the vehicle?', N'' , N'^[0-9\.]+$', 5, 4, 1, 2)
			   end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Sum Insured', [ParentId] = null, [VisibleIndex] = 4, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the sum insured of the vehicle?', [RegexPattern] = N'^[0-9\.]+$',
				  [Hide] = 1, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Registration Number', @CoverDefinitionId, 97, NULL, 1, 5, 0, 0, 0,
               N'Can you please provide me with the license plate number?', N'' , N'^.{0,10}$', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Registration Number', [ParentId] = null, [VisibleIndex] = 5, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Can you please provide me with the license plate number?', [RegexPattern] = N'^.{0,10}$',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'VIN Number', @CoverDefinitionId, 1010, NULL, 1, 5, 0, 0, 0,
               N'VIN Number', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'VIN Number', [ParentId] = null, [VisibleIndex] = 5, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'VIN Number', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Engine Number', @CoverDefinitionId, 1013, NULL, 1, 5, 0, 0, 0,
               N'Engine Number', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Engine Number', [ParentId] = null, [VisibleIndex] = 5, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Engine Number', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1558 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Excess Eraser Cover Level', @CoverDefinitionId, 1558, NULL, 1, 5, 0, 0, 0,
               N'Excess Eraser Cover Level', N'' , N'', 0, NULL, 0, NULL)
              end	
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Excess Eraser Cover Level', [ParentId] = null, [VisibleIndex] = 5, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Excess Eraser Cover Level', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = Null
				  Where QuestionId=1558 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1561 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Track Day', @CoverDefinitionId, 1561, NULL, 1, 8, 0, 0, 0,
               N'Track Day', N'' , N'', 0, NULL, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Track Day', [ParentId] = null, [VisibleIndex] = 8, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Track Day', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = Null
				  Where QuestionId=1561 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1560 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Householder & Buildings Taken', @CoverDefinitionId, 1560, null, 1, 6, 0, 0, 0,
               N'Householder & Buildings Taken', N'' , N'', 0, NULL, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Householder & Buildings Taken', [ParentId] = null, [VisibleIndex] = 6, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Householder & Buildings Taken', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = Null
				  Where QuestionId=1560 and CoverDefinitionId = @CoverDefinitionId
             end

--FCM - This is to get parent id to link to
Set @ParentID = (select ID from QuestionDefinition where QuestionId=1560 and CoverDefinitionId = @CoverDefinitionId)

if not exists(select * from QuestionDefinition where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Householder & Buildings Address', @CoverDefinitionId, 88, @ParentID, 1, 7, 0, 0, 0,
               N'Householder & Buildings Address', N'' , N'', 0, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Householder & Buildings Address', [ParentId] = @ParentID, [VisibleIndex] = 7, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Householder & Buildings Address', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=88 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1562 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Motorcycle accessories', @CoverDefinitionId, 1562, NULL, 1, 9, 0, 0, 0,
               N'Motorcycle accessories', N'' , N'', 0, NULL, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Motorcycle accessories', [ParentId] = NULL, [VisibleIndex] = 9, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Motorcycle accessories', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = Null
				  Where QuestionId=1562 and CoverDefinitionId = @CoverDefinitionId
             end

SELECT 'Product Info' As [Info Type], * FROM dbo.Product WHERE id IN (@productid)
SELECT 'Cover Definition Info' As [Info Type], * FROM dbo.CoverDefinition WHERE ProductId IN (@productid)

--------------------Power Train Warranty-----------------------
SELECT 'Power Train Warranty' As [Section]
SET @ProductCode = N'POWERTRAINWAR'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Power Train Warranty', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'POWERTRAINWAR', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 1)
			   end
else
      begin
	       update dbo.Product Set [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 1
		   Where ProductCode = @ProductCode
	  end

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 387 			

SET @CoverName = 'Power Train Warranty'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'Mechanical and Electrical components in the Engine, Gearbox and Differential are costly to replace. Motorvaps make life easier by covering these components in the event of an unforeseen Mechanical or Electrical breakdown in exchange for a small monthly premium. Your cover does not expire and the warranty may be transferred upon private sale of the vehicle.A benefit of the warranty is that it allows you to cancel your benefits at any time with immediate effect as long as it is in written notice. There is no relation between your comprehensive insurance and the warranty; your claims do not affect your other insurance premium or policy. There is no excess when you claim on your warranty. All claims are settled directly with the repairer. All warranty plans include 24 hour roadside assistance.')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId

	Update [dbo].[CoverDefinition] Set CoverLevelWording = N'Mechanical and Electrical components in the Engine, Gearbox and Differential are costly to replace. Motorvaps make life easier by covering these components in the event of an unforeseen Mechanical or Electrical breakdown in exchange for a small monthly premium. Your cover does not expire and the warranty may be transferred upon private sale of the vehicle.A benefit of the warranty is that it allows you to cancel your benefits at any time with immediate effect as long as it is in written notice. There is no relation between your comprehensive insurance and the warranty; your claims do not affect your other insurance premium or policy. There is no excess when you claim on your warranty. All claims are settled directly with the repairer. All warranty plans include 24 hour roadside assistance.'
	Where ID = @CoverDefinitionId

END

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 0, 0,
               N'Asset', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Asset', [ParentId] = NULL, [VisibleIndex] = 0, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Asset', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 0, 1, 1, 1,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', 5, 4, 1, 2)
			   end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'MM Code', [ParentId] = NULL, [VisibleIndex] = 0, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 1, 
				  [ToolTip] = N'What is the Mead and McGrouther code for this vehicle?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 1, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'Id like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', N'' , N'^[0-9]+$', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Year Of Manufacture', [ParentId] = NULL, [VisibleIndex] = 2, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Id like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 3, 1, 1, 0,
               N'{Title} {Name} what is the make and model of the vehicle?', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Make', [ParentId] = NULL, [VisibleIndex] = 3, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'{Title} {Name} what is the make and model of the vehicle?', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 4, 1, 1, 0,
               N'What is the model of this vehicle?', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Model', [ParentId] = NULL, [VisibleIndex] = 4, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the model of this vehicle?', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 4, 1, 1, 0,
               N'What is the sum insured of the vehicle?', N'' , N'^[0-9\.]+$', NULL, NULL, 1, 2)
			   end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Sum Insured', [ParentId] = NULL, [VisibleIndex] = 4, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the sum insured of the vehicle?', [RegexPattern] = N'^[0-9\.]+$',
				  [Hide] = 1, [QuestionGroupId] = 2
				  Where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Registration Number', @CoverDefinitionId, 97, NULL, 1, 5, 0, 0, 0,
               N'Can you please provide me with the license plate number?', N'' , N'^.{0,10}$', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Registration Number', [ParentId] = NULL, [VisibleIndex] = 5, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Can you please provide me with the license plate number?', [RegexPattern] = N'^.{0,10}$',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'VIN Number', @CoverDefinitionId, 1010, NULL, 1, 5, 0, 0, 0,
               N'VIN Number', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'VIN Number', [ParentId] = NULL, [VisibleIndex] = 5, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'VIN Number', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Engine Number', @CoverDefinitionId, 1013, NULL, 1, 5, 0, 0, 0,
               N'Engine Number', N'' , N'', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Engine Number', [ParentId] = NULL, [VisibleIndex] = 5, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Engine Number', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1406 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Odometer', @CoverDefinitionId, 1406, NULL, 1, 5, 1, 1, 0,
               N'Odometer', N'' , N'^[0-9]+$', 5, 4, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Odometer', [ParentId] = NULL, [VisibleIndex] = 5, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Odometer', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 0, [QuestionGroupId] = 2, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=1406 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1572 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Vehicle Type', @CoverDefinitionId, 1572, NULL, 1, 10, 0, 0, 0,
               N'Vehicle Type', N'' , N'', 0, NULL, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Vehicle Type', [ParentId] = NULL, [VisibleIndex] = 10, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Vehicle Type', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 2
				  Where QuestionId=1572 and CoverDefinitionId = @CoverDefinitionId
             end

--FCM - This is to get parent id to link to
Set @ParentID = (select ID from QuestionDefinition where QuestionId=1572 and CoverDefinitionId = @CoverDefinitionId)

if not exists(select * from QuestionDefinition where QuestionId=1564 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Passenger Plan Type', @CoverDefinitionId, 1564, @ParentID, 1, 11, 0, 0, 0,
               N'Passenger Plan Type', N'' , N'', 0, NULL, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Passenger Plan Type', [ParentId] = @ParentID, [VisibleIndex] = 11, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Passenger Plan Type', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = Null
				  Where QuestionId=1564 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1565 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Utility Plan Type', @CoverDefinitionId, 1565, @ParentID, 1, 12, 0, 0, 0,
               N'Utility Plan Type', N'' , N'', 0, NULL, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Utility Plan Type', [ParentId] = @ParentID, [VisibleIndex] = 12, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Utility Plan Type', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = Null
				  Where QuestionId=1565 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1566 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Passenger Cover Plus', @CoverDefinitionId, 1566, @ParentID, 1, 13, 0, 0, 0,
               N'Passenger Cover Plus', N'' , N'', 0, NULL, 0, 4)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Passenger Cover Plus', [ParentId] = @ParentID, [VisibleIndex] = 13, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Passenger Cover Plus', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = 4
				  Where QuestionId=1566 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1567 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Passenger Scratch Plus', @CoverDefinitionId, 1567, @ParentID, 1, 14, 0, 0, 0,
               N'Passenger Scratch Plus', N'' , N'', 0, NULL, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Passenger Scratch Plus', [ParentId] = @ParentID, [VisibleIndex] = 14, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Passenger Scratch Plus', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = NULL
				  Where QuestionId=1567 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1568 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Passenger Tyre Plus', @CoverDefinitionId, 1568, @ParentID, 1, 15, 0, 0, 0,
               N'Passenger Tyre Plus', N'' , N'', 0, NULL, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Passenger Tyre Plus', [ParentId] = @ParentID, [VisibleIndex] = 15, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Passenger Tyre Plus', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = NULL
				  Where QuestionId=1568 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1569 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Utility Cover Plus', @CoverDefinitionId, 1569, @ParentID, 1, 16, 0, 0, 0,
               N'Utility Cover Plus', N'' , N'', 0, NULL, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Utility Cover Plus', [ParentId] = @ParentID, [VisibleIndex] = 16, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Utility Cover Plus', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = NULL
				  Where QuestionId=1569 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1570 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Utility Scratch Plus', @CoverDefinitionId, 1570, @ParentID, 1, 17, 0, 0, 0,
               N'Utility Scratch Plus', N'' , N'', 0, NULL, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Utility Scratch Plus', [ParentId] = @ParentID, [VisibleIndex] = 17, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Utility Scratch Plus', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = NULL
				  Where QuestionId=1570 and CoverDefinitionId = @CoverDefinitionId
             end


if not exists(select * from QuestionDefinition where QuestionId=1571 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Utility Tyre Plus', @CoverDefinitionId, 1571, @ParentID, 1, 18, 0, 0, 0,
               N'Utility Tyre Plus', N'' , N'', 0, NULL, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Utility Tyre Plus', [ParentId] = @ParentID, [VisibleIndex] = 18, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Utility Tyre Plus', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = NULL
				  Where QuestionId=1571 and CoverDefinitionId = @CoverDefinitionId
             end

SELECT 'Product Info' As [Info Type], * FROM dbo.Product WHERE id IN (@productid)
SELECT 'Cover Definition Info' As [Info Type], * FROM dbo.CoverDefinition WHERE ProductId IN (@productid)

--------------------Cosmetic Plan-----------------------
SELECT 'Cosmetic Plan' As [Section]
SET @ProductCode = N'COSMETICPLAN'

if not exists(SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)
              begin
              INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [IsDeleted], [ImageName], 
			  [QuoteExpiration], [ShowInReporting], [BenefitAcceptanceCheck], [HideSasriaLine], [HideCompareButton]) 
			  VALUES (0, N'Cosmetic Plan', @ProductOwnerId, @ProductOwnerId, 7, 123456, N'COSMETICPLAN', getDate(), 0, N'motorv.png', 30, 0, 1, 1, 0)
			   end
else
      begin
	       update dbo.Product Set [ImageName] = N'motorv.png', ProductProviderId = @ProductOwnerId,  ProductOwnerId = @ProductOwnerId,
		   BenefitAcceptanceCheck = 1, HideSasriaLine = 1, HideCompareButton = 0
		   Where ProductCode = @ProductCode
	  end

SET @productid = ( SELECT TOP 1 ID FROM dbo.Product where  ProductCode= @ProductCode)

SET @CoverID = 386 			

SET @CoverName = 'Cosmetic Plan'

SET @coverDefId = ( SELECT ID FROM  [dbo].[CoverDefinition]  WHERE ProductId = @productid AND CoverId = @CoverID)

IF (@coverDefId IS NULL)
BEGIN

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex], IsDeleted, [CoverLevelWording])
	VALUES (0, @CoverName, @productid, @CoverID, NULL, 1, 0, 0, N'')
	SET @CoverDefinitionId = SCOPE_IDENTITY()
END
ELSE
BEGIN
	SET @CoverDefinitionId = @coverDefId

	Update [dbo].[CoverDefinition] Set CoverLevelWording = N''
	Where ID = @CoverDefinitionId
END

if not exists(select * from QuestionDefinition where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Asset', @CoverDefinitionId, 141, NULL, 1, 0, 0, 0, 0,
               N'Asset', N'' , N'', 5, 4, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Asset', [ParentId] = Null, [VisibleIndex] = 0, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Asset', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = NULL, [GroupIndex] = 5, [QuestionDefinitionGroupTypeId] = 4
				  Where QuestionId=141 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'MM Code', @CoverDefinitionId, 94, NULL, 1, 0, 0, 0, 1,
               N'What is the Mead and McGrouther code for this vehicle?', N'' , N'^[0-9]+$', NULL, NULL, 1, NULL)
			   end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'MM Code', [ParentId] = Null, [VisibleIndex] = 0, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 1, 
				  [ToolTip] = N'What is the Mead and McGrouther code for this vehicle?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 1, [QuestionGroupId] = NULL
				  Where QuestionId=94 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Year Of Manufacture', @CoverDefinitionId, 99, NULL, 1, 2, 1, 1, 0,
               N'Id like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', N'' , N'^[0-9]+$', 5, 4, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Year Of Manufacture', [ParentId] = Null, [VisibleIndex] = 2, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'Id like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 0, [QuestionGroupId] = NULL
				  Where QuestionId=99 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Make', @CoverDefinitionId, 95, NULL, 1, 3, 1, 1, 0,
               N'{Title} {Name} what is the make and model of the vehicle?', N'' , N'', 5, 4, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Make', [ParentId] = Null, [VisibleIndex] = 3, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'{Title} {Name} what is the make and model of the vehicle?', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = NULL
				  Where QuestionId=95 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Model', @CoverDefinitionId, 96, NULL, 1, 4, 1, 1, 0,
               N'What is the model of this vehicle?', N'' , N'', 5, 4, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Model', [ParentId] = Null, [VisibleIndex] = 4, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the model of this vehicle?', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = NULL
				  Where QuestionId=96 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Sum Insured', @CoverDefinitionId, 102, NULL, 1, 4, 1, 1, 0,
               N'What is the sum insured of the vehicle?', N'' , N'^[0-9\.]+$', NULL, NULL, 1, 2)
			   end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Sum Insured', [ParentId] = Null, [VisibleIndex] = 4, [RequiredForQuote] = 1, [RatingFactor] = 1, [ReadOnly] = 0, 
				  [ToolTip] = N'What is the sum insured of the vehicle?', [RegexPattern] = N'^[0-9\.]+$',
				  [Hide] = 1, [QuestionGroupId] = 2
				  Where QuestionId=102 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Registration Number', @CoverDefinitionId, 97, NULL, 1, 5, 0, 0, 0,
               N'Can you please provide me with the license plate number?', N'' , N'^.{0,10}$', 5, 4, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Registration Number', [ParentId] = Null, [VisibleIndex] = 5, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Can you please provide me with the license plate number?', [RegexPattern] = N'^.{0,10}$',
				  [Hide] = 0, [QuestionGroupId] = Null
				  Where QuestionId=97 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'VIN Number', @CoverDefinitionId, 1010, NULL, 1, 5, 0, 0, 0,
               N'VIN Number', N'' , N'', 0, NULL, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'VIN Number', [ParentId] = Null, [VisibleIndex] = 5, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'VIN Number', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = NULL
				  Where QuestionId=1010 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Engine Number', @CoverDefinitionId, 1013, NULL, 1, 5, 0, 0, 0,
               N'Engine Number', N'' , N'', 0, NULL, 0, NULL)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Engine Number', [ParentId] = Null, [VisibleIndex] = 5, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Engine Number', [RegexPattern] = N'',
				  [Hide] = 0, [QuestionGroupId] = NULL
				  Where QuestionId=1013 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1406 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Odometer', @CoverDefinitionId, 1406, NULL, 1, 10, 0, 0, 0,
               N'Odometer', N'' , N'^[0-9]+$', 0, NULL, 0, 2)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Odometer', [ParentId] = Null, [VisibleIndex] = 10, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Odometer', [RegexPattern] = N'^[0-9]+$',
				  [Hide] = 0, [QuestionGroupId] = 2
				  Where QuestionId=1406 and CoverDefinitionId = @CoverDefinitionId
             end

if not exists(select * from QuestionDefinition where QuestionId=1592 and CoverDefinitionId = @CoverDefinitionId)
              begin
              INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip],
              [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId], [Hide], [QuestionGroupId]) VALUES (0, N'Cover Limit', @CoverDefinitionId, 1592, NULL, 1, 11, 0, 0, 0,
               N'Cover Limit', N'' , N'', NULL, NULL, 1, Null)
              end
else
              begin
                  Update [dbo].[QuestionDefinition] 
				  Set [MasterId] = 0,  [DisplayName] = N'Cover Limit', [ParentId] = Null, [VisibleIndex] = 11, [RequiredForQuote] = 0, [RatingFactor] = 0, [ReadOnly] = 0, 
				  [ToolTip] = N'Cover Limit', [RegexPattern] = N'',
				  [Hide] = 1, [QuestionGroupId] = NULL
				  Where QuestionId=1592 and CoverDefinitionId = @CoverDefinitionId
             end
			   
SELECT 'Product Info' As [Info Type], * FROM dbo.Product WHERE id IN (@productid)
SELECT 'Cover Definition Info' As [Info Type], * FROM dbo.CoverDefinition WHERE ProductId IN (@productid)		   

SELECT 'Organization' As [Section]
SELECT * From dbo.Organization Where PartyId In (@ProductOwnerId)



