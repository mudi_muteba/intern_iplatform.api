﻿-- Table
if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Note_Individual]') AND parent_object_id = OBJECT_ID('Note'))
alter table Note  drop constraint FK_Note_Individual


if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Note_Party]') AND parent_object_id = OBJECT_ID('Note'))
alter table Note  drop constraint FK_Note_Party

if exists (select * from dbo.sysobjects where id = object_id(N'Note') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table Note