﻿if not exists(select * from md.Question where Name = 'Cash Back')
	begin
		INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
		VALUES		('Cash Back', 1, 4)
	end

if not exists(select * from md.Question where Name = 'Retired')
	begin
		INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
		VALUES		('Retired', 1, 1)
	end

if not exists(select * from md.Question where Name = 'Guest House')
	begin
		INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
		VALUES	
					('Guest House', 1, 4)
	end


