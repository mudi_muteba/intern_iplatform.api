alter table [dbo].[Organization] add TemporaryFSP bit  DEFAULT 0 NULL
alter table [dbo].[Organization] add ProfessionalIndemnityInsurer nvarchar(50) NULL
alter table [dbo].[Organization] add PICoverPolicyNo nvarchar(50) NULL
alter table [dbo].[Organization] add TypeOfAgency nvarchar(50) NULL
alter table [dbo].[Organization] add FGPolicyNo nvarchar(50) NULL
alter table [dbo].[Organization] add FidelityGuaranteeInsurer nvarchar(50) NULL
alter table [dbo].[Organization] add OrganizationTypeId int NULL

	CREATE TABLE md.OrganizationType (
		Id INT IDENTITY NOT NULL,
		Name VARCHAR(50) NOT NULL UNIQUE,
		primary key (Id))

	INSERT INTO [md].OrganizationType
           ([name])
	VALUES
		('SoleProprietor'),
		('Partnership'),
		('CloseCorporation'),
		('PrivateCompany_Pty_Ltd'),
		('PublicCompany'),
		('BusinessTrust'),
		('NonProfitOrganisations')

		alter table dbo.Organization 
        add constraint FK_Organization_OrganizationType 
        foreign key (OrganizationTypeId) 
        references md.OrganizationType