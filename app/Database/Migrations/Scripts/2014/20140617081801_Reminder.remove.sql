﻿if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_LeadActivity_Reminder]') AND parent_object_id = OBJECT_ID('LeadActivity'))
	alter table LeadActivity  drop constraint FK_LeadActivity_Reminder

-- drop table
drop table Reminder