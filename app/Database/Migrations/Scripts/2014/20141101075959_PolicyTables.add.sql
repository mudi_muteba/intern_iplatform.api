﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if NOT exists (select * from dbo.sysobjects where id = object_id(N'PolicyHeader') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
CREATE TABLE [dbo].[PolicyHeader](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MasterPolicyId] [int] NULL,
	[LeadActivityID] [int] NULL,
	[PolicyId] [int] NULL,
	[QuoteNo] [nvarchar](255) NULL,
	[PolicyNo] [nvarchar](255) NULL,
	[PartyId] [int] NULL,
	[InsurerId] [int] NULL,
	[ProductId] [int] NULL,
	[DateEffective] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[DateRenewal] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
	[Frequency] [int] NULL,
	[Renewable] [bit] NULL,
	[PolicyStatus] [int] NULL,
	[NewBusiness] [bit] NULL,
	[VAP] [bit] NULL,
 CONSTRAINT [PK_Policy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End

if NOT exists (select * from dbo.sysobjects where id = object_id(N'PolicyItem') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
CREATE TABLE [dbo].[PolicyItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AssetNumber] [int] NULL,
	[PolicyHeaderId] [int] NULL,
	[ItemDescription] [nvarchar](255) NULL,
	[DateEffective] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_PolicyItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End

if NOT exists (select * from dbo.sysobjects where id = object_id(N'PolicyCoverage') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
CREATE TABLE [dbo].[PolicyCoverage](
	[Id] [int]  NOT NULL,
	[PolicyCoverageId] [int] NULL,
	[PolicyFinanceLinkId] [int] NULL,
	[AssetNumber] [int] NULL,
	[CoverageType] [int] NULL,
	[CoverageLimit] [decimal](18, 4) NULL,
	[ParentCoverage] [int] NULL,
	[DateEffective] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_PolicyCoverage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End

if NOT exists (select * from dbo.sysobjects where id = object_id(N'PolicyFinance') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
CREATE TABLE [dbo].[PolicyFinance](
	[Id] [int] NOT NULL,
	[PolicyFinanceId] [int] NULL,
	[PolicyCoverageLinkId] [int] NULL,
	[AssetNumber] [int] NULL,
	[FinanceTypeId] [int] NULL,
	[Amount] [decimal](18, 4) NULL,
	[TAX] [decimal](18, 4) NULL,
	[AmountProRata] [decimal](18, 4) NULL,
	[TAXProRata] [decimal](18, 4) NULL,
	[TAXTypeId] [int] NULL,
	[DateEffective] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_PolicyFinance] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End

if NOT exists (select * from dbo.sysobjects where id = object_id(N'PolicyAllRisk') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
CREATE TABLE [dbo].[PolicyAllRisk](
	[Id] [int] NOT NULL,
	[AllRisksId] [int] NULL,
	[AssetNumber] [int] NULL,
	[AddressId] [int] NULL,
	[TypeCode] [nvarchar](50) NULL,
	[ArticleClassCodeId] [int] NULL,
	[ArticleTypeCodeId] [int] NULL,
	[Description] [nvarchar](255) NULL,
	[SerialNo] [nvarchar](50) NULL,
	[StructureTypeCodeId] [int] NULL,
	[ConstructionTypeCodeId] [int] NULL,
	[RoofTypeCodeId] [int] NULL,
	[DateEffective] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_PolicyAllRisk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End

if NOT exists (select * from dbo.sysobjects where id = object_id(N'PolicyBuilding') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
CREATE TABLE [dbo].[PolicyBuilding](
	[Id] [int] NOT NULL,
	[BuildingsId] [int] NULL,
	[AssetNumber] [int] NULL,
	[AddressId] [int] NULL,
	[TypeCode] [nvarchar](50) NULL,
	[ArticleClassCodeId] [int] NULL,
	[ArticleTypeCodeId] [int] NULL,
	[Description] [nvarchar](255) NULL,
	[SerialNo] [nvarchar](50) NULL,
	[StructureTypeCodeId] [int] NULL,
	[ConstructionTypeCodeId] [int] NULL,
	[RoofTypeCodeId] [int] NULL,
	[DateEffective] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_PolicyBuilding] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End

if NOT exists (select * from dbo.sysobjects where id = object_id(N'PolicyContent') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
CREATE TABLE [dbo].[PolicyContent](
	[Id] [int]  NOT NULL,
	[ContentsId] [int] NULL,
	[AssetNumber] [int] NULL,
	[AddressId] [int] NULL,
	[TypeCode] [nvarchar](50) NULL,
	[ArticleClassCodeId] [int] NULL,
	[ArticleTypeCodeId] [int] NULL,
	[Description] [nvarchar](255) NULL,
	[SerialNo] [nvarchar](50) NULL,
	[StructureTypeCodeId] [int] NULL,
	[ConstructionTypeCodeId] [int] NULL,
	[RoofTypeCodeId] [int] NULL,
	[DateEffective] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_PolicyContent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End

if NOT exists (select * from dbo.sysobjects where id = object_id(N'PolicyPersonalVehicle') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
CREATE TABLE [dbo].[PolicyPersonalVehicle](
	[Id] [int]  NOT NULL,
	[PersonalVehicleId] [int] NULL,
	[AssetNumber] [int] NULL,
	[AddressId] [int] NULL,
	[VehicleSymbolCode] [nvarchar](50) NULL,
	[Make] [nvarchar](50) NULL,
	[Model] [nvarchar](50) NULL,
	[Year] [int] NULL,
	[CFG] [int] NULL,
	[ChassisNo] [nvarchar](50) NULL,
	[EngineNo] [nvarchar](50) NULL,
	[RegistrationNo] [nvarchar](20) NULL,
	[VinNo] [nvarchar](50) NULL,
	[VehicleClassCodeId] [int] NULL,
	[VehicleUseTypeCodeId] [int] NULL,
	[VehicleTypeCodeId] [int] NULL,
	[VehicleAlteredIndicator] [bit] NULL,
	[VehicleOptionsIncludedIndicator] [bit] NULL,
	[VehicleModificationsIncludedIndicator] [bit] NULL,
	[AntiTheftDeviceIndicator] [bit] NULL,
	[AntiLockBrakesIndicator] [bit] NULL,
	[ExistingUnrepairedDamageIndicator] [bit] NULL,
	[VehicleInspectionStatusCodeId] [int] NULL,
	[VehiclePerformanceCodeId] [int] NULL,
	[FourWheelDriveIndicator] [bit] NULL,
	[TractionControlIndicator] [bit] NULL,
	[SeenVehicleIndicator] [bit] NULL,
	[DualControlsIndicator] [bit] NULL,
	[CarpoolIndicator] [bit] NULL,
	[EmployeeOperatedIndicator] [bit] NULL,
	[IsRegisteredIndicator] [bit] NULL,
	[NonOwnedVehicleIndicator] [bit] NULL,
	[AgreedValueRequiredIndicator] [bit] NULL,
	[VehicleGaragingCodeId] [int] NULL,
	[DateEffective] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_PolicyPersonalVehicle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End

if NOT exists (select * from dbo.sysobjects where id = object_id(N'PolicyLiability') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
CREATE TABLE [dbo].[PolicyLiability](
	[Id] [int]  NOT NULL,
	[LiabilityId] [int] NULL,
	[AssetNumber] [int] NULL,
	[TypeCode] [nvarchar](50) NULL,
	[Description] [nvarchar](255) NULL,
	[DateEffective] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_PolicyLiability] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End

if NOT exists (select * from dbo.sysobjects where id = object_id(N'PolicyAccident') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
CREATE TABLE [dbo].[PolicyAccident](
	[Id] [int]  NOT NULL,
	[AccidentId] [int] NULL,
	[AssetNumber] [int] NULL,
	[TypeCode] [nvarchar](50) NULL,
	[Description] [nvarchar](255) NULL,
	[DateEffective] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_PolicyAccident] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End

if NOT exists (select * from dbo.sysobjects where id = object_id(N'PolicyWatercraft') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
CREATE TABLE [dbo].[PolicyWatercraft](
	[Id] [int]  NOT NULL,
	[WatercraftId] [int] NULL,
	[AssetNumber] [int] NULL,
	[TypeCode] [nvarchar](50) NULL,
	[Description] [nvarchar](255) NULL,
	[DateEffective] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_PolicyWatercraft] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
End

-- change from decimal 18,0 to decimal 18,4
Alter Table [Quote] Alter Column [Fees] [decimal](18, 4)
Alter Table [QuoteItem] Alter Column [SumInsured] [decimal](18, 4)
Alter Table [QuoteItem] Alter Column [Premium] [decimal](18, 4)
Alter Table [QuoteItem] Alter Column [Sasria] [decimal](18, 4)
Alter Table [QuoteItem] Alter Column [ExcessBasic] [decimal](18, 4)