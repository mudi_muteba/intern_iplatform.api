	create table md.RegistrationQuestion (
		Id INT IDENTITY NOT NULL,
		Name VARCHAR(50) NOT NULL UNIQUE,
		Question NVARCHAR(255) NOT NULL UNIQUE,
		primary key (Id)
	)
	
	INSERT INTO [md].[RegistrationQuestion]
           ([name],[Question])
     VALUES
           ('Question 1','Was any of the mentioned parties/persons sequestrate/liquidated?'),
           ('Question 2','Has any of the parties/persons been found guilty of a criminal offence?'),
		   ('Question 3','Are there any criminal matters/law suits pending against the party/person?'),
		   ('Question 4','Is there a civil judgment, which was granted against the party/person which has not been settled?'),
		   ('Question 5','Has any person ever been dismissed from employment?'),
		   ('Question 6','Do any of the persons mentioned above have an existing agency with CIMS'),
		   ('Question 7','Has any insurer ever cancelled an agency of any persons mentioned above?')


		   
	CREATE TABLE [dbo].[MapPartyToRegistrationQuestions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RegistrationQuestionId] [int] NULL,
	[PartyId] [int] NULL,
	[Answer] [bit] NULL,
	[Comment] [nvarchar](255) NULL,
 CONSTRAINT [PK__MapParty__3214EC077C810E48] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MapPartyToRegistrationQuestions]  WITH CHECK ADD  CONSTRAINT [FK9E8A8AB5DB2A8D2E] FOREIGN KEY([PartyId])
REFERENCES [dbo].[Party] ([Id])
GO
ALTER TABLE [dbo].[MapPartyToRegistrationQuestions] CHECK CONSTRAINT [FK9E8A8AB5DB2A8D2E]
GO
		   
		   
	    alter table MapPartyToRegistrationQuestions 
        add constraint FK_MapPartyToRegistrationQuestions_RegistrationQuestion 
        foreign key (RegistrationQuestionId) 
        references md.[RegistrationQuestion]