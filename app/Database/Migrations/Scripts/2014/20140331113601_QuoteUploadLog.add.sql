﻿
    create table QuoteUploadLog (
        Id INT IDENTITY NOT NULL,
       RequestId UNIQUEIDENTIFIER NOT NULL,
	   ProductId INT NOT NULL,
       InsurerReference NVARCHAR(200) NOT NULL default(''),
       Premium DECIMAL(19, 5) NOT NULL default(0),
       Fees DECIMAL(19, 5) NOT NULL default(0),
       Sasria DECIMAL(19, 5) NOT NULL default(0),
       DateCreated DATETIME NOT NULL default(getdate()),
       primary key (Id)
    )

	-- Constraints --

    alter table QuoteUploadLog 
        add constraint FK_QuoteUploadLog_Product
        foreign key (ProductId) 
        references Product
