
begin tran
declare @Owner int
declare @ProductType int

select top 1 @Owner = id from dbo.Organization where TradingName = 'Auto & General'
select top 1 @ProductType = id from md.ProductType where Name = 'VAP'

INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) 
VALUES ( NULL, N'Excess Protect', @Owner, @Owner, @ProductType, N'123456', N'EXCESSPROTECT', CAST(0x00009CD400000000 AS DateTime), NULL)

commit
GO