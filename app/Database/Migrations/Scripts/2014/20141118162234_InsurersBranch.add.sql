
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Organization' AND  COLUMN_NAME = 'ShortTermInsurers')
alter table [dbo].[Organization] add ShortTermInsurers bit  DEFAULT 0

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Organization' AND  COLUMN_NAME = 'LongTermInsurers')
alter table [dbo].[Organization] add LongTermInsurers bit  DEFAULT 0


IF NOT EXISTS 
(SELECT  * FROM    INFORMATION_SCHEMA.TABLES WHERE   TABLE_SCHEMA = 'md' AND TABLE_NAME   = 'InsurersBranchType')
	create table md.InsurersBranchType (
		Id INT IDENTITY NOT NULL,
		Name VARCHAR(50) NOT NULL UNIQUE,
		primary key (Id))


			INSERT INTO [md].[InsurersBranchType]
           ([name])
     VALUES
		('ShortTerm'),
		('LongTerm')

IF NOT EXISTS 
(SELECT  * FROM    INFORMATION_SCHEMA.TABLES WHERE   TABLE_SCHEMA = 'dbo' AND TABLE_NAME   = 'InsurersBranches')
create table dbo.InsurersBranches (
		Id INT IDENTITY NOT NULL,
		Name VARCHAR(50) NOT NULL ,
		StartDate DateTime  NULL,
		EndDate DateTime  NULL,
		OrganizationId int NOT NULL, 
		InsurersBranchTypeId int NOT NULL, 
		primary key (Id))


IF (OBJECT_ID('FK_InsurersBranches_Organization') IS NULL)
		alter table dbo.InsurersBranches 
        add constraint FK_InsurersBranches_Organization 
        foreign key (OrganizationId) 
        references dbo.Organization

IF (OBJECT_ID('FK_InsurersBranches_InsurersBranchType') IS NULL)
		alter table dbo.InsurersBranches 
        add constraint FK_InsurersBranches_InsurersBranchType 
        foreign key (InsurersBranchTypeId) 
        references md.InsurersBranchType