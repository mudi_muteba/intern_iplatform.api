﻿-- Constraints
if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_AuditTrailItem_Party]') AND parent_object_id = OBJECT_ID('AuditTrailItem'))
alter table AuditTrailItem  drop constraint FK_AuditTrailItem_Party


if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_AuditTrailItem_Individual]') AND parent_object_id = OBJECT_ID('AuditTrailItem'))
alter table AuditTrailItem  drop constraint FK_AuditTrailItem_Individual

-- Table
if exists (select * from dbo.sysobjects where id = object_id(N'AuditTrailItem') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table AuditTrailItem

