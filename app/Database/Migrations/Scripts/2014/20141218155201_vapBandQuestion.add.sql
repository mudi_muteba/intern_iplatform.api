
SET IDENTITY_INSERT [md].[QuestionGroup] ON
GO
IF NOT EXISTS (select * from md.QuestionGroup where name = 'Value added products')
BEGIN
INSERT INTO [md].[QuestionGroup]
           ([Id]
		   ,[Name]
           ,[VisibleIndex])
     VALUES
           (8,'Value added products',7)
END
GO
SET IDENTITY_INSERT [md].[QuestionGroup] OFF
GO

SET IDENTITY_INSERT [md].[Question] ON
GO
declare @QuestionGroupId int
select @QuestionGroupId = id from md.QuestionGroup where name = 'Value added products'

IF NOT EXISTS (select * from md.Question where name = 'Value added product')
BEGIN
	INSERT INTO [md].[Question]
			   ([Id]
			   ,[Name]
			   ,[QuestionTypeId]
			   ,[QuestionGroupId])
		 VALUES
			   (138,'Value added product',3,@QuestionGroupId)
	
END
GO
SET IDENTITY_INSERT [md].[Question] OFF
GO


declare @QuestionId int
select @QuestionId = id from md.Question where name = 'Value added product'

IF NOT EXISTS (select * from md.QuestionAnswer where QuestionId = @QuestionId and name = 'Band A')
BEGIN
	INSERT INTO [md].[QuestionAnswer]
			   ([Name]
			   ,[Answer]
			   ,[QuestionId]
			   ,[VisibleIndex])
		 VALUES
			   ('Band A','Band A',@QuestionId,0)
END

IF NOT EXISTS (select * from md.QuestionAnswer where QuestionId = @QuestionId and name = 'Band B')
BEGIN
	INSERT INTO [md].[QuestionAnswer]
			   ([Name]
			   ,[Answer]
			   ,[QuestionId]
			   ,[VisibleIndex])
		 VALUES
			   ('Band B','Band B',@QuestionId,0)
END

IF NOT EXISTS (select * from md.QuestionAnswer where QuestionId = @QuestionId and name = 'Band C')
BEGIN
	INSERT INTO [md].[QuestionAnswer]
			   ([Name]
			   ,[Answer]
			   ,[QuestionId]
			   ,[VisibleIndex])
		 VALUES
			   ('Band C','Band C',@QuestionId,0)
END

IF NOT EXISTS (select * from md.QuestionAnswer where QuestionId = @QuestionId and name = 'Band D')
BEGIN
	INSERT INTO [md].[QuestionAnswer]
			   ([Name]
			   ,[Answer]
			   ,[QuestionId]
			   ,[VisibleIndex])
		 VALUES
			   ('Band D','Band D',@QuestionId,0)
END

IF NOT EXISTS (select * from md.QuestionAnswer where QuestionId = @QuestionId and name = 'Band E')
BEGIN
	INSERT INTO [md].[QuestionAnswer]
			   ([Name]
			   ,[Answer]
			   ,[QuestionId]
			   ,[VisibleIndex])
		 VALUES
			   ('Band E','Band E',@QuestionId,0)
END

IF NOT EXISTS (select * from md.QuestionAnswer where QuestionId = @QuestionId and name = 'Windscreen')
BEGIN
	INSERT INTO [md].[QuestionAnswer]
			   ([Name]
			   ,[Answer]
			   ,[QuestionId]
			   ,[VisibleIndex])
		 VALUES
			   ('Windscreen','Windscreen',@QuestionId,0)
END

GO


SET IDENTITY_INSERT md.cover ON
GO
IF NOT EXISTS (select * from md.cover where name = 'Excess Protect')
INSERT [md].[Cover] ([Id],[Name],[Code]) VALUES ( 369,N'Excess Protect', 'EXCESS_PROTECT')

IF NOT EXISTS (select * from md.cover where name = 'Tyre Protect')
INSERT [md].[Cover] ([Id],[Name],[Code]) VALUES ( 370,N'Tyre Protect', 'TYRE_PROTECT')

IF NOT EXISTS (select * from md.cover where name = 'Tyre & Rim')
INSERT [md].[Cover] ([Id],[Name],[Code]) VALUES (371, N'Tyre & Rim', 'TYRE_RIM')

GO
SET IDENTITY_INSERT md.cover OFF
GO

declare @CoverId int
declare @ProductId int
declare @CoverDefTypeId int

select top 1 @ProductId = p.id from Product p
inner join Organization o on o.Id = p.ProductOwnerId where p.name = 'Excess Protect' and o.TradingName = 'Auto & General' 
select top 1 @CoverDefTypeId = id from [md].[CoverDefinitionType] where name = 'Add On'
select top 1 @CoverId = id from md.Cover where Name = 'Excess Protect'

IF NOT EXISTS (select * from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId)
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Excess Protect', @ProductId, @CoverId, NULL, @CoverDefTypeId, 0)
GO

declare @CoverId int
declare @ProductId int
declare @CoverDefTypeId int

select top 1 @ProductId = p.id from Product p
inner join Organization o on o.Id = p.ProductOwnerId where p.name = 'Tyre Protect' and o.TradingName = 'Auto & General' 
select top 1 @CoverDefTypeId = id from [md].[CoverDefinitionType] where name = 'Add On'
select top 1 @CoverId = id from md.Cover where Name = 'Tyre Protect'

IF NOT EXISTS (select * from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId)
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Tyre Protect', @ProductId, @CoverId, NULL, @CoverDefTypeId, 0)
GO

declare @CoverId int
declare @ProductId int
declare @CoverDefTypeId int

select top 1 @ProductId = p.id from Product p
inner join Organization o on o.Id = p.ProductOwnerId where p.name = 'Tyre & Rim' and o.TradingName = 'Auto & General' 
select top 1 @CoverDefTypeId = id from [md].[CoverDefinitionType] where name = 'Add On'
select top 1 @CoverId = id from md.Cover where Name = 'Tyre & Rim'

IF NOT EXISTS (select * from CoverDefinition where ProductId = @ProductId and CoverId = @CoverId)
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Tyre & Rim', @ProductId, @CoverId, NULL, @CoverDefTypeId, 0)

GO


declare @CoverDefId int
declare @QuestionId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Tyre & Rim' and p.Name = 'Tyre & Rim'


IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 99 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @CoverDefId, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')

select @QuestionId = id from md.Question where name = 'Value added product'
IF NOT EXISTS( select * from QuestionDefinition where QuestionId = @QuestionId and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Band', @CoverDefId, @QuestionId, NULL, 1, 0, 1, 1, 0, N'What is the tyre specification for this vehicle?', N'', N'^[0-9]+$')
GO

declare @CoverDefId int
declare @QuestionId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Tyre Protect' and p.Name = 'Tyre Protect'


IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 99 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @CoverDefId, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')

select @QuestionId = id from md.Question where name = 'Value added product'
IF NOT EXISTS( select * from QuestionDefinition where QuestionId = @QuestionId and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Band', @CoverDefId, @QuestionId, NULL, 1, 0, 1, 1, 0, N'What is the tyre specification for this vehicle?', N'', N'^[0-9]+$')

GO

declare @CoverDefId int
declare @QuestionId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Excess Protect' and p.Name = 'Excess Protect'


IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 99 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @CoverDefId, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')

select @QuestionId = id from md.Question where name = 'Value added product'
IF NOT EXISTS( select * from QuestionDefinition where QuestionId = @QuestionId and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Band', @CoverDefId, @QuestionId, NULL, 1, 0, 1, 1, 0, N'What is the tyre specification for this vehicle?', N'', N'^[0-9]+$')


delete from QuestionDefinition where CoverDefinitionId in (
select id from CoverDefinition where DisplayName 
in ('Band A','Band B','Band C','Band D','Band E'))


delete from CoverDefinition where DisplayName 
in ('Band A','Band B','Band C','Band D','Band E')

delete from md.Cover where Code in 
('BAND_A','BAND_B','BAND_C','BAND_D','BAND_E')

GO

IF NOT EXISTS (select *from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'AnswerDefinition')

	BEGIN
	CREATE TABLE [dbo].[AnswerDefinition](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[ProductId] [int] NOT NULL,
		[QuestionAnswerId] [int] NOT NULL,
	 CONSTRAINT [PK_AnswerDefinition] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[AnswerDefinition]  WITH CHECK ADD  CONSTRAINT [FK_AnswerDefinition_Product] FOREIGN KEY([ProductId])
	REFERENCES [dbo].[Product] ([Id])

	ALTER TABLE [dbo].[AnswerDefinition] CHECK CONSTRAINT [FK_AnswerDefinition_Product]

	ALTER TABLE [dbo].[AnswerDefinition]  WITH CHECK ADD  CONSTRAINT [FK_AnswerDefinition_QuestionAnswer] FOREIGN KEY([QuestionAnswerId])
	REFERENCES [md].[QuestionAnswer] ([Id])

	ALTER TABLE [dbo].[AnswerDefinition] CHECK CONSTRAINT [FK_AnswerDefinition_QuestionAnswer]

END

Declare @ProductId int
Select @ProductId = id from dbo.Product where ProductCode = 'EXCESSPROTECT'

IF NOT EXISTS (select * from AnswerDefinition where ProductId = @ProductId)
BEGIN
	INSERT INTO AnswerDefinition
	(ProductId,QuestionAnswerId)
	SELECT @ProductId, id
	FROM md.QuestionAnswer where name in ('Band A','Band B','Band C','Band D','Band E','Windscreen') 
END

GO

Declare @ProductId int
Select @ProductId = id from dbo.Product where ProductCode = 'TYRERIM'

IF NOT EXISTS (select * from AnswerDefinition where ProductId = @ProductId)
BEGIN
	INSERT INTO AnswerDefinition
	(ProductId,QuestionAnswerId)
	SELECT @ProductId, id
	FROM md.QuestionAnswer where name in ('Band A','Band B','Band C','Band D','Band E') 
END

GO

Declare @ProductId int
Select @ProductId = id from dbo.Product where ProductCode = 'TYREPROTECT'

IF NOT EXISTS (select * from AnswerDefinition where ProductId = @ProductId)
BEGIN
	INSERT INTO AnswerDefinition
	(ProductId,QuestionAnswerId)
	SELECT @ProductId, id
	FROM md.QuestionAnswer where name in ('Band A','Band B','Band C','Band D') 
END
