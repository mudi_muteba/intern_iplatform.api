﻿if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_QuoteItem_CoverDefinition]') AND parent_object_id = OBJECT_ID('QuoteItem'))
	alter table QuoteItem drop constraint FK_QuoteItem_CoverDefinition

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_QuoteItem_Quote]') AND parent_object_id = OBJECT_ID('QuoteItem'))
	alter table QuoteItem drop constraint FK_QuoteItem_Quote

if exists (select * from dbo.sysobjects where id = object_id(N'QuoteItem') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table QuoteItem

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Quote_LeadActivity]') AND parent_object_id = OBJECT_ID('Quote'))
	alter table Quote drop constraint FK_Quote_LeadActivity

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Quote_Product]') AND parent_object_id = OBJECT_ID('Quote'))
	alter table Quote drop constraint FK_Quote_Product

if exists (select * from dbo.sysobjects where id = object_id(N'Quote') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table Quote