
DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')

UPDATE dbo.QuestionDefinition SET VisibleIndex=11
FROM dbo.QuestionDefinition WHERE CoverDefinitionId=(SELECT ID CoverDefinitionId FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND coverid=218)
AND QuestionId IN (SELECT Id Questionid FROM md.Question WHERE QuestionGroupId=1) AND questionid=47

SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')

UPDATE dbo.QuestionDefinition SET VisibleIndex=12
FROM dbo.QuestionDefinition WHERE CoverDefinitionId=(SELECT ID CoverDefinitionId FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND coverid=218)
AND QuestionId IN (SELECT Id Questionid FROM md.Question WHERE QuestionGroupId=1) AND questionid=91

SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')

DECLARE @CoverID INT 
SET @CoverID=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND coverid=218)

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Suburb (Work)' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          135 , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          13 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the suburb and town name where you work?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )