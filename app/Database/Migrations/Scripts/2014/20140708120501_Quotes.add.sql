﻿if NOT exists (select * from dbo.sysobjects where id = object_id(N'Quote') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
create table Quote (
	Id INT IDENTITY NOT NULL,
	DateCreated DATETIME default (getdate()),
	LeadActivityId INT NOT NULL,
	ProductId INT NOT NULL,
	Fees DECIMAL NULL,
	FailureMessage NVARCHAR(MAX) null,
	WarningMessage NVARCHAR(255) null,
	primary key (Id)
)
END

if not exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Quote_LeadActivity]') AND parent_object_id = OBJECT_ID('Quote'))
BEGIN
	alter table Quote
	add constraint FK_Quote_LeadActivity
	foreign key (LeadActivityId)
	references LeadActivity 
END

if not exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Quote_Product]') AND parent_object_id = OBJECT_ID('Quote'))
BEGIN
	alter table Quote
	add constraint FK_Quote_Product
	foreign key (ProductId)
	references Product 
END

if NOT exists (select * from dbo.sysobjects where id = object_id(N'QuoteItem') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
create table QuoteItem (
	Id INT IDENTITY NOT NULL,
	QuoteId INT NOT NULL,
	CoverDefinitionId INT NULL,
	SumInsured DECIMAL NULL,
	Premium DECIMAL NULL,
	Sasria DECIMAL NULL,
	AssetNumber INT NULL,
	ExcessCalculated BIT NULL,
	ExcessBasic DECIMAL NULL,
	ExcessDescription NVARCHAR(MAX) null,
	Description NVARCHAR(255) null,
	FailureMessage NVARCHAR(MAX) null,
	primary key (Id)
)
END

if not exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_QuoteItem_Quote]') AND parent_object_id = OBJECT_ID('QuoteItem'))
BEGIN

	alter table QuoteItem
	add constraint FK_QuoteItem_Quote
	foreign key (QuoteId)
	references Quote 
END

if not exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_QuoteItem_CoverDefinition]') AND parent_object_id = OBJECT_ID('QuoteItem'))
BEGIN
	alter table QuoteItem
	add constraint FK_QuoteItem_CoverDefinition
	foreign key (CoverDefinitionId)
	references CoverDefinition 
END