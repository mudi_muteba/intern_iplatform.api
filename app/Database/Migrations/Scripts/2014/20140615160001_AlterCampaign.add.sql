﻿-- drop constraints
if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Campaign_CampaignSource]') AND parent_object_id = OBJECT_ID('Campaign'))
	alter table Campaign  drop constraint FK_Campaign_CampaignSource

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_LeadActivity_Campaign]') AND parent_object_id = OBJECT_ID('LeadActivity'))
	alter table LeadActivity  drop constraint FK_LeadActivity_Campaign

-- drop table
drop table Campaign

-- create table
    create table Campaign (
		Id INT IDENTITY NOT NULL,
       Name NVARCHAR(255) NOT NULL,
       Reference NVARCHAR(255) NOT NULL,
       CoverId INT NOT NULL,
       StartDate DATETIME  NOT NULL default(getdate()),
       EndDate DATETIME null,
       DateCreated DATETIME NOT NULL default(getdate()),
       DateUpdated DATETIME NOT NULL default(getdate()),
       primary key (Id)
    )

-- create constraints
if not exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Campaign_Cover]') AND parent_object_id = OBJECT_ID('Campaign'))
	alter table Campaign
		add constraint FK_Campaign_Cover
		foreign key (CoverId)
		references md.Cover

alter table LeadActivity 
    add constraint FK_LeadActivity_Campaign
    foreign key (CampaignId) 
    references Campaign
