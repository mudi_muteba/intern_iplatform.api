﻿----------------------------------------------------------------------
DECLARE @id INT
DECLARE @questionType INT = 3
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Type Of Cover Home', @questionType, 2) -- Risk Info

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Type Of Cover Home - Comprehensive', 'Comprehensive', @id, 0),
		('Type Of Cover Home - Limited Cover','Limited Cover', @id, 1),
		('Type Of Cover Home - Full Cover Excl Subsidence Landslip', 'Full Cover Excl Subsidence Landslip', @id, 2),
		('Type Of Cover Home - Full Cover Incl Subsidence Landslip', 'Full Cover Incl Subsidence Landslip', @id, 3)
		--('Type Of Cover - Pay As You Drive', 'Pay As You Drive', @id, 4),
		--('Type Of Cover - Comprehensive ''No Frills'' Cover', 'Comprehensive ''No Frills'' Cover', @id, 5),
		--('Type Of Cover - Flexi Cover', 'Flexi Cover', @id, 6)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Perimeter Wall', @questionType, 1) -- General Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Perimeter Wall - Brick wall less than 180cm tall', 'Brick wall less than 180cm tall', @id, 0),
		('Perimeter Wall - Brick wall more than 180cm tall', 'Brick wall more than 180cm tall', @id, 1),
		('Perimeter Wall - Brick wall more than 180cm tall with eletric fencing', 'Brick wall more than 180cm tall with eletric fencing', @id, 2),
		('Perimeter Wall - Pallisade wall less than 180cm tall', 'Pallisade wall less than 180cm tall', @id, 3),
		('Perimeter Wall - Pallisade wall more than 180cm tall', 'Pallisade wall more than 180cm tall', @id, 4),
		('Perimeter Wall - Pre-cast wall less than 180cm tall', 'Pre-cast wall less than 180cm tall', @id, 5),
		('Perimeter Wall - Pre-cast wall more than 180cm tall', 'Pre-cast wall more than 180cm tall', @id, 6),
		('Perimeter Wall - Wire fence', 'Wire fence', @id, 7),
		('Perimeter Wall - Wood fence less than 180cm tall', 'Wood fence less than 180cm tall', @id, 8),
		('Perimeter Wall - Wood fence more than 180cm tall', 'Wood fence more than 180cm tall', @id, 9),
		('Perimeter Wall - None', 'None', @id, 10)

----------------------------------------------------------------------
