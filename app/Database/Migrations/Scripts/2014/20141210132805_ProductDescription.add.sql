IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLES 
				WHERE
				TABLE_NAME = 'ProductDescription' )

BEGIN
		CREATE TABLE [dbo].[ProductDescription](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[ProductId] [int] NULL,
			[Index] [int] NULL,
			[Description] [nvarchar](255) NULL,
		 CONSTRAINT [PK_ProductDescription] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		-- Add Foreign Key for ProductID
		ALTER TABLE [dbo].[ProductDescription]  WITH CHECK ADD  CONSTRAINT [FK_ProductDescription_Product] FOREIGN KEY([ProductId])
		REFERENCES [dbo].[Product] ([Id])

		ALTER TABLE [dbo].[ProductDescription] CHECK CONSTRAINT [FK_ProductDescription_Product]

-- AA Product
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (11,1, 'AA Membership included in policy.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (11,2, 'One year fixed premium even if you have a claim.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (11,3, 'Theft excess waiver if vehicle is stolen and not recovered provided that the recovery system is fitted and in working condition.');

-- AA Product
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (25,1, 'AA Membership included in policy.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (25,2, 'One year fixed premium even if you have a claim.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (25,3, 'Theft excess waiver if vehicle is stolen and not recovered provided that the recovery system is fitted and in working condition.');

-- Auto and general
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (20,1, 'One year fixed premium even if you have a claim.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (20,2, 'A life time guarantee on all vehicle repair work.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (20,3, 'We will replace a new vehicle , if the vehicle is written off within the 1st year and have less than 20 000kms on the odometer.');

-- Auto and general
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (6,1, 'One year fixed premium even if you have a claim.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (6,2, 'A life time guarantee on all vehicle repair work.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (6,3, 'We will replace a new vehicle , if the vehicle is written off within the 1st year and have less than 20 000kms on the odometer.');

-- Budget
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (7,1, 'Will pay for reasonable towing and storage costs , to the closest authorized repairer if the Budget Towline and approved towing operator is used.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (7,2, 'We will let you repair windscreen chips and small cracks at no extra cost.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (7,3, 'We will include third party liability for fire , explosion and other damage per incident.');

-- Budget
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (21,1, 'Will pay for reasonable towing and storage costs , to the closest authorized repairer if the Budget Towline and approved towing operator is used.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (21,2, 'We will let you repair windscreen chips and small cracks at no extra cost.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (21,3, 'We will include third party liability for fire , explosion and other damage per incident.');

-- Dial Direct
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (8,1, 'Will pay for reasonable towing and storage costs , to the closest authorized repairer if the Budget Towline and approved towing operator is used.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (8,2, 'We will let you repair windscreen chips and small cracks at no extra cost.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (8,3, 'We will include third party liability for fire , explosion and other damage per incident.');

-- Dial Direct
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (22,1, 'Will pay for reasonable towing and storage costs , to the closest authorized repairer if the Budget Towline and approved towing operator is used.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (22,2, 'We will let you repair windscreen chips and small cracks at no extra cost.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (22,3, 'We will include third party liability for fire , explosion and other damage per incident.');

-- FFW
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (9,1, 'Expo sure cover is included on all policies. This is to ensure that you and your family have the right support and assistance in the event of accidental exposure to HIV / Aids.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (9,2, 'We will pay towards medical cost for each injured person following a motor accident.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (9,3, 'We will also include a guardian angel , your very own man on call for all roadside emergencies.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (9,4, 'A recovery system will be fitted at no cost if required.');

-- FFW
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (23,1, 'Expo sure cover is included on all policies. This is to ensure that you and your family have the right support and assistance in the event of accidental exposure to HIV / Aids.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (23,2, 'We will pay towards medical cost for each injured person following a motor accident.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (23,3, 'We will also include a guardian angel , your very own man on call for all roadside emergencies.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (23,4, 'A recovery system will be fitted at no cost if required.');

-- Hollard
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (4,1, 'Combined personal lines insurance.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (4,2, 'Loss of keys cover in respect of motor up to R1 500-00.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (4,3, 'Towing and storage up to R1 500-00 if roadside assistance is utilized.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (4,4, 'Guests and domestic employees property covered up to R1 500-00 under household contents.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (4,5, 'Fridge and freezer contents covered up to R1 500-00 under household contents.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (4,6, 'Groceries in Transit is covered up to R1 500-00 should you have household contents cover.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (4,7, 'Keys, locks and remote control units under the household contents cover is covered up to R3 000-00.');

-- Unity
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (24,1, 'We will replace a new vehicle , if the vehicle is written off within the 1st year and have less than 20 000kms on the odometer.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (24,2, 'A life time guarantee on all vehicle repair work.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (24,3, 'We will also include Road, Home, Medical, Legal, Entertainment and Trauma Assist for your benefit at no extra cost.');

-- Unity
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (10,1, 'We will replace a new vehicle , if the vehicle is written off within the 1st year and have less than 20 000kms on the odometer.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (10,2, 'A life time guarantee on all vehicle repair work.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (10,3, 'We will also include Road, Home, Medical, Legal, Entertainment and Trauma Assist for your benefit at no extra cost.');

-- Virgin
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (12,1, 'Sections covered: Motor, Portable Possessions.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (12,2, 'Motor section allows business use for policy holder drivers over the age of 25 years.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (12,3, 'Not allowed for under 25 year old drivers.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (12,4, 'Should the Householders cover be taken, the policyholder will automatically get the R3900 Unspecified Portable Possessions cover.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (12,5, 'Road Side Assistance (OPTIONAL).');

-- Virgin
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (26,1, 'Sections covered: Motor, Portable Possessions.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (26,2, 'Motor section allows business use for policy holder drivers over the age of 25 years.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (26,3, 'Not allowed for under 25 year old drivers.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (26,4, 'Should the Householders cover be taken, the policyholder will automatically get the R3900 Unspecified Portable Possessions cover.');
INSERT INTO [ProductDescription] ([ProductId], [Index],[Description]) VALUES (26,5, 'Road Side Assistance (OPTIONAL).');

END	
GO
