﻿if not exists(select * from sys.columns where Name = N'IsDeleted' and Object_ID = Object_ID(N'Address'))
begin
	alter table Address
	add IsDeleted bit default 0 not null

	print 'ADDED IsDeleted'
end