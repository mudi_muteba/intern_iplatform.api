if exists(select * from sys.indexes where name = 'IX_QuestionAnswer_Name')
	begin
		DROP INDEX IX_QuestionAnswer_Name ON md.QuestionAnswer
		ALTER TABLE md.QuestionAnswer SET (LOCK_ESCALATION = TABLE)
	end

GO

declare @questionId int
declare @existingProvinceQuestionId int
declare @workSuburbQuestionId int
declare @workSuburbQuestionGroupIndex int
declare @workSuburbQuestionCoverId int

-- get existing province ID to use as template
select @existingProvinceQuestionId = id
from md.Question
where Name = 'Province'

-- get suburb (work) reference
select @workSuburbQuestionId = Id
from md.Question
where Name = 'Suburb (Work)'

-- new province (work) question
insert into md.question
(Name, QuestionTypeId, QuestionGroupId)
values('Province (Work)', 3, 1)

select @questionId = @@IDENTITY

-- make copy of existing province question's answers
insert into md.QuestionAnswer
(Name, Answer, QuestionId, VisibleIndex)
select 'Province - Work - ' + Answer, Answer, @questionId, VisibleIndex
from md.QuestionAnswer
where QuestionId = @existingProvinceQuestionId

-- add the question to all covers based on the existing province's configuration
insert into questionDefinition
(
	MasterId, DisplayName, CoverDefinitionId, QuestionId, ParentId, QuestionDefinitionTypeId, 
	VisibleIndex, RequiredForQuote, RatingFactor, [ReadOnly], ToolTip, DefaultValue, 
	RegexPattern, GroupIndex, QuestionDefinitionGroupTypeId
)
select 	qd.MasterId, 'Province (Work)', CoverDefinitionId, @questionId, qd.ParentId, QuestionDefinitionTypeId, 
	100, RequiredForQuote, RatingFactor, [ReadOnly], 'Which province is the vehicle during working hours?', DefaultValue, 
	RegexPattern, 2, 3
from questionDefinition qd
		inner join coverDefinition cd
			on qd.CoverDefinitionId = cd.Id and coverId = 218
where questionId = @existingProvinceQuestionId
