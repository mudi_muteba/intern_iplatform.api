﻿----------------------------------------------------------------------
DECLARE @questionType INT = 3
----------------------------------------------------------------------

DECLARE @SumAssuredid INT
SET @SumAssuredid = 157

DECLARE @TypeOfCoverid INT
SET @TypeOfCoverid = 158

DECLARE @MainMemberid INT
SET @MainMemberid = 159
	
DECLARE @AddChildid INT
SET @AddChildid = 160

DECLARE @ExtMember1id INT
SET @ExtMember1id = 161
DECLARE @ExtMember2id INT
SET @ExtMember2id = 162
DECLARE @ExtMember3id INT
SET @ExtMember3id = 163
DECLARE @ExtMember4id INT
SET @ExtMember4id = 164
DECLARE @ExtMember5id INT
SET @ExtMember5id = 165
DECLARE @ExtMember6id INT
SET @ExtMember6id = 166
DECLARE @ExtMember7id INT
SET @ExtMember7id = 167
DECLARE @ExtMember8id INT
SET @ExtMember8id = 168
DECLARE @ExtMember9id INT
SET @ExtMember9id = 169
DECLARE @ExtMember10id INT
SET @ExtMember10id = 170

DECLARE @Partyid INT
set @Partyid=(Select PartyID from Organization Where Code='AIG')

--product
INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES 
(0, N'Virgin Domestic Funeral', @Partyid, @Partyid, 7, N'123456', N'VMSAFuneral', CAST(0x00009CD400000000 AS DateTime),NULL)

DECLARE @Productid INT
set @Productid=IDENT_CURRENT('Product')

--covers
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Funeral', @Productid, 142, NULL, 1, 0)

DECLARE @CoverDefFuneralid INT
set @CoverDefFuneralid=IDENT_CURRENT('CoverDefinition')

--questions
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Type Of Cover', @CoverDefFuneralid, @TypeOfCoverid, NULL, 1, 0, 1, 1, 0, N'Please select the type of cover required', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Main Member Date Of Birth', @CoverDefFuneralid, @MainMemberid, NULL, 1, 1, 1, 1, 0, N'Please enter the main members date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Number Of Additional Children', @CoverDefFuneralid, @AddChildid, NULL, 1, 2, 1, 1, 0, N'Please enter the number of additional children', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 1 DoB', @CoverDefFuneralid, @ExtMember1id, NULL, 1, 3, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 2 DoB', @CoverDefFuneralid, @ExtMember2id, NULL, 1, 4, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 3 DoB', @CoverDefFuneralid, @ExtMember3id, NULL, 1, 5, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 4 DoB', @CoverDefFuneralid, @ExtMember4id, NULL, 1, 6, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 5 DoB', @CoverDefFuneralid, @ExtMember5id, NULL, 1, 7, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 6 DoB', @CoverDefFuneralid, @ExtMember6id, NULL, 1, 8, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 7 DoB', @CoverDefFuneralid, @ExtMember7id, NULL, 1, 9, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 8 DoB', @CoverDefFuneralid, @ExtMember8id, NULL, 1, 10, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 9 DoB', @CoverDefFuneralid, @ExtMember9id, NULL, 1, 11, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 10 DoB', @CoverDefFuneralid, @ExtMember10id, NULL, 1, 12, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Sum Assured', @CoverDefFuneralid, @SumAssuredid, NULL, 1, 13, 1, 1, 0, N'Please select the sum assured value', N'', N'^[0-9]+$')

