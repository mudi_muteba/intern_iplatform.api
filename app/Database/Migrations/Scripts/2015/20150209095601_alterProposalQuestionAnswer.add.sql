if exists (select * from dbo.sysobjects where id = object_id(N'ProposalQuestionAnswer_Question'))
begin
	alter table [dbo].[ProposalQuestionAnswer]
	drop constraint ProposalQuestionAnswer_Question

	alter table [dbo].[ProposalQuestionAnswer]
	drop column QuestionId
end
go

if not exists(select * from sys.columns where Name = N'QuestionDefinitionId' and Object_ID = Object_ID(N'ProposalQuestionAnswer'))
begin
	alter table [dbo].[ProposalQuestionAnswer]
	add QuestionDefinitionId int null
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'ProposalQuestionAnswer_QuestionDefinition'))
begin
	ALTER TABLE [ProposalQuestionAnswer] ADD CONSTRAINT [ProposalQuestionAnswer_QuestionDefinition] 
	FOREIGN KEY (QuestionDefinitionId) REFERENCES [dbo].[QuestionDefinition] ([Id])
end
go