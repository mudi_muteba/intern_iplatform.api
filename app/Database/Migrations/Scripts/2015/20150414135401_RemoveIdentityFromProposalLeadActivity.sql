﻿CREATE TABLE dbo.Tmp_ProposalLeadActivity
	(
	Id bigint NOT NULL,
	ProposalHeaderId bigint NOT NULL
	)  ON [PRIMARY]

ALTER TABLE dbo.Tmp_ProposalLeadActivity SET (LOCK_ESCALATION = TABLE)
IF EXISTS(SELECT * FROM dbo.ProposalLeadActivity)
	 EXEC('INSERT INTO dbo.Tmp_ProposalLeadActivity (Id, ProposalHeaderId)
		SELECT Id, ProposalHeaderId FROM dbo.ProposalLeadActivity WITH (HOLDLOCK TABLOCKX)')

DROP TABLE dbo.ProposalLeadActivity

EXECUTE sp_rename N'dbo.Tmp_ProposalLeadActivity', N'ProposalLeadActivity', 'OBJECT' 
ALTER TABLE dbo.ProposalLeadActivity ADD CONSTRAINT
	PK_ProposalLeadActivity PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

