﻿ if exists(select * from sys.tables where Name = N'MapPartyToRegistrationQuestions' )
begin
	drop table MapPartyToRegistrationQuestions

end

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'md' 
                 AND  TABLE_NAME = 'RegistrationQuestion'))
BEGIN
    drop table [md].[RegistrationQuestion]
END