
DECLARE @Partyid INT
set @Partyid=(Select PartyID from Organization Where Code='AIG')

--product
INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (0, N'Virgin Domestic', @Partyid, @Partyid, 7, N'123456', N'VIRGIN', CAST(0x00009CD400000000 AS DateTime),NULL)

DECLARE @Productid INT
set @Productid=IDENT_CURRENT('Product')

--covers
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Motor', @Productid, 218, NULL, 1, 0)

DECLARE @CoverDefMotorid INT
set @CoverDefMotorid=IDENT_CURRENT('CoverDefinition')

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Household Contents', @Productid, 78, NULL, 1, 1)

DECLARE @CoverDefContentsid INT
set @CoverDefContentsid=IDENT_CURRENT('CoverDefinition')

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'House Owners', @Productid, 44, NULL, 1, 2)

DECLARE @CoverDefBuildingid INT
set @CoverDefBuildingid=IDENT_CURRENT('CoverDefinition')

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'All Risks', @Productid, 17, NULL, 1, 3)

DECLARE @CoverDefAllRiskid INT
set @CoverDefAllRiskid=IDENT_CURRENT('CoverDefinition')

--questions
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @CoverDefMotorid, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Make', @CoverDefMotorid, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefMotorid, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefMotorid, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Sum Insured', @CoverDefMotorid, 102, NULL, 1, 4, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Main Driver ID Number', @CoverDefMotorid, 104, NULL, 1, 5, 1, 1, 0, N'What is the ID Number for the main driver of this vehicle?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Marital Status', @CoverDefMotorid, 72, NULL, 1, 6, 0, 1, 0, N'What is the marital status of this client?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Drivers Licence First Issued Date', @CoverDefMotorid, 41, NULL, 1, 7, 1, 1, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Drivers Licence Type', @CoverDefMotorid, 73, NULL, 1, 8, 1, 1, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Suburb', @CoverDefMotorid, 89, NULL, 1, 9, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Postal Code', @CoverDefMotorid, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Type of Cover', @CoverDefMotorid, 53, NULL, 1, 11, 0, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Class of Use', @CoverDefMotorid, 48, NULL, 1, 12, 0, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Tracking Device', @CoverDefMotorid, 87, NULL, 1, 13, 0, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Immobiliser', @CoverDefMotorid, 80, NULL, 1, 14, 1, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Gear Lock', @CoverDefMotorid, 8, NULL, 1, 15, 0, 0, 0, N'Does this vehicle have a gear lock?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Overnight Parking', @CoverDefMotorid, 85, NULL, 1, 16, 0, 0, 0, N'Where is this vehicle parked over night?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Main Driver Date of Birth', @CoverDefMotorid, 42, NULL, 1, 15, 1, 1, 0, N'What is the Date of Birth of the main driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Main Driver Gender', @CoverDefMotorid, 70, NULL, 1, 16, 0, 0, 0, N'What is the Gender of the main driver of this vehicle?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extras Value', @CoverDefMotorid, 107, NULL, 1, 17, 0, 1, 0, N'What is the extras value for this vehicle?', N'', N'^[0-9\.]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Radio Value', @CoverDefMotorid, 110, NULL, 1, 18, 0, 1, 0, N'What is the radio value for this vehicle?', N'', N'^[0-9\.]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Car Hire', @CoverDefMotorid, 77, NULL, 1, 21, 0, 0, 0, N'Does the client require car hire for this vehicle?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'No Claim Bonus', @CoverDefMotorid, 43, NULL, 1, 19, 0, 1, 0, N'How many years has this client been accident free?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Selected Excess', @CoverDefMotorid, 74, NULL, 1, 20, 0, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Credit Shortfall', @CoverDefMotorid, 32, NULL, 1, 21, 0, 0, 0, N'Does the client require credit shortfall for this vehicle?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'NCB Protector', @CoverDefMotorid, 33, NULL, 1, 22, 0, 0, 0, N'Does the client want the NCB Protector option for this vehicle?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Current Insurance Period', @CoverDefMotorid, 114, NULL, 1, 28, 1, 1, 0, N'How long has the client been at his current insurance company?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Claims in the past 12 months', @CoverDefMotorid, 44, NULL, 1, 24, 0, 0, 0, N'How many claims have the client registered in the past 12 months?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Claims in the past 12 to 24 months', @CoverDefMotorid, 45, NULL, 1, 25, 0, 0, 0, N'How many claims have the client registered in the past 12 to 24 months?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Claims in the past 24 to 36 months', @CoverDefMotorid, 46, NULL, 1, 26, 0, 0, 0, N'How many claims have the client registered in the past 24 to 36 months?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Pensioner', @CoverDefMotorid, 1, NULL, 1, 27, 0, 0, 0, N'Is the client a pensioner?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Near Open Ground', @CoverDefMotorid, 129, NULL, 1, 21, 1, 1, 0, N'Is there open ground near the item?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Any Construction Work Nearby', @CoverDefMotorid, 130, NULL, 1, 22, 1, 1, 0, N'Is there construction work near the item?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'ID Number', @CoverDefContentsid, 101, NULL, 1, 0, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Type Of Cover', @CoverDefContentsid, 115, NULL, 1, 1, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Sum Insured', @CoverDefContentsid, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Postal Code', @CoverDefContentsid, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Suburb', @CoverDefContentsid, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Province', @CoverDefContentsid, 47, NULL, 1, 5, 1, 1, 0, N'In which province is the property where the content is located?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Burglar Alarm Type', @CoverDefContentsid, 82, NULL, 1, 6, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Burglar Bars', @CoverDefContentsid, 11, NULL, 1, 7, 1, 1, 0, N'Does the house where the content is located have burglar bars?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Security Gates', @CoverDefContentsid, 15, NULL, 1, 8, 1, 1, 0, N'Does the house where the content is located have security gates?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Type Of Residence', @CoverDefContentsid, 49, NULL, 1, 9, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Residence Is', @CoverDefContentsid, 50, NULL, 1, 10, 1, 1, 0, N'What is the property where the content is located used for?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Wall Construction', @CoverDefContentsid, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Roof Construction', @CoverDefContentsid, 56, NULL, 1, 12, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Unoccupied', @CoverDefContentsid, 57, NULL, 1, 13, 1, 1, 0, N'How often is the house unoccupied?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'No Claim Bonus', @CoverDefContentsid, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Accidental Damage', @CoverDefContentsid, 76, NULL, 1, 15, 1, 1, 0, N'Does the client want to include accidental damage cover?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Situation of Residence', @CoverDefContentsid, 52, NULL, 1, 16, 1, 1, 0, N'Where is this residence situated?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Voluntary Excess', @CoverDefContentsid, 74, NULL, 1, 18, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Exclude Theft', @CoverDefContentsid, 19, NULL, 1, 19, 1, 1, 0, N'Does the client want to exlude Theft from cover?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Subsidence and Landslip', @CoverDefContentsid, 36, NULL, 1, 22, 0, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Electric Fence', @CoverDefContentsid, 18, NULL, 1, 20, 1, 1, 0, N'Does the house where the content is located have an electric fence surrounding the property?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Risk Address', @CoverDefBuildingid, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'ID Number', @CoverDefBuildingid, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Sum Insured', @CoverDefBuildingid, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Postal Code', @CoverDefBuildingid, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Suburb', @CoverDefBuildingid, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Type of Property', @CoverDefBuildingid, 49, NULL, 1, 5, 1, 1, 0, N'What is the type of the Property?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Roof Construction', @CoverDefBuildingid, 56, NULL, 1, 6, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Wall Construction', @CoverDefBuildingid, 55, NULL, 1, 7, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Selected Excess', @CoverDefBuildingid, 64, NULL, 1, 9, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Voluntary Excess', @CoverDefBuildingid, 74, NULL, 1, 10, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Unoccupied', @CoverDefBuildingid, 57, NULL, 1, 13, 1, 1, 0, N'How often is the house unoccupied?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Subsidence and Landslip', @CoverDefBuildingid, 36, NULL, 1, 22, 0, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'All Risk Category', @CoverDefAllRiskid, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Risk Address', @CoverDefAllRiskid, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Postal Code', @CoverDefAllRiskid, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Suburb', @CoverDefAllRiskid, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Desciption', @CoverDefAllRiskid, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Sum Insured', @CoverDefAllRiskid, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Serial/IMEI Number', @CoverDefAllRiskid, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Pensioner', @CoverDefAllRiskid, 1, NULL, 1, 7, 1, 1, 0, N'Is the insured a pensioner?', N'', N'')