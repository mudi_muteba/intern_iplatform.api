﻿DECLARE @default sysname, @sql nvarchar(max);

SELECT @default = i.name
FROM sys.indexes i
INNER JOIN sys.objects o ON i.object_id = o.object_id
INNER JOIN sys.schemas sc ON o.schema_id = sc.schema_id
WHERE i.name IS NOT NULL
AND o.type = 'U'
AND o.name = 'QuoteItemState'
AND i.name like '%QuoteItemId%';

IF EXISTS(SELECT * FROM sys.indexes  WHERE name= @default AND object_id = OBJECT_ID('QuoteItemState'))
BEGIN
	SET @sql = N'DROP INDEX ' + @default + ' ON [dbo].[QuoteItemState]';
	EXEC sp_executesql @sql;
END