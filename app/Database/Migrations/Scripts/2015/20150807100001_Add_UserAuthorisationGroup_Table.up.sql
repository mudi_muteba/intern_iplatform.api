﻿ALTER TABLE dbo.UserAuthorisationGroup ADD CONSTRAINT
	UserAuthorisationGroup_AuthorisationGroup FOREIGN KEY
	(
	AuthorisationGroupId
	) REFERENCES md.AuthorisationGroup
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
