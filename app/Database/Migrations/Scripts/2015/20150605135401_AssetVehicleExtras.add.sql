if not exists(select * from sys.all_objects where  Object_ID = Object_ID(N'AssetExtras'))
BEGIN
	CREATE TABLE [dbo].[AssetExtras](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[AssetId] [int] NOT NULL,
		[Description] [nvarchar](100) NOT NULL,
		[Value] [decimal](18, 2) NOT NULL,
		[IsUserDefined] [bit] NOT NULL,
		[Selected] [bit] NOT NULL
	) ON [PRIMARY]

	

	ALTER TABLE [dbo].[AssetExtras] ADD  CONSTRAINT [DF_AssetExtras_IsUserDefined]  DEFAULT ((0)) FOR [IsUserDefined]
	ALTER TABLE [dbo].[AssetExtras] ADD  CONSTRAINT [DF_AssetExtras_Selected]  DEFAULT ((0)) FOR [Selected]
	

	ALTER TABLE [dbo].[AssetExtras]  WITH CHECK ADD  CONSTRAINT [FK_AssetExtras_Asset] FOREIGN KEY([AssetId])
	REFERENCES [dbo].[Asset] ([Id])
	

	ALTER TABLE [dbo].[AssetExtras] CHECK CONSTRAINT [FK_AssetExtras_Asset]
	


end