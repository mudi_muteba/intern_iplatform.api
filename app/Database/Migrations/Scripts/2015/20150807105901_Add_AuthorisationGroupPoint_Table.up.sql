﻿ALTER TABLE dbo.AuthorisationGroupPoint ADD CONSTRAINT
	FK_AuthorisationGroupPoint_AuthorisationGroup FOREIGN KEY
	(
	AuthorisationGroupId
	) REFERENCES md.AuthorisationGroup
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
