

-- Insert first 4 questions for all products 

INSERT INTO SecondLevelQuestionDefinition (MasterId, DisplayName, CoverDefinitionId, ProductId, SecondLevelQuestionId, ParentId, QuestionDefinitionTypeId, VisibleIndex, [Required], 
RatingFactor, [ReadOnly], ToolTip, DefaultValue, RegexPattern)
SELECT 0, 'Have you or anyone you intend on covering under this policy ever had cover cancelled by an insurer, been refused renewal of insurance, been advised to get alternative insurance or special terms and/or conditions imposed by an insurer on your policy?',
NULL, Id, 1, NULL, 1, 1, 1, 0, 0, '',
NULL, '' FROM Product

INSERT INTO SecondLevelQuestionDefinition (MasterId, DisplayName, CoverDefinitionId, ProductId, SecondLevelQuestionId, ParentId, QuestionDefinitionTypeId, VisibleIndex, [Required], 
RatingFactor, [ReadOnly], ToolTip, DefaultValue, RegexPattern)
SELECT 0, 'Have you or anyone you intend on covering under this policy ever been placed under debt review or administration?',
NULL, Id, 2, NULL, 1, 2, 1, 0, 0, '',
NULL, '' FROM Product

INSERT INTO SecondLevelQuestionDefinition (MasterId, DisplayName, CoverDefinitionId, ProductId, SecondLevelQuestionId, ParentId, QuestionDefinitionTypeId, VisibleIndex, [Required], 
RatingFactor, [ReadOnly], ToolTip, DefaultValue, RegexPattern)
SELECT 0, 'Does you or anyone you intend on covering under this policy have defaults or judgements passed against you/them or been found guilty of any criminal offence?',
NULL, Id, 3, NULL, 1, 3, 1, 0, 0, '',
NULL, '' FROM Product

INSERT INTO SecondLevelQuestionDefinition (MasterId, DisplayName, CoverDefinitionId, ProductId, SecondLevelQuestionId, ParentId, QuestionDefinitionTypeId, VisibleIndex, [Required], 
RatingFactor, [ReadOnly], ToolTip, DefaultValue, RegexPattern)
SELECT 0, 'Have you or any company you were involved with or anyone you intend on covering under this policy been onsolvent, sequestrated or liquidated?',
NULL, Id, 4, NULL, 1, 4, 1, 0, 0, '',
NULL, '' FROM Product


-- Insert item level question per cover type for motor
DECLARE @CoverDefinitionId INT
DECLARE @ProductId INT

DECLARE CoverDefinitionCursor CURSOR FOR
SELECT Id, ProductId FROM CoverDefinition WHERE CoverId = 218

OPEN CoverDefinitionCursor

FETCH NEXT FROM CoverDefinitionCursor
INTO @CoverDefinitionId, @ProductId

WHILE @@FETCH_STATUS = 0
BEGIN

	INSERT INTO SecondLevelQuestionDefinition (MasterId, DisplayName, CoverDefinitionId, ProductId, SecondLevelQuestionId, ParentId, QuestionDefinitionTypeId, VisibleIndex, [Required], 
	RatingFactor, [ReadOnly], ToolTip, DefaultValue, RegexPattern)
	VALUES (0, 'Will anyone use the car for business purposes such as selling, servicing, maintenance work, deliveries, visiting clients or basically anything where you need your car to make a living? Or would it be used for private use only? (If yes, inform the client that only that client and spouse will be covered for business.)',
	@CoverDefinitionId, @ProductId, 5, NULL, 1, 1, 1, 0, 0, '',
	NULL, '')
	
	FETCH NEXT FROM CoverDefinitionCursor
	INTO @CoverDefinitionId, @ProductId
END

CLOSE CoverDefinitionCursor
DEALLOCATE CoverDefinitionCursor