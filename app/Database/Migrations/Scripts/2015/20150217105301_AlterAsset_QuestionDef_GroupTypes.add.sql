if exists (select * from dbo.sysobjects where id = object_id(N'Proposal_Asset'))
begin
	alter table [dbo].ProposalDefinition
	drop constraint Proposal_Asset

	alter table ProposalDefinition
	drop column assetId

	print 'REMOVED Proposal_Asset'
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'Asset_Party'))
begin
	alter table [dbo].Asset
	add PartyId int null 

	alter table [Asset] add constraint [Asset_Party] 
	foreign key (PartyId) references [dbo].[Party] ([Id])

	print 'ADDED Asset_Party'
end
go


if not exists(select * from sys.objects where Name = N'QuestionDefinitionGroupType' )
begin
create table [md].[QuestionDefinitionGroupType](
	[Id] [int] primary key NOT NULL,
	[Name] [nvarchar](255) NOT NULL) 

	print 'CREATED QuestionDefinitionGroupType'
end
go

if not exists(select * from md.QuestionDefinitionGroupType where Name = N'Overnight address')
begin
insert into md.QuestionDefinitionGroupType values (1,'Overnight address')
 end
go

if not exists(select * from md.QuestionDefinitionGroupType where Name = N'Home address')
begin
insert into md.QuestionDefinitionGroupType values (2,'Home address')
 end
go

if not exists(select * from md.QuestionDefinitionGroupType where Name = N'Work address')
begin
insert into md.QuestionDefinitionGroupType values (3,'Work address')
 end
go

if not exists(select * from md.QuestionDefinitionGroupType where Name = N'Vehicle asset')
begin
insert into md.QuestionDefinitionGroupType values (4,'Vehicle asset')
 end
go

if not exists(select * from sys.columns where Name = N'GroupIndex' and Object_ID = Object_ID(N'QuestionDefinition'))
begin
	alter table [dbo].QuestionDefinition
	add GroupIndex int null
	print 'ADDED GroupIndex'

end
go

if not exists(select * from sys.columns where Name = N'GroupTypeId' and Object_ID = Object_ID(N'QuestionDefinition'))
begin
	alter table [dbo].QuestionDefinition
	add GroupTypeId int null
	
	alter table [QuestionDefinition] add constraint [QuestionDefinition_QuestionDefinitionGroupType] 
	foreign key (GroupTypeId) references [md].QuestionDefinitionGroupType ([Id])
	print 'ADDED GroupTypeId'

end
go





