



UPDATE md.SecondLevelQuestion
SET Name = 'Policy cancelled or refused renewal'
WHERE Id = 1

UPDATE md.SecondLevelQuestion
SET Name = 'Under debt review or administration'
WHERE Id = 2

UPDATE md.SecondLevelQuestion
SET Name = 'Any defaults or judgements'
WHERE Id = 3

UPDATE md.SecondLevelQuestion
SET Name = 'Insolvent, sequestrated or liquidated'
WHERE Id = 4

UPDATE md.SecondLevelQuestion
SET Name = 'Vehicle Use'
WHERE Id = 5



UPDATE SecondLevelQuestionDefinition
SET DisplayName = 'Policy cancelled or refused renewal', ToolTip = 'Have you or anyone you intend on covering under this policy ever had cover cancelled by an insurer, been refused renewal of insurance, been advised to get alternative insurance or special terms and/or conditions imposed by an insurer on your policy?'
WHERE SecondLevelQuestionId = 1

UPDATE SecondLevelQuestionDefinition
SET DisplayName = 'Under debt review or administration', ToolTip = 'Have you or anyone you intend on covering under this policy ever been placed under debt review or administration?'
WHERE SecondLevelQuestionId = 2


UPDATE SecondLevelQuestionDefinition
SET DisplayName = 'Any defaults or judgements', ToolTip = 'Does you or anyone you intend on covering under this policy have defaults or judgements passed against you/them or been found guilty of any criminal offence?'
WHERE SecondLevelQuestionId = 3

UPDATE SecondLevelQuestionDefinition
SET DisplayName = 'Insolvent, sequestrated or liquidated', ToolTip = 'Have you or any company you were involved with or anyone you intend on covering under this policy been insolvent, sequestrated or liquidated?'
WHERE SecondLevelQuestionId = 4


UPDATE SecondLevelQuestionDefinition
SET DisplayName = 'Vehicle Use', ToolTip = 'Will anyone use the car for business purposes such as selling, servicing, maintenance work, deliveries, visiting clients or basically anything where you need your car to make a living? Or would it be used for private use only? (If yes, inform the client that only that client and spouse will be covered for business.)'
WHERE CoverDefinitionId  in (SELECT Id FROM CoverDefinition WHERE CoverId = 218)
