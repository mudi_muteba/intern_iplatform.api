﻿--add new Group
INSERT INTO md.ClaimsQuestionGroup(Name,VisibleIndex)
VALUES		('Injured Persons',650),
			('Loss / Damages',1350),
			('Medical Information',1260),
			('Other Insurance Details',1270),
			('Previous Claims',1280),
			('Details Of Property',1360)
Go

--Add Questions
INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId) 
VALUES		('Injured Person 1 Name', 4, 26), --PLiability continued
			('Injured Person 1 Address', 4, 26),
			('Injured Person 1 Age', 10, 26),
			('Injury Details Of Person 1', 4, 26),
			('Injured Person 2 Name', 4, 26),
			('Injured Person 2 Address', 4, 26),
			('Injured Person 2 Age', 10, 26),
			('Injury Details Of Person 2', 4, 26),
			('Relationship (If The Person Named Above Is In Your Sevice, Or Your Tenant, Or Related To You, Give Full Details)', 4, 26),
			('Claim (If Claim Is Made Against You, Give Details And Attach Any Correspondence)', 9, 26),
			('Loss/Damage Occurance: Date Of Loss Or Date Discover', 2, 27),
			('Loss/Damage Occurance: Time Of Loss', 4, 27),
			('Loss/Damage Occurance: Place Of Loss/Damage If Different From Risk Address Above', 4, 27),
			('Loss/Damage Occurance: If Loss/Damage Was Caused By Another Party, Name', 4, 27),
			('Loss/Damage Occurance: If Loss/Damage Was Caused By Another Party, Address', 4, 27),
			('Letter From Third Party Stating Reason For Holding Our Client Liable', 9, 24),
			('Quotations/Invoices', 9, 24),
			('If TP Insured: No-Claim Letter From TP Insurer', 9, 24),
			('If TP Uninsured: Sworn Affidavit Of Non-Insurance', 9, 24),
			('Staff Injury On Property: What Happened', 4, 5), --Personal Accident
			('Passenger/Death/Disability: What Happened', 4, 5),
			('Medical Expenses: What Happened', 4, 5),
			('Bodily Injury/Death: What Happened', 4, 5),
			('Permanent Disability: What Happened', 4, 5),
			('Temporary Disability: What Happened', 4, 5),
			('Accidental Death: What Happened', 4, 5),
			('Injury/Illness: Give Full Particulars Of The Accident And Nature Of Injuries Or The Name Of The Illness', 4, 12),
			('Doctor: Name Of Doctor Who Attended To You', 4, 28),
			('Doctor: Address Of Doctor Who Attended To You', 4, 28),
			('Doctor: Name Of Your Usual Doctor', 4, 28),
			('Doctor:  Address Of Your Usual Doctor', 4, 28),
			('Disablement: Period Of Temporary Total Disablement From', 4, 28),
			('Disablement: Period Of Temporary Total Disablement To', 4, 28),
			('Disablement: Period Of Temporary Partial Disablement From', 4, 28),
			('Disablement: Period Of Temporary Partial Disablement To', 4, 28),
			('Disablement: Give Date Normal Occupation Resumed', 2, 28),
			('Has Any Permanent Disablement Resulted? Give Details', 4, 28),
			('Other Insurances: Give Name Of Any Other Insurer With Whom Insured Person Is Insured', 4, 29),
			('Previous Claims: Give Details Of All Claims Made Against Insurer Or In Terms Of The WCA By The Insured Person. Compensation For Occupational  Injuries And Diseases Act No. 150 Of 1993', 4, 30),
			('Letter From Third Party Stating Reason For Liability', 9, 24),
			('Medical Invoices', 9, 24),
			('Reports From Doctor Confirming Injury', 9, 24),
			('Laptop Stolen/Notebooks: What Happened', 4, 6), --Theft Of Property Out Of Motor
			('Radio Stolen: What Happened', 4, 6),
			('Personal Effect Stolen: What Happened', 4, 6),
			('Tools: What Happened', 4, 6),
			('GPS: What Happened', 4, 6),
			('Mobile Communications Equip: What Happened', 4, 6),
			('Theft Date', 2, 25),
			('Theft Time', 4, 25),
			('Theft Place', 4, 25),
			('Loss/Damage Occurence: Details Of How Loss/Damage Occured/If Applicatable State How Entry Was Gained', 4, 27),
			('Details Of Property Lost, Stolen Or Damaged: Description Of Property', 4, 31),
			('Details Of Property Lost, Stolen Or Damaged: From Whom Purchased/Acquired', 4, 31),
			('Details Of Property Lost, Stolen Or Damaged: Serial No.', 4, 31),
			('Details Of Property Lost, Stolen Or Damaged: Value', 10, 31),
			('Details Of Property Lost, Stolen Or Damaged: Amount Claimed', 10, 31),
			('Details Of Property Lost, Stolen Or Damaged: Please Supply A Quotation In Respect Of Items Claimed', 9, 31),
			('Where Item(s) Was Kept In The Vehicle?', 4, 31),
			('Forced Entry (Proof) Required', 9, 31),
			('Proof Of Forcible And Violent Entry/Exit', 9, 24),
			('List Of Stolen Items', 4, 24)


----------------------------------------------------------------------
DECLARE @id INT
----------------------------------------------------------------------

INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId) 
VALUES		('Vehicle Attended/Unattended', 3, 31)

SET @id = SCOPE_IDENTITY()

INSERT INTO md.ClaimsQuestionAnswer (Name, Answer, ClaimsQuestionId, VisibleIndex)
VALUES	('Vehicle Attended/Unattended - Attended', 'Attended', @id, 0),
		('Vehicle Attended/Unattended - Unattended','Unattended', @id, 1)

----------------------------------------------------------------------
INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId) 
VALUES		('Type Of Vehicle', 3, 31)

SET @id = SCOPE_IDENTITY()

INSERT INTO md.ClaimsQuestionAnswer (Name, Answer, ClaimsQuestionId, VisibleIndex)
VALUES	('Type Of Vehicle - Sedan', 'Sedan', @id, 0),
		('Type Of Vehicle - LDV','LDV', @id, 1),
		('Type Of Vehicle - 4x4','4x4', @id, 2)