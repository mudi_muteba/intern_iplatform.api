﻿--Change visibleindex to have gaps to cater for differet claims types
UPDATE md.ClaimsQuestionGroup Set VisibleIndex=VisibleIndex*100
Go

--update existing data to correct ID
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=24 Where ClaimsQuestionGroupId=14
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=23 Where ClaimsQuestionGroupId=13
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=22 Where ClaimsQuestionGroupId=12
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=21 Where ClaimsQuestionGroupId=11
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=20 Where ClaimsQuestionGroupId=10
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=19 Where ClaimsQuestionGroupId=9
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=18 Where ClaimsQuestionGroupId=8
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=17 Where ClaimsQuestionGroupId=7
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=16 Where ClaimsQuestionGroupId=6
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=15 Where ClaimsQuestionGroupId=5
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=14 Where ClaimsQuestionGroupId=4
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=13 Where ClaimsQuestionGroupId=3
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=12 Where ClaimsQuestionGroupId=2
Go
UPDATE md.ClaimsQuestion Set ClaimsQuestionGroupId=2 Where ClaimsQuestionGroupId=1
Go

--add new Group
INSERT INTO md.ClaimsQuestionGroup(Name,VisibleIndex)
VALUES		('Theft Details',1250)
Go

UPDATE md.ClaimsQuestionGroup Set Name='Public Liability / Personal Liability' Where Name ='Public Liability'
Go

Delete From md.ClaimsQuestionGroup Where Name ='Personal Liability'
Go

--Add Questions
INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId) 
VALUES		('Stolen Car: What Happened', 4, 1), --Motor Theft
			('Hijacking: What Happened', 4, 1),
			('Attempted Theft/Parts Stolen: What Happened', 4, 1),
			('Caravan Theft: What Happened', 4, 1),
			('Trailer Theft: What Happened', 4, 1),
			('Business Telephone Number', 10, 12),
			('Police Date Reported', 2, 19),
			('Vehicle Exterior Colour', 4, 22),
			('Vehicle Interior Colour', 4, 22),
			('Copy Of Registration Certificate', 9, 22),
			('SAP 13 Form', 9, 24),
			('Original Registration Paper', 9, 24),
			('Was The Vehicle Locked?', 1, 25),
			('If Not, Give A Reason', 4, 25),
			('Details Of Stolen Accessories (Please Attach Invoice)', 4, 25),
			('Invoice for Stolen Accessories', 9, 25),
			('Are These Items Seprately Insured?', 1, 25),
			('Anti-Theft/Vehicle Recovery Device Details:  Make', 4, 25),
			('Details Of Window Markings',4, 25),
			('Details Of Scratches, Dents, Defects On Vehicle', 4, 25),
			('Details Of Other Features Which Would Assist In Identification', 4, 25),
			('Dog Bite: What Happened', 4, 3), --PLiability
			('Injury To Third Party: What Happened', 4, 3),
			('Loss Or Damage To Third Party Property: What Happened', 4, 3),
			('Accidental Death Third Party: What Happened', 4, 3),
			('Bodily Injury Third Party: What Happened', 4, 3),
			('Illness Of Third Party: What Happened', 4, 3),
			('Wrongful Arrest Of Third Party: What Happened', 4, 3),
			('Products Liability: What Happened', 4, 3),
			('Defective Workmanship: What Happened', 4, 3)
