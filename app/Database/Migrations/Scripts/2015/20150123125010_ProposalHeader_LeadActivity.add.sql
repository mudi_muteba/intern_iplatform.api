if EXISTS (select * from dbo.sysobjects where id = object_id(N'Proposal_LeadActivity'))
begin
	alter table [dbo].[ProposalDefinition]
	drop constraint Proposal_LeadActivity
end

IF EXISTS(SELECT *FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'ProposalDefinition' AND COLUMN_NAME = 'LeadActivityId') 
begin				 
	alter table [dbo].[ProposalDefinition]
	drop column LeadActivityId
end

IF NOT EXISTS(SELECT *FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'ProposalHeader' AND COLUMN_NAME = 'LeadActivityId') 
begin				 
	alter table [dbo].[ProposalHeader]
	add [LeadActivityId] Int NULL
end

if NOT EXISTS (select * from dbo.sysobjects where id = object_id(N'ProposalHeader_LeadActivity'))
begin
	ALTER TABLE [ProposalHeader] ADD CONSTRAINT [ProposalHeader_LeadActivity] FOREIGN KEY ([LeadActivityId]) REFERENCES [LeadActivity] ([Id])
end

