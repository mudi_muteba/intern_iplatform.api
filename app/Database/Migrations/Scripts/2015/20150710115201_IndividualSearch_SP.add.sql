
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'IndividualSearch')
                    AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE IndividualSearch
GO
-- =============================================
-- Author:		Marius Pruis
-- Create date: 2015 07 10
-- Description:	Search for an Individual based on Cell, Email, ID Number and Policy Number
-- =============================================
CREATE PROCEDURE [dbo].[IndividualSearch] 
	@Cell nvarchar(10), 
	@Email nvarchar(50), 
	@IdentityNo nvarchar(13), 
	@PolicyNo nvarchar(20), 
	@FirstName nvarchar(20),
	@SurName nvarchar(20) 

	AS
BEGIN
	select I.Id,
	I.FirstName,
	I.Surname,
	I.IdentityNo,
	I.PassportNo,
	I.MiddleName,
	I.DateOfBirth,
	I.GenderId,
	I.MaritalStatusId,
	I.HomeLanguageId,
	I.TitleId,
	PH.Id as PolicyId,
	PH.PolicyNo,
	CD.Cell,
	CD.Email,
	O.Id as OccupationId,
	O.Code as OccupationCode,
	O.Name as OccupationName,
	A.Description as Address,
	P.DateCreated,
	P.DateUpdated
	from Individual as I
		inner join Party as P with (nolock) on P.Id = I.Id
		inner join Lead as L with (nolock) on L.PartyId = I.Id
		left outer join ContactDetail as CD with (nolock) on CD.Id = P.ContactDetailId
		left outer join Address as A with (nolock) on A.PartyId = P.Id
		left outer join PolicyHeader as PH with (nolock) on PH.PartyId = I.Id
		left outer join Occupation as O with (nolock) on O.Id = i.OccupationId
	where 
	(@Cell is null or CD.Cell like '%'+@Cell+'%') 
	AND(@Email is null or CD.Email like '%'+@Email+'%') 
	AND(@IdentityNo is null or I.IdentityNo like '%'+@IdentityNo+'%') 
	AND(@PolicyNo is null or PH.PolicyNo like '%'+@PolicyNo+'%') 
	AND(@FirstName is null or I.FirstName like '%'+@FirstName+'%') 
	AND(@SurName is null or I.Surname like '%'+@SurName+'%') 


		
END

