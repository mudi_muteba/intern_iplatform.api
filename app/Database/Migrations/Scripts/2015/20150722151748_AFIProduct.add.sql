
SET ANSI_PADDING ON
GO

DECLARE @id INT

--AFI POC

--party
INSERT [dbo].[Party] ([MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (0, 2, NULL, N'Alexander Forbes Insurance', NULL, NULL)

DECLARE @Partyid INT
set @Partyid=SCOPE_IDENTITY()--IDENT_CURRENT('Party')

--organization
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES 
(@Partyid, N'AFI', N'Alexander Forbes Insurance', N'Alexander Forbes Insurance', '2011-05-03', '', N'', N'', N'')

--address
INSERT [dbo].[Address] ([PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], 
[DateFrom], [DateTo]) VALUES (@Partyid, 0, N'Head Office', N'', N'115 West Street', N'', N'Sandown', N'Johannesburg', N'2196', 
CAST(-25.789952 AS Decimal(9, 6)), CAST(28.278506 AS Decimal(9, 6)), 4, 1, 1, 1, '03/05/2011', NULL)

--product
INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) 
VALUES (0, N'Personal Motor', @Partyid, @Partyid, 7, N'123456', N'POC-MOTOR', CAST(0x00009CD400000000 AS DateTime), NULL)

SET @id = SCOPE_IDENTITY()

--cover
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) 
VALUES (0, N'Motor', @id, 218, NULL, 1, 0)

SET @id = SCOPE_IDENTITY()

--questions
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @id, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Make', @id, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @id, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @id, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @id, 97, NULL, 1, 5, 1, 1, 0, N'What is the registration number for this vehicle?', N'', N'^.{0,10}$')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Type of Cover', @id, 53, NULL, 1, 6, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Suburb', @id, 89, NULL, 1, 7, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Postal Code', @id, 90, NULL, 1, 8, 1, 1, 0, N'What is the postal code where this vehicle will be parked overnight?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Class of Use', @id, 48, NULL, 1, 9, 0, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Immobiliser', @id, 80, NULL, 1, 11, 0, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Tracking Device', @id, 87, NULL, 1, 12, 0, 0, 0, N'Is there a tracking device installed in this vehicle?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Drivers Licence Type', @id, 73, NULL, 1, 13, 1, 0, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Drivers Licence First Issued Date', @id, 41, NULL, 1, 14, 1, 0, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Main Driver Date of Birth', @id, 42, NULL, 1, 15, 1, 1, 0, N'What is the Date of Birth of the main driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Main Driver Gender', @id, 70, NULL, 1, 16, 0, 0, 0, N'What is the Gender of the main driver of this vehicle?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Sum Insured', @id, 102, NULL, 1, 17, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'No Claim Bonus', @id, 43, NULL, 1, 18, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Car Hire', @id, 77, NULL, 1, 19, 0, 1, 0, N'Does the client require car hire?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Overnight Parking', @id, 85, NULL, 1, 20, 1, 1, 0, N'Where is this vehicle parked overnight?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Driver for this vehicle', @id, 63, NULL, 1, 21, 1, 1, 0, N'Who will be the main driver for this vehicle?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Relationship to the Insured', @id, 60, NULL, 1, 22, 1, 1, 0, N'What is the drivers relationship to the insured?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Voluntary Excess', @id, 74, NULL, 1, 23, 0, 1, 0, N'Does the client want to pay a voluntary excess that will lower premium?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Extras Value', @id, 107, NULL, 1, 24, 1, 1, 0, N'What is the value of the any non-factory extras?', N'', N'^[0-9\.]+$')