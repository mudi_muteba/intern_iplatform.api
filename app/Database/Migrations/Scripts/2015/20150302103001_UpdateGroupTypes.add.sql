/**************************************/


if exists (select * from dbo.sysobjects where id = object_id(N'ProposalQuestionAnswer_Question'))
begin
	alter table [dbo].[ProposalQuestionAnswer]
	drop constraint ProposalQuestionAnswer_Question

	alter table [dbo].[ProposalQuestionAnswer]
	drop column QuestionId
end
go

if not exists(select * from sys.columns where Name = N'QuestionDefinitionId' and Object_ID = Object_ID(N'ProposalQuestionAnswer'))
begin
	alter table [dbo].[ProposalQuestionAnswer]
	add QuestionDefinitionId int null
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'ProposalQuestionAnswer_QuestionDefinition'))
begin
	ALTER TABLE [ProposalQuestionAnswer] ADD CONSTRAINT [ProposalQuestionAnswer_QuestionDefinition] 
	FOREIGN KEY (QuestionDefinitionId) REFERENCES [dbo].[QuestionDefinition] ([Id])
end
go
/**************************************/

if exists (select * from dbo.sysobjects where id = object_id(N'Proposal_Asset'))
begin
	alter table [dbo].ProposalDefinition
	drop constraint Proposal_Asset

	alter table ProposalDefinition
	drop column assetId

	print 'REMOVED Proposal_Asset'
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'Asset_Party'))
begin
	alter table [dbo].Asset
	add PartyId int null 

	alter table [Asset] add constraint [Asset_Party] 
	foreign key (PartyId) references [dbo].[Party] ([Id])

	print 'ADDED Asset_Party'
end
go


if not exists(select * from sys.objects where Name = N'QuestionDefinitionGroupType' )
begin
create table [md].[QuestionDefinitionGroupType](
	[Id] [int] primary key NOT NULL,
	[Name] [nvarchar](255) NOT NULL) 

	print 'CREATED QuestionDefinitionGroupType'
end
go

if not exists(select * from md.QuestionDefinitionGroupType where Name = N'Overnight address')
begin
insert into md.QuestionDefinitionGroupType values (1,'Overnight address')
 end
go

if not exists(select * from md.QuestionDefinitionGroupType where Name = N'Home address')
begin
insert into md.QuestionDefinitionGroupType values (2,'Home address')
 end
go

if not exists(select * from md.QuestionDefinitionGroupType where Name = N'Work address')
begin
insert into md.QuestionDefinitionGroupType values (3,'Work address')
 end
go

if not exists(select * from md.QuestionDefinitionGroupType where Name = N'Vehicle asset')
begin
insert into md.QuestionDefinitionGroupType values (4,'Vehicle asset')
 end
go

if not exists(select * from sys.columns where Name = N'GroupIndex' and Object_ID = Object_ID(N'QuestionDefinition'))
begin
	alter table [dbo].QuestionDefinition
	add GroupIndex int null
	print 'ADDED GroupIndex'

end
go

if not exists(select * from sys.columns where Name = N'GroupTypeId' and Object_ID = Object_ID(N'QuestionDefinition'))
begin
	alter table [dbo].QuestionDefinition
	add GroupTypeId int null
	
	if not exists(select * from sys.objects where Name = N'QuestionDefinition_QuestionDefinitionGroupType' )
	begin
		alter table [QuestionDefinition] add constraint [QuestionDefinition_QuestionDefinitionGroupType] 
		foreign key (GroupTypeId) references [md].QuestionDefinitionGroupType ([Id])
		print 'ADDED GroupTypeId'
	end
end
go




/**************************************/


if not exists(select * from sys.columns where Name = N'Answer' and Object_ID = Object_ID(N'ProposalQuestionAnswer'))
begin
	alter table ProposalQuestionAnswer
	add Answer nvarchar(255) null

	print 'ADDED Answer'
end
go

if not exists(select * from sys.columns where Name = N'QuestionTypeId' and Object_ID = Object_ID(N'ProposalQuestionAnswer'))
begin
	alter table ProposalQuestionAnswer
	add QuestionTypeId int null
	print 'ADDED QuestionTypeId'

	ALTER TABLE [ProposalQuestionAnswer] ADD CONSTRAINT [ProposalQuestionAnswer_QuestionType] 
	FOREIGN KEY (QuestionTypeId) REFERENCES [md].[QuestionType] ([Id])
end
go

if exists(select * from sys.columns where Name = N'QuestionAnswerId' and Object_ID = Object_ID(N'ProposalQuestionAnswer'))
begin
	alter table ProposalQuestionAnswer
	drop ProposalQuestionAnswer_QuestionAnswer
	
	alter table ProposalQuestionAnswer
	drop column QuestionAnswerId
	print 'DROPED QuestionAnswerId'

end
go

if exists(select * from sys.columns where Name = N'CustomAnswer' and Object_ID = Object_ID(N'ProposalQuestionAnswer'))
begin
	alter table ProposalQuestionAnswer
	drop column CustomAnswer
	print 'DROPED CustomAnswer'

end
go

-- Question Type
SET IDENTITY_INSERT md.QuestionType ON

	if not exists(select * from md.QuestionType where Name = 'Address')
	begin
		insert into md.QuestionType (Id,Name) values (5,'Address')
	end
	go

	if not exists(select * from md.QuestionType where Name = 'Asset')
	begin
		insert into md.QuestionType (Id,Name) values (6,'Asset')
	end
	go

SET IDENTITY_INSERT md.QuestionType OFF

-- Question
SET IDENTITY_INSERT md.Question ON
	
	if not exists(select * from md.Question where Name = 'Work Address')
	begin
		insert into md.Question (Id,Name,QuestionTypeId,QuestionGroupId) values (139,'Work Address',5,1)
	end
	go

	if not exists(select * from md.Question where Name = 'Overnight Address')
	begin
		insert into md.Question (Id,Name,QuestionTypeId,QuestionGroupId) values (140,'Overnight Address',5,1)
	end
	go

	if not exists(select * from md.Question where Name = 'Asset')
	begin
		insert into md.Question (Id,Name,QuestionTypeId,QuestionGroupId) values (141,'Asset',6,2)
	end
	go

SET IDENTITY_INSERT md.Question OFF


-- Question Definition

	if not exists(select * from QuestionDefinition where DisplayName = 'Work Address')
	begin
		INSERT [dbo].[QuestionDefinition] ( [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex], [GroupTypeId])
		VALUES (0, N'Work Address', 96, 139, NULL, 1, 0, 1, 1, 0, N'{Title} {Name} would you please provide me with your work address?', N'', N'', 2, 3)
	end
	go


	if not exists(select * from QuestionDefinition where DisplayName = 'Overnight Address')
	begin
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex], [GroupTypeId]) 
		VALUES (0, N'Overnight Address', 96, 140, NULL, 1, 0, 1, 1, 0, N'{Title} {Name} would you please provide me with the overnight address?', N'', N'', 1, 1)
	end
	go

	if not exists(select * from QuestionDefinition where DisplayName = 'Asset')
	begin
	INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex], [GroupTypeId]) 
	VALUES (0, N'Asset', 96, 141, NULL, 1, 0, 1, 1, 0, N'I''d like to gather some information about the asset you would like to insure.', N'', N'^[0-9]+$', 1, 4)
	end
	go

	if exists(select * from QuestionDefinition where id = 1424)
	begin
		update QuestionDefinition set GroupIndex = 1, GroupTypeId = 1 where id = 1424
	end
	go






/**************************************/
--select qd.id,qd.DisplayName,VisibleIndex from QuestionDefinition qd 
--inner join md.Question q on q.id = qd.QuestionId
--where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 order by qd.VisibleIndex

declare  @count integer
set @count = 0

update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Asset'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Year Of Manufacture'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Make'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Model'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'MM Code'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Type'


set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Registration Number'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Class of Use'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Type of Cover'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Registered Owner ID Number'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Modified/Imported'

go


--select qd.id,qd.DisplayName,VisibleIndex from QuestionDefinition qd 
--inner join md.Question q on q.id = qd.QuestionId
--where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 order by qd.VisibleIndex


declare  @count integer
set @count = 0

update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Overnight Address'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Suburb'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Province'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Postal Code'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Work Address'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Suburb (Work)'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Postal Code (Work)'


/**************************************/

if not exists(select * from sys.columns where Name = N'QuestionDefinitionGroupTypeId' and Object_ID = Object_ID(N'QuestionDefinition'))
begin
	EXEC sp_RENAME 'QuestionDefinition.GroupTypeId' , 'QuestionDefinitionGroupTypeId', 'COLUMN'
end

if exists(select * from sys.columns where Name = N'GroupTypeId' and Object_ID = Object_ID(N'QuestionDefinition'))
begin
	alter table QuestionDefinition
	drop column GroupTypeId
end

/**************************************/
/**************************************/
GO


update QuestionDefinition set GroupIndex = 1, QuestionDefinitionGroupTypeId = 4 where CoverDefinitionId = 96 and QuestionId in (97,99,95,96,94,141)
update QuestionDefinition set GroupIndex = 1, QuestionDefinitionGroupTypeId = 1 where CoverDefinitionId = 96 and QuestionId in (140,47,89,90)
update QuestionDefinition set GroupIndex = 2, QuestionDefinitionGroupTypeId = 3 where CoverDefinitionId = 96 and QuestionId in (139,91,135)

if not exists(select * from QuestionDefinition where DisplayName = 'Asset' and CoverDefinitionId = 122)
begin
	INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) 
	VALUES (0, N'Asset', 122, 141, NULL, 1, 0, 1, 1, 0, N'I''d like to gather some information about the asset you would like to insure.', N'', N'^[0-9]+$', 1, 4)
	update QuestionDefinition set GroupIndex =1, QuestionDefinitionGroupTypeId = 4 where id in (1675,1673,1672,1671,1670)
end
go


if not exists(select * from QuestionDefinition where DisplayName = 'Asset' and CoverDefinitionId = 121)
begin
	INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) 
	VALUES (0, N'Asset', 121, 141, NULL, 1, 0, 1, 1, 0, N'I''d like to gather some information about the asset you would like to insure.', N'', N'^[0-9]+$', 1, 4)
	update QuestionDefinition set GroupIndex =1, QuestionDefinitionGroupTypeId = 4 where id in (1677,1678,1679,1680,1682)
end
go

if not exists(select * from QuestionDefinition where DisplayName = 'Asset' and CoverDefinitionId = 120)
begin
	INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) 
	VALUES (0, N'Asset', 120, 141, NULL, 1, 0, 1, 1, 0, N'I''d like to gather some information about the asset you would like to insure.', N'', N'^[0-9]+$', 1, 4)
	update QuestionDefinition set GroupIndex =1, QuestionDefinitionGroupTypeId = 4 where id in (1684,1685,1686,1687,1689)
end
go


if not exists(select * from QuestionDefinition where DisplayName = 'Asset' and CoverDefinitionId = 110)
begin
	INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) 
	VALUES (0, N'Asset', 110, 141, NULL, 1, 0, 1, 1, 0, N'I''d like to gather some information about the asset you would like to insure.', N'', N'^[0-9]+$', 1, 4)
	update QuestionDefinition set GroupIndex =1, QuestionDefinitionGroupTypeId = 4 where id in (1582,1583,1584,1585,1587)
end
go

if not exists(select * from QuestionDefinition where DisplayName = 'Asset' and CoverDefinitionId = 109)
begin
	INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) 
	VALUES (0, N'Asset', 109, 141, NULL, 1, 0, 1, 1, 0, N'I''d like to gather some information about the asset you would like to insure.', N'', N'^[0-9]+$', 1, 4)
	update QuestionDefinition set GroupIndex =1, QuestionDefinitionGroupTypeId = 4 where id in (1618,1619,1620,1621,1623)
end
go

declare  @count integer
set @count = 0

update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 122 and q.QuestionGroupId = 2 and DisplayName = 'Asset'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 122 and q.QuestionGroupId = 2 and DisplayName = 'Year Of Manufacture'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 122 and q.QuestionGroupId = 2 and DisplayName = 'Make'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 122 and q.QuestionGroupId = 2 and DisplayName = 'Model'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 122 and q.QuestionGroupId = 2 and DisplayName = 'MM Code'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 122 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Type'


set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 122 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Registration Number'

go


declare  @count integer
set @count = 0

update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 121 and q.QuestionGroupId = 2 and DisplayName = 'Asset'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 121 and q.QuestionGroupId = 2 and DisplayName = 'Year Of Manufacture'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 121 and q.QuestionGroupId = 2 and DisplayName = 'Make'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 121 and q.QuestionGroupId = 2 and DisplayName = 'Model'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 121 and q.QuestionGroupId = 2 and DisplayName = 'MM Code'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 121 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Type'


set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 121 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Registration Number'

go

declare  @count integer
set @count = 0

update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 120 and q.QuestionGroupId = 2 and DisplayName = 'Asset'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 120 and q.QuestionGroupId = 2 and DisplayName = 'Year Of Manufacture'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 120 and q.QuestionGroupId = 2 and DisplayName = 'Make'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 120 and q.QuestionGroupId = 2 and DisplayName = 'Model'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 120 and q.QuestionGroupId = 2 and DisplayName = 'MM Code'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 120 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Type'


set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 120 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Registration Number'

go

declare  @count integer
set @count = 0

update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 110 and q.QuestionGroupId = 2 and DisplayName = 'Asset'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 110 and q.QuestionGroupId = 2 and DisplayName = 'Year Of Manufacture'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 110 and q.QuestionGroupId = 2 and DisplayName = 'Make'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 110 and q.QuestionGroupId = 2 and DisplayName = 'Model'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 110 and q.QuestionGroupId = 2 and DisplayName = 'MM Code'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 110 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Type'


set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 110 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Registration Number'

go

declare  @count integer
set @count = 0

update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 109 and q.QuestionGroupId = 2 and DisplayName = 'Asset'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 109 and q.QuestionGroupId = 2 and DisplayName = 'Year Of Manufacture'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 109 and q.QuestionGroupId = 2 and DisplayName = 'Make'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 109 and q.QuestionGroupId = 2 and DisplayName = 'Model'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 109 and q.QuestionGroupId = 2 and DisplayName = 'MM Code'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 109 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Type'


set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 109 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Registration Number'

go
