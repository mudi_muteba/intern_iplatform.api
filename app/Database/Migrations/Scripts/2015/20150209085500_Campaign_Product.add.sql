-- Remove Cover from Campaign
IF EXISTS(SELECT *FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'Campaign' AND COLUMN_NAME = 'CoverId') 
BEGIN				 
	alter table [dbo].[Campaign]
	drop column CoverId
END

IF NOT EXISTS(SELECT *FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'CampaignProduct') 
BEGIN				 
	create table CampaignProduct (
        Id INT IDENTITY NOT NULL,
       CampaignId INT null,
       ProductId INT null,
       primary key (Id)
    )

	alter table CampaignProduct 
        add constraint FK_Campaign_Product 
        foreign key (CampaignId) 
        references Campaign

    alter table CampaignProduct 
        add constraint FK_Product_Campaign 
        foreign key (ProductId) 
        references Product

END




