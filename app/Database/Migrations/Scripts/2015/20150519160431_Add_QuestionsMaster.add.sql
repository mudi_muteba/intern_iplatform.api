﻿----------------------------------------------------------------------
DECLARE @id INT
DECLARE @idPaintType INT
DECLARE @questionType INT = 3
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Colour', @questionType, 2) -- Risk Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Colour - Beige','Beige', @id, 0),
		('Vehicle Colour - Black','Black', @id, 1),
		('Vehicle Colour - Blue','Blue', @id, 2),
		('Vehicle Colour - Blue/Green','Blue/Green', @id, 3),
		('Vehicle Colour - Bronze','Bronze', @id, 4),
		('Vehicle Colour - Brown','Brown', @id, 5),
		('Vehicle Colour - Burgundy','Burgundy', @id, 6),
		('Vehicle Colour - Champagne','Champagne', @id, 7),
		('Vehicle Colour - Cream','Cream', @id, 8),
		('Vehicle Colour - Gold','Gold', @id, 9),
		('Vehicle Colour - Gold/Silver','Gold/Silver', @id, 10),
		('Vehicle Colour - Green','Green', @id, 11),
		('Vehicle Colour - Grey','Grey', @id, 12),
		('Vehicle Colour - Grey/Black','Grey/Black', @id, 13),
		('Vehicle Colour - Maroon','Maroon', @id, 14),
		('Vehicle Colour - Orange','Orange', @id, 15),
		('Vehicle Colour - Pink','Pink', @id, 16),
		('Vehicle Colour - Pink/Red','Pink/Red', @id, 17),
		('Vehicle Colour - Purple','Purple', @id, 18),
		('Vehicle Colour - Red','Red', @id, 19),
		('Vehicle Colour - Silver','Silver', @id, 20),
		('Vehicle Colour - Tan','Tan', @id, 21),
		('Vehicle Colour - Tan/Brown','Tan/Brown', @id, 22),
		('Vehicle Colour - White','White', @id, 23),
		('Vehicle Colour - White/Cream','White/Cream', @id, 24),
		('Vehicle Colour - Yellow','Yellow', @id, 25),
		('Vehicle Colour - Yellow/Orange','Yellow/Orange', @id, 26)


INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Paint Type', @questionType, 2) -- Risk Information

SET @idPaintType = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Paint Type - Metallic','Metallic', @idPaintType, 0),
		('Vehicle Paint Type - Pearl','Pearl', @idPaintType, 1),
		('Vehicle Paint Type - Plain','Plain', @idPaintType, 2),
		('Vehicle Paint Type - Wrapped Multi Colour','Wrapped Multi Colour', @idPaintType, 3),
		('Vehicle Paint Type - Wrapped Single Colour','Wrapped Single Colour', @idPaintType, 4)


DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')
DECLARE @CoverID INT 
SET @CoverID=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND coverid=218)

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Vehicle Colour' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          32 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the colour of the vehicle?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Vehicle Paint Type' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @idPaintType , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          33 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the type of paint on the vehicle?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )