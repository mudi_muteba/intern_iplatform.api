﻿declare @questionId int
select @questionId = id from md.Question where name = 'Year Of Manufacture'

update md.Question set QuestionTypeId = 4 where name = 'Year Of Manufacture'


declare @questionDefinitionId int 
select @questionDefinitionId = id from QuestionDefinition where questionId = @questionId and CoverDefinitionId = 96

update ProposalQuestionAnswer set QuestionTypeId = 4 where QuestionDefinitionId = @questionDefinitionId