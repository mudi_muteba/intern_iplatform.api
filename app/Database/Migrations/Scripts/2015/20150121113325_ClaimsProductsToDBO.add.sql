﻿
UPDATE md.ClaimsQuestionDefinition SET ClaimsTypeId=11 WHERE ClaimsTypeId=0
Go

ALTER TABLE md.ClaimsQuestionDefinition ADD DisplayName NVARCHAR(255)
Go

UPDATE md.ClaimsQuestionDefinition SET Displayname=Name
Go

UPDATE md.ClaimsQuestionDefinition SET Name = 'Motor Theft_' + Name WHERE ClaimsTypeId = 1
UPDATE md.ClaimsQuestionDefinition SET Name = 'Motor Accident_' + Name WHERE ClaimsTypeId = 2
UPDATE md.ClaimsQuestionDefinition SET Name = 'Public Liability_' + Name WHERE ClaimsTypeId = 3
UPDATE md.ClaimsQuestionDefinition SET Name = 'Personal Liability_' + Name WHERE ClaimsTypeId = 4
UPDATE md.ClaimsQuestionDefinition SET Name = 'Personal Accident_' + Name WHERE ClaimsTypeId = 5
UPDATE md.ClaimsQuestionDefinition SET Name = 'Theft Of Property Out Of Motor_' + Name WHERE ClaimsTypeId = 6
UPDATE md.ClaimsQuestionDefinition SET Name = 'Non-Motor Claims Up To R10 000_' + Name WHERE ClaimsTypeId = 7
UPDATE md.ClaimsQuestionDefinition SET Name = 'Property Loss Damage_' + Name WHERE ClaimsTypeId = 8
UPDATE md.ClaimsQuestionDefinition SET Name = 'Marine GIT_' + Name WHERE ClaimsTypeId = 9
UPDATE md.ClaimsQuestionDefinition SET Name = 'Pleasure Craft_' + Name WHERE ClaimsTypeId = 10
UPDATE md.ClaimsQuestionDefinition SET Name = 'Contract Works_' + Name WHERE ClaimsTypeId = 11

Go
