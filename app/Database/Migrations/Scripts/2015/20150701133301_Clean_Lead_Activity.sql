DECLARE @ConstraintName nvarchar(200)
SELECT @ConstraintName = Name FROM SYS.DEFAULT_CONSTRAINTS
WHERE PARENT_OBJECT_ID = OBJECT_ID('LeadActivity')
AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns
                        WHERE NAME = N'Priority'
                        AND object_id = OBJECT_ID(N'LeadActivity'))
IF @ConstraintName IS NOT NULL
EXEC('ALTER TABLE LeadActivity DROP CONSTRAINT ' + @ConstraintName)

GO

DECLARE @ConstraintName nvarchar(200)
SELECT @ConstraintName = Name FROM SYS.DEFAULT_CONSTRAINTS
WHERE PARENT_OBJECT_ID = OBJECT_ID('LeadActivity')
AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns
                        WHERE NAME = N'Selected'
                        AND object_id = OBJECT_ID(N'LeadActivity'))
IF @ConstraintName IS NOT NULL
EXEC('ALTER TABLE LeadActivity DROP CONSTRAINT ' + @ConstraintName)

GO

DECLARE @ConstraintName nvarchar(200)
SELECT @ConstraintName = Name FROM SYS.DEFAULT_CONSTRAINTS
WHERE PARENT_OBJECT_ID = OBJECT_ID('LeadActivity')
AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns
                        WHERE NAME = N'ProposalBlob'
                        AND object_id = OBJECT_ID(N'LeadActivity'))
IF @ConstraintName IS NOT NULL
EXEC('ALTER TABLE LeadActivity DROP CONSTRAINT ' + @ConstraintName)

GO

DECLARE @ConstraintName nvarchar(200)
SELECT @ConstraintName = Name FROM SYS.DEFAULT_CONSTRAINTS
WHERE PARENT_OBJECT_ID = OBJECT_ID('LeadActivity')
AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns
                        WHERE NAME = N'LeadStatusId'
                        AND object_id = OBJECT_ID(N'LeadActivity'))
IF @ConstraintName IS NOT NULL
EXEC('ALTER TABLE LeadActivity DROP CONSTRAINT ' + @ConstraintName)

GO
DECLARE @ConstraintName nvarchar(200)
SELECT @ConstraintName = Name FROM SYS.DEFAULT_CONSTRAINTS
WHERE PARENT_OBJECT_ID = OBJECT_ID('LeadActivity')
AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns
                        WHERE NAME = N'ReminderId'
                        AND object_id = OBJECT_ID(N'LeadActivity'))
IF @ConstraintName IS NOT NULL
EXEC('ALTER TABLE LeadActivity DROP CONSTRAINT ' + @ConstraintName)

GO
DECLARE @ConstraintName nvarchar(200)
SELECT @ConstraintName = Name FROM SYS.DEFAULT_CONSTRAINTS
WHERE PARENT_OBJECT_ID = OBJECT_ID('LeadActivity')
AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns
                        WHERE NAME = N'Method'
                        AND object_id = OBJECT_ID(N'LeadActivity'))
IF @ConstraintName IS NOT NULL
EXEC('ALTER TABLE LeadActivity DROP CONSTRAINT ' + @ConstraintName)

GO

if exists(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_LeadActivity_LeadStatus')
begin
	alter table LeadActivity
	drop constraint FK_LeadActivity_LeadStatus
end

if exists(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_LeadActivity_Reminder')
begin
	alter table LeadActivity
	drop constraint FK_LeadActivity_Reminder
end


if exists(select * from sys.columns where Name = N'Priority' and Object_ID = Object_ID(N'LeadActivity'))
begin
	alter table LeadActivity
	drop column Priority,Selected,ProposalBlob,LeadStatusId,ReminderId,Method
end

GO


IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo'AND  TABLE_NAME = 'DelayLeadActivity'))
BEGIN
CREATE TABLE [dbo].[DelayLeadActivity](
	[Id] [bigint] primary key NOT NULL)
END


IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo'AND  TABLE_NAME = 'LossLeadActivity'))
BEGIN
CREATE TABLE [dbo].[LossLeadActivity](
	[Id] [bigint] primary key NOT NULL)
END

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo'AND  TABLE_NAME = 'DeadLeadActivity'))
BEGIN
CREATE TABLE [dbo].[DeadLeadActivity](
	[Id] [bigint] primary key NOT NULL)
END

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo'AND  TABLE_NAME = 'QuotedLeadActivity'))
BEGIN
CREATE TABLE [dbo].[QuotedLeadActivity](
	[Id] [bigint] primary key NOT NULL)
END

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo'AND  TABLE_NAME = 'ImportedLeadActivity'))
BEGIN
CREATE TABLE [dbo].[ImportedLeadActivity](
	[Id] [bigint] primary key NOT NULL)
END


IF NOT EXISTS (select * from md.LeadStatus where id = 7)
BEGIN
	update md.LeadStatus set Name = 'Created' where id = 1
	update md.LeadStatus set Name = 'Proposal' where id = 2
	update md.LeadStatus set Name = 'Delayed' where id = 3
	update md.LeadStatus set Name = 'Loss' where id = 4
	update md.LeadStatus set Name = 'Quoted' where id = 6
	update md.LeadStatus set Name = 'Dead' where id = 5

	SET IDENTITY_INSERT [md].[LeadStatus] ON 
	INSERT [md].[LeadStatus] ([Id], [Name], [VisibleIndex]) VALUES (7, N'QuoteAccepted', 6)
	INSERT [md].[LeadStatus] ([Id], [Name], [VisibleIndex]) VALUES (8, N'Imported', 7)
	SET IDENTITY_INSERT [md].[LeadStatus] OFF

	alter table Lead
	add LeadStatusId int null

	CONSTRAINT FK_Lead_LeadStatus FOREIGN KEY (LeadStatusId) 
	REFERENCES md.LeadStatus (Id)
END

alter table ProposalLeadActivity
alter column ProposalHeaderId int null

