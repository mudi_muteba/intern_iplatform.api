﻿SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT * FROM sys.objects WHERE name = 'vw_rating_configuration' AND TYPE = 'V')
	BEGIN
		DROP VIEW [vw_rating_configuration]
	END
GO

CREATE VIEW [vw_rating_configuration]
AS
SELECT
	   r.Id AS ChannelId 
	  ,r.ChannelId AS ChannelSystemId
	  ,r.ProductId
	  ,[Password] AS RatingPassword
      ,[AgentCode] AS RatingAgentCode
      ,[BrokerCode] AS RatingBrokerCode
      ,[AuthCode] AS RatingAuthCode
      ,[SchemeCode] AS RatingSchemeCode
      ,[Token] AS RatingToken
      ,r.[Environment] AS RatingEnvironment
      ,[UserId] AS RatingUserId
      ,r.[SubscriberCode] AS RatingSubscriberCode
      ,[CompanyCode] AS RatingCompanyCode
      ,[UwCompanyCode] AS RatingUwCompanyCode
      ,[UwProductCode] AS RatingUwProductCode
	  ,[CheckRequired] AS ITCCheckRequired
      ,p.[SubscriberCode] AS ITCSubscriberCode
      ,[BranchNumber] AS ITCBranchNumber
      ,[BatchNumber] AS ITCBatchNumber
      ,[SecurityCode] AS ITCSecurityCode
      ,[EnquirerContactName] AS ITCEnquirerContactName
      ,[EnquirerContactPhoneNo] AS ITCEnquirerContactPhoneNo
      ,p.[Environment] AS ITCEnvironment
	  ,o.Code As InsurerCode
	  ,product.ProductCode AS ProductCode
	  ,r.IsDeleted
FROM SettingsiRate r
	INNER JOIN Channel c
		ON r.ChannelId = c.SystemId
	INNER JOIN Product product
		ON r.ProductId = product.Id
	INNER JOIN Organization o
		ON product.ProductOwnerId = o.PartyId
	LEFT JOIN SettingsiPerson p
		ON r.ChannelId = p.ChannelId
WHERE r.IsDeleted = 0
	AND c.IsActive = 1


GO