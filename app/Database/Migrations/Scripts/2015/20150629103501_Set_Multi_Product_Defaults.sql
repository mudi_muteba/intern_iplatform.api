
declare @questionIds table(id int)

insert into @questionIds values  (151),(152), (87), (80), (85), (86), (77), (74), (63),
 (64), (72), (60), (164), (149), (150), (43), (44), (45), (46), (53), (48), (69)

	declare @answer nvarchar(100)


	update qd set DefaultValue = m.Id
	from QuestionDefinition qd 
	inner join @questionIds qi on qi.id = qd.QuestionId
	cross apply
        (
        select  top 1 *
        from    md.QuestionAnswer m
        where   m.QuestionId = qd.QuestionId
        order by
               VisibleIndex
        ) m
	where qd.CoverDefinitionId = 96


	update qd set DefaultValue = 515
	from QuestionDefinition qd 
	where qd.CoverDefinitionId = 96 and qd.QuestionId = 142

	update qd set DefaultValue = 521
	from QuestionDefinition qd 
	where qd.CoverDefinitionId = 96 and qd.QuestionId = 143

	update qd set DefaultValue = 598
	from QuestionDefinition qd 
	where qd.CoverDefinitionId = 96 and qd.QuestionId = 153

	update qd set DefaultValue = 190
	from QuestionDefinition qd 
	where qd.CoverDefinitionId = 96 and qd.QuestionId = 73

