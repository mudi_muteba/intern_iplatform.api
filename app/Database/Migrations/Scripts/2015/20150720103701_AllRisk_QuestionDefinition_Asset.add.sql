if not exists (select * from [md].[QuestionDefinitionGroupType] where Id = 6)
begin
	INSERT INTO [md].[QuestionDefinitionGroupType] ([Id],[Name])VALUES(6,'All Risk Asset')
end

GO

if not exists (select * from QuestionDefinition  where CoverDefinitionId = 99 and questionId = 141)
begin
	INSERT INTO [dbo].[QuestionDefinition]
			   ([MasterId]
			   ,[DisplayName]
			   ,[CoverDefinitionId]
			   ,[QuestionId]
			   ,[ParentId]
			   ,[QuestionDefinitionTypeId]
			   ,[VisibleIndex]
			   ,[RequiredForQuote]
			   ,[RatingFactor]
			   ,[ReadOnly]
			   ,[ToolTip]
			   ,[DefaultValue]
			   ,[RegexPattern]
			   ,[GroupIndex]
			   ,[QuestionDefinitionGroupTypeId])
		 VALUES
			   (0
			   ,'Risk Item'
			   ,99
			   ,141
			   ,null
			   ,1
			   ,0
			   ,1
			   ,1
			   ,0
			   ,'Which risk item would you like to insure?'
			   ,''
			   ,''
			   ,0
			   ,6)

end

update QuestionDefinition set QuestionDefinitionGroupTypeId = 6, GroupIndex = 0, VisibleIndex = 1  where CoverDefinitionId = 99 and questionId = 126
update QuestionDefinition set QuestionDefinitionGroupTypeId = 6, GroupIndex = 0, VisibleIndex = 2 where CoverDefinitionId = 99 and questionId = 127
update QuestionDefinition set QuestionDefinitionGroupTypeId = 6, GroupIndex = 0, VisibleIndex = 3 where CoverDefinitionId = 99 and questionId = 128

GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='AssetRiskItem') 
BEGIN
	CREATE TABLE [dbo].[AssetRiskItem](
		[Id] [int] NOT NULL,
		[SerialIMEINumber] [nvarchar](50) null,
		[AllRiskCategory] [int] null
	 CONSTRAINT [AssetRiskItem_Id] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[AssetRiskItem]  WITH CHECK ADD  CONSTRAINT [AssetRiskItem_Asset] FOREIGN KEY([Id])
	REFERENCES [dbo].[Asset] ([Id])

	ALTER TABLE [dbo].[AssetRiskItem] CHECK CONSTRAINT [AssetRiskItem_Asset]
END