if not exists(select * from sys.columns where Name = N'IsDeleted' and Object_ID = Object_ID(N'AssetExtras'))
begin
	alter table [dbo].[AssetExtras]
	add IsDeleted bit default(0) not null
end