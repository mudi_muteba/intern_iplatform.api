﻿if NOT exists (select * from dbo.sysobjects where id = object_id(N'SettingsiPerson') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    create table SettingsiPerson (
    [Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[Required] [bit] NOT NULL,
	[SubscriberCode] [nvarchar](50) NOT NULL,
	[BranchNumber] [nvarchar](20) NOT NULL,
	[BatchNumber] [nvarchar](20) NOT NULL,
	[SecurityCode] [nvarchar](50) NOT NULL,
	[EnquirerContactName] [nvarchar](50) NOT NULL,
	[EnquirerContactPhoneNo] [nvarchar](50) NOT NULL,
	CONSTRAINT [PK_SettingsiPerson] PRIMARY KEY CLUSTERED 
	(	[Id] ASC )) ON [PRIMARY]
END

if NOT exists (select * from dbo.sysobjects where id = object_id(N'SettingsiRate') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    create table SettingsiRate (
    [Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[AgentCode] [nvarchar](50) NULL,
	[BrokerCode] [nvarchar](50) NULL,
	[AuthCode] [nvarchar](50) NULL,
	[SchemeCode] [nvarchar](50) NULL,
	[Token] [nvarchar](50) NULL,
	CONSTRAINT [PK_SettingsiRate] PRIMARY KEY CLUSTERED 
	(	[Id] ASC)) ON [PRIMARY]
END