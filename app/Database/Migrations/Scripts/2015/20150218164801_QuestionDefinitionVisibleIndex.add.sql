--select qd.id,qd.DisplayName,VisibleIndex from QuestionDefinition qd 
--inner join md.Question q on q.id = qd.QuestionId
--where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 order by qd.VisibleIndex

declare  @count integer
set @count = 0

update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Asset'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Year Of Manufacture'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Make'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Model'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'MM Code'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Type'


set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Vehicle Registration Number'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Class of Use'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Type of Cover'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Registered Owner ID Number'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 2 and DisplayName = 'Modified/Imported'

go


--select qd.id,qd.DisplayName,VisibleIndex from QuestionDefinition qd 
--inner join md.Question q on q.id = qd.QuestionId
--where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 order by qd.VisibleIndex


declare  @count integer
set @count = 0

update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Overnight Address'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Suburb'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Province'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Postal Code'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Work Address'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Suburb (Work)'

set @count = @count + 1
update qd set qd.VisibleIndex = @count from QuestionDefinition qd 
inner join md.Question q on q.id = qd.QuestionId
where  qd.CoverDefinitionId = 96 and q.QuestionGroupId = 1 and DisplayName = 'Postal Code (Work)'