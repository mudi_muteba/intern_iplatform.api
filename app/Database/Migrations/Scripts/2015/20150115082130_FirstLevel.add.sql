﻿if NOT exists (select * from dbo.sysobjects where id = object_id(N'FirstLevelQuestion') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
	CREATE TABLE [dbo].[FirstLevelQuestion](
       [Id] [int] IDENTITY(1,1) NOT NULL,
       [Name] [nvarchar](100) NOT NULL,
       [QuestionTypeId] [int] NOT NULL,
	CONSTRAINT [PK_FirstLevelQuestion] PRIMARY KEY CLUSTERED 
	(
		   [Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
End
Go

if NOT exists (select * from dbo.sysobjects where id = object_id(N'FirstLevelQuestionDefinition') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
	CREATE TABLE [dbo].[FirstLevelQuestionDefinition](
       [Id] [int] IDENTITY(1,1) NOT NULL,
       [FirstLevelQuestionID] [int] NOT NULL,
       [ProductID] [int] NOT NULL,
       [CampaignID] [int] NULL,
       [LowerBoundVal] [decimal](18, 4) NULL,
       [UpperBoundVal] [decimal](18, 4) NULL,
       [YesNo] [bit] NULL,
       [Deleted] [bit] NOT NULL,
	CONSTRAINT [PK_FirstLevelQuestionDefinition] PRIMARY KEY CLUSTERED 
	(
		   [Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
End
Go
