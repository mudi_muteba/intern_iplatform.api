﻿if not exists(select * from sys.columns where Name = N'RequestId' and Object_ID = Object_ID(N'Quote'))
begin
	alter table Quote
	add RequestId UNIQUEIDENTIFIER  NULL
end