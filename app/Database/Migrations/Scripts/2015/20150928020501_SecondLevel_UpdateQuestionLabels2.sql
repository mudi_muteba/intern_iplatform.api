UPDATE SecondLevelQuestionDefinition
SET DisplayName = 'Have you ever been, or are you currently under any defaults or judgements'
WHERE SecondLevelQuestionId = 3

UPDATE SecondLevelQuestionDefinition
SET DisplayName = 'Have you ever been, or are you currently under and insolvency, sequestration or liquidation orders'
WHERE SecondLevelQuestionId = 4