﻿SET QUOTED_IDENTIFIER ON
GO

if exists(select * from sys.objects where name = 'vw_product_allocation' and type = 'V')
	begin
		drop view dbo.vw_product_allocation
	end
go
