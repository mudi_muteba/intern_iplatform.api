﻿
if ((select QuestionTypeId from md.Question where name = 'Year Of Manufacture') != 3)
begin


update md.Question set QuestionTypeId = 3 where name = 'Year Of Manufacture'

declare @questionId int
select @questionId = id from md.Question where name = 'Year Of Manufacture'

INSERT INTO [md].[QuestionAnswer]
VALUES
           ('Year Of Manufacture - 1980','1980',@questionId,0),
           ('Year Of Manufacture - 1981','1981',@questionId,1),
           ('Year Of Manufacture - 1982','1982',@questionId,2),
           ('Year Of Manufacture - 1983','1983',@questionId,3),
           ('Year Of Manufacture - 1984','1984',@questionId,4),
           ('Year Of Manufacture - 1985','1985',@questionId,5),
           ('Year Of Manufacture - 1986','1986',@questionId,6),
           ('Year Of Manufacture - 1987','1987',@questionId,7),
           ('Year Of Manufacture - 1989','1989',@questionId,8),
           ('Year Of Manufacture - 1990','1990',@questionId,9),
           ('Year Of Manufacture - 1991','1991',@questionId,10),
           ('Year Of Manufacture - 1992','1992',@questionId,11),
           ('Year Of Manufacture - 1993','1993',@questionId,12),
           ('Year Of Manufacture - 1994','1994',@questionId,13),
           ('Year Of Manufacture - 1995','1995',@questionId,14),
           ('Year Of Manufacture - 1996','1996',@questionId,15),
           ('Year Of Manufacture - 1997','1997',@questionId,16),
           ('Year Of Manufacture - 1998','1998',@questionId,17),
           ('Year Of Manufacture - 1999','1999',@questionId,18),
           ('Year Of Manufacture - 2000','2000',@questionId,19),
           ('Year Of Manufacture - 2001','2001',@questionId,20),
           ('Year Of Manufacture - 2002','2002',@questionId,21),
           ('Year Of Manufacture - 2003','2003',@questionId,22),
           ('Year Of Manufacture - 2004','2004',@questionId,23),
           ('Year Of Manufacture - 2005','2005',@questionId,24),
           ('Year Of Manufacture - 2006','2006',@questionId,25),
           ('Year Of Manufacture - 2007','2007',@questionId,26),
           ('Year Of Manufacture - 2008','2008',@questionId,27),
           ('Year Of Manufacture - 2009','2009',@questionId,28),
		   ('Year Of Manufacture - 2010','2010',@questionId,29),
		   ('Year Of Manufacture - 2011','2011',@questionId,30),
		   ('Year Of Manufacture - 2012','2012',@questionId,31),
		   ('Year Of Manufacture - 2013','2013',@questionId,32),
		   ('Year Of Manufacture - 2014','2014',@questionId,33),
		   ('Year Of Manufacture - 2015','2015',@questionId,34)
	end	  