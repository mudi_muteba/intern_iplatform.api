﻿if not exists(select * from sys.columns where Name = N'IsDeleted' and Object_ID = Object_ID(N'Asset'))
begin
	alter table Asset
	add IsDeleted bit default 0 not null

	print 'ADDED IsDeleted'
end

if not exists(select * from sys.columns where Name = N'SumInsured' and Object_ID = Object_ID(N'Asset'))
begin
	alter table Asset
	add SumInsured decimal(18,2) null

	print 'ADDED SumInsured'
end

if not exists(select * from sys.columns where Name = N'VehicleType' and Object_ID = Object_ID(N'AssetVehicle'))
begin
	alter table AssetVehicle
	add VehicleTypeId int null

	print 'ADDED VehicleType'
end


if not exists(select * from sys.all_objects where  Object_ID = Object_ID(N'md.VehicleType'))
begin

	CREATE TABLE [md].[VehicleType](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Name] [varchar](50) NOT NULL,
		[Code] [varchar](30) NULL,
		[VisibleIndex] [int] NOT NULL)

	INSERT INTO [md].[VehicleType] values ('Sedan','Sedan',0)
	INSERT INTO [md].[VehicleType] values ('Motorcycle','Motorcycle',1)

end