update md.Question set QuestionTypeId = 5 where id = 88
update ProposalQuestionAnswer set QuestionTypeId = 5 where QuestionDefinitionId in(
select id from QuestionDefinition where CoverDefinitionId in (99) and questionId in (88))


if not exists (select * from QuestionDefinition  where CoverDefinitionId = 99 and questionId = 47)
begin
	INSERT INTO [dbo].[QuestionDefinition]
			   ([MasterId]
			   ,[DisplayName]
			   ,[CoverDefinitionId]
			   ,[QuestionId]
			   ,[ParentId]
			   ,[QuestionDefinitionTypeId]
			   ,[VisibleIndex]
			   ,[RequiredForQuote]
			   ,[RatingFactor]
			   ,[ReadOnly]
			   ,[ToolTip]
			   ,[DefaultValue]
			   ,[RegexPattern]
			   ,[GroupIndex]
			   ,[QuestionDefinitionGroupTypeId])
		 VALUES
			   (0
			   ,'Province'
			   ,99
			   ,47
			   ,null
			   ,1
			   ,3
			   ,1
			   ,1
			   ,0
			   ,'In which province is the risk item is located?'
			   ,''
			   ,''
			   ,0
			   ,2)
end



update QuestionDefinition set QuestionDefinitionGroupTypeId = 2, GroupIndex = 0, VisibleIndex = 0  where CoverDefinitionId = 99 and questionId = 88
update QuestionDefinition set QuestionDefinitionGroupTypeId = 2, GroupIndex = 0, VisibleIndex = 1 where CoverDefinitionId = 99 and questionId = 89
update QuestionDefinition set QuestionDefinitionGroupTypeId = 2, GroupIndex = 0, VisibleIndex = 2 where CoverDefinitionId = 99 and questionId = 90
update QuestionDefinition set QuestionDefinitionGroupTypeId = 2, GroupIndex = 0, VisibleIndex = 3 where CoverDefinitionId = 99 and questionId = 47