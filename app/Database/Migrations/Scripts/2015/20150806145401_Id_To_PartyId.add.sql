﻿IF Not EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'PartyId' AND Object_ID = Object_ID(N'Individual'))
BEGIN
EXEC sp_RENAME 'Individual.Id' , 'PartyId', 'COLUMN'
END

IF Not EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'PartyId' AND Object_ID = Object_ID(N'Organization'))
BEGIN
	EXEC sp_RENAME 'Organization.Id' , 'PartyId', 'COLUMN'
END