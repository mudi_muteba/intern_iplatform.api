﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170126074900)]
    public class Update_SP_Reports_AIG_ContentExtract_PerformanceV2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170126074900_Update_SP_Reports_AIG_ContentExtract_PerformanceV2.sql");
        }

        public override void Down()
        {

        }
    }
}