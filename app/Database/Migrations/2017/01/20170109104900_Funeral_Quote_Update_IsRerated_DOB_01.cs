﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20170109104900)]
    public class Funeral_Quote_Update_IsRerated_DOB_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170109104900_Funeral_Quote_Update_IsRerated_DOB_01.sql");
        }

        public override void Down()
        {

        }
    }
}