﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170126102730)]
    public class Remove_Report_Schedule_Individual_Owner_Ref : Migration
    {
        public override void Up()
        {
            if (Schema.Table("ReportSchedule").Constraint("FK_ReportSchedule_Individual_OwnerId").Exists())
                Delete.ForeignKey("FK_ReportSchedule_Individual_OwnerId").OnTable("ReportSchedule");
        }

        public override void Down()
        {

        }
    }
}