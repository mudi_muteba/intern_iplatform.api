using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170126151800)]
    public class Detailed_Insurer_Upload_Report_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170126151800_Detailed_Insurer_Upload_Report_01.sql");
        }

        public override void Down()
        {

        }
    }
}