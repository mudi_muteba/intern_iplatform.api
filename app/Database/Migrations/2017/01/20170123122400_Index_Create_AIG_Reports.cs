﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170123122400)]
    public class Index_Create_AIG_Reports : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170123122400_Index_Create_AIG_Reports.sql");
        }

        public override void Down()
        {

        }
    }
}