using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170131110900)]
    public class Update_KingPrice_Dev_Urls : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170131110900_Update_KingPrice_Dev_Urls.sql");
        }

        public override void Down()
        {

        }
    }
}