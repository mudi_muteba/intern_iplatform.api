using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170129224130)]
    public class Report_Schedule_Unique_Constraint : Migration
    {
        public override void Up()
        {
            Alter.Column("Name").OnTable("ReportSchedule").AsString().NotNullable();
            Create.UniqueConstraint("UQ_ReportSchedule_Name").OnTable("ReportSchedule").Column("Name");
        }

        public override void Down()
        {

        }
    }
}