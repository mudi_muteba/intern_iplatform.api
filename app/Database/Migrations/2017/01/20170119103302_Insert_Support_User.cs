using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170119103302)]
    public class Insert_Support_User : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170119103302_Insert_Support_User.sql");
        }

        public override void Down()
        {

        }
    }
}