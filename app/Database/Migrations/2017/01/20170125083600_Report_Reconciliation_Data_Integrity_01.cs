using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170125083600)]
    public class Report_Reconciliation_Data_Integrity_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170125083600_Report_Reconciliation_Data_Integrity_01.sql");
        }

        public override void Down()
        {

        }
    }
}