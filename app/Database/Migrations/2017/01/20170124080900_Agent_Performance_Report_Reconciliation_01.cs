using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170124080900)]
    public class Agent_Performance_Report_Reconciliation_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170124080900_Agent_Performance_Report_Reconciliation_01.sql");
        }

        public override void Down()
        {

        }
    }
}