﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170123120700)]
    public class Add_Setting_Authorisation : Migration
    {
        public override void Up()
        {
            //SettingIrate
            Delete.FromTable(Tables.AuthorisationGroupPoint)
            .Row(new { AuthorisationPointId = 71, AuthorisationGroupId = 5 })
            .Row(new { AuthorisationPointId = 72, AuthorisationGroupId = 5 })
            .Row(new { AuthorisationPointId = 73, AuthorisationGroupId = 5 })
            .Row(new { AuthorisationPointId = 74, AuthorisationGroupId = 5 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
            .Row(new { AuthorisationPointId = 71, AuthorisationGroupId = 7 })
            .Row(new { AuthorisationPointId = 72, AuthorisationGroupId = 7 })
            .Row(new { AuthorisationPointId = 73, AuthorisationGroupId = 7 })
            .Row(new { AuthorisationPointId = 74, AuthorisationGroupId = 7 });


            //SettingiPerson
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 29, Name = "SettingsIperson" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                .Row(new { Id = 94, Name = "Create", AuthorisationPointCategoryId = 29 })
                .Row(new { Id = 95, Name = "Edit", AuthorisationPointCategoryId = 29 })
                .Row(new { Id = 96, Name = "Delete", AuthorisationPointCategoryId = 29 })
                .Row(new { Id = 97, Name = "List", AuthorisationPointCategoryId = 29 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 94, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 95, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 96, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 97, AuthorisationGroupId = 7 });

            //SettingsQuoteUpload
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 30, Name = "SettingsQuoteUpload" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                .Row(new { Id = 98, Name = "Create", AuthorisationPointCategoryId = 30 })
                .Row(new { Id = 99, Name = "Edit", AuthorisationPointCategoryId = 30 })
                .Row(new { Id = 100, Name = "Delete", AuthorisationPointCategoryId = 30 })
                .Row(new { Id = 101, Name = "List", AuthorisationPointCategoryId = 30 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 98, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 99, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 100, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 101, AuthorisationGroupId = 7 });

            //EmailCommunicationSettings
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 31, Name = "EmailCommunicationSetting" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                .Row(new { Id = 102, Name = "Create", AuthorisationPointCategoryId = 31 })
                .Row(new { Id = 103, Name = "Edit", AuthorisationPointCategoryId = 31 })
                .Row(new { Id = 104, Name = "Delete", AuthorisationPointCategoryId = 31 })
                .Row(new { Id = 105, Name = "List", AuthorisationPointCategoryId = 31 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 102, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 103, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 104, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 105, AuthorisationGroupId = 7 });

            //CommunicationSetting
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 32, Name = "CommunicationSetting" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                .Row(new { Id = 106, Name = "Create", AuthorisationPointCategoryId = 32 })
                .Row(new { Id = 107, Name = "Edit", AuthorisationPointCategoryId = 32 })
                .Row(new { Id = 108, Name = "Delete", AuthorisationPointCategoryId = 32 })
                .Row(new { Id = 109, Name = "List", AuthorisationPointCategoryId = 32 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 106, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 107, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 108, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 109, AuthorisationGroupId = 7 });

            //Product Fee
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 75, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 76, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 77, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 78, AuthorisationGroupId = 5 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 75, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 76, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 77, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 78, AuthorisationGroupId = 7 });

            //VehicleGuideSettings
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 33, Name = "VehicleGuideSetting" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                .Row(new { Id = 110, Name = "Create", AuthorisationPointCategoryId = 33 })
                .Row(new { Id = 111, Name = "Edit", AuthorisationPointCategoryId = 33 })
                .Row(new { Id = 112, Name = "Delete", AuthorisationPointCategoryId = 33 })
                .Row(new { Id = 113, Name = "List", AuthorisationPointCategoryId = 33 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 110, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 111, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 112, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 113, AuthorisationGroupId = 7 });

        }

        public override void Down()
        {
            //SettingIrate
            Insert.IntoTable(Tables.AuthorisationGroupPoint)
            .Row(new { AuthorisationPointId = 71, AuthorisationGroupId = 5 })
            .Row(new { AuthorisationPointId = 72, AuthorisationGroupId = 5 })
            .Row(new { AuthorisationPointId = 73, AuthorisationGroupId = 5 })
            .Row(new { AuthorisationPointId = 74, AuthorisationGroupId = 5 });

            Delete.FromTable(Tables.AuthorisationGroupPoint)
            .Row(new { AuthorisationPointId = 71, AuthorisationGroupId = 7 })
            .Row(new { AuthorisationPointId = 72, AuthorisationGroupId = 7 })
            .Row(new { AuthorisationPointId = 73, AuthorisationGroupId = 7 })
            .Row(new { AuthorisationPointId = 74, AuthorisationGroupId = 7 });

            //SettingsIperson
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 94, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 95, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 96, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 97, AuthorisationGroupId = 7 });

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 94, Name = "Create", AuthorisationPointCategoryId = 29 })
                .Row(new { Id = 95, Name = "Edit", AuthorisationPointCategoryId = 29 })
                .Row(new { Id = 96, Name = "Delete", AuthorisationPointCategoryId = 29 })
                .Row(new { Id = 97, Name = "List", AuthorisationPointCategoryId = 29 });

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 29, Name = "SettingsIperson" });

            //SettingsQuoteUpload
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 98, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 99, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 100, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 101, AuthorisationGroupId = 7 });

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 98, Name = "Create", AuthorisationPointCategoryId = 30 })
                .Row(new { Id = 99, Name = "Edit", AuthorisationPointCategoryId = 30 })
                .Row(new { Id = 100, Name = "Delete", AuthorisationPointCategoryId = 30 })
                .Row(new { Id = 101, Name = "List", AuthorisationPointCategoryId = 30 });

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 30, Name = "SettingsQuoteUpload" });

            //EmailCommunicationSettings
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 102, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 103, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 104, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 105, AuthorisationGroupId = 7 });

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 102, Name = "Create", AuthorisationPointCategoryId = 31 })
                .Row(new { Id = 103, Name = "Edit", AuthorisationPointCategoryId = 31 })
                .Row(new { Id = 104, Name = "Delete", AuthorisationPointCategoryId = 31 })
                .Row(new { Id = 105, Name = "List", AuthorisationPointCategoryId = 31 });

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 31, Name = "EmailCommunicationSetting" });

            //EmailCommunicationSettings
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 106, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 107, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 108, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 109, AuthorisationGroupId = 7 });

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 106, Name = "Create", AuthorisationPointCategoryId = 32 })
                .Row(new { Id = 107, Name = "Edit", AuthorisationPointCategoryId = 32 })
                .Row(new { Id = 108, Name = "Delete", AuthorisationPointCategoryId = 32 })
                .Row(new { Id = 109, Name = "List", AuthorisationPointCategoryId = 32 });

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 32, Name = "CommunicationSetting" });

            //Product Fee
            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 75, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 76, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 77, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 78, AuthorisationGroupId = 5 });

            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 75, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 76, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 77, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 78, AuthorisationGroupId = 7 });

            //VehicleGuideSettings
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 110, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 111, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 112, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 113, AuthorisationGroupId = 7 });


            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 110, Name = "Create", AuthorisationPointCategoryId = 33 })
                .Row(new { Id = 111, Name = "Edit", AuthorisationPointCategoryId = 33 })
                .Row(new { Id = 112, Name = "Delete", AuthorisationPointCategoryId = 33 })
                .Row(new { Id = 113, Name = "List", AuthorisationPointCategoryId = 33 });

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 33, Name = "VehicleGuideSetting" });

        }
    }
}