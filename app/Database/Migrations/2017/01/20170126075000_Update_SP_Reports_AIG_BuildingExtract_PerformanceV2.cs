﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170126075000)]
    public class Update_SP_Reports_AIG_BuildingExtract_PerformanceV2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170126075000_Update_SP_Reports_AIG_BuildingExtract_PerformanceV2.sql");
        }

        public override void Down()
        {

        }
    }
}