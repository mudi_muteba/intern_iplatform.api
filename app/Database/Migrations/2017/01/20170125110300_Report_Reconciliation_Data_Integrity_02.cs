using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170125110300)]
    public class Report_Reconciliation_Data_Integrity_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170125110300_Report_Reconciliation_Data_Integrity_02.sql");
        }

        public override void Down()
        {

        }
    }
}