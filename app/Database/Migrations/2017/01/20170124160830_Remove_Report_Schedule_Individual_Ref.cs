﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170124160830)]
    public class Remove_Report_Schedule_Individual_Ref : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.Individual).Constraint("FKA2D64CFE0F6D630").Exists())
                Delete.ForeignKey("FKA2D64CFE0F6D630").OnTable(Tables.Individual);
            if (Schema.Table(Tables.Individual).Column("ReportScheduleId").Exists())
                Delete.Column("ReportScheduleId").FromTable(Tables.Individual);
            if (Schema.Table("ReportScheduleRecipient").Constraint("FK_ReportScheduleRecipient_Individual_RecipientId").Exists())
                Delete.ForeignKey("FK_ReportScheduleRecipient_Individual_RecipientId").OnTable("ReportScheduleRecipient");
        }

        public override void Down()
        {

        }
    }
}