﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170113110200)]
    public class Removing_Default_Answers_On_Buildings_and_Contents_For_MultiQuote : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170113110200_Removing_Default_Answers_On_Buildings_and_Contents_For_MultiQuote.sql");
        }

        public override void Down()
        {

        }
    }
}