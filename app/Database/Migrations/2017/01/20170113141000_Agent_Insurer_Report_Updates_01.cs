﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170113141000)]
    public class Agent_Insurer_Report_Updates_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170113141000_Agent_Insurer_Report_Updates_01.sql");
        }

        public override void Down()
        {

        }
    }
}