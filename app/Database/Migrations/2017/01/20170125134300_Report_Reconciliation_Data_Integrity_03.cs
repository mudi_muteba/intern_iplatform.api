using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170125134300)]
    public class Report_Reconciliation_Data_Integrity_03 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170125134300_Report_Reconciliation_Data_Integrity_03.sql");
        }

        public override void Down()
        {

        }
    }
}