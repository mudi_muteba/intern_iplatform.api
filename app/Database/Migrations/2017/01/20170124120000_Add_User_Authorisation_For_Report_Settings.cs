﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170124120000)]
    public class Add_User_Authorisation_For_Report_Settings : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170124120000_Add_User_Authorisation_For_Report_Settings.sql");
        }

        public override void Down()
        {

        }
    }
}