﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170118120300)]
    public class Adding_Loss_History_Question_MissedPremiumLast6Months_Clients : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170118120300_Adding_Loss_History_Question_MissedPremiumLast6Months_Clients.sql");
        }

        public override void Down()
        {

        }
    }
}