using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170127131900)]
    public class Detailed_Insurer_Upload_Report_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170127131900_Detailed_Insurer_Upload_Report_02.sql");
        }

        public override void Down()
        {

        }
    }
}