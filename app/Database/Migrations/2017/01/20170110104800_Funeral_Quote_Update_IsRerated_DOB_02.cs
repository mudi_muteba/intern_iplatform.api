﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20170110104800)]
    public class Funeral_Quote_Update_IsRerated_DOB_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170110104800_Funeral_Quote_Update_IsRerated_DOB_02.sql");
        }

        public override void Down()
        {

        }
    }
}