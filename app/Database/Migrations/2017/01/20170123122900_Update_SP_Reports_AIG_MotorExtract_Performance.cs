﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170123122900)]
    public class Update_SP_Reports_AIG_MotorExtract_Performance : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170123122900_Update_SP_Reports_AIG_MotorExtract_Performance.sql");
        }

        public override void Down()
        {

        }
    }
}