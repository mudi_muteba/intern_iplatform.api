using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170123075700)]
    public class Lead_Management_Report_Description_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170123075700_Lead_Management_Report_Description_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}