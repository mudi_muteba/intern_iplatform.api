﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170112090800)]
    public class ProductFee_BrokerageId_Update : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170112090800_ProductFee_BrokerageId_Update.sql");
        }

        public override void Down()
        {

        }
    }
}