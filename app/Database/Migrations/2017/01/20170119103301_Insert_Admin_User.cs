using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170119103301)]
    public class Insert_Admin_User : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170119103301_Insert_Admin_User.sql");
        }

        public override void Down()
        {

        }
    }
}