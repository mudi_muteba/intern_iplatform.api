﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20170113104500)]
    public class Add_ChannelEvent_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 27, Name = "ChannelEvent" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                // address
                .Row(new { Id = 79, Name = "Create", AuthorisationPointCategoryId = 27 })
                .Row(new { Id = 80, Name = "Edit", AuthorisationPointCategoryId = 27 })
                .Row(new { Id = 81, Name = "Delete", AuthorisationPointCategoryId = 27 })
                .Row(new { Id = 82, Name = "List", AuthorisationPointCategoryId = 27 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 79, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 80, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 81, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 82, AuthorisationGroupId = 5 });
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 79, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 80, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 81, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 82, AuthorisationGroupId = 5 });

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 79, Name = "Create", AuthorisationPointCategoryId = 27 })
                .Row(new { Id = 80, Name = "Edit", AuthorisationPointCategoryId = 27 })
                .Row(new { Id = 81, Name = "Delete", AuthorisationPointCategoryId = 27 })
                .Row(new { Id = 82, Name = "List", AuthorisationPointCategoryId = 27 });

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 27, Name = "ChannelEvent" });
        }
    }
}