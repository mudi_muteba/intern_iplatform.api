using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170124081900)]
    public class Insurer_Data_ToolTip_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170124081900_Insurer_Data_ToolTip_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}