﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170113151100)]
    public class Adding_KingPrice_Channel_And_Renaming_The_Old : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170113151100_Adding_KingPrice_Channel_And_Renaming_The_Old.sql");
        }

        public override void Down()
        {

        }
    }
}