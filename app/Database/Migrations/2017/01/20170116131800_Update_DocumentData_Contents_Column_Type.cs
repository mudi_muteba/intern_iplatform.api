﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170116131800)]
    public class Update_DocumentData_Contents_Column_Type : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170116131800_Update_DocumentData_Contents_Column_Type.sql");
        }

        public override void Down()
        {

        }
    }
}