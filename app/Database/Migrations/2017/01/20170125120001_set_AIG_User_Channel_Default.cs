﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170125120001)]
    public class set_AIG_User_Channel_Default : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170125120001_set_AIG_User_Channel_Default.sql");
        }

        public override void Down()
        {

        }
    }
}