﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170123115200)]
    public class Add_Report_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 28, Name = "Report" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                .Row(new { Id = 83, Name = "AllRiskProposalReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 84, Name = "BuildingProposalReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 85, Name = "ContentProposalReport ", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 86, Name = "DisasterCashProposalReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 87, Name = "FuneralProposalReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 88, Name = "IdentityTheftReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 89, Name = "LeadReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 90, Name = "MotorProposalReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 91, Name = "PersonalLegalLiabilityProposalReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 92, Name = "SalesForceIntegrationLogReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 93, Name = "SalesForceSummeryLogReport", AuthorisationPointCategoryId = 28 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 83, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 84, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 85, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 86, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 87, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 88, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 89, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 90, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 91, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 92, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 93, AuthorisationGroupId = 6 })

                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 83, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 84, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 85, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 86, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 87, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 88, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 89, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 90, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 91, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 92, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 93, AuthorisationGroupId = 6 });

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 83, Name = "AllRiskProposalReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 84, Name = "BuildingProposalReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 85, Name = "ContentProposalReport ", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 86, Name = "DisasterCashProposalReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 87, Name = "FuneralProposalReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 88, Name = "IdentityTheftReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 89, Name = "LeadReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 90, Name = "MotorProposalReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 91, Name = "PersonalLegalLiabilityProposalReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 92, Name = "SalesForceIntegrationLogReport", AuthorisationPointCategoryId = 28 })
                .Row(new { Id = 93, Name = "SalesForceSummeryLogReport", AuthorisationPointCategoryId = 28 });

            Delete.FromTable(Tables.AuthorisationPointCategory)
                    .Row(new { Id = 28, Name = "Report" });
        }
    }
}