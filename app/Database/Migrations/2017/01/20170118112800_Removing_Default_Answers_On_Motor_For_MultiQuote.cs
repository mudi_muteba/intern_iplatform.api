﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170118112800)]
    public class Removing_Default_Answers_On_Motor_For_MultiQuote : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170118112800_Removing_Default_Answers_On_Motor_For_MultiQuote.sql");
        }

        public override void Down()
        {

        }
    }
}