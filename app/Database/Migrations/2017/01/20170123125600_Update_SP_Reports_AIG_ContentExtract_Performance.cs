﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170123125600)]
    public class Update_SP_Reports_AIG_ContentExtract_Performance : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170123125600_Update_SP_Reports_AIG_ContentExtract_Performance.sql");
        }

        public override void Down()
        {

        }
    }
}