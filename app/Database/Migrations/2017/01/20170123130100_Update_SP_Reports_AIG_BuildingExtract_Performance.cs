﻿using FluentMigrator;

namespace Database.Migrations._2017._01
{
    [Migration(20170123130100)]
    public class Update_SP_Reports_AIG_BuildingExtract_Performance : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170123130100_Update_SP_Reports_AIG_BuildingExtract_Performance.sql");
        }

        public override void Down()
        {

        }
    }
}