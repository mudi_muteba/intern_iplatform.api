﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170515112000)]
    public class Create_Report_Indexes_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170515112000_Create_Report_Indexes_01.sql");
        }

        public override void Down()
        {

        }
    }
}
