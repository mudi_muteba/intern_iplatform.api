﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170523120700)]
    public class Zbox_Add_PL_Cover : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170523120700_Zbox_Add_PL_Cover.sql");
        }

        public override void Down()
        {

        }
    }
}
