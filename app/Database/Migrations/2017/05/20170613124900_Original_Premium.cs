﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170613124900)]
    public class _20170613124900_Original_Premium : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170613124900_Original_Premium.sql");
        }

        public override void Down()
        {

        }
    }
}
