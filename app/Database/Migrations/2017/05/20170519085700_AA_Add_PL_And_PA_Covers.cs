﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170519085700)]
    public class AA_Add_PL_And_PA_Covers : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170519085700_AA_Add_PL_And_PA_Covers.sql");
        }

        public override void Down()
        {

        }
    }
}
