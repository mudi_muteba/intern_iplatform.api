﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170515100301)]
    public class Update_Report_CallCentre_AgentPerformanceByProduct_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170515100301_Update_Report_CallCentre_AgentPerformanceByProduct_01.sql");
        }

        public override void Down()
        {

        }
    }
}
