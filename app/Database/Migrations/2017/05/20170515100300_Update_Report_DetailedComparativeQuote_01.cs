﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170515100300)]
    public class Update_Report_DetailedComparativeQuote_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170515100300_Update_Report_DetailedComparativeQuote_01.sql");
        }

        public override void Down()
        {

        }
    }
}
