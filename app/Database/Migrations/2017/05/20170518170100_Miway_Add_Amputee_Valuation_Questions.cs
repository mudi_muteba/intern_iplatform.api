﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170518170100)]
    public class Miway_Add_Amputee_Valuation_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170518170100_Miway_Add_Amputee_Valuation_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
