﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170516143800)]
    public class Reports_DetailedComparativeQuote_Content_Procedures_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170516143800_Reports_DetailedComparativeQuote_Content_Procedures_01.sql");
        }

        public override void Down()
        {

        }
    }
}
