﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170515100308)]
    public class Update_Report_CallCentre_User_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170515100308_Update_Report_CallCentre_User_01.sql");
        }

        public override void Down()
        {

        }
    }
}
