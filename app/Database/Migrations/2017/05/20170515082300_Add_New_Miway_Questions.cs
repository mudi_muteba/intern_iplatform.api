﻿using FluentMigrator;

namespace Database.Migrations._2017._04
{
    [Migration(20170515082300)]
    public class Add_New_Miway_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170515082300_Add_New_Miway_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
