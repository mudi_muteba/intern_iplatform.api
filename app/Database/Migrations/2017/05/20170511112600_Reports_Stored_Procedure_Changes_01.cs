﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170511112600)]
    public class Reports_Stored_Procedure_Changes_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170511112600_Reports_Stored_Procedure_Changes_01.sql");
        }

        public override void Down()
        {

        }
    }
}
