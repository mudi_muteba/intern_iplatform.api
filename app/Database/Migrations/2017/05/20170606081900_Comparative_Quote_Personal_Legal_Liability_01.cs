﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170606081900)]
    public class Comparative_Quote_Personal_Legal_Liability_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170606081900_Comparative_Quote_Personal_Legal_Liability_01.sql");
        }

        public override void Down()
        {

        }
    }
}
