﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170614142000)]
    public class Add_SMART_Channel_And_Person_Settings : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170614142000_Add_SMART_Channel_And_Person_Settings.sql");
        }

        public override void Down()
        {

        }
    }
}
