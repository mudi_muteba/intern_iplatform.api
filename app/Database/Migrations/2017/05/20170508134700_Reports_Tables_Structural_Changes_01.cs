﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170508134700)]
    public class Reports_Tables_Structural_Changes_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170508134700_Reports_Tables_Structural_Changes_01.sql");
        }

        public override void Down()
        {

        }
    }
}
