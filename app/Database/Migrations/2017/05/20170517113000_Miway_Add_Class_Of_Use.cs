﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170517113000)]
    public class Miway_Add_Class_Of_Use : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170517113000_Miway_Add_Class_Of_Use.sql");
        }

        public override void Down()
        {

        }
    }
}
