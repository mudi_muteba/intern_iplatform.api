﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170515104000)]
    public class Update_Report_ReportList_ByChannelId_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170515104000_Update_Report_ReportList_ByChannelId_01.sql");
        }

        public override void Down()
        {

        }
    }
}
