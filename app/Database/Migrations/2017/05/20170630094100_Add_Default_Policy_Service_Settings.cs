﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170630094100)]
    public class Add_Default_Policy_Service_Settings : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170630094100_Add_Default_Policy_Service_Settings.sql");
        }

        public override void Down()
        {

        }
    }
}
