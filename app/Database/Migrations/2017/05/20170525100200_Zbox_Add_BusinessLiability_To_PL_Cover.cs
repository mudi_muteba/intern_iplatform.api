﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170525100200)]
    public class Zbox_Add_BusinessLiability_To_PL_Cover : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170525100200_Zbox_Add_BusinessLiability_To_PL_Cover.sql");
        }

        public override void Down()
        {

        }
    }
}
