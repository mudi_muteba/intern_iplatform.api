﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170515100304)]
    public class Update_Report_CallCentre_DetailedInsurerUpload_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170515100304_Update_Report_CallCentre_DetailedInsurerUpload_01.sql");
        }

        public override void Down()
        {

        }
    }
}
