﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170531150900)]
    public class AA_Add_Motor_Questions_and_Vaps : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170531150900_AA_Add_Motor_Questions_and_Vaps.sql");
        }

        public override void Down()
        {

        }
    }
}
