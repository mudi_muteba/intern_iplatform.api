﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170508100000)]
    public class Mirror_AG_Product_Benefits_Virseker_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170508100000_Mirror_AG_Product_Benefits_Virseker_01.sql");
        }

        public override void Down()
        {

        }
    }
}
