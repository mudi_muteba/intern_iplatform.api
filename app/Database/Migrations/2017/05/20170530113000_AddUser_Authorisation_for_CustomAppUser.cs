﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170530113000)]
    public class AddUser_Authorisation_for_CustomAppUser : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170530113000_AddUser_Authorisation_for_CustomAppUser.sql");
        }

        public override void Down()
        {

        }
    }
}
