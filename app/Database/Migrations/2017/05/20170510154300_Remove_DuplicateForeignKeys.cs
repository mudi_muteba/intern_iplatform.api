﻿using FluentMigrator;

namespace Database.Migrations._2017._04
{
    [Migration(20170510154300)]
    public class _20170510154300_Remove_DuplicateForeignKeys : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170510154300_Remove_DuplicateForeignKeys.sql");
        }

        public override void Down()
        {

        }
    }
}
