﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170510101540)]
    public class IPL_845_Scan_Driver_License : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170510101540_IPL_845_Scan_Driver_License.sql");
        }

        public override void Down()
        {

        }
    }
}
