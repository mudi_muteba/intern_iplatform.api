﻿using FluentMigrator;

namespace Database.Migrations._2017._05
{
    [Migration(20170614135800)]
    public class Fix_Duplicates_vw_rating_configuration_View : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170614135800_Fix_Duplicates_vw_rating_configuration_View.sql");
        }

        public override void Down()
        {

        }
    }
}
