﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170922102500)]
    public class Update_Garrun_Motor_Theft_Claims_IPL1231 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170922102500_Update_Garrun_Motor_Theft_Claims_IPL1231.sql");
        }

        public override void Down()
        {

        }
    }
}
