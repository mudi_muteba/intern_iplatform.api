﻿using FluentMigrator;

namespace Database.Migrations._2017._09
{
    [Migration(20170904104100)]
    public class Alter_Report_CallCentre_InsurerQuoteBreakdown_Header : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170904104100_Alter_Report_CallCentre_InsurerQuoteBreakdown_Header.sql");
        }

        public override void Down()
        {

        }
    }
}
