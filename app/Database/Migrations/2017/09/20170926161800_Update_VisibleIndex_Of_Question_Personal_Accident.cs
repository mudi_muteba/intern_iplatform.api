﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170926161800)]
    public class Update_VisibleIndex_Of_Question_Personal_Accident : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170926161800_Update_VisibleIndex_Of_Question_Personal_Accident.sql");
        }

        public override void Down()
        {

        }
    }
}
