﻿using FluentMigrator;

namespace Database.Migrations._2017._09
{
    [Migration(20170904121101)]
    public class Alter_Report_CallCentre_BrokerQuoteUpload_GetSummary_WithChannelIds_WithoutInsurerIds : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170904121101_Alter_Report_CallCentre_BrokerQuoteUpload_GetSummary_WithChannelIds_WithoutInsurerIds.sql");
        }

        public override void Down()
        {

        }
    }
}
