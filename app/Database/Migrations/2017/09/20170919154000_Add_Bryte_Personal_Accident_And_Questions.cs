﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170919154000)]
    public class Add_Bryte_Personal_Accident_And_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170919154000_Add_Bryte_Personal_Accident_And_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
