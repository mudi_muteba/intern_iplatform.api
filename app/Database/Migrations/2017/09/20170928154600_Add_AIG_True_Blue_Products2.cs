﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170928154600)]
    public class Add_AIG_True_Blue_Products2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170928154600_Add_AIG_True_Blue_Products2.sql");
        }

        public override void Down()
        {

        }
    }
}
