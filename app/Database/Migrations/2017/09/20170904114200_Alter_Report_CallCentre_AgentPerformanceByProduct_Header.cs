﻿using FluentMigrator;

namespace Database.Migrations._2017._09
{
    [Migration(20170904114200)]
    public class Alter_Report_CallCentre_AgentPerformanceByProduct_Header : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170904114200_Alter_Report_CallCentre_AgentPerformanceByProduct_Header.sql");
        }

        public override void Down()
        {

        }
    }
}
