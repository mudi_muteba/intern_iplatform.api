﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170926141500)]
    public class Add_Bryte_MotorCycle_ClassOfUse_To_MapQuestion : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170926141500_Add_Bryte_MotorCycle_ClassOfUse_To_MapQuestion.sql");
        }

        public override void Down()
        {

        }
    }
}
