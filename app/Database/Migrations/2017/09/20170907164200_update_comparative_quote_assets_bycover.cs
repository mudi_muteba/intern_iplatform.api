﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170907164200)]
    public class Update_Comparative_Quote_Assets_ByCover : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170907164200_Update_Comparative_Quote_Assets_ByCover.sql");
        }

        public override void Down()
        {

        }
    }
}
