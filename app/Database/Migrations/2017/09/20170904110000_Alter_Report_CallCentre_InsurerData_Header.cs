﻿using FluentMigrator;

namespace Database.Migrations._2017._09
{
    [Migration(20170904110000)]
    public class Alter_Report_CallCentre_InsurerData_Header : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170904110000_Alter_Report_CallCentre_InsurerData_Header.sql");
        }

        public override void Down()
        {

        }
    }
}
