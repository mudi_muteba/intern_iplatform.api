﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170919153500)]
    public class Fix_Email_Template_for_IP_channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170919153500_Fix_Email_Template_for_IP_channel.sql");
        }

        public override void Down()
        {

        }
    }
}
