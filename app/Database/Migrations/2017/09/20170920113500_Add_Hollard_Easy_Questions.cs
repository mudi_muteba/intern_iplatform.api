﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170920113500)]
    public class Add_Hollard_Easy_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170920113500_Add_Hollard_Easy_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
