﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170906165700)]
    public class Add_10_Percent_Excess : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170906165700_Add_10_Percent_Excess.sql");
        }

        public override void Down()
        {

        }
    }
}
