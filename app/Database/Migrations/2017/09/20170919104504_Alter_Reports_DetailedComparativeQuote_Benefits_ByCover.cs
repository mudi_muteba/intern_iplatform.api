﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170919104504)]
    public class Alter_Reports_DetailedComparativeQuote_Benefits_ByCover : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170919104504_Alter_Reports_DetailedComparativeQuote_Benefits_ByCover.sql");
        }

        public override void Down()
        {

        }
    }
}
