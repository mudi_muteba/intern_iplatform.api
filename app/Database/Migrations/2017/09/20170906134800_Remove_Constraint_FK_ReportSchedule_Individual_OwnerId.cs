﻿using FluentMigrator;

namespace Database.Migrations._2017._09
{
    [Migration(20170906134800)]
    public class Remove_Constraint_FK_ReportSchedule_Individual_OwnerId : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170906134800_Remove_Constraint_FK_ReportSchedule_Individual_OwnerId.sql");
        }

        public override void Down()
        {

        }
    }
}
