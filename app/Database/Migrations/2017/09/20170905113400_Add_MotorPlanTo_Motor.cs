﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170905113400)]
    public class Add_MotorService : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170905113400_Add_MotorService.sql");
        }

        public override void Down()
        {

        }
    }
}
