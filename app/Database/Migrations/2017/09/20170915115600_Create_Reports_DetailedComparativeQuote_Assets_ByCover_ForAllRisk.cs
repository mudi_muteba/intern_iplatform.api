﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170915115600)]
    public class Create_Reports_DetailedComparativeQuote_Assets_ByCover_ForAllRisk : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170915115600_Create_Reports_DetailedComparativeQuote_Assets_ByCover_ForAllRisk.sql");
        }

        public override void Down()
        {

        }
    }
}
