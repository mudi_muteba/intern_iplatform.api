﻿using FluentMigrator;

namespace Database.Migrations._2017._09
{
    [Migration(20170904153700)]
    public class Alter_Report_CallCentre_LeadManagement_Header : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170904153700_Alter_Report_CallCentre_LeadManagement_Header.sql");
        }

        public override void Down()
        {

        }
    }
}
