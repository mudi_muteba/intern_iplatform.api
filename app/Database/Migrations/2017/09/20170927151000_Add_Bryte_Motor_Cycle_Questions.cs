﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170927151000)]
    public class Add_Bryte_Motor_Cycle_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170927151000_Add_Bryte_Motor_Cycle_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
