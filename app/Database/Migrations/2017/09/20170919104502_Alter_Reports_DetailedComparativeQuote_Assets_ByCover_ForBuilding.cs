﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170919104502)]
    public class Alter_Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170919104502_Alter_Reports_DetailedComparativeQuote_Assets_ByCover_ForBuilding.sql");
        }

        public override void Down()
        {

        }
    }
}
