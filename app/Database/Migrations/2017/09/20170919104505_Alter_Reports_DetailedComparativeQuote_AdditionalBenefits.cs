﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170919104505)]
    public class Alter_Reports_DetailedComparativeQuote_AdditionalBenefits : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170919104505_Alter_Reports_DetailedComparativeQuote_AdditionalBenefits.sql");
        }

        public override void Down()
        {

        }
    }
}
