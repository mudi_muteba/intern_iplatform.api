﻿using FluentMigrator;

namespace Database.Migrations._2017._09
{
    [Migration(20170904115900)]
    public class Alter_Report_CallCentre_DetailedInsurerUpload_Header : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170904115900_Alter_Report_CallCentre_DetailedInsurerUpload_Header.sql");
        }

        public override void Down()
        {

        }
    }
}
