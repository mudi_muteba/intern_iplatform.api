﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170926141300)]
    public class Add_Bryte_MotorCycle_To_Cover_Link : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170926141300_Add_Bryte_MotorCycle_To_Cover_Link.sql");
        }

        public override void Down()
        {

        }
    }
}
