﻿using FluentMigrator;

namespace Database.Migrations._2017._09
{
    [Migration(20170904121100)]
    public class Alter_Report_CallCentre_BrokerQuoteUpload_GetSummary_WithChannelIds_WithInsurerIds : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170904121100_Alter_Report_CallCentre_BrokerQuoteUpload_GetSummary_WithChannelIds_WithInsurerIds.sql");
        }

        public override void Down()
        {

        }
    }
}
