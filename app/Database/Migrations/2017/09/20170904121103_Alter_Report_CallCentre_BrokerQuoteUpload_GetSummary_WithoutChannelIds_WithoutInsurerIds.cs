﻿using FluentMigrator;

namespace Database.Migrations._2017._09
{
    [Migration(20170904121103)]
    public class Alter_Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithoutInsurerIds : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170904121103_Alter_Report_CallCentre_BrokerQuoteUpload_GetSummary_WithoutChannelIds_WithoutInsurerIds.sql");
        }

        public override void Down()
        {

        }
    }
}
