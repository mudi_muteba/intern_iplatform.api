﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170912113600)]
    public class Create_DataView_LeadImport_StoredProcedure : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170912113600_Create_DataView_LeadImport_StoredProcedure.sql");
        }

        public override void Down()
        {

        }
    }
}
