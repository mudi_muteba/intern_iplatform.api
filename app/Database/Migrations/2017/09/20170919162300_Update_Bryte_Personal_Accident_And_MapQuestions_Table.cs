﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170919162300)]
    public class Update_Bryte_Personal_Accident_And_MapQuestions_Table : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170919162300_Update_Bryte_Personal_Accident_And_MapQuestions_Table.sql");
        }

        public override void Down()
        {

        }
    }
}
