﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20170919104501)]
    public class Alter_Reports_DetailedComparativeQuote_Assets_ByCover_ForContents : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170919104501_Alter_Reports_DetailedComparativeQuote_Assets_ByCover_ForContents.sql");
        }

        public override void Down()
        {

        }
    }
}
