﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170727093500)]
    public class _20170727093500_Add_SMART_QuestionDefinition_Value : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170727093500_Add_SMART_QuestionDefinition_Value.sql");
        }

        public override void Down()
        {

        }
    }
}
