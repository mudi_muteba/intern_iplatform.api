﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170707135100)]
    public class Reports_DetailedComparativeQuote_Assets_ByCover_ForPersonalAccident : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170707135100_Reports_DetailedComparativeQuote_Assets_ByCover_ForPersonalAccident.sql");
        }

        public override void Down()
        {

        }
    }
}
