﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170720143000)]
    public class Add_EngineCC_Mulltiquote_Motor : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170720143000_Add_EngineCC_Mulltiquote_Motor.sql");
        }

        public override void Down()
        {

        }
    }
}
