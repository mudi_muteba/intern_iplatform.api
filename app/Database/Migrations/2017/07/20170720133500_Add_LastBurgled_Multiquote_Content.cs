﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170720133500)]
    public class Add_LastBurgled_Multiquote_Content : Migration    
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170720133500_Add_LastBurgled_Multiquote_Content.sql");
        }

        public override void Down()
        {

        }
    }
}
