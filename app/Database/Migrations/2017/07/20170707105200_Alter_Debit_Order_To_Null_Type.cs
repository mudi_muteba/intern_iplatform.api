﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170707105200)]
    public class Alter_Debit_Order_To_Null_Type : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170707105200_Alter_Debit_Order_To_Null_Type.sql");
        }

        public override void Down()
        {

        }
    }
}
