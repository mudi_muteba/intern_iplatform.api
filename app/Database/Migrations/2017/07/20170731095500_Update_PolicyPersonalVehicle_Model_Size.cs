﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170731095500)]
    public class Update_PolicyPersonalVehicle_Model_Size : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170731095500_Update_PolicyPersonalVehicle_Model_Size.sql");
        }

        public override void Down()
        {

        }
    }
}
