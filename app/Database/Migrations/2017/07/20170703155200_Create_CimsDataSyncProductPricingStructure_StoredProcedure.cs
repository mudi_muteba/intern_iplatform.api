﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170703155200)]
    public class _20170703155200_Create_CimsDataSyncProductPricingStructure_StoredProcedure : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170703155200_Create_CimsDataSyncProductPricingStructure_StoredProcedure.sql");
        }

        public override void Down()
        {

        }
    }
}
