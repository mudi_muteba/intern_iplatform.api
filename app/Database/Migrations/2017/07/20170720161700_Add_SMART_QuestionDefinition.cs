﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170720161700)]
    public class _20170720161700_Add_SMART_QuestionDefinition : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170720161700_Add_SMART_QuestionDefinition.sql");
        }

        public override void Down()
        {

        }
    }
}
