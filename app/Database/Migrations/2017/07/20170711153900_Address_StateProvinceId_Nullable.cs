﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170711153900)]
    public class _20170711153900_Address_StateProvinceId_Nullable : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170711153900_Address_StateProvinceId_Nullable.sql");
        }

        public override void Down()
        {

        }
    }
}
