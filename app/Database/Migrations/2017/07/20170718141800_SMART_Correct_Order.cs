﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170718141800)]
    public class SMART_Correct_Order : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170718141800_SMART_Correct_Order.sql");
        }

        public override void Down()
        {

        }
    }
}
