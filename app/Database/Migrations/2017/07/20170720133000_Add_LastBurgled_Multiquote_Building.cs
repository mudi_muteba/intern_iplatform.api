﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170720133000)]
    public class Add_LastBurgled_Multiquote_Building : Migration    
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170720133000_Add_LastBurgled_Multiquote_Building.sql");
        }

        public override void Down()
        {

        }
    }
}
