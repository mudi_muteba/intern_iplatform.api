﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170703090200)]
    public class Add_SMART_Product_MMCode : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170703090200_Add_SMART_Product_MMCode.sql");
        }

        public override void Down()
        {

        }
    }
}
