﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170703143600)]
    public class Indwe_Email_Value_Added_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170703143600_Indwe_Email_Value_Added_Product.sql");
        }

        public override void Down()
        {

        }
    }
}
