﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170720141900)]
    public class Reports_DetailedComparativeQuote_Benefits_ByCover_ForNonBuilding_CaterVap : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170720141900_Reports_DetailedComparativeQuote_Benefits_ByCover_ForNonBuilding_CaterVap.sql");
        }

        public override void Down()
        {

        }
    }
}