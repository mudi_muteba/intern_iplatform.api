﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170726170100)]
    public class Add_PolicyBindingAuthorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 34, Name = "PolicyBinding" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                .Row(new { Id = 114, Name = "Create", AuthorisationPointCategoryId = 34 })
                .Row(new { Id = 115, Name = "Edit", AuthorisationPointCategoryId = 34 })
                .Row(new { Id = 116, Name = "Delete", AuthorisationPointCategoryId = 34 })
                .Row(new { Id = 117, Name = "List", AuthorisationPointCategoryId = 34 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 114, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 115, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 116, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 117, AuthorisationGroupId = 5 })
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 114, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 115, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 116, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 117, AuthorisationGroupId = 5 })
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 114, Name = "Create", AuthorisationPointCategoryId = 34 })
                .Row(new { Id = 115, Name = "Edit", AuthorisationPointCategoryId = 34 })
                .Row(new { Id = 116, Name = "Delete", AuthorisationPointCategoryId = 34 })
                .Row(new { Id = 117, Name = "List", AuthorisationPointCategoryId = 34 })
                ;

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 34, Name = "PolicyBinding" });
        }
    }
}
