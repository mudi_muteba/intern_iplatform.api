﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170727110400)]
    public class Add_NonMotor_CoverDefinitions_To_Oakhurst_Products : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170727110400_Add_NonMotor_CoverDefinitions_To_Oakhurst_Products.sql");
        }

        public override void Down()
        {

        }
    }
}
