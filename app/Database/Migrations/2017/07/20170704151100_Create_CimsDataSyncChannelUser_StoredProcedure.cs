﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170704151100)]
    public class _20170704151100_Create_CimsDataSyncChannelUser_StoredProcedure : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170704151100_Create_CimsDataSyncChannelUser_StoredProcedure.sql");
        }

        public override void Down()
        {

        }
    }
}
