﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170711145000)]
    public class Add_AA_NCB : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170711145000_Add_AA_NCB.sql");
        }

        public override void Down()
        {

        }
    }
}
