using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170216084730)]
    public class Update_Report_IsVisibleInScheduler : Migration
    {
        public override void Up()
        {
            Update.Table("Report").Set(new { IsVisibleInScheduler = false }).Where(new { Name = "Lead Management" });
            Update.Table("Report").Set(new { IsVisibleInScheduler = false }).Where(new { Name = "Target & Sales Management" });
        }

        public override void Down()
        {

        }
    }
}