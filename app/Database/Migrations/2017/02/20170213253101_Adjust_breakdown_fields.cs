using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170213253101)]
    public class Adjust_breakdown_fields : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170213253101_Adjust_breakdown_fields.sql");
        }

        public override void Down()
        {

        }
    }
}