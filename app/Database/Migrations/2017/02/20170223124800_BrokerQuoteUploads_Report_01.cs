using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170223124800)]
    public class BrokerQuoteUploads_Report_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170223124800_BrokerQuoteUploads_Report_01.sql");
        }

        public override void Down()
        {

        }
    }
}