﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170207075202)]
    public class Remove_UserAuthorisationGroup_For_Report_Settings : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170207075202_Remove_UserAuthorisationGroup_For_Report_Settings.sql");
        }

        public override void Down()
        {
           

        }
    }
}