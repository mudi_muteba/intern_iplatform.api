using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170214131500)]
    public class Add_Automatic_Vaps_Question_IPL_624 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170214131500_Add_Automatic_Vaps_Question_IPL_624.sql");
        }

        public override void Down()
        {

        }
    }
}