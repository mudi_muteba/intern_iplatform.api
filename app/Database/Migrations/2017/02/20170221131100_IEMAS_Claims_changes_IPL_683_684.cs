using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170221131100)]
    public class IEMAS_Claims_changes_IPL_683_684 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170221131100_IEMAS_Claims_changes_IPL_683_684.sql");
        }

        public override void Down()
        {

        }
    }
}