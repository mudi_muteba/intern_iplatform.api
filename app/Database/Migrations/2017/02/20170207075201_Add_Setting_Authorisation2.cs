﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170207075201)]
    public class Add_Setting_Authorisation2 : Migration
    {
        public override void Up()
        {
            //SettingIrate
            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 71, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 72, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 73, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 74, AuthorisationGroupId = 5 });

            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 71, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 72, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 73, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 74, AuthorisationGroupId = 7 });

            //SettingiPerson
            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 94, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 95, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 96, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 97, AuthorisationGroupId = 5 });

            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 94, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 95, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 96, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 97, AuthorisationGroupId = 7 });

            //SettingsQuoteUpload
            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 98, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 99, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 100, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 101, AuthorisationGroupId = 5 });

            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 98, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 99, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 100, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 101, AuthorisationGroupId = 7 });

            //EmailCommunicationSettings
            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 102, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 103, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 104, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 105, AuthorisationGroupId = 5 });

            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 102, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 103, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 104, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 105, AuthorisationGroupId = 7 });

            //CommunicationSetting
            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 106, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 107, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 108, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 109, AuthorisationGroupId = 5 });

            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 106, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 107, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 108, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 109, AuthorisationGroupId = 7 });

            //Product Fee
            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 75, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 76, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 77, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 78, AuthorisationGroupId = 5 });

            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 75, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 76, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 77, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 78, AuthorisationGroupId = 7 });

            //VehicleGuideSettings
            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 110, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 111, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 112, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 113, AuthorisationGroupId = 5 });

            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 110, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 111, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 112, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 113, AuthorisationGroupId = 7 });

        }

        public override void Down()
        {
            //SettingIrate
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 71, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 72, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 73, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 74, AuthorisationGroupId = 5 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 71, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 72, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 73, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 74, AuthorisationGroupId = 7 });

            //SettingiPerson
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 94, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 95, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 96, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 97, AuthorisationGroupId = 5 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 94, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 95, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 96, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 97, AuthorisationGroupId = 7 });

            //SettingsQuoteUpload
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 98, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 99, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 100, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 101, AuthorisationGroupId = 5 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 98, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 99, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 100, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 101, AuthorisationGroupId = 7 });

            //EmailCommunicationSettings
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 102, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 103, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 104, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 105, AuthorisationGroupId = 5 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 102, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 103, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 104, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 105, AuthorisationGroupId = 7 });

            //CommunicationSetting
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 106, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 107, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 108, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 109, AuthorisationGroupId = 5 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 106, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 107, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 108, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 109, AuthorisationGroupId = 7 });

            //Product Fee
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 75, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 76, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 77, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 78, AuthorisationGroupId = 5 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 75, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 76, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 77, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 78, AuthorisationGroupId = 7 });

            //VehicleGuideSettings
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 110, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 111, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 112, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 113, AuthorisationGroupId = 5 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new { AuthorisationPointId = 110, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 111, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 112, AuthorisationGroupId = 7 })
                .Row(new { AuthorisationPointId = 113, AuthorisationGroupId = 7 });

        }
    }
}