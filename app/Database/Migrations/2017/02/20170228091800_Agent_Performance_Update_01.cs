using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170228091800)]
    public class Agent_Performance_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170228091800_Agent_Performance_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}