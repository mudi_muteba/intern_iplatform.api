﻿using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170227150100)]
    public class Add_MiWay_Glasses_CarHire_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170227150100_Add_MiWay_Glasses_CarHire_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
