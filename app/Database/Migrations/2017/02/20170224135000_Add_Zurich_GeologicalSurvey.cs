﻿using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170224135000)]
    public class Add_Zurich_GeologicalSurvey : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170224135000_Add_Zurich_GeologicalSurvey.sql");
        }

        public override void Down()
        {

        }
    }
}
