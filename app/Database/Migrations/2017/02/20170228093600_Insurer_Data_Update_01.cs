using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170228093600)]
    public class Insurer_Data_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170228093600_Insurer_Data_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}