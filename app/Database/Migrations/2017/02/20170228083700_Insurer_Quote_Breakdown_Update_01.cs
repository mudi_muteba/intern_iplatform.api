using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170228083700)]
    public class Insurer_Quote_Breakdown_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170228083700_Insurer_Quote_Breakdown_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}