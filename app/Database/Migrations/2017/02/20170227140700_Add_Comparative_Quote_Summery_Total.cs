﻿using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170227140700)]
    public class Add_Comparative_Quote_Summery_Total : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170227140700_Add_Comparative_Quote_Summery_Total.sql");
        }

        public override void Down()
        {

        }
    }
}
