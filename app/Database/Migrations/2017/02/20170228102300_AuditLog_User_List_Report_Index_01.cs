using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170228102300)]
    public class AuditLog_User_List_Report_Index_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170228102300_AuditLog_User_List_Report_Index_01.sql");
        }

        public override void Down()
        {

        }
    }
}