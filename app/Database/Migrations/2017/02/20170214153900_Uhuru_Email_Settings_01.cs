using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170214153900)]
    public class Uhuru_Email_Settings_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170214153900_Uhuru_Email_Settings_01.sql");
        }

        public override void Down()
        {

        }
    }
}