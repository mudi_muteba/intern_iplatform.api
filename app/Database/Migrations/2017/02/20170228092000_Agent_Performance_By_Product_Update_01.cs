using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170228092000)]
    public class Agent_Performance_By_Product_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170228092000_Agent_Performance_By_Product_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}