using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170228094800)]
    public class Insurer_Data_Update_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170228094800_Insurer_Data_Update_02.sql");
        }

        public override void Down()
        {

        }
    }
}