using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170202112901)]
    public class Add_Rodel_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170202112901_Add_Rodel_Channel.sql");
        }

        public override void Down()
        {

        }
    }
}