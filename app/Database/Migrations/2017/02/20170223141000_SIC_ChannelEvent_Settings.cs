using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170223141000)]
    public class SIC_ChannelEvent_Settings : Migration
    {
        public override void Up()
        {
            // Disable, not required for Rodel ChannelId 20
            //Execute.EmbeddedScript("20170223141000_SIC_ChannelEvent_Settings.sql");
        }

        public override void Down()
        {

        }
    }
}