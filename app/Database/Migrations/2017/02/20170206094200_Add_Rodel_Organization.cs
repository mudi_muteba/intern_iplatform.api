using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170206094200)]
    public class Add_Rodel_Organization : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170206094200_Add_Rodel_Organization.sql");
        }

        public override void Down()
        {

        }
    }
}