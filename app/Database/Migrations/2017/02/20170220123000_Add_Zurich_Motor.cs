using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170220123000)]
    public class Adding_Zurich_Motor : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170220123000_Add_Zurich_Motor.sql");
        }

        public override void Down()
        {

        }
    }
}