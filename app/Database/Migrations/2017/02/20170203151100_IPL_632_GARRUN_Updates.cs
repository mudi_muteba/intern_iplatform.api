using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170203151100)]
    public class IPL_632_GARRUN_Updates : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170203151100_IPL_632_GARRUN_Updates.sql");
        }

        public override void Down()
        {

        }
    }
}