using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170202083400)]
    public class Claims_IEMAS_Changes_IPL625 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170202083400_Claims_IEMAS_Changes_IPL625.sql");
        }

        public override void Down()
        {

        }
    }
}