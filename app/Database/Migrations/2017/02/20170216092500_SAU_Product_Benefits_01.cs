using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170216092500)]
    public class SAU_Product_Benefits_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170216092500_SAU_Product_Benefits_01.sql");
        }

        public override void Down()
        {

        }
    }
}