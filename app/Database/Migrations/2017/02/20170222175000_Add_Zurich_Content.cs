﻿using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170222175000)]
    public class Add_Zurich_Content : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170222175000_Add_Zurich_Content.sql");
        }

        public override void Down()
        {

        }
    }
}
