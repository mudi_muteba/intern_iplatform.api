﻿using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170222164000)]
    public class Add_Zurich_Building : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170222164000_Add_Zurich_Building.sql");
        }

        public override void Down()
        {

        }
    }
}
