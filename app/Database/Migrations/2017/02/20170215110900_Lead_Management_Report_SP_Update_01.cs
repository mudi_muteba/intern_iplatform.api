using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170215110900)]
    public class Lead_Management_Report_SP_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170215110900_Lead_Management_Report_SP_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}