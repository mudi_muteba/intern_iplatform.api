using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170207155730)]
    public class Report_Schedule_Remove_Unique_Constraint : Migration
    {
        public override void Up()
        {
            Delete.UniqueConstraint("UQ_ReportSchedule_Name").FromTable("ReportSchedule");
        }

        public override void Down()
        {

        }
    }
}