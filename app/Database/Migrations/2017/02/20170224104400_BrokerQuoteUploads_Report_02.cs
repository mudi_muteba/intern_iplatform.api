using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170224104400)]
    public class BrokerQuoteUploads_Report_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170224104400_BrokerQuoteUploads_Report_02.sql");
        }

        public override void Down()
        {

        }
    }
}