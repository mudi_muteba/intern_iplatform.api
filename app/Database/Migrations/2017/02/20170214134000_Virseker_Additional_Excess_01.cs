using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170214134000)]
    public class Virseker_Additional_Excess_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170214134000_Virseker_Additional_Excess_01.sql");
        }

        public override void Down()
        {

        }
    }
}