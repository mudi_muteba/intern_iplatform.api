using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170216135400)]
    public class Update_Organisation_Product_IPL_624 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170216135400_Update_Organisation_Product_IPL_624.sql");
        }

        public override void Down()
        {

        }
    }
}