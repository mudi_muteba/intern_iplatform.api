using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170221091600)]
    public class User_List_Report_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170221091600_User_List_Report_01.sql");
        }

        public override void Down()
        {

        }
    }
}