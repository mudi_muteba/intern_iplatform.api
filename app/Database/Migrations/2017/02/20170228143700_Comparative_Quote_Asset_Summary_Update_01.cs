using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170228143700)]
    public class Comparative_Quote_Asset_Summary_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170228143700_Comparative_Quote_Asset_Summary_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}