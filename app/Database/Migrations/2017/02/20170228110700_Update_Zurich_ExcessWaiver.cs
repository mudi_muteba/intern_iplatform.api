﻿using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170228110700)]
    public class Update_Zurich_ExcessWaiver : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170228110700_Update_Zurich_ExcessWaiver.sql");
        }

        public override void Down()
        {

        }
    }
}
