using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170217135412)]
    public class Adding_New_Channels : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170217135412_Adding_New_Channels.sql");
        }

        public override void Down()
        {

        }
    }
}