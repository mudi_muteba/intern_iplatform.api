﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170207075200)]
    public class Add_Report_Authorisation2 : Migration
    {
        public override void Up()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 83, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 84, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 85, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 86, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 87, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 88, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 89, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 90, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 91, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 92, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 93, AuthorisationGroupId = 6 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 83, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 84, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 85, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 86, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 87, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 88, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 89, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 90, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 91, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 92, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 93, AuthorisationGroupId = 5 })

                ;
        }

        public override void Down()
        {
            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 83, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 84, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 85, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 86, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 87, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 88, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 89, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 90, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 91, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 92, AuthorisationGroupId = 6 })
                .Row(new { AuthorisationPointId = 93, AuthorisationGroupId = 6 });

            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 83, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 84, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 85, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 86, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 87, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 88, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 89, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 90, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 91, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 92, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 93, AuthorisationGroupId = 5 })

                ;
        }
    }
}