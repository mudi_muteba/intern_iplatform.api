using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170227162300)]
    public class Comparative_Quote_Summary_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170227162300_Comparative_Quote_Summary_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}