﻿using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170227101800)]
    public class Add_Vehicle_Values_MM_Credentials_To_Channels : Migration
    {
        public override void Up()
        {
            Update.Table("VehicleGuideSetting").Set(new
            {
                RegCheckUkEnabled = false,
                VehicleValuesPassword = "T3st194#",
                VehicleValuesHashKey = "Cli3ntH@sah",
                VehicleValuesApiKey = "C4EA646D-7C82-48D5-8D29-3F4518A4F805",
                VehicleValuesUserId = "183194",
                VehicleValuesReportCode = "9849A63B-FC07-4BBD-B1D9-3BFAF6B2EE21"
            }).AllRows();
        }

        public override void Down()
        {

        }
    }
}
