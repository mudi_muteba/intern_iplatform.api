using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170203133000)]
    public class Update_OrganizationClaimsTypeCategpry_DisplayName : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170203133000_Update_OrganizationClaimsTypeCategpry_DisplayName.sql");
        }

        public override void Down()
        {

        }
    }
}