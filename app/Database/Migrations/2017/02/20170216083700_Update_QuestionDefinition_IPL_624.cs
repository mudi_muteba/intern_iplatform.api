using FluentMigrator;

namespace Database.Migrations._2017._02
{
    [Migration(20170216083700)]
    public class Update_QuestionDefinition_IPL_624 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170216083700_Update_QuestionDefinition_IPL_624.sql");
        }

        public override void Down()
        {

        }
    }
}