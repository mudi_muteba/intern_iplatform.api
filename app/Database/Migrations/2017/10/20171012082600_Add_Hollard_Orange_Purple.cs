﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20171012082600)]
    public class Add_Hollard_Orange_Purple : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171012082600_Add_Hollard_Orange_Purple.sql");
        }

        public override void Down()
        {

        }
    }
}
