﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171017102000)]
    public class Add_Caravan_Or_Trailer_Questions_On_Bryte_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171017102000_Add_Caravan_Or_Trailer_Questions_On_Bryte_Product.sql");
        }

        public override void Down()
        {

        }
    }
}
