﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171020105701)]
    public class Add_AIG_True_Blue_SecurityInstalled_Contents : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171020105701_Add_AIG_True_Blue_SecurityInstalled_Contents.sql");
        }

        public override void Down()
        {

        }
    }
}
