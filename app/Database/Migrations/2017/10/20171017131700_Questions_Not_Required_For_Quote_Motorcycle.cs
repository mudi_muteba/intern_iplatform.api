﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171017131700)]
    public class Questions_Not_Required_For_Quote_Motorcycle : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171017131700_Questions_Not_Required_For_Quote_Motorcycle.sql");
        }

        public override void Down()
        {

        }
    }
}
