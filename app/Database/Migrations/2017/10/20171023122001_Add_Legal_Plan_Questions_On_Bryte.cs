﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171023122001)]
    public class Add_Legal_Plan_Questions_On_Bryte : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171023122001_Add_Legal_Plan_Questions_On_Bryte.sql");
        }

        public override void Down()
        {

        }
    }
}
