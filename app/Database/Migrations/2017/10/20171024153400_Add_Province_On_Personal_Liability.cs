﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171024153400)]
    public class Add_Province_On_Personal_Liability : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171024153400_Add_Province_On_Personal_Liability.sql");
        }

        public override void Down()
        {

        }
    }
}
