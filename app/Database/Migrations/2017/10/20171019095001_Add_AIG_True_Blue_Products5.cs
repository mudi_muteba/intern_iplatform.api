﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171019095001)]
    public class Add_AIG_True_Blue_Products5 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171019095001_Add_AIG_True_Blue_Products5.sql");
        }

        public override void Down()
        {

        }
    }
}
