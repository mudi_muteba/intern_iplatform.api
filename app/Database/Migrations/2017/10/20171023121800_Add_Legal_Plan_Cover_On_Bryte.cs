﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171023121800)]
    public class Add_Legal_Plan_Cover_On_Bryte : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171023121800_Add_Legal_Plan_Cover_On_Bryte.sql");
        }

        public override void Down()
        {

        }
    }
}
