﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171017102600)]
    public class Add_Bryte_Caravan_Or_Trailer_Cover : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171017102600_Add_Bryte_Caravan_Or_Trailer_Cover.sql");
        }

        public override void Down()
        {

        }
    }
}
