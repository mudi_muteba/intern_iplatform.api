﻿using FluentMigrator;

namespace Database.Migrations._2017._10
{
    [Migration(20171009133700)]
    public class UpdateUserReport_Add_IsBillable : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171009133700_UpdateUserReport_Add_IsBillable.sql");
        }

        public override void Down()
        {

        }
    }
}