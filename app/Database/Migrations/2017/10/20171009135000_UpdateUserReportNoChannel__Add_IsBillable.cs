﻿using FluentMigrator;

namespace Database.Migrations._2017._10
{
    [Migration(20171009135000)]
    public class UpdateUserReportNoChannel__Add_IsBillable : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171009135000_UpdateUserReportNoChannel__Add_IsBillable.sql");
        }

        public override void Down()
        {

        }
    }
}