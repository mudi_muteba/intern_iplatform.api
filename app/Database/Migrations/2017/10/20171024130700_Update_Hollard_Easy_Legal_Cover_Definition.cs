﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171024130700)]
    public class Update_Hollard_Easy_Legal_Cover_Definition : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171024130700_Update_Hollard_Easy_Legal_Cover_Definition.sql");
        }

        public override void Down()
        {

        }
    }
}
