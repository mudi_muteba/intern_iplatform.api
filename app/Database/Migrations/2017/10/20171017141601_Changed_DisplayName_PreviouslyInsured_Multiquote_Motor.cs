﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171017141601)]
    public class Changed_DisplayName_PreviouslyInsured_Multiquote_Motor : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171017141601_Changed_DisplayName_PreviouslyInsured_Multiquote_Motor.sql");
        }

        public override void Down()
        {

        }
    }
}
