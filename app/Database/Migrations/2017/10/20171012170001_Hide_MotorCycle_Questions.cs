﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171012170001)]
    public class Hide_MotorCycle_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171012170001_Hide_MotorCycle_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
