﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20171010113400)]
    public class Hide_Bryte_Personal_Accident_Question : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171010113400_Hide_Bryte_Personal_Accident_Question.sql");
        }

        public override void Down()
        {

        }
    }
}
