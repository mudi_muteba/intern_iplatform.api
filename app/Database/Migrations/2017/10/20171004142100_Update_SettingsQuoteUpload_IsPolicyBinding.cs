﻿using FluentMigrator;

namespace Database.Migrations._2017._09
{
    [Migration(20171004142100)]
    public class Update_SettingsQuoteUpload_IsPolicyBinding : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171004142100_Update_SettingsQuoteUpload_IsPolicyBinding.sql");
        }

        public override void Down()
        {

        }
    }
}
