﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._09
{
    [Migration(20171004171700)]
    public class Add_AIG_True_Blue_Products4 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171004171700_Add_AIG_True_Blue_Products4.sql");
        }

        public override void Down()
        {

        }
    }
}
