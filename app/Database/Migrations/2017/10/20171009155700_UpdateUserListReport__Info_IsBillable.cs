﻿using FluentMigrator;

namespace Database.Migrations._2017._10
{
    [Migration(20171009155700)]
    public class UpdateUserListReport__Info_IsBillable : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171009155700_UpdateUserListReport__Info_IsBillable.sql");
        }

        public override void Down()
        {

        }
    }
}