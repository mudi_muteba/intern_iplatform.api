﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171016170001)]
    public class Setting_Default_Values_Motorcycle : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171016170001_Setting_Default_Values_Motorcycle.sql");
        }

        public override void Down()
        {

        }
    }
}
