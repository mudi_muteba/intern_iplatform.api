﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171005155800)]
    public class Update_Personal_Accident_Questions_With_Default_Values : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171005155800_Update_Personal_Accident_Questions_With_Default_Values.sql");
        }

        public override void Down()
        {

        }
    }
}
