﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171025103200)]
    public class Unhide_MotorCycle_Question : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171025103200_Unhide_MotorCycle_Question.sql");
        }

        public override void Down()
        {

        }
    }
}
