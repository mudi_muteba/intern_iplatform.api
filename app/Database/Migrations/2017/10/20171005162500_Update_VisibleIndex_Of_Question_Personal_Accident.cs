﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171005162500)]
    public class Update_VisibleIndex_Of_Question_Personal_Accident : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171005162500_Update_VisibleIndex_Of_Question_Personal_Accident.sql");
        }

        public override void Down()
        {

        }
    }
}
