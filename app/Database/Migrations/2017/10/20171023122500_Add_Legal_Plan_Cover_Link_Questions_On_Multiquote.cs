﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171023122500)]
    public class Add_Legal_Plan_Cover_Link_Questions_On_Multiquote : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171023122500_Add_Legal_Plan_Cover_Link_Questions_On_Multiquote.sql");
        }

        public override void Down()
        {

        }
    }
}
