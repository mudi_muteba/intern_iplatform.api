﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2017._10
{
    [Migration(20171018165300)]
    public class Add_CampaignBucketAuthorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // call centre agents
                .Row(new { AuthorisationPointId = 69, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 70, AuthorisationGroupId = 1 })
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // call centre agents
                .Row(new { AuthorisationPointId = 69, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 70, AuthorisationGroupId = 1 })
                ;
        }
    }
}
