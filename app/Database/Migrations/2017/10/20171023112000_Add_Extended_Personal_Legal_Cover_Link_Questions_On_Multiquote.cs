﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171023112000)]
    public class _Add_Extended_Personal_Legal_Cover_Link_Questions_On_Multiquote : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171023112000_Add_Extended_Personal_Legal_Cover_Link_Questions_On_Multiquote.sql");
        }

        public override void Down()
        {

        }
    }
}
