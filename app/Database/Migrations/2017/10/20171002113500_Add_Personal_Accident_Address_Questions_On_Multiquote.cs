﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171002113500)]
    public class Add_Personal_Accident_Address_Questions_On_Multiquote : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171002113500_Add_Personal_Accident_Address_Questions_On_Multiquote.sql");
        }

        public override void Down()
        {

        }
    }
}
