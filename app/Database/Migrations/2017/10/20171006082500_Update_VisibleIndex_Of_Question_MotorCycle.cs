﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._10
{
    [Migration(20171006082500)]
    public class Update_VisibleIndex_Of_Question_MotorCycle : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171006082500_Update_VisibleIndex_Of_Question_MotorCycle.sql");
        }

        public override void Down()
        {

        }
    }
}
