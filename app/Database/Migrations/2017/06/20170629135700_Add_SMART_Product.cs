﻿using FluentMigrator;

namespace Database.Migrations._2017._06
{
    [Migration(20170629135700)]
    public class Add_SMART_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170629135700_Add_SMART_Product.sql");
        }

        public override void Down()
        {

        }
    }
}
