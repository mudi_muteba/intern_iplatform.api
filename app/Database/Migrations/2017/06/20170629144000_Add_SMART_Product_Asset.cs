﻿using FluentMigrator;

namespace Database.Migrations._2017._06
{
    [Migration(20170629144000)]
    public class Add_SMART_Product_Asset : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170629144000_Add_SMART_Product_Asset.sql");
        }

        public override void Down()
        {

        }
    }
}
