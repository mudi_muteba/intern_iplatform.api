﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170829170100)]
    public class Comparative_Quote_Assets_For_Contents : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170829170100_Comparative_Quote_Assets_For_Contents.sql");
        }

        public override void Down()
        {
            
        }
    }
}