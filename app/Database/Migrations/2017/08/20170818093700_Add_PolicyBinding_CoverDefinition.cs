﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170818093700)]
    public class Add_PolicyBinding_CoverDefinition : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170818093700_Add_PolicyBinding_CoverDefinition.sql");
        }

        public override void Down()
        {

        }
    }
}
