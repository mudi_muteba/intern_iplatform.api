﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170808094100)]
    public class Add_Motorcycle_To_Multiquote : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170808094100_Add_Motorcycle_To_Multiquote.sql");
        }

        public override void Down()
        {

        }
    }
}
