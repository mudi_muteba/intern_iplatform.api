﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170810155500)]
    public class Update_Garun_FNOL_Question : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170810155500_Update_Garun_FNOL_Question.sql");
        }

        public override void Down()
        {

        }
    }
}