﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170810131300)]
    public class Add_Update_Garun_FNOL_Changes : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170810131300_Add_Update_Garun_FNOL_Changes.sql");
        }

        public override void Down()
        {

        }
    }
}