﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170810171300)]
    public class _20170810171300_Alter_Column_Types : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170810171300_Alter_Column_Types.sql");
        }

        public override void Down()
        {

        }
    }
}
