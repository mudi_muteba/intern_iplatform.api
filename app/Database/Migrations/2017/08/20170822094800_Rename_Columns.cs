﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170822094800)]
    public class _20170822094800_Rename_Columns : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170822094800_Rename_Columns.sql");
        }

        public override void Down()
        {

        }
    }
}
