﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170830150400)]
    public class Alter_vw_product_allocation : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170830150400_Alter_vw_product_allocation.sql");
        }

        public override void Down()
        {
            
        }
    }
}