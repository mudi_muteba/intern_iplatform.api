﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170830165300)]
    public class Add_Extended_Selected_Excess_List_For_KP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170830165300_Add_Extended_Selected_Excess_List_For_KP.sql");
        }

        public override void Down()
        {

        }
    }
}
