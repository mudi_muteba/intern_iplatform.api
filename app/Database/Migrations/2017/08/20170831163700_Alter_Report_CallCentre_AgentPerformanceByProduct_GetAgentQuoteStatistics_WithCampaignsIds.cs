﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170831163700)]
    public class Alter_Report_CallCentre_AgentPerformanceByProduct_GetAgentQuoteStatistics_WithCampaignsIds : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170831163700_Alter_Report_CallCentre_AgentPerformanceByProduct_GetAgentQuoteStatistics_WithCampaignsIds.sql");
        }

        public override void Down()
        {

        }
    }
}
