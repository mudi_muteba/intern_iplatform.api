﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170808094200)]
    public class Add_New_AA_Covers : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170808094200_Add_New_AA_Covers.sql");
        }

        public override void Down()
        {

        }
    }
}
