﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170822140000)]
    public class Alter_SP_Basic_Excess_Description_Comparative_Quote : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170822140000_Alter_SP_Basic_Excess_Description_Comparative_Quote.sql");
        }

        public override void Down()
        {

        }
    }
}
