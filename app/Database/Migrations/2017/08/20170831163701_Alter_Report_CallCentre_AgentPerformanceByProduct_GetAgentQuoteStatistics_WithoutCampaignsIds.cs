﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170831163701)]
    public class Alter_Report_CallCentre_AgentPerformanceByProduct_GetAgentQuoteStatistics_WithoutCampaignsIds : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170831163701_Alter_Report_CallCentre_AgentPerformanceByProduct_GetAgentQuoteStatistics_WithoutCampaignsIds.sql");
        }

        public override void Down()
        {

        }
    }
}
