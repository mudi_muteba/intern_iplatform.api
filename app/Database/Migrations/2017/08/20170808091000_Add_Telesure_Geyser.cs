﻿using FluentMigrator;

namespace Database.Migrations._2017._07
{
    [Migration(20170808091000)]
    public class Add_Telesure_Geyser : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170808091000_Add_Telesure_Geyser.sql");
        }

        public override void Down()
        {

        }
    }
}
