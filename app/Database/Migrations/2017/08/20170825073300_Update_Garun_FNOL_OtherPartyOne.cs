﻿using FluentMigrator;

namespace Database.Migrations._2017._08
{
    [Migration(20170825073300)]
    public class Update_Garun_FNOL_OtherPartyOne : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170825073300_Update_Garun_FNOL_OtherPartyOne.sql");
        }

        public override void Down()
        {
            
        }
    }
}