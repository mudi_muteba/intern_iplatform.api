﻿using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170316144000)]
    public class Add_Zurich_AllRisk_Categories : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170316144000_Add_Zurich_AllRisk_Categories.sql");
        }

        public override void Down()
        {

        }
    }
}
