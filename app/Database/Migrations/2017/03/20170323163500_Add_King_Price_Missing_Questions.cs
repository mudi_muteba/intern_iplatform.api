﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170323163500)]
    public class Add_King_Price_Missing_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170323163500_Add_King_Price_Missing_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
