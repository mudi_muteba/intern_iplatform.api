﻿using System;
using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170308144600)]
    public class Update_Rodel_Channel_SystemId : Migration
    {
        public override void Up()
        {
            Update.Table("Channel").Set(new { SystemId = "BBB2E912-7FE7-4BBA-A1E2-7EC1586D099B" }).Where(new { Name = "Rodel" });
        }

        public override void Down()
        {

        }
    }
}