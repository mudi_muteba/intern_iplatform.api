using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170303082500)]
    public class User_List_Report_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170303082500_User_List_Report_02.sql");
        }

        public override void Down()
        {

        }
    }
}