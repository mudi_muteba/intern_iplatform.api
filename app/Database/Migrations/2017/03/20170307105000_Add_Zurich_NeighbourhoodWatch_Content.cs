﻿using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170307105000)]
    public class Add_Zurich_NeighbourhoodWatch_Content : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170307105000_Add_Zurich_NeighbourhoodWatch_Content.sql");
        }

        public override void Down()
        {

        }
    }
}
