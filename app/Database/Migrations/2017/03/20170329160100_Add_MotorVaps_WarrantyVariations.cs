﻿using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170329160100)]
    public class Add_MotorVaps_WarrantyVariations : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170329160100_Add_MotorVaps_WarrantyVariations.sql");
        }

        public override void Down()
        {

        }
    }
}
