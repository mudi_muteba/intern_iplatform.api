using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170302092000)]
    public class Add_King_Price_V2_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170302092000_Add_King_Price_V2_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}