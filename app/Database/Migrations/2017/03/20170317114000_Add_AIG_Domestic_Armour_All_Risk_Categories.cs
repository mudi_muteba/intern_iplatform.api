﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170317114000)]
    public class Add_AIG_Domestic_Armour_All_Risk_Categories : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170317114000_Add_AIG_Domestic_Armour_All_Risk_Categories.sql");
        }

        public override void Down()
        {

        }
    }
}
