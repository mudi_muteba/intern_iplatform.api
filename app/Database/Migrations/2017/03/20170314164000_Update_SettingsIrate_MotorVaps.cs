﻿using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170314164000)]
    public class Update_SettingsIrate_MotorVaps : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170314164000_Update_SettingsIrate_MotorVaps.sql");
        }

        public override void Down()
        {

        }
    }
}
