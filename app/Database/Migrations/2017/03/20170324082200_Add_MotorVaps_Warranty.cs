﻿using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170324082200)]
    public class Add_MotorVaps_Warranty : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170324082200_Add_MotorVaps_Warranty.sql");
        }

        public override void Down()
        {

        }
    }
}
