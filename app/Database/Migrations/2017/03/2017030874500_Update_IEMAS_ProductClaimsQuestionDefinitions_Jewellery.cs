﻿using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(2017030874500)]
    public class Update_IEMAS_ProductClaimsQuestionDefinitions_Jewellery : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("2017030874500_Update_IEMAS_ProductClaimsQuestionDefinitions_Jewellery.sql");
        }

        public override void Down()
        {

        }
    }
}