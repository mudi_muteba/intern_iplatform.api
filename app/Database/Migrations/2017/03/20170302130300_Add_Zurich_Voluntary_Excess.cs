﻿using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170302130300)]
    public class Add_Zurich_Voluntary_Excess : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170302130300_Add_Zurich_Voluntary_Excess.sql");
        }

        public override void Down()
        {

        }
    }
}
