using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170306122600)]
    public class Update_Main_Driver_Date_Of_Birth : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170306122600_Update_Main_Driver_Date_Of_Birth.sql");
        }

        public override void Down()
        {

        }
    }
}