﻿using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170308113400)]
    public class Add_Zurich_AllRisk : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170308113400_Add_Zurich_AllRisk.sql");
        }

        public override void Down()
        {

        }
    }
}
