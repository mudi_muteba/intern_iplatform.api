using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170315091100)]
    public class Product_Additional_Excess_Update_AA_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170315091100_Product_Additional_Excess_Update_AA_01.sql");
        }

        public override void Down()
        {

        }
    }
}