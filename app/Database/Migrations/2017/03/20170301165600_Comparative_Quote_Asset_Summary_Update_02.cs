using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170301165600)]
    public class Comparative_Quote_Asset_Summary_Update_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170301165600_Comparative_Quote_Asset_Summary_Update_02.sql");
        }

        public override void Down()
        {

        }
    }
}