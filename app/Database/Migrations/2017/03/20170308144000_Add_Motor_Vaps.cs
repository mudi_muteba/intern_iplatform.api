﻿using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170308144000)]
    public class Add_Motor_Vaps : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170308144000_Add_Motor_Vaps.sql");
        }

        public override void Down()
        {

        }
    }
}
