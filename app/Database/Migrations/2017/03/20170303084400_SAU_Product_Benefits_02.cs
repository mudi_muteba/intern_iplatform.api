using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170303084400)]
    public class SAU_Product_Benefits_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170303084400_SAU_Product_Benefits_02.sql");
        }

        public override void Down()
        {

        }
    }
}