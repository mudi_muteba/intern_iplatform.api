using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170302142030)]
    public class Update_Report_IsVisibleInScheduler : Migration
    {
        public override void Up()
        {
            Update.Table("Report").Set(new { IsVisibleInScheduler = false }).Where(new { Name = "User List" });
            Update.Table("Report").Set(new { IsVisibleInScheduler = false }).Where(new { Name = "Broker Quotes and Uploads" });
        }

        public override void Down()
        {

        }
    }
}