﻿using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170316084500)]
    public class Add_Component_KP_MotorClaim_QuestionDefinitions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170316084500_Add_Component_KP_MotorClaim_QuestionDefinitions.sql");
        }

        public override void Down()
        {

        }
    }
}
