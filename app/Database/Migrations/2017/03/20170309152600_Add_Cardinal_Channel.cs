﻿using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170309152600)]
    public class Add_Cardinal_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170309152600_Add_Cardinal_Channel.sql");
        }

        public override void Down()
        {

        }
    }
}
