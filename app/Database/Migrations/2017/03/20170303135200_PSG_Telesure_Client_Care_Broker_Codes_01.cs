using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170303135200)]
    public class PSG_Telesure_Client_Care_Broker_Codes_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170303135200_PSG_Telesure_Client_Care_Broker_Codes_01.sql");
        }

        public override void Down()
        {

        }
    }
}