using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170302152000)]
    public class Insurer_Data_Overflow_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170302152000_Insurer_Data_Overflow_01.sql");
        }

        public override void Down()
        {

        }
    }
}