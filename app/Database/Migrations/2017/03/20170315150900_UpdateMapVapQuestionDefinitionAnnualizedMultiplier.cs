using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170315150900)]
    public class _20170315150900_UpdateMapVapQuestionDefinitionAnnualizedMultiplier : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170315150900_UpdateMapVapQuestionDefinitionAnnualizedMultiplier.sql");
        }

        public override void Down()
        {

        }
    }
}