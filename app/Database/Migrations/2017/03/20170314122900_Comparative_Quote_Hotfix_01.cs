using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170314122900)]
    public class Comparative_Quote_Hotfix_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170314122900_Comparative_Quote_Hotfix_01.sql");
        }

        public override void Down()
        {

        }
    }
}