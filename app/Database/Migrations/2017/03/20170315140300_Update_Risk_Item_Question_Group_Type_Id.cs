using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170315140300)]
    public class Update_Risk_Item_Question_Group_Type_Id : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170315140300_Update_Risk_Item_Question_Group_Type_Id.sql");
        }

        public override void Down()
        {

        }
    }
}