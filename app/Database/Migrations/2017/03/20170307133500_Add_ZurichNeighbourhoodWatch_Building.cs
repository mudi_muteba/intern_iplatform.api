﻿using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170307133500)]
    public class Add_ZurichNeighbourhoodWatch_Building : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170307133500_Add_ZurichNeighbourhoodWatch_Building.sql");
        }

        public override void Down()
        {

        }
    }
}
