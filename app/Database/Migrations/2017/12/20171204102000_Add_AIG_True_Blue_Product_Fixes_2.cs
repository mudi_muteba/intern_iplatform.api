﻿using FluentMigrator;

namespace Database.Migrations._2017._12
{
    [Migration(20171204102003)]
    public class Add_AIG_True_Blue_Product_Fixes_2 : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171204102000_Add_AIG_True_Blue_Product_Fixes_2.sql");
        }

        public override void Down()
        {

        }
    }
}
