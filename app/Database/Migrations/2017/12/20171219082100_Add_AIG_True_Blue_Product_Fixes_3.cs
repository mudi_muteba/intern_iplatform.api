﻿using FluentMigrator;

namespace Database.Migrations._2017._12
{
    [Migration(20171219082100)]
    public class Add_AIG_True_Blue_Product_Fixes_3 : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171219082100_Add_AIG_True_Blue_Product_Fixes_3.sql");
        }

        public override void Down()
        {

        }
    }
}
