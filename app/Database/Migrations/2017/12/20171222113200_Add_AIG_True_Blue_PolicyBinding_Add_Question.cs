﻿using FluentMigrator;

namespace Database.Migrations._2017._12
{
    [Migration(20171222113200)]
    public class Add_AIG_True_Blue_PolicyBinding_Add_Question : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171222113200_Add_AIG_True_Blue_PolicyBinding_Add_Question.sql");
        }

        public override void Down()
        {

        }
    }
}
