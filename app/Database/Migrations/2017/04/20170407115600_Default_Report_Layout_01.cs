﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170407115600)]
    public class Default_Report_Layout_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170407115600_Default_Report_Layout_01.sql");
        }

        public override void Down()
        {

        }
    }
}
