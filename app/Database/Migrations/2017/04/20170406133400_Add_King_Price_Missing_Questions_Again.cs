﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170406133400)]
    public class Add_King_Price_Missing_Questions_Again : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170406133400_Add_King_Price_Missing_Questions_Again.sql");
        }

        public override void Down()
        {

        }
    }
}
