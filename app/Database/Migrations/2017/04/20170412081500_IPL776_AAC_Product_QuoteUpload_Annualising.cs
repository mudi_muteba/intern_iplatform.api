﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Database.Migrations._2017._04
{
    [Migration(20170412081500)]
    public class IPL776_AAC_Product_QuoteUpload_Annualising : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170412081500_IPL776_AAC_Product_QuoteUpload_Annualising.sql");
        }

        public override void Down()
        {

        }
    }
}
