﻿using FluentMigrator;

namespace Database.Migrations._2017._04
{
    [Migration(20170425142800)]
    public class Oakhurst_Add_Additional_Covers_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170425142800_Oakhurst_Add_Additional_Covers_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
