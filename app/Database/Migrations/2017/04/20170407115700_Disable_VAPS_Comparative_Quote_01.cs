﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170407115700)]
    public class Disable_VAPS_Comparative_Quote_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170407115700_Disable_VAPS_Comparative_Quote_01.sql");
        }

        public override void Down()
        {

        }
    }
}
