﻿using FluentMigrator;

namespace Database.Migrations._2017._04
{
    [Migration(20170419155000)]
    public class Report_ComparativeQuote_Configuration_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170419155000_Report_ComparativeQuote_Configuration_01.sql");
        }

        public override void Down()
        {

        }
    }
}
