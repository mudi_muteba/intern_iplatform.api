﻿using FluentMigrator;

namespace Database.Migrations._2017._04
{
    [Migration(20170421104400)]
    public class Bryte_Remove_Required_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170421104400_Bryte_Remove_Required_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
