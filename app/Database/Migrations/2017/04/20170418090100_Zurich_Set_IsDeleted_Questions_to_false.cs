﻿using FluentMigrator;

namespace Database.Migrations._2017._04
{
    [Migration(20170418090100)]
    public class Zurich_Set_IsDeleted_Questions_to_false : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170418090100_Zurich_Set_IsDeleted_Questions_to_false.sql");
        }

        public override void Down()
        {

        }
    }
}
