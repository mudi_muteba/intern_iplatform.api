﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Database.Migrations._2017._03
{
    [Migration(20170411140900)]
    public class _20170411140900_MigrateDataAndDropFields : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170411140900_MigrateDataAndDropFields.sql");
        }

        public override void Down()
        {

        }
    }
}
