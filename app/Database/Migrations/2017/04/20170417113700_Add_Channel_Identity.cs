﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Database.Migrations._2017._04
{
    [Migration(20170417113700)]
    public class Add_Channel_Identity : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20170417113700_Add_Channel_Identity.sql");
        }

        public override void Down()
        {

        }
    }
}
