﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._11
{
    [Migration(20171112172000)]
    public class Add_Hollard_Easy_Benefits_And_Question_Fixes : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171112172000_Add_Hollard_Easy_Benefits_And_Question_Fixes.sql");
        }

        public override void Down()
        {

        }
    }
}
