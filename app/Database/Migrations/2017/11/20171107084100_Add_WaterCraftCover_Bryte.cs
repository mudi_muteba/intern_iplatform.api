﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._11
{
    [Migration(20171107084100)]
    public class Add_WaterCraftCover_Bryte : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171107084100_Add_WaterCraftCover_Bryte.sql");
        }

        public override void Down()
        {

        }
    }
}
