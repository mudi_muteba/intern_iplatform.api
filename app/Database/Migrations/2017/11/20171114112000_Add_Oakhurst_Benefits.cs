﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._11
{
    [Migration(20171114112000)]
    public class Add_Oakhurst_Benefits : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171114112000_Add_Oakhurst_Benefits.sql");
        }

        public override void Down()
        {

        }
    }
}
