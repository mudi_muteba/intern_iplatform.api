﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._11
{
    [Migration(20171107084900)]
    public class Add_WaterCraftQuestions_On_Multiquote : Migration
    {
        public override void Up()
        {
          Execute.EmbeddedScript("20171107084900_Add_WaterCraftQuestions_On_Multiquote.sql");
        }

        public override void Down()
        {

        }
    }
}
