﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._11
{
    [Migration(20171103084500)]
    public class Update_Personal_Liability_Required_Status : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171103084500_Update_Personal_Liability_Required_Status.sql");
        }

        public override void Down()
        {

        }
    }
}
