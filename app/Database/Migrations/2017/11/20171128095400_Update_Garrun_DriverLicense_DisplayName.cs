﻿using FluentMigrator;

namespace Database.Migrations._2017._11
{
    [Migration(20171128095400)]
    public class Update_Garrun_DriverLicense_DisplayName : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171128095400_Update_Garrun_DriverLicense_DisplayName.sql");
        }

        public override void Down()
        {

        }
    }
}