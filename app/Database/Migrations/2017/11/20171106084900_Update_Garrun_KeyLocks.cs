﻿using FluentMigrator;

namespace Database.Migrations._2017._11
{
    [Migration(20171106084900)]
    public class Update_Garrun_KeyLocks : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171106084900_Update_Garrun_KeyLocks.sql");
        }

        public override void Down()
        {

        }
    }
}