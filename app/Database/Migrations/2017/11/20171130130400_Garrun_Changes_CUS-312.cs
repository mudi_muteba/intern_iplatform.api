﻿using FluentMigrator;

namespace Database.Migrations._2017._11
{
    [Migration(20171130130400)]
    public class Garrun_Changes_CUS_312 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20171130130400_Garrun_Changes_CUS-312.sql");
        }

        public override void Down()
        {

        }
    }
}