﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._11
{
    [Migration(20171122103200)]
    public class Add_AIG_True_Blue_Product_Fixes : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171122103200_Add_AIG_True_Blue_Product_Fixes.sql");
        }

        public override void Down()
        {

        }
    }
}
