﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._11
{
    [Migration(20171120122000)]
    public class Add_Hollard_Motor_Questions : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171120122000_Add_Hollard_Motor_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
