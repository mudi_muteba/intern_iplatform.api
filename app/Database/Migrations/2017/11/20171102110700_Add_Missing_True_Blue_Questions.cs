﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._11
{
    [Migration(20171102110700)]
    public class Add_Missing_True_Blue_Questions : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171102110700_Add_Missing_True_Blue_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
