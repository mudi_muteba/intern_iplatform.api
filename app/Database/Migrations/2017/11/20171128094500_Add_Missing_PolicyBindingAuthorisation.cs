﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2017._11
{
    [Migration(20171128094500)]
    public class Add_Missing_PolicyBindingAuthorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // Call center agent
                .Row(new { AuthorisationPointId = 114, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 115, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 116, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 117, AuthorisationGroupId = 1 })
                ;


            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // Call center manager
                .Row(new { AuthorisationPointId = 114, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 115, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 116, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 117, AuthorisationGroupId = 2 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // broker
                .Row(new { AuthorisationPointId = 114, AuthorisationGroupId = 3 })
                .Row(new { AuthorisationPointId = 115, AuthorisationGroupId = 3 })
                .Row(new { AuthorisationPointId = 116, AuthorisationGroupId = 3 })
                .Row(new { AuthorisationPointId = 117, AuthorisationGroupId = 3 })
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // Call center agent
                .Row(new { AuthorisationPointId = 114, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 115, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 116, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 117, AuthorisationGroupId = 1 })
                ;

            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // Call center manager
                .Row(new { AuthorisationPointId = 114, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 115, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 116, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 117, AuthorisationGroupId = 2 })
                ;

            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // Broker
                .Row(new { AuthorisationPointId = 114, AuthorisationGroupId = 3 })
                .Row(new { AuthorisationPointId = 115, AuthorisationGroupId = 3 })
                .Row(new { AuthorisationPointId = 116, AuthorisationGroupId = 3 })
                .Row(new { AuthorisationPointId = 117, AuthorisationGroupId = 3 })
                ;
        }
    }
}
