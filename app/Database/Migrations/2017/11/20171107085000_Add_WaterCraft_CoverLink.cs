﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2017._11
{
    [Migration(20171107085000)]
    public class Add_WaterCraft_CoverLink : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20171107085000_Add_WaterCraft_CoverLink.sql");
        }

        public override void Down()
        {

        }
    }
}
