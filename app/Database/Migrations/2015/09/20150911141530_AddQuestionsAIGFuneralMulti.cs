﻿using FluentMigrator;

namespace iBroker.Database.Migrations
{
    [Migration(20150911141530)]
    public class AddQuestionsAIGFuneralMulti : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150911141530_AddQuestionsAIGFuneralMulti.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}