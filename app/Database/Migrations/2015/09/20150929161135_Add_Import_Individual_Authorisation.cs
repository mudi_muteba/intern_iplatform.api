using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150929161135)]
    public class Add_Import_Individual_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPoint)
                // address
                .Row(new { Id = 43, Name = "Import", AuthorisationPointCategoryId = 3 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 43, AuthorisationGroupId = 5})

                // call centre agent
                .Row(new {AuthorisationPointId = 43, AuthorisationGroupId = 1})

                // call centre manager
                .Row(new {AuthorisationPointId = 43, AuthorisationGroupId = 2})
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 43, AuthorisationGroupId = 5})

                // call centre agent
                .Row(new {AuthorisationPointId = 43, AuthorisationGroupId = 1})

                // call centre manager
                .Row(new {AuthorisationPointId = 43, AuthorisationGroupId = 2})
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new {Id = 43, Name = "Import", AuthorisationPointCategoryId = 3});

        }
    }
}