﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150916161701)]
    public class OrganizationClaimTypeExclusion : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.OrganizationClaimTypeExclusion)
                .Row(new { OrganizationId = 29, ClaimsTypeId = 2 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 3 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 4 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 5 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 6 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 7 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 8 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 9 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 10 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 11 });
        }

        public override void Down()
        {
            
        }
    }
}
