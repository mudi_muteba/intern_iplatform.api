using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150922161301)]
    public class Change_Request_Id_To_String_On_Quote_Header : Migration
    {
        public override void Up()
        {
            // copy data from RequestId to ExternalReference
            if (Schema.Table(Tables.QuoteHeader).Column("RequestId").Exists())
            {
                Execute.EmbeddedScript("20150922161301_Change_Request_Id_To_String_On_Quote_Header.up.sql");

                Delete.Column("RequestId").FromTable(Tables.QuoteHeader);
            }
        }

        public override void Down()
        {
            // copy data from ExternalReference to RequestId, where it can
            Execute.EmbeddedScript("20150922161301_Change_Request_Id_To_String_On_Quote_Header.down.sql");
        }
    }
}