﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150910132301)]
    public class _20150910132301_Rename_Relationship_Party : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.Relationship).Column("ParentPartyId").Exists())
            {
                Execute.Sql(@"EXEC sp_RENAME 'Relationship.PartyId' , 'ChildPartyId', 'COLUMN'");
                Execute.Sql(@"EXEC sp_RENAME 'Relationship.ParentPartyId' , 'PartyId', 'COLUMN'");
            }
        }

        public override void Down()
        {

        }
    }
}