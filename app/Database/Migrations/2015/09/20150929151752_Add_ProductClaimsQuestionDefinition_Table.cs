using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150929151752)]
    public class Add_ProductClaimsQuestionDefinition_Table : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150929153625_Insert_Data_ProductClaimsQuestionDefinition.sql");
        }

        public override void Down()
        {

        }
    }
}