using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150922125001)]
    public class Add_Is_Deleted_To_Quote_Tables : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.QuoteHeaderState).Column("IsDeleted").Exists())
                Alter.Table(Tables.QuoteHeaderState)
                     .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

            if (!Schema.Table(Tables.QuoteItem).Column("IsDeleted").Exists())
                Alter.Table(Tables.QuoteItem)
                     .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

            if (!Schema.Table(Tables.QuoteState).Column("IsDeleted").Exists())
                Alter.Table(Tables.QuoteState)
                     .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

            if (!Schema.Table(Tables.QuoteItemState).Column("IsDeleted").Exists())
                Alter.Table(Tables.QuoteItemState)
                     .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);
        }

        public override void Down()
        {
            
        }
    }
}