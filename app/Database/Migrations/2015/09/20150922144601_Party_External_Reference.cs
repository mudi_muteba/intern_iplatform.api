using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150922144601)]
    public class Party_External_Reference : Migration
    {
        private const string IndexName = "Party_External_Reference";

        public override void Up()
        {
            Create.Index(IndexName)
                .OnTable(Tables.Party)
                .InSchema("dbo")
                .OnColumn("ExternalReference");
        }

        public override void Down()
        {
            Delete.Index(IndexName)
                .OnTable(Tables.Party)
                .InSchema("dbo");
        }
    }
}