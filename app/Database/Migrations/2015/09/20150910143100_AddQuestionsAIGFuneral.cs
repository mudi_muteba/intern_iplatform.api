﻿using FluentMigrator;

namespace iBroker.Database.Migrations
{
    [Migration(20150910143100)]
    public class AddQuestionsAIGFuneral : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150910143100_AddQuestionsAIGFuneral.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}