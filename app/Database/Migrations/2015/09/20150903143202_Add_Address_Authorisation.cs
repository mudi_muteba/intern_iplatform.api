using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150903143202)]
    public class Add_Address_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new {Id = 14, Name = "Address"});

            Insert.IntoTable(Tables.AuthorisationPoint)
                // address
                .Row(new {Id = 29, Name = "Create", AuthorisationPointCategoryId = 14})
                .Row(new {Id = 30, Name = "Edit", AuthorisationPointCategoryId = 14})
                .Row(new {Id = 31, Name = "Delete", AuthorisationPointCategoryId = 14})
                .Row(new {Id = 32, Name = "List", AuthorisationPointCategoryId = 14})
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 29, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 30, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 31, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 32, AuthorisationGroupId = 5})

                // call centre agent
                .Row(new {AuthorisationPointId = 29, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 30, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 31, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 32, AuthorisationGroupId = 1})

                // call centre manager
                .Row(new {AuthorisationPointId = 29, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 30, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 31, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 32, AuthorisationGroupId = 2})
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 29, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 30, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 31, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 32, AuthorisationGroupId = 5})

                // call centre agent
                .Row(new {AuthorisationPointId = 29, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 30, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 31, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 32, AuthorisationGroupId = 1})

                // call centre manager
                .Row(new {AuthorisationPointId = 29, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 30, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 31, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 32, AuthorisationGroupId = 2})
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new {Id = 29, Name = "Create", AuthorisationPointCategoryId = 14})
                .Row(new {Id = 30, Name = "Edit", AuthorisationPointCategoryId = 14})
                .Row(new {Id = 31, Name = "Delete", AuthorisationPointCategoryId = 14})
                .Row(new {Id = 32, Name = "List", AuthorisationPointCategoryId = 14});

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new {Id = 14, Name = "Address"});
        }
    }
}