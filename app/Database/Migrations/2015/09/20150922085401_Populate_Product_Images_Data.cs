using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150922085401)]
    public class Populate_Product_Images_Data : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150922085401_Populate_Product_Images_Data.up.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20150922085401_Populate_Product_Images_Data.down.sql");
        }
    }
}