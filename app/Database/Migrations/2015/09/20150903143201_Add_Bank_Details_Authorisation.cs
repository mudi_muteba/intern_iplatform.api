using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150903143201)]
    public class Add_bank_details_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new {Id = 15, Name = "BankDetails"});

            Insert.IntoTable(Tables.AuthorisationPoint)
                // address
                .Row(new {Id = 33, Name = "Create", AuthorisationPointCategoryId = 15})
                .Row(new {Id = 34, Name = "Edit", AuthorisationPointCategoryId = 15})
                .Row(new {Id = 35, Name = "Delete", AuthorisationPointCategoryId = 15})
                .Row(new {Id = 36, Name = "List", AuthorisationPointCategoryId = 15})
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 33, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 34, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 35, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 36, AuthorisationGroupId = 5})

                // call centre agent
                .Row(new {AuthorisationPointId = 33, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 34, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 35, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 36, AuthorisationGroupId = 1})

                // call centre manager
                .Row(new {AuthorisationPointId = 33, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 34, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 35, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 36, AuthorisationGroupId = 2})
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 33, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 34, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 35, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 36, AuthorisationGroupId = 5})

                // call centre agent
                .Row(new {AuthorisationPointId = 33, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 34, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 35, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 36, AuthorisationGroupId = 1})

                // call centre manager
                .Row(new {AuthorisationPointId = 33, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 34, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 35, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 36, AuthorisationGroupId = 2})
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new {Id = 33, Name = "Create", AuthorisationPointCategoryId = 15})
                .Row(new {Id = 34, Name = "Edit", AuthorisationPointCategoryId = 15})
                .Row(new {Id = 35, Name = "Delete", AuthorisationPointCategoryId = 15})
                .Row(new {Id = 36, Name = "List", AuthorisationPointCategoryId = 15});

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new {Id = 15, Name = "BankDetails"});
        }
    }
}