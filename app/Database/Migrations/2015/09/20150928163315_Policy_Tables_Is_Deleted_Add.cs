using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150928163315)]
    public class Policy_Tables_Is_Deleted_Add : Migration
    {
        public override void Up()
        {
            if (Schema.Table("PolicyAccident").Column("IsDeleted").Exists())
                Update.Table("PolicyAccident").Set(new { IsDeleted = false }).AllRows();

            if (Schema.Table("PolicyAllRisk").Column("IsDeleted").Exists())
                Update.Table("PolicyAllRisk").Set(new { IsDeleted = false }).AllRows();

            if (Schema.Table("PolicyBuilding").Column("IsDeleted").Exists())
                Update.Table("PolicyBuilding").Set(new { IsDeleted = false }).AllRows();

            if (Schema.Table("PolicyContent").Column("IsDeleted").Exists())
                Update.Table("PolicyContent").Set(new { IsDeleted = false }).AllRows();

            if (Schema.Table("PolicyCoverage").Column("IsDeleted").Exists())
                Update.Table("PolicyCoverage").Set(new { IsDeleted = false }).AllRows();

            if (Schema.Table("PolicyFinance").Column("IsDeleted").Exists())
                Update.Table("PolicyFinance").Set(new { IsDeleted = false }).AllRows();

            if (Schema.Table("PolicyHeader").Column("IsDeleted").Exists())
                Update.Table("PolicyHeader").Set(new { IsDeleted = false }).AllRows();

            if (Schema.Table("PolicyItem").Column("IsDeleted").Exists())
                Update.Table("PolicyItem").Set(new { IsDeleted = false }).AllRows();

            if (Schema.Table("PolicyLeadActivity").Column("IsDeleted").Exists())
                Update.Table("PolicyLeadActivity").Set(new { IsDeleted = false }).AllRows();

            if (Schema.Table("PolicyLiability").Column("IsDeleted").Exists())
                Update.Table("PolicyLiability").Set(new { IsDeleted = false }).AllRows();

            if (Schema.Table("PolicyPersonalVehicle").Column("IsDeleted").Exists())
                Update.Table("PolicyPersonalVehicle").Set(new { IsDeleted = false }).AllRows();

            if (Schema.Table("PolicyWatercraft").Column("IsDeleted").Exists())
                Update.Table("PolicyWatercraft").Set(new { IsDeleted = false }).AllRows();
        }

        public override void Down()
        {

        }
    }
}