using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150904144601)]
    public class Contact_Rename_PartyId : Migration
    {
        private readonly string _table = Tables.Relationship;

        public override void Up()
        {
            Execute.Sql(
                @"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'PartyId' AND Object_ID = Object_ID(N'Contact')) EXEC sp_RENAME 'Contact.Id' , 'PartyId', 'COLUMN'");
            Execute.Sql(
                @"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'LanguageId' AND Object_ID = Object_ID(N'Contact')) EXEC sp_RENAME 'Contact.HomeLanguageId' , 'LanguageId', 'COLUMN'");
        }

        public override void Down()
        {
            Execute.Sql(
                @"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'Contact')) EXEC sp_RENAME 'Contact.PartyId' , 'Id', 'COLUMN'");
            Execute.Sql(
                @"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'HomeLanguageId' AND Object_ID = Object_ID(N'Contact')) EXEC sp_RENAME 'Contact.LanguageId' , 'HomeLanguageId', 'COLUMN'");
        }
    }
}