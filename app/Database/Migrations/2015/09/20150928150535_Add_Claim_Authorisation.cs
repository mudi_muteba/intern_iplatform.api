using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150928150535)]
    public class Add_Claim_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPoint)
                // address
                .Row(new { Id = 39, Name = "Create", AuthorisationPointCategoryId = 8 })
                .Row(new { Id = 40, Name = "Edit", AuthorisationPointCategoryId = 8 })
                .Row(new { Id = 41, Name = "Delete", AuthorisationPointCategoryId = 8 })
                .Row(new { Id = 42, Name = "List", AuthorisationPointCategoryId = 8 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 39, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 40, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 41, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 42, AuthorisationGroupId = 5})

                // call centre agent
                .Row(new {AuthorisationPointId = 39, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 40, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 41, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 42, AuthorisationGroupId = 1})

                // call centre manager
                .Row(new {AuthorisationPointId = 39, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 40, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 41, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 42, AuthorisationGroupId = 2})
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 39, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 40, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 41, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 42, AuthorisationGroupId = 5})

                // call centre agent
                .Row(new {AuthorisationPointId = 39, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 40, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 41, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 42, AuthorisationGroupId = 1})

                // call centre manager
                .Row(new {AuthorisationPointId = 39, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 40, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 41, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 42, AuthorisationGroupId = 2})
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new {Id = 39, Name = "Create", AuthorisationPointCategoryId = 8})
                .Row(new {Id = 40, Name = "Edit", AuthorisationPointCategoryId = 8})
                .Row(new {Id = 41, Name = "Delete", AuthorisationPointCategoryId = 8})
                .Row(new {Id = 42, Name = "List", AuthorisationPointCategoryId = 8});

        }
    }
}