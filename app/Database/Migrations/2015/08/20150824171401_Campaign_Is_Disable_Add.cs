using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150824171401)]
    public class Campaign_Is_Disable_Add : Migration
    {
        public override void Up()
        {
            Update.Table("Campaign")
                  .Set(new { IsDisabled = false })
                  .AllRows();
        }

        public override void Down()
        {
            Delete.Column("IsDisabled").FromTable("Campaign");
        }
    }
}