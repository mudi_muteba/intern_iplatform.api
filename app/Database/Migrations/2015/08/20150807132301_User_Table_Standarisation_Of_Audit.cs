using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150807132301)]
    public class User_Table_Standarisation_Of_Audit : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.User).Column("CreatedAt").Exists())
            {
                Alter.Table(Tables.User)
                     .AlterColumn("CreatedAt").AsDateTime().Nullable()
                     .AlterColumn("ModifiedAt").AsDateTime().Nullable();

                Execute.EmbeddedScript("20150807132301_User_Table_Standarisation_Of_Audit.up.sql");

                Delete
                    .Column("CreatedAt")
                    .Column("ModifiedAt")
                    .FromTable(Tables.User);
            }
        }

        public override void Down()
        {
            Alter.Table(Tables.User)
                .AddColumn("CreatedAt").AsDateTime().Nullable()
                .AddColumn("ModifiedAt").AsDateTime().Nullable();

            Execute.EmbeddedScript("20150807132301_User_Table_Standarisation_Of_Audit.down.sql");

            Delete
                .Column("CreatedOn")
                .Column("ModifiedOn")
                .FromTable(Tables.User);
        }
    }
}