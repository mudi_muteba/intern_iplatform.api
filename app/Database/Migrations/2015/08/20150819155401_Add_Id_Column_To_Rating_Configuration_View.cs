using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150819155401)]
    public class Add_Id_Column_To_Rating_Configuration_View : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150819155401_Add_Id_Column_To_Rating_Configuration_View.up.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20150819155401_Add_Id_Column_To_Rating_Configuration_View.down.sql");
        }
    }
}