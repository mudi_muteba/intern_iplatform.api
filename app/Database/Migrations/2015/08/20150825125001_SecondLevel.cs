﻿using FluentMigrator;
namespace Database.Migrations._2015._08
{
    [Migration(20150825125001)]
    public class SecondLevel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150825125001_SecondLevelQuestionDefinitions_Add.sql");
        }

        public override void Down()
        {

        }
    }
}