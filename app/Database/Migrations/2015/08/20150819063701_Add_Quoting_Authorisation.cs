using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150819063701)]
    public class Add_Quoting_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPoint)
                .Row(new {Id = 23, Name = "CanQuote", AuthorisationPointCategoryId = 6})
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 23, AuthorisationGroupId = 5})
                // call centre agent
                .Row(new {AuthorisationPointId = 23, AuthorisationGroupId = 1})
                // Call Centre Manager
                .Row(new { AuthorisationPointId = 23, AuthorisationGroupId = 2 })
                // Broker
                .Row(new {AuthorisationPointId = 23, AuthorisationGroupId = 3})
                // Client
                .Row(new {AuthorisationPointId = 23, AuthorisationGroupId = 4})
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 23, AuthorisationGroupId = 5 })
                // call centre agent
                .Row(new { AuthorisationPointId = 23, AuthorisationGroupId = 1 })
                // Call Centre Manager
                .Row(new { AuthorisationPointId = 23, AuthorisationGroupId = 2 })
                // Broker
                .Row(new { AuthorisationPointId = 23, AuthorisationGroupId = 3 })
                // Client
                .Row(new { AuthorisationPointId = 23, AuthorisationGroupId = 4 });

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 23 })
                ;
        }
    }
}