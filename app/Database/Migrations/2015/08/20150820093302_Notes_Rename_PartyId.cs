﻿using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150820093302)]
    public class Notes_Rename_PartyId : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'PartyId' AND Object_ID = Object_ID(N'Note')) EXEC sp_RENAME 'Note.ClientId' , 'PartyId', 'COLUMN'");
        }

        public override void Down()
        {
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'ClientId' AND Object_ID = Object_ID(N'Note')) EXEC sp_RENAME 'Note.PartyId' , 'ClientId', 'COLUMN'");
        }
    }
}