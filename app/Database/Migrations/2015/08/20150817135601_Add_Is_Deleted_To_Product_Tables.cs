using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150817135601)]
    public class Add_Is_Deleted_To_Product_Tables : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.Product).Column("IsDeleted").Exists())
                Alter.Table(Tables.Product)
                     .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

            Update.Table(Tables.Product)
                .Set(new {IsDeleted = false})
                .AllRows();
        }

        public override void Down()
        {
            if (Schema.Table(Tables.Product).Column("IsDeleted").Exists())
                Delete.Column("IsDeleted").FromTable(Tables.Product);
        }
    }
}