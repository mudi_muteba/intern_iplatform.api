using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150817120501)]
    public class Add_Product_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new {Id = 13, Name = "Product"})
                ;

            Insert.IntoTable(Tables.AuthorisationPoint)
                // products
                .Row(new {Id = 18, Name = "Create", AuthorisationPointCategoryId = 13})
                .Row(new {Id = 19, Name = "Edit", AuthorisationPointCategoryId = 13})
                .Row(new {Id = 20, Name = "Delete", AuthorisationPointCategoryId = 13})
                .Row(new {Id = 21, Name = "Disable", AuthorisationPointCategoryId = 13})
                .Row(new {Id = 22, Name = "List", AuthorisationPointCategoryId = 13});

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 18, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 19, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 20, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 21, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 22, AuthorisationGroupId = 5})
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 18, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 19, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 20, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 21, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 22, AuthorisationGroupId = 5 })
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 18 })
                .Row(new { Id = 19 })
                .Row(new { Id = 20 })
                .Row(new { Id = 21 })
                .Row(new { Id = 22 })
                ;

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new {Id = 13});
        }
    }
}