using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150807103501)]
    public class Add_AuthorisationPointCategory_Table : Migration
    {
        public override void Up()
        {
            if(!Schema.Table(Tables.AuthorisationPointCategory).Exists())
                Create
                .Table(Tables.AuthorisationPointCategory)
                .WithColumn("Id")
                .AsInt32()
                .NotNullable()
                .PrimaryKey()
                .WithColumn("Name").AsString(512)
                ;

            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new {Id = 1, Name = "Campaign"})
                .Row(new {Id = 2, Name = "User"})
                .Row(new {Id = 3, Name = "Individual"})
                .Row(new {Id = 4, Name = "Organisation"})
                .Row(new {Id = 5, Name = "Proposal"})
                .Row(new {Id = 6, Name = "Quoting"})
                .Row(new {Id = 7, Name = "SecondLevelUnderwriting"})
                .Row(new {Id = 8, Name = "Claims"})

                .Row(new {Id = 9, Name = "PersonalInformationLookup"})
                .Row(new {Id = 10, Name = "CreditScoring"})
                .Row(new {Id = 11, Name = "VehicleAssetLookup"})
                .Row(new {Id = 12, Name = "HomeAssetLookup"})
                ;
        }

        public override void Down()
        {
            Delete.Table(Tables.AuthorisationPointCategory);
        }
    }
}