using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150819103201)]
    public class Party_Channel_Id_Add : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.Party).Column("ChannelId").Exists())
                Alter.Table(Tables.Party)
                     .AddColumn("ChannelId").AsInt32().Nullable();

            Update.Table(Tables.Party).Set(new { ChannelId = 1 }).AllRows();
        }

        public override void Down()
        {
            if (Schema.Table(Tables.Party).Column("ChannelId").Exists())
                Delete.Column("ChannelId").FromTable(Tables.Party);
        }
    }
}