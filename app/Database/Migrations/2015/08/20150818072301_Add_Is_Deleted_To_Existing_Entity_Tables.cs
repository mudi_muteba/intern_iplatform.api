using System.Collections.Generic;
using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150818072301)]
    public class Add_Is_Deleted_To_Existing_Entity_Tables_3 : Migration
    {
        private readonly List<string> tables = new List<string>()
        {
            Tables.SettingsiRate
        };

        public override void Up()
        {
            foreach (var table in tables)
            {
                if (!Schema.Table(table).Column("IsDeleted").Exists())
                {
                    if (!Schema.Table(table).Column("IsDeleted").Exists())
                        Alter.Table(table)
                             .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

                    Update.Table(table)
                        .Set(new {IsDeleted = false})
                        .AllRows();
                }
            }
        }

        public override void Down()
        {
            foreach (var table in tables)
                if (Schema.Table(table).Column("IsDeleted").Exists())
                    Delete.Column("IsDeleted").FromTable(table);
        }
    }
}