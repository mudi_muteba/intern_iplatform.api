using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150807132201)]
    public class Make_Token_Field_On_User_Table_Smaller : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.User).Column("Token").Exists())
            {
                Update
                    .Table(Tables.User)
                    .Set(new { Token = "" })
                    .AllRows();

                Alter.Table(Tables.User)
                     .AlterColumn("Token").AsString(1500).Nullable();
            }

            Alter.Table(Tables.User)
                .AlterColumn("LoggedInFromSystem").AsString(128).Nullable();
        }

        public override void Down()
        {
            Alter.Table(Tables.User)
                .AlterColumn("Token").AsString(1500).Nullable();

            Alter.Table(Tables.User)
                .AlterColumn("LoggedInFromSystem").AsString(128).Nullable();
        }
    }
}