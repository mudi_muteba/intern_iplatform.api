using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150814082201)]
    public class Add_Individual_Authorisation_Points : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPoint)
                // individual
                .Row(new {Id = 12, Name = "Create", AuthorisationPointCategoryId = 3})
                .Row(new {Id = 13, Name = "Edit", AuthorisationPointCategoryId = 3})
                .Row(new {Id = 14, Name = "Delete", AuthorisationPointCategoryId = 3})
                .Row(new {Id = 15, Name = "Disable", AuthorisationPointCategoryId = 3})
                .Row(new {Id = 16, Name = "MakeDefault", AuthorisationPointCategoryId = 3})
                .Row(new {Id = 17, Name = "List", AuthorisationPointCategoryId = 3})
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 12, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 13, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 14, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 15, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 16, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 17, AuthorisationGroupId = 5 })

                // call centre agent
                .Row(new { AuthorisationPointId = 12, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 13, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 14, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 15, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 16, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 17, AuthorisationGroupId = 1 })

                // call centre manager
                .Row(new { AuthorisationPointId = 12, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 13, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 14, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 15, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 16, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 17, AuthorisationGroupId = 2 })
                ;

        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 12, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 13, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 14, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 15, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 16, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 17, AuthorisationGroupId = 5 })

                // call centre agent
                .Row(new { AuthorisationPointId = 12, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 13, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 14, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 15, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 16, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 17, AuthorisationGroupId = 1 })

                // call centre manager
                .Row(new { AuthorisationPointId = 12, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 13, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 14, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 15, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 16, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 17, AuthorisationGroupId = 2 })
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new {Id = 12, Name = "Create", AuthorisationPointCategoryId = 3})
                .Row(new {Id = 13, Name = "Edit", AuthorisationPointCategoryId = 3})
                .Row(new {Id = 14, Name = "Delete", AuthorisationPointCategoryId = 3})
                .Row(new {Id = 15, Name = "Disable", AuthorisationPointCategoryId = 3})
                .Row(new {Id = 16, Name = "MakeDefault", AuthorisationPointCategoryId = 3})
                .Row(new {Id = 17, Name = "List", AuthorisationPointCategoryId = 3});

        }
    }
}