using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150820070501)]
    public class Add_SecondLevelUnderwriting_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPoint)
                .Row(new {Id = 24, Name = "CanAcceptQuote", AuthorisationPointCategoryId = 7})
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 24, AuthorisationGroupId = 5})
                // call centre agent
                .Row(new {AuthorisationPointId = 24, AuthorisationGroupId = 1})
                // Call Centre Manager
                .Row(new { AuthorisationPointId = 24, AuthorisationGroupId = 2 })
                // Broker
                .Row(new {AuthorisationPointId = 24, AuthorisationGroupId = 3})
                // Client
                .Row(new {AuthorisationPointId = 24, AuthorisationGroupId = 4})
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 24, AuthorisationGroupId = 5 })
                // call centre agent
                .Row(new { AuthorisationPointId = 24, AuthorisationGroupId = 1 })
                // Call Centre Manager
                .Row(new { AuthorisationPointId = 24, AuthorisationGroupId = 2 })
                // Broker
                .Row(new { AuthorisationPointId = 24, AuthorisationGroupId = 3 })
                // Client
                .Row(new { AuthorisationPointId = 24, AuthorisationGroupId = 4 });

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 24 })
                ;
        }
    }
}