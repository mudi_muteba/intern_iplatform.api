using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150819080501)]
    public class Add_Rating_Configuration_View : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150819080501_Add_Rating_Configuration_View.up.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20150819080501_Add_Rating_Configuration_View.down.sql");
        }
    }
}