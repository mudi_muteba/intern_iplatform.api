using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151012105701)]
    public class Add_Quote_Item_To_Quote_Item_State_Table : Migration
    {
        public override void Up()
        {

        }

        public override void Down()
        {
            Execute.EmbeddedScript("20151014153601_Drop_Index_IX_QuoteItemState_QuoteItemId.sql");
        }
    }
}