using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._12
{
    [Migration(20151201155101)]
    public class LeadQuality_Is_Deleted_Add : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.LeadQuality).Column("IsDeleted").Exists())
                Update.Table(Tables.LeadQuality)
                  .Set(new { IsDeleted = false })
                  .AllRows();
        }

        public override void Down()
        {
            
        }
    }
}