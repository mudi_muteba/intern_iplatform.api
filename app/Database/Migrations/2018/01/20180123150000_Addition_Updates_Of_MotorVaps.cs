﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180123150000)]
    public class Addition_Updates_Of_MotorVaps : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20180123150000_Addition_Updates_Of_MotorVaps.sql");
        }

        public override void Down()
        {

        }
    }
}
