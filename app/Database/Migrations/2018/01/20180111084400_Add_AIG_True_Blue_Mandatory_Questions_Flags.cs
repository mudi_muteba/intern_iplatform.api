﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180111084400)]
    public class Add_AIG_True_Blue_Mandatory_Questions_Flags : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20180111084400_Add_AIG_True_Blue_Mandatory_Questions_Flags.sql");
        }

        public override void Down()
        {

        }
    }
}
