﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180129160000)]
    public class Add_Update_Cover_Definitions_Limits : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20180129160000_Add_Update_Cover_Definitions_Limits.sql");
        }

        public override void Down()
        {

        }
    }
}

