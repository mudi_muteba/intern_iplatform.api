﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180109160100)]
    public class Add_Report_PolicyBindingReportHollard : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20180109160100_Add_Report_PolicyBindingReportHollard.sql");
        }

        public override void Down()
        {

        }
    }
}