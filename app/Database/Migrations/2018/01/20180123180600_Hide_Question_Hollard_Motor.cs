﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180123180600)]
    public class Hide_Question_Hollard_Motor : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20180123180600_Hide_Question_Hollard_Motor.sql");
        }

        public override void Down()
        {

        }
    }
}
