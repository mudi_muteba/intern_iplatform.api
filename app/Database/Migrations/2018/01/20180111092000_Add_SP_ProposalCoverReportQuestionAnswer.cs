﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180111092000)]
    public class Add_SP_ProposalCoverReportQuestionAnswer: Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20180111092000_Add_SP_ProposalCoverReportQuestionAnswer.sql");
        }

        public override void Down()
        {
        }
    }
}