﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180103142300)]
    public class Add_AIG_True_Blue_Motorcycle_Questions_Fixes : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20180103142300_Add_AIG_True_Blue_Motorcycle_Questions_Fixes.sql");
        }

        public override void Down()
        {

        }
    }
}
