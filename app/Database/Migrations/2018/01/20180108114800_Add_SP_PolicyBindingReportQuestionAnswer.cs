﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180108114800)]
    public class Add_SP_PolicyBindingReportQuestionAnswer : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20180108114800_Add_SP_PolicyBindingReportQuestionAnswer.sql");
        }

        public override void Down()
        {

        }
    }
}