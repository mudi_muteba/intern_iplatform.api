﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180112142400)]
    public class Add_ITC_Questions_For_Channels : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20180112142400_Add_ITC_Questions_For_Channels.sql");
        }

        public override void Down()
        {

        }
    }
}
