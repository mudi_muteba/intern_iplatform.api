﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20181028160000)]
    public class Add_Cover_Definitions_Limits : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20181028160000_Add_Cover_Definitions_Limits.sql");
        }

        public override void Down()
        {

        }
    }
}

