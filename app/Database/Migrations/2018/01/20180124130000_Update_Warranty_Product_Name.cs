﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180124130000)]
    public class Update_Warranty_Product_Name : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20180124130000_Update_Warranty_Product_Name.sql");
        }

        public override void Down()
        {

        }
    }
}
