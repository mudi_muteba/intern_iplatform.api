﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180124150000)]
    public class Add_New_Products : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20180124150000_Add_New_Products.sql");
        }

        public override void Down()
        {

        }
    }
}
