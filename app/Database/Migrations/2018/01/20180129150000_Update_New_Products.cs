﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180129150000)]
    public class Update_New_Products : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20180129150000_Update_New_Products.sql");
        }

        public override void Down()
        {

        }
    }
}

