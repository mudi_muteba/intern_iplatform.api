﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180130092500)]
    public class Update_Report_PolicyBindingReportHollard : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20180130092500_Update_Report_PolicyBindingReportHollard.sql");
        }

        public override void Down()
        {

        }
    }
}