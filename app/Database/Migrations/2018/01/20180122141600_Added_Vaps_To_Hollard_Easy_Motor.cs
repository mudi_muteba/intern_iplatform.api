﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180122141600)]
    public class Added_Vaps_To_Hollard_Easy_Motor : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20180122141600_Added_Vaps_To_Hollard_Easy_Motor.sql");
        }

        public override void Down()
        {

        }
    }
}
