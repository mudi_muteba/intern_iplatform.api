﻿using FluentMigrator;

namespace Database.Migrations._2018._01
{
    [Migration(20180124090000)]
    public class Hide_Existing_Questions : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20180124090000_Hide_Existing_Questions.sql");
        }

        public override void Down()
        {

        }
    }
}
