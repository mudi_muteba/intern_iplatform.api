﻿using FluentMigrator;

namespace Database.Migrations._2018._02
{
    [Migration(20180203160000)]
    public class Add_MotorVAP_Benefits : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20180203160000_Add_MotorVAP_Benefits.sql");
        }

        public override void Down()
        {

        }
    }
}

