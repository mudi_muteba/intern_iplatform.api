﻿using FluentMigrator;

namespace Database.Migrations._2018._02
{
    [Migration(20180203140000)]
    public class Hide_Questions_MotorVAPS_V2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20180203140000_Hide_Questions_MotorVAPS_V2.sql");
        }

        public override void Down()
        {

        }
    }
}