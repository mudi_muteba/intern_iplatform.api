﻿using FluentMigrator;

namespace Database.Migrations._2018._02
{
    [Migration(20180205180400)]
    public class Add_Miway_Benefits : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20180205180400_Add_Miway_Benefits.sql");
        }

        public override void Down()
        {

        }
    }
}

