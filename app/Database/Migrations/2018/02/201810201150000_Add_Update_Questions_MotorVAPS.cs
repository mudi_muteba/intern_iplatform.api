﻿using FluentMigrator;

namespace Database.Migrations._2018._02
{
    [Migration(201810201150000)]
    public class Add_Update_Questions_MotorVAPS : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("201810201150000_Add_Update_Questions_MotorVAPS.sql");
        }

        public override void Down()
        {

        }
    }
}

