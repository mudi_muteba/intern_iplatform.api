﻿using FluentMigrator;

namespace Database.Migrations._2018._02
{
    [Migration(20180201140000)]
    public class Hide_Questions_MotorVAPS : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20180201140000_Hide_Questions_MotorVAPS.sql");
        }

        public override void Down()
        {

        }
    }
}