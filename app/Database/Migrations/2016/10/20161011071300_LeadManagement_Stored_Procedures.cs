using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161011071300)]
    public class LeadManagement_Stored_Procedures : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161011071300_LeadManagement_Stored_Procedures.sql");
        }

        public override void Down()
        {
            
        }
    }
}