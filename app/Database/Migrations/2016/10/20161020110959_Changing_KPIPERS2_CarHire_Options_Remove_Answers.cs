using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020110959)]
    public class Changing_KPIPERS2_CarHire_Options_Remove_Answers : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020110959_Changing_KPIPERS2_CarHire_Options_Remove_Answers.sql");
        }

        public override void Down()
        {
            
        }
    }
}