﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161024064000)]
    public class Update_Report_Tooltips_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161024064000_Update_Report_Tooltips_01.sql");
        }

        public override void Down()
        {

        }
    }
}