﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161024075100)]
    public class Update_Report_Tooltips_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161024075100_Update_Report_Tooltips_02.sql");
        }

        public override void Down()
        {

        }
    }
}