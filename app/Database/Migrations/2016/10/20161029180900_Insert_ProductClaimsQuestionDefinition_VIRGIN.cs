﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161029180900)]
    public class Insert_ProductClaimsQuestionDefinition_VIRGIN : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161029180900_Insert_ProductClaimsQuestionDefinition_VIRGIN.sql");
        }

        public override void Down()
        {

        }
    }
}