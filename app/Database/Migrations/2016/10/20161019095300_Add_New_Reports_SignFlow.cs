﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161019095300)]
    public class Add_New_Reports_SignFlow : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161019095300_Add_New_Reports_SignFlow.sql");
        }

        public override void Down()
        {

        }
    }
}