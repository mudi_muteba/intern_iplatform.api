using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161014113500)]
    public class Add_Telesure_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161014113500_Add_Telesure_Channel.sql");
        }

        public override void Down()
        {
            
        }
    }
}