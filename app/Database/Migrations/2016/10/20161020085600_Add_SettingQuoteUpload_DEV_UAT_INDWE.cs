﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020085600)]
    public class Add_SettingQuoteUpload_DEV_UAT_INDWE : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020085600_Add_SettingQuoteUpload_DEV_UAT_INDWE.sql");
        }

        public override void Down()
        {

        }
    }
}