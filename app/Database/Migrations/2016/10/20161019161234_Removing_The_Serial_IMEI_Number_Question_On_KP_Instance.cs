using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161019161234)]
    public class Removing_The_Serial_IMEI_Number_Question_On_KP_Instance : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161019161234_Removing_The_Serial_IMEI_Number_Question_On_KP_Instance.sql");
        }

        public override void Down()
        {
            
        }
    }
}