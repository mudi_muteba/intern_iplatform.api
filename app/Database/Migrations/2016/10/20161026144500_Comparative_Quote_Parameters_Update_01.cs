﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161026144500)]
    public class Comparative_Quote_Parameters_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161026144500_Comparative_Quote_Parameters_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}