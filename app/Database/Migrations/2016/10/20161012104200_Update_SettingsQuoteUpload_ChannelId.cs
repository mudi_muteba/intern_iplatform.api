using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161012104200)]
    public class Update_SettingsQuoteUpload_ChannelId : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161012104200_Update_SettingsQuoteUpload_ChannelId.sql");
        }

        public override void Down()
        {
            
        }
    }
}