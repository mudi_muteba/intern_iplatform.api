﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161028141400)]
    public class Update_SP_Reports_AIG_MotorExtractV3 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161028141400_Update_SP_Reports_AIG_MotorExtractV3.sql");
        }

        public override void Down()
        {

        }
    }
}