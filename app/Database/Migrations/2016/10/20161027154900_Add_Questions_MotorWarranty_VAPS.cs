﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161027154900)]
    public class Add_Questions_MotorWarranty_VAPS : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161027154900_Add_Questions_MotorWarranty_VAPS.sql");
        }

        public override void Down()
        {

        }
    }
}