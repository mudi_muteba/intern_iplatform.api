using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020111012)]
    public class Update_Default_Values_For_UAP_Excess_Political_Violence_Field : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020111012_Update_Default_Values_For_UAP_Excess_Political_Violence_Field.sql");
        }

        public override void Down()
        {
            
        }
    }
}