﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020153500)]
    public class Add_SettingQuoteUpload_LIVE_KP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020153500_Add_SettingQuoteUpload_LIVE_KP.sql");
        }

        public override void Down()
        {

        }
    }
}