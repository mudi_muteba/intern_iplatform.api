﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161019092700)]
    public class Update_MaximumLeadsPerAgent_Default_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161019092700_Update_MaximumLeadsPerAgent_Default_01.sql");
        }

        public override void Down()
        {

        }
    }
}