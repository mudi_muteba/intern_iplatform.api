using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020072359)]
    public class Changing_KPIPERS2_CarHire_Options : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020072359_Changing_KPIPERS2_CarHire_Options.sql");
        }

        public override void Down()
        {
            
        }
    }
}