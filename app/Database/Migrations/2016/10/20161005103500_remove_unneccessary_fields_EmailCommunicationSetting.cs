using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161005103500)]
    public class remove_unneccessary_fields_EmailCommunicationSetting : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161005103500_remove_unneccessary_fields_EmailCommunicationSetting.sql");
        }

        public override void Down()
        {
            
        }
    }
}