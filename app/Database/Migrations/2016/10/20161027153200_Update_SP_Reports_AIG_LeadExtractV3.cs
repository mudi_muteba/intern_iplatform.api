﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161027153200)]
    public class Update_SP_Reports_AIG_LeadExtractV3 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161027153200_Update_SP_Reports_AIG_LeadExtractV3.sql");
        }

        public override void Down()
        {

        }
    }
}
