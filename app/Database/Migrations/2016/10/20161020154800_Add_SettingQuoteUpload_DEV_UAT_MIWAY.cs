﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020154800)]
    public class Add_SettingQuoteUpload_DEV_UAT_MIWAY : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020154800_Add_SettingQuoteUpload_DEV_UAT_MIWAY.sql");
        }

        public override void Down()
        {

        }
    }
}