using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161007095700)]
    public class Report_Default_Data_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161007095700_Report_Default_Data_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}