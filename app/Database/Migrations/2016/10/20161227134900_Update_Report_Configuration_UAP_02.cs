﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161227134900)]
    public class Update_Report_Configuration_UAP_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161227134900_Update_Report_Configuration_UAP_02.sql");
        }

        public override void Down()
        {

        }
    }
}