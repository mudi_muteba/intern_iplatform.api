﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161026113500)]
    public class Update_Report_Param_SP_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161026113500_Update_Report_Param_SP_01.sql");
        }

        public override void Down()
        {

        }
    }
}