﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020154300)]
    public class Add_SettingQuoteUpload_DEV_UAT_UAP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020154300_Add_SettingQuoteUpload_DEV_UAT_UAP.sql");
        }

        public override void Down()
        {

        }
    }
}