using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161019123500)]
    public class Add_MiWay_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161019123500_Add_MiWay_Channel.sql");
        }

        public override void Down()
        {
            
        }
    }
}