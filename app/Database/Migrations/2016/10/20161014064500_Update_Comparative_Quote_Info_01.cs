using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161014064500)]
    public class Update_Comparative_Quote_Info_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161014064500_Update_Comparative_Quote_Info_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}