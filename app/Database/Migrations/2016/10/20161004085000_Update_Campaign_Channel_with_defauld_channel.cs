using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161004085000)]
    public class Update_Campaign_Channel_with_defauld_channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161004085000_Update_Campaign_Channel_with_defauld_channel.sql");
        }

        public override void Down()
        {
            
        }
    }
}