using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(2016101011490)]
    public class InsurerQuoteBreakdown_Stored_Procedures : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161010114900_InsurerQuoteBreakdown_Stored_Procedures.sql");
        }

        public override void Down()
        {
            
        }
    }
}