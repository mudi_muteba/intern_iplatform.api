using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161007074200)]
    public class Update_Report_List_SPs_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161007074200_Update_Report_List_SPs_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}