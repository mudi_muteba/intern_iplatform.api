using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161010121200)]
    public class Update_Default_Values_For_UAP_Standard_Cover_Fields : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161010121200_Update_Default_Values_For_UAP_Standard_Cover_Fields.sql");
        }

        public override void Down()
        {
            
        }
    }
}