using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161005093500)]
    public class Add_PSG_Client_Care_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161005093500_Add_PSG_Client_Care_Channel.sql");
        }

        public override void Down()
        {
            
        }
    }
}