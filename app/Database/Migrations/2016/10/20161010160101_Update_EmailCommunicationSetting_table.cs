using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161010160101)]
    public class Update_EmailCommunicationSetting_table : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161010160101_Update_EmailCommunicationSetting_table.sql");
        }

        public override void Down()
        {
            
        }
    }
}