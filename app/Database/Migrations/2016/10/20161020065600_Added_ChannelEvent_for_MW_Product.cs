using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020065600)]
    public class Added_ChannelEvent_for_MW_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020065600_Added_ChannelEvent_for_MW_Product.sql");
        }

        public override void Down()
        {
            
        }
    }
}