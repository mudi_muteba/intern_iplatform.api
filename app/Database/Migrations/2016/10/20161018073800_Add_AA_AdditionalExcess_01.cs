﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161018073800)]
    public class Add_AA_AdditionalExcess_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161018073800_Add_AA_AdditionalExcess_01.sql");
        }

        public override void Down()
        {

        }
    }
}