﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161026114200)]
    public class InsurerCode_ChannelSettings : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161026114200_InsurerCode_ChannelSettings.sql");
        }

        public override void Down()
        {

        }
    }
}