using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161005165700)]
    public class Add_Last_Claim_Question_AA_Vehicle_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161005165700_Add_Last_Claim_Question_AA_Vehicle_Product.sql");
        }

        public override void Down()
        {
            
        }
    }
}