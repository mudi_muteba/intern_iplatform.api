﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161017143200)]
    public class Update_Indwe_Email_Settings_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161017143200_Update_Indwe_Email_Settings_01.sql");
        }

        public override void Down()
        {

        }
    }
}