using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161007063600)]
    public class Add_New_Reports_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161007063600_Add_New_Reports_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}