﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161026170020)]
    public class Adding_Auto_Pedigree_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161026170020_Adding_Auto_Pedigree_Channel.sql");
        }

        public override void Down()
        {

        }
    }
}