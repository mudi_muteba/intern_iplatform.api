﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161025144700)]
    public class Add_SettingQuoteUpload_LIVE_BOWER : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161025144700_Add_SettingQuoteUpload_LIVE_BOWER.sql");
        }

        public override void Down()
        {

        }
    }
}