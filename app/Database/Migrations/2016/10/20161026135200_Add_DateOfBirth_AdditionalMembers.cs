﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161026135200)]
    public class Add_DateOfBirth_AdditionalMembers : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.AdditionalMembers).Column("DateOfBirth").Exists())
                Alter.Table(Tables.AdditionalMembers)
                 .AddColumn("DateOfBirth")
                    .AsDateTime()
                    .Nullable();
        }

        public override void Down()
        {

        }
    }
}