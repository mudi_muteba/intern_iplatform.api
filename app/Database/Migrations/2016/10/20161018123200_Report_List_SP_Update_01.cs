﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161018123200)]
    public class Report_List_SP_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161018123200_Report_List_SP_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}