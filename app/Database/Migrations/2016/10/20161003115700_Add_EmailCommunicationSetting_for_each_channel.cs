using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161003115700)]
    public class Add_EmailCommunicationSetting_for_each_channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161003115700_Add_EmailCommunicationSetting_for_each_channel.sql");
        }

        public override void Down()
        {
            
        }
    }
}