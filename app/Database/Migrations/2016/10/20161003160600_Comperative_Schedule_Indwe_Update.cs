using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20161003160600)]
    public class Comperative_Schedule_Indwe_Update : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161003160600_Comperative_Schedule_Indwe_Update.sql");
        }

        public override void Down()
        {
            
        }
    }
}