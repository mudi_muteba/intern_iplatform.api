﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161025161810)]
    public class UAP_ADDRESS_AND_Province_Changes : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161025161810_UAP_ADDRESS_AND_Province_Changes.sql");
        }

        public override void Down()
        {

        }
    }
}