using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161010111901)]
    public class Removed_KP_Hail_Cover_From_MQ : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161010111901_Removed_KP_Hail_Cover_From_MQ.sql");
        }

        public override void Down()
        {
            
        }
    }
}