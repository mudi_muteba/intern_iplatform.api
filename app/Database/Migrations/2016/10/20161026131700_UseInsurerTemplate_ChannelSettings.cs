﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161026131700)]
    public class UseInsurerTemplate_ChannelSettings : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161026131700_UseInsurerTemplate_ChannelSettings.sql");
        }

        public override void Down()
        {

        }
    }
}