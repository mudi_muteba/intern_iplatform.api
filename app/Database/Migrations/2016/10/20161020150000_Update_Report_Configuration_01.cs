using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020150000)]
    public class Update_Report_Configuration_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020150000_Update_Report_Configuration_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}