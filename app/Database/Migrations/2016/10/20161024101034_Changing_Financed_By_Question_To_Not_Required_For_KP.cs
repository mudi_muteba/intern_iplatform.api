﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161024101034)]
    public class Changing_Financed_By_Question_To_Not_Required_For_ : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161024101034_Changing_Financed_By_Question_To_Not_Required_For_KP.sql");
        }

        public override void Down()
        {

        }
    }
}