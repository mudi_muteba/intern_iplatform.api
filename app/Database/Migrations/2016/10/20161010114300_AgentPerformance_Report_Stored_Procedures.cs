using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161010114300)]
    public class AgentPerformance_Report_Stored_Procedures : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161010114300_AgentPerformance_Report_Stored_Procedures.sql");
        }

        public override void Down()
        {
            
        }
    }
}