﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161021160000)]
    public class Add_VAPS_Products_Extended_PreOwned_Warranty : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161021160000_Add_VAPS_Products_Extended_PreOwned_Warranty.sql");
        }

        public override void Down()
        {

        }
    }
}