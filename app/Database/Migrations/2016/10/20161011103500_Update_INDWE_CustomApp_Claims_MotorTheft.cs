using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161011103500)]
    public class Update_INDWE_CustomApp_Claims_MotorTheft : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161011103500_Update_INDWE_CustomApp_Claims_MotorTheft.sql");
        }

        public override void Down()
        {
            
        }
    }
}