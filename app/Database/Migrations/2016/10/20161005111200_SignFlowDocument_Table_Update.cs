﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161005111200)]
    public class SignFlowDocument_Table_Update : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161005111200_SignFlowDocument_Table_Update.sql");
        }

        public override void Down()
        {

        }
    }
}