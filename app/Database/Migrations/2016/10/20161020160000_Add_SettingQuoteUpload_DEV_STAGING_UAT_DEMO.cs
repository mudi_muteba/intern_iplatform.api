﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020160000)]
    public class Add_SettingQuoteUpload_DEV_STAGING_UAT_DEMO : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020160000_Add_SettingQuoteUpload_DEV_STAGING_UAT_DEMO.sql");
        }

        public override void Down()
        {

        }
    }
}