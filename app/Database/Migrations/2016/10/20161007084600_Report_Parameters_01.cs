using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161007084600)]
    public class Report_Parameters_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161007084600_Report_Parameters_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}