﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020154500)]
    public class Add_SettingQuoteUpload_LIVE_UAP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020154500_Add_SettingQuoteUpload_LIVE_UAP.sql");
        }

        public override void Down()
        {

        }
    }
}