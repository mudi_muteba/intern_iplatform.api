using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161007100801)]
    public class Added_ChannelEvent_for_AA_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161007100801_Added_ChannelEvent_for_AA_Product.sql");
        }

        public override void Down()
        {
            
        }
    }
}