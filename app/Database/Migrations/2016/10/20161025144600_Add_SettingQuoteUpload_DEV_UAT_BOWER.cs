﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161025144600)]
    public class Add_SettingQuoteUpload_DEV_UAT_BOWER : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161025144600_Add_SettingQuoteUpload_DEV_UAT_BOWER.sql");
        }

        public override void Down()
        {

        }
    }
}