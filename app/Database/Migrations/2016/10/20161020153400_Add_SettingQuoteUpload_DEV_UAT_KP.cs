﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020153400)]
    public class Add_SettingQuoteUpload_DEV_UAT_KP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020153400_Add_SettingQuoteUpload_DEV_UAT_KP.sql");
        }

        public override void Down()
        {

        }
    }
}