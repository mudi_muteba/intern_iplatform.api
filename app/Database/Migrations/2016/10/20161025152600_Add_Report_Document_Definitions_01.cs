﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161025152600)]
    public class Add_Report_Document_Definitions_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161025152600_Add_Report_Document_Definitions_01.sql");
        }

        public override void Down()
        {

        }
    }
}