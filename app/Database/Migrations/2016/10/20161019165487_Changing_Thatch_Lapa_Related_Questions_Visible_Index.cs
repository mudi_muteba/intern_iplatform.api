using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161019165487)]
    public class Changing_Thatch_Lapa_Related_Questions_Visible_Index : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161019165487_Changing_Thatch_Lapa_Related_Questions_Visible_Index.sql");
        }

        public override void Down()
        {
            
        }
    }
}