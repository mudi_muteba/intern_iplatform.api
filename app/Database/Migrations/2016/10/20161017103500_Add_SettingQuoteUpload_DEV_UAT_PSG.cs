﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161017103500)]
    public class Add_SettingQuoteUpload_DEV_UAT_PSG : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161017103500_Add_SettingQuoteUpload_DEV_UAT_PSG.sql");
        }

        public override void Down()
        {

        }
    }
}