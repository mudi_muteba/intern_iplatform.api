using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161019155500)]
    public class Add_KP_Tracking_Device_Question : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161019155500_Add_KP_Tracking_Device_Question.sql");
        }

        public override void Down()
        {
            
        }
    }
}