﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161031162900)]
    public class Update_SP_Reports_AIG_BuildingExtractV3 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161031162900_Update_SP_Reports_AIG_BuildingExtractV3.sql");
        }

        public override void Down()
        {

        }
    }
}