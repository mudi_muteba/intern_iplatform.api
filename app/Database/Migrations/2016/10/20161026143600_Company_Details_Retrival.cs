﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161026143600)]
    public class Company_Details_Retrival : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161026143600_Company_Details_Retrival.sql");
        }

        public override void Down()
        {

        }
    }
}

