using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161019164567)]
    public class Hiding_Vehicle_Milage_Question_On_KP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161019164567_Hiding_Vehicle_Milage_Question_On_KP.sql");
        }

        public override void Down()
        {
            
        }
    }
}