using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161006111300)]
    public class Update_SalesFNI_removing_party_constraint : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161006111300_Update_SalesFNI_removing_party_constraint.sql");
        }

        public override void Down()
        {
            
        }
    }
}