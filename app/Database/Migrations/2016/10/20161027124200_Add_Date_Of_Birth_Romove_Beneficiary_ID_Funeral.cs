﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161027124200)]
    public class Add_Date_Of_Birth_Romove_Beneficiary_ID_Funeral : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161027124200_Add_Date_Of_Birth_Romove_Beneficiary_ID_Funeral.sql");
        }

        public override void Down()
        {

        }
    }
}