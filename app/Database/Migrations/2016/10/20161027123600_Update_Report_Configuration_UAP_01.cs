﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161027123600)]
    public class Update_Report_Configuration_UAP_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161027123600_Update_Report_Configuration_UAP_01.sql");
        }

        public override void Down()
        {

        }
    }
}