using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161019161765)]
    public class Adding_Missing_All_Risk_Category_Choice_On_KP_Instance : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161019161765_Adding_Missing_All_Risk_Category_Choice_On_KP_Instance.sql");
        }

        public override void Down()
        {
            
        }
    }
}