﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161025140300)]
    public class Creating_BOWER_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161025140300_Creating_BOWER_Channel.sql");
        }

        public override void Down()
        {

        }
    }
}