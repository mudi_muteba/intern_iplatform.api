﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161027090000)]
    public class Update_SettingsQuoteUplaod_INDWE_SA : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161027090000_Update_SettingsQuoteUpload_INDWE_SAU.sql");
        }

        public override void Down()
        {

        }
    }
}