using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161011112800)]
    public class CallCentre_Reporting_Calculation_Updates_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161011112800_CallCentre_Reporting_Calculation_Updates_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}