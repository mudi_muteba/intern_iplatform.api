using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161019155987)]
    public class Hide_Or_Show_Thatch_Lapa_Related_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161019155987_Hide_Or_Show_Thatch_Lapa_Related_Questions.sql");
        }

        public override void Down()
        {
            
        }
    }
}