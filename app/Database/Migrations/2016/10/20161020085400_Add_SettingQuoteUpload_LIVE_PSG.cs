﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020085400)]
    public class Add_SettingQuoteUpload_LIVE_PSG : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020085400_Add_SettingQuoteUpload_LIVE_PSG.sql");
        }

        public override void Down()
        {

        }
    }
}