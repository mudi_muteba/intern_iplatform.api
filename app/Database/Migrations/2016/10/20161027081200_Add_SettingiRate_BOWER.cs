﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161027081200)]
    public class Add_SettingiRate_BOWER : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161027081200_Add_SettingiRate_BOWER.sql");
        }

        public override void Down()
        {

        }
    }
}