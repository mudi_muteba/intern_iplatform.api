﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161020094700)]
    public class Add_SettingQuoteUpload_LIVE_INDWE : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161020094700_Add_SettingQuoteUpload_LIVE_INDWE.sql");
        }

        public override void Down()
        {

        }
    }
}