using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161005101011)]
    public class Add_Personal_Accident_MultiQuote_MiWay : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161005101011_Add_Personal_Accident_MultiQuote_MiWay.sql");
        }

        public override void Down()
        {
            
        }
    }
}