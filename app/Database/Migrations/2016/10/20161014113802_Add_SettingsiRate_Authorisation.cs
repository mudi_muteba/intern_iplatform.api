﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161014113802)]
    public class Add_SettingsiRate_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 25, Name = "SettingsiRate" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                // address
                .Row(new { Id = 71, Name = "Create", AuthorisationPointCategoryId = 25 })
                .Row(new { Id = 72, Name = "Edit", AuthorisationPointCategoryId = 25 })
                .Row(new { Id = 73, Name = "Delete", AuthorisationPointCategoryId = 25 })
                .Row(new { Id = 74, Name = "List", AuthorisationPointCategoryId = 25 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 71, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 72, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 73, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 74, AuthorisationGroupId = 5 });
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 71, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 72, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 73, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 74, AuthorisationGroupId = 5 });

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 71, Name = "Create", AuthorisationPointCategoryId = 25 })
                .Row(new { Id = 72, Name = "Edit", AuthorisationPointCategoryId = 25 })
                .Row(new { Id = 73, Name = "Delete", AuthorisationPointCategoryId = 25 })
                .Row(new { Id = 74, Name = "List", AuthorisationPointCategoryId = 25 });

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 25, Name = "SettingsiRate" });
        }
    }
}