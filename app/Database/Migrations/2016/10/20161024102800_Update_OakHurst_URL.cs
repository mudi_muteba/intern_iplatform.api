﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161024102800)]
    public class Update_OakHurst_URL : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161024102800_Update_OakHurst_URL.sql");
        }

        public override void Down()
        {

        }
    }
}