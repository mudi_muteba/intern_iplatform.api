﻿using FluentMigrator;

namespace Database.Migrations._2016._10
{
    [Migration(20161031124200)]
    public class Update_SP_Reports_AIG_ContentExtractV3 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161031124200_Update_SP_Reports_AIG_ContentExtractV3.sql");
        }

        public override void Down()
        {

        }
    }
}