﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._01
{
    [Migration(20160118140401)]
    public class OrganizationPolicyServiceSetting_Add_IsDeleted : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.OrganizationPolicyServiceSetting).Column("IsDeleted").Exists())
                Update.Table(Tables.OrganizationPolicyServiceSetting)
                  .Set(new {IsDeleted = false})
                  .AllRows();
        }

        public override void Down()
        {
            
        }
    }
}
