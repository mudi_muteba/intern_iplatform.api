﻿using System;
using FluentMigrator;
using Database.Schema;

namespace Database.Migrations._2016._01
{
    [Migration(201601141041)]
    public class Occupation_Add_IsDeleted : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.Occupation).Column("IsDeleted").Exists())
                Update.Table(Tables.Occupation)
                  .Set(new {IsDeleted = false})
                  .AllRows();
        }

        public override void Down()
        {
            
        }
    }
}
