﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._01
{
    [Migration(20160118145501)]
    public class PolicyServiceSetting_Add_IsDeleted : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.PolicyServiceSetting).Column("IsDeleted").Exists())
                Update.Table(Tables.PolicyServiceSetting)
                  .Set(new {IsDeleted = false})
                  .AllRows();
        }

        public override void Down()
        {
            
        }
    }
}
