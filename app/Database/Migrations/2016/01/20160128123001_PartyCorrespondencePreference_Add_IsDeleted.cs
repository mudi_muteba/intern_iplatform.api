﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._01
{
    [Migration(20160128123001)]
    public class PartyCorrespondencePreference_Add_IsDeleted : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.PartyCorrespondencePreference).Column("IsDeleted").Exists())
                Update.Table(Tables.PartyCorrespondencePreference)
                  .Set(new {IsDeleted = false})
                  .AllRows();
        }

        public override void Down()
        {
            
        }
    }
}
