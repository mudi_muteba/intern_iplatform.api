﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._01
{
    [Migration(20160122094601)]
    public class Update_Channel_Language_to_Languageid : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.Channel).Column("Language").Exists())
                Delete.Column("Language").FromTable(Tables.Channel);

            Update.Table(Tables.Channel).Set(new {LanguageId = 2}).AllRows();
        }

        public override void Down()
        {
            
        }
    }
}