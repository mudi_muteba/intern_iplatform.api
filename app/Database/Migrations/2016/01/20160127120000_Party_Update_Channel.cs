﻿using FluentMigrator;
using Database.Schema;

namespace Database.Migrations._2016._01
{
    [Migration(20160127120000)]
    public class Party_Update_Channel : Migration
    {
        public override void Up()
        {
            Update.Table(Tables.Party)
                .Set(new { ChannelId = 1 })
                .Where(new { ChannelId = (int?)null });
        }

        public override void Down()
        {
            
        }
    }
}
