﻿using System;
using FluentMigrator;

namespace Database.Migrations._2016._01
{
    [Migration(20160114114301)]
    // 20160114114301_Extend_iGuide_Settings
    public class Extend_iGuide_Settings : Migration
    {
        private const string TableName = "VehicleGuideSetting";

        public override void Up()
        {
            Update.Table(TableName)
                  .Set(new {LightstoneEnabled = true})
                  .AllRows();

            Alter.Column("LightstoneEnabled")
                  .OnTable(TableName)
                  .AsBoolean().NotNullable().WithDefaultValue(true);
        }

        public override void Down()
        {

        }
    }
}
