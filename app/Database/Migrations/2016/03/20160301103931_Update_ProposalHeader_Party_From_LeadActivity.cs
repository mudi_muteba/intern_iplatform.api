﻿using FluentMigrator;

namespace Database.Migrations._2016._03
{
    [Migration(20160301103931)]
    public class Update_ProposalHeader_Party_From_LeadActivity : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160301103931_Update_ProposalHeader_Party_From_LeadActivity.sql");
        }

        public override void Down()
        {
            
        }
    }
}