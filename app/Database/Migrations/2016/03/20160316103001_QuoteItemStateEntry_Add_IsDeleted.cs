﻿using FluentMigrator;
using Database.Schema;

namespace Database.Migrations._2016._03
{
    [Migration(20160316103001)]
    public class QuoteItemStateEntry_Add_IsDeleted : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.QuoteItemStateEntry).Column("IsDeleted").Exists())
            {
                Update.Table(Tables.QuoteItemStateEntry)
                    .Set(new {IsDeleted = false})
                    .AllRows();
            }
        }

        public override void Down()
        {

        }
    }
}
