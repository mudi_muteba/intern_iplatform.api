using FluentMigrator;

namespace Database.Migrations._2016._02
{
    [Migration(20160202140030)]
    public class Add_Root_User : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160202140030_Insert_Root_User.sql");
        }

        public override void Down()
        {
            
        }
    }
}