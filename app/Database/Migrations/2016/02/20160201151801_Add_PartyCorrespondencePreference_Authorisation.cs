using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._02
{
    [Migration(20160201151801)]
    public class Add_PartyCorrespondencePreference_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 21, Name = "CorrespondenceReference" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                // individual
                .Row(new { Id = 57, Name = "Create", AuthorisationPointCategoryId = 21 })
                .Row(new { Id = 58, Name = "Edit", AuthorisationPointCategoryId = 21 })
                .Row(new { Id = 59, Name = "Delete", AuthorisationPointCategoryId = 21 })
                .Row(new { Id = 60, Name = "List", AuthorisationPointCategoryId = 21 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 57, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 58, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 59, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 60, AuthorisationGroupId = 5 })

                // call centre agent
                .Row(new { AuthorisationPointId = 57, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 58, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 59, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 60, AuthorisationGroupId = 1 })

                // call centre manager
                .Row(new { AuthorisationPointId = 57, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 58, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 59, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 60, AuthorisationGroupId = 2 })
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 57, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 58, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 59, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 60, AuthorisationGroupId = 5 })

                // call centre agent
                .Row(new { AuthorisationPointId = 57, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 58, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 59, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 60, AuthorisationGroupId = 1 })

                // call centre manager
                .Row(new { AuthorisationPointId = 57, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 58, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 59, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 60, AuthorisationGroupId = 2 })
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 57, Name = "Create", AuthorisationPointCategoryId = 21 })
                .Row(new { Id = 58, Name = "Edit", AuthorisationPointCategoryId = 21 })
                .Row(new { Id = 59, Name = "Delete", AuthorisationPointCategoryId = 21 })
                .Row(new { Id = 60, Name = "List", AuthorisationPointCategoryId = 21 })
                ;

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 21, Name = "CorrespondenceReference" });
        }
    }
}