﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._02
{
    [Migration(20160217153801)]
    public class PolicyHeaderClaimableItem_Add_IsDeleted : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.PolicyHeaderClaimableItem).Column("IsDeleted").Exists())
                Update.Table(Tables.PolicyHeaderClaimableItem)
                  .Set(new {IsDeleted = false})
                  .AllRows();
        }

        public override void Down()
        {

        }
    }
}
