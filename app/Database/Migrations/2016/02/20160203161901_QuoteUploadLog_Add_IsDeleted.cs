﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._02
{
    [Migration(20160203161901)]
    public class QuoteUploadLog_Add_IsDeleted : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.QuoteUploadLog).Column("IsDeleted").Exists())
                Update.Table(Tables.QuoteUploadLog)
                  .Set(new {IsDeleted = false})
                  .AllRows();
        }

        public override void Down()
        {

        }
    }
}
