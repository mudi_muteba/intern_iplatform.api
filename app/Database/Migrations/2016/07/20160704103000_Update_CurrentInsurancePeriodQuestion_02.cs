using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160704103000)]
    public class Update_CurrentInsurancePeriodQuestion_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160704103000_Update_CurrentInsurancePeriodQuestion_02.sql");
        }

        public override void Down()
        {
            
        }
    }
}