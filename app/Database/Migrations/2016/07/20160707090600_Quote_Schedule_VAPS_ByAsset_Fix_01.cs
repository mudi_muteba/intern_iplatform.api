using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160707090600)]
    public class Quote_Schedule_VAPS_ByAsset_Fix_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160707090600_Quote_Schedule_VAPS_ByAsset_Fix_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}