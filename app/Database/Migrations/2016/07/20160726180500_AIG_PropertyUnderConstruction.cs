using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160726180500)]
    public class AIG_PropertyUnderConstructio : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160726180500_AIG_PropertyUnderConstruction.sql");
        }

        public override void Down()
        {
            
        }
    }
}