using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160721114100)]
    public class Hide_Show_Motor_And_Building_Based_On_Credit_ShortFall_And_Thatch_Lapa_V2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160721114100_Hide_Show_Motor_And_Building_Based_On_Credit_ShortFall_And_Thatch_Lapa_V2.sql");
        }

        public override void Down()
        {
            
        }
    }
}