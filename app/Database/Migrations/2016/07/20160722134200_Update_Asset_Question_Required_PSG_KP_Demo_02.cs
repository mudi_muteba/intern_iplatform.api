using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160722134200)]
    public class Update_Asset_Question_Required_PSG_KP_Demo_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160722134200_Update_Asset_Question_Required_PSG_KP_Demo_02.sql");
        }

        public override void Down()
        {
            
        }
    }
}