using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160701110000)]
    public class Update_Multi_Qoute_Dummy_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160701110000_Update_Multi_Qoute_Dummy_Questions.sql");
        }

        public override void Down()
        {
            
        }
    }
}