using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160701112001)]
    public class Update_MapQuestions_For_The_MultiQoute : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160701112001_Update_MapQuestions_For_The_MultiQoute.sql");
        }

        public override void Down()
        {
            
        }
    }
}