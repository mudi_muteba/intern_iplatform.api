using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160718094453)]
    public class Update_KPVersion2_Unoccupied : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160718094453_Update_KPVersion2_Unoccupied.sql");
        }

        public override void Down()
        {
            
        }
    }
}