using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160708083459)]
    public class Update_Sort_for_KingPrice : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160708083459_Update_Sort_for_KingPrice.sql");
        }

        public override void Down()
        {
            
        }
    }
}