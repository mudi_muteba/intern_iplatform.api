using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160707071800)]
    public class Quote_Schedule_Asset_By_Cover_Fix_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160707071800_Quote_Schedule_Asset_By_Cover_Fix_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}