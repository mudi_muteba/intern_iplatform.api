using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160712130700)]
    public class Quote_Schedule_Removed_Content_Excess_On_Building : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160712130700_Quote_Schedule_Removed_Content_Excess_On_Building.sql");
        }

        public override void Down()
        {
            
        }
    }
}