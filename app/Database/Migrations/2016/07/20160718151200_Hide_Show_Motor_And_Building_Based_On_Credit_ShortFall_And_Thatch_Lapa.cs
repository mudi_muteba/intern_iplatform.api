using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160718151200)]
    public class Hide_Show_Motor_And_Building_Based_On_Credit_ShortFall_And_Thatch_Lapa : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160718151200_Hide_Show_Motor_And_Building_Based_On_Credit_ShortFall_And_Thatch_Lapa.sql");
        }
           
        public override void Down()
        {
            
        }
    }
}