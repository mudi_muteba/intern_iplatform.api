using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160728114601)]
    public class SalesForceStatusLog_SP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160728114601_SalesForceStatusLog_SP.sql");
        }

        public override void Down()
        {
            
        }
    }
}