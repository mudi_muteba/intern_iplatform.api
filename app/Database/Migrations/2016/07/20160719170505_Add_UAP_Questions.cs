using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160719170505)]
    public class Add_UAP_Questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160719170505_Add_UAP_Questions.sql");
        }

        public override void Down()
        {
            
        }
    }
}