using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160706115701)]
    public class Domestic_Quote_Schedule_Changes_All_AIG_Covers : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160706115701_Domestic_Quote_Schedule_Changes_All_AIG_Covers.sql");
        }

        public override void Down()
        {
            
        }
    }
}