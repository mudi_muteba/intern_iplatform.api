using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160715124900)]
    public class Update_Asset_Question_Required_PSG_KP_Demo_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160715124900_Update_Asset_Question_Required_PSG_KP_Demo_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}