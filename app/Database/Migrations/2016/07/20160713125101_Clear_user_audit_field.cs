using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160713125101)]
    public class Clear_user_audit_field : Migration
    {
        public override void Up()
        {
            Execute.Sql("update [user] set audit = null");
        }

        public override void Down()
        {
            
        }
    }
}