using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160714154500)]
    public class Update_Required_First_For_Woman_ProductBenefit_per_product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160714154500_Update_Required_First_For_Woman_ProductBenefit_per_product.sql");
        }

        public override void Down()
        {
            
        }
    }
}