using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160727135100)]
    public class Update_ComparativeQuoteReport_StoredProcedures_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160727135100_Update_ComparativeQuoteReport_StoredProcedures_02.sql");
        }

        public override void Down()
        {
            
        }
    }
}