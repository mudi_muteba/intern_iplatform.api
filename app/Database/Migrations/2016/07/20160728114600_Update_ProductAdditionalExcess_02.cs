using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160728114600)]
    public class Update_ProductAdditionalExcess_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160728114600_Update_ProductAdditionalExcess_02.sql");
        }

        public override void Down()
        {
            
        }
    }
}