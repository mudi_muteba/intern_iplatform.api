using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160714160000)]
    public class Update_Required_Auto_General_Insurance_ProductBenefit_per_product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160714160000_Update_Required_Auto_General_Insurance_ProductBenefit_per_product.sql");
        }

        public override void Down()
        {
            
        }
    }
}