using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160714155000)]
    public class Update_Required_Unity_Insurance_ProductBenefit_per_product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160714155000_Update_Required_Unity_Insurance_ProductBenefit_per_product.sql");
        }

        public override void Down()
        {
            
        }
    }
}