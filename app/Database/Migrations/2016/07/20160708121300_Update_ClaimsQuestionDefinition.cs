using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160708121300)]
    public class Update_ClaimsQuestionDefinitio : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160708121300_Update_ClaimsQuestionDefinition.sql");
        }

        public override void Down()
        {
            
        }
    }
}