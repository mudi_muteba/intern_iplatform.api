using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160705165901)]
    public class Quote_Schedule_Value_Producs_Covers_Updated : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160705165901_Quote_Schedule_Value_Producs_Covers_Updated.sql");
        }

        public override void Down()
        {
            
        }
    }
}