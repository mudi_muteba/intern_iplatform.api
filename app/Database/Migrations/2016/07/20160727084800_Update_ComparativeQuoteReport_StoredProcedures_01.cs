using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160727084800)]
    public class Update_ComparativeQuoteReport_StoredProcedures_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160727084800_Update_ComparativeQuoteReport_StoredProcedures_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}