using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160727084600)]
    public class Update_ProductAdditionalExcess_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160727084600_Update_ProductAdditionalExcess_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}