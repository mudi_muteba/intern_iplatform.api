using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160725101900)]
    public class Drop_MotorUninterruptedPolicyNoClaim_Column_Form_LossHistory : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160725101900_Drop_MotorUninterruptedPolicyNoClaim_Column_Form_LossHistory.sql");
        }

        public override void Down()
        {
            
        }
    }
}