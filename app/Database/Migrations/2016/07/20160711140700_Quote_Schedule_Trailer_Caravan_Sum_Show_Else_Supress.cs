using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160711140700)]
    public class Quote_Schedule_Trailer_Caravan_Sum_Show_Else_Supress : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160711140700_Quote_Schedule_Trailer_Caravan_Sum_Show_Else_Supress.sql");
        }

        public override void Down()
        {
            
        }
    }
}