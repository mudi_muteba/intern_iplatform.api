using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160704075400)]
    public class Update_CurrentInsurancePeriodQuestion_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160704075400_Update_CurrentInsurancePeriodQuestion_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}