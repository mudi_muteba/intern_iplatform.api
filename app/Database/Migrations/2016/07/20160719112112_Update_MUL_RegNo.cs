using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160719112112)]
    public class Update_MUL_RegNo : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160719112112_Update_MUL_RegNo.sql");
        }

        public override void Down()
        {
            
        }
    }
}