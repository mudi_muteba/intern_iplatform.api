using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160704113001)]
    public class Disable_KP_KPIPERS_And_Enable_KPIPERS2_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160704113001_Disable_KP_KPIPERS_And_Enable_KPIPERS2_Product.sql");
        }

        public override void Down()
        {
            
        }
    }
}