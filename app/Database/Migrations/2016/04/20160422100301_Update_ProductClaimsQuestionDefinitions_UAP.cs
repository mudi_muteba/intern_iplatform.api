﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._04
{
    [Migration(20160422100301)]
    public class Update_ProductClaimsQuestionDefinitions_UAP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160422100301_Update_ProductClaimsQuestionDefinitions_UAP.sql");
        }

        public override void Down()
        {

        }
    }
}