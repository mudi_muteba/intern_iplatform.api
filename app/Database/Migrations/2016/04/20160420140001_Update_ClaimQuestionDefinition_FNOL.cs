﻿using FluentMigrator;

namespace Database.Migrations._2016._04
{
    [Migration(20160420140001)]
    public class Update_ClaimQuestionDefinition_FNOL : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160420140001_Update_ClaimQuestionDefinition_FNOL.sql");
        }

        public override void Down()
        {

        }
    }
}