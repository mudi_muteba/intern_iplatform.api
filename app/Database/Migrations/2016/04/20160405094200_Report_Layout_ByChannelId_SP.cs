using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._04
{
    [Migration(20160405094200)]
    public class Report_Layout_ByChannelId_SP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160405094200_Report_Layout_ByChannelId_SP.sql");
        }

        public override void Down()
        {

        }
    }
}