using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._04
{
    [Migration(20160405144700)]
    public class ReportList_SPs : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160405144700_ReportList_SPs.sql");
        }

        public override void Down()
        {

        }
    }
}