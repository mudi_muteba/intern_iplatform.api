﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._04
{
    [Migration(20160420153801)]
    public class Update_ProductClaimsQuestionDefinitions_FNOL : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160420153801_Update_ProductClaimsQuestionDefinitions_FNOL.sql");
        }

        public override void Down()
        {

        }
    }
}