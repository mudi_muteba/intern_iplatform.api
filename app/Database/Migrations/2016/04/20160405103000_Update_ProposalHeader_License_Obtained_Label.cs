﻿using FluentMigrator;

namespace iBroker.Database.Migrations
{
    [Migration(20160405103000)]
    public class Update_ProposalHeader_License_Obtained_Label : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160405103000_Update_ProposalHeader_License_Obtained_Label.sql");
        }

        public override void Down()
        {
            
        }
    }
}