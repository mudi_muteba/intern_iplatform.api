using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160809092000)]
    public class Delete_Status_From_IndividualUpload_Tables : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160809092000_Delete_Status_From_IndividualUpload_Tables.sql");
        }

        public override void Down()
        {
        }
    }
}