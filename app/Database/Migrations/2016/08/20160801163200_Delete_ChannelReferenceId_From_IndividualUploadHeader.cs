using FluentMigrator;

namespace Database.Migrations._2016._08
{
[Migration(20160801163200)]
    public class Delete_ChannelReferenceId_From_IndividualUploadHeader : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160801163200_Delete_ChannelReferenceId_From_IndividualUploadHeader.sql");
        }

        public override void Down()
        {
            
        }
    }
}