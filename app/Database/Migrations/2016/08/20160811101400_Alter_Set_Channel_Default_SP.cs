using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160811101400)]
    public class Alter_Set_Channel_Default_SP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160811101400_Alter_Set_Channel_Default_SP.sql");
        }

        public override void Down()
        {

        }
    }
}