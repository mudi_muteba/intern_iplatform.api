using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160822094501)]
    public class BankDetails_Nullable : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160822094501_BankDetails_Nullable.sql");
        }

        public override void Down()
        {

        }
    }
}