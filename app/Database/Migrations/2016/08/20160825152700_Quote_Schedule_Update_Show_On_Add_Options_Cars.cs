using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160825152700)]
    public class Quote_Schedule_Update_Show_On_Add_Options_Cars : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160825152700_Quote_Schedule_Update_Show_On_Add_Options_Cars.sql");
        }

        public override void Down()
        {

        }
    }
}