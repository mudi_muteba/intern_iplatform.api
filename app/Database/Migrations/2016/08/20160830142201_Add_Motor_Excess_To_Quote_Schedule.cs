using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160830142201)]
    public class Add_Motor_Excess_To_Quote_Schedule : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160830142201_Add_Motor_Excess_To_Quote_Schedule.sql");
        }

        public override void Down()
        {
            
        }
    }
}