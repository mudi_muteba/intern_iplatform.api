using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160801114601)]
    public class CleanUp_ChannelIds : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160801114601_CleanUp_ChannelIds.sql");
        }

        public override void Down()
        {
            
        }
    }
}