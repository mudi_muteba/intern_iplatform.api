using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160812133600)]
    public class Add_LossHistoryQuestionDefinition : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160812133600_Add_LossHistoryQuestionDefinition.sql");
        }

        public override void Down()
        {

        }
    }
}