using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160804105201)]
    public class Set_Default_Channel_SP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160804105201_Set_Default_Channel_SP.sql");
        }

        public override void Down()
        {
            
        }
    }
}