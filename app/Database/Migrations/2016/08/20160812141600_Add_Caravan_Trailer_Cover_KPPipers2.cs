using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160812141600)]
    public class Add_Caravan_Trailer_Cover_KPPipers2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160812141600_Add_Caravan_Trailer_Cover_KPPipers2.sql");
        }

        public override void Down()
        {

        }
    }
}