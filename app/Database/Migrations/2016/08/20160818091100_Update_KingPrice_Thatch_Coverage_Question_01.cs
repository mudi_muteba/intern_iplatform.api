using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160818091100)]
    public class Update_KingPrice_Thatch_Coverage_Question_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160818091100_Update_KingPrice_Thatch_Coverage_Question_01.sql");
        }

        public override void Down()
        {

        }
    }
}