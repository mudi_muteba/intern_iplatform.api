using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160801114604)]
    public class DEMO_Channels_update : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160801114604_DEMO_Channels_update.sql");
        }

        public override void Down()
        {
            
        }
    }
}