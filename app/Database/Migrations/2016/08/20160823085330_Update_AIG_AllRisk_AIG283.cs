using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160823085330)]
    public class Update_AIG_AllRisk_AIG283 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160823085330_Update_AIG_AllRisk_AIG283.sql");
        }

        public override void Down()
        {

        }
    }
}