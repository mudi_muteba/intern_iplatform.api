using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160823110130)]
    public class Clean_Up_Escalations : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.EscalationPlan).Constraint("FK_EscalationPlan_Event_EventId").Exists())
                Delete.ForeignKey("FK_EscalationPlan_Event_EventId").OnTable(Tables.EscalationPlan);
            if (Schema.Table(Tables.EscalationPlan).Constraint("FK_EscalationPlan_Channel_ChannelId").Exists())
                Delete.ForeignKey("FK_EscalationPlan_Channel_ChannelId").OnTable(Tables.EscalationPlan);
            if (Schema.Table(Tables.EscalationPlan).Constraint("FK_EscalationPlan_ChannelReference_ChannelId").Exists())
                Delete.ForeignKey("FK_EscalationPlan_ChannelReference_ChannelId").OnTable(Tables.EscalationPlan);
            if (Schema.Table(Tables.EscalationPlan).Constraint("FK_EscalationPlan_Product_ProductId").Exists())
                Delete.ForeignKey("FK_EscalationPlan_Product_ProductId").OnTable(Tables.EscalationPlan);

            //if (Schema.Table(Tables.EscalationPlan).Exists())
            //{
            //    //Delete.Column("EventId").FromTable(Tables.EscalationPlan);
            //    //Delete.Column("ChannelId").FromTable(Tables.EscalationPlan);
            //    //Delete.Column("ProductId").FromTable(Tables.EscalationPlan);
            //}

            if (Schema.Table("Event").Exists())
                Delete.Table("Event");

            if (Schema.Table("UserSecurityRole").Exists())
                Delete.Table("UserSecurityRole");

            if (Schema.Table("SecurityRole").Exists())
                Delete.Table("SecurityRole");

            if (Schema.Table(Tables.EscalationPlanStep).Column("Delay").Exists())
                Delete.Column("Delay").FromTable(Tables.EscalationPlanStep);

            if (Schema.Table(Tables.EscalationPlanStep).Exists())
            {
                Alter.Column("DelayType").OnTable(Tables.EscalationPlanStep).AsString();
                Alter.Column("IntervalType").OnTable(Tables.EscalationPlanStep).AsString();
            }

            if (Schema.Table(Tables.EscalationPlanStepWorkflowMessage).Column("WorkflowMessageEnumType").Exists())
                Delete.Column("WorkflowMessageEnumType").FromTable(Tables.EscalationPlanStepWorkflowMessage);

            if (Schema.Table(Tables.EscalationPlanExecutionHistory).Column("EscalationWorkflowMessageStatusEnumType").Exists())
                Alter.Column("EscalationWorkflowMessageStatusEnumType").OnTable(Tables.EscalationPlanExecutionHistory).AsString();
        }

        public override void Down()
        {
            
        }
    }
}