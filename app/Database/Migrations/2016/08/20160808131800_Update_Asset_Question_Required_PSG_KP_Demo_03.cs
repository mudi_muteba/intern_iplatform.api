using FluentMigrator;

namespace Database.Migrations._2016._07
{
    [Migration(20160808131800)]
    public class Update_Asset_Question_Required_PSG_KP_Demo_03 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160808131800_Update_Asset_Question_Required_PSG_KP_Demo_03.sql");
        }

        public override void Down()
        {
            
        }
    }
}