using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160824085100)]
    public class Update_Work_Address_To_DayTime_Address : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160824085100_Update_Work_Address_To_DayTime_Address.sql");
        }

        public override void Down()
        {

        }
    }
}