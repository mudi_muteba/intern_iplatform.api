using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160804114559)]
    public class Add_UAPQuestionsToMUL_Version2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160804114559_Add_UAPQuestionsToMUL_Version2.sql");
        }

        public override void Down()
        {
            
        }
    }
}