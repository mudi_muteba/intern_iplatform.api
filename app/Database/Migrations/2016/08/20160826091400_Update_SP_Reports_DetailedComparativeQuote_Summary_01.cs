using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160826091400)]
    public class Update_SP_Reports_DetailedComparativeQuote_Summary_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160826091400_Update_SP_Reports_DetailedComparativeQuote_Summary_01.sql");
        }

        public override void Down()
        {

        }
    }
}