using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160830140001)]
    public class Update_Person_Setting_Add_Claim_Drivers_Columns : Migration
    {
        public override void Up()
        {

            if (!Schema.Table("PersonLookupSetting").Column("TransUnionGetClaims").Exists())
            {
                Alter.Table("PersonLookupSetting")
               .AddColumn("TransUnionGetClaims").AsBoolean().Nullable();
            }

            if (!Schema.Table("PersonLookupSetting").Column("TransUnionGetDrivers").Exists())
            {
                Alter.Table("PersonLookupSetting")
                .AddColumn("TransUnionGetDrivers").AsBoolean().Nullable();
            }
        }

        public override void Down()
        {

        }
    }
}