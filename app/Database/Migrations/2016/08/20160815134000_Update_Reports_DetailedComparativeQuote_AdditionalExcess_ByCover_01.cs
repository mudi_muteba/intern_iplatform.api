using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160815134000)]
    public class Update_Reports_DetailedComparativeQuote_AdditionalExcess_ByCover_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160815134000_Update_Reports_DetailedComparativeQuote_AdditionalExcess_ByCover_01.sql");
        }

        public override void Down()
        {

        }
    }
}