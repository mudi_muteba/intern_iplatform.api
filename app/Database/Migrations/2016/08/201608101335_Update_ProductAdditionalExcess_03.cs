using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(201608101335)]
    public class Update_ProductAdditionalExcess_03 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("201608101335_Update_ProductAdditionalExcess_03.sql");
        }

        public override void Down()
        {
            
        }
    }
}