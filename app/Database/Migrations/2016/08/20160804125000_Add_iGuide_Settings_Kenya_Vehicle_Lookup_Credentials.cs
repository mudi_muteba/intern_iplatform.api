using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160804125000)]
    public class Add_iGuide_Settings_Kenya_Vehicle_Lookup_Credentials : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.VehicleGuideSetting).Column("KeAutoKey").Exists())
            {
                Alter.Table("VehicleGuideSetting")
                    .AddColumn("KeAutoKey")
                    .AsString()
                    .Nullable();
            }

            if (!Schema.Table(Tables.VehicleGuideSetting).Column("KeAutoSecret").Exists())
            {
                Alter.Table("VehicleGuideSetting")
                    .AddColumn("KeAutoSecret")
                    .AsString()
                    .Nullable();
            }
        }

        public override void Down()
        {
            Delete.Column("KeAutoKey").Column("KeAutoSecret").FromTable("VehicleGuideSetting");
        }
    }
}