using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160801114602)]
    public class KP_UAG_Channels : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160801114602_KP_UAG_Channels.sql");
        }

        public override void Down()
        {
            
        }
    }
}