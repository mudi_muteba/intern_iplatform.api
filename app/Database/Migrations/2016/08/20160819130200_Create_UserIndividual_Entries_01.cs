using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160819130200)]
    public class Create_UserIndividual_Entries_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160819130200_Create_UserIndividual_Entries_01.sql");
        }

        public override void Down()
        {

        }
    }
}