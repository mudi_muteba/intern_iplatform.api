using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160811124000)]
    public class Update_Caravan_Trailer_Hide_MMCode : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160811124000_Update_Caravan_Trailer_Hide_MMCode.sql");
        }

        public override void Down()
        {

        }
    }
}