using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160811120800)]
    public class Drop_ClientCode_MD_LossHistoryQuestions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160811120800_Drop_ClientCode_MD_LossHistoryQuestions.sql");
        }

        public override void Down()
        {

        }
    }
}