using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160823144000)]
    public class Add_Missing_ChannelEventTask : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160823144000_Add_Missing_ChannelEventTask.sql");
        }

        public override void Down()
        {

        }
    }
}