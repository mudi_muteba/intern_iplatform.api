using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160801114603)]
    public class DEMO_Channels : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160801114603_DEMO_Channels.sql");
        }

        public override void Down()
        {
            
        }
    }
}