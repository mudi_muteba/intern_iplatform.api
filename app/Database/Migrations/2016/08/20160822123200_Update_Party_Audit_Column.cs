using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160822123200)]
    public class Update_Party_Audit_Column : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160822123200_Update_Party_Audit_Column.sql");
        }

        public override void Down()
        {

        }
    }
}