using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160815162929)]
    public class AIG_MotorLimitedCover : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160815162929_AIG_MotorLimitedCover.sql");
        }

        public override void Down()
        {

        }
    }
}