using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160810124000)]
    public class Add_Caravan_Or_Trailer_Cover : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160810124000_Add_Caravan_Or_Trailer_Cover.sql");
        }

        public override void Down()
        {
        }
    }
}