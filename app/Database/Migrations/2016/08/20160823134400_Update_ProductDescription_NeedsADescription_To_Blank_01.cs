using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160823134400)]
    public class Update_ProductDescription_NeedsADescription_To_Blank_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160823134400_Update_ProductDescription_NeedsADescription_To_Blank_01.sql");
        }

        public override void Down()
        {

        }
    }
}