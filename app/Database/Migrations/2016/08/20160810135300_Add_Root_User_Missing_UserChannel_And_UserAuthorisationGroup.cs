using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160810135300)]
    public class Add_Root_User_Missing_UserChannel_And_UserAuthorisationGroup : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160810135300_Add_Root_User_Missing_UserChannel_And_UserAuthorisationGroup.sql");
        }

        public override void Down()
        {

        }
    }
}