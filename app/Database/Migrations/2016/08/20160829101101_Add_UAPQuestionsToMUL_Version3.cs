using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160829101101)]
    public class Add_UAPQuestionsToMUL_Version3 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160829101101_Add_UAPQuestionsToMUL_Version3.sql");
        }

        public override void Down()
        {
            
        }
    }
}