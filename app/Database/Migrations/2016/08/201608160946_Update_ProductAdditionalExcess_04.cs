using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(201608160946)]
    public class Update_ProductAdditionalExcess_04 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("201608160946_Update_ProductAdditionalExcess_04.sql");
        }

        public override void Down()
        {

        }
    }
}