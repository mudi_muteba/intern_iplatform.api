using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160804160100)]
    public class Add_Channel_CountryId : Migration
    {
        public override void Up()
        {
            Alter.Table("Channel")
                .AddColumn("CountryId")
                .AsInt32()
                .WithDefaultValue(197);

            Execute.EmbeddedScript("20160804160100_Add_Channel_CountryId.sql");
        }

        public override void Down()
        {
            Delete.Column("CountryId").FromTable("Channel");
        }
    }
}