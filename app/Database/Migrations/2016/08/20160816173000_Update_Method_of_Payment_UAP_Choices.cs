using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160816173000)]
    public class Update_Method_of_Payment_UAP_Choices : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160816173000_Update_Method_of_Payment_UAP_Choices.sql");
        }

        public override void Down()
        {

        }
    }
}