using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160815135959)]
    public class AIG_MotorVehicleType : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160815135959_AIG_MotorVehicleType.sql");
        }

        public override void Down()
        {

        }
    }
}