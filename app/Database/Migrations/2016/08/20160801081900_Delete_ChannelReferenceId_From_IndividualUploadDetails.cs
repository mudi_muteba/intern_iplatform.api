using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160801081900)]
    public class Delete_ChannelReferenceId_From_IndividualUploadDetails : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160801081900_Delete_ChannelReferenceId_From_IndividualUploadDetails.sql");
        }

        public override void Down()
        {
            
        }
    }
}