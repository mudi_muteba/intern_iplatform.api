using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160811110201)]
    public class ProposalDef_desc_size : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160811110201_ProposalDef_desc_size.sql");
        }

        public override void Down()
        {

        }
    }
}