using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160822140659)]
    public class Update_KPIPERS2_Balloon_Payment : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160822140659_Update_KPIPERS2_Balloon_Payment.sql");
        }

        public override void Down()
        {

        }
    }
}