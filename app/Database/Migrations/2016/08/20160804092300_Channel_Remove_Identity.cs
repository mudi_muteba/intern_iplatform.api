using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160804092300)]
    public class Channel_Remove_Identity : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160804092300_Channel_Remove_Identity.sql");
        }

        public override void Down()
        {
            
        }
    }
}