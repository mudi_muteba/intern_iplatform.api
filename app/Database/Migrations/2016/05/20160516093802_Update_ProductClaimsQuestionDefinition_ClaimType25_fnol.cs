﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160516093802)]
    public class Update_ProductClaimsQuestionDefinition_ClaimType25_fnol : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160516093802_Update_ProductClaimsQuestionDefinition_ClaimType25_fnol.sql");

     
        }
        
        public override void Down()
        {

        }
    }
}