﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace iBroker.Database.Migrations
{
    [Migration(20160517065830)]
    public class KP_Version2_Questions_Product2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160517065830_KP_Version2_Questions_Product2.sql");
        }

        public override void Down()
        {

        }

    }
}
