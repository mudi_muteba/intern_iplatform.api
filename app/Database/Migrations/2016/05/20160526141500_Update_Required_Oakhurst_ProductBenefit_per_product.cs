﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160526141500)]
    public class Update_Required_Oakhurst_ProductBenefit_per_product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160526141500_Update_Required_Oakhurst_ProductBenefit_per_product.sql");
        }
        
        public override void Down()
        {

        }
    }
}