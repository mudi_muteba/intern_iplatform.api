﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160527160501)]
    public class Added_ChannelEvent_for_PSG : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160527160501_Added_ChannelEvent_for_PSG.sql");
        }
        
        public override void Down()
        {

        }
    }
}