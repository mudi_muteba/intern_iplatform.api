﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace iBroker.Database.Migrations
{
    [Migration(20160516112059)]
    public class KP_Version2_Questions_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160516112059_KP_Version2_Questions_Product.sql");
        }

        public override void Down()
        {

        }

    }
}
