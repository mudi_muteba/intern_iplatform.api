using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160529092801)]
    public class Add_AdditionalMember_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 23, Name = "AdditionalMembers"})
                ;

            Insert.IntoTable(Tables.AuthorisationPoint)
                .Row(new { Id = 65, Name = "Create", AuthorisationPointCategoryId = 23 })
                .Row(new { Id = 66, Name = "Edit", AuthorisationPointCategoryId = 23 })
                .Row(new { Id = 67, Name = "Delete", AuthorisationPointCategoryId = 23 })
                .Row(new { Id = 68, Name = "List", AuthorisationPointCategoryId = 23 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 65, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 66, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 67, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 68, AuthorisationGroupId = 5 })


                // call centre agent
                .Row(new { AuthorisationPointId = 65, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 66, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 67, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 68, AuthorisationGroupId = 1 })


                // call centre manager
                .Row(new { AuthorisationPointId = 65, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 66, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 67, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 68, AuthorisationGroupId = 2 })

                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 65, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 66, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 67, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 68, AuthorisationGroupId = 5})


                // call centre agent
                .Row(new {AuthorisationPointId = 65, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 66, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 67, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 68, AuthorisationGroupId = 1})


                // call centre manager
                .Row(new {AuthorisationPointId = 65, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 66, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 67, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 68, AuthorisationGroupId = 2})
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 65, Name = "Create", AuthorisationPointCategoryId = 23 })
                .Row(new { Id = 66, Name = "Edit", AuthorisationPointCategoryId = 23 })
                .Row(new { Id = 67, Name = "Delete", AuthorisationPointCategoryId = 23 })
                .Row(new { Id = 68, Name = "List", AuthorisationPointCategoryId = 23 })
                ;

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 23, Name = "AdditionalMembers" })
                ;

        }
    }
}