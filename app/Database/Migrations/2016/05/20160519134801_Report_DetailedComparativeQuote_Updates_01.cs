﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160519134801)]
    public class Report_DetailedComparativeQuote_Updates_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160519134801_Report_DetailedComparativeQuote_Updates_01.sql");
        }
        
        public override void Down()
        {

        }
    }
}