﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160523103300)]
    public class Update_LossHistory_PartyId_Column : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160523103300_Update_LossHistory_PartyId_Column.sql");
        }
        
        public override void Down()
        {

        }
    }
}