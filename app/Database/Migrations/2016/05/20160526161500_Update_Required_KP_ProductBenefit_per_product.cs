﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160526161500)]
    public class Update_Required_KP_ProductBenefit_per_product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160526161500_Update_Required_KP_ProductBenefit_per_product.sql");
        }
        
        public override void Down()
        {

        }
    }
}