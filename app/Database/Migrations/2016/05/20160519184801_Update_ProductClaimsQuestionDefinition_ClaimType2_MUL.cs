﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160519184801)]
    public class Update_ProductClaimsQuestionDefinition_ClaimType2_MUL : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160519184801_Update_ProductClaimsQuestionDefinition_ClaimType2_MUL.sql");

     
        }
        
        public override void Down()
        {

        }
    }
}