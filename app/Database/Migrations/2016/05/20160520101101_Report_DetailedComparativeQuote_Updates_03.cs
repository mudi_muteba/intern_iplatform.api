﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160520101101)]
    public class Report_DetailedComparativeQuote_Updates_03 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160520101101_Report_DetailedComparativeQuote_Updates_03.sql");
        }
        
        public override void Down()
        {

        }
    }
}