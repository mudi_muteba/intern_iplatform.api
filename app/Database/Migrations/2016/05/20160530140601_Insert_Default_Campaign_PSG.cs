using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160530140601)]
    public class Insert_Default_Campaign_PSG : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160530140601_Insert_Default_Campaign_PSG.sql");
        }

        public override void Down()
        {
            
        }
    }
}