﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160523151601)]
    public class Update_Audit_Column_Datatype : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160523151601_Update_Audit_Column_Datatype.sql");
        }
        
        public override void Down()
        {

        }
    }
}