﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160523135601)]
    public class Update_Reports_FuneralQuote_Header_Spouse_Hide : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160523135601_Update_Reports_FuneralQuote_Header_Spouse_Hide.sql");
        }
        
        public override void Down()
        {

        }
    }
}