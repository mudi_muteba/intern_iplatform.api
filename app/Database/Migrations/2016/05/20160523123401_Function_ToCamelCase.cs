﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160523123401)]
    public class Function_ToCamelCase : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160523123401_Function_ToCamelCase.sql");
        }
        
        public override void Down()
        {

        }
    }
}