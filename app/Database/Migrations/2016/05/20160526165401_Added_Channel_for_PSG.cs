﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160526165401)]
    public class Added_Channel_for_PSG : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160526165401_Added_Channel_for_PSG.sql");
        }
        
        public override void Down()
        {

        }
    }
}