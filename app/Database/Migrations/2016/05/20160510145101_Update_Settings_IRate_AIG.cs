﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160510145101)]
    public class Update_Settings_IRate_AIG : Migration
    {
        public override void Up()
        {
            

            //<!-- AIG VMSA Funeral -->
            //  <add key="blackbox/aigvmsaf/InsurerCode" value="AIG" />
            //  <add key="blackbox/aigvmsaf/ProductCode" value="VMSAFuneral" />
            //  <add key="blackbox/aigvmsaf/AgentCode" value="IPLATFORM" />
            //  <add key="blackbox/aigvmsaf/Password" value="I2P0L1A5T2F0O1R5M" />
 
            //  <!-- AIG VMSA -->
            //  <add key="blackbox/aigvmsa/InsurerCode" value="AIG" />
            //  <add key="blackbox/aigvmsa/ProductCode" value="VMSA" />
            //  <add key="blackbox/aigvmsa/AgentCode" value="IPLATFORM" />
            //  <add key="blackbox/aigvmsa/Password" value="I2P0L1A5T2F0O1R5M" /> 
            //  <add key="blackbox/aigvmsa/UserName" value="VMSA_WEB" />
            //  <add key="blackbox/aigvmsa/SubscriberCode" value="V2M0S1A6W2E0B16" />

            Execute.EmbeddedScript("20160510145101_Update_Settings_IRate_AIG.sql");

     
        }
        
        public override void Down()
        {

        }
    }
}