﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160506124400)]
    public class Add_TransUnion_EndPoint_Property_Person_Settings : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.PersonLookupSetting).Column("TransunionFirstName").Exists())
                Delete.Column("TransunionFirstName").FromTable(Tables.PersonLookupSetting);

            if (Schema.Table(Tables.PersonLookupSetting).Column("TransunionSurname").Exists())
                Delete.Column("TransunionSurname").FromTable(Tables.PersonLookupSetting);
        }

        public override void Down()
        {
            if (Schema.Table(Tables.PersonLookupSetting).Column("TransUnionEndPoint").Exists())
                Delete.Column("TransUnionEndPoint").FromTable(Tables.PersonLookupSetting);
        }
    }
}