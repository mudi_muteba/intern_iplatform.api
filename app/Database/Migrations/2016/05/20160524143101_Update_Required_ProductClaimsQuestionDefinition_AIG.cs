﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160524143101)]
    public class Update_Required_ProductClaimsQuestionDefinition_AIG : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160524143101_Update_Required_ProductClaimsQuestionDefinition_AIG.sql");
        }
        
        public override void Down()
        {

        }
    }
}