﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160509162201)]
    public class Insert_Settings_IRate_AIG : Migration
    {
        public override void Up()
        {
            Insert.IntoTable("SettingsiRate")
                .InSchema(Schemas.Dbo)
                .Row(new{ ProductId = "1", Password = "Password", AgentCode = "AIG", BrokerCode = "BrokerCode",  AuthCode = "AuthCode", SchemeCode = "SchemeCode", Token = "Token", ChannelId = "2A2339E6-D556-428E-BD0B-9E2ECF1715A5", Environment = "Environment", UserId = "1", SubscriberCode = "SubscriberCode", CompanyCode = "CompanyCode", UwCompanyCode = "UwCompanyCode",  UwProductCode = "UwProductCode", IsDeleted = "0"})
                .Row(new{ ProductId = "34", Password = "Password", AgentCode = "AIG", BrokerCode = "BrokerCode",  AuthCode = "AuthCode", SchemeCode = "SchemeCode", Token = "Token", ChannelId = "2A2339E6-D556-428E-BD0B-9E2ECF1715A5", Environment = "Environment", UserId = "1", SubscriberCode = "SubscriberCode", CompanyCode = "CompanyCode", UwCompanyCode = "UwCompanyCode",  UwProductCode = "UwProductCode", IsDeleted = "0"})
                .Row(new{ ProductId = "46", Password = "Password", AgentCode = "AIG", BrokerCode = "BrokerCode",  AuthCode = "AuthCode", SchemeCode = "SchemeCode", Token = "Token", ChannelId = "2A2339E6-D556-428E-BD0B-9E2ECF1715A5", Environment = "Environment", UserId = "1", SubscriberCode = "SubscriberCode", CompanyCode = "CompanyCode", UwCompanyCode = "UwCompanyCode",  UwProductCode = "UwProductCode", IsDeleted = "0"})
                .Row(new{ ProductId = "47", Password = "Password", AgentCode = "AIG", BrokerCode = "BrokerCode",  AuthCode = "AuthCode", SchemeCode = "SchemeCode", Token = "Token", ChannelId = "2A2339E6-D556-428E-BD0B-9E2ECF1715A5", Environment = "Environment", UserId = "1", SubscriberCode = "SubscriberCode", CompanyCode = "CompanyCode", UwCompanyCode = "UwCompanyCode",  UwProductCode = "UwProductCode", IsDeleted = "0"})
                
                .Row(new{ ProductId = "1", Password = "Password", AgentCode = "AIG", BrokerCode = "BrokerCode",  AuthCode = "AuthCode", SchemeCode = "SchemeCode", Token = "Token", ChannelId = "60506D0E-0A60-48F3-9CD4-728555E1B3AF", Environment = "Environment", UserId = "1", SubscriberCode = "SubscriberCode", CompanyCode = "CompanyCode", UwCompanyCode = "UwCompanyCode",  UwProductCode = "UwProductCode", IsDeleted = "0"})
                .Row(new{ ProductId = "34", Password = "Password", AgentCode = "AIG", BrokerCode = "BrokerCode",  AuthCode = "AuthCode", SchemeCode = "SchemeCode", Token = "Token", ChannelId = "60506D0E-0A60-48F3-9CD4-728555E1B3AF", Environment = "Environment", UserId = "1", SubscriberCode = "SubscriberCode", CompanyCode = "CompanyCode", UwCompanyCode = "UwCompanyCode",  UwProductCode = "UwProductCode", IsDeleted = "0"})
                .Row(new{ ProductId = "46", Password = "Password", AgentCode = "AIG", BrokerCode = "BrokerCode",  AuthCode = "AuthCode", SchemeCode = "SchemeCode", Token = "Token", ChannelId = "60506D0E-0A60-48F3-9CD4-728555E1B3AF", Environment = "Environment", UserId = "1", SubscriberCode = "SubscriberCode", CompanyCode = "CompanyCode", UwCompanyCode = "UwCompanyCode",  UwProductCode = "UwProductCode", IsDeleted = "0"})
                .Row(new{ ProductId = "47", Password = "Password", AgentCode = "AIG", BrokerCode = "BrokerCode",  AuthCode = "AuthCode", SchemeCode = "SchemeCode", Token = "Token", ChannelId = "60506D0E-0A60-48F3-9CD4-728555E1B3AF", Environment = "Environment", UserId = "1", SubscriberCode = "SubscriberCode", CompanyCode = "CompanyCode", UwCompanyCode = "UwCompanyCode",  UwProductCode = "UwProductCode", IsDeleted = "0"})
                
                .Row(new{ ProductId = "1", Password = "Password", AgentCode = "AIG", BrokerCode = "BrokerCode",  AuthCode = "AuthCode", SchemeCode = "SchemeCode", Token = "Token", ChannelId = "8168BBF8-468E-4628-9AF8-2A074654557D", Environment = "Environment", UserId = "1", SubscriberCode = "SubscriberCode", CompanyCode = "CompanyCode", UwCompanyCode = "UwCompanyCode",  UwProductCode = "UwProductCode", IsDeleted = "0"})
                .Row(new{ ProductId = "34", Password = "Password", AgentCode = "AIG", BrokerCode = "BrokerCode",  AuthCode = "AuthCode", SchemeCode = "SchemeCode", Token = "Token", ChannelId = "8168BBF8-468E-4628-9AF8-2A074654557D", Environment = "Environment", UserId = "1", SubscriberCode = "SubscriberCode", CompanyCode = "CompanyCode", UwCompanyCode = "UwCompanyCode",  UwProductCode = "UwProductCode", IsDeleted = "0"})
                .Row(new{ ProductId = "46", Password = "Password", AgentCode = "AIG", BrokerCode = "BrokerCode",  AuthCode = "AuthCode", SchemeCode = "SchemeCode", Token = "Token", ChannelId = "8168BBF8-468E-4628-9AF8-2A074654557D", Environment = "Environment", UserId = "1", SubscriberCode = "SubscriberCode", CompanyCode = "CompanyCode", UwCompanyCode = "UwCompanyCode",  UwProductCode = "UwProductCode", IsDeleted = "0"})
                .Row(new{ ProductId = "47", Password = "Password", AgentCode = "AIG", BrokerCode = "BrokerCode",  AuthCode = "AuthCode", SchemeCode = "SchemeCode", Token = "Token", ChannelId = "8168BBF8-468E-4628-9AF8-2A074654557D", Environment = "Environment", UserId = "1", SubscriberCode = "SubscriberCode", CompanyCode = "CompanyCode", UwCompanyCode = "UwCompanyCode",  UwProductCode = "UwProductCode", IsDeleted = "0"})
                ;
        }
        
        public override void Down()
        {

        }
    }
}