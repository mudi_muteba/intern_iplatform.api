﻿using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160506130901)]
    public class Insert_Default_Campaign : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160506130901_Insert_Default_Campaign.sql");
        }
        
        public override void Down()
        {

        }
    }
}