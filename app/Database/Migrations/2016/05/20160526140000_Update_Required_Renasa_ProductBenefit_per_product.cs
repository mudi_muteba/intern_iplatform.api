﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160526140000)]
    public class Update_Required_Renasa_ProductBenefit_per_product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160526140000_Update_Required_Renasa_ProductBenefit_per_product.sql");
        }
        
        public override void Down()
        {

        }
    }
}