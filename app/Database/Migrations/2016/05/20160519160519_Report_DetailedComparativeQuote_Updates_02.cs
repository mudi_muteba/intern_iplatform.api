﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._05
{
    [Migration(20160519160519)]
    public class Report_DetailedComparativeQuote_Updates_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160519160519_Report_DetailedComparativeQuote_Updates_02.sql");
        }
        
        public override void Down()
        {

        }
    }
}