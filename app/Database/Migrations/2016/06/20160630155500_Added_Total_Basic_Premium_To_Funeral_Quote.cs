using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160630155500)]
    public class Added_Total_Basic_Premium_To_Funeral_Quote : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160630155500_Added_Total_Basic_Premium_To_Funeral_Quote.sql");
        }

        public override void Down()
        {
            
        }
    }
}