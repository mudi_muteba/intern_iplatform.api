﻿using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160606131500)]
    public class Create_ProposalDecline_Table : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160606131500_Create_ProposalDecline_Table.sql");
        }

        public override void Down()
        {

        }
    }
}
