﻿using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160610151500)]
    public class Update_Required_KP_ProductBenefit_per_product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160610151500_Update_Required_KP_ProductBenefit_per_product.sql");
        }

        public override void Down()
        {

        }
    }
}