using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160701122000)]
    public class _Drop_PolicyItem_Ids : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160701122000_Drop_PolicyItem_Ids.sql");
        }

        public override void Down()
        {
            
        }
    }
}