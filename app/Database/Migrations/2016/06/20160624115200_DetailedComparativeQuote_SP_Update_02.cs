using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160624115200)]
    public class DetailedComparativeQuote_SP_Update_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160624115200_DetailedComparativeQuote_SP_Update_02.sql");
        }

        public override void Down()
        {
            
        }
    }
}