﻿using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160615131500)]
    public class Update_MapQuestionDefinition_KingPrice_Quick_Quote_imageName : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160615131500_Update_MapQuestionDefinition_KingPrice_Quick_Quote_imageName.sql");
        }

        public override void Down()
        {

        }
    }
}