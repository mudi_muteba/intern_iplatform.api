using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160621131201)]
    public class Add_CampaignLeadBucket_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 24, Name = "CampaignLeadBucket" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                // individual
                .Row(new { Id = 69, Name = "Assign", AuthorisationPointCategoryId = 24 })
                .Row(new { Id = 70, Name = "List", AuthorisationPointCategoryId = 24 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 69, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 70, AuthorisationGroupId = 5 })

                // call centre manager
                .Row(new { AuthorisationPointId = 69, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 70, AuthorisationGroupId = 2 })
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 69, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 70, AuthorisationGroupId = 5 })

                // call centre manager
                .Row(new { AuthorisationPointId = 69, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 70, AuthorisationGroupId = 2 })
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 69, Name = "Assign", AuthorisationPointCategoryId = 24 })
                .Row(new { Id = 70, Name = "List", AuthorisationPointCategoryId = 24 })
                ;

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 24, Name = "CampaignLeadBucket" });
        }
    }
}