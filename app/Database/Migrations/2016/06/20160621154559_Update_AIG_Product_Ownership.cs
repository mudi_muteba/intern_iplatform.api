using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160621154559)]
    public class Update_AIG_Product_Ownership : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160621154559_Update_AIG_Product_Ownership.sql");
        }

        public override void Down()
        {
            
        }
    }
}