using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160629115200)]
    public class Update_PolicyItem_Tables_Id : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160629115200_Update_PolicyItem_Tables_Id.sql");
        }

        public override void Down()
        {
            
        }
    }
}