using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160609082959)]
    public class Update_TelesureBE : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160609082959_Update_TelesureBE.sql");
        }

        public override void Down()
        {
            
        }
    }
}