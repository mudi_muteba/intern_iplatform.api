﻿using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160621123000)]
    public class Update_Required_KP_ProductDescription : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160621123000_Update_Required_KP_ProductDescription.sql");
        }

        public override void Down()
        {

        }
    }
}