using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160602150700)]
    public class Remove_AG_VehicleUsage : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160602150700_Remove_AG_VehicleUsage.sql");
        }

        public override void Down()
        {
            
        }
    }
}