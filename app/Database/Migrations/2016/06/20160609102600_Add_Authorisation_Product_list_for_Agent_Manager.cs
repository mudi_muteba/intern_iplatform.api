﻿using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160609102600)]
    public class Add_Authorisation_Product_list_for_Agent_Manager : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160609102600_Add_Authorisation_Product_list_for_Agent_Manager.sql");
        }

        public override void Down()
        {

        }
    }
}