using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160606130000)]
    public class Reorder_Motor_Proposal_Risk_Questions_UAP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160606130000_Reorder_Motor_Proposal_Risk_Questions_UAP.sql");
        }

        public override void Down()
        {
            
        }
    }
}