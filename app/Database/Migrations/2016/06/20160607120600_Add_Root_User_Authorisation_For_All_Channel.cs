using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160607120600)]
    public class Add_Root_User_Authorisation_For_All_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160607120600_Add_Root_User_Authorisation_For_All_Channel.sql");
        }

        public override void Down()
        {
            
        }
    }
}