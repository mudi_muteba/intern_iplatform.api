using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160621140059)]
    public class Update_KP_Product2_Part2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160621140059_Update_KP_Product2_Part2.sql");
        }

        public override void Down()
        {
            
        }
    }
}