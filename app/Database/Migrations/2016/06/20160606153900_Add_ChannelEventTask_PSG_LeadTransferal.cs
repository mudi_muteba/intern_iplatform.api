using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160606153900)]
    public class Add_ChannelEventTask_PSG_LeadTransferal : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160606153900_Add_ChannelEventTask_PSG_LeadTransferal.sql");
        }

        public override void Down()
        {
            
        }
    }
}