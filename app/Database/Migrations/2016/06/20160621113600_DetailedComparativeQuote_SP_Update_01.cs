using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160621113600)]
    public class DetailedComparativeQuote_SP_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160621113600_DetailedComparativeQuote_SP_Update_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}