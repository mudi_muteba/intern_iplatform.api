using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160609102330)]
    public class Update_TelesureBE_ExtraItems : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160609102330_Update_TelesureBE_ExtraItems.sql");
        }

        public override void Down()
        {
            
        }
    }
}