using FluentMigrator;

namespace Database.Migrations._2016._06
{
    [Migration(20160620153730)]
    public class Update_KP_Product2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160620153730_Update_KP_Product2.sql");
        }

        public override void Down()
        {
            
        }
    }
}