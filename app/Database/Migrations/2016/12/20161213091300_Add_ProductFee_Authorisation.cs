﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161213091300)]
    public class Add_ProductFee_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 26, Name = "ProductFee" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                // address
                .Row(new { Id = 75, Name = "Create", AuthorisationPointCategoryId = 26 })
                .Row(new { Id = 76, Name = "Edit", AuthorisationPointCategoryId = 26 })
                .Row(new { Id = 77, Name = "Delete", AuthorisationPointCategoryId = 26 })
                .Row(new { Id = 78, Name = "List", AuthorisationPointCategoryId = 26 });

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 75, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 76, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 77, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 78, AuthorisationGroupId = 5 });
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 75, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 76, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 77, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 78, AuthorisationGroupId = 5 });

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 75, Name = "Create", AuthorisationPointCategoryId = 26 })
                .Row(new { Id = 76, Name = "Edit", AuthorisationPointCategoryId = 26 })
                .Row(new { Id = 77, Name = "Delete", AuthorisationPointCategoryId = 26 })
                .Row(new { Id = 78, Name = "List", AuthorisationPointCategoryId = 26 });

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 26, Name = "ProductFee" });
        }
    }
}