﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161206105400)]
    public class Comparative_Quote_Summary_SP_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161206105400_Comparative_Quote_Summary_SP_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}