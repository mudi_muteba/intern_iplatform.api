﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161207130000)]
    public class Comparative_Quote_Summary_Asset_Joins_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161207130000_Comparative_Quote_Summary_Asset_Joins_01.sql");
        }

        public override void Down()
        {

        }
    }
}