﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161205103508)]
    public class Add_JP_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161205103508_Add_JP_Channel.sql");
        }

        public override void Down()
        {

        }
    }
}