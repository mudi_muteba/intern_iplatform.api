﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161222063100)]
    public class Add_Lead_Management_Report_Filters_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161222063100_Add_Lead_Management_Report_Filters_01.sql");
        }

        public override void Down()
        {

        }
    }
}