﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161222191500)]
    public class Alter_vw_product_allocation : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161222191500_Alter_vw_product_allocation.sql");
        }

        public override void Down()
        {

        }
    }
}