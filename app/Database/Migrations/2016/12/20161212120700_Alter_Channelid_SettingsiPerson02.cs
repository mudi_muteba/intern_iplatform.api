﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161212120700)]
    public class Alter_Channelid_SettingsiPerson02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161212120700_Alter_Channelid_SettingsiPerson02.sql");
        }

        public override void Down()
        {

        }
    }
}