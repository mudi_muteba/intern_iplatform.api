﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161221154900)]
    public class Alter_vw_rating_configuration02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161221154900_Alter_vw_rating_configuration02.sql");
        }

        public override void Down()
        {

        }
    }
}