﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161212113500)]
    public class Alter_Channelid_SettingsiPerson : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161212113500_Alter_Channelid_SettingsiPerson.sql");
        }

        public override void Down()
        {

        }
    }
}