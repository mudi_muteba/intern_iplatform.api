﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161214074200)]
    public class AIG_Quote_Schedule_Updates_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161214074200_AIG_Quote_Schedule_Updates_01.sql");
        }

        public override void Down()
        {

        }
    }
}