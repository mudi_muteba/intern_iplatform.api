﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161219142100)]
    public class Agent_Performance_Report_Changes_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161219142100_Agent_Performance_Report_Changes_01.sql");
        }

        public override void Down()
        {

        }
    }
}