﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161205103100)]
    public class Add_AA_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161205103100_Add_AA_Channel.sql");
        }

        public override void Down()
        {

        }
    }
}