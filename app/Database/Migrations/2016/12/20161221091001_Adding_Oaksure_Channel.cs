﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161221091001)]
    public class Adding_Oaksure_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161221091001_Adding_Oaksure_Channel.sql");
        }

        public override void Down()
        {
        }
    }
}
