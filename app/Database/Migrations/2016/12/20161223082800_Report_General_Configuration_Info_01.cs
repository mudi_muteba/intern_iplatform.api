﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161223082800)]
    public class Report_General_Configuration_Info_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161223082800_Report_General_Configuration_Info_01.sql");
        }

        public override void Down()
        {

        }
    }
}