﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161222142700)]
    public class Report_List_SP_Update_Info_Field_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161222142700_Report_List_SP_Update_Info_Field_01.sql");
        }

        public override void Down()
        {

        }
    }
}