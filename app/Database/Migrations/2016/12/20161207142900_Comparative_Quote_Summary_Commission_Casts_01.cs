﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161207142900)]
    public class Comparative_Quote_Summary_Commission_Casts_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161207142900_Comparative_Quote_Summary_Commission_Casts_01.sql");
        }

        public override void Down()
        {

        }
    }
}