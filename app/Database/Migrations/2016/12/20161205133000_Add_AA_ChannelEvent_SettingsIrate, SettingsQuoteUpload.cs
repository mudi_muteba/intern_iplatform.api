﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161205133000)]
    public class Add_AA_ChannelEvent_SettingsIrate_SettingsQuoteUpload : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161205133000_Add_AA_ChannelEvent_SettingsIrate_SettingsQuoteUpload.sql");
        }

        public override void Down()
        {

        }
    }
}