﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161220411300)]
    public class SettingsIRate_Column_Updates : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161220411300_SettingsIRate_Column_Updates.sql");
        }

        public override void Down()
        {
        }
    }
}
