﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161206124900)]
    public class Bower_Website_Comparative_Quote_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161206124900_Bower_Website_Comparative_Quote_01.sql");
        }

        public override void Down()
        {

        }
    }
}