﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161226102600)]
    public class ProductFee_Add_ChannelId : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161226102600_ProductFee_Add_ChannelId.sql");
        }

        public override void Down()
        {

        }
    }
}