﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161206092108)]
    public class Add_Question_NCB_On_Trailer_Or_Caravan_KP_Proposal : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161206092108_Add_Question_NCB_On_Trailer_Or_Caravan_KP_Proposal.sql");
        }

        public override void Down()
        {

        }
    }
}