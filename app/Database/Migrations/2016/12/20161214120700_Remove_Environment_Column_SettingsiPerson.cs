﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161214120700)]
    public class Remove_Environment_Column_SettingsiPerson : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161214120700_Remove_Environment_Column_SettingsiPerson.sql");
        }

        public override void Down()
        {

        }
    }
}