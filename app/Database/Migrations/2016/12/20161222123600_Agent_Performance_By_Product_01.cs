﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161222123600)]
    public class Agent_Performance_By_Product_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161222123600_Agent_Performance_By_Product_01.sql");
        }

        public override void Down()
        {

        }
    }
}