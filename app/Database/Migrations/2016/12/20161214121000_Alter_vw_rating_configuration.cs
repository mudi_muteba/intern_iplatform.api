﻿using FluentMigrator;

namespace Database.Migrations._2016._12
{
    [Migration(20161214121000)]
    public class Alter_vw_rating_configuration : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161214121000_Alter_vw_rating_configuration.sql");
        }

        public override void Down()
        {

        }
    }
}