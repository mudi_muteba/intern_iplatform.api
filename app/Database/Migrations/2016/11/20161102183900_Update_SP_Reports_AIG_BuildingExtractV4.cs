﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161102183900)]
    public class Update_SP_Reports_AIG_BuildingExtractV4 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161102183900_Update_SP_Reports_AIG_BuildingExtractV4.sql");
        }

        public override void Down()
        {

        }
    }
}