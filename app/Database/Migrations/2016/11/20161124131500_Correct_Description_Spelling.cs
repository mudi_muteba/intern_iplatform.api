﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161124131500)]
    public class Correct_Description_Spelling : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161124131500_Correct_Description_Spelling.sql");
        }

        public override void Down()
        {

        }
    }
}