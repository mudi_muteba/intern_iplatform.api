﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161124095600)]
    public class Update_SP_Reports_AIG_BuildingExtractIsRerated : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161124095600_Update_SP_Reports_AIG_BuildingExtractIsRerated.sql");
        }

        public override void Down()
        {

        }
    }
}