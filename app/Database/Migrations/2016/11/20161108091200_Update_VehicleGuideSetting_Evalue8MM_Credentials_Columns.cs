﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161108091200)]
    public class Update_VehicleGuideSetting_Evalue8MM_Credentials_Columns : Migration
    {
        public override void Up()
        {
            if (!Schema.Table("VehicleGuideSetting").Column("Evalue8Username").Exists())
            {
                Alter.Table("VehicleGuideSetting")
                .AddColumn("Evalue8Username").AsString().Nullable();
            }

            if (!Schema.Table("VehicleGuideSetting").Column("Evalue8Password").Exists())
            {
                Alter.Table("VehicleGuideSetting")
                .AddColumn("Evalue8Password").AsString().Nullable();
            }

            if (!Schema.Table("VehicleGuideSetting").Column("Evalue8Enabled").Exists())
            {
                Alter.Table("VehicleGuideSetting")
                .AddColumn("Evalue8Enabled").AsBoolean().Nullable();
            }

            Execute.EmbeddedScript("20161108091200_Update_VehicleGuideSetting_Evalue8MM_Credentials_Columns.sql");
        }

        public override void Down()
        {

        }
    }
}
