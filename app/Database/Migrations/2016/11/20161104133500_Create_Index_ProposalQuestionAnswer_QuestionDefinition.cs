﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161104133500)]
    public class _20161104133500_Create_Index_ProposalQuestionAnswer_QuestionDefinition : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161104133500_Create_Index_ProposalQuestionAnswer_QuestionDefinition.sql");
        }

        public override void Down()
        {

        }
    }
}