﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161114174400)]
    public class Update_SP_Reports_AIG_LeadPreviousMotorLossBugFix : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161114174400_Update_SP_Reports_AIG_LeadPreviousMotorLossBugFix.sql");
        }

        public override void Down()
        {

        }
    }
}