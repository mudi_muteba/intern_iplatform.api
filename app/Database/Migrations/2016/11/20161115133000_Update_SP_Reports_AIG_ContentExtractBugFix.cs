﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161115133000)]
    public class Update_SP_Reports_AIG_ContentExtractBugFix : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161115133000_Update_SP_Reports_AIG_ContentExtractBugFix.sql");
        }

        public override void Down()
        {

        }
    }
}