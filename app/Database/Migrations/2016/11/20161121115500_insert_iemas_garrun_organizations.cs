﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161121115500)]
    public class insert_iemas_garrun_organizations : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161121115500_insert_iemas_garrun_organizations.sql");
        }

        public override void Down()
        {

        }
    }
}
