﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161102102000)]
    public class AIG_Question_Renaming_Update : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161102102000_AIG_Question_Renaming_Update.sql");
        }

        public override void Down()
        {

        }
    }
}