﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161124095200)]
    public class Update_SP_Reports_AIG_LeadExtractIsRerated : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161124095200_Update_SP_Reports_AIG_LeadExtractIsRerated.sql");
        }

        public override void Down()
        {

        }
    }
}