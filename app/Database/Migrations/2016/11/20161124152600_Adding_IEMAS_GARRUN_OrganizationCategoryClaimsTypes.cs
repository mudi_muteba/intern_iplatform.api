﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161124152600)]
    public class Adding_IEMAS_GARRUN_OrganizationCategoryClaimsTypes : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161124152600_Adding_IEMAS_GARRUN_OrganizationCategoryClaimsTypes.sql");
        }

        public override void Down()
        {

        }
    }
}
