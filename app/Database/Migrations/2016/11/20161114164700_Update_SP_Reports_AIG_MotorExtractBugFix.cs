﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161114164700)]
    public class Update_SP_Reports_AIG_MotorExtractBugFix : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161114164700_Update_SP_Reports_AIG_MotorExtractBugFix.sql");
        }

        public override void Down()
        {

        }
    }
}