﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161114161400)]
    public class Update_AIG_Question_Spaces : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161114161400_Update_AIG_Question_Spaces.sql");
        }

        public override void Down()
        {

        }
    }
}
