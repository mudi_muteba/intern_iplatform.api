﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161130070900)]
    public class ComparativeQuote_Cover_SP_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161130070900_ComparativeQuote_Cover_SP_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}