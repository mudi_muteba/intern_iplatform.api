﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161123110000)]
    public class Comparative_Quote_UAP_Additional_Benefits_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161123110000_Comparative_Quote_UAP_Additional_Benefits_01.sql");
        }

        public override void Down()
        {

        }
    }
}
