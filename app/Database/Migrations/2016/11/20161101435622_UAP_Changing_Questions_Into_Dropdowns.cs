﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161101435622)]
    public class UAP_Changing_Questions_Into_Dropdowns : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161101435622_UAP_Changing_Questions_Into_Dropdowns.sql");
        }

        public override void Down()
        {

        }
    }
}