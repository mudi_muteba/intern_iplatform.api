﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161110084800)]
    public class Update_Beneficiary_Date_Of_Birth_Required : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161110084800_Update_Beneficiary_Date_Of_Birth_Required.sql");
        }

        public override void Down()
        {

        }
    }
}

