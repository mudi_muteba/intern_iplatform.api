﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161110111250)]
    public class Update_Attributes_To_SalesForceInterationLogSummery : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161110111250_Update_Attributes_To_SalesForceInterationLogSummery.sql");
        }

        public override void Down()
        {

        }
    }
}
