﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161123063700)]
    public class Report_Configuration_Tooltip_Updates_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161123063700_Report_Configuration_Tooltip_Updates_01.sql");
        }

        public override void Down()
        {

        }
    }
}
