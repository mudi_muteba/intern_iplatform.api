﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161118062200)]
    public class Update_InsurerDataReport_SPs_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161118062200_Update_InsurerDataReport_SPs_02.sql");
        }

        public override void Down()
        {

        }
    }
}
