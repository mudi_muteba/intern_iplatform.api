﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161114093500)]
    public class Update_AIG_Question_Car_Hire : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161114093500_Update_AIG_Question_Car_Hire.sql");
        }

        public override void Down()
        {

        }
    }
}
