﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161104122200)]
    public class AIG_Brackets_Removal_On_Question : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161104122200_AIG_Brackets_Removal_On_Question.sql");
        }

        public override void Down()
        {

        }
    }
}