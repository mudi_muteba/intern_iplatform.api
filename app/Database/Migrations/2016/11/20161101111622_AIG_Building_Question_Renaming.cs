﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161101111622)]
    public class AIG_Building_Question_Renaming : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161101111622_AIG_Building_Question_Renaming.sql");
        }

        public override void Down()
        {

        }
    }
}