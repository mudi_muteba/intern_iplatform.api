﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161108110000)]
    public class ComparativeQuote_AdditionalBenefits_SPs_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161108110000_ComparativeQuote_AdditionalBenefits_SPs_01.sql");
        }

        public override void Down()
        {

        }
    }
}