﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161125131700)]
    public class ComparativeQuote_ReportLayout_AP_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161125131700_ComparativeQuote_ReportLayout_AP_01.sql");
        }

        public override void Down()
        {

        }
    }
}