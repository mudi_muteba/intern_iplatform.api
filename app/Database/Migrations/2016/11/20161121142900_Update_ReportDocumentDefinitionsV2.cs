﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161121142900)]
    public class Update_ReportDocumentDefinitionsV2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161121142900_Update_ReportDocumentDefinitionsV2.sql");
        }

        public override void Down()
        {

        }
    }
}