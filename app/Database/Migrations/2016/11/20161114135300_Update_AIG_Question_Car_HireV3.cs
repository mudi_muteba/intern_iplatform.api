﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161114135300)]
    public class Update_AIG_Question_Car_HireV3 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161114135300_Update_AIG_Question_Car_HireV3.sql");
        }

        public override void Down()
        {

        }
    }
}
