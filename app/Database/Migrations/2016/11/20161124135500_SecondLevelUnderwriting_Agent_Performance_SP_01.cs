﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161124135500)]
    public class SecondLevelUnderwriting_Agent_Performance_SP_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161124135500_SecondLevelUnderwriting_Agent_Performance_SP_01.sql");
        }

        public override void Down()
        {

        }
    }
}