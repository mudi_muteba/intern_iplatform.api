﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161122172800)]
    public class Update_Product_Allocation_View : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161122172800_Update_Product_Allocation_View.sql");
        }

        public override void Down()
        {

        }
    }
}