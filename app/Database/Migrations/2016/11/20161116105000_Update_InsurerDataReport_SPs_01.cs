﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161116105000)]
    public class Update_InsurerDataReport_SPs_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161116105000_Update_InsurerDataReport_SPs_01.sql");
        }

        public override void Down()
        {

        }
    }
}
