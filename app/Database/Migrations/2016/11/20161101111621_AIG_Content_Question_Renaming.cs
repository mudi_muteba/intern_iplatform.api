﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161101111621)]
    public class AIG_Content_Question_Renaming : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161101111621_AIG_Content_Question_Renaming.sql");
        }

        public override void Down()
        {

        }
    }
}