﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161103085000)]
    public class AIG_Sum_Insured_Question_Renaming_Update : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161103085000_AIG_Sum_Insured_Question_Renaming_Update.sql");
        }

        public override void Down()
        {

        }
    }
}