﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161117154800)]
    public class Update_Car_Hire_TooltipV3 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161117154800_Update_Car_Hire_TooltipV3.sql");
        }

        public override void Down()
        {

        }
    }
}