﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161124144600)]
    public class Adding_GARRUN_ProductClaimsQuestionDefinitions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161124144600_Adding_GARRUN_ProductClaimsQuestionDefinitions.sql");
        }

        public override void Down()
        {

        }
    }
}
