﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161104145600)]
    public class Update_SP_Reports_AIG_ContentExtractOptimised : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161104145600_Update_SP_Reports_AIG_ContentExtractOptimised.sql");
        }

        public override void Down()
        {

        }
    }
}
