﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161124095400)]
    public class Update_SP_Reports_AIG_ContentExtractIsRerated : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161124095400_Update_SP_Reports_AIG_ContentExtractIsRerated.sql");
        }

        public override void Down()
        {

        }
    }
}