﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161108085000)]
    public class Update_UAP_Addtional_Benefits_Making_Them_Required : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161108085000_Update_UAP_Addtional_Benefits_Making_Them_Required.sql");
        }

        public override void Down()
        {

        }
    }
}
