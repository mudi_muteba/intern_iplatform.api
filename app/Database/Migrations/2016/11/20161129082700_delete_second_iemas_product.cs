﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161129082700)]
    public class delete_second_iemas_product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161129082700_delete_second_iemas_product.sql");
        }

        public override void Down()
        {

        }
    }
}