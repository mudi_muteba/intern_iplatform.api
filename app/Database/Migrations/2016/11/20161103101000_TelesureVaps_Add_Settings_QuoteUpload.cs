﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161103101000)]
    public class TelesureVaps_Add_Settings_QuoteUpload : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161103101000_TelesureVaps_Add_Settings_QuoteUpload.sql");
        }

        public override void Down()
        {

        }
    }
}