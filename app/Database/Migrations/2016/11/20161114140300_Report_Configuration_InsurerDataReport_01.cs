﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161114140300)]
    public class Report_Configuration_InsurerDataReport_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161114140300_Report_Configuration_InsurerDataReport_01.sql");
        }

        public override void Down()
        {

        }
    }
}