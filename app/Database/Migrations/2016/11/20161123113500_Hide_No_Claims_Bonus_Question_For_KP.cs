﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161123113500)]
    public class Hide_No_Claims_Bonus_Question_For_KP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161123113500_Hide_No_Claims_Bonus_Question_For_KP.sql");
        }

        public override void Down()
        {

        }
    }
}
