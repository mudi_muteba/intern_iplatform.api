﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161115162600)]
    public class Update_SP_Reports_AIG_BuildingExtractBugFix : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161115162600_Update_SP_Reports_AIG_BuildingExtractBugFix.sql");
        }

        public override void Down()
        {

        }
    }
}