﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161124095300)]
    public class Update_SP_Reports_AIG_MotorExtractIsRerated : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161124095300_Update_SP_Reports_AIG_MotorExtractIsRerated.sql");
        }

        public override void Down()
        {

        }
    }
}