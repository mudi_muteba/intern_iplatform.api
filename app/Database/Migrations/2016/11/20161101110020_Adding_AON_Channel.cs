﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161101110020)]
    public class Adding_AON_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161101110020_Adding_AON_Channel.sql");
        }

        public override void Down()
        {

        }
    }
}