﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161116110100)]
    public class Update_Tool_Tip_Column_Size : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161116110100_Update_Tool_Tip_Column_Size.sql");
        }

        public override void Down()
        {

        }
    }
}
