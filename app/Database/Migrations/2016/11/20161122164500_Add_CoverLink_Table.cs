﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161122164500)]
    public class Add_CoverLink_Table : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema("dbo").Table("CoverLink").Exists())
                Create.Table("CoverLink")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                .WithColumn("ChannelId").AsInt32()
                .WithColumn("CoverDefinitionId").AsInt32()
                .WithColumn("VisibleIndex").AsInt32().WithDefaultValue(0)
                .WithColumn("IsDeleted").AsBoolean().WithDefaultValue(false);

            Execute.EmbeddedScript("20161122164500_Add_CoverLink_Table.sql");
        }

        public override void Down()
        {

        }
    }
}
