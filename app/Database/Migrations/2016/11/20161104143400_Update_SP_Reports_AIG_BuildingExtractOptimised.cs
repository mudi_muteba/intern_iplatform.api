﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161104143400)]
    public class Update_SP_Reports_AIG_BuildingExtractOptimised : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161104143400_Update_SP_Reports_AIG_BuildingExtractOptimised.sql");
        }

        public override void Down()
        {

        }
    }
}