﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161110091800)]
    public class Report_InsurerQuoteBreakdown_SP_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161110091800_Report_InsurerQuoteBreakdown_SP_Update_01.sql");
        }

        public override void Down()
        {

        }
    }
}