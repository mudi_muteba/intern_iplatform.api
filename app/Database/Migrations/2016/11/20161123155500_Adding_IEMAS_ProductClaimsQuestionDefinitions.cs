﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161123155500)]
    public class Adding_IEMAS_ProductClaimsQuestionDefinitions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161123155500_Adding_IEMAS_ProductClaimsQuestionDefinitions.sql");
        }

        public override void Down()
        {

        }
    }
}
