﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161115082300)]
    public class Update_Additional_Excess_Structures_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161115082300_Update_Additional_Excess_Structures_01.sql");
        }

        public override void Down()
        {

        }
    }
}
