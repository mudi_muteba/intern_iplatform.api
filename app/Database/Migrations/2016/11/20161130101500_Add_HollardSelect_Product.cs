﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161130101500)]
    public class Add_HollardSelect_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161130101500_Add_HollardSelect_ProductMotor.sql");
            Execute.EmbeddedScript("20161130101500_Add_HollardSelect_ProductBuilding.sql");
            Execute.EmbeddedScript("20161130101500_Add_HollardSelect_ProductContents.sql");
            Execute.EmbeddedScript("20161130101500_Add_HollardSelect_ProductAllRisk.sql");
        }

        public override void Down()
        {

        }
    }
}