﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161115080500)]
    public class Update_SP_Reports_AIG_LeadExtractBugFix : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161115080500_Update_SP_Reports_AIG_LeadExtractBugFix.sql");
        }

        public override void Down()
        {

        }
    }
}