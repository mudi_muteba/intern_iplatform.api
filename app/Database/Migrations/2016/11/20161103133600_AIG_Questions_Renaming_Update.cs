﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161103133600)]
    public class AIG_Questions_Renaming_Update : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161103133600_AIG_Questions_Renaming_Update.sql");
        }

        public override void Down()
        {

        }
    }
}