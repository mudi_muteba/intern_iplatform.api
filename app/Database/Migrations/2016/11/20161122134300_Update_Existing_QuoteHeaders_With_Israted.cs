﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161122134300)]
    public class Update_Existing_QuoteHeaders_With_Israted : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161122134300_Update_Existing_QuoteHeaders_With_Israted.sql");
        }

        public override void Down()
        {

        }
    }
}
