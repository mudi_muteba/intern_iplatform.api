﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161125114200)]
    public class Quote_Schedule_IsRated_Quoteheader_Update : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161125114200_Quote_Schedule_IsRated_Quoteheader_Update.sql");
        }

        public override void Down()
        {

        }
    }
}