﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161110134000)]
    public class Update_AIG_Question_ToolTipsV2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161110134000_Update_AIG_Question_ToolTipsV2.sql");
        }

        public override void Down()
        {

        }
    }
}
