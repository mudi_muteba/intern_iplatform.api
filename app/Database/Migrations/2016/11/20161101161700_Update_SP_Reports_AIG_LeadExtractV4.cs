﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161101161700)]
    public class Update_SP_Reports_AIG_LeadExtractV4 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161101161700_Update_SP_Reports_AIG_LeadExtractV4.sql");
        }

        public override void Down()
        {

        }
    }
}
