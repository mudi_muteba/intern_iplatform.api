﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161104130000)]
    public class Update_SP_Reports_AIG_MotorExtractOptimised : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161104130000_Update_SP_Reports_AIG_MotorExtractOptimised.sql");
        }

        public override void Down()
        {

        }
    }
}
