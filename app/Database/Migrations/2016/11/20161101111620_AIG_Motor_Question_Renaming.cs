﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161101111620)]
    public class AIG_Motor_Question_Renaming : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161101111620_AIG_Motor_Question_Renaming.sql");
        }

        public override void Down()
        {

        }
    }
}