﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161109150000)]
    public class Update_Funeral_Questions_Remove_Student_Question : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161109150000_Update_Funeral_Questions_Remove_Student_Question.sql");
        }

        public override void Down()
        {

        }
    }
}

