﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161122075100)]
    public class Report_DocumentDefinition_SP_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161122075100_Report_DocumentDefinition_SP_01.sql");
        }

        public override void Down()
        {

        }
    }
}
