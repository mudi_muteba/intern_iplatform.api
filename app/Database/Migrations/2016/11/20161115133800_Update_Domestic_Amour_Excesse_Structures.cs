﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161115133800)]
    public class Update_Domestic_Amour_Excesse_Structures : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161115133800_Update_Domestic_Amour_Excesse_Structures.sql");
        }

        public override void Down()
        {

        }
    }
}
