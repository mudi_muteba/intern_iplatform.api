﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161109110800)]
    public class Update_AIG_Question_ToolTips : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161109110800_Update_AIG_Question_ToolTips.sql");
        }

        public override void Down()
        {

        }
    }
}
