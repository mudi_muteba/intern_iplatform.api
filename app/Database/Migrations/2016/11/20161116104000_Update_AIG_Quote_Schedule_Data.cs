﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161116104000)]
    public class Update_AIG_Quote_Schedule_Data : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161116104000_Update_AIG_Quote_Schedule_Data.sql");
        }

        public override void Down()
        {

        }
    }
}
