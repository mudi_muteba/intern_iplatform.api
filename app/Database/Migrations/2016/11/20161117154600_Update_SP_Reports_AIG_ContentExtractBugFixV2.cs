﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161117154600)]
    public class Update_SP_Reports_AIG_ContentExtractBugFixV2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161117154600_Update_SP_Reports_AIG_ContentExtractBugFixV2.sql");
        }

        public override void Down()
        {

        }
    }
}