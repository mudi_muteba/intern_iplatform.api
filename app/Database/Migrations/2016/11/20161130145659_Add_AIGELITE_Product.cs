﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161130145659)]
    public class Add_AIGELITE_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161130145659_Add_AIGELITE_Product.sql");
        }

        public override void Down()
        {

        }
    }
}