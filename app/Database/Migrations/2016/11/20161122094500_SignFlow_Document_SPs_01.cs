﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161122094500)]
    public class SignFlow_Document_SPs_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161122094500_SignFlow_Document_SPs_01.sql");
        }

        public override void Down()
        {

        }
    }
}
