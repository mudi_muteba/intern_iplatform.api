﻿using FluentMigrator;

namespace Database.Migrations._2016._11
{
    [Migration(20161102191300)]
    public class Update_SP_Reports_AIG_MotorExtractV4 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20161102191300_Update_SP_Reports_AIG_MotorExtractV4.sql");
        }

        public override void Down()
        {

        }
    }
}