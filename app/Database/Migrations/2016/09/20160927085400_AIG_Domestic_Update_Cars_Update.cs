using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160927085400)]
    public class AIG_Domestic_Update_Cars_Update : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160927085400_AIG_Domestic_Update_Cars_Update.sql");
        }

        public override void Down()
        {
            
        }
    }
}