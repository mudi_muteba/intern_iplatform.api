﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160928161200)]
    public class Delete_AIG_Report_Tables : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160928161200_Delete_AIG_Report_Tables.sql");
        }

        public override void Down()
        {

        }
    }
}
