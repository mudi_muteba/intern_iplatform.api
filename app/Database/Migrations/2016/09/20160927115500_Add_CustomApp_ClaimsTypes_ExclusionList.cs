using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160927115500)]
    public class Add_CustomApp_ClaimsTypes_ExclusionList : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160927115500_Add_CustomApp_ClaimsTypes_ExclusionList.sql");
        }

        public override void Down()
        {
            
        }
    }
}