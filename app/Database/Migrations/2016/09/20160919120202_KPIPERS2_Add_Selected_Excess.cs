using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160919120202)]
    public class KPIPERS2_Add_Selected_Excess : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160919120202_KPIPERS2_Add_Selected_Excess.sql");
        }

        public override void Down()
        {
            
        }
    }
}