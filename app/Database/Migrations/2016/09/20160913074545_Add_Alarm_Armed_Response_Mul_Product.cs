using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160913074545)]
    public class Add_Alarm_Armed_Response_Mul_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160913074545_Add_Alarm_Armed_Response_Mul_Product.sql");
        }

        public override void Down()
        {
            
        }
    }
}