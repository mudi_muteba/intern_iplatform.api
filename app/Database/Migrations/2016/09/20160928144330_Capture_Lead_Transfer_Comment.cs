using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(2016092814330)]
    public class Capture_Lead_Transfer_Comment : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.Organization).Column("CaptureLeadTransferComment").Exists())
            {
                Update.Table(Tables.Organization).Set(new { CaptureLeadTransferComment = true }).Where(new { Code = "AA" });
                Update.Table(Tables.Organization).Set(new { CaptureLeadTransferComment = true }).Where(new { Code = "AUG" });
                Update.Table(Tables.Organization).Set(new { CaptureLeadTransferComment = true }).Where(new { Code = "BIB" });
                Update.Table(Tables.Organization).Set(new { CaptureLeadTransferComment = true }).Where(new { Code = "DIAD" });
                Update.Table(Tables.Organization).Set(new { CaptureLeadTransferComment = true }).Where(new { Code = "FFW" });
                Update.Table(Tables.Organization).Set(new { CaptureLeadTransferComment = true }).Where(new { Code = "SAU" });
                Update.Table(Tables.Organization).Set(new { CaptureLeadTransferComment = true }).Where(new { Code = "UNITY" });
                Update.Table(Tables.Organization).Set(new { CaptureLeadTransferComment = true }).Where(new { Code = "VIRSEKER" });
            }
        }

        public override void Down()
        {
            
        }
    }
}