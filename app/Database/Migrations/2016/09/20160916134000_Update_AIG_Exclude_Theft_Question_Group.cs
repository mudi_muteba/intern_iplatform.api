using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160916134000)]
    public class Update_AIG_Exclude_Theft_Question_Group : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160916134000_Update_AIG_Exclude_Theft_Question_Group.sql");
        }

        public override void Down()
        {
            
        }
    }
}