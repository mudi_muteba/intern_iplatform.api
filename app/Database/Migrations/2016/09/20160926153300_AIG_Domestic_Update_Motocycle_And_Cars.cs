using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160926153300)]
    public class AIG_Domestic_Update_Motocycle_And_Cars : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160926153300_AIG_Domestic_Update_Motocycle_And_Cars.sql");
        }

        public override void Down()
        {
            
        }
    }
}