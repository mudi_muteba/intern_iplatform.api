using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160914141400)]
    public class UAP_Report_Configuration_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160914141400_UAP_Report_Configuration_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}