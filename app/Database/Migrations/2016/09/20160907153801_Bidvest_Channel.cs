using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160907153801)]
    public class Bidvest_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160907153801_Bidvest_Channel.sql");
        }

        public override void Down()
        {
            
        }
    }
}