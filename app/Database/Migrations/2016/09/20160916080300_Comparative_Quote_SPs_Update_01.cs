using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160916080300)]
    public class Comparative_Quote_SPs_Update_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160916080300_Comparative_Quote_SPs_Update_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}