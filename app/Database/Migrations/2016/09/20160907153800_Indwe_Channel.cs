using FluentMigrator;

namespace Database.Migrations._2016._08
{
    [Migration(20160907153800)]
    public class Indwe_Channel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160907153800_Indwe_Channel.sql");
        }

        public override void Down()
        {
            
        }
    }
}