using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160912115400)]
    public class Add_Additional_Excess_For_TelesureV2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160912115400_Add_Additional_Excess_For_TelesureV2.sql");
        }

        public override void Down()
        {
            
        }
    }
}