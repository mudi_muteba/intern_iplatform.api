﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160907091400)]
    public class Update_INDWE_ClaimsQuestion : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160907091400_Update_INDWE_ClaimsQuestion.sql");
        }

        public override void Down()
        {

        }
    }
}
