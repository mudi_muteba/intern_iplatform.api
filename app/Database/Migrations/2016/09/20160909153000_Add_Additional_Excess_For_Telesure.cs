using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160909153000)]
    public class Additional_Excess_For_Telesure : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160909153000_Add_Additional_Excess_For_Telesure.sql");
        }

        public override void Down()
        {
            
        }
    }
}