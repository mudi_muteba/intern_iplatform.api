using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160921130759)]
    public class IWYZE_Product_Rename : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160921130759_IWYZE_Product_Rename.sql");
        }

        public override void Down()
        {
            
        }
    }
}