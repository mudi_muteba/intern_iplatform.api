using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160923123000)]
    public class AIG_Report_Reconfiguration_03 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160923123000_AIG_Report_Reconfiguration_03.sql");
        }

        public override void Down()
        {
            
        }
    }
}