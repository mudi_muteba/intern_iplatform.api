using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160902130500)]
    public class Move_AIG_questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160902130500_Move_AIG_questions.sql");
        }

        public override void Down()
        {
            
        }
    }
}