using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160915132400)]
    public class Update_UserChannel_Keeping_Unique_Record : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160915132400_Update_UserChannel_Keeping_Unique_Record.sql");
        }

        public override void Down()
        {
            
        }
    }
}