using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160922124200)]
    public class AIG_Report_Reconfiguration_02 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160922124200_AIG_Report_Reconfiguration_02.sql");
        }

        public override void Down()
        {
            
        }
    }
}