using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160927120500)]
    public class Add_CustomApp_ClaimsTypes_Category : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160927120500_Add_CustomApp_ClaimsTypes_Category.sql");
        }

        public override void Down()
        {
            
        }
    }
}