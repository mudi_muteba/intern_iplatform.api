using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160923083930)]
    public class Fix_QuestionDefinitions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160923083930_Fix_QuestionDefinitions.sql");
        }

        public override void Down()
        {
            
        }
    }
}