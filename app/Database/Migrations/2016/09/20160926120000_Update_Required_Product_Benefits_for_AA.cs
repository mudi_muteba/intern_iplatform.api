using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160926120000)]
    public class Update_Required_Product_Benefits_for_AA : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160926120000_Update_Required_Product_Benefits_for_AA.sql");
        }

        public override void Down()
        {
            
        }
    }
}