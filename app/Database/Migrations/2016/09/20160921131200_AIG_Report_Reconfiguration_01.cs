using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160921131200)]
    public class AIG_Report_Reconfiguration_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160921131200_AIG_Report_Reconfiguration_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}