using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160922161330)]
    public class Telesure_Add_Question_MUL : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160922161330_Telesure_Add_Question_MUL.sql");
        }

        public override void Down()
        {
            
        }
    }
}