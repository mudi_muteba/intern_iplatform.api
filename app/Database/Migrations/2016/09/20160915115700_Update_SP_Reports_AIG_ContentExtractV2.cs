﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160915115700)]
    public class Update_SP_Reports_AIG_ContentExtractV2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160915115700_Update_SP_Reports_AIG_ContentExtractV2.sql");
        }

        public override void Down()
        {

        }
    }
}
