using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160909105600)]
    public class Remove_OrganizationClaimTypeCover_Add_OrganizationClaimTypeCategory : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160909105600_Remove_OrganizationClaimTypeCover_Add_OrganizationClaimTypeCategory.sql");
        }

        public override void Down()
        {
            
        }
    }
}