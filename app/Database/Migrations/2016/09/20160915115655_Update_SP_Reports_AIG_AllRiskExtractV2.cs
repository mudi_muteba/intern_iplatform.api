﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160915115655)]
    public class Update_SP_Reports_AIG_AllRiskExtractV2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160915115655_Update_SP_Reports_AIG_AllRiskExtractV2.sql");
        }

        public override void Down()
        {
            
        }
    }
}
