﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160915115600)]
    public class _20160915115600_Update_SP_Reports_AIG_MotorExtractV2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160915115600_Update_SP_Reports_AIG_MotorExtractV2.sql");
        }

        public override void Down()
        {

        }
    }
}