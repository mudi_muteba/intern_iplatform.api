﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160908215600)]
    class Update_SP_Reports_AIG_BuildingExtract : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160908215600_Update_SP_Reports_AIG_BuildingExtract.sql");
        }

        public override void Down()
        {

        }
    }
}