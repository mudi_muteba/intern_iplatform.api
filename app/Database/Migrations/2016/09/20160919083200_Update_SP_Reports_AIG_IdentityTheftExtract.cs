﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160919083200)]
    public class Update_SP_Reports_AIG_IdentityTheftExtract : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160919083200_Update_SP_Reports_AIG_IdentityTheftExtract.sql");
        }

        public override void Down()
        {

        }
    }
}
