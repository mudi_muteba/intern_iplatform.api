using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160928164200)]
    public class Domestic_Schedule_Show_Extras_On_Comprehensive_Cars : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160928164200_Domestic_Schedule_Show_Extras_On_Comprehensive_Cars.sql");
        }

        public override void Down()
        {
            
        }
    }
}