using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160921085101)]
    public class AIG_reorder_questions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160921085101_AIG_reorder_questions.sql");
        }

        public override void Down()
        {
            
        }
    }
}