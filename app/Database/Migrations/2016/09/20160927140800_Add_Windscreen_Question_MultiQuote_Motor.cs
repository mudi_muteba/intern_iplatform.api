using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160927140800)]
    public class Add_Windscreen_Question_MultiQuote_Motor : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160927140800_Add_Windscreen_Question_MultiQuote_Motor.sql");
        }

        public override void Down()
        {
            
        }
    }
}