﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160921163900)]
    public class INDWE_ProductclaimsQuestionUpdate : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160921163900_INDWE_ProductclaimsQuestionUpdate.sql");
        }

        public override void Down()
        {

        }
    }
}
