﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160905170400)]
    public class Update_SP_Reports_AIG_AllRiskExtract : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160905170400_Update_SP_Reports_AIG_AllRiskExtract.sql");
        }

        public override void Down()
        {

        }
    }
}
