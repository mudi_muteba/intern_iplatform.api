using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160920152859)]
    public class MUL_Add_Occupation_Date : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160920152859_MUL_Add_Occupation_Date.sql");
        }

        public override void Down()
        {
            
        }
    }
}