using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160914193259)]
    public class Multiquote_Add_KP_DaytimeParking : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160914193259_Multiquote_Add_KP_DaytimeParking.sql");
        }

        public override void Down()
        {
            
        }
    }
}