using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160927115000)]
    public class Add_CustomApp_ProductClaimsQuestionDefinition : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160927115000_Add_CustomApp_ProductClaimsQuestionDefinition.sql");
        }

        public override void Down()
        {
            
        }
    }
}