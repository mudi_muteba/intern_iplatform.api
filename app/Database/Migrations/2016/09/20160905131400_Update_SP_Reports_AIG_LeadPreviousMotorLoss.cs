﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160905131400)]
    public class Update_SP_Reports_AIG_LeadPreviousMotorLoss : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160905131400_Update_SP_Reports_AIG_LeadPreviousMotorLoss.sql");
        }

        public override void Down()
        {

        }
    }
}
