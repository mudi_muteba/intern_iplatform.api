using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160912141000)]
    public class Global_Report_Configuration : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160912141000_Global_Report_Configuration.sql");
        }

        public override void Down()
        {
            
        }
    }
}