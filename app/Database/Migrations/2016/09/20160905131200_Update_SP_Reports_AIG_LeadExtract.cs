﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160905131200)]
    public class Update_SP_Reports_AIG_LeadExtract : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160905131200_Update_SP_Reports_AIG_LeadExtract.sql");
        }

        public override void Down()
        {

        }
    }
}
