using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160905113300)]
    public class Create_CallCentre_Reports_SPs_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160905113300_Create_CallCentre_Reports_SPs_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}