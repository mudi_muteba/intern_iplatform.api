using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(201609231449000)]
    public class AIG_Domestic_Retail_Market_On_Cars : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("201609231449000_AIG_Domestic_Retail_Market_On_Cars.sql");
        }

        public override void Down()
        {
            
        }
    }
}