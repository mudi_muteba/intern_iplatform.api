﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160919093700)]
    public class Update_SP_Reports_AIG_PersonalLegalLiabilityExtract : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160919093700_Update_SP_Reports_AIG_PersonalLegalLiabilityExtract.sql");
        }

        public override void Down()
        {

        }
    }
}
