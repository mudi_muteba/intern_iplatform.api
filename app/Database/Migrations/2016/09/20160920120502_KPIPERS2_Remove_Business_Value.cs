using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160920120502)]
    public class KPIPERS2_Remove_Business_Value : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160920120502_KPIPERS2_Remove_Business_Value.sql");
        }

        public override void Down()
        {
            
        }
    }
}