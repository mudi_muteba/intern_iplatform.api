using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160905081200)]
    public class Add_INDWE_ProductClaimsQuestionDefinition : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160905081200_Add_INDWE_ProductClaimsQuestionDefinition.sql");
        }

        public override void Down()
        {
            
        }
    }
}