using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160929145500)]
    public class Domestic_Schedule_Default_Choose_Additional_Option : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160929145500_Domestic_Schedule_Default_Choose_Additional_Option.sql");
        }

        public override void Down()
        {
            
        }
    }
}