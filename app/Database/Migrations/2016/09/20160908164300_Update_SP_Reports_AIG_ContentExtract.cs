﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160908164300)]
    public class Update_SP_Reports_AIG_ContentExtract : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160908164300_Update_SP_Reports_AIG_ContentExtract.sql");
        }

        public override void Down()
        {

        }
    }
}
