using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160916101000)]
    public class AIG_SalesForce_Reports_Extract : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160916101000_AIG_SalesForce_Reports_Extract.sql");
        }

        public override void Down()
        {
            
        }
    }
}