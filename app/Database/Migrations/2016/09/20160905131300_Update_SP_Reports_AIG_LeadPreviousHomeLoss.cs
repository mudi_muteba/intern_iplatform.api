﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160905131300)]
    class Update_SP_Reports_AIG_LeadPreviousHomeLoss : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160905131300_Update_SP_Reports_AIG_LeadPreviousHomeLoss.sql");
        }

        public override void Down()
        {

        }
    }
}
