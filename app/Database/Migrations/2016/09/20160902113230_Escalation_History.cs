using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160902113230)]
    public class Escalation_History : Migration
    {
        public override void Up()
        {
            if (Schema.Table(Tables.EscalationPlanExecutionHistory).Column("EscalationWorkflowMessageStatusEnumType").Exists())
                Delete.Column("EscalationWorkflowMessageStatusEnumType").FromTable(Tables.EscalationPlanExecutionHistory);

            if (Schema.Table(Tables.EscalationPlanExecutionHistory).Column("EntityId").Exists())
                Delete.Column("EntityId").FromTable(Tables.EscalationPlanExecutionHistory);
        }

        public override void Down()
        {
            
        }
    }
}