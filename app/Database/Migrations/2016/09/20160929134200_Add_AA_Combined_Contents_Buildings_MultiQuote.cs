using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160929134200)]
    public class Add_AA_Combined_Contents_Buildings_MultiQuote : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160929134200_Add_AA_Combined_Contents_Buildings_MultiQuote.sql");
            Execute.EmbeddedScript("20160929134300_Add_AA_Combined_MultiQuote_Comparison.sql");
        }

        public override void Down()
        {
            
        }
    }
}