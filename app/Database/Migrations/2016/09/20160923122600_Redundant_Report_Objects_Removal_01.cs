using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160923122600)]
    public class Redundant_Report_Objects_Removal_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160923122600_Redundant_Report_Objects_Removal_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}