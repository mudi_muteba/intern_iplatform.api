using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160922073000)]
    public class Remove_VIN_Engine_From_KP_MQ_Mapping : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160922073000_Remove_VIN_Engine_From_KP_MQ_Mapping.sql");
        }

        public override void Down()
        {

        }
    }
}