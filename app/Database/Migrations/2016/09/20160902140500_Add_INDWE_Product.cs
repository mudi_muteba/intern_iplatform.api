using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160902140500)]
    public class Add_INDWE_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160902140500_Add_INDWE_Product.sql");
        }

        public override void Down()
        {
            
        }
    }
}