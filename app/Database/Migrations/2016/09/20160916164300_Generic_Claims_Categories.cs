using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160916164300)]
    public class Generic_Claims_Categories : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160916164300_Generic_Claims_Categories.sql");
        }

        public override void Down()
        {
            
        }
    }
}