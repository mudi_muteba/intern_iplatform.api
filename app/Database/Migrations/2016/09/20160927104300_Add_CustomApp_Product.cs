using FluentMigrator;

namespace Database.Migrations._2016._09
{
[Migration(20160927104300)]
    public class Add_CustomApp_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160927104300_Add_CustomApp_Product.sql");
        }

        public override void Down()
        {
            
        }
    }
}