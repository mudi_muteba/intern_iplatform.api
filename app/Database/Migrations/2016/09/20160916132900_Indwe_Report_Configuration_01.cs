using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160916132900)]
    public class Indwe_Report_Configuration_01 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160916132900_Indwe_Report_Configuration_01.sql");
        }

        public override void Down()
        {
            
        }
    }
}