﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160919133800)]
    public class Update_SP_Reports_AIG_FuneralExtract : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160919133800_Update_SP_Reports_AIG_FuneralExtract.sql");
        }

        public override void Down()
        {

        }
    }
}
