﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160919100100)]
    public class Update_SP_Reports_AIG_DisasterCashExtract : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160919100100_Update_SP_Reports_AIG_DisasterCashExtract.sql");
        }

        public override void Down()
        {

        }
    }
}
