﻿using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160905131500)]
    public class Update_SP_Reports_AIG_MotorExtract : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160905131500_Update_SP_Reports_AIG_MotorExtract.sql");
        }

        public override void Down()
        {

        }
    }
}
