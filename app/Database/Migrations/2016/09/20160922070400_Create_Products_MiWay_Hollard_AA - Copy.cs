using FluentMigrator;

namespace Database.Migrations._2016._09
{
    [Migration(20160922070400)]
    public class Create_Products_MiWay_Hollard_AA : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160922070400_Create_Products_Add_To_MultiQuote.sql");
            Execute.EmbeddedScript("20160922070400_Create_Products_AA.sql");
            Execute.EmbeddedScript("20160922070400_Create_Products_Hollard.sql");
            Execute.EmbeddedScript("20160922070400_Create_Products_MiWay.sql");
        }

        public override void Down()
        {

        }
    }
}