﻿using FluentMigrator;

namespace Database.Profiles
{
    [Profile("Development")]
    public class Development : Migration
    {
        public override void Up()
        {
            //Execute.EmbeddedScript("_remove_all_data.sql");
            Execute.EmbeddedScript("0_channels.sql");
            Execute.EmbeddedScript("1_users.sql");
            Execute.EmbeddedScript("2_authorisation.sql");
            //Execute.EmbeddedScript("3_workflow.sql");
            Execute.EmbeddedScript("Insert_Root_User.sql");

            //Execute.EmbeddedScript("4_individual.sql");
            Execute.EmbeddedScript("3_propsal_header.sql");
            Execute.EmbeddedScript("0_insert_quote_header.sql");
            Execute.EmbeddedScript("1_insert_quote.sql");
            Execute.EmbeddedScript("2_insert_quote_items.sql");
            
           
            Execute.EmbeddedScript("5_propsal_definition.sql");
            Execute.EmbeddedScript("6_update_party_with_channel_id.sql");

            Execute.EmbeddedScript("0_Address.data.sql");
            Execute.EmbeddedScript("1_ProposalQuestionAnswer.data.sql");
            Execute.EmbeddedScript("2_ContactDetail.data.sql");

            Execute.EmbeddedScript("0_Unreferenced_LeadActivity.data.sql");
        }

        public override void Down()
        {
        }
    }
}