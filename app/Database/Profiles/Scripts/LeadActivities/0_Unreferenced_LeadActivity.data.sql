﻿delete from ClaimsLeadActivity;
delete from ImportedLeadActivity;
delete from ProposalLeadActivity;
delete from QuotedLeadActivity;
delete from QuoteAcceptedLeadActivity;
delete from ClaimsLeadActivity;
delete from DelayLeadActivity;
delete from DeadLeadActivity;
delete from LossLeadActivity;
delete from PolicyLeadActivity;
delete from LossLeadActivity;
delete from LeadActivity WHERE Id not in (SELECT LeadActivityId from ProposalHeader);

update LeadActivity set UserId = 1

DECLARE @ContactDetailId int, @PartyId int, @LeadActivityId int;

--Insert Contact Details
INSERT INTO ContactDetail (Work, Direct, Cell, Home, Fax, Email, Website, Facebook, Twitter, LinkedIn, Skype, IsDeleted) 
VALUES ('0114921474', NULL, '0731451425', null, null, null, null, null, null, null, null, 0); 
select @ContactDetailId = SCOPE_IDENTITY();

--Insert Party
INSERT INTO Party (ExternalReference, PartyTypeId, DisplayName, DateCreated, DateUpdated, MasterId, IsDeleted, ContactDetailId, ChannelId) 
VALUES (NEWID(), 1, 'Florence, Khambule', GetDate(), GetDate(), 0, 0, @ContactDetailId, 1); 
select @PartyId = SCOPE_IDENTITY();

--Insert Individual
INSERT INTO Individual (TitleId, GenderId, FirstName, MiddleName, Surname, DateOfBirth, IdentityNo, PassportNo, MaritalStatusId, 
LanguageId, AnyJudgements, AnyJudgementsReason, UnderAdministrationOrDebtReview, UnderAdministrationOrDebtReviewReason, 
BeenSequestratedOrLiquidated, BeenSequestratedOrLiquidatedReason, PartyId) 
VALUES (29, 2, 'Florence', NULL, 'Khambule', '1987-08-07 20:00:00', '985745621236', NULL, 4, 8, 0, NULL, 0, NULL, 0, NULL, @PartyId);


--CREATED
--Insert LeadActivity
INSERT INTO LeadActivity (DateCreated, DateUpdated, CampaignSourceId, ActivityTypeId, IsDeleted, CampaignId, UserId, PartyId, LeadId) 
VALUES (GetDate(), GetDate(), 0, 1, 0, NULL, 1, NULL, NULL);
select @LeadActivityId = SCOPE_IDENTITY();

--Insert CreateLeadActivity
INSERT INTO CreateLeadActivity (Description, LeadActivityId) VALUES ('Created Individual ' + CONVERT(varchar(50), @PartyId), @LeadActivityId);


--IMPORTED
--Insert LeadActivity
INSERT INTO LeadActivity (DateCreated, DateUpdated, CampaignSourceId, ActivityTypeId, IsDeleted, CampaignId, UserId, PartyId, LeadId) 
VALUES (GetDate(), GetDate(), 0, 1, 0, NULL, 1, NULL, NULL);
select @LeadActivityId = SCOPE_IDENTITY();

--Insert CreateLeadActivity
INSERT INTO ImportedLeadActivity (LeadActivityId) VALUES (@LeadActivityId);


--LOSS
--Insert LeadActivity
INSERT INTO LeadActivity (DateCreated, DateUpdated, CampaignSourceId, ActivityTypeId, IsDeleted, CampaignId, UserId, PartyId, LeadId) 
VALUES (GetDate(), GetDate(), 0, 1, 0, NULL, 1, NULL, NULL);
select @LeadActivityId = SCOPE_IDENTITY();

--Insert CreateLeadActivity
INSERT INTO LossLeadActivity (LeadActivityId) VALUES (@LeadActivityId);


--Delay
--Insert LeadActivity
INSERT INTO LeadActivity (DateCreated, DateUpdated, CampaignSourceId, ActivityTypeId, IsDeleted, CampaignId, UserId, PartyId, LeadId) 
VALUES (GetDate(), GetDate(), 0, 1, 0, NULL, 1, NULL, NULL);
select @LeadActivityId = SCOPE_IDENTITY();

--Insert CreateLeadActivity
INSERT INTO DelayLeadActivity (LeadActivityId) VALUES (@LeadActivityId);

--Dead
--Insert LeadActivity
INSERT INTO LeadActivity (DateCreated, DateUpdated, CampaignSourceId, ActivityTypeId, IsDeleted, CampaignId, UserId, PartyId, LeadId) 
VALUES (GetDate(), GetDate(), 0, 1, 0, NULL, 1, NULL, NULL);
select @LeadActivityId = SCOPE_IDENTITY();

--Insert CreateLeadActivity
INSERT INTO DeadLeadActivity (LeadActivityId) VALUES (@LeadActivityId);

