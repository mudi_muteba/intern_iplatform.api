﻿if not exists(select * from userAuthorisationGroup where userId = 1 and AuthorisationGroupId = 5 and ChannelId = 1)
	begin
		insert into userAuthorisationGroup
		(AuthorisationGroupId, UserId, ChannelId)
		values(5, 1, 1)
	end

if not exists(select * from userAuthorisationGroup where userId = 1 and AuthorisationGroupId = 5 and ChannelId = 2)
	begin
		insert into userAuthorisationGroup
		(AuthorisationGroupId, UserId, ChannelId)
		values(5, 1, 2)
	end

if not exists(select * from userAuthorisationGroup where userId = 2 and AuthorisationGroupId = 5 and ChannelId = 2)
	begin
		insert into userAuthorisationGroup
		(AuthorisationGroupId, UserId, ChannelId)
		values(5, 2, 2)
	end

if not exists(select * from userAuthorisationGroup where userId = 1 and AuthorisationGroupId = 5 and ChannelId = 3)
	begin
		insert into userAuthorisationGroup
		(AuthorisationGroupId, UserId, ChannelId)
		values(5, 1, 3)
	end


if not exists (select id from UserChannel where userid = 1 and channelid = 1)
	begin
		insert into UserChannel 
		(CreatedAt, ModifiedAt, UserId, ChannelId)
		values(GETDATE(), GETDATE(), 1, 1)
	end

if not exists (select id from UserChannel where userid = 1 and channelid = 2)
	begin
		insert into UserChannel 
		(CreatedAt, ModifiedAt, UserId, ChannelId)
		values(GETDATE(), GETDATE(), 1, 2)
	end

if not exists (select id from UserChannel where userid = 1 and channelid = 3)
	begin
		insert into UserChannel 
		(CreatedAt, ModifiedAt, UserId, ChannelId)
		values(GETDATE(), GETDATE(), 1, 3)
	end

if not exists (select id from UserChannel where userid = 2 and channelid = 2)
	begin
		insert into UserChannel 
		(CreatedAt, ModifiedAt, UserId, ChannelId)
		values(GETDATE(), GETDATE(), 2, 2)
	end

if not exists (select id from UserChannel where userid = 1000000 and channelid = 1000000)
	begin
		insert into UserChannel 
		(CreatedAt, ModifiedAt, UserId, ChannelId)
		values(GETDATE(), GETDATE(), 1000000, 1000000)
	end
