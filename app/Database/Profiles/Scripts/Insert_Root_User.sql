﻿IF NOT EXISTS(SELECT * FROM [dbo].[User] WHERE UserName = 'root@iplatform.co.za')
	BEGIN
		INSERT INTO [dbo].[User]
           ([UserName]
           ,[Password]
           ,[SystemId]
           ,[IsApproved]
           ,[IsActive]
           ,[CreatedOn]
           ,[ModifiedOn]
           ,[IsDeleted])
     VALUES
           ('root@iplatform.co.za'
           ,'100:gBzVCiDjP2HUuPIt6FhOCBB9pTH+JursOyDX4hjxbqgc44VPROPsKeQYAVqVYEXifuM8MpZRh/QbIHK18dAq0L+X2MfEgwvob1gi8/ZuZn3h2WkIVndUXjDCGZMxcKcdiOMM9yAMWf2rOrbCw6i5Ohy7yS7qSre3E01ormTPKytRa1bOsGFjj70HULP+pTl7z4TgIIy8ojzVz/j46DuAc5E5hQraqI3a0GYsZ4BIHzadP9dO4UAC7p/fZ8iseOm2Ac3vyLwO0JsTlRA16zWUr9Ku30WzzVUFwDYYrRlKQEE4B73k+yqKFmWKpWaBjybTpEZNVHwqR/bFOn536o0vfQ==:DJRV8cSKEfxVMNnqLi+Ecr94pgazc1AyOOoullVznKH/3NfgwHoeuWZFwX8Cgs0eDdCqSuR86gosG9Ld/xkCki7dBkB3tFrIuwapccG2s6pJKII941B5pfE8kgwq6idAj5zmLvWFlHI3CcyNjRJmjZ+cryfhIYUS/yncQKDWCb5B6IhDNZqUJH3aE+MMKVhJ2ARMI3FKQTZCGBmT9cdy+8ciOtUImd8udjayj0o6nD61uCBLw1jzMSuXbmtY1CAwM0zunX2jNKnCauxRd1r/WyGb3ENvVFGbvWfYJNV7IvOs4WOSFBDRM0XdBOy6cLV8ZZYy4nPnZRFY1F+Q5Bdi2g=='
           , NEWID()
		   , 1
           , 1
           ,GETDATE()
           ,GETDATE()
           ,0)
	END
GO

IF NOT EXISTS(SELECT * FROM [dbo].[Channel] WHERE Id = 1)
	BEGIN
		SET IDENTITY_INSERT [dbo].[Channel] ON;
		INSERT INTO [dbo].[Channel]
           ([Id]
		   ,[SystemId]
           ,[IsActive]
           ,[ActivatedOn]
           ,[DeactivatedOn]
           ,[IsDefault]
           ,[IsDeleted]
           ,[CurrencyId]
           ,[DateFormat]
           ,[Name]
           ,[LanguageId])
     VALUES
           (1
		   ,NEWID()
           ,1
           ,GETDATE()
           ,GETDATE()
           ,0
           ,0
           ,1
           ,'dd MMM yyyy'
           ,'Channel'
           ,2)
		SET IDENTITY_INSERT [dbo].[Channel] OFF;
	END

DECLARE @userId INT, @authorisationGroupId INT;
SET @userId = (SELECT Id FROM [dbo].[User] WHERE UserName = 'root@iplatform.co.za')
SET @authorisationGroupId = (SELECT Id FROM [md].AuthorisationGroup WHERE Name = 'Admin')

IF NOT EXISTS(SELECT * FROM [dbo].[UserChannel] WHERE UserId = @userId AND ChannelId = 1)
	BEGIN
		INSERT INTO [dbo].[UserChannel]
           ([CreatedAt]
           ,[ModifiedAt]
           ,[UserId]
           ,[ChannelId]
           ,[IsDeleted])
		VALUES
           (GETDATE()
           ,GETDATE()
           ,@userId
           ,1
           ,0)
	END

IF NOT EXISTS(SELECT * FROM [dbo].[UserAuthorisationGroup] WHERE AuthorisationGroupId = AuthorisationGroupId AND UserId = @userId AND ChannelId = 1)
	BEGIN
		INSERT INTO [dbo].[UserAuthorisationGroup]
           ([AuthorisationGroupId]
           ,[UserId]
           ,[ChannelId]
           ,[IsDeleted])
		VALUES
           (@authorisationGroupId
           ,@userId
           ,1
           ,0)
	END

GO