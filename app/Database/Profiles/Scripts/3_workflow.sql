﻿SET IDENTITY_INSERT dbo.ChannelEvent ON;
GO

declare @channel1 int
declare @channel2 int
declare @channel3 int
declare @channel4 int

set @channel1 = 1
set @channel2 = 2
set @channel3 = 3
set @channel4 = 4

insert into ChannelEvent
(Id, EventName, ChannelId, IsDeleted, ProductCode)
values(1, 'ExternalRatingRequestReceivedEvent', 1, 0, null)

insert into ChannelEventTask
(TaskName, ChannelEventId, IsDeleted)
values(1, 1, 0)

insert into ChannelEvent
(Id, EventName, ChannelId, IsDeleted, ProductCode)
values(2, 'QuoteDistributionViaSMSRequestedEvent', 1, 0, null)

insert into ChannelEventTask
(TaskName, ChannelEventId, IsDeleted)
values(0, 2, 0)

SET IDENTITY_INSERT dbo.ChannelEvent OFF;
