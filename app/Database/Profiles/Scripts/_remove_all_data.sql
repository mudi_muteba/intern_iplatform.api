﻿delete from quoteAcceptedLeadActivity

delete from quoteItemStateEntry
delete from quoteItemState
delete from quoteItem

delete from QuoteStateEntry
delete from QuoteState
delete from QuotedLeadActivity
delete from Quote

delete from QuoteHeaderStateEntry
delete from quoteheaderstate
delete from QuoteAcceptedLeadActivity
delete from quoteheader

delete from proposalQuestionAnswer
delete from proposalDefinition
delete from proposalLeadActivity
delete from proposalheader
delete from Note

delete from individual

delete from [dbo].[PolicyFuneral]
delete from [dbo].[PolicySpecial]
delete from [dbo].[AssetAddress]
delete from [address]

delete from SecondLevelQuestionSavedAnswer

delete from ChannelEventTask
delete from ChannelEvent
delete from userChannel
DELETE FROM [dbo].[AuthorisationGroupPoint]
DELETE FROM [dbo].[AuthorisationPoint] 
delete from UserAuthorisationGroup
delete from [User]

delete from channel

update [dbo].[Party] set ContactDetailId = null
delete from [dbo].[ContactDetail]
delete from [dbo].[BankDetails]
delete from [dbo].[Address]

DELETE FROM AuthorisationGroupPoint WHERE AuthorisationPointId in (44,45,46,47,48);

DELETE FROM [dbo].[AuthorisationPoint] WHERE [Id] = 44 AND [Name] = 'Create' AND [AuthorisationPointCategoryId] = 18;
DELETE FROM [dbo].[AuthorisationPoint] WHERE [Id] = 45 AND [Name] = 'Edit' AND [AuthorisationPointCategoryId] = 18;
DELETE FROM [dbo].[AuthorisationPoint] WHERE [Id] = 46 AND [Name] = 'Delete' AND [AuthorisationPointCategoryId] = 18;
DELETE FROM [dbo].[AuthorisationPoint] WHERE [Id] = 47 AND [Name] = 'List' AND [AuthorisationPointCategoryId] = 18;
DELETE FROM [dbo].[AuthorisationPoint] WHERE [Id] = 48 AND [Name] = 'Import' AND [AuthorisationPointCategoryId] = 18

Delete From [dbo].[AuthorisationPointCategory] WHERE Id = 18 AND Name = 'Lead';


/*Clear all existing users*/
delete from [dbo].[UserChannel] WHERE [Userid] > 1000000;
delete from [dbo].[UserIndividual] WHERE [Userid] > 1000000;
delete from [dbo].[UserAuthorisationGroup] WHERE [Userid] > 1000000;
delete from [dbo].[User] WHERE [Id] > 1000000;

