﻿/* CHANNEL */
SET IDENTITY_INSERT dbo.Channel ON;
GO

declare @channel1 uniqueIdentifier
declare @channel2 uniqueIdentifier
declare @channel3 uniqueIdentifier
declare @channel4 uniqueIdentifier
declare @currencyId int
declare @dateFormat varchar(50)
declare @languageId varchar(50)

set @channel1 = '16f67549-b713-4558-a38d-a399bee346f0'
set @channel2 = '6b526364-cf69-4adf-8d79-b26b5d66fab5'
set @channel3 = '5b3892c0-c013-44f9-920b-6122e6d0ff42'
set @channel4 = 'd1454cb3-8f89-46d1-9aa4-e1d11a3c6774'

set @currencyId = 1
set @dateFormat = 'dd MMM yyyy'
set @languageId = 2

if not exists(select * from channel where id = 1)
	begin
		insert into Channel
		(id, SystemId, IsActive, ActivatedOn, IsDefault, CurrencyId, DateFormat, LanguageId)
		values(1, @channel1, 1, getdate(), 0, @currencyId, @dateFormat, @languageId)
	end

if not exists(select * from channel where id = 2)
	begin
		insert into Channel
		(id, SystemId, IsActive, ActivatedOn, IsDefault, CurrencyId, DateFormat, LanguageId)
		values(2, @channel2, 1, getdate(), 0, @currencyId, @dateFormat, @languageId)
	end

if not exists(select * from channel where id = 3)
	begin
		insert into Channel
		(id, SystemId, IsActive, ActivatedOn, IsDefault, CurrencyId, DateFormat, LanguageId)
		values(3, @channel3, 1, getdate(), 0, @currencyId, @dateFormat, @languageId)
	end

if not exists(select * from channel where id = 1000000)
	begin
		insert into Channel
		(id, SystemId, IsActive, ActivatedOn, IsDefault, CurrencyId, DateFormat, LanguageId)
		values(1000000, @channel4, 1, getdate(), 0, @currencyId, @dateFormat, @languageId)
	end

update product set EndDate = null where Id = 1
if not exists(select * from settingsiRate where ChannelId = @channel1 and ProductId = 1)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(1, @channel1, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 2
if not exists(select * from settingsiRate where ChannelId = @channel1 and ProductId = 2)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(2, @channel1, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 2
if not exists(select * from settingsiRate where ChannelId = @channel2 and ProductId = 2)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(2, @channel2, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 3
if not exists(select * from settingsiRate where ChannelId = @channel1 and ProductId = 3)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(3, @channel1, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 4
if not exists(select * from settingsiRate where ChannelId = @channel1 and ProductId = 4)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(4, @channel1, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 5
if not exists(select * from settingsiRate where ChannelId = @channel1 and ProductId = 5)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(5, @channel1, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 6
if not exists(select * from settingsiRate where ChannelId = @channel1 and ProductId = 6)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(6, @channel1, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 7
if not exists(select * from settingsiRate where ChannelId = @channel1 and ProductId = 7)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(7, @channel1, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 8
if not exists(select * from settingsiRate where ChannelId = @channel1 and ProductId = 8)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(8, @channel1, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 9
if not exists(select * from settingsiRate where ChannelId = @channel1 and ProductId = 9)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(9, @channel1, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 10
if not exists(select * from settingsiRate where ChannelId = @channel1 and ProductId = 10)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(10, @channel1, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 11
if not exists(select * from settingsiRate where ChannelId = @channel1 and ProductId = 11)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(11, @channel1, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 11
if not exists(select * from settingsiRate where ChannelId = @channel2 and ProductId = 11)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(11, @channel2, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 44
if not exists(select * from settingsiRate where ChannelId = @channel2 and ProductId = 44)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(44, @channel2, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 43
if not exists(select * from settingsiRate where ChannelId = @channel2 and ProductId = 43)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(43, @channel2, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 37
if not exists(select * from settingsiRate where ChannelId = @channel2 and ProductId = 37)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(37, @channel2, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 34
if not exists(select * from settingsiRate where ChannelId = @channel2 and ProductId = 34)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(34, @channel2, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 33
if not exists(select * from settingsiRate where ChannelId = @channel2 and ProductId = 33)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(33, @channel2, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 30
if not exists(select * from settingsiRate where ChannelId = @channel3 and ProductId = 30)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(30, @channel3, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 24
if not exists(select * from settingsiRate where ChannelId = @channel3 and ProductId = 24)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(24, @channel3, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 26
if not exists(select * from settingsiRate where ChannelId = @channel3 and ProductId = 26)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(26, @channel3, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 18
if not exists(select * from settingsiRate where ChannelId = @channel3 and ProductId = 18)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(18, @channel3, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 16
if not exists(select * from settingsiRate where ChannelId = @channel3 and ProductId = 16)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(16, @channel3, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 25
if not exists(select * from settingsiRate where ChannelId = @channel4 and ProductId = 25)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(25, @channel4, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 26
if not exists(select * from settingsiRate where ChannelId = @channel4 and ProductId = 26)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(26, @channel4, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 10
if not exists(select * from settingsiRate where ChannelId = @channel4 and ProductId = 10)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(10, @channel4, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 36
if not exists(select * from settingsiRate where ChannelId = @channel4 and ProductId = 36)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(36, @channel4, 'LIVE', 'userId')
	end

update product set EndDate = null where Id = 28
if not exists(select * from settingsiRate where ChannelId = @channel4 and ProductId = 28)
	begin
		insert into SettingsiRate
		(ProductId, ChannelId, Environment, UserId)
		values(28, @channel4, 'LIVE', 'userId')
	end


if not exists(select * from communicationSetting where ChannelId = @channel1 and SMSProviderName = 'CellFind')
	begin
		insert into CommunicationSetting
		(ChannelId, SMSProviderName, SMSUsername, SMSPassword, SMSUrl)
		values(@channel1, 'CellFind', 'AIGtest2', '@iGt3st2', 'http://cfwinqa.cellfind.co.za/mpgproxy/Service.asmx')
	end

SET IDENTITY_INSERT dbo.Channel OFF;
