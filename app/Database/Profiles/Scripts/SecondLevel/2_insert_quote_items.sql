﻿SET IDENTITY_INSERT [dbo].[QuoteItem] ON
INSERT INTO [dbo].[QuoteItem] ([Id], [QuoteId], [CoverDefinitionId], [SumInsured], [Premium], [Sasria], [AssetNumber], [ExcessCalculated], [ExcessBasic], [ExcessDescription], [Description], [voluntaryExcess]) 
VALUES (2221, 2006, 14, CAST(0.0000 AS Decimal(18, 4)), CAST(168.1818 AS Decimal(18, 4)), CAST(0.0360 AS Decimal(18, 4)), 222, 1, CAST(3000.0000 AS Decimal(18, 4)), N'Note that the total sum insured for all products combined cannot exceed R500,000..1. R250 for each and every claim excluding cellular phones and pedal cycles.2. R500 for each and every claim in respect of cellular phones.
3. 10% of claim minimum R250 for each and every claim in respect of pedal cycles.
', NULL, CAST(0.00000 AS Decimal(19, 5)))
SET IDENTITY_INSERT [dbo].[QuoteItem] OFF
