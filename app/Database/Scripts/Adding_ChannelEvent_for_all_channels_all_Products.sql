﻿DECLARE @ChannelCursor CURSOR, @productCursor CURSOR,  @ChannelId int, @productcode varchar(50), @channelEventId int;

SET @ChannelCursor = CURSOR FOR select Id FROM Channel WHERE IsDeleted != 1;  
OPEN @ChannelCursor FETCH NEXT FROM @ChannelCursor INTO @ChannelId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		print 'looking in Channel:'+ STR(@ChannelId);

		SET @productCursor = CURSOR FOR select ProductCode FROM Product WHERE IsDeleted != 1;  
		OPEN @productCursor FETCH NEXT FROM @productCursor INTO @productcode
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				print 'looking in Channel:'+ STR(@ChannelId) + ' and Product: '+ @productcode;
				if NOT EXISTs(SELECT * FROM ChannelEvent WHERE ChannelId = @ChannelId AND ProductCode = @productcode AND EventName = 'ExternalQuoteAcceptedWithUnderwritingEnabledEvent')
				BEGIN
					INSERT INTO ChannelEvent (EventName, ChannelId, ProductCode, IsDeleted) VALUES ('ExternalQuoteAcceptedWithUnderwritingEnabledEvent', @ChannelId, @productcode, 0);
					SELECT @channelEventId = @@IDENTITY;
					INSERT INTO ChannelEventTask(ChannelEventId, TaskName, IsDeleted) Values (@channelEventId, '2', 0)
				END
				FETCH NEXT FROM @productCursor INTO @productcode
			END
			CLOSE @productCursor 
			DEALLOCATE @productCursor;

		FETCH NEXT FROM @ChannelCursor INTO @ChannelId
	END
		
CLOSE @ChannelCursor 
DEALLOCATE @ChannelCursor;

SELECT * FROM ChannelEvent order by ChannelId, ProductCode