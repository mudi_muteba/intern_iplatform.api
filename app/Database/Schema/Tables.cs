namespace Database.Schema
{
    public static class Tables
    {
        public const string Lead = "Lead";

        public const string Party = "Party";
        public const string PartyType = "PartyType";
        public const string Organization = "Organization";
        public const string Individual = "Individual";

        public const string Role = "Role";
        public const string RoleType = "RoleType";
        public const string RelationshipType = "RelationshipType";

        public const string Product = "Product";
        public const string ProductType = "ProductType";

        public const string Cover = "Cover";
        public const string CoverDefinition = "CoverDefinition";
        public const string CoverDefinitionType = "CoverDefinitionType";

        public const string Question = "Question";
        public const string QuestionType = "QuestionType";
        public const string QuestionGroup = "QuestionGroup";
        public const string QuestionAnswer = "QuestionAnswer";

        public const string QuestionDefinition = "QuestionDefinition";
        public const string QuestionDefinitionType = "QuestionDefinitionType";
        public const string QuestionAnswerExclusion = "QuestionAnswerExclusion";

        public const string Address = "Address";
        public const string AddressType = "AddressType";
        public const string StateProvince = "StateProvince";
        public const string Country = "Country";
        public const string PostalCode = "PostalCode";

        public const string ProductFee = "ProductFee";
        public const string ProductFeeType = "ProductFeeType";
        public const string PaymentPlan = "PaymentPlan";

        public const string ClientSite = "ClientSite";
        public const string MessageQueue = "MessageQueue";
        public const string MessageClient = "MessageClient";

        public const string FirstLevelQuestion = "FirstLevelQuestion";
        public const string FirstLevelQuestionDefinition = "FirstLevelQuestionDefinition";

        public const string Channel = "Channel";
        public const string VehicleGuideSetting = "VehicleGuideSetting";
        public const string PersonLookupSetting = "PersonLookupSetting";
        public const string UserChannel = "UserChannel";

        public const string User = "User";
        public const string UserAuthorisationGroup = "UserAuthorisationGroup";
        public const string AuthorisationPointCategory = "AuthorisationPointCategory";
        public const string AuthorisationPoint = "AuthorisationPoint";
        public const string AuthorisationGroupPoint = "AuthorisationGroupPoint";
        public const string AuthorisationGroup = "AuthorisationGroup";
        public const string ChannelEvent = "ChannelEvent";
        public const string ChannelEventTask = "ChannelEventTask";
        public const string SettingsiRate = "SettingsiRate";
        public const string ContactDetail = "ContactDetail";

        public static string ProductBenefits = "ProductBenefit";

        public static string UserIndividual = "UserIndividual";

        public static string ProposalHeader = "ProposalHeader";
        public static string ProposalQuestionAnswer = "ProposalQuestionAnswer";
        public static string Relationship = "Relationship";
        public const string AssetAddress = "AssetAddress";
        public const string Tag = "Tag";
        public const string TagCampaignMap = "TagCampaignMap";
        public const string TagProductMap = "TagProductMap";
        public const string CampaignReference = "CampaignReference";
        public const string Campaign = "Campaign";
        public static string QuoteHeader = "QuoteHeader";
        public static string QuoteHeaderState = "QuoteHeaderState";
        public static string Quote = "Quote";
        public static string QuoteItem = "QuoteItem";
        public static string QuoteState = "QuoteState";
        public static string QuoteItemState = "QuoteItemState";
        public static string QuoteItemStateEntry = "QuoteItemStateEntry";
        public static string CommunicationSettings = "CommunicationSetting";
        public static string QuoteDistribution = "QuoteDistribution";
        public static string PartyCampaign = "PartyCampaign";

        public static string ActivityType = "ActivityType";
        public static string LeadActivity = "LeadActivity";
        public static string ProductClaimsQuestionDefinition = "ProductClaimsQuestionDefinition";

        public static string PolicyHeader = "PolicyHeader";
        public static string PolicyItem = "PolicyItem";

        public static string ClaimsHeader = "ClaimsHeader";
        public static string ClaimsItem = "ClaimsItem";
        public static string ClaimsItemQuestionAnswer = "ClaimsItemQuestionAnswer";
        public static string ClaimsLeadActivity = "ClaimsLeadActivity";
        public static string ClaimsType = "ClaimsType";

        public static string LeadImport = "LeadImport";


        public const string SecondLevelQuestion = "SecondLevelQuestion";
        public const string SecondLevelQuestionDefinition = "SecondLevelQuestionDefinition";
        public const string SecondLevelQuestionGroup = "SecondLevelQuestionGroup";
        public const string SecondLevelQuestionAnswer = "SecondLevelQuestionAnswer";
        public const string SecondLevelQuestionSavedAnswer = "SecondLevelQuestionSavedAnswer";

        public const string ProposalLeadActivity = "ProposalLeadActivity";
        public const string PolicyLeadActivity = "PolicyLeadActivity";
        public const string QuotedLeadActivity = "QuotedLeadActivity";
        public const string QuoteAcceptedLeadActivity = "QuoteAcceptedLeadActivity";

        public const string Team = "Team";
        public const string TeamUser = "TeamUser";
        public const string TeamCampaign = "TeamCampaign";

        public const string IndividualUploadHeader = "IndividualUploadHeader";
        public const string IndividualUploadDetail = "IndividualUploadDetail";

        public const string MemberRelationship = "MemberRelationship";
        public const string FuneralMember = "FuneralMember";
        public const string ProposalDefinition = "ProposalDefinition";
        public const string PolicyStatus = "PolicyStatus";
        public const string MethodOfPayment = "MethodOfPayment";


        public const string ProductLineOfLoss = "ProductLineOfLoss";
        public const string HomeTypeOfLoss = "HomeTypeOfLoss";
        public const string MotorTypeOfLoss = "MotorTypeOfLoss";
        public const string LossHistory = "LossHistory";
        public const string HomeClaimAmount = "HomeClaimAmount";
        public const string MotorClaimAmount = "MotorClaimAmount";
        public const string HomeClaimLocation = "HomeClaimLocation";
        public const string CurrentlyInsured = "CurrentlyInsured";
        public const string UninterruptedPolicy = "UninterruptedPolicy";
        public const string MotorCurrentTypeOfCover = "MotorCurrentTypeOfCover";
        public const string MotorUninterruptedPolicyNoClaim = "MotorUninterruptedPolicyNoClaim";

        public const string HomeLossHistory = "HomeLossHistory";
        public const string MotorLossHistory = "MotorLossHistory";

        public const string Contact = "Contact";

        public const string Currency = "Currency";

        public const string OrganizationClaimTypeExclusion = "OrganizationClaimTypeExclusion";

        public const string Occupation = "Occupation";
        public const string OrganizationPolicyServiceSetting = "OrganizationPolicyServiceSetting";
        public const string PolicyServiceSetting = "PolicyServiceSetting";

        public const string Language = "Language";
        public const string ChannelPermission = "ChannelPermission";
        public const string PartyCorrespondencePreference = "PartyCorrespondencePreference";
        public const string PaymentDetails = "PaymentDetails";
        public const string InsurersBranchType = "InsurersBranchType";
        public const string InsurersBranches = "InsurersBranches";
        public const string QuoteUploadLog = "QuoteUploadLog";


        public const string PolicyAccident = "PolicyAccident";
        public const string PolicyAllRisk = "PolicyAllRisk";
        public const string PolicyBuilding = "PolicyBuilding";
        public const string PolicyContent = "PolicyContent";
        public const string PolicyFinance = "PolicyFinance";
        public const string PolicyLiability = "PolicyLiability";
        public const string PolicyCoverage = "PolicyCoverage";
        public const string PolicyPersonalVehicle = "PolicyPersonalVehicle";
        public const string PolicyWatercraft = "PolicyWatercraft";
        public const string LeadQuality = "LeadQuality";
        public const string PerformanceCounter = "PerformanceCounter";

        public const string PolicyHeaderClaimableItem = "PolicyHeaderClaimableItem";
        public const string CimsConnectIntegrationLog = "CimsConnectIntegrationLog";

        public const string SalesForceIntegrationLog = "SalesForceIntegrationLog";
        public const string Audit = "Audit";

        public const string Report = "Report";
        public const string ReportLayout = "ReportLayout";
        public const string ReportChannel = "ReportChannel";
        public const string ReportChannelLayout = "ReportChannelLayout";

        public const string EscalationPlan = "EscalationPlan";
        public const string EscalationPlanStep = "EscalationPlanStep";
        public const string EscalationPlanStepWorkflowMessage = "EscalationPlanStepWorkflowMessage";
        public const string EscalationPlanExecutionHistory = "EscalationPlanExecutionHistory";
        public const string AdditionalMembers = "AdditionalMembers";
    }
}
