﻿namespace Database.Schema
{
    // ReSharper disable InconsistentNaming
    public static class Constraints
    {
        #region ForeignKey
        public const string FK_CoverDefinition_CoverDefinitionType = "FK_CoverDefinition_CoverDefinitionType";
        public const string FK_CoverDefinition_CoverDefinition = "FK_CoverDefinition_CoverDefinition";
        public const string FK_CoverDefinition_Cover = "FK_CoverDefinition_Cover";
        public const string FK_CoverDefinition_Product = "FK_CoverDefinition_Product";

        public const string FK_Question_QuestionType = "FK_Question_QuestionType";
        public const string FK_Question_QuestionGroup = "FK_Question_QuestionGroup";
        public const string FK_QuestionAnswer_Question = "FK_QuestionAnswer_Question";

        public const string FK_QuestionDefinition_QuestionDefinitionType = "FK_QuestionDefinition_QuestionDefinitionType";
        public const string FK_QuestionDefinition_Question = "FK_QuestionDefinition_Question";
        public const string FK_QuestionDefinition_QuestionDefinition = "FK_QuestionDefinition_QuestionDefinition";
        public const string FK_QuestionDefinition_CoverDefinition = "FK_QuestionDefinition_CoverDefinition";

        public const string FK_QuestionAnswerExclusion_Product = "FK_QuestionAnswerExclusion_Product";
        public const string FK_QuestionAnswerExclusion_CoverDefinition = "FK_QuestionAnswerExclusion_CoverDefinition";
        public const string FK_QuestionAnswerExclusion_QuestionDefinition = "FK_QuestionAnswerExclusion_QuestionDefinition";
        public const string FK_QuestionAnswerExclusion_QuestionAnswer = "FK_QuestionAnswerExclusion_QuestionAnswer";

        public const string FK_StateProvince_Country = "FK_StateProvince_Country";
        public const string FK_Address_StateProvince = "FK_Address_StateProvince";
        public const string FK_Address_AddressType = "FK_Address_AddressType";
        public const string FK_Party_Address = "FK_Party_Address";

        public const string FK_ProductFee_Product = "FK_ProductFee_Product";
        public const string FK_ProductFee_ProductFeeType = "FK_ProductFee_ProductFeeType";
        public const string FK_ProductFee_PaymentPlan = "FK_ProductFee_PaymentPlan";

        public const string FK_UserChannel_User = "FK_UserChannel_User";
        public const string FK_UserChannel_Channel = "FK_UserChannel_Channel";
        
        public const string FK_UserSecurityRole_User = "FK_UserSecurityRole_User";
        public const string FK_UserSecurityRole_SecurityRole = "FK_UserSecurityRole_SecurityRole";

        public const string FK_SecondLevelQuestionAnswer_SecondLevelQuestion = "FK_SecondLevelQuestionAnswer_SecondLevelQuestion";
        public const string FK_SecondLevelQuestion_SecondLevelQuestionGroup = "FK_SecondLevelQuestion_SecondLevelQuestionGroup";
        public const string FK_SecondLevelQuestionDefinition_SecondLevelQuestion = "FK_SecondLevelQuestionDefinition_SecondLevelQuestion";
        public const string FK_SecondLevelQuestionDefinition_SecondLevelQuestionDefinition = "FK_SecondLevelQuestionDefinition_SecondLevelQuestionDefinition";
        public const string FK_SecondLevelQuestionDefinition_CoverDefinition = "FK_SecondLevelQuestionDefinition_CoverDefinition";
        public const string FK_SecondLevelQuestionDefinition_QuestionDefinitionType = "FK_SecondLevelQuestionDefinition_QuestionDefinitionType";
        public const string FK_SecondLevelQuestion_QuestionType = "FK_SecondLevelQuestion_QuestionType";

        #endregion

        #region Unique
        public const string UC_Channel = "UC_Channel";
        #endregion

    }
    // ReSharper restore InconsistentNaming
}