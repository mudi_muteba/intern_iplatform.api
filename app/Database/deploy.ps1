$module = "ToolBox.Builds"
$dirX86 = Join-Path $env:windir System32\WindowsPowerShell\v1.0\Modules\$module
$dirX64 = Join-Path $env:windir SysNative\WindowsPowerShell\v1.0\Modules\$module

robocopy .\tools $dirX86 "$module.psm1"
robocopy .\tools $dirX64 "$module.psm1"

if ($global:lastExitCode -lt 8){ $global:lastExitCode = 0 }

Remove-Module ToolBox.Builds -ErrorAction SilentlyContinue
Import-Module ToolBox.Builds