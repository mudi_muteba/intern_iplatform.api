﻿----------------------------------------------------------------------
DECLARE @id INT
DECLARE @questionType INT = 3
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('No Claim Bonus', @questionType, 6) -- Insurance History

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('NCB0', '0', @id, 0),
		('NCB1', '1', @id, 1),
		('NCB2', '2', @id, 2),
		('NCB3', '3', @id, 3),
		('NCB4', '4', @id, 4),
		('NCB5', '5', @id, 5),
		('NCB6', '6', @id, 6),
		('NCB7', '7', @id, 7),
		('NCB8', '8', @id, 8),
		('NCB9', '9', @id, 9),
		('NCB10', '10', @id, 10)
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Claims Last 0 to 12 Months', @questionType, 6) -- Insurance History

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Claims Last 12 Months - 0', '0', @id, 0),
		('Claims Last 12 Months - 1', '1', @id, 1),
		('Claims Last 12 Months - 2', '2', @id, 2),
		('Claims Last 12 Months - 3', '3', @id, 3),
		('Claims Last 12 Months - 4', '4', @id, 4),
		('Claims Last 12 Months - 5', '5', @id, 5),
		('Claims Last 12 Months - 6', '6', @id, 6),
		('Claims Last 12 Months - 7', '7', @id, 7),
		('Claims Last 12 Months - 8', '8', @id, 8),
		('Claims Last 12 Months - 9', '9', @id, 9),
		('Claims Last 12 Months - 10', '10', @id, 10)
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Claims Last 12 to 24 Months', @questionType, 6) -- Insurance History

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Claims Last 24 Months - 0', '0', @id, 0),
		('Claims Last 24 Months - 1', '1', @id, 1),
		('Claims Last 24 Months - 2', '2', @id, 2),
		('Claims Last 24 Months - 3', '3', @id, 3),
		('Claims Last 24 Months - 4', '4', @id, 4),
		('Claims Last 24 Months - 5', '5', @id, 5),
		('Claims Last 24 Months - 6', '6', @id, 6),
		('Claims Last 24 Months - 7', '7', @id, 7),
		('Claims Last 24 Months - 8', '8', @id, 8),
		('Claims Last 24 Months - 9', '9', @id, 9),
		('Claims Last 24 Months - 10', '10', @id, 10)
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Claims Last 24 to 36 Months', @questionType, 6) -- Insurance History

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Claims Last 36 Months - 0', '0', @id, 0),
		('Claims Last 36 Months - 1', '1', @id, 1),
		('Claims Last 36 Months - 2', '2', @id, 2),
		('Claims Last 36 Months - 3', '3', @id, 3),
		('Claims Last 36 Months - 4', '4', @id, 4),
		('Claims Last 36 Months - 5', '5', @id, 5),
		('Claims Last 36 Months - 6', '6', @id, 6),
		('Claims Last 36 Months - 7', '7', @id, 7),
		('Claims Last 36 Months - 8', '8', @id, 8),
		('Claims Last 36 Months - 9', '9', @id, 9),
		('Claims Last 36 Months - 10', '10', @id, 10)
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Province', @questionType, 1) -- General Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Province - Eastern Cape', 'Eastern Cape', @id, 0),
		('Province - Free State', 'Free State', @id, 1),
		('Province - Gauteng', 'Gauteng', @id, 2),
		('Province - KwaZulu-Natal', 'KwaZulu-Natal', @id, 3),
		('Province - Limpopo', 'Limpopo', @id, 4),
		('Province - Mpumalanga', 'Mpumalanga', @id, 5),
		('Province - Northern Cape', 'Northern Cape', @id, 6),
		('Province - North West', 'North West', @id, 7),
		('Province - Western Cape', 'Western Cape', @id, 8)
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Class Of Use', @questionType, 2) -- Risk Info

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Class Of Use - Private (Including Work)', 'Private (Including Work)', @id, 0),
		('Class Of Use - Business', 'Business', @id, 1),
		('Class Of Use - Strictly Private', 'Strictly Private', @id, 2),
		('Class Of Use - Farming', 'Farming', @id, 3),
		('Class Of Use - Company (Multiple Drivers)', 'Company (Multiple Drivers)', @id, 4),
		('Class Of Use - Taxi', 'Taxi', @id, 5),
		('Class Of Use - Unknown', 'Unknown', @id, 6),
		('Class Of Use - Business - Goods Carrying', 'Business - Goods Carrying', @id, 7)
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Type Of Residence', @questionType, 2) -- Risk Info

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Type Of Residence - Detached House/Cottage', 'Detached House/Cottage', @id, 0),												--TPRESDDETHSE
		('Type Of Residence - Double Storey House', 'Double Storey House', @id, 1),														--TPRESDDSHOUS
		('Type Of Residence - Double Storey Townhouse', 'Double Storey Townhouse', @id, 2),												--TPRESDDSTOWN
		('Type Of Residence - Garden Cottage', 'Garden Cottage', @id, 3),																--TPRESDGARCOT
		('Type Of Residence - Semi Detached House/Cottage', 'Semi Detached House/Cottage', @id, 4),										--TPRESDSDTHSE
		('Type Of Residence - Town House/Cluster House (Limited Access)', 'Town House/Cluster House (Limited Access)', @id, 5),			--TPRESDTHLIMA
		('Type Of Residence - Town House/Cluster Home (No Limited Access)', 'Town House/Cluster Home (No Limited Access)', @id, 6),		--TPRESDTHNLAC
		('Type Of Residence - Apartment/Flat (Ground or 1st Floor)', 'Apartment/Flat (Ground or 1st Floor)', @id, 7),					--TPRESDFLATGF
		('Type Of Residence - Apartment/Flat (Above 1st Floor)', 'Apartment/Flat (Above 1st Floor)', @id, 8),							--TPRESDFLATA1
		('Type Of Residence - Park Home', 'Park Home', @id, 9)																			--TPRESDPRKHOM
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Residence Is', @questionType, 2) -- Risk Info

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Residence Is - Main Residence', 'Main Residence', @id, 0),	--RESDISMAINRS
		('Residence Is - Holiday Home', 'Holiday Home', @id, 1),		--RESDISHOLHOM
		('Residence Is - Other Residence', 'Other Residence', @id, 2)	--RESDISOTHERR

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId)				--RESUSE
VALUES	('Residence Use', @questionType, 2) -- Risk Info

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Residence Use - Standard', 'Standard', @id, 0),				--RESUSESTNDRD
		('Residence Use - Hotel', 'Hotel', @id, 1),						--RESUSEHOTELA
		('Residence Use - Lodging-House', 'Lodging-House', @id, 2),		--RESUSELODHSE
		('Residence Use - Old Age Home', 'Old Age Home', @id, 3),		--RESUSEOLDAGE
		('Residence Use - Commune', 'Commune', @id, 4)					--RESUSECOMMUN
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId)				--SITRES
VALUES	('Situation Of Residence', @questionType, 2) -- Risk Info

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Situation Of Residence - Residential Area, No Access Control', 'Residential Area, No Access Control', @id, 0),
		('Situation Of Residence - Enclosed Access Controlled Area', 'Enclosed Access Controlled Area', @id, 1),
		('Situation Of Residence - Security Village', 'Security Village', @id, 2),
		('Situation Of Residence - Retirement Village', 'Retirement Village', @id, 3),
		('Situation Of Residence - Smallholding/Plot', 'Smallholding/Plot', @id, 4),
		('Situation Of Residence - Park Home', 'Park Home', @id, 5),
		('Situation Of Residence - Caravan Park', 'Caravan Park', @id, 6),
		('Situation Of Residence - Farm', 'Farm', @id, 7),
		('Situation Of Residence - Industrial Area', 'Industrial Area', @id, 8)
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Type Of Cover Motor', @questionType, 2) -- Risk Info

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Type Of Cover Motor - Comprehensive', 'Comprehensive', @id, 0),
		('Type Of Cover Motor - Third Party, Fire and Theft', 'Third Party, Fire and Theft', @id, 1),
		('Type Of Cover Motor - Third Party Only', 'Third Party Only', @id, 2),
		('Type Of Cover Motor - Theft Excluded', 'Theft Excluded', @id, 3)
		--('Type Of Cover - Pay As You Drive', 'Pay As You Drive', @id, 4),
		--('Type Of Cover - Comprehensive ''No Frills'' Cover', 'Comprehensive ''No Frills'' Cover', @id, 5),
		--('Type Of Cover - Flexi Cover', 'Flexi Cover', @id, 6)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Roof Gradient', @questionType, 2) -- Risk Info

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Roof Gradient - Flat', 'Flat', @id, 0),
		('Roof Gradient - Gradient', 'Gradient', @id, 1)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Wall Construction', @questionType, 2) -- Risk Info

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Wall Construction - Standard', 'Standard', @id, 0),
		('Wall Construction - Non Standard', 'Non Standard', @id, 1)
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Roof Construction', @questionType, 2) -- Risk Info

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Roof Construction - Standard', 'Standard', @id, 0),
		('Roof Construction - Non Standard', 'Non Standard', @id, 1),
		('Roof Construction - Thatch with Lightning conductor', 'Thatch with Lightning conductor', @id, 2),
		('Roof Construction - Thatch without Lightning conductor', 'Thatch without Lightning conductor', @id, 3)
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Unoccupied', @questionType, 2) -- Risk Info

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Unoccupied - During Work Hours', 'During Work Hours', @id, 0),
		('Unoccupied - No', 'No', @id, 1),
		('Unoccupied - More Than 7 Days In First Month', 'More Than 7 Days In First Month', @id, 2),
		('Unoccupied - More Than 30 Consecutive Days', 'More Than 30 Consecutive Days', @id, 3),
		('Unoccupied - More Than 60 Days Per Year', 'More Than 60 Days Per Year', @id, 4),
		('Unoccupied - 90 Days In Excess Of Limit', '90 Days In Excess Of Limit', @id, 5),
		('Unoccupied - 120 Days In Excess Of Limit', '120 Days In Excess Of Limit', @id, 6),
		('Unoccupied - 150 Days In Excess Of Limit', '150 Days In Excess Of Limit', @id, 7),
		('Unoccupied - 180 Days In Excess Of Limit', '180 Days In Excess Of Limit', @id, 8),
		('Unoccupied - 210 Days In Excess Of Limit', '210 Days In Excess Of Limit', @id, 9),
		('Unoccupied - 240 Days In Excess Of Limit', '240 Days In Excess Of Limit', @id, 10),
		('Unoccupied - 270 Days In Excess Of Limit', '270 Days In Excess Of Limit', @id, 11),
		('Unoccupied - 300 Days In Excess Of Limit', '300 Days In Excess Of Limit', @id, 12)
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('PAYD Cover', @questionType, 1) -- General Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('PAYD Cover - Drive 500', 'Drive 500', @id, 0),
		('PAYD Cover - Drive 750', 'Drive 750', @id, 1),
		('PAYD Cover - Drive 1000', 'Drive 1000', @id, 2),
		('PAYD Cover - Drive 1250', 'Drive 1250', @id, 3),
		('PAYD Cover - Drive 1500', 'Drive 1500', @id, 4),
		('PAYD Cover - Drive Max', 'Drive Max', @id, 5)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId)											--HOMIND
VALUES	('Home Industry', @questionType, 1) -- General Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Home Industry - None', 'None', @id, 0),													--HOMINDNONEAA
		('Home Industry - Baking', 'Baking', @id, 1),												--HOMINDBAKING
		('Home Industry - Beauty Care Products', 'Beauty Care Products', @id, 2),					--HOMINDCAREPR
		('Home Industry - Beauty Salon/Hairdresser', 'Beauty Salon/Hairdresser', @id, 3),			--HOMINDHAIRSL
		('Home Industry - Cleaning Products', 'Cleaning Products', @id, 4),							--HOMINDCLEANA
		('Home Industry - Clothing (Not Second Hand)', 'Clothing (Not Second Hand)', @id, 5),		--HOMINDCLOTHN
		('Home Industry - Educational Toys', 'Educational Toys', @id, 6),							--HOMINDEDTOYS
		('Home Industry - Framing', 'Framing', @id, 7),												--HOMINDFRAMEA
		('Home Industry - Healthcare', 'Healthcare', @id, 8),										--HOMINDHEALTH
		('Home Industry - Homeware (Not Second Hand)', 'Homeware (Not Second Hand)', @id, 9),		--HOMINDHOMEWA
		('Home Industry - Jewellery', 'Jewellery', @id, 10),										--HOMINDJEWELL
		('Home Industry - Needlework', 'Needlework', @id, 11),										--HOMINDNEEDLE
		('Home Industry - Pottery', 'Pottery', @id, 12),											--HOMINDPOTTER
		('Home Industry - Tupperware', 'Tupperware', @id, 13)										--HOMINDTUPPER

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Relationship To Insured', @questionType, 5) -- General Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Relationship To Insured - Insured', 'Insured', @id, 0),
		('Relationship To Insured - Spouse', 'Spouse', @id, 1),
		('Relationship To Insured - Child', 'Child', @id, 2),
		('Relationship To Insured - Residing Family Member', 'Residing Family Member', @id, 3),
		('Relationship To Insured - Domestic Worker', 'Domestic Worker', @id, 4),
		('Relationship To Insured - Other', 'Other', @id, 5)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Tracker Deal', @questionType, 4) -- Additional Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Tracker Deal - Option Not Selected', 'Drive 500', @id, 0),
		('Tracker Deal - Tracker Retrieve', 'Drive 750', @id, 1),
		('Tracker Deal - NetStar', 'Drive 1000', @id, 2)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('NN Excess', @questionType, 4) -- Additional Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('NN Excess - 2500', '2500', @id, 0),
		('NN Excess - 5000', '5000', @id, 1),
		('NN Excess - 7500', '7500', @id, 2),
		('NN Excess - 10000', '10000', @id, 3),
		('NN Excess - 20000', '20000', @id, 4)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Drivers For This Vehicle', @questionType, 4) -- Additional Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Drivers For This Vehicle - Restricted To Insured Only', 'Restricted To Insured Only', @id, 0),
		('Drivers For This Vehicle - Restricted To Insured and Spouse', 'Restricted To Insured and Spouse', @id, 1),
		('Drivers For This Vehicle - Restricted To Other Named Drivers', 'Restricted To Other Named Drivers', @id, 2),
		('Drivers For This Vehicle - No Restricted Drivers', 'No Restricted Drivers', @id, 3)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Selected Excess', @questionType, 4) -- Additional Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Selected Excess - 0', 'R0', @id, 0),
		('Selected Excess - 2000', 'R2 000', @id, 1),
		('Selected Excess - 3000', 'R3 000', @id, 2),
		('Selected Excess - 4000', 'R4 000', @id, 3),
		('Selected Excess - 5000', 'R5 000', @id, 4),
		('Selected Excess - 6000', 'R6 000', @id, 5),
		('Selected Excess - 7000', 'R7 000', @id, 6)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Valuation Method', @questionType, 4) -- Additional Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Valuation Method - Market', 'Market', @id, 0),
		('Valuation Method - Retail', 'Retail', @id, 1),
		('Valuation Method - Trade', 'Trade', @id, 2)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Registered Owner', @questionType, 2) -- Risk Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Registered Owner - Policy Holder', 'Policy Holder', @id, 0),
		('Registered Owner - Spouse', 'Spouse', @id, 1),
		('Registered Owner - Financially Dependent Child', 'Financially Dependent Child', @id, 2),
		('Registered Owner - Other', 'Other', @id, 3)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Credit Shortfall', @questionType, 2) -- Risk Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Credit Shortfall - 10% of Sum Insured', '10% of Sum Insured', @id, 0),
		('Credit Shortfall - 20% of Sum Insured', '20% of Sum Insured', @id, 1)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('NN Type Of Cover', @questionType, 2) -- Risk Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('NN Type Of Cover - Comprehensive', 'Comprehensive', @id, 0),
		('NN Type Of Cover - Fire and Theft Only', 'Fire and Theft Only', @id, 1),
		('NN Type Of Cover - To be specified', 'To be specified', @id, 2)

----------------------------------------------------------------------
------------------ VEHICLE TYPE - ONLY THESE FOR NOW -----------------
----------------------------------------------------------------------
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Type', @questionType, 2) -- Risk Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Type - Sedan', 'Sedan', @id, 0),
		('Vehicle Type - Motorcycle', 'Motorcycle', @id, 1)

-- NO SUCH QUESTION IN WEBGATE
----------------------------------------------------------------------
------------------ VEHICLE CODE --------------------------------------
----------------------------------------------------------------------
--INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
--VALUES	('Vehicle Code', @questionType, 2) -- Risk Information

--SET @id = SCOPE_IDENTITY()


--INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex) 
--VALUES	('Vehicle Code - New', 'New', @id, 0),
--		('Vehicle Code - 2nd Hand', '2nd Hand', @id, 1),
--		('Vehicle Code - Permanently Unifit for Use', 'Permanently Unifit for Use', @id, 2),
--		('Vehicle Code - Permanently Demolished', 'Permanently Demolished', @id, 3)

----------------------------------------------------------------------
---------------------- TO BE LINKED TO GENDER ------------------------
----------------------------------------------------------------------
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Main Driver Gender', @questionType, 5) -- Driver Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Main Driver Gender - Male', 'Male', @id, 0),
		('Main Driver Gender - Female', 'Female', @id, 1),
		('Main Driver Gender - Unknown', 'Unknown', @id, 2)

----------------------------------------------------------------------
------------------ Previously Insured DropDown -----------------------
----------------------------------------------------------------------
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Previously Insured Combo', @questionType, 2) -- Risk Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Previously Insured - Yes', 'Yes', @id, 0),
		('Previously Insured - No', 'No', @id, 1)

----------------------------------------------------------------------
------------------ TO BE LINKED TO MARITAL STATUS --------------------
----------------------------------------------------------------------
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Main Driver Marital Status', @questionType, 5) -- Driver Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Main Driver Marital Status - Married', 'Married', @id, 0),
		('Main Driver Marital Status - Single', 'Single', @id, 1),
		('Main Driver Marital Status - Divorced', 'Divorced', @id, 2),
		('Main Driver Marital Status - Separated', 'Separated', @id, 3),
		('Main Driver Marital Status - Domestic Partner', 'Domestic Partner', @id, 4),
		('Main Driver Marital Status - Civil Union', 'Civil Union', @id, 5),
		('Main Driver Marital Status - Unincorporated Association', 'Unincorporated Association', @id, 6),
		('Main Driver Marital Status - Widowed', 'Widowed', @id, 7),
		('Main Driver Marital Status - Other', 'Other', @id, 8),
		('Main Driver Marital Status - Unknown', 'Unknown', @id, 9)

----------------------------------------------------------------------
------------------ TO BE LINKED TO LICENCE TYPE ----------------------
----------------------------------------------------------------------
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Drivers Licence Type', @questionType, 5) -- Driver Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Drivers Licence Type - A', 'A', @id, 0),
		('Drivers Licence Type - A1', 'A1', @id, 1),
		('Drivers Licence Type - B', 'B', @id, 2),
		('Drivers Licence Type - C', 'C', @id, 3),
		('Drivers Licence Type - C1', 'C1', @id, 4),
		('Drivers Licence Type - EB', 'EB', @id, 5),
		('Drivers Licence Type - EC', 'EC', @id, 6),
		('Drivers Licence Type - EC1', 'EC1', @id, 7),
		('Drivers Licence Type - Learner''s Licence', 'Learner''s Licence', @id, 8),
		('Drivers Licence Type - International Drivers Permit', 'International Drivers Permit', @id, 9),
		('Drivers Licence Type - Neighbouring State', 'Neighbouring State', @id, 10),
		('Drivers Licence Type - Common Wealth', 'Common Wealth', @id, 11),
		('Drivers Licence Type - None', 'None', @id, 12)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Voluntary Excess', @questionType, 4) -- Additional Covers

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Voluntary Excess - 0', '0', @id, 0),
		('Voluntary Excess - 250', '250', @id, 1),
		('Voluntary Excess - 500', '500', @id, 2),
		('Voluntary Excess - 1000', '1000', @id, 3),
		('Voluntary Excess - 1750', '1750', @id, 4),
		('Voluntary Excess - 2000', '2000', @id, 5),
		('Voluntary Excess - 2500', '2500', @id, 6),
		('Voluntary Excess - 3000', '3000', @id, 7),
		('Voluntary Excess - 4000', '4000', @id, 8),
		('Voluntary Excess - 5000', '5000', @id, 9),
		('Voluntary Excess - 6000', '6000', @id, 10),
		('Voluntary Excess - 7000', '7000', @id, 11),
		('Voluntary Excess - 7500', '7500', @id, 12),
		('Voluntary Excess - 8000', '8000', @id, 13),
		('Voluntary Excess - 9000', '9000', @id, 14),
		('Voluntary Excess - 10000', '10000', @id, 15),
		('Voluntary Excess - 15000', '15000', @id, 16),
		('Voluntary Excess - 20000', '20000', @id, 17),
		('Voluntary Excess - 25000', '25000', @id, 18),
		('Voluntary Excess - 30000', '30000', @id, 19)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Compulsory Excess', @questionType, 4) -- Additional Covers

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Compulsory Excess - 0', 'None', @id, 0),
		('Compulsory Excess - 1000', '1000', @id, 1),
		('Compulsory Excess - 2000', '2000', @id, 2),
		('Compulsory Excess - 2500', '2500', @id, 3),
		('Compulsory Excess - 3000', '3000', @id, 4),
		('Compulsory Excess - 4000', '4000', @id, 5),
		('Compulsory Excess - 5000', '5000', @id, 6)
		
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Accidental Damage', @questionType, 4) -- Additional Covers

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Accidental Damage - 0', 'None', @id, 0),
		('Accidental Damage - 5000', '5000', @id, 1),
		('Accidental Damage - 10000', '10000', @id, 2),
		('Accidental Damage - 15000', '15000', @id, 3),
		('Accidental Damage - 20000', '20000', @id, 4),
		('Accidental Damage - 25000', '25000', @id, 5),
		('Accidental Damage - 30000', '30000', @id, 6),
		('Accidental Damage - 40000', '40000', @id, 7),
		('Accidental Damage - 50000', '50000', @id, 8)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Car Hire', @questionType, 4) -- Additional Covers

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Car Hire - None', 'None', @id, 0),
		('Car Hire - 10 Days', '10 Days', @id, 1),
		('Car Hire - 20 Days', '20 Days', @id, 2),
		('Car Hire - 30 Days', '30 Days', @id, 3)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('NN Roof', @questionType, 4) -- Additional Covers

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('NN Roof - Roof Tiles', 'Roof Tiles', @id, 0),
		('NN Roof - Thatch', 'Thatch', @id, 1),
		('NN Roof - Corregated Iron', 'Corregated Iron', @id, 2),
		('NN Roof - Slate Tiles', 'Slate Tiles', @id, 3),
		('NN Roof - Cast Concrete', 'Cast Concrete', @id, 4),
		('NN Roof - Asbestos', 'Asbestos', @id, 5)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('NN Wall', @questionType, 4) -- Additional Covers

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('NN Wall - Brick', 'Brick', @id, 0),
		('NN Wall - Timber', 'Timber', @id, 1)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Immobiliser', @questionType, 3) -- Security Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Immobiliser - None', 'None', @id, 0),
		('Vehicle Immobiliser - Factory Fitted', 'Factory Fitted', @id, 1),
		('Vehicle Immobiliser - VESA4 Secure Device', 'VESA4 Secure Device', @id, 2),
		('Vehicle Immobiliser - VESA3 Secure Device', 'VESA3 Secure Device', @id, 3),
		('Vehicle Immobiliser - VESA4 with Anti Hi-Jack', 'VESA4 with Anti Hi-Jack', @id, 4)

---------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('NN Days Unattended', @questionType, 3) -- Security Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('NN Days Unattended - Less than 60 days', 'Less than 60 days', @id, 0),
		('NN Days Unattended - Less than 120 days', 'Less than 120 days', @id, 1),
		('NN Days Unattended - Less than 180 days', 'Less than 180 days', @id, 2),
		('NN Days Unattended - Less than 320 days', 'Less than 320 days', @id, 3),
		('NN Days Unattended - Less than 360 days', 'Less than 360 days', @id, 4),
		('NN Days Unattended - More than 360 days', 'More than 360 days', @id, 5)

---------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Alarm Type', @questionType, 3) -- Security Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Alarm Type - None', 'None', @id, 0),
		('Alarm Type - Linked Alarm', 'Linked Alarm', @id, 1),
		('Alarm Type - Non Linked Alarm', 'Non Linked Alarm', @id, 2)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('NN Alarm Type', @questionType, 3) -- Security Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('NN Alarm Type - None', 'None', @id, 0),
		('NN Alarm Type - Linked Alarm', 'Linked Alarm', @id, 1),
		('NN Alarm Type - Non Linked Alarm', 'Non Linked Alarm', @id, 2)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Garaging', @questionType, 3) -- Security Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Garaging - In Locked Garage', 'In Locked Garage', @id, 0),
		('Vehicle Garaging - Behind Locked Gates', 'Behind Locked Gates', @id, 1),
		('Vehicle Garaging - Security Complex', 'Security Complex', @id, 2),
		('Vehicle Garaging - Carport', 'Carport', @id, 3),
		('Vehicle Garaging - Driveway', 'Driveway', @id, 4),
		('Vehicle Garaging - Off Street', 'Off Street', @id, 5),
		('Vehicle Garaging - On Street', 'On Street', @id, 6),
		('Vehicle Garaging - Parking Lot', 'Parking Lot', @id, 7),
		('Vehicle Garaging - Off Street At School', 'Off Street At School', @id, 8),
		('Vehicle Garaging - On Street At School', 'On Street At School', @id, 9),
		('Vehicle Garaging - Garaged At School', 'Garaged At School', @id, 10),
		('Vehicle Garaging - None', 'None', @id, 11)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Overnight Parking', @questionType, 3) -- Security Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Overnight Parking - In Locked Garage', 'In Locked Garage', @id, 0),
		('Vehicle Overnight Parking - Behind Locked Gates', 'Behind Locked Gates', @id, 1),
		('Vehicle Overnight Parking - Security Complex', 'Security Complex', @id, 2),
		('Vehicle Overnight Parking - Carport', 'Carport', @id, 3),
		('Vehicle Overnight Parking - Driveway', 'Driveway', @id, 4),
		('Vehicle Overnight Parking - Off Street', 'Off Street', @id, 5),
		('Vehicle Overnight Parking - On Street', 'On Street', @id, 6),
		('Vehicle Overnight Parking - Parking Lot', 'Parking Lot', @id, 7),
		('Vehicle Overnight Parking - Off Street At School', 'Off Street At School', @id, 8),
		('Vehicle Overnight Parking - On Street At School', 'On Street At School', @id, 9),
		('Vehicle Overnight Parking - Garaged At School', 'Garaged At School', @id, 10),
		('Vehicle Overnight Parking - None', 'None', @id, 11)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Daytime Parking', @questionType, 3) -- Security Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	
		('Vehicle Daytime Parking - Parking Lot', 'Parking Lot', @id, 0),
		('Vehicle Daytime Parking - Security Complex', 'Security Complex', @id, 1),
		('Vehicle Daytime Parking - Office Park', 'Office Park', @id, 2), -- Just for display - will map to Security Complex
		('Vehicle Daytime Parking - Behind Locked Gates', 'Behind Locked Gates', @id, 3),
		('Vehicle Daytime Parking - In Locked Garage', 'In Locked Garage', @id, 4),
		('Vehicle Daytime Parking - Carport', 'Carport', @id, 5),
		('Vehicle Daytime Parking - Driveway', 'Driveway', @id, 6),
		('Vehicle Daytime Parking - Off Street', 'Off Street', @id, 7),
		('Vehicle Daytime Parking - On Street', 'On Street', @id, 8),
		('Vehicle Daytime Parking - Off Street At School', 'Off Street At School', @id, 9),
		('Vehicle Daytime Parking - On Street At School', 'On Street At School', @id, 10),
		('Vehicle Daytime Parking - Garaged At School', 'Garaged At School', @id, 11),
		('Vehicle Daytime Parking - None', 'None', @id, 12)

----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Tracking Device', @questionType, 3) -- Security Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Tracking Device - None', 'None', @id, 0),
	  --('Vehicle Tracking Device - Bandit', 'Bandit', @id, 1),
		('Vehicle Tracking Device - Bandit Interac', 'Bandit Interac', @id, 2),
		('Vehicle Tracking Device - Bandit Interac Active', 'Bandit Interac Active', @id, 3),
		('Vehicle Tracking Device - Bandit Interac Hi-Alert', 'Bandit Interac Hi-Alert', @id, 4),
	  --('Vehicle Tracking Device - Bandit Interlock', 'Bandit Interlock', @id, 5),
	  --('Vehicle Tracking Device - Bandit PIN Lock', 'Bandit PIN Lock', @id, 6),
		('Vehicle Tracking Device - Car Track / CT1', 'Car Track / CT1', @id, 7),
	  --('Vehicle Tracking Device - Car Track (executive plus)', 'Car Track (executive plus)', @id, 8),
		('Vehicle Tracking Device - Car Track Quick CT1', 'Car Track Quick CT1', @id, 9),
		('Vehicle Tracking Device - Car Track Quick Plus', 'Car Track Quick Plus', @id, 10),
		('Vehicle Tracking Device - Cell Secure Eagle Find it V5', 'Cell Secure Eagle Find it V5', @id, 11),
		('Vehicle Tracking Device - Cell Secure Eagle Tracee V4', 'Cell Secure Eagle Tracee V4', @id, 12),
		('Vehicle Tracking Device - Cell Stop Mk5', 'Cell Stop Mk5', @id, 13),
	  --('Vehicle Tracking Device - DataTrak', 'DataTrak', @id, 14),
		('Vehicle Tracking Device - DataTrak MK2D', 'DataTrak MK2D', @id, 15),
		('Vehicle Tracking Device - DataTrak MK4', 'DataTrak MK4', @id, 16),
	  --('Vehicle Tracking Device - DataTrak (only Gauteng)', 'DataTrak (only Gauteng)', @id, 17),
		('Vehicle Tracking Device - DigiCore C-Track', 'DigiCore C-Track', @id, 18),
	  --('Vehicle Tracking Device - DigiCore / C-Track Secure Plus', 'DigiCore / C-Track Secure Plus', @id, 19),
	  --('Vehicle Tracking Device - DigiCore Insure Protect', 'DigiCore Insure Protect', @id, 20),
	  --('Vehicle Tracking Device - DigiCore Secure Plus Protect', 'DigiCore Secure Plus Protect', @id, 21),
		('Vehicle Tracking Device - Duo Solutions Datalogger', 'Duo Solutions Datalogger', @id, 22),
	  --('Vehicle Tracking Device - Matrix', 'Matrix', @id, 23),
		('Vehicle Tracking Device - Matrix Escosec', 'Matrix Escosec', @id, 24),
		('Vehicle Tracking Device - Matrix MX1', 'Matrix MX1', @id, 25),
		('Vehicle Tracking Device - Matrix MX2', 'Matrix MX2', @id, 26),
		('Vehicle Tracking Device - Matrix MX3', 'Matrix MX3', @id, 27),
		('Vehicle Tracking Device - Mobile Tracker Frankie', 'Mobile Tracker Frankie', @id, 28),
		('Vehicle Tracking Device - Mobile Tracker Frankie Plus', 'Mobile Tracker Frankie Plus', @id, 29),
		('Vehicle Tracking Device - MTrack', 'MTrack', @id, 30),
	  --('Vehicle Tracking Device - NetStar', 'NetStar', @id, 31),
		('Vehicle Tracking Device - NetStar Cyber Sleuth', 'NetStar Cyber Sleuth', @id, 32),
	  --('Vehicle Tracking Device - NetStar VBU 150/220', 'NetStar VBU 150/220', @id, 33),
	  --('Vehicle Tracking Device - NetStar VBU 170/210', 'NetStar VBU 170/210', @id, 34),
		('Vehicle Tracking Device - NetStar VBU 450 Early Warning', 'NetStar VBU 450 Early Warning', @id, 35),
		('Vehicle Tracking Device - NetStar VBU 470 Phone In', 'NetStar VBU 470 Phone In', @id, 36),
		('Vehicle Tracking Device - NetStar VBU 480 Sleuth', 'NetStar VBU 480 Sleuth', @id, 37),
	  --('Vehicle Tracking Device - NetStar Vigil', 'NetStar Vigil', @id, 38),
		('Vehicle Tracking Device - NetStar Vigil Supreme', 'NetStar Vigil Supreme', @id, 39),
		('Vehicle Tracking Device - Orbtech Buddi 3', 'Orbtech Buddi 3', @id, 40),
		('Vehicle Tracking Device - Orbtech Buddi 5', 'Orbtech Buddi 5', @id, 41),
		('Vehicle Tracking Device - Orbtech Buddi Track', 'Orbtech Buddi Track', @id, 42),
		('Vehicle Tracking Device - Orchid DX120', 'Orchid DX120', @id, 43),
		('Vehicle Tracking Device - Orchid DX170', 'Orchid DX170', @id, 44),
		('Vehicle Tracking Device - Orchid DX200', 'Orchid DX200', @id, 45),
	  --('Vehicle Tracking Device - Orchid DX250', 'Orchid DX250', @id, 46),
		('Vehicle Tracking Device - Other', 'Other', @id, 47),
		('Vehicle Tracking Device - SkyTrax', 'SkyTrax', @id, 48),
		('Vehicle Tracking Device - SkyTrax F4', 'SkyTrax F4', @id, 49),
	  --('Vehicle Tracking Device - SmartTrak Active Plus', 'SmartTrak Active Plus', @id, 50),
		('Vehicle Tracking Device - SmartTrak Classic', 'SmartTrak Classic', @id, 51),
		('Vehicle Tracking Device - SmartTrak Elite', 'SmartTrak Elite', @id, 52),
		('Vehicle Tracking Device - SmartTrak Lifestyle', 'SmartTrak Lifestyle', @id, 53),
	  --('Vehicle Tracking Device - SmartTrak Lifestyle Active', 'SmartTrak Lifestyle Active', @id, 54),
	  --('Vehicle Tracking Device - SmartTrak Lifestyle', 'SmartTrak Lifestyle', @id, 55),
		('Vehicle Tracking Device - Tracetec', 'Tracetec', @id, 56),
		('Vehicle Tracking Device - Tracker Alert', 'Tracker Alert', @id, 57),
		('Vehicle Tracking Device - Tracker Locate Alert', 'Tracker Locate Alert', @id, 58),
		('Vehicle Tracking Device - Tracker Locate Retrieve', 'Tracker Locate Retrieve', @id, 59),
	  --('Vehicle Tracking Device - Tracker Motorola STRK 9700 SA', 'Tracker Motorola STRK 9700 SA', @id, 60),
		('Vehicle Tracking Device - Tracker Retrieve', 'Tracker Retrieve', @id, 61)

----------------------------------------------------------------------

--INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
--VALUES	('Vehicle Color', @questionType, 2) -- Risk Information

--SET @id = SCOPE_IDENTITY()

--INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
--VALUES	('Vehicle Color - Beige', 'Beige', @id, 0),
--		('Vehicle Color - Black', 'Black', @id, 1),
--		('Vehicle Color - Blue', 'Blue', @id, 2),
--		('Vehicle Color - Brown', 'Brown', @id, 3),
--		('Vehicle Color - Bronze', 'Bronze', @id, 4),
--		('Vehicle Color - Burgundy', 'Burgundy', @id, 5),
--		('Vehicle Color - Cerise', 'Cerise', @id, 6),
--		('Vehicle Color - Champagne', 'Champagne', @id, 7),
--		('Vehicle Color - Charcoal', 'Charcoal', @id, 8),
--		('Vehicle Color - Copper', 'Copper', @id, 9),
--		('Vehicle Color - Cream', 'Cream', @id, 10),
--		('Vehicle Color - Gold', 'Gold', @id, 11),
--		('Vehicle Color - Green', 'Green', @id, 12),
--		('Vehicle Color - Grey', 'Grey', @id, 13),
--		('Vehicle Color - Ivory', 'Ivory', @id, 14),
--		('Vehicle Color - Jade', 'Jade', @id, 15),
--		('Vehicle Color - Maroon', 'Maroon', @id, 16),
--		('Vehicle Color - Mustard', 'Mustard', @id, 17),
--		('Vehicle Color - Orange', 'Orange', @id, 18),
--		('Vehicle Color - Pink', 'Pink', @id, 19),
--		('Vehicle Color - Purple', 'Purple', @id, 20),
--		('Vehicle Color - Red', 'Red', @id, 21),
--		('Vehicle Color - Silver', 'Silver', @id, 22),
--		('Vehicle Color - Tan', 'Tan', @id, 23),
--		('Vehicle Color - White', 'White', @id, 24),
--		('Vehicle Color - Yellow', 'Yellow', @id, 25),
--		('Vehicle Color - Other', 'Other', @id, 26)

----------------------------------------------------------------------
--('Vehicle Engine Type', 3, 2),
--('Vehicle Transmission Type', 3, 2),
--('Vehicle Valuation Method', 3, 2),
--('Vehicle Car Hire', 3, 4) -- Additional Options