
if not exists(select * from sys.columns where Name = N'CreatedDate' and Object_ID = Object_ID(N'ProposalHeader'))
begin
	alter table [dbo].[ProposalHeader]
	ADD [CreatedDate] [datetime] NULL
end

if not exists(select * from sys.columns where Name = N'ModifiedDate' and Object_ID = Object_ID(N'ProposalHeader'))
begin
	alter table [dbo].[ProposalHeader]
	ADD	[ModifiedDate] [datetime] NULL
end

if not exists(select * from sys.columns where Name = N'RatingDate' and Object_ID = Object_ID(N'ProposalHeader'))
begin
	alter table [dbo].[ProposalHeader]
	ADD	[RatingDate] [datetime] NULL
end

