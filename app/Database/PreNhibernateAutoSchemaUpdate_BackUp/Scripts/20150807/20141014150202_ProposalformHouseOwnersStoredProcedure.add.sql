﻿IF NOT EXISTS (SELECT * FROM sys.procedures WHERE Name = 'ursp_proposalform_HouseOwners')

EXEC(
'CREATE PROCEDURE [dbo].[ursp_proposalform_HouseOwners]
@ProductId INT

AS
BEGIN



  CREATE TABLE #TEMP
  (Id INT, 
   Value VARCHAR(250));

   INSERT INTO #TEMP Values (90, ''2021'');
   INSERT INTO #TEMP Values (89, ''Bryanston'');
   INSERT INTO #TEMP Values (47, ''Gauteng'');



WITH HHC AS
(
SELECT 1 ItemNumber,
       UPPER(qg.Name) Name, 
       REPLACE(REPLACE(REPLACE(qd.Displayname,''Postal Code'',''Risk Address''), ''Suburb'', ''Risk Address''),''Province'', ''Risk Address'') Displayname,
	   Value,
       qg.VisibleIndex VisibleIndexqg,
	   qd.VisibleIndex VisibleIndexqd,
	   t.Id
FROM questiondefinition qd 
INNER JOIN md.question q ON q.id = qd.questionid 
INNER JOIN md.questiongroup qg ON qg.id = q.questiongroupid
INNER JOIN coverdefinition cd ON qd.coverdefinitionid = cd.id
LEFT JOIN #Temp t on qd.questionid = t.Id
WHERE cd.coverid = 44
  AND productid = @ProductId 

)


SELECT  ItemNumber, Name, Displayname,
                SUBSTRING((
				          SELECT '', ''+Value AS [text()]
						  FROM HHC
						  WHERE HHC.Displayname = HHC2.Displayname
						    AND HHC.ItemNumber = HHC2.ItemNumber
							order by Id
						  FOR XML PATH ('''')
						  ), 2, 1000) [Value],
						  VisibleIndexqg, MIN(VisibleIndexqd) VisibleIndexqd

FROM HHC HHC2 
GROUP BY ItemNumber, Name, Displayname, VisibleIndexqg
ORDER BY Itemnumber, VisibleIndexqg, MIN(VisibleIndexqd)

DROP TABLE #TEMP;

END')
