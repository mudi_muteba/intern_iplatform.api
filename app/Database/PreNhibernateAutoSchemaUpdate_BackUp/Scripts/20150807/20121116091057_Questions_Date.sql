﻿INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES		('Occupation Date', 2, 1), -- General Information
			('Drivers Licence First Issued Date', 2, 5), -- Driver Information
			('Main Driver Date of Birth', 2, 5)