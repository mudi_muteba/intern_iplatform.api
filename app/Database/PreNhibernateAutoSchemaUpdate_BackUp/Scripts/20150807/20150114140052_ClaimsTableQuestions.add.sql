﻿ALTER TABLE md.ClaimsQuestion ADD [VisibleIndex] [int] NOT NULL 
Go

if NOT exists (select * from dbo.sysobjects where id = object_id(N'ClaimsQuestionAnswer') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
	CREATE TABLE [md].[ClaimsQuestionAnswer](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Name] [nvarchar](200) NOT NULL,
		[Answer] [nvarchar](100) NOT NULL,
		[ClaimsQuestionId] [int] NOT NULL,
		[VisibleIndex] [int] NOT NULL,
	 CONSTRAINT [PK_ClaimsQuestionAnswer] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [md].[ClaimsQuestionAnswer]  WITH CHECK ADD  CONSTRAINT [FK_ClaimsQuestionAnswer_Question] FOREIGN KEY([ClaimsQuestionId])
	REFERENCES [md].[ClaimsQuestion] ([Id])

	ALTER TABLE [md].[ClaimsQuestionAnswer] CHECK CONSTRAINT [FK_ClaimsQuestionAnswer_Question]
End
Go

INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId, VisibleIndex) 
VALUES		('Car Accident:What Happened', 4, 1, 0),
			('Malicious Damage: What Happened', 4, 1, 1),
			('Write Off: What Happened', 4, 1, 2),
			('If Yes, Explanation', 4, 1, 3),
			('Caravan Damage: What Happened', 4, 1, 4),
			('Trailers Damage: What Happened', 4, 1, 5),
			('Insurer', 4, 2, 0),
			('Policy Number', 4, 2, 1),
			('CO REG Number', 4, 2, 2),
			('Insured Name', 4, 2, 3),
			('Insured Company Name', 4, 2, 4),
			('Insured Person''s Age', 10, 2, 5),
			('Insured Surname', 4, 2, 6),
			('Insured Initials', 4, 2, 7),
			('ID Number', 10, 2, 8),
			('VAT Number', 4, 2, 9),
			('Insured Ocupation/Business', 4, 2, 10),
			('Physical Address', 4, 2, 11),
			('Home Telephone Number', 10, 2, 12),
			('E-mail', 4, 2, 13),
			('Insured Fax', 10, 2, 14),
			('Relationship Of Person To The Insured', 4, 2, 15),
			('Injury/Illness When And Where Did The Accident Occur Or Illness Commence?', 4, 2, 16),
			('Scan Vehicle Licence', 6, 3, 0),
			('Vehicle Make', 4, 3, 1),
			('Vehicle Model', 4, 3, 2),
			('Vehicle Year', 4, 3, 3),
			('Vehicle Tare/MASS', 4, 3, 4),
			('Vehicle Registration Number', 4, 3, 5),
			('Vehicle Kilometers Completed', 10, 3, 6),
			('Vehicle Date Purchase', 2, 3, 7),
			('Vehicle Price Paid', 10, 3, 8),
			('If Vehicle Subject To HP/Lease: State Name', 4, 3, 9),
			('If Vehicle Subject To HP/Lease: Account Number', 10, 3, 10),
			('If Vehicle Subject To HP/Lease: Contact Person', 4, 3, 11),
			('If Vehicle Submect To HP/Lease: Contact Number', 10, 3, 12),
			('In Whose Name Is The Vehicle Registered?', 4, 3, 13),
			('Damage To Own Vehicle', 4, 3, 14),
			('Estimate For Repairs Or Attach Quotation', 9, 3, 15),
			('Repairer''s Name', 4, 3, 16),
			('Repairer''s Contact Number', 10, 3, 17),
			('Is The Vehicle Drivable Or Not', 1, 3, 18),
			('If Not, Please Give Advise Current Location Of Vehicle', 4, 3, 19),
			('Driver''s Full Name', 4, 4, 0),
			('Driver''s Address', 4, 4, 1),
			('Driver''s Occupation', 4, 4, 2),
			('Driver''s ID Number', 10, 4, 3),
			('Scan Driver Licence', 5, 4, 4),
			('Driver''s Licence Number', 4, 4, 5),
			('Driver''s Licence Date Of First Issue', 2, 4, 6),
			('Driver''s Licence Expiry Date (PrDP)', 2, 4, 7),
			('Driver''s Licence Code', 4, 4, 8),
			('Was He/She Driving With Your Permission', 1, 4, 11),
			('Was He/She In Your Employ', 1, 4, 12),
			('Is He/She The Owner Of Another Vehicle?', 1, 4, 13),
			('If Yes, Give Insured Name', 4, 4, 14),
			('If Yes, Give Insured Policy Number', 4, 4, 15),
			('Details Of Any Convictions For Motor Offences: Also Note Pending Cases', 4, 4, 16),
			('Has Licence Ever Been Endorsed', 1, 4, 17),
			('Has He/She Any Physical Defects', 1, 4, 18),
			('Passenger 1: Name', 4, 5, 0),
			('Passenger 1: Injury', 4, 5, 1),
			('Passenger 1: Address', 4, 5, 2),
			('Passenger 2: Name', 4, 5, 3),
			('Passenger 2: Injury', 4, 5, 4),
			('Passenger 2: Address', 4, 5, 5),
			('Passenger 3: Name', 4, 5, 6),
			('Passenger 3: Injury', 4, 5, 7),
			('Passenger 3: Address', 4, 5, 8),
			('Passenger:  Are They Employees?', 1, 5, 10),
			('Other Party 1: Vehicle Registration Number', 4, 6, 0),
			('Other Party 1: Vehicle Make', 4, 6, 1),
			('Other Party 1: Vehicle Owner Name', 4, 6, 2),
			('Other Party 1: Owner Address', 4, 6, 3),
			('Other Party 1: Insurance Details', 4, 6, 4),
			('Other Party 1: Damages', 4, 6, 5),
			('Other Party 2: Vehicle Registration Number', 4, 6, 6),
			('Other Party 2: Vehicle Make', 4, 6, 7),
			('Other Party 2: Vehicle Owner Name', 4, 6, 8),
			('Other Party 2: Owner Address', 4, 6, 9),
			('Other Party 2: Insurance Details', 4, 6, 10),
			('Other Party 2: Damages', 4, 6, 11),
			('Damage: Other Party Property Other Than Vehicles', 1, 6, 12),
			('Other Party: Name', 4, 6, 13),
			('Other Party: Address Of Owner', 4, 6, 14),
			('Other Party: Damage Details', 4, 6, 15),
			('Other Party: Personal Injuries (Other Than Insured Vehicle)', 4, 6, 16),
			('Other Party: Name Of Injuried', 4, 6, 17),
			('Other Party: Relationship To Accident EG Driver, Passenger', 4, 6, 18),
			('Other Party: Injury Details', 4, 6, 19),
			('Other Party: Name Of Hospital If Applicable', 4, 6, 20),
			('Witness 1: Name', 4, 7, 0),
			('Witness 1: Address', 4, 7, 1),
			('Witness 1: Phone Number', 10, 7, 2),
			('Witness 2: Name', 4, 7, 3),
			('Witness 2: Address', 4, 7, 4),
			('Witness 2: Phone Number', 10, 7, 5),
			('Accident/Theft Date', 2, 8, 0),
			('Accident/Theft Time', 4, 8, 1),
			('Accident/Theft Place', 4, 8, 2),
			('Speed Before Accident', 10, 8, 3),
			('Speed At Moment Of Impact', 10, 8, 4),
			('Where The Street Lights On?', 1, 8, 10),
			('Did You Brake?', 1, 8, 11),
			('Did The Vehicle Skid?', 1, 8, 12),
			('Were Any Warnings Given By You? (Hooting, Indicator etc)', 1, 8, 13),
			('Did The Police Attend The Scene', 1, 9, 0),
			('Police Details: Name Of Police /Traffice Offer Who Recorded Accident Details.', 4, 9, 1),
			('Police Station', 4, 9, 2),
			('Police Station Reference Number', 4, 9, 3),
			('Was The Driver/Person Tested For Drugs Or Alcohol?', 1, 9, 4),
			('Description Of Accident/Incident', 4, 10, 0),
			('Where Were You Coming From', 4, 10, 1),
			('Where Were You Going To', 4, 10, 2),
			('Sketch Of Accident: Please Show Clearly The Point Of Impact And Indicate The Direction Of Travel By Arrows', 7, 10, 3),
			('Give Details Of Any Road Safety Signs Or Warning Signs In The Vicinity Of Scene Of Accident', 4, 10, 4),
			('Declaration: We Hereby Declare The Forgoing Particulars To Be True In Every Respect', 1, 11, 0),
			('Declaration Signature Of Driver', 8, 11, 1),
			('Declaration Signature Of Insured', 8, 11, 2),
			('Declaration Date', 2, 11, 4),
			('I Have Inspected The Driver''s Licence And It Is Free Of Endorsements/Endorsed As Shown', 1, 12, 0),
			('Scan Vehicle Licence', 6, 12, 1),
			('Vehicle Identification No. (VIN)', 4, 12, 2),
			('Vehicle Chassis Number', 4, 12, 3),
			('Vehicle Engine Number', 4, 12, 4),
			('Declaration: I/We Hereby Declare The Forgoing Particulars To Be True In Every Respect', 8, 13, 0),
			('Driver''s License', 9, 14, 0),
			('ID Number', 9, 14, 1),
			('2x Repair Quotation', 9, 14, 2),
			('SAPS Reference Number', 9, 14, 3),
			('If Write-Off: Original Registration Paper', 9, 14, 4),
			('2x Change Of Ownership Paper', 9, 14, 5),
			('Spare Keys', 9, 14, 6),
			('Proof Of Extras On Vehicle', 9, 14, 7)
Go
----------------------------------------------------------------------
DECLARE @id INT
----------------------------------------------------------------------

INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId, VisibleIndex) 
VALUES		('Driver''s Licence', 3, 4, 9)

SET @id = SCOPE_IDENTITY()

INSERT INTO md.ClaimsQuestionAnswer (Name, Answer, ClaimsQuestionId, VisibleIndex)
VALUES	('Driver Licence - Comprehensive', 'Full', @id, 0),
		('Driver Licence - Limited Cover','Learners', @id, 1)

----------------------------------------------------------------------

INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId, VisibleIndex) 
VALUES		('For What Purpose Is The Vehicle Being Used', 3, 4, 10)

SET @id = SCOPE_IDENTITY()

INSERT INTO md.ClaimsQuestionAnswer (Name, Answer, ClaimsQuestionId, VisibleIndex)
VALUES	('For What Purpose Is The Vehicle Being Used - Private', 'Private', @id, 0),
		('For What Purpose Is The Vehicle Being Used - Business', 'Business', @id, 1)

----------------------------------------------------------------------

INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId, VisibleIndex) 
VALUES	('Passengers: For What Purpose Were They Carried', 3, 5, 9)

SET @id = SCOPE_IDENTITY()

INSERT INTO md.ClaimsQuestionAnswer (Name, Answer, ClaimsQuestionId, VisibleIndex)
VALUES	('Passengers: For What Purpose Were They Carried - Private', 'Private', @id, 0),
		('Passengers: For What Purpose Were They Carried - Business', 'Business', @id, 1)

----------------------------------------------------------------------

INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId, VisibleIndex) 
VALUES	('Accident Weather Conditions', 3, 8, 5)

SET @id = SCOPE_IDENTITY()

INSERT INTO md.ClaimsQuestionAnswer (Name, Answer, ClaimsQuestionId, VisibleIndex)
VALUES	('Accident Weather Conditions - Cloudy', 'Cloudy', @id, 0),
		('Accident Weather Conditions - Fog', 'Fog', @id, 1),
		('Accident Weather Conditions - Misty', 'Misty', @id, 2),
		('Accident Weather Conditions - Raining', 'Raining', @id, 3),
		('Accident Weather Conditions - Sunny', 'Sunny', @id, 4)

----------------------------------------------------------------------

INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId, VisibleIndex) 
VALUES	('Visability', 3, 8, 6)

SET @id = SCOPE_IDENTITY()

INSERT INTO md.ClaimsQuestionAnswer (Name, Answer, ClaimsQuestionId, VisibleIndex)
VALUES	('Visability - Good', 'Good', @id, 0),
		('Visability - Poor', 'Poor', @id, 1),
		('Visability - Very Poor', 'Very Poor', @id, 2)

----------------------------------------------------------------------

INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId, VisibleIndex) 
VALUES	('Road Surface', 3, 8, 7)

SET @id = SCOPE_IDENTITY()

INSERT INTO md.ClaimsQuestionAnswer (Name, Answer, ClaimsQuestionId, VisibleIndex)
VALUES	('Road Surface - Wet Tar', 'Wet Tar', @id, 0),
		('Road Surface - Dry Tar', 'Dry Tar', @id, 1),
		('Road Surface - Wet Gravel', 'Wet Gravel', @id, 2),
		('Road Surface - Dry Gravel', 'Dry Gravel', @id, 3)

----------------------------------------------------------------------		

INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId, VisibleIndex) 
VALUES	('Width Of Road', 3, 8, 8)

SET @id = SCOPE_IDENTITY()

INSERT INTO md.ClaimsQuestionAnswer (Name, Answer, ClaimsQuestionId, VisibleIndex)
VALUES	('Width Of Road - Single Lane', 'Single Lane', @id, 0),
		('Width Of Road - Double Lane', 'Double Lane', @id, 1),
		('Width Of Road - Single Carriage Way', 'Single Carriage Way', @id, 2),
		('Width Of Road - Double Carriage Way', 'Double Carriage Way', @id, 3)

----------------------------------------------------------------------		

INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId, VisibleIndex) 
VALUES	('Which Vehicle Lights Were On?', 3, 8, 9)

SET @id = SCOPE_IDENTITY()

INSERT INTO md.ClaimsQuestionAnswer (Name, Answer, ClaimsQuestionId, VisibleIndex)
VALUES	('Which Vehicle Lights Were On - Insured', 'Insured', @id, 0),
		('Which Vehicle Lights Were On - Other Vehicle', 'Other Vehicle', @id, 1),
		('Which Vehicle Lights Were On - Neither', 'Neither', @id, 2)

----------------------------------------------------------------------				
	
INSERT INTO md.ClaimsQuestion(Name, ClaimsQuestionTypeId, ClaimsQuestionGroupId, VisibleIndex) 
VALUES	('Declaration Of Capacity', 3, 11, 3)

SET @id = SCOPE_IDENTITY()

INSERT INTO md.ClaimsQuestionAnswer (Name, Answer, ClaimsQuestionId, VisibleIndex)
VALUES	('Declaration Of Capacity - Policy Holder', 'Policy Holder', @id, 0),
		('Declaration Of Capacity - Representative', 'Representative', @id, 1)

----------------------------------------------------------------------		
			