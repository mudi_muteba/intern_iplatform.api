﻿	if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_ProductBenefit_CoverDefinition]') AND parent_object_id = OBJECT_ID('ProductBenefit'))
	alter table ProductBenefit drop constraint FK_ProductBenefit_CoverDefinition

if exists (select * from dbo.sysobjects where id = object_id(N'ProductBenefit') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table ProductBenefit