﻿if NOT exists (select * from dbo.sysobjects where id = object_id(N'ProductBenefit') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    create table ProductBenefit (
        Id INT IDENTITY NOT NULL,
       CoverDefinitionId int NOT NULL,
	   Name NVARCHAR(200) NOT NULL default(''),
       Value NVARCHAR(200) NOT NULL default(''),
       ShowToClient bit NOT NULL,
       VisibleIndex int NOT NULL,
       primary key (Id)
    )
END

if not exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_ProductBenefit_CoverDefinition]') AND parent_object_id = OBJECT_ID('ProductBenefit'))
BEGIN
    alter table ProductBenefit 
        add constraint FK_ProductBenefit_CoverDefinition
        foreign key (CoverDefinitionId) 
        references CoverDefinition
END