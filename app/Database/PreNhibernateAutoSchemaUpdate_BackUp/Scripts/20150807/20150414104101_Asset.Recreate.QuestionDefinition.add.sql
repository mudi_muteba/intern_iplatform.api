﻿
if not exists(select * from sys.columns where Name = N'Answer' and Object_ID = Object_ID(N'ProposalQuestionAnswer'))
begin
	alter table ProposalQuestionAnswer
	add Answer nvarchar(255) null

	print 'ADDED Answer'
end
go

if not exists(select * from sys.columns where Name = N'QuestionTypeId' and Object_ID = Object_ID(N'ProposalQuestionAnswer'))
begin
	alter table ProposalQuestionAnswer
	add QuestionTypeId int null
	print 'ADDED QuestionTypeId'

	ALTER TABLE [ProposalQuestionAnswer] ADD CONSTRAINT [ProposalQuestionAnswer_QuestionType] 
	FOREIGN KEY (QuestionTypeId) REFERENCES [md].[QuestionType] ([Id])
end
go

if exists(select * from sys.columns where Name = N'QuestionAnswerId' and Object_ID = Object_ID(N'ProposalQuestionAnswer'))
begin
	alter table ProposalQuestionAnswer
	drop ProposalQuestionAnswer_QuestionAnswer
	
	alter table ProposalQuestionAnswer
	drop column QuestionAnswerId
	print 'DROPED QuestionAnswerId'

end
go

if exists(select * from sys.columns where Name = N'CustomAnswer' and Object_ID = Object_ID(N'ProposalQuestionAnswer'))
begin
	alter table ProposalQuestionAnswer
	drop column CustomAnswer
	print 'DROPED CustomAnswer'

end
go

-- Question Type
SET IDENTITY_INSERT md.QuestionType ON

	if not exists(select * from md.QuestionType where Name = 'Address')
	begin
		insert into md.QuestionType (Id,Name) values (5,'Address')
	end
	go

	if not exists(select * from md.QuestionType where Name = 'Asset')
	begin
		insert into md.QuestionType (Id,Name) values (6,'Asset')
	end
	go

SET IDENTITY_INSERT md.QuestionType OFF

-- Question
SET IDENTITY_INSERT md.Question ON
	
	if not exists(select * from md.Question where Name = 'Work Address')
	begin
		insert into md.Question (Id,Name,QuestionTypeId,QuestionGroupId) values (139,'Work Address',5,1)
	end
	go

	if not exists(select * from md.Question where Name = 'Overnight Address')
	begin
		insert into md.Question (Id,Name,QuestionTypeId,QuestionGroupId) values (140,'Overnight Address',5,1)
	end
	go

	if not exists(select * from md.Question where Name = 'Asset')
	begin
		insert into md.Question (Id,Name,QuestionTypeId,QuestionGroupId) values (141,'Asset',6,2)
	end
	go

SET IDENTITY_INSERT md.Question OFF


-- Question Definition

	if not exists(select * from QuestionDefinition where DisplayName = 'Work Address')
	begin
		INSERT [dbo].[QuestionDefinition] ( [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId])
		VALUES (0, N'Work Address', 96, 139, NULL, 1, 0, 1, 1, 0, N'{Title} {Name} would you please provide me with your work address?', N'', N'', 2, 3)
	end
	go


	if not exists(select * from QuestionDefinition where DisplayName = 'Overnight Address')
	begin
		INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) 
		VALUES (0, N'Overnight Address', 96, 140, NULL, 1, 0, 1, 1, 0, N'{Title} {Name} would you please provide me with the overnight address?', N'', N'', 1, 1)
	end
	go

	if not exists(select * from QuestionDefinition where DisplayName = 'Asset')
	begin
	INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern], [GroupIndex], [QuestionDefinitionGroupTypeId]) 
	VALUES (0, N'Asset', 96, 141, NULL, 1, 0, 1, 1, 0, N'I''d like to gather some information about the asset you would like to insure.', N'', N'^[0-9]+$', 1, 4)
	end
	go

	if exists(select * from QuestionDefinition where DisplayName = 'Province' and QuestionId =47 and CoverDefinitionId = 96)
	begin
		update QuestionDefinition set GroupIndex = 1, QuestionDefinitionGroupTypeId = 1 where id = 1424
	end
	go




