﻿    
	if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK85BC8D3CDB2A8D2E]') AND parent_object_id = OBJECT_ID('Configuration'))
		alter table Configuration  drop constraint FK_Configuration_Party

    if exists (select * from dbo.sysobjects where id = object_id(N'Configuration') and OBJECTPROPERTY(id, N'IsUserTable') = 1) 
		drop table Configuration