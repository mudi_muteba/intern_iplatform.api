begin tran
IF NOT EXISTS (select * from md.cover where name = 'Band A')
INSERT [md].[Cover] ([Name]) VALUES ( N'Band A')
IF NOT EXISTS (select * from md.cover where name = 'Band B')
INSERT [md].[Cover] ([Name]) VALUES ( N'Band B')
IF NOT EXISTS (select * from md.cover where name = 'Band C')
INSERT [md].[Cover] ([Name]) VALUES ( N'Band C')
IF NOT EXISTS (select * from md.cover where name = 'Band D')
INSERT [md].[Cover] ([Name]) VALUES ( N'Band D')
IF NOT EXISTS (select * from md.cover where name = 'Band E')
INSERT [md].[Cover] ([Name]) VALUES ( N'Band E')
commit
GO