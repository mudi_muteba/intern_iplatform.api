INSERT INTO [md].[RelationshipType]
           ([Name])
     VALUES
           ('Director'),
           ('Member'),
           ('Trustee'),
           ('Owner'),
           ('ComplianceOfficer'),
		   ('Branch'),
		   ('ContactPerson')
