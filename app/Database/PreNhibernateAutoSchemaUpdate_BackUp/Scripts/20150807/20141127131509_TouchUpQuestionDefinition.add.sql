begin tran
declare @CoverDefId int

select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Touch Up' and p.Name = 'Touch Up'

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @CoverDefId, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')

commit
GO