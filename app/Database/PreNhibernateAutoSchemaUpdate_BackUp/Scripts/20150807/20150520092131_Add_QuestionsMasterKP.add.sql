﻿----------------------------------------------------------------------
DECLARE @id INT
DECLARE @idPaintType INT

DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')
DECLARE @CoverID INT 
SET @CoverID=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND coverid=218)
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Main Driver First Name', 4, 5) -- Driver Information

SET @id = SCOPE_IDENTITY()

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Main Driver First Name' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          34 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the main drivers first name?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Main Driver Surname', 4, 5) -- Driver Information

SET @id = SCOPE_IDENTITY()

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Main Driver Surname' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          35 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the main drivers surname?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )


INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Main Driver Title', 3, 5) -- Driver Information

SET @id = SCOPE_IDENTITY()

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Main Driver Title' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          33 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the main drivers title?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Main Driver Title - Mr','Mr', @id, 0),
		('Main Driver Title - Mrs','Mrs', @id, 1),
		('Main Driver Title - Mr (Jnr)','Mr (Jnr)', @id, 2),
		('Main Driver Title - Mr (Snr)','Mr (Snr)', @id, 3),
		('Main Driver Title - Miss','Miss', @id, 4),
		('Main Driver Title - Admiral','Admiral', @id, 5),
		('Main Driver Title - Advocate','Advocate', @id, 6),
		('Main Driver Title - Archbishop','Archbishop', @id, 7),
		('Main Driver Title - Bishop','Bishop', @id, 8),
		('Main Driver Title - Brigadier','Brigadier', @id, 9),
		('Main Driver Title - Captain','Captain', @id, 10),
		('Main Driver Title - Colonel','Colonel', @id, 11),
		('Main Driver Title - Commissioner','Commissioner', @id, 12),
		('Main Driver Title - Director','Director', @id, 13),
		('Main Driver Title - Doctor','Doctor', @id, 14),
		('Main Driver Title - Estate Late','Estate Late', @id, 15),
		('Main Driver Title - General','General', @id, 16),
		('Main Driver Title - Honourable','Honourable', @id, 17),
		('Main Driver Title - Honourable Judge','Honourable Judge', @id, 18),
		('Main Driver Title - Inspector','Inspector', @id, 19),
		('Main Driver Title - Judge','Judge', @id, 20),
		('Main Driver Title - Justice','Justice', @id, 21),
		('Main Driver Title - Lady','Lady', @id, 22),
		('Main Driver Title - Lieutenant','Lieutenant', @id, 23),
		('Main Driver Title - Lieutenant Colonel','Lieutenant Colonel', @id, 24),
		('Main Driver Title - Lieutenant Commander','Lieutenant Commander', @id, 25),
		('Main Driver Title - Magistrate','Magistrate', @id, 26),
		('Main Driver Title - Major','Major', @id, 27),
		('Main Driver Title - Minister','Minister', @id, 28),
		('Main Driver Title - Pastor','Pastor', @id, 29),
		('Main Driver Title - Professor','Professor', @id, 30),
		('Main Driver Title - Psychologist','Psychologist', @id, 31),
		('Main Driver Title - Rabbi','Rabbi', @id, 32),
		('Main Driver Title - Reverend','Reverend', @id, 33),
		('Main Driver Title - Senior Superintendent','Senior Superintendent', @id, 34),
		('Main Driver Title - Sergeant','Sergeant', @id, 35),
		('Main Driver Title - Sir','Sir', @id, 36),
		('Main Driver Title - Superintendent','Superintendent', @id, 37),
		('Main Driver Title - The Honourable','The Honourable', @id, 38),
		('Main Driver Title - Trust','Trust', @id, 39),
		('Main Driver Title - Trustee','Trustee', @id, 40),
		('Main Driver Title - Unknown','Unknown', @id, 41)


INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Main Driver Card Date Valid From', 2, 5) -- Driver Information

SET @id = SCOPE_IDENTITY()

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Main Driver Card Date Valid From' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          36 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the main drivers license card valid from date?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Main Driver Card Date Valid To', 2, 5) -- Driver Information

SET @id = SCOPE_IDENTITY()

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Main Driver Card Date Valid To' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          37 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the main drivers license card valid to date?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Main Driver Last Accident Claim', 3, 5) -- Driver Information

SET @id = SCOPE_IDENTITY()

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Main Driver Last Accident Claim' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          38 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the number of years since the main drivers last accident claim?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Main Driver Last Accident Claim - 0','0', @id, 0),
		('Main Driver Last Accident Claim - 1','1', @id, 1),
		('Main Driver Last Accident Claim - 2','2', @id, 2),
		('Main Driver Last Accident Claim - 3','3', @id, 3),
		('Main Driver Last Accident Claim - 4','4', @id, 4),
		('Main Driver Last Accident Claim - 5','5', @id, 5),
		('Main Driver Last Accident Claim - 6','6', @id, 6),
		('Main Driver Last Accident Claim - 7','7', @id, 7),
		('Main Driver Last Accident Claim - 8','8', @id, 8),
		('Main Driver Last Accident Claim - 9','9', @id, 9),
		('Main Driver Last Accident Claim - 10','10', @id, 10)

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Main Driver Last Theft Claim', 3, 5) -- Driver Information

SET @id = SCOPE_IDENTITY()

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Main Driver Last Theft Claim' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          39 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the number of years since the main drivers last theft claim?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Main Driver Last Theft Claim - 0','0', @id, 0),
		('Main Driver Last Theft Claim - 1','1', @id, 1),
		('Main Driver Last Theft Claim - 2','2', @id, 2),
		('Main Driver Last Theft Claim - 3','3', @id, 3),
		('Main Driver Last Theft Claim - 4','4', @id, 4),
		('Main Driver Last Theft Claim - 5','5', @id, 5),
		('Main Driver Last Theft Claim - 6','6', @id, 6),
		('Main Driver Last Theft Claim - 7','7', @id, 7),
		('Main Driver Last Theft Claim - 8','8', @id, 8),
		('Main Driver Last Theft Claim - 9','9', @id, 9),
		('Main Driver Last Theft Claim - 10','10', @id, 10)

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Mileage', 3, 2) -- Risk Information

SET @id = SCOPE_IDENTITY()

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Vehicle Mileage' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          40 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the mileage for the vehicle?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Mileage - Average','Average', @id, 0),
		('Vehicle Mileage - High','High', @id, 1),
		('Vehicle Mileage - Very High','Very High', @id, 2),
		('Vehicle Mileage - Very Low','Very Low', @id, 3),
		('Vehicle Mileage - Low','Low', @id, 4)

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Monthly Mileage', 3, 2) -- Risk Information

SET @id = SCOPE_IDENTITY()

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Vehicle Monthly Mileage' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          41 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the monthly mileage for the vehicle?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Monthly Mileage - 0 To 1000','0 To 1000', @id, 0),
		('Vehicle Monthly Mileage - 1001 To 2000','1001 To 2000', @id, 1),
		('Vehicle Monthly Mileage - 2001 To 4000','2001 To 4000', @id, 2),
		('Vehicle Monthly Mileage - 4000 Or More','4000 Or More', @id, 3)

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Condition', 3, 2) -- Risk Information

SET @id = SCOPE_IDENTITY()

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Vehicle Condition' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          42 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you please provide me with the condition for the vehicle?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Condition - Excellent','Excellent', @id, 0),
		('Vehicle Condition - Good','Good', @id, 1),
		('Vehicle Condition - Very Good','Very Good', @id, 2),
		('Vehicle Condition - Poor','Poor', @id, 3),
		('Vehicle Condition - Very Poor','Very Poor', @id, 4)