﻿--s Schema
--IF NOT EXISTS (SELECT * FROM master.sys.schemas WHERE name = 'md') EXECUTE ('CREATE SCHEMA md AUTHORIZATION dbo;');
--GO


-- Table
create table md.AuditType (
    Id INT IDENTITY NOT NULL,
    Name NVARCHAR(30) NOT NULL UNIQUE,
    primary key (Id)
)

-- Data
INSERT INTO md.AuditType(Name) Values('Note')
INSERT INTO md.AuditType(Name) Values('Lead')