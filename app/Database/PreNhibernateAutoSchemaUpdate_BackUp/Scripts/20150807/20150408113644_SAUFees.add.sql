﻿UPDATE dbo.ProductFee SET ProductFee.value=1200 
FROM dbo.ProductFee 
	INNER JOIN dbo.Product ON dbo.Product.Id = dbo.ProductFee.ProductId
WHERE product.ProductCode IN ('CENTND','CENTRD','SNTMND','SNTMRD')
	AND ProductFee.PaymentPlanId=1

UPDATE dbo.ProductFee SET ProductFee.value=600 
FROM dbo.ProductFee 
	INNER JOIN dbo.Product ON dbo.Product.Id = dbo.ProductFee.ProductId
WHERE product.ProductCode IN ('CENTND','CENTRD','SNTMND','SNTMRD')
	AND ProductFee.PaymentPlanId=2

UPDATE dbo.ProductFee SET ProductFee.value=100 
FROM dbo.ProductFee 
	INNER JOIN dbo.Product ON dbo.Product.Id = dbo.ProductFee.ProductId
WHERE product.ProductCode IN ('CENTND','CENTRD','SNTMND','SNTMRD')
	AND ProductFee.PaymentPlanId=3

UPDATE dbo.ProductFee SET ProductFee.value=300 
FROM dbo.ProductFee 
	INNER JOIN dbo.Product ON dbo.Product.Id = dbo.ProductFee.ProductId
WHERE product.ProductCode IN ('CENTND','CENTRD','SNTMND','SNTMRD')
	AND ProductFee.PaymentPlanId=4