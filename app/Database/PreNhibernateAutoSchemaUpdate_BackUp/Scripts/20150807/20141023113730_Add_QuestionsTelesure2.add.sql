﻿
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES		('Suburb (Work)', 4, 1) -- General Information
			

----------------------------------------------------------------------
DECLARE @id INT
DECLARE @questionType INT = 3
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Daytime Access Control', @questionType, 3) -- Security Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Daytime Access Control - Sentry And Or Boomed Gate', 'Sentry And Or Boomed Gate', @id, 2),
		('Vehicle Daytime Access Control - None', 'None', @id, 1),
		('Vehicle Daytime Access Control - Electronic Access', 'Electronic Access', @id, 3)

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Vehicle Overnight Access Control', @questionType, 3) -- Security Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Overnight Access Control - Sentry And Or Boomed Gate', 'Sentry And Or Boomed Gate', @id, 2),
		('Vehicle Overnight Access Control - None', 'None', @id, 1),
		('Vehicle Overnight Access Control - Electronic Access', 'Electronic Access', @id, 3)
		