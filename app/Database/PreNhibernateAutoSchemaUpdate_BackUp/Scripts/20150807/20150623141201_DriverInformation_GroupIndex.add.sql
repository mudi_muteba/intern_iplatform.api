if not exists(select  * from md.QuestionDefinitionGroupType where id = 5)
insert into md.QuestionDefinitionGroupType select 5, 'Driver Information'

update qa 
set qa.QuestionDefinitionGroupTypeId = 5
from QuestionDefinition qa 
inner join md.Question q on q.Id = qa.QuestionId
where qa.CoverDefinitionId = 96
and q.QuestionGroupId = 5