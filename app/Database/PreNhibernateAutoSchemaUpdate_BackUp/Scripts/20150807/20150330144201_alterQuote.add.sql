﻿
if not exists(select * from sys.columns where Name = N'RequestDto' and Object_ID = Object_ID(N'Quote'))
begin
	alter table Quote
	add RequestDto varbinary(MAX) null
end

if not exists(select * from sys.columns where Name = N'IsAccepted' and Object_ID = Object_ID(N'Quote'))
begin
	alter table Quote
	add IsAccepted bit default 0
end

if not exists(select * from sys.columns where Name = N'AcceptedDate' and Object_ID = Object_ID(N'Quote'))
begin
	alter table Quote
	add AcceptedDate DateTime null
end