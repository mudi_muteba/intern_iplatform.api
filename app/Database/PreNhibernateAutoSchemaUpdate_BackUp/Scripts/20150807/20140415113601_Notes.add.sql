﻿-- Table
create table Note (
    Id INT IDENTITY NOT NULL,
    DateCreated DATETIME null,
    Notes NVARCHAR(255) null,
    UserId INT null,
    ClientId INT null,
    Confidential BIT null,
    Warning BIT null,
    primary key (Id)
)

-- Constraints
alter table Note 
    add constraint FK_Note_Individual 
    foreign key (UserId) 
    references Individual

alter table Note
    add constraint FK_Note_Party 
    foreign key (ClientId) 
    references Party