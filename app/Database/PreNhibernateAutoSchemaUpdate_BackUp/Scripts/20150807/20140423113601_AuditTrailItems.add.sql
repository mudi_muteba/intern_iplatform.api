﻿-- Table
create table AuditTrailItem (
    Id INT IDENTITY NOT NULL,
    DateCreated DATETIME NOT NULL default(getdate()),
    [Description] NVARCHAR(255) null,
    Reason NVARCHAR(255) null,
    PartyId INT null,
	IndividualId INT null,
    primary key (Id)
)

-- Constraints
alter table AuditTrailItem 
    add constraint FK_AuditTrailItem_Party
    foreign key (PartyId) 
    references Party


alter table AuditTrailItem
    add constraint FK_AuditTrailItem_Individual 
    foreign key (IndividualId) 
    references Individual