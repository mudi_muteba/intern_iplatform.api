/*
Created: 16/01/2015
Modified: 19/01/2015
Model: Microsoft SQL Server 2008
Database: MS SQL Server 2008
*/


-- Create tables section -------------------------------------------------

-- Table ProposalDefinition

if not exists (select * from dbo.sysobjects where id = object_id(N'ProposalDefinition'))
begin
	CREATE TABLE [ProposalDefinition]
	(
	 [Id] Int IDENTITY NOT NULL,
	 [ProposalHeaderId] Int NOT NULL,
	 [AssetId] Int NOT NULL,
	 [PartyId] Int NOT NULL,
	 [CoverId] Int NOT NULL,
	 [LeadActivityId] Int NULL,
	 [ProductId] Int NULL,
	 [IsVap] Bit DEFAULT 0 NULL,
	 [Created] Datetime NULL,
	 [Modified] Datetime NULL,
	 [IsDeleted] Bit DEFAULT 0 NULL
	)
end
go

-- Add keys for table ProposalDefinition

if not exists (select * from dbo.sysobjects where id = object_id(N'ProposalDefinition_Id'))
begin
	ALTER TABLE [ProposalDefinition] ADD CONSTRAINT [ProposalDefinition_Id] PRIMARY KEY ([Id])
end
go

-- Table ProposalHeader

if not exists (select * from dbo.sysobjects where id = object_id(N'ProposalHeader'))
begin
	CREATE TABLE [ProposalHeader]
	(
	 [Id] Int IDENTITY NOT NULL,
	 [Description] Nvarchar(50) NULL,
	)
end
go

-- Add keys for table Asset
if not exists (select * from dbo.sysobjects where id = object_id(N'ProposalHeader_Id'))
begin
	ALTER TABLE [ProposalHeader] ADD CONSTRAINT [ProposalHeader_Id] PRIMARY KEY ([Id])
end
go

-- Table Asset
if not exists (select * from dbo.sysobjects where id = object_id(N'Asset'))
begin
	CREATE TABLE [Asset]
	(
	 [Id] Int IDENTITY NOT NULL,
	 [Description] Nvarchar(50) NULL,
	)
end
go

-- Add keys for table Asset

if not exists (select * from dbo.sysobjects where id = object_id(N'Asset_Id'))
begin
	ALTER TABLE [Asset] ADD CONSTRAINT [Asset_Id] PRIMARY KEY ([Id])
end
go


-- Table Asset
if not exists (select * from dbo.sysobjects where id = object_id(N'AssetVehicle'))
begin
	CREATE TABLE [AssetVehicle]
	(
	 [Id] Int NOT NULL,
	 [YearOfManufacture] Int NULL,
	 [VehicleMake] Nvarchar(50) NULL,
	 [VehicleModel] Nvarchar(50) NULL,
	 [VehicleRegistrationNumber] Nvarchar(50) NULL,
	 [VehicleMMCode] Nvarchar(50) NULL
	)
end
go

-- Add keys for table Asset
if not exists (select * from dbo.sysobjects where id = object_id(N'AssetVehicle_Id'))
begin
	ALTER TABLE [AssetVehicle] ADD CONSTRAINT [AssetVehicle_Id] PRIMARY KEY ([Id])
end
go

-- Table ProposalQuestionAnswer
if not exists (select * from dbo.sysobjects where id = object_id(N'ProposalQuestionAnswer'))
begin
	CREATE TABLE [ProposalQuestionAnswer]
	(
	 [Id] Int IDENTITY NOT NULL,
	 [ProposalDefinitionId] Int NOT NULL,
	 [QuestionId] Int NOT NULL,
	 [QuestionAnswerId] Int NULL,
	 [CustomAnswer] Nvarchar(100) NULL
	)
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'ProposalQuestionAnswer_Id'))
begin
	ALTER TABLE [ProposalQuestionAnswer] ADD CONSTRAINT [ProposalQuestionAnswer_Id] PRIMARY KEY ([Id])
end
go

-- Create relationships section ------------------------------------------------- 

if not exists (select * from dbo.sysobjects where id = object_id(N'Proposal_Asset'))
begin
	ALTER TABLE [ProposalDefinition] ADD CONSTRAINT [Proposal_Asset] FOREIGN KEY ([AssetId]) REFERENCES [Asset] ([Id])
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'Proposal_Party'))
begin
	ALTER TABLE [ProposalDefinition] ADD CONSTRAINT [Proposal_Party] FOREIGN KEY ([PartyId]) REFERENCES [Party] ([Id])
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'Proposal_Cover'))
begin
	ALTER TABLE [ProposalDefinition] ADD CONSTRAINT [Proposal_Cover] FOREIGN KEY ([CoverId]) REFERENCES [md].[Cover] ([Id])
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'Proposal_Product'))
begin
	ALTER TABLE [ProposalDefinition] ADD CONSTRAINT [Proposal_Product] FOREIGN KEY ([ProductId]) REFERENCES [Product] ([Id])
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'ProposalQuestionAnswer_Proposal'))
begin
	ALTER TABLE [ProposalQuestionAnswer] ADD CONSTRAINT ProposalQuestionAnswer_Proposal FOREIGN KEY ([ProposalDefinitionId]) REFERENCES [ProposalDefinition] ([Id])
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'Proposal_ProposalHeader'))
begin
	ALTER TABLE [ProposalDefinition] ADD CONSTRAINT [Proposal_ProposalHeader] FOREIGN KEY ([ProposalHeaderId]) REFERENCES [ProposalHeader] ([Id])
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'ProposalQuestionAnswer_Question'))
begin
	ALTER TABLE [ProposalQuestionAnswer] ADD CONSTRAINT [ProposalQuestionAnswer_Question] FOREIGN KEY ([QuestionId]) REFERENCES [md].[Question] ([Id])
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'ProposalQuestionAnswer_QuestionAnswer'))
begin
	ALTER TABLE [ProposalQuestionAnswer] ADD CONSTRAINT [ProposalQuestionAnswer_QuestionAnswer] FOREIGN KEY ([QuestionAnswerId]) REFERENCES [md].[QuestionAnswer] ([Id])
end
go

if not exists (select * from dbo.sysobjects where id = object_id(N'Proposal_LeadActivity'))
begin
	ALTER TABLE [ProposalDefinition] ADD CONSTRAINT [Proposal_LeadActivity] FOREIGN KEY ([LeadActivityId]) REFERENCES [LeadActivity] ([Id])
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'AssetVehicle_Asset'))
begin
	ALTER TABLE [dbo].[AssetVehicle]  WITH CHECK ADD  CONSTRAINT [AssetVehicle_Asset] FOREIGN KEY([Id]) REFERENCES [dbo].[Asset] ([Id])
end