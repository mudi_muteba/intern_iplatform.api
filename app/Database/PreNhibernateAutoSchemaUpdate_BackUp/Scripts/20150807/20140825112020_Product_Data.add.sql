﻿SET IDENTITY_INSERT [dbo].[Party] ON 

GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (1, 0, 2, NULL, N'Santam', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (2, 0, 2, NULL, N'Santam', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (3, 0, 2, NULL, N'Hollard', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (4, 0, 2, NULL, N'Hollard', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (5, 0, 2, NULL, N'Regent', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (6, 0, 2, NULL, N'Auto & General', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (7, 0, 2, NULL, N'Budget', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (8, 0, 2, NULL, N'Dial Direct', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (9, 0, 2, NULL, N'First for Women', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (10, 0, 2, NULL, N'Unity', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (11, 0, 2, NULL, N'AA Insurance', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (12, 0, 2, NULL, N'Virgin Money', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (13, 0, 2, NULL, N'Renasa', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (14, 0, 2, NULL, N'New National', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (15, 0, 2, NULL, N'SA Underwriters', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (16, 0, 2, NULL, N'SA Underwriters', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (17, 0, 2, NULL, N'SA Underwriters', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (18, 0, 2, NULL, N'SA Underwriters', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (19, 0, 2, NULL, N'Oakhurst Insurance Company Limited', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (20, 0, 2, NULL, N'Auto & General', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (21, 0, 2, NULL, N'Budget', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (22, 0, 2, NULL, N'Dial Direct', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (23, 0, 2, NULL, N'First for Women', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (24, 0, 2, NULL, N'Unity', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (25, 0, 2, NULL, N'AA Insurance', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (26, 0, 2, NULL, N'Virgin Money', NULL, NULL)
GO
INSERT [dbo].[Party] ([Id], [MasterId], [PartyTypeId], [ContactDetailId], [DisplayName], [DateCreated], [DateUpdated]) VALUES (27, 0, 2, NULL, N'Master', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Party] OFF
GO
SET ANSI_PADDING ON
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (1, N'STM', N'Santam Insurance Company Limited', N'Santam', CAST(0x521E0B00 AS Date), N'At Santam, we believe in doing insurance good and proper, and we''ve been doing it that way for nearly a 100 years. With a market share exceeding 22%, Santam focuses on corporate, commercial and personal markets.', N'1918/001680/06', N'3416', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (2, N'STM', N'Santam Insurance Company Limited', N'Santam', CAST(0x521E0B00 AS Date), N'At Santam, we believe in doing insurance good and proper, and we''ve been doing it that way for nearly a 100 years. With a market share exceeding 22%, Santam focuses on corporate, commercial and personal markets.', N'1918/001680/06', N'3416', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (3, N'HOL', N'Hollard Insurance Company Limited', N'Hollard', CAST(0x7E070B00 AS Date), N'Hollard is South Africa’s largest private insurance company. We are passionate about our people, our customers, our partners and our employees. We’ve been doing business differently for twenty eight years and today we have 1,300 dedicated Hollardites. Hollard provide both short-term (motor and household) and life (funeral, life, endowment and disability) insurance product solutions to more than 6 million policy holders. We are also proud to be a local company making a difference globally in 9 countries and growing.', N'1952/003004/06', N'17698', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (4, N'HOL', N'Hollard Insurance Company Limited', N'Hollard', CAST(0x7E070B00 AS Date), N'Hollard is South Africa’s largest private insurance company. We are passionate about our people, our customers, our partners and our employees. We’ve been doing business differently for twenty eight years and today we have 1,300 dedicated Hollardites. Hollard provide both short-term (motor and household) and life (funeral, life, endowment and disability) insurance product solutions to more than 6 million policy holders. We are also proud to be a local company making a difference globally in 9 countries and growing.', N'1952/003004/06', N'17698', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (5, N'REG', N'Regent Insurance Company Limited', N'Regent', CAST(0x30170B00 AS Date), N'The Regent Group is part of the Imperial family. Imperial is a diversified multinational mobility group with activities that include motor vehicles and related operations across all modes of transport for people and freight, both locally and abroad. As part of this diversified group, the Regent culture is based on entrepreneurship, innovation and an adherence to industry-specific best practices that characterise the way Imperial does business. The Regent Group has become a well-known specialist and market leader in its chosen markets and an exceptional range of short-term insurance and life assurance products are available under one Regent brand, offering a one-stop-shop.', N'1966/007612/06', N'25511', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (6, N'AUG', N'Auto & General Insurance Company Limited', N'Auto & General', CAST(0x380F0B00 AS Date), N'Auto & General Insurance has a proud and illustrious history of innovation and has been an influential player in the short-term insurance industry since its inception in 1985. Over the years, the Auto & General name has become synonymous with service excellence, and affordable, innovatively-packaged and quality insurance products.', N'1973/016880/06', N'16354', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (7, N'BIB', N'Budget Insurance Company Limited', N'Budget', CAST(0x2D210B00 AS Date), N'Since 1998, Budget Insurance has provided the South African public with low, low premiums on their motor and household insurance, by cutting the cost and not the cover. They also have great value added benefits including: Tracker installation, on qualifying vehicles; Home Assist which covers the call-out fee plus one hour’s labour for the service of plumbers, electricians, glaziers and locksmiths; Roadside Assist, which includes a towing service, emergency petrol delivery and many other benefits. The company also offers Business Insurance.', N'2004/025764/06', N'18178', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (8, N'DIAD', N'Dial Direct Insurance Company Limited', N'Dial Direct', CAST(0xE51C0B00 AS Date), N'Dial Direct was named the most popular short-term insurance brand in South Africa in the 2008 Sunday Times Business Times / Ipsos Markinor Brand Survey. Dial Direct has a comprehensive online policy management system where clients can amend their policies, and submit and track their insurance claims online. In addition Dial Direct’s Bucks Back Bonus pays out up 25% of premiums paid, or the first years premiums (whichever is the lesser) after four years of continuous cover, even if certain claims are made.', N'1995/008764/06', N'15259', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (9, N'FFW', N'First for Women Insurance Company Limited', N'First for Women', CAST(0xE2260B00 AS Date), N'1st for Women Insurance understands women’s unique insurance needs. 1st for Women offers car, home contents, buildings, business and life insurance as well as a host of unique value-added products. The company’s growth has exceeded all expectations, indicating that women welcome a short-term insurance offering that is tailor-made to meet their needs - not just a generic product in pink packaging!', N'1998/004804/07', N'15261', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (10, N'UNITY', N'Unity Insurance Company Limited', N'Unity', CAST(0xED2A0B00 AS Date), N'Established in November 2004, Unity Insurance is a short-term underwriter which delivers specialist short-term insurance products to address the lifestyle needs of all South Africans. The company takes its products to market through various channels including a national network of brokers and affinity partners, thereby creating employment and contributing to South African business development.', N'2004/025764/06', N'18178', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (11, N'AA', N'AA Insurance', N'AA Insurance', CAST(0x4C320B00 AS Date), N'Since Insurance driven by the AA’s inception they have been delivering trusted motor, home, buildings and business insurance to South African consumers. Insurance driven by the AA is affiliated to the well-known and trusted Automobile Association of South Africa (AA). Insurance driven by the AA offers its policy holders the unique benefit of an automatic Advantage Plus membership to the AA (if you’re not already a member).', N'1973/016880/06', N'16354', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (12, N'VRGNMNY', N'Virgin Money South Africa (Pty) Ltd.', N'Virgin Money', CAST(0x87320B00 AS Date), N'Virgin Money - Insurance like it should be launched in March 2010 with a view to shake up and transform South Africa’s short-term insurance industry. Through Virgin Money – Insurance like it should be consumers have access to car and home insurance that’s simple and easy to understand. They also offer meaningful benefits, like no motor excess payments when the damage to the insured car is over R 5000 and fixed premiums for 24 months.', N'2005/016196/07', N'31730', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (13, N'REN', N'Renasa Insurance Company Limited', N'Renasa', CAST(0x2D210B00 AS Date), N'Renasa has established itself as one of South Africa''s leading independent general insurers. It is A-rated and focuses on servicing independent intermediaries and their clients with a broad product range, a national footprint and industry-leading technology.', N'1998/000916/06', N'15491', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (14, N'NEWNAT', N'New National Assurance Company Limited', N'New National', CAST(0xA1FB0A00 AS Date), N'The New National Assurance Company is South Africa’s oldest black-owned short term insurer. Our business is to provide risk management solutions to our clients, and to do so in a sustainable and value-creating manner for our shareholders. Having traded profitably for over four decades, we are proud to be a transformation pioneer in the insurance industry.', N'1971/10190/06', N'2603', N'4380101289')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (15, N'SAU', N'SA Underwriting Agencies (Pty) Ltd', N'SA Underwriters', CAST(0x9D180B00 AS Date), N'SAU was established in 1992 and through our solid track record we have become one of the largest personal line underwriting managers in South Africa. We at SAU have proven ourselves as specialists in motor stand alone insurance with various innovative products.', N'1992/03324/07', N'281', N'RSA1029384756')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (16, N'SAU', N'SA Underwriting Agencies (Pty) Ltd', N'SA Underwriters', CAST(0x9D180B00 AS Date), N'SAU was established in 1992 and through our solid track record we have become one of the largest personal line underwriting managers in South Africa. We at SAU have proven ourselves as specialists in motor stand alone insurance with various innovative products.', N'1992/03324/07', N'281', N'RSA1029384756')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (17, N'SAU', N'SA Underwriting Agencies (Pty) Ltd', N'SA Underwriters', CAST(0x9D180B00 AS Date), N'SAU was established in 1992 and through our solid track record we have become one of the largest personal line underwriting managers in South Africa. We at SAU have proven ourselves as specialists in motor stand alone insurance with various innovative products.', N'1992/03324/07', N'281', N'RSA1029384756')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (18, N'SAU', N'SA Underwriting Agencies (Pty) Ltd', N'SA Underwriters', CAST(0x9D180B00 AS Date), N'SAU was established in 1992 and through our solid track record we have become one of the largest personal line underwriting managers in South Africa. We at SAU have proven ourselves as specialists in motor stand alone insurance with various innovative products.', N'1992/03324/07', N'281', N'RSA1029384756')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (19, N'LLOYDS', N'Oakhurst Insurance Company Limited', N'Oakhurst Insurance Company Limited', CAST(0xA62C0B00 AS Date), N'Oakhurst Insurance Company Limited is a licensed insurance company focusing very specifically on our clients'' car insurance needs..', N'2006-000723-06', N'39925', N'9174122177')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (20, N'AUG', N'Auto & General Insurance Company Limited', N'Auto & General', CAST(0x380F0B00 AS Date), N'Auto & General Insurance has a proud and illustrious history of innovation and has been an influential player in the short-term insurance industry since its inception in 1985. Over the years, the Auto & General name has become synonymous with service excellence, and affordable, innovatively-packaged and quality insurance products.', N'1973/016880/06', N'16354', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (21, N'BIB', N'Budget Insurance Company Limited', N'Budget', CAST(0x2D210B00 AS Date), N'Since 1998, Budget Insurance has provided the South African public with low, low premiums on their motor and household insurance, by cutting the cost and not the cover. They also have great value added benefits including: Tracker installation, on qualifying vehicles; Home Assist which covers the call-out fee plus one hour’s labour for the service of plumbers, electricians, glaziers and locksmiths; Roadside Assist, which includes a towing service, emergency petrol delivery and many other benefits. The company also offers Business Insurance.', N'2004/025764/06', N'18178', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (22, N'DIAD', N'Dial Direct Insurance Company Limited', N'Dial Direct', CAST(0xE51C0B00 AS Date), N'Dial Direct was named the most popular short-term insurance brand in South Africa in the 2008 Sunday Times Business Times / Ipsos Markinor Brand Survey. Dial Direct has a comprehensive online policy management system where clients can amend their policies, and submit and track their insurance claims online. In addition Dial Direct’s Bucks Back Bonus pays out up 25% of premiums paid, or the first years premiums (whichever is the lesser) after four years of continuous cover, even if certain claims are made.', N'1995/008764/06', N'15259', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (23, N'FFW', N'First for Women Insurance Company Limited', N'First for Women', CAST(0xE2260B00 AS Date), N'1st for Women Insurance understands women’s unique insurance needs. 1st for Women offers car, home contents, buildings, business and life insurance as well as a host of unique value-added products. The company’s growth has exceeded all expectations, indicating that women welcome a short-term insurance offering that is tailor-made to meet their needs - not just a generic product in pink packaging!', N'1998/004804/07', N'15261', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (24, N'UNITY', N'Unity Insurance Company Limited', N'Unity', CAST(0xED2A0B00 AS Date), N'Established in November 2004, Unity Insurance is a short-term underwriter which delivers specialist short-term insurance products to address the lifestyle needs of all South Africans. The company takes its products to market through various channels including a national network of brokers and affinity partners, thereby creating employment and contributing to South African business development.', N'2004/025764/06', N'18178', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (25, N'AA', N'AA Insurance', N'AA Insurance', CAST(0x4C320B00 AS Date), N'Since Insurance driven by the AA’s inception they have been delivering trusted motor, home, buildings and business insurance to South African consumers. Insurance driven by the AA is affiliated to the well-known and trusted Automobile Association of South Africa (AA). Insurance driven by the AA offers its policy holders the unique benefit of an automatic Advantage Plus membership to the AA (if you’re not already a member).', N'1973/016880/06', N'16354', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (26, N'VRGNMNY', N'Virgin Money South Africa (Pty) Ltd.', N'Virgin Money', CAST(0x87320B00 AS Date), N'Virgin Money - Insurance like it should be launched in March 2010 with a view to shake up and transform South Africa’s short-term insurance industry. Through Virgin Money – Insurance like it should be consumers have access to car and home insurance that’s simple and easy to understand. They also offer meaningful benefits, like no motor excess payments when the damage to the insured car is over R 5000 and fixed premiums for 24 months.', N'2005/016196/07', N'31730', N'')
GO
INSERT [dbo].[Organization] ([Id], [Code], [RegisteredName], [TradingName], [TradingSince], [Description], [RegNo], [FspNo], [VatNo]) VALUES (27, N'MST', N'Master Insurer', N'Master', NULL, N'', N'', N'', N'')
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (1, 0, N'Multiplex', 1, 1, 7, N'567890', N'MULPLX', CAST(0x00009CD400000000 AS DateTime), NULL)
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (2, 0, N'MultiMotor', 2, 2, 7, N'567890', N'MULMOT', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (3, 0, N'Select', 3, 3, 7, N'123456', N'ALLIN', CAST(0x00009CD400000000 AS DateTime), NULL)
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (4, 0, N'Hollard Motor and Home', 4, 4, 7, N'123456', N'HOLHMH', CAST(0x00009CD400000000 AS DateTime), NULL)
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (5, 0, N'Motor Comprehensive', 5, 5, 7, N'123456', N'REGPRD', CAST(0x00009CD400000000 AS DateTime), NULL)
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (6, 0, N'Auto & General Product', 6, 6, 7, N'123456', N'AUGPRD', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (7, 0, N'Budget Product', 7, 7, 7, N'123456', N'BUD', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (8, 0, N'Dial Direct Product', 8, 8, 7, N'123456', N'DIA', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (9, 0, N'First For Women Product', 9, 9, 7, N'123456', N'FIR', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (10, 0, N'Unity Product', 10, 10, 7, N'123456', N'UNI', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (11, 0, N'AA Product', 11, 11, 7, N'123456', N'AA', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (12, 0, N'Virgin Money Product', 12, 12, 7, N'123456', N'VIR', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (13, 0, N'Personal Scheme', 13, 13, 7, N'123456', N'RENPRD', CAST(0x00009CD400000000 AS DateTime), NULL)
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (14, 0, N'Personal Protector', 14, 14, 7, N'123456', N'FLS', CAST(0x00009CD400000000 AS DateTime), NULL)
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (15, 0, N'Centriq Named Driver', 15, 15, 7, N'123456', N'CENTND', CAST(0x00009CD400000000 AS DateTime), NULL)
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (16, 0, N'Centriq Regular Driver', 16, 16, 7, N'123456', N'CENTRD', CAST(0x00009CD400000000 AS DateTime), NULL)
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (17, 0, N'Santam Named Driver', 17, 17, 7, N'123456', N'SNTMND', CAST(0x00009CD400000000 AS DateTime), NULL)
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (18, 0, N'Santam Regular Driver', 18, 18, 7, N'123456', N'SNTMRD', CAST(0x00009CD400000000 AS DateTime), NULL)
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (19, 0, N'Lite', 19, 19, 7, N'123456', N'LITEAA', CAST(0x00009CD400000000 AS DateTime), NULL)
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (20, 0, N'Auto & General Product', 20, 20, 7, N'123456', N'CRSAUG', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (21, 0, N'Budget Product', 21, 21, 7, N'123456', N'CRSBUD', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (22, 0, N'Dial Direct Product', 22, 22, 7, N'123456', N'CRSDIA', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (23, 0, N'First For Women Product', 23, 23, 7, N'123456', N'CRSFIR', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (24, 0, N'Unity Product', 24, 24, 7, N'123456', N'CRSUNI', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (25, 0, N'AA Product', 25, 25, 7, N'123456', N'CRSAA', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (26, 0, N'Virgin Money Product', 26, 26, 7, N'123456', N'CRSVIR', CAST(0x00009CD400000000 AS DateTime), CAST(0x0000A24400000000 AS DateTime))
GO
INSERT [dbo].[Product] ([Id], [MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES (27, 0, N'MultiQuote', 27, 27, 7, N'12345', N'MUL', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[CoverDefinition] ON 

GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (1, 0, N'Motor', 1, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (2, 0, N'Household Contents', 1, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (3, 0, N'House Owners', 1, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (4, 0, N'All Risks', 1, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (5, 0, N'Motor', 2, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (6, 0, N'Motor', 3, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (7, 0, N'Motor', 4, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (8, 0, N'Household Contents', 4, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (9, 0, N'House Owners', 4, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (10, 0, N'All Risks', 4, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (11, 0, N'Motor', 5, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (12, 0, N'Household Contents', 5, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (13, 0, N'House Owners', 5, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (14, 0, N'All Risks', 5, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (15, 0, N'Motor', 6, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (16, 0, N'Household Contents', 6, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (17, 0, N'House Owners', 6, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (18, 0, N'All Risks', 6, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (19, 0, N'Motor', 7, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (20, 0, N'Household Contents', 7, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (21, 0, N'House Owners', 7, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (22, 0, N'All Risks', 7, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (23, 0, N'Motor', 8, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (24, 0, N'Household Contents', 8, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (25, 0, N'House Owners', 8, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (26, 0, N'All Risks', 8, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (27, 0, N'Motor', 9, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (28, 0, N'Household Contents', 9, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (29, 0, N'House Owners', 9, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (30, 0, N'All Risks', 9, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (31, 0, N'Motor', 10, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (32, 0, N'Household Contents', 10, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (33, 0, N'House Owners', 10, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (34, 0, N'All Risks', 10, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (35, 0, N'Motor', 11, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (36, 0, N'Household Contents', 11, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (37, 0, N'House Owners', 11, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (38, 0, N'All Risks', 11, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (39, 0, N'Motor', 12, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (40, 0, N'Household Contents', 12, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (41, 0, N'House Owners', 12, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (42, 0, N'All Risks', 12, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (43, 0, N'Motor', 13, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (44, 0, N'Household Contents', 13, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (45, 0, N'House Owners', 13, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (46, 0, N'All Risks', 13, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (47, 0, N'Motor', 14, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (48, 0, N'Household Contents', 14, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (49, 0, N'House Owners', 14, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (50, 0, N'All Risks', 14, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (51, 0, N'Motor', 15, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (52, 0, N'Household Contents', 15, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (53, 0, N'House Owners', 15, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (54, 0, N'All Risks', 15, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (55, 0, N'Motor', 16, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (56, 0, N'Household Contents', 16, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (57, 0, N'House Owners', 16, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (58, 0, N'All Risks', 16, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (59, 0, N'Motor', 17, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (60, 0, N'Household Contents', 17, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (61, 0, N'House Owners', 17, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (62, 0, N'All Risks', 17, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (63, 0, N'Motor', 18, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (64, 0, N'Household Contents', 18, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (65, 0, N'House Owners', 18, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (66, 0, N'All Risks', 18, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (67, 0, N'Motor', 19, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (68, 0, N'Motor', 20, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (69, 0, N'Household Contents', 20, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (70, 0, N'House Owners', 20, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (71, 0, N'All Risks', 20, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (72, 0, N'Motor', 21, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (73, 0, N'Household Contents', 21, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (74, 0, N'House Owners', 21, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (75, 0, N'All Risks', 21, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (76, 0, N'Motor', 22, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (77, 0, N'Household Contents', 22, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (78, 0, N'House Owners', 22, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (79, 0, N'All Risks', 22, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (80, 0, N'Motor', 23, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (81, 0, N'Household Contents', 23, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (82, 0, N'House Owners', 23, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (83, 0, N'All Risks', 23, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (84, 0, N'Motor', 24, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (85, 0, N'Household Contents', 24, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (86, 0, N'House Owners', 24, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (87, 0, N'All Risks', 24, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (88, 0, N'Motor', 25, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (89, 0, N'Household Contents', 25, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (90, 0, N'House Owners', 25, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (91, 0, N'All Risks', 25, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (92, 0, N'Motor', 26, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (93, 0, N'Household Contents', 26, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (94, 0, N'House Owners', 26, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (95, 0, N'All Risks', 26, 17, NULL, 1, 3)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (96, 0, N'Motor', 27, 218, NULL, 1, 0)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (97, 0, N'Household Contents', 27, 78, NULL, 1, 1)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (98, 0, N'House Owners', 27, 44, NULL, 1, 2)
GO
INSERT [dbo].[CoverDefinition] ([Id], [MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (99, 0, N'All Risks', 27, 17, NULL, 1, 3)
GO
SET IDENTITY_INSERT [dbo].[CoverDefinition] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductFee] ON 

GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (1, 0, 13, 2, 3, NULL, CAST(60.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (2, 0, 13, 2, 4, NULL, CAST(180.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (3, 0, 13, 2, 2, NULL, CAST(360.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (4, 0, 13, 2, 1, NULL, CAST(720.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (5, 0, 13, 3, 3, NULL, CAST(30.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (6, 0, 13, 3, 4, NULL, CAST(90.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (7, 0, 13, 3, 2, NULL, CAST(180.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (8, 0, 13, 3, 1, NULL, CAST(360.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (9, 0, 13, 3, 3, NULL, CAST(10.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 1, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (10, 0, 13, 3, 4, NULL, CAST(10.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 1, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (11, 0, 13, 3, 2, NULL, CAST(10.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 1, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (12, 0, 13, 3, 1, NULL, CAST(10.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 1, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (13, 0, 14, 2, 3, NULL, CAST(60.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (14, 0, 14, 2, 4, NULL, CAST(180.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (15, 0, 14, 2, 2, NULL, CAST(360.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (16, 0, 14, 2, 1, NULL, CAST(720.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (17, 0, 14, 3, 3, NULL, CAST(40.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (18, 0, 14, 3, 4, NULL, CAST(120.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (19, 0, 14, 3, 2, NULL, CAST(240.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (20, 0, 14, 3, 1, NULL, CAST(480.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (21, 0, 14, 3, 3, NULL, CAST(30.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (22, 0, 14, 3, 4, NULL, CAST(90.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (23, 0, 14, 3, 2, NULL, CAST(180.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (24, 0, 14, 3, 1, NULL, CAST(360.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (25, 0, 14, 3, 3, NULL, CAST(10.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 1, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (26, 0, 14, 3, 4, NULL, CAST(10.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 1, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (27, 0, 14, 3, 2, NULL, CAST(10.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 1, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (28, 0, 14, 3, 1, NULL, CAST(10.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 1, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (29, 0, 15, 2, 3, NULL, CAST(99.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (30, 0, 15, 2, 4, NULL, CAST(297.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (31, 0, 15, 2, 2, NULL, CAST(594.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (32, 0, 15, 2, 1, NULL, CAST(1188.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (33, 0, 16, 2, 3, NULL, CAST(99.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (34, 0, 16, 2, 4, NULL, CAST(297.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (35, 0, 16, 2, 2, NULL, CAST(594.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (36, 0, 16, 2, 1, NULL, CAST(1188.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (37, 0, 17, 2, 3, NULL, CAST(99.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (38, 0, 17, 2, 4, NULL, CAST(297.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (39, 0, 17, 2, 2, NULL, CAST(594.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (40, 0, 17, 2, 1, NULL, CAST(1188.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (41, 0, 18, 2, 3, NULL, CAST(99.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (42, 0, 18, 2, 4, NULL, CAST(297.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (43, 0, 18, 2, 2, NULL, CAST(594.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
INSERT [dbo].[ProductFee] ([Id], [MasterId], [ProductId], [ProductFeeTypeId], [PaymentPlanId], [BrokerageId], [Value], [MinValue], [MaxValue], [IsOnceOff], [IsPercentage], [AllowRefund], [AllowProRata]) VALUES (44, 0, 18, 2, 1, NULL, CAST(1188.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), CAST(0.00000 AS Decimal(19, 5)), 0, 0, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[ProductFee] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductBenefit] ON 

GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (1, 11, N'Private Type Vehicle', N'', 0, 0)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (2, 11, N'Included', N'Yes', 0, 1)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (3, 11, N'Use of vehicle', N'copy from system', 0, 2)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (4, 11, N'Comprehensive Cover', N'Yes', 0, 3)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (5, 11, N'New for Old', N'Yes', 0, 4)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (6, 11, N'Windscreen', N'Comprehensive only', 0, 5)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (7, 11, N'Third Party Fire and theft Cover', N'if requested', 0, 6)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (8, 11, N'Third Party Cover', N'if requested', 0, 7)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (9, 11, N'Riot and Strike outside RSA', N'No', 0, 8)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (10, 11, N'Tracking device to be covered', N'No', 0, 9)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (11, 11, N'Towing and Storage', N'Yes', 0, 10)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (12, 11, N'Sound System [not factory fitted]', N'if requested', 0, 11)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (13, 11, N'Hail Damage', N'Included', 0, 12)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (14, 11, N'Canopy of the pick-up', N'if requested', 0, 13)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (15, 11, N'Car Hire', N'if requested', 0, 14)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (16, 11, N'Emergency Hotel Expenses', N'Included', 0, 15)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (17, 11, N'Credit Shortfall ', N'if requested', 0, 16)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (18, 11, N'Medical Costs', N'R5,000', 0, 17)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (19, 11, N'Third Party Liability', N'R3,000,000', 0, 18)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (20, 11, N'Third Party fire and explosion Liability', N'R1,000,000', 0, 19)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (21, 12, N'Included', N'yes', 0, 0)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (22, 12, N'Automatic anniversary/renewal sum insured increase', N'renewal', 0, 1)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (23, 12, N'Accidental Damage extension cover', N'R5,000', 0, 2)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (24, 12, N'Documents limitation', N'R5, 000', 0, 3)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (25, 12, N'Deterioration of Food', N'R5,000', 0, 4)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (26, 12, N'Washing stolen at your home', N'R5,000', 0, 5)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (27, 12, N'Garden furniture stolen at your home', N'R5,000', 0, 6)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (28, 12, N'Damage to Garden', N'R5,000', 0, 7)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (29, 12, N'Guests belongings stolen at your home', N'R5,000', 0, 8)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (30, 12, N'Personal documents/coins/stamps - Loss of', N'R5,000', 0, 9)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (31, 12, N'Locks and Keys - Lost/damaged', N'R5,000', 0, 10)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (32, 12, N'Remote control units', N'not included', 0, 11)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (33, 12, N'Loss of water by leakage', N'not included', 0, 12)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (34, 12, N'Credit or Bank Cards - fraudulent use', N'R5,000', 0, 13)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (35, 12, N'Hole in one / bowling full house', N'R1,000', 0, 14)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (36, 12, N'Insured and Spouse death - fire or a break-in', N'R10,000', 0, 15)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (37, 12, N'Your domestics belongings - stolen following break-in', N'R5,000', 0, 16)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (38, 12, N'Medical Expenses', N'R5,000', 0, 17)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (39, 12, N'Veterinary Fees', N'R3,000', 0, 18)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (40, 12, N'Rent to live elsewhere', N'Included', 0, 19)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (41, 12, N'Storage costs for contents after damage', N'not included', 0, 20)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (42, 12, N'Belongings in a removal truck', N'Included', 0, 21)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (43, 12, N'Accidental breakage of mirrors and gals', N'R5, 000', 0, 22)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (44, 12, N'Accidental breakage of television set', N'R5, 000', 0, 23)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (45, 12, N'Fire brigade charges', N'Included', 0, 24)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (46, 12, N'Security Guards', N'R5, 000', 0, 25)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (47, 12, N'Public Liability', N'Included', 0, 26)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (48, 12, N'Tenants Liability', N'Included', 0, 27)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (49, 12, N'Liability to Domestic Employees', N'Included', 0, 28)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (50, 13, N'Included', N'yes', 0, 0)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (51, 13, N'If yes Home building definition to be checked', N'yes', 0, 1)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (52, 13, N'Thatch roof home', N'yes', 0, 2)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (53, 13, N'Swimming Pool covered', N'yes', 0, 3)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (54, 13, N'Swimming Pool equipment covered', N'Accidental damage to machinery R5,000', 0, 4)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (55, 13, N'Borehole pump/equipment', N'Accidental damage to machinery R5,000', 0, 5)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (56, 13, N'Tennis court covered', N'yes', 0, 6)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (57, 13, N'Damage to Gardens', N'no', 0, 7)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (58, 13, N'Automatic anniversary/renewal sum insured increase', N'renewal', 0, 8)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (59, 13, N'Loss of water by leakage', N'yes', 0, 9)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (60, 13, N'Rent ', N'yes', 0, 10)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (61, 13, N'Removal of fallen trees', N'yes', 0, 11)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (62, 13, N'Professional Fees', N'yes', 0, 12)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (63, 13, N'Glass and Sanitary ware', N'yes', 0, 13)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (64, 13, N'Power supply', N'yes', 0, 14)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (65, 13, N'Aerials', N'yes', 0, 15)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (66, 13, N'Fire brigade charges', N'yes', 0, 16)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (67, 13, N'Security Guards', N'yes', 0, 17)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (68, 13, N'Public Liability as a Home Owner', N'yes', 0, 18)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (69, 13, N'Liability to Domestic Employees', N'yes', 0, 19)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (70, 13, N'Subsidence, heave and landslip', N'Limited, full cover must be requested.', 0, 20)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (71, 14, N'Included', N'yes', 0, 0)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (72, 14, N'Riot and Strike outside RSA', N'No', 0, 1)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (73, 14, N'Unspecified Items as defined', N'Yes', 0, 2)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (74, 14, N'If yes ~ Cover for Jewellery, Clothing and Personal items', N'Yes', 0, 3)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (75, 14, N'If yes ~ Limit any one item', N'25% of sum insured', 0, 4)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (76, 14, N'If yes ~ Cover for money and negotiable instruments', N'must be specified', 0, 5)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (77, 14, N'Specified Items [If yes refer to list of items]', N'Yes', 0, 6)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (78, 14, N'Items in a Bank Vault', N'must be specified', 0, 7)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (79, 14, N'Bicycles', N'must be specified', 0, 8)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (80, 14, N'Wheelchairs and its accessories', N'must be specified', 0, 9)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (81, 14, N'Prescription Glasses', N'must be specified', 0, 10)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (82, 14, N'Contact Lenses', N'must be specified', 0, 11)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (83, 14, N'Cellular phones', N'must be specified', 0, 12)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (84, 14, N'Stamp Collection', N'must be specified', 0, 13)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (85, 14, N'Coin Collection', N'must be specified', 0, 14)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (86, 14, N'Transport of groceries and household goods', N'Not included', 0, 15)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (87, 14, N'All Risks cover for caravan contents', N'if requested', 0, 16)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (88, 14, N'All Risks cover for car radios', N'if requested', 0, 17)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (89, 14, N'Borehole and swimming pool equipment', N'no', 0, 18)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (90, 14, N'Items stolen from the cabin of a vehicle limit', N'no', 0, 19)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (91, 14, N'Incident Limit : Items stolen from the locked boot of a vehicle ', N'No Limit', 0, 20)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (92, 46, N'Included', N'yes', 0, 0)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (93, 46, N'Riot and Strike outside RSA', N'No', 0, 1)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (94, 46, N'Unspecified Items as defined', N'Yes', 0, 2)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (95, 46, N'If yes ~ Cover for Jewellery, Clothing and Personal items', N'Yes', 0, 3)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (96, 46, N'If yes ~ Limit any one item', N'25% of sum insured', 0, 4)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (97, 46, N'If yes ~ Cover for money and negotiable instruments', N'must be specified', 0, 5)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (98, 46, N'Specified Items [If yes refer to list of items]', N'Yes', 0, 6)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (99, 46, N'Items in a Bank Vault', N'must be specified', 0, 7)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (100, 46, N'Bicycles', N'must be specified', 0, 8)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (101, 46, N'Wheelchairs and its accessories', N'must be specified', 0, 9)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (102, 46, N'Prescription Glasses', N'must be specified', 0, 10)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (103, 46, N'Contact Lenses', N'must be specified', 0, 11)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (104, 46, N'Cellular phones', N'must be specified', 0, 12)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (105, 46, N'Stamp Collection', N'must be specified', 0, 13)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (106, 46, N'Coin Collection', N'must be specified', 0, 14)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (107, 46, N'Transport of groceries and household goods', N'Not included', 0, 15)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (108, 46, N'All Risks cover for caravan contents', N'if requested', 0, 16)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (109, 46, N'All Risks cover for car radios', N'if requested', 0, 17)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (110, 46, N'Borehole and swimming pool equipment', N'no', 0, 18)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (111, 46, N'Items stolen from the cabin of a vehicle limit', N'no', 0, 19)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (112, 46, N'Incident Limit : Items stolen from the locked boot of a vehicle ', N'No Limit', 0, 20)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (113, 67, N'Private Type Vehicle', N'', 0, 0)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (114, 67, N'Included', N'Yes', 0, 1)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (115, 67, N'Use of vehicle', N'Business or Private use', 0, 2)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (116, 67, N'Comprehensive Cover', N'If requested', 0, 3)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (117, 67, N'New for Old', N'Not included', 0, 4)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (118, 67, N'Windscreen', N'Comprehensive only', 0, 5)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (119, 67, N'Third Party Fire and theft Cover', N'If requested', 0, 6)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (120, 67, N'Third Party Cover', N'If requested', 0, 7)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (121, 67, N'Riot and Strike outside RSA', N'No', 0, 8)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (122, 67, N'Tracking device to be covered', N'Not included', 0, 9)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (123, 67, N'Towing and Storage', N'Yes - insurer supplier', 0, 10)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (124, 67, N'Sound System [not factory fitted]', N'If requested', 0, 11)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (125, 67, N'Hail Damage', N'Yes', 0, 12)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (126, 67, N'Canopy of the pick-up', N'If requested', 0, 13)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (127, 67, N'Car Hire', N'Not included', 0, 14)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (128, 67, N'Emergency Hotel Expenses', N'Yes (Gold Club benefits)', 0, 15)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (129, 67, N'Credit Shortfall ', N'Not included', 0, 16)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (130, 67, N'Medical Costs', N'Not included', 0, 17)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (131, 67, N'Third Party Liability', N'R 1,000,000', 0, 18)
GO
INSERT [dbo].[ProductBenefit] ([Id], [CoverDefinitionId], [Name], [Value], [ShowToClient], [VisibleIndex]) VALUES (132, 67, N'Third Party fire and explosion Liability', N'R 250,000', 0, 19)
GO
SET IDENTITY_INSERT [dbo].[ProductBenefit] OFF
GO
SET IDENTITY_INSERT [dbo].[QuestionDefinition] ON 

GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1, 0, N'Year Of Manufacture', 1, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (2, 0, N'Make', 1, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (3, 0, N'Model', 1, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (4, 0, N'MM Code', 1, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (5, 0, N'Vehicle Type', 1, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (6, 0, N'Class of Use', 1, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (7, 0, N'Type of Cover', 1, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (8, 0, N'Vehicle Tracking Device', 1, 87, NULL, 1, 8, 0, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (9, 0, N'Immobiliser', 1, 80, NULL, 1, 9, 0, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (10, 0, N'Gear Lock', 1, 8, NULL, 1, 10, 0, 1, 0, N'Does this vehicle have a gear lock?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (11, 0, N'Data Dot', 1, 9, NULL, 1, 11, 0, 1, 0, N'Does this vehicle have a data dot?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (12, 0, N'Sum Insured', 1, 102, NULL, 1, 12, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (13, 0, N'Registered Owner ID Number', 1, 100, NULL, 1, 13, 1, 1, 0, N'What is the ID Number for the registered owner of this vehicle?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (14, 0, N'Registered Owner', 1, 66, NULL, 1, 14, 0, 0, 0, N'Who is the registered owner of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (15, 0, N'Drivers Licence First Issued Date.', 1, 41, NULL, 1, 15, 0, 0, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (16, 0, N'Drivers Licence Type', 1, 73, NULL, 1, 16, 0, 0, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (17, 0, N'Vehicle Extras Value', 1, 107, NULL, 1, 17, 0, 1, 0, N'What is the value of the any non-factory extras?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (18, 0, N'Motor Radio Value', 1, 110, NULL, 1, 18, 0, 0, 0, N'What is the value if the vehicle has a non-factory fitted radio installed?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (19, 0, N'Tools, spare parts, travel accessories value', 1, 111, NULL, 1, 19, 0, 0, 0, N'Does the client want to specify a value any any tools, spare parts or travel accessories in the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (20, 0, N'No Claim Bonus', 1, 43, NULL, 1, 20, 0, 0, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (21, 0, N'Voluntary Excess', 1, 74, NULL, 1, 21, 0, 1, 0, N'A voluntary excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (22, 0, N'Top-up Cover', 1, 25, NULL, 1, 23, 0, 1, 0, N'Would the client want to add top-up cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (23, 0, N'4x4 Cover', 1, 30, NULL, 1, 24, 0, 1, 0, N'Would the client want to add 4x4 cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (24, 0, N'Luxury Cover', 1, 31, NULL, 1, 25, 0, 1, 0, N'Would the client want to add luxury cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (25, 0, N'Overnight Parking', 1, 85, NULL, 1, 26, 0, 0, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (26, 0, N'Marital Status', 1, 72, NULL, 1, 27, 0, 0, 0, N'What is the marital status of this client?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (27, 0, N'Suburb', 1, 89, NULL, 1, 28, 0, 0, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (28, 0, N'Risk Address', 2, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (29, 0, N'Sum Insured', 2, 102, NULL, 1, 1, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (30, 0, N'Suburb', 2, 89, NULL, 1, 2, 1, 1, 0, N'What is the suburb where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (31, 0, N'Postal Code', 2, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (32, 0, N'Residence Is', 2, 50, NULL, 1, 4, 1, 1, 0, N'What is the property where the content is located used for?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (33, 0, N'Type Of Residence', 2, 49, NULL, 1, 5, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (34, 0, N'Residence Use', 2, 51, NULL, 1, 6, 0, 0, 0, N'What is this residence used for?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (35, 0, N'Situation of Residence', 2, 52, NULL, 1, 7, 0, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (36, 0, N'Home Industry Type', 2, 59, NULL, 1, 8, 0, 1, 0, N'Does the client have a home industry they would like to add on the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (37, 0, N'Home Industry Sum Insured', 2, 112, NULL, 1, 9, 0, 1, 0, N'What is the value of the home industry they would like to add on the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (38, 0, N'Unoccupied', 2, 57, NULL, 1, 10, 0, 0, 0, N'How often is the house unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (39, 0, N'Wall Construction', 2, 55, NULL, 1, 11, 0, 1, 0, N'What is the wall construction of this property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (40, 0, N'Roof Construction', 2, 56, NULL, 1, 12, 0, 1, 0, N'What is the roof contruction of this property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (41, 0, N'Thatch Safe', 2, 16, NULL, 1, 13, 0, 1, 0, N'Does this residence have thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (42, 0, N'Security Gates', 2, 15, NULL, 1, 14, 1, 1, 0, N'Does the house where the content is located have security gates?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (43, 0, N'Burglar Bars', 2, 11, NULL, 1, 15, 1, 1, 0, N'Does the house where the content is located have burglar bars?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (44, 0, N'Burglar Bars on Passage Side', 2, 12, NULL, 1, 16, 0, 0, 0, N'Does the house where the content is located have burglar bars on the passage side?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (45, 0, N'Security Gates on Passage Side', 2, 13, NULL, 1, 17, 0, 0, 0, N'Does the house where the content is located have security gates on the passage side?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (46, 0, N'Electric Fence', 2, 18, NULL, 1, 18, 0, 0, 0, N'Does the house where the content is located have an electric fence surrounding the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (47, 0, N'Claim Free Group', 2, 43, NULL, 1, 19, 0, 1, 0, N'What is the CFG for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (48, 0, N'Voluntary Excess', 2, 74, NULL, 1, 20, 0, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (49, 0, N'Accidental Damage', 2, 76, NULL, 1, 21, 0, 1, 0, N'Does this client want to add accidental damage?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (50, 0, N'Subsidence and Landslip', 2, 36, NULL, 1, 22, 0, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (51, 0, N'Exclude Flood', 2, 23, NULL, 1, 23, 0, 1, 0, N'Does the client want to exlude flood from cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (52, 0, N'Exclude Theft', 2, 19, NULL, 1, 24, 0, 1, 0, N'Does the client want to exlude Theft from cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (53, 0, N'Limited to B&B', 2, 17, NULL, 1, 25, 0, 1, 0, N'Will the contents cover be limited to bed and breakfast cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (54, 0, N'Risk Address', 3, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (55, 0, N'ID Number', 3, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (56, 0, N'Sum Insured', 3, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (57, 0, N'Postal Code', 3, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (58, 0, N'Suburb', 3, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (59, 0, N'Type Of Cover', 3, 115, NULL, 1, 5, 1, 1, 0, N'What is the type of cover required for the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (60, 0, N'Type Of Residence', 3, 49, NULL, 1, 6, 1, 1, 0, N'What is the type of the residence of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (61, 0, N'Situation of Residence', 3, 52, NULL, 1, 7, 1, 1, 0, N'Where is this property situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (62, 0, N'Property Is', 3, 50, NULL, 1, 8, 1, 1, 0, N'What is the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (63, 0, N'Type of Property', 3, 49, NULL, 1, 9, 1, 1, 0, N'What is the type of the Property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (64, 0, N'Property Use', 3, 49, NULL, 1, 10, 1, 1, 0, N'What is the Property used for?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (65, 0, N'Roof Construction', 3, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (66, 0, N'Wall Construction', 3, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (67, 0, N'Subsidence and Landslip', 3, 36, NULL, 1, 13, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (68, 0, N'Unoccupied', 3, 57, NULL, 1, 14, 1, 1, 0, N'How often is the Property unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (69, 0, N'No Claim Bonus', 3, 43, NULL, 1, 15, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (70, 0, N'Selected Excess', 3, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (71, 0, N'Voluntary Excess', 3, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (72, 0, N'All Risk Category', 4, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (73, 0, N'Risk Address', 4, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (74, 0, N'Postal Code', 4, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (75, 0, N'Suburb', 4, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (76, 0, N'Desciption', 4, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (77, 0, N'Sum Insured', 4, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (78, 0, N'Serial/IMEI Number', 4, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (79, 0, N'Year Of Manufacture', 5, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (80, 0, N'Make', 5, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (81, 0, N'Model', 5, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (82, 0, N'MM Code', 5, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (83, 0, N'Vehicle Type', 5, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (84, 0, N'Class of Use', 5, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (85, 0, N'Type of Cover', 5, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (86, 0, N'Vehicle Tracking Device', 5, 87, NULL, 1, 8, 0, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (87, 0, N'Immobiliser', 5, 80, NULL, 1, 9, 0, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (88, 0, N'Gear Lock', 5, 8, NULL, 1, 10, 0, 1, 0, N'Does this vehicle have a gear lock?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (89, 0, N'Data Dot', 5, 9, NULL, 1, 11, 0, 1, 0, N'Does this vehicle have a data dot?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (90, 0, N'Sum Insured', 5, 102, NULL, 1, 12, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (91, 0, N'Registered Owner ID Number', 5, 100, NULL, 1, 13, 1, 1, 0, N'What is the ID Number for the registered owner of this vehicle?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (92, 0, N'Registered Owner', 5, 66, NULL, 1, 14, 0, 0, 0, N'Who is the registered owner of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (93, 0, N'Drivers Licence First Issued Date.', 5, 41, NULL, 1, 15, 0, 0, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (94, 0, N'Drivers Licence Type', 5, 73, NULL, 1, 16, 0, 0, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (95, 0, N'Vehicle Extras Value', 5, 107, NULL, 1, 17, 0, 1, 0, N'What is the value of the any non-factory extras?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (96, 0, N'Motor Radio Value', 5, 110, NULL, 1, 18, 0, 0, 0, N'What is the value if the vehicle has a non-factory fitted radio installed?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (97, 0, N'Tools, spare parts, travel accessories value', 5, 111, NULL, 1, 19, 0, 0, 0, N'Does the client want to specify a value any any tools, spare parts or travel accessories in the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (98, 0, N'No Claim Bonus', 5, 43, NULL, 1, 20, 0, 0, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (99, 0, N'Voluntary Excess', 5, 74, NULL, 1, 21, 0, 1, 0, N'A voluntary excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (100, 0, N'Top-up Cover', 5, 25, NULL, 1, 23, 0, 1, 0, N'Would the client want to add top-up cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (101, 0, N'Overnight Parking', 5, 85, NULL, 1, 24, 0, 0, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (102, 0, N'Marital Status', 5, 72, NULL, 1, 25, 0, 0, 0, N'What is the marital status of this client?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (103, 0, N'Suburb', 5, 89, NULL, 1, 26, 0, 0, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (104, 0, N'Year Of Manufacture', 6, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (105, 0, N'Make', 6, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (106, 0, N'Model', 6, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (107, 0, N'MM Code', 6, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (108, 0, N'Sum Insured', 6, 102, NULL, 1, 4, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (109, 0, N'Main Driver ID Number', 6, 104, NULL, 1, 5, 1, 1, 0, N'What is the ID Number for the main driver of this vehicle?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (110, 0, N'Marital Status', 6, 72, NULL, 1, 6, 0, 1, 0, N'What is the marital status of this client?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (111, 0, N'Drivers Licence First Issued Date', 6, 41, NULL, 1, 7, 1, 1, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (112, 0, N'Drivers Licence Type', 6, 73, NULL, 1, 8, 1, 1, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (113, 0, N'Suburb', 6, 89, NULL, 1, 9, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (114, 0, N'Postal Code', 6, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (115, 0, N'Type of Cover', 6, 53, NULL, 1, 11, 0, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (116, 0, N'Class of Use', 6, 48, NULL, 1, 12, 0, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (117, 0, N'Vehicle Tracking Device', 6, 87, NULL, 1, 13, 0, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (118, 0, N'Immobiliser', 6, 80, NULL, 1, 14, 1, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (119, 0, N'Gear Lock', 6, 8, NULL, 1, 15, 0, 0, 0, N'Does this vehicle have a gear lock?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (120, 0, N'Overnight Parking', 6, 85, NULL, 1, 16, 0, 0, 0, N'Where is this vehicle parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (121, 0, N'Extras Value', 6, 107, NULL, 1, 17, 0, 1, 0, N'What is the extras value for this vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (122, 0, N'Radio Value', 6, 110, NULL, 1, 18, 0, 1, 0, N'What is the radio value for this vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (123, 0, N'Car Hire', 6, 77, NULL, 1, 21, 0, 0, 0, N'Does the client require car hire for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (124, 0, N'No Claim Bonus', 6, 43, NULL, 1, 19, 0, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (125, 0, N'Selected Excess', 6, 74, NULL, 1, 20, 0, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (126, 0, N'Credit Shortfall', 6, 32, NULL, 1, 21, 0, 0, 0, N'Does the client require credit shortfall for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (127, 0, N'NCB Protector', 6, 33, NULL, 1, 22, 0, 0, 0, N'Does the client want the NCB Protector option for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (128, 0, N'Current Insurance Period', 6, 114, NULL, 1, 23, 0, 0, 0, N'How long has the client been at his current insurance company?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (129, 0, N'Claims in the past 12 months', 6, 44, NULL, 1, 24, 0, 0, 0, N'How many claims have the client registered in the past 12 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (130, 0, N'Claims in the past 12 to 24 months', 6, 45, NULL, 1, 25, 0, 0, 0, N'How many claims have the client registered in the past 12 to 24 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (131, 0, N'Claims in the past 24 to 36 months', 6, 46, NULL, 1, 26, 0, 0, 0, N'How many claims have the client registered in the past 24 to 36 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (132, 0, N'Pensioner', 6, 1, NULL, 1, 27, 0, 0, 0, N'Is the client a pensioner?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (133, 0, N'Year Of Manufacture', 7, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (134, 0, N'Make', 7, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (135, 0, N'Model', 7, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (136, 0, N'MM Code', 7, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (137, 0, N'Sum Insured', 7, 102, NULL, 1, 4, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (138, 0, N'Main Driver ID Number', 7, 104, NULL, 1, 5, 1, 1, 0, N'What is the ID Number for the main driver of this vehicle?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (139, 0, N'Marital Status', 7, 72, NULL, 1, 6, 0, 1, 0, N'What is the marital status of this client?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (140, 0, N'Drivers Licence First Issued Date', 7, 41, NULL, 1, 7, 1, 1, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (141, 0, N'Drivers Licence Type', 7, 73, NULL, 1, 8, 1, 1, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (142, 0, N'Suburb', 7, 89, NULL, 1, 9, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (143, 0, N'Postal Code', 7, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (144, 0, N'Type of Cover', 7, 53, NULL, 1, 11, 0, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (145, 0, N'Class of Use', 7, 48, NULL, 1, 12, 0, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (146, 0, N'Vehicle Tracking Device', 7, 87, NULL, 1, 13, 0, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (147, 0, N'Immobiliser', 7, 80, NULL, 1, 14, 1, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (148, 0, N'Gear Lock', 7, 8, NULL, 1, 15, 0, 0, 0, N'Does this vehicle have a gear lock?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (149, 0, N'Overnight Parking', 7, 85, NULL, 1, 16, 0, 0, 0, N'Where is this vehicle parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (150, 0, N'Extras Value', 7, 107, NULL, 1, 17, 0, 1, 0, N'What is the extras value for this vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (151, 0, N'Radio Value', 7, 110, NULL, 1, 18, 0, 1, 0, N'What is the radio value for this vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (152, 0, N'Car Hire', 7, 77, NULL, 1, 21, 0, 0, 0, N'Does the client require car hire for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (153, 0, N'No Claim Bonus', 7, 43, NULL, 1, 19, 0, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (154, 0, N'Selected Excess', 7, 74, NULL, 1, 20, 0, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (155, 0, N'Credit Shortfall', 7, 32, NULL, 1, 21, 0, 0, 0, N'Does the client require credit shortfall for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (156, 0, N'NCB Protector', 7, 33, NULL, 1, 22, 0, 0, 0, N'Does the client want the NCB Protector option for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (157, 0, N'Current Insurance Period', 7, 114, NULL, 1, 28, 1, 1, 0, N'How long has the client been at his current insurance company?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (158, 0, N'Claims in the past 12 months', 7, 44, NULL, 1, 24, 0, 0, 0, N'How many claims have the client registered in the past 12 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (159, 0, N'Claims in the past 12 to 24 months', 7, 45, NULL, 1, 25, 0, 0, 0, N'How many claims have the client registered in the past 12 to 24 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (160, 0, N'Claims in the past 24 to 36 months', 7, 46, NULL, 1, 26, 0, 0, 0, N'How many claims have the client registered in the past 24 to 36 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (161, 0, N'Pensioner', 7, 1, NULL, 1, 27, 0, 0, 0, N'Is the client a pensioner?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (162, 0, N'ID Number', 8, 101, NULL, 1, 0, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (163, 0, N'Type Of Cover', 8, 115, NULL, 1, 1, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (164, 0, N'Sum Insured', 8, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (165, 0, N'Postal Code', 8, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (166, 0, N'Suburb', 8, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (167, 0, N'Province', 8, 47, NULL, 1, 5, 1, 1, 0, N'In which province is the property where the content is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (168, 0, N'Burglar Alarm Type', 8, 82, NULL, 1, 6, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (169, 0, N'Burglar Bars', 8, 11, NULL, 1, 7, 1, 1, 0, N'Does the house where the content is located have burglar bars?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (170, 0, N'Security Gates', 8, 15, NULL, 1, 8, 1, 1, 0, N'Does the house where the content is located have security gates?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (171, 0, N'Type Of Residence', 8, 49, NULL, 1, 9, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (172, 0, N'Residence Is', 8, 50, NULL, 1, 10, 1, 1, 0, N'What is the property where the content is located used for?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (173, 0, N'Wall Construction', 8, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (174, 0, N'Roof Construction', 8, 56, NULL, 1, 12, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (175, 0, N'Unoccupied', 8, 57, NULL, 1, 13, 1, 1, 0, N'How often is the house unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (176, 0, N'No Claim Bonus', 8, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (177, 0, N'Accidental Damage', 8, 76, NULL, 1, 15, 1, 1, 0, N'Does the client want to include accidental damage cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (178, 0, N'Situation of Residence', 8, 52, NULL, 1, 16, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (179, 0, N'Voluntary Excess', 8, 74, NULL, 1, 18, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (180, 0, N'Exclude Theft', 8, 19, NULL, 1, 19, 1, 1, 0, N'Does the client want to exlude Theft from cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (181, 0, N'Electric Fence', 8, 18, NULL, 1, 20, 1, 1, 0, N'Does the house where the content is located have an electric fence surrounding the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (182, 0, N'Risk Address', 9, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (183, 0, N'ID Number', 9, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (184, 0, N'Sum Insured', 9, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (185, 0, N'Postal Code', 9, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (186, 0, N'Suburb', 9, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (187, 0, N'Type of Property', 9, 49, NULL, 1, 5, 1, 1, 0, N'What is the type of the Property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (188, 0, N'Roof Construction', 9, 56, NULL, 1, 6, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (189, 0, N'Wall Construction', 9, 55, NULL, 1, 7, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (190, 0, N'Selected Excess', 9, 64, NULL, 1, 9, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (191, 0, N'Voluntary Excess', 9, 74, NULL, 1, 10, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (192, 0, N'All Risk Category', 10, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (193, 0, N'Risk Address', 10, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (194, 0, N'Postal Code', 10, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (195, 0, N'Suburb', 10, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (196, 0, N'Desciption', 10, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (197, 0, N'Sum Insured', 10, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (198, 0, N'Serial/IMEI Number', 10, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (199, 0, N'Pensioner', 10, 1, NULL, 1, 7, 1, 1, 0, N'Is the insured a pensioner?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (200, 0, N'Year Of Manufacture', 11, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (201, 0, N'Make', 11, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (202, 0, N'Model', 11, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (203, 0, N'MM Code', 11, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (204, 0, N'Sum Insured', 11, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (205, 0, N'Drivers Licence First Issued Date', 11, 41, NULL, 1, 6, 1, 1, 0, N'When was the first date of issue of the drivers licence for the main driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (206, 0, N'Type of Cover', 11, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (207, 0, N'Class of Use', 11, 48, NULL, 1, 8, 0, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (208, 0, N'Postal Code', 11, 90, NULL, 1, 9, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (209, 0, N'Postal Code (Work)', 11, 91, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked during working hours?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (210, 0, N'Main Driver ID Number', 11, 104, NULL, 1, 11, 1, 1, 0, N'What is the ID Number for the main driver of this vehicle?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (211, 0, N'Marital Status', 11, 72, NULL, 1, 12, 0, 1, 0, N'What is the marital status of this client?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (212, 0, N'Gender', 11, 70, NULL, 1, 13, 0, 1, 0, N'What is the gender of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (213, 0, N'Tracking Device', 11, 87, NULL, 1, 14, 0, 1, 0, N'Is there a tracking device installed in this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (214, 0, N'Voluntary Excess', 11, 74, NULL, 1, 15, 0, 1, 0, N'Does the client want to pay a voluntary excess that will lower premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (215, 0, N'Overnight Parking', 11, 85, NULL, 1, 16, 0, 0, 0, N'Where will this vehicle be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (216, 0, N'Basic Excess Waiver', 11, 24, NULL, 1, 17, 0, 0, 0, N'Does the client want to waive his excess?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (217, 0, N'Sum Insured', 12, 102, NULL, 1, 0, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (218, 0, N'Postal Code', 12, 90, NULL, 1, 1, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (219, 0, N'Burglar Alarm Type', 12, 82, NULL, 1, 2, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (220, 0, N'Burglar Bars', 12, 11, NULL, 1, 3, 1, 1, 0, N'Does the house where the content is located have burglar bars?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (221, 0, N'Secure Complex', 12, 14, NULL, 1, 4, 1, 1, 0, N'Is the house where the content is located in a security complex?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (222, 0, N'Electric Fencing', 12, 18, NULL, 1, 5, 1, 1, 0, N'Does the house where the content is located have an electric fence?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (223, 0, N'Residence Is', 12, 50, NULL, 1, 6, 1, 1, 0, N'What is the property where the content is located used for?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (224, 0, N'Insured Age', 12, 93, NULL, 1, 7, 1, 1, 0, N'What is the age of the person living in the house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (225, 0, N'Exclude Burglary', 12, 19, NULL, 1, 8, 1, 1, 0, N'Does the client want to exclude burglary for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (226, 0, N'Subsidence & Landslip', 12, 36, NULL, 1, 9, 1, 1, 0, N'Does the client want to include subsidence & landslip for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (227, 0, N'Wall Construction', 12, 55, NULL, 1, 10, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (228, 0, N'Roof Construction', 12, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (229, 0, N'Unoccupied', 12, 57, NULL, 1, 12, 1, 1, 0, N'When is the house where the contents is unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (230, 0, N'Claims Free Group', 12, 43, NULL, 1, 13, 0, 1, 0, N'What is the value for CFG for this client?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (231, 0, N'Voluntary Excess', 12, 74, NULL, 1, 14, 0, 1, 0, N'Does the client want to pay a voluntary excess that will lower premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (232, 0, N'Accidental Damage', 12, 76, NULL, 1, 15, 0, 1, 0, N'Does the client want accidental damage for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (233, 0, N'Type Of Cover', 12, 115, NULL, 1, 16, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (234, 0, N'Risk Address', 13, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (235, 0, N'ID Number', 13, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (236, 0, N'Sum Insured', 13, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (237, 0, N'Postal Code', 13, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (238, 0, N'Suburb', 13, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (239, 0, N'Situation of Residence', 13, 52, NULL, 1, 7, 1, 1, 0, N'Where is this property situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (240, 0, N'Property Is', 13, 50, NULL, 1, 8, 1, 1, 0, N'What is the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (241, 0, N'Type of Property', 13, 49, NULL, 1, 9, 1, 1, 0, N'What is the type of the Property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (242, 0, N'Property Use', 13, 49, NULL, 1, 10, 1, 1, 0, N'What is the Property used for?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (243, 0, N'Roof Construction', 13, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (244, 0, N'Wall Construction', 13, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (245, 0, N'Subsidence and Landslip', 13, 36, NULL, 1, 13, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (246, 0, N'Unoccupied', 13, 57, NULL, 1, 14, 1, 1, 0, N'How often is the Property unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (247, 0, N'No Claim Bonus', 13, 43, NULL, 1, 15, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (248, 0, N'Selected Excess', 13, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (249, 0, N'Voluntary Excess', 13, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (250, 0, N'All Risk Category', 14, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (251, 0, N'Risk Address', 14, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (252, 0, N'Postal Code', 14, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (253, 0, N'Suburb', 14, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (254, 0, N'Desciption', 14, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (255, 0, N'Sum Insured', 14, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (256, 0, N'Serial/IMEI Number', 14, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (257, 0, N'Year Of Manufacture', 15, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (258, 0, N'Make', 15, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (259, 0, N'Model', 15, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (260, 0, N'MM Code', 15, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (261, 0, N'Vehicle Type', 15, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (262, 0, N'Class of Use', 15, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (263, 0, N'Type of Cover', 15, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (264, 0, N'Vehicle Tracking Device', 15, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (265, 0, N'Overnight Parking', 15, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (266, 0, N'Postal Code', 15, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (267, 0, N'Suburb', 15, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (268, 0, N'Main Driver Date of Birth', 15, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (269, 0, N'Main Driver Gender', 15, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (270, 0, N'Main Driver Marital Status', 15, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (271, 0, N'No Claim Bonus', 15, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (272, 0, N'Risk Address', 16, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (273, 0, N'ID Number', 16, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (274, 0, N'Type Of Cover', 16, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (275, 0, N'Sum Insured', 16, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (276, 0, N'Postal Code', 16, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (277, 0, N'Suburb', 16, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (278, 0, N'Situation of Residence', 16, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (279, 0, N'Selected Excess', 16, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (280, 0, N'Voluntary Excess', 16, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (281, 0, N'Burglar Alarm Type', 16, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (282, 0, N'Roof Construction', 16, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (283, 0, N'Wall Construction', 16, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (284, 0, N'Type Of Residence', 16, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (285, 0, N'Occupation Date', 16, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (286, 0, N'No Claim Bonus', 16, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (287, 0, N'Subsidence and Landslip', 16, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (288, 0, N'Thatch Safe', 16, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (289, 0, N'Guest House', 16, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (290, 0, N'Retired', 16, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (291, 0, N'Risk Address', 17, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (292, 0, N'ID Number', 17, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (293, 0, N'Sum Insured', 17, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (294, 0, N'Postal Code', 17, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (295, 0, N'Suburb', 17, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (296, 0, N'Roof Construction', 17, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (297, 0, N'Wall Construction', 17, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (298, 0, N'Selected Excess', 17, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (299, 0, N'Voluntary Excess', 17, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (300, 0, N'Retired', 17, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (301, 0, N'All Risk Category', 18, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (302, 0, N'Risk Address', 18, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (303, 0, N'Postal Code', 18, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (304, 0, N'Suburb', 18, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (305, 0, N'Desciption', 18, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (306, 0, N'Sum Insured', 18, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (307, 0, N'Serial/IMEI Number', 18, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (308, 0, N'Retired', 18, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (309, 0, N'Year Of Manufacture', 19, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (310, 0, N'Make', 19, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (311, 0, N'Model', 19, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (312, 0, N'MM Code', 19, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (313, 0, N'Vehicle Type', 19, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (314, 0, N'Class of Use', 19, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (315, 0, N'Type of Cover', 19, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (316, 0, N'Vehicle Tracking Device', 19, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (317, 0, N'Overnight Parking', 19, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (318, 0, N'Postal Code', 19, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (319, 0, N'Suburb', 19, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (320, 0, N'Main Driver Date of Birth', 19, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (321, 0, N'Main Driver Gender', 19, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (322, 0, N'Main Driver Marital Status', 19, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (323, 0, N'No Claim Bonus', 19, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (324, 0, N'Risk Address', 20, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (325, 0, N'ID Number', 20, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (326, 0, N'Type Of Cover', 20, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (327, 0, N'Sum Insured', 20, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (328, 0, N'Postal Code', 20, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (329, 0, N'Suburb', 20, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (330, 0, N'Situation of Residence', 20, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (331, 0, N'Selected Excess', 20, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (332, 0, N'Voluntary Excess', 20, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (333, 0, N'Burglar Alarm Type', 20, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (334, 0, N'Roof Construction', 20, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (335, 0, N'Wall Construction', 20, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (336, 0, N'Type Of Residence', 20, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (337, 0, N'Occupation Date', 20, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (338, 0, N'No Claim Bonus', 20, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (339, 0, N'Subsidence and Landslip', 20, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (340, 0, N'Thatch Safe', 20, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (341, 0, N'Guest House', 20, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (342, 0, N'Retired', 20, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (343, 0, N'Risk Address', 21, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (344, 0, N'ID Number', 21, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (345, 0, N'Sum Insured', 21, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (346, 0, N'Postal Code', 21, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (347, 0, N'Suburb', 21, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (348, 0, N'Roof Construction', 21, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (349, 0, N'Wall Construction', 21, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (350, 0, N'Selected Excess', 21, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (351, 0, N'Voluntary Excess', 21, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (352, 0, N'Retired', 21, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (353, 0, N'All Risk Category', 22, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (354, 0, N'Risk Address', 22, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (355, 0, N'Postal Code', 22, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (356, 0, N'Suburb', 22, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (357, 0, N'Desciption', 22, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (358, 0, N'Sum Insured', 22, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (359, 0, N'Serial/IMEI Number', 22, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (360, 0, N'Retired', 22, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (361, 0, N'Year Of Manufacture', 23, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (362, 0, N'Make', 23, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (363, 0, N'Model', 23, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (364, 0, N'MM Code', 23, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (365, 0, N'Vehicle Type', 23, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (366, 0, N'Class of Use', 23, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (367, 0, N'Type of Cover', 23, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (368, 0, N'Vehicle Tracking Device', 23, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (369, 0, N'Overnight Parking', 23, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (370, 0, N'Postal Code', 23, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (371, 0, N'Suburb', 23, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (372, 0, N'Main Driver Date of Birth', 23, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (373, 0, N'Main Driver Gender', 23, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (374, 0, N'Main Driver Marital Status', 23, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (375, 0, N'No Claim Bonus', 23, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (376, 0, N'Risk Address', 24, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (377, 0, N'ID Number', 24, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (378, 0, N'Type Of Cover', 24, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (379, 0, N'Sum Insured', 24, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (380, 0, N'Postal Code', 24, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (381, 0, N'Suburb', 24, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (382, 0, N'Situation of Residence', 24, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (383, 0, N'Selected Excess', 24, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (384, 0, N'Voluntary Excess', 24, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (385, 0, N'Burglar Alarm Type', 24, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (386, 0, N'Roof Construction', 24, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (387, 0, N'Wall Construction', 24, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (388, 0, N'Type Of Residence', 24, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (389, 0, N'Occupation Date', 24, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (390, 0, N'No Claim Bonus', 24, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (391, 0, N'Subsidence and Landslip', 24, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (392, 0, N'Thatch Safe', 24, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (393, 0, N'Guest House', 24, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (394, 0, N'Retired', 24, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (395, 0, N'Risk Address', 25, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (396, 0, N'ID Number', 25, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (397, 0, N'Sum Insured', 25, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (398, 0, N'Postal Code', 25, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (399, 0, N'Suburb', 25, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (400, 0, N'Roof Construction', 25, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (401, 0, N'Wall Construction', 25, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (402, 0, N'Selected Excess', 25, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (403, 0, N'Voluntary Excess', 25, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (404, 0, N'Retired', 25, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (405, 0, N'All Risk Category', 26, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (406, 0, N'Risk Address', 26, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (407, 0, N'Postal Code', 26, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (408, 0, N'Suburb', 26, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (409, 0, N'Desciption', 26, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (410, 0, N'Sum Insured', 26, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (411, 0, N'Serial/IMEI Number', 26, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (412, 0, N'Retired', 26, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (413, 0, N'Year Of Manufacture', 27, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (414, 0, N'Make', 27, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (415, 0, N'Model', 27, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (416, 0, N'MM Code', 27, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (417, 0, N'Vehicle Type', 27, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (418, 0, N'Class of Use', 27, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (419, 0, N'Type of Cover', 27, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (420, 0, N'Vehicle Tracking Device', 27, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (421, 0, N'Overnight Parking', 27, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (422, 0, N'Postal Code', 27, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (423, 0, N'Suburb', 27, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (424, 0, N'Main Driver Date of Birth', 27, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (425, 0, N'Main Driver Gender', 27, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (426, 0, N'Main Driver Marital Status', 27, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (427, 0, N'No Claim Bonus', 27, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (428, 0, N'Risk Address', 28, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (429, 0, N'ID Number', 28, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (430, 0, N'Type Of Cover', 28, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (431, 0, N'Sum Insured', 28, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (432, 0, N'Postal Code', 28, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (433, 0, N'Suburb', 28, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (434, 0, N'Situation of Residence', 28, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (435, 0, N'Selected Excess', 28, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (436, 0, N'Voluntary Excess', 28, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (437, 0, N'Burglar Alarm Type', 28, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (438, 0, N'Roof Construction', 28, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (439, 0, N'Wall Construction', 28, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (440, 0, N'Type Of Residence', 28, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (441, 0, N'Occupation Date', 28, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (442, 0, N'No Claim Bonus', 28, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (443, 0, N'Subsidence and Landslip', 28, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (444, 0, N'Thatch Safe', 28, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (445, 0, N'Guest House', 28, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (446, 0, N'Retired', 28, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (447, 0, N'Risk Address', 29, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (448, 0, N'ID Number', 29, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (449, 0, N'Sum Insured', 29, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (450, 0, N'Postal Code', 29, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (451, 0, N'Suburb', 29, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (452, 0, N'Roof Construction', 29, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (453, 0, N'Wall Construction', 29, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (454, 0, N'Selected Excess', 29, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (455, 0, N'Voluntary Excess', 29, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (456, 0, N'Retired', 29, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (457, 0, N'All Risk Category', 30, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (458, 0, N'Risk Address', 30, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (459, 0, N'Postal Code', 30, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (460, 0, N'Suburb', 30, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (461, 0, N'Desciption', 30, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (462, 0, N'Sum Insured', 30, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (463, 0, N'Serial/IMEI Number', 30, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (464, 0, N'Retired', 30, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (465, 0, N'Year Of Manufacture', 31, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (466, 0, N'Make', 31, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (467, 0, N'Model', 31, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (468, 0, N'MM Code', 31, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (469, 0, N'Vehicle Type', 31, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (470, 0, N'Class of Use', 31, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (471, 0, N'Type of Cover', 31, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (472, 0, N'Vehicle Tracking Device', 31, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (473, 0, N'Overnight Parking', 31, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (474, 0, N'Postal Code', 31, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (475, 0, N'Suburb', 31, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (476, 0, N'Main Driver Date of Birth', 31, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (477, 0, N'Main Driver Gender', 31, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (478, 0, N'Main Driver Marital Status', 31, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (479, 0, N'No Claim Bonus', 31, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (480, 0, N'Risk Address', 32, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (481, 0, N'ID Number', 32, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (482, 0, N'Type Of Cover', 32, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (483, 0, N'Sum Insured', 32, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (484, 0, N'Postal Code', 32, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (485, 0, N'Suburb', 32, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (486, 0, N'Situation of Residence', 32, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (487, 0, N'Selected Excess', 32, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (488, 0, N'Voluntary Excess', 32, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (489, 0, N'Burglar Alarm Type', 32, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (490, 0, N'Roof Construction', 32, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (491, 0, N'Wall Construction', 32, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (492, 0, N'Type Of Residence', 32, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (493, 0, N'Occupation Date', 32, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (494, 0, N'No Claim Bonus', 32, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (495, 0, N'Subsidence and Landslip', 32, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (496, 0, N'Thatch Safe', 32, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (497, 0, N'Guest House', 32, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (498, 0, N'Retired', 32, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (499, 0, N'Risk Address', 33, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (500, 0, N'ID Number', 33, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (501, 0, N'Sum Insured', 33, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (502, 0, N'Postal Code', 33, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (503, 0, N'Suburb', 33, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (504, 0, N'Roof Construction', 33, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (505, 0, N'Wall Construction', 33, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (506, 0, N'Selected Excess', 33, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (507, 0, N'Voluntary Excess', 33, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (508, 0, N'Retired', 33, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (509, 0, N'All Risk Category', 34, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (510, 0, N'Risk Address', 34, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (511, 0, N'Postal Code', 34, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (512, 0, N'Suburb', 34, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (513, 0, N'Desciption', 34, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (514, 0, N'Sum Insured', 34, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (515, 0, N'Serial/IMEI Number', 34, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (516, 0, N'Retired', 34, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (517, 0, N'Year Of Manufacture', 35, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (518, 0, N'Make', 35, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (519, 0, N'Model', 35, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (520, 0, N'MM Code', 35, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (521, 0, N'Vehicle Type', 35, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (522, 0, N'Class of Use', 35, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (523, 0, N'Type of Cover', 35, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (524, 0, N'Vehicle Tracking Device', 35, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (525, 0, N'Overnight Parking', 35, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (526, 0, N'Postal Code', 35, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (527, 0, N'Suburb', 35, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (528, 0, N'Main Driver Date of Birth', 35, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (529, 0, N'Main Driver Gender', 35, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (530, 0, N'Main Driver Marital Status', 35, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (531, 0, N'No Claim Bonus', 35, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (532, 0, N'Risk Address', 36, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (533, 0, N'ID Number', 36, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (534, 0, N'Type Of Cover', 36, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (535, 0, N'Sum Insured', 36, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (536, 0, N'Postal Code', 36, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (537, 0, N'Suburb', 36, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (538, 0, N'Situation of Residence', 36, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (539, 0, N'Selected Excess', 36, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (540, 0, N'Voluntary Excess', 36, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (541, 0, N'Burglar Alarm Type', 36, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (542, 0, N'Roof Construction', 36, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (543, 0, N'Wall Construction', 36, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (544, 0, N'Type Of Residence', 36, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (545, 0, N'Occupation Date', 36, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (546, 0, N'No Claim Bonus', 36, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (547, 0, N'Subsidence and Landslip', 36, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (548, 0, N'Thatch Safe', 36, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (549, 0, N'Guest House', 36, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (550, 0, N'Retired', 36, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (551, 0, N'Risk Address', 37, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (552, 0, N'ID Number', 37, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (553, 0, N'Sum Insured', 37, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (554, 0, N'Postal Code', 37, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (555, 0, N'Suburb', 37, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (556, 0, N'Roof Construction', 37, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (557, 0, N'Wall Construction', 37, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (558, 0, N'Selected Excess', 37, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (559, 0, N'Voluntary Excess', 37, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (560, 0, N'Retired', 37, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (561, 0, N'All Risk Category', 38, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (562, 0, N'Risk Address', 38, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (563, 0, N'Postal Code', 38, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (564, 0, N'Suburb', 38, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (565, 0, N'Desciption', 38, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (566, 0, N'Sum Insured', 38, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (567, 0, N'Serial/IMEI Number', 38, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (568, 0, N'Retired', 38, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (569, 0, N'Year Of Manufacture', 39, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (570, 0, N'Make', 39, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (571, 0, N'Model', 39, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (572, 0, N'MM Code', 39, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (573, 0, N'Vehicle Type', 39, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (574, 0, N'Class of Use', 39, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (575, 0, N'Type of Cover', 39, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (576, 0, N'Vehicle Tracking Device', 39, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (577, 0, N'Overnight Parking', 39, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (578, 0, N'Postal Code', 39, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (579, 0, N'Suburb', 39, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (580, 0, N'Main Driver Date of Birth', 39, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (581, 0, N'Main Driver Gender', 39, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (582, 0, N'Main Driver Marital Status', 39, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (583, 0, N'No Claim Bonus', 39, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (584, 0, N'Risk Address', 40, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (585, 0, N'ID Number', 40, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (586, 0, N'Type Of Cover', 40, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (587, 0, N'Sum Insured', 40, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (588, 0, N'Postal Code', 40, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (589, 0, N'Suburb', 40, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (590, 0, N'Situation of Residence', 40, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (591, 0, N'Selected Excess', 40, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (592, 0, N'Voluntary Excess', 40, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (593, 0, N'Burglar Alarm Type', 40, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (594, 0, N'Roof Construction', 40, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (595, 0, N'Wall Construction', 40, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (596, 0, N'Type Of Residence', 40, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (597, 0, N'Occupation Date', 40, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (598, 0, N'No Claim Bonus', 40, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (599, 0, N'Subsidence and Landslip', 40, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (600, 0, N'Thatch Safe', 40, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (601, 0, N'Guest House', 40, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (602, 0, N'Retired', 40, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (603, 0, N'Risk Address', 41, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (604, 0, N'ID Number', 41, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (605, 0, N'Sum Insured', 41, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (606, 0, N'Postal Code', 41, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (607, 0, N'Suburb', 41, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (608, 0, N'Roof Construction', 41, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (609, 0, N'Wall Construction', 41, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (610, 0, N'Selected Excess', 41, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (611, 0, N'Voluntary Excess', 41, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (612, 0, N'Retired', 41, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (613, 0, N'All Risk Category', 42, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (614, 0, N'Risk Address', 42, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (615, 0, N'Postal Code', 42, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (616, 0, N'Suburb', 42, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (617, 0, N'Desciption', 42, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (618, 0, N'Sum Insured', 42, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (619, 0, N'Serial/IMEI Number', 42, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (620, 0, N'Retired', 42, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (621, 0, N'Year Of Manufacture', 43, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (622, 0, N'Make', 43, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (623, 0, N'Model', 43, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (624, 0, N'MM Code', 43, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (625, 0, N'Vehicle Registration Number', 43, 97, NULL, 1, 5, 1, 1, 0, N'What is the registration number for this vehicle?', N'', N'^.{0,10}$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (626, 0, N'Class of Use', 43, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (627, 0, N'Type of Cover', 43, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (628, 0, N'Postal Code', 43, 90, NULL, 1, 8, 1, 1, 0, N'What is the postal code where this vehicle will be parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (629, 0, N'Suburb', 43, 89, NULL, 1, 9, 1, 1, 0, N'What is the suburb name where this vehicle will be kept overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (630, 0, N'Province', 43, 47, NULL, 1, 10, 1, 1, 0, N'In which province is this vehicle mainly operated in?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (631, 0, N'Immobiliser', 43, 80, NULL, 1, 11, 1, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (632, 0, N'Gear Lock', 43, 8, NULL, 1, 12, 1, 1, 0, N'Does this vehicle have a gear lock?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (633, 0, N'Data Dot', 43, 9, NULL, 1, 13, 1, 1, 0, N'Does this vehicle have a Data Dot?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (634, 0, N'Tracking Device', 43, 87, NULL, 1, 14, 1, 1, 0, N'Is there a tracking device installed in this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (635, 0, N'Modified/Imported', 43, 7, NULL, 1, 15, 1, 1, 0, N'Has this vehicle been modified or imported?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (636, 0, N'Overnight Parking', 43, 85, NULL, 1, 16, 1, 1, 0, N'Where is this vehicle parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (637, 0, N'Daytime Parking', 43, 86, NULL, 1, 17, 1, 1, 0, N'Where is this vehicle parked during the day?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (638, 0, N'Voluntary Excess', 43, 74, NULL, 1, 18, 1, 1, 0, N'Does the client want to pay a voluntary excess that will lower premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (639, 0, N'Drivers Licence Type', 43, 73, NULL, 1, 19, 1, 1, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (640, 0, N'Drivers Licence First Issued Date', 43, 41, NULL, 1, 20, 1, 1, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (641, 0, N'Main Driver Date of Birth', 43, 42, NULL, 1, 21, 1, 1, 0, N'What is the Date of Birth of the main driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (642, 0, N'Main Driver Gender', 43, 70, NULL, 1, 22, 1, 1, 0, N'What is the Gender of the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (643, 0, N'Main Driver Marital Status', 43, 72, NULL, 1, 23, 1, 1, 0, N'What is the Marital Status of the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (644, 0, N'Nominated Driver', 43, 37, NULL, 1, 24, 1, 1, 0, N'Does the client want to insure only the nominated driver?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (645, 0, N'Previously Insured', 43, 39, NULL, 1, 25, 1, 1, 0, N'Has this vehicle been insured before?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (646, 0, N'Sum Insured', 43, 102, NULL, 1, 26, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (647, 0, N'Finance House', 43, 103, NULL, 1, 27, 0, 1, 0, N'At which institution is the vehicle financed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (648, 0, N'No Claim Bonus', 43, 43, NULL, 1, 28, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (649, 0, N'Claims in the past 12 months', 43, 44, NULL, 1, 29, 1, 1, 0, N'How many claims have the client registered in the past 12 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (650, 0, N'Claims in the past 12 to 24 months', 43, 45, NULL, 1, 30, 1, 1, 0, N'How many claims have the client registered in the past 12 to 24 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (651, 0, N'Claims in the past 24 to 36 months', 43, 46, NULL, 1, 31, 1, 1, 0, N'How many claims have the client registered in the past 24 to 36 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (652, 0, N'Sum Insured', 44, 102, NULL, 1, 0, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (653, 0, N'Postal Code', 44, 90, NULL, 1, 1, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (654, 0, N'Suburb', 44, 89, NULL, 1, 2, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (655, 0, N'Province', 44, 47, NULL, 1, 3, 1, 1, 0, N'In which province is the property where the content is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (656, 0, N'Burglar Alarm Type', 44, 82, NULL, 1, 4, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (657, 0, N'Burglar Bars', 44, 11, NULL, 1, 5, 1, 1, 0, N'Does the house where the content is located have burglar bars?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (658, 0, N'Security Gates', 44, 15, NULL, 1, 6, 1, 1, 0, N'Does the house where the content is located have security gates?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (659, 0, N'Type Of Residence', 44, 49, NULL, 1, 7, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (660, 0, N'Subsidence & Landslip', 44, 36, NULL, 1, 8, 1, 1, 0, N'Does the client want to include subsidence & landslip for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (661, 0, N'Wall Construction', 44, 55, NULL, 1, 9, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (662, 0, N'Roof Construction', 44, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (663, 0, N'Roof Gradient', 44, 54, NULL, 1, 11, 1, 1, 0, N'What is the gradient of the roof?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (664, 0, N'Unoccupied', 44, 57, NULL, 1, 12, 1, 1, 0, N'How often is the house unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (665, 0, N'No Claim Bonus', 44, 43, NULL, 1, 13, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (666, 0, N'Accidental Damage', 44, 76, NULL, 1, 14, 1, 1, 0, N'Does the client want to include accidental damage cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (667, 0, N'Residence Is', 44, 50, NULL, 1, 15, 1, 1, 0, N'What is the property where the content is located used for?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (668, 0, N'Situation of Residence', 44, 52, NULL, 1, 16, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (669, 0, N'Occupation Date', 44, 40, NULL, 1, 17, 1, 1, 0, N'When did the client occupy this property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (670, 0, N'Borehole', 44, 3, NULL, 1, 18, 0, 1, 0, N'Does the property where the content is located have a borehole?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (671, 0, N'Near River', 44, 5, NULL, 1, 19, 0, 1, 0, N'Is the property where the content is located near a river?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (672, 0, N'Claims in the past 12 months', 44, 44, NULL, 1, 20, 1, 1, 0, N'How many claims have the client registered in the past 12 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (673, 0, N'Claims in the past 12 to 24 months', 44, 45, NULL, 1, 21, 1, 1, 0, N'How many claims have the client registered in the past 12 to 24 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (674, 0, N'Claims in the past 24 to 36 months', 44, 46, NULL, 1, 22, 1, 1, 0, N'How many claims have the client registered in the past 24 to 36 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (675, 0, N'Type Of Cover', 44, 115, NULL, 1, 23, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (676, 0, N'Risk Address', 45, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (677, 0, N'ID Number', 45, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (678, 0, N'Sum Insured', 45, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (679, 0, N'Postal Code', 45, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (680, 0, N'Suburb', 45, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (681, 0, N'Province', 45, 47, NULL, 1, 5, 1, 1, 0, N'In which province is the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (682, 0, N'Type Of Residence', 45, 49, NULL, 1, 6, 1, 1, 0, N'What is the type of the residence of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (683, 0, N'Situation of Residence', 45, 52, NULL, 1, 7, 1, 1, 0, N'Where is this property situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (684, 0, N'Property Is', 45, 50, NULL, 1, 8, 1, 1, 0, N'What is the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (685, 0, N'Roof Construction', 45, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (686, 0, N'Wall Construction', 45, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (687, 0, N'Subsidence and Landslip', 45, 36, NULL, 1, 13, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (688, 0, N'Unoccupied', 45, 57, NULL, 1, 14, 1, 1, 0, N'How often is the Property unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (689, 0, N'No Claim Bonus', 45, 43, NULL, 1, 15, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (690, 0, N'Selected Excess', 45, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (691, 0, N'Voluntary Excess', 45, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (692, 0, N'All Risk Category', 46, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (693, 0, N'Risk Address', 46, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (694, 0, N'Postal Code', 46, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (695, 0, N'Suburb', 46, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (696, 0, N'Desciption', 46, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (697, 0, N'Sum Insured', 46, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (698, 0, N'Serial/IMEI Number', 46, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (699, 0, N'Year Of Manufacture', 47, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (700, 0, N'Make', 47, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (701, 0, N'Model', 47, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (702, 0, N'MM Code', 47, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (703, 0, N'Vehicle Registration Number', 47, 97, NULL, 1, 5, 1, 1, 0, N'What is the registration number for this vehicle?', N'', N'^.{0,10}$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (704, 0, N'Class of Use', 47, 48, NULL, 1, 6, 0, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (705, 0, N'Vehicle Type', 47, 69, NULL, 1, 7, 0, 0, 0, N'What type of cehicle is this?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (706, 0, N'Type of Cover', 47, 53, NULL, 1, 8, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (707, 0, N'Postal Code', 47, 90, NULL, 1, 9, 1, 1, 0, N'What is the postal code where this vehicle will be parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (708, 0, N'Immobiliser', 47, 80, NULL, 1, 10, 0, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (709, 0, N'Tracking Device', 47, 87, NULL, 1, 11, 0, 0, 0, N'Is there a tracking device installed in this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (710, 0, N'Overnight Parking', 47, 85, NULL, 1, 12, 0, 0, 0, N'Where is this vehicle parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (711, 0, N'Daytime Parking', 47, 86, NULL, 1, 13, 0, 0, 0, N'Where is this vehicle parked during the day?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (712, 0, N'Voluntary Excess', 47, 74, NULL, 1, 14, 0, 1, 0, N'Does the client want to pay a voluntary excess that will lower premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (713, 0, N'Drivers Licence Type', 47, 73, NULL, 1, 15, 1, 0, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (714, 0, N'Drivers Licence First Issued Date', 47, 41, NULL, 1, 16, 1, 0, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (715, 0, N'Main Driver Date of Birth', 47, 42, NULL, 1, 17, 1, 1, 0, N'What is the Date of Birth of the main driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (716, 0, N'Main Driver Gender', 47, 70, NULL, 1, 18, 0, 0, 0, N'What is the Gender of the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (717, 0, N'Sum Insured', 47, 102, NULL, 1, 19, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (718, 0, N'No Claim Bonus', 47, 43, NULL, 1, 20, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (719, 0, N'Excess Buster', 47, 24, NULL, 1, 21, 0, 1, 0, N'Does the client want to waive the excess?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (720, 0, N'Extras Value', 47, 107, NULL, 1, 22, 0, 0, 0, N'What is the value of the extras on the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (721, 0, N'Advanced Driver', 47, 38, NULL, 1, 23, 0, 1, 0, N'Does the driver of this vehicle have an advanced driver certificate?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (722, 0, N'Car Hire', 47, 77, NULL, 1, 24, 0, 0, 0, N'Does the client require car hire?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (723, 0, N'ID Number', 48, 101, NULL, 1, 0, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (724, 0, N'Type Of Cover', 48, 115, NULL, 1, 1, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (725, 0, N'Sum Insured', 48, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (726, 0, N'Postal Code', 48, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (727, 0, N'Suburb', 48, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (728, 0, N'Province', 48, 47, NULL, 1, 5, 1, 1, 0, N'In which province is the property where the content is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (729, 0, N'Burglar Alarm Type', 48, 82, NULL, 1, 6, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (730, 0, N'Burglar Bars', 48, 11, NULL, 1, 7, 1, 1, 0, N'Does the house where the content is located have burglar bars?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (731, 0, N'Security Gates', 48, 15, NULL, 1, 8, 1, 1, 0, N'Does the house where the content is located have security gates?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (732, 0, N'Type Of Residence', 48, 49, NULL, 1, 9, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (733, 0, N'Subsidence & Landslip', 48, 36, NULL, 1, 10, 1, 1, 0, N'Does the client want to include subsidence & landslip for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (734, 0, N'Wall Construction', 48, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (735, 0, N'Roof Construction', 48, 56, NULL, 1, 12, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (736, 0, N'Roof Gradient', 48, 54, NULL, 1, 13, 1, 1, 0, N'What is the roof gradient of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (737, 0, N'Unoccupied', 48, 57, NULL, 1, 14, 1, 1, 0, N'How often is the house unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (738, 0, N'No Claim Bonus', 48, 43, NULL, 1, 15, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (739, 0, N'Accidental Damage', 48, 76, NULL, 1, 16, 1, 1, 0, N'Does the client want to include accidental damage cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (740, 0, N'Situation of Residence', 48, 52, NULL, 1, 17, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (741, 0, N'Near River', 48, 5, NULL, 1, 18, 0, 1, 0, N'Is the property where the content is located near a river?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (742, 0, N'Risk Address', 49, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (743, 0, N'ID Number', 49, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (744, 0, N'Sum Insured', 49, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (745, 0, N'Postal Code', 49, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (746, 0, N'Suburb', 49, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (747, 0, N'Roof Construction', 49, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (748, 0, N'Wall Construction', 49, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (749, 0, N'Selected Excess', 49, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (750, 0, N'Voluntary Excess', 49, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (751, 0, N'All Risk Category', 50, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (752, 0, N'Risk Address', 50, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (753, 0, N'Postal Code', 50, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (754, 0, N'Suburb', 50, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (755, 0, N'Desciption', 50, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (756, 0, N'Sum Insured', 50, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (757, 0, N'Serial/IMEI Number', 50, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/IMEI number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (758, 0, N'Year Of Manufacture', 51, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (759, 0, N'Make', 51, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (760, 0, N'Model', 51, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (761, 0, N'MM Code', 51, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (762, 0, N'Vehicle Registration Number', 51, 97, NULL, 1, 5, 1, 1, 0, N'What is the registration number for this vehicle?', N'', N'^.{0,10}$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (763, 0, N'Type of Cover', 51, 53, NULL, 1, 6, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (764, 0, N'Suburb', 51, 89, NULL, 1, 7, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (765, 0, N'Postal Code', 51, 90, NULL, 1, 8, 1, 1, 0, N'What is the postal code where this vehicle will be parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (766, 0, N'Class of Use', 51, 48, NULL, 1, 9, 0, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (767, 0, N'Immobiliser', 51, 80, NULL, 1, 11, 0, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (768, 0, N'Tracking Device', 51, 87, NULL, 1, 12, 0, 0, 0, N'Is there a tracking device installed in this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (769, 0, N'Drivers Licence Type', 51, 73, NULL, 1, 13, 1, 0, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (770, 0, N'Drivers Licence First Issued Date', 51, 41, NULL, 1, 14, 1, 0, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (771, 0, N'Main Driver ID Number', 51, 104, NULL, 1, 15, 1, 1, 0, N'What is the ID Number of the main driver of this vehicle?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (772, 0, N'Main Driver Date of Birth', 51, 42, NULL, 1, 16, 1, 1, 0, N'What is the Date of Birth of the main driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (773, 0, N'Main Driver Gender', 51, 70, NULL, 1, 17, 0, 0, 0, N'What is the Gender of the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (774, 0, N'Sum Insured', 51, 102, NULL, 1, 18, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (775, 0, N'No Claim Bonus', 51, 43, NULL, 1, 19, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (776, 0, N'Car Hire', 51, 77, NULL, 1, 20, 0, 1, 0, N'Does the client require car hire?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (777, 0, N'Overnight Parking', 51, 85, NULL, 1, 21, 1, 1, 0, N'Where is this vehicle parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (778, 0, N'Driver for this vehicle', 51, 63, NULL, 1, 22, 1, 1, 0, N'Who will be the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (779, 0, N'Relationship to the Insured', 51, 60, NULL, 1, 23, 1, 1, 0, N'What is the drivers relationship to the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (780, 0, N'Selected Excess', 51, 64, NULL, 1, 24, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (781, 0, N'ID Number', 52, 101, NULL, 1, 0, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (782, 0, N'Type Of Cover', 52, 115, NULL, 1, 1, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (783, 0, N'Sum Insured', 52, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (784, 0, N'Postal Code', 52, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (785, 0, N'Suburb', 52, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (786, 0, N'Province', 52, 47, NULL, 1, 5, 1, 1, 0, N'In which province is the property where the content is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (787, 0, N'Burglar Alarm Type', 52, 82, NULL, 1, 6, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (788, 0, N'Burglar Bars', 52, 11, NULL, 1, 7, 1, 1, 0, N'Does the house where the content is located have burglar bars?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (789, 0, N'Security Gates', 52, 15, NULL, 1, 8, 1, 1, 0, N'Does the house where the content is located have security gates?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (790, 0, N'Type Of Residence', 52, 49, NULL, 1, 9, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (791, 0, N'Residence Is', 52, 50, NULL, 1, 10, 1, 1, 0, N'What is the property where the content is located used for?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (792, 0, N'Wall Construction', 52, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (793, 0, N'Roof Construction', 52, 56, NULL, 1, 12, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (794, 0, N'Unoccupied', 52, 57, NULL, 1, 13, 1, 1, 0, N'How often is the house unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (795, 0, N'No Claim Bonus', 52, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (796, 0, N'Accidental Damage', 52, 76, NULL, 1, 15, 1, 1, 0, N'Does the client want to include accidental damage cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (797, 0, N'Situation of Residence', 52, 52, NULL, 1, 16, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (798, 0, N'Situation of Residence', 52, 52, NULL, 1, 17, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (799, 0, N'Voluntary Excess', 52, 74, NULL, 1, 18, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (800, 0, N'Selected Excess', 52, 64, NULL, 1, 19, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (801, 0, N'Exclude Theft', 52, 19, NULL, 1, 20, 1, 1, 0, N'Does the client want to exlude Theft from cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (802, 0, N'Electric Fence', 52, 18, NULL, 1, 21, 1, 1, 0, N'Does the house where the content is located have an electric fence surrounding the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (803, 0, N'Risk Address', 53, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (804, 0, N'ID Number', 53, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (805, 0, N'Sum Insured', 53, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (806, 0, N'Postal Code', 53, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (807, 0, N'Suburb', 53, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (808, 0, N'Roof Construction', 53, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (809, 0, N'Wall Construction', 53, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (810, 0, N'Selected Excess', 53, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (811, 0, N'Voluntary Excess', 53, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (812, 0, N'All Risk Category', 54, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (813, 0, N'Risk Address', 54, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (814, 0, N'Postal Code', 54, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (815, 0, N'Suburb', 54, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (816, 0, N'Desciption', 54, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (817, 0, N'Sum Insured', 54, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (818, 0, N'Serial/IMEI Number', 54, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (819, 0, N'Selected Excess', 54, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (820, 0, N'Year Of Manufacture', 55, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (821, 0, N'Make', 55, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (822, 0, N'Model', 55, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (823, 0, N'MM Code', 55, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (824, 0, N'Vehicle Registration Number', 55, 97, NULL, 1, 5, 1, 1, 0, N'What is the registration number for this vehicle?', N'', N'^.{0,10}$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (825, 0, N'Type of Cover', 55, 53, NULL, 1, 6, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (826, 0, N'Suburb', 55, 89, NULL, 1, 7, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (827, 0, N'Postal Code', 55, 90, NULL, 1, 8, 1, 1, 0, N'What is the postal code where this vehicle will be parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (828, 0, N'Class of Use', 55, 48, NULL, 1, 9, 0, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (829, 0, N'Immobiliser', 55, 80, NULL, 1, 11, 0, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (830, 0, N'Tracking Device', 55, 87, NULL, 1, 12, 0, 0, 0, N'Is there a tracking device installed in this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (831, 0, N'Drivers Licence Type', 55, 73, NULL, 1, 13, 1, 0, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (832, 0, N'Drivers Licence First Issued Date', 55, 41, NULL, 1, 14, 1, 0, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (833, 0, N'Main Driver ID Number', 55, 104, NULL, 1, 15, 1, 1, 0, N'What is the ID Number of the main driver of this vehicle?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (834, 0, N'Main Driver Date of Birth', 55, 42, NULL, 1, 16, 1, 1, 0, N'What is the Date of Birth of the main driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (835, 0, N'Main Driver Gender', 55, 70, NULL, 1, 17, 0, 0, 0, N'What is the Gender of the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (836, 0, N'Sum Insured', 55, 102, NULL, 1, 18, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (837, 0, N'No Claim Bonus', 55, 43, NULL, 1, 19, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (838, 0, N'Car Hire', 55, 77, NULL, 1, 20, 0, 1, 0, N'Does the client require car hire?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (839, 0, N'Overnight Parking', 55, 85, NULL, 1, 21, 1, 1, 0, N'Where is this vehicle parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (840, 0, N'Driver for this vehicle', 55, 63, NULL, 1, 22, 1, 1, 0, N'Who will be the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (841, 0, N'Relationship to the Insured', 55, 60, NULL, 1, 23, 1, 1, 0, N'What is the drivers relationship to the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (842, 0, N'Selected Excess', 55, 64, NULL, 1, 24, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (843, 0, N'ID Number', 56, 101, NULL, 1, 0, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (844, 0, N'Type Of Cover', 56, 115, NULL, 1, 1, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (845, 0, N'Sum Insured', 56, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (846, 0, N'Postal Code', 56, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (847, 0, N'Suburb', 56, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (848, 0, N'Province', 56, 47, NULL, 1, 5, 1, 1, 0, N'In which province is the property where the content is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (849, 0, N'Burglar Alarm Type', 56, 82, NULL, 1, 6, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (850, 0, N'Burglar Bars', 56, 11, NULL, 1, 7, 1, 1, 0, N'Does the house where the content is located have burglar bars?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (851, 0, N'Security Gates', 56, 15, NULL, 1, 8, 1, 1, 0, N'Does the house where the content is located have security gates?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (852, 0, N'Type Of Residence', 56, 49, NULL, 1, 9, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (853, 0, N'Residence Is', 56, 50, NULL, 1, 10, 1, 1, 0, N'What is the property where the content is located used for?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (854, 0, N'Wall Construction', 56, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (855, 0, N'Roof Construction', 56, 56, NULL, 1, 12, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (856, 0, N'Unoccupied', 56, 57, NULL, 1, 13, 1, 1, 0, N'How often is the house unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (857, 0, N'No Claim Bonus', 56, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (858, 0, N'Accidental Damage', 56, 76, NULL, 1, 15, 1, 1, 0, N'Does the client want to include accidental damage cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (859, 0, N'Situation of Residence', 56, 52, NULL, 1, 16, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (860, 0, N'Situation of Residence', 56, 52, NULL, 1, 17, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (861, 0, N'Voluntary Excess', 56, 74, NULL, 1, 18, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (862, 0, N'Selected Excess', 56, 64, NULL, 1, 19, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (863, 0, N'Exclude Theft', 56, 19, NULL, 1, 20, 1, 1, 0, N'Does the client want to exlude Theft from cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (864, 0, N'Electric Fence', 56, 18, NULL, 1, 21, 1, 1, 0, N'Does the house where the content is located have an electric fence surrounding the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (865, 0, N'Risk Address', 57, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (866, 0, N'ID Number', 57, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (867, 0, N'Sum Insured', 57, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (868, 0, N'Postal Code', 57, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (869, 0, N'Suburb', 57, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (870, 0, N'Roof Construction', 57, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (871, 0, N'Wall Construction', 57, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (872, 0, N'Selected Excess', 57, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (873, 0, N'Voluntary Excess', 57, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (874, 0, N'All Risk Category', 58, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (875, 0, N'Risk Address', 58, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (876, 0, N'Postal Code', 58, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (877, 0, N'Suburb', 58, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (878, 0, N'Desciption', 58, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (879, 0, N'Sum Insured', 58, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (880, 0, N'Serial/IMEI Number', 58, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (881, 0, N'Selected Excess', 58, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (882, 0, N'Year Of Manufacture', 59, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (883, 0, N'Make', 59, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (884, 0, N'Model', 59, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (885, 0, N'MM Code', 59, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (886, 0, N'Vehicle Registration Number', 59, 97, NULL, 1, 5, 1, 1, 0, N'What is the registration number for this vehicle?', N'', N'^.{0,10}$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (887, 0, N'Type of Cover', 59, 53, NULL, 1, 6, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (888, 0, N'Suburb', 59, 89, NULL, 1, 7, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (889, 0, N'Postal Code', 59, 90, NULL, 1, 8, 1, 1, 0, N'What is the postal code where this vehicle will be parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (890, 0, N'Class of Use', 59, 48, NULL, 1, 9, 0, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (891, 0, N'Immobiliser', 59, 80, NULL, 1, 11, 0, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (892, 0, N'Tracking Device', 59, 87, NULL, 1, 12, 0, 0, 0, N'Is there a tracking device installed in this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (893, 0, N'Drivers Licence Type', 59, 73, NULL, 1, 13, 1, 0, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (894, 0, N'Drivers Licence First Issued Date', 59, 41, NULL, 1, 14, 1, 0, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (895, 0, N'Main Driver ID Number', 59, 104, NULL, 1, 15, 1, 1, 0, N'What is the ID Number of the main driver of this vehicle?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (896, 0, N'Main Driver Date of Birth', 59, 42, NULL, 1, 16, 1, 1, 0, N'What is the Date of Birth of the main driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (897, 0, N'Main Driver Gender', 59, 70, NULL, 1, 17, 0, 0, 0, N'What is the Gender of the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (898, 0, N'Sum Insured', 59, 102, NULL, 1, 18, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (899, 0, N'No Claim Bonus', 59, 43, NULL, 1, 19, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (900, 0, N'Car Hire', 59, 77, NULL, 1, 20, 0, 1, 0, N'Does the client require car hire?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (901, 0, N'Overnight Parking', 59, 85, NULL, 1, 21, 1, 1, 0, N'Where is this vehicle parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (902, 0, N'Driver for this vehicle', 59, 63, NULL, 1, 22, 1, 1, 0, N'Who will be the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (903, 0, N'Relationship to the Insured', 59, 60, NULL, 1, 23, 1, 1, 0, N'What is the drivers relationship to the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (904, 0, N'Selected Excess', 59, 64, NULL, 1, 24, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (905, 0, N'ID Number', 60, 101, NULL, 1, 0, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (906, 0, N'Type Of Cover', 60, 115, NULL, 1, 1, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (907, 0, N'Sum Insured', 60, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (908, 0, N'Postal Code', 60, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (909, 0, N'Suburb', 60, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (910, 0, N'Province', 60, 47, NULL, 1, 5, 1, 1, 0, N'In which province is the property where the content is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (911, 0, N'Burglar Alarm Type', 60, 82, NULL, 1, 6, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (912, 0, N'Burglar Bars', 60, 11, NULL, 1, 7, 1, 1, 0, N'Does the house where the content is located have burglar bars?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (913, 0, N'Security Gates', 60, 15, NULL, 1, 8, 1, 1, 0, N'Does the house where the content is located have security gates?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (914, 0, N'Type Of Residence', 60, 49, NULL, 1, 9, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (915, 0, N'Residence Is', 60, 50, NULL, 1, 10, 1, 1, 0, N'What is the property where the content is located used for?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (916, 0, N'Wall Construction', 60, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (917, 0, N'Roof Construction', 60, 56, NULL, 1, 12, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (918, 0, N'Unoccupied', 60, 57, NULL, 1, 13, 1, 1, 0, N'How often is the house unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (919, 0, N'No Claim Bonus', 60, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (920, 0, N'Accidental Damage', 60, 76, NULL, 1, 15, 1, 1, 0, N'Does the client want to include accidental damage cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (921, 0, N'Situation of Residence', 60, 52, NULL, 1, 16, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (922, 0, N'Situation of Residence', 60, 52, NULL, 1, 17, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (923, 0, N'Voluntary Excess', 60, 74, NULL, 1, 18, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (924, 0, N'Selected Excess', 60, 64, NULL, 1, 19, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (925, 0, N'Exclude Theft', 60, 19, NULL, 1, 20, 1, 1, 0, N'Does the client want to exlude Theft from cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (926, 0, N'Electric Fence', 60, 18, NULL, 1, 21, 1, 1, 0, N'Does the house where the content is located have an electric fence surrounding the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (927, 0, N'Risk Address', 61, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (928, 0, N'ID Number', 61, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (929, 0, N'Sum Insured', 61, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (930, 0, N'Postal Code', 61, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (931, 0, N'Suburb', 61, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (932, 0, N'Roof Construction', 61, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (933, 0, N'Wall Construction', 61, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (934, 0, N'Selected Excess', 61, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (935, 0, N'Voluntary Excess', 61, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (936, 0, N'All Risk Category', 62, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (937, 0, N'Risk Address', 62, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (938, 0, N'Postal Code', 62, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (939, 0, N'Suburb', 62, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (940, 0, N'Desciption', 62, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (941, 0, N'Sum Insured', 62, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (942, 0, N'Serial/IMEI Number', 62, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (943, 0, N'Selected Excess', 62, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (944, 0, N'Year Of Manufacture', 63, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (945, 0, N'Make', 63, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (946, 0, N'Model', 63, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (947, 0, N'MM Code', 63, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (948, 0, N'Vehicle Registration Number', 63, 97, NULL, 1, 5, 1, 1, 0, N'What is the registration number for this vehicle?', N'', N'^.{0,10}$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (949, 0, N'Type of Cover', 63, 53, NULL, 1, 6, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (950, 0, N'Suburb', 63, 89, NULL, 1, 7, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (951, 0, N'Postal Code', 63, 90, NULL, 1, 8, 1, 1, 0, N'What is the postal code where this vehicle will be parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (952, 0, N'Class of Use', 63, 48, NULL, 1, 9, 0, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (953, 0, N'Immobiliser', 63, 80, NULL, 1, 11, 0, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (954, 0, N'Tracking Device', 63, 87, NULL, 1, 12, 0, 0, 0, N'Is there a tracking device installed in this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (955, 0, N'Drivers Licence Type', 63, 73, NULL, 1, 13, 1, 0, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (956, 0, N'Drivers Licence First Issued Date', 63, 41, NULL, 1, 14, 1, 0, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (957, 0, N'Main Driver ID Number', 63, 104, NULL, 1, 15, 1, 1, 0, N'What is the ID Number of the main driver of this vehicle?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (958, 0, N'Main Driver Date of Birth', 63, 42, NULL, 1, 16, 1, 1, 0, N'What is the Date of Birth of the main driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (959, 0, N'Main Driver Gender', 63, 70, NULL, 1, 17, 0, 0, 0, N'What is the Gender of the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (960, 0, N'Sum Insured', 63, 102, NULL, 1, 18, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (961, 0, N'No Claim Bonus', 63, 43, NULL, 1, 19, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (962, 0, N'Car Hire', 63, 77, NULL, 1, 20, 0, 1, 0, N'Does the client require car hire?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (963, 0, N'Overnight Parking', 63, 85, NULL, 1, 21, 1, 1, 0, N'Where is this vehicle parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (964, 0, N'Driver for this vehicle', 63, 63, NULL, 1, 22, 1, 1, 0, N'Who will be the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (965, 0, N'Relationship to the Insured', 63, 60, NULL, 1, 23, 1, 1, 0, N'What is the drivers relationship to the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (966, 0, N'Selected Excess', 63, 64, NULL, 1, 24, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (967, 0, N'ID Number', 64, 101, NULL, 1, 0, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (968, 0, N'Type Of Cover', 64, 115, NULL, 1, 1, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (969, 0, N'Sum Insured', 64, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (970, 0, N'Postal Code', 64, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (971, 0, N'Suburb', 64, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (972, 0, N'Province', 64, 47, NULL, 1, 5, 1, 1, 0, N'In which province is the property where the content is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (973, 0, N'Burglar Alarm Type', 64, 82, NULL, 1, 6, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (974, 0, N'Burglar Bars', 64, 11, NULL, 1, 7, 1, 1, 0, N'Does the house where the content is located have burglar bars?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (975, 0, N'Security Gates', 64, 15, NULL, 1, 8, 1, 1, 0, N'Does the house where the content is located have security gates?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (976, 0, N'Type Of Residence', 64, 49, NULL, 1, 9, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (977, 0, N'Residence Is', 64, 50, NULL, 1, 10, 1, 1, 0, N'What is the property where the content is located used for?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (978, 0, N'Wall Construction', 64, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (979, 0, N'Roof Construction', 64, 56, NULL, 1, 12, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (980, 0, N'Unoccupied', 64, 57, NULL, 1, 13, 1, 1, 0, N'How often is the house unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (981, 0, N'No Claim Bonus', 64, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (982, 0, N'Accidental Damage', 64, 76, NULL, 1, 15, 1, 1, 0, N'Does the client want to include accidental damage cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (983, 0, N'Situation of Residence', 64, 52, NULL, 1, 16, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (984, 0, N'Situation of Residence', 64, 52, NULL, 1, 17, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (985, 0, N'Voluntary Excess', 64, 74, NULL, 1, 18, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (986, 0, N'Selected Excess', 64, 64, NULL, 1, 19, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (987, 0, N'Exclude Theft', 64, 19, NULL, 1, 20, 1, 1, 0, N'Does the client want to exlude Theft from cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (988, 0, N'Electric Fence', 64, 18, NULL, 1, 21, 1, 1, 0, N'Does the house where the content is located have an electric fence surrounding the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (989, 0, N'Risk Address', 65, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (990, 0, N'ID Number', 65, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (991, 0, N'Sum Insured', 65, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (992, 0, N'Postal Code', 65, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (993, 0, N'Suburb', 65, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (994, 0, N'Roof Construction', 65, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (995, 0, N'Wall Construction', 65, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (996, 0, N'Selected Excess', 65, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (997, 0, N'Voluntary Excess', 65, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (998, 0, N'All Risk Category', 66, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (999, 0, N'Risk Address', 66, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1000, 0, N'Postal Code', 66, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1001, 0, N'Suburb', 66, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1002, 0, N'Desciption', 66, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1003, 0, N'Sum Insured', 66, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1004, 0, N'Serial/IMEI Number', 66, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1005, 0, N'Selected Excess', 66, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1006, 0, N'Year Of Manufacture', 67, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1007, 0, N'Make', 67, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1008, 0, N'Model', 67, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1009, 0, N'MM Code', 67, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1010, 0, N'Vehicle Registration Number', 67, 97, NULL, 1, 5, 1, 1, 0, N'What is the registration number for this vehicle?', N'', N'^.{0,10}$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1011, 0, N'Type of Cover', 67, 53, NULL, 1, 6, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1012, 0, N'Suburb', 67, 89, NULL, 1, 7, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1013, 0, N'Postal Code', 67, 90, NULL, 1, 8, 1, 1, 0, N'What is the postal code where this vehicle will be parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1014, 0, N'Class of Use', 67, 48, NULL, 1, 9, 0, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1015, 0, N'Immobiliser', 67, 80, NULL, 1, 11, 0, 1, 0, N'Does this vehicle have an immobiliser installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1016, 0, N'Tracking Device', 67, 87, NULL, 1, 12, 0, 0, 0, N'Is there a tracking device installed in this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1017, 0, N'Drivers Licence Type', 67, 73, NULL, 1, 13, 1, 0, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1018, 0, N'Drivers Licence First Issued Date', 67, 41, NULL, 1, 14, 1, 0, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1019, 0, N'Main Driver Date of Birth', 67, 42, NULL, 1, 15, 1, 1, 0, N'What is the Date of Birth of the main driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1020, 0, N'Main Driver Gender', 67, 70, NULL, 1, 16, 0, 0, 0, N'What is the Gender of the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1021, 0, N'Sum Insured', 67, 102, NULL, 1, 17, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1022, 0, N'No Claim Bonus', 67, 43, NULL, 1, 18, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1023, 0, N'Car Hire', 67, 77, NULL, 1, 19, 0, 1, 0, N'Does the client require car hire?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1024, 0, N'Overnight Parking', 67, 85, NULL, 1, 20, 1, 1, 0, N'Where is this vehicle parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1025, 0, N'Driver for this vehicle', 67, 63, NULL, 1, 21, 1, 1, 0, N'Who will be the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1026, 0, N'Relationship to the Insured', 67, 60, NULL, 1, 22, 1, 1, 0, N'What is the drivers relationship to the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1027, 0, N'Voluntary Excess', 67, 74, NULL, 1, 23, 0, 1, 0, N'Does the client want to pay a voluntary excess that will lower premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1028, 0, N'Vehicle Extras Value', 67, 107, NULL, 1, 24, 1, 1, 0, N'What is the value of the any non-factory extras?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1029, 0, N'Year Of Manufacture', 68, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1030, 0, N'Make', 68, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1031, 0, N'Model', 68, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1032, 0, N'MM Code', 68, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1033, 0, N'Vehicle Type', 68, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1034, 0, N'Class of Use', 68, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1035, 0, N'Type of Cover', 68, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1036, 0, N'Vehicle Tracking Device', 68, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1037, 0, N'Overnight Parking', 68, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1038, 0, N'Postal Code', 68, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1039, 0, N'Suburb', 68, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1040, 0, N'Main Driver Date of Birth', 68, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1041, 0, N'Main Driver Gender', 68, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1042, 0, N'Main Driver Marital Status', 68, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1043, 0, N'No Claim Bonus', 68, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1044, 0, N'Risk Address', 69, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1045, 0, N'ID Number', 69, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1046, 0, N'Type Of Cover', 69, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1047, 0, N'Sum Insured', 69, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1048, 0, N'Postal Code', 69, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1049, 0, N'Suburb', 69, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1050, 0, N'Situation of Residence', 69, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1051, 0, N'Selected Excess', 69, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1052, 0, N'Voluntary Excess', 69, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1053, 0, N'Burglar Alarm Type', 69, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1054, 0, N'Roof Construction', 69, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1055, 0, N'Wall Construction', 69, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1056, 0, N'Type Of Residence', 69, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1057, 0, N'Occupation Date', 69, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1058, 0, N'No Claim Bonus', 69, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1059, 0, N'Subsidence and Landslip', 69, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1060, 0, N'Thatch Safe', 69, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1061, 0, N'Guest House', 69, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1062, 0, N'Retired', 69, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1063, 0, N'Risk Address', 70, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1064, 0, N'ID Number', 70, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1065, 0, N'Sum Insured', 70, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1066, 0, N'Postal Code', 70, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1067, 0, N'Suburb', 70, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1068, 0, N'Roof Construction', 70, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1069, 0, N'Wall Construction', 70, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1070, 0, N'Selected Excess', 70, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1071, 0, N'Voluntary Excess', 70, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1072, 0, N'Retired', 70, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1073, 0, N'All Risk Category', 71, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1074, 0, N'Risk Address', 71, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1075, 0, N'Postal Code', 71, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1076, 0, N'Suburb', 71, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1077, 0, N'Desciption', 71, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1078, 0, N'Sum Insured', 71, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1079, 0, N'Serial/IMEI Number', 71, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1080, 0, N'Retired', 71, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1081, 0, N'Year Of Manufacture', 72, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1082, 0, N'Make', 72, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1083, 0, N'Model', 72, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1084, 0, N'MM Code', 72, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1085, 0, N'Vehicle Type', 72, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1086, 0, N'Class of Use', 72, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1087, 0, N'Type of Cover', 72, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1088, 0, N'Vehicle Tracking Device', 72, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1089, 0, N'Overnight Parking', 72, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1090, 0, N'Postal Code', 72, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1091, 0, N'Suburb', 72, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1092, 0, N'Main Driver Date of Birth', 72, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1093, 0, N'Main Driver Gender', 72, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1094, 0, N'Main Driver Marital Status', 72, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1095, 0, N'No Claim Bonus', 72, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1096, 0, N'Risk Address', 73, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1097, 0, N'ID Number', 73, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1098, 0, N'Type Of Cover', 73, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1099, 0, N'Sum Insured', 73, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1100, 0, N'Postal Code', 73, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1101, 0, N'Suburb', 73, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1102, 0, N'Situation of Residence', 73, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1103, 0, N'Selected Excess', 73, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1104, 0, N'Voluntary Excess', 73, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1105, 0, N'Burglar Alarm Type', 73, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1106, 0, N'Roof Construction', 73, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1107, 0, N'Wall Construction', 73, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1108, 0, N'Type Of Residence', 73, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1109, 0, N'Occupation Date', 73, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1110, 0, N'No Claim Bonus', 73, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1111, 0, N'Subsidence and Landslip', 73, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1112, 0, N'Thatch Safe', 73, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1113, 0, N'Guest House', 73, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1114, 0, N'Retired', 73, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1115, 0, N'Risk Address', 74, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1116, 0, N'ID Number', 74, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1117, 0, N'Sum Insured', 74, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1118, 0, N'Postal Code', 74, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1119, 0, N'Suburb', 74, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1120, 0, N'Roof Construction', 74, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1121, 0, N'Wall Construction', 74, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1122, 0, N'Selected Excess', 74, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1123, 0, N'Voluntary Excess', 74, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1124, 0, N'Retired', 74, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1125, 0, N'All Risk Category', 75, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1126, 0, N'Risk Address', 75, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1127, 0, N'Postal Code', 75, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1128, 0, N'Suburb', 75, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1129, 0, N'Desciption', 75, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1130, 0, N'Sum Insured', 75, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1131, 0, N'Serial/IMEI Number', 75, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1132, 0, N'Retired', 75, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1133, 0, N'Year Of Manufacture', 76, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1134, 0, N'Make', 76, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1135, 0, N'Model', 76, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1136, 0, N'MM Code', 76, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1137, 0, N'Vehicle Type', 76, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1138, 0, N'Class of Use', 76, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1139, 0, N'Type of Cover', 76, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1140, 0, N'Vehicle Tracking Device', 76, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1141, 0, N'Overnight Parking', 76, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1142, 0, N'Postal Code', 76, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1143, 0, N'Suburb', 76, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1144, 0, N'Main Driver Date of Birth', 76, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1145, 0, N'Main Driver Gender', 76, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1146, 0, N'Main Driver Marital Status', 76, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1147, 0, N'No Claim Bonus', 76, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1148, 0, N'Risk Address', 77, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1149, 0, N'ID Number', 77, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1150, 0, N'Type Of Cover', 77, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1151, 0, N'Sum Insured', 77, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1152, 0, N'Postal Code', 77, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1153, 0, N'Suburb', 77, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1154, 0, N'Situation of Residence', 77, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1155, 0, N'Selected Excess', 77, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1156, 0, N'Voluntary Excess', 77, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1157, 0, N'Burglar Alarm Type', 77, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1158, 0, N'Roof Construction', 77, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1159, 0, N'Wall Construction', 77, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1160, 0, N'Type Of Residence', 77, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1161, 0, N'Occupation Date', 77, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1162, 0, N'No Claim Bonus', 77, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1163, 0, N'Subsidence and Landslip', 77, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1164, 0, N'Thatch Safe', 77, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1165, 0, N'Guest House', 77, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1166, 0, N'Retired', 77, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1167, 0, N'Risk Address', 78, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1168, 0, N'ID Number', 78, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1169, 0, N'Sum Insured', 78, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1170, 0, N'Postal Code', 78, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1171, 0, N'Suburb', 78, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1172, 0, N'Roof Construction', 78, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1173, 0, N'Wall Construction', 78, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1174, 0, N'Selected Excess', 78, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1175, 0, N'Voluntary Excess', 78, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1176, 0, N'Retired', 78, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1177, 0, N'All Risk Category', 79, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1178, 0, N'Risk Address', 79, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1179, 0, N'Postal Code', 79, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1180, 0, N'Suburb', 79, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1181, 0, N'Desciption', 79, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1182, 0, N'Sum Insured', 79, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1183, 0, N'Serial/IMEI Number', 79, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1184, 0, N'Retired', 79, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1185, 0, N'Year Of Manufacture', 80, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1186, 0, N'Make', 80, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1187, 0, N'Model', 80, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1188, 0, N'MM Code', 80, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1189, 0, N'Vehicle Type', 80, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1190, 0, N'Class of Use', 80, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1191, 0, N'Type of Cover', 80, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1192, 0, N'Vehicle Tracking Device', 80, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1193, 0, N'Overnight Parking', 80, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1194, 0, N'Postal Code', 80, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1195, 0, N'Suburb', 80, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1196, 0, N'Main Driver Date of Birth', 80, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1197, 0, N'Main Driver Gender', 80, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1198, 0, N'Main Driver Marital Status', 80, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1199, 0, N'No Claim Bonus', 80, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1200, 0, N'Risk Address', 81, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1201, 0, N'ID Number', 81, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1202, 0, N'Type Of Cover', 81, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1203, 0, N'Sum Insured', 81, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1204, 0, N'Postal Code', 81, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1205, 0, N'Suburb', 81, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1206, 0, N'Situation of Residence', 81, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1207, 0, N'Selected Excess', 81, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1208, 0, N'Voluntary Excess', 81, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1209, 0, N'Burglar Alarm Type', 81, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1210, 0, N'Roof Construction', 81, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1211, 0, N'Wall Construction', 81, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1212, 0, N'Type Of Residence', 81, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1213, 0, N'Occupation Date', 81, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1214, 0, N'No Claim Bonus', 81, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1215, 0, N'Subsidence and Landslip', 81, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1216, 0, N'Thatch Safe', 81, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1217, 0, N'Guest House', 81, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1218, 0, N'Retired', 81, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1219, 0, N'Risk Address', 82, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1220, 0, N'ID Number', 82, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1221, 0, N'Sum Insured', 82, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1222, 0, N'Postal Code', 82, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1223, 0, N'Suburb', 82, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1224, 0, N'Roof Construction', 82, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1225, 0, N'Wall Construction', 82, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1226, 0, N'Selected Excess', 82, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1227, 0, N'Voluntary Excess', 82, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1228, 0, N'Retired', 82, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1229, 0, N'All Risk Category', 83, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1230, 0, N'Risk Address', 83, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1231, 0, N'Postal Code', 83, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1232, 0, N'Suburb', 83, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1233, 0, N'Desciption', 83, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1234, 0, N'Sum Insured', 83, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1235, 0, N'Serial/IMEI Number', 83, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1236, 0, N'Retired', 83, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1237, 0, N'Year Of Manufacture', 84, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1238, 0, N'Make', 84, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1239, 0, N'Model', 84, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1240, 0, N'MM Code', 84, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1241, 0, N'Vehicle Type', 84, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1242, 0, N'Class of Use', 84, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1243, 0, N'Type of Cover', 84, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1244, 0, N'Vehicle Tracking Device', 84, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1245, 0, N'Overnight Parking', 84, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1246, 0, N'Postal Code', 84, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1247, 0, N'Suburb', 84, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1248, 0, N'Main Driver Date of Birth', 84, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1249, 0, N'Main Driver Gender', 84, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1250, 0, N'Main Driver Marital Status', 84, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1251, 0, N'No Claim Bonus', 84, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1252, 0, N'Risk Address', 85, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1253, 0, N'ID Number', 85, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1254, 0, N'Type Of Cover', 85, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1255, 0, N'Sum Insured', 85, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1256, 0, N'Postal Code', 85, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1257, 0, N'Suburb', 85, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1258, 0, N'Situation of Residence', 85, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1259, 0, N'Selected Excess', 85, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1260, 0, N'Voluntary Excess', 85, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1261, 0, N'Burglar Alarm Type', 85, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1262, 0, N'Roof Construction', 85, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1263, 0, N'Wall Construction', 85, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1264, 0, N'Type Of Residence', 85, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1265, 0, N'Occupation Date', 85, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1266, 0, N'No Claim Bonus', 85, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1267, 0, N'Subsidence and Landslip', 85, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1268, 0, N'Thatch Safe', 85, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1269, 0, N'Guest House', 85, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1270, 0, N'Retired', 85, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1271, 0, N'Risk Address', 86, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1272, 0, N'ID Number', 86, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1273, 0, N'Sum Insured', 86, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1274, 0, N'Postal Code', 86, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1275, 0, N'Suburb', 86, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1276, 0, N'Roof Construction', 86, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1277, 0, N'Wall Construction', 86, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1278, 0, N'Selected Excess', 86, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1279, 0, N'Voluntary Excess', 86, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1280, 0, N'Retired', 86, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1281, 0, N'All Risk Category', 87, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1282, 0, N'Risk Address', 87, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1283, 0, N'Postal Code', 87, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1284, 0, N'Suburb', 87, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1285, 0, N'Desciption', 87, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1286, 0, N'Sum Insured', 87, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1287, 0, N'Serial/IMEI Number', 87, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1288, 0, N'Retired', 87, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1289, 0, N'Year Of Manufacture', 88, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1290, 0, N'Make', 88, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1291, 0, N'Model', 88, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1292, 0, N'MM Code', 88, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1293, 0, N'Vehicle Type', 88, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1294, 0, N'Class of Use', 88, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1295, 0, N'Type of Cover', 88, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1296, 0, N'Vehicle Tracking Device', 88, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1297, 0, N'Overnight Parking', 88, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1298, 0, N'Postal Code', 88, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1299, 0, N'Suburb', 88, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1300, 0, N'Main Driver Date of Birth', 88, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1301, 0, N'Main Driver Gender', 88, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1302, 0, N'Main Driver Marital Status', 88, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1303, 0, N'No Claim Bonus', 88, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1304, 0, N'Risk Address', 89, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1305, 0, N'ID Number', 89, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1306, 0, N'Type Of Cover', 89, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1307, 0, N'Sum Insured', 89, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1308, 0, N'Postal Code', 89, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1309, 0, N'Suburb', 89, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1310, 0, N'Situation of Residence', 89, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1311, 0, N'Selected Excess', 89, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1312, 0, N'Voluntary Excess', 89, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1313, 0, N'Burglar Alarm Type', 89, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1314, 0, N'Roof Construction', 89, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1315, 0, N'Wall Construction', 89, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1316, 0, N'Type Of Residence', 89, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1317, 0, N'Occupation Date', 89, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1318, 0, N'No Claim Bonus', 89, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1319, 0, N'Subsidence and Landslip', 89, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1320, 0, N'Thatch Safe', 89, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1321, 0, N'Guest House', 89, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1322, 0, N'Retired', 89, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1323, 0, N'Risk Address', 90, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1324, 0, N'ID Number', 90, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1325, 0, N'Sum Insured', 90, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1326, 0, N'Postal Code', 90, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1327, 0, N'Suburb', 90, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1328, 0, N'Roof Construction', 90, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1329, 0, N'Wall Construction', 90, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1330, 0, N'Selected Excess', 90, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1331, 0, N'Voluntary Excess', 90, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1332, 0, N'Retired', 90, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1333, 0, N'All Risk Category', 91, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1334, 0, N'Risk Address', 91, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1335, 0, N'Postal Code', 91, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1336, 0, N'Suburb', 91, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1337, 0, N'Desciption', 91, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1338, 0, N'Sum Insured', 91, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1339, 0, N'Serial/IMEI Number', 91, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1340, 0, N'Retired', 91, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1341, 0, N'Year Of Manufacture', 92, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1342, 0, N'Make', 92, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1343, 0, N'Model', 92, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1344, 0, N'MM Code', 92, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1345, 0, N'Vehicle Type', 92, 69, NULL, 1, 5, 1, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1346, 0, N'Class of Use', 92, 48, NULL, 1, 6, 1, 1, 0, N'What is the intended class of use for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1347, 0, N'Type of Cover', 92, 53, NULL, 1, 7, 1, 1, 0, N'What type of cover is required for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1348, 0, N'Vehicle Tracking Device', 92, 87, NULL, 1, 8, 1, 1, 0, N'Does this vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1349, 0, N'Overnight Parking', 92, 85, NULL, 1, 9, 1, 1, 0, N'Where is this vehicle parked over-night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1350, 0, N'Postal Code', 92, 90, NULL, 1, 10, 1, 1, 0, N'What is the postal code where this vehicle will be parked over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1351, 0, N'Suburb', 92, 89, NULL, 1, 11, 1, 1, 0, N'What is the suburb name where this vehicle will be over night?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1352, 0, N'Main Driver Date of Birth', 92, 42, NULL, 1, 12, 0, 0, 0, NULL, NULL, N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1353, 0, N'Main Driver Gender', 92, 70, NULL, 1, 13, 1, 1, 0, N'What is the date of birth for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1354, 0, N'Main Driver Marital Status', 92, 72, NULL, 1, 14, 1, 1, 0, N'What is the marital status for the main driver of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1355, 0, N'No Claim Bonus', 92, 43, NULL, 1, 15, 1, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1356, 0, N'Risk Address', 93, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1357, 0, N'ID Number', 93, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1358, 0, N'Type Of Cover', 93, 115, NULL, 1, 2, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1359, 0, N'Sum Insured', 93, 102, NULL, 1, 3, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1360, 0, N'Postal Code', 93, 90, NULL, 1, 4, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1361, 0, N'Suburb', 93, 89, NULL, 1, 5, 1, 1, 0, N'What is the suburb name where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1362, 0, N'Situation of Residence', 93, 52, NULL, 1, 6, 1, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1363, 0, N'Selected Excess', 93, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1364, 0, N'Voluntary Excess', 93, 74, NULL, 1, 8, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1365, 0, N'Burglar Alarm Type', 93, 82, NULL, 1, 9, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1366, 0, N'Roof Construction', 93, 56, NULL, 1, 10, 1, 1, 0, N'What is the roof construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1367, 0, N'Wall Construction', 93, 55, NULL, 1, 11, 1, 1, 0, N'What is the wall construction of the house where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1368, 0, N'Type Of Residence', 93, 49, NULL, 1, 12, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1369, 0, N'Occupation Date', 93, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1370, 0, N'No Claim Bonus', 93, 43, NULL, 1, 14, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1371, 0, N'Subsidence and Landslip', 93, 36, NULL, 1, 15, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1372, 0, N'Thatch Safe', 93, 16, NULL, 1, 16, 1, 1, 0, N'Is the thatch treated with thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1373, 0, N'Guest House', 93, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1374, 0, N'Retired', 93, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1375, 0, N'Risk Address', 94, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1376, 0, N'ID Number', 94, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1377, 0, N'Sum Insured', 94, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1378, 0, N'Postal Code', 94, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1379, 0, N'Suburb', 94, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1380, 0, N'Roof Construction', 94, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1381, 0, N'Wall Construction', 94, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1382, 0, N'Selected Excess', 94, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1383, 0, N'Voluntary Excess', 94, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1384, 0, N'Retired', 94, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1385, 0, N'All Risk Category', 95, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1386, 0, N'Risk Address', 95, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1387, 0, N'Postal Code', 95, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1388, 0, N'Suburb', 95, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1389, 0, N'Desciption', 95, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1390, 0, N'Sum Insured', 95, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1391, 0, N'Serial/IMEI Number', 95, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1392, 0, N'Retired', 95, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1393, 0, N'Year Of Manufacture', 96, 99, NULL, 1, 0, 1, 1, 0, N'I''d like to gather some information about the vehicle you would like to insure. Firstly what is the year your vehicle was first registered in?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1394, 0, N'Make', 96, 95, NULL, 1, 1, 1, 1, 0, N'{Title} {Name} what is the make and model of the vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1395, 0, N'Model', 96, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1396, 0, N'MM Code', 96, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1397, 0, N'Vehicle Type', 96, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1398, 0, N'Class of Use', 96, 48, NULL, 1, 6, 1, 1, 0, N'{Title} {Name} is this vehicle mainly for private or business use?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1399, 0, N'Type of Cover', 96, 53, NULL, 1, 7, 1, 1, 0, N'Would you like full comprehensive cover on this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1400, 0, N'Vehicle Tracking Device', 96, 87, NULL, 1, 8, 0, 1, 0, N'Thank you, does the vehicle have a tracking device installed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1401, 0, N'Immobiliser', 96, 80, NULL, 1, 9, 0, 1, 0, N'Does the vehicle have a factory fitted immobiliser?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1402, 0, N'Gear Lock', 96, 8, NULL, 1, 10, 0, 1, 0, N'Does the vehicle have a gearlock?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1403, 0, N'Data Dot', 96, 9, NULL, 1, 11, 0, 1, 0, N'Does the vehicle have data dot?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1404, 0, N'Sum Insured', 96, 102, NULL, 1, 12, 1, 1, 0, N'What is the sum insured of the vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1405, 0, N'Registered Owner ID Number', 96, 100, NULL, 1, 13, 1, 1, 0, N'Please provide the Identity Number of the registered owner of this vehicle.', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1406, 0, N'Vehicle Extras Value', 96, 107, NULL, 1, 17, 0, 1, 0, N'What is the value of the any non-factory extras?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1407, 0, N'Voluntary Excess', 96, 74, NULL, 1, 21, 0, 1, 0, N'A voluntary excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1408, 0, N'Top-up Cover', 96, 25, NULL, 1, 23, 0, 1, 0, N'Would the client want to add top-up cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1409, 0, N'4x4 Cover', 96, 30, NULL, 1, 24, 0, 1, 0, N'Would the client want to add 4x4 cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1410, 0, N'Luxury Cover', 96, 31, NULL, 1, 25, 0, 1, 0, N'Would the client want to add luxury cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1411, 0, N'Main Driver ID Number', 96, 104, NULL, 1, 5, 1, 1, 0, N'What is the ID Number for the main driver of this vehicle?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1412, 0, N'Marital Status', 96, 72, NULL, 1, 6, 0, 1, 0, N'What is the marital status of this client?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1413, 0, N'Drivers Licence First Issued Date', 96, 41, NULL, 1, 7, 1, 1, 0, N'When was the first date of issue of the drivers licence for the mian driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1414, 0, N'Drivers Licence Type', 96, 73, NULL, 1, 8, 1, 1, 0, N'What is the drivers licence type of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1415, 0, N'Suburb', 96, 89, NULL, 1, 9, 1, 1, 0, N'{Title} {Name} would you please provide me with the suburb and town name where you live?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1416, 0, N'Postal Code', 96, 90, NULL, 1, 10, 1, 1, 0, N'Thank you, please confirm your street postal code', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1417, 0, N'Radio Value', 96, 110, NULL, 1, 18, 0, 1, 0, N'What is the radio value for this vehicle?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1418, 0, N'No Claim Bonus', 96, 43, NULL, 1, 19, 0, 1, 0, N'How many years has this client been accident free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1419, 0, N'Current Insurance Period', 96, 114, NULL, 1, 28, 1, 1, 0, N'How long has the client been at his current insurance company?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1420, 0, N'Postal Code (Work)', 96, 91, NULL, 1, 10, 1, 1, 0, N'{Title} {Name} would you also please provide me with the suburb and town name where you work?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1421, 0, N'Gender', 96, 70, NULL, 1, 13, 0, 1, 0, N'What is the gender of the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1422, 0, N'Overnight Parking', 96, 85, NULL, 1, 9, 1, 1, 0, N'Where is the vehicle parked overnight?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1423, 0, N'Vehicle Registration Number', 96, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1424, 0, N'Province', 96, 47, NULL, 1, 10, 1, 1, 0, N'In which province is this vehicle mainly operated in?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1425, 0, N'Modified/Imported', 96, 7, NULL, 1, 15, 1, 1, 0, N'Was this vehicle imported or modified in any way?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1426, 0, N'Daytime Parking', 96, 86, NULL, 1, 17, 1, 1, 0, N'Where is the vehicle parked during the day?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1427, 0, N'Main Driver Date of Birth', 96, 42, NULL, 1, 21, 1, 1, 0, N'What is the Date of Birth of the main driver of this vehicle?', N'', N'^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1428, 0, N'Nominated Driver', 96, 37, NULL, 1, 24, 1, 1, 0, N'Does the client want to insure only the nominated driver?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1429, 0, N'Previously Insured', 96, 39, NULL, 1, 25, 1, 1, 0, N'Has this vehicle been insured before?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1430, 0, N'Finance House', 96, 103, NULL, 1, 27, 0, 1, 0, N'At which institution is the vehicle financed?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1431, 0, N'Claims in the past 12 months', 96, 44, NULL, 1, 29, 1, 1, 0, N'How many claims have the client registered in the past 12 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1432, 0, N'Claims in the past 12 to 24 months', 96, 45, NULL, 1, 30, 1, 1, 0, N'How many claims have the client registered in the past 12 to 24 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1433, 0, N'Claims in the past 24 to 36 months', 96, 46, NULL, 1, 31, 1, 1, 0, N'How many claims have the client registered in the past 24 to 36 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1434, 0, N'Excess Buster', 96, 24, NULL, 1, 21, 0, 1, 0, N'Does the client want to waive the excess?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1435, 0, N'Advanced Driver', 96, 38, NULL, 1, 23, 0, 1, 0, N'Does the driver of this vehicle have an advanced driver certificate?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1436, 0, N'Car Hire', 96, 77, NULL, 1, 20, 0, 1, 0, N'Does the client require car hire?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1437, 0, N'Driver for this vehicle', 96, 63, NULL, 1, 22, 1, 1, 0, N'Who will be the main driver for this vehicle?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1438, 0, N'Relationship to the Insured', 96, 60, NULL, 1, 23, 1, 1, 0, N'What is the drivers relationship to the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1439, 0, N'Selected Excess', 96, 64, NULL, 1, 24, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1440, 0, N'Risk Address', 97, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property where the goods are kept?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1441, 0, N'Sum Insured', 97, 102, NULL, 1, 1, 1, 1, 0, N'What is the sum insured of the contents?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1442, 0, N'Suburb', 97, 89, NULL, 1, 2, 1, 1, 0, N'What is the suburb where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1443, 0, N'Postal Code', 97, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1444, 0, N'Residence Is', 97, 50, NULL, 1, 4, 1, 1, 0, N'What is the property where the content is located used for?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1445, 0, N'Type Of Residence', 97, 49, NULL, 1, 5, 1, 1, 0, N'What is the type of the residence where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1446, 0, N'Situation of Residence', 97, 52, NULL, 1, 7, 0, 1, 0, N'Where is this residence situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1447, 0, N'Home Industry Type', 97, 59, NULL, 1, 8, 0, 1, 0, N'Does the client have a home industry they would like to add on the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1448, 0, N'Home Industry Sum Insured', 97, 112, NULL, 1, 9, 0, 1, 0, N'What is the value of the home industry they would like to add on the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1449, 0, N'Wall Construction', 97, 55, NULL, 1, 11, 0, 1, 0, N'What is the wall construction of this property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1450, 0, N'Roof Construction', 97, 56, NULL, 1, 12, 0, 1, 0, N'What is the roof contruction of this property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1451, 0, N'Thatch Safe', 97, 16, NULL, 1, 13, 0, 1, 0, N'Does this residence have thatch safe?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1452, 0, N'Security Gates', 97, 15, NULL, 1, 14, 1, 1, 0, N'Does the house where the content is located have security gates?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1453, 0, N'Burglar Bars', 97, 11, NULL, 1, 15, 1, 1, 0, N'Does the house where the content is located have burglar bars?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1454, 0, N'Claim Free Group', 97, 43, NULL, 1, 19, 0, 1, 0, N'What is the CFG for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1455, 0, N'Voluntary Excess', 97, 74, NULL, 1, 20, 0, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1456, 0, N'Accidental Damage', 97, 76, NULL, 1, 21, 0, 1, 0, N'Does this client want to add accidental damage?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1457, 0, N'Subsidence and Landslip', 97, 36, NULL, 1, 22, 0, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1458, 0, N'Exclude Flood', 97, 23, NULL, 1, 23, 0, 1, 0, N'Does the client want to exlude flood from cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1459, 0, N'Exclude Theft', 97, 19, NULL, 1, 24, 0, 1, 0, N'Does the client want to exlude Theft from cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1460, 0, N'Limited to B&B', 97, 17, NULL, 1, 25, 0, 1, 0, N'Will the contents cover be limited to bed and breakfast cover?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1461, 0, N'ID Number', 97, 101, NULL, 1, 0, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1462, 0, N'Type Of Cover', 97, 115, NULL, 1, 1, 1, 1, 0, N'What is the type of cover required for the contents?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1463, 0, N'Province', 97, 47, NULL, 1, 5, 1, 1, 0, N'In which province is the property where the content is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1464, 0, N'Burglar Alarm Type', 97, 82, NULL, 1, 6, 1, 1, 0, N'What type of alarm does the house have where the contents is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1465, 0, N'Unoccupied', 97, 57, NULL, 1, 13, 1, 1, 0, N'How often is the house unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1466, 0, N'Electric Fence', 97, 18, NULL, 1, 20, 1, 1, 0, N'Does the house where the content is located have an electric fence surrounding the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1467, 0, N'Secure Complex', 97, 14, NULL, 1, 4, 1, 1, 0, N'Is the house where the content is located in a security complex?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1468, 0, N'Insured Age', 97, 93, NULL, 1, 7, 1, 1, 0, N'What is the age of the person living in the house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1469, 0, N'Selected Excess', 97, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1470, 0, N'Occupation Date', 97, 40, NULL, 1, 13, 1, 1, 0, N'When was the house occupied by the insured?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1471, 0, N'Guest House', 97, 124, NULL, 1, 17, 1, 1, 0, N'Is this property used as a guest house?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1472, 0, N'Retired', 97, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1473, 0, N'Roof Gradient', 97, 54, NULL, 1, 11, 1, 1, 0, N'What is the gradient of the roof?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1474, 0, N'Borehole', 97, 3, NULL, 1, 18, 0, 1, 0, N'Does the property where the content is located have a borehole?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1475, 0, N'Near River', 97, 5, NULL, 1, 19, 0, 1, 0, N'Is the property where the content is located near a river?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1476, 0, N'Claims in the past 12 months', 97, 44, NULL, 1, 20, 1, 1, 0, N'How many claims have the client registered in the past 12 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1477, 0, N'Claims in the past 12 to 24 months', 97, 45, NULL, 1, 21, 1, 1, 0, N'How many claims have the client registered in the past 12 to 24 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1478, 0, N'Claims in the past 24 to 36 months', 97, 46, NULL, 1, 22, 1, 1, 0, N'How many claims have the client registered in the past 24 to 36 months?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1479, 0, N'Risk Address', 98, 88, NULL, 1, 0, 1, 1, 0, N'What is the address of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1480, 0, N'ID Number', 98, 101, NULL, 1, 1, 1, 1, 0, N'What is the ID Number of the insured?', N'', N'([0-9][0-9])(([0][1-9])|([1][0-2]))(([0-2][0-9])|([3][0-1]))([0-9])([0-9]{3})([0-9])([0-9])([0-9])')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1481, 0, N'Sum Insured', 98, 102, NULL, 1, 2, 1, 1, 0, N'What is the sum insured of the property?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1482, 0, N'Postal Code', 98, 90, NULL, 1, 3, 1, 1, 0, N'What is the postal code where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1483, 0, N'Suburb', 98, 89, NULL, 1, 4, 1, 1, 0, N'What is the suburb name where the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1484, 0, N'Type Of Cover', 98, 115, NULL, 1, 5, 1, 1, 0, N'What is the type of cover required for the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1485, 0, N'Type Of Residence', 98, 49, NULL, 1, 6, 1, 1, 0, N'What is the type of the residence of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1486, 0, N'Situation of Residence', 98, 52, NULL, 1, 7, 1, 1, 0, N'Where is this property situated?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1487, 0, N'Property Is', 98, 50, NULL, 1, 8, 1, 1, 0, N'What is the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1488, 0, N'Roof Construction', 98, 56, NULL, 1, 11, 1, 1, 0, N'What is the roof construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1489, 0, N'Wall Construction', 98, 55, NULL, 1, 12, 1, 1, 0, N'What is the wall construction of the property?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1490, 0, N'Subsidence and Landslip', 98, 36, NULL, 1, 13, 1, 1, 0, N'Does this client want to add subsidence and landslip?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1491, 0, N'Unoccupied', 98, 57, NULL, 1, 14, 1, 1, 0, N'How often is the Property unoccupied?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1492, 0, N'No Claim Bonus', 98, 43, NULL, 1, 15, 1, 1, 0, N'How many years have this client been claim free?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1493, 0, N'Selected Excess', 98, 64, NULL, 1, 16, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1494, 0, N'Voluntary Excess', 98, 74, NULL, 1, 17, 1, 1, 0, N'Does this client want a voluntary excess to reduce their premium?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1495, 0, N'Retired', 98, 123, NULL, 1, 18, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1496, 0, N'Province', 98, 47, NULL, 1, 5, 1, 1, 0, N'In which province is the property is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1497, 0, N'All Risk Category', 99, 126, NULL, 1, 0, 1, 1, 0, N'What category does the item fall under?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1498, 0, N'Risk Address', 99, 88, NULL, 1, 1, 1, 1, 0, N'What is the address where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1499, 0, N'Postal Code', 99, 90, NULL, 1, 2, 1, 1, 0, N'What is the postal code where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1500, 0, N'Suburb', 99, 89, NULL, 1, 3, 1, 1, 0, N'What is the suburb name where the item is located?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1501, 0, N'Desciption', 99, 127, NULL, 1, 4, 1, 1, 0, N'Enter a description of the item', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1502, 0, N'Sum Insured', 99, 102, NULL, 1, 5, 1, 1, 0, N'What is the sum insured of the item?', N'', N'^[0-9\.]+$')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1503, 0, N'Serial/IMEI Number', 99, 128, NULL, 1, 6, 1, 1, 0, N'What is the serial/imei number of the item?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1504, 0, N'Pensioner', 99, 1, NULL, 1, 7, 1, 1, 0, N'Is the insured a pensioner?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1505, 0, N'Retired', 99, 123, NULL, 1, 7, 1, 1, 0, N'Is the insured retired?', N'', N'')
GO
INSERT [dbo].[QuestionDefinition] ([Id], [MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (1506, 0, N'Selected Excess', 99, 64, NULL, 1, 7, 1, 1, 0, N'A selected excess will reduce a clients premium.', N'', N'')
GO
SET IDENTITY_INSERT [dbo].[QuestionDefinition] OFF
GO
SET IDENTITY_INSERT [dbo].[Address] ON 

GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (1, 1, 0, N'Head Office', NULL, N'1 Sportica Crescent', N'', N'Tyger Valley', N'Belville', N'7530', CAST(-33.878811 AS Decimal(9, 6)), CAST(18.633513 AS Decimal(9, 6)), 10, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (2, 2, 0, N'Head Office', NULL, N'1 Sportica Crescent', N'', N'Tyger Valley', N'Belville', N'7530', CAST(-33.878811 AS Decimal(9, 6)), CAST(18.633513 AS Decimal(9, 6)), 10, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (3, 3, 0, N'Head Office', NULL, N'22 Oxford Road', N'', N'Parktown', N'Johannesburg', N'2193', CAST(-26.174969 AS Decimal(9, 6)), CAST(28.039867 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (4, 4, 0, N'Head Office', NULL, N'22 Oxford Road', N'', N'Parktown', N'Johannesburg', N'2193', CAST(-26.174969 AS Decimal(9, 6)), CAST(28.039867 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (5, 5, 0, N'Head Office', NULL, N'146 Boeing Road East', N'', N'Elma Park', N'Edenvale', N'1609', CAST(-26.160446 AS Decimal(9, 6)), CAST(28.156768 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (6, 6, 0, N'Head Office', NULL, N'Auto & General Park', N'1 Telesure Lane', N'Riverglen', N'Dainfern', N'2191', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (7, 7, 0, N'Head Office', NULL, N'Block B, Investment Place', N'10th Road', N'', N'Hyde Park', N'2196', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (8, 8, 0, N'Head Office', NULL, N'Block B, Investment Place', N'10th Road', N'', N'Hyde Park', N'2196', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (9, 9, 0, N'Head Office', NULL, N'Auto & General Park', N'1 Telesure Lane', N'Riverglen', N'Dainfern', N'2191', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (10, 10, 0, N'Head Office', NULL, N'Block B, Investment Place', N'10th Road', N'', N'Hyde Park', N'2196', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (11, 11, 0, N'Head Office', NULL, N'Auto & General Park', N'1 Telesure Lane', N'Riverglen', N'Dainfern', N'2191', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (12, 12, 0, N'Head Office', NULL, N'Auto & General Park', N'1 Telesure Lane', N'Riverglen', N'Dainfern', N'2191', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (13, 13, 0, N'Head Office', NULL, N'Renasa House', N'170 Oxford Road', N'Melrose', N'Johannesburg', N'2001', CAST(-26.137197 AS Decimal(9, 6)), CAST(28.046719 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (14, 14, 0, N'Head Office', NULL, N'5th Floor', N'25 Joe Slovo Street', N'', N'Durban', N'4001', CAST(-29.860704 AS Decimal(9, 6)), CAST(30.992462 AS Decimal(9, 6)), 5, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (15, 15, 0, N'Head Office', NULL, N'S.A. Underwriters Place', N'Bond Street Business Park', N'Randburg', N'Johannesburg', N'2194', CAST(-26.085210 AS Decimal(9, 6)), CAST(27.993760 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (16, 16, 0, N'Head Office', NULL, N'S.A. Underwriters Place', N'Bond Street Business Park', N'Randburg', N'Johannesburg', N'2194', CAST(-26.085210 AS Decimal(9, 6)), CAST(27.993760 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (17, 17, 0, N'Head Office', NULL, N'S.A. Underwriters Place', N'Bond Street Business Park', N'Randburg', N'Johannesburg', N'2194', CAST(-26.085210 AS Decimal(9, 6)), CAST(27.993760 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (18, 18, 0, N'Head Office', NULL, N'S.A. Underwriters Place', N'Bond Street Business Park', N'Randburg', N'Johannesburg', N'2194', CAST(-26.085210 AS Decimal(9, 6)), CAST(27.993760 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (19, 19, 0, N'Head Office', NULL, N'127A York Street', N'', N'', N'George', N'6530', CAST(-33.955816 AS Decimal(9, 6)), CAST(22.458071 AS Decimal(9, 6)), 2, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (20, 20, 0, N'Head Office', NULL, N'Auto & General Park', N'1 Telesure Lane', N'Riverglen', N'Dainfern', N'2191', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (21, 21, 0, N'Head Office', NULL, N'Block B, Investment Place', N'10th Road', N'', N'Hyde Park', N'2196', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (22, 22, 0, N'Head Office', NULL, N'Block B, Investment Place', N'10th Road', N'', N'Hyde Park', N'2196', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (23, 23, 0, N'Head Office', NULL, N'Auto & General Park', N'1 Telesure Lane', N'Riverglen', N'Dainfern', N'2191', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (24, 24, 0, N'Head Office', NULL, N'Block B, Investment Place', N'10th Road', N'', N'Hyde Park', N'2196', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (25, 25, 0, N'Head Office', NULL, N'Auto & General Park', N'1 Telesure Lane', N'Riverglen', N'Dainfern', N'2191', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
INSERT [dbo].[Address] ([Id], [PartyId], [MasterId], [Description], [Complex], [Line1], [Line2], [Line3], [Line4], [Code], [Latitude], [Longitude], [StateProvinceId], [AddressTypeId], [IsDefault], [IsComplex], [DateFrom], [DateTo]) VALUES (26, 26, 0, N'Head Office', NULL, N'Auto & General Park', N'1 Telesure Lane', N'Riverglen', N'Dainfern', N'2191', CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 4, 1, 1, 0, CAST(0xED380B00 AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[Address] OFF
GO
