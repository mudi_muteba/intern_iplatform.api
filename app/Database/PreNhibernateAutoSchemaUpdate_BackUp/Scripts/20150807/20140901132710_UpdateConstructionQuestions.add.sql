﻿Declare @id int
Declare @VIndex int

set @id = (select id from md.Question where name ='Wall Construction')

set @VIndex = (Select max(VisibleIndex)+1 from md.QuestionAnswer where QuestionId=@id)

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Wall Construction - Cement', 'Cement', @id, @VIndex),
		('Wall Construction - Concrete', 'Concrete', @id, @VIndex+1),
		('Wall Construction - Corrugated Iron', 'Corrugated Iron', @id, @VIndex+2),
		('Wall Construction - Precast', 'Precast', @id, @VIndex+3),
		('Wall Construction - Stone', 'Stone', @id, @VIndex+4),
		('Wall Construction - Wood', 'Wood', @id, @VIndex+5),
		('Wall Construction - Asbestos', 'Asbestos', @id, @VIndex+6)

Declare @idRoof int
Declare @VIndexR int

set @idRoof = (select id from md.Question where name ='Roof Construction')

set @VIndexR = (Select max(VisibleIndex)+1 from md.QuestionAnswer where QuestionId=@idRoof)

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Roof Construction - Concrete', 'Concrete', @idRoof, @VIndexR),
		('Roof Construction - Slate', 'Slate', @idRoof, @VIndexR+1),
		('Roof Construction - Tile', 'Tile', @idRoof, @VIndexR+2),
		('Roof Construction - Shingles', 'Shingles', @idRoof, @VIndexR+3),
		('Roof Construction - Corrugated Iron', 'Corrugated Iron', @idRoof, @VIndexR+4),
		('Roof Construction - Thatch', 'Thatch', @idRoof, @VIndexR+5),
		('Roof Construction - Asbestos', 'Asbestos', @idRoof, @VIndexR+6)






