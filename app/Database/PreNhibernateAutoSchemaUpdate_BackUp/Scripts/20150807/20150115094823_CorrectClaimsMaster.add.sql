﻿UPDATE md.ClaimsQuestion SET Name='Two x Repair Quotation' WHERE ID=129
UPDATE md.ClaimsQuestionDefinition SET Displayname='Two x Repair Quotation' WHERE ClaimsQuestionId=129

UPDATE md.ClaimsQuestion SET Name='Two x Change Of Ownership Paper' WHERE ID=132
UPDATE md.ClaimsQuestionDefinition SET Displayname='Two x Change Of Ownership Paper' WHERE ClaimsQuestionId=132

UPDATE md.ClaimsQuestion SET Name='Insured ID Number' WHERE ID=15
UPDATE md.ClaimsQuestionDefinition SET Displayname='Insured ID Number' WHERE ClaimsQuestionId=15

UPDATE md.ClaimsQuestion SET Name='Scan Vehicle Licence Disc' WHERE ID=122
UPDATE md.ClaimsQuestionDefinition SET Displayname='Scan Vehicle Licence Disc' WHERE ClaimsQuestionId=122

EXEC sp_rename 'md.ClaimsQuestionDefinition.Displayname', 'Name','COLUMN'
