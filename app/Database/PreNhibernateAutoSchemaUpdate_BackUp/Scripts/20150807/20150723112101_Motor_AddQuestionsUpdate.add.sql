﻿SET ANSI_PADDING ON 
GO

DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')

DECLARE @CoverID INT 
SET @CoverID=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND coverid=218)

DECLARE @id INT
SET @id = (SELECT ID FROM md.Question WHERE Name = 'Limited Mileage')

UPDATE md.QuestionAnswer SET Answer = 'No Limit' WHERE QuestionId=@id AND Answer='0' 
UPDATE md.QuestionAnswer SET Answer = '5 000 KM' WHERE QuestionId=@id AND Answer='5000' 
UPDATE md.QuestionAnswer SET Answer = '10 0000 KM' WHERE QuestionId=@id AND Answer='10000' 