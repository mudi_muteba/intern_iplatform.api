﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if NOT exists (select * from dbo.sysobjects where id = object_id(N'ClaimsType') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
	CREATE TABLE [md].[ClaimsType](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Name] [nvarchar](80) NOT NULL,
	 CONSTRAINT [PK_ClaimsType] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
End

INSERT INTO md.ClaimsType(Name) 
VALUES		('Motor Theft'), 
			('Motor Accident'), 
			('Public Liability'), 
			('Personal Liability'), 
			('Personal Accident'), 
			('Theft Of Property Out Of Motor'), 
			('Non-Motor Claims Up To R10 000'), 
			('Property Loss / Damage'), 
			('Marine / GIT'), 
			('Pleasure Craft'), 
			('Contract Works') 