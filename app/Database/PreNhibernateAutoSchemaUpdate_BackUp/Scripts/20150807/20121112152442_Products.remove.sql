﻿if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Product_Owner]') AND parent_object_id = OBJECT_ID('Product'))
alter table Product  drop constraint FK_Product_Owner

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Product_Provider]') AND parent_object_id = OBJECT_ID('Product'))
alter table Product  drop constraint FK_Product_Provider

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Product_ProductType]') AND parent_object_id = OBJECT_ID('Product'))
alter table Product  drop constraint FK_Product_ProductType

if exists (select * from dbo.sysobjects where id = object_id(N'Product') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table Product
if exists (select * from dbo.sysobjects where id = object_id(N'md.ProductType') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table md.ProductType
