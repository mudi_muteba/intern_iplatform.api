﻿IF NOT EXISTS (SELECT * FROM [dbo].[Campaign] WHERE [Reference] = 'DEFAULT')
BEGIN
	INSERT [dbo].[Campaign] ([Name], [Reference], [StartDate], [EndDate], [DateCreated], [DateUpdated], [DefaultCampaign])
	VALUES (N'Default Campaign', N'DEFAULT', GETDATE(), NULL, GETDATE(), GETDATE(), 1)
END

