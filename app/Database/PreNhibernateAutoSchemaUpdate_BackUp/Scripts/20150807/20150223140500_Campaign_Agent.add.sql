IF NOT EXISTS(SELECT *FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'CampaignAgent') 
BEGIN				 
	create table CampaignAgent (
        Id INT IDENTITY NOT NULL,
       CampaignId INT null,
       PartyId INT null,
       primary key (Id)
    )

	alter table CampaignAgent 
        add constraint FK_Campaign_Agent
        foreign key (CampaignId) 
        references Campaign

    alter table CampaignAgent 
        add constraint FK_Agent_Campaign 
        foreign key (PartyId) 
        references Party

END