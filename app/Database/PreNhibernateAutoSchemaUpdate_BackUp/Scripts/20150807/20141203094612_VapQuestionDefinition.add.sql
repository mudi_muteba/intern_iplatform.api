begin tran/* Excess Protect */
GO
declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band A' and p.Name = 'Excess Protect'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 99 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @CoverDefId, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO


declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band B' and p.Name = 'Excess Protect'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 99 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @CoverDefId, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO


declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band C' and p.Name = 'Excess Protect'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 99 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @CoverDefId, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO


declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band D' and p.Name = 'Excess Protect'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 99 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @CoverDefId, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO


declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band E' and p.Name = 'Excess Protect'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 99 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @CoverDefId, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO


declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Windscreen' and p.Name = 'Excess Protect'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 99 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Year Of Manufacture', @CoverDefId, 99, NULL, 1, 0, 1, 1, 0, N'What is the year of manufacture for this vehicle?', N'', N'^[0-9]+$')
GO

/* Tyre & Rim */

declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band A' and p.Name = 'Tyre & Rim'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO


declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band B' and p.Name = 'Tyre & Rim'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO


declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band C' and p.Name = 'Tyre & Rim'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO


declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band D' and p.Name = 'Tyre & Rim'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO


declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band E' and p.Name = 'Tyre & Rim'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO

/* Tyre Protect */

declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band A' and p.Name = 'Tyre Protect'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO


declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band B' and p.Name = 'Tyre Protect'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO


declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band C' and p.Name = 'Tyre Protect'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO


declare @CoverDefId int
select @CoverDefId = cd.id from CoverDefinition cd 
inner join Product p on p.id = cd.ProductId
where cd.DisplayName = 'Band D' and p.Name = 'Tyre Protect'

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 95 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES ( 0, N'Make', @CoverDefId, 95, NULL, 1, 1, 1, 1, 0, N'What is the make of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 96 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Model', @CoverDefId, 96, NULL, 1, 2, 1, 1, 0, N'What is the model of this vehicle?', N'', N'')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 94 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'MM Code', @CoverDefId, 94, NULL, 1, 4, 1, 1, 1, N'What is the Mead and McGrouther code for this vehicle?', N'', N'^[0-9]+$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 97 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Registration Number', @CoverDefId, 97, NULL, 1, 5, 1, 1, 0, N'Can you please provide me with the license plate number?', N'', N'^.{0,10}$')

IF NOT EXISTS( select * from QuestionDefinition where QuestionId = 69 and CoverDefinitionId = @CoverDefId)
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Vehicle Type', @CoverDefId, 69, NULL, 1, 5, 0, 1, 0, N'What type of vehicle is the vehicle in question?', N'', N'')
GO


commit
GO