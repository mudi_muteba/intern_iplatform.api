﻿
--Add CampaignSourceID field
ALTER TABLE dbo.LeadActivity ADD CampaignSourceId INT
Go

--Add a default field
ALTER TABLE dbo.Campaign ADD DefaultCampaign bit
Go

--Add Reference Lookup Table for Campaigns
if NOT exists (select * from dbo.sysobjects where id = object_id(N'CampaignReference') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    CREATE TABLE [CampaignReference](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CampaignId] [int] NOT NULL,
	[Reference] [nvarchar](50) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	CONSTRAINT [PK_CampaignReference] PRIMARY KEY CLUSTERED 
	(	[Id] ASC)) ON [PRIMARY]

	ALTER TABLE [CampaignReference] ADD  CONSTRAINT [DF_CampaignReference_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
END

--Add Debitday Lookup Table for Products
if NOT exists (select * from dbo.sysobjects where id = object_id(N'ProductDebitDay') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    CREATE TABLE [ProductDebitDay](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[DebitDay] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	CONSTRAINT [PK_ProductDebitDay] PRIMARY KEY CLUSTERED 
	(	[Id] ASC)) ON [PRIMARY]

	ALTER TABLE [ProductDebitDay] ADD  CONSTRAINT [DF_ProductDebitDay_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
END

