﻿IF NOT EXISTS (SELECT * FROM sys.procedures WHERE Name = 'usp_AgentLeadStatsCurrent')

EXEC(
'CREATE PROCEDURE [dbo].[usp_AgentLeadStatsCurrent]
(
@Frequency Varchar(10),
@Value INT,
@CampaignId INT
)


AS
BEGIN



---Frequency
declare @fromDate datetime
declare @todate datetime

IF @Frequency = ''Daily'' 
Begin
Set @fromDate = dateadd(wk, datediff(wk, 0, getdate()), @Value)
Set @toDate = dateadd(wk, datediff(wk, 0, getdate()), @Value) + ''23:59:59.000''
End;

IF @Frequency = ''Weekly''
Begin 
Set @fromDate = dateadd(wk,-1,dateadd(wk, datediff(wk, 0, getdate()), @Value))   ---7 Days from selected day
Set @toDate = dateadd(wk, datediff(wk, 0, getdate()), @Value) + ''23:59:59.000''
End;

IF @Frequency = ''Monthly''
Begin
Set @FromDate = Convert(datetime,Cast(Year(Getdate()) as Varchar(4))  + ''-'' + Cast(@Value As Varchar(4)) + ''-01'')
Set @ToDate = DATEADD(DAY, -(DAY(DATEADD(MONTH, 1, @FromDate))),DATEADD(MONTH, 1, @FromDate))
End;

IF @Frequency = ''Yearly''
Begin 
Set @Fromdate = Convert(Datetime,Convert(Varchar(4),@Value) + ''-01-01'')
Set @Todate = Convert(Datetime,Convert(Varchar(4),@Value) + ''-12-31 23:59:59.000'')
End;


With CampaignLeadActivity AS

(
SELECT (select DisplayName from Party where Id = La.PartyId)  ''User'',
      C.Name CampaignName,
	  Ls.Name LeadName,
      Count(CampaignId) TotalLeads
FROM md.leadstatus ls
INNER JOIN LeadActivity la ON ls .Id = la.LeadStatusId
INNER JOIN lead l ON la.LeadId = l.id
INNER JOIN party P ON L.PartyId = p.Id
INNER JOIN Campaign C ON la.CampaignId = c.Id
LEFT OUTER JOIN Role R ON P.id = R.PartyId
LEFT OUTER JOIN md.RoleType RT ON R.RoleTypeId = RT.Id
WHERE isnull(@CampaignId, C.Id) = C.Id
  AND dateadd(d,0,Datediff(d,0, la.DateCreated)) Between @Fromdate and @Todate
GROUP BY C.Name, La.PartyId, Ls.Name
)

SELECT [User], 
             Sum(isnull([New] ,0)) [New],
             Sum(isnull([Open],0)) [Open],
             Sum(isnull([Hold],0)) [Hold],
             Sum(isnull([Lost],0)) [Lost],
             Sum(isnull([Dead],0)) [Dead],
             Sum(isnull([Sold],0)) [Sold],
			 Sum(isnull([New] ,0) + isnull([Open],0) + isnull([Hold],0) + 
			 isnull([Lost],0) + isnull([Dead],0) + isnull([Sold],0)) [Total]

FROM CampaignLeadActivity
Pivot(Sum(TotalLeads) For LeadName in ([New],[Open],[Hold],[Lost],[Dead],[Sold],[Total] )) P
Group by [User]

END')