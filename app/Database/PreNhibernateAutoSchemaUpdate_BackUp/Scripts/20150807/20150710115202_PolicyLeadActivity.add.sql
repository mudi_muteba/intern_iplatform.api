
IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'PolicyLeadActivity'))

BEGIN

	CREATE TABLE [dbo].[PolicyLeadActivity](
		[Id] [bigint] NOT NULL,
		[PolicyHeaderId] [int] NOT NULL,
	 CONSTRAINT [PK_PolicyLeadActivity] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[PolicyLeadActivity]  WITH CHECK ADD  CONSTRAINT [FK_PolicyLeadActivity_PolicyHeader] FOREIGN KEY([PolicyHeaderId])
	REFERENCES [dbo].[PolicyHeader] ([Id])

	ALTER TABLE [dbo].[PolicyLeadActivity] CHECK CONSTRAINT [FK_PolicyLeadActivity_PolicyHeader]


END