if not exists (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Quote' AND COLUMN_NAME = 'InsurerReference')
begin
	alter table Quote
	add InsurerReference nvarchar(50) null
end

if not exists (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Quote' AND COLUMN_NAME = 'ITCScore')
begin
	alter table Quote
	add ITCScore nvarchar(10) null
end

if not exists (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Quote' AND COLUMN_NAME = 'ExtraITCScore')
begin
	alter table Quote
	add ExtraITCScore nvarchar(10) null
end
