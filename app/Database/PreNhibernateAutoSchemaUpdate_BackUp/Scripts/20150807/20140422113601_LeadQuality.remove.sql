﻿-- Constraints
if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Lead_LeadQuality]') AND parent_object_id = OBJECT_ID('Lead'))
alter table Lead  drop constraint FK_Lead_LeadQuality


-- Table
if exists (select * from dbo.sysobjects where id = object_id(N'LeadQuality') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table LeadQuality

