IF NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'QuoteAcceptedLeadActivity')
BEGIN

CREATE TABLE [dbo].[QuoteAcceptedLeadActivity](
	[Id] [bigint] NOT NULL,
	[QuoteId] [int] NOT NULL,
 CONSTRAINT [PK_QuoteAcceptedLeadActivity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[QuoteAcceptedLeadActivity]  WITH CHECK ADD  CONSTRAINT [FK_QuoteAcceptedLeadActivity_Quote] FOREIGN KEY([QuoteId])
REFERENCES [dbo].[Quote] ([Id])

ALTER TABLE [dbo].[QuoteAcceptedLeadActivity] CHECK CONSTRAINT [FK_QuoteAcceptedLeadActivity_Quote]
END


