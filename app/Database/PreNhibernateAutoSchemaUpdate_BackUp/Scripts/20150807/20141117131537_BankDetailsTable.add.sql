if NOT exists (select * from dbo.sysobjects where id = object_id(N'[BankDetails]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
CREATE TABLE [dbo].[BankDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PartyId] [int] NULL,
	[BankAccHolder] [nvarchar](255) NULL,
	[Bank] [nvarchar](255) NULL,
	[BankBranch] [nvarchar](255) NULL,
	[BankBranchCode] [nvarchar](255) NULL,
	[AccountNo] [nvarchar](255) NULL,
	[TypeAccount] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
end

ALTER TABLE [dbo].[BankDetails]  WITH CHECK ADD  CONSTRAINT [FK6B681666DB2A8D2E] FOREIGN KEY([PartyId])
REFERENCES [dbo].[Party] ([Id])

ALTER TABLE [dbo].[BankDetails] CHECK CONSTRAINT [FK6B681666DB2A8D2E]

