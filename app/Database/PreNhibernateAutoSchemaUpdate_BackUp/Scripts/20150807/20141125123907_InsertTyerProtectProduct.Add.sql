begin tran
/**Product **/

declare @Owner int
declare @ProductType int

select top 1 @Owner = id from dbo.Organization where TradingName = 'Auto & General'
select top 1 @ProductType = id from md.ProductType where Name = 'VAP'

INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) 
VALUES ( 0, N'Tyre Protect', @Owner, @Owner, @ProductType, N'123456', N'TYREPROTECT', CAST(0x00009CD400000000 AS DateTime), NULL)
commit
GO
/**CoverDefinition **/
begin tran
declare @CoverId int
declare @ProductId int
declare @CoverDefTypeId int

select top 1 @ProductId = p.id from Product p
inner join Organization o on o.Id = p.ProductOwnerId where p.name = 'Tyre Protect' and o.TradingName = 'Auto & General' 

select top 1 @CoverDefTypeId = id from [md].[CoverDefinitionType] where name = 'Add On'


select top 1 @CoverId = id from md.Cover where Name = 'Band A'
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Band A', @ProductId, @CoverId, NULL, @CoverDefTypeId, 0)

select top 1 @CoverId = id from md.Cover where Name = 'Band B'
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Band B', @ProductId, @CoverId, NULL, @CoverDefTypeId, 0)

select top 1 @CoverId = id from md.Cover where Name = 'Band C'
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Band C', @ProductId, @CoverId, NULL, @CoverDefTypeId, 0)

select top 1 @CoverId = id from md.Cover where Name = 'Band D'
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Band D', @ProductId, @CoverId, NULL, @CoverDefTypeId, 0)

GO
commit
GO