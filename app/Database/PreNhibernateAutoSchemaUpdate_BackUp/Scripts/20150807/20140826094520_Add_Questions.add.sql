﻿
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES		('Near Open Ground', 1, 2), -- Risk Information
			('Any Construction Work Nearby', 1, 2)

----------------------------------------------------------------------
DECLARE @id INT
DECLARE @questionType INT = 3
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Property Border', @questionType, 2) -- Risk Information

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Property Border - Park', 'Park', @id, 0),
		('Property Border - Sports field', 'Sports field', @id, 1),
		('Property Border - Golf course', 'Golf course', @id, 2),
		('Property Border - Vacant land', 'Vacant land', @id, 3),
		('Property Border - School', 'School', @id, 4),
		('Property Border - Shopping centre', 'Shopping centre', @id, 5),
		('Property Border - Small holding / farm', 'Small holding / farm', @id, 6),
		('Property Border - Informal settlement', 'Informal settlement', @id, 7),
		('Property Border - Stream / river', 'Stream / river', @id, 8),
		('Property Border - None of the above', 'None of the above', @id, 9)