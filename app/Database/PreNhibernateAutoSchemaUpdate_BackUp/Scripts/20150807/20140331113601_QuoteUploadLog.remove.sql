﻿if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_QuoteUploadLog_Product]') AND parent_object_id = OBJECT_ID('QuoteUploadLog'))
	alter table QuoteUploadLog drop constraint FK_QuoteUploadLog_Product

if exists (select * from dbo.sysobjects where id = object_id(N'QuoteUploadLog') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table QuoteUploadLog