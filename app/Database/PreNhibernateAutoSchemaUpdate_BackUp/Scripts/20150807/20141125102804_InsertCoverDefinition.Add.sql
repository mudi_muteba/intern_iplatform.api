begin tran
declare @CoverId int
declare @ProductId int
declare @CoverDefTypeId int

select top 1 @ProductId = p.id from Product p
inner join Organization o on o.Id = p.ProductOwnerId where p.name = 'Excess Protect' and o.TradingName = 'Auto & General' 

select top 1 @CoverDefTypeId = id from [md].[CoverDefinitionType] where name = 'Add On'

select top 1 @CoverId = id from md.Cover where Name = 'Band A'
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Band A', @ProductId, @CoverId, NULL, @CoverDefTypeId, 0)
select top 1 @CoverId = id from md.Cover where Name = 'Band B'
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Band B', @ProductId, @CoverId, NULL, @CoverDefTypeId, 1)
select top 1 @CoverId = id from md.Cover where Name = 'Band C'
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Band C', @ProductId, @CoverId, NULL, @CoverDefTypeId, 2)
select top 1 @CoverId = id from md.Cover where Name = 'Band D'
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Band D', @ProductId, @CoverId, NULL, @CoverDefTypeId, 3)
select top 1 @CoverId = id from md.Cover where Name = 'Band E'
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Band E', @ProductId, @CoverId, NULL, @CoverDefTypeId, 4)
select top 1 @CoverId = id from md.Cover where Name = 'Windscreen'
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Windscreen', @ProductId, @CoverId, NULL, @CoverDefTypeId, 5)
GO
commit
GO

