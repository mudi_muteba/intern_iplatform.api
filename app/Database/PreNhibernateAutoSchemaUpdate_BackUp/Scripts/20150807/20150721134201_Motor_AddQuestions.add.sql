﻿DECLARE @id INT

DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')
DECLARE @CoverID INT 
SET @CoverID=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND coverid=218)

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES		('Discount / (Loading)', 4, 7) -- Finance Information

SET @id = SCOPE_IDENTITY()

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Discount / (Loading)' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          26 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'Please select the discount or loading percentage applicable to this vehicle' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Limited Mileage', 3, 2) -- Risk Information

SET @id = SCOPE_IDENTITY()

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Limited Mileage' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          @id , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          44 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'{Title} {Name} would you be prepared to limit the mileage for the vehicle?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Limited Mileage - No Limit','0', @id, 0),
		('Limited Mileage - 5 000 KM','5000', @id, 1),
		('Limited Mileage - 10 0000 KM','10000', @id, 2)