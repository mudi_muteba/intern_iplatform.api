﻿    
	create table Configuration (
		Id INT IDENTITY NOT NULL,
       Settings NVARCHAR(MAX) NOT NULL,
       PartyId INT null,
       LastUpdatedDate DATETIME NOT NULL default(getdate()),
       primary key (Id)
    )

	alter table Configuration 
        add constraint FK_Configuration_Party
        foreign key (PartyId) 
        references Party