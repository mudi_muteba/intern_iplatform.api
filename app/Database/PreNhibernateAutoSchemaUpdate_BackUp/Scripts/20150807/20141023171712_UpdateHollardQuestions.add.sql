﻿Declare @id int
Declare @VIndex int

set @id = (select id from md.Question where name ='Voluntary Excess')

update md.QuestionAnswer set VisibleIndex=VisibleIndex+1 where QuestionId=@id and answer>1500

set @VIndex = (Select max(VisibleIndex)+1 from md.QuestionAnswer where QuestionId=@id and answer<1500)

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Voluntary Excess - 1500', '1500', @id, @VIndex)







