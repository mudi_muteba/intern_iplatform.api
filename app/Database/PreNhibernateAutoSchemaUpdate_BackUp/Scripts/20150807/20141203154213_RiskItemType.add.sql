IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLES 
            WHERE
			TABLE_SCHEMA = 'md' 
			AND TABLE_NAME = 'RiskItemType' )

BEGIN
	create table md.RiskItemType (
		Id INT IDENTITY NOT NULL,
		Name VARCHAR(50) NOT NULL UNIQUE,
		Code NVARCHAR(50) NOT NULL UNIQUE,
		primary key (Id)
	)

	 PRINT N'table md.RiskItemType added';
END	
GO
	
	IF NOT EXISTS (select top 1 * from [md].[RiskItemType] where code = 'MOTOR')
	INSERT INTO [md].[RiskItemType] ([Name],[Code]) VALUES ('Motor','MOTOR')

	IF NOT EXISTS (select top 1 * from [md].[RiskItemType] where code = 'ALLRISKS')
	INSERT INTO [md].[RiskItemType] ([Name],[Code]) VALUES ('All Risks','ALLRISKS')

	IF NOT EXISTS (select top 1 * from [md].[RiskItemType] where code = 'BUILDINGS')
	INSERT INTO [md].[RiskItemType] ([Name],[Code]) VALUES ('Buildings','BUILDINGS')

	IF NOT EXISTS (select top 1 * from [md].[RiskItemType] where code = 'CONTENT')
	INSERT INTO [md].[RiskItemType] ([Name],[Code]) VALUES ('Content','CONTENT')

	IF NOT EXISTS (select top 1 * from [md].[RiskItemType] where code = 'PERSONALLIABILITY')
	INSERT INTO [md].[RiskItemType] ([Name],[Code]) VALUES ('Personal Liability','PERSONALLIABILITY')

GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE
			TABLE_SCHEMA = 'dbo' 
			AND TABLE_NAME = 'Product' 
            AND COLUMN_NAME = 'RiskItemTypeId')
BEGIN
  alter table dbo.Product
  add [RiskItemTypeId] int null
  PRINT N'RiskItemTypeId added to product';
END

GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_Product_RiskItemType')
	BEGIN
		ALTER TABLE dbo.Product
        add constraint FK_Product_RiskItemType 
        foreign key (RiskItemTypeId) 
        references md.RiskItemType
		  PRINT N'constraint FK_Product_RiskItemType added';
	END

GO
	DECLARE @RiskItemTypeId int
	select @RiskItemTypeId = id from md.RiskItemType where code = 'MOTOR'

	update dbo.product set RiskItemTypeId = @RiskItemTypeId where productCode in 
   ('EXCESSPROTECT',
	'TOUCHUP',
	'TYRERIM',
	'TYREPROTECT')

GO
