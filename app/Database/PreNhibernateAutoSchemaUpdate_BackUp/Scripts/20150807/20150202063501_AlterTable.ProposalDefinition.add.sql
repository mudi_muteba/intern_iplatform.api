
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'ProposalDefinition' AND COLUMN_NAME = 'AssetId') 
begin
	alter table [dbo].[ProposalDefinition]
	alter column AssetId int null
end

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'ProposalHeader' AND COLUMN_NAME = 'IsDeleted') 
begin
	alter table [dbo].[ProposalHeader]
	add IsDeleted bit not null default 0
end

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'ProposalDefinition' AND COLUMN_NAME = 'IsDeleted') 
begin
	alter table [dbo].[ProposalDefinition]
	alter column IsDeleted bit not null 
end