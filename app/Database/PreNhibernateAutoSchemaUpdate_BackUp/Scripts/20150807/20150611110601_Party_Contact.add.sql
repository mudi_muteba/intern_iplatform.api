IF NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Contact')
BEGIN

	CREATE TABLE [dbo].[Contact](
		[Id] [int] NOT NULL,
		[PartyTypeId]  AS ((3)) PERSISTED NOT NULL,
		[TitleId] [int] NULL,
		[FirstName] [nvarchar](255) NULL,
		[MiddleName] [nvarchar](255) NULL,
		[Surname] [nvarchar](255) NOT NULL,
		[DateOfBirth] [date] NULL,
		[IdentityNo] [nvarchar](20) NULL,
		[PassportNo] [nvarchar](20) NULL,
		[GenderId] [int] NULL,
		[MaritalStatusId] [int] NULL,
		[HomeLanguageId] [int] NULL,
		[OccupationId] [int] NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


	SET ANSI_PADDING OFF

	ALTER TABLE [dbo].[Contact]  WITH CHECK ADD  CONSTRAINT [FK_Contact_Gender] FOREIGN KEY([GenderId])
	REFERENCES [md].[Gender] ([Id])

	ALTER TABLE [dbo].[Contact] CHECK CONSTRAINT [FK_Contact_Gender]

	ALTER TABLE [dbo].[Contact]  WITH CHECK ADD  CONSTRAINT [FK_Contact_HomeLanguage] FOREIGN KEY([HomeLanguageId])
	REFERENCES [md].[Language] ([Id])

	ALTER TABLE [dbo].[Contact] CHECK CONSTRAINT [FK_Contact_HomeLanguage]

	ALTER TABLE [dbo].[Contact]  WITH CHECK ADD  CONSTRAINT [FK_Contact_MaritalStatus] FOREIGN KEY([MaritalStatusId])
	REFERENCES [md].[MaritalStatus] ([Id])

	ALTER TABLE [dbo].[Contact] CHECK CONSTRAINT [FK_Contact_MaritalStatus]

	ALTER TABLE [dbo].[Contact]  WITH CHECK ADD  CONSTRAINT [FK_Contact_Occupation] FOREIGN KEY([OccupationId])
	REFERENCES [dbo].[Occupation] ([Id])

	ALTER TABLE [dbo].[Contact] CHECK CONSTRAINT [FK_Contact_Occupation]

	ALTER TABLE [dbo].[Contact]  WITH CHECK ADD  CONSTRAINT [FK_Contact_Party] FOREIGN KEY([Id], [PartyTypeId])
	REFERENCES [dbo].[Party] ([Id], [PartyTypeId])

	ALTER TABLE [dbo].[Contact] CHECK CONSTRAINT [FK_Contact_Party]

	ALTER TABLE [dbo].[Contact]  WITH CHECK ADD  CONSTRAINT [FK_Contact_Title] FOREIGN KEY([TitleId])
	REFERENCES [md].[Title] ([Id])

	ALTER TABLE [dbo].[Contact] CHECK CONSTRAINT [FK_Contact_Title]

	INSERT INTO [md].[PartyType]
           ([Name])
     VALUES
           ('Contact')


END