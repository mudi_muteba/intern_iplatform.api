﻿INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], 
	[RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES 
(0, N'Near Open Ground', 8, 129, NULL, 1, 21, 1, 1, 0, N'Is there open ground near the item?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], 
	[RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES 
(0, N'Any Construction Work Nearby', 8, 130, NULL, 1, 22, 1, 1, 0, N'Is there construction work near the item?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], 
	[RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES 
(0, N'Property Border', 8, 131, NULL, 1, 23, 1, 1, 0, N'Do any of these border the item?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], 
	[RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES 
(0, N'Near Open Ground', 97, 129, NULL, 1, 26, 1, 1, 0, N'Is there open ground near the item?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], 
	[RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES 
(0, N'Any Construction Work Nearby', 97, 27, NULL, 1, 22, 1, 1, 0, N'Is there construction work near the item?', N'', N'')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], 
	[RequiredForQuote], [RatingFactor], [ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES 
(0, N'Property Border', 97, 131, NULL, 1, 28, 1, 1, 0, N'Do any of these border the item?', N'', N'')

DECLARE @id INT
SET @id = (Select [id] From md.question Where Name='Property Border')

UPDATE md.QuestionAnswer Set Answer='Park' Where Questionid=@id And Name = 'Property Border - Park'
UPDATE md.QuestionAnswer Set Answer='Sports field' Where Questionid=@id And Name = 'Property Border - Sports field'
UPDATE md.QuestionAnswer Set Answer='Golf course' Where Questionid=@id And Name = 'Property Border - Golf course'
UPDATE md.QuestionAnswer Set Answer='Vacant land' Where Questionid=@id And Name = 'Property Border - Vacant land'
UPDATE md.QuestionAnswer Set Answer='School' Where Questionid=@id And Name = 'Property Border - School'
UPDATE md.QuestionAnswer Set Answer='Shopping centre' Where Questionid=@id And Name = 'Property Border - Shopping centre'
UPDATE md.QuestionAnswer Set Answer='Small holding / farm' Where Questionid=@id And Name = 'Property Border - Small holding / farm'
UPDATE md.QuestionAnswer Set Answer='Informal settlement' Where Questionid=@id And Name = 'Property Border - Informal settlement'
UPDATE md.QuestionAnswer Set Answer='Stream / river' Where Questionid=@id And Name = 'Property Border - Stream / river'
UPDATE md.QuestionAnswer Set Answer='None of the above' Where Questionid=@id And Name = 'Property Border - None of the above'
	