begin tran
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE
			TABLE_SCHEMA = 'md' 
			AND TABLE_NAME = 'Cover' 
            AND COLUMN_NAME = 'Code')
begin  
  alter table md.cover
  add [Code] nvarchar(255) null
end


GO
 
	update md.Cover set Code = REPLACE(REPLACE( UPPER(name) , ' ' , '_' ), '-' , '')


	IF NOT EXISTS( SELECT
    * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='MDCOVERCODE_UNIQUE')
	BEGIN
		ALTER TABLE md.cover
		ALTER COLUMN Code nvarchar(255)  NOT NULL 

		ALTER TABLE  md.Cover ADD CONSTRAINT
				MDCOVERCODE_UNIQUE UNIQUE NONCLUSTERED (Code)
	END


	commit
GO