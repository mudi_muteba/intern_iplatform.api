﻿-- Table
create table LeadQuality (
    Id INT IDENTITY NOT NULL,
    DateCreated DATETIME null,
    WealthIndex NVARCHAR(255) null,
    CreditGradeNonCPA NVARCHAR(255) null,
    CreditActiveNonCPA BIT null,
    MosaicCPAGroupMerged NVARCHAR(255) null,
    DemLSM NVARCHAR(255) null,
    FASNonCPAGroupDescriptionShort NVARCHAR(255) null,
    primary key (Id)
)

alter table Lead
add LeadQualityId INT null

-- Constraints
alter table Lead
    add constraint FK_Lead_LeadQuality  
    foreign key (LeadQualityId) 
    references LeadQuality