﻿----------------------------------------------------------------------
DECLARE @id INT
DECLARE @questionType INT = 3
----------------------------------------------------------------------

if not exists(select * from md.Question where Name = 'All Risk Category')
	begin
		INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
		VALUES	('All Risk Category', @questionType, 2) -- Risk Info
	end

SET @id = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('All Risk Category - Assets Out', 'Assets Out', @id ,0),
		('All Risk Category - Bikers Riding Apparel', 'Bikers Riding Apparel', @id ,1),
		('All Risk Category - Binoculars', 'Binoculars', @id ,2),
		('All Risk Category - Calculators', 'Calculators', @id ,3),
		('All Risk Category - Camping Equipment', 'Camping Equipment', @id ,4),
		('All Risk Category - Car Sound Equipment', 'Car Sound Equipment', @id ,5),
		('All Risk Category - Caravan && Camping Equipment', 'Caravan && Camping Equipment', @id ,6),
		('All Risk Category - Caravan Contents', 'Caravan Contents', @id ,7),
		('All Risk Category - Cell Car Phone', 'Cell Car Phone', @id ,8),
		('All Risk Category - Cell Phones', 'Cell Phones', @id ,9),
		('All Risk Category - Clothing/Personal Effects (Specified)', 'Clothing/Personal Effects (Specified)', @id ,10),
		('All Risk Category - Clothing/Personal Effects (Unspecified)', 'Clothing/Personal Effects (Unspecified)', @id ,11),
		('All Risk Category - Compact Discs', 'Compact Discs', @id ,12),
		('All Risk Category - Contact Lenses', 'Contact Lenses', @id ,13),
		('All Risk Category - Costume Jewellery', 'Costume Jewellery', @id ,14),
		('All Risk Category - Crystal', 'Crystal', @id ,15),
		('All Risk Category - Decoder', 'Decoder', @id ,16),
		('All Risk Category - Diving Equipment', 'Diving Equipment', @id ,17),
		('All Risk Category - Doctor Bags', 'Doctor Bags', @id ,18),
		('All Risk Category - Documents', 'Documents', @id ,19),
		('All Risk Category - DVD and Playstation Equipment', 'DVD and Playstation Equipment', @id ,20),
		('All Risk Category - Electrical Goods', 'Electrical Goods', @id ,21),
		('All Risk Category - Extended Assets Out', 'Extended Assets Out', @id ,22),
		('All Risk Category - Firearms', 'Firearms', @id ,23),
		('All Risk Category - Fishfinder', 'Fishfinder', @id ,24),
		('All Risk Category - Fishing Equipment', 'Fishing Equipment', @id ,25),
		('All Risk Category - Furs', 'Furs', @id ,26),
		('All Risk Category - General', 'General', @id ,27),
		('All Risk Category - Generators', 'Generators', @id ,28),
		('All Risk Category - Glassware && China', 'Glassware && China', @id ,29),
		('All Risk Category - Golf Clubs', 'Golf Clubs', @id ,30),
		('All Risk Category - Goods In Bank Safe', 'Goods In Bank Safe', @id ,31),
		('All Risk Category - GPS', 'GPS', @id ,32),
		('All Risk Category - Hearing Aid', 'Hearing Aid', @id ,33),
		('All Risk Category - Hearing aids and prosthesis', 'Hearing aids and prosthesis', @id ,34),
		('All Risk Category - Household Goods', 'Household Goods', @id ,35),
		('All Risk Category - IPod', 'IPod', @id ,36),
		('All Risk Category - Items kept in the Vault', 'Items kept in the Vault', @id ,37),
		('All Risk Category - Items removed from bank vault', 'Items removed from bank vault', @id ,38),
		('All Risk Category - Items Theft from Vehicles', 'Items Theft from Vehicles', @id ,39),
		('All Risk Category - Jewellery', 'Jewellery', @id ,40),
		('All Risk Category - Jewellery and watches specified', 'Jewellery and watches specified', @id ,41),
		('All Risk Category - Keys And Locks', 'Keys And Locks', @id ,42),
		('All Risk Category - Laptop', 'Laptop', @id ,43),
		('All Risk Category - Leather Garments', 'Leather Garments', @id ,44),
		('All Risk Category - Leather Goods', 'Leather Goods', @id ,45),
		('All Risk Category - Loss Of Money', 'Loss Of Money', @id ,46),
		('All Risk Category - Miscellaneous Goods', 'Miscellaneous Goods', @id ,47),
		('All Risk Category - Mobility scooters or shopriders', 'Mobility scooters or shopriders', @id ,48),
		('All Risk Category - Motor Accessories', 'Motor Accessories', @id ,49),
		('All Risk Category - Motorcycle Helmets', 'Motorcycle Helmets', @id ,50),
		('All Risk Category - Motorised and non-motorised wheelchairs', 'Motorised and non-motorised wheelchairs', @id ,51),
		('All Risk Category - Motorised Equipment', 'Motorised Equipment', @id ,52),
		('All Risk Category - Motorised Golf Carts', 'Motorised Golf Carts', @id ,53),
		('All Risk Category - Music Instruments', 'Music Instruments', @id ,54),
		('All Risk Category - Musical Instruments', 'Musical Instruments', @id ,55),
		('All Risk Category - Musical Instruments - Professional', 'Musical Instruments - Professional', @id ,56),
		('All Risk Category - Navigational Equipment', 'Navigational Equipment', @id ,57),
		('All Risk Category - Off Road Motor Bikes', 'Off Road Motor Bikes', @id ,58),
		('All Risk Category - On Road Motor Bikes', 'On Road Motor Bikes', @id ,59),
		('All Risk Category - Opthalmic Glasses', 'Opthalmic Glasses', @id ,60),
		('All Risk Category - Other', 'Other', @id ,61),
		('All Risk Category - Paintings/Books/Pictures', 'Paintings/Books/Pictures', @id ,62),
		('All Risk Category - Parachutes, paragliders and hang gliders', 'Parachutes, paragliders and hang gliders', @id ,63),
		('All Risk Category - Pedal Cycles', 'Pedal Cycles', @id ,64),
		('All Risk Category - Persian Rugs', 'Persian Rugs', @id ,65),
		('All Risk Category - Personal Computer', 'Personal Computer', @id ,66),
		('All Risk Category - Photo Equipment', 'Photo Equipment', @id ,67),
		('All Risk Category - Portable Radio/Cassette/Cd Players', 'Portable Radio/Cassette/Cd Players', @id ,68),
		('All Risk Category - Portable Tv', 'Portable Tv', @id ,69),
		('All Risk Category - Prosthetics/Medical Aids', 'Prosthetics/Medical Aids', @id ,70),
		('All Risk Category - Quad Bikes - Non Road Licensed', 'Quad Bikes - Non Road Licensed', @id ,71),
		('All Risk Category - Quad Bikes - Restricted Cover', 'Quad Bikes - Restricted Cover', @id ,72),
		('All Risk Category - Radio / Communication Equipment', 'Radio / Communication Equipment', @id ,73),
		('All Risk Category - Radios', 'Radios', @id ,74),
		('All Risk Category - Satellite Dishes', 'Satellite Dishes', @id ,75),
		('All Risk Category - Ski Equipment', 'Ski Equipment', @id ,76),
		('All Risk Category - Solar Panel', 'Solar Panel', @id ,77),
		('All Risk Category - Specified', 'Specified', @id ,78),
		('All Risk Category - Specified Contents', 'Specified Contents', @id ,79),
		('All Risk Category - Spectacles/Sunglasses', 'Spectacles/Sunglasses', @id ,80),
		('All Risk Category - Sports', 'Sports', @id ,81),
		('All Risk Category - Sports Equipment', 'Sports Equipment', @id ,82),
		('All Risk Category - Stamp & Coin Collections', 'Stamp & Coin Collections', @id ,83),
		('All Risk Category - Swimming Pool Equipment', 'Swimming Pool Equipment', @id ,84),
		('All Risk Category - Tools / Hand Tools', 'Tools / Hand Tools', @id ,85),
		('All Risk Category - Transport Of Groceries And Household Goods', 'Transport Of Groceries And Household Goods', @id ,86),
		('All Risk Category - Two Way Radios', 'Two Way Radios', @id ,87),
		('All Risk Category - Video Camera', 'Video Camera', @id ,88),
		('All Risk Category - Video Machine (Vcr)', 'Video Machine (Vcr)', @id ,89),
		('All Risk Category - Watches', 'Watches', @id ,90),
		('All Risk Category - Wheelchairs', 'Wheelchairs', @id ,91),
		('All Risk Category - Zippy Nippys / Kiddies Cycles', 'Zippy Nippys / Kiddies Cycles', @id ,92)

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		