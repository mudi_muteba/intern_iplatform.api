﻿	-- Tables
    create table Party (
        Id INT IDENTITY NOT NULL,
		MasterId INT null,
		PartyTypeId INT not null,
		ContactDetailId INT null,
		DisplayName NVARCHAR(550) NOT NULL,
		DateCreated DATETIME default (getdate()),
		DateUpdated DATETIME default (getdate()),
		primary key (Id)
    )

    create table md.PartyType (
        Id INT IDENTITY NOT NULL,
		Name NVARCHAR(30) NOT NULL UNIQUE,
		primary key (Id)
    )

    create table Relationship (
        PartyId INT not null,
		ParentPartyId INT not null,
		MasterId INT null,
		RelationshipTypeId INT not null,
		primary key (PartyId, ParentPartyId)
    )

    create table md.RelationshipType (
        Id INT IDENTITY NOT NULL,
		Name NVARCHAR(30) NOT NULL UNIQUE,
		primary key (Id)
    )

    create table [Role] (
		PartyId INT not null,
		RoleTypeId INT not null,
		MasterId INT null,
		primary key (PartyId, RoleTypeId)
    )

    create table md.RoleType (
        Id INT IDENTITY NOT NULL,
		Name NVARCHAR(30) NOT NULL UNIQUE,
		primary key (Id)
    )

	-- Constraints
    alter table Party 
        add constraint FK_Party_PartyType 
        foreign key (PartyTypeId) 
        references md.PartyType

    alter table Party 
		add constraint Party_UC unique (id, PartyTypeId) --Custom Constraint

    alter table Relationship 
        add constraint FK_Relationship_Party 
        foreign key (PartyId) 
        references Party
		
    alter table [Role]
        add constraint FK_Role_Party
        foreign key (PartyId) 
        references Party

    alter table [Role]
        add constraint FK_Role_RoleType 
        foreign key (RoleTypeId) 
        references md.RoleType

    alter table Relationship 
        add constraint FK_Relationship_ParentParty 
        foreign key (ParentPartyId) 
        references Party

    alter table Relationship 
        add constraint FK_Relationship_RelationshipType
        foreign key (RelationshipTypeId) 
        references md.RelationshipType

	alter table Party 
    add constraint FK_Party_ContactDetail
    foreign key (ContactDetailId) 
    references ContactDetail (Id)    