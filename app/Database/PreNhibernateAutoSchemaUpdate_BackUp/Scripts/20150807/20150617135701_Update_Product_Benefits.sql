update ProductBenefit
set ShowToClient = 1
where value != 'copy from system'
	and isnull(ltrim(rtrim(value)), '') != ''

update ProductBenefit
set ShowToClient = 0
where value = 'copy from system'
	or isnull(ltrim(rtrim(value)), '') = ''


update ProductBenefit
set Value = 'If requested'
where value = 'if requested'

update ProductBenefit
set Value = 'Yes'
where value = 'yes'

update ProductBenefit
set Value = 'No'
where value = 'No'

update ProductBenefit
set Value = 'Not included'
where value = 'not included'

update ProductBenefit
set Value = 'Must be specified'
where value = 'must be specified'

update ProductBenefit
set Value = 'Renewal'
where value = 'renewal'
