
if exists(select * from sys.columns where Name = N'GroupTypeId' and Object_ID = Object_ID(N'QuestionDefinition'))
begin
	EXEC sp_RENAME 'QuestionDefinition.GroupTypeId' , 'QuestionDefinitionGroupTypeId', 'COLUMN'
end