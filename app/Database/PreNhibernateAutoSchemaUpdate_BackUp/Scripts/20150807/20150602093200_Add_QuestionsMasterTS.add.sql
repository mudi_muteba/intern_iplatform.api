﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')
DECLARE @CoverID INT 
SET @CoverID=(SELECT ID FROM dbo.CoverDefinition WHERE ProductId=@ProductID AND coverid=218)

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Valuation Method' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          65 , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          43 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'Please select the valuation method?' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Vehicle Daytime Access Control' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          136 , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          18 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'Please select the access control for daytime parking' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

INSERT INTO dbo.QuestionDefinition
        ( MasterId ,
          DisplayName ,
          CoverDefinitionId ,
          QuestionId ,
          ParentId ,
          QuestionDefinitionTypeId ,
          VisibleIndex ,
          RequiredForQuote ,
          RatingFactor ,
          ReadOnly ,
          ToolTip ,
          DefaultValue ,
          RegexPattern
        )
VALUES  ( 0 , -- MasterId - int
          N'Vehicle Overnight Access Control' , -- DisplayName - nvarchar(255)
          @CoverID , -- CoverDefinitionId - int
          137 , -- QuestionId - int
          NULL , -- ParentId - int
          1 , -- QuestionDefinitionTypeId - int
          10 , -- VisibleIndex - int
          1 , -- RequiredForQuote - bit
          1 , -- RatingFactor - bit
          0 , -- ReadOnly - bit
          N'Please select the access control for overnight parking' , -- ToolTip - nvarchar(255)
          N'' , -- DefaultValue - nvarchar(255)
          N''  -- RegexPattern - nvarchar(255)
        )

UPDATE dbo.QuestionDefinition SET VisibleIndex=11 WHERE QuestionId=8 AND CoverDefinitionId=@CoverID 
UPDATE dbo.QuestionDefinition SET VisibleIndex=12 WHERE QuestionId=9 AND CoverDefinitionId=@CoverID 