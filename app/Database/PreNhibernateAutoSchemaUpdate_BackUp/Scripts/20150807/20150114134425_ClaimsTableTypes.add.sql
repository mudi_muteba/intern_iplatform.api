﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

INSERT INTO md.ClaimsQuestionType(Name) 
VALUES		('Checkbox'), 
			('Date'), 
			('Dropdown'), 
			('Textbox'), 
			('LicenceScan'), 
			('VehicleLicenceScan'),
			('Draw'),
			('Signature'),
			('SelectFromGallery'),
			('TextboxNo')