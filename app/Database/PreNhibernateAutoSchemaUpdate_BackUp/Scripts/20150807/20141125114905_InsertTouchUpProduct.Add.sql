begin tran
/**Cover**/

SET IDENTITY_INSERT md.Cover ON
GO
IF NOT EXISTS (select * from md.Cover where name = 'Touch Up')
INSERT [md].[Cover] ([Id],[Name]) VALUES (368, N'Touch Up')
SET IDENTITY_INSERT md.Cover OFF
GO
/**Product **/

declare @Owner int
declare @ProductType int

select top 1 @Owner = id from dbo.Organization where TradingName = 'Auto & General'
select top 1 @ProductType = id from md.ProductType where Name = 'VAP'

INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) 
VALUES ( 0, N'Touch Up', @Owner, @Owner, @ProductType, N'123456', N'TOUCHUP', CAST(0x00009CD400000000 AS DateTime), NULL)

/**CoverDefinition **/

declare @CoverId int
declare @ProductId int
declare @CoverDefTypeId int

select top 1 @ProductId = p.id from Product p
inner join Organization o on o.Id = p.ProductOwnerId where p.name = 'Touch Up' and o.TradingName = 'Auto & General' 

select top 1 @CoverDefTypeId = id from [md].[CoverDefinitionType] where name = 'Add On'
select top 1 @CoverId = id from md.Cover where Name = 'Touch Up'

INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Touch Up', @ProductId, @CoverId, NULL, @CoverDefTypeId, 0)
GO
commit
GO