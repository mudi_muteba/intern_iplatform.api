﻿IF NOT EXISTS (SELECT * FROM sys.procedures WHERE Name = 'usp_AgentLeadsSoldPreviousFrequency')

EXEC(
'CREATE PROCEDURE [dbo].[usp_AgentLeadsSoldPreviousFrequency] 
(
@Frequency Varchar(10),
@Value INT,
@CampaignId INT
)

AS
BEGIN

declare @PrevValue INT
set @PrevValue = @Value -1

IF @Frequency = ''Daily'' and @Value = 0
BEGIN
SET @PrevValue = @Value -3
END


---Frequency

declare @fromDate datetime
declare @todate datetime

IF @Frequency = ''Daily'' 
Begin
Set @fromDate = dateadd(wk, datediff(wk, 0, getdate()), @PrevValue)
Set @toDate = dateadd(wk, datediff(wk, 0, getdate()), @PrevValue) + ''23:59:59.000''
End;

IF @Frequency = ''Weekly''
Begin 
Set @fromDate = dateadd(wk,-2,dateadd(wk, datediff(wk, 0, getdate()), @Value))   ---7 Days from selected day
Set @toDate = dateadd(wk,-1,dateadd(wk, datediff(wk, 0, getdate()) , @Value -1)) + ''23:59:59.000''
End;

IF @Frequency = ''Monthly''
Begin
Set @FromDate = DateAdd(M, -1,Convert(datetime,Cast(Year(Getdate()) as Varchar(4))  + ''-'' + Cast(@Value As Varchar(4)) + ''-01''))
Set @ToDate =   DATEADD(DAY, -(DAY(DATEADD(MONTH, 1, @FromDate))),DATEADD(MONTH, 1, @FromDate))
End;

IF @Frequency = ''Yearly''
Begin 
Set @Fromdate = Convert(Datetime,Convert(Varchar(4),@PrevValue) + ''-01-01'')
Set @Todate = Convert(Datetime,Convert(Varchar(4),@PrevValue) + ''-12-31 23:59:59.000'')
END


Select (select DisplayName from Party where Id = La.PartyId)  ''User'',
      Ls.Name LeadStatus,
      Count(CampaignId) Leadcount
FROM md.leadstatus ls
INNER JOIN LeadActivity la ON ls .Id = la.LeadStatusId
INNER JOIN lead l ON la.LeadId = l.id
INNER JOIN party P ON L.PartyId = p.Id
INNER JOIN Campaign C ON la.CampaignId = c.Id
LEFT OUTER JOIN Role R ON P.id = R.PartyId
LEFT OUTER JOIN md.RoleType RT ON R.RoleTypeId = RT.Id
WHERE isnull(@CampaignId, C.Id) = C.Id
  AND dateadd(d,0,Datediff(d,0, LA.DateCreated)) Between @Fromdate and @Todate
GROUP BY La.PartyId, Ls.Name


END')