﻿if not exists(select * from sys.columns where Name = N'IsDeleted' and Object_ID = Object_ID(N'Quote'))
begin
	alter table [dbo].[Quote]
	add IsDeleted bit null
end