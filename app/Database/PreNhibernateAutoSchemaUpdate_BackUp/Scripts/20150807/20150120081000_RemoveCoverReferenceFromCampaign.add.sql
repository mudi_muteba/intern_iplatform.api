﻿if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_Campaign_Cover]') AND parent_object_id = OBJECT_ID('Campaign'))
	begin
		alter table Campaign
			drop constraint FK_Campaign_Cover
	end

alter table Campaign
	alter column CoverId int null
