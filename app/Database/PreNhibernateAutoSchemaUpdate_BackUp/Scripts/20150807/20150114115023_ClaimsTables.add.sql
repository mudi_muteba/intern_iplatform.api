﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if NOT exists (select * from dbo.sysobjects where id = object_id(N'ClaimsQuestionGroup') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
	CREATE TABLE [md].[ClaimsQuestionGroup](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Name] [nvarchar](80) NOT NULL,
		[VisibleIndex] [int] NOT NULL,
	 CONSTRAINT [PK_ClaimsQuestionGroup] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
End

if NOT exists (select * from dbo.sysobjects where id = object_id(N'ClaimsQuestionType') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
	CREATE TABLE [md].[ClaimsQuestionType](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Name] [nvarchar](20) NOT NULL,
	 CONSTRAINT [PK_ClaimsQuestionType] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
End

if NOT exists (select * from dbo.sysobjects where id = object_id(N'ClaimsQuestion') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
	CREATE TABLE [md].[ClaimsQuestion](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Name] [nvarchar](200) NOT NULL,
		[ClaimsQuestionTypeId] [int] NOT NULL,
		[ClaimsQuestionGroupId] [int] NOT NULL,
	 CONSTRAINT [PK_ClaimsQuestion] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [md].[ClaimsQuestion]  WITH CHECK ADD  CONSTRAINT [FK_ClaimsQuestion_QuestionGroup] FOREIGN KEY([ClaimsQuestionGroupId])
	REFERENCES [md].[ClaimsQuestionGroup] ([Id])

	ALTER TABLE [md].[ClaimsQuestion] CHECK CONSTRAINT [FK_ClaimsQuestion_QuestionGroup]

	ALTER TABLE [md].[ClaimsQuestion]  WITH CHECK ADD  CONSTRAINT [FK_ClaimsQuestion_QuestionType] FOREIGN KEY([ClaimsQuestionTypeId])
	REFERENCES [md].[ClaimsQuestionType] ([Id])

	ALTER TABLE [md].[ClaimsQuestion] CHECK CONSTRAINT [FK_ClaimsQuestion_QuestionType]
End


INSERT INTO md.ClaimsQuestionGroup(Name, VisibleIndex) 
VALUES		('Motor Theft', 1), 
			('Motor Accident', 1), 
			('Public Liability', 1), 
			('Personal Liability', 1), 
			('Personal Accident', 1), 
			('Theft Of Property Out Of Motor', 1), 
			('Non-Motor Claims Up To R10 000', 1), 
			('Property Loss / Damage', 1), 
			('Marine / GIT', 1), 
			('Pleasure Craft', 1), 
			('Contract Works', 1), 
			('Insured', 2), 
			('Vehicle', 3),
			('Driver', 4),
			('Passengers', 5),
			('Other Party', 6),
			('Witnesses', 7),
			('Accident', 8),
			('Police', 9),
			('Accident Description', 10),
			('Accident Declaration', 11),
			('Inspection', 12),
			('Theft Declaration', 13),
			('Supporting Documentation', 14)

