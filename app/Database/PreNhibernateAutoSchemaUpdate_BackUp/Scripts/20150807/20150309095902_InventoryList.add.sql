IF ( NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'InventoryList'))
BEGIN

CREATE TABLE [dbo].[InventoryList](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InventoryGroup] [varchar](255) NULL,
	[InventoryList] [varchar](255) NULL,
 CONSTRAINT [PK_temp_inventorylisting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

  INSERT INTO [InventoryList] VALUES ('Bedrooms','Bed & Mattress');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Bedside Radio/Clock');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Clothing & Footwear');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Curtains & Loose Carpets');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Furs & Jewellry');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Heaters');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Linen Blankets & Bedding');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Painting & Ornaments');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Reading Lamps');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Suitcases');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','TV Set/Video');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Tables and Chairs');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Toys & Games');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Wardrobe/Dressing Table');
  INSERT INTO [InventoryList] VALUES ('Bedrooms','Other');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Books');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Cabinets/Cupboards');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Cameras & Projector');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Computer Equipment/Typewriter');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Curtains & Loose Carpets');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Desk & Bookcase');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Firearms & Binoculars');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Heaters');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Knitting Machine');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Painting & Ornaments');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Reading Lamps');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Sewing Machine');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Sports Equipment');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Table & Chairs');
  INSERT INTO [InventoryList] VALUES ('Study/Workroom','Other');
  INSERT INTO [InventoryList] VALUES ('Bathroom/Toilet','Towels & Toiletries');
  INSERT INTO [InventoryList] VALUES ('Bathroom/Toilet','Hairdryer');
  INSERT INTO [InventoryList] VALUES ('Bathroom/Toilet','Shaving Equipment');
  INSERT INTO [InventoryList] VALUES ('Bathroom/Toilet','Curtains & Loose Carpets');
  INSERT INTO [InventoryList] VALUES ('Bathroom/Toilet','Other');
  INSERT INTO [InventoryList] VALUES ('Laundry','Iron/Ironing Board');
  INSERT INTO [InventoryList] VALUES ('Laundry','Linen Stored');
  INSERT INTO [InventoryList] VALUES ('Laundry','Tumble Dryer');
  INSERT INTO [InventoryList] VALUES ('Laundry','Washing Machine');
  INSERT INTO [InventoryList] VALUES ('Laundry','Other');
  INSERT INTO [InventoryList] VALUES ('Entrance/Passage','Loose Carpets');
  INSERT INTO [InventoryList] VALUES ('Entrance/Passage','Linen Stored');
  INSERT INTO [InventoryList] VALUES ('Entrance/Passage','Paintings & Ornaments');
  INSERT INTO [InventoryList] VALUES ('Entrance/Passage','Tables & Chairs');
  INSERT INTO [InventoryList] VALUES ('Entrance/Passage','Other');
  INSERT INTO [InventoryList] VALUES ('Lounge','Curtains & Loose Carpets');
  INSERT INTO [InventoryList] VALUES ('Lounge','Display Articles');
  INSERT INTO [InventoryList] VALUES ('Lounge','Display Cabinet');
  INSERT INTO [InventoryList] VALUES ('Lounge','Heaters');
  INSERT INTO [InventoryList] VALUES ('Lounge','Hi-fi & Tape Deck');
  INSERT INTO [InventoryList] VALUES ('Lounge','Liquor & Glass Set');
  INSERT INTO [InventoryList] VALUES ('Lounge','Lounge Suite');
  INSERT INTO [InventoryList] VALUES ('Lounge','Paintings & Ornaments');
  INSERT INTO [InventoryList] VALUES ('Lounge','Reading Lamps');
  INSERT INTO [InventoryList] VALUES ('Lounge','Records/CD''s/Tapes');
  INSERT INTO [InventoryList] VALUES ('Lounge','TV/Video & Decoder');
  INSERT INTO [InventoryList] VALUES ('Lounge','Tables & Chairs');
  INSERT INTO [InventoryList] VALUES ('Lounge','Other');
  INSERT INTO [InventoryList] VALUES ('Family Room','Curtains & Loose Carpets');
  INSERT INTO [InventoryList] VALUES ('Family Room','Heaters');
  INSERT INTO [InventoryList] VALUES ('Family Room','Hi-fi & Tape Deck');
  INSERT INTO [InventoryList] VALUES ('Family Room','Liquor & Glass Set');
  INSERT INTO [InventoryList] VALUES ('Family Room','Musical Instruments');
  INSERT INTO [InventoryList] VALUES ('Family Room','Paintings & Ornaments');
  INSERT INTO [InventoryList] VALUES ('Family Room','Personal Computer');
  INSERT INTO [InventoryList] VALUES ('Family Room','Reading Lamps');
  INSERT INTO [InventoryList] VALUES ('Family Room','Records/CD''s/Tapes');
  INSERT INTO [InventoryList] VALUES ('Family Room','TV/Video & Video Games');
  INSERT INTO [InventoryList] VALUES ('Family Room','Tables & Chairs');
  INSERT INTO [InventoryList] VALUES ('Family Room','Other');
  INSERT INTO [InventoryList] VALUES ('Dining Room','Curtains & Loose Carpets');
  INSERT INTO [InventoryList] VALUES ('Dining Room','Cutlery & Silverware');
  INSERT INTO [InventoryList] VALUES ('Dining Room','Dinner Service');
  INSERT INTO [InventoryList] VALUES ('Dining Room','Dress & Sideboard');
  INSERT INTO [InventoryList] VALUES ('Dining Room','Glassware');
  INSERT INTO [InventoryList] VALUES ('Dining Room','Heaters');
  INSERT INTO [InventoryList] VALUES ('Dining Room','Hot Tray');
  INSERT INTO [InventoryList] VALUES ('Dining Room','Paintings & Ornaments');
  INSERT INTO [InventoryList] VALUES ('Dining Room','Reading Lamps');
  INSERT INTO [InventoryList] VALUES ('Dining Room','Tables & Chairs');
  INSERT INTO [InventoryList] VALUES ('Dining Room','Tea Trolley');
  INSERT INTO [InventoryList] VALUES ('Dining Room','Other');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Cutlery & Crockery');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Dishwasher');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Electrical Appliances');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Freezer & Contents');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Fridge & Contents');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Furniture & Curtains');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Groceries');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Microwave Oven');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Mixer & Blender');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Stove');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Utensils');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Vacuum & Polisher');
  INSERT INTO [InventoryList] VALUES ('Kitchen','Other');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Bicycles');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Braai Equipment');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Camping Equipment');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Garden Furniture');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Garden Implements');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Hand Tools');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Lawnmower & Weedeater');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Power Tools');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Swimming Pool Equipment');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Welding Equipment');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Woodworking Equipment');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Workbench & Vice');
  INSERT INTO [InventoryList] VALUES ('Garage & Workshop','Other');
  INSERT INTO [InventoryList] VALUES ('Domestic Quarters','Bed & Mattress');
  INSERT INTO [InventoryList] VALUES ('Domestic Quarters','Curtains & Loose Carpets');
  INSERT INTO [InventoryList] VALUES ('Domestic Quarters','Linen, Blankets, Bedding');
  INSERT INTO [InventoryList] VALUES ('Domestic Quarters','Painting & Ornaments');
  INSERT INTO [InventoryList] VALUES ('Domestic Quarters','Radio & TV');
  INSERT INTO [InventoryList] VALUES ('Domestic Quarters','Tables & Chairs');
  INSERT INTO [InventoryList] VALUES ('Domestic Quarters','Wardrobe');
  INSERT INTO [InventoryList] VALUES ('Domestic Quarters','Other');
 

  END