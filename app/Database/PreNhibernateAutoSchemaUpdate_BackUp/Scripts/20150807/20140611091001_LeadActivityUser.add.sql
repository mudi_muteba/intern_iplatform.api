﻿-- table
ALTER TABLE LeadActivity ADD PartyId INT NULL

-- constraint
ALTER TABLE LeadActivity
    ADD CONSTRAINT FK_LeadActivity_User 
    FOREIGN KEY (PartyId) 
    REFERENCES Party