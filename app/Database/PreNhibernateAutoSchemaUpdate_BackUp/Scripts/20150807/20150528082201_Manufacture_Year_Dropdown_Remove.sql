﻿declare @questionId int
select @questionId = id from md.Question where name = 'Year Of Manufacture'

delete from md.QuestionAnswer where QuestionId = @questionId


declare @questionDefinitionId int 
select @questionDefinitionId = id from QuestionDefinition where questionId = 99 and CoverDefinitionId = 96

update ProposalQuestionAnswer set QuestionTypeId = 3 where QuestionDefinitionId = @questionDefinitionId