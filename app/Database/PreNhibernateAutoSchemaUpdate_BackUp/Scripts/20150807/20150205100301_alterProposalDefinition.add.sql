IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'ProposalDefinition' AND COLUMN_NAME = 'Description') 
begin
	alter table [dbo].[ProposalDefinition]
	add Description nvarchar(100) null
end

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'Asset' AND COLUMN_NAME = 'Description') 
begin
	alter table [dbo].[Asset]
	alter column Description nvarchar(100) null
END