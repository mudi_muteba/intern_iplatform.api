﻿IF NOT EXISTS (SELECT * FROM sys.procedures WHERE Name = 'ursp_proposalform_HouseholdContents')

EXEC(
'CREATE procedure [dbo].[ursp_proposalform_HouseholdContents]
@ProductId int

as
begin




  CREATE TABLE #TEMP
  (Id INT, 
   Value VARCHAR(250));

   INSERT INTO #TEMP Values (90, ''2021'');
   INSERT INTO #TEMP Values (89, ''Bryanston'');
   INSERT INTO #TEMP Values (47, ''Gauteng'');



With HHC as
(
SELECT 1 ItemNumber,
       UPPER(qg.Name) Name, 
       REPLACE(REPLACE(REPLACE(qd.Displayname,''Postal Code'',''Risk Address''), ''Suburb'', ''Risk Address''),''Province'', ''Risk Address'') Displayname,
	   Value,
       qg.VisibleIndex VisibleIndexqg,
	   qd.VisibleIndex VisibleIndexqd,
	   t.Id
FROM questiondefinition qd 
INNER JOIN md.question q on q.id = qd.questionid 
INNER JOIN md.questiongroup qg on qg.id = q.questiongroupid
INNER JOIN coverdefinition cd on qd.coverdefinitionid = cd.id
LEFT JOIN #Temp t on qd.questionid = t.Id
WHERE cd.coverid = 78
  AND productid = @ProductId 

)


SELECT  ItemNumber, Name, Displayname,
                SUBSTRING((
				          SELECT '', ''+Value AS [text()]
						  FROM HHC
						  WHERE HHC.Displayname = HHC2.Displayname
						    AND HHC.ItemNumber = HHC2.ItemNumber
							order by id
						  FOR XML PATH ('''')
						  ), 2, 1000) [Value],
						  VisibleIndexqg, MAX(VisibleIndexqd) VisibleIndexqd

FROM HHC HHC2 
GROUP BY ItemNumber, Name, Displayname, VisibleIndexqg
ORDER BY Itemnumber, VisibleIndexqg, MAX(VisibleIndexqd) DESC
DROP TABLE #TEMP;

END')