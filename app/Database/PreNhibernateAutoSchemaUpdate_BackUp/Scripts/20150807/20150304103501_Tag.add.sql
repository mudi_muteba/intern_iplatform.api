
IF ( NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Tag'))
BEGIN

	/****** Object:  Table [dbo].[Tag]    Script Date: 04/03/2015 10:21:41 ******/

	CREATE TABLE [dbo].[Tag](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Name] [nvarchar](50) NOT NULL,
	 CONSTRAINT [PK_Tags] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


END

IF ( NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'TagProductMap'))
BEGIN
/****** Object:  Table [dbo].[TagCampaignMap]    Script Date: 04/03/2015 10:21:41 ******/

	CREATE TABLE [dbo].[TagProductMap](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[TagId] [int] NOT NULL,
		[ProductId] [int] NOT NULL,
	 CONSTRAINT [PK_TagProductMap] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

END

GO

IF ( NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'TagCampaignMap'))
BEGIN
	/****** Object:  Table [dbo].[TagProductMap]    Script Date: 04/03/2015 10:21:41 ******/

	CREATE TABLE [dbo].[TagCampaignMap](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[TagId] [int] NOT NULL,
		[CampaignId] [int] NOT NULL,
	 CONSTRAINT [PK_TagCampaignMap] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

--ALTER TABLE [dbo].[TagCampaignMap] CHECK CONSTRAINT [FK_TagCampaignMap_Campaign]
GO
if not exists (select * from dbo.sysobjects where id = object_id(N'FK_TagCampaignMap_Tag'))
begin
	ALTER TABLE [dbo].[TagCampaignMap]  WITH CHECK ADD  CONSTRAINT [FK_TagCampaignMap_Tag] FOREIGN KEY([TagId])
	REFERENCES [dbo].[Tag] ([Id])
end

GO
if not exists (select * from dbo.sysobjects where id = object_id(N'FK_TagProductMap_Product'))
begin
	ALTER TABLE [dbo].[TagProductMap]  WITH CHECK ADD  CONSTRAINT [FK_TagProductMap_Product] FOREIGN KEY([ProductId])
	REFERENCES [dbo].[Product] ([Id])
end
GO
if not exists (select * from dbo.sysobjects where id = object_id(N'FK_TagProductMap_Tag'))
begin
	ALTER TABLE [dbo].[TagProductMap]  WITH CHECK ADD  CONSTRAINT [FK_TagProductMap_Tag] FOREIGN KEY([TagId])
	REFERENCES [dbo].[Tag] ([Id])
end
GO
