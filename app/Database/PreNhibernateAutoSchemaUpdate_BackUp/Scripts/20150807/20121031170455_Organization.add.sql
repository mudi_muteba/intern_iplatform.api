﻿
    create table Organization (
        Id INT NOT NULL,
		PartyTypeId as 2 persisted, -- Organization
		Code NVARCHAR(20) null,
		RegisteredName NVARCHAR(500) null,
		TradingName NVARCHAR(500) NOT NULL,
		TradingSince DATE null,
		Description NVARCHAR(2000) null,
		RegNo NVARCHAR(20) null,
		FspNo NVARCHAR(20) null,
		VatNo NVARCHAR(20) null,
		primary key (Id)
    )

	alter table Organization 
        add constraint FK_Organization_Party
        foreign key (Id, PartyTypeId) 
        references Party(Id, PartyTypeId) --Custom Columns