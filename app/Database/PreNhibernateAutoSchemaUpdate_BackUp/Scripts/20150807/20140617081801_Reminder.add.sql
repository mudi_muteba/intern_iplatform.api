﻿-- Tables
create table Reminder (
    Id INT IDENTITY(1,1) NOT NULL,
	RemindAt DateTime NOT NULL,
	Reason NVARCHAR(255) NULL,
	primary key (Id))

alter table LeadActivity
	ADD ReminderId int null

-- Constraint
alter table LeadActivity 
    add constraint FK_LeadActivity_Reminder
    foreign key (ReminderId) 
    references Reminder