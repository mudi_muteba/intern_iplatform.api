﻿Declare @id int
Declare @VIndex int

set @id = (select id from md.Question where name ='Vehicle Overnight Parking')

set @VIndex = (Select max(VisibleIndex)+1 from md.QuestionAnswer where QuestionId=@id)

Update md.QuestionAnswer Set Visibleindex=@VIndex Where Name='Vehicle Overnight Parking - None' And QuestionID=@id

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Vehicle Overnight Parking - Basement', 'Basement', @id, @VIndex-1)
		



