

UPDATE SecondLevelQuestionDefinition
SET DisplayName = 'Have you ever had an insurance policy cancelled or refused renewal'
WHERE SecondLevelQuestionId = 1

UPDATE SecondLevelQuestionDefinition
SET DisplayName = 'Have you ever been, or are currently under debt review or administration'
WHERE SecondLevelQuestionId = 2

UPDATE SecondLevelQuestionDefinition
SET DisplayName = 'Have you ever been, or are you currently under and defaults or judgements'
WHERE SecondLevelQuestionId = 3

UPDATE SecondLevelQuestionDefinition
SET DisplayName = 'Have you ever been, or are you currently under and insolvency, sequestration or liquidation'
WHERE SecondLevelQuestionId = 4