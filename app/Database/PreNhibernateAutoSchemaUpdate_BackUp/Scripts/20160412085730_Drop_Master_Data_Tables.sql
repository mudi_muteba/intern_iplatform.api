﻿DROP TABLE [md].[Country]
DROP TABLE [md].[ProductLineOfLoss]
DROP TABLE [md].[HomeTypeOfLoss]
DROP TABLE [md].[MotorTypeOfLoss]
DROP TABLE [md].[HomeClaimAmount]
DROP TABLE [md].[MotorClaimAmount]
DROP TABLE [md].[HomeClaimLocation]
DROP TABLE [md].[CurrentlyInsured]
DROP TABLE [md].[ProductFeeType]
DROP TABLE [md].[UninterruptedPolicy]
DROP TABLE [md].[PaymentPlan]
DROP TABLE [md].[MotorCurrentTypeOfCover]
DROP TABLE [md].[VehicleType]
DROP TABLE [md].[MotorUninterruptedPolicyNoClaim]
DROP TABLE [md].[FinanceHouse]
DROP TABLE [md].[PartyType]
DROP TABLE [md].[InsurersBranchType]
DROP TABLE [md].[AccessoryType]
DROP TABLE [md].[RelationshipType]
DROP TABLE [md].[AuthorisationGroup]
DROP TABLE [md].[OrganizationType]
DROP TABLE [md].[RoleType]
DROP TABLE [md].[RiskItemType]
DROP TABLE [md].[LeadStatus]
DROP TABLE [md].[CampaignSource]
DROP TABLE [md].[Gender]
DROP TABLE [md].[Title]
DROP TABLE [md].[ClaimsQuestionGroup]
DROP TABLE [md].[ClaimsQuestionType]
DROP TABLE [md].[ClaimsQuestion]
DROP TABLE [md].[Language]
DROP TABLE [md].[MaritalStatus]
DROP TABLE [md].[ClaimsType]
DROP TABLE [md].[ClaimsQuestionAnswer]
DROP TABLE [md].[ClaimsQuestionDefinition]
DROP TABLE [md].[ProductType]
DROP TABLE [md].[AuditType]
DROP TABLE [md].[Cover]
DROP TABLE [md].[CoverDefinitionType]
DROP TABLE [md].[CorrespondenceType]
DROP TABLE [md].[SecondLevelQuestionAnswer]
DROP TABLE [md].[QuestionAnswer]
DROP TABLE [md].[SecondLevelQuestion]
DROP TABLE [md].[QuestionDefinitionGroupType]
DROP TABLE [md].[Question]
DROP TABLE [md].[SecondLevelQuestionGroup]
DROP TABLE [md].[QuestionType]
DROP TABLE [md].[QuestionGroup]
DROP TABLE [md].[QuestionDefinitionType]
DROP TABLE [md].[PolicyStatus]
DROP TABLE [md].[Currency]
DROP TABLE [md].[MethodOfPayment]
DROP TABLE [md].[AddressType]
DROP TABLE [md].[ActivityType]
DROP TABLE [md].[StateProvince]
DROP TABLE [md].[MemberRelationship]