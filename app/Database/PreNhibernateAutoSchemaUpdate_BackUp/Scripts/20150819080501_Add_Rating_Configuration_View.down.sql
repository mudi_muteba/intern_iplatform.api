﻿SET QUOTED_IDENTIFIER ON
GO

if exists(select * from sys.objects where name = 'vw_rating_configuration' and type = 'V')
	begin
		drop view dbo.vw_rating_configuration
	end
go
