﻿UPDATE quoteHeader
SET RequestId = cast(externalReference as uniqueIdentifier)
from quoteHeader
where externalReference LIKE 
       REPLACE('00000000-0000-0000-0000-000000000000', '0', '[0-9a-fA-F]');
