﻿DECLARE @ProductID INT
SET @ProductID=(SELECT ID FROM dbo.Product WHERE ProductCode='MUL')

--covers
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Funeral', @Productid, 142, NULL, 1, 0)

DECLARE @CoverDefFuneralid INT
set @CoverDefFuneralid=IDENT_CURRENT('CoverDefinition')

--questions
DECLARE @id int
SET @id=(SELECT ID FROM md.Question WHERE Name='Type Of Cover Funeral')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Type Of Cover', @CoverDefFuneralid, @id, NULL, 1, 0, 1, 1, 0, N'Please select the type of cover required', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Main Member Date Of Birth')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Main Member Date Of Birth', @CoverDefFuneralid, @id, NULL, 1, 1, 1, 1, 0, N'Please enter the main members date of birth', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Number Of Additional Children')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Number Of Additional Children', @CoverDefFuneralid, @id, NULL, 1, 2, 1, 1, 0, N'Please enter the number of additional children', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Extended Family Member 1 DoB')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 1 DoB', @CoverDefFuneralid, @id, NULL, 1, 3, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Extended Family Member 2 DoB')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 2 DoB', @CoverDefFuneralid, @id, NULL, 1, 4, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Extended Family Member 3 DoB')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 3 DoB', @CoverDefFuneralid, @id, NULL, 1, 5, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Extended Family Member 4 DoB')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 4 DoB', @CoverDefFuneralid, @id, NULL, 1, 6, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Extended Family Member 5 DoB')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 5 DoB', @CoverDefFuneralid, @id, NULL, 1, 7, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Extended Family Member 6 DoB')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 6 DoB', @CoverDefFuneralid, @id, NULL, 1, 8, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Extended Family Member 7 DoB')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 7 DoB', @CoverDefFuneralid, @id, NULL, 1, 9, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Extended Family Member 8 DoB')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 8 DoB', @CoverDefFuneralid, @id, NULL, 1, 10, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Extended Family Member 9 DoB')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 9 DoB', @CoverDefFuneralid, @id, NULL, 1, 11, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Extended Family Member 10 DoB')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 10 DoB', @CoverDefFuneralid, @id, NULL, 1, 12, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

SET @id=(SELECT ID FROM md.Question WHERE Name='Sum Assured')
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Sum Assured', @CoverDefFuneralid, @id, NULL, 1, 13, 1, 1, 0, N'Please select the sum assured value', N'', N'^[0-9]+$')

