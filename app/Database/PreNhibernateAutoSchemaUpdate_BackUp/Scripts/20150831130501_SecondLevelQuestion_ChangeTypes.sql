
UPDATE md.SecondLevelQuestion
SET QuestionTypeId = 3
WHERE Id IN (1,2,3,4)

SET IDENTITY_INSERT md.SecondLevelQuestionAnswer ON

INSERT INTO md.SecondLevelQuestionAnswer (Id, Name, Answer, SecondLevelQuestionId, VisibleIndex)
VALUES (3, 'None_1', 'None', 1, 0)
INSERT INTO md.SecondLevelQuestionAnswer (Id, Name, Answer, SecondLevelQuestionId, VisibleIndex)
VALUES (4, 'Yes_1', 'Yes', 1, 1)
INSERT INTO md.SecondLevelQuestionAnswer (Id, Name, Answer, SecondLevelQuestionId, VisibleIndex)
VALUES (5, 'No_1', 'No', 1, 2)

INSERT INTO md.SecondLevelQuestionAnswer (Id, Name, Answer, SecondLevelQuestionId, VisibleIndex)
VALUES (6, 'None_2', 'None', 2, 0)
INSERT INTO md.SecondLevelQuestionAnswer (Id, Name, Answer, SecondLevelQuestionId, VisibleIndex)
VALUES (7, 'Yes_2', 'Yes', 2, 1)
INSERT INTO md.SecondLevelQuestionAnswer (Id, Name, Answer, SecondLevelQuestionId, VisibleIndex)
VALUES (8, 'No_2', 'No', 2, 2)

INSERT INTO md.SecondLevelQuestionAnswer (Id, Name, Answer, SecondLevelQuestionId, VisibleIndex)
VALUES (9, 'None_3', 'None', 3, 0)
INSERT INTO md.SecondLevelQuestionAnswer (Id, Name, Answer, SecondLevelQuestionId, VisibleIndex)
VALUES (10, 'Yes_3', 'Yes', 3, 1)
INSERT INTO md.SecondLevelQuestionAnswer (Id, Name, Answer, SecondLevelQuestionId, VisibleIndex)
VALUES (11, 'No_3', 'No', 3, 2)

INSERT INTO md.SecondLevelQuestionAnswer (Id, Name, Answer, SecondLevelQuestionId, VisibleIndex)
VALUES (12, 'None_4', 'None', 4, 0)
INSERT INTO md.SecondLevelQuestionAnswer (Id, Name, Answer, SecondLevelQuestionId, VisibleIndex)
VALUES (13, 'Yes_4', 'Yes', 4, 1)
INSERT INTO md.SecondLevelQuestionAnswer (Id, Name, Answer, SecondLevelQuestionId, VisibleIndex)
VALUES (14, 'No_4', 'No', 4, 2)


SET IDENTITY_INSERT md.SecondLevelQuestionAnswer OFF