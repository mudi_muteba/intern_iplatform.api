﻿-- add columns to Quote and Policyheader for linking

ALTER TABLE dbo.Quote ADD PlatformId [uniqueidentifier] NULL
ALTER TABLE dbo.PolicyHeader ADD PlatformId [uniqueidentifier] NULL
ALTER TABLE dbo.PolicyHeader ADD QuoteId [int] NULL
