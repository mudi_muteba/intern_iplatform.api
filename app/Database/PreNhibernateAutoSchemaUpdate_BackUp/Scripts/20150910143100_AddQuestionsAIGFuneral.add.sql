﻿----------------------------------------------------------------------
DECLARE @questionType INT = 3
----------------------------------------------------------------------

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Sum Assured', @questionType, 7) -- Finance Information

DECLARE @SumAssuredid INT
SET @SumAssuredid = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Sum Assured - R 10 000',' R 10 000', @SumAssuredid, 0),
		('Sum Assured - R 15 000',' R 15 000', @SumAssuredid, 1),
		('Sum Assured - R 20 000',' R 20 000', @SumAssuredid, 2),
		('Sum Assured - R 25 000',' R 25 000', @SumAssuredid, 3),
		('Sum Assured - R 30 000',' R 30 000', @SumAssuredid, 4),
		('Sum Assured - R 35 000',' R 35 000', @SumAssuredid, 5),
		('Sum Assured - R 40 000',' R 40 000', @SumAssuredid, 6),
		('Sum Assured - R 45 000',' R 45 000', @SumAssuredid, 7),
		('Sum Assured - R 50 000',' R 50 000', @SumAssuredid, 8),
		('Sum Assured - R 55 000',' R 55 000', @SumAssuredid, 9),
		('Sum Assured - R 60 000',' R 60 000', @SumAssuredid, 10),
		('Sum Assured - R 65 000',' R 65 000', @SumAssuredid, 11),
		('Sum Assured - R 70 000',' R 70 000', @SumAssuredid, 12),
		('Sum Assured - R 75 000',' R 75 000', @SumAssuredid, 13),
		('Sum Assured - R 80 000',' R 80 000', @SumAssuredid, 14),
		('Sum Assured - R 85 000',' R 85 000', @SumAssuredid, 15),
		('Sum Assured - R 90 000',' R 90 000', @SumAssuredid, 16),
		('Sum Assured - R 95 000',' R 95 000', @SumAssuredid, 17),
		('Sum Assured - R 100 000',' R 100 000', @SumAssuredid, 18)


INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Type Of Cover Funeral', @questionType, 2) -- Risk Information

DECLARE @TypeOfCoverid INT
SET @TypeOfCoverid = SCOPE_IDENTITY()

INSERT INTO md.QuestionAnswer (Name, Answer, QuestionId, VisibleIndex)
VALUES	('Type Of Cover Funeral - Single',' Single', @TypeOfCoverid, 0),
		('Type Of Cover Funeral - Main And Spouse',' Main And Spouse', @TypeOfCoverid, 1),
		('Type Of Cover Funeral - Family',' Family', @TypeOfCoverid, 2),
		('Type Of Cover Funeral - Single And Children',' Single And Children', @TypeOfCoverid, 3)

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Main Member Date Of Birth', 2, 2) -- Risk Information
DECLARE @MainMemberid INT
SET @MainMemberid = SCOPE_IDENTITY()

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) 
VALUES	('Number Of Additional Children', 4, 2) -- Risk Information		
DECLARE @AddChildid INT
SET @AddChildid = SCOPE_IDENTITY()

INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) VALUES	('Extended Family Member 1 DoB', 2, 2) -- Risk Information
DECLARE @ExtMember1id INT
SET @ExtMember1id = SCOPE_IDENTITY()
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) VALUES	('Extended Family Member 2 DoB', 2, 2) -- Risk Information
DECLARE @ExtMember2id INT
SET @ExtMember2id = SCOPE_IDENTITY()
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) VALUES	('Extended Family Member 3 DoB', 2, 2) -- Risk Information
DECLARE @ExtMember3id INT
SET @ExtMember3id = SCOPE_IDENTITY()
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) VALUES	('Extended Family Member 4 DoB', 2, 2) -- Risk Information
DECLARE @ExtMember4id INT
SET @ExtMember4id = SCOPE_IDENTITY()
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) VALUES	('Extended Family Member 5 DoB', 2, 2) -- Risk Information
DECLARE @ExtMember5id INT
SET @ExtMember5id = SCOPE_IDENTITY()
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) VALUES	('Extended Family Member 6 DoB', 2, 2) -- Risk Information
DECLARE @ExtMember6id INT
SET @ExtMember6id = SCOPE_IDENTITY()
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) VALUES	('Extended Family Member 7 DoB', 2, 2) -- Risk Information
DECLARE @ExtMember7id INT
SET @ExtMember7id = SCOPE_IDENTITY()
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) VALUES	('Extended Family Member 8 DoB', 2, 2) -- Risk Information
DECLARE @ExtMember8id INT
SET @ExtMember8id = SCOPE_IDENTITY()
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) VALUES	('Extended Family Member 9 DoB', 2, 2) -- Risk Information
DECLARE @ExtMember9id INT
SET @ExtMember9id = SCOPE_IDENTITY()
INSERT INTO md.Question(Name, QuestionTypeId, QuestionGroupId) VALUES	('Extended Family Member 10 DoB', 2, 2) -- Risk Information
DECLARE @ExtMember10id INT
SET @ExtMember10id = SCOPE_IDENTITY()

DECLARE @Partyid INT
set @Partyid=(Select PartyID from Organization Where Code='AIG')

--product
INSERT [dbo].[Product] ([MasterId], [Name], [ProductOwnerId], [ProductProviderId], [ProductTypeId], [AgencyNumber], [ProductCode], [StartDate], [EndDate]) VALUES 
(0, N'Virgin Domestic Funeral', @Partyid, @Partyid, 7, N'123456', N'VMSAFuneral', CAST(0x00009CD400000000 AS DateTime),NULL)

DECLARE @Productid INT
set @Productid=IDENT_CURRENT('Product')

--covers
INSERT [dbo].[CoverDefinition] ([MasterId], [DisplayName], [ProductId], [CoverId], [ParentId], [CoverDefinitionTypeId], [VisibleIndex]) VALUES (0, N'Funeral', @Productid, 142, NULL, 1, 0)

DECLARE @CoverDefFuneralid INT
set @CoverDefFuneralid=IDENT_CURRENT('CoverDefinition')

--questions
INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Type Of Cover', @CoverDefFuneralid, @TypeOfCoverid, NULL, 1, 0, 1, 1, 0, N'Please select the type of cover required', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Main Member Date Of Birth', @CoverDefFuneralid, @MainMemberid, NULL, 1, 1, 1, 1, 0, N'Please enter the main members date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Number Of Additional Children', @CoverDefFuneralid, @AddChildid, NULL, 1, 2, 1, 1, 0, N'Please enter the number of additional children', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 1 DoB', @CoverDefFuneralid, @ExtMember1id, NULL, 1, 3, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 2 DoB', @CoverDefFuneralid, @ExtMember2id, NULL, 1, 4, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 3 DoB', @CoverDefFuneralid, @ExtMember3id, NULL, 1, 5, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 4 DoB', @CoverDefFuneralid, @ExtMember4id, NULL, 1, 6, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 5 DoB', @CoverDefFuneralid, @ExtMember5id, NULL, 1, 7, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 6 DoB', @CoverDefFuneralid, @ExtMember6id, NULL, 1, 8, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 7 DoB', @CoverDefFuneralid, @ExtMember7id, NULL, 1, 9, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 8 DoB', @CoverDefFuneralid, @ExtMember8id, NULL, 1, 10, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 9 DoB', @CoverDefFuneralid, @ExtMember9id, NULL, 1, 11, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Extended Family Member 10 DoB', @CoverDefFuneralid, @ExtMember10id, NULL, 1, 12, 1, 1, 0, N'Please enter the date of birth', N'', N'^[0-9]+$')

INSERT [dbo].[QuestionDefinition] ([MasterId], [DisplayName], [CoverDefinitionId], [QuestionId], [ParentId], [QuestionDefinitionTypeId], [VisibleIndex], [RequiredForQuote], [RatingFactor], 
[ReadOnly], [ToolTip], [DefaultValue], [RegexPattern]) VALUES (0, N'Sum Assured', @CoverDefFuneralid, @SumAssuredid, NULL, 1, 13, 1, 1, 0, N'Please select the sum assured value', N'', N'^[0-9]+$')

