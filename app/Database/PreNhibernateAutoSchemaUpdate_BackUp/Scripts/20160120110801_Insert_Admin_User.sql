﻿SET IDENTITY_INSERT dbo.[User] ON;

if not exists(select * from [User] where UserName = 'admin@ibroker.co.za')
	begin
		insert into [user]
		(id, UserName, SystemId, IsApproved, IsActive, CreatedOn, ModifiedOn, Password)
		values(137, 'admin@ibroker.co.za', newid(), 1, 1, getdate(), getdate(), '100:BzUAUbfGtNq9jZSfjXz6tCAeDiouIGsNdWxXLWjqKBQ32Q4DQJ89QIJr6wkOLVNINJcp8XKu+EMZHwqITJnj5qO6PBI628qZ4RCL8pAccMSFDtLZIj1tRNFNQ23DhI1QNL1UEo0QJTg0et8v+YC1gVjDlIC3rrCrk6s6uNzuX9REIaci8ccLfMVrFMh81Isvh/3E7S4gYHZM1t4ev8QyKKXFvPegQ8dS82aWqJKgScBbpEdOTUY0bEWCn4VcpqCjHnVdW0oVWsc+FE1zKKmKE7HzsGC+xIJw2x24kVcImnO9mzRI5eQKlNktzkpF3w1cYAyU7dxzUEh0XmllBLMPiw==:JTutox6Q99TFr/mwqnZtqzUGotkxbcgtMsF1pEGFgSmxP+NAjf4Y/4Ubq+O/qhQYP3mSnMa6V0ztyUPCEM81zUvIYEMPDJmWIlhc8l2xjDOivkjukJsGcw9e8G6vptiQMqKTQ7vZp5EpQXZhg/DFdbDdXGIIZC/H8kFHfSuHHiclNN2kFzNzJ7Wr1xMkU10C6fEixF/MwNy5ko4oYNm6vKcnkv2okf+owPPNabgt27XK937EwcFtb1uWLmTRH3ZV5tUj4YQcy6s7nhpVYpcqJhYCvXxnutlar6JgQIzxWSMqkRamUSpMlP3F5EpGXxAAYf1+OD2C67P83fXtzLrXZQ==')

		INSERT INTO [dbo].[UserChannel]
				   ([CreatedAt]
				   ,[ModifiedAt]
				   ,[UserId]
				   ,[ChannelId]
				   ,[IsDeleted])
			 VALUES
				   (GETDATE()
				   ,GETDATE()
				   ,137
				   ,1
				   ,0)
	end
GO

SET IDENTITY_INSERT dbo.[User] OFF;


