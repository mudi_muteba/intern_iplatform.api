﻿
  ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [FK_Contact_Title];

  ALTER TABLE [md].[StateProvince] DROP CONSTRAINT [FK_StateProvince_Country];

  ALTER TABLE [dbo].[IdentityUserClaim] DROP CONSTRAINT [FK92F34A7D6331F7DC];

  ALTER TABLE [dbo].[Address] DROP CONSTRAINT [FK_Party_Address];

  ALTER TABLE [dbo].[IdentityUserLogin] DROP CONSTRAINT [FK25FB07746331F7DC];

  ALTER TABLE [dbo].[PolicyFuneral] DROP CONSTRAINT [FK_PolicyFuneralAddressId_AddressId];

  ALTER TABLE [dbo].[IdentityUserRole] DROP CONSTRAINT [FKE1FC89AB6331F7DC];

  ALTER TABLE [dbo].[IdentityUserRole] DROP CONSTRAINT [FKE1FC89AB9A3C3A4C];

  ALTER TABLE [dbo].[BankDetails] DROP CONSTRAINT [FK6B681666DB2A8D2E];

  ALTER TABLE [dbo].[PolicyHeaderClaimableItem] DROP CONSTRAINT [FK_PolicyHeaderClaimableItem_PolicyHeaderId_PolicyHeader_Id];

  ALTER TABLE [dbo].[UserSecurityRole] DROP CONSTRAINT [FK_UserSecurityRole_User];

  ALTER TABLE [dbo].[PolicyHeaderClaimableItem] DROP CONSTRAINT [FK_PolicyHeaderClaimableItem_PolicyItemId_PolicyItem_Id];

  ALTER TABLE [dbo].[UserSecurityRole] DROP CONSTRAINT [FK_UserSecurityRole_SecurityRole];

  ALTER TABLE [dbo].[UserChannel] DROP CONSTRAINT [FK_UserChannel_User];

  ALTER TABLE [dbo].[ProductFee] DROP CONSTRAINT [FK_ProductFee_ProductFeeType];

  ALTER TABLE [dbo].[UserChannel] DROP CONSTRAINT [FK_UserChannel_Channel];

  ALTER TABLE [dbo].[ProductFee] DROP CONSTRAINT [FK_ProductFee_PaymentPlan];

  ALTER TABLE [dbo].[ProductFee] DROP CONSTRAINT [FK_ProductFee_Product];

  ALTER TABLE [dbo].[PolicySpecial] DROP CONSTRAINT [FK_PolicySpecialAddressId_AddressId];

  ALTER TABLE [dbo].[InsurersBranches] DROP CONSTRAINT [FK_InsurersBranches_Organization];

  ALTER TABLE [dbo].[InsurersBranches] DROP CONSTRAINT [FK_InsurersBranches_InsurersBranchType];

  ALTER TABLE [dbo].[UserAuthorisationGroup] DROP CONSTRAINT [User_UserAuthorisationGroup];

  ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [FK_Organization_OrganizationType];

  ALTER TABLE [dbo].[UserAuthorisationGroup] DROP CONSTRAINT [Channel_UserAuthorisationGroup];

  ALTER TABLE [dbo].[Party] DROP CONSTRAINT [FK_Party_PartyType];

  ALTER TABLE [dbo].[CampaignReference] DROP CONSTRAINT [Campaign_CampaingnReference];

  ALTER TABLE [dbo].[Relationship] DROP CONSTRAINT [FK_Relationship_Party];

  ALTER TABLE [dbo].[Role] DROP CONSTRAINT [FK_Role_Party];

  ALTER TABLE [dbo].[Memberships] DROP CONSTRAINT [MembershipApplication];

  ALTER TABLE [dbo].[Role] DROP CONSTRAINT [FK_Role_RoleType];

  ALTER TABLE [dbo].[Memberships] DROP CONSTRAINT [MembershipUser];

  ALTER TABLE [dbo].[AuthorisationPoint] DROP CONSTRAINT [AuthorisationPointCategory_authorisationPoint];

  ALTER TABLE [dbo].[Relationship] DROP CONSTRAINT [FK_Relationship_ParentParty];

  ALTER TABLE [dbo].[Roles] DROP CONSTRAINT [RoleApplication];

  ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Product_RiskItemType];

  ALTER TABLE [dbo].[Relationship] DROP CONSTRAINT [FK_Relationship_RelationshipType];

  ALTER TABLE [dbo].[Lead] DROP CONSTRAINT [FK_Lead_LeadStatus];

  ALTER TABLE [dbo].[Users] DROP CONSTRAINT [UserApplication];

  ALTER TABLE [dbo].[Party] DROP CONSTRAINT [FK_Party_ContactDetail];

  ALTER TABLE [dbo].[Profiles] DROP CONSTRAINT [UserProfile];

  ALTER TABLE [dbo].[AuthorisationGroupPoint] DROP CONSTRAINT [AuthorisationPoint_AuthorisationGroupPoint];

  ALTER TABLE [dbo].[UsersInRoles] DROP CONSTRAINT [UsersInRoleRole];

  ALTER TABLE [dbo].[ProductDescription] DROP CONSTRAINT [FK_ProductDescription_Product];

  ALTER TABLE [dbo].[UsersInRoles] DROP CONSTRAINT [UsersInRoleUser];

  ALTER TABLE [dbo].[Lead] DROP CONSTRAINT [FK_Lead_Party];

  ALTER TABLE [dbo].[PartyCampaign] DROP CONSTRAINT [Campaign_PartyCampaign];

  ALTER TABLE [dbo].[LeadActivity] DROP CONSTRAINT [FK_LeadActivity_Lead];

  ALTER TABLE [dbo].[PartyCampaign] DROP CONSTRAINT [Party_PartyCampaign];

  ALTER TABLE [dbo].[AnswerDefinition] DROP CONSTRAINT [FK_AnswerDefinition_Product];

  ALTER TABLE [dbo].[PolicyItem] DROP CONSTRAINT [PolicyHeader_PolicyItem];

  ALTER TABLE [dbo].[AnswerDefinition] DROP CONSTRAINT [FK_AnswerDefinition_QuestionAnswer];

  ALTER TABLE [dbo].[ClaimsItem] DROP CONSTRAINT [ClaimsHeader_ClaimsItem];

  ALTER TABLE [dbo].[ClaimsItem] DROP CONSTRAINT [PolicyItem_ClaimsItem];

  ALTER TABLE [dbo].[ClaimsItemQuestionAnswer] DROP CONSTRAINT [ClaimsItem_ClaimsItemQuestionAnswer];

  ALTER TABLE [dbo].[Configuration] DROP CONSTRAINT [FK_Configuration_Party];

  ALTER TABLE [md].[ClaimsQuestion] DROP CONSTRAINT [FK_ClaimsQuestion_QuestionGroup];

  ALTER TABLE [md].[ClaimsQuestion] DROP CONSTRAINT [FK_ClaimsQuestion_QuestionType];

  ALTER TABLE [dbo].[LeadImport] DROP CONSTRAINT [Campaign_LeadImport];

  ALTER TABLE [dbo].[Individual] DROP CONSTRAINT [FK_Individual_Party];

  ALTER TABLE [dbo].[LeadImport] DROP CONSTRAINT [User_LeadImport];

  ALTER TABLE [dbo].[QuoteUploadLog] DROP CONSTRAINT [FK_QuoteUploadLog_Product];

  ALTER TABLE [dbo].[Individual] DROP CONSTRAINT [FK_Individual_Gender];

  ALTER TABLE [dbo].[LeadActivity] DROP CONSTRAINT [Party_LeadActivity];

  ALTER TABLE [md].[ClaimsQuestionAnswer] DROP CONSTRAINT [FK_ClaimsQuestionAnswer_Question];

  ALTER TABLE [dbo].[Individual] DROP CONSTRAINT [FK_Individual_Title];

  ALTER TABLE [dbo].[ProposalLeadActivity] DROP CONSTRAINT [ProposalHeader_ProposalLeadActivity];

  ALTER TABLE [dbo].[Individual] DROP CONSTRAINT [FK_Individual_Occupation];

  ALTER TABLE [dbo].[PolicyLeadActivity] DROP CONSTRAINT [PolicyHeader_PolicyLeadActivity];

  ALTER TABLE [dbo].[Note] DROP CONSTRAINT [FK_Note_Individual];

  ALTER TABLE [dbo].[Individual] DROP CONSTRAINT [FK_Individual_HomeLanguage];

  ALTER TABLE [dbo].[ClaimsLeadActivity] DROP CONSTRAINT [ClaimsHeader_ClaimsLeadActivity];

  ALTER TABLE [dbo].[Note] DROP CONSTRAINT [FK_Note_Party];

  ALTER TABLE [dbo].[Individual] DROP CONSTRAINT [FK_Individual_MaritalStatus];

  ALTER TABLE [dbo].[QuotedLeadActivity] DROP CONSTRAINT [QuoteHeader_QuotedLeadActivity];

  ALTER TABLE [dbo].[PolicyLeadActivity] DROP CONSTRAINT [FK_PolicyLeadActivity_PolicyHeader];

  ALTER TABLE [dbo].[QuoteAcceptedLeadActivity] DROP CONSTRAINT [Quote_QuoteAcceptedLeadActivity];

  ALTER TABLE [dbo].[Lead] DROP CONSTRAINT [FK_Lead_LeadQuality];

  ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [FK_Organization_Party];

  ALTER TABLE [dbo].[AuditTrailItem] DROP CONSTRAINT [FK_AuditTrailItem_Party];

  ALTER TABLE [dbo].[AuditTrailItem] DROP CONSTRAINT [FK_AuditTrailItem_Individual];

  ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Product_Owner];

  ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Product_Provider];

  ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Product_ProductType];

  ALTER TABLE [dbo].[LeadActivity] DROP CONSTRAINT [FK_LeadActivity_User];

  ALTER TABLE [dbo].[UserIndividual] DROP CONSTRAINT [FK_UserIndividual_User];

  ALTER TABLE [dbo].[ProposalDefinition] DROP CONSTRAINT [Proposal_Party];

  ALTER TABLE [dbo].[UserIndividual] DROP CONSTRAINT [FK_UserIndividual_Individual];

  ALTER TABLE [dbo].[ProposalDefinition] DROP CONSTRAINT [Proposal_Cover];

  ALTER TABLE [dbo].[AssetExtras] DROP CONSTRAINT [FK_AssetExtras_Asset];

  ALTER TABLE [dbo].[ProposalDefinition] DROP CONSTRAINT [Proposal_Product];

  ALTER TABLE [dbo].[LeadActivity] DROP CONSTRAINT [FK_LeadActivity_Campaign];

  ALTER TABLE [dbo].[MessageClient] DROP CONSTRAINT [FK_MessageQueue_Clients];

  ALTER TABLE [dbo].[ProposalQuestionAnswer] DROP CONSTRAINT [ProposalQuestionAnswer_Proposal];

  ALTER TABLE [dbo].[AssetAddress] DROP CONSTRAINT [FK_AssetAddress_Asset];

  ALTER TABLE [dbo].[ProposalDefinition] DROP CONSTRAINT [Proposal_ProposalHeader];

  ALTER TABLE [dbo].[AssetAddress] DROP CONSTRAINT [FK_AssetAddress_Address];

  ALTER TABLE [dbo].[CoverDefinition] DROP CONSTRAINT [FK_CoverDefinition_Cover];

  ALTER TABLE [dbo].[AssetVehicle] DROP CONSTRAINT [AssetVehicle_Asset];

  ALTER TABLE [dbo].[CoverDefinition] DROP CONSTRAINT [FK_CoverDefinition_CoverDefinition];

  ALTER TABLE [dbo].[ProposalHeader] DROP CONSTRAINT [ProposalHeader_LeadActivity];

  ALTER TABLE [dbo].[CoverDefinition] DROP CONSTRAINT [FK_CoverDefinition_Product];

  ALTER TABLE [dbo].[ProductBenefit] DROP CONSTRAINT [FK_ProductBenefit_CoverDefinition];

  ALTER TABLE [dbo].[CoverDefinition] DROP CONSTRAINT [FK_CoverDefinition_CoverDefinitionType];

  ALTER TABLE [dbo].[CampaignProduct] DROP CONSTRAINT [FK_Campaign_Product];

  ALTER TABLE [dbo].[MessageClient] DROP CONSTRAINT [FK_ClientSite_Clients];

  ALTER TABLE [dbo].[CampaignProduct] DROP CONSTRAINT [FK_Product_Campaign];

  ALTER TABLE [dbo].[ClientSite] DROP CONSTRAINT [FK_ClientSite_Organization];

  ALTER TABLE [dbo].[ProposalQuestionAnswer] DROP CONSTRAINT [ProposalQuestionAnswer_QuestionDefinition];

  ALTER TABLE [dbo].[Asset] DROP CONSTRAINT [Asset_Party];

  ALTER TABLE [md].[SecondLevelQuestionAnswer] DROP CONSTRAINT [FK_SecondLevelQuestionAnswer_SecondLevelQuestion];

  ALTER TABLE [dbo].[QuoteItem] DROP CONSTRAINT [FK_QuoteItem_Quote];

  ALTER TABLE [dbo].[QuestionDefinition] DROP CONSTRAINT [QuestionDefinition_QuestionDefinitionGroupType];

  ALTER TABLE [dbo].[QuoteItem] DROP CONSTRAINT [FK_QuoteItem_CoverDefinition];

  ALTER TABLE [md].[QuestionAnswer] DROP CONSTRAINT [FK_QuestionAnswer_Question];

  ALTER TABLE [dbo].[ProposalQuestionAnswer] DROP CONSTRAINT [ProposalQuestionAnswer_QuestionType];

  ALTER TABLE [md].[SecondLevelQuestion] DROP CONSTRAINT [FK_SecondLevelQuestion_SecondLevelQuestionGroup];

  ALTER TABLE [md].[Question] DROP CONSTRAINT [FK_Question_QuestionType];

  ALTER TABLE [dbo].[CampaignAgent] DROP CONSTRAINT [FK_Campaign_Agent];

  ALTER TABLE [dbo].[CampaignAgent] DROP CONSTRAINT [FK_Agent_Campaign];

  ALTER TABLE [md].[Question] DROP CONSTRAINT [FK_Question_QuestionGroup];

  ALTER TABLE [dbo].[SecondLevelQuestionDefinition] DROP CONSTRAINT [FK_SecondLevelQuestionDefinition_SecondLevelQuestion];

  ALTER TABLE [dbo].[SecondLevelQuestionDefinition] DROP CONSTRAINT [FK_SecondLevelQuestionDefinition_SecondLevelQuestionDefinition];

  ALTER TABLE [dbo].[SecondLevelQuestionDefinition] DROP CONSTRAINT [FK_SecondLevelQuestionDefinition_CoverDefinition];

  ALTER TABLE [dbo].[SecondLevelQuestionDefinition] DROP CONSTRAINT [FK_SecondLevelQuestionDefinition_QuestionDefinitionType];

  ALTER TABLE [md].[SecondLevelQuestion] DROP CONSTRAINT [FK_SecondLevelQuestion_QuestionType];

  ALTER TABLE [dbo].[QuestionDefinition] DROP CONSTRAINT [FK_QuestionDefinition_Question];

  ALTER TABLE [dbo].[QuestionDefinition] DROP CONSTRAINT [FK_QuestionDefinition_QuestionDefinition];

  ALTER TABLE [dbo].[QuestionDefinition] DROP CONSTRAINT [FK_QuestionDefinition_CoverDefinition];

  ALTER TABLE [dbo].[QuestionDefinition] DROP CONSTRAINT [FK_QuestionDefinition_QuestionDefinitionType];

  ALTER TABLE [dbo].[TeamCampaign] DROP CONSTRAINT [Campaign_TeamCampaign];

  ALTER TABLE [dbo].[TeamCampaign] DROP CONSTRAINT [Team_TeamCampaign];

  ALTER TABLE [dbo].[TagCampaignMap] DROP CONSTRAINT [FK_TagCampaignMap_Tag];

  ALTER TABLE [dbo].[TeamUser] DROP CONSTRAINT [Team_TeamUser];

  ALTER TABLE [dbo].[TagProductMap] DROP CONSTRAINT [FK_TagProductMap_Product];

  ALTER TABLE [dbo].[QuestionAnswerExclusion] DROP CONSTRAINT [FK_QuestionAnswerExclusion_Product];

  ALTER TABLE [dbo].[TeamUser] DROP CONSTRAINT [User_TeamUser];

  ALTER TABLE [dbo].[TagProductMap] DROP CONSTRAINT [FK_TagProductMap_Tag];

  ALTER TABLE [dbo].[QuestionAnswerExclusion] DROP CONSTRAINT [FK_QuestionAnswerExclusion_CoverDefinition];

  ALTER TABLE [dbo].[QuestionAnswerExclusion] DROP CONSTRAINT [FK_QuestionAnswerExclusion_QuestionDefinition];

  ALTER TABLE [dbo].[QuestionAnswerExclusion] DROP CONSTRAINT [FK_QuestionAnswerExclusion_QuestionAnswer];

  ALTER TABLE [dbo].[AssetRiskItem] DROP CONSTRAINT [AssetRiskItem_Asset];

  ALTER TABLE [md].[ClaimsQuestionDefinition] DROP CONSTRAINT [FK_ClaimsQuestionDefinition_ClaimsQuestionGroup];

  ALTER TABLE [dbo].[QuoteAcceptedLeadActivity] DROP CONSTRAINT [FK_QuoteAcceptedLeadActivity_Quote];

  ALTER TABLE [dbo].[FuneralMember] DROP CONSTRAINT [ProposalDefinition_FuneralMember];

  ALTER TABLE [dbo].[Address] DROP CONSTRAINT [FK_Address_AddressType];

  ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [FK_Contact_Gender];

  ALTER TABLE [md].[ClaimsQuestionDefinition] DROP CONSTRAINT [FK_ClaimsQuestionDefinition_ClaimsQuestion];

  ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [FK_Contact_HomeLanguage];

  ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [FK_Contact_MaritalStatus];

  ALTER TABLE [dbo].[Address] DROP CONSTRAINT [FK_Address_StateProvince];

  ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [FK_Contact_Occupation];