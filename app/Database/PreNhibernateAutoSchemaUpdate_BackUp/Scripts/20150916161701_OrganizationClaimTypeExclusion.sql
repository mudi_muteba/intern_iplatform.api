﻿SET IDENTITY_INSERT [md].[ClaimsType] ON;

IF NOT EXISTS ( SELECT 1 FROM [md].[ClaimsType] WHERE ID = 12) 
BEGIN
	INSERT INTO [md].[ClaimsType] ([Id], [Name]) VALUES (12, 'Motor Accident (No 3rd Party)');
END

IF NOT EXISTS ( SELECT 1 FROM [md].[ClaimsType] WHERE ID = 13) 
BEGIN
	INSERT INTO [md].[ClaimsType] ([Id], [Name]) VALUES (13, 'Motor Accident (Yes 3rd Party)');
END

IF NOT EXISTS ( SELECT 1 FROM [md].[ClaimsType] WHERE ID = 14) 
BEGIN
	INSERT INTO [md].[ClaimsType] ([Id], [Name]) VALUES (14, 'Motor Hijack');
END

IF NOT EXISTS ( SELECT 1 FROM [md].[ClaimsType] WHERE ID = 15) 
BEGIN
	INSERT INTO [md].[ClaimsType] ([Id], [Name]) VALUES (15, 'Glass (Windscreen)');
END

IF NOT EXISTS ( SELECT 1 FROM [md].[ClaimsType] WHERE ID = 16) 
BEGIN
	INSERT INTO [md].[ClaimsType] ([Id], [Name]) VALUES (16, 'Property (Specified)');
END

IF NOT EXISTS ( SELECT 1 FROM [md].[ClaimsType] WHERE ID = 17) 
BEGIN
	INSERT INTO [md].[ClaimsType] ([Id], [Name]) VALUES (17, 'Property (Not Specified)');
END

IF NOT EXISTS ( SELECT 1 FROM [md].[ClaimsType] WHERE ID = 18) 
BEGIN
	INSERT INTO [md].[ClaimsType] ([Id], [Name]) VALUES (18, 'Funeral (Accidental)');
END

IF NOT EXISTS ( SELECT 1 FROM [md].[ClaimsType] WHERE ID = 19) 
BEGIN
	INSERT INTO [md].[ClaimsType] ([Id], [Name]) VALUES (19, 'Funeral (Non-accidental)');
END

SET IDENTITY_INSERT [md].[ClaimsType] OFF;
