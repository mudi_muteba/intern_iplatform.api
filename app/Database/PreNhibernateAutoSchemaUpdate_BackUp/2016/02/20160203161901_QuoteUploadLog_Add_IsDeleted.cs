﻿using System;
using FluentMigrator;
using Database.Schema;

namespace Database.Migrations._2016._02
{
    [Migration(20160203161901)]
    public class QuoteUploadLog_Add_IsDeleted : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.QuoteUploadLog).Column("IsDeleted").Exists())
            {
                Alter
                    .Table(Tables.QuoteUploadLog)
                    .AddColumn("IsDeleted").AsBoolean().Nullable();

                Update.Table(Tables.QuoteUploadLog)
                    .Set(new {IsDeleted = false})
                    .AllRows();
            }
        }

        public override void Down()
        {

        }
    }
}
