﻿using System;
using FluentMigrator;
using Database.Schema;
using System.Collections.Generic;

namespace Database.Migrations._2016._02
{
    [Migration(20160212143201)]
    public class Performance_Counter : Migration
    {
      

        public override void Up()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.PerformanceCounter).Exists())
                Create.Table(Tables.PerformanceCounter)
                    .WithIdColumn()
                    .WithColumn("StartDateTime").AsDateTime()
                    .WithColumn("StopDateTime").AsDateTime()
                    .WithColumn("ElapsedTime").AsTime()
                    .WithColumn("TrackingId").AsString(100)
                    .WithColumn("Description").AsString(100).Nullable()
                    .WithColumn("UserId").AsInt32().Nullable()
                    .WithColumn("ApplicationMemory").AsInt64().Nullable()
                    .WithIsDeletedColumn();

        }

        public override void Down()
        {
            if (Schema.Schema(Schemas.Dbo).Table(Tables.PerformanceCounter).Exists())
                Delete.Table(Tables.PerformanceCounter);
        }
    }
}
