using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._02
{
    [Migration(20160202135301)]
    public class Add_PaymentDetails_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 22, Name = "PaymentDetails" });

            Insert.IntoTable(Tables.AuthorisationPoint)
                // individual
                .Row(new { Id = 61, Name = "Create", AuthorisationPointCategoryId = 22 })
                .Row(new { Id = 62, Name = "Edit", AuthorisationPointCategoryId = 22 })
                .Row(new { Id = 63, Name = "Delete", AuthorisationPointCategoryId = 22 })
                .Row(new { Id = 64, Name = "List", AuthorisationPointCategoryId = 22 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 61, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 62, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 63, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 64, AuthorisationGroupId = 5 })

                // call centre agent
                .Row(new { AuthorisationPointId = 61, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 62, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 63, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 64, AuthorisationGroupId = 1 })

                // call centre manager
                .Row(new { AuthorisationPointId = 61, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 62, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 63, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 64, AuthorisationGroupId = 2 })
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 61, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 62, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 63, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 64, AuthorisationGroupId = 5 })

                // call centre agent
                .Row(new { AuthorisationPointId = 61, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 62, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 63, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 64, AuthorisationGroupId = 1 })

                // call centre manager
                .Row(new { AuthorisationPointId = 61, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 62, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 63, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 64, AuthorisationGroupId = 2 })
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 61, Name = "Create", AuthorisationPointCategoryId = 22 })
                .Row(new { Id = 62, Name = "Edit", AuthorisationPointCategoryId = 22 })
                .Row(new { Id = 63, Name = "Delete", AuthorisationPointCategoryId = 22 })
                .Row(new { Id = 64, Name = "List", AuthorisationPointCategoryId = 22 })
                ;

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 22, Name = "PaymentDetails" });
        }
    }
}