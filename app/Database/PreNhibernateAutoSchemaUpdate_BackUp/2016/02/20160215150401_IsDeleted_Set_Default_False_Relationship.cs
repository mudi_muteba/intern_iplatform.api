﻿using System;
using FluentMigrator;
using Database.Schema;
using System.Collections.Generic;

namespace Database.Migrations._2016._02
{
    [Migration(20160215150401)]
    public class IsDeleted_Set_Default_False_Relationship : Migration
    {
        private readonly List<string> tables = new List<string>()
        {
            Tables.Relationship,
        };

        public override void Up()
        {
            foreach (var table in tables)
            {
                if (Schema.Table(table).Column("IsDeleted").Exists())
                {
                    Execute.Sql("Update " + table + " SET IsDeleted = 0 WHERE IsDeleted IS NULL;");
                    Alter.Table(table).AlterColumn("IsDeleted").AsBoolean().WithDefaultValue(false);
                }
            }
        }

        public override void Down()
        {

        }
    }
}
