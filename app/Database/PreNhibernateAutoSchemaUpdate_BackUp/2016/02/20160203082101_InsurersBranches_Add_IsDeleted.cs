﻿using System;
using FluentMigrator;
using Database.Schema;

namespace Database.Migrations._2016._02
{
    [Migration(20160203082101)]
    public class InsurersBranches_Add_IsDeleted : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.InsurersBranches).Column("IsDeleted").Exists())
            {
                Alter
                    .Table(Tables.InsurersBranches)
                    .AddColumn("IsDeleted").AsBoolean().Nullable();

                Update.Table(Tables.InsurersBranches)
                    .Set(new {IsDeleted = false})
                    .AllRows();
            }
        }

        public override void Down()
        {

        }
    }
}
