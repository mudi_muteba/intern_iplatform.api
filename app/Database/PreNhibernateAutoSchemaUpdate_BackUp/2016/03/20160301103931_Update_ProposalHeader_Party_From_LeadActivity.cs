﻿using FluentMigrator;

namespace iBroker.Database.Migrations
{
    [Migration(20160301103931)]
    public class Update_ProposalHeader_Party_From_LeadActivity : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160301103931_Update_ProposalHeader_Party_From_LeadActivity.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}