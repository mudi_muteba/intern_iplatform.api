﻿using FluentMigrator;

namespace Database.Migrations._2016._03
{
    [Migration(20160303080000)]
    public class Add_IPRS_Credentials_to_PersonSettings : Migration
    {
        public override void Up()
        {
            Alter.Table("PersonLookupSetting")
                .AddColumn("IPRSEnabled")
                .AsBoolean()
                .Nullable()
                .AddColumn("IPRSUsername")
                .AsString()
                .Nullable()
                .AddColumn("IPRSPassword")
                .AsString()
                .Nullable();
        }

        public override void Down()
        {
            Delete.Column("IPRSEnabled").Column("IPRSUsername").Column("IPRSPassword")
                .FromTable("PersonLookupSetting");
        }
    }
}