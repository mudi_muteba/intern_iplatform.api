﻿using System;
using FluentMigrator;
using Database.Schema;

namespace Database.Migrations._2016._01
{
    [Migration(20160118140401)]
    public class OrganizationPolicyServiceSetting_Add_IsDeleted : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.OrganizationPolicyServiceSetting).Column("IsDeleted").Exists())
            {
                Alter
                    .Table(Tables.OrganizationPolicyServiceSetting)
                    .AddColumn("IsDeleted").AsBoolean().Nullable();

                Update.Table(Tables.OrganizationPolicyServiceSetting)
                    .Set(new {IsDeleted = false})
                    .AllRows();
            }
        }

        public override void Down()
        {
            //Delete.Column("IsDeleted").FromTable(Tables.OrganizationClaimTypeExclusion);
        }
    }
}
