﻿using System;
using FluentMigrator;

namespace Database.Migrations._2016._01
{
    [Migration(20160108102001)]

    public class ExternalReference_Column_Type_Update : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160108102001_ExternalReference_Column_Type_Update_Up.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20160108102001_ExternalReference_Column_Type_Update_Down.sql");
        }
    }
}
