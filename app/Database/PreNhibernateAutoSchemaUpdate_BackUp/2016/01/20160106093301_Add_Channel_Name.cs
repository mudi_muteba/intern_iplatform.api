﻿using System;
using FluentMigrator;

namespace Database.Migrations._2016._01
{
    [Migration(20160106093301)]
    public class Add_Channel_Name : Migration
    {
        public override void Up()
        {
            Alter
                .Table("Channel")
                .AddColumn("Name").AsAnsiString(100).Nullable();
        }

        public override void Down()
        {
            Delete
               .Column("Name")
               .FromTable("Channel");
        }
    }
}
