﻿using System;
using FluentMigrator;
using Database.Schema;

namespace Database.Migrations._2016._01
{
    [Migration(20160111113601)]
    public class OrganizationClaimTypeExclusion_Add_IsDeleted : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.OrganizationClaimTypeExclusion).Column("IsDeleted").Exists())
            {
                Alter
                    .Table(Tables.OrganizationClaimTypeExclusion)
                    .AddColumn("IsDeleted").AsBoolean().Nullable();

                Update.Table(Tables.OrganizationClaimTypeExclusion)
                    .Set(new {IsDeleted = false})
                    .AllRows();
            }
        }

        public override void Down()
        {
            //Delete.Column("IsDeleted").FromTable(Tables.OrganizationClaimTypeExclusion);
        }
    }
}
