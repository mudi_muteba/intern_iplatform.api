﻿using System;
using FluentMigrator;

namespace Database.Migrations._2016._01
{
    [Migration(20160114114301)]
    // 20160114114301_Extend_iGuide_Settings
    public class Extend_iGuide_Settings : Migration
    {
        private const string tableName = "VehicleGuideSetting";
        private const string lsaEnabledField = "LightstoneEnabled";
        private const string lsaUserIdField = "LSA_UserId";
        private const string lsaContactIdField = "LSA_ContractId";
        private const string lsaCustomerIdField = "LSA_CustomerId";
        private const string lsaPackageIdField = "LSA_PackageId";
        private const string lsaUserNameField = "LSA_UserName";
        private const string lsaPasswordField = "LSA_Password";

        public override void Up()
        {
            if (!Schema.Table(tableName).Column(lsaEnabledField).Exists())
            {
                Alter.Table(tableName)
                    .AddColumn(lsaEnabledField).AsBoolean().Nullable();

                Update.Table(tableName)
                    .Set(new { LightstoneEnabled = true })
                    .AllRows();

                Alter.Column(lsaEnabledField)
                    .OnTable(tableName)
                    .AsBoolean().NotNullable().WithDefaultValue(true);
            }

            if (!Schema.Table(tableName).Column(lsaUserIdField).Exists())
            {
                Alter.Table(tableName)
                    .AddColumn(lsaUserIdField).AsGuid().Nullable();
            }

            if (!Schema.Table(tableName).Column(lsaContactIdField).Exists())
            {
                Alter.Table(tableName)
                    .AddColumn(lsaContactIdField).AsGuid().Nullable();
            }

            if (!Schema.Table(tableName).Column(lsaCustomerIdField).Exists())
            {
                Alter.Table(tableName)
                    .AddColumn(lsaCustomerIdField).AsGuid().Nullable();
            }

            if (!Schema.Table(tableName).Column(lsaPackageIdField).Exists())
            {
                Alter.Table(tableName)
                    .AddColumn(lsaPackageIdField).AsGuid().Nullable();
            }

            if (!Schema.Table(tableName).Column(lsaUserNameField).Exists())
            {
                Alter.Table(tableName)
                    .AddColumn(lsaUserNameField).AsString(255).Nullable();
            }

            if (!Schema.Table(tableName).Column(lsaPasswordField).Exists())
            {
                Alter.Table(tableName)
                    .AddColumn(lsaPasswordField).AsString(2048).Nullable();
            }

        }

        public override void Down()
        {
            if (Schema.Table(tableName).Column(lsaUserIdField).Exists())
            {
                Delete.Column(lsaEnabledField).FromTable(tableName);
            }

            if (Schema.Table(tableName).Column(lsaUserIdField).Exists())
            {
                Delete.Column(lsaUserIdField).FromTable(tableName);
            }

            if (Schema.Table(tableName).Column(lsaContactIdField).Exists())
            {
                Delete.Column(lsaContactIdField).FromTable(tableName);
            }

            if (Schema.Table(tableName).Column(lsaCustomerIdField).Exists())
            {
                Delete.Column(lsaCustomerIdField).FromTable(tableName);
            }

            if (Schema.Table(tableName).Column(lsaPackageIdField).Exists())
            {
                Delete.Column(lsaPackageIdField).FromTable(tableName);
            }

            if (Schema.Table(tableName).Column(lsaUserNameField).Exists())
            {
                Delete.Column(lsaUserNameField).FromTable(tableName);
            }

            if (Schema.Table(tableName).Column(lsaPasswordField).Exists())
            {
                Delete.Column(lsaPasswordField).FromTable(tableName);
            }
        }
    }
}
