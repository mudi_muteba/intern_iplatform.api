﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._01
{
    [Migration(20160122110501)]
    public class Update_Language_Table : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema(Schemas.Master).Table(Tables.Language).Column("ISO").Exists())
                Alter.Table(Tables.Language).InSchema(Schemas.Master).AddColumn("ISO").AsString(50).Nullable();

            Update.Table(Tables.Language).InSchema(Schemas.Master).Set(new { ISO = "af-ZA" }).Where(new { Code = "afr" });
            Update.Table(Tables.Language).InSchema(Schemas.Master).Set(new { ISO = "en-US" }).Where(new { Code = "eng" });
            Update.Table(Tables.Language).InSchema(Schemas.Master).Set(new { ISO = "nr" }).Where(new { Code = "nbl" });
            Update.Table(Tables.Language).InSchema(Schemas.Master).Set(new { ISO = "nso" }).Where(new { Code = "nso" });
            Update.Table(Tables.Language).InSchema(Schemas.Master).Set(new { ISO = "st" }).Where(new { Code = "sot" });
            Update.Table(Tables.Language).InSchema(Schemas.Master).Set(new { ISO = "ss" }).Where(new { Code = "ssw" });
            Update.Table(Tables.Language).InSchema(Schemas.Master).Set(new { ISO = "ts" }).Where(new { Code = "tso" });
            Update.Table(Tables.Language).InSchema(Schemas.Master).Set(new { ISO = "tn-ZA" }).Where(new { Code = "tsn" });
            Update.Table(Tables.Language).InSchema(Schemas.Master).Set(new { ISO = "ve" }).Where(new { Code = "ven" });
            Update.Table(Tables.Language).InSchema(Schemas.Master).Set(new { ISO = "xh" }).Where(new { Code = "xho" });
            Update.Table(Tables.Language).InSchema(Schemas.Master).Set(new { ISO = "zu" }).Where(new { Code = "zul" });
        }
                

        public override void Down()
        {
 
        }
    }
}