﻿using System;
using FluentMigrator;
using Database.Schema;

namespace Database.Migrations._2016._01
{
    [Migration(20160126100601)]
    public class ChannelPermission_Add_IsDeleted : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.ChannelPermission).Column("IsDeleted").Exists())
            {
                Alter
                    .Table(Tables.ChannelPermission)
                    .AddColumn("IsDeleted").AsBoolean().Nullable();

                Update.Table(Tables.ChannelPermission)
                    .Set(new {IsDeleted = false})
                    .AllRows();
            }
        }

        public override void Down()
        {
            //Delete.Column("IsDeleted").FromTable(Tables.ChannelPermission);
        }
    }
}
