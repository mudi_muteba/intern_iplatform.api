﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2016._01
{
    [Migration(20160105134900)]

    public class Individual_External_Reference : Migration
    {
        public override void Up()
        {
            if (!Schema.Table("Individual").Column("ExternalReference").Exists())
                Create.Column("ExternalReference").OnTable("Individual").AsString(40).Nullable();
        }

        public override void Down()
        {

        }
    }
}
