﻿using FluentMigrator;

namespace iBroker.Database.Migrations
{
    [Migration(20160412090130)]
    public class Drop_Master_Data_Tables : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160412082130_Drop_All_FK_Constraints.sql");
            Execute.EmbeddedScript("20160412085730_Drop_Master_Data_Tables.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}