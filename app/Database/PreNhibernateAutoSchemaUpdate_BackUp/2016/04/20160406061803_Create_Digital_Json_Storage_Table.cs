﻿using FluentMigrator;

namespace Database.Migrations._2016._04
{
    [Migration(20160406061803)]
    public class Create_Digital_Json_Storage_Table : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20160406061803_Create_Digital_Json_Storage_Table.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}
