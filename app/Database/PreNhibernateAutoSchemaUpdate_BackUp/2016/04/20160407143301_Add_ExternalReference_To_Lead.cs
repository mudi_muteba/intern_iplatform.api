﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2016._04
{
    [Migration(20160407143301)]
    public class Add_ExternalReference_To_Lead : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.Lead).Column("ExternalReference").Exists())
                Alter.Table(Tables.Lead).AddColumn("ExternalReference").AsString().Nullable();
        }

        public override void Down()
        {
            if (Schema.Table(Tables.Lead).Column("ExternalReference").Exists())
                Delete.Column("ExternalReference").FromTable(Tables.Lead);
        }
    }
}