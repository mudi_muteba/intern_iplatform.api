using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151012105701)]
    public class Add_Quote_Item_To_Quote_Item_State_Table : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.QuoteItemState).Column("QuoteItemId").Exists())
            {
                Alter
                    .Table(Tables.QuoteItemState)
                    .AddColumn("QuoteItemId").AsInt32().Nullable();
            }
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20151014153601_Drop_Index_IX_QuoteItemState_QuoteItemId.sql");

            Delete
                .Column("QuoteItemId")
                .FromTable(Tables.QuoteItemState)
                ;
        }
    }
}