using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151030100901)]
    public class Add_Table_PolicyStatus_MethodOfPayment : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema(Schemas.Master).Table(Tables.MethodOfPayment).Exists())
            {
                Create.Table(Tables.MethodOfPayment).InSchema(Schemas.Master)
                     .WithColumn("Id").AsInt32().NotNullable()
                     .WithColumn("Name").AsString(20).Nullable();

                Insert.IntoTable(Tables.MethodOfPayment)
                    .InSchema(Schemas.Master)
                    .Row(new { Id = 1, Name = "Debit Order" })
                    .Row(new { Id = 2, Name = "EFT/Cash" });
            }

            if (!Schema.Schema(Schemas.Master).Table(Tables.PolicyStatus).Exists())
            {
                Create.Table(Tables.PolicyStatus).InSchema(Schemas.Master)
                     .WithIdColumn()
                     .WithColumn("Name").AsString(20).Nullable()
                     .WithColumn("VisibleIndex").AsInt32().Nullable();

                Insert.IntoTable(Tables.PolicyStatus)
                    .InSchema(Schemas.Master)
                    .Row(new { Name = "Cancelled", VisibleIndex = 0 })
                    .Row(new { Name = "Potential", VisibleIndex = 1 })
                    .Row(new { Name = "Active", VisibleIndex = 2 });
            }
        }

        public override void Down()
        {

        }
    }
}