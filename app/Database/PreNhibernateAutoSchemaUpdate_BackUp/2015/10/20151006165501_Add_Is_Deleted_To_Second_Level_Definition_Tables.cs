using System.Collections.Generic;
using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151006165501)]
    public class Add_Is_Deleted_To_Second_Level_Definition_Tables : Migration
    {
        private readonly List<string> tables = new List<string>()
        {
            Tables.SecondLevelQuestionDefinition,
            Tables.SecondLevelQuestionSavedAnswer
        };

        public override void Up()
        {
            foreach (var table in tables)
            {

                if (!Schema.Table(table).Column("IsDeleted").Exists())
                {
                    Alter
                        .Table(table)
                        .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

                    Update.Table(table)
                        .Set(new {IsDeleted = false})
                        .AllRows();
                }
            }
        }

        public override void Down()
        {
            foreach (var table in tables)
            {
                Delete
                    .Column("IsDeleted")
                    .FromTable(table)
                    ;
            }
        }
    }
}