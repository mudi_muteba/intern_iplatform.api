using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151012153001)]
    public class Add_Lead_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                // Lead
                .Row(new { Id = 18, Name = "Lead"})
                ;

            Insert.IntoTable(Tables.AuthorisationPoint)
                // address
                .Row(new { Id = 44, Name = "Create", AuthorisationPointCategoryId = 18 })
                .Row(new { Id = 45, Name = "Edit", AuthorisationPointCategoryId = 18 })
                .Row(new { Id = 46, Name = "Delete", AuthorisationPointCategoryId = 18 })
                .Row(new { Id = 47, Name = "List", AuthorisationPointCategoryId = 18 })
                .Row(new { Id = 48, Name = "Import", AuthorisationPointCategoryId = 18 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 44, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 45, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 46, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 47, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 48, AuthorisationGroupId = 5 })

                // call centre agent
                .Row(new { AuthorisationPointId = 44, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 45, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 46, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 47, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 48, AuthorisationGroupId = 1 })

                // call centre manager
                .Row(new { AuthorisationPointId = 44, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 45, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 46, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 47, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 48, AuthorisationGroupId = 2 })
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 44, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 45, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 46, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 47, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 48, AuthorisationGroupId = 5 })

                // call centre agent
                .Row(new { AuthorisationPointId = 44, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 45, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 46, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 47, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 48, AuthorisationGroupId = 1 })

                // call centre manager
                .Row(new { AuthorisationPointId = 44, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 45, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 46, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 47, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 48, AuthorisationGroupId = 2 })
                ;

            //Delete.FromTable(Tables.AuthorisationPoint)
            //    .Row(new { Id = 44, Name = "Create", AuthorisationPointCategoryId = 18 })
            //    .Row(new { Id = 45, Name = "Edit", AuthorisationPointCategoryId = 18 })
            //    .Row(new { Id = 46, Name = "Delete", AuthorisationPointCategoryId = 18 })
            //    .Row(new { Id = 47, Name = "List", AuthorisationPointCategoryId = 18 })
            //    .Row(new { Id = 48, Name = "Import", AuthorisationPointCategoryId = 18 })
            //    ;

            //Delete.FromTable(Tables.AuthorisationPointCategory)
            //    .Row(new { Id = 18, Name = "Lead" })
            //    ;

        }
    }
}