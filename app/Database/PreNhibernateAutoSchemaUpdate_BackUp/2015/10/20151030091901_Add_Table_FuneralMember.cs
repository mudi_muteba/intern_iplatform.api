using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151030091901)]
    public class Add_Table_FuneralMember : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema(Schemas.Master).Table(Tables.MemberRelationship).Exists())
            {
                Create.Table(Tables.MemberRelationship).InSchema(Schemas.Master)
                     .WithColumn("Id").AsInt32().NotNullable()
                     .WithColumn("Name").AsString(20).Nullable()
                     .WithColumn("VisibleIndex").AsInt32().Nullable();

                Insert.IntoTable(Tables.MemberRelationship)
                    .InSchema(Schemas.Master)
                    .Row(new { Id = 1, Name = "Spouse", VisibleIndex = 0 })
                    .Row(new { Id = 2, Name = "Mother", VisibleIndex = 1 })
                    .Row(new { Id = 3, Name = "Motherinlaw", VisibleIndex = 2 })
                    .Row(new { Id = 4, Name = "Fatherinlaw", VisibleIndex = 3 })
                    .Row(new { Id = 5, Name = "Brotherinlaw", VisibleIndex = 4 })
                    .Row(new { Id = 6, Name = "Sisterinlaw", VisibleIndex = 5 })
                    .Row(new { Id = 7, Name = "Brother", VisibleIndex = 6 })
                    .Row(new { Id = 8, Name = "Sister", VisibleIndex = 7 })
                    .Row(new { Id = 9, Name = "Aunt", VisibleIndex = 8 })
                    .Row(new { Id = 10, Name = "Uncle", VisibleIndex = 9 })
                    .Row(new { Id = 11, Name = "Domestic worker", VisibleIndex = 10 })
                    .Row(new { Id = 12, Name = "Child", VisibleIndex = 11 })
                    .Row(new { Id = 13, Name = "Additional spouse(s)", VisibleIndex = 12 })
                    .Row(new { Id = 14, Name = "Grandmother", VisibleIndex = 13 })
                    .Row(new { Id = 15, Name = "Grandfather", VisibleIndex = 14 })
                    .Row(new { Id = 16, Name = "Cousin", VisibleIndex = 15 })
                    .Row(new { Id = 17, Name = "Niece", VisibleIndex = 16 })
                    .Row(new { Id = 18, Name = "Nephew", VisibleIndex = 17 });

            }

            Create.Table(Tables.FuneralMember)
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("ProposalDefinitionId").AsInt32().NotNullable()
                .WithColumn("MemberRelationshipId").AsInt32().Nullable()
                .WithColumn("Initials").AsString(5).Nullable()
                .WithColumn("Surname").AsString(100).Nullable()
                .WithColumn("IdNumber").AsString(20).Nullable()
                .WithColumn("IsStudent").AsBoolean().Nullable()
                .WithColumn("GenderId").AsInt32().Nullable()
                .WithColumn("DateOnCover").AsDateTime().Nullable()
                .WithColumn("SumInsured").AsDouble().Nullable()
                .WithColumn("IsDeleted").AsBoolean().Nullable();

            Create.ForeignKey("ProposalDefinition_FuneralMember")
                .FromTable(Tables.FuneralMember).ForeignColumn("ProposalDefinitionId")
                .ToTable(Tables.ProposalDefinition).PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.
                ForeignKey("ProposalDefinition_FuneralMember")
                .OnTable(Tables.FuneralMember);

            Delete.Table(Tables.FuneralMember)
                .InSchema(Schemas.Dbo);
        }
    }
}