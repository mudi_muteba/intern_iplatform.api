using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151027091601)]
    public class Motor_Home_History_Loss : Migration
    {
        public override void Up()
        {
            string table = Tables.MotorLossHistory;
            if (!Schema.Schema(Schemas.Dbo).Table(table).Exists())
            {
                Create.Table(table)
                    .WithIdColumn()
                    .WithColumn("PartyId").AsInt32().NotNullable()
                    .WithColumn("MotorTypeOfLossId").AsInt32().NotNullable()
                    .WithColumn("DateOfLoss").AsString(10).Nullable()
                    .WithColumn("MotorClaimAmountId").AsInt32().Nullable()
                    .WithColumn("CurrentlyInsuredId").AsInt32().Nullable()
                    .WithColumn("UninterruptedPolicyId").AsInt32().Nullable()
                    .WithColumn("InsurerCancel").AsBoolean().Nullable()
                    .WithColumn("MotorCurrentTypeOfCoverId").AsInt32().Nullable()
                    .WithColumn("PreviousComprehensiveInsurance").AsBoolean().Nullable()
                    .WithColumn("MotorUninterruptedPolicyNoClaimId").AsInt32().Nullable()
                    .WithColumn("Why").AsString(255).Nullable()
                    .WithColumn("IsDeleted").AsBoolean().Nullable()
                    ;

            }
            table = Tables.HomeLossHistory;
            if (!Schema.Schema(Schemas.Dbo).Table(table).Exists())
            {
                Create.Table(table)
                    .WithIdColumn()
                    .WithColumn("PartyId").AsInt32().NotNullable()
                    .WithColumn("HomeTypeOfLossId").AsInt32().NotNullable()
                    .WithColumn("DateOfLoss").AsString(10).Nullable()
                    .WithColumn("HomeClaimAmountId").AsInt32().Nullable()
                    .WithColumn("HomeClaimLocationId").AsInt32().Nullable()
                    .WithColumn("CurrentlyInsuredId").AsInt32().Nullable()
                    .WithColumn("UninterruptedPolicyId").AsInt32().Nullable()
                    .WithColumn("InsurerCancel").AsBoolean().Nullable()
                    .WithColumn("Why").AsString(255).Nullable()
                    .WithColumn("IsDeleted").AsBoolean().Nullable()
                    ;
            }
        }

        public override void Down()
        {
            Delete.Table(Tables.HomeLossHistory);
            Delete.Table(Tables.MotorLossHistory);
        }
    }
}