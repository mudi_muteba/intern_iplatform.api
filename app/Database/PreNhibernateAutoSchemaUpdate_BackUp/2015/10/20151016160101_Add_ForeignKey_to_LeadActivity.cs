 using Database.Schema;

using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151016160101)]
    public class Add_ForeignKey_to_LeadActivity : Migration
    {
        public override void Up()
        {

            Create.ForeignKey("Party_LeadActivity")
                .FromTable(Tables.LeadActivity).ForeignColumn("PartyId")
                .ToTable(Tables.Party).PrimaryColumn("Id");

            //Update.Table(Tables.LeadActivity)
            //            .Set(new { ActivityTypeId = 1 })
            //            .AllRows();

            //Create.ForeignKey("ActivityType_LeadActivity")
            //    .FromTable(Tables.LeadActivity).ForeignColumn("ActivityTypeId")
            //    .ToTable(Tables.ActivityType).PrimaryColumn("Id");

        }

        public override void Down()
        {
            Delete.
                ForeignKey("Party_LeadActivity")
                .OnTable(Tables.LeadActivity);

            //Delete.
            //    ForeignKey("ActivityType_LeadActivity")
            //    .OnTable(Tables.LeadActivity);
        }
    }
}