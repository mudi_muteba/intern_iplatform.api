 using Database.Schema;

using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151021091101)]
    public class Add_ForeignKey_to_LeadActivities : Migration
    {
        public override void Up()
        {

            Create.ForeignKey("ProposalHeader_ProposalLeadActivity")
                .FromTable(Tables.ProposalLeadActivity).ForeignColumn("ProposalHeaderId")
                .ToTable(Tables.ProposalHeader).PrimaryColumn("Id");

            Create.ForeignKey("PolicyHeader_PolicyLeadActivity")
                .FromTable(Tables.PolicyLeadActivity).ForeignColumn("PolicyHeaderId")
                .ToTable(Tables.PolicyHeader).PrimaryColumn("Id");

            Create.ForeignKey("ClaimsHeader_ClaimsLeadActivity")
                .FromTable(Tables.ClaimsLeadActivity).ForeignColumn("ClaimsHeaderId")
                .ToTable(Tables.ClaimsHeader).PrimaryColumn("Id");

            Create.ForeignKey("QuoteHeader_QuotedLeadActivity")
                .FromTable(Tables.QuotedLeadActivity).ForeignColumn("QuoteHeaderId")
                .ToTable(Tables.QuoteHeader).PrimaryColumn("Id");

            Create.ForeignKey("Quote_QuoteAcceptedLeadActivity")
                .FromTable(Tables.QuoteAcceptedLeadActivity).ForeignColumn("QuoteId")
                .ToTable(Tables.Quote).PrimaryColumn("Id");
            
        }

        public override void Down()
        {
            Delete.
                ForeignKey("ProposalHeader_ProposalLeadActivity")
                .OnTable(Tables.ProposalLeadActivity);

            Delete.
                ForeignKey("PolicyHeader_PolicyLeadActivity")
                .OnTable(Tables.PolicyLeadActivity);

            Delete.
                ForeignKey("ClaimsHeader_ClaimsLeadActivity")
                .OnTable(Tables.ClaimsLeadActivity);

            Delete.
                ForeignKey("QuoteHeader_QuotedLeadActivity")
                .OnTable(Tables.QuotedLeadActivity);

            Delete.
                ForeignKey("Quote_QuoteAcceptedLeadActivity")
                .OnTable(Tables.QuoteAcceptedLeadActivity);
        }
    }
}