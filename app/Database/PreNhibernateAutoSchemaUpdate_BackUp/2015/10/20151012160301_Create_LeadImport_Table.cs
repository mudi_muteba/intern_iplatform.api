using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151012160301)]
    public class Create_LeadImport_Table : Migration
    {
        public override void Up()
        {
            Create
                 .Table(Tables.LeadImport)
                 .InSchema(Schemas.Dbo)
                 .WithColumn("Id")
                 .AsInt32()
                 .NotNullable()
                 .PrimaryKey()
                 .Identity()
                 .WithColumn("LeadImportReference").AsGuid().NotNullable()
                 .WithColumn("Description").AsString(500).NotNullable()
                 .WithColumn("FileName").AsString(500).NotNullable()
                 .WithColumn("CampaignId").AsInt32().NotNullable()
                 .WithColumn("CreatedBy").AsInt32().NotNullable()
                 .WithColumn("DateCreated").AsDateTime().NotNullable()
                 .WithColumn("DateUpdated").AsDateTime().NotNullable()
                 .WithColumn("DateImported").AsDateTime().Nullable()
                 .WithColumn("Status").AsString(255).NotNullable()
                 .WithColumn("TotalRecords").AsInt32()
                 .WithColumn("TotalImported").AsInt32()
                 .WithColumn("TotalErrors").AsInt32()
                 .WithColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

            Create.ForeignKey("Campaign_LeadImport")
                .FromTable(Tables.LeadImport).ForeignColumn("CampaignId")
                .ToTable(Tables.Campaign).PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.Table(Tables.LeadImport).InSchema(Schemas.Dbo);
        }
    }
}