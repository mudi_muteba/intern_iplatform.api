﻿using Database.Schema;
using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2015._10
{
    [Migration(20151027091602)]
    public class Accessory_Types : Migration
    {
        const string table = "AccessoryType";
        public override void Up()
        {
            Create.Table(table).InSchema(Schemas.Master)
                .WithColumn("Id").AsInt32().NotNullable()
                .WithColumn("Name").AsString(50).NotNullable()
                .WithColumn("Answer").AsString(50).NotNullable()
                .WithColumn("VisibleIndex").AsInt32().WithDefaultValue(0);

            Insert.IntoTable(table)
                .InSchema(Schemas.Master)
                .Row(new { Id = 1, Name = table + " Sound equipment", Answer = "Sound equipment" })
                .Row(new { Id = 2, Name = table + " Mag Wheels", Answer = "Mag Wheels" })
                .Row(new { Id = 3, Name = table + " Sun Roof", Answer = "Sun Roof" })
                .Row(new { Id = 4, Name = table + " Xenon Lights", Answer = "Xenon Lights" })
                .Row(new { Id = 5, Name = table + " Bull Bars or Tow Bars", Answer = @"Bull Bars or Tow Bars" })
                .Row(new { Id = 6, Name = table + " Bokdy Kit (Spoliers)", Answer = "Bokdy Kit (Spoliers)" })
                .Row(new { Id = 7, Name = table + " Anti-Smash and Grab", Answer = "Anti-Smash and Grab" })
                .Row(new { Id = 8, Name = table + " Other", Answer = "Other" })
                ;


            
        }

        public override void Down()
        {
            Delete.Table(table).InSchema(Schemas.Master);
        }
    }
}
