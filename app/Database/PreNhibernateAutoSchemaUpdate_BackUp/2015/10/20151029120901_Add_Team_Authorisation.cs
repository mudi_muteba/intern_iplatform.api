using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151029120901)]
    public class Add_Team_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                // Lead
                .Row(new { Id = 19, Name = "Team"})
                ;

            Insert.IntoTable(Tables.AuthorisationPoint)
                // address
                .Row(new { Id = 49, Name = "Create", AuthorisationPointCategoryId = 19 })
                .Row(new { Id = 50, Name = "Edit", AuthorisationPointCategoryId = 19 })
                .Row(new { Id = 51, Name = "Delete", AuthorisationPointCategoryId = 19 })
                .Row(new { Id = 52, Name = "List", AuthorisationPointCategoryId = 19 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 49, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 50, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 51, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 52, AuthorisationGroupId = 5 })


                // call centre agent
                .Row(new { AuthorisationPointId = 49, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 50, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 51, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 52, AuthorisationGroupId = 1 })

                // call centre manager
                .Row(new { AuthorisationPointId = 49, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 50, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 51, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 52, AuthorisationGroupId = 2 })
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 49, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 50, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 51, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 52, AuthorisationGroupId = 5 })

                // call centre agent
                .Row(new { AuthorisationPointId = 49, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 50, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 51, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 52, AuthorisationGroupId = 1 })

                // call centre manager
                .Row(new { AuthorisationPointId = 49, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 50, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 51, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 52, AuthorisationGroupId = 2 })
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 49, Name = "Create", AuthorisationPointCategoryId = 19 })
                .Row(new { Id = 50, Name = "Edit", AuthorisationPointCategoryId = 19 })
                .Row(new { Id = 51, Name = "Delete", AuthorisationPointCategoryId = 19 })
                .Row(new { Id = 52, Name = "List", AuthorisationPointCategoryId = 19 })
                ;

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 19, Name = "Team" })
                ;

        }
    }
}