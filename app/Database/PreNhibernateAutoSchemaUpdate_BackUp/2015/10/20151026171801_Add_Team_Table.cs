using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151026171801)]
    public class Add_Team_Table : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.Team).Exists())
            {
                Create
                    .Table(Tables.Team)
                    .InSchema(Schemas.Dbo)
                    .WithColumn("Id")
                    .AsInt32()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                    .WithColumn("Name").AsString(255)
                    .WithColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false)
                    .WithColumn("DateCreated").AsDateTime().Nullable().WithDefault(SystemMethods.CurrentUTCDateTime)
                    .WithColumn("DateUpdated").AsDateTime().Nullable().WithDefault(SystemMethods.CurrentUTCDateTime)
                    ;

                Create
                    .Table(Tables.TeamUser)
                    .InSchema(Schemas.Dbo)
                    .WithColumn("Id")
                    .AsInt32()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                    .WithColumn("TeamId").AsInt32().NotNullable()
                    .WithColumn("UserId").AsInt32().NotNullable()
                    .WithColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false)
                    ;

                Create
                    .Table(Tables.TeamCampaign)
                    .InSchema(Schemas.Dbo)
                    .WithColumn("Id")
                    .AsInt32()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                    .WithColumn("TeamId").AsInt32().NotNullable()
                    .WithColumn("CampaignId").AsInt32().NotNullable()
                    .WithColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false)
                    ;
                Create.ForeignKey("Campaign_TeamCampaign")
                    .FromTable(Tables.TeamCampaign).ForeignColumn("CampaignId")
                    .ToTable(Tables.Campaign).PrimaryColumn("Id");

                Create.ForeignKey("Team_TeamCampaign")
                    .FromTable(Tables.TeamCampaign).ForeignColumn("TeamId")
                    .ToTable(Tables.Team).PrimaryColumn("Id");

                Create.ForeignKey("Team_TeamUser")
                    .FromTable(Tables.TeamUser).ForeignColumn("TeamId")
                    .ToTable(Tables.Team).PrimaryColumn("Id");

                Create.ForeignKey("User_TeamUser")
                    .FromTable(Tables.TeamUser).ForeignColumn("UserId")
                    .ToTable(Tables.User).PrimaryColumn("Id");
            }
        }

        public override void Down()
        {
            Delete.ForeignKey("Campaign_TeamCampaign").OnTable(Tables.TeamCampaign);
            Delete.ForeignKey("Team_TeamCampaign").OnTable(Tables.TeamCampaign);
            Delete.ForeignKey("Team_TeamUser").OnTable(Tables.TeamUser);
            Delete.ForeignKey("User_TeamUser").OnTable(Tables.TeamUser);


            Delete.Table(Tables.TeamCampaign).InSchema(Schemas.Dbo);
            Delete.Table(Tables.TeamUser).InSchema(Schemas.Dbo);
            Delete.Table(Tables.Team).InSchema(Schemas.Dbo);
        }
    }
}