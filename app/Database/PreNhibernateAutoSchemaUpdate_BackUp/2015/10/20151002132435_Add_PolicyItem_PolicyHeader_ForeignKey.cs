using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151002132435)]
    public class Add_PolicyItem_PolicyHeader_ForeignKey : Migration
    {
        public override void Up()
        {
            Create.ForeignKey("PolicyHeader_PolicyItem")
                .FromTable(Tables.PolicyItem).ForeignColumn("PolicyHeaderId")
                .ToTable(Tables.PolicyHeader).PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.
                ForeignKey("PolicyHeader_PolicyItem")
                .OnTable(Tables.PolicyItem);
        }
    }
}