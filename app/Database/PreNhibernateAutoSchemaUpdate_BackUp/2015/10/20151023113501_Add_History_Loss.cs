using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151023113501)]
    public class Add_History_Loss : Migration
    {
        public override void Up()
        {
            string table = Tables.ProductLineOfLoss;
            if (!Schema.Schema(Schemas.Master).Table(table).Exists())
            {
                Create.Table(table).InSchema(Schemas.Master)
                    .WithColumn("Id").AsInt32().NotNullable()
                    .WithColumn("Name").AsString(50).NotNullable()
                    .WithColumn("Answer").AsString(50).NotNullable()
                    .WithColumn("VisibleIndex").AsInt32().WithDefaultValue(0);

                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 1, Name = table + " Motor", Answer = "Motor" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 2, Name = table + " Home", Answer = "Home" });
            }

            table = Tables.HomeTypeOfLoss;
            if (!Schema.Schema(Schemas.Master).Table(table).Exists())
            {
                Create.Table(table).InSchema(Schemas.Master)
                    .WithColumn("Id").AsInt32().NotNullable()
                    .WithColumn("Name").AsString(50).NotNullable()
                    .WithColumn("Answer").AsString(50).NotNullable()
                    .WithColumn("VisibleIndex").AsInt32().WithDefaultValue(0);

                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 1, Name = table + " Theft", Answer = "Theft" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 2, Name = table + " Pipe leakage", Answer = "Pipe leakage" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 3, Name = table + " Fire", Answer = "Fire" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 4, Name = table + " Storm", Answer = "Storm" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 5, Name = table + " geysers", Answer = "geysers" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 6, Name = table + " Accidental damage", Answer = "Accidental damage" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 7, Name = table + " subsidence and landslip", Answer = "subsidence and landslip" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 8, Name = table + " Other (if other specify)", Answer = "Other (if other specify)" });
            }
            table = Tables.MotorTypeOfLoss;
            if (!Schema.Schema(Schemas.Master).Table(table).Exists())
            {
                Create.Table(table).InSchema(Schemas.Master)
                    .WithColumn("Id").AsInt32().NotNullable()
                    .WithColumn("Name").AsString(50).NotNullable()
                    .WithColumn("Answer").AsString(50).NotNullable()
                    .WithColumn("VisibleIndex").AsInt32().WithDefaultValue(0);

                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 1, Name = table + " Hijack", Answer = "Hijack" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 2, Name = table + " Accident", Answer = "Accident" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 3, Name = table + " Theft", Answer = "Theft" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 4, Name = table + " Windscreen", Answer = "Windscreen" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 5, Name = table + " Weather and Elements", Answer = "Weather and Elements" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 6, Name = table + " Strike and Riot", Answer = "Strike and Riot" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 7, Name = table + " Third party liability", Answer = "Third party liability" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 8, Name = table + " Fire and Explosion", Answer = "Fire and Explosion" });
            }
            table = Tables.HomeClaimAmount;
            if (!Schema.Schema(Schemas.Master).Table(table).Exists())
            {
                Create.Table(table).InSchema(Schemas.Master)
                    .WithColumn("Id").AsInt32().NotNullable()
                    .WithColumn("Name").AsString(50).NotNullable()
                    .WithColumn("Answer").AsString(50).NotNullable()
                    .WithColumn("VisibleIndex").AsInt32().WithDefaultValue(0);

                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 1, Name = table + " 0", Answer = "0" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 2, Name = table + " 1- 1000", Answer = "1- 1000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 3, Name = table + " 1001-  2000", Answer = "1001-  2000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 4, Name = table + " 2001 - 3000", Answer = "2001 - 3000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 5, Name = table + " 3001 - 4000", Answer = "3001 - 4000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 6, Name = table + " 4001 - 5000", Answer = "4001 - 5000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 7, Name = table + " 5001 - 6000", Answer = "5001 - 6000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 8, Name = table + " 6001 - 7000", Answer = "6001 - 7000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 9, Name = table + " 7001- 8000", Answer = "7001- 8000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 10, Name = table + " 8001  - 9000", Answer = "8001  - 9000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 11, Name = table + " 9001 - 10000", Answer = "9001 - 10000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 12, Name = table + " 10001 - 12000", Answer = "10001 - 12000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 13, Name = table + " 12001 - 14000", Answer = "12001 - 14000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 14, Name = table + " 14001-  16000", Answer = "14001-  16000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 15, Name = table + " 16001 -  18000", Answer = "16001 -  18000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 16, Name = table + " 18001  - 20000", Answer = "18001  - 20000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 17, Name = table + " 20001 - 25000", Answer = "20001 - 25000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 18, Name = table + " 25001 - 30000", Answer = "25001 - 30000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 19, Name = table + " 30001  - 35000", Answer = "30001  - 35000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 20, Name = table + " 35001 - 40000", Answer = "35001 - 40000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 21, Name = table + " 40001 - 45000", Answer = "40001 - 45000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 22, Name = table + " 45001 - 50000", Answer = "45001 - 50000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 23, Name = table + " 50001 - 75000", Answer = "50001 - 75000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 24, Name = table + " 75001  - 100000", Answer = "75001  - 100000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 25, Name = table + " 100001 or more", Answer = "100001 or more" });
            }
            table = Tables.MotorClaimAmount;
            if (!Schema.Schema(Schemas.Master).Table(table).Exists())
            {
                Create.Table(table).InSchema(Schemas.Master)
                    .WithColumn("Id").AsInt32().NotNullable()
                    .WithColumn("Name").AsString(50).NotNullable()
                    .WithColumn("Answer").AsString(50).NotNullable()
                    .WithColumn("VisibleIndex").AsInt32().WithDefaultValue(0);

                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 1, Name = table + " 0", Answer = "0" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 2, Name = table + " 250", Answer = "250" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 3, Name = table + " 500", Answer = "500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 4, Name = table + " 750", Answer = "750" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 5, Name = table + " 1000", Answer = "1000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 6, Name = table + " 1250", Answer = "1250" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 7, Name = table + " 1500", Answer = "1500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 8, Name = table + " 1750", Answer = "1750" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 9, Name = table + " 2000", Answer = "2000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 10, Name = table + " 2250", Answer = "2250" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 11, Name = table + " 2500", Answer = "2500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 12, Name = table + " 2750", Answer = "2750" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 13, Name = table + " 3000", Answer = "3000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 14, Name = table + " 3250", Answer = "3250" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 15, Name = table + " 3500", Answer = "3500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 16, Name = table + " 3750", Answer = "3750" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 17, Name = table + " 4000", Answer = "4000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 18, Name = table + " 4250", Answer = "4250" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 19, Name = table + " 4500", Answer = "4500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 20, Name = table + " 4750", Answer = "4750" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 21, Name = table + " 5000", Answer = "5000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 22, Name = table + " 5500", Answer = "5500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 23, Name = table + " 6000", Answer = "6000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 24, Name = table + " 6500", Answer = "6500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 25, Name = table + " 7000", Answer = "7000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 26, Name = table + " 7500", Answer = "7500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 27, Name = table + " 8000", Answer = "8000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 28, Name = table + " 8500", Answer = "8500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 29, Name = table + " 9000", Answer = "9000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 30, Name = table + " 9500", Answer = "9500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 31, Name = table + " 10000", Answer = "10000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 32, Name = table + " 11000", Answer = "11000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 33, Name = table + " 12000", Answer = "12000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 34, Name = table + " 13000", Answer = "13000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 35, Name = table + " 14000", Answer = "14000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 36, Name = table + " 15000", Answer = "15000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 37, Name = table + " 16000", Answer = "16000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 38, Name = table + " 17000", Answer = "17000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 39, Name = table + " 18000", Answer = "18000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 40, Name = table + " 19000", Answer = "19000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 41, Name = table + " 20000", Answer = "20000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 42, Name = table + " 22500", Answer = "22500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 43, Name = table + " 25000", Answer = "25000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 44, Name = table + " 27500", Answer = "27500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 45, Name = table + " 30000", Answer = "30000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 46, Name = table + " 32500", Answer = "32500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 47, Name = table + " 35000", Answer = "35000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 48, Name = table + " 37500", Answer = "37500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 49, Name = table + " 40000", Answer = "40000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 50, Name = table + " 42500", Answer = "42500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 51, Name = table + " 45000", Answer = "45000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 52, Name = table + " 47500", Answer = "47500" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 53, Name = table + " 50000", Answer = "50000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 54, Name = table + " 55000", Answer = "55000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 55, Name = table + " 60000", Answer = "60000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 56, Name = table + " 65000", Answer = "65000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 57, Name = table + " 70000", Answer = "70000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 58, Name = table + " 75000", Answer = "75000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 59, Name = table + " 80000", Answer = "80000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 60, Name = table + " 85000", Answer = "85000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 61, Name = table + " 90000", Answer = "90000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 62, Name = table + " 95000", Answer = "95000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 63, Name = table + " 100000", Answer = "100000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 64, Name = table + " 125000", Answer = "125000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 65, Name = table + " 150000", Answer = "150000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 66, Name = table + " 175000", Answer = "175000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 67, Name = table + " 200000", Answer = "200000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 68, Name = table + " 225000", Answer = "225000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 69, Name = table + " 250000", Answer = "250000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 70, Name = table + " 275000", Answer = "275000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 71, Name = table + " 300000", Answer = "300000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 72, Name = table + " 325000", Answer = "325000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 73, Name = table + " 350000", Answer = "350000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 74, Name = table + " 375000", Answer = "375000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 75, Name = table + " 400000", Answer = "400000" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 76, Name = table + " >400000A", Answer = ">400000" });
            }
            table = Tables.HomeClaimLocation;
            if (!Schema.Schema(Schemas.Master).Table(table).Exists())
            {
                Create.Table(table).InSchema(Schemas.Master)
                    .WithColumn("Id").AsInt32().NotNullable()
                    .WithColumn("Name").AsString(50).NotNullable()
                    .WithColumn("Answer").AsString(50).NotNullable()
                    .WithColumn("VisibleIndex").AsInt32().WithDefaultValue(0);

                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 1, Name = table + " Main residence", Answer = "Main residence" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 2, Name = table + " Secondary residence", Answer = "Secondary residence" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 3, Name = table + " Previous residence", Answer = "Previous residence" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 4, Name = table + " Holiday home", Answer = "Holiday home" });
            }
            table = Tables.CurrentlyInsured;
            if (!Schema.Schema(Schemas.Master).Table(table).Exists())
            {
                Create.Table(table).InSchema(Schemas.Master)
                    .WithColumn("Id").AsInt32().NotNullable()
                    .WithColumn("Name").AsString(50).NotNullable()
                    .WithColumn("Answer").AsString(50).NotNullable()
                    .WithColumn("VisibleIndex").AsInt32().WithDefaultValue(0);

                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 1, Name = table + " no", Answer = "no" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 2, Name = table + " auto-yes", Answer = "auto-yes" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 3, Name = table + " home-yes", Answer = "home-yes" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 4, Name = table + " auto & home-yes", Answer = "auto & home-yes" });
            }
            table = Tables.UninterruptedPolicy;
            if (!Schema.Schema(Schemas.Master).Table(table).Exists())
            {
                Create.Table(table).InSchema(Schemas.Master)
                    .WithColumn("Id").AsInt32().NotNullable()
                    .WithColumn("Name").AsString(50).NotNullable()
                    .WithColumn("Answer").AsString(50).NotNullable()
                    .WithColumn("VisibleIndex").AsInt32().WithDefaultValue(0);

                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 1, Name = table + " 0", Answer = "0" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 2, Name = table + " 0.5A", Answer = "0.5" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 3, Name = table + " 1.0A", Answer = "1.0" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 4, Name = table + " 1.5A", Answer = "1.5" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 5, Name = table + " 2.0A", Answer = "2.0" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 6, Name = table + " 2.5A", Answer = "2.5" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 7, Name = table + " 3", Answer = "3" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 8, Name = table + " 4", Answer = "4" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 9, Name = table + " 5", Answer = "5" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 10, Name = table + " 6", Answer = "6" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 11, Name = table + " 7", Answer = "7" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 12, Name = table + " 8", Answer = "8" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 13, Name = table + " 9", Answer = "9" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 14, Name = table + " 10B", Answer = "10" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 15, Name = table + " 11", Answer = "11" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 16, Name = table + " 12", Answer = "12" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 17, Name = table + " 13", Answer = "13" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 18, Name = table + " 14", Answer = "14" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 19, Name = table + " 15B", Answer = "15" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 20, Name = table + " 16", Answer = "16" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 21, Name = table + " 17", Answer = "17" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 22, Name = table + " 18", Answer = "18" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 23, Name = table + " 19", Answer = "19" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 24, Name = table + " 20B", Answer = "20" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 25, Name = table + " 21", Answer = "21" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 26, Name = table + " 22", Answer = "22" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 27, Name = table + " 23", Answer = "23" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 28, Name = table + " 24", Answer = "24" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 29, Name = table + " 25B", Answer = "25" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 30, Name = table + " 26", Answer = "26" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 31, Name = table + " 27", Answer = "27" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 32, Name = table + " 28", Answer = "28" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 33, Name = table + " 29", Answer = "29" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 34, Name = table + " 30", Answer = "30" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 35, Name = table + " 31", Answer = "31" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 36, Name = table + " 32", Answer = "32" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 37, Name = table + " 33", Answer = "33" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 38, Name = table + " 34", Answer = "34" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 39, Name = table + " 35", Answer = "35" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 40, Name = table + " 36", Answer = "36" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 41, Name = table + " 37", Answer = "37" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 42, Name = table + " 38", Answer = "38" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 43, Name = table + " 39", Answer = "39" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 44, Name = table + " 40", Answer = "40" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 45, Name = table + " >40C", Answer = ">40" });
            }
            table = Tables.MotorCurrentTypeOfCover;
            if (!Schema.Schema(Schemas.Master).Table(table).Exists())
            {
                Create.Table(table).InSchema(Schemas.Master)
                    .WithColumn("Id").AsInt32().NotNullable()
                    .WithColumn("Name").AsString(50).NotNullable()
                    .WithColumn("Answer").AsString(50).NotNullable()
                    .WithColumn("VisibleIndex").AsInt32().WithDefaultValue(0);

                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 1, Name = table + " Comprehensive", Answer = "Comprehensive" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 2, Name = table + " TP Only", Answer = "TP Only" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 3, Name = table + " Total  loss", Answer = "Total  loss" });
            }
            table = Tables.MotorUninterruptedPolicyNoClaim;
            if (!Schema.Schema(Schemas.Master).Table(table).Exists())
            {
                Create.Table(table).InSchema(Schemas.Master)
                    .WithColumn("Id").AsInt32().NotNullable()
                    .WithColumn("Name").AsString(50).NotNullable()
                    .WithColumn("Answer").AsString(50).NotNullable()
                    .WithColumn("VisibleIndex").AsInt32().WithDefaultValue(0);

                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 1, Name = table + " 0", Answer = "0" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 2, Name = table + " 0.5A", Answer = "0.5" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 3, Name = table + " 1.0A", Answer = "1.0" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 4, Name = table + " 1.5A", Answer = "1.5" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 5, Name = table + " 2.0A", Answer = "2.0" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 6, Name = table + " 2.5A", Answer = "2.5" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 7, Name = table + " 3", Answer = "3" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 8, Name = table + " 4", Answer = "4" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 9, Name = table + " 5", Answer = "5" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 10, Name = table + " 6", Answer = "6" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 11, Name = table + " 7", Answer = "7" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 12, Name = table + " 8", Answer = "8" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 13, Name = table + " 9", Answer = "9" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 14, Name = table + " 10B", Answer = "10" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 15, Name = table + " 11", Answer = "11" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 16, Name = table + " 12", Answer = "12" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 17, Name = table + " 13", Answer = "13" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 18, Name = table + " 14", Answer = "14" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 19, Name = table + " 15B", Answer = "15" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 20, Name = table + " 16", Answer = "16" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 21, Name = table + " 17", Answer = "17" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 22, Name = table + " 18", Answer = "18" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 23, Name = table + " 19", Answer = "19" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 24, Name = table + " 20B", Answer = "20" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 25, Name = table + " 21", Answer = "21" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 26, Name = table + " 22", Answer = "22" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 27, Name = table + " 23", Answer = "23" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 28, Name = table + " 24", Answer = "24" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 29, Name = table + " 25B", Answer = "25" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 30, Name = table + " 26", Answer = "26" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 31, Name = table + " 27", Answer = "27" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 32, Name = table + " 28", Answer = "28" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 33, Name = table + " 29", Answer = "29" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 34, Name = table + " 30", Answer = "30" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 35, Name = table + " 31", Answer = "31" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 36, Name = table + " 32", Answer = "32" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 37, Name = table + " 33", Answer = "33" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 38, Name = table + " 34", Answer = "34" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 39, Name = table + " 35", Answer = "35" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 40, Name = table + " 36", Answer = "36" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 41, Name = table + " 37", Answer = "37" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 42, Name = table + " 38", Answer = "38" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 43, Name = table + " 39", Answer = "39" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 44, Name = table + " 40", Answer = "40" });
                Insert.IntoTable(table).InSchema(Schemas.Master).Row(new { Id = 45, Name = table + " >40C", Answer = ">40" });
            }
        }

        public override void Down()
        {
        }
    }
}