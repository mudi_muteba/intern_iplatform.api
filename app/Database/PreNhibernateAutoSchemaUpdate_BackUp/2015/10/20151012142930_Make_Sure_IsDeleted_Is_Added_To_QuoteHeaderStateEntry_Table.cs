using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20151012142930)]
    public class Make_Sure_IsDeleted_Is_Added_To_QuoteHeaderStateEntry_Table : Migration
    {
        public override void Up()
        {
            if (!Schema.Table("QuoteHeaderStateEntry").Column("IsDeleted").Exists())
            {
                Alter.Table("QuoteHeaderStateEntry")
                    .AddColumn("IsDeleted").AsBoolean().WithDefaultValue(false);
            }
        }

        public override void Down()
        {
            if (Schema.Table("QuoteHeaderStateEntry").Column("IsDeleted").Exists())
            {
                Delete
                    .Column("IsDeleted").FromTable("QuoteHeaderStateEntry");
            }
        }
    }
}