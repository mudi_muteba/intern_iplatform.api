using System.Collections.Generic;
using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151008082501)]
    public class Add_Fees_To_PolicyHeader_Tables : Migration
    {


        public override void Up()
        {
            if (!Schema.Table(Tables.PolicyHeader).Column("Fees").Exists())
            {
                Alter
                    .Table(Tables.PolicyHeader)
                    .AddColumn("Fees").AsDecimal().Nullable();
            }


            Alter.Table(Tables.PolicyItem).AlterColumn("PolicyHeaderId").AsInt32().NotNullable();
        }

        public override void Down()
        {
            Delete
                .Column("Fees")
                .FromTable(Tables.PolicyHeader)
                ;
            Alter.Table(Tables.PolicyItem).AlterColumn("PolicyHeaderId").AsInt32().Nullable();
        }

        
    }
}