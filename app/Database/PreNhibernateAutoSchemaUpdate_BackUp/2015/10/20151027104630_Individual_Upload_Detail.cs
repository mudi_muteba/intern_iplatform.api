using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20151027104630)]
    public class Individual_Upload_Detail : Migration
    {
        public override void Up()
        {
            Alter.Table(Tables.IndividualUploadDetail)
                .AddColumn("CampaignReferenceId").AsInt32().NotNullable()
                .AddColumn("CreatedById").AsInt32().NotNullable()
                .AddColumn("DateCreated").AsDateTime().NotNullable()
                ;
        }

        public override void Down()
        {
            Delete.Column("CampaignReferenceId").Column("CreatedById").Column("DateCreated").FromTable(Tables.IndividualUploadDetail);
        }
    }
}