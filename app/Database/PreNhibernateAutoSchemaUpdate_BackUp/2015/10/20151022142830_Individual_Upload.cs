using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20151022142830)]
    public class Individual_Upload : Migration
    {
        public override void Up()
        {
            Create.Table(Tables.IndividualUploadHeader)
                .WithIdColumn()
                .WithColumn("LeadImportReference").AsGuid().NotNullable()
                .WithColumn("ChannelId").AsInt32().NotNullable()
                .WithColumn("FileName").AsString().NotNullable()
                .WithColumn("CampaignReferenceId").AsInt32().NotNullable()
                .WithColumn("CreatedById").AsInt32().NotNullable()
                .WithColumn("DateCreated").AsDateTime().NotNullable()
                .WithColumn("DateUpdated").AsDateTime()
                .WithColumn("FileRecordCount").AsInt32()
                .WithColumn("NewCount").AsInt32()
                .WithColumn("DuplicateCount").AsInt32()
                .WithColumn("DuplicateFileRecordCount").AsInt32()
                .WithColumn("FailureCount").AsInt32()
                .WithColumn("Status").AsInt32()
                .WithColumn("IsDeleted").AsBoolean()
                ;

            Create.Table(Tables.IndividualUploadDetail)
                .WithIdColumn()
                .WithColumn("LeadImportReference").AsGuid().NotNullable()
                .WithColumn("ChannelId").AsInt32().NotNullable()
                .WithColumn("FileName").AsString().NotNullable()
                .WithColumn("ContactNumber").AsString().NotNullable()
                .WithColumn("DuplicateFileRecordCount").AsInt32()
                .WithColumn("Status").AsInt32()
                .WithColumn("IsDeleted").AsBoolean()
                ;
        }

        public override void Down()
        {
            Delete.Table(Tables.IndividualUploadHeader);
            Delete.Table(Tables.IndividualUploadDetail);
        }
    }
}