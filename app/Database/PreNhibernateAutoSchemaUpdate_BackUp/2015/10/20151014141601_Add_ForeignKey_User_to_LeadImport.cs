using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151014141601)]
    public class Add_ForeignKey_User_to_LeadImport : Migration
    {
        public override void Up()
        {

            Create.ForeignKey("User_LeadImport")
                .FromTable(Tables.LeadImport).ForeignColumn("CreatedBy")
                .ToTable(Tables.User).PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.
                ForeignKey("User_LeadImport")
                .OnTable(Tables.LeadImport);
        }
    }
}