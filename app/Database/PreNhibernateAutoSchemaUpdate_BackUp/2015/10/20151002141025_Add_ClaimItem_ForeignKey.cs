using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._10
{
    [Migration(20151002141025)]
    public class Add_ClaimItem_ForeignKey : Migration
    {
        public override void Up()
        {
            Create.ForeignKey("ClaimsHeader_ClaimsItem")
                .FromTable(Tables.ClaimsItem).ForeignColumn("ClaimsHeaderId")
                .ToTable(Tables.ClaimsHeader).PrimaryColumn("Id");

            Create.ForeignKey("PolicyItem_ClaimsItem")
                .FromTable(Tables.ClaimsItem).ForeignColumn("PolicyItemId")
                .ToTable(Tables.PolicyItem).PrimaryColumn("Id");

            Create.ForeignKey("ClaimsItem_ClaimsItemQuestionAnswer")
                .FromTable(Tables.ClaimsItemQuestionAnswer).ForeignColumn("ClaimsItemId")
                .ToTable(Tables.ClaimsItem).PrimaryColumn("Id");

 
        }

        public override void Down()
        {
            Delete.
                ForeignKey("ClaimsHeader_ClaimsItem")
                .OnTable(Tables.ClaimsItem);

            Delete.
                ForeignKey("PolicyItem_ClaimsItem")
                .OnTable(Tables.ClaimsItem);

            Delete.
                ForeignKey("ClaimsItem_ClaimsItemQuestionAnswer")
                .OnTable(Tables.ClaimsItemQuestionAnswer);
        }
    }
}