using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20151103112430)]
    public class Individual_Upload_Header_FailureCount_Nullable : Migration
    {
        public override void Up()
        {
            Alter.Column("FailureCount").OnTable(Tables.IndividualUploadHeader).AsInt32().Nullable();
        }

        public override void Down()
        {
            Alter.Column("FailureCount").OnTable(Tables.IndividualUploadHeader).AsInt32().NotNullable();
        }
    }
}