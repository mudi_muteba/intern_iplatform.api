using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20151103104830)]
    public class Individual_Upload_Header_Rename_DuplicateCount : Migration
    {
        public override void Up()
        {
            Rename.Column("DuplicateCount").OnTable(Tables.IndividualUploadHeader).To("ExistingCount");
        }

        public override void Down()
        {
            Rename.Column("ExistingCount").OnTable(Tables.IndividualUploadHeader).To("DuplicateCount");
        }
    }
}