﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2015._11
{
    [Migration(20151119112501)]
    public class PolicyHeader_ExternalId : Migration
    {
        //20151119112501_PolicyHeader_ExternalId.cs
        public override void Up()
        {
            if (!Schema.Table("PolicyHeader").Column("ExternalPlatformGuid").Exists())
                Create.Column("ExternalPlatformGuid").OnTable("PolicyHeader").AsGuid().Nullable();
        }

        public override void Down()
        {

        }
    }
}
