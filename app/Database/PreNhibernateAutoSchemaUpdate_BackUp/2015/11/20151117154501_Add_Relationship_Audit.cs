using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._11
{
    [Migration(20151117154501)]
    public class Add_Relationship_Audit : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.Relationship).Column("Audit").Exists())
                Alter.Table(Tables.Relationship).AddColumn("Audit").AsString(int.MaxValue).Nullable();
        }

        public override void Down()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.Relationship).Column("Audit").Exists())
                Delete.Column("Audit").FromTable(Tables.Relationship).InSchema(Schemas.Dbo);
        }
    }
}