﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2015._11
{
    [Migration(20151110152500)]
    public class QuotePolicyHeaderAddPlatformId : Migration
    {
        public override void Up()
        {
            if (!Schema.Table("Quote").Column("PlatformId").Exists())
                Alter.Table("Quote").AddColumn("PlatformId").AsGuid().Nullable();

            if (!Schema.Table("PolicyHeader").Column("PlatformId").Exists())
                Alter.Table("PolicyHeader").AddColumn("PlatformId").AsGuid().Nullable();

            if (!Schema.Table("PolicyHeader").Column("QuoteId").Exists())
                Alter.Table("PolicyHeader").AddColumn("QuoteId").AsInt32().Nullable();

           // Execute.EmbeddedScript("20151110152500_QuotePolicyHeaderAddPlatformId.sql");
        }

        public override void Down()
        {

        }
    }
}
