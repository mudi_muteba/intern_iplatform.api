using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._11
{
    [Migration(20151116094301)]
    public class Add_Contact_Audit : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.Contact).Column("Audit").Exists())
                Alter.Table(Tables.Contact).AddColumn("Audit").AsString(int.MaxValue).Nullable();
        }

        public override void Down()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.Contact).Column("Audit").Exists())
                Delete.Column("Audit").FromTable(Tables.Contact).InSchema(Schemas.Dbo);
        }
    }
}