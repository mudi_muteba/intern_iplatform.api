using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._11
{
    [Migration(20151102141301)]
    public class Add_FuneralMember_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                // Lead
                .Row(new { Id = 20, Name = "FuneralMember"})
                ;

            Insert.IntoTable(Tables.AuthorisationPoint)
                // address
                .Row(new { Id = 53, Name = "Create", AuthorisationPointCategoryId = 20 })
                .Row(new { Id = 54, Name = "Edit", AuthorisationPointCategoryId = 20 })
                .Row(new { Id = 55, Name = "Delete", AuthorisationPointCategoryId = 20 })
                .Row(new { Id = 56, Name = "List", AuthorisationPointCategoryId = 20 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 53, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 54, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 55, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 56, AuthorisationGroupId = 5 })


                // call centre agent
                .Row(new { AuthorisationPointId = 53, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 54, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 55, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 56, AuthorisationGroupId = 1 })


                // call centre manager
                .Row(new { AuthorisationPointId = 53, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 54, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 55, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 56, AuthorisationGroupId = 2 })

                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 53, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 54, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 55, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 56, AuthorisationGroupId = 5 })


                // call centre agent
                .Row(new { AuthorisationPointId = 53, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 54, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 55, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 56, AuthorisationGroupId = 1 })


                // call centre manager
                .Row(new { AuthorisationPointId = 53, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 54, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 55, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 56, AuthorisationGroupId = 2 })
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 53, Name = "Create", AuthorisationPointCategoryId = 20 })
                .Row(new { Id = 54, Name = "Edit", AuthorisationPointCategoryId = 20 })
                .Row(new { Id = 55, Name = "Delete", AuthorisationPointCategoryId = 20 })
                .Row(new { Id = 56, Name = "List", AuthorisationPointCategoryId = 20 })
                ;

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new { Id = 20, Name = "FuneralMember" })
                ;

        }
    }
}