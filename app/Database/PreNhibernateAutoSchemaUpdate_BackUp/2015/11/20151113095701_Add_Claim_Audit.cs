using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._11
{
    [Migration(20151113095701)]
    public class Add_Claim_Audit : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.ClaimsHeader).Column("Audit").Exists())
                Alter.Table(Tables.ClaimsHeader).AddColumn("Audit").AsString(int.MaxValue).Nullable();

            Alter.Table(Tables.ClaimsHeader).AddColumn("ChannelId").AsInt32().Nullable();

            //Create.ForeignKey("Channel_ClaimsHeader")
            //    .FromTable(Tables.ClaimsHeader).ForeignColumn("ChannelId")
            //    .ToTable(Tables.Channel).PrimaryColumn("Id");

        }

        public override void Down()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.ClaimsHeader).Column("Audit").Exists())
               Delete.Column("Audit").FromTable(Tables.ClaimsHeader).InSchema(Schemas.Dbo);

            //Delete.ForeignKey("Channel_ClaimsHeader").OnTable(Tables.ClaimsHeader);

            Delete.Column("ChannelId").FromTable(Tables.ClaimsHeader).InSchema(Schemas.Dbo); 
        }
    }
}