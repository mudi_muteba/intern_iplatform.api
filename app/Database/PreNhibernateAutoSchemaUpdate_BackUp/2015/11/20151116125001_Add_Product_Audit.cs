using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._11
{
    [Migration(20151116125001)]
    public class Add_Product_Audit : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.Product).Column("Audit").Exists())
                Alter.Table(Tables.Product).AddColumn("Audit").AsString(int.MaxValue).Nullable();
        }

        public override void Down()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.Product).Column("Audit").Exists())
                Delete.Column("Audit").FromTable(Tables.Product).InSchema(Schemas.Dbo);
        }
    }
}