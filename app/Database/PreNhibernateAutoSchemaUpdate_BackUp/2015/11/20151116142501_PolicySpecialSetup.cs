﻿using Database.Schema;
using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2015._11
{

    [Migration(20151116142501)]
    public class PolicySpecialSetup : Migration
    {
        public override void Up()
        {
            if (!Schema.Table("PolicySpecial").Exists())
            Create.Table("PolicySpecial").InSchema(Schemas.Dbo)
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("AddressId")
                .AsInt32()
                .ForeignKey("FK_PolicySpecialAddressId_AddressId", "Address", "Id")
                .Nullable()
                .WithColumn("Description").AsString(255).Nullable()
                .WithColumn("DateEffective").AsDateTime().Nullable()
                .WithColumn("DateEnd").AsDateTime().Nullable()
                .WithColumn("DateCreated").AsDateTime().Nullable()
                .WithColumn("DateUpdated").AsDateTime().Nullable()
                .WithColumn("IsDeleted").AsDateTime().Nullable();
        }

        public override void Down()
        {

        }
    }
}
