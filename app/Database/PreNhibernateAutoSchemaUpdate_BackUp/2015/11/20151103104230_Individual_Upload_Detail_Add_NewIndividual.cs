using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20151103104230)]
    public class Individual_Upload_Detail_Add_NewIndividual : Migration
    {
        public override void Up()
        {
            Alter.Table(Tables.IndividualUploadDetail)
                .AddColumn("NewIndividual").AsBoolean().NotNullable()
                ;
        }

        public override void Down()
        {
            Delete.Column("NewIndividual").FromTable(Tables.IndividualUploadDetail);
        }
    }
}