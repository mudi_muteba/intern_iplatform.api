using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._11
{
    [Migration(20151110111301)]
    public class Add_Party_Audit : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.Party).Column("Audit").Exists())
                Alter.Table(Tables.Party).AddColumn("Audit").AsString(int.MaxValue).Nullable();
        }

        public override void Down()
        {
            //Delete.Column("Audit").FromTable(Tables.Campaign);
        }
    }
}