using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._11
{
    [Migration(20151109152101)]
    public class Add_Campaign_Audit : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.Campaign).Column("Audit").Exists())
                Alter.Table(Tables.Campaign).AddColumn("Audit").AsString(int.MaxValue).Nullable();
        }

        public override void Down()
        {
            //Delete.Column("Audit").FromTable(Tables.Campaign);
        }
    }
}