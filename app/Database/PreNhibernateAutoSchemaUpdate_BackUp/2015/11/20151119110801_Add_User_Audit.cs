using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._11
{
    [Migration(20151119110801)]
    public class Add_User_Audit : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.User).Column("Audit").Exists())
                Alter.Table(Tables.User).AddColumn("Audit").AsString(int.MaxValue).Nullable();
        }

        public override void Down()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.User).Column("Audit").Exists())
                Delete.Column("Audit").FromTable(Tables.User).InSchema(Schemas.Dbo);
        }
    }
}