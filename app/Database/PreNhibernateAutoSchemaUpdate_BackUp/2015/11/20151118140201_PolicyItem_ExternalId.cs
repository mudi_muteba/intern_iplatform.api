﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2015._11
{
    [Migration(20151118140201)]
    public class PolicyItem_ExternalId : Migration
    {
        //20151118140201_PolicyItem_ExternalId.cs
        public override void Up()
        {
            if (!Schema.Table("PolicyItem").Column("ExternalGuid").Exists())
                Create.Column("ExternalGuid").OnTable("PolicyItem").AsGuid().Nullable();
            if (!Schema.Table("PolicyItem").Column("ExternalMasterGuid").Exists())
                Create.Column("ExternalMasterGuid").OnTable("PolicyItem").AsGuid().Nullable();
        }

        public override void Down()
        {

        }
    }
}
