using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150922124401)]
    public class Add_Quote_State_Related_Field : Migration
    {
        public override void Up()
        {
            Alter
                .Table(Tables.QuoteHeader)
                .AddColumn("StateId").AsInt32().Nullable();

            Alter
                .Table(Tables.Quote)
                .AddColumn("StateId").AsInt32().Nullable();
        }

        public override void Down()
        {
            Delete
                .Column("StateId").FromTable(Tables.QuoteHeader);

            Delete
                .Column("StateId").FromTable(Tables.Quote);
        }
    }
}