using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150923073301)]
    public class Add_QuoteDistribution_Table : Migration
    {
        public override void Up()
        {
            Create.Table(Tables.QuoteDistribution)
                .WithIdColumn()
                .WithColumn("QuoteId").AsInt32().NotNullable()
                .WithColumn("DistributionMethod").AsString(512).NotNullable()
                .WithColumn("ProviderName").AsString(512).NotNullable()
                .WithColumn("ProviderReference").AsString(512).NotNullable()
                .WithColumn("DateTracked").AsDateTime().NotNullable()
                .WithColumn("IsDeleted").AsBoolean().NotNullable().WithDefaultValue(false)
                ;
        }

        public override void Down()
        {
            Delete.Table(Tables.QuoteDistribution);
        }
    }
}