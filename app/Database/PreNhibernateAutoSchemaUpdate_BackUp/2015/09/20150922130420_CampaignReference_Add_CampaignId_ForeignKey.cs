using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150922130420)]
    public class CampaignReference_Add_CampaignId_ForeignKey : Migration
    {
        public override void Up()
        {
            Create.ForeignKey("Campaign_CampaingnReference")
                .FromTable(Tables.CampaignReference).ForeignColumn("CampaignId")
                .ToTable(Tables.Campaign).PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.
                ForeignKey("Campaign_CampaingnReference")
                .OnTable(Tables.CampaignReference);
        }
    }
}