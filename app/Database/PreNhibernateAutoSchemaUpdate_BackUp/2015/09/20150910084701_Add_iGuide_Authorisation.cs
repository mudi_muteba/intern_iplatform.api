using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150910084701)]
    public class Add_iGuide_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new {Id = 16, Name = "iGuide"})
                ;

            Insert.IntoTable(Tables.AuthorisationPoint)
                .Row(new {Id = 37, Name = "Access", AuthorisationPointCategoryId = 16});

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new {AuthorisationPointId = 37, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 37, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 37, AuthorisationGroupId = 3})
                .Row(new {AuthorisationPointId = 37, AuthorisationGroupId = 4})
                .Row(new {AuthorisationPointId = 37, AuthorisationGroupId = 5})
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new {AuthorisationPointId = 37, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 37, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 37, AuthorisationGroupId = 3})
                .Row(new {AuthorisationPointId = 37, AuthorisationGroupId = 4})
                .Row(new {AuthorisationPointId = 37, AuthorisationGroupId = 5})
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new {Id = 37 })
                ;

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new {Id = 16 });
        }
    }
}