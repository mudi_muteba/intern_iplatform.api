﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2015._09
{
    [Migration(20150901132601)]
    public class PolicyHeader_TypeColumns : Migration
    {
        public override void Up()
        {
            if (!Schema.Table("PolicyHeader").Column("PolicyStatusId").Exists())
                Alter.Table("PolicyHeader").AddColumn("PolicyStatusId").AsInt32().Nullable();

            if (!Schema.Table("PolicyHeader").Column("PaymentPlanId").Exists())
                Alter.Table("PolicyHeader").AddColumn("PaymentPlanId").AsInt32().Nullable();

        }

        public override void Down()
        {
        }
    }
}
