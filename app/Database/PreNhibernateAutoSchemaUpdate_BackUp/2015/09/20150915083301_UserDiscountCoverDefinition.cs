﻿using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150915083301)]
    public class UserDiscountCoverDefinitionAdd : Migration
    {
        public override void Up()
        {
            Create
                .Table("UserDiscountCoverDefinition")
                .WithIdColumn()
                .WithColumn("UserId").AsInt32()
                .WithColumn("CoverDefinitionId").AsInt32()
                .WithColumn("Discount").AsDecimal()
                .WithColumn("IsDeleted").AsInt32();
        }

        public override void Down()
        {
            Delete.Table("UserDiscountCoverDefinition");
        }
    }
}