using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150921065001)]
    public class Add_iPerson_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPointCategory)
                .Row(new {Id = 17, Name = "iPerson"})
                ;

            Insert.IntoTable(Tables.AuthorisationPoint)
                .Row(new {Id = 38, Name = "PersonLookup", AuthorisationPointCategoryId = 17});

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                .Row(new {AuthorisationPointId = 38, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 38, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 38, AuthorisationGroupId = 3})
                .Row(new {AuthorisationPointId = 38, AuthorisationGroupId = 4})
                .Row(new {AuthorisationPointId = 38, AuthorisationGroupId = 5})
                ;
        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                .Row(new {AuthorisationPointId = 38, AuthorisationGroupId = 1})
                .Row(new {AuthorisationPointId = 38, AuthorisationGroupId = 2})
                .Row(new {AuthorisationPointId = 38, AuthorisationGroupId = 3})
                .Row(new {AuthorisationPointId = 38, AuthorisationGroupId = 4})
                .Row(new {AuthorisationPointId = 38, AuthorisationGroupId = 5})
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new {Id = 38 })
                ;

            Delete.FromTable(Tables.AuthorisationPointCategory)
                .Row(new {Id = 17 });
        }
    }
}