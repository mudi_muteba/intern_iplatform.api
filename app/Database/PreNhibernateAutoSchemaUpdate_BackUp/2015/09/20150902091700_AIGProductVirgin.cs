﻿using FluentMigrator;

namespace iBroker.Database.Migrations
{
    [Migration(20150902091700)]
    public class AIGProductVirgin : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150902091700_AIGProductVirgin.add.sql");
        }

        public override void Down()
        {
        }
    }
}