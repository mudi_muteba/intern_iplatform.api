using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150929151752)]
    public class Add_ProductClaimsQuestionDefinition_Table : Migration
    {
        public override void Up()
        {
            if (!Schema.Schema(Schemas.Dbo).Table(Tables.ProductClaimsQuestionDefinition).Exists())
            { 
                Create
                    .Table(Tables.ProductClaimsQuestionDefinition)
                    .InSchema(Schemas.Dbo)
                    .WithColumn("Id")
                    .AsInt32()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                    .WithColumn("ProductId").AsInt32()
                    .WithColumn("ClaimsQuestionDefinitionId").AsInt32()
                    .WithColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);
                ;

                Create.ForeignKey("Product_PCQD")
                    .FromTable(Tables.ProductClaimsQuestionDefinition).ForeignColumn("ProductId")
                    .ToTable(Tables.Product).PrimaryColumn("Id");

                Execute.EmbeddedScript("20150929153625_Insert_Data_ProductClaimsQuestionDefinition.sql");
            }
        }

        public override void Down()
        {
            Delete.Table(Tables.ProductClaimsQuestionDefinition).InSchema(Schemas.Dbo);
        }
    }
}