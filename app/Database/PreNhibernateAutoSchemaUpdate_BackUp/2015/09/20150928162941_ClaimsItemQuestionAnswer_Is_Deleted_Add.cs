using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150928162941)]
    public class ClaimsItemQuestionAnswer : Migration
    {
        public override void Up()
        {
            Alter
                .Table("ClaimsItemQuestionAnswer")
                .AddColumn("IsDeleted").AsBoolean().Nullable();

            Update.Table("ClaimsItemQuestionAnswer")
                .Set(new { IsDeleted = false })
                .AllRows();
        }

        public override void Down()
        {
            Delete.Column("IsDeleted").FromTable("ClaimsItemQuestionAnswer");
        }
    }
}