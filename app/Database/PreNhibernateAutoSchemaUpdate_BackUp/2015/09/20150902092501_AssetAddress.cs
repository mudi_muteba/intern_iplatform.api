﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150902092501)]
    public class AssetAddress : Migration
    {
        public override void Up()
        {
            //Create tables
            Create.Table(Tables.AssetAddress)
                .WithColumn("AssetId").AsInt32().NotNullable().Unique()
                .WithColumn("AddressId").AsInt32().NotNullable();


            Create.ForeignKey("FK_AssetAddress_Asset")
                .FromTable(Tables.AssetAddress).ForeignColumn("AssetId")
                .ToTable("Asset").PrimaryColumn("Id");

            Create.ForeignKey("FK_AssetAddress_Address")
                .FromTable(Tables.AssetAddress).ForeignColumn("AddressId")
                .ToTable("Address").PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.ForeignKey("FK_AssetAddress_Asset").OnTable(Tables.AssetAddress);
            Delete.ForeignKey("FK_AssetAddress_Address").OnTable(Tables.AssetAddress);
            Delete.Table(Tables.AssetAddress);
        }
    }
}
