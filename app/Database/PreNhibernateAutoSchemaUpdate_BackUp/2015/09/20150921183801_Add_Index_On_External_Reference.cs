using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150921183801)]
    public class Add_Index_On_External_Reference : Migration
    {
        private string indexName = "Proposal_Header_External_Reference";

        public override void Up()
        {
            Create.Index(indexName)
                .OnTable(Tables.ProposalHeader)
                .InSchema("dbo")
                .OnColumn("ExternalReference");
        }

        public override void Down()
        {
            Delete.Index(indexName)
                .OnTable(Tables.ProposalHeader)
                .InSchema("dbo");
        }
    }
}