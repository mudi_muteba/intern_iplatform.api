﻿using Database;
using Database.Schema;
using FluentMigrator;
using FluentMigrator.Runner.Extensions;

namespace iBroker.Database.Migrations
{
    [Migration(20150910130301)]
    public class PartyCorrespondence : Migration
    {
        public override void Up()
        {
            Create.Table("CorrespondenceType")
                .InSchema(Schemas.Master)
                .WithIdColumn()
                .WithColumn("Name").AsString(200).NotNullable().Unique()
                .WithColumn("Code").AsString(200).NotNullable().Unique()
                .WithColumn("VisibleIndex").AsInt32().NotNullable();

            Insert.IntoTable("CorrespondenceType")
                .InSchema(Schemas.Master)
                .WithIdentityInsert()
                .Row(new {Id = 1, Name = "Policy Documents", Code = "PolicyDocuments", VisibleIndex = 0})
                .Row(new {Id = 2, Name = "General Documents", Code = "GeneralDocuments", VisibleIndex = 1})
                .Row(new {Id = 3, Name = "Marketing Information", Code = "MarketingInformation", VisibleIndex = 2});


            Create.Table("PartyCorrespondencePreference")
                .WithIdColumn()
                .WithColumn("PartyId").AsInt32().NotNullable()
                .WithColumn("CorrespondenceTypeId").AsInt32().NotNullable()
                .WithColumn("Email").AsBoolean()
                .WithColumn("Telephone").AsBoolean()
                .WithColumn("SMS").AsBoolean()
                .WithColumn("Post").AsBoolean()
                .WithColumn("VisibleIndex").AsInt32().NotNullable();
        }

        public override void Down()
        {
            Delete.Table("CorrespondenceType").InSchema(Schemas.Master);
            Delete.Table("PartyCorrespondencePreference");
        }
    }
}