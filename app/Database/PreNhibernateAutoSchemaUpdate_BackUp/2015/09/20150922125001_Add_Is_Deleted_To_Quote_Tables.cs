using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150922125001)]
    public class Add_Is_Deleted_To_Quote_Tables : Migration
    {
        public override void Up()
        {
            Alter
                .Table(Tables.QuoteHeaderState)
                .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

            Alter
                .Table(Tables.QuoteItem)
                .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

            Alter
                .Table(Tables.QuoteState)
                .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

            Alter
                .Table(Tables.QuoteItemState)
                .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete
                .Column("IsDeleted").FromTable(Tables.QuoteHeaderState);

            Delete
                .Column("IsDeleted").FromTable(Tables.QuoteItem);

            Delete
                .Column("IsDeleted").FromTable(Tables.QuoteState);

            Delete
                .Column("IsDeleted").FromTable(Tables.QuoteItemState);
        }
    }
}