﻿using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150911104301)]
    public class PartyCorrespondence_DeleteVisibleIndex : Migration
    {
        public override void Up()
        {
            Delete.Column("VisibleIndex").FromTable("PartyCorrespondencePreference");
        }

        public override void Down()
        {
        }
    }
}