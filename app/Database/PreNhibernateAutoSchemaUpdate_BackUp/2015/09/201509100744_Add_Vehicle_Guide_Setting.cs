using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(201509100744)]
    public class Add_Vehicle_Guide_Setting : Migration
    {
        public override void Up()
        {
            Create.Table(Tables.VehicleGuideSetting)
                .WithIdColumn()
                .WithColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false)
                .WithColumn("ChannelId").AsInt32().NotNullable()
                .WithColumn("CountryId").AsInt32().NotNullable()
                .WithColumn("ApiKey").AsString(512).NotNullable()
                .WithColumn("Email").AsString(512).NotNullable()
                .WithColumn("MMBookEnabled").AsBoolean().NotNullable().WithDefaultValue(true)
                ;
        }

        public override void Down()
        {
            Delete.Table(Tables.VehicleGuideSetting);
        }
    }
}