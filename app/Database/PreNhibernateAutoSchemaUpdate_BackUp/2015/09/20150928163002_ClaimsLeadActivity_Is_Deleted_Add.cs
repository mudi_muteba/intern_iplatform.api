using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150928163002)]
    public class ClaimsLeadActivity_Is_Deleted_Add : Migration
    {
        public override void Up()
        {
            Alter
                .Table("ClaimsLeadActivity")
                .AddColumn("IsDeleted").AsBoolean().Nullable();

            Update.Table("ClaimsLeadActivity")
                .Set(new { IsDeleted = false })
                .AllRows();
        }

        public override void Down()
        {
            Delete.Column("IsDeleted").FromTable("ClaimsLeadActivity");
        }
    }
}