using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150926125302)]
    public class Add_ActivityType_Table : Migration
    {
        public override void Up()
        {

            Create
                .Table(Tables.ActivityType)
                .InSchema(Schemas.Master)
                .WithColumn("Id").AsInt32().NotNullable()
                .WithColumn("Name").AsString().NotNullable();
            ;


            Insert.IntoTable(Tables.ActivityType).InSchema(Schemas.Master).Row(new { Id = 1, Name = "Created" });
            Insert.IntoTable(Tables.ActivityType).InSchema(Schemas.Master).Row(new { Id = 2, Name = "Imported" });
            Insert.IntoTable(Tables.ActivityType).InSchema(Schemas.Master).Row(new { Id = 3, Name = "Proposal Created" });
            Insert.IntoTable(Tables.ActivityType).InSchema(Schemas.Master).Row(new { Id = 4, Name = "Quote Requested" });
            Insert.IntoTable(Tables.ActivityType).InSchema(Schemas.Master).Row(new { Id = 5, Name = "Quote Accepted" });
            Insert.IntoTable(Tables.ActivityType).InSchema(Schemas.Master).Row(new { Id = 6, Name = "Claims Created" });
            Insert.IntoTable(Tables.ActivityType).InSchema(Schemas.Master).Row(new { Id = 7, Name = "Delayed" });
            Insert.IntoTable(Tables.ActivityType).InSchema(Schemas.Master).Row(new { Id = 8, Name = "Dead" });
            Insert.IntoTable(Tables.ActivityType).InSchema(Schemas.Master).Row(new { Id = 9, Name = "Loss" });
            Insert.IntoTable(Tables.ActivityType).InSchema(Schemas.Master).Row(new { Id = 10, Name = "Policy Created" });

            Alter.Table(Tables.LeadActivity).AddColumn("ActivityTypeId").AsInt32().Nullable();

        }

        public override void Down()
        {
            Delete.Column("ActivityTypeId").FromTable(Tables.LeadActivity);
            Delete.Table(Tables.ActivityType).InSchema(Schemas.Master);
        }
    }
}