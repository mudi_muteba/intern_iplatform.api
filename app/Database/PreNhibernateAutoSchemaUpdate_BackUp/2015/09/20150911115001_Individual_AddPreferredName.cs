﻿using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150911115001)]
    public class Individual_AddPreferredName : Migration
    {
        public override void Up()
        {
            Create.Column("PreferredName").OnTable("Individual").AsString(255).Nullable();
        }

        public override void Down()
        {
            Delete.Column("PreferredName").FromTable("Individual");
        }
    }
}