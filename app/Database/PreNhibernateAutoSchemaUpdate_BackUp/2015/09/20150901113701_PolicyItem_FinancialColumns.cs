﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations._2015._09
{
    [Migration(20150901113701)]
    public class PolicyItem_FinancialColumns : Migration
    {
        public override void Up()
        {
            if (!Schema.Table("PolicyItem").Column("SumInsured").Exists())
                Alter.Table("PolicyItem").AddColumn("SumInsured").AsDecimal(18, 4).Nullable();
            if (!Schema.Table("PolicyItem").Column("Premium").Exists())
                Alter.Table("PolicyItem").AddColumn("Premium").AsDecimal(18, 4).Nullable();
            if (!Schema.Table("PolicyItem").Column("Sasria").Exists())
                Alter.Table("PolicyItem").AddColumn("Sasria").AsDecimal(18, 4).Nullable();
            if (!Schema.Table("PolicyItem").Column("ExcessBasic").Exists())
                Alter.Table("PolicyItem").AddColumn("ExcessBasic").AsDecimal(18, 4).Nullable();
            if (!Schema.Table("PolicyItem").Column("VoluntaryExcess").Exists())
                Alter.Table("PolicyItem").AddColumn("VoluntaryExcess").AsDecimal(18, 4).Nullable();
            if (!Schema.Table("PolicyItem").Column("CoverId").Exists())
                Alter.Table("PolicyItem").AddColumn("CoverId").AsInt32().Nullable();
        }

        public override void Down()
        {

        }
    }
}
