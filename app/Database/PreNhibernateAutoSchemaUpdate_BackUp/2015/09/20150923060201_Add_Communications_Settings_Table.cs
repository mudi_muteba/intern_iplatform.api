using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150923060201)]
    public class Add_Communications_Settings_Table : Migration
    {
        public override void Up()
        {
            Create.Table(Tables.CommunicationSettings)
                .WithIdColumn()
                .WithColumn("ChannelId").AsGuid().NotNullable()
                .WithColumn("SMSProviderName").AsString(512).NotNullable()
                .WithColumn("SMSUserName").AsString(512).NotNullable()
                .WithColumn("SMSPassword").AsString(512).NotNullable()
                .WithColumn("SMSUrl").AsString(512).NotNullable()
                .WithColumn("IsDeleted").AsBoolean().NotNullable().WithDefaultValue(false)
                ;
        }

        public override void Down()
        {
            Delete.Table(Tables.CommunicationSettings);
        }
    }
}