using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150922161301)]
    public class Change_Request_Id_To_String_On_Quote_Header : Migration
    {
        public override void Up()
        {
            Alter.Table(Tables.QuoteHeader)
                .AddColumn("ExternalReference")
                .AsString(512)
                .Nullable();

            // copy data from RequestId to ExternalReference
            Execute.EmbeddedScript("20150922161301_Change_Request_Id_To_String_On_Quote_Header.up.sql");

            Delete.Column("RequestId")
                .FromTable(Tables.QuoteHeader);
        }

        public override void Down()
        {
            Alter.Table(Tables.QuoteHeader)
                .AddColumn("RequestId").AsGuid().Nullable();

            // copy data from ExternalReference to RequestId, where it can
            Execute.EmbeddedScript("20150922161301_Change_Request_Id_To_String_On_Quote_Header.down.sql");

            Delete
                .Column("ExternalReference")
                .FromTable(Tables.QuoteHeader);
        }
    }
}