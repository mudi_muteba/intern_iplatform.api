using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150922084601)]
    public class Add_Image_Name_To_Product_Table : Migration
    {
        public override void Up()
        {
            if(!Schema.Table("Product").Column("ImageName").Exists())
            {
                Alter.Table(Tables.Product)
                    .AddColumn("ImageName").AsString().Nullable();
            }
        }

        public override void Down()
        {
            Delete.Column("ImageName").FromTable(Tables.Product);
        }
    }
}