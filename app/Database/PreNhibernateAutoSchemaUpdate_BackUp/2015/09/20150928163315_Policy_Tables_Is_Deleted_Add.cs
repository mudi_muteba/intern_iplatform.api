using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150928163315)]
    public class Policy_Tables_Is_Deleted_Add : Migration
    {
        public override void Up()
        {
            Alter.Table("PolicyAccident").AddColumn("IsDeleted").AsBoolean().Nullable();
            Update.Table("PolicyAccident").Set(new { IsDeleted = false }).AllRows();

            Alter.Table("PolicyAllRisk").AddColumn("IsDeleted").AsBoolean().Nullable();
            Update.Table("PolicyAllRisk").Set(new { IsDeleted = false }).AllRows();

            Alter.Table("PolicyBuilding").AddColumn("IsDeleted").AsBoolean().Nullable();
            Update.Table("PolicyBuilding").Set(new { IsDeleted = false }).AllRows();

            Alter.Table("PolicyContent").AddColumn("IsDeleted").AsBoolean().Nullable();
            Update.Table("PolicyContent").Set(new { IsDeleted = false }).AllRows();

            Alter.Table("PolicyCoverage").AddColumn("IsDeleted").AsBoolean().Nullable();
            Update.Table("PolicyCoverage").Set(new { IsDeleted = false }).AllRows();

            Alter.Table("PolicyFinance").AddColumn("IsDeleted").AsBoolean().Nullable();
            Update.Table("PolicyFinance").Set(new { IsDeleted = false }).AllRows();

            Alter.Table("PolicyHeader").AddColumn("IsDeleted").AsBoolean().Nullable();
            Update.Table("PolicyHeader").Set(new { IsDeleted = false }).AllRows();

            Alter.Table("PolicyItem").AddColumn("IsDeleted").AsBoolean().Nullable();
            Update.Table("PolicyItem").Set(new { IsDeleted = false }).AllRows();

            Alter.Table("PolicyLeadActivity").AddColumn("IsDeleted").AsBoolean().Nullable();
            Update.Table("PolicyLeadActivity").Set(new { IsDeleted = false }).AllRows();

            Alter.Table("PolicyLiability").AddColumn("IsDeleted").AsBoolean().Nullable();
            Update.Table("PolicyLiability").Set(new { IsDeleted = false }).AllRows();

            Alter.Table("PolicyPersonalVehicle").AddColumn("IsDeleted").AsBoolean().Nullable();
            Update.Table("PolicyPersonalVehicle").Set(new { IsDeleted = false }).AllRows();

            Alter.Table("PolicyWatercraft").AddColumn("IsDeleted").AsBoolean().Nullable();
            Update.Table("PolicyWatercraft").Set(new { IsDeleted = false }).AllRows();
        }

        public override void Down()
        {
            Delete.Column("IsDeleted").FromTable("PolicyAccident");
            Delete.Column("IsDeleted").FromTable("PolicyAllRisk");
            Delete.Column("IsDeleted").FromTable("PolicyBuilding");
            Delete.Column("IsDeleted").FromTable("PolicyContent");
            Delete.Column("IsDeleted").FromTable("PolicyCoverage");
            Delete.Column("IsDeleted").FromTable("PolicyFinance");
            Delete.Column("IsDeleted").FromTable("PolicyHeader");
            Delete.Column("IsDeleted").FromTable("PolicyItem");
            Delete.Column("IsDeleted").FromTable("PolicyLeadActivity");
            Delete.Column("IsDeleted").FromTable("PolicyLiability");
            Delete.Column("IsDeleted").FromTable("PolicyPersonalVehicle");
            Delete.Column("IsDeleted").FromTable("PolicyWatercraft");
        }
    }
}