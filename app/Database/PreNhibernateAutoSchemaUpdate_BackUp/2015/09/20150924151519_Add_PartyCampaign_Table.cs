using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150924151519)]
    public class Add_PartyCampaign_Table : Migration
    {
        public override void Up()
        {
            Create
                .Table(Tables.PartyCampaign)
                .InSchema(Schemas.Dbo)
                .WithColumn("Id")
                .AsInt32()
                .NotNullable()
                .PrimaryKey()
                .Identity()
                .WithColumn("PartyId").AsInt32()
                .WithColumn("CampaignId").AsInt32()
                .WithColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);
                ;


            Create.ForeignKey("Campaign_PartyCampaign")
                .FromTable(Tables.PartyCampaign).ForeignColumn("CampaignId")
                .ToTable(Tables.Campaign).PrimaryColumn("Id");

            Create.ForeignKey("Party_PartyCampaign")
                .FromTable(Tables.PartyCampaign).ForeignColumn("PartyId")
                .ToTable(Tables.Party).PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.
                ForeignKey("Campaign_PartyCampaign")
                .OnTable(Tables.PartyCampaign);

            Delete.
                ForeignKey("Party_PartyCampaign")
                .OnTable(Tables.PartyCampaign);

            Delete
                .Table(Tables.PartyCampaign)
                .InSchema(Schemas.Dbo)
                ;
        }
    }
}