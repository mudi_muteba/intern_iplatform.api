﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Schema;
using FluentMigrator.Runner.Extensions;

namespace Database.Migrations._2015._09
{
    [Migration(20150916161701)]
    public class OrganizationClaimTypeExclusion : Migration
    {
        public override void Up()
        {

            Execute.EmbeddedScript("20150916161701_OrganizationClaimTypeExclusion.sql");

            //Insert.IntoTable("ClaimsType")
            //    .InSchema(Schemas.Master)
            //    .WithIdentityInsert()
            //    .Row(new { Id = 12, Name = "Motor Accident (No 3rd Party)" })
            //    .Row(new { Id = 13, Name = "Motor Accident (Yes 3rd Party)" })
            //    .Row(new { Id = 14, Name = "Motor Hijack" })
            //    .Row(new { Id = 15, Name = "Glass (Windscreen)" })
            //    .Row(new { Id = 16, Name = "Property (Specified)" })
            //    .Row(new { Id = 17, Name = "Property (Not Specified)" })
            //    .Row(new { Id = 18, Name = "Funeral (Accidental)" })
            //    .Row(new { Id = 19, Name = "Funeral (Non-accidental)" });

            Create.Table("OrganizationClaimTypeExclusion")
                .WithIdColumn()
                .WithColumn("OrganizationId").AsInt32().NotNullable()
                .WithColumn("ClaimsTypeId").AsInt32().NotNullable();

            Insert.IntoTable("OrganizationClaimTypeExclusion")
                .Row(new { OrganizationId = 29, ClaimsTypeId = 2 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 3 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 4 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 5 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 6 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 7 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 8 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 9 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 10 })
                .Row(new { OrganizationId = 29, ClaimsTypeId = 11 });
        }

        public override void Down()
        {
            Delete.Table("OrganizationClaimTypeExclusion");
        }
    }
}
