using FluentMigrator;

namespace Database.Migrations._2015._09
{
    [Migration(20150925131520)]
    public class Tag_Is_Deleted_Add : Migration
    {
        public override void Up()
        {
            //version has been changed so dropping old one from _Version_iBroker
            Execute.Sql(@"DELETE FROM [dbo].[_Version_iBroker] WHERE [Version] =  201509170845820");
            //Checking if column does not exist add IsDeleted in Tag Table
            Execute.Sql(@"IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'IsDeleted' AND Object_ID = Object_ID(N'Tag')) BEGIN ALTER TABLE dbo.Tag ADD IsDeleted BIT NOT NULL CONSTRAINT DF_Tag_IsDeleted DEFAULT (0) END;");
            

            //Alter
            //    .Table("Tag")
            //    .AddColumn("IsDeleted").AsBoolean().Nullable();

            //Update.Table("Tag")
            //    .Set(new { IsDeleted = false })
            //    .AllRows();
        }

        public override void Down()
        {
            Delete.Column("IsDeleted").FromTable("Tag");
        }
    }
}