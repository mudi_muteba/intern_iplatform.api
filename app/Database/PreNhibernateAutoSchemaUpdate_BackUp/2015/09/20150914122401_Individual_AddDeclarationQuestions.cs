﻿using FluentMigrator;


namespace Database.Migrations._2015._09
{
    [Migration(20150914122401)]
    public class Individual_AddDeclarationQuestions : Migration
    {
        public override void Up()
        {
            Create.Column("AnyJudgements").OnTable("Individual").AsBoolean().Nullable();
            Create.Column("AnyJudgementsReason").OnTable("Individual").AsString(500).Nullable();
            Create.Column("UnderAdministrationOrDebtReview").OnTable("Individual").AsBoolean().Nullable();
            Create.Column("UnderAdministrationOrDebtReviewReason").OnTable("Individual").AsString(500).Nullable();
            Create.Column("BeenSequestratedOrLiquidated").OnTable("Individual").AsBoolean().Nullable();
            Create.Column("BeenSequestratedOrLiquidatedReason").OnTable("Individual").AsString(500).Nullable();
        }

        public override void Down()
        {
            Delete.Column("AnyJudgements").FromTable("Individual");
            Delete.Column("AnyJudgementsReason").FromTable("Individual");
            Delete.Column("UnderAdministrationOrDebtReview").FromTable("Individual");
            Delete.Column("UnderAdministrationOrDebtReviewReason").FromTable("Individual");
            Delete.Column("BeenSequestratedOrLiquidated").FromTable("Individual");
            Delete.Column("BeenSequestratedOrLiquidatedReason").FromTable("Individual");
        }
    }
}