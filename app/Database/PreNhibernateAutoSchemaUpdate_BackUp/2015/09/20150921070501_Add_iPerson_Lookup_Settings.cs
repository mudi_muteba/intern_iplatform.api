using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150921070501)]
    public class Add_iPerson_Lookup_Settings : Migration
    {
        public override void Up()
        {
            Create
                .Table(Tables.PersonLookupSetting)
                .WithIdColumn()
                .WithColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false)
                .WithColumn("ChannelId").AsInt32().NotNullable()
                .WithColumn("CountryId").AsInt32().NotNullable()
                .WithColumn("ApiKey").AsString(512).NotNullable()
                .WithColumn("Email").AsString(512).NotNullable()
                .WithColumn("InvoiceReference").AsString(512).Nullable()
                .WithColumn("PCubedEnabled").AsBoolean().NotNullable().WithDefaultValue(true)
                .WithColumn("PCubedUsername").AsString(512).Nullable()
                .WithColumn("PCubedPassword").AsString(512).Nullable()
                ;
        }

        public override void Down()
        {
            Delete.Table(Tables.PersonLookupSetting);
        }
    }
}