using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150930164730)]
    public class Change_TaskName_To_Int : Migration
    {
        public override void Up()
        {
            Alter.Table(Tables.ChannelEventTask)
                .AlterColumn("TaskName").AsInt32();
        }

        public override void Down()
        {
            Alter.Table(Tables.ChannelEventTask)
                .AlterColumn("TaskName").AsString();
        }
    }
}