using Database.Schema;
using FluentMigrator;

namespace Database.Migrations
{
    [Migration(20150921130301)]
    public class Add_External_Reference_To_Proposal_Header : Migration
    {
        public override void Up()
        {
            Alter.Table(Tables.ProposalHeader)
                .AddColumn("ExternalReference").AsString(512).Nullable();
        }

        public override void Down()
        {
            Delete
                .Column("ExternalReference")
                .FromTable(Tables.ProposalHeader);
        }
    }
}