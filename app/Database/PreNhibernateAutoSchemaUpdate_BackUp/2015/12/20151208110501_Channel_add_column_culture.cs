using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._12
{
    [Migration(20151208110501)]
    public class Channel_add_column_culture : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.Channel).Column("CurrencyId").Exists())
                Alter.Table(Tables.Channel).InSchema(Schemas.Dbo)
                    .AddColumn("CurrencyId").AsInt32().Nullable()
                    .AddColumn("DateFormat").AsString().Nullable()
                    .AddColumn("Language").AsString().Nullable();


            Update.Table(Tables.Channel)
                .Set(new { CurrencyId = 1, DateFormat = "dd MMM yyyy", Language = "en-US" })
                .AllRows();

        }

        public override void Down()
        {

        }
    }
}