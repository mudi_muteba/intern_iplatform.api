using FluentMigrator;

namespace Database.Migrations._2015._12
{
    [Migration(20151201155101)]
    public class LeadQuality_Is_Deleted_Add : Migration
    {
        public override void Up()
        {
            Alter
                .Table("LeadQuality")
                .AddColumn("IsDeleted").AsBoolean().Nullable();

            Update.Table("LeadQuality")
                .Set(new { IsDeleted = false })
                .AllRows();
        }

        public override void Down()
        {
            Delete.Column("IsDeleted").FromTable("LeadQuality");
        }
    }
}