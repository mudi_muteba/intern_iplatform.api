using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._12
{
    [Migration(20151203110501)]
    public class Currency_Table_Add : Migration
    {
        public override void Up()
        {
            Create
               .Table(Tables.Currency)
               .InSchema(Schemas.Master)
               .WithColumn("Id")
               .AsInt32()
               .NotNullable()
               .PrimaryKey()
               .WithColumn("Name").AsString(255).NotNullable()
               .WithColumn("Description").AsString(512).NotNullable()
               .WithColumn("Code").AsString(3).NotNullable()
               .WithColumn("NumericCode").AsInt32().NotNullable()
               .WithColumn("Front").AsBoolean().NotNullable().WithDefaultValue(true);
            ;
            Insert.IntoTable(Tables.Currency).InSchema(Schemas.Master)
                .Row(new { Id = 1, Name = "South African Rand", Description = "South Africa Rand", Code = "ZAR", NumericCode = "710", Front = "true" })
                .Row(new { Id = 2, Name = "Kenyan Shilling", Description = "Kenyan Shilling", Code = "KES", NumericCode = "404", Front = "true" })
                .Row(new { Id = 3, Name = "Mauritius Rupee", Description = "Mauritius Rupee", Code = "MUR", NumericCode = "480", Front = "true" })
                .Row(new { Id = 4, Name = "US Dollar", Description = "US Dollar", Code = "USD", NumericCode = "840", Front = "true" })
                .Row(new { Id = 5, Name = "Euro", Description = "Euro", Code = "EUR", NumericCode = "978", Front = "true" })
                .Row(new { Id = 6, Name = "Pound Sterling", Description = "UK Pound Sterling", Code = "GBP", NumericCode = "826", Front = "true" })
                ;
        }

        public override void Down()
        {
            Delete.Table(Tables.Currency).InSchema(Schemas.Master);
        }
    }
}