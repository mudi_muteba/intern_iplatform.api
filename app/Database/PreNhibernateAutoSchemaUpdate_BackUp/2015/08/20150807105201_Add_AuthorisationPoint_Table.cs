using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150807105201)]
    public class Add_AuthorisationPoint_Table : Migration
    {
        public override void Up()
        {
            Create
                .Table(Tables.AuthorisationPoint)
                .WithColumn("Id")
                .AsInt32()
                .NotNullable()
                .PrimaryKey()
                .WithColumn("Name").AsString(512)
                .WithColumn("AuthorisationPointCategoryId").AsInt32().NotNullable()
                ;

            Create.ForeignKey("AuthorisationPointCategory_authorisationPoint")
                .FromTable(Tables.AuthorisationPoint).ForeignColumn("AuthorisationPointCategoryId")
                .ToTable(Tables.AuthorisationPointCategory).PrimaryColumn("Id")
                ;

            Insert.IntoTable(Tables.AuthorisationPoint)
                // campaigns
                .Row(new {Id = 1, Name = "Create", AuthorisationPointCategoryId = 1 })
                .Row(new {Id = 2, Name = "Edit", AuthorisationPointCategoryId = 1 })
                .Row(new {Id = 3, Name = "Delete", AuthorisationPointCategoryId = 1 })
                .Row(new {Id = 4, Name = "Disable", AuthorisationPointCategoryId = 1 })
                .Row(new {Id = 5, Name = "MakeDefault", AuthorisationPointCategoryId = 1 })
                .Row(new {Id = 6, Name = "List", AuthorisationPointCategoryId = 1 })

                // users
                .Row(new {Id = 7, Name = "Create", AuthorisationPointCategoryId = 2 })
                .Row(new {Id = 8, Name = "Edit", AuthorisationPointCategoryId = 2 })
                .Row(new {Id = 9, Name = "Delete", AuthorisationPointCategoryId = 2 })
                .Row(new {Id = 10, Name = "Disable", AuthorisationPointCategoryId = 2 })
                .Row(new {Id = 11, Name = "List", AuthorisationPointCategoryId = 2 })
                ;
        }

        public override void Down()
        {
            Delete.ForeignKey("AuthorisationPointCategory_authorisationPoint")
                .OnTable(Tables.AuthorisationPoint);

            Delete.Table(Tables.AuthorisationPoint);
        }
    }
}