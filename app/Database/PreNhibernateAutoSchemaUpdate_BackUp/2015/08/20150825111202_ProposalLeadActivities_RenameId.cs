﻿using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150825111202)]
    public class _ProposalLeadActivities_RenameId : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'LeadActivityId' AND Object_ID = Object_ID(N'ProposalLeadActivity')) EXEC sp_RENAME 'ProposalLeadActivity.Id' , 'LeadActivityId', 'COLUMN'");
        }

        public override void Down()
        {
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'ProposalLeadActivity')) EXEC sp_RENAME 'ProposalLeadActivity.LeadActivityId' , 'Id', 'COLUMN'");
        }
    }
}