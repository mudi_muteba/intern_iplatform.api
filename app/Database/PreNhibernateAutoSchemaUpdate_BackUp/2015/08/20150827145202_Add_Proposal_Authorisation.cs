using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150827145202)]
    public class Add_Proposal_Authorisation : Migration
    {
        public override void Up()
        {
            Insert.IntoTable(Tables.AuthorisationPoint)
                // individual
                .Row(new { Id = 25, Name = "Create", AuthorisationPointCategoryId = 5 })
                .Row(new { Id = 26, Name = "Edit", AuthorisationPointCategoryId = 5 })
                .Row(new { Id = 27, Name = "Delete", AuthorisationPointCategoryId = 5 })
                .Row(new { Id = 28, Name = "List", AuthorisationPointCategoryId = 5 })
                ;

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 25, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 26, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 27, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 28, AuthorisationGroupId = 5 })

                // call centre agent
                .Row(new { AuthorisationPointId = 25, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 26, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 27, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 28, AuthorisationGroupId = 1 })

                // call centre manager
                .Row(new { AuthorisationPointId = 25, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 26, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 27, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 28, AuthorisationGroupId = 2 })
                ;

        }

        public override void Down()
        {
            Delete.FromTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new { AuthorisationPointId = 25, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 26, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 27, AuthorisationGroupId = 5 })
                .Row(new { AuthorisationPointId = 28, AuthorisationGroupId = 5 })

                // call centre agent
                .Row(new { AuthorisationPointId = 25, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 26, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 27, AuthorisationGroupId = 1 })
                .Row(new { AuthorisationPointId = 28, AuthorisationGroupId = 1 })

                // call centre manager
                .Row(new { AuthorisationPointId = 25, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 26, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 27, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 28, AuthorisationGroupId = 2 })
                ;

            Delete.FromTable(Tables.AuthorisationPoint)
                .Row(new { Id = 25, Name = "Create", AuthorisationPointCategoryId = 5 })
                .Row(new { Id = 26, Name = "Edit", AuthorisationPointCategoryId = 5 })
                .Row(new { Id = 27, Name = "Delete", AuthorisationPointCategoryId = 5 })
                .Row(new { Id = 28, Name = "List", AuthorisationPointCategoryId = 5 });

        }
    }
}