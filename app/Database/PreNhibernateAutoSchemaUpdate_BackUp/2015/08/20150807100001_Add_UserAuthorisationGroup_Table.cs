using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150807100001)]
    public class Add_UserAuthorisationGroup_Table : Migration
    {
        public override void Up()
        {
            Create
                .Table(Tables.UserAuthorisationGroup)
                .WithIdColumn()
                .WithColumn("AuthorisationGroupId").AsInt32().NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("ChannelId").AsInt32().NotNullable()
                ;

            Create.ForeignKey("User_UserAuthorisationGroup")
                .FromTable(Tables.UserAuthorisationGroup).ForeignColumn("UserId")
                .ToTable(Tables.User).PrimaryColumn("Id");

            Create.ForeignKey("Channel_UserAuthorisationGroup")
                .FromTable(Tables.UserAuthorisationGroup).ForeignColumn("ChannelId")
                .ToTable(Tables.Channel).PrimaryColumn("Id");

//            Execute.EmbeddedScript("20150807100001_Add_UserAuthorisationGroup_Table.up.sql");
        }

        public override void Down()
        {
            Delete.ForeignKey("User_UserAuthorisationGroup")
                .OnTable(Tables.UserAuthorisationGroup);

            Delete.ForeignKey("Channel_UserAuthorisationGroup")
                .OnTable(Tables.UserAuthorisationGroup);

//            Execute.EmbeddedScript("20150807100001_Add_UserAuthorisationGroup_Table.down.sql");

            Delete.Table(Tables.UserAuthorisationGroup);
        }
    }
}