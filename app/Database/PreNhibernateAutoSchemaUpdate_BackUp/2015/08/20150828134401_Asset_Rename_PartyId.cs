﻿using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150828134401)]
    public class Asset_Rename_AssetId : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'AssetId' AND Object_ID = Object_ID(N'AssetVehicle')) EXEC sp_RENAME 'AssetVehicle.Id' , 'AssetId', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'AssetId' AND Object_ID = Object_ID(N'AssetRiskItem')) EXEC sp_RENAME 'AssetRiskItem.Id' , 'AssetId', 'COLUMN'");
        }

        public override void Down()
        {
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'AssetVehicle')) EXEC sp_RENAME 'AssetVehicle.AssetId' , 'Id', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'AssetRiskItem')) EXEC sp_RENAME 'AssetRiskItem.AssetId' , 'Id', 'COLUMN'");
        }
    }
}