using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150806145401)]
    public class Flatten_Migration : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150806145401_Flatten.sql");
        }

        public override void Down()
        {
            
        }
    }
}