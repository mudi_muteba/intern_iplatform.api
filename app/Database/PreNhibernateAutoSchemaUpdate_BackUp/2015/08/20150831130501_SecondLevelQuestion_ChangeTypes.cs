﻿using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150831130501)]
    public class SecondLevelQuestion_ChangeTypes : Migration
    {
        public override void Up()
        {
            
            Execute.EmbeddedScript("20150831130501_SecondLevelQuestion_ChangeTypes.sql");
        }

        public override void Down()
        {
            

        }
    }
}