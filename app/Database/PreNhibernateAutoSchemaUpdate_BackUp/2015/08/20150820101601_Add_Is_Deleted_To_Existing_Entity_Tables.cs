using System.Collections.Generic;
using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150820101601)]
    public class Add_Is_Deleted_To_Existing_Entity_Tables_6 : Migration
    {
        private readonly List<string> tables = new List<string>()
        {
            Tables.PostalCode
        };

        public override void Up()
        {
            foreach (var table in tables)
            {
                Alter
                    .Table(table)
                    .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

                Update.Table(table)
                    .Set(new {IsDeleted = false})
                    .AllRows();
            }
        }

        public override void Down()
        {
            foreach (var table in tables)
            {
                Delete
                    .Column("IsDeleted")
                    .FromTable(table)
                    ;
            }
        }
    }
}