using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150818081001)]
    public class Remove_Token_Field_From_User_Table : Migration
    {
        public override void Up()
        {
            Delete.Column("Token")
                .FromTable(Tables.User);
        }

        public override void Down()
        {
            Alter.Table(Tables.User)
                .AddColumn("Token").AsString(150).Nullable();
        }
    }
}