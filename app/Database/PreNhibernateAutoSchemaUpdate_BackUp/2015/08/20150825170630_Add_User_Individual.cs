using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150825170630)]
    public class Add_User_Individual : Migration
    {
        public override void Up()
        {
            Create.Table(Tables.UserIndividual)
                .WithIdColumn() // NB to avoid null identifier exception - SCOPE_IDENTITY() returns null, nhibernate could not know the value of the inserted record
                .WithColumn("UserId").AsInt32().NotNullable() 
                .WithColumn("IndividualId").AsInt32().NotNullable()
                .WithColumn("CreatedAt").AsDateTime().NotNullable()
                .WithColumn("ModifiedAt").AsDateTime().NotNullable()
                .WithColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

            Create.ForeignKey("FK_UserIndividual_User") 
                .FromTable(Tables.UserIndividual).ForeignColumn("UserId")
                .ToTable(Tables.User).PrimaryColumn("Id");

            Create.ForeignKey("FK_UserIndividual_Individual")
                .FromTable(Tables.UserIndividual).ForeignColumn("IndividualId")
                .ToTable(Tables.Individual).PrimaryColumn("PartyId");

        }

        public override void Down()
        {
            Delete.ForeignKey("FK_UserIndividual_User").OnTable(Tables.UserIndividual);
            Delete.ForeignKey("FK_UserIndividual_Individual").OnTable(Tables.UserIndividual);
            Delete.Table(Tables.UserIndividual);
        }
    }
}