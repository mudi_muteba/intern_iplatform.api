using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150817135601)]
    public class Add_Is_Deleted_To_Product_Tables : Migration
    {
        public override void Up()
        {
            Alter.Table(Tables.Product)
                .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

            Update.Table(Tables.Product)
                .Set(new {IsDeleted = false})
                .AllRows();
        }

        public override void Down()
        {
            Delete.Column("IsDeleted")
                .FromTable(Tables.Product);
        }
    }
}