using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150821101501)]
    public class Add_Product_Code_To_Channel_Event : Migration
    {
        public override void Up()
        {
            Alter.Table(Tables.ChannelEvent)
                .AddColumn("ProductCode").AsString(255).Nullable();
        }

        public override void Down()
        {
            Delete.Column("ProductCode")
                .FromTable(Tables.ChannelEvent);
        }
    }
}