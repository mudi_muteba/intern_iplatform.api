﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150831104901)]
    public class SecondLevelQuestionDefinitions_AdjustToolTips : Migration
    {
        public override void Up()
        {
            Alter.Table(Tables.SecondLevelQuestionDefinition).AlterColumn("ToolTip").AsString(int.MaxValue);

            Execute.EmbeddedScript("20150831104901_SecondLevelQuestionDefinitions_AdjustToolTips.sql");
        }

        public override void Down()
        {
            

        }
    }
}