using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150807105901)]
    public class Add_AuthorisationGroupPoint_Table : Migration
    {
        public override void Up()
        {
            Create
                .Table(Tables.AuthorisationGroupPoint)
                .WithIdColumn()
                .WithColumn("AuthorisationPointId").AsInt32().NotNullable()
                .WithColumn("AuthorisationGroupId").AsInt32().NotNullable()
                ;

            Create.ForeignKey("AuthorisationPoint_AuthorisationGroupPoint")
                .FromTable(Tables.AuthorisationGroupPoint).ForeignColumn("AuthorisationPointId")
                .ToTable(Tables.AuthorisationPoint).PrimaryColumn("Id");

//            Execute.EmbeddedScript("20150807105901_Add_AuthorisationGroupPoint_Table.up.sql");

            Insert.IntoTable(Tables.AuthorisationGroupPoint)
                // admin
                .Row(new {AuthorisationPointId = 1, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 2, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 3, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 4, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 5, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 6, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 7, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 8, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 9, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 10, AuthorisationGroupId = 5})
                .Row(new {AuthorisationPointId = 11, AuthorisationGroupId = 5})

                // call centre agent
                .Row(new {AuthorisationPointId = 6, AuthorisationGroupId = 1})

                // call centre manager
                .Row(new { AuthorisationPointId = 1, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 2, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 3, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 4, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 5, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 6, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 7, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 8, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 9, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 10, AuthorisationGroupId = 2 })
                .Row(new { AuthorisationPointId = 11, AuthorisationGroupId = 2 })
                ;
        }

        public override void Down()
        {
            Delete.
                ForeignKey("AuthorisationPoint_AuthorisationGroupPoint")
                .OnTable(Tables.AuthorisationGroupPoint);

//            Execute.EmbeddedScript("20150807105901_Add_AuthorisationGroupPoint_Table.down.sql");

            Delete.Table(Tables.AuthorisationGroupPoint);
        }
    }
}