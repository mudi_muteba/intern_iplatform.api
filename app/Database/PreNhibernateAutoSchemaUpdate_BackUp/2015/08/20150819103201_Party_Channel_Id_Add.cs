using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150819103201)]
    public class Party_Channel_Id_Add : Migration
    {
        public override void Up()
        {
            Alter
                .Table("Party")
                .AddColumn("ChannelId").AsInt32().Nullable();

            Update.Table(Tables.Party).Set(new { ChannelId = 1 }).AllRows();
        }

        public override void Down()
        {
            Delete.Column("ChannelId").FromTable("Party");
        }
    }
}