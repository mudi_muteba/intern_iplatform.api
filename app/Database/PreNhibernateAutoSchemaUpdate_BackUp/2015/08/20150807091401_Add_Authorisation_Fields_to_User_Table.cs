using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150807091401)]
    public class Add_Authorisation_Fields_to_User_Table : Migration
    {
        public override void Up()
        {
            Alter
                .Table(Tables.User)
                .AddColumn("LoggedInFromSystem").AsString(512).Nullable();
        }

        public override void Down()
        {
            Delete.Column("LoggedInFromSystem").FromTable(Tables.User);
        }
    }
}