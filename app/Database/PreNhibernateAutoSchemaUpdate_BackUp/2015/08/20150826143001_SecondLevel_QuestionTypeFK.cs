﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150826143001)]
    public class SecondLevel_QuestionTypeFK : Migration
    {
        public override void Up()
        {
            Create.ForeignKey(Constraints.FK_SecondLevelQuestion_QuestionType)
                .FromTable(Tables.SecondLevelQuestion).InSchema(Schemas.Master).ForeignColumn("QuestionTypeId")
                .ToTable(Tables.QuestionType).InSchema(Schemas.Master).PrimaryColumn("Id");

        }

        public override void Down()
        {

            Delete.ForeignKey(Constraints.FK_SecondLevelQuestion_QuestionType).OnTable(Tables.SecondLevelQuestion).InSchema(Schemas.Master);

        }
    }
}