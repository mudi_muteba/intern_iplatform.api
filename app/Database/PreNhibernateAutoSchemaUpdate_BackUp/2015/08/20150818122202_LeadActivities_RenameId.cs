﻿using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150818122202)]
    public class LeadActivities_RenameId : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'LeadActivityId' AND Object_ID = Object_ID(N'ClaimsLeadActivity')) EXEC sp_RENAME 'ClaimsLeadActivity.Id' , 'LeadActivityId', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'LeadActivityId' AND Object_ID = Object_ID(N'CreateLeadActivity')) EXEC sp_RENAME 'CreateLeadActivity.Id' , 'LeadActivityId', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'LeadActivityId' AND Object_ID = Object_ID(N'DeadLeadActivity')) EXEC sp_RENAME 'DeadLeadActivity.Id' , 'LeadActivityId', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'LeadActivityId' AND Object_ID = Object_ID(N'DelayLeadActivity')) EXEC sp_RENAME 'DelayLeadActivity.Id' , 'LeadActivityId', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'LeadActivityId' AND Object_ID = Object_ID(N'ImportedLeadActivity')) EXEC sp_RENAME 'ImportedLeadActivity.Id' , 'LeadActivityId', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'LeadActivityId' AND Object_ID = Object_ID(N'LossLeadActivity')) EXEC sp_RENAME 'LossLeadActivity.Id' , 'LeadActivityId', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'LeadActivityId' AND Object_ID = Object_ID(N'PolicyLeadActivity')) EXEC sp_RENAME 'PolicyLeadActivity.Id' , 'LeadActivityId', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'LeadActivityId' AND Object_ID = Object_ID(N'QuotedLeadActivity')) EXEC sp_RENAME 'QuotedLeadActivity.Id' , 'LeadActivityId', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'LeadActivityId' AND Object_ID = Object_ID(N'QuoteAcceptedLeadActivity')) EXEC sp_RENAME 'QuoteAcceptedLeadActivity.Id' , 'LeadActivityId', 'COLUMN'");
        }

        public override void Down()
        {
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'ClaimsLeadActivity')) EXEC sp_RENAME 'ClaimsLeadActivity.LeadActivityId' , 'Id', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'CreateLeadActivity')) EXEC sp_RENAME 'CreateLeadActivity.LeadActivityId' , 'Id', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'DeadLeadActivity')) EXEC sp_RENAME 'DeadLeadActivity.LeadActivityId' , 'Id', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'DelayLeadActivity')) EXEC sp_RENAME 'DelayLeadActivity.LeadActivityId' , 'Id', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'ImportedLeadActivity')) EXEC sp_RENAME 'ImportedLeadActivity.LeadActivityId' , 'Id', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'LossLeadActivity')) EXEC sp_RENAME 'LossLeadActivity.LeadActivityId' , 'Id', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'PolicyLeadActivity')) EXEC sp_RENAME 'PolicyLeadActivity.LeadActivityId' , 'Id', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'QuotedLeadActivity')) EXEC sp_RENAME 'QuotedLeadActivity.LeadActivityId' , 'Id', 'COLUMN'");
            Execute.Sql(@"IF Not EXISTS(SELECT * FROM sys.columns WHERE Name = N'Id' AND Object_ID = Object_ID(N'QuoteAcceptedLeadActivity')) EXEC sp_RENAME 'QuoteAcceptedLeadActivity.LeadActivityId' , 'Id', 'COLUMN'");
        }
    }
}