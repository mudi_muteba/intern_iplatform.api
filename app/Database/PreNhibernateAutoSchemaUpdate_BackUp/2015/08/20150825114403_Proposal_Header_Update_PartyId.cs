﻿using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150825114403)]
    public class Proposal_Header_Update_PartyId : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150825114403_Proposal_Header_Update_PartyId.sql");
        }

        public override void Down()
        {
        }
    }
}