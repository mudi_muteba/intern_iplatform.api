using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150807093001)]
    public class Add_AuthorisationGroup_Table : Migration
    {
        public override void Up()
        {
            Create
                .Table(Tables.AuthorisationGroup)
                .InSchema(Schemas.Master)
                .WithColumn("Id")
                .AsInt32()
                .NotNullable()
                .PrimaryKey()
                .WithColumn("Name").AsString(512).NotNullable()
                ;

            Insert.IntoTable(Tables.AuthorisationGroup).InSchema(Schemas.Master)
                .Row(new {Id = 1, Name = "Call Centre Agent"})
                .Row(new {Id = 2, Name = "Call Centre Manager"})
                .Row(new {Id = 3, Name = "Broker"})
                .Row(new {Id = 4, Name = "Client" })
                .Row(new {Id = 5, Name = "Admin" })
                ;
        }

        public override void Down()
        {
            Delete
                .Table(Tables.AuthorisationGroup)
                .InSchema(Schemas.Master)
                ;
        }
    }
}