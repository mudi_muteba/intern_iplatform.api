using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150825111201)]
    public class Proposal_Header_Party_Id_Add : Migration
    {
        public override void Up()
        {
            Alter
                .Table("ProposalHeader")
                .AddColumn("PartyId").AsInt32().Nullable();
        }

        public override void Down()
        {
            Delete.Column("PartyId").FromTable("ProposalHeader");
        }
    }
}