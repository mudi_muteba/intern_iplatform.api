using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150824114301)]
    public class Campaign_Channel_Id_Add : Migration
    {
        public override void Up()
        {
            Alter
                .Table("Campaign")
                .AddColumn("ChannelId").AsInt32().Nullable();
        }

        public override void Down()
        {
            Delete.Column("ChannelId").FromTable("Campaign");
        }
    }
}