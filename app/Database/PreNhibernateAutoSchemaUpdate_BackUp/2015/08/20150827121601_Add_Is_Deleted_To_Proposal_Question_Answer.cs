using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150827121601)]
    public class Add_Is_Deleted_To_Proposal_Question_Answer : Migration
    {

        private readonly string _table = Tables.ProposalQuestionAnswer;

        public override void Up()
        {
            Alter
                .Table(_table)
                .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

            Update.Table(_table)
                .Set(new { IsDeleted = false })
                .AllRows();
        }

        public override void Down()
        {
            Delete
                .Column("IsDeleted")
                .FromTable(_table)
                ;
        }
    }
}