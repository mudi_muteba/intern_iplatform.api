using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150818111901)]
    public class Lead_Nullable : Migration
    {


        public override void Up()
        {
            Alter
                .Table("LeadActivity")
                .AlterColumn("LeadId").AsInt32().Nullable(); ;

        }

        public override void Down()
        {
            //Alter
            //    .Table("LeadActivity")
            //    .AlterColumn("LeadId").AsInt32().NotNullable();
        }
    }
}