using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150813122430)]
    public class Channel_Event_Add : Migration
    {
        public override void Up()
        {
            Create
                .Table("ChannelEvent")
                .WithIdColumn()
                .WithColumn("EventName").AsString()
                .WithColumn("ChannelId").AsInt32();
        }

        public override void Down()
        {
            Delete.Table("ChannelEvent");
        }
    }
}