﻿using Database;
using Database.Schema;
using FluentMigrator;
using FluentMigrator.Runner.Extensions;

namespace Database.Migrations._2015._08
{
    [Migration(20150825125001)]
    public class SecondLevel : Migration
    {
        public override void Up()
        {
            //Create tables
            Create.Table(Tables.SecondLevelQuestionAnswer)
                .InSchema(Schemas.Master)
                .WithIdColumn()
                .WithColumn("Name").AsString(200).NotNullable().Unique()
                .WithColumn("Answer").AsString(100).NotNullable()
                .WithColumn("SecondLevelQuestionId").AsInt32().NotNullable()
                .WithColumn("VisibleIndex").AsInt32().NotNullable();

            Create.Table(Tables.SecondLevelQuestion)
                .InSchema(Schemas.Master)
                .WithIdColumn()
                    .ReferencedBy(Constraints.FK_SecondLevelQuestionAnswer_SecondLevelQuestion, Schemas.Master, Tables.SecondLevelQuestionAnswer, "SecondLevelQuestionId")
                .WithColumn("Name").AsString(1000).NotNullable().Unique()
                .WithColumn("QuestionTypeId").AsInt32().NotNullable()
                .WithColumn("SecondLevelQuestionGroupId").AsInt32().NotNullable();



            Create.Table(Tables.SecondLevelQuestionGroup)
                .InSchema(Schemas.Master)
                .WithIdColumn()
                    .ReferencedBy(Constraints.FK_SecondLevelQuestion_SecondLevelQuestionGroup, Schemas.Master, Tables.SecondLevelQuestion, "SecondLevelQuestionGroupId")
                .WithColumn("Name").AsString(200).NotNullable().Unique()
                .WithColumn("VisibleIndex").AsInt32().NotNullable();

            Create.Table(Tables.SecondLevelQuestionDefinition)
                .WithIdColumn()
                .WithColumn("MasterId").AsInt32()
                .WithColumn("DisplayName").AsString(1000)
                .WithColumn("CoverDefinitionId").AsInt32().Nullable()
                .WithColumn("ProductId").AsInt32()
                .WithColumn("SecondLevelQuestionId").AsInt32().NotNullable()
                .WithColumn("ParentId").AsInt32().Nullable()
                .WithColumn("QuestionDefinitionTypeId").AsInt32().NotNullable()
                .WithColumn("VisibleIndex").AsInt32().NotNullable()
                .WithColumn("Required").AsBoolean().NotNullable()
                .WithColumn("RatingFactor").AsBoolean().NotNullable()
                .WithColumn("ReadOnly").AsBoolean().NotNullable()
                .WithColumn("ToolTip").AsString().Nullable()
                .WithColumn("DefaultValue").AsString().Nullable()
                .WithColumn("RegexPattern").AsString().Nullable();


            Create.Table(Tables.SecondLevelQuestionSavedAnswer)
                .WithIdColumn()
                .WithColumn("QuoteId").AsInt32().NotNullable()
                .WithColumn("QuoteItemId").AsInt32().Nullable()
                .WithColumn("Answer").AsString(255)
                .WithColumn("SecondLevelQuestionId").AsInt32().NotNullable()
                .WithColumn("VisibleIndex").AsInt32().NotNullable();


            // Create Foreign Keys
            Create.ForeignKey(Constraints.FK_SecondLevelQuestionDefinition_SecondLevelQuestion)
                .FromTable(Tables.SecondLevelQuestionDefinition).ForeignColumn("SecondLevelQuestionId")
                .ToTable(Tables.SecondLevelQuestion)
                .InSchema(Schemas.Master)
                .PrimaryColumn("Id");


            Create.ForeignKey(Constraints.FK_SecondLevelQuestionDefinition_SecondLevelQuestionDefinition)
                .FromTable(Tables.SecondLevelQuestionDefinition).ForeignColumn("ParentId")
                .ToTable(Tables.SecondLevelQuestionDefinition).PrimaryColumn("Id");


            Create.ForeignKey(Constraints.FK_SecondLevelQuestionDefinition_CoverDefinition)
                .FromTable(Tables.SecondLevelQuestionDefinition).ForeignColumn("CoverDefinitionId")
                .ToTable(Tables.CoverDefinition).PrimaryColumn("Id");


            Create.ForeignKey(Constraints.FK_SecondLevelQuestionDefinition_QuestionDefinitionType)
                .FromTable(Tables.SecondLevelQuestionDefinition).ForeignColumn("QuestionDefinitionTypeId")
                .ToTable(Tables.QuestionDefinitionType)
                .InSchema(Schemas.Master)
                .PrimaryColumn("Id");

            //Insert data
            Insert.IntoTable(Tables.SecondLevelQuestionGroup)
                .InSchema(Schemas.Master)
                .WithIdentityInsert()
                .Row(new { Id = 1, Name = "Administration and Judgements", VisibleIndex = 0 });


            Insert.IntoTable(Tables.SecondLevelQuestion)
                .InSchema(Schemas.Master)
                .WithIdentityInsert()
                .Row(new { Id = 1, Name = "Have you or anyone you intend on covering under this policy ever had cover cancelled by an insurer, been refused renewal of insurance, been advised to get alternative insurance or special terms and/or conditions imposed by an insurer on your policy?", QuestionTypeId = 1, SecondLevelQuestionGroupId = 1 })
                .Row(new { Id = 2, Name = "Have you or anyone you intend on covering under this policy ever been placed under debt review or administration?", QuestionTypeId = 1, SecondLevelQuestionGroupId = 1 })
                .Row(new { Id = 3, Name = "Does you or anyone you intend on covering under this policy have defaults or judgements passed against you/them or been found guilty of any criminal offence?", QuestionTypeId = 1, SecondLevelQuestionGroupId = 1 })
                .Row(new { Id = 4, Name = "Have you or any company you were involved with or anyone you intend on covering under this policy been onsolvent, sequestrated or liquidated?", QuestionTypeId = 1, SecondLevelQuestionGroupId = 1 })
                .Row(new { Id = 5, Name = "Will anyone use the car for business purposes such as selling, servicing, maintenance work, deliveries, visiting clients or basically anything where you need your car to make a living? Or would it be used for private use only? (If yes, inform the client that only that client and spouse will be covered for business.)", QuestionTypeId = 3, SecondLevelQuestionGroupId = 1 });


            Insert.IntoTable(Tables.SecondLevelQuestionAnswer)
                .InSchema(Schemas.Master)
                .WithIdentityInsert()
                .Row(
                    new
                    {
                        Id = 1,
                        Name = "Business",
                        Answer = "Business",
                        SecondLevelQuestionId = 5,
                        VisibleIndex = 0
                    })
                .Row(
                    new
                    {
                        Id = 2,
                        Name = "Private",
                        Answer = "Private",
                        SecondLevelQuestionId = 5,
                        VisibleIndex = 0
                    });


            Execute.EmbeddedScript("20150825125001_SecondLevelQuestionDefinitions_Add.sql");

            /*
            

            // Add Questions & Answers
            Execute.EmbeddedScript("20121116091057_Questions_Checkbox.sql");
            Execute.EmbeddedScript("20121116091057_Questions_Date.sql");
            Execute.EmbeddedScript("20121116091057_Questions_Dropdown.sql");
            Execute.EmbeddedScript("20121116091057_Questions_Textbox.sql");
            Execute.EmbeddedScript("20140211093001_Questions_Dropdown.sql");
            Execute.EmbeddedScript("20140211112001_Questions_Checkbox.sql");*/
        }

        public override void Down()
        {

            Delete.ForeignKey(Constraints.FK_SecondLevelQuestionAnswer_SecondLevelQuestion).OnTable(Tables.SecondLevelQuestionAnswer).InSchema(Schemas.Master);
            Delete.ForeignKey(Constraints.FK_SecondLevelQuestion_SecondLevelQuestionGroup).OnTable(Tables.SecondLevelQuestion).InSchema(Schemas.Master);
            Delete.ForeignKey(Constraints.FK_SecondLevelQuestionDefinition_SecondLevelQuestion).OnTable(Tables.SecondLevelQuestionDefinition);
            Delete.ForeignKey(Constraints.FK_SecondLevelQuestionDefinition_CoverDefinition).OnTable(Tables.SecondLevelQuestionDefinition);
            Delete.ForeignKey(Constraints.FK_SecondLevelQuestionDefinition_QuestionDefinitionType).OnTable(Tables.SecondLevelQuestionDefinition);

            Delete.Table(Tables.SecondLevelQuestionAnswer).InSchema(Schemas.Master);
            Delete.Table(Tables.SecondLevelQuestion).InSchema(Schemas.Master);
            Delete.Table(Tables.SecondLevelQuestionGroup).InSchema(Schemas.Master);
            Delete.Table(Tables.SecondLevelQuestionDefinition);
            Delete.Table(Tables.SecondLevelQuestionSavedAnswer);


            /*Delete.ForeignKey(Constraints.FK_QuestionAnswer_Question).OnTable(Tables.QuestionAnswer).InSchema(Schemas.Master);
            Delete.ForeignKey(Constraints.FK_Question_QuestionGroup).OnTable(Tables.Question).InSchema(Schemas.Master);
            Delete.ForeignKey(Constraints.FK_Question_QuestionType).OnTable(Tables.Question).InSchema(Schemas.Master);

            Delete.Table(Tables.QuestionAnswer).InSchema(Schemas.Master);
            Delete.Table(Tables.QuestionGroup).InSchema(Schemas.Master);
            Delete.Table(Tables.QuestionType).InSchema(Schemas.Master);
            Delete.Table(Tables.Question).InSchema(Schemas.Master);
             * */
        }
    }
}