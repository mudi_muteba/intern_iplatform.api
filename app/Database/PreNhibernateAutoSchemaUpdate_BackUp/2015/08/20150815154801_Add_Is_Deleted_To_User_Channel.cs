using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150815154801)]
    public class Add_Is_Deleted_To_User_Channel : Migration
    {
        public override void Up()
        {
            Alter
                .Table(Tables.UserChannel)
                .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

            Update.Table(Tables.UserChannel)
                .Set(new { IsDeleted = false })
                .AllRows();
        }

        public override void Down()
        {
            Delete
                .Column("IsDeleted")
                .FromTable(Tables.UserChannel)
                ;
        }
    }
}