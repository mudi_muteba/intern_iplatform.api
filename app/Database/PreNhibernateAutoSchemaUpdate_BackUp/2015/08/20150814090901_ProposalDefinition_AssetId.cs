﻿using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150814090901)]
    public class ProposalDefinition_AssetId : Migration
    {
        public override void Up()
        {
            Alter
                .Table("ProposalDefinition")
                .AddColumn("AssetId").AsInt32().Nullable();
        }

        public override void Down()
        {
            Delete.Column("AssetId").FromTable("ProposalDefinition");
        }
    }
}