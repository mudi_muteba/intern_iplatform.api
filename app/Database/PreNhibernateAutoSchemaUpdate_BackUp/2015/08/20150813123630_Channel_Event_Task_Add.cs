using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150813123630)]
    public class Channel_Event_Task_Add : Migration
    {
        public override void Up()
        {
            Create
                .Table("ChannelEventTask")
                .WithIdColumn()
                .WithColumn("TaskName").AsString()
                .WithColumn("ChannelEventId").AsInt32();
        }

        public override void Down()
        {
            Delete.Table("ChannelEventTask");
        }
    }
}