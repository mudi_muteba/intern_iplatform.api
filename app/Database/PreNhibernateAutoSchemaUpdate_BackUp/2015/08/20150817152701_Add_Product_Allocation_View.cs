using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150817152701)]
    // 20150817152701_Add_Product_Allocation_View
    public class Add_Product_Allocation_View : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150817152701_Add_Product_Allocation_View.up.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20150817152701_Add_Product_Allocation_View.down.sql");
        }
    }
}