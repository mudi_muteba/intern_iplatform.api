using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._2015._08
{
    [Migration(20150821063001)]
    public class Add_Country_To_Postal_Codes : Migration
    {
        public override void Up()
        {
            if (!Schema.Table(Tables.PostalCode).Column("CountryId").Exists())
                Alter.Table(Tables.PostalCode)
                    .AddColumn("CountryId").AsInt32().Nullable();

            Update.Table(Tables.PostalCode)
                .Set(new { CountryId = 197 })
                .AllRows();
        }

        public override void Down()
        {
           /// Delete.Column("CountryId")
           //     .FromTable(Tables.PostalCode);
        }
    }
}