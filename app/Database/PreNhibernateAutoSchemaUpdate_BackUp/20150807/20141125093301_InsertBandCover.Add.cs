﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141125093301)]
    public class InsertBandCover : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141125093301_InsertBandCover.Add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}