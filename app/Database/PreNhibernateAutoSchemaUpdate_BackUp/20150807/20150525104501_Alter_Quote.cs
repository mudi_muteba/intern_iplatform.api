﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150525104501)]
    public class AlterQuote : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150525104501_Alter_Quote.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}