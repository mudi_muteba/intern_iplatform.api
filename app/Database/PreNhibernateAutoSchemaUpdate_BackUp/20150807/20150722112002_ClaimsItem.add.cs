using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150722112002)]
    public class Claims_Item : Migration
    {
        public override void Up()
        {
            Create
                .Table("ClaimsItem")
                .WithIdColumn()
                .WithTimeStamps()
                .WithColumn("ClaimsHeaderId").AsInt32().NotNullable().Indexed()
                .WithColumn("ClaimsTypeId").AsInt32()
                .WithColumn("PolicyItemId").AsInt32().NotNullable();
        }

        public override void Down()
        {
            Delete.Table("ClaimsItem");
        }
    }
}