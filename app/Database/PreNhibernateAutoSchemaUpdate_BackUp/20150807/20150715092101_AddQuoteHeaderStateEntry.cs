using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150715092101)]
    public class AddQuoteHeaderStateEntry : Migration
    {
        public override void Up()
        {
            Create
                .Table("QuoteHeaderStateEntry")
                .WithIdColumn()
                .WithColumn("QuoteHeaderStateId").AsInt32().NotNullable().Indexed()
                .WithColumn("Message").AsString(int.MaxValue).NotNullable()
                .WithColumn("IsError").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Table("QuoteHeaderStateEntry");
        }
    }
}