﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150512140615)]
    public class AddIsUserdToCampaignTable : Migration
    {
        public override void Up()
        {
            Alter
                .Table("Campaign")
                .AddColumn("IsUsed").AsBoolean().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete
                .Column("IsUsed")
                .FromTable("Campaign");
        }
    }
}