﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150624131101)]
    // 20150624131101_Add_Work_Province_Question
    public class Add_Work_Province_Question : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150624131101_Add_Work_Province_Question.sql");
        }

        public override void Down()
        {
        }
    }
}