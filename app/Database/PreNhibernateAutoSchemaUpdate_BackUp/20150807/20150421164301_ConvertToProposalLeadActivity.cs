﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150421164301)]
    public class ConvertToProposalLeadActivity : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150421164301_ConvertToProposalLeadActivity.sql");
        }

        public override void Down()
        {
        }
    }
}