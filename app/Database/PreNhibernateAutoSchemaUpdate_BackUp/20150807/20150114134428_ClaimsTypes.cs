﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150114134428)]
    public class Claims_Types : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150114134428_ClaimsTypes.add.sql");
        }

        public override void Down()
        {
        }
    }
}