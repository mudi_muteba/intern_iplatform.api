﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150119083645)]
    public class ClaimsMasterDataPart3 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150119083645_ClaimsMasterDataPart3.add.sql");
        }

        public override void Down()
        {
        }
    }
}