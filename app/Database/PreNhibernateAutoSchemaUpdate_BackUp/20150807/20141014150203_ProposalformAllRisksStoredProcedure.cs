﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141014150203)]
    public class ProposalformAllRisksSP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141014150203_ProposalformAllRisksStoredProcedure.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20141014150203_ProposalformAllRisksStoredProcedure.remove.sql");
        }
    }
}
