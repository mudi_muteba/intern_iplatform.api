﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150721145801)]
    public class RemoveLeadActivityFromQuoteHeader : Migration
    {
        public override void Up()
        {
            Delete
                .Index("IX_QuoteHeader_LeadActivityId")
                .OnTable("QuoteHeader");

            Delete
                .Column("LeadActivityId")
                .FromTable("QuoteHeader");
        }

        public override void Down()
        {
        }
    }
}