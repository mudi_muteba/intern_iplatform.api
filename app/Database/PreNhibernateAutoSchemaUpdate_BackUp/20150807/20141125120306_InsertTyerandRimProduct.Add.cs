﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141125120306)]
    public class InsertTyerandRimProduct : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141125120306_InsertTyerandRimProduct.Add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}