﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141015094100)]
    public class DateRangeFunction : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141015094100_DateRangeTableFunction.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20141015094100_DateRangeTableFunction.remove.sql");
        }
    }
}