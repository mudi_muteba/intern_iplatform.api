﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121116121918)]
    public class QuestionAnswerExclusions : Migration
    {
        public override void Up()
        {
            // Tables
            Create.Table(Tables.QuestionAnswerExclusion)
                .WithIdColumn()
                .WithColumn("MasterId").AsInt32()
                .WithColumn("ProductId").AsInt32().NotNullable()
                .WithColumn("CoverDefinitionId").AsInt32().NotNullable()
                .WithColumn("QuestionDefinitionId").AsInt32().NotNullable()
                .WithColumn("QuestionAnswerId").AsInt32().NotNullable();

            // Foreign Keys
            Create.ForeignKey(Constraints.FK_QuestionAnswerExclusion_Product)
                .FromTable(Tables.QuestionAnswerExclusion).ForeignColumn("ProductId")
                .ToTable(Tables.Product).PrimaryColumn("Id");  
            
            Create.ForeignKey(Constraints.FK_QuestionAnswerExclusion_CoverDefinition)
                .FromTable(Tables.QuestionAnswerExclusion).ForeignColumn("CoverDefinitionId")
                .ToTable(Tables.CoverDefinition).PrimaryColumn("Id");

            Create.ForeignKey(Constraints.FK_QuestionAnswerExclusion_QuestionDefinition)
                .FromTable(Tables.QuestionAnswerExclusion).ForeignColumn("QuestionDefinitionId")
                .ToTable(Tables.QuestionDefinition).PrimaryColumn("Id");

            Create.ForeignKey(Constraints.FK_QuestionAnswerExclusion_QuestionAnswer)
              .FromTable(Tables.QuestionAnswerExclusion).ForeignColumn("QuestionAnswerId")
              .ToTable(Tables.QuestionAnswer)
              .InSchema(Schemas.Master)
              .PrimaryColumn("Id");
           
        }

        public override void Down()
        {
            Delete.ForeignKey(Constraints.FK_QuestionAnswerExclusion_Product).OnTable(Tables.QuestionAnswerExclusion);
            Delete.ForeignKey(Constraints.FK_QuestionAnswerExclusion_CoverDefinition).OnTable(Tables.QuestionAnswerExclusion);
            Delete.ForeignKey(Constraints.FK_QuestionAnswerExclusion_QuestionDefinition).OnTable(Tables.QuestionAnswerExclusion);
            Delete.ForeignKey(Constraints.FK_QuestionAnswerExclusion_QuestionAnswer).OnTable(Tables.QuestionAnswerExclusion);

            Delete.Table(Tables.QuestionAnswerExclusion);
        }
    }
}