﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150414104101)]
    public class AssetRecreateQuestionDefinition : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150414104101_Asset.Recreate.QuestionDefinition.add.sql");
        }

        public override void Down()
        {
        }
    }
}