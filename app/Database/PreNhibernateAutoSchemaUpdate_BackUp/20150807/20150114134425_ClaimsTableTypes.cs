﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150114134425)]
    public class ClaimsTableTypes : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150114134425_ClaimsTableTypes.add.sql");
        }

        public override void Down()
        {
        }
    }
}