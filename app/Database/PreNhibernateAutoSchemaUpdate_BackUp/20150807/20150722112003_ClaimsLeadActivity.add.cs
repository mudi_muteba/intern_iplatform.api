using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150722112003)]
    public class ClaimsLeadActivity : Migration
    {
        public override void Up()
        {
            Create
                .Table("ClaimsLeadActivity")
                .WithIdColumn()
                .WithColumn("ClaimsHeaderId").AsInt32().NotNullable().Indexed();
        }

        public override void Down()
        {
            Delete.Table("ClaimsLeadActivity");
        }
    }
}