﻿using System;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150610084401)]
    public class AddUserTable : Migration
    {
        public override void Up()
        {
            Create
                .Table("[User]")
                .WithIdColumn()
                .WithTimeStamps()
                .WithColumn("UserName").AsString(512).NotNullable()
                .WithColumn("Password").AsString(2048).NotNullable()
                .WithColumn("SystemId").AsGuid().WithDefaultValue(Guid.NewGuid()).NotNullable()
                .WithColumn("IsApproved").AsBoolean().WithDefaultValue(false).NotNullable()
                .WithColumn("IsLoggedIn").AsBoolean().WithDefaultValue(false).NotNullable()
                .WithColumn("LastLoggedInDate").AsDateTime().Nullable()
                .WithColumn("LoggedInFromIP").AsString(25).Nullable()
                .WithColumn("LoggedInFromSystemName").AsString(255).Nullable()
                .WithColumn("Token").AsString(int.MaxValue).Nullable();
        }

        public override void Down()
        {
            Delete.Table("[Users]");
        }
    }
}