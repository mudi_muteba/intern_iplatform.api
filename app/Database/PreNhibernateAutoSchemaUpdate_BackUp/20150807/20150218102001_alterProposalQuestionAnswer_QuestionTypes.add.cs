﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150218102001)]
    public class SlterProposalQuestionAnswerQuestionTypes : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150218102001_alterProposalQuestionAnswer_QuestionTypes.add.sql");
        }

        public override void Down()
        {
        }
    }
}