﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150505120002)]
    public class AlterAddress : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150505120002_AlterAddress.add.sql");
        }

        public override void Down()
        {
        }
    }
}