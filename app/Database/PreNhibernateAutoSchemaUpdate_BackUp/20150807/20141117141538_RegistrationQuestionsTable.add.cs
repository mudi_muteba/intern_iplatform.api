﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141117141538)]
    public class RegistrationQuestionsTable : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141117141538_RegistrationQuestionsTable.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}