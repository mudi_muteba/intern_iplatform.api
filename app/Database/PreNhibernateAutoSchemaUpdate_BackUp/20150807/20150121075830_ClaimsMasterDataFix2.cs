﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150121075830)]
    public class ClaimsMasterDataFix2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150121075830_ClaimsMasterDataFix2.add.sql");
        }

        public override void Down()
        {
        }
    }
}