﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150408113644)]
    public class SauFees : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150408113644_SAUFees.add.sql");
        }

        public override void Down()
        {
        }
    }
}