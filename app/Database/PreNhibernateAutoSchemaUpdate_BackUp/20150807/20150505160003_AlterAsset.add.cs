﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150505160003)]
    public class AlterAsset2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150505160003_AlterAsset.add.sql");
        }

        public override void Down()
        {
        }
    }
}