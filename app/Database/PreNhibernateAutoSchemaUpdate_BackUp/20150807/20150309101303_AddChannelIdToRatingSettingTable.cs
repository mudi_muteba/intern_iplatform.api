﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150309101303)]
    public class AddChannelIdAndEnvironmentToRatingSettingTable : Migration
    {
        public override void Up()
        {
            Delete
                .Column("UserName")
                .FromTable("SettingsiRate");

            Alter
                .Table("SettingsiRate")
                .AddColumn("ChannelId").AsGuid()
                .AddColumn("Environment").AsString(255)
                .AddColumn("UserId").AsString(255)
                ;
        }

        public override void Down()
        {
            Alter
                .Table("SettingsiRate")
                .AddColumn("UserName").AsString(50).NotNullable();

            Delete
                .Column("ChannelId")
                .Column("Environment")
                .FromTable("SettingsiRate");
        }
    }
}