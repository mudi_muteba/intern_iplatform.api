﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141023075230)]
    public class AddQuestionsTelesure : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141023075230_Add_QuestionsTelsure.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}