﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121112152442)]
    public class Products : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20121112152442_Products.add.sql");

            // ACORD: LineOfBusinessCodeContent_Type
            Insert.IntoTable(Tables.ProductType)
                .InSchema(Schemas.Master)
                .Row(new { Name = "Accident" })
                .Row(new { Name = "Commercial Package" })
                .Row(new { Name = "Commercial Property" })
                .Row(new { Name = "Commercial Vehicle" })
                .Row(new { Name = "Liability" })
                .Row(new { Name = "Package" })
                .Row(new { Name = "Personal Package" })
                .Row(new { Name = "Personal Property" })
                .Row(new { Name = "Personal Vehicle" })
                .Row(new { Name = "Specified Business Articles" })
                .Row(new { Name = "Specified Personal Articles" })
                .Row(new { Name = "Watercraft" });
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20121112152442_Products.remove.sql");
        }
    }
}
