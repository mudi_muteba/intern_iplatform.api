﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150721134201)]
    public class Motor_AddQuestions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150721134201_Motor_AddQuestions.add.sql");
        }

        public override void Down()
        {
        }
    }
}