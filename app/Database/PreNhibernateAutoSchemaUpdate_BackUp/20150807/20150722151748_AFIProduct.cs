using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150722151748)]
    public class AFIProduct : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150722151748_AFIProduct.add.sql");
        }

        public override void Down()
        {
        }
    }
}