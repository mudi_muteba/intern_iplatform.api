using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150715091701)]
    public class AddQuoteStateEntry : Migration
    {
        public override void Up()
        {
            Create
                .Table("QuoteStateEntry")
                .WithIdColumn()
                .WithColumn("QuoteStateId").AsInt32().NotNullable().Indexed()
                .WithColumn("Message").AsString(int.MaxValue).NotNullable()
                .WithColumn("IsError").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Table("QuoteStateEntry");
        }
    }
}