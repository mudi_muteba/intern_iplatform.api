using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150617144530)]
    public class RenameChannelChannelIdColumn : Migration
    {
        public override void Up()
        {
            Rename.Column("ChannelId").OnTable("Channel").To("SystemId");
        }

        public override void Down()
        {
            Rename.Column("SystemId").OnTable("Channel").To("ChannelId");
        }
    }
}