﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141127131509)]
    public class TouchUpQuestionDefinition : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141127131509_TouchUpQuestionDefinition.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}