﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121115131537)]
    public class Covers : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20121115131537_Covers.add.sql");
        }

        public override void Down()
        {
            Delete.Table(Tables.Cover).InSchema(Schemas.Master);
        }
    }
}