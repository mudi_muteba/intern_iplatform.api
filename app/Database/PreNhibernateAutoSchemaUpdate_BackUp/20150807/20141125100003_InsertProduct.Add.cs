﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141125100003)]
    public class InsertProduct : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141125100003_InsertProduct.Add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}