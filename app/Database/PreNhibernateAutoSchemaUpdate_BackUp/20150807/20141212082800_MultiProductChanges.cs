﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141212082800)]
    public class MultiProductChanges : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141212082800_MultiProductChanges.add.sql");
        }

        public override void Down()
        {
        }
    }
}