﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141203094612)]
    public class VapQuestionDefinition : Migration
    {
        public override void Up()
        {
           Execute.EmbeddedScript("20141203094612_VapQuestionDefinition.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}