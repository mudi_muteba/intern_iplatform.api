using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150803134501)]
    public class LeadActivity_User : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150803134501_LeadActivity_User.add.sql");
        }

        public override void Down()
        {
            
        }
    }
}