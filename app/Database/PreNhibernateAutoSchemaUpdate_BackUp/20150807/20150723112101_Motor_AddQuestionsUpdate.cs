﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150723112101)]
    public class Motor_AddQuestionsUpdate : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150723112101_Motor_AddQuestionsUpdate.add.sql");
        }

        public override void Down()
        {
        }
    }
}