﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150618102604)]
    public class ConvertLeadActivities : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150618102604_Convert_LeadActivities.add.sql");
        }

        public override void Down()
        {
        }
    }
}