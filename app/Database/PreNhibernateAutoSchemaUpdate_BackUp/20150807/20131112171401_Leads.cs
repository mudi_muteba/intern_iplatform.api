﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20131112171401)]
    public class Leads : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20131112171401_Leads.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20131112171401_Leads.remove.sql");
        }
    }
}