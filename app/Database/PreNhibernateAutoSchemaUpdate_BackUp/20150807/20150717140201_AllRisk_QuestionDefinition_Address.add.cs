﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150717140201)]
    public class AllRisk_QuestionDefinition_Address : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150717140201_AllRisk_QuestionDefinition_Address.add.sql");
        }

        public override void Down()
        {
        }
    }
}