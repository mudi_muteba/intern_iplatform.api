﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150304103501)]
    public class Tag : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150304103501_Tag.add.sql");
        }

        public override void Down()
        {
        }
    }
}