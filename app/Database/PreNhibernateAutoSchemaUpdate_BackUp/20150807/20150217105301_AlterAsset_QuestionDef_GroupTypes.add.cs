﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150217105301)]
    public class AlterAssetQuestionDefGroupTypes : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150217105301_AlterAsset_QuestionDef_GroupTypes.add.sql");
        }

        public override void Down()
        {
        }
    }
}