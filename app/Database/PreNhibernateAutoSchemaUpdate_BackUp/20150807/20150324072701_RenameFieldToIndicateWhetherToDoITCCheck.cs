﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150324072701)]
    public class RenameFieldToIndicateWhetherToDoITCCheck : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150324072701_RenameFieldToIndicateWhetherToDoITCCheck.sql");              
        }

        public override void Down()
        {
        }
    }
}