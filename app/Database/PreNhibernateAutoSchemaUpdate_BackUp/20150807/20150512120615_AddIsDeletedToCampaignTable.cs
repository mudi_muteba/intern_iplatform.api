﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150512120615)]
    public class AddIsDeletedToCampaignTable : Migration
    {
        public override void Up()
        {
            Alter
                .Table("Campaign")
                .AddColumn("IsDeleted").AsBoolean().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete
                .Column("IsDeleted")
                .FromTable("Campaign");
        }
    }
}