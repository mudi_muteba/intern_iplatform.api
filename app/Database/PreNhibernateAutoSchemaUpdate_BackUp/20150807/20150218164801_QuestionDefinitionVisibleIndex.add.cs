﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150218164801)]
    public class QuestionDefinitionVisibleIndex : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150218164801_QuestionDefinitionVisibleIndex.add.sql");
        }

        public override void Down()
        {
        }
    }
}