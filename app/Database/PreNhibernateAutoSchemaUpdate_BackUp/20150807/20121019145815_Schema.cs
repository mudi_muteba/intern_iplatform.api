﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121019145815)]
    public class Schema : Migration
    {
        public override void Up()
        {
            Create.Schema(Schemas.Master);
        }

        public override void Down()
        {
            Delete.Schema(Schemas.Master);
        }
    }
}