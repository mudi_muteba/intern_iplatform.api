﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150116130218)]
    public class ClaimsMasterDataPart2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150116130218_ClaimsMasterDataPart2.add.sql");
        }

        public override void Down()
        {
        }
    }
}