﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141014082001)]
    public class AddKingPrice : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141014082001_AddKingPrice.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}