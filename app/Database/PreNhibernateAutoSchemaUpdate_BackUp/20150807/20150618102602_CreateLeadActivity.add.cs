﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150618102602)]
    public class CreateLeadActivity : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150618102602_CreateLeadActivity.add.sql");
        }

        public override void Down()
        {
        }
    }
}