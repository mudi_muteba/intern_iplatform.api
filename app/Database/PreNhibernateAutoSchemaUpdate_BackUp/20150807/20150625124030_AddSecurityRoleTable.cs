﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150625124030)]
    public class AddSecurityRoleTable : Migration
    {
        public override void Up()
        {
            Create
                .Table("[SecurityRole]")
                .WithIdColumn()
                .WithColumn("RoleName").AsString().NotNullable()
                .WithColumn("ApplicationName").AsString();
        }

        public override void Down()
        {
            Delete.Table("[SecurityRole]");
        }
    }
}