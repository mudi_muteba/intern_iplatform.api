﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150528082201)]
    public class ManufactureYearDropdownRemove : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150528082201_Manufacture_Year_Dropdown_Remove.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}