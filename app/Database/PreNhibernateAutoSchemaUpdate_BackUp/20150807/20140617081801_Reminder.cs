﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140617081801)]
    public class Reminder : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140617081801_Reminder.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20140617081801_Reminder.remove.sql");
        }
    }
}