using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150714143801)]
    public class AddQuoteState : Migration
    {
        public override void Up()
        {
            Create
                .Table("QuoteState")
                .WithIdColumn()
                .WithColumn("QuoteId").AsInt32().NotNullable().Indexed()
                .WithColumn("Errors").AsBoolean().NotNullable().WithDefaultValue(false)
                .WithColumn("Warnings").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Table("QuoteState");
        }
    }
}