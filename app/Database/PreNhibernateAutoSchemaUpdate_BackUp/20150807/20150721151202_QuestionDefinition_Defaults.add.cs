﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150721151202)]
    public class QuestionDefinition_Defaults : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150721151202_QuestionDefinition_Defaults.add.sql");
        }

        public override void Down()
        {
        }
    }
}