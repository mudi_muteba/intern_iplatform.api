﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150121083233)]
    public class ClaimsProducts : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150121083233_ClaimsProducts.add.sql");
        }

        public override void Down()
        {
        }
    }
}