﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150625141030)]
    public class AddUserSecurityRoleTable : Migration
    {
        public override void Up()
        {
            Create
                .Table(Tables.UserSecurityRole)
                .WithIdColumn()
                .WithTimeStamps()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("SecurityRoleId").AsInt32().NotNullable();

            Create.ForeignKey(Constraints.FK_UserSecurityRole_User)
                .FromTable(Tables.UserSecurityRole)
                .ForeignColumn("UserId")
                .ToTable(Tables.User)
                .InSchema(Schemas.Dbo)
                .PrimaryColumn("Id");

            Create.ForeignKey(Constraints.FK_UserSecurityRole_SecurityRole)
                .FromTable(Tables.UserSecurityRole)
                .ForeignColumn("SecurityRoleId")
                .ToTable(Tables.SecurityRole)
                .InSchema(Schemas.Dbo)
                .PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.Table(Tables.UserSecurityRole);
            Delete.ForeignKey(Constraints.FK_UserSecurityRole_User).OnTable(Tables.UserSecurityRole).InSchema(Schemas.Dbo);
            Delete.ForeignKey(Constraints.FK_UserSecurityRole_SecurityRole).OnTable(Tables.UserSecurityRole).InSchema(Schemas.Dbo);
        }
    }
}