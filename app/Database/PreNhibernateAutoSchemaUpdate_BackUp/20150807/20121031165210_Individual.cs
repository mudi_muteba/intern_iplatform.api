﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121031165210)]
    public class IndividualDetails : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20121031165210_Individual.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20121031165210_Individual.remove.sql");
        }
    }
}
