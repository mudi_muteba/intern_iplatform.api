﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141117131537)]
    public class BankDetailsTable : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141117131537_BankDetailsTable.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}