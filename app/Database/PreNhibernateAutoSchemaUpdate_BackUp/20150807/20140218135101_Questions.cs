﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140218135101)]
    public class QuestionsRenasaBuilding : Migration
    {

        public override void Up()
        {
            Execute.EmbeddedScript("20140218135101_Questions_Checkbox.sql");
        }

        public override void Down()
        {
        }
    }
}