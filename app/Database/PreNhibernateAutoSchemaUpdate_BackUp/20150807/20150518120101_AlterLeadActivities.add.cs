﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150518120101)]
    public class AlterLeadActivities : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150518120101_AlterLeadActivities.add.sql");
        }

        public override void Down()
        {
        }
    }
}