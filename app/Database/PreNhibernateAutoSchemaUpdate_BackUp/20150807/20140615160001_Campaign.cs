﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140615160001)]
    public class Campaign : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140615160001_AlterCampaign.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20140615160001_AlterCampaign.remove.sql");
        }
    }
}