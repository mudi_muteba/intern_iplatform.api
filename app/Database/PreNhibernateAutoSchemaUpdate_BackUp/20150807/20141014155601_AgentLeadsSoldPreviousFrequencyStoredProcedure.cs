﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141014155601)]
    public class AgentLeadsSoldPreviousFrequencySP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141014155601_AgentLeadsSoldPreviousFrequencyStoredProcedure.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20141014155601_AgentLeadsSoldPreviousFrequencyStoredProcedure.remove.sql");
        }
    }
}