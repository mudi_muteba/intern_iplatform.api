﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121031163402)]
    public class Parties : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20121031163402_Parties.add.sql");

            Insert.IntoTable(Tables.PartyType)
                .InSchema(Schemas.Master)
                .Row(new { Name = "Individual" })
                .Row(new { Name = "Organization" });

            Insert.IntoTable(Tables.RelationshipType)
                .InSchema(Schemas.Master)
                .Row(new { Name = "Department" })
                .Row(new { Name = "Dependant" });

            Insert.IntoTable(Tables.RoleType)
                .InSchema(Schemas.Master)
                .Row(new { Name = "Insurer" })
                .Row(new { Name = "Client Site" });
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20121031163402_Parties.remove.sql");
        }
    }
}