﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150710115202)]
    public class Policy_LeadActivity_Add : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150710115202_PolicyLeadActivity.add.sql");
        }

        public override void Down()
        {
        }
    }
}