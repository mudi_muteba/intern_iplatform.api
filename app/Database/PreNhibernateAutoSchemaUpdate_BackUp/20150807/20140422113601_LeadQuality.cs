﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140422113601)]
    public class LeadQuality : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140422113601_LeadQuality.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20140422113601_LeadQuality.remove.sql");
        }
    }
}