﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150324072301)]
    public class AddChannelInfoToITCSettings : Migration
    {
        public override void Up()
        {
            Alter
                .Table("SettingsiPerson")
                .AddColumn("ChannelId").AsGuid().NotNullable();
        }

        public override void Down()
        {
            Delete
                .Column("ChannelId")
                .FromTable("SettingsiPerson");
        }
    }
}