﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150716111401)]
    public class RiskAddress_dropdown : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150716111401_RiskAddress_dropdown.add.sql");
        }

        public override void Down()
        {
        }
    }
}