﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141127093008)]
    public class Alter_mdCover : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141127093008_Alter_mdCover.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}