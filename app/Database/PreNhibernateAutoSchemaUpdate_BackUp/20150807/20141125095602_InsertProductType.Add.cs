﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141125095602)]
    public class InsertProductType : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141125095602_InsertProductType.Add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}