﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150220113001)]
    public class QuestionDefinitionGroupType : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150220113001_QuestionDefinitionGroupType.add.sql");
        }

        public override void Down()
        {
        }
    }
}