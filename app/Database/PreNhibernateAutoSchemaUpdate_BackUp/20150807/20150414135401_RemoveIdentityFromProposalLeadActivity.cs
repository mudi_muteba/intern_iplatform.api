﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150414135401)]
    public class RemoveIdentityFromProposalLeadActivity : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150414135401_RemoveIdentityFromProposalLeadActivity.sql");
        }

        public override void Down()
        {
        }
    }
}