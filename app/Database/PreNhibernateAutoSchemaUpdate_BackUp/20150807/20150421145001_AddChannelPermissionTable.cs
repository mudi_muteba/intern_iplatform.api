﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150421145001)]
    public class AddChannelPermissionTable : Migration
    {
        public override void Up()
        {
            Create
                .Table("ChannelPermission")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity().NotNullable()
                .WithColumn("ChannelId").AsGuid().NotNullable()
                .WithColumn("ProductId").AsInt32().NotNullable()
                .WithColumn("AllowProposalCreation").AsBoolean().NotNullable().WithDefaultValue(false)
                .WithColumn("AllowQuoting").AsBoolean().NotNullable().WithDefaultValue(false)
                .WithColumn("AllowSecondLevelUnderwriting").AsBoolean().NotNullable().WithDefaultValue(false)
                ;
        }

        public override void Down()
        {
            Delete
                .Table("ChannelPermission");
        }
    }
}