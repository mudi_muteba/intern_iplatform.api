﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150612120601)]
    public class PartyContactPartyType : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150612120601_Party_Contact_PartyType.add.sql");
        }

        public override void Down()
        {
        }
    }
}