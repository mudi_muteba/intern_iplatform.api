﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141125114905)]
    public class InsertTouchUpProduct : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141125114905_InsertTouchUpProduct.Add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}