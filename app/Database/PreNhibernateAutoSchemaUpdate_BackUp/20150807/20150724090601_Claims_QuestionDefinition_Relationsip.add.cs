﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150724090601)]
    public class Claims_QuestionDefinition_Relationsip : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150724090601_Claims_QuestionDefinition_Relationsip.add.sql");
        }

        public override void Down()
        {
        }
    }
}