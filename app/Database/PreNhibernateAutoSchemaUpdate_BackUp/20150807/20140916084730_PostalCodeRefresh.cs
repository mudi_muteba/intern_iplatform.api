﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140916084730)]
    public class PostalCodeRefresh : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140916084730_PostalCodeRefresh.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}