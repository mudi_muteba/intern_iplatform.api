﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20130820103511)]
    public class FinanceHouses : Migration
    {
        public const string FinanceHouse = "FinanceHouse";

        public override void Up()
        {
            Execute.EmbeddedScript("20130820103511_FinanceHouses.add.sql");
        }

        public override void Down()
        {
            Delete.Table(FinanceHouse).InSchema(Schemas.Master);
        }
    }
}