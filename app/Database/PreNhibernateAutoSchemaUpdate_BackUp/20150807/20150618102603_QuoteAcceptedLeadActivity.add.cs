﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150618102603)]
    public class QuoteAcceptedLeadActivity : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150618102603_QuoteAcceptedLeadActivity.add.sql");
        }

        public override void Down()
        {
        }
    }
}