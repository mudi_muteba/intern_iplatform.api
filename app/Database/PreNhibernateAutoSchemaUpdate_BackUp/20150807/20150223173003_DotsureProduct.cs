﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150223173003)]
    public class DotsureProduct : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150223173003_DotsureProduct.add.sql");
        }

        public override void Down()
        {
        }
    }
}