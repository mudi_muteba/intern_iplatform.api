﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140225101701)]
    public class QuestionsAllRisks : Migration
    {

        public override void Up()
        {
            Execute.EmbeddedScript("20140225101701_Questions_Dropdown.sql");
            Execute.EmbeddedScript("20140225115201_Questions_Textbox.sql");
        }

        public override void Down()
        {
        }
    }
}