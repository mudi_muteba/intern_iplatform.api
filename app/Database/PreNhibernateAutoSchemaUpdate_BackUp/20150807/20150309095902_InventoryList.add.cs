﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150309095902)]
    public class InventoryList : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150309095902_InventoryList.add.sql");
        }

        public override void Down()
        {
        }
    }
}