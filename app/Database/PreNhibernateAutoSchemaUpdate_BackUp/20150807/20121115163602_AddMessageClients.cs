﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121115163602)]
    public class AddMessageClients : Migration
    {
        public override void Up()
        {
            Create.Table(Tables.MessageClient)
                  .WithIdColumn()
                  .WithColumn("MessageQueueId").AsInt32().NotNullable()
                  .WithColumn("ClientSiteId").AsInt32().NotNullable()
                  .WithColumn("MachineName").AsString().Nullable()
                  .WithColumn("SystemName").AsString().Nullable()
                  .WithColumn("Success").AsBoolean().NotNullable()
                  .WithColumn("FeedbackReceivedOn").AsDateTime().Nullable()
                ;

            Create.ForeignKey("FK_MessageQueue_Clients")
                  .FromTable(Tables.MessageClient)
                  .ForeignColumn("MessageQueueId")
                  .ToTable(Tables.MessageQueue)
                  .PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.ForeignKey("FK_MessageQueue_Clients").OnTable(Tables.MessageClient);
            Delete.Table(Tables.MessageClient);
        }
    }
}