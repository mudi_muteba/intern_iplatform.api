﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150123125010)]
    public class ProposalHeader_LeadActivity : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150123125010_ProposalHeader_LeadActivity.add.sql");
        }

        public override void Down()
        {
        }
    }
}