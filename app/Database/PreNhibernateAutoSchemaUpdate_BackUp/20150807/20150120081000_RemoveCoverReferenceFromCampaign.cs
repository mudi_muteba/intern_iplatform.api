﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150120081000)]
    public class RemoveCoverReferenceFromCampaign : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150120081000_RemoveCoverReferenceFromCampaign.add.sql");
        }

        public override void Down()
        {
        }
    }
}