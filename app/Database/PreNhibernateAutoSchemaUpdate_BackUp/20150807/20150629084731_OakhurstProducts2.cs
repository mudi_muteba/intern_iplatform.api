﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150629084731)]
    public class OakhurstProducts2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150629084731_OakhurstProducts2.add.sql");
        }

        public override void Down()
        {
        }
    }
}