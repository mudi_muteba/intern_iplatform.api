﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141023113730)]
    public class AddQuestionsTelesure2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141023113730_Add_QuestionsTelesure2.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}