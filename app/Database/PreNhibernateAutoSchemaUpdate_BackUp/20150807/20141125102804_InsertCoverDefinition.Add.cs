﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141125102804)]
    public class InsertCoverDefinition : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141125102804_InsertCoverDefinition.Add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}