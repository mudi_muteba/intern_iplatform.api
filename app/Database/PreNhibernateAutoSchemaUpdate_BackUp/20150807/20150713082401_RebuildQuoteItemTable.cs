using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150713082401)]
    public class RebuildQuoteItemTable : Migration
    {
        public override void Up()
        {
            Delete
                .Column("FailureMessage")
                .FromTable("QuoteItem");
        }

        public override void Down()
        {
        }
    }
}