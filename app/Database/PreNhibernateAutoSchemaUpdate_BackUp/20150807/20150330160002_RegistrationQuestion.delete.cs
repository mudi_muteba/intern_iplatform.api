﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150330160002)]
    public class removeRegistrationQuestion : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150330160002_RegistrationQuestion.delete.sql");
        }

        public override void Down()
        {
        }
    }
}