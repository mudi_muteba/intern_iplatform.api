﻿using Database.Schema;
using FluentMigrator;
using FluentMigrator.Runner.Extensions;

namespace Database.Migrations._20150807
{
    [Migration(20121116091057)]
    public class Questions : Migration
    {
        public override void Up()
        {
            Create.Table(Tables.QuestionAnswer)
                .InSchema(Schemas.Master)
                .WithIdColumn()
                .WithColumn("Name").AsString(200).NotNullable().Unique()
                .WithColumn("Answer").AsString(100).NotNullable()
                .WithColumn("QuestionId").AsInt32().NotNullable()
                .WithColumn("VisibleIndex").AsInt32().NotNullable();

            Create.Table(Tables.Question)
                .InSchema(Schemas.Master)
                .WithIdColumn()
                    .ReferencedBy(Constraints.FK_QuestionAnswer_Question, Schemas.Master, Tables.QuestionAnswer, "QuestionId")
                .WithColumn("Name").AsString(100).NotNullable().Unique()
                .WithColumn("QuestionTypeId").AsInt32().NotNullable()
                .WithColumn("QuestionGroupId").AsInt32().NotNullable();

            Create.Table(Tables.QuestionType)
                .InSchema(Schemas.Master)
                .WithIdColumn()
                    .ReferencedBy(Constraints.FK_Question_QuestionType, Schemas.Master, Tables.Question, "QuestionTypeId")
                .WithColumn("Name").AsString(20).NotNullable().Unique();

            Create.Table(Tables.QuestionGroup)
                .InSchema(Schemas.Master)
                .WithIdColumn()
                    .ReferencedBy(Constraints.FK_Question_QuestionGroup, Schemas.Master, Tables.Question, "QuestionGroupId")
                .WithColumn("Name").AsString(80).NotNullable().Unique()
                .WithColumn("VisibleIndex").AsInt32().NotNullable();

            // Master Data
            Insert.IntoTable(Tables.QuestionType)
                .InSchema(Schemas.Master)
                .WithIdentityInsert()
                .Row(new { Id = 1, Name = "Checkbox" })
                .Row(new { Id = 2, Name = "Date" })
                .Row(new { Id = 3, Name = "Dropdown" })
                .Row(new { Id = 4, Name = "Textbox" });

            Insert.IntoTable(Tables.QuestionGroup)
                .InSchema(Schemas.Master)
                .WithIdentityInsert()
                .Row(new { Id = 1, Name = "General Information", VisibleIndex = 0 })
                .Row(new { Id = 2, Name = "Risk Information", VisibleIndex = 1 })
                .Row(new { Id = 3, Name = "Security Information", VisibleIndex = 2 })
                .Row(new { Id = 4, Name = "Additional Options", VisibleIndex = 3 })
                .Row(new { Id = 5, Name = "Driver Information", VisibleIndex = 4 })
                .Row(new { Id = 6, Name = "Insurance History", VisibleIndex = 5 })
                .Row(new { Id = 7, Name = "Finance Information", VisibleIndex = 6 });

            // Add Questions & Answers
            Execute.EmbeddedScript("20121116091057_Questions_Checkbox.sql");
            Execute.EmbeddedScript("20121116091057_Questions_Date.sql");
            Execute.EmbeddedScript("20121116091057_Questions_Dropdown.sql");
            Execute.EmbeddedScript("20121116091057_Questions_Textbox.sql");
            Execute.EmbeddedScript("20140211093001_Questions_Dropdown.sql");
            Execute.EmbeddedScript("20140211112001_Questions_Checkbox.sql");
        }

        public override void Down()
        {
            Delete.ForeignKey(Constraints.FK_QuestionAnswer_Question).OnTable(Tables.QuestionAnswer).InSchema(Schemas.Master);
            Delete.ForeignKey(Constraints.FK_Question_QuestionGroup).OnTable(Tables.Question).InSchema(Schemas.Master);
            Delete.ForeignKey(Constraints.FK_Question_QuestionType).OnTable(Tables.Question).InSchema(Schemas.Master);

            Delete.Table(Tables.QuestionAnswer).InSchema(Schemas.Master);
            Delete.Table(Tables.QuestionGroup).InSchema(Schemas.Master);
            Delete.Table(Tables.QuestionType).InSchema(Schemas.Master);
            Delete.Table(Tables.Question).InSchema(Schemas.Master);
        }
    }
}