﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    // 20150506135702_AddIsDeletedColumnsToCampaignRelatedTables
    [Migration(20150506135702)]
    public class AddIsDeletedColumnsToCampaignRelatedTables : Migration
    {
        public override void Up()
        {
            Alter
                .Table("CampaignAgent")
                .AddColumn("IsDeleted").AsBoolean().WithDefaultValue(false);

            Alter
                .Table("TagCampaignMap")
                .AddColumn("IsDeleted").AsBoolean().WithDefaultValue(false);

            Alter
                .Table("CampaignProduct")
                .AddColumn("IsDeleted").AsBoolean().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Column("IsDeleted").FromTable("CampaignAgent");
            Delete.Column("IsDeleted").FromTable("TagCampaignMap");
            Delete.Column("IsDeleted").FromTable("CampaignProduct");
        }
    }
}