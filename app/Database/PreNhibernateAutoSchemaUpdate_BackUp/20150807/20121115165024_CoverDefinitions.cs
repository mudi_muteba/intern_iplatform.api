﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121115165024)]
    public class CoverDefinitions : Migration
    {
        public override void Up()
        {
            // Tables
            Create.Table(Tables.CoverDefinition)
                .WithIdColumn()
                .WithColumn("MasterId").AsInt32()
                .WithColumn("DisplayName").AsString()
                .WithColumn("ProductId").AsInt32()
                .WithColumn("CoverId").AsInt32().NotNullable()
                .WithColumn("ParentId").AsInt32().Nullable()
                .WithColumn("CoverDefinitionTypeId").AsInt32().NotNullable()
                .WithColumn("VisibleIndex").AsInt32().NotNullable();

            Create.Table(Tables.CoverDefinitionType)
                .InSchema(Schemas.Master)
                .WithIdColumn()
                .WithColumn("Name").AsString().NotNullable().Unique();

            // Foreign Keys
            Create.ForeignKey(Constraints.FK_CoverDefinition_Cover)
                .FromTable(Tables.CoverDefinition).ForeignColumn("CoverId")
                .ToTable(Tables.Cover)
                .InSchema(Schemas.Master)
                .PrimaryColumn("Id");

            Create.ForeignKey(Constraints.FK_CoverDefinition_CoverDefinition)
                .FromTable(Tables.CoverDefinition).ForeignColumn("ParentId")
                .ToTable(Tables.CoverDefinition).PrimaryColumn("Id");

            Create.ForeignKey(Constraints.FK_CoverDefinition_Product)
                .FromTable(Tables.CoverDefinition).ForeignColumn("ProductId")
                .ToTable(Tables.Product).PrimaryColumn("Id");

            Create.ForeignKey(Constraints.FK_CoverDefinition_CoverDefinitionType)
                .FromTable(Tables.CoverDefinition).ForeignColumn("CoverDefinitionTypeId")
                .ToTable(Tables.CoverDefinitionType)
                .InSchema(Schemas.Master)
                .PrimaryColumn("Id");

            // Master Data
            Insert.IntoTable(Tables.CoverDefinitionType)
                .InSchema(Schemas.Master)
                .Row(new { Name = "Main Cover" })
                .Row(new { Name = "Sub Cover" })
                .Row(new { Name = "Extension" })
                .Row(new { Name = "Add On" })
                .Row(new { Name = "Cover Per Item" })
                .Row(new { Name = "Cover Per Policy" });
        }

        public override void Down()
        {
            Delete.ForeignKey(Constraints.FK_CoverDefinition_Cover).OnTable(Tables.CoverDefinition);
            Delete.ForeignKey(Constraints.FK_CoverDefinition_CoverDefinition).OnTable(Tables.CoverDefinition);
            Delete.ForeignKey(Constraints.FK_CoverDefinition_Product).OnTable(Tables.CoverDefinition);
            Delete.ForeignKey(Constraints.FK_CoverDefinition_CoverDefinitionType).OnTable(Tables.CoverDefinition);

            Delete.Table(Tables.CoverDefinitionType).InSchema(Schemas.Master);
            Delete.Table(Tables.CoverDefinition);
        }
    }
}