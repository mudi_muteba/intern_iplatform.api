﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150713074401)]
    public class QuoteHeaderTable : Migration
    {
        public override void Up()
        {
            Create.Table("QuoteHeader")
                .WithIdColumn()
                .WithTimeStamps()
                .WithColumn("ProposalHeaderId").AsInt32().NotNullable().Indexed()
                .WithColumn("LeadActivityId").AsInt32().NotNullable().Indexed()
                .WithColumn("RequestId").AsGuid().NotNullable()
                .WithColumn("IsAccepted").AsBoolean().WithDefaultValue(false)
                .WithColumn("AcceptedOn").AsDateTime().Nullable()
                .WithColumn("AcceptedBy").AsInt32().NotNullable().WithDefaultValue(1)
                .WithColumn("IsDeleted").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Table("QuoteHeader");
        }
    }
}