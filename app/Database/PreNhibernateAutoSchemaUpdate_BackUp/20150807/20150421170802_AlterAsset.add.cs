﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150421170802)]
    public class AlterAsset : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150421170802_AlterAsset.add.sql");
        }

        public override void Down()
        {
        }
    }
}