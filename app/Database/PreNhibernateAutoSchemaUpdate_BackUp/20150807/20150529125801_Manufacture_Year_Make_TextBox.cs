﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150529125801)]
    public class ManufactureYearDropdownMakeTextBox : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150529125801_Manufacture_Year_Make_TextBox.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}