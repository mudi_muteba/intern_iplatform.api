﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150629103501)]
    public class Set_Multi_Product_Defaults : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150629103501_Set_Multi_Product_Defaults.sql");
        }

        public override void Down()
        {
        }
    }
}