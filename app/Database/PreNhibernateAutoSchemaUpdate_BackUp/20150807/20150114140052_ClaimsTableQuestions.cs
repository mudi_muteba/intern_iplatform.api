﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150114140052)]
    public class ClaimsTableTypeQuestions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150114140052_ClaimsTableQuestions.add.sql");
        }

        public override void Down()
        {
        }
    }
}