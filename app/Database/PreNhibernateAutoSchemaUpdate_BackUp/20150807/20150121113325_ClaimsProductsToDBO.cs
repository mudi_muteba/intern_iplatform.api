﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150121113325)]
    public class ClaimsProductsToDBO : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150121113325_ClaimsProductsToDBO.add.sql");
        }

        public override void Down()
        {
        }
    }
}