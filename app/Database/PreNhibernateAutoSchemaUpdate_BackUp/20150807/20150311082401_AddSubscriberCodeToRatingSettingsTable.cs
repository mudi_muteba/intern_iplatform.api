﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150311082401)]
    public class AddSubscriberCodeToRatingSettingsTable : Migration
    {
        public override void Up()
        {
            Alter
                .Table("SettingsiRate")
                .AddColumn("SubscriberCode").AsString(255).Nullable();
        }

        public override void Down()
        {
            Delete
                .Column("SubscriberCode")
                .FromTable("SettingsiRate");
        }
    }
}