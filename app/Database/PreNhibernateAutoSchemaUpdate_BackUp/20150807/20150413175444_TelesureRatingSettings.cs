﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150413175444)]
    public class TelesureRatingSettings : Migration
    {
        public override void Up()
        {
            Alter
                .Table("SettingsiRate")
                .AddColumn("CompanyCode").AsString(512).Nullable()
                .AddColumn("UwCompanyCode").AsString(512).Nullable()
                .AddColumn("UwProductCode").AsString(512).Nullable();

        }

        public override void Down()
        {
            Delete
                .Column("CompanyCode")
                .Column("UwCompanyCode")
                .Column("UwProductCode")
                .FromTable("SettingsiRate");
        }
    }
}