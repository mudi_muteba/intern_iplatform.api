﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141014150202)]
    public class ProposalformHouseOwnersSP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141014150202_ProposalformHouseOwnersStoredProcedure.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20141014150202_ProposalformHouseOwnersStoredProcedure.remove.sql");
        }
    }
}