using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150714143601)]
    public class AddQuoteHeaderState : Migration
    {
        public override void Up()
        {
            Create
                .Table("QuoteHeaderState")
                .WithIdColumn()
                .WithColumn("QuoteHeaderId").AsInt32().NotNullable().Indexed()
                .WithColumn("Errors").AsBoolean().NotNullable().WithDefaultValue(false)
                .WithColumn("Warnings").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Table("QuoteHeaderState");
        }
    }
}