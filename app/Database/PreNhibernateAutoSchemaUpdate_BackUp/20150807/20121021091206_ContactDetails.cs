﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121021091206)]
    public class ContactDetails : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20121021091206_ContactDetails.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20121021091206_ContactDetails.remove.sql");
        }
    }
}











