﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150323084501)]
    public class alterQuote : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150323084501_alterQuote.add.sql");
        }

        public override void Down()
        {
        }
    }
}