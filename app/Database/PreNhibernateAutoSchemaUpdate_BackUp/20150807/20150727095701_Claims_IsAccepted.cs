using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150727095701)]
    public class Claims_IsAccepted : Migration
    {
        public override void Up()
        {
            Alter
                .Table("ClaimsHeader")
                .AddColumn("IsAccepted").AsBoolean().Nullable().WithDefaultValue(false)
                .AddColumn("AcceptedOn").AsDateTime().Nullable()
                .AddColumn("AcceptedBy").AsInt32().Nullable().WithDefaultValue(1);
        }

        public override void Down()
        {
            
        }
    }
}