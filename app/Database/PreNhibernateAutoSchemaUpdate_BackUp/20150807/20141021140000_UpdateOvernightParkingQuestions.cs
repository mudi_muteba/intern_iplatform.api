﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141021140000)]
    public class UpdateOvernightParkingQuestions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141021140000_UpdateOvernightParkingQuestions.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}