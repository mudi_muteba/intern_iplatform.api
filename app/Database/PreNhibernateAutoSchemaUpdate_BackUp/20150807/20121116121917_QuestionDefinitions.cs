﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121116121917)]
    public class QuestionDefinitions : Migration
    {
        public override void Up()
        {
            // Tables
            Create.Table(Tables.QuestionDefinition)
                .WithIdColumn()
                .WithColumn("MasterId").AsInt32()
                .WithColumn("DisplayName").AsString()
                .WithColumn("CoverDefinitionId").AsInt32()
                .WithColumn("QuestionId").AsInt32().NotNullable()
                .WithColumn("ParentId").AsInt32().Nullable()
                .WithColumn("QuestionDefinitionTypeId").AsInt32().NotNullable()
                .WithColumn("VisibleIndex").AsInt32().NotNullable()
                .WithColumn("RequiredForQuote").AsBoolean().NotNullable()
                .WithColumn("RatingFactor").AsBoolean().NotNullable()
                .WithColumn("ReadOnly").AsBoolean().NotNullable()
                .WithColumn("ToolTip").AsString().Nullable()
                .WithColumn("DefaultValue").AsString().Nullable()
                .WithColumn("RegexPattern").AsString().Nullable();


            Create.Table(Tables.QuestionDefinitionType)
                .InSchema(Schemas.Master)
                .WithIdColumn()
                .WithColumn("Name").AsString().NotNullable();

            // Foreign Keys
            Create.ForeignKey(Constraints.FK_QuestionDefinition_Question)
                .FromTable(Tables.QuestionDefinition).ForeignColumn("QuestionId")
                .ToTable(Tables.Question)
                .InSchema(Schemas.Master)
                .PrimaryColumn("Id");

            Create.ForeignKey(Constraints.FK_QuestionDefinition_QuestionDefinition)
                .FromTable(Tables.QuestionDefinition).ForeignColumn("ParentId")
                .ToTable(Tables.QuestionDefinition).PrimaryColumn("Id");

            Create.ForeignKey(Constraints.FK_QuestionDefinition_CoverDefinition)
                .FromTable(Tables.QuestionDefinition).ForeignColumn("CoverDefinitionId")
                .ToTable(Tables.CoverDefinition).PrimaryColumn("Id");

            Create.ForeignKey(Constraints.FK_QuestionDefinition_QuestionDefinitionType)
                .FromTable(Tables.QuestionDefinition).ForeignColumn("QuestionDefinitionTypeId")
                .ToTable(Tables.QuestionDefinitionType)
                .InSchema(Schemas.Master)
                .PrimaryColumn("Id");

            // Master Data
            Insert.IntoTable(Tables.QuestionDefinitionType)
                .InSchema(Schemas.Master)
                .Row(new { Name = "Standard" })
                .Row(new { Name = "Sub Question" });
        }

        public override void Down()
        {
            Delete.ForeignKey(Constraints.FK_QuestionDefinition_Question).OnTable(Tables.QuestionDefinition);
            Delete.ForeignKey(Constraints.FK_QuestionDefinition_QuestionDefinition).OnTable(Tables.QuestionDefinition);
            Delete.ForeignKey(Constraints.FK_QuestionDefinition_CoverDefinition).OnTable(Tables.QuestionDefinition);
            Delete.ForeignKey(Constraints.FK_QuestionDefinition_QuestionDefinitionType).OnTable(Tables.QuestionDefinition);

            Delete.Table(Tables.QuestionDefinitionType).InSchema(Schemas.Master);
            Delete.Table(Tables.QuestionDefinition);
        }

    }
}
