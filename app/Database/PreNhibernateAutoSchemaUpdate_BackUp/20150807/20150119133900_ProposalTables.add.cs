﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150119133900)]
    public class ProposalTables : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150119133900_ProposalTables.add.sql");
        }

        public override void Down()
        {
        }
    }
}