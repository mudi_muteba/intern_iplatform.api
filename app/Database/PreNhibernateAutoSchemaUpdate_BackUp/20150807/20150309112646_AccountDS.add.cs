﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150309112646)]
    public class AccountDS : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150309112646_AccountDS.add.sql");
        }

        public override void Down()
        {
        }
    }
}