﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141110171712)]
    public class AddIdentityTables : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141110171712_Add_IdentityTables.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}