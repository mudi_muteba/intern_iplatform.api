﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150325113501)]
    public class AddEnvironmentToITCSettings : Migration
    {
        public override void Up()
        {
            Alter
                .Table("SettingsiPerson")
                .AddColumn("Environment").AsString(255).Nullable();
        }

        public override void Down()
        {
            Delete
                .Column("Environment")
                .FromTable("SettingsiPerson");
        }
    }
}