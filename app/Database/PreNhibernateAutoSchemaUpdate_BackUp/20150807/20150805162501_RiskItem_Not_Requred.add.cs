using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150805162501)]
    public class RiskItem_Not_Requred : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150805162501_RiskItem_Not_Requred.add.sql");
        }

        public override void Down()
        {
            
        }
    }
}