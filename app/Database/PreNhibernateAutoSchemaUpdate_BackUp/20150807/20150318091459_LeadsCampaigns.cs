﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150318091459)]
    public class LeadsCampaigns : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150318091459_LeadsCampaigns.add.sql");
        }

        public override void Down()
        {
        }
    }
}