﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150315135026)]
    public class Banks : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150315135026_Banks.add.sql");
        }

        public override void Down()
        {
        }
    }
}