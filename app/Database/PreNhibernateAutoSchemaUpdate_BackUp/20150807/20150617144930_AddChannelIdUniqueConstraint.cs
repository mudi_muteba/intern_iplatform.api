﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150617144930)]
    public class AddChannelIdUniqueConstraint : Migration
    {
        public override void Up()
        {
            Create.UniqueConstraint(Constraints.UC_Channel)
                .OnTable("Channel")
                .Columns(new[] { "SystemId" });
        }

        public override void Down()
        {
            Delete.UniqueConstraint(Constraints.UC_Channel);
        }
    }
}