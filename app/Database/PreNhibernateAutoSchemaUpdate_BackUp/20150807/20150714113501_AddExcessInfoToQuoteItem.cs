using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150714113501)]
    public class AddExcessInfoToQuoteItem : Migration
    {
        public override void Up()
        {
            Alter.Table("QuoteItem")
                .AddColumn("voluntaryExcess").AsDecimal().Nullable().WithDefaultValue(0);
        }

        public override void Down()
        {
            Delete
                .Column("voluntaryExcess")
                .FromTable("QuoteItem");
        }
    }
}