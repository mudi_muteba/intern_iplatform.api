using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150722112005)]
    public class ClaimItemQuestionAnswer : Migration
    {
        public override void Up()
        {
            Create
                .Table("ClaimsItemQuestionAnswer")
                .WithIdColumn()
                .WithColumn("ClaimsItemId").AsInt32().NotNullable().Indexed()
                .WithColumn("ClaimsQuestionDefinitionId").AsInt32().NotNullable()
                .WithColumn("Answer").AsString(255)
                .WithColumn("ClaimsQuestionTypeId").AsInt32().NotNullable();
        }

        public override void Down()
        {
            Delete.Table("ClaimsLeadActivity");
        }
    }
}