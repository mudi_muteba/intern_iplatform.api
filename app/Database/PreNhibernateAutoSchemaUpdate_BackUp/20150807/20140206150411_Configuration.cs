﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140206150411)]
    public class Configuration : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140206150411_Configuration.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20140206150411_Configuration.remove.sql");
        }
    }
}
