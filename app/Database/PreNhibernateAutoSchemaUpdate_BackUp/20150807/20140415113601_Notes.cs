﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140415113601)]
    public class Notes : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140415113601_Notes.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20140415113601_Notes.remove.sql");
        }
    }
}