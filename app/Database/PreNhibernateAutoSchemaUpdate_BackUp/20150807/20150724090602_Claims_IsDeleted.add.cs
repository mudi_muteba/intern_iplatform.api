using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150724090602)]
    public class Claims_IsDeleted : Migration
    {
        public override void Up()
        {
            Alter
                .Table("ClaimsItem")
                .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);
            Alter
                .Table("ClaimsHeader")
                .AddColumn("IsDeleted").AsBoolean().Nullable().WithDefaultValue(false);

        }

        public override void Down()
        {
            
        }
    }
}