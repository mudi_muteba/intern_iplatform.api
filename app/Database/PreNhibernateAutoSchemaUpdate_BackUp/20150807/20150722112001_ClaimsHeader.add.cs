using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150722112001)]
    public class Claims_Header : Migration
    {
        public override void Up()
        {
            Create
                .Table("ClaimsHeader")
                .WithIdColumn()
                .WithTimeStamps()
                .WithColumn("PolicyHeaderId").AsInt32().NotNullable().Indexed()
                .WithColumn("PartyId").AsInt32().NotNullable()
                .WithColumn("ExternalReference").AsString(255).Nullable();

        }

        public override void Down()
        {
            Delete.Table("ClaimsHeader");
        }
    }
}