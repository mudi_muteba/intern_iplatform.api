﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141210140816)]
    public class RiskItemTypeProper : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141210140816_ufn_ProperCase.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20141210140816_ufn_ProperCase.remove.sql");
        }
    }
}