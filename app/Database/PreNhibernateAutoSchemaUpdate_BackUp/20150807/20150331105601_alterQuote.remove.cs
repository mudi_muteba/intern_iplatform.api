﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150331105601)]
    public class AlterQuoteRemove : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150331105601_alterQuote.remove.sql");
        }

        public override void Down()
        {
        }
    }
}