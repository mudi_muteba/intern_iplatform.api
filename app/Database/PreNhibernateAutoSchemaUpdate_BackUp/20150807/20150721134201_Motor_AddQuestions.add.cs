﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150720103701)]
    public class AllRisk_QuestionDefinition_Asset : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150720103701_AllRisk_QuestionDefinition_Asset.add.sql");
        }

        public override void Down()
        {
        }
    }
}