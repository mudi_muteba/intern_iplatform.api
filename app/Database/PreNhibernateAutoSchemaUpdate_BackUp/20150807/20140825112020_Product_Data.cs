﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Profile("Test")]
    [Migration(20140825112020)]
    public class Product_Data : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140825112020_Product_Data.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}