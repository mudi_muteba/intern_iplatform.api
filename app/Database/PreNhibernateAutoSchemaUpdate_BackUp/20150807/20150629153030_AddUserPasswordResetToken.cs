﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150629153030)]
    public class AddUserPasswordResetToken : Migration
    {
        public override void Up()
        {
            Alter.Table(Tables.User).AddColumn("PasswordResetToken").AsGuid().Nullable();
        }

        public override void Down()
        {
            Delete.Column("PasswordResetToken").FromTable(Tables.User);
        }
    }
}