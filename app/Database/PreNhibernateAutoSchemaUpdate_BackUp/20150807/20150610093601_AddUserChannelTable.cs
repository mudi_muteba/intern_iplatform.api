﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150610093601)]
    public class AddUserChannelTable : Migration
    {
        public override void Up()
        {
            Create
                .Table("UserChannel")
                .WithIdColumn()
                .WithTimeStamps()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("ChannelId").AsInt32().NotNullable();
        }

        public override void Down()
        {
            Delete.Table("UserChannel");
        }
    }
}