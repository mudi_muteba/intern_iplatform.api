﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140901132710)]
    public class UpdateConstructionQuestions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140901132710_UpdateConstructionQuestions.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}