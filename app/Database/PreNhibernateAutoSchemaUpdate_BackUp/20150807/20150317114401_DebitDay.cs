﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150317114401)]
    public class DebitDay : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150317114401_DebitDay.add.sql");
        }

        public override void Down()
        {
        }
    }
}