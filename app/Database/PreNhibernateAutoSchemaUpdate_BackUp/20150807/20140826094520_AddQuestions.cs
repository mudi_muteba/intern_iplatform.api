﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140826094520)]
    public class AddQuestions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140826094520_Add_Questions.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}