﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141014155600)]
    public class AgentLeadStatsCurrentSP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141014155600_AgentLeadStatsCurrentStoredProcedure.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20141014155600_AgentLeadStatsCurrentStoredProcedure.remove.sql");
        }
    }
}