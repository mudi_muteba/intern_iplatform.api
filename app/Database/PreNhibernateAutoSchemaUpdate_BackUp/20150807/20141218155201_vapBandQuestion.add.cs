﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141218155201)]
    public class vapBandQuestion : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141218155201_vapBandQuestion.add.sql");
        }

        public override void Down()
        {
        }
    }
}