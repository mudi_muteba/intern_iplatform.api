using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150806145402)]
    public class Id_To_PartyId : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150806145401_Id_To_PartyId.add.sql");
        }

        public override void Down()
        {
            
        }
    }
}