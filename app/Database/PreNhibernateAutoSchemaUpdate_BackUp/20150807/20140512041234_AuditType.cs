﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140512041234)]
    public class AuditType : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140512041234_AuditType.add.sql");
            Execute.EmbeddedScript("20140512041234_AuditTrialItem.Update.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20140512041234_AuditType.remove.sql");
            Execute.EmbeddedScript("20140512041234_AuditTrialItem.UpdateRemove.sql");
        }
    }
}
