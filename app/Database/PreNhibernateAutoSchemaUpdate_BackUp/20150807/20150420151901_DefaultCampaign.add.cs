﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150420151901)]
    public class DefaultCampaign : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150420151901_DefaultCampaign.add.sql");
        }

        public override void Down()
        {
        }
    }
}