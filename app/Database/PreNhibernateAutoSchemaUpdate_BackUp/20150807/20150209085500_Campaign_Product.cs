﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150209085500)]

    public class Campaign_Product : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150209085500_Campaign_Product.add.sql");
        }

        public override void Down()
        {
        }
    }
}