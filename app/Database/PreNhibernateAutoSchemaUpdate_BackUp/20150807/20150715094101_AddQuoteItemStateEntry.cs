using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150715094101)]
    public class AddQuoteItemStateEntry : Migration
    {
        public override void Up()
        {
            Create
                .Table("QuoteItemStateEntry")
                .WithIdColumn()
                .WithColumn("QuoteItemStateId").AsInt32().NotNullable().Indexed()
                .WithColumn("Message").AsString(int.MaxValue).NotNullable()
                .WithColumn("IsError").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Table("QuoteItemStateEntry");
        }
    }
}