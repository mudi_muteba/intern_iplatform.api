﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150318114201)]
    public class RenameProposalFieldOnLeadActivity : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150318114201_RenameProposalFieldOnLeadActivity.sql");
        }

        public override void Down()
        {
        }
    }
}