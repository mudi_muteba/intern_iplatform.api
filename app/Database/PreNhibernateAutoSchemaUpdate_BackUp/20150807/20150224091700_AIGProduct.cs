﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150224091700)]
    public class AIGProduct : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150224091700_AIGProduct.add.sql");
        }

        public override void Down()
        {
        }
    }
}