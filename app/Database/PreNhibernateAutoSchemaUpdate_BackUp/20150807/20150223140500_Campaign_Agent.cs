﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150223140500)]

    public class Campaign_Agent : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150223140500_Campaign_Agent.add.sql");
        }

        public override void Down()
        {
        }
    }
}