﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150527130601)]
    public class ManufactureYearDropdown : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150527130601_Manufacture_Year_Dropdown.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}