﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150625151030)]
    public class AddUserChannelForeignKeys : Migration
    {
        public override void Up()
        {
            Create.ForeignKey(Constraints.FK_UserChannel_User)
                .FromTable(Tables.UserChannel)
                .ForeignColumn("UserId")
                .ToTable(Tables.User)
                .InSchema(Schemas.Dbo)
                .PrimaryColumn("Id");

            Create.ForeignKey(Constraints.FK_UserChannel_Channel)
                .FromTable(Tables.UserChannel)
                .ForeignColumn("ChannelId")
                .ToTable(Tables.Channel)
                .InSchema(Schemas.Dbo)
                .PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.ForeignKey(Constraints.FK_UserChannel_User).OnTable(Tables.UserChannel).InSchema(Schemas.Dbo);
            Delete.ForeignKey(Constraints.FK_UserChannel_Channel).OnTable(Tables.UserChannel).InSchema(Schemas.Dbo);
        }
    }
}