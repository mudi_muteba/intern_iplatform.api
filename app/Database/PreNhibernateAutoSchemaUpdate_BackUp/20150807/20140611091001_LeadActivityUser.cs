﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140611091001)]
    public class LeadActivityUser : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140611091001_LeadActivityUser.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20140611091001_LeadActivityUser.remove.sql");
        }
    }
}