﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150116082905)]
    public class ClaimsMasterData : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150116082905_ClaimsMasterData.add.sql");
        }

        public override void Down()
        {
        }
    }
}