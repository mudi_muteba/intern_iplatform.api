﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141014150204)]
    public class ProposalformHouseholdContentsSP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141014150204_ProposalformHouseholdContentsStoredProcedure.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20141014150204_ProposalformHouseholdContentsStoredProcedure.remove.sql");
        }
    }
}
