﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141125123907)]
    public class InsertTyerProtectProduct : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141125123907_InsertTyerProtectProduct.Add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}