﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150626134530)]
    public class AddUserIsActiveFlag : Migration
    {
        public override void Up()
        {
            Alter.Table(Tables.User).AddColumn("IsActive").AsBoolean().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Column("IsActive").FromTable(Tables.User);
        }
    }
}