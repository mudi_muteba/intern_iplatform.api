﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150115072601)]
    public class ClaimsTableQuestionDefinition : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150115072601_ClaimsTableQuestionDefinition.add.sql");
        }

        public override void Down()
        {
        }
    }
}