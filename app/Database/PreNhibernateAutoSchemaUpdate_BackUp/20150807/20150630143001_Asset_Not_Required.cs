﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150630143001)]
    public class Asset_Not_Required : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150630143001_Asset_Not_Required.sql");
        }

        public override void Down()
        {
        }
    }
}