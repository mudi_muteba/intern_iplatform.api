﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150603141905)]
    public class OakhurstProducts : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150603141905_OakhurstProducts.add.sql");
        }

        public override void Down()
        {
        }
    }
}