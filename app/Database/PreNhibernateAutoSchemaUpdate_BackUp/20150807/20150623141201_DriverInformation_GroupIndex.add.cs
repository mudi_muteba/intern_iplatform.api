﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150623141201)]
    public class DriverInformationGroupIndex : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150623141201_DriverInformation_GroupIndex.add.sql");
        }

        public override void Down()
        {
        }
    }
}