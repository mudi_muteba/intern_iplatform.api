﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150520092131)]
    public class Add_QuestionsMasterKP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150520092131_Add_QuestionsMasterKP.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}