﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140826170030)]
    public class UpdateProduct : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140826170030_Update_Product.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}