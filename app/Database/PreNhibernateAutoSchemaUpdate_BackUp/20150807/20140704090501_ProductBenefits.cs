﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140704090501)]
    public class AddProductBenefits : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140704090501_ProductBenefits.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20140704090501_ProductBenefits.remove.sql");
        }
    }
}