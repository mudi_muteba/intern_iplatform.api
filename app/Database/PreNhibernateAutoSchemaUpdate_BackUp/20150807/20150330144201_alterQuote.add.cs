﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150330144201)]
    public class alterQuote2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150330144201_alterQuote.add.sql");
        }

        public override void Down()
        {
        }
    }
}