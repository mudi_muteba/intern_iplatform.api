﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150519160431)]
    public class AddQuestionsMaster : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150519160431_Add_QuestionsMaster.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}