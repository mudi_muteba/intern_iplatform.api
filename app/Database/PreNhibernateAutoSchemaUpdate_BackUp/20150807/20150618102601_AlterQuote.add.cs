﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150618102601)]
    public class AlterQuote2 : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150618102601_AlterQuote.add.sql");
        }

        public override void Down()
        {
        }
    }
}