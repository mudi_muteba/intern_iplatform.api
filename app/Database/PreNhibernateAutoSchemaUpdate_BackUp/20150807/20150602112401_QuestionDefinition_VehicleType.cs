﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150602112401)]
    public class QuestionDefinitionVehicleType : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150602112401_QuestionDefinition_VehicleType.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}