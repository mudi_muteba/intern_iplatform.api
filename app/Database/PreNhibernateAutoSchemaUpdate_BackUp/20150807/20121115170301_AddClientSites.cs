﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121115170301)]
    public class AddClientSites : Migration
    {
        public override void Up()
        {
            Create.Table(Tables.ClientSite)
                  .WithColumn("Id").AsInt32().PrimaryKey().NotNullable() // No identity, the PK is an FK from Organization
                  .WithColumn("MessageQueueAddress").AsString().NotNullable();

            Create.ForeignKey("FK_ClientSite_Clients")
                  .FromTable(Tables.MessageClient)
                  .ForeignColumn("ClientSiteId")
                  .ToTable(Tables.ClientSite)
                  .PrimaryColumn("Id");

            Create.ForeignKey("FK_ClientSite_Organization")
                  .FromTable(Tables.ClientSite)
                  .ForeignColumn("Id")
                  .ToTable("Organization")
                  .PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.ForeignKey("FK_ClientSite_Clients").OnTable(Tables.MessageClient);
            Delete.Table(Tables.ClientSite);
        }
    }
}