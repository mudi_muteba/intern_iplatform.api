﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141023171712)]
    public class UpdateHollardQuestions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141023171712_UpdateHollardQuestions.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}