﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141119094111)]
    public class RelationshipType : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141119094111_RelationshipType.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}