﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140117110057)]
    public class QuestionsTelesure : Migration
    {

        public override void Up()
        {
            Execute.EmbeddedScript("20140117110057_Questions_Checkbox.sql");
        }

        public override void Down()
        {
        }
    }
}