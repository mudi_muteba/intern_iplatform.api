﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121115163301)]
    public class AddMessageQueues : Migration
    {
        public override void Up()
        {
            Create.Table(Tables.MessageQueue)
                  .WithIdColumn()
                  .WithColumn("PublishedDate").AsInt64().Nullable()
                  .WithColumn("MessageType").AsString().NotNullable()
                  .WithColumn("DomainId").AsInt32().NotNullable()
                  .WithColumn("Completed").AsBoolean().NotNullable()
                  .WithColumn("Success").AsInt32().NotNullable()
                  .WithColumn("UserId").AsInt32().NotNullable();
        }

        public override void Down() 
        {
            Delete.Table(Tables.MessageQueue);
        }
    }
}