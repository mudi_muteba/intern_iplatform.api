﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150115082130)]
    public class FirstLevel : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150115082130_FirstLevel.add.sql");
        }

        public override void Down()
        {
        }
    }
}