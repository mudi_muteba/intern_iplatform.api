﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150605135401)]
    public class AssetVehicleExtras : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150605135401_AssetVehicleExtras.add.sql");
        }

        public override void Down()
        {
        }
    }
}