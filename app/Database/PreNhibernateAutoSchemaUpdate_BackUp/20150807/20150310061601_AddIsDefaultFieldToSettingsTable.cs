﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150310061601)]
    public class AddIsDefaultFieldToSettingsTable : Migration
    {
        public override void Up()
        {
            Alter
                .Table("Settings")
                .AddColumn("IsDefault").AsBoolean().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete
                .Column("IsDefault")
                .FromTable("Settings");
        }
    }
}