﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150610121301)]
    public class AlterExtras : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150610121301_AlterExtras.add.sql");
        }

        public override void Down()
        {
        }
    }
}