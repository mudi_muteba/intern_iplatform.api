using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150713163801)]
    public class EnableVirsekereAndAutoAndGeneralProducts : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150713163801_EnableVirsekereAndAutoAndGeneralProducts.sql");
        }

        public override void Down()
        {
        }
    }
}