﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141203154213)]
    public class RiskItemTypeAdd : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141203154213_RiskItemType.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}