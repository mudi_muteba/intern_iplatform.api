﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141119133938)]
    public class ContactPerson : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141119133938_ContactPerson.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}