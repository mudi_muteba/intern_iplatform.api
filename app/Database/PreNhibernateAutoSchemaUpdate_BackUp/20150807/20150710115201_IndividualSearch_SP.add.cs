﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150710115201)]
    public class IndividualSearch : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150710115201_IndividualSearch_SP.add.sql");
        }

        public override void Down()
        {
        }
    }
}