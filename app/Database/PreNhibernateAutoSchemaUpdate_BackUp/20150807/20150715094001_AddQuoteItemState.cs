using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150715094001)]
    public class AddQuoteItemState : Migration
    {
        public override void Up()
        {
            Create
                .Table("QuoteItemState")
                .WithIdColumn()
                .WithColumn("QuoteItemId").AsInt32().NotNullable().Indexed()
                .WithColumn("Errors").AsBoolean().NotNullable().WithDefaultValue(false)
                .WithColumn("Warnings").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Table("QuoteItemState");
        }
    }
}