﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150713075701)]
    public class RebuildQuoteTable : Migration
    {
        public override void Up()
        {
            Delete.ForeignKey("FK_Quote_LeadActivity").OnTable("Quote");
            Delete.ForeignKey("FK_Quote_Product").OnTable("Quote");

            Delete
                .Column("DateCreated")
                .Column("AcceptedDate")
                .Column("RequestId")
                .Column("FailureMessage")
                .Column("WarningMessage")
                .Column("LeadActivityId")
                .FromTable("Quote");
                
            Alter.Table("Quote")
                .AddColumn("QuoteHeaderId").AsInt32().Nullable()
                .AddColumn("CreatedAt").AsDateTime().Nullable()
                .AddColumn("ModifiedAt").AsDateTime().Nullable()
                .AddColumn("AcceptedOn").AsDate().Nullable();
        }

        public override void Down()
        {
        }
    }
}