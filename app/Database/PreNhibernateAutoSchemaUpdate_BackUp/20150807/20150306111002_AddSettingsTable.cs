﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150306111002)]
    public class AddSettingsTable : Migration
    {
        public override void Up()
        {
            Create
                .Table("Settings")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity().NotNullable()
                .WithColumn("ChannelId").AsGuid().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable().WithDefaultValue(false)
                .WithColumn("ActivatedOn").AsDateTime().Nullable()
                .WithColumn("DeactivatedOn").AsDateTime().Nullable()
                ;
        }

        public override void Down()
        {
            Delete
                .Table("Settings");
        }
    }
}