﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20130114160432)]
    public class Addresses : Migration
    {
        public override void Up()
        {
            Create.Table(Tables.Address)
                .WithIdColumn()
                .WithColumn("PartyId").AsInt32().NotNullable()
                .WithColumn("MasterId").AsInt32()
                .WithColumn("Description").AsString()
                .WithColumn("Complex").AsString(100).Nullable()
                .WithColumn("Line1").AsString(100).NotNullable()
                .WithColumn("Line2").AsString(100).NotNullable()
                .WithColumn("Line3").AsString(100)
                .WithColumn("Line4").AsString(100)
                .WithColumn("Code").AsAnsiString(10).NotNullable()
                .WithColumn("Latitude").AsDecimal(9, 6).NotNullable().WithDefaultValue(0D)
                .WithColumn("Longitude").AsDecimal(9, 6).NotNullable().WithDefaultValue(0D)
                .WithColumn("StateProvinceId").AsInt32().NotNullable()
                .WithColumn("AddressTypeId").AsInt32().NotNullable()
                .WithColumn("IsDefault").AsBoolean().NotNullable().WithDefaultValue(false)
                .WithColumn("IsComplex").AsBoolean().NotNullable().WithDefaultValue(false)
                .WithColumn("DateFrom").AsDate().Nullable()
                .WithColumn("DateTo").AsDate().Nullable();

            Create.Table(Tables.AddressType)
                .InSchema(Schemas.Master)
                .WithIdColumn().ReferencedBy(Constraints.FK_Address_AddressType, Tables.Address, "AddressTypeId")
                .WithColumn("Name").AsAnsiString(20).NotNullable();

            Create.Table(Tables.StateProvince)
                .InSchema(Schemas.Master)
                .WithIdColumn().ReferencedBy(Constraints.FK_Address_StateProvince, Tables.Address, "StateProvinceId")
                .WithColumn("Name").AsAnsiString(50).NotNullable()
                .WithColumn("CountryId").AsInt32().NotNullable();

            Create.Table(Tables.Country)
                .InSchema(Schemas.Master)
                .WithIdColumn().ReferencedBy(Constraints.FK_StateProvince_Country, Schemas.Master, Tables.StateProvince, "CountryId")
                .WithColumn("Name").AsString(100).NotNullable()
                .WithColumn("Code").AsAnsiString(2).NotNullable();

            Create.Table(Tables.PostalCode)
                .WithIdColumn()
                .WithColumn("Town").AsString(50).NotNullable()
                .WithColumn("City").AsString(30).NotNullable()
                .WithColumn("StreetCode").AsAnsiString(4).NotNullable()
                .WithColumn("BoxCode").AsAnsiString(4).NotNullable();

            // Foreign Key (Party)
            Create.ForeignKey(Constraints.FK_Party_Address)
                .FromTable(Tables.Address).ForeignColumn("PartyId")
                .ToTable(Tables.Party).PrimaryColumn("Id");

            // Master Data
            Execute.EmbeddedScript("20130114160432_Countries.add.sql");
            Execute.EmbeddedScript("20130114160432_PostalCodes.add.sql");

            Insert.IntoTable(Tables.AddressType)
                .InSchema(Schemas.Master)
                .Row(new {Name = "Physical Address"})
                .Row(new {Name = "Postal Address"});

            Insert.IntoTable(Tables.StateProvince)
                .InSchema(Schemas.Master)
                .Row(new { Name = "[Not Specified]", CountryId = 197 })
                .Row(new { Name = "Eastern Cape", CountryId = 197 })
                .Row(new { Name = "Free State", CountryId = 197 })
                .Row(new { Name = "Gauteng", CountryId = 197 })
                .Row(new { Name = "KwaZulu-Natal", CountryId = 197 })
                .Row(new { Name = "Limpopo", CountryId = 197 })
                .Row(new { Name = "Mpumalanga", CountryId = 197 })
                .Row(new { Name = "Northern Cape", CountryId = 197 })
                .Row(new { Name = "North West", CountryId = 197 })
                .Row(new { Name = "Western Cape", CountryId = 197 });
        }

        public override void Down()
        {
            Delete.ForeignKey(Constraints.FK_Party_Address).OnTable(Tables.Address);
            Delete.ForeignKey(Constraints.FK_Address_AddressType).OnTable(Tables.Address);
            Delete.ForeignKey(Constraints.FK_Address_StateProvince).OnTable(Tables.Address);

            Delete.ForeignKey(Constraints.FK_StateProvince_Country).OnTable(Tables.StateProvince).InSchema(Schemas.Master);

            Delete.Table(Tables.StateProvince).InSchema(Schemas.Master);
            Delete.Table(Tables.Country).InSchema(Schemas.Master);
            Delete.Table(Tables.AddressType).InSchema(Schemas.Master);

            Delete.Table(Tables.PostalCode);
            Delete.Table(Tables.Address);
        }
    }
}
