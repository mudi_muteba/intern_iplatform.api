using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150715104201)]
    public class AddQuoteHeaderToQuotedLeadActivity : Migration
    {
        public override void Up()
        {
            Alter
                .Table("QuotedLeadActivity")
                .AddColumn("QuoteHeaderId").AsInt32().Nullable().Indexed();
        }

        public override void Down()
        {
            Delete.Column("QuoteHeaderId").FromTable("QuotedLeadActivity");
        }
    }
}