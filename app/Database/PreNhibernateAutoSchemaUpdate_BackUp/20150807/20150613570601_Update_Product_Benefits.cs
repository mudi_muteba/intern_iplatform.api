﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150613570601)]
    public class UpdateProductBenefits : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150617135701_Update_Product_Benefits.sql");
        }

        public override void Down()
        {
        }
    }
}