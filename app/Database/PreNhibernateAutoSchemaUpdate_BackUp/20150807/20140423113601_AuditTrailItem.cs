﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140423113601)]
    public class AuditTrailItem : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140423113601_AuditTrailItems.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20140423113601_AuditTrailItems.remove.sql");
        }
    }
}