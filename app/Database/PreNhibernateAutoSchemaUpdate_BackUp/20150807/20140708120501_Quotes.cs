﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140708120501)]
    public class Quotes : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140708120501_Quotes.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20140708120501_Quotes.remove.sql");
        }
    }
}