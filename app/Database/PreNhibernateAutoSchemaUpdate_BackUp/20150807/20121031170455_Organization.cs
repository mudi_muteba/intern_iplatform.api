﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20121031170455)]
    public class OrganizationDetails : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20121031170455_Organization.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20121031170455_Organization.remove.sql");
        }
    }
}
