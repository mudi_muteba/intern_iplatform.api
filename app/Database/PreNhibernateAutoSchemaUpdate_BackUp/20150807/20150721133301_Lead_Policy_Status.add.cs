﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150721133301)]
    public class Lead_Policy_Status : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150721133301_Lead_Policy_Status.add.sql");
        }

        public override void Down()
        {
        }
    }
}