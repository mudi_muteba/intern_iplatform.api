﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150202063501)]
    public class AlterTableProposalDefinition : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150202063501_AlterTable.ProposalDefinition.add.sql");
        }

        public override void Down()
        {
        }
    }
}