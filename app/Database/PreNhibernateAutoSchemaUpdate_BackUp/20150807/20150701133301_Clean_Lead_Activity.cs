﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150701133301)]
    public class Clean_Lead_Activity : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150701133301_Clean_Lead_Activity.sql");
        }

        public override void Down()
        {
        }
    }
}