﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150520085858)]
    public class Alter_ClaimsQuestionDefinitions : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150520085858_Alter_ClaimsQuestionDefinitions.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}