﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150505082401)]
    public class AlterBankDetails : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150505082401_Alter.BankDetails.add.sql");
        }

        public override void Down()
        {
        }
    }
}