﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150617112530)]
    public class RenameSettingsTableToChannel : Migration
    {
        public override void Up()
        {
            Rename.Table("Settings").To("Channel");
        }

        public override void Down()
        {
            Rename.Table("Channel").To("Settings");
        }
    }
}