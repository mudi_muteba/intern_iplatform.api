﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150611110601)]
    public class PartyContact : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150611110601_Party_Contact.add.sql");
        }

        public override void Down()
        {
        }
    }
}