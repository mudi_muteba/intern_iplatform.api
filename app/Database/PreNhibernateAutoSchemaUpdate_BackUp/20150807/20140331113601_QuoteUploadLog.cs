﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140331113601)]
    public class QuoteUploadLog : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140331113601_QuoteUploadLog.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20140331113601_QuoteUploadLog.remove.sql");
        }
    }
}
