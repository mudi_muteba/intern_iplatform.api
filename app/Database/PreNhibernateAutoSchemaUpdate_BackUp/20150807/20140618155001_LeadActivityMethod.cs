﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140618155001)]
    public class LeadActivityMethod : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140618155001_LeadActivityMethod.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20140618155001_LeadActivityMethod.remove.sql");
        }
    }
}