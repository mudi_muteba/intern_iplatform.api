﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150402085510)]
    public class AddGreenBox : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150402085510_AddMFGreenbox.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}