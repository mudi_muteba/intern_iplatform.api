﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141118162234)]
    public class InsurersBranch : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141118162234_InsurersBranch.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}