﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150701082101)]
    public class Party_IsDeleted : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150701082101_Party_IsDeleted.add.sql");
        }

        public override void Down()
        {
        }
    }
}