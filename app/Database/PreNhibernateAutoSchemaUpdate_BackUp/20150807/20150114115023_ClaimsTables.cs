﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150114115023)]
    public class ClaimsTables : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150114115023_ClaimsTables.add.sql");
        }

        public override void Down()
        {
        }
    }
}