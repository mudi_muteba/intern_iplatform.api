﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150715112901)]
    public class ValuationDefault : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150715112901_ValuationDefault.add.sql");
        }

        public override void Down()
        {
        }
    }
}