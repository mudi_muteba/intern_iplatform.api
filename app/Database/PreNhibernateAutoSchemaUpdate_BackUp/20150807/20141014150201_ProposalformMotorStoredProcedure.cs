﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141014150201)]
    public class ProposalformMotorSP : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141014150201_ProposalformMotorStoredProcedure.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20141014150201_ProposalformMotorStoredProcedure.remove.sql");
        }
    }
}