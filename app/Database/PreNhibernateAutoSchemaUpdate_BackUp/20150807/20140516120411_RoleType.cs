﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20140516120411)]
    public class RoleType : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20140516120411_RoleType.add.sql");
        }

        public override void Down()
        {
        }
    }
}