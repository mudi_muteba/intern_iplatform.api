﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150302095230)]
    public class RatingSettings : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150302095230_RatingSettings.add.sql");
        }

        public override void Down()
        {
        }
    }
}