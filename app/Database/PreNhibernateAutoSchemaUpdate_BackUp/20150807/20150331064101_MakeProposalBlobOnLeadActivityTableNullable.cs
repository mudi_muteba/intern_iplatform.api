﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150331064101)]
    public class MakeProposalBlobOnLeadActivityTableNullable : Migration
    {
        public override void Up()
        {
            Alter
                .Table("LeadActivity")
                .AlterColumn("ProposalBlob").AsString(int.MaxValue).Nullable();
        }

        public override void Down()
        {
        }
    }
}