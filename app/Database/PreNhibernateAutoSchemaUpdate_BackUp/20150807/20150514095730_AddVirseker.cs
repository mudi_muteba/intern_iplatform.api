﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150514095730)]
    public class AddVirseker : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150514095730_AddVirseker.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}