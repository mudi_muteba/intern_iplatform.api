﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150506152103)]
    public class AlterNotes : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150506152103_AlterNotes.add.sql");
        }

        public override void Down()
        {
        }
    }
}