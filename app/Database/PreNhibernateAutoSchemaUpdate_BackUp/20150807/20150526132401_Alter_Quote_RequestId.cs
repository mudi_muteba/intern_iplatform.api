﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150526132401)]
    public class AlterQuoteRequestId : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150526132401_Alter_Quote_RequestId.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}