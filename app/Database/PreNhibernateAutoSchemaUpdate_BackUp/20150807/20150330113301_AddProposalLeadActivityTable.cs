﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150330113301)]
    public class AddProposalLeadActivityTable : Migration
    {
        public override void Up()
        {
            Create
                .Table("ProposalLeadActivity")
                .WithColumn("Id").AsInt64().Identity().NotNullable()
                .WithColumn("ProposalHeaderId").AsInt64().NotNullable();
        }

        public override void Down()
        {
            Delete
                .Table("ProposalLeadActivity");
        }
    }
}