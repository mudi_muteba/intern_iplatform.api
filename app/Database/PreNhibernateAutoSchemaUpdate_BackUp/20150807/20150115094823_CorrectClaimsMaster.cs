﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150115094823)]
    public class CorrectClaimsMaster : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150115094823_CorrectClaimsMaster.add.sql");
        }

        public override void Down()
        {
        }
    }
}