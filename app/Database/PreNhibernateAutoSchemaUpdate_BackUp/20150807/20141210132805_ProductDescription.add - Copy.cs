﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141210132805)]
    public class RiskItemType : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141210132805_ProductDescription.add.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20141210132805_ProductDescription.remove.sql");
        }
    }
}