﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150312095501)]
    public class alterProposalHeader : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150312095501_alterProposalHeader.add.sql");
        }

        public override void Down()
        {
        }
    }
}