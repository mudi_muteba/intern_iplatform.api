﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150209095601)]
    public class alterProposalQuestionAnswer : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150209095601_alterProposalQuestionAnswer.add.sql");
        }

        public override void Down()
        {
        }
    }
}