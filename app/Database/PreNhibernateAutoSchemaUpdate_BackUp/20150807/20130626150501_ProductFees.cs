﻿using Database.Schema;
using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20130626150501)]
    public class ProductFees : Migration
    {
        public override void Up()
        {
            Create.Table(Tables.ProductFee)
                .WithIdColumn()
                .WithColumn("MasterId").AsInt32()
                .WithColumn("ProductId").AsInt32().NotNullable()
                .WithColumn("ProductFeeTypeId").AsInt32().NotNullable()
                .WithColumn("PaymentPlanId").AsInt32().NotNullable()
                .WithColumn("BrokerageId").AsInt32().Nullable()
                .WithColumn("Value").AsDecimal(19, 5).NotNullable().WithDefaultValue(0D)
                .WithColumn("MinValue").AsDecimal(19, 5).NotNullable().WithDefaultValue(0D)
                .WithColumn("MaxValue").AsDecimal(19, 5).NotNullable().WithDefaultValue(0D)
                .WithColumn("IsOnceOff").AsBoolean().NotNullable().WithDefaultValue(false)
                .WithColumn("IsPercentage").AsBoolean().NotNullable().WithDefaultValue(false)
                .WithColumn("AllowRefund").AsBoolean().NotNullable().WithDefaultValue(true)
                .WithColumn("AllowProRata").AsBoolean().NotNullable().WithDefaultValue(true);

            Create.Table(Tables.ProductFeeType)
                .InSchema(Schemas.Master)
                .WithIdColumn()
                .WithColumn("Name").AsString(20).NotNullable().Unique();

            Insert.IntoTable(Tables.ProductFeeType)
                .InSchema(Schemas.Master)
                .Row(new { Name = "Administrator" })
                .Row(new { Name = "Broker" })
                .Row(new { Name = "Underwriter" });

            Create.Table(Tables.PaymentPlan)
                .InSchema(Schemas.Master)
                .WithIdColumn()
                .WithColumn("Name").AsString(20).NotNullable().Unique();

            Insert.IntoTable(Tables.PaymentPlan)
                .InSchema(Schemas.Master)
                .Row(new { Name = "Annual" })
                .Row(new { Name = "Bi-Annual" })
                .Row(new { Name = "Monthly" })
                .Row(new { Name = "Quarterly" })
                .Row(new { Name = "Once-Off" });

            Create.ForeignKey(Constraints.FK_ProductFee_ProductFeeType)
                .FromTable(Tables.ProductFee).ForeignColumn("ProductFeeTypeId")
                .ToTable(Tables.ProductFeeType)
                .InSchema(Schemas.Master)
                .PrimaryColumn("Id");

            Create.ForeignKey(Constraints.FK_ProductFee_PaymentPlan)
                .FromTable(Tables.ProductFee).ForeignColumn("PaymentPlanId")
                .ToTable(Tables.PaymentPlan)
                .InSchema(Schemas.Master)
                .PrimaryColumn("Id");

            Create.ForeignKey(Constraints.FK_ProductFee_Product)
                .FromTable(Tables.ProductFee).ForeignColumn("ProductId")
                .ToTable(Tables.Product).PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.ForeignKey(Constraints.FK_ProductFee_ProductFeeType).OnTable(Tables.ProductFee);
            Delete.ForeignKey(Constraints.FK_ProductFee_PaymentPlan).OnTable(Tables.ProductFee);
            Delete.ForeignKey(Constraints.FK_ProductFee_Product).OnTable(Tables.ProductFee);

            Delete.Table(Tables.ProductFeeType).InSchema(Schemas.Master);
            Delete.Table(Tables.PaymentPlan).InSchema(Schemas.Master);

            Delete.Table(Tables.ProductFee);
        }
    }
}