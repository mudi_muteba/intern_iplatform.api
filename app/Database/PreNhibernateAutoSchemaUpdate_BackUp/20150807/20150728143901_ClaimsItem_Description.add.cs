using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150728143901)]
    public class ClaimsITem_Description : Migration
    {
        public override void Up()
        {
            Alter
                .Table("ClaimsItem")
                .AddColumn("Description").AsString().Nullable();
        }

        public override void Down()
        {
            
        }
    }
}