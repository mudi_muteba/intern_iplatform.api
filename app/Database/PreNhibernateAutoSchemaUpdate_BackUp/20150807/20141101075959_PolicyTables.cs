﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141101075959)]
    public class PolicyTables : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141101075959_PolicyTables.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
        
    }
}