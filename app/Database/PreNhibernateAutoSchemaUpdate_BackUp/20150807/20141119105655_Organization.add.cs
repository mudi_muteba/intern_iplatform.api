﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20141119105655)]
    public class OrganizationTypeAdd : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20141119105655_Organization.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}