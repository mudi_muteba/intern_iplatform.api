﻿using FluentMigrator;

namespace Database.Migrations._20150807
{
    [Migration(20150602093200)]
    public class AddQuestionsMasterTS : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20150602093200_Add_QuestionsMasterTS.add.sql");
        }

        public override void Down()
        {
            // Not required
        }
    }
}