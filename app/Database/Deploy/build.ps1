param([string]$isVisualStudio, [string]$targetDir)

if ($isVisualStudio -eq $true)
{
	Remove-Module ToolBox.Builds -ErrorAction SilentlyContinue
	Import-Module ToolBox.Builds

	Push-Location (Split-Path $MyInvocation.MyCommand.Path -Parent)
	Read-Configuration (Join-Path $targetDir "App.config")

	$assembly = Resolve-Path "$targetDir\iBroker.Database.dll"
	$migrator = Resolve-Path "..\..\..\tools\migrator\migrate.exe"

	$DatabaseUpgrade = $true

	Update-Database `
		$connectionStrings["iBrokerConnectionString"] $migrator $assembly
}
else
{
	Write-Host "[BUILD] Skipping PostBuild Event - Not inside Visual Studio"
}