﻿using System.Collections.ObjectModel;

namespace ValidationMessages.Settings
{
    public class SettingValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage InvalidPasswordStrength = new ValidationErrorMessage("Setting",
            "The password specified does not meet the necessary security requirements. " +
            "The password must: be at least 8 characters long, " +
            "contain 1 lowercase character, contain 1 uppercase character, " +
            "contain 1 special character, contain 1 numeric character", "WEAK_PASSWORD");

        public SettingValidationMessages() : base(new[] { InvalidPasswordStrength })
        {
        }
    }
}