using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class ProductValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage InvalidProductCode = new ValidationErrorMessage("Search",
            "ProductCode {0} does not exist", "PRODUCT_INVALID_PRODUCTCODE");


        public ProductValidationMessages()
            : base(
                new[]
                {
                    InvalidProductCode,
                })
        {
        }
    }
}