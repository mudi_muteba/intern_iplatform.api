﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationMessages
{
    public class OverrideRatingQuestionValidationMessage : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("OverrideRatingQuestionDto.Id",
            "Id can not be empty", "OVERRIDERATINGQUESTION_ID_EMPTY");

        public static ValidationErrorMessage ProductIdRequired = new ValidationErrorMessage("OverrideRatingQuestionDto.ProductIdRequired",
            "ProductIdRequired can not be empty", "OVERRIDERATINGQUESTION_PRODUCTIDREQUIRED_EMPTY");

        public static ValidationErrorMessage CoverIdRequired = new ValidationErrorMessage("OverrideRatingQuestionDto.CoverIdRequired",
            "CoverIdRequired can not be empty", "OVERRIDERATINGQUESTION_COVERIDREQUIRED_EMPTY");

        public static ValidationErrorMessage QuestionIdRequired = new ValidationErrorMessage("OverrideRatingQuestionDto.QuestionId",
            "QuestionId can not be empty", "OVERRIDERATINGQUESTION_QUESTIONID_EMPTY");

        public static ValidationErrorMessage CoverDefinitionIdNotFound = new ValidationErrorMessage("OverrideRatingQuestionDto.CoverDefinitionId",
            "CoverDefinitionId could not be found", "OVERRIDERATINGQUESTION_COVERDEFINITIONID_NOTFOUND");

        public static ValidationErrorMessage QuestionDefinitionIdNotFound = new ValidationErrorMessage("OverrideRatingQuestionDto.QuestionDefinitionId",
            "QuestionDefinitionId could not be found", "OVERRIDERATINGQUESTION_QUESTIONDEFINITIONID_NOTFOUND");

        public OverrideRatingQuestionValidationMessage() : base(new[]
        {
            IdRequired,
            ProductIdRequired,
            CoverIdRequired,
            QuestionIdRequired,
            CoverDefinitionIdNotFound,
            QuestionDefinitionIdNotFound
        })
        {
        }
    }
}
