using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class PaymentDetailsValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("PaymentDetails.Id",
            "Id cannot be empty", "PAYMENTDETAILS_ID_EMPTY");

        public static ValidationErrorMessage QuoteIdRequired = new ValidationErrorMessage("PaymentDetails.QuoteId",
            "QuoteId cannot be empty", "PAYMENTDETAILS_QUOTEID_REQUIRED");

        public static ValidationErrorMessage PartyIdRequired = new ValidationErrorMessage("PaymentDetails.PartyId",
            "PartyId cannot be empty", "PAYMENTDETAILS_PARTYID_REQUIRED");

        public static ValidationErrorMessage BankDetailIdRequired = new ValidationErrorMessage("PaymentDetails.BankDetailsId",
            "BankDetailsId cannot be empty", "PAYMENTDETAILS_BANKDETAILSID_REQUIRED");

        public static ValidationErrorMessage InvalidPartyId = new ValidationErrorMessage("PaymentDetails.PartyId",
            "Invalid PartyId: {0}", "RELATIONSHIP_PARTYID_INVALID");

        public static ValidationErrorMessage InvalidQuoteId = new ValidationErrorMessage("PaymentDetails.QuoteId",
            "Invalid QuoteId: {0}", "RELATIONSHIP_QUOTEID_INVALID");

        public static ValidationErrorMessage InvalidBankDetailsId = new ValidationErrorMessage("PaymentDetails.BankDetailsId",
            "Invalid BankDetailsId: {0}", "RELATIONSHIP_BANKDETAILSID_INVALID");

        public PaymentDetailsValidationMessages()
            : base(
                new[]
                {
                    IdRequired,
                    QuoteIdRequired,
                    PartyIdRequired,
                    BankDetailIdRequired,
                    InvalidPartyId,
                    InvalidQuoteId,
                    InvalidBankDetailsId,
                })
        {
        }
    }
}