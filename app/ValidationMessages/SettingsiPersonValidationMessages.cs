using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class SettingsiPersonValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("EditSettingsiPersonDto.Id",
            "Id can not be empty", "SETTINGSIPERSON_ID_EMPTY");

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("CreateSettingsiPersonDto.ChannelId",
            "ChannelId can not be empty", "SETTINGSIPERSON_CHANNELID_EMPTY");

        public static ValidationErrorMessage ProductIdRequired = new ValidationErrorMessage("CreateSettingsiPersonDto.ProductId",
            "ProductId can not be empty", "SETTINGSIPERSON_PRODUCTID_EMPTY");

        public static ValidationErrorMessage EnvironmentRequired = new ValidationErrorMessage("CreateSettingsiPersonDto.Environment",
            "Environment can not be empty", "SETTINGSIPERSON_ENVIRONMENT_EMPTY");

        public static ValidationErrorMessage IdRequiredForDelete = new ValidationErrorMessage("DisableSettingsiPersonDto.Id",
            "Id can not be empty or zero", "SETTINGSIPERSON_ID_EMPTY");

        public SettingsiPersonValidationMessages()
            : base(
                new[]
                {
                    ChannelIdRequired, ProductIdRequired, IdRequired, EnvironmentRequired, IdRequiredForDelete
                })
        {
        }
    }
}