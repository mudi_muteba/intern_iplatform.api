using System.Collections.ObjectModel;

namespace ValidationMessages.Individual.Address
{

    public class AddressValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage Line1Required = new ValidationErrorMessage("Address.Line1",
            "Line 1 can not be empty", "ADDRESS_LINE1_EMPTY");
        public static ValidationErrorMessage Line4Required = new ValidationErrorMessage("Address.Line4",
            "Line 4 can not be empty", "ADDRESS_LINE4_EMPTY");
        public static ValidationErrorMessage CodeRequired = new ValidationErrorMessage("Address.Code",
            "Code can not be empty", "ADDRESS_CODE_EMPTY");

        public AddressValidationMessages() : base(new[] { Line1Required, Line4Required,CodeRequired }) { }
    }
}