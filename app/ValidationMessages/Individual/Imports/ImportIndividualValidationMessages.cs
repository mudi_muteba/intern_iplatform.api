using System.Collections.ObjectModel;

namespace ValidationMessages.Individual.Imports
{
    public class ImportIndividualValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage TitleRequired = new ValidationErrorMessage("ImportIndividualDto.Title",
            "Title can not be empty", "IMPORT_INDIVIDUAL_TITLE_EMPTY");

        public static ValidationErrorMessage FirstNameRequired = new ValidationErrorMessage("ImportIndividualDto.FirstName",
            "First Name can not be empty", "IMPORT_INDIVIDUAL_FIRSTNAME_EMPTY");

        public static ValidationErrorMessage SurnameRequired = new ValidationErrorMessage("ImportIndividualDto.Surname",
            "Surname can not be empty", "IMPORT_INDIVIDUAL_SURNAME_EMPTY");

        public static ValidationErrorMessage IDNumberRequired = new ValidationErrorMessage("ImportIndividualDto.IDNumber",
            "ID number can not be empty", "IMPORT_INDIVIDUAL_IDNUMBER_EMPTY");

        public static ValidationErrorMessage MaritalStatusRequired = new ValidationErrorMessage("ImportIndividualDto.MaritalStatus",
            "Marital Status can not be empty", "IMPORT_INDIVIDUAL_MARITALSTATUS_EMPTY");

        public static ValidationErrorMessage ContactNumberRequired = new ValidationErrorMessage("ImportIndividualDto.ContactNumber",
            "Contact number can not be empty", "IMPORT_INDIVIDUAL_CONTACTNUMBER_EMPTY");

        public static ValidationErrorMessage EmailAddressRequired = new ValidationErrorMessage("ImportIndividualDto.EmailAddress",
            "Email address can not be empty", "IMPORT_INDIVIDUAL_EMAILADDRESS_EMPTY");
        
        public static ValidationErrorMessage ITCConsentRequired = new ValidationErrorMessage("ImportIndividualDto.ITCConsent",
            "ITC Consent can not be empty", "IMPORT_INDIVIDUAL_ITCCONSENT_EMPTY");

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("ImportIndividualDto.ChannelId",
            "Channel Id can not be empty", "IMPORT_INDIVIDUAL_CHANNELID_EMPTY");

        public static ValidationErrorMessage DateOfBirthRequired = new ValidationErrorMessage("ImportIndividualDto.DateOfBirth",
            "Date of birth can not be empty", "IMPORT_INDIVIDUAL_DOB_EMPTY");

        public static ValidationErrorMessage ExternalReferenceRequired = new ValidationErrorMessage("ImportIndividualDto.ExternalReference",
            "External reference can not be empty", "IMPORT_INDIVIDUAL_EXTERNALREFERENCE_EMPTY");

        public static ValidationErrorMessage ExternalReferenceInvalid = new ValidationErrorMessage("ImportIndividualDto.ExternalReference",
            "External reference is not a valid GUID", "IMPORT_INDIVIDUAL_EXTERNALREFERENCE_INVALID");

        public ImportIndividualValidationMessages() : base(new[] {  TitleRequired, FirstNameRequired,  IDNumberRequired, 
            MaritalStatusRequired, ContactNumberRequired, EmailAddressRequired, ITCConsentRequired, ChannelIdRequired,
            DateOfBirthRequired, ExternalReferenceRequired, SurnameRequired, ExternalReferenceInvalid
        }) { }
    }


}