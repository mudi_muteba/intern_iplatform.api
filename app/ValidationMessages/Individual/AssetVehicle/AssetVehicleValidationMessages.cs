using System.Collections.ObjectModel;

namespace ValidationMessages.Individual.AssetVehicle
{
    public class AssetVehicleValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage YearOfManufactureRequired =
            new ValidationErrorMessage("AssetVehicle.YearOfManufacture",
                " Year Of Manufacture can not be empty", "YEAR_OF_MANUFACTURE_EMPTY");

        public static ValidationErrorMessage VehicleMakeRequired = new ValidationErrorMessage(
            "AssetVehicle.VehicleMake",
            " Vehicle Make can not be empty", "VEHICLE_MAKE_EMPTY");

        public static ValidationErrorMessage VehicleModelRequired =
            new ValidationErrorMessage("AssetVehicle.VehicleModel",
                " Vehicle Model can not be empty", "VEHICLE_MODEL_EMPTY");

        public static ValidationErrorMessage VehicleMMCodeRequired = new ValidationErrorMessage(
            "AssetVehicle.VehicleMMCode",
            " Vehicle MMCode can not be empty", "VEHICLE_MMCODE_EMPTY");

        public static ValidationErrorMessage PartyIdRequired = new ValidationErrorMessage(
            "AssetVehicle.PartyId",
            "Party Id can not be empty", "PARTY_ID_EMPTY");

        public AssetVehicleValidationMessages()
            : base(new[] {YearOfManufactureRequired, VehicleMakeRequired, VehicleModelRequired, VehicleMMCodeRequired})
        {
        }
    }
}