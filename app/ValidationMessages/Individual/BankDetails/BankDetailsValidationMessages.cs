using System.Collections.ObjectModel;

namespace ValidationMessages.Individual.AssetVehicle
{
    public class BankDetailsValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage BankRequired =
            new ValidationErrorMessage("BankDetails.Bank",
                " Bank can not be empty", "BANK_EMPTY");

        public static ValidationErrorMessage BankBranchRequired =
            new ValidationErrorMessage("BankDetails.BankBranch",
                " Bank Branch can not be empty", "BANK_BRANCH_EMPTY");

        public static ValidationErrorMessage BankBranchCodeRequired =
            new ValidationErrorMessage("BankDetails.BankBranchCode",
                " Bank Branch Code can not be empty", "BANK_BRANCH_CODE_EMPTY");

        public static ValidationErrorMessage AccountNoRequired =
            new ValidationErrorMessage("BankDetails.AccountNo",
                " Account No Code can not be empty", "ACCOUNT_NO_EMPTY");

        public static ValidationErrorMessage TypeAccount =
            new ValidationErrorMessage("BankDetails.TypeAccount",
                " Type Account can not be empty", "TYPE_ACCOUNT_EMPTY");

        public BankDetailsValidationMessages()
            : base(new[] {BankRequired, BankBranchRequired, BankBranchCodeRequired, AccountNoRequired, TypeAccount})
        {
        }
    }
}