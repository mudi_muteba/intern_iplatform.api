using System.Collections.ObjectModel;

namespace ValidationMessages.Individual
{
    public class PartyValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
         public static ValidationErrorMessage InvalidId = new ValidationErrorMessage("Party.Id",
            "Invalid Party Id {0}", "PARTY_INVALID_ID");

         public PartyValidationMessages()
            : base(
                new[]
                {
                    InvalidId
                })
        {
        }
    }
}