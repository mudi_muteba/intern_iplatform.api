using System.Collections.ObjectModel;

namespace ValidationMessages.Individual
{
    public class IndividualValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public IndividualValidationMessages()
            : base(new[]
            {
                DuplicateIdNumber(),
                InvalidSearchCriteria,
            })
        {
            
        }



        public static ValidationErrorMessage InvalidSearchCriteria = new ValidationErrorMessage("Search",
            "Invalid search criteria", "INDIVIDUAL_INVALID_SEARCH_CRITERIA");

        public static ValidationErrorMessage DuplicateIdNumber()
        {
            return new ValidationErrorMessage("IdNumber", "Individual with Id Number '{0}'",
                "INDIVIDUAL_IDNUMBER_DUPLICATE");
        }

        public static ValidationErrorMessage DuplicateEmail()
        {
            return new ValidationErrorMessage("Email", "Individual with Email address '{0}'",
                "INDIVIDUAL_EMAIL_DUPLICATE");
        }

        public static ValidationErrorMessage UnknownIndividual()
        {
            return new ValidationErrorMessage("Individual", "{0}",
                "INDIVIDUAL_NOT_FOUND");
        }
    }
 }