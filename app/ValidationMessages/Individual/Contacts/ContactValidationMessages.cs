using System.Collections.ObjectModel;

namespace ValidationMessages.Individual.Contacts
{

    public class ContactValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdentityNoRequired = new ValidationErrorMessage("Contact.IdentityNo",
            "Identity No can not be empty", "CONTACT_IDENTITY_NO_EMPTY");

        public static ValidationErrorMessage RelationshipTypeRequired = new ValidationErrorMessage("Contact.RelationshipType",
            "Relationship type empty", "CONTACT_RELATIONSHIP_TYPE_EMPTY");


        public ContactValidationMessages() : base(new[] { RelationshipTypeRequired, IdentityNoRequired}) { }
    }
}