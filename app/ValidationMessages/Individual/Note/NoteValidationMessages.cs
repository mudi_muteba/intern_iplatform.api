using System.Collections.ObjectModel;

namespace ValidationMessages.Individual.Note
{

    public class NoteValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage NotesRequired = new ValidationErrorMessage("Note.Notes",
            "Notes can not be empty", "NOTES_EMPTY");

        public NoteValidationMessages() : base(new[] { NotesRequired }) { }
    }


}