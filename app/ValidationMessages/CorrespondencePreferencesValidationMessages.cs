using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class CorrespondencePreferencesValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {

        public static ValidationErrorMessage PartyIdInvalid = new ValidationErrorMessage("CorrespondencePreference.PartyId",
            "Invalid PartyId: {0}", "CORRESPONDENCE_PREFERENCES_PARTYID_INVALID");

        public CorrespondencePreferencesValidationMessages()
            : base(
                new[]
                {
                    PartyIdInvalid
                })
        {
        }
    }
}