using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class ProposalValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage ProposalIdRequired = new ValidationErrorMessage("GetRatingByProposalIdDto.Id",
            "Proposal Id cannot be empty", "GET_RATING_PROPOSAL_ID_EMPTY");

        public static ValidationErrorMessage ProposalIdInvalid = new ValidationErrorMessage("GetRatingByProposalIdDto.Id",
           "Proposal Id ({0}) is invalid ", "GET_RATING_PROPOSAL_ID_INVALID");

        public static ValidationErrorMessage ProposalDefinitionIdInvalid = new ValidationErrorMessage("GetDefinitionQuestionsDto.ProposalDefinitionId",
           "Proposal Definition Id ({0}) is invalid ", "GET_QUESTIONDEFITNITION_PROPOSALDEFINITIONID_INVALID");

        public static ValidationErrorMessage ProposalDefinitionIdEmpty = new ValidationErrorMessage("GetDefinitionQuestionsDto.ProposalDefinitionId",
           "Proposal Definition Id cannot be empty ", "GET_QUESTIONDEFITNITION_PROPOSALDEFINITIONID_EMPTY");
        public ProposalValidationMessages()
            : base(
                new[]
                {
                    ProposalIdRequired,
                    ProposalIdInvalid,
                    ProposalDefinitionIdInvalid,
                    ProposalDefinitionIdEmpty
                })
        {
        }
    }
}