using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class AdditionalMemberValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("AdditionalMemberDto.Id",
            "Id cannot be empty", "ADDITIONALMEMBER_ID_EMPTY");

        public static ValidationErrorMessage InitialRequired = new ValidationErrorMessage("CreateAdditionalMemberDto.Initial",
            "Initial cannot be empty", "ADDITIONALMEMBER_INITIAL_EMPTY");

        public static ValidationErrorMessage SurnameRequired = new ValidationErrorMessage("CreateAdditionalMemberDto.Surname",
            "Surname cannot be empty", "ADDITIONALMEMBER_SURNAME_EMPTY");

        public static ValidationErrorMessage IdNumberRequired = new ValidationErrorMessage("CreateAdditionalMemberDto.IdNumber",
            "IdNumber cannot be empty", "ADDITIONALMEMBER_IDNUMBER_EMPTY");

        public static ValidationErrorMessage MemberRelationshipIdRequired = new ValidationErrorMessage("CreateAdditionalMemberDto.MemberRelationshipId",
            "MemberRelationshipId cannot be empty", "ADDITIONALMEMBER_MEMBERRELATIONSHIPID_EMPTY");

        public static ValidationErrorMessage ProposalDefinitionIdRequired = new ValidationErrorMessage("CreateAdditionalMemberDto.ProposalDefinitionId",
           "ProposalDefinitionId cannot be empty", "ADDITIONALMEMBER_PROPOSALDEFINITIONID_EMPTY");

        public static ValidationErrorMessage SumInsuredRequired = new ValidationErrorMessage("CreateAdditionalMemberDto.SumInsured",
            "SumInsured cannot be empty", "ADDITIONALMEMBER_SUMINSURED_EMPTY");

        public static ValidationErrorMessage DateOnCoverInvalid = new ValidationErrorMessage("CreateAdditionalMemberDto.DateOnCover",
            "DateOnCover cannot be empty.", "ADDITIONALMEMBER_DATEONCOVER_INVALID");

        public AdditionalMemberValidationMessages()
            : base(
                new[]
                {
                    InitialRequired,
                    SurnameRequired,
                    IdNumberRequired,
                    MemberRelationshipIdRequired,
                    ProposalDefinitionIdRequired,
                    SumInsuredRequired,
                    DateOnCoverInvalid,
                    IdRequired
                })
        {
        }
    }
}