using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class HomeLossHistoryValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("HomeLossHistoryDto.Id",
            "Id cannot be empty", "HOMELOSSHISTORY_ID_EMPTY");

        public static ValidationErrorMessage HomeTypeOfLossIdInvalid = new ValidationErrorMessage("HomeLossHistoryDto.HomeTypeOfLossId",
            "Invalid HomeTypeOfLossId: {0}", "HOMELOSSHISTORY_HOMETYPEOFLOSSID_INVALID");

        public static ValidationErrorMessage HomeClaimAmountIdInvalid = new ValidationErrorMessage("HomeLossHistoryDto.HomeClaimAmountId",
            "Invalid HomeClaimAmountId: {0}", "HOMELOSSHISTORY_HOMECLAIMAMOUNTID_INVALID");

        public static ValidationErrorMessage UninterruptedPolicyIdInvalid = new ValidationErrorMessage("HomeLossHistoryDto.UninterruptedPolicyId",
            "Invalid UninterruptedPolicyId: {0}", "HOMELOSSHISTORY_UNINTERRUPTEDPOLICYID_INVALID");

        public static ValidationErrorMessage HomeClaimLocationIdInvalid = new ValidationErrorMessage("HomeLossHistoryDto.HomeClaimLocationId",
            "Invalid HomeClaimLocationId: {0}", "HOMELOSSHISTORY_HOMECLAIMLOCATIONID_INVALID");

        public static ValidationErrorMessage CurrentlyInsuredIdInvalid = new ValidationErrorMessage("HomeLossHistoryDto.CurrentlyInsuredId",
            "Invalid CurrentlyInsuredId: {0}", "HOMELOSSHISTORY_CURRENTLYINSUREDID_INVALID");

        public static ValidationErrorMessage DateOfLossEmpty = new ValidationErrorMessage("HomeLossHistoryDto.DateOfLoss",
            "DateOfLoss cannot be empty", "HOMELOSSHISTORY_DATEOFLOSS_EMPTY");

        public static ValidationErrorMessage WhyEmpty = new ValidationErrorMessage("HomeLossHistoryDto.Why",
            "Why cannot be empty", "HOMELOSSHISTORY_WHY_EMPTY");

        public static ValidationErrorMessage PartyIdInvalid = new ValidationErrorMessage("HomeLossHistoryDto.PartyId",
            "Invalid PartyId: {0}", "HOMELOSSHISTORY_PARTYID_INVALID");

        public HomeLossHistoryValidationMessages()
            : base(
                new[]
                {
                    IdRequired,
                    HomeTypeOfLossIdInvalid,
                    HomeClaimAmountIdInvalid,
                    UninterruptedPolicyIdInvalid,
                    HomeClaimLocationIdInvalid,
                    CurrentlyInsuredIdInvalid,
                    DateOfLossEmpty,
                    WhyEmpty
                })
        {
        }
    }
}