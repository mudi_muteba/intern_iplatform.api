﻿using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class FormBuilderDetailValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage FormBuilderIdRequired = new ValidationErrorMessage("CreateFormBuilderDetailDto.FormBuilderId",
            "FormBuilderId can not be empty", "FORMID_EMPTY");

        public static ValidationErrorMessage FormBuilderDetailIdRequired = new ValidationErrorMessage("CreateFormBuilderDetailDto.FormBuilderId",
           "FormBuilderDetailId can not be empty", "FORMBUILDERDETAILID_EMPTY");

        public static ValidationErrorMessage HtmlFormRequired = new ValidationErrorMessage("CreateFormBuilderDetailDto.HtmlForm",
            "HtmlForm can not be empty", "HTMLFORM_EMPTY");

        public static ValidationErrorMessage CommentRequired = new ValidationErrorMessage("CreateFormBuilderDetailDto.ChangeComment",
            "ChangeComment can not be empty", "COMMENT_EMPTY");

        public FormBuilderDetailValidationMessages()
            : base(
                new[]
                {
                    FormBuilderIdRequired, HtmlFormRequired, CommentRequired
                })
        {
        }
    }
}