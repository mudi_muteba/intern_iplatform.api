﻿using System.Collections.ObjectModel;

namespace ValidationMessages.SignFlow
{
    public class SignFlowValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage CorrespondUserRequired = new ValidationErrorMessage("SignFlow.CorrespondantUser",
            "CorrespondantUser can not be empty", "CORRESPONDANTUSER_EMPTY");

        public static ValidationErrorMessage PathOfDocumentRequired = new ValidationErrorMessage("SignFlow.PathOfDocument",
            "PathOfDocument can not be empty", "PATHOFDOCUMENT_EMPTY");

        public static ValidationErrorMessage MessageToUserRequired = new ValidationErrorMessage("SignFlow.MessageToUser",
            "MessageToUser can not be empty", "MESSAGETOUSER_EMPTY");

        public static ValidationErrorMessage NameDocUploadedRequired = new ValidationErrorMessage("SignFlow.NameDocUploaded",
            "NameDocUploaded can not be empty", "NAMEDOCUPLOADED_EMPTY");

        public static ValidationErrorMessage DueDateRequired = new ValidationErrorMessage("SignFlow.DueDate",
            "DueDate can not be empty", "DUEDATE_EMPTY");

        public static ValidationErrorMessage DueDatePassRequired = new ValidationErrorMessage("SignFlow.DueDate",
            "DueDate can not be in the past", "DUEDATE_PAST_DATE_NOT_ALLOWED");

        public SignFlowValidationMessages() : base(new[] { CorrespondUserRequired, PathOfDocumentRequired, MessageToUserRequired, NameDocUploadedRequired, DueDateRequired, DueDatePassRequired })
        {
        }
    }
}
