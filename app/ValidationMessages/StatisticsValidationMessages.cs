using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class StatisticsValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage AgentIdRequired = new ValidationErrorMessage("GetDayStatsDto.AgentId",
            "AgentId cannot be empty", "AGENT_ID_EMPTY");


        public StatisticsValidationMessages()
            : base(
                new[]
                {
                    AgentIdRequired,
                })
        {
        }
    }
}