using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class SettingsiRateUserValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("EditSettingsiRateUserDto.Id",
            "Id can not be empty", "SETTINGSIRATEUSER_ID_EMPTY");
   
        public static ValidationErrorMessage ProductIdRequired = new ValidationErrorMessage("CreateSettingsiRateUserDto.ProductId",
            "ProductId can not be empty", "SETTINGSIRATEUSER_PRODUCTID_EMPTY");

        public static ValidationErrorMessage IdRequiredForDelete = new ValidationErrorMessage("DisableSettingsiRateUserDto.Id",
            "Id can not be empty or zero", "SETTINGSIRATEUSER_ID_EMPTY");

        public static ValidationErrorMessage UserIdRequired = new ValidationErrorMessage("CreateSettingsiRateUserDto.UserId",
          "UserId can not be empty", "SETTINGSIRATEUSER_USERID_EMPTY");

        public SettingsiRateUserValidationMessages()
            : base(
                new[]
                {
                     ProductIdRequired, IdRequired,  IdRequiredForDelete, UserIdRequired
                })
        {
        }
    }
}