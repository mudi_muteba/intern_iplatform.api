﻿using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class UserChannelValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("CreateUserChannelDto.ChannelId",
            "ChannelId can not be empty", "USERCHANNEL_CHANNELID_EMPTY");

        public static ValidationErrorMessage UserIdRequired = new ValidationErrorMessage("CreateUserChannelDto.UserId",
            "UserId can not be empty", "USERCHANNEL_USERID_EMPTY");

        public static ValidationErrorMessage CombinationExist = new ValidationErrorMessage("CreateUserChannelDto.Id",
            "This combination alreay exist in user channel", "USERCHANNEL_COMBINATION_EXIST");

        public static ValidationErrorMessage UsersRequired = new ValidationErrorMessage("SaveUserChannelsDto.Users",
            "Users can not be empty. At least one user required", "USERCHANNEL_USER_EMPTY");

        public UserChannelValidationMessages(): base(new[] { ChannelIdRequired, UserIdRequired, CombinationExist })
        {
        }
    }
}