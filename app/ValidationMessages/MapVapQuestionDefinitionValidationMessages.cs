using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class MapVapQuestionDefinitionValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("MapVapQuestionDefinitionValidation.Id",
            "Id can not be empty", "MAPVAPQUESTIONDEFITNION_ID_EMPTY");

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("MapVapQuestionDefinitionValidation.ChannelId",
            "ChannelId can not be empty", "MAPVAPQUESTIONDEFITNION_CHANNELID_EMPTY");

        public static ValidationErrorMessage ProductIdRequired = new ValidationErrorMessage("MapVapQuestionDefinitionValidation.ProductId",
            "ProductId can not be empty", "MAPVAPQUESTIONDEFITNION_PRODUCTID_EMPTY");

        public static ValidationErrorMessage QuestionIdRequired = new ValidationErrorMessage("MapVapQuestionDefinitionValidation.QuestionId",
            "QuestionfinitionId can not be empty", "MAPVAPQUESTIONDEFITNION_QUESTIONID_EMPTY");

        public static ValidationErrorMessage IdInvalid = new ValidationErrorMessage("MapVapQuestionDefinitionValidation.Id",
            "Id {0} does not exist", "MAPVAPQUESTIONDEFITNION_ID_INVALID");

        public static ValidationErrorMessage ChannelIdInvalid = new ValidationErrorMessage("MapVapQuestionDefinitionValidation.ChannelId",
            "ChannelId {0} does not exist", "MAPVAPQUESTIONDEFITNION_CHANNELID_INVALID");

        public static ValidationErrorMessage ProductIdInvalid = new ValidationErrorMessage("MapVapQuestionDefinitionValidation.ProductId",
            "ProductId {0} does not exist", "MAPVAPQUESTIONDEFITNION_PRODUCTID_INVALID");

        public static ValidationErrorMessage QuestionIdInvalid = new ValidationErrorMessage("MapVapQuestionDefinitionValidation.QuestionId",
            "QuestionfinitionId {0} does not exist", "MAPVAPQUESTIONDEFITNION_QUESTIONID_INVALID");

        public static ValidationErrorMessage AnnualizedMultiplierInvalid = new ValidationErrorMessage("MapVapQuestionDefinitionValidation.AnnualizedMultiplier",
            "AnnualizedMultiplier {0} does not does not fall within the specified time range", "MAPVAPQUESTIONDEFITNION_ANNUALIZEDMULTIPLIER_INVALID");


        public MapVapQuestionDefinitionValidationMessages()
            : base(
                new[]
                {
                    IdRequired, ChannelIdRequired, ProductIdRequired, QuestionIdRequired, IdInvalid, ChannelIdInvalid, ProductIdInvalid, QuestionIdInvalid, AnnualizedMultiplierInvalid
                })
        {
        }
    }
}