using System;
using System.Collections.Generic;
using System.Linq;
using ValidationMessages.Campaigns;
using ValidationMessages.Individual;
using ValidationMessages.Individual.Address;
using ValidationMessages.Individual.AssetVehicle;
using ValidationMessages.Individual.Contacts;
using ValidationMessages.Individual.Note;
using ValidationMessages.ProposalQuestionDefinition;
using ValidationMessages.Users.Discounts;

namespace ValidationMessages
{
    public static class ValidationMessageCollection
    {
        private static readonly List<ValidationErrorMessage> _collection;


        static ValidationMessageCollection()
        {
            if (_collection == null)
                _collection = new List<ValidationErrorMessage>();

            _collection.AddRange(new NoteValidationMessages());
            _collection.AddRange(new CampaignValidationMessages());
            _collection.AddRange(new IndividualValidationMessages());
            _collection.AddRange(new AddressValidationMessages());
            _collection.AddRange(new AssetVehicleValidationMessages());
            _collection.AddRange(new BankDetailsValidationMessages());
            _collection.AddRange(new ContactValidationMessages());
            _collection.AddRange(new DiscountValidationMessages());
            _collection.AddRange(new ProposalDefinitionValidationMessages());

        }

        public static ValidationErrorMessage FindByKey(string key)
        {
            ValidationErrorMessage message = _collection.FirstOrDefault(a => a.MessageKey.Equals(key));

            if (message == null)
                throw new ApplicationException(string.Format("Validationv message key not found{0}", key));

            return message;
        }
    }
}