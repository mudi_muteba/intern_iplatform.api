using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class CustomerSatisfactionSurveyValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {

        public static ValidationErrorMessage PartyIdEmpty = new ValidationErrorMessage("CreateCustomerSatisfactionSurveyDto.PartyId",
            "PartyId cannot be empty", "CUSTOMERSATISFACTIONSURVEY_PARTYID_EMPTY");

        public static ValidationErrorMessage EventEmpty = new ValidationErrorMessage("CreateCustomerSatisfactionSurveyDto.Event",
            "Event cannot be empty", "CUSTOMERSATISFACTIONSURVEY_EVENT_EMPTY");

        public static ValidationErrorMessage AnswersEmpty = new ValidationErrorMessage("CreateCustomerSatisfactionSurveyDto.CustomerSatisfactionSurveyAnswers",
            "Answers cannot be empty", "CUSTOMERSATISFACTIONSURVEY_ANSWERS_EMPTY");

        public static ValidationErrorMessage NameEmpty = new ValidationErrorMessage("CreateCustomerSatisfactionSurveyDto.Name",
            "Name cannot be empty", "CUSTOMERSATISFACTIONSURVEY_NAME_EMPTY");

        public static ValidationErrorMessage CellNumberEmpty = new ValidationErrorMessage("CreateCustomerSatisfactionSurveyDto.CellNumber",
            "CellNumber cannot be empty", "CUSTOMERSATISFACTIONSURVEY_CELLNUMBER_EMPTY");

        public static ValidationErrorMessage EmailAddressEmpty = new ValidationErrorMessage("CreateCustomerSatisfactionSurveyDto.EmailAddress",
            "EmailAddress cannot be empty", "CUSTOMERSATISFACTIONSURVEY_EMAILADDRESS_EMPTY");

        public static ValidationErrorMessage InvalidCellNumberOrEmailAddress = new ValidationErrorMessage("CreateCustomerSatisfactionSurveyDto.CellNumber.Emailaddress",
            "Invalid Emailaddress '{0}' and CellNumber '{1}'", "CUSTOMERSATISFACTIONSURVEY_EMAILADDRESS_CELLNUMBER_INVALID");

        public CustomerSatisfactionSurveyValidationMessages()
            : base(
                new[]
                {
                    PartyIdEmpty,
                    EventEmpty,
                    AnswersEmpty,
                    NameEmpty,
                    CellNumberEmpty,
                    EmailAddressEmpty,
                    InvalidCellNumberOrEmailAddress
                })
        {
        }
    }
}