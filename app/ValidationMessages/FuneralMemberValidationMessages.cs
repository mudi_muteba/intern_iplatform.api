using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class FuneralMemberValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("FuneralMemberDto.Id",
            "Id cannot be empty", "FUNERALMEMBER_ID_EMPTY");

        public static ValidationErrorMessage InitialRequired = new ValidationErrorMessage("CreateFuneralMemberDto.Initial",
            "Initial cannot be empty", "FUNERALMEMBER_INITIAL_EMPTY");

        public static ValidationErrorMessage SurnameRequired = new ValidationErrorMessage("CreateFuneralMemberDto.Surname",
            "Surname cannot be empty", "FUNERALMEMBER_SURNAME_EMPTY");

        public static ValidationErrorMessage IdNumberRequired = new ValidationErrorMessage("CreateFuneralMemberDto.IdNumber",
            "IdNumber cannot be empty", "FUNERALMEMBER_IDNUMBER_EMPTY");

        public static ValidationErrorMessage MemberRelationshipIdRequired = new ValidationErrorMessage("CreateFuneralMemberDto.MemberRelationshipId",
            "MemberRelationshipId cannot be empty", "FUNERALMEMBER_MEMBERRELATIONSHIPID_EMPTY");

        public static ValidationErrorMessage ProposalDefinitionIdRequired = new ValidationErrorMessage("CreateFuneralMemberDto.ProposalDefinitionId",
           "ProposalDefinitionId cannot be empty", "FUNERALMEMBER_PROPOSALDEFINITIONID_EMPTY");

        public static ValidationErrorMessage SumInsuredRequired = new ValidationErrorMessage("CreateFuneralMemberDto.SumInsured",
            "SumInsured cannot be empty", "FUNERALMEMBER_SUMINSURED_EMPTY");

        public static ValidationErrorMessage DateOnCoverInvalid = new ValidationErrorMessage("CreateFuneralMemberDto.DateOnCover",
            "DateOnCover cannot be empty.", "FUNERALMEMBER_DATEONCOVER_INVALID");

        public FuneralMemberValidationMessages()
            : base(
                new[]
                {
                    InitialRequired,
                    SurnameRequired,
                    IdNumberRequired,
                    MemberRelationshipIdRequired,
                    ProposalDefinitionIdRequired,
                    SumInsuredRequired,
                    DateOnCoverInvalid,
                    IdRequired
                })
        {
        }
    }
}