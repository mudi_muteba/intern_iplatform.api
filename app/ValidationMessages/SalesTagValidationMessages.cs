using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class SalesTagValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("Relationship.Id",
            "Id cannot be empty", "RELATIONSHIP_ID_EMPTY");

        public static ValidationErrorMessage InvalidSearchCriteria = new ValidationErrorMessage("SalesTagSearchDto",
            "Invalid search criteria", "SALESTAG_INVALID_SEARCH_CRITERIA");

        public SalesTagValidationMessages()
            : base(
                new[]
                {
                    IdRequired,
                    InvalidSearchCriteria,
                })
        {
        }
    }
}