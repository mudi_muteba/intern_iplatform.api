using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class MotorLossHistoryValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("MotorLossHistoryDto.Id",
            "Id cannot be empty", "MOTORLOSSHISTORY_ID_EMPTY");

        public static ValidationErrorMessage MotorTypeOfLossIdInvalid = new ValidationErrorMessage("MotorLossHistoryDto.otorTypeOfLossId",
            "Invalid MotorTypeOfLossId: {0}", "MOTORLOSSHISTORY_MOTORTYPEOFLOSSID_INVALID");

        public static ValidationErrorMessage MotorClaimAmountIdInvalid = new ValidationErrorMessage("MotorLossHistoryDto.MotorClaimAmountId",
            "Invalid MotorClaimAmountId: {0}", "MOTORLOSSHISTORY_MOTORCLAIMAMOUNTID_INVALID");

        public static ValidationErrorMessage UninterruptedPolicyIdInvalid = new ValidationErrorMessage("MotorLossHistoryDto.UninterruptedPolicyId",
            "Invalid UninterruptedPolicyId: {0}", "MOTORLOSSHISTORY_MOTORUNINTERRUPTEDPOLICYID_INVALID");

        public static ValidationErrorMessage MotorCurrentTypeOfCoverIdInvalid = new ValidationErrorMessage("MotorLossHistoryDto.MotorCurrentTypeOfCoverIdRequired",
            "Invalid MotorUninterruptedPolicyId: {0}", "MOTORLOSSHISTORY_MOTORCURRENTTYPEOFCOVERID_INVALID");

        public static ValidationErrorMessage MotorUninterruptedPolicyNoClaimIdInvalid = new ValidationErrorMessage("MotorLossHistoryDto.MotorUninterruptedPolicyNoClaimId",
            "Invalid MotorUninterruptedPolicyNoClaimId: {0}", "MOTORLOSSHISTORY_MOTORUNINTERRUPTEDPOLICYNOCLAIM_INVALID");

        public static ValidationErrorMessage DateOfLossEmpty = new ValidationErrorMessage("MotorLossHistoryDto.DateOfLoss",
            "DateOfLoss cannot be empty", "MOTORLOSSHISTORY_DATEOFLOSS_EMPTY");

        public static ValidationErrorMessage WhyEmpty = new ValidationErrorMessage("MotorLossHistoryDto.Why",
            "Why cannot be empty", "MOTORLOSSHISTORY_WHY_EMPTY");

        public static ValidationErrorMessage PartyIdInvalid = new ValidationErrorMessage("MotorLossHistoryDto.PartyId",
            "Invalid PartyId: {0}", "MOTORLOSSHISTORY_PARTYID_INVALID");

        public MotorLossHistoryValidationMessages()
            : base(
                new[]
                {
                    IdRequired,
                    MotorTypeOfLossIdInvalid,
                    MotorClaimAmountIdInvalid,
                    UninterruptedPolicyIdInvalid,
                    MotorCurrentTypeOfCoverIdInvalid,
                    MotorUninterruptedPolicyNoClaimIdInvalid,
                    DateOfLossEmpty,
                    WhyEmpty
                })
        {
        }
    }
}