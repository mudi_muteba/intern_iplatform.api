using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class QuoteValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage QuoteIdRequired = new ValidationErrorMessage("IntentToBuyQuoteDto.Id",
            "Quote Id cannot be empty", "QUOTE_ID_EMPTY");

        public static ValidationErrorMessage QuoteIdEmpty = new ValidationErrorMessage("QuoteUploadSuccessDto.QuoteId",
            "Quote Id cannot be empty", "QUOTE_ID_EMPTY");

        public static ValidationErrorMessage ProductIdRequired = new ValidationErrorMessage("QuoteUploadSuccessDto.ProductId",
            "Product Id cannot be empty", "PRODUCT_ID_EMPTY");

        public QuoteValidationMessages()
            : base(
                new[]
                {
                    QuoteIdRequired,
                    ProductIdRequired
                })
        {
        }
    }
}