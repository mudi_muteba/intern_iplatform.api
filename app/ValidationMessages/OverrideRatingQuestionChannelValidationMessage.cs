﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationMessages
{
    public class OverrideRatingQuestionChannelValidationMessage : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("OverrideRatingQuestionChannelDto.Id",
            "Id can not be empty", "OVERRIDERATINGQUESTIONCHANNEL_ID_EMPTY");

        public static ValidationErrorMessage OverrideRatingQuestionIdRequired = new ValidationErrorMessage("OverrideRatingQuestionChannelDto.OverrideRatingQuestionId",
            "OverrideRatingQuestionId can not be empty", "OVERRIDERATINGQUESTIONCHANNEL_OVERRIDERATINGQUESTIONID_EMPTY");

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("OverrideRatingQuestionChannelDto.ChannelId",
            "ChannelId can not be empty", "OVERRIDERATINGQUESTIONCHANNEL_CHANNELID_EMPTY");

        public OverrideRatingQuestionChannelValidationMessage() : base(new []
        {
            IdRequired,
            OverrideRatingQuestionIdRequired,
            ChannelIdRequired
        })
        {
        }
    }
}
