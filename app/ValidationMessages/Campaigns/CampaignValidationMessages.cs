using System.Collections.ObjectModel;

namespace ValidationMessages.Campaigns
{
    public class CampaignValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage CampaignIdRequired = new ValidationErrorMessage("Campaign.Id",
            "Id can not be empty", "CAMPAIGN_ID_EMPTY");

        public static ValidationErrorMessage CampaignNameRequired = new ValidationErrorMessage("Campaign.Name",
            "Name can not be empty", "CAMPAIGN_NAME_EMPTY");

        public static ValidationErrorMessage StartDateNotProvided = new ValidationErrorMessage("Campaign.StartDate",
            "Start date must be provided", "CAMPAIGN_START_DATE_EMPTY");

        public static ValidationErrorMessage StartDateAfterEndDate = new ValidationErrorMessage("Campaign.StartDate",
            "Start date must be before end date", "CAMPAIGN_START_DATE_BEFORE_END_DATE");

        public static ValidationErrorMessage UnknownCampaignToUpdate = new ValidationErrorMessage("Campaign.Id",
            "Unknown campaign to update", "CAMPAIGN_UNKNOWN");

        public static ValidationErrorMessage UnknownCampaign = new ValidationErrorMessage("Campaign.Id",
            "Unknown campaign id {0}", "CAMPAIGN_ID_UNKNOWN");

        public static ValidationErrorMessage InvalidSearchCriteria = new ValidationErrorMessage("Search",
            "Invalid search criteria", "CAMPAIGN_INVALID_SEARCH_CRITERIA");

        public static ValidationErrorMessage DuplicateCampaignName = new ValidationErrorMessage("Name",
            "Campaign with name '{0}'",
            "CAMPAIGN_NAME_DUPLICATE");

        public static ValidationErrorMessage InvalidProductId = new ValidationErrorMessage("Campaign.Products.Id",
            "Invalid Product with Id '{0}'",
            "CAMPAIGN_PRODUCT_ID_INVALID");

        public static ValidationErrorMessage InvalidTagId = new ValidationErrorMessage("Campaign.Tags.Id",
            "Invalid Tag with Id '{0}'",
            "CAMPAIGN_TAG_ID_INVALID");

        public static ValidationErrorMessage InvalidAgentId = new ValidationErrorMessage("Campaign.Agents.Id",
            "Invalid Agent with Id '{0}'",
            "CAMPAIGN_AGENT_ID_INVALID");

        public static ValidationErrorMessage InvalidCampaignReference = new ValidationErrorMessage("Campaign.References.Name",
            "Invalid Reference '{0}'",
            "CAMPAIGN_Reference_INVALID");

        public static ValidationErrorMessage UnknownCampaignReference = new ValidationErrorMessage("Campaign.Reference",
            "Unknown campaign reference {0}", "CAMPAIGN_REFERENCE_UNKNOWN");

        public static ValidationErrorMessage LeadBucketEmpty = new ValidationErrorMessage("AssignLeadToCampaignDto.Leads",
            "Lead List cannot be empty", "CAMPAIGNLEADBUCKET_LEADS_EMPTY");

        public static ValidationErrorMessage NoLeadInBucket = new ValidationErrorMessage("GetNextLeadDto",
            "No Leads available in Bucket", "CAMPAIGNLEADBUCKET_NO_LEADS");

        public static ValidationErrorMessage MaximumLeadsExceeded = new ValidationErrorMessage("GetNextLeadDto",
            "Maximum Leads Exceeded", "CAMPAIGNLEADBUCKET_MAXIMUM_LEADS_EXCEEDED");

        public static ValidationErrorMessage NextLead = new ValidationErrorMessage("GetNextLeadDto",
            "Next Lead Retrieved Successfully", "CAMPAIGNLEADBUCKET_NEXT_LEADS");

        public static ValidationErrorMessage UpdatePendingLead = new ValidationErrorMessage("GetSeritiImportNextLeadDto",
          "Please Finalise Your Lead Assigned", "Update_Lead_Assigned");

        public static ValidationErrorMessage NoSeritiLeadsImported = new ValidationErrorMessage("GetSeritiImportNextLeadDto",
          "No Leads Found", "CAMPAIGNLEADBUCKET_No_Seriti_NEXT_LEADS");
        public static ValidationErrorMessage StartdateShouldBeGreaterNow = new ValidationErrorMessage("Campaign.StartDate",
            "Startdate cannot be earlier than today", "CAMPAIGNLEADBUCKET_NEXT_LEADS");

        public CampaignValidationMessages()
            : base(
                new[]
                {
                    CampaignNameRequired, StartDateAfterEndDate, UnknownCampaignToUpdate, InvalidSearchCriteria,
                    DuplicateCampaignName, CampaignIdRequired,InvalidProductId, InvalidTagId, InvalidAgentId, InvalidCampaignReference,LeadBucketEmpty,NoLeadInBucket,MaximumLeadsExceeded
                })
        {
        }
    }
}