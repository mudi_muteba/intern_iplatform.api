﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationMessages
{
    public class MapVapQuestionDefinitionCoverHeaderValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("MapVapQuestionDefinitionCoverHeaderDto.Id",
            "Id can not be empty", "MAPVAPQUESTIONDEFINITIONCOVERHEADER_ID_EMPTY");

        public static ValidationErrorMessage MapVapQuestionDefinitionIdRequired = new ValidationErrorMessage("MapVapQuestionDefinitionCoverHeaderDto.MapVapQuestionDefinitionId",
            "MapVapQuestionDefinitionId can not be empty", "MAPVAPQUESTIONDEFINITIONCOVERHEADER_MAPVAPQUESTIONDEFINITIONID_EMPTY");

        public MapVapQuestionDefinitionCoverHeaderValidationMessages() : base(new []
        {
            IdRequired,
            MapVapQuestionDefinitionIdRequired,
        })
        {
        }
    }
}
