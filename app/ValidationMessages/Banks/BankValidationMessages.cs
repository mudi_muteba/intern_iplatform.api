﻿namespace ValidationMessages.Banks
{
    public static class BankValidationMessages
    {
        public static ValidationErrorMessage InvalidBranchCode =
            new ValidationErrorMessage("Bank", "Invalid Branch Code", "INVALID_BRANCH_CODE");

        public static ValidationErrorMessage InvalidAccountNumber =
            new ValidationErrorMessage("Bank", "Invalid Account Number", "INVALID_ACCOUNT_NUMBER");

        public static ValidationErrorMessage InvalidAccountType =
            new ValidationErrorMessage("Bank", "Invalid Account Type", "INVALID_ACCOUNT_TYPE");

        public static ValidationErrorMessage InvalidLookupPath =
            new ValidationErrorMessage("Bank", "Invalid Lookup Path", "INVALID_LOOKUP_PATH");
    }
}