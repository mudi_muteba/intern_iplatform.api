using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class SalesValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("CreateSalesStructuresDto.ChannelId",
            "ChannelId cannot be empty", "SALESTRUCTURE_CHANNELID_EMPTY");

        public static ValidationErrorMessage SaleStructuresRequired = new ValidationErrorMessage("CreateSalesStructuresDto.SaleStructures",
            "SaleStructures cannot be empty", "SALESTRUCTURE_SALESSTRUCTURES_EMPTY");

        public static ValidationErrorMessage SaleStructureChannelIdRequired = new ValidationErrorMessage("CreateSalesStructuresDto.SaleStructures.ChannelId",
            "ChannelId cannot be empty for SalesStructure '{0}'", "SALESTRUCTURE_SALESSTRUCTURE_CHANNELID_EMPTY");

        public static ValidationErrorMessage SaleStructureParentSalesTagIdRequired = new ValidationErrorMessage("CreateSalesStructuresDto.SaleStructures.ParentSalesTagId",
            "ParentSalesTagId cannot be empty for SalesStructure '{0}'", "SALESTRUCTURE_SALESSTRUCTURE_PARENTSALESTAGID_EMPTY");

        public static ValidationErrorMessage SaleStructureGroupSalesTagIdRequired = new ValidationErrorMessage("CreateSalesStructuresDto.SaleStructures.GroupSalesTagId",
            "GroupSalesTagId cannot be empty for SalesStructure '{0}'", "SALESTRUCTURE_SALESSTRUCTURE_GROUPSALESTAGID_EMPTY");

        public static ValidationErrorMessage SaleStructureSalesTagIdRequired = new ValidationErrorMessage("CreateSalesStructuresDto.SaleStructures.SalesTagId",
            "SalesTagId cannot be empty for SalesStructure '{0}'", "SALESTRUCTURE_SALESSTRUCTURE_SALESTAGID_EMPTY");

        public static ValidationErrorMessage SaleStructureLevelRequired = new ValidationErrorMessage("CreateSalesStructuresDto.SaleStructures.Level",
            "Level cannot be empty for SalesStructure '{0}'", "SALESTRUCTURE_SALESSTRUCTURE_SALESTAGID_EMPTY");

        public SalesValidationMessages()
            : base(
                new[]
                {
                    ChannelIdRequired,
                    SaleStructuresRequired,
                    SaleStructureChannelIdRequired,
                    SaleStructureParentSalesTagIdRequired,
                    SaleStructureGroupSalesTagIdRequired,
                    SaleStructureSalesTagIdRequired,
                    SaleStructureLevelRequired
                })
        {
        }
    }
}