using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class LossHistoryValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("LossHistoryDto.Id",
            "Id cannot be empty", "LOSSHISTORY_ID_EMPTY");

        public static ValidationErrorMessage PartyIdInvalid = new ValidationErrorMessage("MotorLossHistoryDto.PartyId",
            "Invalid PartyId: {0}", "LOSSHISTORY_PARTYID_INVALID");

        public LossHistoryValidationMessages()
            : base(
                new[]
                {
                    IdRequired,
                    PartyIdInvalid
                })
        {
        }
    }
}