﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationMessages
{
    public class MapVapQuestionDefinitionCoverValidationMessages: ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("MapVapQuestionDefinitionCoverDto.Id",
            "Id can not be empty", "MAPVAPQUESTIONDEFINITIONCOVER_ID_EMPTY");

        public static ValidationErrorMessage MapVapQuestionDefinitionCoverHeaderIdRequired = new ValidationErrorMessage("MapVapQuestionDefinitionCoverDto.MapVapQuestionDefinitionId",
            "MapVapQuestionDefinitionCoverHeaderId can not be empty", "MAPVAPQUESTIONDEFINITIONCOVER_MAPVAPQUESTIONDEFINITIONCOVERHEADERID_EMPTY");

        public static ValidationErrorMessage CoverIdRequired = new ValidationErrorMessage("MapVapQuestionDefinitionCoverDto.CoverId",
            "CoverId can not be empty", "MAPVAPQUESTIONDEFINITIONCOVER_COVERID_EMPTY");
        public MapVapQuestionDefinitionCoverValidationMessages() : base(new []
        {
            IdRequired,
            MapVapQuestionDefinitionCoverHeaderIdRequired,
            CoverIdRequired
        })
        {
        }
    }
}
