using System.Collections.ObjectModel;


namespace ValidationMessages.Channels
{
    public class ChannelValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage NoChannelEventConfiguration(string eventName)
        {
            return new ValidationErrorMessage("Channel",
                string.Format("No event configuration in channel for '{0}'", eventName),
                "CHANNEL_CONFIGURATION_MISSING");
        }

        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("CreateChannelDto.Id",
            "Id cannot be empty", "CHANNEL_ID_EMPTY");

        public static ValidationErrorMessage CurrencyIdRequired = new ValidationErrorMessage("CreateChannelDto.CurrencyId",
            "Currency Id cannot be empty", "CHANNEL_CURRENCYID_EMPTY");

        public static ValidationErrorMessage DateFormatRequired = new ValidationErrorMessage("CreateChannelDto.DateFormat",
            "DateFormat cannot be empty", "CHANNEL_DATEFORMAT_EMPTY");

        public static ValidationErrorMessage LanguageIdRequired = new ValidationErrorMessage("CreateChannelDto.LanguageId",
            "Language Id cannot be empty", "CHANNEL_LANGUAGEID_EMPTY");

        public static ValidationErrorMessage ChannelNameRequired = new ValidationErrorMessage("CreateChannelDto.Name",
            "Channel Name cannot be empty", "CHANNEL_NAME_EMPTY");

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("CreateChannelPermissionDto.ChannelId",
            "Channel Id cannot be empty", "CHANNELPERMISSION_CHANNELID_EMPTY");

        public static ValidationErrorMessage ExistingChannelPermission = new ValidationErrorMessage("CreateChannelPermissionDto",
            "A Channel Permission already exist with the details provided.", "CHANNEL_PERMISSION_EXIST_ALREADY");

        public static ValidationErrorMessage ProductCodeRequired = new ValidationErrorMessage("CreateChannelPermissionDto.ProductCode",
            "ProductCode cannot be empty", "CHANNELPERMISSION_PRODUCTCODE_EMPTY");

        public static ValidationErrorMessage InsurerCodeRequired = new ValidationErrorMessage("CreateChannelPermissionDto.InsurerCode",
            "InsurerCode cannot be empty", "CHANNELPERMISSION_INSURERCODE_EMPTY");

        public static ValidationErrorMessage ChannelIdInvalid = new ValidationErrorMessage("ChannelPermissionSearchDto.ChannelId",
            "ChannelId is invalid", "CHANNEL_ID_INVALID");

        public static ValidationErrorMessage ProductInsurerCodeInvalid = new ValidationErrorMessage("CreateChannelPermissionDto.ProductInsurerCode",
            "Product with ProductCode ({0}) and InsurerCode ({1}) was not found.", "CHANNEL_PERMISSION_EXIST_ALREADY");

        public static ValidationErrorMessage DuplicateId = new ValidationErrorMessage("CreateChannelDto.Id",
            "Id already exist : {0}", "CHANNEL_ID_DUPLICATE");

        public static ValidationErrorMessage DuplicateSystemId = new ValidationErrorMessage("CreateChannelDto.SystemId",
            "SystemId already exist : {0}", "CHANNEL_SYSTEMID_DUPLICATE");

        public static ValidationErrorMessage CannotUpdateSystemId = new ValidationErrorMessage("EditChannelDto.SystemId",
            "SystemId must not be updated : {0}", "CHANNEL_SYSTEMID_CANNOT_UPDATE");

        public static ValidationErrorMessage DuplicateCode = new ValidationErrorMessage("CreateChannelDto.Code",
            "Code already exist : {0}", "CHANNEL_CODE_DUPLICATE");

        public static ValidationErrorMessage DuplicateExternalReference = new ValidationErrorMessage("CreateChannelDto.ExternalReference",
            "ExternalReference already exist : {0}", "CHANNEL_EXTERNALREFERENCE_DUPLICATE");

        public static ValidationErrorMessage SystemIdNotExist = new ValidationErrorMessage("CreateSettingsiRateDto.ChannelId",
           "System Id does not exist : {0}", "CHANNEL_SYSTEM_ID_DOES_NOT_EXIST");

        public ChannelValidationMessages()
            : base(
                new[]
                {
                    IdRequired,
                    CurrencyIdRequired,
                    DateFormatRequired,
                    LanguageIdRequired,
                    ChannelNameRequired,
                    ChannelIdRequired,
                    ProductCodeRequired,
                    InsurerCodeRequired,
                    ProductInsurerCodeInvalid,
                    ExistingChannelPermission,
                    SystemIdNotExist,
                    DuplicateExternalReference
                })
        {
        }
    }
}