using System.Collections.ObjectModel;

namespace ValidationMessages.Teams
{
    public class TeamValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage NameRequired = new ValidationErrorMessage("CreateTeamDto.Name",
            "Name cannot be empty", "TEAM_NAME_EMPTY");

        public static ValidationErrorMessage InvalidUserId = new ValidationErrorMessage("CreateTeamDto.Users.Id",
            "Invalid User Id : {0}", "TEAM_USERID_INVALID");

        public static ValidationErrorMessage InvalidCampaignId = new ValidationErrorMessage("CreateTeamDto.Campaigns.Id",
            "Invalid Campaign Id : {0}", "TEAM_CAMPAIGNID_INVALID");

        public static ValidationErrorMessage DuplicateName = new ValidationErrorMessage("CreateTeamDto.Name",
            "Name already exist : {0}", "TEAM_NAME_DUPLICATE");

        public static ValidationErrorMessage InvalidSearchCriteria = new ValidationErrorMessage("SearchTeamDto",
            "No valid search criteria", "TEAM_SEARCH_CRITERIA_INVALID");

        public static ValidationErrorMessage InvalidTeamId = new ValidationErrorMessage("EditTeamDto.Id",
            "Invalid team id. Id cannot be empty.", "TEAMID_INVALID");

        public TeamValidationMessages()
            : base(
                new[]
                {
                    NameRequired,
                    InvalidUserId,
                    InvalidCampaignId,
                    InvalidSearchCriteria,
                    InvalidTeamId
                })
        {
        }
    }
}