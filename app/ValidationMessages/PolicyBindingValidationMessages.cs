using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class PolicyBindingValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("PolicyBindingDto.Id",
            "Id cannot be empty", "POLICYBINDING_ID_EMPTY");

        public static ValidationErrorMessage ProductIdRequired = new ValidationErrorMessage("PolicyBindingDto.ProductId",
            "ProductId cannot be empty", "POLICYBINDING_PRODUCTID_REQUIRED");

        public static ValidationErrorMessage CoverIdRequired = new ValidationErrorMessage("PolicyBindingDto.CoverId",
            "CoverId cannot be empty", "POLICYBINDING_COVERID_REQUIRED");

        public static ValidationErrorMessage NameRequired = new ValidationErrorMessage("PolicyBindingDto.Name",
            "Name cannot be empty", "POLICYBINDING_NAME_REQUIRED");

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("PolicyBindingDto.ChannelId",
            "ChannelId cannot be empty", "POLICYBINDING_CHANNELID_REQUIRED");

        public static ValidationErrorMessage QuoteIdRequired = new ValidationErrorMessage("GetPolicyBindingByQuoteDto.QuoteId",
            "QuoteId cannot be empty", "POLICYBINDING_QUOTEID_REQUIRED");
        public static ValidationErrorMessage QuoteItemIdRequired = new ValidationErrorMessage("GetPolicyBindingByQuoteDto.QuoteId",
            "QuoteItemId cannot be empty", "POLICYBINDING_QUOTEITEMID_REQUIRED");
        public static ValidationErrorMessage InvalidProductId = new ValidationErrorMessage("PolicyBindingDto.ProductId",
            "Invalid ProductId: {0}", "POLICYBINDING_PRODUCTID_INVALID");

        public static ValidationErrorMessage InvalidCoverId = new ValidationErrorMessage("PolicyBindingDto.CoverId",
            "Invalid CoverId: {0}", "POLICYBINDING_COVERID_INVALID");

        public static ValidationErrorMessage InvalidId = new ValidationErrorMessage("PolicyBindingDto.Id",
            "Invalid Id: {0}", "POLICYBINDING_ID_INVALID");

        public static ValidationErrorMessage InvalidChannelId = new ValidationErrorMessage("PolicyBindingDto.ChannelId",
            "Invalid ChannelId: {0}", "POLICYBINDING_CHANNELID_INVALID");

        public static ValidationErrorMessage NoCoverDefinition = new ValidationErrorMessage("PolicyBindingDto.CoverDefinitionId",
            "No CoverDefinition exists with CoverId: {0} ProductId: {1}", "POLICYBINDING_COVERDEFINTIONID_INVALID");

        public static ValidationErrorMessage EmptyAnswers = new ValidationErrorMessage("SavePolicyBindingAnswerDto.Answers",
           "PolicyBindingAnswers cannot be empty.", "POLICYBINDING_ANSWERS_EMPTY");

        public static ValidationErrorMessage CampaignIdRequired = new ValidationErrorMessage("SubmitPolicyBindingDto.CampaignId",
            "CampaignId cannot be empty", "POLICYBINDING_CAMPAIGNID_REQUIRED");

        public static ValidationErrorMessage EmptyQuoteId = new ValidationErrorMessage("DeletePolicyBindingCompletedDto.QuoteId",
            "QuoteId cannot be empty", "DELETE_POLICYBINDINGCOMPLETED_EMPTY_QUOTEID");

        public static ValidationErrorMessage EmptyRequestId = new ValidationErrorMessage("DeletePolicyBindingCompletedDto.RequestId",
            "RequestId cannot be empty", "DELETE_POLICYBINDINGCOMPLETED_EMPTY_REQUESTID");

        public PolicyBindingValidationMessages()
            : base(
                new[]
                {
                    IdRequired,
                    ProductIdRequired,
                    CoverIdRequired,
                    NameRequired,
                    ChannelIdRequired,
                    InvalidProductId,
                    InvalidCoverId,
                    InvalidId,
                    InvalidChannelId,
                    NoCoverDefinition,
                    EmptyAnswers,
                    CampaignIdRequired,
                    EmptyQuoteId,
                    EmptyRequestId
                })
        {
        }
    }
}