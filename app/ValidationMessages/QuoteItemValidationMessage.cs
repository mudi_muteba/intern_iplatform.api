﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationMessages
{
    public class QuoteItemValidationMessage : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage UserIdRequired = new ValidationErrorMessage("OverrideRatingQuestionDto.UserId",
            "UserId can not be empty", "QUOTEITEM_USERID_EMPTY");

        public static ValidationErrorMessage QuoteIdRequired = new ValidationErrorMessage("OverrideRatingQuestionDto.QuoteId",
            "QuoteId can not be empty", "QUOTEITEM_QUOTEID_EMPTY");

        public static ValidationErrorMessage AssetNoRequired = new ValidationErrorMessage("OverrideRatingQuestionDto.AssetNo",
            "AssetNo can not be empty", "QUOTEITEM_ASSETNO_EMPTY");

        public static ValidationErrorMessage CoverDefinitionIdRequired = new ValidationErrorMessage("OverrideRatingQuestionDto.CoverDefinitionId",
            "CoverDefinitionId can not be empty", "QUOTEITEM_COVERDEFINITIONID_EMPTY");

        public QuoteItemValidationMessage() : base(new[]
        {
            UserIdRequired,
            QuoteIdRequired,
            AssetNoRequired,
            CoverDefinitionIdRequired
        })
        { }
    }
}
