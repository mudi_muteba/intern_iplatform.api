using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class InsurerValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("Insurer.Id",
            "Id can not be empty", "INSURER_ID_EMPTY");

        public static ValidationErrorMessage InvalidSearchCriteria = new ValidationErrorMessage("Insurer.Search",
            "Invalid search criteria", "INSURER_INVALID_SEARCH_CRITERIA");
        public InsurerValidationMessages()
            : base(
                new[]
                {
                    IdRequired,
                    InvalidSearchCriteria
                })
        {
        }
    }
}