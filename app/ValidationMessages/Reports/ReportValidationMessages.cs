﻿using System.Collections.ObjectModel;

namespace ValidationMessages.Reports
{
    public class ReportValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage ProposalHeaderIdRequired = new ValidationErrorMessage("Report",
            "ProposalHeaderId is required.", "PROPOSAL_HEADER_ID_IS_REQUIRED");

        public static ValidationErrorMessage EmailRequired = new ValidationErrorMessage("Report",
            "Email is required.", "EMAIL_IS_REQUIRED");

        public static ValidationErrorMessage OrganizationCodeRequired = new ValidationErrorMessage("Report",
            "OrganizationCode is required.", "PROPOSAL_HEADER_ID_IS_REQUIRED");

        public static ValidationErrorMessage QuoteUriRequired = new ValidationErrorMessage("Report",
            "QuoteUri is required.", "QUOTE_URI_IS_REQUIRED");

        public static ValidationErrorMessage PartyIdRequired = new ValidationErrorMessage("Report",
            "PartyId is required.", "QUOTE_URI_IS_REQUIRED");

        public static ValidationErrorMessage CampaignListEmpty = new ValidationErrorMessage("ValidateReportCampaignsDto",
           "Campaign List cannot be empty.", "CAMPAIGN_LIST_EMPTY");

        public static ValidationErrorMessage ReportListEmpty = new ValidationErrorMessage("ValidateReportCampaignsDto",
           "Report List cannot be empty.", "REPORT_LIST_EMPTY");

        public static ValidationErrorMessage MultiChannel = new ValidationErrorMessage("ValidateReportCampaignsDto",
           "This is a multi-channel report", "REPORT_MULTICHANNEL");

        public static ValidationErrorMessage MissingReportConfiguration = new ValidationErrorMessage("ValidateReportCampaignsDto",
          "Missing configuration for the reports '{0}' and campaigns '{1}'", "REPORT_MISSING_CONFIGURATION");

        public static ValidationErrorMessage ReportConfigurationExist = new ValidationErrorMessage("ValidateReportCampaignsDto",
         "Configurations exist for reports '{0}' and campaigns '{1}'", "REPORT_EXIST_CONFIGURATION");

        public static ValidationErrorMessage CampaignsDoNotExist = new ValidationErrorMessage("ValidateReportCampaignsDto",
         "Campaigns do not exist", "REPORT_CAMPAIGNS_NOT_EXIST");

        public ReportValidationMessages()
            : base(
                new[]
                {
                    ProposalHeaderIdRequired, EmailRequired, OrganizationCodeRequired, QuoteUriRequired, PartyIdRequired, CampaignListEmpty, ReportListEmpty, MultiChannel, MissingReportConfiguration,
                    ReportConfigurationExist, CampaignsDoNotExist
                })
        {
        }
    }
}