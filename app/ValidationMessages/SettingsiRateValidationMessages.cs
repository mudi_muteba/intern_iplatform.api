using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class SettingsiRateValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("EditSettingsiRateDto.Id",
            "Id can not be empty", "SETTINGSIRATE_ID_EMPTY");

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("CreateSettingsiRateDto.ChannelId",
            "ChannelId can not be empty", "SETTINGSIRATE_CHANNELID_EMPTY");

        public static ValidationErrorMessage ProductIdRequired = new ValidationErrorMessage("CreateSettingsiRateDto.ProductId",
            "ProductId can not be empty", "SETTINGSIRATE_PRODUCTID_EMPTY");

        public static ValidationErrorMessage EnvironmentRequired = new ValidationErrorMessage("CreateSettingsiRateDto.Environment",
            "Environment can not be empty", "SETTINGSIRATE_ENVIRONMENT_EMPTY");

        public static ValidationErrorMessage IdRequiredForDelete = new ValidationErrorMessage("DisableSettingsiRateDto.Id",
            "Id can not be empty or zero", "SETTINGSIRATE_ID_EMPTY");

        public SettingsiRateValidationMessages()
            : base(
                new[]
                {
                    ChannelIdRequired, ProductIdRequired, IdRequired, EnvironmentRequired, IdRequiredForDelete
                })
        {
        }
    }
}