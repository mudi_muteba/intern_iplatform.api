using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class BankValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage InvalidSearchCriteria = new ValidationErrorMessage("Search",
            "Invalid search criteria", "BANK_INVALID_SEARCH_CRITERIA");


        public BankValidationMessages()
            : base(
                new[]
                {
                    InvalidSearchCriteria,
                })
        {
        }
    }
}