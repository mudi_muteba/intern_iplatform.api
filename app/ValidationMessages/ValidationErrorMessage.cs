namespace ValidationMessages
{
    public class ValidationErrorMessage
    {
        public ValidationErrorMessage() { }
        public ValidationErrorMessage(string propertyName, string defaultMessage, string messageKey)
        {
            PropertyName = propertyName;
            DefaultMessage = defaultMessage;
            MessageKey = messageKey;
        }

        public ValidationErrorMessage(string defaultMessage, string messageKey)
        {
            DefaultMessage = defaultMessage;
            MessageKey = messageKey;
        }

        public string PropertyName { get; set; }
        public string DefaultMessage { get; set; }
        public string MessageKey { get; set; }

        public override string ToString()
        {
            return string.Format("{0} / {1} / {2}", PropertyName, DefaultMessage, MessageKey);
        }

        protected bool Equals(ValidationErrorMessage other)
        {
            return string.Equals(PropertyName, other.PropertyName) &&
                   string.Equals(DefaultMessage, other.DefaultMessage) && string.Equals(MessageKey, other.MessageKey);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ValidationErrorMessage) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (PropertyName != null ? PropertyName.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (DefaultMessage != null ? DefaultMessage.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (MessageKey != null ? MessageKey.GetHashCode() : 0);
                return hashCode;
            }
        }

        public ValidationErrorMessage AddParameters( string[] args)
        {
            var message = new ValidationErrorMessage(this.PropertyName, this.DefaultMessage, this.MessageKey);
            message.DefaultMessage = string.Format(message.DefaultMessage, args);
            return message;
        }
    }
}