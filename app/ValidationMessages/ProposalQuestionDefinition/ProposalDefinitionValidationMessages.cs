﻿using System.Collections.ObjectModel;

namespace ValidationMessages.ProposalQuestionDefinition
{
    public class ProposalDefinitionValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage ComprehensiveMotorCoverRequired = new ValidationErrorMessage("PROPOSAL.ID",
            "Cannot add a R1ITEM without a Motor with Type Of Cover Comprehensive ", "NO_MOTOR_COMPREHENSIVE_FOR_R1ITEM"
        );

        public static ValidationErrorMessage CannotAddDuplicateR1Item = new ValidationErrorMessage("PROPOSAL.COVER.ID",
            "Cannot add duplicate R1ITEMS", "CANNOT_ADD_DUPLICATE_R1ITEM"
            );

        public static ValidationErrorMessage R1ItemCannotBeMoreThanMotorComprehensiveCovers = new ValidationErrorMessage("PROPOSAL.COVER.ID",
            "R1ITEMS cannot be more than Motor Comprehensive Covers", "R1ITEM_CANNOT_BE_MORE_THAN_MOTOR"
            );

        public ProposalDefinitionValidationMessages()
            : base(
                new[]
                {
                    ComprehensiveMotorCoverRequired , CannotAddDuplicateR1Item ,R1ItemCannotBeMoreThanMotorComprehensiveCovers
                })
        {
        }
    }
}
