﻿using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class FormBuilderValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage NameRequired = new ValidationErrorMessage("CreateFormBuilderDto.Name",
            "Name can not be empty", "NAME_EMPTY");

        public static ValidationErrorMessage DescriptionRequired = new ValidationErrorMessage("CreateFormBuilderDto.Description",
            "Description can not be empty", "DESCRIPTION_EMPTY");

        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("DisableFormBuilderDto.Id",
            "ID can not be empty", "ID_EMPTY");

        public FormBuilderValidationMessages()
            : base(
                new[]
                {
                    NameRequired, DescriptionRequired, IdRequired
                })
        {
        }
    }
}