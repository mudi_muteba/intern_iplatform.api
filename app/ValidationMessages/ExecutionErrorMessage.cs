using Shared; 

namespace ValidationMessages
{
    public class ExecutionErrorMessage
    {
        public ExecutionErrorMessage(string message, string messageKey)
        {
#if DEBUG
            Message = message;
#endif

#if !DEBUG
            Message = string.Format("Failed to complete the request. The timestamp is {0}", SystemTime.Now());
#endif
            MessageKey = messageKey;
        }

        public string Message { get; private set; }
        public string MessageKey { get; private set; }
    }
}