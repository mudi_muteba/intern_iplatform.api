﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationMessages.LossHistoryQuestionDefinition
{
    public class LossHistoryDefinitionValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage ClientCodeRequired = new ValidationErrorMessage(
            "LossHistoryQuestionDefinition.ClientCode",
            "ClientCode can not be empty or null"
            , "LossHistoryQuestionDefinition_ClientCode_Empty"
        );

        public LossHistoryDefinitionValidationMessages()
            : base(
                new[]
                {
                    ClientCodeRequired
                })
        {
        }
    }
}
