﻿using System.Collections.ObjectModel;

namespace ValidationMessages.Notifications
{
    public class NotificationValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public NotificationValidationMessages()
            : base(new[]
            {
                MissingQuestionText
            })
        { }

        public static ValidationErrorMessage MissingId
        {
            get
            {
                return new ValidationErrorMessage("Notification", "Missing Notification identifier", "ID_MISSING");
            }
        }


        public static ValidationErrorMessage MissingQuestionText
        {
            get
            {
                return new ValidationErrorMessage("Notification", "Missing Notification message", "MESSAGE_MISSING");
            }
        }


    }
}
