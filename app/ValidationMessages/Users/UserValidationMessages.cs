﻿using System.Collections.ObjectModel;

namespace ValidationMessages.Users
{
    public class UserValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage UserNotFound = new ValidationErrorMessage("User",
            "User not found", "USER_NOT_FOUND");

        public static ValidationErrorMessage UserNameRequired = new ValidationErrorMessage("UserName",
            "User name is required", "USER_NAME_IS_REQUIRED");

        public static ValidationErrorMessage NameRequired = new ValidationErrorMessage("Name",
            "Name of user is required", "NAME_IS_REQUIRED");

        public static ValidationErrorMessage LastNameRequired = new ValidationErrorMessage("LastName",
            "Lastname of user is required", "LASTNAME_IS_REQUIRED");

        public static ValidationErrorMessage PasswordRequired = new ValidationErrorMessage("Password",
            "Password is required", "USER_PASSWORD_IS_REQUIRED");

        public static ValidationErrorMessage NoChannelsAllocated = new ValidationErrorMessage("Channels",
            "The user does not belong to any channels", "USER_CHANNELS_ARE_REQUIRED");

        public static ValidationErrorMessage NoRolesAllocated = new ValidationErrorMessage("Roles",
            "The user does not belong to any roles", "USER_ROLES_ARE_REQUIRED");

        public static ValidationErrorMessage PasswordResetTokenRequired =
            new ValidationErrorMessage("PasswordResetToken",
                "The password reset token is empty", "USER_PASSWORD_RESET_TOKEN_REQUIRED");

        public static ValidationErrorMessage RegistrationTokenRequired =
            new ValidationErrorMessage("RegistrationToken",
                "The registration token is empty", "USER_REGISTRATION_TOKEN_REQUIRED");

        public static ValidationErrorMessage UserIdRequired = new ValidationErrorMessage("Id", "User id required",
            "USER_ID_REQUIRED");

        public static ValidationErrorMessage InvalidPasswordResetToken = new ValidationErrorMessage(
            "PasswordResetToken", "Password reset token mismatch",
            "USER_PASSWORD_RESET_TOKEN_MISTMATCH");

        public static ValidationErrorMessage InvalidSearchCriteria = new ValidationErrorMessage("Search",
            "Invalid search criteria", "USER_INVALID_SEARCH_CRITERIA");

        public static ValidationErrorMessage PhoneNumberRequired = new ValidationErrorMessage("PhoneNumber", "Phone number required",
            "PHONE_NUMBER_REQUIRED");

        public static ValidationErrorMessage IdNumberRequired = new ValidationErrorMessage("IdNumber", "ID number required",
            "ID_NUMBER_REQUIRED");

        public static ValidationErrorMessage DuplicateExternalReference = new ValidationErrorMessage("ExternalReference",
            "ExternalReference already exist : {0}", "USER_EXTERNALREFERENCE_DUPLICATE");

        public static ValidationErrorMessage InvalidChannelAllocation(int channelId)
        {
            return new ValidationErrorMessage("Channel", string.Format("Invalid channel allocation for '{0}'", channelId),
                "INVALID_CHANNEL_ALLOCATION");
        }


        public static ValidationErrorMessage DuplicateUserName(string userName)
        {
            return new ValidationErrorMessage("Name", string.Format("User with name '{0}'", userName),
                "USER_NAME_DUPLICATE");
        }

        public UserValidationMessages()
            : base(
                new[]
                {
                    IdNumberRequired,
                    PhoneNumberRequired,
                    InvalidSearchCriteria,
                    InvalidPasswordResetToken,
                    UserIdRequired,
                    RegistrationTokenRequired,
                    PasswordResetTokenRequired,
                    NoRolesAllocated,
                    NoChannelsAllocated,
                    PasswordRequired,
                    LastNameRequired,
                    NameRequired,
                    UserNameRequired,
                    UserNotFound,
                    DuplicateExternalReference,

                })
        {
        }
    }
}