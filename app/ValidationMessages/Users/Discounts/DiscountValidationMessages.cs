using System.Collections.ObjectModel;

namespace ValidationMessages.Users.Discounts
{
    public class DiscountValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage DiscountAllReadyExists =
            new ValidationErrorMessage("Discount.CoverDefinitionId",
                "Discount for user {0} and cover definition {1} all ready exists", "DISCOUNT_ALL_READY_EXISTS");

        public static ValidationErrorMessage UserIdNotSet =
            new ValidationErrorMessage("Discount.UserId",
                "UserId is Required", "USER_ID_REQUIRED");

        public static ValidationErrorMessage CoverIdNotSet =
            new ValidationErrorMessage("Discount.UserId",
                "CoverId is Required", "COVER_ID_REQUIRED");

        public static ValidationErrorMessage ProductIdNotSet =
            new ValidationErrorMessage("Discount.UserId",
                "ProductId is Required", "PRODUCT_ID_REQUIRED");

        public DiscountValidationMessages()
            : base(new[] {DiscountAllReadyExists, UserIdNotSet, CoverIdNotSet, ProductIdNotSet})
        {
        }
    }
}