namespace ValidationMessages
{
    public class MissingEntityErrorMessage
    {
        public MissingEntityErrorMessage(string typeName, string defaultMessage, string messageKey)
        {
            TypeName = typeName;
            DefaultMessage = defaultMessage;
            MessageKey = messageKey;
        }

        public string TypeName { get; private set; }
        public string DefaultMessage { get; private set; }
        public string MessageKey { get; private set; }
    }
}