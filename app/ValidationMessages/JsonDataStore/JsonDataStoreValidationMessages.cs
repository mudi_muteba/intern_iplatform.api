﻿using System.Collections.ObjectModel;

namespace ValidationMessages.JsonDataStore
{
    public class JsonDataStoreValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage SessionIdRequired = new ValidationErrorMessage(
            "DigitalJsonStorage.SessionId",
            "SessionId can not be empty or null"
            , "DIGITALJSONSTORAGE_SESSIONID_EMPTY"
        );

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage(
            "DigitalJsonStorage.ChannelID",
            "ChannelID can not be empty or null"
            , "DIGITALJSONSTORAGE_CHANNELID_EMPTY"
        );

        public static ValidationErrorMessage PartyIdRequired = new ValidationErrorMessage(
            "DigitalJsonStorage.PartyId",
            "PartyId can not be empty or null"
            , "DIGITALJSONSTORAGE_PARTYID_EMPTY"
        );

        public static ValidationErrorMessage SectionRequired = new ValidationErrorMessage(
            "DigitalJsonStorage.Section",
            "Section can not be empty"
            , "DIGITALJSONSTORAGE_SECTION_EMPTY"
        );

        public static ValidationErrorMessage SavePointRequired = new ValidationErrorMessage(
            "DigitalJsonStorage.SavePoint",
            "SavePoint can not be empty"
            , "DIGITALJSONSTORAGE_SAVEPOINT_EMPTY"
        );

        public static ValidationErrorMessage TransactionDate = new ValidationErrorMessage(
            "DigitalJsonStorage.TransactionDate",
            "TransactionDate can not be less than 1"
            , "DIGITALJSONSTORAGE_TRANSACTIONDATE_EMPTY"
        );

        public static ValidationErrorMessage InvalidSavePoint = new ValidationErrorMessage(
            "DigitalJsonStorage.SavePoint",
            "SavePoint can not be less than 1"
            , "DIGITALJSONSTORAGE_SAVEPOINT_LESS_THAN_ONE"
        );

        public static ValidationErrorMessage InvalidGetLatestByPartyIdCriteria = new ValidationErrorMessage(
            "DigitalJsonStorage.PartyId",
            "PartyId can not be empty or null when searching by latest"
            , "DIGITALJSONSTORAGE_PARTYID_EMPTY"
        );

        public static ValidationErrorMessage InvalidSearchCriteria = new ValidationErrorMessage("Search",
            "Invalid search criteria", "JSONDATASTORE_INVALID_SEARCH_CRITERIA");

        public JsonDataStoreValidationMessages()
            : base(
                new[]
                {
                    SessionIdRequired
                    ,ChannelIdRequired
                    ,PartyIdRequired
                    ,SectionRequired
                    ,SavePointRequired
                    ,InvalidSavePoint
                    ,TransactionDate
                    ,InvalidSearchCriteria
                    ,InvalidGetLatestByPartyIdCriteria
                })
        {
        }

        
    }
}
