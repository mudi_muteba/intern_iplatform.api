﻿using System.Collections.ObjectModel;

namespace ValidationMessages.Sales
{
    public class SalesFNIValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage UserNameRequired = new ValidationErrorMessage("SalesFNI.UserName",
            "UserName can not be empty", "  USERNAME_EMPTY");

        public static ValidationErrorMessage WorkTelephoneCodeRequired = new ValidationErrorMessage("SalesFNI.UserWorkTelephoneCode",
            "UserWorkTelephoneCode can not be empty", "USERWORKTELEPHONECODE_EMPTY");

        public static ValidationErrorMessage WorkTelephoneNumberRequired = new ValidationErrorMessage("SalesFNI.UserWorkTelephoneNumber",
            "UserWorkTelephoneNumber can not be empty", "USERWORKTELEPHONENUMBER_EMPTY");

        public static ValidationErrorMessage EmailAddressRequired = new ValidationErrorMessage("SalesFNI.UserEmailAddress",
            "UserEmailAddress can not be empty", "USEREMAILADDRESS_EMPTY");

        public static ValidationErrorMessage EmployeeNumberRequired = new ValidationErrorMessage("SalesFNI.EmployeeNumber",
            "EmployeeNumber can not be empty", "EMPLOYEENUMBER_EMPTY");

        public static ValidationErrorMessage FirstNameRequired = new ValidationErrorMessage("SalesFNI.FiFirstName",
            "FiFirstName can not be empty", "FIRSTNAME_EMPTY");

        public static ValidationErrorMessage LastNameRequired = new ValidationErrorMessage("SalesFNI.FiLastName",
            "FiLastName can not be empty", "LASTNAME_EMPTY");

        public static ValidationErrorMessage IdNumberRequired = new ValidationErrorMessage("SalesFNI.FiidNumber",
            "FiidNumber can not be empty", "IDNUMBER_EMPTY");

        public SalesFNIValidationMessages() : base(new[] { UserNameRequired, WorkTelephoneCodeRequired, WorkTelephoneNumberRequired, EmailAddressRequired, EmployeeNumberRequired, FirstNameRequired, LastNameRequired, IdNumberRequired })
        {
        }
    }
}
