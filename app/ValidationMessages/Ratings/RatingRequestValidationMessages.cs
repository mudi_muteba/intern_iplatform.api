namespace ValidationMessages.Ratings
{
    public static class RatingRequestValidationMessages
    {
        public static ValidationErrorMessage NoItemsAddedToRequest = new ValidationErrorMessage("RatingRequest.Items",
            "No items added to request", "RATING_REQUEST_NO_ITEMS");

        public static ValidationErrorMessage NoPolicyInfo = new ValidationErrorMessage("RatingRequest.Policy",
            "No policy information", "RATING_REQUEST_NO_POLICY_INFO");

        public static ValidationErrorMessage NoPersons = new ValidationErrorMessage("RatingRequest.Policy.Persons",
            "No persons associated with the policy", "RATING_REQUEST_NO_PERSONS_INFO");

        public static ValidationErrorMessage NoRatingContext = new ValidationErrorMessage("RatingRequest.RatingContext",
            "No rating context is available to perform rating", "RATING_REQUEST_NO_CONTEXT");

        public static ValidationErrorMessage NoChannelProvided = new ValidationErrorMessage("RatingRequest.RatingContext.ChannelId",
            "No channel Id provided", "RATING_REQUEST_NO_CHANNEL");

        public static ValidationErrorMessage InvalidChannelProvided = new ValidationErrorMessage("RatingRequest.RatingContext.ChannelId",
            "Invalid channel provided", "RATING_REQUEST_INVALID_CHANNEL");

        public static ValidationErrorMessage NoRatingEngines = new ValidationErrorMessage("RatingRequest.RatingContext.Engines",
            "No rating engines defined for rating", "RATING_REQUEST_NO_ENGINES");

        public static ValidationErrorMessage InvalidPerson = new ValidationErrorMessage("RatingRequest.Policy.Person",
            "Invalid person info provided", "RATING_REQUEST_INVALID_PERSON");

        public static ValidationErrorMessage NoAddresses = new ValidationErrorMessage("RatingRequest.Policy.Person",
            "No addresses associated with the person", "RATING_REQUEST_NO_ADDRESSES");

        public static ValidationErrorMessage InvalidAddress = new ValidationErrorMessage("RatingRequest.Policy.Person",
            "Invalid address associated with person", "RATING_REQUEST_INVALID_ADDRESS");

        public static ValidationErrorMessage NoRatingReference = new ValidationErrorMessage("RatingRequest.Id",
            "No rating reference provided", "RATING_REQUEST_NO_RATING_REFERENCE");

        public static ValidationErrorMessage NoAnswersForItem(int assetNo)
        {
            return new ValidationErrorMessage("RatingRequest.Items",
                string.Format("No answers added to item with asset number '{0}'", assetNo),
                "RATING_REQUEST_MISSING_ANSWERS");
        }

        public static ValidationErrorMessage InvalidQuestionAnswer(string errorMessage)
        {
            return new ValidationErrorMessage("RatingRequest.Items.Answers",
                errorMessage,
                "RATING_REQUEST_INVALID_ANSWER");
        }
    }
}