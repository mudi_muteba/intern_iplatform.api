namespace ValidationMessages.Ratings
{
    public class QuoteDistributionValidationMessages
    {
        public static ValidationErrorMessage InvalidExternalReference = new ValidationErrorMessage("DistributeQuoteDto.ExternalReference",
            "The external reference is not valid", "QUOTE_DISTRIBUTION_INVALID_EXTERNAL_REFERENCE");

        public static ValidationErrorMessage UnknownExternalReference = new ValidationErrorMessage("DistributeQuoteDto.ExternalReference",
            "A quote for the external reference could not be found", "QUOTE_DISTRIBUTION_NOT_FOUND_QUOTE_FOR_EXTERNAL_REFERENCE");

        public static ValidationErrorMessage InvalidDistributionMethod = new ValidationErrorMessage("DistributeQuoteDto.Via",
            "Invalid distribution method defined", "QUOTE_DISTRIBUTION_INVALID_DISTRIBUTION_METHOD");

        public static ValidationErrorMessage NoProductsSelectedForDistribution = new ValidationErrorMessage("DistributeQuoteDto.ProductCodes",
            "No products selected for distribution", "QUOTE_DISTRIBUTION_NO_PRODUCT_SELECTED");

        public static ValidationErrorMessage NoCellPhoneNumberProvided = new ValidationErrorMessage("DistributeQuoteDto.CellPhoneNumber",
            "No cellphone number provided", "QUOTE_DISTRIBUTION_NO_CELLPHONE_NUMBER_PROVIDED");

        public static ValidationErrorMessage ProductNotQuoted(string productCode)
        {
            return new ValidationErrorMessage("DistributeQuoteDto.ProductCodes",
                string.Format("Product {0} was not quoted on", productCode), "QUOTE_DISTRIBUTION_PRODUCT_NOT_QUOTED");
        }
    }
}