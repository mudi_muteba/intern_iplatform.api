namespace ValidationMessages.Ratings
{
    public class QuoteCreationValidationMessages
    {
        public static ValidationErrorMessage NoPolicyAccepted = new ValidationErrorMessage("RatingResultDto.Policy",
            "No policies available", "QUOTE_CREATION_NO_POLICIES_AVAILABLE");

        public static ValidationErrorMessage NoStateDefined = new ValidationErrorMessage("RatingResultDto.State",
            "No state defined", "QUOTE_CREATION_NO_STATE_DEFINED");

        public static ValidationErrorMessage NoProposalFoundForTheRatingResult = new ValidationErrorMessage("RatingResultDto.RatingRequestId",
            "No proposal found for the rating result", "QUOTE_CREATION_NO_PROPOSAL_FOUND_FOR_THE_RATING_RESULT");

        public static ValidationErrorMessage NoRatingRequestIdProvided = new ValidationErrorMessage("RatingResultDto.RatingRequestId",
            "No rating request ID provided", "QUOTE_CREATION_NO_RATING_REQUEST_ID_PROVIDED");

        public static ValidationErrorMessage NoProductCode = new ValidationErrorMessage("Policy.ProductCode",
            "No product code defined", "QUOTE_CREATION_NO_PRODUCT_CODE_DEFINED");

        public static ValidationErrorMessage NoInsurerCode = new ValidationErrorMessage("Policy.InsurerCode",
            "No insurer code defined", "QUOTE_CREATION_NO_INSURER_CODE_DEFINED");
    }
}