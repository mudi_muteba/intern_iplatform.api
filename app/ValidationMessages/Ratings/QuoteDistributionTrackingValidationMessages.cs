namespace ValidationMessages.Ratings
{
    public class QuoteDistributionTrackingValidationMessages
    {
        public static ValidationErrorMessage InvalidQuote = new ValidationErrorMessage("TrackQuoteDistributionDto.QuoteId",
            "Quote id is invalid", "QUOTE_DISTRIBUTION_TRACKING_INVALID_QUOTE");

        public static ValidationErrorMessage InvalidDistributionMethod = new ValidationErrorMessage("TrackQuoteDistributionDto.Via",
            "Invalid distribution method defined", "QUOTE_DISTRIBUTION_TRACKING_INVALID_DISTRIBUTION_METHOD");

        public static ValidationErrorMessage NoProductProvided = new ValidationErrorMessage("TrackQuoteDistributionDto.ProductCode",
            "No product code provided", "QUOTE_DISTRIBUTION_TRACKING_NO_PRODUCT_CODE");

        public static ValidationErrorMessage NoProvider = new ValidationErrorMessage("TrackQuoteDistributionDto.Provider",
            "No provider specified", "QUOTE_DISTRIBUTION_TRACKING_NO_PROVIDER");

        public static ValidationErrorMessage NoQuoteFound(int quoteId)
        {
            return new ValidationErrorMessage("TrackQuoteDistributionDto.QuoteId",
                string.Format("Quote {0} not found", quoteId), "QUOTE_DISTRIBUTION_TRACKING_QUOTE_NOT_FOUND");
        }

        public static ValidationErrorMessage ProductNotQuoted(string productCode, int quoteId)
        {
            return new ValidationErrorMessage("TrackQuoteDistributionDto.ProductCode",
                string.Format("Product {0} not found on quote {1}", productCode, quoteId), "QUOTE_DISTRIBUTION_TRACKING_PRODUCT_NOT_QUOTED");
        }
    }
}