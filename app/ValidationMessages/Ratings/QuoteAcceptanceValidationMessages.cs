namespace ValidationMessages.Ratings
{
    public static class QuoteAcceptanceValidationMessages
    {
        public static ValidationErrorMessage NoPolicyAccepted = new ValidationErrorMessage("QuoteAcceptance.Policy",
            "No policy accepted", "QUOTE_ACCEPTANCE_NO_POLICY_ACCEPTED");

        public static ValidationErrorMessage NoRatingRequest = new ValidationErrorMessage("QuoteAcceptance.Request",
            "No rating request associated with quote acceptance", "QUOTE_ACCEPTANCE_NO_RATING_REQUEST");

        public static ValidationErrorMessage NoInsuredInfo = new ValidationErrorMessage("QuoteAcceptance.InsuredInfo",
            "No insured info associated with quote acceptance", "QUOTE_ACCEPTANCE_NO_INSURED_INFO");

        public static ValidationErrorMessage InvalidInsured = new ValidationErrorMessage("QuoteAcceptance.InsuredInfo",
            "Invalid insured info provided", "QUOTE_ACCEPTANCE_INVALID_INSURED_INFO");

        public static ValidationErrorMessage InvalidQuoteId = new ValidationErrorMessage("QuoteAcceptance.QuoteId",
            "Invalid Quote Id {0} provided", "QUOTE_ACCEPTANCE_INVALID_QUOTE_ID");

        public static ValidationErrorMessage EmptyQuoteId = new ValidationErrorMessage("DeleteQuoteAcceptanceDto.QuoteId",
            "QuoteId cannot be empty", "DELETE_QUOTE_ACCEPTANCE_EMPTY_QUOTE_ID");

    }
}