﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationMessages
{
    public class OverrideRatingQuestionMapVapQuestionDefinitionValidationMessage : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("OverrideRatingQuestionMapVapQuestionDefinitionDto.Id",
            "Id can not be empty", "OVERRIDERATINGQUESTIONMAPVAPQUESTIONDEFINITION_ID_EMPTY");

        public static ValidationErrorMessage OverrideRatingQuestionIdRequired = new ValidationErrorMessage("OverrideRatingQuestionMapVapQuestionDefinitionDto.OverrideRatingQuestionId",
            "OverrideRatingQuestionId can not be empty", "OVERRIDERATINGQUESTIONMAPVAPQUESTIONDEFINITION_OVERRIDERATINGQUESTIONID_EMPTY");

        public static ValidationErrorMessage MapVapQuestionDefinitionIdIdRequired = new ValidationErrorMessage("OverrideRatingQuestionMapVapQuestionDefinitionDto.MapVapQuestionDefinitionId",
            "MapVapQuestionDefinitionId can not be empty", "OVERRIDERATINGQUESTIONMAPVAPQUESTIONDEFINITION_MAPVAPQUESTIONDEFINITIONID_EMPTY");

        public OverrideRatingQuestionMapVapQuestionDefinitionValidationMessage() : base(new []
        {
            IdRequired,
            OverrideRatingQuestionIdRequired,
            MapVapQuestionDefinitionIdIdRequired
        })
        {
        }
    }
}
