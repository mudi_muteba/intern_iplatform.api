using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class SettingsQuoteUploadValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("EditSettingsQuoteUploadDto.Id",
            "Id can not be empty", "SETTINGSQUOTEUPLOAD_ID_EMPTY");

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("CreateSettingsQuoteUploadDto.ChannelId",
            "ChannelId can not be empty", "SETTINGSQUOTEUPLOAD_CHANNELID_EMPTY");

        public static ValidationErrorMessage ProductIdRequired = new ValidationErrorMessage("CreateSettingsQuoteUploadDto.ProductId",
            "ProductId can not be empty", "SETTINGSQUOTEUPLOAD_PRODUCTID_EMPTY");

        public static ValidationErrorMessage EnvironmentRequired = new ValidationErrorMessage("CreateSettingsQuoteUploadDto.ProductId",
            "Environment can not be empty", "SETTINGSQUOTEUPLOAD_PRODUCTID_EMPTY");

        public static ValidationErrorMessage NoSettings = new ValidationErrorMessage("CreateSettingsQuoteUploadDto.ProductId",
            "No Settings to Save", "SETTINGSQUOTEUPLOAD_SETTINGS_EMPTY");

        public static ValidationErrorMessage SettingNameRequired = new ValidationErrorMessage("EditSettingsQuoteUploadDto.SettingName",
            "SettingName cannot be empty", "SETTINGSQUOTEUPLOAD_SETTINGNAME_EMPTY");

        public static ValidationErrorMessage SettingValueRequired = new ValidationErrorMessage("EditSettingsQuoteUploadDto.SettingValue",
            "SettingValue cannot be empty", "SETTINGSQUOTEUPLOAD_SETTINGVALUE_EMPTY");

        public SettingsQuoteUploadValidationMessages()
            : base(
                new[]
                {
                    ChannelIdRequired, ProductIdRequired, IdRequired, EnvironmentRequired, SettingValueRequired, SettingNameRequired, NoSettings
                })
        {
        }
    }
}