﻿using System.Collections.ObjectModel;

namespace ValidationMessages.Policy
{
    public class PolicyValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage InvalidId = new ValidationErrorMessage("Policy.Id",
            "Invalid Policy Id {0}", "POLICY_INVALID_ID");

        public static ValidationErrorMessage EmptyDateOfLoss = new ValidationErrorMessage("GetPolicyAvailableItemsForFNOLDto.DateOfLoss",
            "DateOfLoss cannot be empty", "EMPTY_DATEOFLOSS");

        public static ValidationErrorMessage InvalidDateOfLoss = new ValidationErrorMessage("GetPolicyAvailableItemsForFNOLDto.DateOfLoss",
            "Invalid DateOfLoss {0}", "INVALID_DATEOFLOSS");

        public static ValidationErrorMessage InvalidIplatformId = new ValidationErrorMessage("GetPolicyByIPlatformIdDto.IPlatformId",
            "Invalid IPlatformId {0}", "INVALID_IPLATFORMID");

        public static ValidationErrorMessage EmptyQuoteId = new ValidationErrorMessage("CreatePolicyWhenQuoteAcceptedDto.QuoteId",
            "QuoteId cannot be empty", "EMPTY_QUOTEID");

        public static ValidationErrorMessage EmptyReference = new ValidationErrorMessage("CreatePolicyWhenQuoteAcceptedDto.Reference",
            "Reference cannot be empty", "EMPTY_REFERENCE");

        public static ValidationErrorMessage EmptyQuoteAcceptedLeadActivityId = new ValidationErrorMessage("CreatePolicyWhenQuoteAcceptedDto.QuoteAcceptedLeadActivityId",
            "QuoteAcceptedLeadActivityId cannot be empty", "EMPTY_QUOTEACCEPTEDLEADACTIVITYID");

        public PolicyValidationMessages()
            : base(
                new[]
                {
                    InvalidId,
                    EmptyDateOfLoss,
                    InvalidDateOfLoss,
                    InvalidIplatformId,
                    EmptyQuoteId,
                    EmptyReference,
                    EmptyQuoteAcceptedLeadActivityId
                })
        {
        }
    }
}