﻿using System;
using System.Collections.ObjectModel;

namespace ValidationMessages.Policy
{
    public class PolicyUploadConfirmationValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage InvalidId = new ValidationErrorMessage("PolicyHeader.PlatformId",
            "Invalid Policy Platform Id", "POLICY_INVALID_PlatformId");

        public static ValidationErrorMessage EmptyPolicyNo = new ValidationErrorMessage("PolicyHeader.PolicyNo",
            "Policy No cannot be empty", "EMPTY_POLICYNO");

        public static ValidationErrorMessage PlatformIdNotFound = new ValidationErrorMessage("PolicyHeader.PlatformId",
            "Policy not found with Platform Id {0}", "PLATFORMID_NOTFOUND");

        public PolicyUploadConfirmationValidationMessages()
            : base(
                new[]
                {
                    InvalidId,
                    EmptyPolicyNo,
                    PlatformIdNotFound
                })
        {
        }
    }
}