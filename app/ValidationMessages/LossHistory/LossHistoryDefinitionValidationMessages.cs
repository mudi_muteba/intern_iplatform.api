﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ValidationMessages.LossHistory
{
    public class LossHistoryDefinitionValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage PartyIdRequired = new ValidationErrorMessage(
            "PartyId",
            "PartyId can not be empty or null"
            , "LOSSHISTORY_PARTYID_EMPTY"
        );

        public static ValidationErrorMessage PartyTypeIdRequired = new ValidationErrorMessage(
            "PartyTypeId",
            "PartyTypeId can not be empty or null"
            , "LOSSHISTORY_PARTYTYPEID_EMPTY"
        );

        public static ValidationErrorMessage DateCreatedRequired = new ValidationErrorMessage(
            "DateCreated",
            "DateCreated can not be empty or null, use UTC date time yyyy-mm-dd-hh-mm-ss"
            , "LOSSHISTORY_DateCreated_EMPTY"
        );

        public LossHistoryDefinitionValidationMessages()
            : base(
                  new[]
                  {
                      PartyIdRequired
                      ,PartyTypeIdRequired
                      ,DateCreatedRequired
                  }
                )
        {
        }
    }
}
