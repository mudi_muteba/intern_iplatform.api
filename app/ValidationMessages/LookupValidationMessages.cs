using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class LookupValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage NameEmpty = new ValidationErrorMessage("OccupationSearchDto.Name",
            "Name can not be empty", "OCCUPATION_NAME_EMPTY");


        public LookupValidationMessages()
            : base(
                new[]
                {
                    NameEmpty,
                })
        {
        }
    }
}