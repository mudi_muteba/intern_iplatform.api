using System;
using Shared;

namespace ValidationMessages
{
    public static class SystemErrorMessages
    {
        public static MissingEntityErrorMessage MissingEntityErrorMessage(Type type, int entityId)
        {
            var errorMessage = string.Format("Could not find entity of type {0} with id {1}",
                type,
                entityId);

            return new MissingEntityErrorMessage(type.FullName, errorMessage, "MISSING_ENTITY");
        }

        public static ExecutionErrorMessage GeneralFailure(string message)
        {
            var failureMessage = string.Format("Failed to complete the request. Timestamp {0}. Message: {1}",
                SystemTime.Now(), message);

            return new ExecutionErrorMessage(failureMessage, "GENERAL_FAILURE");
        }
    }
}