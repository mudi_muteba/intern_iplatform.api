﻿using System.Collections.ObjectModel;

namespace ValidationMessages.RatingRuleHeader
{
   public class RatingRuleHeaderValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public RatingRuleHeaderValidationMessages()
            : base(
                  new[]
                  {
                      StartDateRequired,
                      EndDateRequired,
                    IDRequired
                  })
        {
        }

        public static ValidationErrorMessage StartDateRequired = new ValidationErrorMessage("RatingRuleHeaderValidation.StartDate",
          "StartDate can not be empty", "RatingRuleHeader_StartDate_Empty");

        public static ValidationErrorMessage EndDateRequired = new ValidationErrorMessage("RatingRuleHeaderValidation.EndDate",
            "EndDate can not be empty", "RatingRuleHeader_EndDate_Empty");

        public static ValidationErrorMessage IDRequired =
            new ValidationErrorMessage("RatingRuleHeaderValidation.ID",
            "ID can not be empty", "RatingRuleHeader_ID_Empty");
    }
}
