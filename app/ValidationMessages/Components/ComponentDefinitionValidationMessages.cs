﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationMessages.Components
{
    public class ComponentDefinitionValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage PartyIdInvalid = new ValidationErrorMessage("ComponentHeaderDto.PartyId",
            "Invalid PartyId: {0}", "COMPONENTHEADER_PARTYID_INVALID");

        public static ValidationErrorMessage ComponentTypeIdInvalId = new ValidationErrorMessage("ComponentHeaderDto.ComponentTypeId",
            "Invalid ComponentTypeId: {0}", "COMPONENTHEADER_COMPONENTTYPEID_INVALID");

        public static ValidationErrorMessage ComponentHeaderIdInvalid = new ValidationErrorMessage("ComponentHeaderDto.HeaderId",
            "Invalid HeaderId: {0}", "COMPONENTHEADER_HEADERID_INVALID");

        public ComponentDefinitionValidationMessages()
            : base(
                new[]
                {
                    ComponentHeaderIdInvalid,
                    PartyIdInvalid,
                    ComponentTypeIdInvalId
                })
        {
        }
    }
}

