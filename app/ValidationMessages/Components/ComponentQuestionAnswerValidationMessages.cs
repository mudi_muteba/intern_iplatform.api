﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationMessages.Components
{
    public class ComponentQuestionAnswerValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage ComponentDefinitionInvalId = new ValidationErrorMessage("ComponentQuestionAnswerDto.ComponentDefinitionId",
            "Invalid PartyId: {0}", "COMPONENTQUESTIONANSWERR_PARTYID_INVALID");

        public static ValidationErrorMessage ComponentQuestionDefinitionIdInvalId = new ValidationErrorMessage("ComponentHeaderDto.ComponentQuestionDefinitionId",
            "Invalid ComponentQuestionDefinitionId: {0}", "COMPONENTQUESTIONANSWER_COMPOENNTQUESTIONDEFINITIONID_INVALID");

        public static ValidationErrorMessage ComponentQuestionTypeIdInvalId = new ValidationErrorMessage("ComponentHeaderDto.ComponentQuestionTypeId",
            "Invalid ComponentQuestionTypeId: {0}", "COMPONENTQUESTIONANSWER_COMPONENTQUESTIONTYPEID_INVALID");

        public ComponentQuestionAnswerValidationMessages()
            : base(
                new[]
                {
                    ComponentDefinitionInvalId,
                    ComponentQuestionDefinitionIdInvalId,
                    ComponentQuestionTypeIdInvalId
                })
        {
        }
    }
}
