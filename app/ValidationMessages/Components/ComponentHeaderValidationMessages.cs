﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationMessages.Components
{
    public class ComponentHeaderValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage PartyIdInvalid = new ValidationErrorMessage("ComponentHeaderDto.PartyId",
    "Invalid PartyId: {0}", "COMPONENTHEADER_PARTYID_INVALID");
        public ComponentHeaderValidationMessages()
            : base(
                new[]
                {
                    PartyIdInvalid
                })
        {
        }
    }
}
