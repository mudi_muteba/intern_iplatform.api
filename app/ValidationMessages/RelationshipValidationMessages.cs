using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class RelationshipValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("Relationship.Id",
            "Id cannot be empty", "RELATIONSHIP_ID_EMPTY");

        public static ValidationErrorMessage FunctionCharacterExceeded = new ValidationErrorMessage("Relationship.Functions",
            "Functions cannot exceed 50 characters", "RELATIONSHIP_FUNCTION_EXCEED_CHARACTER");

        public static ValidationErrorMessage ClaimCharacterExceeded = new ValidationErrorMessage("Relationship.Claims",
            "Claims cannot exceed 50 characters", "RELATIONSHIP_CLAIM_EXCEED_CHARACTER");

        public static ValidationErrorMessage OtherCharacterExceeded = new ValidationErrorMessage("Relationship.Other",
            "Other cannot exceed 50 characters", "RELATIONSHIP_OTHER_EXCEED_CHARACTER");

        public static ValidationErrorMessage PartyIdRequired = new ValidationErrorMessage("Relationship.PartyId",
            "PartyId cannot be empty", "RELATIONSHIP_PARTYID_REQUIRED");

        public static ValidationErrorMessage ChildPartyIdRequired = new ValidationErrorMessage("Relationship.ChildPartyId",
            "ChildPartyId cannot be empty", "RELATIONSHIP_CHILDPARTYID_REQUIRED");

        public static ValidationErrorMessage ExistingRelationship = new ValidationErrorMessage("Relationship.Id",
            "Relationship already exist with Id: {0}", "RELATIONSHIP_ALREADY_EXIST");

        public static ValidationErrorMessage InvalidPartyId = new ValidationErrorMessage("Relationship.PartyId",
            "Invalid PartyId: {0}", "RELATIONSHIP_PARTYID_INVALID");

        public static ValidationErrorMessage InvalidChildPartyId = new ValidationErrorMessage("Relationship.ChildPartyId",
            "Invalid ChildPartyId: {0}", "RELATIONSHIP_CHILDPARTYID_INVALID");

        public RelationshipValidationMessages()
            : base(
                new[]
                {
                    IdRequired,
                    FunctionCharacterExceeded,
                    ClaimCharacterExceeded,
                    OtherCharacterExceeded,
                    PartyIdRequired,
                    ChildPartyIdRequired,
                    ExistingRelationship,
                    InvalidPartyId,
                    InvalidChildPartyId
                })
        {
        }
    }
}