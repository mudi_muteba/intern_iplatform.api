﻿using System.Collections.ObjectModel;

namespace ValidationMessages.Escalations
{
    public class EscalationPlanValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage EventIdRequired = new ValidationErrorMessage("GetEscalationDto.EventId",
            "Event Id cannot be empty", "GET_ESCALATION_PLAN_EVENT_ID_EMPTY");

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("GetEscalationDto.ChannelId",
            "Channel Id cannot be empty", "GET_ESCALATION_PLAN_CHANNEL_ID_EMPTY");

        public static ValidationErrorMessage ProductIdRequired = new ValidationErrorMessage("GetEscalationDto.ProductId",
            "Product Id cannot be empty", "GET_ESCALATION_PLAN_PRODUCT_ID_EMPTY");

        public EscalationPlanValidationMessages()
            : base(
                new[]
                {
                    EventIdRequired,
                    ChannelIdRequired,
                    ProductIdRequired
                })
        {
        }
    }
}