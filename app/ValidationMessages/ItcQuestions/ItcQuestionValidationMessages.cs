﻿using System.Collections.ObjectModel;

namespace ValidationMessages.ItcQuestions
{
    public class ItcQuestionValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public ItcQuestionValidationMessages()
            :base(new []
                 {
                     MissingQuestionText
                 })
        {}

        public static ValidationErrorMessage MissingId
        {
            get
            {
                return new ValidationErrorMessage("Question", "Missing ITC Question identifier", "ID_MISSING");
            }
        }


        public static ValidationErrorMessage MissingQuestionText
        {
            get
            {
                return new ValidationErrorMessage("Question", "Missing ITC Question content", "CONTENT_MISSING");
            }
        }
            

    }
}
