﻿using System.Collections.ObjectModel;

namespace ValidationMessages.Claims
{
    public class ClaimsHeaderValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage InvalidSearchCriteria = new ValidationErrorMessage("Search",
            "Invalid search criteria", "CLAIMHEADER_INVALID_SEARCH_CRITERIA");

        public static ValidationErrorMessage InvalidId = new ValidationErrorMessage("Id",
            "Invalid Claim Id {0}", "CLAIMHEADER_INVALID_ID");

        public static ValidationErrorMessage InvalidPartyId = new ValidationErrorMessage("PartyId",
            "Invalid Party Id", "CLAIMHEADER_INVALID_PARTYID");

        public static ValidationErrorMessage InvalidClaimItemId = new ValidationErrorMessage("ClaimItemId",
            "Invalid ClaimItemId Id", "CLAIMHEADER_INVALID_CLAIMITEMID");

        public ClaimsHeaderValidationMessages()
            : base(
                new[]
                {
                    InvalidSearchCriteria, InvalidId, InvalidClaimItemId, InvalidPartyId
                })
        {
        }
    }
}