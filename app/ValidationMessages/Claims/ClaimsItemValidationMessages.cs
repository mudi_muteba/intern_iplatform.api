﻿using System.Collections.ObjectModel;

namespace ValidationMessages.Claims
{
    public class ClaimsItemValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {

        public static ValidationErrorMessage InvalidId = new ValidationErrorMessage("ClaimsItem.Id",
            "Invalid ClaimsItem Id {0} ", "CLAIMSITEM_INVALID_ID");

        public static ValidationErrorMessage InvalidPartyId = new ValidationErrorMessage("ClaimsItem.PartyId",
            "Invalid Party Id", "CLAIMSITEM_INVALID_PARTYID");

        public static ValidationErrorMessage InvalidClaimsHeaderId = new ValidationErrorMessage("ClaimItem.ClaimsHeaderId",
            "Invalid ClaimsHeader Id {0} ", "CLAIMSITEM_INVALID_CLAIMSHEADERID");

        public static ValidationErrorMessage InvalidClaimsItemQuestionAnswerId = new ValidationErrorMessage("ClaimItem.Answers.Id",
            "Invalid ClaimItem QuestionAnswer Id {0} ", "CLAIMSITEM_INVALID_CLAIMSITEMQUESTIONANSWERID");

        public ClaimsItemValidationMessages()
            : base(
                new[]
                {
                    InvalidId, InvalidPartyId, InvalidClaimsHeaderId
                })
        {
        }
    }
}