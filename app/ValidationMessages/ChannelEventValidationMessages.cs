using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class ChannelEventValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("EditChannelEventDto.Id",
            "Id can not be empty", "CHANNELEVENT_ID_EMPTY");

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("CreateChannelEventDto.ChannelId",
            "ChannelId can not be empty", "CHANNELEVENT_CHANNELID_EMPTY");

        public static ValidationErrorMessage EventNameRequired = new ValidationErrorMessage("CreateChannelEventDto.EventName",
            "EventName can not be empty", "CHANNELEVENT_EVENTNAME_EMPTY");

        public static ValidationErrorMessage TaskNameRequired = new ValidationErrorMessage("CreateChannelEventDto.TaskName",
            "TaskName can not be empty", "CHANNELEVENT_TASKNAME_EMPTY");

        public static ValidationErrorMessage ProductCodeRequired = new ValidationErrorMessage("CreateChannelEventDto.ProductCode",
            "ProductCode can not be empty", "CHANNELEVENT_PRODUCTCODE_EMPTY");

        public static ValidationErrorMessage TasksRequired = new ValidationErrorMessage("CreateChannelEventDto.Task",
            "ChannelEvent should contain at least one Task", "CHANNELEVENT_TASK_REQUIRED");

        public ChannelEventValidationMessages()
            : base(
                new[]
                {
                    ChannelIdRequired, EventNameRequired, IdRequired, TaskNameRequired, ProductCodeRequired, TasksRequired,
                })
        {
        }
    }
}