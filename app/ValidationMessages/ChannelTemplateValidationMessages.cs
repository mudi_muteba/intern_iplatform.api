using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class ChannelTemplateValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage ChannelSystemIdRequiredForCreate =
            new ValidationErrorMessage("CreateChannelTemplateDto.ChannelSystemId", "ChannelSystemId can not be empty or zero", "CREATE_CHANNELTEMPLATE_CHANNELSYSTEMID_EMPTY");

        public static ValidationErrorMessage ChannelTemplateSystemIdRequiredForCreate =
            new ValidationErrorMessage("CreateChannelTemplateDto.SystemId", "SystemId can not be empty or zero", "CREATE_CHANNELTEMPLATE_SYSTEMID_EMPTY");

        public static ValidationErrorMessage ChannelTemplateSystemIdRequiredForEdit =
            new ValidationErrorMessage("UpdateChannelTemplateDto.SystemId", "SystemId can not be empty or zero", "EDIT_CHANNELTEMPLATE_SYSTEMID_EMPTY");

        public static ValidationErrorMessage ChannelTemplateSystemIdRequiredForDelete =
            new ValidationErrorMessage("UpdateChannelTemplateDto.SystemId", "SystemId can not be empty or zero", "DELETE_CHANNELTEMPLATE_SYSTEMID_EMPTY");


        public ChannelTemplateValidationMessages()
            : base(
                new[]
                {
                    ChannelSystemIdRequiredForCreate,
                    ChannelTemplateSystemIdRequiredForCreate,
                    ChannelTemplateSystemIdRequiredForEdit,
                    ChannelTemplateSystemIdRequiredForDelete
                }) {}
    }
}