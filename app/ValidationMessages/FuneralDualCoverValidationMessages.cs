using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class FuneralDualCoverValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdNumberRequired = new ValidationErrorMessage("GetFuneralDualCoverDto.IdNumber",
            "IdNumber cannot be empty", "FUNERALDUALCOVER_IDNUMBER_EMPTY");

        public static ValidationErrorMessage SumInsuredRequired = new ValidationErrorMessage("GetFuneralDualCoverDto.SumInsured",
            "SumInsured cannot be empty", "FUNERALDUALCOVER_SUMINSURED_EMPTY");



        public FuneralDualCoverValidationMessages()
            : base(
                new[]
                {
                    IdNumberRequired,
                    SumInsuredRequired,
                })
        {
        }
    }
}