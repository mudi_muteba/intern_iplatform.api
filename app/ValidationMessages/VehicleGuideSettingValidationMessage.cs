﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationMessages
{
    public class VehicleGuideSettingValidationMessage : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("CreateVehicleGuideSettingDto.ChannelId",
            "ChannelId can not be empty", "VEHICLEGUIDESETTING_CHANNELID_EMPTY");

        public static ValidationErrorMessage IdRequiredForDelete = new ValidationErrorMessage("DisableVehicleGuideSettingDto.Id",
            "Id can not be empty or zero", "VEHICLEGUIDESETTING_ID_EMPTY");

        public VehicleGuideSettingValidationMessage(IList<ValidationErrorMessage> list) : base(
            new []
            {
                ChannelIdRequired, IdRequiredForDelete
            })
        {
        }
    }
}
