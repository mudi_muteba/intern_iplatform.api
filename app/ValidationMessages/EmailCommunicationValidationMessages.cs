using System.Collections.ObjectModel;

namespace ValidationMessages
{
    public class EmailCommunicationValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("EmailCommunicationSettingDto.Id",
            "Id can not be empty", "EMAILCOMMUNICATION_ID_EMPTY");

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("EmailCommunicationSettingDto.ChannelId",
            "ChannelId can not be empty", "EMAILCOMMUNICATION_CHANNELID_EMPTY");

        public static ValidationErrorMessage ProductIdRequired = new ValidationErrorMessage("EmailCommunicationSettingDto.ProductId",
            "ProductId can not be empty", "EMAILCOMMUNICATION_PRODUCTID_EMPTY");

        public static ValidationErrorMessage EnvironmentRequired = new ValidationErrorMessage("EmailCommunicationSettingDto.Environment",
            "Environment can not be empty", "EMAILCOMMUNICATION_ENVIRONMENT_EMPTY");

        public static ValidationErrorMessage IdRequiredForDelete = new ValidationErrorMessage("EmailCommunicationSettingDto.Id",
            "Id can not be empty or zero", "EMAILCOMMUNICATION_ID_EMPTY");

        public EmailCommunicationValidationMessages()
            : base(
                new[]
                {
                    ChannelIdRequired, ProductIdRequired, IdRequired, EnvironmentRequired, IdRequiredForDelete
                })
        {
        }
    }
}