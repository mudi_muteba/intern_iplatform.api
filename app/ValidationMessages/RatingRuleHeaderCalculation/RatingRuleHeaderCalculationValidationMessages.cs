﻿using System.Collections.ObjectModel;

namespace ValidationMessages.RatingRuleHeaderCalculation
{
    public class RatingRuleHeaderCalculationValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public RatingRuleHeaderCalculationValidationMessages()
            : base(
                  new[]
                  {
                      ChannelIdRequired,
                      CoverIdRequired,
                      ProductIdRequired,
                      IDRequired,
                      QuestionIdRequired,
                      RatingRuleHeaderIDRequired
                  })
        {
        }

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("RatingRuleHeaderCalculationValidation.ChannelId",
           "ChannelId can not be empty", "RatingRuleHeaderCalculation_ChannelID_Empty");

        public static ValidationErrorMessage CoverIdRequired = new ValidationErrorMessage("RatingRuleHeaderCalculationValidation.CoverId",
           "CoverId can not be empty", "RatingRuleHeaderCalculation_CoverID_Empty");

        public static ValidationErrorMessage ProductIdRequired = new ValidationErrorMessage("RatingRuleHeaderCalculationValidation.ProductId",
            "ProductId can not be empty", "RatingRuleHeaderCalculation_ProductID_Empty");

        public static ValidationErrorMessage IDRequired = new ValidationErrorMessage("RatingRuleHeaderCalculationValidation.ID",
            "ID can not be empty", "RatingRuleHeaderCalculation_ID_Empty");

        public static ValidationErrorMessage QuestionIdRequired = new ValidationErrorMessage("RatingRuleHeaderCalculationValidation.QuestionId",
                    "QuestionId can not be empty", "RatingRuleHeaderCalculation_QuestionId_Empty");

        public static ValidationErrorMessage RatingRuleHeaderIDRequired = new ValidationErrorMessage("RatingRuleHeaderCalculationValidation.RatingRuleHeaderID",
                    "RatingRuleHeaderID can not be empty", "RatingRuleHeaderCalculation_RatingRuleHeaderID_Empty");
    }
}

