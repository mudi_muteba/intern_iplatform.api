using System.Collections.ObjectModel;

namespace ValidationMessages.Leads
{
    public class LeadQualityValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage InvalidId = new ValidationErrorMessage("LeadQuality.Id",
            "Id can not be empty", "LEADQUALITY_ID_EMPTY");

        public static ValidationErrorMessage UnKnownId = new ValidationErrorMessage("LeadQuality.Id",
            "Unknown Id: {0}", "LEADQUALITY_ID_UNKNOWN");

        public static ValidationErrorMessage InvalidLeadId = new ValidationErrorMessage("LeadQuality.LeadId",
            "LeadId can not be empty", "LEADQUALITY_LEADID_EMPTY");

        public static ValidationErrorMessage UnKnownLeadId = new ValidationErrorMessage("LeadQuality.LeadId",
            "Unknown LeadId: {0}", "LEADQUALITY_LEADID_UNKNOWN");

        public LeadQualityValidationMessages()
            : base(
                new[]
                {
                    InvalidId,
                    UnKnownId,
                    InvalidLeadId,
                    UnKnownLeadId
                })
        {
        }
    }
}