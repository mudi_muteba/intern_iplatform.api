using System.Collections.ObjectModel;

namespace ValidationMessages.Leads
{
    public class LeadImportValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage DeserializationFailure = new ValidationErrorMessage("LeadImportDto",
            "Failed to Deserialize XML.  Please ensure your XML are in the correct format.", "LEAD_IMPORT_NULL");

        public static ValidationErrorMessage InvalidXML = new ValidationErrorMessage("LeadImportXMLDto",
            "Invalid XML.  Please ensure your XML contains the required elements", "LEAD_IMPORT_XML_INVALID");

        public static ValidationErrorMessage InvalidClient = new ValidationErrorMessage("LeadImportDto.Client",
            "Client data not present in the file.", "LEAD_IMPORT_CLIENT_EMPTY");

        public static ValidationErrorMessage InvalidClientLastName = new ValidationErrorMessage("LeadImportDto.Client.LastName",
            "Client surname is not present in the file.", "LEAD_IMPORT_CLIENT_LASTNAME_EMPTY");

        public static ValidationErrorMessage InvalidClientFirstName = new ValidationErrorMessage("LeadImportDto.Client.FirstName",
            "Client firstname is not present in the file.", "LEAD_IMPORT_CLIENT_FIRSTNAME_EMPTY");

        public static ValidationErrorMessage InvalidClientContact = new ValidationErrorMessage("LeadImportDto.Client.HomeTelephoneNumber",
            "No contact number for the client present in the file.", "LEAD_IMPORT_CLIENT_CONTACT_EMPTY");

        public static ValidationErrorMessage InvalidClientEmail = new ValidationErrorMessage("LeadImportDto.Client.EmailAddress",
            "No contact email for the client present in the file.", "LEAD_IMPORT_CLIENT_EMAIL_EMPTY");

        public static ValidationErrorMessage ImportFailure = new ValidationErrorMessage("LeadImportExcelDto",
            "Unable to read the file.  Please upload the excel again", "LEAD_IMPORT_EXCEL_NULL");

        public static ValidationErrorMessage InvalidFormat = new ValidationErrorMessage("LeadImportExcelDto",
           "Incorrect excel format {0}.  Please ensure your excel is in the correct format {1}.", "LEAD_IMPORT_EXCEL_FORMAT");

        public static ValidationErrorMessage InvalidDescription = new ValidationErrorMessage("LeadImportExcelDto",
           "Invalid Description.  Please ensure the description is not empty.", "LEAD_IMPORT_EXCEL_DESCRIPTION");

        public static ValidationErrorMessage InvalidCampaignId = new ValidationErrorMessage("LeadImport",
           "Invalid CampaignId.  Please ensure the CampaignId is not empty.", "LEAD_IMPORT_CAMPAIGNID");

        public static ValidationErrorMessage InvalidChannelId = new ValidationErrorMessage("LeadImport",
           "Invalid ChannelId.  Please ensure the ChannelId is not empty.", "LEAD_IMPORT_CAMPAIGNID");

        public static ValidationErrorMessage EmptyXML = new ValidationErrorMessage("LeadImportXMLDto",
            "XML file is empty.  Please ensure your XML is in the correct format.", "LEAD_IMPORT_XML_NULL");

        public static ValidationErrorMessage InvalidReference = new ValidationErrorMessage("LeadImport",
            "Invalid Reference.  Please ensure your reference is not empty", "LEAD_IMPORT_REFERENCE_INVALID");

        public static ValidationErrorMessage IncompleteRow = new ValidationErrorMessage("LeadImportExcelDto.Leads",
            "Invalid Row.  Please ensure that row {0} has all information required", "LEAD_IMPORT_EXCEL_ROW_INCOMPLETE");

        public static ValidationErrorMessage IncompleteFirstname = new ValidationErrorMessage("LeadImportExcelDto.Leads",
            "Invalid firstname.  Please ensure the firstname is not empty in row {0}", "LEAD_IMPORT_EXCEL_FIRSTNAME_EMPTY");

        public static ValidationErrorMessage IncompleteSurname = new ValidationErrorMessage("LeadImportExcelDto.Leads",
            "Invalid surname.  Please ensure the Surname is not empty in row {0}", "LEAD_IMPORT_EXCEL_SURNAME_EMPTY");

        public static ValidationErrorMessage IncompleteEmail = new ValidationErrorMessage("LeadImportExcelDto.Leads",
            "Invalid email address.  Please ensure the email address is not empty in row {0}", "LEAD_IMPORT_EXCEL_EMAIL_EMPTY");

        public static ValidationErrorMessage ChannelNotExist = new ValidationErrorMessage("LeadImportDto.CompanyCode",
            "No Channel exist for this companyCode {0}");

        public LeadImportValidationMessages()
            : base(
                new[]
                {
                    DeserializationFailure, InvalidXML, InvalidClient, InvalidClientLastName, InvalidClientFirstName, InvalidClientContact,
                    ImportFailure, InvalidFormat, InvalidDescription, InvalidCampaignId, ChannelNotExist, InvalidChannelId, EmptyXML, InvalidReference,
                    IncompleteRow, IncompleteFirstname, IncompleteSurname, IncompleteEmail
                })
        {
        }
    }
}