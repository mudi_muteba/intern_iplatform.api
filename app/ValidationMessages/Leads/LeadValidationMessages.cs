using System.Collections.ObjectModel;

namespace ValidationMessages.Leads
{
    public class LeadValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage InvalidId = new ValidationErrorMessage("Lead.Id",
            "Id can not be empty", "LEAD_ID_EMPTY");

        public static ValidationErrorMessage UnKnownId = new ValidationErrorMessage("Lead.Id",
            "Unknown Id: {0}", "LEAD_ID_UNKNOWN");

        public static ValidationErrorMessage LeadDoesNotExist = new ValidationErrorMessage("Lead.Id",
            "Lead does not exist", "LEAD_NOT_EXIST");

        public static ValidationErrorMessage InvalidPartyId = new ValidationErrorMessage("Lead.PartyId",
            "Invalid Id: {0}", "LEAD_PARTYID_EMPTY");

        public static ValidationErrorMessage InvalidChannelId = new ValidationErrorMessage("Lead.ChannelId",
            "Invalid Id: {0}", "LEAD_CHANNELID_EMPTY");

        public static ValidationErrorMessage InvalidCampaignId = new ValidationErrorMessage("Lead.CampaignId",
            "Invalid Id: {0}", "LEAD_CAMPAIGNID_INVALID");

        public static ValidationErrorMessage CampaignIdEmpty = new ValidationErrorMessage("Lead.CampaignId",
            "CampiagnId can not be empty", "LEAD_CAMPAIGNID_EMPTY");

        public static ValidationErrorMessage UnKnownPartyId = new ValidationErrorMessage("Lead.PartyId",
            "Unknown Id: {0}", "LEAD_PARTYID_UNKNOWN");

        public static ValidationErrorMessage UnKnownChannelId = new ValidationErrorMessage("Lead.ChannelId",
            "Unknown Id: {0}", "LEAD_CHANNELID_UNKNOWN");

        public static ValidationErrorMessage UnKnownCampaignId = new ValidationErrorMessage("Lead.CampaignId",
            "Unknown Id: {0}", "LEAD_CAMPAIGNID_UNKNOWN");

        public static ValidationErrorMessage EmptyCampaignReference = new ValidationErrorMessage("SubmitLeadDto.CampaignReference",
            "CampaignReference cannot be empty", "SUBMIT_LEAD_CAMPAIGNREFERENCE_EMPTY");

        public static ValidationErrorMessage EmptySubmitInsuredInfo = new ValidationErrorMessage("SubmitLeadDto.InsuredInfo",
            "InsuredInfo cannot be empty", "SUBMIT_LEAD_INSUREDINFO_EMPTY");

        public static ValidationErrorMessage EmptySubmitLeadRatingItems = new ValidationErrorMessage("SubmitLeadDto.Request.Items",
            "Rating items cannot be empty", "SUBMIT_LEAD_REQUEST_ITEMS_EMPTY");

        public static ValidationErrorMessage SubmitLeadRatingItemDeserialisation = new ValidationErrorMessage("SubmitLeadDto.Request.Items",
            "Rating items cannot be deserialised", "SUBMIT_LEAD_REQUEST_ITEMS_FAILED_DESERIALISATION");

        public static ValidationErrorMessage LeadCallCentreCodesEmpty = new ValidationErrorMessage("UpdateLeadStatusDto.LeadCallCentreCodeId",
            "LeadCallCentreCode cannot be empty", "LEAD_CALLCENTRECODE_EMPTY");

        public static ValidationErrorMessage UserIdEmpty = new ValidationErrorMessage("GetNextLeadDto.UserId",
            "UserId cannot be empty", "LEAD_USER_EMPTY");

        public static ValidationErrorMessage EmptyCampaignList = new ValidationErrorMessage("AssignLeadCampaigns.CampaignList",
            "Campaign List cannot be empty", "LEAD_CAMPAIGNLIST_EMPTY");

        public static ValidationErrorMessage LeadIdEmpty = new ValidationErrorMessage("Lead.Id",
            "LeadId can not be empty", "LEAD_ID_EMPTY");

        public LeadValidationMessages()
            : base(
                new[]
                {
                    InvalidId,
                    UnKnownId,
                    InvalidPartyId,
                    InvalidChannelId,
                    InvalidCampaignId,
                    CampaignIdEmpty,
                    UnKnownPartyId,
                    UnKnownChannelId,
                    UnKnownCampaignId,
                    EmptyCampaignReference,
                    EmptySubmitInsuredInfo,
                    EmptySubmitLeadRatingItems,
                    SubmitLeadRatingItemDeserialisation,
                    LeadCallCentreCodesEmpty,
                    UserIdEmpty,
                    EmptyCampaignList
                })
        {
        }
    }
}