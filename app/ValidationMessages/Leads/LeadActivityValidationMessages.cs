using System.Collections.ObjectModel;

namespace ValidationMessages.Leads
{
    public class LeadActivityValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage InvalidId = new ValidationErrorMessage("DisableLeadActivityDto.Id",
            "Id can not be empty", "LEADACTIVITY_ID_EMPTY");

        public static ValidationErrorMessage UnKnownId = new ValidationErrorMessage("DisableLeadActivityDto.Id",
            "Unknown Id: {0}", "LEADACTIVITY_ID_UNKNOWN");

        public static ValidationErrorMessage ActivityTypeExists = new ValidationErrorMessage("ActivityType",
            "Lead Activity of type: {0} & with lead id: {1} already exists", "LEADACTIVITY_OF_TYPE_EXISTS");

        public LeadActivityValidationMessages()
            : base(
                new[]
                {
                    InvalidId,
                    UnKnownId,
                    ActivityTypeExists
                })
        {
        }
    }
}