﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationMessages
{
    public class DisplaySettingValidationMessages : ReadOnlyCollection<ValidationErrorMessage>
    {
        public static ValidationErrorMessage IdRequired = new ValidationErrorMessage("EditDisplaySettingDto.Id",
            "Id can not be empty", "DISPLAYSETTING_ID_EMPTY");

        public static ValidationErrorMessage ChannelIdRequired = new ValidationErrorMessage("CreateDisplaySettingDto.ChannelId",
            "ChannelId can not be empty", "DISPLAYSETTING_CHANNELID_EMPTY");

        public static ValidationErrorMessage ProductIdRequired =new ValidationErrorMessage("CreateDisplaySettingDto.ProductId",
                "ProductId can not be empty", "DISPLAYSETTING_PRODUCTID_EMPTY");

        public DisplaySettingValidationMessages() : base(new []
        {
            IdRequired,
            ChannelIdRequired,
            ProductIdRequired
        })
        {
        }
    }
}
