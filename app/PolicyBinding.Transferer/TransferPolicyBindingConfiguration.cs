﻿using System;
using System.Collections.Generic;
using Domain.QuoteAcceptance.Products;
using Workflow.Messages;
using Workflow.PolicyBinding.Domain;
using Domain.PolicyBindings.Messages;
using Workflow.PolicyBinding.Transferer.PolicyBinding;

namespace Workflow.PolicyBinding.Transferer
{
    public class TransferPolicyBindingConfiguration : IConfigureTransferOfPolicyBinding
    {
        public IDictionary<IEnumerable<string>, Func<SubmitPolicyBindingMessage, WorkflowEngine, ITransferPolicyBinding>> Insurers
        {
            get
            {
                return new Dictionary
                    <IEnumerable<string>, Func<SubmitPolicyBindingMessage, WorkflowEngine, ITransferPolicyBinding>>()
                {
                    {IdsProducts.Codes, TransferPolicyBindingToIds.ToIds},
                    {HollardEasyProducts.Codes, TransferPolicyBindingToHollard.ToHollardEasy},

                    //TODO policybinding for other products as and when required.
                };
            }
        }
    }
}
 