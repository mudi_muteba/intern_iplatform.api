﻿using System;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.PolicyBinding.Domain;
using Domain.PolicyBindings.Messages;
using Workflow.PolicyBinding.IDS.IDSService;
using Workflow.PolicyBinding.IDS.PolicyBinding;
using Domain.PolicyBindings.Metadata;
using System.ServiceModel;

namespace Workflow.PolicyBinding.Transferer.PolicyBinding
{
    public class TransferPolicyBindingToIds : ITransferPolicyBinding
    {
        private static readonly ILog m_Log = LogManager.GetLogger<TransferPolicyBindingToIds>();
        private readonly ITransferPolicyBindingToInsurer m_ServiceProvider;
        private readonly SubmitPolicyBindingMessage m_Message;
        private readonly WorkflowEngine m_Workflow;

        private TransferPolicyBindingToIds(SubmitPolicyBindingMessage message, ITransferPolicyBindingToInsurer serviceProvider, WorkflowEngine workflow)
        {
            m_Message = message;
            m_ServiceProvider = serviceProvider;
            m_Workflow = workflow;
        }

        public ITransferPolicyBinding Transfer()
        {
            try
            {
                m_Log.InfoFormat("Transferring Policy binding to IDS with Product {0} and Request ID {1}", m_Message.Dto.RequestInfo.ProductCode, m_Message.Dto.RequestId);
                Result = m_ServiceProvider.Transfer(m_Message) ?? new PolicyBindingTransferResult(false);
            }
            catch (Exception ex)
            {
                m_Log.ErrorFormat("An error occurred transferring Policy binding to IDS because of {0}", ex, ex.Message);
                m_ServiceProvider.ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(ex.Message, ex.Print(), m_Message.Dto.RequestId, InsurerName.Ids.Name(), m_Message.GetMetadata<PolicyBindingMetadata>().ChannelId, m_Message.GetMetadata<PolicyBindingMetadata>().Mail, m_Message.Dto.Environment));
                m_ServiceProvider.ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, GetType().Name, m_Message.Dto.RequestId, ex, m_Message.GetMetadata<PolicyBindingMetadata>().ChannelId));
                Result = new PolicyBindingTransferResult(false);
            }

            m_ServiceProvider.ExecutionPlan.Execute(m_Workflow);
            return this;
        }

        private static IdsServiceProvider GetProvider(IIdsServiceClient client, SubmitPolicyBindingMessage message)
        {
            m_Log.InfoFormat("---------------------------------Using Credentials---------------------------------");
            m_Log.InfoFormat("Using URL: {0}", message.GetMetadata<PolicyBindingMetadata>().ApiMetadata.Url);
            m_Log.InfoFormat("---------------------------------Using Credentials---------------------------------");

            return new IdsServiceProvider(client, new PolicyBindingFactory(message), message.RetryStrategy, message.ExecutionPlan,
                message.GetMetadata<PolicyBindingMetadata>().Mail, message.Dto.ChannelInfo.SystemId);
        }

        public PolicyBindingTransferResult Result { get; private set; }

        public static readonly Func<SubmitPolicyBindingMessage, WorkflowEngine, ITransferPolicyBinding> ToIds =
            (message, workflow) =>
                new TransferPolicyBindingToIds(message, GetProvider(new CommandClient(new BasicHttpBinding(), new EndpointAddress(message.GetMetadata<PolicyBindingMetadata>().ApiMetadata.Url)),
                        message), workflow);
    }
}