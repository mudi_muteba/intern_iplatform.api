﻿using System;
using Common.Logging;
using Domain.Base.Extentions;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Workflow.Messages;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Workflow.PolicyBinding.Domain;
using Domain.PolicyBindings.Messages;
using Domain.PolicyBindings.Metadata;
using Domain.QuoteAcceptance.Metadata.Insurers;
using HollardEasyPolicyBindingTransfer.HollardEasyPolicyBindingService;

namespace Workflow.PolicyBinding.Transferer.PolicyBinding
{
    public class TransferPolicyBindingToHollard : ITransferPolicyBinding
    {
        private static readonly ILog m_Log = LogManager.GetLogger<TransferPolicyBindingToIds>();
        private readonly ITransferPolicyBindingToInsurer m_ServiceProvider;
        private readonly SubmitPolicyBindingMessage m_Message;
        private readonly WorkflowEngine m_Workflow;

        private TransferPolicyBindingToHollard(SubmitPolicyBindingMessage message, ITransferPolicyBindingToInsurer serviceProvider, WorkflowEngine workflow)
        {
            m_Message = message;
            m_ServiceProvider = serviceProvider;
            m_Workflow = workflow;
        }

        public ITransferPolicyBinding Transfer()
        {
            try
            {
                m_Log.InfoFormat("Transferring Lead to Hollard with Easy Product {0} and Request ID {1}", m_Message.Dto.RequestInfo.ProductCode,
                    m_Message.Dto.RequestInfo.InsurerReference);
                Result = m_ServiceProvider.Transfer(m_Message) ?? new PolicyBindingTransferResult(false);
            }
            catch (Exception ex)
            {
                m_Log.ErrorFormat("An error occurred transferring Easy Policy binding to Hollard  because of {0}", ex, ex.Message);
                m_ServiceProvider.ExecutionPlan.AddMessage(LeadTransferMessage.CreateErrorMailMessage(ex.Message, ex.Print(), m_Message.Dto.RequestId, InsurerName.HollardEasy.Name(), m_Message.GetMetadata<PolicyBindingMetadata>().ChannelId, m_Message.GetMetadata<PolicyBindingMetadata>().Mail, m_Message.Dto.Environment));
                m_ServiceProvider.ExecutionPlan.AddFailureMessage(LeadTransferMessage.CreateException(ex.Message, GetType().Name, m_Message.Dto.RequestId, ex, m_Message.GetMetadata<PolicyBindingMetadata>().ChannelId));
                Result = new PolicyBindingTransferResult(false);
            }

            m_ServiceProvider.ExecutionPlan.Execute(m_Workflow);
            return this;
        }

        private static HollardEasyPolicyBindingServiceProvider GetProvider(IHollardTiaClient client, SubmitPolicyBindingMessage message)
        {
            var policyBindingMetadata = message.GetMetadata<PolicyBindingMetadata>();
            m_Log.InfoFormat("---------------------------------Using Credentials---------------------------------");
            m_Log.InfoFormat("Using URL: {0}", policyBindingMetadata.ApiMetadata.Url);
            m_Log.InfoFormat("Using AuthenticationToken: {0}", policyBindingMetadata.ApiMetadata.AuthenticationToken);
            m_Log.InfoFormat("---------------------------------Using Credentials---------------------------------");

            client.SetAuthorisationHeader(policyBindingMetadata.ApiMetadata.AuthenticationToken);

            return new HollardEasyPolicyBindingServiceProvider(
                client,
                message.RetryStrategy,
                message.ExecutionPlan,
                policyBindingMetadata.Mail,
                message.Dto.ChannelInfo.SystemId,
                policyBindingMetadata.GetTaskMetadata<LeadTransferHollardEasyTaskMetadata>().AcceptancEmailAddress);
        }
        public PolicyBindingTransferResult Result { get; private set; }

        public static readonly Func<SubmitPolicyBindingMessage, WorkflowEngine, ITransferPolicyBinding> ToHollardEasy =
            (message, workflow) =>
                new TransferPolicyBindingToHollard(message,
                                                    GetProvider(new HollardTiaClient(message.GetMetadata<PolicyBindingMetadata>().ApiMetadata.Url), message),
                                                    workflow);
    }
}