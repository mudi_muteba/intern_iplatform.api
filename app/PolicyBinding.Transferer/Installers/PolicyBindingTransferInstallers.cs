﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Workflow.PolicyBinding.Domain;
using Workflow.PolicyBinding.Transferer.Factories;
using System;
using Common.Logging;

namespace Workflow.PolicyBinding.Transferer.Installers
{
    
    public class PolicyBindingTransferInstallers : IWindsorInstaller
    {
        private static readonly ILog log = LogManager.GetLogger<PolicyBindingTransferInstallers>();
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            Console.Write("Registering ICreateTransferOfPolicyBinding");
            container.Register(Component.For<ICreateTransferOfPolicyBinding>().ImplementedBy<PolicyBindingTransferDestinationResolver>());
            container.Register(Component.For<IConfigureTransferOfPolicyBinding>().ImplementedBy<TransferPolicyBindingConfiguration>());
            container.Register(Classes.FromAssemblyContaining<CreatePolicyBindingTransferDestination>()
            .BasedOn(typeof(ICreateTransferOfPolicyBinding< , >))
            .WithServiceAllInterfaces()
            .LifestyleTransient());
        }
    }
}