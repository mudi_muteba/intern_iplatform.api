﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Workflow.Messages;
using Domain.PolicyBindings.Messages;
using Workflow.PolicyBinding.Domain;

namespace Workflow.PolicyBinding.Transferer.Factories
{
    public class CreatePolicyBindingTransferDestination : AbstractTransferOfPolicyBindingFactory<SubmitPolicyBindingMessage, WorkflowEngine>
    {
        private readonly IConfigureTransferOfPolicyBinding m_Configuration;

        public CreatePolicyBindingTransferDestination(IConfigureTransferOfPolicyBinding configuration)
        {
            m_Configuration = configuration;
        }

        public override IEnumerable<ITransferPolicyBinding> Create(SubmitPolicyBindingMessage message, WorkflowEngine workflow)
        {
            var insurer = m_Configuration.Insurers
                .Where(w => w.Key.Any(productCode => productCode.Equals(message.Dto.RequestInfo.ProductCode, StringComparison.CurrentCultureIgnoreCase)))
                .ToList();

            if (!insurer.Any())
            {
                throw new Exception(string.Format("Cannot find an insurer to transfer lead with product code {0}", message.Dto.RequestInfo.ProductCode));
            }
                

            return insurer.Select(s => s.Value(message, workflow));
        }
    }
}