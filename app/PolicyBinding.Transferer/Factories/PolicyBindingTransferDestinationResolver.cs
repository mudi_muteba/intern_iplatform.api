﻿using System.Collections.Generic;
using Castle.Windsor;
using Workflow.PolicyBinding.Domain;

namespace Workflow.PolicyBinding.Transferer.Factories
{
    public class PolicyBindingTransferDestinationResolver : ICreateTransferOfPolicyBinding
    {
        private readonly IWindsorContainer m_Container;

        public PolicyBindingTransferDestinationResolver(IWindsorContainer container)
        {
            m_Container = container;
        }

        public IEnumerable<ITransferPolicyBinding> Create(object message, object workflow)
        {
            var type = message.GetType();
            var executoryType = typeof (ICreateTransferOfPolicyBinding).MakeGenericType(type);
            var executor = (ICreateTransferOfPolicyBinding)m_Container.Resolve(executoryType);
            return ExecuteHandler(message, workflow, executor);
        }

        private IEnumerable<ITransferPolicyBinding> ExecuteHandler(object message, object workflow, ICreateTransferOfPolicyBinding executor)
        {
            try
            {
                return executor.Create(message, workflow);
            }
            finally
            {
                m_Container.Release(executor);
            }
        }

    }
}