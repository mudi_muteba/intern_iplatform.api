﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.AuditUserLog
{
    public class AuditUserLogDto
    {
        public int EndpointChannelId { get; set; }

        public string RequestUrl { get; set; }

        public DateTime Timestamp { get; set; }

        public string Username { get; set; }

        public string Event { get; set; }

        public Guid SystemId { get; set; }

        public string EnvironmentType { get; set; }
    }
}
