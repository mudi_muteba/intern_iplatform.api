﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SalesForce
{
    public class SalesForceQuoteStatusDto : Resource, IExecutionDto, IAffectExistingEntity, ISalesForceStatusDto
    {
        public string FailureReason;

        public int PartyId { get; set; }
        public int QuoteId { get; set; }
        public int ProposalHeaderId { get; set; }
        public string QuoteStatus { get; set; }
        public string QuoteAmount { get; set; }
        public string QuoteNumber { get; set; }
        public string QuotedProduct { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}