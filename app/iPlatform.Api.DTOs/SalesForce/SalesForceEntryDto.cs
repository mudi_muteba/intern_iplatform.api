﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SalesForce
{
    public class SalesForceEntryDto : Resource
    {
        public SalesForceEntryDto()
        {
        }
        
        public  int PartyId { get; set; }
        public  Guid MessageId { get; set; }
        public  bool Delayed { get; set; }
        public  string Status { get; set; }
        public  DateTime ModifiedOn { get; set; }
        public  DateTime CreatedOn { get; set; }
        public bool Suspended { get; set; }
    }
}