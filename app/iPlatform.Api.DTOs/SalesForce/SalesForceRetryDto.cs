using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SalesForce
{
    public class SalesForceRetryDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public string ObjectType { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}