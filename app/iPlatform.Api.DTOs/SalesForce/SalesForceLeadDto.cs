﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Remoting.Contexts;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SalesForce
{
    public class SalesForceLeadDto : Resource, IExecutionDto
    {
        [Required]
        public int PartyId { get; set; }
        public string Salutation { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MobilePhone { get; set; }
        public string HomePhone { get; set; }
        public string Phone { get; set; }
        public string QuoteAmount { get; set; }
        public string QuoteNumber { get; set; }
        public string QuoteStatus { get; set; }
        public string QuotedProduct { get; set; }
        public string CMPID { get; set; }
        public string Email { get; set; }
        public string IdNumber { get; set; }
        public string LocalTimeZone { get; set; }
        public string Status { get; set; }
        public string DateOfSubmission { get; set; }
        public string SubmittedFromWebsitePage { get; set; }

        public SalesForceSubmitDto GetSubmitDto(string source = "iPlatform")
        {
            return new SalesForceSubmitDto
            {
                Salutation = Salutation,
                LastName = LastName,
                FirstName = FirstName,
                MobilePhone = MobilePhone,
                GCC__CAT_HomePhone__c = HomePhone,
                Phone = Phone,
                VMSA_QuoteAmount__c = QuoteAmount,
                VMSA_QuoteNumber__c = QuoteNumber,
                VMSA_QuotedProduct__c = QuotedProduct,
                VMSA_QuoteStatus__c = QuoteStatus,
                VMSA_CMPID__c = CMPID,
                Email = Email,
                VMSA_NationalId__c = IdNumber,
                GCC__CAT_LocalTimeZone__c = LocalTimeZone,
                Status = Status,
                VMSA_DateOfSubmission__c = DateOfSubmission,
                VMSA_SubmittedFromWebsitePage__c = SubmittedFromWebsitePage,
                LeadSource = source
            };
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}

