namespace iPlatform.Api.DTOs.SalesForce
{
    public class SalesForceExternalResponseDto
    {
        public string Response { get; set; }
    }
}