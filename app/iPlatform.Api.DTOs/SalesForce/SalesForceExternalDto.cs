using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SalesForce
{
    public class SalesForceExternalDto : Resource, IExecutionDto
    {
        public string ExternalReference { get; set; }
        public string ObjectType { get; set; }
        public string Model { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}