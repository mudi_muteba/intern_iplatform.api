﻿using System.IO.MemoryMappedFiles;

namespace iPlatform.Api.DTOs.SalesForce
{
    public class SalesForceSubmitDto
    {
        public string Salutation { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MobilePhone { get; set; }
        public string GCC__CAT_HomePhone__c { get; set; }
        public string Phone { get; set; }
        public string VMSA_QuoteAmount__c { get; set; }
        public string VMSA_QuoteNumber__c { get; set; }
        public string VMSA_QuoteStatus__c { get; set; }
        public string VMSA_QuotedProduct__c { get; set; }
        public string VMSA_CMPID__c { get; set; } //VMSA_OnlineMarketingDesignation__c
        public string Email { get; set; }
        public string VMSA_NationalId__c { get; set; }
        public string GCC__CAT_LocalTimeZone__c { get; set; }
        public string Status { get; set; }
        public string VMSA_DateOfSubmission__c { get; set; }
        public string VMSA_SubmittedFromWebsitePage__c { get; set; }
        public string LeadSource { get; set; }

        public void UpdateStatus(SalesForceLeadStatusDto dto, string status, string submittedFromWebsitePage, string leadSource)
        {
            Status = status;
            VMSA_SubmittedFromWebsitePage__c = submittedFromWebsitePage;
            LeadSource = leadSource;
            VMSA_QuoteStatus__c = dto.QuoteStatus;
            VMSA_CMPID__c = dto.CMPID;
        }
    }
}