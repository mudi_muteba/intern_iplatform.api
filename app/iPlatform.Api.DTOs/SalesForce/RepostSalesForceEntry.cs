using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SalesForce
{
    public class RepostSalesForceEntryDto : Resource, IExecutionDto, IAffectExistingEntity
    {

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}