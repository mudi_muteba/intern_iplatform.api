﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SalesForce
{
    public class EditSalesForceEntryDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public EditSalesForceEntryDto()
        {
        }

        //represents the PartyId
        public new int Id { get; set; }

        public bool Delayed { get; set; }
        public bool Suspended { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}