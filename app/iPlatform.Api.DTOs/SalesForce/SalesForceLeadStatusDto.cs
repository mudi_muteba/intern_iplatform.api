﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SalesForce
{
    public class SalesForceLeadStatusDto : Resource, IExecutionDto, IAffectExistingEntity, ISalesForceStatusDto
    {
        public string QuoteAmount { get; set; }
        public string QuoteNumber { get; set; }
        public string QuotedProduct { get; set; }
        public string CMPID { get; set; }
        public string FailureReason { get; set; }

        //Properties not Mapped to SF
        public int PartyId { get; set; }
        public int QuoteId { get; set; }
        public int ProposalHeaderId { get; set; }
        public string QuoteStatus { get; set; }
        public virtual Guid MessageId { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }

    public interface ISalesForceStatusDto
    {
         int PartyId { get; set; }
         int QuoteId { get; set; }
         int ProposalHeaderId { get; set; }
        string QuoteStatus { get; set; }
        string QuoteAmount { get; set; }
        string QuoteNumber { get; set; }
        string QuotedProduct { get; set; }




    }
}