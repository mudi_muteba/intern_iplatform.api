﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using MasterData;

namespace iPlatform.Api.DTOs.AdditionalMembers
{
    public class ListAdditionalMemberDto :Resource, ICultureAware
    {
        public MemberRelationship MemberRelationship { get; set; }
        public string Initials { get; set; }
        public string Surname { get; set; }
        public string IdNumber { get; set; }
        public bool IsStudent { get; set; }
        public Gender Gender { get; set; }
        public DateTimeDto DateOnCover { get; set; }
        public MoneyDto SumInsured { get; set; }
        public int ProposalDefinitionId { get; set; }
        public ListProposalDefinitionDto ProposalDefinition { get; set; }
        public DateTimeDto DateOfBirth { get; set; }
    }
}
