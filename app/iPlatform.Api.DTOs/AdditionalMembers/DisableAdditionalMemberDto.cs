﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.AdditionalMembers
{
    public class DisableAdditionalMemberDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {

        public DisableAdditionalMemberDto()
        {
            Context = DtoContext.NoContext();
        }

        public int Id { get; set; }
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {

        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                validation.Add(AdditionalMemberValidationMessages.IdRequired);
            }

            return validation;
        }

    }
}
