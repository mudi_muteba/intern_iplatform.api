﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Campaigns;
using MasterData;
using ValidationMessages;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Leads.Campaign
{
    public class UpdateLeadStatusDto : AttributeValidationDto, IExecutionDto
    {
        public UpdateLeadStatusDto()
        {
            Context = DtoContext.NoContext();
        }
        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public int Id { get; set; }
        public int CampaignId { get; set; }
        public int LeadCallCentreCodeId { get; set; }
        public LeadCallCentreCode CallCentreCode { get; set; }
        public string FaxNumber { get; set; }
        public string Comments { get; set; }
        public DateTime CallBackDate { get; set; }
        public LeadCallCentreCodeDto CallCentreCodeDto { get; set; }
        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id == 0)
                validation.Add(LeadValidationMessages.InvalidId);

            if (CampaignId == 0)
                validation.Add(LeadValidationMessages.CampaignIdEmpty);

            if (CallCentreCode == null)
                validation.Add(LeadValidationMessages.LeadCallCentreCodesEmpty);

            return validation;
        }
    }
}
