﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Leads.Campaign
{
    public class CallCentreStatusEventDetailDto
    {
        public CallCentreStatusEventDetailDto() { }
        public CallCentreStatusEventDetailDto(string code, string description)
        {
            Code = code;
            Description = description;
        }

        public string Code { get; set; }
        public string Description { get; set; }
    }
}
