﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Leads.Campaign
{
    public class CallCentreEventDetailsDto
    {
        public CallCentreEventDetailsDto()
        {
            Details = new List<CallCentreStatusEventDetailDto>();
        }

        public List<CallCentreStatusEventDetailDto> Details { get; set; }
    }
}
