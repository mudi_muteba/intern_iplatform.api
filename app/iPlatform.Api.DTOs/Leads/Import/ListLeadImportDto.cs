﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Leads
{
    public class ListLeadImportDto : Resource
    {
        public Guid LeadImportReference { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string Status { get; set; }
        public ListCampaignDto Campaign { get; set; }
        public ListUserDto CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public DateTime? DateImported { get; set; }
        public int TotalRecords { get; set; }
        public int TotalImported { get; set; }
        public int TotalErrors { get; set; }

        public ListLeadImportDto()
        {

        }

       


    }
}