﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Leads
{
    public class LeadImportSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public Guid Reference { get; set; }

        public LeadImportSearchDto()
        {
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Reference == Guid.Empty)
                validation.Add(LeadImportValidationMessages.InvalidReference);

            return validation;
        }
    }
}