﻿using System.Xml.Serialization;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Leads
{
    [XmlRoot("Campaign")]
    public class LeadImportCampaign
    {
        public string Reference { get; set; }
    }

    [XmlRoot("ApplicationData")]
    public class LeadImportDto : IExecutionDto
    {
        public DtoContext Context { get; set; }

        public LeadImportDto()
        {
            Context = DtoContext.NoContext();
            Campaign = new LeadImportCampaign();
            Deal = new LeadImportDeal();
            User = new LeadImportUser();
            Client = new LeadImportClient();
            Driver = new LeadImportDriver();
            Vehicle = new LeadImportVehicle();
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public LeadImportCampaign Campaign { get; set; }
        public LeadImportDeal Deal { get; set; }
        public LeadImportUser User { get; set; }
        public LeadImportClient Client { get; set; }
        public LeadImportDriver Driver { get; set; }
        public LeadImportVehicle Vehicle { get; set; }
    }

    [XmlRoot("Deal")]
    public class LeadImportDeal
    {
        public string SystemMode { get; set; }
        public string CurrentDate { get; set; }
        public string PolicyNumber { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string FinanceCompanyCode { get; set; }
        public string FinanceCompanyName { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string CompanyCode { get; set; }
    }

    [XmlRoot("User")]
    public class LeadImportUser
    {
        public string UserName { get; set; }
        public string UserWorkTelephoneCode { get; set; }
        public string UserWorkTelephoneNumber { get; set; }
        public string UserEmailAddress { get; set; }
        public string UserFaxCode { get; set; }
        public string UserFaxNumber { get; set; }
        public string EmployeeNumber { get; set; }
        public string FIFirstName { get; set; }
        public string FILastName { get; set; }
        public string FIIDNumber { get; set; }
    }

    [XmlRoot("Client")]
    public class LeadImportClient
    {
        public string Title { get; set; }
        public string Initials { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public string IDNumber { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string HomeTelephoneCode { get; set; }
        public string HomeTelephoneNumber { get; set; }
        public string WorkTelephoneCode { get; set; }
        public string WorkTelephoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set; }
        public string PhysicalAddress1 { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
        public string PostalAddress1 { get; set; }
        public string Suburb1 { get; set; }
        public string PostCode1 { get; set; }
        public string clnCorrespondenceLanguage { get; set; }
        public string empEmploymentType { get; set; }
        public string empOccupation { get; set; }
        public string clnMaritalStatus { get; set; }
        public string PreviousInsurance { get; set; }
        public string insCardLicence { get; set; }
        public string insDriversLicenceDate { get; set; }

        public string ContactNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(MobileNumber))
                    return MobileNumber;
                if (!string.IsNullOrEmpty(HomeTelephoneNumber))
                    return string.Format("{0} {1}", HomeTelephoneCode, HomeTelephoneNumber).Trim();
                if (!string.IsNullOrEmpty(WorkTelephoneNumber))
                    return string.Format("{0} {1}", WorkTelephoneCode, WorkTelephoneNumber).Trim();
                return string.Empty;
            }
        }
    }

    [XmlRoot("Driver")]
    public class LeadImportDriver
    {
        public string drvRelation { get; set; }
        public string drvTitle { get; set; }
        public string drvInitials { get; set; }
        public string drvFirstName { get; set; }
        public string drvLastName { get; set; }
        public string drvIDNumber { get; set; }
        public string drvBirthDate { get; set; }
        public string drvGender { get; set; }
    }

    [XmlRoot("Vehicle")]
    public class LeadImportVehicle
    {
        public string VehicleRetailPrice { get; set; }
        public string VehicleAccessoryTotal { get; set; }
        public string TotalSumInsured { get; set; }
        public string VehiclePurpose { get; set; }
        public string VehicleCode { get; set; }
        public string VehicleManufacturer { get; set; }
        public string VehicleModel { get; set; }
        public string VehicleBodyStyle { get; set; }
        public string VehicleRegistrationNumber { get; set; }
        public string VehicleFirstRegistrationDate { get; set; }
        public string VehicleEngineNumber { get; set; }
        public string VehicleVINNumber { get; set; }
        public string VehicleColour { get; set; }
        public string VehicleImmobiliser { get; set; }
        public string VehicleGearlock { get; set; }
        public string VehicleTrackingDevice { get; set; }
    }
}
