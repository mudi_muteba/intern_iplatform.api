﻿using iPlatform.Api.DTOs.Base;
using MasterData;
using System;

namespace iPlatform.Api.DTOs.Leads.Import
{
    public class CreateLeadImportDto: Resource, IExecutionDto
    {
        public DtoContext Context { get; set; }

        public virtual ImportType ImportType { get; protected set; }
        public virtual string FileName { get; protected set; }
        public virtual int CampaignId { get; set; }
        public virtual int ChannelId { get; set; }
        public virtual int CreatedBy { get; protected set; }

        public CreateLeadImportDto() { }

        public CreateLeadImportDto(ImportType importType,  int channelId, 
            int campaignId, int userId,  string fileName)
        {
            ImportType = importType;
            ChannelId = channelId;
            CreatedBy = userId;
            CampaignId = campaignId;
            FileName = fileName;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }


    }
}
