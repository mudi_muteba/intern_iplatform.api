﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Leads;
using System;

namespace iPlatform.Api.DTOs.Leads
{
    public class LeadImportXMLDto : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; set; }
        public string XMLString { get; set; }
        public int CampaignId { get;  set; }
        public int ChannelId { get;  set; }
        public string FileName { get; set; }
        public Guid Reference { get; set; }

        public LeadImportXMLDto()
        {
            Context = DtoContext.NoContext();
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrEmpty(XMLString))
                validation.Add(LeadImportValidationMessages.EmptyXML);

            return validation;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

    }
}