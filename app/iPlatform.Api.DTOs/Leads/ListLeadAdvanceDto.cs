﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party;
using MasterData;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;

namespace iPlatform.Api.DTOs.Leads
{
    public class ListLeadAdvanceDto : Resource, ICultureAware
    {
        public ListLeadAdvanceDto()
        {
            Individual = new IndividualDto();
            Activities = new List<ListLeadActivityDto>();
            CorrespondencePreferences = new List<PartyCorrespondencePreferenceDto>();
        }

        public string Name { get; set; }
        public IndividualDto Individual { get; set; }
        public ActivityType LeadStatus { get; set; }
        public List<ListHomeLossHistoryDto> HomeLossHistory { get; set; }
        public List<ListMotorLossHistoryDto> MotorLossHistory { get; set; }
        public List<ListLeadActivityDto> Activities { get; set; }
        public List<PartyCorrespondencePreferenceDto> CorrespondencePreferences { get; set; }
        public LossHistoryDto LossHistoryQuestions { get; set; }

    }
}
