﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Leads.Quality
{
    public class EditLeadQualityDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public EditLeadQualityDto()
        {
            Context = DtoContext.NoContext();
        }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }
        public int Id { get; set; }
        public DtoContext Context { get; set; }
        public DateTime DateCreated { get; set; }
        public string WealthIndex { get; set; }
        public string CreditGradeNonCPA { get; set; }
        public bool? CreditActiveNonCPA { get; set; }
        public string MosaicCPAGroupMerged { get; set; }
        public string DemLSM { get; set; }
        public string FASNonCPAGroupDescriptionShort { get; set; }
        public string IdentityNumber { get; set; }
        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var list = new List<ValidationErrorMessage>();
            if (Id < 1)
                list.Add(LeadQualityValidationMessages.InvalidLeadId);

            return list;
        }
    }
}
