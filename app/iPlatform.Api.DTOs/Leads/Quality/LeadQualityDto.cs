﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Culture;
using ValidationMessages;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Leads.Quality
{
    public class LeadQualityDto : Resource, ICultureAware
    {
        public LeadQualityDto()
        {
           
        }

        public int LeadId { get; set; }
        public DateTimeDto DateCreated { get; set; }
        public string WealthIndex { get; set; }
        public string CreditGradeNonCPA { get; set; }
        public bool? CreditActiveNonCPA { get; set; }
        public string MosaicCPAGroupMerged { get; set; }
        public string DemLSM { get; set; }
        public string FASNonCPAGroupDescriptionShort { get; set; }
        public string IdentityNumber { get; set; }

    }
}
