﻿
using System;
using iPlatform.Api.DTOs.Base;
using MasterData;
using iPlatform.Api.DTOs.Leads.Quality;
using ValidationMessages;
using System.Collections.Generic;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Leads
{
    public class CreateLeadDto :  AttributeValidationDto, IExecutionDto
    {

        public void Created()
        {
            LeadStatus = LeadStatuses.Created;
            Context = DtoContext.NoContext();
        }


        public int PartyId { get; set; }

        public LeadStatus LeadStatus { get; set; }

        public int ChannelId { get; set; }
        public int CampaignId { get; set; }
        public string ExternalReference { get; set; }

        public CreateLeadQualityDto LeadQuality { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }


        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId < 1)
                validation.Add(LeadValidationMessages.InvalidChannelId);

            if (CampaignId < 1)
                validation.Add(LeadValidationMessages.InvalidCampaignId);

            if (PartyId < 1)
                validation.Add(LeadValidationMessages.UnKnownPartyId);
            

            return validation;
        }


    }
}
