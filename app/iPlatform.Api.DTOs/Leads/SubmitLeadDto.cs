﻿using System;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using iPlatform.Api.DTOs.Extensions;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Leads
{
    [XmlRoot(ElementName = "SubmitLeadDto", DataType = "string", IsNullable = true)]
    public class SubmitLeadDto : AttributeValidationDto, IExecutionDto
    {

        public InsuredInfoDto InsuredInfo { get; set; }
        public RatingRequestDto Request { get; set; }
        public RatingResultDto Result { get; set; }

        public string CampaignReference { get; set; }
        public ExternalSystemInfoDto ExternalSystemInfo { get; set; } 
        public bool HasQuoted { get; set; }
        protected bool Equals(SubmitLeadDto other)
        {
            return Equals(Request, other.Request) && Equals(InsuredInfo, other.InsuredInfo);
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((SubmitLeadDto)obj);
        }

        #region context
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
        #endregion

        public bool CanCreateProposal()
        {
            return Request != null && Request.Policy.Persons.Any() && Request.Items.Any();
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrEmpty(CampaignReference))
                validation.Add(LeadValidationMessages.EmptyCampaignReference);
            if (InsuredInfo == null)
                validation.Add(LeadValidationMessages.EmptySubmitInsuredInfo);

            if (Request == null || Request.Items == null || Request.Items.Count <= 0)
                return validation;
            else
            {
                try
                {
                    foreach (var item in Request.Items)
                    {
                        item.Answers.EnsureDeserialization();
                    }
                }
                catch (Exception)
                {
                    validation.Add(LeadValidationMessages.SubmitLeadRatingItemDeserialisation);
                }
            }

            return validation;
        }
    }
}
