﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using MasterData;

namespace iPlatform.Api.DTOs.Leads
{
    public class ListLeadSimplifiedDto : Resource, ICultureAware
    {
        public ListLeadSimplifiedDto()
        {
            Individual = new IndividualSimplifiedDto();
        }

        public string Name { get; set; }
        public IndividualSimplifiedDto Individual { get; set; }
    }
}
