﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Leads
{
   public class RegOwnerContactDto
    {
        public int PartyId { get; set; }
    }
}
