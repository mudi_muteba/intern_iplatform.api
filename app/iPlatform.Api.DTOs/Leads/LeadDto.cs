﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party;
using MasterData;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Leads.Quality;

namespace iPlatform.Api.DTOs.Leads
{
    public class LeadDto : Resource, ICultureAware
    {
        public LeadDto()
        {
            Party = new PartyDto();
            Activities = new List<ListLeadActivityDto>();
            CampaignLeadBuckets = new List<ListCampaignLeadBucketDto>();
        }

        public string Name { get; set; }
        public PartyDto Party { get; set; }
        public ActivityType LeadStatus { get; set; }
        public int LeadQualityId { get; set; }
        public LeadQualityDto LeadQuality { get; set; }
        public List<ListLeadActivityDto> Activities { get; set; }
        public LeadCallCentreCode CallCentreStatus { get; set; }
        public List<ListCampaignLeadBucketDto> CampaignLeadBuckets { get; set; }
        public string ExternalReference { get; set; }
    }
}
