﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Leads.Excel
{
    public class LeadImportExcelRowDto
    {
        public LeadImportExcelRowDto()
        {
            Cell = new List<string>();
        }

        public List<string> Cell { get; set; }
    }
}
