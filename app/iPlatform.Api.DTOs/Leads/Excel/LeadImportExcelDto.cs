﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Leads.Excel
{
    public class LeadImportExcelDto : AttributeValidationDto, IExecutionDto, IChannelAwareDto
    {
        public Guid LeadImportReference { get; set; }
        public DtoContext Context { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string Status { get; set; }
        public int CampaignId { get; set; }
        public int ChannelId { get; set; }
        public int UserId { get; set; }
        public Byte[] File { get; set; }
        public List<LeadImportExcelRowDto> Leads { get; set; } 

        public LeadImportExcelDto() { }

        public LeadImportExcelDto(Guid leadImportReference, string fileName, string description, int campaignId, int channelId, int userId, byte[] file)
        {
            LeadImportReference = leadImportReference;
            FileName = fileName;
            Description = description;
            CampaignId = campaignId;
            ChannelId = channelId;
            UserId = userId;
            File = file;
        }

        public virtual LeadImportExcelDto AddFile(Byte[] file)
        {
            File = file;
            return this;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (File == null || File.Length < 1)
                validation.Add(LeadImportValidationMessages.ImportFailure);

            if (!FileName.Contains("xlsx"))
                validation.Add(LeadImportValidationMessages.InvalidFormat);

            if (string.IsNullOrEmpty(Description))
                validation.Add(LeadImportValidationMessages.InvalidDescription);

            if (CampaignId <= 0)
                validation.Add(LeadImportValidationMessages.InvalidCampaignId);

            return validation;
        }
    }
}