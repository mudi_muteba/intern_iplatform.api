﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Leads
{
    public class LeadImportResponseDto: Resource
    {
        public int LeadId { get; set; }
        public int PartyId { get; set; }
    }
}
