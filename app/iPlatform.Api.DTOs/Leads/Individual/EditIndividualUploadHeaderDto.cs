﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Leads
{
    public class EditIndividualUploadHeaderDto : Resource, IExecutionDto
    {
        public DtoContext Context { get; private set; }
        public Guid LeadImportReference { get; set; }

        public EditIndividualUploadHeaderDto() { }

        public EditIndividualUploadHeaderDto(Guid leadImportReference)
        {
            LeadImportReference = leadImportReference;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}