﻿using System;
using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Leads
{
    public class EditIndividualUploadDetailDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public DtoContext Context { get; set; }

        public Guid LeadImportReference { get; set; }
        public int DuplicateFileRecordCount { get; set; }
        public LeadImportStatus LeadImportStatus { get; set; }
        public bool NewIndividual { get; set; }
        public string Comment { get; set; }
        public int PartyId { get; set; }
        public EditIndividualUploadDetailDto() { }

        public EditIndividualUploadDetailDto(Guid leadImportReference, int duplicateFileRecordCount, LeadImportStatus leadImportStatus, bool newIndividual)
        {
            LeadImportReference = leadImportReference;
            DuplicateFileRecordCount = duplicateFileRecordCount;
            LeadImportStatus = leadImportStatus;
            NewIndividual = newIndividual;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public EditIndividualUploadDetailDto Success()
        {
            LeadImportStatus = LeadImportStatuses.Success;
            return this;
        }

        public EditIndividualUploadDetailDto Failure()
        {
            LeadImportStatus = LeadImportStatuses.Failure;
            return this;
        }

    }
}