﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Leads
{
    public class CreateIndividualUploadHeaderDto : Resource, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public Guid LeadImportReference { get; set; }
        public int ChannelId { get; set; }
        public int UserId { get; set; }
        public int CampaignId { get; set; }
        public string FileName { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int FileRecordCount { get; set; }
        public int NewCount { get; set; }
        public int DuplicateCount { get; set; }
        public int DuplicateFileRecordCount { get; set; }
        public int? FailureCount { get; set; }
        public string LeadImportStatus { get; set; }

        public CreateIndividualUploadHeaderDto() { }

        public CreateIndividualUploadHeaderDto(Guid leadImportReference, int channelId, int userId, int campaignId, string fileName, int fileRecordCount, int newCount, int duplicateCount, int duplicateFileRecordCount, int? failureCount, string leadImportStatus)
        {
            LeadImportReference = leadImportReference;
            ChannelId = channelId;
            UserId = userId;
            CampaignId = campaignId;
            FileName = fileName;
            FileRecordCount = fileRecordCount;
            NewCount = newCount;
            DuplicateCount = duplicateCount;
            DuplicateFileRecordCount = duplicateFileRecordCount;
            FailureCount = failureCount;
            LeadImportStatus = leadImportStatus;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}