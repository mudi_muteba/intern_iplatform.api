﻿using System;
using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Leads
{
    public class IndividualUploadDetailDto : Resource, IExecutionDto
    {
        public DtoContext Context { get; set; }

        public Guid LeadImportReference { get; set; }
        public int ChannelId { get; set; }
        public int UserId { get; set; }
        public int CampaignId { get; set; }
        public string FileName { get; set; }
        public string ContactNumber { get; set; }
        public int DuplicateFileRecordCount { get; set; }
        public LeadImportStatus LeadImportStatus { get; set; }
        public bool NewIndividual { get; set; }
        public string Comment { get; set; }
        public IndividualUploadDetailDto() { }

        public IndividualUploadDetailDto(Guid leadImportReference, int channelId, int userId, 
            int campaignId, string fileName, string contactNumber, int duplicateFileRecordCount, LeadImportStatus leadImportStatus, bool newIndividual)
        {
            LeadImportReference = leadImportReference;
            ChannelId = channelId;
            UserId = userId;
            CampaignId = campaignId;
            FileName = fileName;
            ContactNumber = contactNumber;
            DuplicateFileRecordCount = duplicateFileRecordCount;
            LeadImportStatus = leadImportStatus;
            NewIndividual = newIndividual;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public IndividualUploadDetailDto Success()
        {
            LeadImportStatus = LeadImportStatuses.Success;
            return this;
        }

        public IndividualUploadDetailDto Failure()
        {
            LeadImportStatus = LeadImportStatuses.Failure;
            return this;
        }

    }
}