﻿using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Individual;
using MasterData;

namespace iPlatform.Api.DTOs.Leads
{
    public class LeadBasicDto: ICultureAware
    {
        public LeadBasicDto()
        {
        }
        public virtual int Id { get; set; }
        public string Name { get; set; }
        public IndividualDto Individual { get; set; }
        public ActivityType LeadStatus { get; set; }
    }
}
