﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Leads
{
    public class ExternalSystemInfoDto
    {
        public string CampaignReference { get; set; }
        public string CallCentreUserName { get; set; }
        public string CallCentreUserId { get; set; }
        public string SourceId { get; set; }
        public string ExternalReference { get; set; }
    }
}
