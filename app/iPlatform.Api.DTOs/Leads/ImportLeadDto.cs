﻿using System;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Sales;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;

namespace iPlatform.Api.DTOs.Leads
{
    public class ImportLeadDto : Resource, IExecutionDto
    {

        public ImportLeadDto() { }
        public Guid LeadImportReference { get; set; }
        public InsuredInfoDto InsuredInfo { get; set; }
        public RatingRequestDto Request { get; set; }
        public SalesFNIDto SalesFniRequest { get; set; }
        public DealDto DealDto { get; set; }
        public string CampaignReference { get; set; }
        public int ChannelId { get; set; }
        public int CampaignId { get; set; }
        public int UserId { get; set; }
        public ActivityType StatusActivityType { get; set; }
        public string FileName { get; set; }
        public int DuplicateFileRecordCount { get; set; }
        public bool isNew { get; set; }

        protected bool Equals(ImportLeadDto other)
        {
            return Equals(Request, other.Request) && Equals(InsuredInfo, other.InsuredInfo);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ImportLeadDto)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return 0;
            }
        }

        public bool CanCreateProposal()
        {
            return Request != null && Request.Policy.Persons.Any() && Request.Items.Any();
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}
