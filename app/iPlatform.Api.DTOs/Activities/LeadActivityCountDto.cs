﻿namespace iPlatform.Api.DTOs.Activities
{
    public class LeadActivityCountDto 
    {
        public LeadActivityCountDto()
        {
        }

        public int Count { get; set; }

    }
}
