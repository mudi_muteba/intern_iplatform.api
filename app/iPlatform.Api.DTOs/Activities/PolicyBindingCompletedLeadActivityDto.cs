﻿
using System;

namespace iPlatform.Api.DTOs.Activities
{
    public class PolicyBindingCompletedLeadActivityDto : BaseLeadActivityDto
    {
        public int UserId { get; set; }
        public int QuoteId { get; set; }
        public int ChannelId { get; set; }
        public Guid RequestId { get; set; }
        public string Comments { get; set; }

        public PolicyBindingCompletedLeadActivityDto() { }
        public PolicyBindingCompletedLeadActivityDto(int id, int campaignId, int userId, int quoteId, int channelId, Guid requestId, string comments)
        {
            QuoteId = quoteId;
            Id = id;
            CampaignId = campaignId;
            UserId = userId;
            ChannelId = channelId;
            RequestId = requestId;
            Comments = comments;
        }
    }
}
