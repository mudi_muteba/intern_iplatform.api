﻿

using iPlatform.Api.DTOs.Base;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Activities
{
    public class ImportedLeadActivityDto : BaseLeadActivityDto
    {
        public ImportedLeadActivityDto()
        {
            Existing = false;
        }
        public ImportedLeadActivityDto(int campaignId)
        {
            CampaignId = campaignId;
        }

        public ImportType ImportType { get; set; }
        public bool Existing { get; set; }
    }
}
