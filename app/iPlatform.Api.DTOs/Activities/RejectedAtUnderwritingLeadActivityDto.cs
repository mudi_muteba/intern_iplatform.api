﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Activities
{
    public class RejectedAtUnderwritingLeadActivityDto : BaseLeadActivityDto
    {
        public RejectedAtUnderwritingLeadActivityDto() { }
        public RejectedAtUnderwritingLeadActivityDto(int campaignId, int policyHeaderId, int leadId)
        {
            CampaignId = campaignId;
            PolicyHeaderId = policyHeaderId;
            LeadId = leadId;
            Id = leadId;
        }

        public int PolicyHeaderId { get; set; }
        public int LeadId { get; set; }
    }
}
