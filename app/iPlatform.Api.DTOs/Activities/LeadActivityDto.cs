﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Policy;
using iPlatform.Api.DTOs.Users;
using MasterData;
using iPlatform.Api.DTOs.Admin;

namespace iPlatform.Api.DTOs.Activities
{
    public class LeadActivityDto : Resource, ICultureAware
    {
        public LeadActivityDto()
        {
            CampaignLeadBuckets = new List<ListCampaignLeadBucketDto>();
        }

        public LeadBasicDto Lead { get; set; }
        public ChannelReferenceDto Channel { get; set; }
        public CampaignDto Campaign { get; set; }
        public DateTimeDto DateCreated { get; set; }
        public DateTimeDto DateUpdated { get; set; }
        public ListUserDto User { get; set; }
        public BasicIndividualDto Party { get; set; }
        public List<ListCampaignLeadBucketDto> CampaignLeadBuckets { get; set; }
        public ActivityTypeDto ActivityType { get; set; }
        public string ErrorMessage { get; set; }

        //From ClaimsLeadActivity
        public ClaimsHeaderDto ClaimsHeader { get; set; }

        //From CreateLeadActivity
        public string Description { get; set; }

        //From PolicyLeadActivity
        public PolicyHeaderDto PolicyHeader { get; set; }

        //From ProposalLeadActivity
        public ProposalHeaderDto ProposalHeader { get; set; }

        //From QuotedLeadActivity
        public QuoteDto Quote { get; set; }

        //From QuoteAcceptedLeadActivity
        public QuoteHeaderDto QuoteHeader { get; set; }

        //From SurveyRespondedLeadActivity
        public CustomerSatisfactionSurveyDto CustomerSatisfactionSurvey { get; set; }

        //From ImportedLeadActivity
        public ImportType ImportType { get; set; }
        public bool Existing { get; set; }
    }
}
