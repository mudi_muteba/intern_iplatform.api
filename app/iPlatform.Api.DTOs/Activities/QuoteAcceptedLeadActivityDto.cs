﻿namespace iPlatform.Api.DTOs.Activities
{
    public class QuoteAcceptedLeadActivityDto : LeadActivityDto
    {
        public QuoteAcceptedLeadActivityDto()
        {
        }

        public int QuoteId { get; set; }
    }
}
