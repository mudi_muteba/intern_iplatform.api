﻿using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Activities
{
    public class LeadActivitySearchDto : BaseCriteria
    {
        public int? AgentId { get; set; }
        public int? PartyId { get; set; }
        public int? LeadId { get; set; }
        public int MaxItems { get; set; }
        public ActivityType ActivityType { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public int CampaignId { get; set; }
        public int ChannelId { get; set; }

        public LeadActivitySearchDto() { }
    }
}
