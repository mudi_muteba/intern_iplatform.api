﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Activities
{
    public class DisableLeadActivityDto: IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
        public DisableLeadActivityDto() { }
        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                list.Add(LeadActivityValidationMessages.InvalidId);
            }

            return list;
        }
    }
}
