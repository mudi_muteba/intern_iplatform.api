﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Activities
{
    public class ListLeadActivityDto : Resource, ICultureAware
    {
        public ListLeadActivityDto()
        {
        }

        public LeadBasicDto Lead { get; set; }
        public ListCampaignDto Campaign { get; set; }
        public DateTimeDto DateCreated { get; set; }
        public DateTimeDto DateUpdated { get; set; }
        public BasicUserDto User { get; set; }
    }
}
