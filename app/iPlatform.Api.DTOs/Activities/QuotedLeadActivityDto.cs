﻿namespace iPlatform.Api.DTOs.Activities
{
    public class QuotedLeadActivityDto : LeadActivityDto
    {
        public QuotedLeadActivityDto()
        {
        }

        public int QuoteId { get; set; }

    }
}
