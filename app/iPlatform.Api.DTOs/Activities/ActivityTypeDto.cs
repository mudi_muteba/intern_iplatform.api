﻿namespace iPlatform.Api.DTOs.Activities
{
    public class ActivityTypeDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
