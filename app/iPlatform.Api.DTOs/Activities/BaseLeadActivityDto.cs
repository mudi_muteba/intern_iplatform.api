﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Activities
{
    public class BaseLeadActivityDto: IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
        public BaseLeadActivityDto() { }
        public int Id { get; set; }

        public int CampaignId { get; set; }
        public string Comment { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                list.Add(LeadValidationMessages.InvalidId);
            }

            return list;
        }
    }
}
