﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Activities
{
    public class CreateQuoteAcceptedLeadActivityDto: BaseLeadActivityDto
    {
        public CreateQuoteAcceptedLeadActivityDto()
        {
            
        }

        public int QuoteId { get; set; }
    }
}
