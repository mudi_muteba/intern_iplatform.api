﻿
using System;

namespace iPlatform.Api.DTOs.Activities
{
    public class PolicyBindingCreatedLeadActivityDto : BaseLeadActivityDto
    {
        public int UserId { get; set; }
        public int QuoteId { get; set; }
        public int ChannelId { get; set; }

        public PolicyBindingCreatedLeadActivityDto() { }
        public PolicyBindingCreatedLeadActivityDto(int id, int campaignId, int userId, int quoteId, int channelId)
        {
            QuoteId = quoteId;
            Id = id;
            CampaignId = campaignId;
            UserId = userId;
            ChannelId = channelId;

        }
    }
}
