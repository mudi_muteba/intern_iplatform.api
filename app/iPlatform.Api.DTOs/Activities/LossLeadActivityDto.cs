﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Activities
{
    public class LossLeadActivityDto : BaseLeadActivityDto
    {
        public LossLeadActivityDto() { }
        public LossLeadActivityDto(int campaignId)
        {
            CampaignId = campaignId;
        }
    }
}
