﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.CustomApp
{
    /// <summary>
    /// CusromApp VerifyMember response object
    /// </summary>
    public class VerifyMemberDto
    {
        public bool IsValid { get; set; }
    }
}
