﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.CustomApp
{
    public class VerifyMemberSearchDto : Resource, IAuditableDto, IExecutionDto
    {
        public VerifyMemberSearchDto()
        {
            Events = new List<AuditEventDto>();
        }

        public string prmIDNumber { get; set; }

        public string prmCellNumber { get; set; }

        public string prmPolicyNumber { get; set; }

        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}
