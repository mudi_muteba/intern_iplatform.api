﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.CustomApp
{
    /// <summary>
    /// CustomApp Register response object
    /// </summary>
    public class RegisterDto
    {
        public string ClientCode { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string IDNumber { get; set; }

        public string MobileNumber { get; set; }

        public string EmailAddress { get; set; }

        public string WorkNumber { get; set; }

        public string HomePhone { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string AddressPostalCode { get; set; }

        public string BusinessName { get; set; }

        public string Occupation { get; set; }

        public string ProgrammeCode { get; set; }

        public string BranchCode { get; set; }
    }
}
