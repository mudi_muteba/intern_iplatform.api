﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.CustomApp
{
    public class GetPolicyInfoSearchDto : Resource, IAuditableDto, IExecutionDto
    {
        public GetPolicyInfoSearchDto()
        {
            Events = new List<AuditEventDto>();
        }

        public string prmIdNumber { get; set; }

        public string prmCellNumber { get; set; }

        public string prmPolicyNumber { get; set; }

        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}
