﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.CustomApp
{
    /// <summary>
    /// CustomApp GetPolicyInfo response object
    /// </summary>
    public class GetPolicyInfoDto
    {
        public string Insurer { get; set; }

        public string Product { get; set; }

        public string PolicyNumber { get; set; }

        public string AnnualRenewalDate { get; set; }

        public string OriginalInceptionDate { get; set; }

        public string PolicyType { get; set; }

        public string PolicyStatus { get; set; }

        public string TotalPayment { get; set; }

        public string CoverType { get; set; }

        public string Id { get; set; }

        public string RiskItemType { get; set; }

        public string ItemDescription1 { get; set; }

        public string ItemDescription2 { get; set; }

        public string ItemDescription3 { get; set; }

        public string ItemDescription4 { get; set; }

        public string ItemDescription5 { get; set; }

        public string SumInsured { get; set; }

        public string RiskPayment { get; set; }

        public string ExcessValue { get; set; }

        public bool IsMotor { get; set; }

        public bool IsProperty { get; set; }

        public bool IsVAP { get; set; }
    }
}
