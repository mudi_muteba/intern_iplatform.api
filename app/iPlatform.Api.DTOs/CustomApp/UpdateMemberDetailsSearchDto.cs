﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.CustomApp
{
    public class UpdateMemberDetailsSearchDto : Resource, IAuditableDto, IExecutionDto
    {
        public UpdateMemberDetailsSearchDto()
        {
            Events = new List<AuditEventDto>();
        }

        public string prmIDNumber { get; set; }

        public string prmUpdatedBusinessName { get; set; }

        public string prmUpdatedOccupation { get; set; }

        public string prmUpdatedWorkNumber { get; set; }

        public string prmUpdatedHomeNumber { get; set; }

        public string prmUpdatedAddressLine1 { get; set; }

        public string prmUpdatedAddressLine2 { get; set; }

        public string prmUpdatedAddressLine3 { get; set; }

        public string prmUpdatedPostalCode { get; set; }

        public string prmUpdatedFirstName { get; set; }

        public string prmUpdatedSurname { get; set; }

        public string prmUpdatedCellphone { get; set; }

        public string prmUpdatedEmailAddress { get; set; }

        public List<AuditEventDto> Events { get; private set; }


        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}
