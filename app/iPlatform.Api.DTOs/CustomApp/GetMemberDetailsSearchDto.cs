﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.CustomApp
{
    public class GetMemberDetailsSearchDto : Resource, IAuditableDto, IExecutionDto
    {
        public GetMemberDetailsSearchDto()
        {
            Events = new List<AuditEventDto>();
        }

        public string prmIDNumber { get; set; }       

        public List<AuditEventDto> Events { get; private set; }


        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}
