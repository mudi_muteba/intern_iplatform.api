﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Products;
using MasterData;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.CoverDefinitions
{
    public class CoverDefinitionViewDto : ICultureAware
    {
        public CoverDefinitionViewDto()
        {
            Benefits = new List<ProductBenefitDto>();
        }
        public string DisplayName { get; set; }
        public Cover Cover { get; set; }
        public ListProductDto Product { get; set; }
        public int VisibleIndex { get; set; }
        public CoverDefinitionType CoverDefinitionType { get; set; }

        public List<ProductBenefitDto> Benefits { get; set; }
    }
}