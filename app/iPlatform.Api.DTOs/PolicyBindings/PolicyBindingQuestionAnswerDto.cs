﻿
namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingQuestionAnswerDto
    {
        public PolicyBindingQuestionAnswerDto()
        {

        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Answer { get; set; }
        public int VisibleIndex { get; set; }
        public string Reference { get; set; }
    }
}
