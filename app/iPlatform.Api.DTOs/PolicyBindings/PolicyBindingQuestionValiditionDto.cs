﻿using MasterData;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingQuestionValiditionDto
    {
        public PolicyBindingQuestionValiditionDto()
        {

        }
        public int Id { get; set; }
        public string Message { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Reference { get; set; }
        public QuestionAlert QuestionAlert { get; set; }
        public QuestionComparison QuestionComparison { get; set; }
    }
}