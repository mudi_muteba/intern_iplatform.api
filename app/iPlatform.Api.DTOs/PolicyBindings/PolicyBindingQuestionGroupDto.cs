﻿namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingQuestionGroupDto
    {
        public PolicyBindingQuestionGroupDto()
        {

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int VisibleIndex { get; set; }
        public string Reference { get; set; }
    }
}
