﻿
using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using System;
using ValidationMessages;
using System.Linq;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class SavePolicyBindingAnswerDto : AttributeValidationDto, IExecutionDto
    {
        public SavePolicyBindingAnswerDto()
        {
            Answers = new List<PolicyBindingAnswerDto>();
            Context = DtoContext.NoContext();
        }
        public List<PolicyBindingAnswerDto> Answers { get; set; }

        public DtoContext Context { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if(!Answers.Any())
            {
                validation.Add(PolicyBindingValidationMessages.EmptyAnswers);
            }

            return validation;
        }
    }
}
