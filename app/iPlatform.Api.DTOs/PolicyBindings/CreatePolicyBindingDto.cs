﻿using iPlatform.Api.DTOs.Base;
using MasterData;
using System.Collections.Generic;
using ValidationMessages;
using System.Linq;
using iPlatform.Api.DTOs.Audit;
using System;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class CreatePolicyBindingDto : AttributeValidationDto, IExecutionDto
    {
        public CreatePolicyBindingDto()
        {
            Context = DtoContext.NoContext();
            PolicyBindingQuestionDefinitions = new List<PolicyBindingQuestionDefinitionDto>();
        }
        public string Name { get; set; }
        public Guid ChannelSystemId { get; set; }
        public int ChannelId { get; set; }
        public int CoverId { get; set; }
        public int CoverDefinitionId { get; set; }
        public int ProductId { get; set; }
        public string Reference { get; set; }
        public iAdminUserInfoDto AdminUserInfo { get; set; }
        public List<PolicyBindingQuestionDefinitionDto> PolicyBindingQuestionDefinitions { get; set; }

        public DtoContext Context { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(Name))
            {
                validation.Add(PolicyBindingValidationMessages.NameRequired);
            }

            if (ChannelSystemId == Guid.Empty)
            {
                validation.Add(PolicyBindingValidationMessages.ChannelIdRequired);
            }

            if (ProductId == 0)
            {
                validation.Add(PolicyBindingValidationMessages.ProductIdRequired);
            }

            if (CoverId == 0)
            {
                validation.Add(PolicyBindingValidationMessages.CoverIdRequired);
            }

            if (!new Covers().Any(x => x.Id == CoverId))
            {
                validation.Add(PolicyBindingValidationMessages.InvalidCoverId.AddParameters(new[] { CoverId.ToString() }));
            }
            return validation;
        }
    }
}
