﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class GetPolicyBindingNavigationDto : AttributeValidationDto, IExecutionDto
    {
        public GetPolicyBindingNavigationDto()
        {
        }

        public int QuoteId { get; set; }
        public int ChannelId { get; set; }
        
        public DtoContext Context { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();


            if (QuoteId == 0)
            {
                validation.Add(PolicyBindingValidationMessages.QuoteIdRequired);
            }
            if (ChannelId == 0)
            {
                validation.Add(PolicyBindingValidationMessages.ChannelIdRequired);
            }
            return validation;
        }
    }
}
