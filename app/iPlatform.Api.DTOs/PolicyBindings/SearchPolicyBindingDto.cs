﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class SearchPolicyBindingDto : BaseCriteria, IValidationAvailableDto
    {
        public int? ProductId { get; set; }
        public int? CoverDefinitionId { get; set; }
        public int? ChannelId { get; set; }
        public int Reference { get; set; }
        public string Name { get; set; }

        public SearchPolicyBindingDto()
        {
            
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();
            
            return validation;
        }
    }
}