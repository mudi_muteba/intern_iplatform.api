using System;
using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class SubmitPolicyBindingResponseDto : Resource
    {
        public SubmitPolicyBindingResponseDto()
        {
            Message = new List<string>();
        }

        public string PolicyReference { get; set; }
        public bool Success { get; set; }
        public List<string> Message  { get; set; }


        public static SubmitPolicyBindingResponseDto GetSuccess()
        {
            return new SubmitPolicyBindingResponseDto { Success = true };
        }
    }
}