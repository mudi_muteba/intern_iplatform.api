﻿using iPlatform.Api.DTOs.Base;
using MasterData;
using System.Collections.Generic;
using ValidationMessages;
using System.Linq;
using System;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class UpdatePolicyBindingDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public UpdatePolicyBindingDto()
        {
            Context = DtoContext.NoContext();
            PolicyBindingQuestionDefinitions = new List<PolicyBindingQuestionDefinitionDto>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public Guid ChannelSystemId { get; set; }
        public int ChannelId { get; set; }
        public int CoverId { get; set; }
        public int ProductId { get; set; }
        public int CoverDefinitionId { get; set; }
        public List<PolicyBindingQuestionDefinitionDto> PolicyBindingQuestionDefinitions { get; set; }

        public DtoContext Context { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(Name))
            {
                validation.Add(PolicyBindingValidationMessages.NameRequired);
            }

            if (Id == 0)
            {
                validation.Add(PolicyBindingValidationMessages.IdRequired);
            }

            if (ChannelSystemId == Guid.Empty)
            {
                validation.Add(PolicyBindingValidationMessages.ChannelIdRequired);
            }

            if (ProductId == 0)
            {
                validation.Add(PolicyBindingValidationMessages.ProductIdRequired);
            }

            if (CoverId == 0)
            {
                validation.Add(PolicyBindingValidationMessages.CoverIdRequired);
            }

            if (CoverId > 0 && new Covers().All(x => x.Id != CoverId))
            {
                validation.Add(PolicyBindingValidationMessages.InvalidCoverId.AddParameters(new[] { CoverId.ToString() }));
            }
            return validation;
        }
    }
}
