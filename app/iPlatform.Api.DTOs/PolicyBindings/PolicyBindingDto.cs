﻿using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using System.Collections.Generic;
using System.Linq;
namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingDto: Resource
    {
        public PolicyBindingDto()
        {
            PolicyBindingQuestionDefinitions = new List<PolicyBindingQuestionDefinitionDto>();
        }

        public string Name { get; set; }
        public ChannelInfoDto Channel { get; set; }
        public string Reference { get; set; }
        public CoverDefinitionInfoDto CoverDefinition { get; set; }
        public List<PolicyBindingQuestionDefinitionDto> PolicyBindingQuestionDefinitions { get; set; }
        public int TotalQuestions
        {
            get
            {
                return PolicyBindingQuestionDefinitions.Count;
            }
        }
        public int AnsweredQuestions { get; set; }
    }
}
