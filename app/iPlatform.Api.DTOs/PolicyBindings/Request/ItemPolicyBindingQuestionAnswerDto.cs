﻿using MasterData;

namespace iPlatform.Api.DTOs.PolicyBindings.Request
{
    public class ItemPolicyBindingQuestionAnswerDto
    {
        public ItemPolicyBindingQuestionAnswerDto()
        {

        }

        public PolicyBindingQuestionInfoDto Question { get; set; }
        public string Answer { get; set; }

    }
}
