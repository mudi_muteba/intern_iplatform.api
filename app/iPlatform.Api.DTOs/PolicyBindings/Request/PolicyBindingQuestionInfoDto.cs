﻿using MasterData;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.PolicyBindings.Request
{
    public class PolicyBindingQuestionInfoDto
    {
        public PolicyBindingQuestionInfoDto()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public QuestionType QuestionType { get; set; }
        public int VisibleIndex { get; set; }
        public string ToolTip { get; set; }
        public string Regex { get; set; }
        public string Reference { get; set; }
        public bool IsProposalQuestion { get; set; }
        public PolicyBindingQuestionGroupInfoDto Group { get; set; }
    }
}
