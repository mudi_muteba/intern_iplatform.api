﻿

using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.QuoteAcceptance;
using iPlatform.Api.DTOs.PolicyBindings.Request;
using System;
using System.Collections.Generic;
using System.Globalization;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingRequestDto 
    {
        public PolicyBindingRequestDto()
        {
            UserRatingSettings = new List<SettingsiRateUserDto>();
        }

        public Guid RequestId { get; set; }
        public int LeadId { get; set; }
        public int QuoteId { get; set; }
        public int AgentId { get; set; }
        public Guid QuotePlatformId { get; set; }
        public string Environment { get; set; }
        public string RatingRequestId { get; set; }
        public string ApiURL { get; set; }
        public string Comments { get; set; }

        public NumberFormatInfo NumberFormatInfo { get; set; }
        public EchoTCFDto EchoTCF  { get; set; }
        public AgentDetailDto AgentDetail { get; set; }
        public ChannelDto ChannelInfo { get; set; }
        public CampaignInfoDto CampaignInfo { get; set; }
        public InsuredInfoDto InsuredInfo { get; set; }
        public PolicyBindingRequestInfoDto RequestInfo { get; set; }
        public List<SettingsiRateUserDto> UserRatingSettings { get; set; }
        public int AgentPartyId { get; set; }

        public int BrokerUserId { get; set; }
        public string BrokerUserExternalReference { get; set; }
        public int AccountExecutiveUserId { get; set; }
        public string AccountExecutiveExternalReference { get; set; }
        public int ChannelId { get; set; }
        public string ChannelExternalReference { get; set; }
        public Country Country { get; set; }
        public int PartyId { get; set; }

        public void SetNumberFormat(CultureParameter cultureParam)
        {
            var cultureVisitor = new CultureVisitor(cultureParam);
            NumberFormatInfo = CultureVisitor.GetCurrencyFormatProviderSymbolDecimals(cultureVisitor.Currency.Code);
        }
    }
}