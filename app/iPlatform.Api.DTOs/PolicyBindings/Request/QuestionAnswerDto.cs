﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.PolicyBindings.Request
{
    public class QuestionAnswerDto
    {
        public QuestionAnswerDto()
        {

        }

        public int Id { get; set; }
        public string Answer { get; set; }

    }
}
