﻿using MasterData;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.PolicyBindings.Request
{
    public class PolicyBindingQuestionGroupInfoDto
    {
        public PolicyBindingQuestionGroupInfoDto()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int VisibleIndex { get; set; }
    }
}
