﻿using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Users;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.PolicyBindings.Request
{
    public class PolicyBindingRequestInfoDto
    {
        public PolicyBindingRequestInfoDto()
        {
            Items = new List<PolicyBindingRequestItemDto>();
            PolicyBindingQuestions = new List<ItemPolicyBindingQuestionAnswerDto>();
        }
        public int ProductId { get; set; }
        public string Source { get; set; }
        public string InsurerCode { get; set; }
        public string InsurerName { get; set; }
        public string InsurerLogoPath { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string InsurerProductCode { get; set; }
        public string InsurerReference { get; set; }
        public decimal Fees { get; set; }
        public AboutProductDto AboutProduct { get; set; }
        public string ITCScore { get; set; }
        public string ExtraITCScore { get; set; }
        public InsuredInfoDto InsuredInfo { get; set; }
        public List<PolicyBindingRequestItemDto> Items { get; set; }
        public List<ItemPolicyBindingQuestionAnswerDto> PolicyBindingQuestions { get; set; }
        public UserInfoDto UserInfo { get; set; }

        public int BrokerUserId { get; set; }
        public string BrokerUserExternalReference { get; set; }
        public int AccountExecutiveUserId { get; set; }
        public string AccountExecutiveExternalReference { get; set; }
        public int ChannelId { get; set; }
        public string ChannelExternalReference { get; set; }
        public string ApiUrl { get; set; }
        public Country Country { get; set; }


        /// <summary>
        /// The name from channel table
        /// </summary>
        public string ChannelName { get; set; }

        /// <summary>
        /// The code from channel table
        /// </summary>
        public string ChannelCode { get; set; }

        /// <summary>
        /// List of persons in the policy
        /// </summary>
        public List<RatingRequestPersonDto> Persons { get; set; }

        /// <summary>
        /// this is rating request from irate
        /// </summary>
        public string ExternalRatingResult { get; set; }

        /// <summary>
        /// This is rating response from irate
        /// </summary>
        public string ExternalRatingRequest  { get; set; }
    }
}
