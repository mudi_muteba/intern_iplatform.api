﻿using MasterData;

namespace iPlatform.Api.DTOs.PolicyBindings.Request
{
    public class ItemRatingQuestionDto
    {
        public ItemRatingQuestionDto()
        {

        }

        public Question Question { get; set; }
        public string Answer { get; set; }

    }
}
