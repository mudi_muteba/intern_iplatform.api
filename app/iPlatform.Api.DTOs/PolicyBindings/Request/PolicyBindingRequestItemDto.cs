﻿using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.PolicyBindings.Request
{
    public class PolicyBindingRequestItemDto
    {
        public PolicyBindingRequestItemDto()
        {
            PolicyBindingQuestions = new List<ItemPolicyBindingQuestionAnswerDto>();
        }
        public string Description { get; set; }
        public string SumInsuredDescription { get; set; }
        public Cover Cover { get; set; }
        public decimal SumInsured { get; set; }
        public decimal Premium { get; set; }
        public decimal DiscountedPremium { get; set; }
        public decimal DiscountedPercentage { get; set; }
        public decimal Sasria { get; set; }
        public decimal SasriaCalulated { get; set; }
        public decimal SasriaShortfall { get; set; }
        public int AssetNumber { get; set; }
        public bool ExcessCalculated { get; set; }
        public decimal ExcessBasic { get; set; }
        public string ExcessDescription { get; set; }
        public RatingRequestItemDto RatingRequestItem { get; set; }
        public List<ItemPolicyBindingQuestionAnswerDto> PolicyBindingQuestions { get; set; }
    }
}
