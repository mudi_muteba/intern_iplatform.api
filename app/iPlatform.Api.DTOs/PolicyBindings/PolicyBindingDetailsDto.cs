﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingDetailsDto : Resource
    {
        public PolicyBindingDetailsDto()
        {
            Navigations = new List<PolicyBindingNavigationDto>();
        }

        public int LeadId { get; set; }
        public int PartyId { get; set; }
        public string LeadName { get; set; }
        public int QuoteId { get; set; }
        public int ProposalHeaderId { get; set; }
        public int ProposalDefinitionId { get; set; }
        public int CurrentQuoteItemId { get; set; }
        public string QuoteItemDescription { get; set; }
        public int PreviousQuoteItemId { get; set; }
        public int NextQuoteItemId { get; set; }
        public PolicyBindingDto PolicyBinding { get; set; }
        public List<PolicyBindingNavigationDto> Navigations { get; set; }
        public PolicyBindingQuoteDto QuoteInformation { get; set; }

    }
}
