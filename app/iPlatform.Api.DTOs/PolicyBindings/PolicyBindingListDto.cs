﻿using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingListDto : Resource
    {
        public PolicyBindingListDto()
        {
        }
        public string Name { get; set; }
        public string Reference { get; set; }
        public ChannelInfoDto Channel { get; set; }
        public CoverDefinitionDto CoverDefinition { get; set; }
    }
}
