﻿using MasterData;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingQuestionDefinitionDto
    {
        public PolicyBindingQuestionDefinitionDto()
        {

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Answer { get; set; }
        public PolicyBindingQuestionGroupDto PolicyBindingQuestionGroup { get; set; }
        public PolicyBindingQuestionDto PolicyBindingQuestion { get; set; }
        public int VisibleIndex { get; set; }
        public bool IsProposalQuestion { get; set; }
        public int QuestionDefinitionId { get; set; }
        public PolicyBindingConditionalRuleDto PolicyBindingConditionalRule { get; set; }
    }
}
