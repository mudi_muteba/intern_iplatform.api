﻿using MasterData;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingQuestionDto
    {
        public PolicyBindingQuestionDto()
        {
            PolicyBindingQuestionValiditions = new List<PolicyBindingQuestionValiditionDto>();
            PolicyBindingQuestionAnswers = new List<PolicyBindingQuestionAnswerDto>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public QuestionType QuestionType { get; set; }
        public int VisibleIndex { get; set; }
        public string ToolTip { get; set; }
        public string Regex { get; set; }
        public string Reference { get; set; }
        public List<PolicyBindingQuestionAnswerDto> PolicyBindingQuestionAnswers { get; set; }
        public List<PolicyBindingQuestionValiditionDto> PolicyBindingQuestionValiditions { get; set; }
    }
}
