﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.QuoteAcceptance;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingNotificationDto : IExecutionDto
    {
        public PolicyBindingNotificationDto()
        {
            Context = DtoContext.NoContext();
        }

        public string Email { get; set; }
        public string AssignAgentEmail { get; set; }
        public AgentDetailDto LoggedAgent { get; set; }
        public string AssignAgentName { get; set; }
        public int AgentId { get; set; }
        public string OrganizationCode { get; set; }
        public int ChannelId { get; set; }
        public string Body { get; set; }
        public string FromEmail { get; set; }
        public string Subject { get; set; }
        public string Uri { get; set; }
        public byte[] Bytefile { get; set; }
        public PolicyBindingReportData PolicyBindingReportData { get; set; }

        public DtoContext Context { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }

    public class PolicyBindingReportData
    {
        public PolicyBindingReportData()
        {
        }
        public string Name { get; set; }
    }
}