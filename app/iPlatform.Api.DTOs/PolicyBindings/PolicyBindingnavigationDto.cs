﻿using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingNavigationDto
    {
        public PolicyBindingNavigationDto()
        {
        }

        public int QuoteItemId { get; set; }
        public string QuoteItemDescription { get; set; }
        public int TotalQuestions { get; set; }
        public int TotalAnswered { get; set; }
        public Cover Cover { get; set; }
    }
}
