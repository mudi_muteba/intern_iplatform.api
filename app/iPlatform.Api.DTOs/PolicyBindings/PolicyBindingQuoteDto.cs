﻿using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingQuoteDto
    {
        public PolicyBindingQuoteDto()
        {
            PolicyTerm = "12 Month";
        }
        public string InsurerCode { get; set; }
        public string ProductCode { get; set; }
        public string InsurerName { get; set; }
        public string PolicyTerm { get; set; }
        public decimal Sasria { get; set; }
        public decimal Premium { get; set; }
        public decimal MonthlyFee{ get; set; }
        public decimal OneTimeFee { get; set;}
        public PaymentPlan PaymentPlan { get; set; }

        public decimal InitialPayment
        {
            get { return Sasria + Premium + OneTimeFee;  }
        }

        public decimal MonthlyPayment
        {
            get { return Sasria + Premium; }
        }
    }
}
