﻿using iPlatform.Api.DTOs.Base;
using MasterData;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingNavigationInfoDto
    {
        public PolicyBindingNavigationInfoDto()
        {
            Navigations = new List<PolicyBindingNavigationDto>();
        }

        public int LeadId { get; set; }
        public string LeadName { get; set; }
        public int PartyId { get; set; }
        public List<PolicyBindingNavigationDto> Navigations { get; set; }
        public string InsurerCode { get; set; }
        public string ProductCode { get; set; }
        public string InsurerName { get; set; }
        public string PolicyTerm { get; set; }
        public decimal Sasria { get; set; }
        public decimal Premium { get; set; }
        public decimal OneTimeFee { get; set;}
        public PaymentPlan PaymentPlan { get; set; }

        public decimal InitialPayment
        {
            get { return Sasria + Premium + OneTimeFee;  }
        }

        public decimal MonthlyPayment
        {
            get { return Sasria + Premium; }
        }
    }
}
