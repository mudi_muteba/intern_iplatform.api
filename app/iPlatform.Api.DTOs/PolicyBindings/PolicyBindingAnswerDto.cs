﻿
namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingAnswerDto
    {
        public PolicyBindingAnswerDto()
        {

        }
        public int Id { get; set; }
        public int QuoteId { get; set; }
        public int QuoteItemId { get; set; }
        public int PolicyBindingQuestionDefinitionId { get; set; }
        public int QuestionDefinitionId { get; set; }
        public string Answer { get; set; }
        public bool IsProposalQuestion { get; set; }
        public int ProposalDefinitionId { get; set; }
        public int ProposalHeaderId { get; set; }
        public int PartyId { get; set; }
        public int CampaignId { get; set; }
    }
}
