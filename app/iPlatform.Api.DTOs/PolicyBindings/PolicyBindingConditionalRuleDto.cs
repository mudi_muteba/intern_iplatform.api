﻿namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class PolicyBindingConditionalRuleDto
    {
        public PolicyBindingConditionalRuleDto()
        {
        }

        public int Id { get; set; }
        public string Note { get; set; }
        public string Answer { get; set; }
        public int QuestionFormComparisonId { get; set; }
        public string Reference { get; set; }
    }
}
