﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.PolicyBindings
{
    public class SubmitPolicyBindingDto : IValidationAvailableDto, IExecutionDto, IAffectExistingEntity
    {
        public SubmitPolicyBindingDto()
        {
            Context = DtoContext.NoContext();
        }
        //used for editing existinghandlerDto 
        public int Id
        {
            get { return QuoteId; }
        }
        public int QuoteId { get; set; }
        public int ChannelId { get; set; }
        public int CampaignId { get; set; }
        public int BrokerUserId { get; set; }
        public int AccountExecutiveUserId { get; set; }
        public string Comments { get; set; }
        
        public DtoContext Context { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            if(QuoteId == 0)
            {
                validation.Add(PolicyBindingValidationMessages.QuoteIdRequired);
            }

            if (ChannelId == 0)
            {
                validation.Add(PolicyBindingValidationMessages.ChannelIdRequired);
            }

            if(CampaignId == 0)
            {
                validation.Add(PolicyBindingValidationMessages.CampaignIdRequired);
            }
            return validation;
        }
    }
}