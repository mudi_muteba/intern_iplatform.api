﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.WorkflowRetries
{
    public class CreateWorkflowRetryLogDto : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; set; }

        public Guid CorrelationId { get; set; }
        public int RetryCount { get; set; }
        public int RetryLimit { get; set; }
        public DateTime PublishDate { get; set; }
        public string MessageDelay { get; set; }
        public DateTime? FuturePublishDate { get; set; }
        public object Message { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public CreateWorkflowRetryLogDto() { }

        public CreateWorkflowRetryLogDto(Guid correlationId, int retryCount, int retryLimit, DateTime publishDate, string messageDelay, DateTime? futurePublishDate, object message)
        {
            CorrelationId = correlationId;
            RetryCount = retryCount;
            RetryLimit = retryLimit;
            PublishDate = publishDate;
            MessageDelay = messageDelay;
            FuturePublishDate = futurePublishDate;
            Message = message;
        }
    }
}