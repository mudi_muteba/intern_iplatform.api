﻿using System;

namespace iPlatform.Api.DTOs.WorkflowRetries
{
    public class SearchWorkflowRetryLogDto
    {
        public Guid CorrelationId { get; set; }
        public int RetryCount { get; set; }

        public SearchWorkflowRetryLogDto() { }

        public SearchWorkflowRetryLogDto(Guid correlationId, int retryCount)
        {
            CorrelationId = correlationId;
            RetryCount = retryCount;
        }
    }
}