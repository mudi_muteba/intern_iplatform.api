using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.WorkflowRetries
{
    public class ListWorkflowRetryLogDto : Resource, ICultureAware
    {
        public Guid CorrelationId { get; set; }
        public int RetryCount { get; set; }
        public int RetryLimit { get; set; }

        public ListWorkflowRetryLogDto(Guid correlationId, int retryCount, int retryLimit)
        {
            CorrelationId = correlationId;
            RetryCount = retryCount;
            RetryLimit = retryLimit;
        }
    }
}