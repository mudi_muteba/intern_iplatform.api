﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Banks
{
    public class BankBranchesSearchDto : BaseCriteria, IValidationAvailableDto
    {

        public string Name { get; set; }

        public string BranchCode { get; set; }

        public int? BankId { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            var valid = Id.HasValue ||
                    BankId.HasValue ||
                   !string.IsNullOrEmpty(Name) ||
                   !string.IsNullOrEmpty(BranchCode);

            if (valid)
                return validation;

            validation.Add(BankValidationMessages.InvalidSearchCriteria);

            return validation;
        }
    }
}
