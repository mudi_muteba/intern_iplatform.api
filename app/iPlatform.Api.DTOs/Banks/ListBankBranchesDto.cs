﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Banks
{
    public class ListBankBranchesDto : Resource
    {
        public string Name { get; set; }
        public string BranchCode { get; set; }
        public ListBankDto Bank { get; set; }
    }
}
