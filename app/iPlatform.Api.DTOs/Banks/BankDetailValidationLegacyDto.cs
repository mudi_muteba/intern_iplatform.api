﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Banks
{
    public class BankDetailValidationLegacyDto : IExecutionDto, IValidationAvailableDto
    {
        public string BranchCode { get; set; }
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public string LookupPath { get; set; }


        public BankDetailValidationLegacyDto()
        {

        }

        public DtoContext Context { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(BranchCode))
            {
                list.Add(ValidationMessages.Banks.BankValidationMessages.InvalidBranchCode);
            }
                

            if (string.IsNullOrWhiteSpace(AccountNumber))
            {
                list.Add(ValidationMessages.Banks.BankValidationMessages.InvalidAccountNumber);
            }
                

            if (string.IsNullOrWhiteSpace(AccountType))
            {
                list.Add(ValidationMessages.Banks.BankValidationMessages.InvalidAccountType);
            }
                
            return list;
        }
    }
}
