﻿namespace iPlatform.Api.DTOs.Banks
{
    public class BankDetailValidationResponseDto
    {
        public BankDetailValidationResponseDto(bool valid = false)
        {
            Valid = valid;
        }

        public bool Valid { get; set; }
    }
}
