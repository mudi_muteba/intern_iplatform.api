﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Banks
{
    public class BankSearchDto : BaseCriteria, IValidationAvailableDto
    {

        public string Name { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            var valid = Id.HasValue ||
                   !string.IsNullOrEmpty(Name);

            if (valid)
                return validation;

            validation.Add(BankValidationMessages.InvalidSearchCriteria);

            return validation;
        }
    }
}
