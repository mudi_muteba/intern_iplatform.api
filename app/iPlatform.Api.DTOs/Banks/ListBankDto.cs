﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Banks
{
    public class ListBankDto : Resource
    {
        public string Name { get; set; }
        public string CountryCode { get; set; }
    }
}
