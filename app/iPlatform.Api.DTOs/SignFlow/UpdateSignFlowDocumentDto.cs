﻿using System;
using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.SignFlow
{
    public class UpdateSignFlowDocumentDto : IExecutionDto, IAffectExistingEntity
    {
        public UpdateSignFlowDocumentDto()
        {
            Context = DtoContext.NoContext();
        }

        public DtoContext Context { get; private set; }

        public Guid Reference { get; set; }
        public string DocId { get; set; }
        public string DocName { get; set; }
        public string User { get; set; }
        public string CorrespondantUserEmail { get; set; }
        public string CorrespondantUserName { get; set; }
        public string CorrespondantUserCell { get; set; }
        public DocumentStatus Status { get; set; }
        public DocumentType DocType { get; set; }
        public DocumentDefinition DocDefinition { get; set; }
        public DateTime ModifiedAt { get; set; }

        public void SetContext(DtoContext context)
        {

        }

        public int Id { get; set; }
    }
}
