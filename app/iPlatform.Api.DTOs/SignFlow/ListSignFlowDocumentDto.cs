﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.SignFlow
{
    public class ListSignFlowDocumentDto : Resource, ICultureAware
    {
        public Guid Reference { get; set; }
        public string DocId { get; set; }
        public string DocName { get; set; }
        public string User { get; set; }
        public string CorrespondantUser { get; set; }
        public DocumentStatus Status { get; set; }
        public DocumentType DocType { get; set; }
        public DocumentDefinition DocDefinition { get; set; }
        public List<AuditEventDto> Events { get; set; }
    }
}
