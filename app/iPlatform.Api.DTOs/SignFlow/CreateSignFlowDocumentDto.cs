﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;
using ValidationMessages;
using ValidationMessages.SignFlow;

namespace iPlatform.Api.DTOs.SignFlow
{
    public class CreateSignFlowDocumentDto : AttributeValidationDto, IExecutionDto // Resource removed and replaced by AttributeValidationDto
    {
        public DtoContext Context { get; private set; }

        public CreateSignFlowDocumentDto()
        {
            Context = DtoContext.NoContext();
            CorrespondantUser = new List<CorrespondantUser>();
        }

        public Guid Reference { get; set; }
        public string DocId { get; set; }
        public string DocName { get; set; }
        public string User { get; set; }
        public List<CorrespondantUser> CorrespondantUser { get; set; }
        public DocumentStatus DocumentStatus { get; set; }
        public DocumentType DocumentType { get; set; }
        public string PathOfDocument { get; set; }
        public string MessageToUser { get; set; }
        public string NameDocUploaded { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public DateTime DueDate { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(PathOfDocument))
            {
                validation.Add(SignFlowValidationMessages.PathOfDocumentRequired);
            }

            if (string.IsNullOrWhiteSpace(MessageToUser))
            {
                validation.Add(SignFlowValidationMessages.MessageToUserRequired);
            }

            if (string.IsNullOrWhiteSpace(NameDocUploaded))
            {
                validation.Add(SignFlowValidationMessages.NameDocUploadedRequired);
            }

            if (DueDate == DateTime.MinValue)
            {
                validation.Add(SignFlowValidationMessages.DueDateRequired);
            }
            else if(DueDate <= DateTime.Today)
            {
                validation.Add(SignFlowValidationMessages.DueDatePassRequired);
            }

            return validation;
        }
    }
}
