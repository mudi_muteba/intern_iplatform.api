﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.SignFlow;

namespace iPlatform.Api.DTOs.SignFlow
{
    public class CreateMultipleSignFlowDocumentDto : AttributeValidationDto, IExecutionDto 
    {
        public DtoContext Context { get; private set; }

        public CreateMultipleSignFlowDocumentDto()
        {
            Context = DtoContext.NoContext();
            CreateSignFlowDocumentDtos = new List<CreateSignFlowDocumentDto>();
        }

        public List<CreateSignFlowDocumentDto> CreateSignFlowDocumentDtos { get; set; }
       
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            foreach (var createSignFlowDocumentDto in CreateSignFlowDocumentDtos)
            {
                if (string.IsNullOrWhiteSpace(createSignFlowDocumentDto.PathOfDocument))
                {
                    validation.Add(SignFlowValidationMessages.PathOfDocumentRequired);
                }

                if (string.IsNullOrWhiteSpace(createSignFlowDocumentDto.MessageToUser))
                {
                    validation.Add(SignFlowValidationMessages.MessageToUserRequired);
                }

                if (string.IsNullOrWhiteSpace(createSignFlowDocumentDto.NameDocUploaded))
                {
                    validation.Add(SignFlowValidationMessages.NameDocUploadedRequired);
                }

                if (createSignFlowDocumentDto.DueDate == DateTime.MinValue)
                {
                    validation.Add(SignFlowValidationMessages.DueDateRequired);
                }
                else if (createSignFlowDocumentDto.DueDate <= DateTime.Today)
                {
                    validation.Add(SignFlowValidationMessages.DueDatePassRequired);
                }
            }

            return validation;
        }
    }
}
