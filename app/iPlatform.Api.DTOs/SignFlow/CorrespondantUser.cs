﻿using System.Collections.Generic;
using MasterData;

namespace iPlatform.Api.DTOs.SignFlow
{
    public class CorrespondantUser
    {
        public CorrespondantUser()
        {
            DocDefinitions =  new List<DocumentDefinition>();
        }

        public string Email { get; set; }
        public string Name { get; set; }
        public string Cell { get; set; }
        public List<DocumentDefinition> DocDefinitions { get; set; }
    }
}
