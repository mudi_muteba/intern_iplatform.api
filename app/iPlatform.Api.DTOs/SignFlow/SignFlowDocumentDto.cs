﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.SignFlow
{
    public class SignFlowDocumentDto : Resource, IAuditableDto, ICultureAware
    {
        public Guid Reference { get; set; }
        public string DocId { get; set; }
        public string DocName { get; set; }
        public string User { get; set; }
        public string CorrespondantUserEmail { get; set; }
        public string CorrespondantUserName { get; set; }
        public string CorrespondantUserCell { get; set; }
        public DocumentStatus DocumentStatus { get; set; }
        public DocumentType DocumentType { get; set; }
        public DocumentDefinition DocDefinition { get; set; }
        public List<AuditEventDto> Events { get; set; }
    }
}
