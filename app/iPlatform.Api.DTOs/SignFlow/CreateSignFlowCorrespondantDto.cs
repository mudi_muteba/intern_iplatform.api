﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SignFlow
{
    public class CreateSignFlowCorrespondantDto : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public CreateSignFlowCorrespondantDto()
        {
            Context = DtoContext.NoContext();
        }

        public int SignFlowDocumentId { get; set; }
        public string Email { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}