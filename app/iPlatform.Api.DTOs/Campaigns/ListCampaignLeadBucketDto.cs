﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Users;

using iPlatform.Api.DTOs.Admin;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class ListCampaignLeadBucketDto : ICultureAware
    {
        public ListCampaignLeadBucketDto()
        {
            Histories = new List<CampaignLeadBucketVerboseDto>();
        }

        public int Id { get; set; }
        public ChannelDto Channel { get; set; }
        public int CampaignId { get; set; }
        public string Description { get; set; }
        public string Comments { get; set; }
        public bool Callback { get; set; }
        public DateTimeDto CallbackDate { get; set; }
        public int CallbackCount { get; set; }
        public DateTimeDto CreatedOn { get; set; }
        public DateTimeDto ModifiedOn { get; set; }
        public int AgentId { get; set; }
        public BasicUserDto Agent { get; set; }
        public LeadCallCentreCodeDto LeadCallCentreCode { get; set; }
        public List<CampaignLeadBucketVerboseDto> Histories { get; set; }


    }
}
