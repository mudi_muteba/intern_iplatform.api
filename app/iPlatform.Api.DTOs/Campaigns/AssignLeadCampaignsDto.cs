﻿
using System;
using iPlatform.Api.DTOs.Base;
using MasterData;
using iPlatform.Api.DTOs.Leads.Quality;
using ValidationMessages;
using System.Collections.Generic;
using System.Linq;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class AssignLeadCampaignsDto : AttributeValidationDto, IExecutionDto
    {


        public AssignLeadCampaignsDto()
        {
            CampaignList = new List<int>();
        }
        public int LeadId { get; set; }

        public List<int> CampaignList { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }


        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (LeadId == 1)
                validation.Add(LeadValidationMessages.InvalidId);

            if (!CampaignList.Any())
                validation.Add(LeadValidationMessages.EmptyCampaignList);

            return validation;
        }


    }
}
