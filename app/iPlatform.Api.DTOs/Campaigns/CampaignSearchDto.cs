﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Campaigns;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class CampaignSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public int? AgentId { get; set; }
        public string Reference { get; set; }
        public List<string> References { get; set; }
        public List<string> Tags { get; set; }
        public string Name { get; set; }

        public CampaignSearchDto()
        {
            References = new List<string>();
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();
            
            return validation;
        }
    }
}