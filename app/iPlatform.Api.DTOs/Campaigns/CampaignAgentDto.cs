﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class CampaignAgentDto : Resource, ICultureAware
    {
        public CampaignAgentDto()
        {
        }

        public PartyDto Party { get; set; }
    }
}