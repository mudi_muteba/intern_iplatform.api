﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class CampaignSimilarCampaignDto : Resource, ICultureAware
    {
        public CampaignSimilarCampaignDto()
        {
        }

        public BasicCampaignDto SimilarCampaign { get; set; }
    }
}