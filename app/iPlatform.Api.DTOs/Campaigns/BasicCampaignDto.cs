﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Tags;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class BasicCampaignDto : Resource, ICultureAware
    {
        public BasicCampaignDto()
        {
            SimpleAudit = new AuditDto();
        }

        public AuditDto SimpleAudit { get; private set; }
        public string Name { get; set; }
        public string Reference { get; set; }
        public DateTimeDto StartDate { get; set; }
        public DateTimeDto EndDate { get; set; }
        public DateTimeDto DateCreated { get; set; }
        public DateTimeDto DateUpdated { get; set; }
        public bool DefaultCampaign { get; set; }
        public int ChannelId { get; set; }
        public string CampaignHoursFrom { get; set; }
        public string CampaignHoursTo { get; set; }
        public int MaximumLeadsPerAgent { get; set; }
        public int MaximumLeadCallbackLimit { get; set; }
        public bool AutoRescheduleCallbacks { get; set; }
        public int AutoRescheduleCallbackHours { get; set; }
        public bool AllowSimilarCampaignLeadCallbacks { get; set; }
        public bool AllowLeadCallbacksOutsideCampaignHours { get; set; }
        public bool UseLastInFirstOutCallScheme { get; set; }
        public int DailySalesTarget { get; set; }
        public decimal DailyPremiumTarget { get; set; }
        public int TotalWeekWorkDays { get; set; }
        public bool IsWorkdayMonday { get; set; }
        public bool IsWorkdayTuesday { get; set; }
        public bool IsWorkdayWednesday { get; set; }
        public bool IsWorkdayThursday { get; set; }
        public bool IsWorkdayFriday { get; set; }
        public bool IsWorkdaySaturday { get; set; }
        public bool IsWorkdaySunday { get; set; }
    }
}