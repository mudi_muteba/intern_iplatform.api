﻿
using System;
using iPlatform.Api.DTOs.Base;
using MasterData;
using iPlatform.Api.DTOs.Leads.Quality;
using ValidationMessages;
using System.Collections.Generic;
using ValidationMessages.Campaigns;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class NextLeadResponseDto : Resource
    {

        public NextLeadResponseDto() { }

        public NextLeadResponseDto(int id)
        {
            this.Id = id;
        }

        public ValidationErrorMessage Message { get; set; }
        public bool IsSuccess { get; set; }


        public static NextLeadResponseDto NoLeads()
        {
            return new NextLeadResponseDto
            {
                IsSuccess = false,
                Message = CampaignValidationMessages.NoLeadInBucket
            };
        }

        public static NextLeadResponseDto MaximumLeadsExceeded()
        {
            return new NextLeadResponseDto
            {
                IsSuccess = false,
                Message = CampaignValidationMessages.MaximumLeadsExceeded
            };
        }

        public static NextLeadResponseDto NextLeads(int id)
        {
            return new NextLeadResponseDto
            {
                Id = id,
                IsSuccess = true,
                Message = CampaignValidationMessages.NextLead
            };
        }

        public static NextLeadResponseDto UpdatePendingLead()
        {
            return new NextLeadResponseDto
            {
                IsSuccess = false,
                Message = CampaignValidationMessages.UpdatePendingLead
            };
        }

        public static NextLeadResponseDto NoSeritiLead()
        {
            return new NextLeadResponseDto
            {
                IsSuccess = false,
                Message = CampaignValidationMessages.NoSeritiLeadsImported
            };
        }
    }
}
