﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class LeadCallCentreCodeDto
    {
        public LeadCallCentreCodeDto() { }
        public virtual int Id { get; set; }
        public virtual int ParentId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string EventTemplate { get; set; }
        public virtual string Icon { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual decimal PerformanceWeight { get; set; }
        public virtual bool HasChildren { get; set; }
        public virtual bool IsFinalStatus { get; set; }
        public virtual bool IsVisibleToCallCentreAgent { get; set; }
        public virtual bool ShouldAllowCallRescheduling { get; set; }
        public virtual bool ShouldBlockLeadFromBeingContacted { get; set; }
        public virtual bool ShouldCreateDelayLeadActivity { get; set; }
        public virtual bool ShouldCreateLossLeadActivity { get; set; }
        public virtual bool ShouldCreateDeadLeadActivity { get; set; }
        public virtual bool RequiresCallCentreAgentComments { get; set; }

        
    }
}
