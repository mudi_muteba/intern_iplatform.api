﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Campaigns;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class AssignLeadToCampaignDto: AttributeValidationDto, IExecutionDto
    {
        public AssignLeadToCampaignDto()
        {
            Leads = new List<int>();
            AllLeads = false;
        }

        public int Id { get; set; }

        public List<int> Leads { get; set; }

        public bool AllLeads { get; set; }
        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }


        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id == 0)
            {
                validation.Add(CampaignValidationMessages.CampaignIdRequired);
            }

            if (!AllLeads && !Leads.Any())
            {
                validation.Add(CampaignValidationMessages.LeadBucketEmpty);
            }

           
            return validation;
        }
    }
}
