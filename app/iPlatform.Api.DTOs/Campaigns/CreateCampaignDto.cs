﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Tags;
using ValidationMessages;
using ValidationMessages.Campaigns;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class CreateCampaignDto : AttributeValidationDto, IChannelAwareDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public CreateCampaignDto()
        {
            Context = DtoContext.NoContext();
            Products = new List<ProductInfoDto>();
            Tags = new List<TagDto>();
            Agents = new List<PartyDto>();
            Managers = new List<int>();
            SimilarCampaigns = new List<CampaignDto>();
            References = new List<CampaignReferenceDto>();
        }

        [Required(ErrorMessage = "CAMPAIGN_NAME_DUPLICATE")]
        public string Name { get; set; }
        public string Reference { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ChannelId { get; set; }
        public List<ProductInfoDto> Products { get; set; }
        public List<TagDto> Tags { get; set; }
        public List<PartyDto> Agents { get; set; }
        public List<int> Managers { get; set; }
        public List<CampaignReferenceDto> References { get; set; }
        public List<CampaignDto> SimilarCampaigns { get; set; }
        public string CampaignHoursFrom { get; set; }
        public string CampaignHoursTo { get; set; }
        public int MaximumLeadsPerAgent { get; set; }
        public int MaximumLeadCallbackLimit { get; set; }
        public bool AutoRescheduleCallbacks { get; set; }
        public int AutoRescheduleCallbackHours { get; set; }
        public bool AllowSimilarCampaignLeadCallbacks { get; set; }
        public bool AllowLeadCallbacksOutsideCampaignHours { get; set; }
        public bool UseLastInFirstOutCallScheme { get; set; }
        public bool DefaultCampaign { get; set; }
        public int DailySalesTarget { get; set; }
        public decimal DailyPremiumTarget { get; set; }
        public int TotalWeekWorkDays { get; set; }
        public bool IsWorkdayMonday { get; set; }
        public bool IsWorkdayTuesday { get; set; }
        public bool IsWorkdayWednesday { get; set; }
        public bool IsWorkdayThursday { get; set; }
        public bool IsWorkdayFriday { get; set; }
        public bool IsWorkdaySaturday { get; set; }
        public bool IsWorkdaySunday { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(Name))
                validation.Add(CampaignValidationMessages.CampaignNameRequired);

            if (StartDate == DateTime.MaxValue || StartDate == DateTime.MinValue)
                validation.Add(CampaignValidationMessages.StartDateNotProvided);

            //if (DateTime.Compare(StartDate, DateTime.UtcNow) < 0) 
            //    validation.Add(CampaignValidationMessages.StartdateShouldBeGreaterNow);

            if (EndDate.HasValue)
                if (StartDate > EndDate.Value)
                    validation.Add(CampaignValidationMessages.StartDateAfterEndDate);

            return validation;
        }

        public override string ToString()
        {
            return string.Format("Name: {0}", Name);
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}