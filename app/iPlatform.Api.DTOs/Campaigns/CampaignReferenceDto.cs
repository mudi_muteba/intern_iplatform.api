﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class CampaignReferenceDto : Resource
    {
        public CampaignReferenceDto() { }
        public string Name { get; set; }
    }
}