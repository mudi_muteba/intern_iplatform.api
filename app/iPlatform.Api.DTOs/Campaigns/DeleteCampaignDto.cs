﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Campaigns;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class DeleteCampaignDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        public DeleteCampaignDto()
        {
            Context = DtoContext.NoContext();
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                list.Add(CampaignValidationMessages.CampaignIdRequired);
            }

            return list;
        }
    }
}