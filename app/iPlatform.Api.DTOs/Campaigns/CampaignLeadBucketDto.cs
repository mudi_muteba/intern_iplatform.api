﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Individual;
using MasterData;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class CampaignLeadBucketDto : ICultureAware
    {
        public CampaignLeadBucketDto()
        {
            Histories = new List<CampaignLeadBucketVerboseDto>();
        }

        public int Id { get; set; }
        public int CampaignId { get; set; }
        public ListIndividualDto Lead { get; set; }
        public string Description { get; set; }
        public string Comments { get; set; }
        public bool Callback { get; set; }
        public DateTimeDto CallbackDate { get; set; }
        public int CallbackCount { get; set; }
        public DateTimeDto CreatedOn { get; set; }
        public DateTimeDto ModifiedOn { get; set; }
        public int AgentId { get; set; }
        public LeadCallCentreCodeDto LeadCallCentreCode { get; set; }
        public List<CampaignLeadBucketVerboseDto> Histories { get; set; }


    }
}
