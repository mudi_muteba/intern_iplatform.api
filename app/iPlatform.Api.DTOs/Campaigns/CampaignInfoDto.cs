﻿namespace iPlatform.Api.DTOs.Campaigns
{
    public class CampaignInfoDto
    {
        public int Id { get; set; }
        public string SourceReference { get; set; }
        public string CampaignReference { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
    }
}