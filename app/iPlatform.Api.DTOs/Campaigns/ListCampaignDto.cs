﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class ListCampaignDto : Resource, ICultureAware
    {
        public ListCampaignDto()
        {
        }

        public string Name { get; set; }
        public string Reference { get; set; }
        public DateTimeDto StartDate { get; set; }
        public DateTimeDto EndDate { get; set; }
        public bool DefaultCampaign { get; set; }
    }
}