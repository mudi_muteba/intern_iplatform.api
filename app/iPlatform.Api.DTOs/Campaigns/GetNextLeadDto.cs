﻿
using System;
using iPlatform.Api.DTOs.Base;
using MasterData;
using iPlatform.Api.DTOs.Leads.Quality;
using ValidationMessages;
using System.Collections.Generic;
using ValidationMessages.Leads;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class GetNextLeadDto : AttributeValidationDto, IExecutionDto
    {


        public GetNextLeadDto() { }
        public int Id { get; set; }

        public CreateLeadQualityDto LeadQuality { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }


        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id == 0)
                validation.Add(LeadValidationMessages.InvalidCampaignId);

            return validation;
        }


    }
}
