﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Tags;
using ValidationMessages;
using ValidationMessages.Campaigns;

namespace iPlatform.Api.DTOs.Campaigns
{
    public class CreateCampaignReferenceDto : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public CreateCampaignReferenceDto()
        {
            Context = DtoContext.NoContext();
        }

        public int CampaignId { get; set; }
        public string Reference { get; set; }
        public string Name { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            

            return validation;
        }


        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}