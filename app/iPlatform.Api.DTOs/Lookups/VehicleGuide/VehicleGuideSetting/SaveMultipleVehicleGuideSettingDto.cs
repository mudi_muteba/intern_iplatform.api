﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Lookups.VehicleGuide.VehicleGuideSetting
{
    public class SaveMultipleVehicleGuideSettingDto : AttributeValidationDto, IExecutionDto
    {
        public SaveMultipleVehicleGuideSettingDto()
        {
            VehicleGuide = new List<SaveVehicleGuideSettingDto>();
        }

        public virtual List<SaveVehicleGuideSettingDto> VehicleGuide { get; set; }

        public virtual int ChannelId { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
