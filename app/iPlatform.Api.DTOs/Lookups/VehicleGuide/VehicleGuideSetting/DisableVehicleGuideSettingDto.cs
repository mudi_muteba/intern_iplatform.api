﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting
{
    public class DisableVehicleGuideSettingDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
        public int Id { get; set; }

        public DtoContext Context { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DisableVehicleGuideSettingDto()
        {
            Context = DtoContext.NoContext();
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                list.Add(VehicleGuideSettingValidationMessage.IdRequiredForDelete);
            }

            return list;
        }
    }
}
