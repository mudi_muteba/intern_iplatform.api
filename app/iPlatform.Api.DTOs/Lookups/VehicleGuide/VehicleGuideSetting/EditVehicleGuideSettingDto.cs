﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting
{
    public class EditVehicleGuideSettingDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public EditVehicleGuideSettingDto() { }
        public int Id { get; set; }
        public int ChannelId { get; set; }
        public int CountryId { get; set; }
        public string ApiKey { get; set; }
        public string Email { get; set; }
        public bool MMBookEnabled { get; set; }
        public bool LightstoneEnabled { get; set; }
        public string LSA_UserName { get; set; }
        public string LSA_Password { get; set; }
        public Guid? LSA_UserId { get; set; }
        public Guid? LSA_ContractId { get; set; }
        public Guid? LSA_CustomerId { get; set; }
        public Guid? LSA_PackageId { get; set; }
        public string KeAutoKey { get; set; }
        public string KeAutoSecret { get; set; }
        public string Evalue8Username { get; set; }
        public string Evalue8Password { get; set; }
        public bool Evalue8Enabled { get; set; }
        public bool TransUnionAutoEnabled { get; set; }

        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId == 0)
                validation.Add(VehicleGuideSettingValidationMessage.ChannelIdRequired);

            return validation;
        }
    }
}
