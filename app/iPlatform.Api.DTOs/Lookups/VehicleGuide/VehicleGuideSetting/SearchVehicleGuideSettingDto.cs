﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Lookups.VehicleGuide.VehicleGuideSetting
{
    public class SearchVehicleGuideSettingDto : BaseCriteria, IValidationAvailableDto
    {
        public SearchVehicleGuideSettingDto()
        {

        }
        public string ChannelName { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}