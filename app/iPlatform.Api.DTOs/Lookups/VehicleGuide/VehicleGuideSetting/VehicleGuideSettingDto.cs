﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting
{
    public class VehicleGuideSettingDto : Resource, ICultureAware
    {
        public VehicleGuideSettingDto() { }
        public virtual ChannelInfoDto Channel { get; set; }
        public virtual int CountryId { get; set; }
        public virtual string ApiKey { get; set; }
        public virtual string Email { get; set; }
        public virtual bool MMBookEnabled { get; set; }
        public virtual bool LightstoneEnabled { get; set; }
        public virtual string LSA_UserName { get; set; }
        public virtual string LSA_Password { get; set; }
        public virtual Guid? LSA_UserId { get; set; }
        public virtual Guid? LSA_ContractId { get; set; }
        public virtual Guid? LSA_CustomerId { get; set; }
        public virtual Guid? LSA_PackageId { get; set; }
        public virtual string KeAutoKey { get; set; }
        public virtual string KeAutoSecret { get; set; }
        public virtual string Evalue8Username { get; set; }
        public virtual string Evalue8Password { get; set; }
        public virtual bool Evalue8Enabled { get; set; }
        public virtual bool TransUnionAutoEnabled { get; set; }
    }
}
