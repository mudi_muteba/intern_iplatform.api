﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Lookups.Person
{
    public class PersonLookupRequest : IValidationAvailableDto
    {
        private string idNumber;
        private string firstName;
        private string surname;
        private string phoneNumber;
        private string email;
        private string source;
        private string propertyId;
        private string search;

        public string IdNumber
        {
            get { return string.IsNullOrWhiteSpace(idNumber) ? "-" : idNumber; }
            set { idNumber = value; }
        }

        public string FirstName
        {
            get { return string.IsNullOrWhiteSpace(firstName) ? "-" : firstName; }
            set { firstName = value; }
        }

        public string Surname
        {
            get { return string.IsNullOrWhiteSpace(surname) ? "-" : surname; }
            set { surname = value; }
        }

        public string PhoneNumber
        {
            get { return string.IsNullOrWhiteSpace(phoneNumber) ? "-" : phoneNumber; }
            set { phoneNumber = value; }
        }

        public string Email
        {
            get { return string.IsNullOrWhiteSpace(email) ? "-" : email; }
            set { email = value; }
        }

        public string Source
        {
            get { return string.IsNullOrWhiteSpace(source) ? "-" : source; }
            set { source = value; }
        }


        public string PropertyId
        {
            get { return string.IsNullOrWhiteSpace(source) ? "-" : propertyId; }
            set { propertyId = value; }
        }

        public string Search
        {
            get { return string.IsNullOrWhiteSpace(search) ? "-" : search; }
            set { search = value; }
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}