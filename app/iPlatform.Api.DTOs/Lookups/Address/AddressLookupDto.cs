﻿using iPlatform.Api.DTOs.Base;
namespace iPlatform.Api.DTOs.Lookups.Address
{
    public class AddressLookupDto: Resource
    {
        public string Name { get; set; }
    }
}
