﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Lookups.Address.GetAddress
{
    public class GetAddressByPostCodeDto : IExecutionDto
    {
        public DtoContext Context { get; private set; }
        public string PostCode { get; set; }

        public GetAddressByPostCodeDto()
        {
            Context = DtoContext.NoContext();
        }

        public GetAddressByPostCodeDto(string postCode)
        {
            Context = DtoContext.NoContext();
            PostCode = postCode;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}
