﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Lookups.Address.GetAddress
{
    public class GetAddressDto : Resource
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public List<string> Addresses { get; set; }
    }
}
