﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Lookups.Address
{
    public class GetProvinceBySuburbDto : IExecutionDto
    {
        public DtoContext Context { get; private set; }
        public GetProvinceBySuburbDto()
        {
            Context = DtoContext.NoContext();


        }

        public GetProvinceBySuburbDto(string suburb, string countrycode)
        {
            Context = DtoContext.NoContext();
            this.Suburb = suburb;
            this.CountryCode = countrycode;
        }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }


        public string Suburb { get; set; }
        public string CountryCode { get; set; }
    }
}
