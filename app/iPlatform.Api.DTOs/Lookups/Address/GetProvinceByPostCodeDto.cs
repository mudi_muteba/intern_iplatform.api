﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Lookups.Address
{
    public class GetProvinceByPostCodeDto : IExecutionDto
    {
        public DtoContext Context { get; private set; }
        public GetProvinceByPostCodeDto()
        {
            Context = DtoContext.NoContext();


        }

        public GetProvinceByPostCodeDto(string postcode, string countrycode)
        {
            Context = DtoContext.NoContext();
            this.PostCode = postcode;
            this.CountryCode = countrycode;
        }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }


        public string PostCode { get; set; }
        public string CountryCode { get; set; }
    }
}
