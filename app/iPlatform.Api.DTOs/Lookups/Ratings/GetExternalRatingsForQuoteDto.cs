﻿namespace iPlatform.Api.DTOs.Lookups.Ratings
{
    public class GetExternalRatingsForQuoteDto
    {
        public GetExternalRatingsForQuoteDto()
        {


        }
        public GetExternalRatingsForQuoteDto(object[] obj)
        {
            this.Request = obj[0] != null ? (string)obj[0] : "";
            this.Result = obj[1] != null ? (string)obj[1] : "";
        }

        public string Request { get; set; }
        public string Result { get; set; }
    }
}