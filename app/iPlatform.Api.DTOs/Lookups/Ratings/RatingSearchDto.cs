﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Lookups.Ratings
{
    public class RatingSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public int QuoteId { get; set; }
        public bool GetRatingRequest { get; set; }

        public RatingSearchDto()
        {
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();
            
            return validation;
        }
    }
}