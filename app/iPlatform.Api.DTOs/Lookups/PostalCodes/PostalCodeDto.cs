﻿using MasterData;

namespace iPlatform.Api.DTOs.Lookups.PostalCodes
{
    public class PostalCodeDto
    {
        public string Town { get; set; }
        public string City { get; set; }
        public string Code { get; set; }
        public int VisibleIndex { get; set; }
        public Country Country { get; set; }
    }
}