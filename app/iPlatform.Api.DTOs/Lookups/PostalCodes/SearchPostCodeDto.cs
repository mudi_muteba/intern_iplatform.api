﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using MasterData;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Lookups.PostalCodes
{
    public class SearchPostCodeDto : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public SearchPostCodeDto()
        {
            Context = DtoContext.NoContext();
        }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public string search { get; set; }

        public Country Country { get; set; }
        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();


            return validation;
        }
    }
}
