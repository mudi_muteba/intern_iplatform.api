﻿using System;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Ratings.Query
{
    public class RatingQueryDto
    {
        public RatingQueryDto()
        {
        }

        public List<RatingQueryItemDto> Quotes { get; set; }
    }

    public class RatingQueryItemDto
    {
        public RatingQueryItemDto()
        {
        }
        public string InsurerCode { get; set; }
        public string ProductCode { get; set; }
        public string Request { get; set; }
        public string Result { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime ResultDate { get; set; }

    }
}
