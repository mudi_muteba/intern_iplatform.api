using System.Collections.Generic;
using System.Xml.Serialization;
using MasterData;

namespace iPlatform.Api.DTOs.Ratings.Request
{
   
    public class RatingRequestPolicyDto
    {
        public  RatingRequestPolicyDto()
        {
            PaymentPlan = PaymentPlans.Monthly;
            Persons = new List<RatingRequestPersonDto>();
        }

        public PaymentPlan PaymentPlan { get; set; }
        public bool ITCConsent { get; set; }

		//[XmlArray("Persons")]
        //[XmlArrayItem("element")]
        public List<RatingRequestPersonDto> Persons { get; set; }
        public string Source { get; set; }

    }
}
