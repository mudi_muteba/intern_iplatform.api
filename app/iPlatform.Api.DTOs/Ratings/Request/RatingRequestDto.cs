using System;
using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using ValidationMessages;
using ValidationMessages.Ratings;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingRequestDto : IValidationAvailableDto, IExecutionDto
    {
        public RatingRequestDto()
        {
            Id = Guid.Empty;

            Items = new List<RatingRequestItemDto>();
            Policy = new RatingRequestPolicyDto();
            RatingContext = new RatingContextDto();
            PolicyHolderLossHistory = new RatingLossHistoryDto();

            ClientLossHome = new List<RatingRequestHomeLossDto>();
            ClientLossMotor = new List<RatingRequestMotorLossDto>();
            ClientLossHistory = new List<RatingRequestLossHistoryDto>();
            OverrideRatingQuestionAnswer = new List<OverrideRatingQuestionAnswerDto>();
            PlatformId = Guid.Empty;
            UserRatingSettings = new List<SettingsiRateUserDto>();
        }

        public Guid Id { get; set; }
        public string Source { get; set; }

        public RatingRequestPolicyDto Policy { get; set; }
        
        //[XmlArray("Items")]
        //[XmlArrayItem("element")]
        public List<RatingRequestItemDto> Items { get; set; }
        public RatingContextDto RatingContext { get; set; }
        public List<RatingRequestHomeLossDto> ClientLossHome { get; set; }
        public List<RatingRequestMotorLossDto> ClientLossMotor { get; set; }
        public List<RatingRequestLossHistoryDto> ClientLossHistory { get; set; }
        public List<OverrideRatingQuestionAnswerDto> OverrideRatingQuestionAnswer { get; set; }
        public Guid PlatformId { get; set; }

        public List<SettingsiRateUserDto> UserRatingSettings { get; set; }


        //added for telesure BE and external rating
        public string ExternalClientReference { get; set; }
        public string ExternalSystemReference { get; set; }
        public string TransactionType { get; set; }
        public string TransactionOption { get; set; }

        //public List<OverrideRatingQuestionDto> OverrideRatingQuestions { get; set; }
        
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        //needs a public setter
        public DtoContext Context { get; set; }
        public RatingLossHistoryDto PolicyHolderLossHistory { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            Validate(list);

            return list;
        }

        private void ValidateAnswers(List<ValidationErrorMessage> list)
        {
            if (Items == null)
                return;

            foreach (RatingRequestItemDto item in Items)
            {
                foreach (RatingRequestItemAnswerDto answer in item.Answers)
                {
                    ValidateAnswer(list, answer);
                }
            }
        }

        private static void ValidateAnswer(List<ValidationErrorMessage> list, RatingRequestItemAnswerDto answer)
        {
            try
            {
                answer.EnsureDeserialization();
            }
            catch (Exception)
            {
                var errorMessage = string.Format("Failed to validate question '{0}' with answer '{1}'",
                    answer.Question.Name, answer.QuestionAnswer);

                list.Add(RatingRequestValidationMessages.InvalidQuestionAnswer(errorMessage));
            }
        }

        private void ValidatePolicy(List<ValidationErrorMessage> list)
        {
            if (Policy == null)
            {
                list.Add(RatingRequestValidationMessages.NoPolicyInfo);
                return;
            }

            if (!Policy.Persons.Any())
            {
                list.Add(RatingRequestValidationMessages.NoPersons);
            }

            foreach (var person in Policy.Persons)
            {
                person.Validate(list, person);
            }
        }

        private void ValidateItems(List<ValidationErrorMessage> list)
        {
            if (Items == null || Items.Count == 0)
            {
                list.Add(RatingRequestValidationMessages.NoItemsAddedToRequest);
            }

            list.AddRange(from item in Items
                where !item.Answers.Any()
                select RatingRequestValidationMessages.NoAnswersForItem(item.AssetNo));
        }

        public void Validate(List<ValidationErrorMessage> list)
        {
            if (Id == Guid.Empty)
            {
                list.Add(RatingRequestValidationMessages.NoRatingReference);
            }

            if (RatingContext == null)
            {
                list.Add(RatingRequestValidationMessages.NoChannelProvided);
            }

            if (RatingContext != null && RatingContext.ChannelId == Guid.Empty)
            {
                list.Add(RatingRequestValidationMessages.NoChannelProvided);
            }

            ValidateItems(list);
            ValidatePolicy(list);
            ValidateAnswers(list);
        }
    }
}