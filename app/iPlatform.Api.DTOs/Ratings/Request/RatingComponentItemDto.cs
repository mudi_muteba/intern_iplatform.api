﻿using MasterData;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingComponentItemDto
    {
        public  int ComponentDefinitionId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public ComponentQuestionGroup QuestionGroup { get; set; }
        public QuestionType QuestionType { get; set; }
    }
}