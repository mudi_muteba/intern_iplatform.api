﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingRequestItemAccessoriesDto
    {
        public RatingRequestItemAccessoriesDto()
        {
            AccessoryAnswers = new List<RatingRequestItemAccessoriesAnswerDto>();
        }

        public int Id { get; set; }
        public List<RatingRequestItemAccessoriesAnswerDto> AccessoryAnswers { get; set; }
    }
}
