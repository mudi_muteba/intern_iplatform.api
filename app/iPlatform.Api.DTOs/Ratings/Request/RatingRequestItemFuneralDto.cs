﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingRequestItemFuneralDto
    {
        public RatingRequestItemFuneralDto()
        {
            FuneralAnswers = new List<RatingRequestItemFuneralAnswerDto>();
        }

        public string MemberIdentifier { get; set; }
        public string MemberIdNumber { get; set; }
        public List<RatingRequestItemFuneralAnswerDto> FuneralAnswers { get; set; }

    }
}
