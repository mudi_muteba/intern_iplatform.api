﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingRequestMotorLossDto
    {
        public virtual MotorTypeOfLoss MotorTypeOfLoss { get; set; }
        public virtual string DateOfLoss { get; set; }
        public virtual MotorClaimAmount MotorClaimAmount { get; set; }
        public virtual CurrentlyInsured CurrentlyInsured { get; set; }
        public virtual UninterruptedPolicy UninterruptedPolicy { get; set; }
        public virtual bool InsurerCancel { get; set; }
        public virtual MotorCurrentTypeOfCover MotorCurrentTypeOfCover { get; set; }
        public virtual bool PreviousComprehensiveInsurance { get; set; }
        public virtual MotorUninterruptedPolicyNoClaim MotorUninterruptedPolicyNoClaim { get; set; }
        public virtual string Why { get; set; }
        public string IdNumber { get; set; }
    }
}
