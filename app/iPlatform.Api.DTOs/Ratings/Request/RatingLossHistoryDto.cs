﻿using MasterData;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingLossHistoryDto
    {
        public int PartyId { get; set; }
        public string IdentityNumber { get; set; }
        public PartyType PartyType { get; set; }
        public bool IsPolicyHolder { get; set; }

        public List<RatingComponentItemDto> Items { get; set; }

        public RatingLossHistoryDto()
        {
            Items = new List<RatingComponentItemDto>();
        }
       

        //e.g "KP-Claimed For Incident" :  "true"
        //e.g "KP-LossValue" :  "12500.99"
        //e.g "KP-IncidentType" :  "MotorTypeOfLoss Accident -Collision -pothole"
        //e.g "KP-DateOfLoss" :  "2017-03-17"
    }
}