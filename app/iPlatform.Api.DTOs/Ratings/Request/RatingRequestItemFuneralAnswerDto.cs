﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingRequestItemFuneralAnswerDto
    {
        public Question Question { get; set; }

        public object QuestionAnswer { get; set; }
    }
}
