﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingLinkedProductsDTO
    {
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }

        public RatingLinkedProductsDTO()
        {

        }

        public RatingLinkedProductsDTO(int productId, string productcode, string productname)
        {
            this.ProductId = productId;
            this.ProductCode = productcode;
            this.ProductName = productname;
        }
    }
}
