using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using MasterData;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingRequestItemDto
    {
        public RatingRequestItemDto()
        {
            Answers = new List<RatingRequestItemAnswerDto>();
            VapCovers = new List<RatingRequestItemDto>();
            FuneralMembers = new List<RatingRequestItemFuneralDto>();
            Accessories = new List<RatingRequestItemAccessoriesDto>();
            DriverHomeLoss = new List<RatingRequestHomeLossDto>();
            DriverMotorLoss = new List<RatingRequestMotorLossDto>();
            DriverLossHistory = new List<RatingRequestLossHistoryDto>();
            MapVapQuestionDefinition = new List<MapVapQuestionDefinitionDto>();
            LossHistoryComponent = new List<RatingLossHistoryDto>();
            LinkedProducts = new List<RatingLinkedProductsDTO>();
            IsVap = false;
        }

        public RatingRequestItemDto(List<RatingRequestItemAnswerDto> answers)
        {
            Answers = answers;
            VapCovers = new List<RatingRequestItemDto>();
            FuneralMembers = new List<RatingRequestItemFuneralDto>();
            Accessories = new List<RatingRequestItemAccessoriesDto>();
            DriverHomeLoss = new List<RatingRequestHomeLossDto>();
            IsVap = false;
            Cover = Covers.Motor;
            AssetNo = 1;
            ParentAssetNo = 0;
            RatingDate = DateTime.UtcNow;
            LinkedProducts = new List<RatingLinkedProductsDTO>();
        }

  //      [XmlArray("Answers")]
		//[XmlArrayItem("element")]
        public List<RatingRequestItemAnswerDto> Answers { get; set; }
        public List<RatingLinkedProductsDTO> LinkedProducts { get; set; }
        public Cover Cover { get; set; }
        public string ProductCode { get; set; }
        
        public int AssetNo { get; set; }
        public string ExternalAssetNo { get; set; }
        public int ParentAssetNo { get; set; }
        public bool PremiumIncluded { get; set; }
        public DateTime RatingDate { get; set; }

        //[XmlArray("VapCovers")]
        //[XmlArrayItem("element")]
        public List<RatingRequestItemDto> VapCovers { get; set; }
        public bool IsVap { get; set; }

        //[XmlArray("LinkedItems")]
        //[XmlArrayItem("element")]
        public List<int> LinkedItems { get; set; }
        public List<RatingRequestItemFuneralDto> FuneralMembers { get; set; }
        public List<RatingRequestItemAccessoriesDto> Accessories { get; set; }
        public List<RatingRequestHomeLossDto> DriverHomeLoss { get; set; }
        public List<RatingRequestMotorLossDto> DriverMotorLoss { get; set; }
        public List<RatingRequestLossHistoryDto> DriverLossHistory { get; set; }
        public List<MapVapQuestionDefinitionDto> MapVapQuestionDefinition { get; set; }
       
        public List<RatingLossHistoryDto> LossHistoryComponent { get; set; }
               
    }
}