using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using MasterData;
using ValidationMessages;
using ValidationMessages.Ratings;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingRequestPersonDto
    {
        public RatingRequestPersonDto()
        {
            Addresses = new List<RatingRequestAddressDto>();
            CommunicationPreferences = new List<PartyCorrespondencePreferenceDto>();
            MaritalStatus = MaritalStatuses.Single;
            Title = Titles.Mr;
            Language = Languages.English;
            Judgement = false;
            Administration = false;
            Sequestrated = false;

            DriverHomeLoss = new List<RatingRequestHomeLossDto>();
            DriverMotorLoss = new List<RatingRequestMotorLossDto>();
            DriverLossHistory = new List<RatingRequestLossHistoryDto>();
    }

        public string Surname { get; set; }
        public string Initials { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public string IdNumber { get; set; }
        public Title Title { get; set; }
        public Language Language { get; set; }
        public bool PolicyHolder { get; set; }
        public Gender Gender { get; set; }
        public List<RatingRequestAddressDto> Addresses { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string FirstName { get; set; }
        public int OccupationId { get; set; }
        public string Occupation { get; set; }
        public string PhoneWork { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneDirect { get; set; }
        public string PhoneCell { get; set; }
        public string PhoneFax { get; set; }
        public string Email { get; set; }
        public string PreferredName { get; set; }
        public bool Judgement { get; set; }
        public string JudgementReason { get; set; }
        public bool Administration { get; set; }
        public string AdministrationReason { get; set; }
        public bool Sequestrated { get; set; }
        public string SequestratedReason { get; set; }
        public string ExternalReference { get; set; }
        public List<PartyCorrespondencePreferenceDto> CommunicationPreferences { get; set; }

        public List<RatingRequestHomeLossDto> DriverHomeLoss { get; set; }
        public List<RatingRequestMotorLossDto> DriverMotorLoss { get; set; }
        public List<RatingRequestLossHistoryDto> DriverLossHistory { get; set; }

        public void Validate(List<ValidationErrorMessage> list, RatingRequestPersonDto person)
        {
            if (string.IsNullOrWhiteSpace(IdNumber) && !DateOfBirth.HasValue)
            {
                list.Add(RatingRequestValidationMessages.InvalidPerson);
            }

            if (!Addresses.Any())
            {
                list.Add(RatingRequestValidationMessages.NoAddresses);
            }

            foreach (var address in Addresses)
            {
                address.Validate(list, address);
            }
        }
    }
}