﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingRequestHomeLossDto
    {
        public RatingRequestHomeLossDto()
        {
        }
        public HomeTypeOfLoss HomeTypeOfLoss { get; set; }
        public string DateOfLoss { get; set; }
        public HomeClaimAmount HomeClaimAmount { get; set; }
        public HomeClaimLocation HomeClaimLocation { get; set; }
        public CurrentlyInsured CurrentlyInsured { get; set; }
        public UninterruptedPolicy UninterruptedPolicy { get; set; }
        public bool InsurerCancel { get; set; }
        public string Why { get; set; }
        public bool IsDeleted { get; set; }
        public string IdNumber { get; set; }
    }
}
