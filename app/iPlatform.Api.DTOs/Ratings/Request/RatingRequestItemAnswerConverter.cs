﻿using System;
using MasterData;
using Newtonsoft.Json;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingRequestItemAnswerConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var dto = value as RatingRequestItemAnswerDto;

            if (dto != null && dto.Question.QuestionType.Id == QuestionTypes.Dropdown.Id)
            {
                serializer.Serialize(writer, dto.QuestionAnswer as QuestionAnswer);
            }
            else
            {
                writer.WriteValue(value);
            }

            writer.Flush();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = serializer.Deserialize(reader);

            return value as RatingRequestItemAnswerDto;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(RatingRequestItemAnswerDto);
        }
    }
}