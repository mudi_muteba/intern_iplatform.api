using System.Collections.Generic;
using System.Linq;
using MasterData;
using Newtonsoft.Json;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    internal static class RatingRequestExtentions
    {
        public static void EnsureDeserialization(this RatingRequestItemAnswerDto answer)
        {
            //Hander Year Of Manufacture as text
            if (answer.Question.Id == Questions.YearOfManufacture.Id)
                return;

            if (answer.Question.QuestionType.Id.Equals(QuestionTypes.Dropdown.Id) && answer.QuestionAnswer != null)
            {
                answer.QuestionAnswer = answer.QuestionAnswer.CastAsQuestionAnswer();
            }
        }

        public static decimal GetSumInsured(this IEnumerable<RatingRequestItemAnswerDto> answers)
        {
            var sumInsured = 0m;

            if (answers == null)
                return sumInsured;

            var answer = answers.FirstOrDefault(x => x.Question.Id == Questions.SumInsured.Id);

            if (answer == null)
                return sumInsured;

            return decimal.TryParse(answer.QuestionAnswer as string, out sumInsured) ? sumInsured : sumInsured;
        }

        public static QuestionAnswer CastAsQuestionAnswer(this object answer)
        {
            var questionAnswer = answer as QuestionAnswer;

            if (questionAnswer == null)
                return JsonConvert.DeserializeObject<QuestionAnswer>(answer.ToString());

            return questionAnswer;
        }

    }
}