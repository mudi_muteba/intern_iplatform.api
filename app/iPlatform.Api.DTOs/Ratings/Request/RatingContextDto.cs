﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingContextDto
    {
        public RatingContextDto()
        {
            RatingEngines = new List<RatingSettingDto>();
        }

        //[XmlArray("RatingEngines")]
        //[XmlArrayItem("element")]
        public List<RatingSettingDto> RatingEngines { get; set; }
        public Guid ChannelId { get; set; }
    }
}