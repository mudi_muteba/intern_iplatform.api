﻿namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class ITCSettingDto
     {
        public bool CheckRequired { get; set; }
        public string SubscriberCode { get; set; }
        public string BranchNumber { get; set; }
        public string BatchNumber { get; set; }
        public string SecurityCode { get; set; }
        public string EnquirerContactName { get; set; }
        public string EnquirerContactPhoneNo { get; set; }
        public string Environment { get; set; }
     }
}
