﻿using MasterData;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingRequestLossHistoryDto
    {
        public virtual CurrentlyInsured CurrentlyInsured { get; set; }
        public virtual UninterruptedPolicy UninterruptedPolicy { get; set; }
        public bool InsurerCancel { get; set; }
        public bool PreviousComprehensiveInsurance { get; set; }
        public virtual MotorUninterruptedPolicyNoClaim MotorUninterruptedPolicyNoClaimId { get; set; }
        public bool InterruptedPolicyClaim { get; set; }
        public string IdNumber { get; set; }
        public bool MissedPremiumLast6Months { get; set; }
    }
}
