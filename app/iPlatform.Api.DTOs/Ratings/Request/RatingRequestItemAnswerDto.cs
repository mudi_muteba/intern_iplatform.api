using MasterData;

namespace iPlatform.Api.DTOs.Ratings.Request
{   
    public class RatingRequestItemAnswerDto
    {
        public Question Question { get; set; }

        public object QuestionAnswer { get; set; }
    }
}