using System.Collections.Generic;
using MasterData;
using ValidationMessages;
using ValidationMessages.Ratings;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingRequestAddressDto
    {
        public RatingRequestAddressDto()
        {
            AddressType = AddressTypes.PhysicalAddress;
        }

        public AddressType AddressType { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Complex { get; set; }
        public string City { get; set; }
        public string Suburb { get; set; }
        public string PostalCode { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public bool DefaultAddress { get; set; }
        public string ExternalAddressID { get; set; }
        public string ExternalAddressReference { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public void Validate(List<ValidationErrorMessage> list, RatingRequestAddressDto address)
        {
            if (
                string.IsNullOrWhiteSpace(Suburb) &&
                string.IsNullOrWhiteSpace(PostalCode)
                )
            {
                list.Add(RatingRequestValidationMessages.InvalidAddress);
            }
        }
    }
}