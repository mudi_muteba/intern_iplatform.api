﻿using System;

namespace iPlatform.Api.DTOs.Ratings.Request
{
    public class RatingSettingDto
    {
        public string InsurerCode { get; set; }
        public string ProductCode { get; set; }

        public DateTime OriginalQuoteDate { get; set; } //date of first rating of quote
        public DateTime EffectiveDate { get; set; } //date policy or cover is effective from

        public string AgentCode { get; set; }
        public string AuthCode { get; set; }
        public string BrokerCode { get; set; }
        public string Password { get; set; }
        public string SchemeCode { get; set; }
        public string SubscriberCode { get; set; }
        public string Token { get; set; }
        public string UserName { get; set; }
        public string Environment { get; set; }
        public string CompanyCode { get; set; }
        public string UwCompanyCode { get; set; }
        public string UwProductCode { get; set; }
        public string Culture { get; set; }

        public ITCSettingDto ITCSettings { get; set; }

        //Telesure Details
        public string Tsas400TransferFromCompany { get; set; }
        public string TsUnderwritingProductCode { get; set; }
        public string TsUnderwritingCompanyCode { get; set; }
        public string TsVdn { get; set; }
        public string TsBrandCode { get; set; }
        public string TsBrokerCode { get; set; }
        public string TsCompanyCode { get; set; }

        public string TsbcEmail { get; set; }
        public string TsbcMobileNumber { get; set; }
        public string TsbcPhoneNumber { get; set; }
        public string TsbcName { get; set; }
        public string TsbcSurname { get; set; }

        
    }
}
