﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Ratings;

namespace iPlatform.Api.DTOs.Ratings.Quoting
{
    public class AcceptQuoteDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
        public int QuoteId { get; set; }
        public string Comments { get; set; }
        public int Id
        {
            get { return QuoteId; }
        }
        public int BrokerUserId { get; set; }
        public int AccountExecutiveUserId { get; set; }
        public int ChannelId { get; set; }
        public DtoContext Context { get; set; }
        public AcceptQuoteDto()
        {
            Context = DtoContext.NoContext();
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (QuoteId == 0)
                validation.Add(QuoteDistributionTrackingValidationMessages.InvalidQuote);

            return validation;
        }
    }
}