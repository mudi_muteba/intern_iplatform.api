﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Ratings;

namespace iPlatform.Api.DTOs.Ratings.Quoting
{
    public class DeleteQuoteAcceptDto : IValidationAvailableDto, IExecutionDto, IAffectExistingEntity
    {
        public DeleteQuoteAcceptDto()
        {

        }

        public DeleteQuoteAcceptDto(int id)
        {
            Id = id;
        }
        public int Id { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id == 0)
                list.Add(QuoteAcceptanceValidationMessages.EmptyQuoteId);
          
            return list;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}