﻿using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Ratings.AdditionalInfo;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using iPlatform.Api.DTOs.Users;
using MasterData;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace iPlatform.Api.DTOs.Ratings.Quoting
{
    [XmlInclude(typeof(QuestionAnswer))]
    public class QuoteDto
    {
        public QuoteDto()
        {
            Request = new RatingRequestDto();
            Policy = new RatingResultPolicyDto();
            InsuredInfo = new InsuredInfoDto();
            CampaignInfo = new CampaignInfoDto();
            Items = new List<QuoteItemDto>();
        }

        public UserInfoDto UserInfo { get; set; }
        public RatingRequestDto Request { get; set; }
        public RatingResultPolicyDto Policy { get; set; }
        public InsuredInfoDto InsuredInfo { get; set; }
        public CampaignInfoDto CampaignInfo { get; set; }
        public PaymentDetailsInfoDto PaymentDetailsInfo { get; set; }
        public List<QuoteItemDto> Items { get; set; }
        public int BrokerUserId { get; set; }
        public string BrokerUserExternalReference { get; set; }
        public int AccountExecutiveUserId { get; set; }
        public string AccountExecutiveExternalReference { get; set; }
        public int ChannelId { get; set; }
        public string ChannelExternalReference { get; set; }
        public string ApiUrl { get; set; }
        public Country Country { get; set; }

        protected bool Equals(QuoteDto other)
        {
            return Equals(Request, other.Request) && Equals(Policy, other.Policy) && Equals(InsuredInfo, other.InsuredInfo);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((QuoteDto)obj);
        }

        public QuoteDto SetUserInfo(string userName, int userId, string firstName, string lastName)
        {
            UserInfo = new UserInfoDto
            {
                UserName = userName,
                UserId = userId,
                FirstName = firstName,
                LastName = lastName
            };

            return this;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return 0;
            }
        }
    }
}
