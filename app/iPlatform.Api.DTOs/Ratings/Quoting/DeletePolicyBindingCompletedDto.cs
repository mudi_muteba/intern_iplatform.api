﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using System;
using Shared.Extentions;

namespace iPlatform.Api.DTOs.Ratings.Quoting
{
    public class DeletePolicyBindingCompletedDto : IValidationAvailableDto, IExecutionDto, IAffectExistingEntity
    {
        public DeletePolicyBindingCompletedDto()
        {

        }

        public DeletePolicyBindingCompletedDto(int id, Guid requestId)
        {
            Id = id;
            RequestId = requestId;
        }
        public int Id { get; set; }
        public Guid RequestId { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id == 0)
            {
                list.Add(PolicyBindingValidationMessages.EmptyQuoteId);
            }
                
            if(RequestId.IsEmpty())
            {
                list.Add(PolicyBindingValidationMessages.EmptyRequestId);
            }
            
            return list;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}