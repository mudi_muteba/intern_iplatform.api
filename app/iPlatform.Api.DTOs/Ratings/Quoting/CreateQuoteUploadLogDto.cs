﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Ratings.Quoting
{
    public class CreateQuoteUploadLogDto : IValidationAvailableDto, IExecutionDto
    {
        public CreateQuoteUploadLogDto()
        {
            Context = DtoContext.NoContext();
        }

        public CreateQuoteUploadLogDto(int quoteId, decimal premium, decimal fees, decimal sasria, int productid,
            string requestId, string message, string quoteUploadObject = "")
        {
            QuoteId = quoteId;
            Premium = premium;
            Fees = fees;
            Sasria = sasria;
            ProductId = productid;
            RequestId = requestId;
            Context = DtoContext.NoContext();
            Message = message;
            QuoteUploadObject = quoteUploadObject;
        }

        [DataMember]
        public int QuoteId { get; set; }
        [DataMember]
        public decimal Premium { get; set; }
        [DataMember]
        public decimal Fees { get; set; }
        [DataMember]
        public decimal Sasria { get; set; }
        [DataMember]
        public int ProductId { get; set; }
        [DataMember]
        public string RequestId { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string InsurerReference { get; set; }
        [DataMember]
        public string QuoteUploadObject { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (QuoteId <= 0)
                validation.Add(QuoteValidationMessages.QuoteIdEmpty);

            if (ProductId <= 0)
                validation.Add(QuoteValidationMessages.ProductIdRequired);

            return validation;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}