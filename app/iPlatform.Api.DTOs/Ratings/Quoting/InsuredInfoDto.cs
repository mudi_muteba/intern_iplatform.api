using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;
using ValidationMessages;
using ValidationMessages.Ratings;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Ratings.Quoting
{
    public class InsuredInfoDto 
    {
        public InsuredInfoDto()
        {
            CommunicationPreferences = new List<PartyCorrespondencePreferenceDto>();
        }

        public Title Title { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string IDNumber { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public string ContactNumber { get; set; }
        public string EmailAddress { get; set; }
        public bool ITCConsent { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PinNo { get; set; }
        public Gender Gender { get; set; }

        public string ExternalReference { get; set; }

        public void Validate(List<ValidationErrorMessage> list)
        {
            if (string.IsNullOrWhiteSpace(IDNumber) && !DateOfBirth.HasValue)
            {
                list.Add(QuoteAcceptanceValidationMessages.InvalidInsured);
            }
        }

        public List<PartyCorrespondencePreferenceDto> CommunicationPreferences { get; set; } 
    }
}