﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Ratings.Quoting
{
    public class QuoteItemDto
    {
        public QuoteItemDto()
        {
            SecondLevelResponses = new List<SecondLevelQuestionResponseDto>();
        }

        public Cover Cover { get; set; }
        public decimal SumInsured { get; set; }
        public string SumInsuredDescription { get; set; }
        public decimal Premium { get; set; }
        public decimal DiscountedPremium { get; set; }
        public decimal DiscountedPercentage { get; set; }
        public decimal Sasria { get; set; }
        public decimal SasriaCalulated { get; set; }
        public decimal SasriaShortfall { get; set; }
        public int AssetNumber { get; set; }
        public virtual bool ExcessCalculated { get; set; }
        public decimal ExcessBasic { get; set; }
        public string ExcessDescription { get; set; }

        public List<SecondLevelQuestionResponseDto> SecondLevelResponses { get; set; }
    }
}
