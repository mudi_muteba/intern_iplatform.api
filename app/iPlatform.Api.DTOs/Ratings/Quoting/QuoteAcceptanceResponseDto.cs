using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Ratings.Quoting
{
    public class QuoteAcceptanceResponseDto : Resource
    {
        public QuoteAcceptanceResponseDto()
        {
        }

        public Guid QuoteReference { get; set; }
    }
}