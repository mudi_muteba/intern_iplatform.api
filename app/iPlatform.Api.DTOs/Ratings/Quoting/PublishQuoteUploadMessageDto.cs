﻿using System;
using System.Collections.Generic;
using System.Globalization;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.QuoteAcceptance;
using iPlatform.Api.DTOs.Ratings.AdditionalInfo;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using ValidationMessages;
using ValidationMessages.Ratings;
using MasterData;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Ratings.Quoting
{
    public class PublishQuoteUploadMessageDto : IValidationAvailableDto, IExecutionDto
    {
        public PublishQuoteUploadMessageDto()
        {
            Request = new RatingRequestDto();
            Policy = new RatingResultPolicyDto();
            InsuredInfo = new InsuredInfoDto();
            CampaignInfo = new CampaignInfoDto();
            PaymentDetailsInfo = new PaymentDetailsInfoDto();
            ChannelInfo = new ChannelDto();
            Reference = Guid.NewGuid(); // this is going to be used in quoteacceptance workflow process to update policyno
            Items = new List<QuoteItemDto>();
        }

        public int Id { get; set; }
        public Guid Reference { get; set; }
        public RatingRequestDto Request { get; set; }
        public RatingResultPolicyDto Policy { get; set; }
        public InsuredInfoDto InsuredInfo { get; set; }
        public CampaignInfoDto CampaignInfo { get; set; }
        public PaymentDetailsInfoDto PaymentDetailsInfo { get; set; }
        public ChannelDto ChannelInfo { get; set; }
        public int LeadId { get; set; }
        public List<QuoteItemDto> Items { get; set; }
        public int QuoteAcceptedLeadActivityId { get; set; }
        public NumberFormatInfo NumberFormatInfo { get; set; } // used for quote acceptance email notification fees, premiums currency symbol
        public string QuoteAcceptanceEnvironment { get; set; } // used for quote acceptance email notification, displaying from which environment does this quote acceptance originates 
        public string WorkflowEnvironment { get; set; } // used during testing to bypass quote acceptance  call to insurer, also used to define rabbitmq queue name
        public EchoTCFDto EchoTCF { get; set; } // passing down echotcf configs
        public string Environment { get; set; } // is it Dev/UAT/Staging/Live
        public AgentDetailDto AgentDetail { get; set; }
        

        public int BrokerUserId { get; set; }
        public string BrokerUserExternalReference { get; set; }
        public int AccountExecutiveUserId { get; set; }
        public string AccountExecutiveExternalReference { get; set; }
        public int ChannelId { get; set; }
        public string ChannelExternalReference { get; set; }
        public string ApiUrl { get; set; }
        public Country Country { get; set; }//Added for upload to ids to know what banking details to use 

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Policy == null)
            {
                list.Add(QuoteAcceptanceValidationMessages.NoPolicyAccepted);
            }

            if (Policy != null)
            {
                Policy.Validate(list);
            }

            if (Request == null)
            {
                list.Add(QuoteAcceptanceValidationMessages.NoRatingRequest);
            }

            if (Request != null)
            {
                Request.Validate(list);
            }

            if (InsuredInfo == null)
            {
                list.Add(QuoteAcceptanceValidationMessages.NoInsuredInfo);
            }

            if (InsuredInfo != null)
            {
                InsuredInfo.Validate(list);
            }

            return list;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public void SetNumberFormat(CultureParameter cultureParam)
        {
            var cultureVisitor = new CultureVisitor(cultureParam);
            NumberFormatInfo = CultureVisitor.GetCurrencyFormatProviderSymbolDecimals(cultureVisitor.Currency.Code);
        }

        public DtoContext Context { get; set; }
    }
}