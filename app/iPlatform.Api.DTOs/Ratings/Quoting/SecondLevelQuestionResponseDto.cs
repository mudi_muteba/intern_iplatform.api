﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Ratings.Quoting
{
    public class SecondLevelQuestionResponseDto
    {
        public SecondLevelQuestion Question { get; set; }
        public string Answer { get; set; }
    }
}
