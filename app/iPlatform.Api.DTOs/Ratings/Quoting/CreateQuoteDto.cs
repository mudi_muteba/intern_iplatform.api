﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Ratings.Quoting
{
    public class CreateQuoteDto : IExecutionDto, IChannelAwareDto
    {
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public Guid RequestId { get; set; }
        public DtoContext Context { get; private set; }
        public int IndividualId { get; set; }
        public int ProposalHeaderId { get; set; }
        public int ChannelId { get; set; }
        public string Source { get; set; }

    }
}