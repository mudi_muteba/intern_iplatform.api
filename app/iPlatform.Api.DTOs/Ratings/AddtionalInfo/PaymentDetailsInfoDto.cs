﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Ratings.AdditionalInfo
{
    public class PaymentDetailsInfoDto: ICultureAware
    {
        public PaymentDetailsInfoDto()
        { }
        public int Id { get; set; }
        public int BankDetailsId { get; set; }
        public int QuoteId { get; set; }
        public DateTimeDto DebitOrderDate { get; set; }
        public DateTimeDto InceptionDate { get; set; }
        public MethodOfPayment MethodOfPayment { get; set; }
        public PaymentPlan PaymentPlan { get; set; }
        public DateTimeDto TermEndDate { get; set; }
        public string BankAccHolder { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string BankBranchCode { get; set; }
        public string AccountNo { get; set; }
        public string TypeAccount { get; set; }
        public DateTimeDto TakeOnStrikeDate { get; set; } 

    }
}
