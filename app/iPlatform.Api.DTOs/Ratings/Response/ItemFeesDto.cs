using System.Collections.Generic;
using System.Linq;

namespace iPlatform.Api.DTOs.Ratings.Response
{
    public class ItemFeesDto
    {
        public ItemFeesDto()
        {
            Fees = new List<ItemFeeDto>();
        }

        public List<ItemFeeDto> Fees { get; set; }

        public ItemFeesDto AddFee(decimal fee)
        {
            Fees.Add(new ItemFeeDto(fee));
            return this;
        }

        public ItemFeesDto AddFee(ItemFeeDto fee)
        {
            Fees.Add(fee);
            return this;
        }

        public decimal TotalFees()
        {
            return Fees.Sum(s => s.Fee);
        }
    }
}