using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace iPlatform.Api.DTOs.Ratings.Response
{
    public class RatingResultState
    {
        public RatingResultState()
        {
            Errors = new List<string>();
            Warnings = new List<string>();
        }

        
        //[XmlArray("Errors")]
        //[XmlArrayItem("Error")]
        public List<string> Errors { get; set; }

        //XmlArray("Warnings")]
        //[XmlArrayItem("Warning")]
        public List<string> Warnings { get; set; }

        public static RatingResultState CreateWithError(string error)
        {
            var state = new RatingResultState();

            state.AddError(error);

            return state;
        }

        public static RatingResultState CreateWithError(List<string> errors)
        {
            var state = new RatingResultState();

            state.AddError(errors);

            return state;
        }

        public static RatingResultState CreateWithWarning(string warning)
        {
            var state = new RatingResultState();

            state.AddWarning(warning);

            return state;
        }

        public static RatingResultState CreateWithWarning(List<string> warnings)
        {
            var state = new RatingResultState();

            state.AddWarning(warnings);

            return state;
        }

        public void AddError(string error)
        {
            if (!string.IsNullOrWhiteSpace(error))
                Errors.Add(error);
        }

        public void AddError(List<string> errors)
        {
            errors.ForEach(AddError);
        }

        public void AddWarning(string warning)
        {
            if (!string.IsNullOrWhiteSpace(warning))
                Warnings.Add(warning);
        }

        public void AddWarning(List<string> warnings)
        {
            warnings.ForEach(AddWarning);
        }

        public RatingResultStatus GetStatus()
        {
            if (Errors.Count > 0)
                return RatingResultStatus.Failure;

            if (Warnings.Count > 0)
                return RatingResultStatus.Warning;

            return RatingResultStatus.Success;
        }

        public override string ToString()
        {
            if (Errors.Count == 0 && Warnings.Count == 0)
                return GetStatus().ToString();

            var builder = new StringBuilder();

            if (Errors.Count > 0)
                builder.AppendLine("Errors:");

            foreach (var error in Errors)
            {
                builder.AppendLine(error);
            }

            if (Warnings.Count > 0)
                builder.AppendLine("Warnings:");

            foreach (var warning in Warnings)
            {
                builder.AppendLine(warning);
            }

            return builder.ToString();
        }
    }
}