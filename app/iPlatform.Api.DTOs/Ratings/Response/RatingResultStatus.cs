namespace iPlatform.Api.DTOs.Ratings.Response
{
    public enum RatingResultStatus
    {
        Success,
        Warning,
        Failure
    }
}