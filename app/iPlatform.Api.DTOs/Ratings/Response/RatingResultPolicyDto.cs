using System.Collections.Generic;
using System.Xml.Serialization;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Products;
using ValidationMessages;
using ValidationMessages.Ratings;
using System;

namespace iPlatform.Api.DTOs.Ratings.Response
{
    public class RatingResultPolicyDto: ICultureAware
    {
        public RatingResultPolicyDto()
        {
            Items = new List<RatingResultItemDto>();
            State = new RatingResultState();
            AboutProduct = new AboutProductDto();
            InspectionStations = new List<RatingResultInspectionStationDto>();
            OtherFees = new ItemFeesDto();
        }

        public RatingResultState State { get; set; }

        public int ProductId { get; set; }
        public string Source { get; set; }
        public string InsurerCode { get; set; }
        public string InsurerName { get; set; }
        public string InsurerLogoPath { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string InsurerProductCode { get; set; }
        public string InsurerReference { get; set; }
        public string ExternalRatingResult { get; set; }
        public string ExternalRatingRequest { get; set; }

        public AboutProductDto AboutProduct { get; set; }
        public decimal Fees { get; set; }
        public ItemFeesDto OtherFees { get; set; }

        public DateTime? EffectiveDate { get; set; }

        //[XmlArray("Items")]
        //[XmlArrayItem("element")]
        public List<RatingResultItemDto> Items { get; set; }
        public string ITCScore { get; set; }
        public string ExtraITCScore { get; set; }

        public bool CreatePolicy { get; set; }

        public List<RatingResultInspectionStationDto> InspectionStations { get; set; }

        public void Validate(List<ValidationErrorMessage> list)
        {
            if (string.IsNullOrWhiteSpace(InsurerCode) && string.IsNullOrWhiteSpace(ProductCode))
            {
                list.Add(QuoteAcceptanceValidationMessages.NoPolicyAccepted);
            }
        }

        public void ValidateCreation(List<ValidationErrorMessage> list)
        {
            if (string.IsNullOrWhiteSpace(InsurerCode))
            {
                list.Add(QuoteCreationValidationMessages.NoInsurerCode);
            }

            if (string.IsNullOrWhiteSpace(ProductCode))
            {
                list.Add(QuoteCreationValidationMessages.NoProductCode);
            }
        }
    }
}