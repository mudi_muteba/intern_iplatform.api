using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Ratings.Response
{
   
    public class ItemFeeDto
    {
        public ItemFeeDto(decimal fee) : this(string.Empty, fee, false)
        {
        }

        public ItemFeeDto(string reason, decimal fee, bool onceOff)
        {
            Reason = reason;
            Fee = fee;
            OnceOff = onceOff;
        }

        public ItemFeeDto()
        {
        }

        public string Reason { get; set; }
        public decimal Fee { get; set; }
        public bool OnceOff { get; set; }
    }
}