﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Ratings.Response
{

    public class RatingResultItemExcessDto
    {
        public bool Calculated { get; set; }
        public decimal Basic { get; set; }
        public string Description { get; set; }

        public decimal PremiumDifference { get; set; }
        public decimal Premium { get; set; }
        public string ExcessCode { get; set; }
    }
}