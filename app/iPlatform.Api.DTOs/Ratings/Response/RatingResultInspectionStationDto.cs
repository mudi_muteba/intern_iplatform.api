﻿namespace iPlatform.Api.DTOs.Ratings.Response
{
    public class RatingResultInspectionStationDto
    {
        public string Name { get; set; }
        public string WorkingHours { get; set; }
        public string PhoneWork { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Suburb { get; set; }
        public string PostalCode { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }
    }
}
