using System.Collections.Generic;
using MasterData;

namespace iPlatform.Api.DTOs.Ratings.Response
{
    public class RatingResultItemDto
    {
        public RatingResultItemDto()
        {
            State = new RatingResultState();
            Excess = new RatingResultItemExcessDto();
            Fees = new ItemFeesDto();
            Excesses = new List<RatingResultItemExcessDto>();
            InspectionRequired = false;
            Clauses = new List<string>();
        }

        public RatingResultState State { get; set; }
        public Cover Cover { get; set; }
        public int AssetNo { get; set; }
        public decimal Premium { get; set; }
        public decimal Sasria { get; set; }
        public decimal SumInsured { get; set; }
        public RatingResultItemExcessDto Excess { get; set; }
        public ItemFeesDto Fees { get; set; }
        public bool InspectionRequired { get; set; }
        public List<string> Clauses { get; set; }
        public string SumInsuredDescription { get; set; }
        public List<RatingResultItemExcessDto> Excesses { get; set; }

        public decimal PremiumWithFees
        {
            get { return Premium + Fees.TotalFees(); }
            set { }
        }

        //Added new 
        public decimal DiscountedPremium { get; set; }
        public decimal DiscountedPercentage { get; set; }
        public decimal DiscountedDifference { get; set; }

        public decimal SasriaCalulated { get; set; }
        public decimal SasriaShortfall { get; set; }

        public bool IsVap { get; set; }
        public string Description { get; set; }
        public int VapQuestionId { get; set; }


    }
}