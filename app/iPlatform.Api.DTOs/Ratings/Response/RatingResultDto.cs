using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using ValidationMessages;
using ValidationMessages.Ratings;

namespace iPlatform.Api.DTOs.Ratings.Response
{
    public class RatingResultDto : IExecutionDto, IValidationAvailableDto
    {
        public RatingResultDto()
        {
            Policies = new List<RatingResultPolicyDto>();
            State = new RatingResultState();
        }

        //[XmlArray("Policies")]
        //[XmlArrayItem("element")]
        public List<RatingResultPolicyDto> Policies { get; set; }
        public RatingResultState State { get; set; }
        public Guid RatingRequestId { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            Validate(list);

            return list;
        }

        private void Validate(List<ValidationErrorMessage> list)
        {
            if (Policies == null)
            {
                list.Add(QuoteCreationValidationMessages.NoPolicyAccepted);
            }

            if (State == null)
            {
                list.Add(QuoteCreationValidationMessages.NoStateDefined);
            }
            
            if(Policies != null)
            {
                foreach (var policy in Policies)
                {
                    policy.ValidateCreation(list);
                }
            }
        }
    }
}