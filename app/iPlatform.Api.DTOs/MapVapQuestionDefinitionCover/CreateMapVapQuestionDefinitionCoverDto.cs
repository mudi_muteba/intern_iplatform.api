﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.MapVapQuestionDefinitionCover
{
    public class CreateMapVapQuestionDefinitionCoverDto : AttributeValidationDto, IExecutionDto
    {
        public CreateMapVapQuestionDefinitionCoverDto()
        {

        }
        public DtoContext Context { get; private set; }

        public int MapVapQuestionDefinitionCoverId { get; set; }

        public int CoverId { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        #region [ Validation ]

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            List<ValidationErrorMessage> errorMessages = new List<ValidationErrorMessage>();
            if (MapVapQuestionDefinitionCoverId == 0)
                errorMessages.Add(MapVapQuestionDefinitionCoverValidationMessages.MapVapQuestionDefinitionCoverHeaderIdRequired);
            if (CoverId == 0)
                errorMessages.Add(MapVapQuestionDefinitionCoverValidationMessages.CoverIdRequired);

            return errorMessages;
        }

        #endregion
    }
}
