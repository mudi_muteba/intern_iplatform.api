﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using MasterData;
using Newtonsoft.Json;

namespace iPlatform.Api.DTOs.MapVapQuestionDefinitionCover
{
    public class MapVapQuestionDefinitionCoverDto : Resource, ICultureAware
    {
        public MapVapQuestionDefinitionCoverDto()
        {

        }
        [JsonIgnore]
        public MapVapQuestionDefinitionDto MapVapQuestionDefinition { get; set; }

        public Cover Cover { get; set; }
    }
}
