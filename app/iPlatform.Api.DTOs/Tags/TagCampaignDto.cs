﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Tags
{
    public class TagCampaignDto : Resource
    {
        public TagCampaignDto() { }
        public TagDto Tag { get; set; }
    }
}
