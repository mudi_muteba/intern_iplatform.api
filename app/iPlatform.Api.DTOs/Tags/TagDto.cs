﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Tags
{
    public class TagDto : Resource
    {
        public TagDto()
        {

        }

        public string Name { get; set; }
    }
}
