﻿using System;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Audit
{
    public class iAdminUserInfoDto
    {
        public iAdminUserInfoDto()
        {

        }
        public int UserId { get; set; }
        public string Username { get; set; }
    }
}
