﻿using iPlatform.Api.DTOs.Base;
using System;
using Shared;

namespace iPlatform.Api.DTOs.Audit
{
    public class CreateAuditLogDto : IExecutionDto
    {
        public string UserAgent { get; set; }
        public string Referrer { get; set; }
        public Guid RequestId { get; set; }
        public string RequestIp { get; set; }
        public string RequestType { get; set; }
        public DateTime? RequestDate { get; set; }
        public string RequestMethod { get; set; }
        public string RequestUrl { get; set; }
        public string ChannelIds { get; set; }
        public int ActiveChannelId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string AuditEntryType { get; set; }
        public string AuditEntryDescription { get; set; }
        public string EntityName { get; set; }
        public int EntityId { get; set; }
        public string FieldName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public Guid ActiveChannelSystemId { get; set; }
        public DateTime Timestamp { get; set; }
        public DtoContext Context { get; private set; }

        public CreateAuditLogDto() { }

        public CreateAuditLogDto(IExecutionContext executionContext, string auditEntryType, string auditEntryDescription, string entityFullName, int entityId, DateTime timestamp, string fieldName = "", string oldValue = "", string newValue = "")
        {
            if (executionContext != null)
            {
                UserAgent = executionContext.UserAgent;
                Referrer = executionContext.Referrer;
                RequestId = executionContext.RequestId;
                RequestType = executionContext.RequestType;
                RequestIp = executionContext.RequestIp;
                RequestDate = executionContext.RequestDate;
                RequestMethod = executionContext.RequestMethod;
                RequestUrl = executionContext.RequestUrl;
                ChannelIds = executionContext.ChannelIds;
                ActiveChannelId = executionContext.ActiveChannelId;
                UserId = executionContext.UserId;
                UserName = executionContext.Username;
                ActiveChannelSystemId = executionContext.ActiveChannelSystemId;

            }
            AuditEntryType = auditEntryType;
            AuditEntryDescription = auditEntryDescription;
            EntityName = entityFullName;
            EntityId = entityId;
            FieldName = fieldName;
            OldValue = oldValue;
            NewValue = newValue;
            Timestamp = timestamp;
        }

        public CreateAuditLogDto(IExecutionContext executionContext, string auditEntryType, string auditEntryDescription, string entityFullName, int entityId, string username, string channelIds, int activeChannelId, Guid activeChannelSystemId, DateTime timestamp, string fieldName = "", string oldValue = "", string newValue = "")
        {
            if (executionContext != null)
            {
                UserAgent = executionContext.UserAgent;
                Referrer = executionContext.Referrer;
                RequestId = executionContext.RequestId;
                RequestType = executionContext.RequestType;
                RequestIp = executionContext.RequestIp;
                RequestDate = executionContext.RequestDate;
                RequestMethod = executionContext.RequestMethod;
                RequestUrl = executionContext.RequestUrl;
            }

            UserId = entityId;
            UserName = username;
            ChannelIds = channelIds;
            ActiveChannelId = activeChannelId;
            ActiveChannelSystemId = activeChannelSystemId;

            AuditEntryType = auditEntryType;
            AuditEntryDescription = auditEntryDescription;
            EntityName = entityFullName;
            EntityId = entityId;
            FieldName = fieldName;
            OldValue = oldValue;
            NewValue = newValue;
            Timestamp = timestamp;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}
