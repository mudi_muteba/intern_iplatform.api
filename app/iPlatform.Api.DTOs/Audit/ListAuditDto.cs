﻿using iPlatform.Api.DTOs.Base;
using System;

namespace iPlatform.Api.DTOs.Audit
{
    public class ListAuditDto :Resource
    {
        public ListAuditDto()
        {
        }
        public int UserId { get; set; }
        public string Event { get; set; }
        public string Detail { get; set; }
    }
}
