﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using iPlatform.Api.DTOs.Products;
using Newtonsoft.Json;

namespace iPlatform.Api.DTOs.OverrideRatingQuestionChannel
{
    public class OverrideRatingQuestionChannelDto : Resource, ICultureAware
    {
        [JsonIgnore]
        public OverrideRatingQuestionDto OverrideRatingQuestion { get; set; }

        public ChannelInfoDto Channel { get; set; }
    }
}
