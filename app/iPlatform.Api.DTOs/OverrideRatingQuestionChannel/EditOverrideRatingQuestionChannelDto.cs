﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using ValidationMessages;

namespace iPlatform.Api.DTOs.OverrideRatingQuestionChannel
{
    public class EditOverrideRatingQuestionChannelDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DtoContext Context { get; private set; }

        public int Id { get; set; }

        public int OverrideRatingQuestionId { get; set; }

        public int ChannelId { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        #region [ Validation ]

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            List<ValidationErrorMessage> errorMessages = new List<ValidationErrorMessage>();
            if (Id == 0)
                errorMessages.Add(OverrideRatingQuestionChannelValidationMessage.IdRequired);
            if (OverrideRatingQuestionId == 0)
                errorMessages.Add(OverrideRatingQuestionChannelValidationMessage.OverrideRatingQuestionIdRequired);
            if (ChannelId == 0)
                errorMessages.Add(OverrideRatingQuestionChannelValidationMessage.ChannelIdRequired);

            return errorMessages;
        }

        #endregion
    }
}
