﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.ItcQuestions;

namespace iPlatform.Api.DTOs.ItcQuestion
{
    public class RemoveItcQuestionDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
        public int Id { set; get; }
        public DtoContext Context { set; get; }

        public RemoveItcQuestionDto()
        {
            Context = DtoContext.NoContext();
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                list.Add(ItcQuestionValidationMessages.MissingId);
            }

            return list;
        }

    }
}
