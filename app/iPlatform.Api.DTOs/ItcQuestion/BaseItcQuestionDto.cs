﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using ValidationMessages;
using ValidationMessages.ItcQuestions;

namespace iPlatform.Api.DTOs.ItcQuestion
{
    public abstract class BaseItcQuestionDto : IValidationAvailableDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ChannelId { get; set; }
        public string QuestionText { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public bool Active { get; set; }

        protected BaseItcQuestionDto()
        {
            DateCreated = DateTime.UtcNow;
            Active = true;
        }
        
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();
            
            if (string.IsNullOrEmpty(QuestionText.Trim()))
            {
                list.Add(ItcQuestionValidationMessages.MissingQuestionText);
            }
            
            list.AddRange(InternalValidate());
            return list;
        }

        protected abstract List<ValidationErrorMessage> InternalValidate();
    }
}
