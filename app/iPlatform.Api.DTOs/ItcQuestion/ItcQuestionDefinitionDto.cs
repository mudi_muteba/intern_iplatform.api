﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.ItcQuestion
{
    public class ItcQuestionDefinitionDto: Resource
    {
        public int Id { get; set; }
        public int? ChannelId { get; set; }
        public string QuestionText { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public bool Active { get; set; }
        public string Name { get; set; }
    }
}
