﻿using System.Collections.Generic;
using ValidationMessages;
using ValidationMessages.ItcQuestions;

namespace iPlatform.Api.DTOs.ItcQuestion
{
    public class CreateItcQuestionDto : BaseItcQuestionDto
    {
        public CreateItcQuestionDto()
        {

        }
        
        protected override List<ValidationErrorMessage> InternalValidate()
        {
            var list = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(QuestionText))
            {
                list.Add(ItcQuestionValidationMessages.MissingQuestionText);
            }
            
            return list;
        }
    }
}
