﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using ValidationMessages;
using ValidationMessages.ItcQuestions;

namespace iPlatform.Api.DTOs.ItcQuestion
{
    public class UpdateItcQuestionDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DtoContext Context { get; private set; }
        public int Id { get; set; }
        public int? ChannelId { get; set; }
        public string QuestionText { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public bool Active { get; set; }
        public string Name { get; set; }

        public UpdateItcQuestionDto()
        {
            Context = DtoContext.NoContext();
            DateUpdated = DateTime.UtcNow;
        }
        
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id <= 0)
                validation.Add(ItcQuestionValidationMessages.MissingId);

            if (string.IsNullOrEmpty(QuestionText))
                validation.Add(ItcQuestionValidationMessages.MissingQuestionText);
            
            return validation;
        }
    }
}
