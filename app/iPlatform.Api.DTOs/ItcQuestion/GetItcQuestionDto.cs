﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.ItcQuestion
{
    public class GetItcQuestionDto: BaseCriteria
    {
        public int ChannelId { get; set; }
        public string ChannelName { get; set; }
    }
}
