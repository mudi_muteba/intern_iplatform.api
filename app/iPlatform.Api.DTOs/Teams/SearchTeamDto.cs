﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Teams;

namespace iPlatform.Api.DTOs.Teams
{
    public class SearchTeamDto : BaseCriteria, IValidationAvailableDto
    {
        public int? TeamId { get; set; }
        public string Name { get; set; }

        public SearchTeamDto()
        {
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            var valid = TeamId.HasValue || !string.IsNullOrEmpty(Name);

            if (valid)
                return validation;

            validation.Add(TeamValidationMessages.InvalidSearchCriteria);

            return validation;
        }
    }
}