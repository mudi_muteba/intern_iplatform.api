﻿using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Campaigns;

namespace iPlatform.Api.DTOs.Teams
{
    public class TeamCampaignDto: ICultureAware
    {
        public ListCampaignDto Campaign { get; set; }
    }
}
