﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Users;
using ValidationMessages;
using ValidationMessages.Teams;

namespace iPlatform.Api.DTOs.Teams
{
    public class EditTeamDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {

        public EditTeamDto()
        {
            Context = DtoContext.NoContext();
            Campaigns = new List<CampaignInfoDto>();
            Users = new List<UserInfoDto>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DtoContext Context { get; private set; }
        public List<CampaignInfoDto> Campaigns { get; set; }
        public List<UserInfoDto> Users { get; set; }

        public void SetContext(DtoContext context)
        {

        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                validation.Add(TeamValidationMessages.InvalidTeamId);
            }

            if (string.IsNullOrWhiteSpace(Name))
            {
                validation.Add(TeamValidationMessages.NameRequired);
            }

            return validation;
        }

    }
}
