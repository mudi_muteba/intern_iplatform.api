﻿using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Teams
{
    public class TeamUserDto: ICultureAware
    {
        public BasicUserDto User { get; set; }
    }
}
