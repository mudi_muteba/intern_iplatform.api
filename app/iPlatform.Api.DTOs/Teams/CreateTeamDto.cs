﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Users;
using ValidationMessages;
using ValidationMessages.Teams;

namespace iPlatform.Api.DTOs.Teams
{
    public class CreateTeamDto : AttributeValidationDto, IExecutionDto
    {
        #region Context
        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }
        #endregion
        public CreateTeamDto()
        {
            Context = DtoContext.NoContext();
            Campaigns = new List<CampaignInfoDto>();
            Users = new List<UserInfoDto>();
        }

        public string Name { get; set; }

        public List<CampaignInfoDto> Campaigns { get; set; }
        public List<UserInfoDto> Users { get; set; }
        
        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(Name))
            {
                validation.Add(TeamValidationMessages.NameRequired);
            }

            return validation;
        }
    }
}
