﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Teams;

namespace iPlatform.Api.DTOs.Teams
{
    public class DisableTeamDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {

        public DisableTeamDto()
        {
            Context = DtoContext.NoContext();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {

        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                validation.Add(TeamValidationMessages.InvalidTeamId);
            }

            return validation;
        }

    }
}
