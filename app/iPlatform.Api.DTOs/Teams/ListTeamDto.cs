﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Teams
{
    public class ListTeamDto : Resource, ICultureAware
    {
        public ListTeamDto()
        {
            Campaigns = new List<TeamCampaignDto>();
            Users = new List<TeamUserDto>();
        }

        public string Name { get; set; }

        public List<TeamCampaignDto> Campaigns { get; set; }
        public List<TeamUserDto> Users { get; set; }
    }
}
