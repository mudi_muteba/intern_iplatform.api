﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.FuneralMembers
{
    public class CreateFuneralMemberDto : AttributeValidationDto, IExecutionDto
    {
        public CreateFuneralMemberDto()
        {
            Context = DtoContext.NoContext();
        }
        public int MemberRelationshipId { get; set; }
        public string Initials { get; set; }
        public string Surname { get; set; }
        public string IdNumber { get; set; }
        public bool IsStudent { get; set; }
        public int GenderId { get; set; }
        public DateTime DateOnCover { get; set; }
        public string SumInsured { get; set; }
        public int ProposalDefinitionId { get; set; }
        public DtoContext Context { get; protected internal set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(Initials))
                validation.Add(FuneralMemberValidationMessages.InitialRequired);

            if (string.IsNullOrWhiteSpace(Surname))
                validation.Add(FuneralMemberValidationMessages.SurnameRequired);

            if (string.IsNullOrWhiteSpace(IdNumber))
                validation.Add(FuneralMemberValidationMessages.IdNumberRequired);

            if (string.IsNullOrWhiteSpace(SumInsured) || Convert.ToDouble(SumInsured) <= 0)
                validation.Add(FuneralMemberValidationMessages.InitialRequired);

            if (ProposalDefinitionId  <= 0)
                validation.Add(FuneralMemberValidationMessages.ProposalDefinitionIdRequired);

            if (MemberRelationshipId <= 0)
                validation.Add(FuneralMemberValidationMessages.MemberRelationshipIdRequired);

            return validation;
        }
    }
}
