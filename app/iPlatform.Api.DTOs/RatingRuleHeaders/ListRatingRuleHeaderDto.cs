﻿using iPlatform.Api.DTOs.Base;
using System;
namespace iPlatform.Api.DTOs.RatingRuleHeaders
{
    public class ListRatingRuleHeaderDto : Resource
    {
        public ListRatingRuleHeaderDto() { }
        public int ID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
