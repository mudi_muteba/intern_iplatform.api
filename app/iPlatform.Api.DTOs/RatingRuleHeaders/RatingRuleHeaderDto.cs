﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.RatingRuleHeaders
{
    public class RatingRuleHeaderDto : Resource
    {
        public RatingRuleHeaderDto()
        {
        }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
