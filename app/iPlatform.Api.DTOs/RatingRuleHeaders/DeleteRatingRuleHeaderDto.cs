﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using ValidationMessages;
using ValidationMessages.RatingRuleHeader;
using System;
namespace iPlatform.Api.DTOs.RatingRuleHeaders
{
    public class DeleteRatingRuleHeaderDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DeleteRatingRuleHeaderDto() { }

        public int Id { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            if (Id == 0)
                validation.Add(RatingRuleHeaderValidationMessages.IDRequired);

            return validation;
        }
    }
}
