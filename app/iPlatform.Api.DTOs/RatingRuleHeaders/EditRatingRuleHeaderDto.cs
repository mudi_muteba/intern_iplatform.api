﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using ValidationMessages;
using ValidationMessages.RatingRuleHeader;
using System;

namespace iPlatform.Api.DTOs.RatingRuleHeaders
{
   public class EditRatingRuleHeaderDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public EditRatingRuleHeaderDto() { }
        public int Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            if (StartDate == null)
                validation.Add(RatingRuleHeaderValidationMessages.StartDateRequired);

            if (EndDate == null)
                validation.Add(RatingRuleHeaderValidationMessages.EndDateRequired);

            return validation;
        }
    }
}
