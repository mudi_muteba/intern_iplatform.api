﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.QuoteBreakdown
{
    public class EditQuoteItemBreakDownDto :Resource, IExecutionDto, IAffectExistingEntity
    {
        public EditQuoteItemBreakDownDto()
        {
            Items = new List<EditBreakDownItemDto>();
        }


        public List<EditBreakDownItemDto> Items { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }

    public class EditBreakDownItemDto
    {
        public int Id { get; set; }
        public int QuoteItemId { get; set; }
        public string FinalPremium { get; set; }
        public string PercentageAdj { get; set; }
        public string AmountAdj { get; set; }
    }

}
