﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.ProposalDefinition;

namespace iPlatform.Api.DTOs.Party.ProposalHeaders
{
    public class ProposalHeaderDto : Resource, IAuditableDto, ICultureAware
    {
        public ProposalHeaderDto()
        {
            Events = new List<AuditEventDto>();
            ShouldReQuote = false;
            ValidateProposals = false;
            QuoteExpired = false;
        }

        public string ExternalReference { get; set; }
        public string Description { get; set; }
        public DateTimeDto CreatedDate { get; set; }
        public DateTimeDto ModifiedDate { get; set; }
        public DateTimeDto RatingDate { get; set; }
        public int PartyId { get; set; }
        public BasicIndividualDto Party { get; set; }
        public int AgentId { get; set; }
        public string AgentName { get; set; }
        public List<AuditEventDto> Events { get; private set; }
        public bool IsIntentToBuy { get; set; }

        public bool IsQuoted { get; set; }
        public bool IsQuotable { get; set; }
        //AIG Crappy rules
        public bool ShouldReQuote { get; set; }
        public bool ValidateProposals { get; set; }
        public bool QuoteExpired { get; set; }
        public int QuoteExpirationDays { get; set; }

        //AIG Crappy rules
    }
}