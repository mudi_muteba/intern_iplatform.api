﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.ProposalHeaders
{
    public class EditProposalHeaderShouldQuoteDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public EditProposalHeaderShouldQuoteDto()
        {
            Events = new List<AuditEventDto>();
        }

        public List<AuditEventDto> Events { get; private set; }

        public bool ShouldReQuote { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}
