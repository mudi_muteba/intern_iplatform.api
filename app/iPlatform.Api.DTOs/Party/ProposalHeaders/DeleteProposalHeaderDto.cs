﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.ProposalHeaders
{
    public class DeleteProposalHeaderDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public DeleteProposalHeaderDto()
        {
            Events = new List<AuditEventDto>();
        }
        public string Description { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual DateTime? ModifiedDate { get; set; }
        public virtual DateTime? RatingDate { get; set; }
        public int PartyId { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}