﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.ProposalHeaders
{
    public class EditProposalHeaderValidateProposalDto : Resource, IExecutionDto, IAffectExistingEntity
    {

        public EditProposalHeaderValidateProposalDto()
        {
            Events = new List<AuditEventDto>();
        }
        
        public bool ValidateProposals { get; set; }

        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}
