﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.ProposalHeaders
{
    public class CreateProposalHeaderDto : Resource, IExecutionDto
    {
        public CreateProposalHeaderDto()
        {
            Events = new List<AuditEventDto>();
        }

        public string Description { get; set; }

        public int CampaignId { get; set; }
        public string CampaignReference { get; set; }
        public string CmpidSource { get; set; }
        public string Source { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? RatingDate { get; set; }
        public int PartyId { get; set; }
        public string ExternalReference { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}