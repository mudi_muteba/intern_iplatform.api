﻿using System.Linq;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.ProposalDefinition;

namespace iPlatform.Api.DTOs.Party.ProposalHeaders
{
    public class ProposalHeaderDetailedDto : Resource, IAuditableDto, ICultureAware
    {
        public ProposalHeaderDetailedDto()
        {
            Events = new List<AuditEventDto>();
            ProposalDefinitions = new List<ProposalDefinitionDto>();
        }

        public string ExternalReference { get; set; }
        public string Description { get; set; }
        public DateTimeDto CreatedDate { get; set; }
        public DateTimeDto ModifiedDate { get; set; }
        public DateTimeDto RatingDate { get; set; }
        public int PartyId { get; set; }
        public BasicIndividualDto  Party { get; set; }
        public int AgentId { get; set; }
        public string AgentName { get; set; }
        public List<AuditEventDto> Events { get; private set; }
        public List<ProposalDefinitionDto> ProposalDefinitions { get; set; }
        public bool IsQuoted { get; set; }

        public bool IsQuotable
        {
            get { return this.ProposalDefinitions.Any(x => x.Asset != null && x.Asset.Id > 0); }
        }
    }
}