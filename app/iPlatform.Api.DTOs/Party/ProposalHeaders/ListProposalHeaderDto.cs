﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Individual;

namespace iPlatform.Api.DTOs.Party.ProposalHeaders
{
    public class ListProposalHeaderDto : Resource, IAuditableDto, ICultureAware
    {
        public ListProposalHeaderDto()
        {
            Events = new List<AuditEventDto>();
        }

        public string Description { get; set; }
        public DateTimeDto CreatedDate { get; set; }
        public DateTimeDto ModifiedDate { get; set; }
        public DateTimeDto RatingDate { get; set; }
        public int PartyId { get; set; }
        public BasicIndividualDto Party { get; set; }
        public int AgentId { get; set; }
        public string AgentName { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public bool IsQuoted { get; set; }
        public bool IsQuotable { get; set; }

    }
}