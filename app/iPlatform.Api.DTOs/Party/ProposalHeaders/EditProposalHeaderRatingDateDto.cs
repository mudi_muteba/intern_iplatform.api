﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.ProposalHeaders
{
    public class EditProposalHeaderRatingDateDto : Resource, IExecutionDto, IAffectExistingEntity
    {

        public EditProposalHeaderRatingDateDto()
        {
            Events = new List<AuditEventDto>();
        }
        public EditProposalHeaderRatingDateDto(int proposalHeaderId, DateTime newRatingDate)
        {
            Events = new List<AuditEventDto>();
            Id = proposalHeaderId;
            RatingDate = newRatingDate;
        }
        
        public DateTime RatingDate { get; set; }

        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}
