﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.ProposalHeaders
{
    public class EditProposalHeaderDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public EditProposalHeaderDto()
        {
            Events = new List<AuditEventDto>();
        }
        
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? RatingDate { get; set; }

        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}