﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey
{
    public class CreateCustomerSatisfactionSurveyDto : AttributeValidationDto, IExecutionDto
    {
        public CreateCustomerSatisfactionSurveyDto()
        {
            Answers = new List<CustomerSatisfactionSurveyAnswersDto>();
        }
        public int PartyId { get; set; }
        public string BrokerCode { get; set; }
        public string Reference { get; set; }
        public string PolicyNumber { get; set; }
        public string Event { get; set; }
        public string Name { get; set; }
        public string CellNumber { get; set; }
        public string EmailAddress { get; set; }

        public List<CustomerSatisfactionSurveyAnswersDto> Answers { get; set; }
        public DtoContext Context { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrEmpty(Name))
            {
                validation.Add(CustomerSatisfactionSurveyValidationMessages.NameEmpty);
            }

            if (string.IsNullOrEmpty(CellNumber))
            {
                validation.Add(CustomerSatisfactionSurveyValidationMessages.CellNumberEmpty);
            }

            if (string.IsNullOrEmpty(EmailAddress))
            {
                validation.Add(CustomerSatisfactionSurveyValidationMessages.EmailAddressEmpty);
            }

            if (!Answers.Any())
            {
                validation.Add(CustomerSatisfactionSurveyValidationMessages.AnswersEmpty);
            }

            return validation;
        }
    }
}
