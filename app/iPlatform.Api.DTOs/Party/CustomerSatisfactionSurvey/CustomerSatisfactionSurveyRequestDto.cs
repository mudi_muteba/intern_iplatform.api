﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.QuoteAcceptance;
using System;

namespace iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey
{
    public class CustomerSatisfactionSurveyRequestDto : IExecutionDto
    {
        public CustomerSatisfactionSurveyRequestDto()
        {
            VendorSystem = "iPlatform";
            Context = DtoContext.NoContext();
        }

        public DateTime ExportedDateTime { get; set; }
        public string VendorSystem { get; protected set; }
        public string SystemUser { get; set; }
        public string GeneralNotes { get; set; }
        public int TriggerEvent { get; set; }
        public int EntityType { get; set; }
        public string EntityCode { get; set; }
        public string EntityName { get; set; }
        public string PolicyHolder { get; set; }
        public int ClientType { get; set; }
        public string IDCoRegNo { get; set; }
        public string PolicyClaimNumber { get; set; }
        public int IsPolicy { get; set; }
        public int IsClaim { get; set; }
        public string PolicyHolderCell { get; set; }
        public string PolicyHolderEmail { get; set; }
        public string InsurerName { get; set; }
        public int InsuranceType { get; set; }
        public DateTime EffectiveDate { get; set; }
        public EchoTCFDto EchoTCF { get; set; }

        public DtoContext Context { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}