﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey
{
    public class CustomerSatisfactionSurveyDto : Resource
    {
        public CustomerSatisfactionSurveyDto()
        {
            Answers = new List<CustomerSatisfactionSurveyAnswersDto>();
        }
        public int PartyId { get; set; }
        public string Event { get; set; }
        public DateTimeDto CreatedOn { get; set; }

        public List<CustomerSatisfactionSurveyAnswersDto> Answers { get; set; }

    }
}
