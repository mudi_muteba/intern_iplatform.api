﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey
{
    public class CustomerSatisfactionSurveyAnswersDto
    {
        public int Id { get; set; }
        public int CustomerSatisfactionSurveyId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        
    }
}
