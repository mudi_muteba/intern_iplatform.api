﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition
{
    public class CreateProposalDefinitionDto : Resource, IExecutionDto, ICloneable
    {
        public CreateProposalDefinitionDto()
        {
            Events = new List<AuditEventDto>();
        }

        public int DefaultFromProposalId { get; set; }
        public int ProposalHeaderId { get; set; }
        public int CoverId { get; set; }
        public string Description { get; set; }
        public int ProductId { get; set; }
        public int ChannelId { get; set; }

        public List<AuditEventDto> Events { get; private set; }


        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}