﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition
{
    public class DeleteProposalDefinitionDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public DeleteProposalDefinitionDto()
        {
            Events = new List<AuditEventDto>();
        }
        public int ProposalHeaderId { get; set; }

        //public int PartyId { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}