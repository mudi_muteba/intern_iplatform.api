﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.Assets;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition
{
  public class ContainsVehicleTypesDto
    {
      public ContainsVehicleTypesDto()
      {
          VehicleTypes = new List<string>();
      }

        public int IndividualId { get; set; }
        public int ProposalId { get; set; }
        public List<string> VehicleTypes { get; set; }
    }
}