﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition
{
    public class EditProposalDefinitionDto : Resource, IExecutionDto,IAffectExistingEntity
    {
        public EditProposalDefinitionDto()
        {
            Events = new List<AuditEventDto>();
            Answers =  new List<ProposalDefinitionAnswerDto>();
            AssetExtras = new List<AssetExtrasDto>();
        }

        public int ProposalHeaderId { get; set; }

        public string Description { get; set; }

        public List<ProposalDefinitionAnswerDto> Answers { get; set; }
        public List<AssetExtrasDto> AssetExtras { get; set; }

        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}