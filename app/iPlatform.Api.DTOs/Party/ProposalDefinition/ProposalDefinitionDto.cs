﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.Assets;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition
{
    public class ProposalDefinitionDto : Resource, IAuditableDto, ICultureAware
    {
        public ProposalDefinitionDto()
        {
            Events = new List<AuditEventDto>();
            QuestionGroups = new List<QuestionGroupDto>();
        }

        public int ProposalHeaderId { get; set; }
        public int MainDriverPartyId { get; set; }
        public Cover Cover { get; set; }
        public AssetDto Asset { get; set; }
        public int ProductId { get; set; }
        public int PartyId { get; set; }
        public string PartyDisplayName { get; set; }
        public string CoverLevelWording { get; set; }        
        public int LeadId { get; set; }
        public bool? IsVap { get; set; }        
        public decimal SumInsured { get; set; }
        public DateTimeDto Created { get; set; }
        public DateTimeDto Modified { get; set; }
        public bool IsDeleted { get; set; }
        public string Description { get; set; }
        public bool IsHeaderQuoted { get; set; }
        public virtual int TotalQuestions { get; set; }

        public virtual int TotalQuestionsAnswered { get; set; }

        public List<QuestionGroupDto> QuestionGroups { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public ProductInfoDto Product { get; set; }
    }
}