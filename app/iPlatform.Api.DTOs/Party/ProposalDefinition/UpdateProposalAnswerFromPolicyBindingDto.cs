﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition
{
    public class UpdateProposalAnswerFromPolicyBindingDto : AttributeValidationDto, IExecutionDto
    {
        public UpdateProposalAnswerFromPolicyBindingDto()
        {
            Context = DtoContext.NoContext();
        }
        public int QuoteItemId { get; set; }
        public int PolicyBindingQuestionDefinitionId { get; set; }
        public int QuestionDefinitionId { get; set; }
        public string Answer { get; set; }
        public int ProposalDefinitionId { get; set; }
        public int ProposalHeaderId { get; set; }
        
        public DtoContext Context { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
