﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.CoverDefinitions;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.Assets;
using MasterData;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition
{
    public class ListProposalDefinitionDto : Resource, IAuditableDto, ICultureAware
    {
        public ListProposalDefinitionDto()
        {
            Events = new List<AuditEventDto>();
        }

        public int ProposalHeaderId { get; set; }
        public Cover Cover { get; set; }
        public CoverDefinitionViewDto CoverDefinition { get; set; }
        public decimal SumInsured { get; set; }
        public bool? IsVap { get; set; }
        public DateTimeDto Created { get; set; }
        public DateTimeDto Modified { get; set; }
        public bool IsDeleted { get; set; }
        public string Description { get; set; }
        public int PartyId { get; set; }
        public BasicIndividualDto Party { get; set; }
        public int LeadId { get; set; }
        public AssetDto Asset { get; set; }
        public List<AuditEventDto> Events { get; private set; }
    }
}