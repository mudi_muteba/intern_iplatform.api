﻿namespace iPlatform.Api.DTOs.Party.ProposalDefinition
{
    public class DeleteProposalDefinitionResponseDto
    {
        public int PartyId { get; set; }
        public int ProposalHeaderId { get; set; }
    }
}