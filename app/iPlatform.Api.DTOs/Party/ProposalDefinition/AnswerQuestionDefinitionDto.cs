﻿namespace iPlatform.Api.DTOs.Party.ProposalDefinition
{
    public class AnswerQuestionDefinitionDto
    {
        public int QuestionId { get; set; }
        public object Answer { get; set; }
    }
}