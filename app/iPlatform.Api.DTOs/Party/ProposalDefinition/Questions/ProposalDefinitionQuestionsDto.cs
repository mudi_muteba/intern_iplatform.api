﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition.Questions
{
    public class ProposalDefinitionQuestionsDto:Resource
    {
        public List<ProposalQuestionDefinitionDto> QuestionDefinitions { get; set; }
        public ProposalDefinitionDto ProposalDefinition { get; set; }

        public ProposalDefinitionQuestionsDto()
        {
            QuestionDefinitions = new List<ProposalQuestionDefinitionDto>();
            ProposalDefinition = new ProposalDefinitionDto();
        }
    }
}
