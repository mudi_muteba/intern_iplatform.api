﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition.Questions
{
    public class SaveProposalDefinitionAnswersDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public SaveProposalDefinitionAnswersDto()
        {
            Answers = new List<ProposalDefinitionAnswerDto>();
            Context = DtoContext.NoContext();
        }
        public int ProposalDefinitionId { get; set; }
        public int PartyId { get; set; }

        public List<ProposalDefinitionAnswerDto> Answers { get; set; }
        public List<AssetExtrasDto> AssetExtras { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}
