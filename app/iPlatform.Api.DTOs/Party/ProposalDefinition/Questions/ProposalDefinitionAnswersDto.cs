﻿using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition.Questions
{
    public class ProposalDefinitionAnswersDto
    {
        public ProposalDefinitionAnswersDto()
        {
            Answers = new List<ProposalDefinitionAnswerDto>();
        }
        public int ProposalDefinitionId { get; set; }
        public int PartyId { get; set; }

        public List<ProposalDefinitionAnswerDto> Answers { get; set; }
        public List<AssetExtrasDto> AssetExtras { get; set; }
    }
}
