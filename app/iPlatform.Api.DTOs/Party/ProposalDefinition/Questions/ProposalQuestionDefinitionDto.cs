﻿using System.Collections.Generic;
using MasterData;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition.Questions
{
    public class ProposalQuestionDefinitionDto 
    {
        public ProposalQuestionDefinitionDto()
        {
            Options = new List<QuestionAnswerDto>();
        }

        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string DisplayNameTranslationKey { get; set; }
        public string QuestionGroupTranslationKey { get; set; }
        public Question Question { get; set; }
        public List<QuestionAnswerDto> Options { get; set; }
        public int ParentId { get; set; }
        public string ParentQuestionName { get; set; }
        public QuestionDefinitionType QuestionDefinitionType { get; set; }
        public QuestionDefinitionGroupType GroupType { get; set; }
        public int VisibleIndex { get; set; }
        public int MasterId { get; set; }
        public bool RequiredForQuote { get; set; }
        public bool RatingFactor { get; set; }
        public bool ReadOnly { get; set; }
        public string ToolTip { get; set; }
        public string TooltipTranslationKey { get; set; }
        public string DefaultValue { get; set; }
        public string RegexPattern { get; set; }
        public int GroupIndex { get; set; }
        public string LogoName { get; set; }
        public bool Hide { get; set; }
        public virtual QuestionGroup QuestionGroup { get; set; }
        public virtual int ProductId { get; set; }



        public ProposalDefinitionAnswerDto AnswerDto { get; set; }
    }

}
