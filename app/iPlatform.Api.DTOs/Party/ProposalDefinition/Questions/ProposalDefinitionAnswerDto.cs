﻿using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition.Questions
{
    public class ProposalDefinitionAnswerDto: Resource
    {
        public string Answer { get; set; }

        public int ProposalDefinitionId { get; set; }

        public QuestionType QuestionType { get; set; }
    }
}