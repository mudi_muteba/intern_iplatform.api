﻿﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition.Questions
{
    public class ProposalDefinitionQuestionsCriteria : BaseCriteria
    {

        public int? GroupType { get; set; }
        public int? GroupIndex { get; set; }


        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
