﻿namespace iPlatform.Api.DTOs.Party.ProposalDefinition.Questions
{
    public class QuestionAnswerDto
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Answer { get; set; }

        public int VisibleIndex { get; set; }
    }
}
