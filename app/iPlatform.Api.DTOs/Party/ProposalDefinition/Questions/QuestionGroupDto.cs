﻿using System.Collections.Generic;
using MasterData;

namespace iPlatform.Api.DTOs.Party.ProposalDefinition.Questions
{
    public class QuestionGroupDto : QuestionGroup
    {

        public List<ProposalQuestionDefinitionDto> QuestionDefinitions { get; set; }
    }
}
