﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.Party.LossHistoryQuestionDefinitions
{
    public class LossHistoryQuestionDefinitionDto : Resource, IAuditableDto, ICultureAware
    {
        public LossHistoryQuestionDefinitionDto()
        {
            Events = new List<AuditEventDto>();
        }

        public string DisplayName { get; set; }
        public int LossHistoryQuestionDefinitionId { get; set; }
        public string ClientCode { get; set; }
        public int VisibleIndex { get; set; }
        public bool RequierdForQuote { get; set; }
        public bool RatingFactor { get; set; }
        public bool IsReadOnly { get; set; }
        public string ToolTip { get; set; }
        public string DefaultValue { get; set; }
        public string RegexPattern { get; set; }
        public int LossHistoryQuestionGroupId { get; set; }
        public int QuestionTypeId { get; set; }
        public List<AuditEventDto> Events { get; set; }
    }
}
