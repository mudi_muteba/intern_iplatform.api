﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Party.LossHistoryQuestionDefinitions
{
    public class CreateLossHistoryQuestionDefinitionDto : AttributeValidationDto, IChannelAwareDto, IExecutionDto
    {
        public int ChannelId { get; set; }
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public CreateLossHistoryQuestionDefinitionDto()
        {
            Events = new List<AuditEventDto>();
        }
        public List<AuditEventDto> Events { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
