﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.LossHistoryQuestionDefinition;

namespace iPlatform.Api.DTOs.Party.LossHistoryQuestionDefinitions
{
    public class LossHistoryQuestionDefinitionSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public string ClientCode { get; set; }
        public int QuestionTypeId { get; set; }

        public LossHistoryQuestionDefinitionSearchDto()
        {

        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrEmpty(ClientCode))
            {
                validation.Add(LossHistoryDefinitionValidationMessages.ClientCodeRequired);
            }

            return validation;

        }
    }
}
