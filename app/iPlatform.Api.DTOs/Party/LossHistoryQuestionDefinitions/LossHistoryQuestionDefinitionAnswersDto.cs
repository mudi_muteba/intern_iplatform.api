﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.Party.LossHistoryQuestionDefinitions
{
    public class LossHistoryQuestionDefinitionAnswersDto : Resource, IAuditableDto, ICultureAware
    {
        public LossHistoryQuestionDefinitionAnswersDto()
        {
            Events = new List<AuditEventDto>();
        }

        public LossHistoryQuestionDefinitionDto DefinitionDto { get; set; }
        public List<LossHistoryQuestionAnswer> Answers { get; set; }

        public List<AuditEventDto> Events { get; set; }
    }
}
