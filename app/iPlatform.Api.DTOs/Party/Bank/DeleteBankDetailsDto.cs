﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Bank
{
    public class DeleteBankDetailsDto : Resource, IExecutionDto
    {
        public DeleteBankDetailsDto()
        {
            Events = new List<AuditEventDto>();
        }

        public string BankAccHolder { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string BankBranchCode { get; set; }
        public string AccountNo { get; set; }
        public string TypeAccount { get; set; }
        public int PartyId { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}