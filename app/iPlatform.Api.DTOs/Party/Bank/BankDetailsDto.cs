﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Bank
{
    public class BankDetailsDto : Resource, IAuditableDto, ICultureAware
    {
        public BankDetailsDto()
        {
            Events = new List<AuditEventDto>();
        }

        public int PartyId { get; set; }

        public string BankAccHolder { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string BankBranchCode { get; set; }
        public string AccountNo { get; set; }
        public string TypeAccount { get; set; }
        public List<AuditEventDto> Events { get; protected set; }
    }
}