﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Bank
{
    public class CreateBankDetailsDto : Resource, IExecutionDto
    {
        public CreateBankDetailsDto()
        {
            Events = new List<AuditEventDto>();
        }

        public string BankAccHolder { get; set; }
        [Required(ErrorMessage = "BANK_EMPTY")]
        public string Bank { get; set; }
        [Required(ErrorMessage = "BANK_BRANCH_EMPTY")]
        public string BankBranch { get; set; }
        [Required(ErrorMessage = "BANK_BRANCH_CODE_EMPTY")]
        public string BankBranchCode { get; set; }
        [Required(ErrorMessage = "ACCOUNT_NO_EMPTY")]
        public string AccountNo { get; set; }
        [Required(ErrorMessage = "TYPE_ACCOUNT_EMPTY")]
        public string TypeAccount { get; set; }
        public int PartyId { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}