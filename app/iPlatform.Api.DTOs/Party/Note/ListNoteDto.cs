﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Note
{
    public class ListNoteDto : Resource, IAuditableDto, ICultureAware
    {
        public ListNoteDto()
        {
            Events = new List<AuditEventDto>();
        }

        public DateTimeDto DateCreated { get; set; } 
        public string Notes { get; set; }
        public bool Confidential { get; set; }
        public bool Warning { get; set; }
        public int PartyId { get; set; }
        public List<AuditEventDto> Events { get; private set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
    }
}