﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Note
{
    public class DeleteNoteDto : Resource, IExecutionDto
    {
        public DeleteNoteDto()
        {
            Events = new List<AuditEventDto>();
        }

        public int PartyId { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}