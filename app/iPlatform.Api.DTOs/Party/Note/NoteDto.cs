﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Note
{
    public class NoteDto : Resource, IAuditableDto, ICultureAware
    {
        public NoteDto()
        {
            Events = new List<AuditEventDto>();
        }

        public DateTimeDto DateCreated { get; set; }
        public string Notes { get; set; }
        public bool Confidential { get; set; }
        public bool Warning { get; set; }
        public int PartyId { get; set; }
        public List<AuditEventDto> Events { get; private set; }
    }
}