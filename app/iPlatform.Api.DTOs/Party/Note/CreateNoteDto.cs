﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Note
{
    public class CreateNoteDto : Resource, IExecutionDto
    {
        public CreateNoteDto()
        {
            Events = new List<AuditEventDto>();
        }

        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage = "NOTES_EMPTY")]
        public string Notes { get; set; }
        public bool Confidential { get; set; }
        public bool Warning { get; set; }
        public int PartyId { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}