﻿namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteItemStateEntryDto
    {
        public string Message { get; set; }
        public bool IsError { get; set; }
    }
}