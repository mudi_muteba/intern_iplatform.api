﻿namespace iPlatform.Api.DTOs.Party.Quotes
{
    public enum DistributeQuoteVia
    {
        Unknown = 0,
        SMS = 1,
        Email = 2
    }
}