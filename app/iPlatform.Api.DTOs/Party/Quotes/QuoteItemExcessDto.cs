﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteItemExcessDto: ICultureAware
    {
        public bool IsCalculated { get; set; }
        public MoneyDto Basic { get; set; }
        public string Description { get; set; }
        public MoneyDto VoluntaryExcess { get; set; }
    }
}