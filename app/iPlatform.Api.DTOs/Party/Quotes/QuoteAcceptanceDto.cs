﻿using System;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteAcceptedDto : ICultureAware
    {
        public DateTimeDto AcceptedOn { get; set; }
        public bool IsAccepted { get; set; }
        public int AcceptedBy { get; set; }
    }
}