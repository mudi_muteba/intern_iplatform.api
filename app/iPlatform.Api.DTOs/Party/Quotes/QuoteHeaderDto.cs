﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteHeaderDto: ICultureAware
    {
        public QuoteHeaderDto()
        {
            Quotes = new List<QuoteDto>();
            State = new QuoteHeaderStateDto();
            Acceptance = new QuoteAcceptedDto();
        }

        public int Id { get; set; }
        public List<QuoteDto> Quotes { get; set; }
        public QuoteHeaderStateDto State { get; set; }
        public DateTimeDto CreatedAt { get; set; }
        public DateTimeDto ModifiedAt { get; set; }
        public int ProposalId { get; set; }
        public Guid RequestId { get; set; }
        public QuoteAcceptedDto Acceptance { get; set; }
    }
}