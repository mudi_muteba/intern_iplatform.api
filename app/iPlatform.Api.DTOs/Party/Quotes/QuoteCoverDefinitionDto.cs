﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Products;
using MasterData;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteCoverDefinitionDto
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string CoverLevelWording { get; set; }
        public Cover Cover { get; set; }
        public int VisibleIndex { get; set; }
        public CoverDefinitionType CoverDefinitionType { get; set; }
        public List<ProductBenefitDto> Benefits { get; set; }
    }
}