﻿namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteStateEntryDto
    {
        public string Message { get; set; }
        public bool IsError { get; set; }
    }
}