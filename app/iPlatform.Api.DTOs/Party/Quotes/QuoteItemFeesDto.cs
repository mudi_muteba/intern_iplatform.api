﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteItemFeesDto:ICultureAware
    {
        public MoneyDto OriginalPremium { get; set; }
        public MoneyDto Premium { get; set; }
        public MoneyDto Sasria { get; set; }
    }
}