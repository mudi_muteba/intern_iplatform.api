﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Ratings;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class TrackQuoteDistributionDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
        public int QuoteId { get; set; }
        public DistributeQuoteVia Via { get; set; }
        public string ProductCode { get; set; }
        public string Provider { get; set; }
        public string ProviderReference { get; set; }

        public int Id
        {
            get { return QuoteId; }
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (QuoteId == 0)
            {
                validation.Add(QuoteDistributionTrackingValidationMessages.InvalidQuote);
            }

            if (Via == DistributeQuoteVia.Unknown)
            {
                validation.Add(QuoteDistributionTrackingValidationMessages.InvalidDistributionMethod);
            }

            if (string.IsNullOrWhiteSpace(ProductCode))
            {
                validation.Add(QuoteDistributionTrackingValidationMessages.NoProductProvided);
            }

            if (string.IsNullOrWhiteSpace(Provider))
            {
                validation.Add(QuoteDistributionTrackingValidationMessages.NoProvider);
            }

            return validation;
        }
    }
}