﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteItemStateDto
    {
        public QuoteItemStateDto()
        {
            Entries = new List<QuoteItemStateEntryDto>();
        }

        public bool Warnings { get; set; }

        public bool Errors { get; set; }

        public IList<QuoteItemStateEntryDto> Entries { get; set; }
    }
}