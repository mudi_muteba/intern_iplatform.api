﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Ratings;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class DistributeQuoteDto : IExecutionDto, IValidationAvailableDto
    {
        public DistributeQuoteDto()
        {
            ProductCodes = new List<string>();
        }

        public string ExternalReference { get; set; }
        public DistributeQuoteVia Via { get; set; }
        public List<string> ProductCodes { get; set; }
        public string Contact { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(ExternalReference))
            {
                validation.Add(QuoteDistributionValidationMessages.InvalidExternalReference);
            }

            if (Via == DistributeQuoteVia.Unknown)
            {
                validation.Add(QuoteDistributionValidationMessages.InvalidDistributionMethod);
            }

            if (ProductCodes == null)
            {
                validation.Add(QuoteDistributionValidationMessages.NoProductsSelectedForDistribution);
            }

            if (ProductCodes != null)
            {
                if (!ProductCodes.Any())
                {
                    validation.Add(QuoteDistributionValidationMessages.NoProductsSelectedForDistribution);
                }
            }

            return validation;
        }
    }
}