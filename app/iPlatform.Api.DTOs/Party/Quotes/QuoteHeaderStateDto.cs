﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteHeaderStateDto
    {
        public QuoteHeaderStateDto()
        {
            Entries = new List<QuoteHeaderStateEntryDto>();
        }

        public bool Warnings { get; set; }

        public bool Errors { get; set; }

        public List<QuoteHeaderStateEntryDto> Entries { get; set; }
    }
}