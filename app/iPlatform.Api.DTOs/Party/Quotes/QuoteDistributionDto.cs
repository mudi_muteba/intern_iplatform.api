﻿using System;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteDistributionDto: ICultureAware
    {
        public string DistributionMethod { get; set; }
        public string ProviderName { get; set; }
        public string ProviderReference { get; set; }
        public DateTimeDto DateTracked { get; set; }
    }
}