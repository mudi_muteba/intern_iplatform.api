namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteHeaderStateEntryDto
    {
        public string Message { get; set; }
        public bool IsError { get; set; }
    }
}