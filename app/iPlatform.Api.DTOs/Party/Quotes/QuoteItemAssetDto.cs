﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteItemAssetDto: ICultureAware
    {
        public int AssetNumber { get; set; }
        public MoneyDto SumInsured { get; set; }
    }
}