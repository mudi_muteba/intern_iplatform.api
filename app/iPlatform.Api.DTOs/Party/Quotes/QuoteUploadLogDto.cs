﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteUploadLogDto : ICultureAware
    {
        public QuoteUploadLogDto()
        {
            IsAvailable = false;
        }

        public virtual string InsurerReference { get; set; }
        public virtual MoneyDto Premium { get; set; }
        public virtual MoneyDto Fees { get; set; }
        public virtual MoneyDto Sasria { get; set; }
        public virtual DateTimeDto DateCreated { get; set; }
        public virtual string Message { get; set; }
        public virtual bool IsAvailable { get; set; }
    }
}
