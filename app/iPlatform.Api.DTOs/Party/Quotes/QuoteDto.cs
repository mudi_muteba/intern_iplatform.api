﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteDto: ICultureAware
    {
        public QuoteDto()
        {
            State = new QuoteStateDto();
            Product = new AboutProductDto();
            Items = new List<QuoteItemDto>();
        }


        public int Id { get; set; }
        public DateTimeDto CreatedAt { get; set; }
        public DateTimeDto ModifiedAt { get; set; }
        public AboutProductDto Product { get; set; }
        public QuoteStateDto State { get; set; }
        public MoneyDto Fees { get; set; }
        public bool IsAccepted { get; set; }
        public DateTimeDto AcceptedOn { get; set; }
        public List<QuoteItemDto> Items { get; set; }
        public List<QuoteDistributionDto> QuoteDistribution { get; set; }
        public string InsurerReference { get; set; }
        public string ITCScore { get; set; }
        public string ExtraITCScore { get; set; }
        public bool IsUploaded { get; set; }
    }
}