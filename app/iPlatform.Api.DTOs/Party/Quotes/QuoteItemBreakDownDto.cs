﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteItemBreakDownDto : ICultureAware
    {
        public QuoteItemBreakDownDto() { }

        public int Id { get; set; }
        public int QuoteItemId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Relationship { get; set; }
        public string Reference { get; set; }
        public MoneyDto Risk { get; set; }
        public MoneyDto Markup { get; set; }
        public string QuestionAnswer { get; set; }
        public MoneyDto Total { get; set; }
        public bool IsChild { get; set; }
        public bool IsMainMember { get; set; }
        public string ExternalId { get; set; }

        public string FinalPremium { get; set; }
        public string PercentageAdj { get; set; }
        public string AmountAdj { get; set; }
    }
}