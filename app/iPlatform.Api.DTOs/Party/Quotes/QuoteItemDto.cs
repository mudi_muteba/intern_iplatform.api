﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteItemDto: ICultureAware
    {
        public QuoteItemDto()
        {
            QuoteItemState = new QuoteItemStateDto();
            CoverDefinition = new QuoteCoverDefinitionDto();
            Fees = new QuoteItemFeesDto();
            Excess = new QuoteItemExcessDto();
            Asset = new QuoteItemAssetDto();
            Clauses = new List<string>();
        }

        public QuoteItemStateDto QuoteItemState { get; set; }
        public QuoteCoverDefinitionDto CoverDefinition { get; set; }
        public QuoteItemFeesDto Fees { get; set; }
        public QuoteItemExcessDto Excess { get; set; }
        public QuoteItemAssetDto Asset { get; set; }
        public string Description { get; set; }
        public string SumInsuredDescription { get; set; }
        public bool IsVap { get; set; }
        public int Id { get; set; }
        public List<string> Clauses { get; set; }
    }
}