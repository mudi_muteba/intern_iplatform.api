﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Party.Quotes
{
    public class QuoteStateDto
    {
        public QuoteStateDto()
        {
            Entries = new List<QuoteStateEntryDto>();
        }

        public List<QuoteStateEntryDto> Entries { get; set; }

        public bool Warnings { get; set; }

        public bool Errors { get; set; }
    }
}