﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Party.Address
{
    public class EditAddressDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public EditAddressDto()
        {
            Events = new List<AuditEventDto>();
        }

        [Required(ErrorMessage = "ADDRESS_LINE1_EMPTY")]
        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string Line3 { get; set; }

        public string Line4 { get; set; }

        public bool IsDefault { get; set; }

        public bool IsComplex { get; set; }

        public string Complex { get; set; }

        public string Description { get; set; }

        public AddressType AddressType { get; set; }
        [Required(ErrorMessage = "ADDRESS_CODE_EMPTY")]
        public string Code { get; set; }
        public bool DefaultAddress { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public StateProvince StateProvince { get; set; }
        public Country Country { get; set; }
        public int PartyId { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
        public DtoContext Context { get; private set; }
    }
}