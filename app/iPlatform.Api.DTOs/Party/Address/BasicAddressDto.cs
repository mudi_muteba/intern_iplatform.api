﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Party.Address
{
    public class BasicAddressDto
    {
        public BasicAddressDto()
        {
        }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public string Line4 { get; set; }
        public bool IsDefault { get; set; }
        public string AddressType { get; set; }
        public string Code { get; set; }
        public bool DefaultAddress { get; set; }
        public string StateProvince { get; set; }
        public int PartyId { get; set; }
    }
}
