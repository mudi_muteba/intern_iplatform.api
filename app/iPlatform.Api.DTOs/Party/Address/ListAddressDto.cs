﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Address
{
    public class ListAddressDto : Resource, IAuditableDto, ICultureAware
    {
        public ListAddressDto()
        {
            Events = new List<AuditEventDto>();
        }

        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public string Line4 { get; set; }
        public bool IsDefault { get; set; }
        public bool IsComplex { get; set; }
        public string Complex { get; set; }
        public string Description { get; set; }
        public AddressType AddressType { get; set; }
        public string Code { get; set; }
        public bool DefaultAddress { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTimeDto DateFrom { get; set; }
        public DateTimeDto DateTo { get; set; }
        public StateProvince StateProvince { get; set; }
        public int PartyId { get; set; }
        public Country Country { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        #region [ Additional Fields ]

        public string Locality { get; set; }

        public string Town { get; set; }

        public string County { get; set; }

        #endregion
    }
}