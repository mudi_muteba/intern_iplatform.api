﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Occupation;
using MasterData;

namespace iPlatform.Api.DTOs.Party.Contacts
{
    public class ContactDto : Resource, IAuditableDto, ICultureAware
    {
        public ContactDto()
        {
            SimpleAudit = new AuditDto();
            Events = new List<AuditEventDto>();
        }

        public int PartyId { get; set; }
        public Title Title { get; set; }
        public Gender Gender { get; set; }
        public OccupationDto Occupation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public DateTimeDto DateOfBirth { get; set; }
        public string IdentityNo { get; set; }
        public string PassportNo { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public Language Language { get; set; }
        public string DisplayName { get; set; }
        public RelationshipType RelationshipType { get; set; }
        public DateTimeDto DateCreated { get; set; }
        public DateTimeDto DateUpdated { get; set; }

        public MarriageType MarriageType { get; set; }
        public DateTimeDto DateOfMarriage { get; set; }

        public AuditDto SimpleAudit { get; private set; }
        public List<AuditEventDto> Events { get; private set; }
    }
}