﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Occupation;
using MasterData;

namespace iPlatform.Api.DTOs.Party.Contacts
{
    public class EditContactDto : Resource, IAuditableDto, IExecutionDto, IAffectExistingEntity
    {
        public EditContactDto()
        {
            SimpleAudit = new AuditDto();
            Events = new List<AuditEventDto>();
        }

        public Title Title { get; set; }
        public Gender Gender { get; set; }
        public OccupationDto Occupation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public DateTime? DateOfBirth { get; set; }

        [Required(ErrorMessage = "CONTACT_IDENTITY_NO_EMPTY")]
        public string IdentityNo { get; set; }

        public string PassportNo { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public Language Language { get; set; }
        public string DisplayName { get; set; }

        public MarriageType MarriageType { get; set; }
        public DateTime? DateOfMarriage { get; set; }


        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public AuditDto SimpleAudit { get; private set; }

        [Required(ErrorMessage = "CONTACT_RELATIONSHIP_TYPE_EMPTY")]
        public RelationshipType RelationshipType { get; set; }

        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }

        public int PartyId { get; set; }
    }
}