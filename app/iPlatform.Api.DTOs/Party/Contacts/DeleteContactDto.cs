﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Contacts
{
    public class DeleteContactDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public DeleteContactDto()
        {
            Events = new List<AuditEventDto>();
        }


        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}