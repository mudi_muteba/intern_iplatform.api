﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.ContactDetail
{
    public class ContactDetailDto : Resource, IAuditableDto, ICultureAware
    {
        public ContactDetailDto()
        {
            Events = new List<AuditEventDto>();
        }

        public string Work { get; set; }

        public string Direct { get; set; }

        public string Cell { get; set; }

        public string Home { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string Website { get; set; }

        public string Facebook { get; set; }

        public string Twitter { get; set; }

        public string LinkedIn { get; set; }

        public string Skype { get; set; }

        public List<AuditEventDto> Events { get; private set; }
    }
}