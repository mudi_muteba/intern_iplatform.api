﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Party.Payments
{
    public class PaymentDetailsDto : Resource, ICultureAware
    {
        public int BankDetailsId { get; set; }
        public int QuoteId { get; set; }
        public DateTimeDto DebitOrderDate { get; set; }
        public DateTimeDto InceptionDate { get; set; }
        public MethodOfPayment MethodOfPayment { get; set; }
        public PaymentPlan PaymentPlan { get; set; }
        public int PartyId { get; set; }
        public DateTimeDto TermEndDate { get; set; }
        public DateTimeDto TakeOnStrikeDate { get; set; }
    }
}
