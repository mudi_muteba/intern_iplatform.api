﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Party.Payments
{
    public class PaymentDetailSearchDto : BaseCriteria
    {
        public int? PartyId { get; set; }
    }
}
