﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Party.Payments
{
    public class CreatePaymentDetailsDto : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public CreatePaymentDetailsDto()
        {
            Context = DtoContext.NoContext();
        }

        public int BankDetailsId { get; set; }
        public int QuoteId { get; set; }
        public DateTime? DebitOrderDate { get; set; }
        public DateTime InceptionDate { get; set; }
        public DateTime TermEndDate { get; set; }
        public DateTime? TakeOnStrikeDate { get; set; }
        public MethodOfPayment MethodOfPayment { get; set; }
        public PaymentPlan PaymentPlan { get; set; }
        public int PartyId { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (QuoteId <= 0)
                validation.Add(PaymentDetailsValidationMessages.InvalidQuoteId);

            if (PartyId <= 0)
                validation.Add(PaymentDetailsValidationMessages.InvalidPartyId);

            return validation;
        }




        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}
