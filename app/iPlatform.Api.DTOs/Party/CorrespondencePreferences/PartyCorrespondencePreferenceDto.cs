﻿using iPlatform.Api.DTOs.Base;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Party.CorrespondencePreferences
{
    public class PartyCorrespondencePreferenceDto: Resource
    {
        public PartyCorrespondencePreferenceDto()
        {

        }

        public int PartyId { get; set; }
        public CorrespondenceType CorrespondenceType { get; set; }
        public bool Email { get; set; }
        public bool Telephone { get; set; }
        public bool SMS { get; set; }
        public bool Post { get; set; }

    }
}
