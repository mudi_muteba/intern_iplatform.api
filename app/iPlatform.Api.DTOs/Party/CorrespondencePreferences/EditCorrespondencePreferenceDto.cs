﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Party.CorrespondencePreferences
{
    public class EditCorrespondencePreferenceDto : IExecutionDto
    {
        public EditCorrespondencePreferenceDto()
        {
            CorrespondencePreferences = new List<PartyCorrespondencePreferenceDto>();
        }

        public int PartyId { get; set; }

        public List<PartyCorrespondencePreferenceDto> CorrespondencePreferences { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}