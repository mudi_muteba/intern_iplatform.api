﻿using System;
using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Correspondence
{
    public class ListPartyCorrespondenceDto : Resource, IAuditableDto, ICultureAware
    {
        public ListPartyCorrespondenceDto()
        {
            SimpleAudit = new AuditDto();
            Events = new List<AuditEventDto>();
        }

        public string Description { get; set; }
        public DateTimeDto CreatedOn { get; set; }
        public string From { get; set; }
        public string To { get; set; }

        public IList<PartyCorrespondenceAttachmentDto> Attachments { get; set; }

        public AuditDto SimpleAudit { get; private set; }
        public List<AuditEventDto> Events { get; private set; }
        public string EmailRecipient { get; set; }
    }
}