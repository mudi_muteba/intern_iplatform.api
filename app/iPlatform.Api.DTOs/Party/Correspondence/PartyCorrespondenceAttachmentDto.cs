﻿using System;
using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Correspondence
{
    public class PartyCorrespondenceAttachmentDto : Resource, IAuditableDto, ICultureAware
    {
        public PartyCorrespondenceAttachmentDto()
        {
            SimpleAudit = new AuditDto();
            Events = new List<AuditEventDto>();
        }

        public int PartyCorrespondenceId { get; set; }
        public string FileName { get; set; }
        public string FileUri { get; set; }
        public DateTime CreatedOn { get; set; }

        public AuditDto SimpleAudit { get; private set; }
        public List<AuditEventDto> Events { get; private set; }
    }
}