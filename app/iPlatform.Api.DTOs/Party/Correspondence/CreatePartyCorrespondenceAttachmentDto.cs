﻿using System;
using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Correspondence
{
    public class CreatePartyCorrespondenceAttachmentDto : Resource, IAuditableDto, ICultureAware
    {
        public CreatePartyCorrespondenceAttachmentDto()
        {
            SimpleAudit = new AuditDto();
            Events = new List<AuditEventDto>();
        }

        public int PartyCorrespondenceAttachmentId { get; set; }
        public string FileName { get; set; }
        public string FileUri { get; set; }

        public AuditDto SimpleAudit { get; private set; }
        public List<AuditEventDto> Events { get; private set; }
    }
}