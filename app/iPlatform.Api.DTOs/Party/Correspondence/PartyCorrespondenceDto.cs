﻿using System;
using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using Newtonsoft.Json;
using RestSharp.Contrib;

namespace iPlatform.Api.DTOs.Party.Correspondence
{
    public class PartyCorrespondenceDto : Resource, IAuditableDto, ICultureAware
    {
        public PartyCorrespondenceDto()
        {
            SimpleAudit = new AuditDto();
            Events = new List<AuditEventDto>();
            Attachments = new List<PartyCorrespondenceAttachmentDto>();
        }

        public int PartyId { get; set; }
        public string Description { get; set; }

        public string Body { get; set; }
        public PartyDto From { get; set; }
        public PartyDto To { get; set; }

        public DateTimeDto CreatedOn { get; set; }

        public IList<PartyCorrespondenceAttachmentDto> Attachments { get; set; }

        public AuditDto SimpleAudit { get; private set; }
        public List<AuditEventDto> Events { get; private set; }
        public string EmailRecipient { get; set; }
    }
}