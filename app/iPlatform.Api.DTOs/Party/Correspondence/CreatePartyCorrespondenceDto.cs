﻿using System;
using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Correspondence
{
    public class CreatePartyCorrespondenceDto : IExecutionDto
    {
        public CreatePartyCorrespondenceDto()
        {
            SimpleAudit = new AuditDto();
            Events = new List<AuditEventDto>();
            Attachments = new List<CreatePartyCorrespondenceAttachmentDto>();
        }

        public int PartyCorrespondenceId { get; set; }
        public int PartyId { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public int From { get; set; }
        public int To { get; set; }
        public string EmailRecipient { get; set; }

        public IList<CreatePartyCorrespondenceAttachmentDto> Attachments { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }

        public AuditDto SimpleAudit { get; private set; }
        public List<AuditEventDto> Events { get; private set; }
    }
}