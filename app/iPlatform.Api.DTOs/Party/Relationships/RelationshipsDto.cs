﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Party.Relationships
{
    public class RelationshipsDto
    {
        public RelationshipsDto()
        {
            Relationships = new List<RelationshipDto>();
        }

        public List<RelationshipDto> Relationships { get; set; }
    }
}
