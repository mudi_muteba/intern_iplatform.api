﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Party.Relationships
{
    public class EditRelationshipDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public EditRelationshipDto()
        {
            Context = DtoContext.NoContext();
        }

        public DtoContext Context { get; private set; }
        public int Id { get; set; }
        public int PartyId { get; set; }
        public int ChildPartyId { get; set; }
        public int ChannelId { get; set; }
        public virtual int RelationshipTypeId { get; set; }
        public virtual string Functions { get; set; }

        public virtual string Claims { get; set; }

        public virtual string Other { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id <= 0)
                validation.Add(RelationshipValidationMessages.IdRequired);

            if (PartyId <= 0)
                validation.Add(RelationshipValidationMessages.PartyIdRequired);

            if (ChildPartyId <= 0)
                validation.Add(RelationshipValidationMessages.ChildPartyIdRequired);

            if (Functions.Length > 50)
                validation.Add(RelationshipValidationMessages.FunctionCharacterExceeded);

            if (Claims.Length > 50)
                validation.Add(RelationshipValidationMessages.ClaimCharacterExceeded);

            if (Other.Length > 50)
                validation.Add(RelationshipValidationMessages.OtherCharacterExceeded);
            

            return validation;
        }
    }
}
