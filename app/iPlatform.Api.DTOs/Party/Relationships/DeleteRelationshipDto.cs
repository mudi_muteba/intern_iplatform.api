﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Party.Relationships
{
    public class DeleteRelationshipDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        public DeleteRelationshipDto()
        {
            Context = DtoContext.NoContext();
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                list.Add(RelationshipValidationMessages.IdRequired);
            }

            return list;
        }
    }
}