﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Individual;
using MasterData;

namespace iPlatform.Api.DTOs.Party.Relationships
{
    public class RelationshipDto: Resource, ICultureAware
    {
        public int ChildPartyId { get; set; }
        public int PartyId { get; set; }
        public IndividualDto Party { get; set; }
        public IndividualDto ChildParty { get; set; }
        public string Function { get; set; }
        public string Claim { get; set; }
        public string Other { get; set; }
        public RelationshipType RelationshipType { get; set; }
    }
}
