﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Party.LossHistoryDefinitions
{
    public class DisableLossHistoryDefinitionDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DisableLossHistoryDefinitionDto()
        {
            Context = DtoContext.NoContext();
        }

        public int Id { get; set; }
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {

        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                validation.Add(LossHistoryValidationMessages.IdRequired);
            }

            return validation;
        }
    }
}
