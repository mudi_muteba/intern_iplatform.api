﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.LossHistory;

namespace iPlatform.Api.DTOs.Party.LossHistoryDefinitions
{
    public class CreateLossHistoryDefinitionDto : AttributeValidationDto, IChannelAwareDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public int ChannelId { get; set; }

        public CreateLossHistoryDefinitionDto()
        {
            Context = DtoContext.NoContext();
        }

        public int PartyId { get; set; }
        public int PartyTypeId { get; set; }
        public DateTime? Created { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrEmpty(Convert.ToString(PartyId)))
            {
                validation.Add(LossHistoryDefinitionValidationMessages.PartyIdRequired);
            }

            if (string.IsNullOrEmpty(Convert.ToString(PartyTypeId)))
            {
                validation.Add(LossHistoryDefinitionValidationMessages.PartyTypeIdRequired);
            }

            if (Created.HasValue == false)
            {
                validation.Add(LossHistoryDefinitionValidationMessages.DateCreatedRequired);
            }

            return validation;
        }
    }
}
