﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.LossHistoryQuestionAnswers
{
    public class LossHistoryQuestionAnswerDto : Resource, IAuditableDto, ICultureAware
    {
        public LossHistoryQuestionAnswerDto()
        {
            Events = new List<AuditEventDto>();
        }

        public int PartyId { get; set; }
        public int PartyTypeId { get; set; }
        public DateTime Created { get; set; }

        public List<AuditEventDto> Events { get; private set; }
    }
}
