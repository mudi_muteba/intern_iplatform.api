﻿using MasterData;

namespace iPlatform.Api.DTOs.Party.Assets.AssetVehicles
{
    public class AssetExtrasDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
        public bool IsUserDefined { get; set; }
        public bool Selected { get; set; }
        public AccessoryType AccessoryType { get; set; }
    }
}
