﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.Party.Assets.AssetVehicles
{
    public class AssetVehicleDto : Resource, IAuditableDto, ICultureAware
    {
        public AssetVehicleDto()
        {
            Events = new List<AuditEventDto>();
        }
        public virtual int? YearOfManufacture { get; set; }
        public virtual string VehicleMake { get; set; }
        public virtual string VehicleModel { get; set; }
        public virtual string VehicleRegistrationNumber { get; set; }
        public virtual string VehicleMMCode { get; set; }
        public virtual VehicleType VehicleType { get; set; }

        public virtual List<AssetExtrasDto> Extras { get; set; }

        [Required]
        public int PartyId { get; set; }

        public List<AuditEventDto> Events { get; protected set; }
    }
}