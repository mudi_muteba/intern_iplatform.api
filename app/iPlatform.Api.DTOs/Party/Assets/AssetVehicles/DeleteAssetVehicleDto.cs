﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Assets.AssetVehicles
{
    public class DeleteAssetVehicleDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public DeleteAssetVehicleDto()
        {
            Events = new List<AuditEventDto>();
        }


        public virtual int AssetId { get; set; }
        [Required]
        public int PartyId { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}