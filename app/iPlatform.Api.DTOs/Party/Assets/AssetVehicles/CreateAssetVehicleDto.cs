﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Party.Assets.AssetVehicles
{
    public class CreateAssetVehicleDto : Resource, IExecutionDto
    {
        public CreateAssetVehicleDto()
        {
            Events = new List<AuditEventDto>();
        }

        [Required(ErrorMessage = "YEAR_OF_MANUFACTURE_EMPTY")]
        public virtual int? YearOfManufacture { get; set; }
        [Required(ErrorMessage = "VEHICLE_MAKE_EMPTY")]
        public virtual string VehicleMake { get; set; }
        [Required(ErrorMessage = "VEHICLE_MODEL_EMPTY")]
        public virtual string VehicleModel { get; set; }
        public virtual string VehicleRegistrationNumber { get; set; }
        [Required(ErrorMessage = "VEHICLE_MMCODE_EMPTY")]
        public virtual string VehicleMMCode { get; set; }
        public virtual VehicleType VehicleType { get; set; }
        [Required(ErrorMessage = "PARTY_ID_EMPTY")]
        public int PartyId { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
            
        }

        public DtoContext Context { get; private set; }
        public decimal SumInsured { get; set; }
    }
}