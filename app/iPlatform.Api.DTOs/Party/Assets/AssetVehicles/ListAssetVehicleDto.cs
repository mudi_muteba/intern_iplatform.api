﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Party.Assets.AssetVehicles
{
    public class ListAssetVehicleDto : Resource, IAuditableDto
    {
        public ListAssetVehicleDto()
        {
            Events = new List<AuditEventDto>();
        }

        public virtual int AssetId { get; set; }
        public virtual int? YearOfManufacture { get; set; }
        public virtual string VehicleMake { get; set; }
        public virtual string VehicleModel { get; set; }
        public virtual string VehicleRegistrationNumber { get; set; }
        public virtual string VehicleMMCode { get; set; }
        public virtual VehicleType VehicleType { get; set; }

        [Required]
        public int PartyId { get; set; }
        public List<AuditEventDto> Events { get; private set; }
    }
}