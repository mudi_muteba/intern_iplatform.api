﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Party.Assets.AssetRiskItems
{
    public class ListAssetRiskItemDto : Resource, IAuditableDto, ICultureAware
    {
        public ListAssetRiskItemDto()
        {
            Events = new List<AuditEventDto>();
        }

        public int PartyId { get; set; }
        public string SerialIMEINumber { get; set; }
        public int? AllRiskCategory { get; set; }
        public string Description { get; set; }
        public List<AuditEventDto> Events { get; private set; }
    }
}