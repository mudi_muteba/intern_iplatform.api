﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Assets.AssetRiskItems
{
    public class DeleteAssetRiskItemDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public DeleteAssetRiskItemDto()
        {
            Events = new List<AuditEventDto>();
        }


        public int PartyId { get; set; }

        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}