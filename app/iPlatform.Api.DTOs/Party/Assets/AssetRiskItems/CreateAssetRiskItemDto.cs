﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Assets.AssetRiskItems
{
    public class CreateAssetRiskItemDto : Resource, IExecutionDto
    {
        public CreateAssetRiskItemDto()
        {
            Events = new List<AuditEventDto>();
        }

        public int PartyId { get; set; }
        public string SerialIMEINumber { get; set; }
        public int? AllRiskCategory { get; set; }
        public string Description { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
            
        }

        public DtoContext Context { get; private set; }
    }
}