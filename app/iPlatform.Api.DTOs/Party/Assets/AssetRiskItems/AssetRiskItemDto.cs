﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;

namespace iPlatform.Api.DTOs.Party.Assets.AssetRiskItems
{
    public class AssetRiskItemDto : Resource, IAuditableDto, ICultureAware
    {
        public AssetRiskItemDto()
        {
            Events = new List<AuditEventDto>();
            Extras = new List<AssetExtrasDto>();
        }
        public int AssetId { get; set; }
        public int PartyId { get; set; }
        public string SerialIMEINumber { get; set; }
        public int? AllRiskCategory { get; set; }
        public string Description { get; set; }
        public List<AuditEventDto> Events { get; protected set; }

        public List<AssetExtrasDto> Extras { get; set; }
    }
}