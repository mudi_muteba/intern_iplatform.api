﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Assets.AssetAddresses
{
    public class CreateAssetAddressDto : Resource, IExecutionDto
    {
        public CreateAssetAddressDto()
        {
            Events = new List<AuditEventDto>();
        }

        public int PartyId { get; set; }
        public int AddressId { get; set; }
        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}