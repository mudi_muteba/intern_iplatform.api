﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Party.Assets
{
    public class AssetDto : Resource
    {
        public string Description { get; set; }
        public decimal SumInsured { get; set; }
    }
}
