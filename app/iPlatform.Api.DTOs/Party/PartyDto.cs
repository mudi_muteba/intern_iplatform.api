﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.ContactDetail;
using MasterData;

namespace iPlatform.Api.DTOs.Party
{
    public class PartyDto : Resource, IAuditableDto, ICultureAware
    {
        public PartyDto()
        {
            SimpleAudit = new AuditDto();
            Events = new List<AuditEventDto>();
            ContactDetail = new ContactDetailDto();
        }

        public List<AuditEventDto> Events { get; private set; }
        public AuditDto SimpleAudit { get; private set; }

        public string DisplayName { get; set; }
        public int ChannelId { get; set; }
        public DateTimeDto DateCreated { get; set; }
        public DateTimeDto DateUpdated { get; set; }
        public int MasterId { get; set; }
        public PartyType PartyType { get; set; }
        public ContactDetailDto ContactDetail { get; set; }
    }
}