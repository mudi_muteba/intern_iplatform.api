﻿using System.Collections.Generic;
using MasterData;

namespace iPlatform.Api.DTOs.Users
{
    public class UserAuthorisationGroupDto
    {
        public UserAuthorisationGroupDto()
        {
            Groups = new List<AuthorisationGroup>();
        }

        public UserAuthorisationGroupDto(List<int> channelIdses, List<AuthorisationGroup> authorisationGroups)
        {

            ChannelIds = channelIdses;
            Groups = authorisationGroups ?? new List<AuthorisationGroup>();
        }

        public List<int> ChannelIds { get; set; }
        public List<AuthorisationGroup> Groups { get; set; }
    }
}