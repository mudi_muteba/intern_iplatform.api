﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Users;

namespace iPlatform.Api.DTOs.Users
{
    public abstract class BaseUserDto : IValidationAvailableDto, IExecutionDto, IMultiChannelDto
    {
        public DtoContext Context { get; private set; }      
        public List<int> Channels { get; set; }
        public List<UserAuthorisationGroupDto> Groups { get; set; }

        protected BaseUserDto()
        {
            Channels = new List<int>();
            Groups = new List<UserAuthorisationGroupDto>();
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (!Channels.Any())
                list.Add(UserValidationMessages.NoChannelsAllocated);

            if (!Groups.Any())
                list.Add(UserValidationMessages.NoRolesAllocated);

            list.AddRange(InternalValidate());
            return list;
        }

        protected abstract List<ValidationErrorMessage> InternalValidate();
    }
}