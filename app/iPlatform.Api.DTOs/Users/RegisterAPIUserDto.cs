﻿using System.Linq;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Users;

namespace iPlatform.Api.DTOs.Users
{
    public class RegisterApiUserDto : AttributeValidationDto, IExecutionDto, IValidationAvailableDto
    {
        public RegisterApiUserDto()
        {
        }

        public RegisterApiUserDto(string name
            , string surname
            , string userName
            , string password) : this()
        {
            Name = name;
            Surname = surname;
            UserName = userName;
            Password = password;
        }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public int ChannelId { get; set; }

        public DtoContext Context { get; private set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var list = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(UserName))
                list.Add(UserValidationMessages.UserNameRequired);

            if (string.IsNullOrWhiteSpace(Password))
                list.Add(UserValidationMessages.PasswordRequired);

            if (string.IsNullOrWhiteSpace(Name))
                list.Add(UserValidationMessages.NameRequired);

            if (string.IsNullOrWhiteSpace(Name))
                list.Add(UserValidationMessages.LastNameRequired);

            if (ChannelId == 0)
                list.Add(UserValidationMessages.NoChannelsAllocated);

            return list;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}