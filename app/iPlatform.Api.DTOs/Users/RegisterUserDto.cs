﻿using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Users
{
    public class RegisterUserDto : AttributeValidationDto, IExecutionDto
    {
        public RegisterUserDto()
        {
        }

        public RegisterUserDto(string name
            , string surname
            , string userName
            , string idNumber
            , string phoneNumber)
        {
            Name = name;
            Surname = surname;
            UserName = userName;
            IdNumber = idNumber;
            PhoneNumber = phoneNumber;
        }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string IdNumber { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        public int ChannelId { get; set; }
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}