﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Users
{
    public class UserChannelSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public int ChannelId { get; set; }
        public int UserId { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
