﻿using System.Linq;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Users
{
    public class SaveUserChannelsDto : AttributeValidationDto, IExecutionDto
    {
        public SaveUserChannelsDto()
        {
            Users = new List<CreateUserDto>();
        }

        public int ChannelId { get; set; }

        public List<CreateUserDto> Users { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId == 0)
                validation.Add(UserChannelValidationMessages.ChannelIdRequired);

            if(!Users.Any())
                validation.Add(UserChannelValidationMessages.UsersRequired);

            return validation;
        }
    }
}
