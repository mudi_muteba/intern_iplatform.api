﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Individual;
using ValidationMessages;
using ValidationMessages.Users;

namespace iPlatform.Api.DTOs.Users
{
    public class EditUserDto : BaseUserDto, IAffectExistingEntity
    {
        public int Id { get; set; }
        public string ExternalReference { get; set; }
        public bool IsBroker { get; set; }
        public bool IsAccountExecutive { get; set; }

        public CreateIndividualDto CreateIndividualDto { get; set; }
        public EditIndividualDto EditIndividualDto { get; set; }
        public int DefaultChannelId { get; set; }
        public bool IsBillable { get; set; }
        public EditUserDto() { }

        public EditUserDto(int id)
        {
            Id = id;
        }

        protected override List<ValidationErrorMessage> InternalValidate()
        {
            if (Id <= 0)
            {
                return new List<ValidationErrorMessage>
                {
                    UserValidationMessages.UserIdRequired
                };
            }

            return new List<ValidationErrorMessage>();
        }
    }
}