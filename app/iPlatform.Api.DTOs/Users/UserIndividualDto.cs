﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Individual;

namespace iPlatform.Api.DTOs.Users
{
    public class UserIndividualDto : Resource, ICultureAware
    {
        public UserDto User;
        public IndividualDto Individual;
        public DateTime CreatedAt;
        public DateTime ModifiedAt;
        
    }
}