﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Individual;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Users.Discount;

namespace iPlatform.Api.DTOs.Users
{
    public class ListUserDto : Resource, ICultureAware
    {
        public ListUserDto()
        {
            Groups = new List<UserAuthorisationGroupDto>();
        }

        public string UserName { get; set; }
        public BasicIndividualDto Individual { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLoggedIn { get; set; }
        public DateTimeDto LastLoggedInDate { get; set; }

        public string ExternalReference { get; set; }
        public bool IsBroker { get; set; }
        public bool IsAccountExecutive { get; set; }

        public List<UserAuthorisationGroupDto> Groups { get; set; }

        public List<DiscountDto> Discounts { get; set; }
        public bool IsBillable { get; set; }
    }
}