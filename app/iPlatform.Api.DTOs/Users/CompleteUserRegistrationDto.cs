﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Users;

namespace iPlatform.Api.DTOs.Users
{
    public class CompleteUserRegistrationDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
        public string RegistrationToken { get; set; }
        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id <= 0)
                list.Add(UserValidationMessages.UserIdRequired);

            if (string.IsNullOrWhiteSpace(RegistrationToken))
                list.Add(UserValidationMessages.RegistrationTokenRequired);

            return list;
        }
    }
}