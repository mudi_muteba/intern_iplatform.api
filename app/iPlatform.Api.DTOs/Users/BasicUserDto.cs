﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Users
{
    public class BasicUserDto : Resource, ICultureAware
    {
        public string UserName { get; set; }
        public bool IsApproved { get; set; }
        public bool IsActive { get; set; }
        public bool IsLoggedIn { get; set; }
        public DateTimeDto LastLoggedInDate { get; set; }
        public string LoggedInFromIP { get; set; }
        public string LoggedInFromSystem { get; set; }
        public DateTimeDto CreatedAt { get; set; }
        public DateTimeDto ModifiedAt { get; set; }
        public bool IsBillable { get; set; }
    }
}