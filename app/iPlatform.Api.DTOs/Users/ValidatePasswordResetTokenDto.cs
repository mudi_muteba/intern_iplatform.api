﻿using System;
using System.Collections.Generic;
using System.Net;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace iPlatform.Api.DTOs.Users
{
    public class ValidatePasswordResetTokenDto : BaseResponseDto, IExecutionDto
    {
        public Guid PasswordResetToken { get; set; }
        public DtoContext Context { get; private set; }
        protected override List<HttpStatusCode> SuccessStatusCodes
        {
            get
            {
                return new List<HttpStatusCode>
                {
                    HttpStatusCode.OK
                };
            }
        }

        public ValidatePasswordResetTokenDto() { }

        public ValidatePasswordResetTokenDto(Guid passwordResetToken)
        {
            PasswordResetToken = passwordResetToken;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}