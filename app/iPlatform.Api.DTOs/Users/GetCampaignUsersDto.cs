﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Users
{
    public class GetCampaignUsersDto: IExecutionDto
    {
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }

        public GetCampaignUsersDto()
        {
            Context = DtoContext.NoContext();
            Campaigns = new List<int>();
        }

        public List<int> Campaigns { get; set; }
        public int UserId { get; set; }
        public int PartyId { get; set; }
    }
}
