﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Individual;
using ValidationMessages;
using ValidationMessages.Users;

namespace iPlatform.Api.DTOs.Users
{
    public class CreateUserDto : BaseUserDto
    {
        public CreateUserDto() { }

        public CreateUserDto(string userName, string password, bool isApproved,
            List<UserAuthorisationGroupDto> authorisationGroups = null)
        {
            UserName = userName;
            Password = password;
            Groups = authorisationGroups ?? new List<UserAuthorisationGroupDto>();
            IsApproved = isApproved;
        }

        public CreateUserDto(string userName, string password, bool isApproved, List<int> channels,
            List<UserAuthorisationGroupDto> authorisationGroups = null, CreateIndividualDto createIndividualDto = null, string externalReference = "", bool isBroker = false, bool isAccountExecutive = false)
        {
            UserName = userName;
            Password = password;
            IsApproved = isApproved;
            Channels = channels ?? new List<int>();
            Groups = authorisationGroups ?? new List<UserAuthorisationGroupDto>();
            CreateIndividualDto = createIndividualDto;
            ExternalReference = externalReference;
            IsBroker = isBroker;
            IsAccountExecutive = isAccountExecutive;
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsApproved { get; set; }
        public CreateIndividualDto CreateIndividualDto { get; set; }
        public int DefaultChannelId { get; set; }
        public string ExternalReference { get; set; }
        public bool IsBroker { get; set; }
        public bool IsAccountExecutive { get; set; }
        public bool IsBillable { get; set; }
        public SaveMultipleSettingsiRateUserDto SaveMultipleSettingsiRateUser { get; set; }
        public CreateSettingsiRateUserDto CreateSettingsiRateUserDto { get; set; }

        protected override List<ValidationErrorMessage> InternalValidate()
        {
            var list = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(UserName))
                list.Add(UserValidationMessages.UserNameRequired);

            if (string.IsNullOrWhiteSpace(Password))
                list.Add(UserValidationMessages.PasswordRequired);

            return list;
        }
    }
}