﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Users.Authentication;

namespace iPlatform.Api.DTOs.Users
{
    public class UserDto : Resource, ICultureAware
    {
        public UserDto()
        {
            Groups = new List<UserAuthorisationGroupDto>();
            SystemAuthorisation = new SystemAuthorisationDto();
            Channels = new List<ChannelReferenceDto>();
            Individuals = new List<IndividualDto>();
        }

        public string UserName { get; set; }
        public bool IsApproved { get; set; }
        public bool IsActive { get; set; }
        public bool IsLoggedIn { get; set; }
        public DateTimeDto LastLoggedInDate { get; set; }
        public string LoggedInFromIP { get; set; }
        public string LoggedInFromSystem { get; set; }
        public DateTimeDto CreatedAt { get; set; }
        public DateTimeDto ModifiedAt { get; set; }
        public SystemAuthorisationDto SystemAuthorisation { get; set; }
        public List<UserAuthorisationGroupDto> Groups { get; set; }
        public List<ChannelReferenceDto> Channels { get; set; }
        public List<IndividualDto> Individuals { get; set; }
        public int DefaultChannelId { get; set; }
        public string ExternalReference { get; set; }
        public bool IsBroker { get; set; }
        public bool IsAccountExecutive { get; set; }
        public bool IsBillable { get; set; }
        public string ApiVerions
        {
            get
            {
                Assembly assembly = typeof(UserDto).Assembly;
                FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);

                string version = versionInfo.FileVersion;
                return version;
            }
        }
    }
}