﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPerson.Api.DTOs;
using iPerson.Api.DTOs.Responses;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Users;
using ValidationMessages.Users.Discounts;

namespace iPlatform.Api.DTOs.Users.Discount
{
    public class SearchDiscountDto : IValidateDto, IValidationAvailableDto
    {
       [Required(ErrorMessage = "USER_ID_REQUIRED")]
       public int UserId { get; set; }
       [Required(ErrorMessage = "PRODUCT_ID_REQUIRED")]
       public int ProductId { get; set; }
       [Required(ErrorMessage = "COVER_NAME_DUPLICATE")]
       public int CoverId { get; set; }
       public List<ValidationErrorMessage> Validate()
       {
           var list = new List<ValidationErrorMessage>();

           if (UserId < 1)
               list.Add(DiscountValidationMessages.UserIdNotSet);
           if (ProductId < 1)
               list.Add(DiscountValidationMessages.ProductIdNotSet);
           if (CoverId < 1)
               list.Add(DiscountValidationMessages.CoverIdNotSet);

           return list;
       }

       List<ResponseErrorMessage> IValidateDto.Validate()
       {
           var list = new List<ResponseErrorMessage>();

           if (UserId < 1)
               list.Add(new ResponseErrorMessage(DiscountValidationMessages.UserIdNotSet.DefaultMessage, DiscountValidationMessages.UserIdNotSet.MessageKey));
           if (ProductId < 1)
               list.Add(new ResponseErrorMessage(DiscountValidationMessages.ProductIdNotSet.DefaultMessage, DiscountValidationMessages.ProductIdNotSet.MessageKey));
           if (CoverId < 1)
               list.Add(new ResponseErrorMessage(DiscountValidationMessages.CoverIdNotSet.DefaultMessage, DiscountValidationMessages.CoverIdNotSet.MessageKey));

           return list;
       }
    }
}
