﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Users.Discount
{
    public class EditDiscountDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public EditDiscountDto()
        {
            Events = new List<AuditEventDto>();
        }

        public int ChannelId { get; set; }

        public int UserId { get; set; }

        public int CoverDefinitionId { get; set; }

        public decimal Discount { get; set; }

        public decimal Load { get; set; }

        public decimal QuoteMaxDiscount { get; set; }

        public decimal ItemMaxDiscount { get; set; }

        public IList<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}