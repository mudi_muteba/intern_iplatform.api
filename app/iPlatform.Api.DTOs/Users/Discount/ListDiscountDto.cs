﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Admin;

namespace iPlatform.Api.DTOs.Users.Discount
{
    public class ListDiscountDto : Resource, IAuditableDto
    {
        public ListDiscountDto()
        {
            Events = new List<AuditEventDto>();
        }

        public int ChannelId { get; set; }

        public BasicUserDto User { get; set; }

        public BasicCoverDefinitionDto CoverDefinition { get; set; }

        public decimal Discount { get; set; }

        public decimal Load { get; set; }

        public decimal QuoteMaxDiscount { get; set; }

        public decimal ItemMaxDiscount { get; set; }

        public List<AuditEventDto> Events { get; private set; }
    }
}