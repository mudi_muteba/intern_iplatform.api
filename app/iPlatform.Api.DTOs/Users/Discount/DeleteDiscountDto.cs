﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Users.Discount
{
    public class DeleteDiscountDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public DeleteDiscountDto()
        {
            Events = new List<AuditEventDto>();
        }

        public int UserId { get; set; }

        public List<AuditEventDto> Events { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
        public DtoContext Context { get; private set; }
    }
}