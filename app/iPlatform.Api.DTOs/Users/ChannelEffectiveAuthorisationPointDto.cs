﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Users
{
    public class ChannelEffectiveAuthorisationPointDto
    {
        public ChannelEffectiveAuthorisationPointDto()
        {
            Points = new List<EffectiveAuthorisationPointDto>();
        }

        public int ChannelId { get; set; }
        public List<EffectiveAuthorisationPointDto> Points { get; set; }
    }
}