﻿using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Users
{
    public class ListUserChannelDto : Resource, ICultureAware
    {
        public ListUserChannelDto()
        {
        }

        public ChannelDto Channel { get; set; }

        public bool IsDefault { get; set; }

        public UserDto User { get; set; }
    }
}