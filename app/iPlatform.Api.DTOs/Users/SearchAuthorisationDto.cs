﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Users;

namespace iPlatform.Api.DTOs.Users
{
    public class SearchUserChannelDto : BaseCriteria, IValidationAvailableDto
    {
        public int UserId { get; set; }
        public int ChannelId { get; set; }
        public bool IsDefault { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();


            return list;
        }
    }
}