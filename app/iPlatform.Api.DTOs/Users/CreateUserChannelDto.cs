﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Channels;

namespace iPlatform.Api.DTOs.Users
{
    public class CreateUserChannelDto : AttributeValidationDto, IExecutionDto
    {
        public CreateUserChannelDto()
        {
        }

        public int ChannelId { get; set; }

        public bool IsDefault { get; set; }

        public int UserId { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId == 0)
                validation.Add(UserChannelValidationMessages.ChannelIdRequired);
            if (UserId == 0)
                validation.Add(UserChannelValidationMessages.UserIdRequired);


            // Check if user & channel combination already exist in UserChannel

            return validation;
        }
    }
}
