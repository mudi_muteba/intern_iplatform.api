﻿using MasterData.Authorisation;

namespace iPlatform.Api.DTOs.Users
{
    public class EffectiveAuthorisationPointDto
    {
        public RequiredAuthorisationPoint Point { get; set; }
        public EffectiveAuthorisationReason Reason { get; set; }
    }
}