﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Users;

namespace iPlatform.Api.DTOs.Users
{
    public class RequestPasswordResetDto : IExecutionDto, IValidationAvailableDto
    {
        public string UserName { get; set; }
        public DtoContext Context { get; set; }

        public RequestPasswordResetDto() { }

        public RequestPasswordResetDto(string userName)
        {
            UserName = userName;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(UserName))
                list.Add(UserValidationMessages.UserNameRequired);

            return list;
        }
    }
}