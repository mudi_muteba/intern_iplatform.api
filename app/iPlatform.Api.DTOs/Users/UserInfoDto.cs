﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Users
{
    public class UserInfoDto : Resource
    {
        public UserInfoDto()
        {

        }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string ExternalReference { get; set; }
        public bool IsBroker { get; set; }
        public bool IsAccountExecutive { get; set; }
        public int IndividualId { get; set; }
        public bool IsBillable { get; set; }

        public string DisplayName
        {
            get { return string.Format("{0} {1}", FirstName, LastName); }
        }
    }
}
