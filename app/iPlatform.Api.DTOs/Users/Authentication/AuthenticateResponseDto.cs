﻿namespace iPlatform.Api.DTOs.Users.Authentication
{
    public class AuthenticateResponseDto
    {
        public bool IsAuthenticated { get; set; }
        public string Token { get; set; }
        public UserDto User { get; set; }

        public AuthenticateResponseDto()
        {
        }

        private AuthenticateResponseDto(string token, bool authenticated)
        {
            Token = token;
            IsAuthenticated = authenticated;
        }

        public static AuthenticateResponseDto Authenticated(string token)
        {
            return new AuthenticateResponseDto(token, true);
        }

        public static AuthenticateResponseDto NotAuthenticated()
        {
            return new AuthenticateResponseDto(string.Empty, false);
        }
    }
}