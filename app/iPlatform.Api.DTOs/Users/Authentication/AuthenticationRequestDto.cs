﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Enums;

namespace iPlatform.Api.DTOs.Users.Authentication
{
    /*
    Sample JSON
    {
        "Email": "valid@iplatform.co.za",
        "Password": "328185F025CF4D128584BE1A67A92EB1"
    }
    */

    public class AuthenticationRequestDto : IExecutionDto
    {
        public string Token { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string IPAddress { get; set; }
        public string System { get; set; }
        public int ActiveChannelId { get; set; }
        public ApiRequestType RequestType { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}