﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Users.Authentication
{
    public class SystemAuthorisationDto
    {
        public SystemAuthorisationDto()
        {
            Channels = new List<ChannelAuthorisationDto>();
        }

        public SystemAuthorisationDto(params ChannelAuthorisationDto[] collections)
        {
            Channels = collections == null
                ? new List<ChannelAuthorisationDto>()
                : new List<ChannelAuthorisationDto>(collections);
        }

        public List<ChannelAuthorisationDto> Channels { get; set; }
    }
}