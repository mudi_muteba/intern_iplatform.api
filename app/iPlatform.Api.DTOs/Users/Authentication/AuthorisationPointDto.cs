﻿namespace iPlatform.Api.DTOs.Users.Authentication
{
    public class AuthorisationPointDto
    {
        public AuthorisationPointDto()
        {
        }

        public AuthorisationPointDto(string category, string name)
        {
            Category = category;
            Name = name;
        }

        public string Category { get; private set; }
        public string Name { get; private set; }

        public string Key
        {
            get { return string.Format("{0}_{1}", Category, Name); }
        }
    }
}