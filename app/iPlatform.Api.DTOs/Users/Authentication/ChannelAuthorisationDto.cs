﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Users.Authentication
{
    public class ChannelAuthorisationDto
    {
        public ChannelAuthorisationDto()
        {
        }

        public ChannelAuthorisationDto(int channelId, params AuthorisationPointDto[] points)
        {
            ChannelId = channelId;
            Points = points == null
                ? new List<AuthorisationPointDto>()
                : new List<AuthorisationPointDto>(points);
        }

        public int ChannelId { get; set; }
        public List<AuthorisationPointDto> Points { get; set; }
    }
}