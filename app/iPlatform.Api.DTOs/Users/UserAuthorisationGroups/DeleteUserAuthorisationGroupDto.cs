﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using MasterData;

namespace iPlatform.Api.DTOs.Users.UserAuthorisationGroups
{
    public class DeleteUserAuthorisationGroupDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DeleteUserAuthorisationGroupDto()
        {

        }
        public int Id { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}