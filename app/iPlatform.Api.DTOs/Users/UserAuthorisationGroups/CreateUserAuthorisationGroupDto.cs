﻿
using iPlatform.Api.DTOs.Base;
using MasterData;
using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Users.UserAuthorisationGroups
{
    public class CreateUserAuthorisationGroupDto  : AttributeValidationDto, IExecutionDto
    {
        public CreateUserAuthorisationGroupDto()
        {
            Context = DtoContext.NoContext();
        }

        public AuthorisationGroup AuthorisationGroup { get; set; }
        public int UserId { get; set; }
        public int ChannelId { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }

    }
}
