﻿
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using MasterData;
using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Users.UserAuthorisationGroups
{
    public class ListUserAuthorisationGroupDto : Resource
    {
        public ListUserAuthorisationGroupDto()
        {

        }

        public AuthorisationGroup AuthorisationGroup { get; set; }
        public UserInfoDto User { get; set; }
        public ChannelInfoDto Channel { get; set; }


    }
}
