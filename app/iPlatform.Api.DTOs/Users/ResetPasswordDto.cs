﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Users;

namespace iPlatform.Api.DTOs.Users
{
    public class ResetPasswordDto : IExecutionDto, IValidationAvailableDto
    {
        public string Password { get; set; }
        public Guid PasswordResetToken { get; set; }
        public DtoContext Context { get; set; }

        public ResetPasswordDto() { }

        public ResetPasswordDto(string password, Guid passwordResetToken)
        {
            Password = password;
            PasswordResetToken = passwordResetToken;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(Password))
                list.Add(UserValidationMessages.PasswordRequired);

            if (PasswordResetToken == new Guid())
                list.Add(UserValidationMessages.PasswordResetTokenRequired);

            return list;
        }
    }
}