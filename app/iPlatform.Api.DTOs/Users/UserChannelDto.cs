﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Users
{
    public class UserChannelDto : Resource
    {
        public UserChannelDto()
        {
        }

        public int ChannelId { get; set; }

        public bool IsDefault { get; set; }

        public int UserId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }
    }
}