using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Users.Search
{
    public class UserSearchOrderByDto
    {
        public OrderByDirection Direction { get; set; }
        public UserOrderByField Field { get; set; }
        public OrderByField XXX { get; set; }
    }
}