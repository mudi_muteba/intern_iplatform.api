namespace iPlatform.Api.DTOs.Users.Search
{
    public enum UserOrderByField
    {
        UserName = 0,
        IsApproved = 1,
        IsLoggedIn = 2,
        LastLoggedIn = 3
    }
}