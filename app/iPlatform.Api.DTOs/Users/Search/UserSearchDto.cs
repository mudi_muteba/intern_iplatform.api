﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Users;

namespace iPlatform.Api.DTOs.Users.Search
{
    public class UserSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public string UserName { get; set; }
        public string Name { get; set; }
        public string ChannelName { get; set; }
        public bool? IsApproved { get; set; }
        public bool? IsLoggedIn { get; set; }
        public IEnumerable<int> AuthorisationGroups { get; set; }
        public bool? IsBillable { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var valid = !string.IsNullOrWhiteSpace(UserName)
                        || !IsApproved.HasValue
                        || !IsLoggedIn.HasValue;

            var list = new List<ValidationErrorMessage>();
            if (!valid)
                list.Add(UserValidationMessages.InvalidSearchCriteria);

            return list;
        }
    }
}