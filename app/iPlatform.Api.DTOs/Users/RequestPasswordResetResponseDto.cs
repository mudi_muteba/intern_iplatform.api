using System;

namespace iPlatform.Api.DTOs.Users
{
    public class RequestPasswordResetResponseDto
    {
        public Guid ResetToken { get; set; }
        public int Id { get; set; }
    }
}