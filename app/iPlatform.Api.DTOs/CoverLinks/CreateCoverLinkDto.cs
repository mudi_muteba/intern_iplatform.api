﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using ValidationMessages;

namespace iPlatform.Api.DTOs.CoverLinks
{
    public class CreateCoverLinkDto : AttributeValidationDto, IExecutionDto
    {
        public CreateCoverLinkDto()
        {
        }

        public ChannelInfoDto Channel { get; set; }
        public int VisibleIndex { get; set; }
        public ListCoverDefinitionDto CoverDefinition { get; set; }


        public DtoContext Context { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
