﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.CoverLinks
{
    public class ListCoverLinkDto : Resource, ICultureAware
    {
        public ListCoverLinkDto()
        {
        }

        public int ChannelId { get; set; }

        public ChannelInfoDto Channel { get; set; }
        public int VisibleIndex { get; set; }
        public ListCoverDefinitionDto CoverDefinition { get; set; }
    }
}