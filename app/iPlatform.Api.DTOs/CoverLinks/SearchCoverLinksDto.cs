﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.CoverLinks
{
    public class SearchCoverLinksDto : BaseCriteria
    {
        public string CoverDefinitionName { get; set; }
        public int CoverDefinitionId { get; set; }
        public int ChannelId { get; set; }
        public string ChannelName { get; set; }

        public SearchCoverLinksDto()
        {
        }
    }
}
