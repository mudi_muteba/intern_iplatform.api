﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.CoverLinks
{
    public class DeleteCoverLinkDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DeleteCoverLinkDto()
        {
            Context = DtoContext.NoContext();
        }
        public DeleteCoverLinkDto(int id)
        {
            Id = id;
            Context = DtoContext.NoContext();
        }
        public DtoContext Context { get; set; }

        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
