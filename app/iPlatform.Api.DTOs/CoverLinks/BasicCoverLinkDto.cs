﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.CoverLinks
{
    public class BasicCoverLinkDto : Resource, ICultureAware
    {
        public BasicCoverLinkDto()
        {
            CoverDefinition = new ListCoverDefinitionDto();
        }

        public int ChannelId { get; set; }
        public ListCoverDefinitionDto CoverDefinition { get; set; }
    }
}