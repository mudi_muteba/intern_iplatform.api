﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.MapVapQuestionDefinitions
{
    public class DeleteMapVapQuestionDefinitionDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DeleteMapVapQuestionDefinitionDto()
        {

        }

        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            if (Id == 0)
                validation.Add(MapVapQuestionDefinitionValidationMessages.IdRequired);

            return validation;
        }
    }
}
