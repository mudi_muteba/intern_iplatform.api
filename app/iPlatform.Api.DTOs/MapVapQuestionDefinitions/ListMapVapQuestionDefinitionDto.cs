﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.MapVapQuestionDefinitionCover;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.MapVapQuestionDefinitions
{
    public class ListMapVapQuestionDefinitionDto : Resource
    {
        public ListMapVapQuestionDefinitionDto()
        {

        }
        public ListProductDto Product { get; set; }
        public ListChannelDto Channel { get; set; }
        public QuestionDefinitionDto QuestionDefinition { get; set; }
        public decimal Premium { get; set; }
        public bool Enabled { get; set; }
        public string ExternalVapProductCode { get; set; }
        public bool PerSectionVap { get; set; }
        public int AnnualizedMultiplier { get; set; }
        public decimal SumInsured { get; set; }
        public List<MapVapQuestionDefinitionCoverDto> MapVapQuestionDefinitionCovers { get; set; }
    }
}