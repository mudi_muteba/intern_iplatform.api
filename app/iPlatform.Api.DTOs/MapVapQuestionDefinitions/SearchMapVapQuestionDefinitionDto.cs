﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.MapVapQuestionDefinitions
{
    public class SearchMapVapQuestionDefinitionDto : BaseCriteria, IValidationAvailableDto
    {
        public string ChannelName { get; set; }

        public string ProductName { get; set; }

        #region [ Validation ]

        public List<ValidationErrorMessage> Validate()
        {
            List<ValidationErrorMessage> validationMessages = new List<ValidationErrorMessage>();
            return validationMessages;
        }

        #endregion
    }
}
