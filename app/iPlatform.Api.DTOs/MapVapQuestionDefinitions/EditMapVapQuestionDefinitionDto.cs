﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using iPlatform.Api.DTOs.MapVapQuestionDefinitionCover;
using ValidationMessages;

namespace iPlatform.Api.DTOs.MapVapQuestionDefinitions
{
    public class EditMapVapQuestionDefinitionDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public EditMapVapQuestionDefinitionDto()
        {

        }

        public int Id { get; set; }
        public int ProductId { get; set; }
        public int ChannelId { get; set; }
        public int QuestionDefinitionId { get; set; }
        public decimal Premium { get; set; }
        public bool Enabled { get; set; }
        public string ExternalVapProductCode { get; set; }
        public bool PerSectionVap { get; set; }
        public int AnnualizedMultiplier { get; set; }
        public decimal SumInsured { get; set; }
        public List<EditMapVapQuestionDefinitionCoverDto> MapVapQuestionDefinitionCovers { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            if (Id == 0)
                validation.Add(MapVapQuestionDefinitionValidationMessages.IdRequired);

            if (ChannelId == 0)
                validation.Add(MapVapQuestionDefinitionValidationMessages.ChannelIdRequired);

            if (ProductId == 0)
                validation.Add(MapVapQuestionDefinitionValidationMessages.ProductIdRequired);

            if (QuestionDefinitionId == 0)
                validation.Add(MapVapQuestionDefinitionValidationMessages.QuestionIdRequired);

            if (AnnualizedMultiplier < 0 || AnnualizedMultiplier > 12)
                validation.Add(MapVapQuestionDefinitionValidationMessages.AnnualizedMultiplierInvalid.AddParameters(new[] { AnnualizedMultiplier.ToString() }));

            return validation;
        }
    }
}
