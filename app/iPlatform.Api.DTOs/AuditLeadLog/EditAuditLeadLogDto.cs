﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.AuditLeadLog
{
    public class EditAuditLeadLogDto
    {
        public int LeadId { get; set; }
        
        public DateTime Timestamp { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string ProductCode { get; set; }

        public int IsQuoted { get; set; }

        public bool IsAccepted { get; set; }

        public bool IsSold { get; set; }

        public Guid SystemId { get; set; }

        public string EnvironmentType { get; set; }
    }
}
