﻿using System.Collections.Generic;
using ValidationMessages;
using ValidationMessages.Notifications;

namespace iPlatform.Api.DTOs.Notifications
{
    public class CreateNotificationDto : BaseNotificationDto
    {
        public CreateNotificationDto()
        {

        }
        
        protected override List<ValidationErrorMessage> InternalValidate()
        {
            var list = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(Message))
            {
                list.Add(NotificationValidationMessages.MissingQuestionText);
            }
            
            return list;
        }
    }
}
