﻿using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Notifications
{
    public class ResponseNotificationDto : BaseNotificationDto
    {
        public ResponseNotificationDto()
        {

        }


        protected override List<ValidationErrorMessage> InternalValidate()
        {
            return null;
        }
    }
}
