﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Notifications;

namespace iPlatform.Api.DTOs.Notifications
{
    public abstract class BaseNotificationDto : IValidationAvailableDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }
        public int Id { get; set; }
        public int SenderUserId { get; set; }
        public int? ChannelId { get; set; }
        public int? RecipientUserId { get; set; }
        public string Message { get; set; }
        public string SenderAlias { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime DateTimeToNotify { get; set; }

        protected BaseNotificationDto()
        {
            DateCreated = DateTime.UtcNow;
        }
        
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();
            
            if (string.IsNullOrEmpty(Message.Trim()))
            {
                list.Add(NotificationValidationMessages.MissingQuestionText);
            }
            
            list.AddRange(InternalValidate());
            return list;
        }

        protected abstract List<ValidationErrorMessage> InternalValidate();
    }
}
