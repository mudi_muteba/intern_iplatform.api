﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Notifications
{
    public class EditNotificationUserDto : Resource, IAffectExistingEntity, IExecutionDto
    {
        public int NotificationUserId { get; set; }

        public DtoContext Context { get; set; }

        public EditNotificationUserDto(int notificationUserId)
        {
            NotificationUserId = notificationUserId;
        }
        public void SetContext(DtoContext context)
        {
            this.Context = context;
        }

    }
}
