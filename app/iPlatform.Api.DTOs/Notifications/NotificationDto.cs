﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Notifications
{
    public class NotificationDto :Resource
    {
        public int NotificationUserId { get; set; }
        public int SenderUserId { get; set; }
        public int RecipientUserId { get; set; }
        public string Message { get; set; }
        public string SenderAlias { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsMessageRead { get; set; }
        
        public void SetContext(DtoContext context)
        {
          
        }

    }
}
