﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.OverrideRatingQuestion
{
    public class DeleteOverrideRatingQuestionDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
        public DtoContext Context { get; private set; }

        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        #region [ Validation ]

        public List<ValidationErrorMessage> Validate()
        {
            List<ValidationErrorMessage> errorMessages = new List<ValidationErrorMessage>();
            if (Id == 0)
                errorMessages.Add(OverrideRatingQuestionValidationMessage.IdRequired);

            return errorMessages;
        }

        #endregion
    }
}
