﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.OverrideRatingQuestion
{
    public class OverrideRatingQuestionAnswerDto : Resource, ICultureAware
    {
        public string ProductCode { get; set; }

        public Question OverrideQuestion { get; set; }

        public Cover Cover { get; set; }

        public string OverrideValue { get; set; } 
    }
}
