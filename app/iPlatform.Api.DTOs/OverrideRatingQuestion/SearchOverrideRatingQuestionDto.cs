﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.OverrideRatingQuestion
{
    public class SearchOverrideRatingQuestionDto : BaseCriteria, IValidationAvailableDto
    {
        public string ProductName { get; set; }

        public string CoverName { get; set; }

        #region [ Validation ]

        public List<ValidationErrorMessage> Validate()
        {
            List<ValidationErrorMessage> validationMessages = new List<ValidationErrorMessage>();
            return validationMessages;
        }

        #endregion
    }
}
