﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.OverrideRatingQuestionChannel;
using iPlatform.Api.DTOs.OverrideRatingQuestionMapVapQuestionDefinition;
using iPlatform.Api.DTOs.Products;
using MasterData;

namespace iPlatform.Api.DTOs.OverrideRatingQuestion
{
    public class OverrideRatingQuestionDto : Resource, ICultureAware
    {
        public CoverDefinitionDto CoverDefinition { get; set; }

        public QuestionDefinitionDto QuestionDefinition { get; set; }

        public string OverrideValue { get; set; }

        public IEnumerable<OverrideRatingQuestionChannelDto> OverrideRatingQuestionChannels { get; set; }

        public IEnumerable<OverrideRatingQuestionMapVapQuestionDefinitionDto> OverrideRatingQuestionMapVapQuestionDefinitions { get; set; }
    }
}
