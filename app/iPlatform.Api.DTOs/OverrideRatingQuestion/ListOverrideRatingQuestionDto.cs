﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using iPlatform.Api.DTOs.OverrideRatingQuestionChannel;
using iPlatform.Api.DTOs.OverrideRatingQuestionMapVapQuestionDefinition;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.OverrideRatingQuestion
{
    public class ListOverrideRatingQuestionDto : Resource, ICultureAware
    {
        public int OverrideRatingQuestionId { get; set; }

        public string ProductName { get; set; }

        public string CoverName { get; set; }

        public string QuestionGroupName { get; set; }

        public string QuestionName { get; set; }

        public string OverrideValue { get; set; }

        public List<OverrideRatingQuestionChannelDto> OverrideRatingQuestionChannels { get; set; }

        public List<OverrideRatingQuestionMapVapQuestionDefinitionDto> OverrideRatingQuestionMapVapQuestionDefinitions { get; set; }
    }
}
