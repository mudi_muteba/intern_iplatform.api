﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.OverrideRatingQuestionChannel;
using iPlatform.Api.DTOs.OverrideRatingQuestionMapVapQuestionDefinition;
using ValidationMessages;

namespace iPlatform.Api.DTOs.OverrideRatingQuestion
{
    public class EditOverrideRatingQuestionDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DtoContext Context { get; private set; }

        public int Id { get; set; }

        public int ProductId { get; set; }

        public int CoverId { get; set; }

        public int QuestionId { get; set; }

        public int CoverDefinitionId { get; set; }

        public int QuestionDefinitionId { get; set; }

        public string OverrideValue { get; set; }

        public List<EditOverrideRatingQuestionChannelDto> OverrideRatingQuestionChannels { get; set; }

        public List<EditOverrideRatingQuestionMapVapQuestionDefinitionDto> OverrideRatingQuestionMapVapQuestionDefinitions { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        #region [ Validation ]

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            List<ValidationErrorMessage> errorMessages = new List<ValidationErrorMessage>();
            if (Id == 0)
                errorMessages.Add(OverrideRatingQuestionValidationMessage.IdRequired);
            if (ProductId == 0)
                errorMessages.Add(OverrideRatingQuestionValidationMessage.ProductIdRequired);
            if (CoverId == 0)
                errorMessages.Add(OverrideRatingQuestionValidationMessage.CoverIdRequired);
            if (QuestionId == 0)
                errorMessages.Add(OverrideRatingQuestionValidationMessage.QuestionIdRequired);

            return errorMessages;
        }

        #endregion
    }
}
