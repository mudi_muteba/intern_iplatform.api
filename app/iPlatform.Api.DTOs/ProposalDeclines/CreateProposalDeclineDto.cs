﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.ProposalDeclines
{
    public class CreateProposalDeclineDto : AttributeValidationDto, IChannelAwareDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public int ChannelId { get; set; }

        public CreateProposalDeclineDto()
        {
            Context = DtoContext.NoContext();
        }

        public int PartyId { get; set; }
        public string RiskItemDeclined { get; set; }
        public string DeclinedReason { get; set; }
        public DateTime TimeStamp { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
