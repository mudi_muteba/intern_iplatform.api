﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.ProposalDeclines
{
    public class ProposalDeclineSearchDto : BaseCriteria
    {
        public int PartyId { get; set; }
        public string RiskItemDeclined { get; set; }
        public string DeclinedReason { get; set; }
    }
}
