﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.ProposalDeclines
{
    public class ProposalDeclineDto : Resource, IAuditableDto, ICultureAware
    {
        public List<AuditEventDto> Events { get; set; }

        public int PartyId { get; set; }
        public string RiskItemDeclined { get; set; }
        public string DeclinedReason { get; set; }
        public DateTime TimeStamp { get; set; }

        public ProposalDeclineDto()
        {
            Events = new List<AuditEventDto>();
        }
    }
}
