﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.Tia.Parties
{
    public class TiaPartiesSearchDto : Resource, IAuditableDto, IExecutionDto
    {
        public TiaPartiesSearchDto()
        {
            Events = new List<AuditEventDto>();
        }

        public string Name { get; set; }
        public string IDNumber { get; set; }
        public List<AuditEventDto> Events { get; private set; }


        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

     
    }
}