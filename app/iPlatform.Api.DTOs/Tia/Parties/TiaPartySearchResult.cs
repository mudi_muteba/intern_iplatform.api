namespace iPlatform.Api.DTOs.Tia.Parties
{
    public class TiaPartySearchResult
    {
        public string id { get; set; }
        public string idAlt { get; set; }
        public string nameType { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public string forename { get; set; }
        public string middleName { get; set; }
        public string surname { get; set; }
        public string civilRegCode { get; set; }
        public string birthDate { get; set; }
        public string deceasedDate { get; set; }
        public string emigrationDate { get; set; }
        public string hiddenIdentity { get; set; }
        public string companyRegNo { get; set; }
        public string companyVatNo { get; set; }
        public string contactPerson { get; set; }
        public string institutionCode { get; set; }
        public string employees { get; set; }
        public string industryCode { get; set; }
        public string language { get; set; }
        public string currencyCode { get; set; }
        public string note { get; set; }
        public string siteSeqNo { get; set; }
        public string houseCoName { get; set; }
        public string street { get; set; }
        public string streetNo { get; set; }
        public string floor { get; set; }
        public string floorExt { get; set; }
        public string postArea { get; set; }
        public string postStreet { get; set; }
        public string postalRegion { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string countryCode { get; set; }
        public string county { get; set; }
        public string addressCode { get; set; }
        public string buildingName { get; set; }
        public string buildingNumber { get; set; }
        public string departmentName { get; set; }
        public string dependLocality { get; set; }
        public string dependStreet { get; set; }
        public string dependStreetDesc { get; set; }
        public string poBox { get; set; }
        public string streetDescriptor { get; set; }
        public string subBuilding { get; set; }
        public string mailHouseCoName { get; set; }
        public string mailStreet { get; set; }
        public string mailStreetNo { get; set; }
        public string mailFloor { get; set; }
        public string mailFloorExt { get; set; }
        public string mailPostArea { get; set; }
        public string mailPostStreet { get; set; }
        public string mailPostalRegion { get; set; }
        public string mailCity { get; set; }
        public string mailCountry { get; set; }
        public string mailCountryCode { get; set; }
        public string mailCounty { get; set; }
        public string mailAddressCode { get; set; }
        public string mailBuildingName { get; set; }
        public string mailBuildingNumber { get; set; }
        public string mailDepartmentName { get; set; }
        public string mailDependLocality { get; set; }
        public string mailDependStreet { get; set; }
        public string mailDependStreetDesc { get; set; }
        public string mailPoBox { get; set; }
        public string mailStreetDescriptor { get; set; }
        public string mailSubBuilding { get; set; }

        public void SetContactDetail(object getContactDetail)
        {

        }
    }
}