using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Tia.Parties
{

    /// <summary>
    /// custom app client search response
    /// </summary>
    public class TiaPartiesSearchResultDto : Resource
    {
        public TiaPartiesSearchResultDto()
        {
            Parties = new List<TiaPartySearchResultDto>();
        }


        public List<TiaPartySearchResultDto> Parties { get; set; }
    }

    /// <summary>
    /// custom app client search response
    /// </summary>
    public class TiaPartySearchResultDto
    {
        public string Initials { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ClientFullName { get; set; }
        public string IDNumber { get; set; }
        public string CellphoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string BusinessName { get; set; }
        public string Occupation { get; set; }
        public string WorkNumber { get; set; }
        public string HomeNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressPostalCode { get; set; }
    }
}