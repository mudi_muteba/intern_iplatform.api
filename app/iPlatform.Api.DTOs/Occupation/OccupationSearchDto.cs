﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Campaigns;

namespace iPlatform.Api.DTOs.Occupation
{
    public class OccupationSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public string Name { get; set; }

        public OccupationSearchDto()
        {
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();
            
            return validation;
        }
    }
}