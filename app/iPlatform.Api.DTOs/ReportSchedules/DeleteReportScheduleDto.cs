﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.ReportSchedules
{
    public class DeleteReportScheduleDto : Resource, IAffectExistingEntity, IExecutionDto
    {
        public DtoContext Context { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}