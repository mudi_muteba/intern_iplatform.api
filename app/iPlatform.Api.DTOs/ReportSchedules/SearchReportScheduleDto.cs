﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.ReportSchedules
{
    public class SearchReportScheduleDto : BaseCriteria, IValidationAvailableDto
    {
        public string Name { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            return new List<ValidationErrorMessage>();
        }
    }
}