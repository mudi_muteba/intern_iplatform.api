﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using ValidationMessages;

namespace iPlatform.Api.DTOs.ReportSchedules
{
    public class InvokeReportScheduleDto : Resource, IExecutionDto, IValidationAvailableDto, ICultureAware
    {
        public InvokeReportScheduleDto() { }

        public InvokeReportScheduleDto(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
        public string RootPath { get; set; }
        public DtoContext Context { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            return new List<ValidationErrorMessage>();
        }
    }
}