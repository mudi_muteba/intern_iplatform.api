﻿using System;
using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Users;
using iPlatform.Enums.ReportSchedules;
using ValidationMessages;

namespace iPlatform.Api.DTOs.ReportSchedules
{
    public class ReportScheduleDto : Resource, IExecutionDto, IValidationAvailableDto, ICultureAware
    {
        public ReportScheduleDto()
        {
            Context = DtoContext.NoContext();
            Reports = new List<ReportScheduleReportDto>();
            Campaigns = new List<ReportScheduleCampaignDto>();
            Recipients = new List<ReportScheduleRecipientDto>();
            ReportScheduleParams = new List<ReportScheduleParamDto>();
        }

        public BasicUserDto Owner { get; set; }

        public IList<ReportScheduleReportDto> Reports { get; set; }
        public IList<ReportScheduleCampaignDto> Campaigns { get; set; }
        public IList<ReportScheduleRecipientDto> Recipients { get; set; }

        public string Name { get; set; }
        public int FormatId { get; set; }
        public int FrequencyId { get; set; }
        public string FrequencyName { get; set; }
        public ReportSchedulePeriodType PeriodType { get; set; }
        public string WeekDay { get; set; }
        public int MonthDay { get; set; }
        public DateTime Time { get; set; }
        public string Message { get; set; }
        
        public string AdhocRecipients { get; set; }
        public DtoContext Context { get; set; }

        public string PathMap { get; set; }

        public bool ShouldZip { get; set; }
        public string ZipPassword { get; set; }
        public DateTime CreatedOn { get; set; }

        public IList<ReportScheduleParamDto> ReportScheduleParams { get; set; }
        public IDictionary<string, string> ReportScheduleParamValues
        {
            get { return ReportScheduleParams.GroupBy(x => x.ReportParam.Field).ToDictionary(grp => grp.Key, grp => grp.Select(x => x.Value).FirstOrDefault()); }
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();
            return list;
        }
    }
}