﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.ContactUs
{
    public class ContactUsEmailDto : IExecutionDto
    {
        public string BaseUrl { get; set; }
        public string Email { get; set; }
        public string OrganizationCode { get; set; }
        public int ChannelId { get; set; }
        public string Body { get; set; }
        public string FromEmail { get; set; }
        public string Subject { get; set; }
        //added new properties
        public string CellPhone { get; set; }
        public string AlternativeEmail { get; set; }
        public DtoContext Context { get; set; }

       public void SetContext(DtoContext context)
        {
            Context = context;
        }

    }
}
