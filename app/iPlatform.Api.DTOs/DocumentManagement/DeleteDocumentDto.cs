﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.DocumentManagement
{
    public class DeleteDocumentDto : IExecutionDto
    {
        public int UserDocumentId { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}