﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.DocumentManagement
{
    public class DocumentResultDto : Resource
    {
        public DocumentResultDto() { }
        public string FileAssociation { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public  DateTime? DateCreated { get; set; }
        public string Tags { get; set; }
        public bool IsDocumentTrue { get; set; }
        public bool NoOriginalDocument { get; set; }
        public int CreatedById { get; set; }
        public int PartyId { get; set; }
        public int ContentLength { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public byte[] ContentBytes { get; set; }
    }
}