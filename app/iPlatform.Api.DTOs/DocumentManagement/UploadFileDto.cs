﻿using System.IO;

namespace iPlatform.Api.DTOs.DocumentManagement
{
    public class UploadFileDto
    {
        public int ContentLength { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public byte[] InputStream { get; set; }
    }
}