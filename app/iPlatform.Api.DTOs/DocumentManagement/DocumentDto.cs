﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.DocumentManagement
{
    public class DocumentDto : Resource, IExecutionDto
    {
        public string FileAssociation { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string Tags { get; set; }
        public int ContentLength { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public byte[] InputStream { get; set; }
        public bool IsDocumentTrue { get; set; }
        public bool NoOriginalDocument { get; set; }
        public int CreatedById { get; set; }
        public string CreatedName { get; set; }
        public int PartyId { get; set; }
        public int LinkedDocumentId { get; set; }
        public DateTime? DateCreated { get; set; }


        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}