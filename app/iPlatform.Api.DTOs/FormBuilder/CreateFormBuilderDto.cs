﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.FormBuilder
{
    public class CreateFormBuilderDto: AttributeValidationDto, IExecutionDto
    {
        public CreateFormBuilderDto()
        {
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public List<FormBuilderDetailDto> FormBuilderDetailsDto { get; set; }


        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrEmpty(Name))
            {
                validation.Add(FormBuilderValidationMessages.NameRequired);
            }

            if (string.IsNullOrEmpty(Description))
            {
                validation.Add(FormBuilderValidationMessages.DescriptionRequired);
            }

            return validation;
        }
    }
}
