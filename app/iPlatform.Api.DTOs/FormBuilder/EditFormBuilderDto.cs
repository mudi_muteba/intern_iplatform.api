﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.FormBuilder
{
    public class EditFormBuilderDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public EditFormBuilderDto()
        {
            FormBuilderDetails = new List<EditFormBuilderDetailDto>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public IList<EditFormBuilderDetailDto> FormBuilderDetails { get; set; }

        public DtoContext Context { get; private set; }

        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrEmpty(Name))
            {
                validation.Add(FormBuilderValidationMessages.NameRequired);
            }

            if (string.IsNullOrEmpty(Description))
            {
                validation.Add(FormBuilderValidationMessages.DescriptionRequired);
            }

            return validation;
        }
    }
}
