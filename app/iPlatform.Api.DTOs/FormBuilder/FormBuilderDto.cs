﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.FormBuilder
{
    public class FormBuilderDto : Resource
    {
        public FormBuilderDto()
        {
            FormBuilderDetails = new List<FormBuilderDetailDto>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public  IList<FormBuilderDetailDto> FormBuilderDetails { get; set; }
    }
}
