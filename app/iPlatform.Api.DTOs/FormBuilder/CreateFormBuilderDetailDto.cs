﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;
using ValidationMessages;

namespace iPlatform.Api.DTOs.FormBuilder
{
    public class CreateFormBuilderDetailDto : AttributeValidationDto, IExecutionDto
    {
        public CreateFormBuilderDetailDto()
        {
        }

        public int FormBuilderId { get; set; }
        public double Version { get; set; }
        public FormType FormType { get; set; }
        public FormStatus FormStatus { get; set; }
        public string HtmlForm { get; set; }
        public string ChangeComment { get; set; }


        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (FormBuilderId <= 0)
            {
                validation.Add(FormBuilderDetailValidationMessages.FormBuilderIdRequired);
            }

            if (string.IsNullOrEmpty(HtmlForm))
            {
                validation.Add(FormBuilderDetailValidationMessages.HtmlFormRequired);
            }

            if (string.IsNullOrEmpty(ChangeComment))
            {
                validation.Add(FormBuilderDetailValidationMessages.CommentRequired);
            }

            return validation;
        }
    }
}
