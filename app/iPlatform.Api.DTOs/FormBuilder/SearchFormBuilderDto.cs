﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.FormBuilder
{
    public class SearchFormBuilderDto : BaseCriteria, IValidationAvailableDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int FormStatusId { get; set; }
        public int FormTypeId { get; set; }

        public SearchFormBuilderDto()
        {
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}