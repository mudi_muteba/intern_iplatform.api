﻿using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.FormBuilder
{
    public class FormBuilderDetailDto: Resource
    {
        public FormBuilderDetailDto()
        {
        }

        public double Version { get; set; }
        public FormType FormType { get; set; }
        public FormStatus FormStatus { get; set; }
        public string HtmlForm { get; set; }
        public string ChangeComment { get; set; }
    }
}
