﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.JsonDataStores
{
    /// <summary>
    /// A class which houses the direct representation of the DigitalJsonStorage table
    /// </summary>
    public class JsonDataStoreDto : Resource, IAuditableDto, ICultureAware
    {
        public List<AuditEventDto> Events { get; set; }

        /// <summary>
        /// A client unique identifier provided by the client
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// A unique identifier which associates this record with the client channel
        /// </summary>
        public int ChannelId { get; set; }

        /// <summary>
        /// An id used to link record with a party
        /// </summary>
        public int PartyId { get; set; }

        /// <summary>
        /// A property which may be used to store information about this record's section
        /// </summary>
        public string Section { get; set; }

        /// <summary>
        /// A property which may be used to save a numerical step of a section
        /// </summary>
        public int SavePoint { get; set; }

        /// <summary>
        /// A property used to contain the saved JSON object of the Section's SavePoint's data
        /// </summary>
        public string Store { get; set; }

        /// <summary>
        /// A property used to store the date time of when this record was saved
        /// </summary>
        public DateTime TransactionDate { get; set; }

        public JsonDataStoreDto()
        {
            Events = new List<AuditEventDto>();
        }
    }
}
