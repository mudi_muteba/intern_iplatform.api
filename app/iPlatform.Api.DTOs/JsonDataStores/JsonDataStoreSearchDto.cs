﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.JsonDataStore;

namespace iPlatform.Api.DTOs.JsonDataStores
{
    public class JsonDataStoreSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public int PartyId { get; set; }
        public bool GetLatestByPartyId { get; set; }
        public string SessionId { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (GetLatestByPartyId == true && PartyId == 0)
            {
                validation.Add(JsonDataStoreValidationMessages.InvalidGetLatestByPartyIdCriteria);
            }

            if (!GetLatestByPartyId)
            {
                if (string.IsNullOrEmpty(SessionId))
                {
                    validation.Add(JsonDataStoreValidationMessages.InvalidSearchCriteria);
                }
            }

            return validation;
        }
    }
}
