﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.JsonDataStore;

namespace iPlatform.Api.DTOs.JsonDataStores
{
    public class CreateJsonDataStoreDto : AttributeValidationDto, IChannelAwareDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }
  
        public int ChannelId { get; set; }

        public CreateJsonDataStoreDto()
        {
            Context = DtoContext.NoContext();
        }

        public string SessionId { get; set; }
        public int PartyId { get; set; }
        public string Section { get; set; }
        public int SavePoint { get; set; }
        public string Store { get; set; }
        public DateTime? TransactionDate { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(SessionId))
            {
                validation.Add(JsonDataStoreValidationMessages.SessionIdRequired);
            }

            if (string.IsNullOrWhiteSpace(Convert.ToString(PartyId)))
            {
                validation.Add(JsonDataStoreValidationMessages.PartyIdRequired);
            }

            if (string.IsNullOrWhiteSpace(Section))
            {
                validation.Add(JsonDataStoreValidationMessages.SectionRequired);
            }

            if (string.IsNullOrWhiteSpace(Convert.ToString(SavePoint)))
            {
                validation.Add(JsonDataStoreValidationMessages.SavePointRequired);
            }

            if (SavePoint < 1)
            {
                validation.Add(JsonDataStoreValidationMessages.InvalidSavePoint);
            }

            if (TransactionDate.HasValue == false)
            {
                validation.Add(JsonDataStoreValidationMessages.TransactionDate);
            }

            return validation;
        }
    }
}
