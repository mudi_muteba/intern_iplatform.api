﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Party.ContactDetail;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Organisations
{
    public class OrganisationDto : Resource, ICultureAware
    {
        public OrganisationDto()
        {
            Addresses = new List<AddressDto>();
        }

        public string Name { get; set; }
        public virtual int PartyId { get; set; }
        public virtual int PartyTypeId { get; set; }
        public virtual string Code { get; set; }
        public virtual string RegisteredName { get; set; }
        public virtual string TradingName { get; set; }
        public virtual DateTimeDto TradingSince { get; set; }
        public virtual string Description { get; set; }
        public virtual string RegNo { get; set; }
        public virtual string FspNo { get; set; }
        public virtual string VatNo { get; set; }
        public virtual bool TemporaryFsp { get; set; }
        public virtual string ProfessionalIndemnityInsurer { get; set; }
        public virtual string PiCoverPolicyNo { get; set; }
        public virtual string TypeOfAgency { get; set; }
        public virtual string FgPolicyNo { get; set; }
        public virtual string FidelityGuaranteeInsurer { get; set; }
        public virtual int OrganizationTypeId { get; set; }
        public virtual bool ShortTermInsurers { get; set; }
        public virtual bool LongTermInsurers { get; set; }
        public virtual string QuoteEMailBody { get; set; }
        public virtual bool CaptureLeadTransferComment { get; set; }
        public List<AddressDto> Addresses { get; set; }
        public ContactDetailDto ContactDetail { get; set; }
    }
}
