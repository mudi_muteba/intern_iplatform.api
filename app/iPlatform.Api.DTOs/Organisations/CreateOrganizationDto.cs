﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using MasterData;
using iPlatform.Api.DTOs.Party.ContactDetail;

namespace iPlatform.Api.DTOs.Organisations
{
    public class CreateOrganizationDto : AttributeValidationDto, IExecutionDto
    {
        public CreateOrganizationDto()
        {

        }

        private string DisplayName { get { return TradingName; } }
        public string Code { get; set; }
        public string TradingName { get; set; }
        public DateTime? TradingSince { get; set; }
        public string RegisteredName { get; set; }
        public string Description { get; set; }
        public string RegNo { get; set; }
        public string FspNo { get; set; }
        public string VatNo { get; set; }
        public bool TemporaryFsp { get; set; }
        public string ProfessionalIndemnityInsurer { get; set; }
        public string PiCoverPolicyNo { get; set; }
        public string TypeOfAgency { get; set; }
        public string FgPolicyNo { get; set; }
        public string FidelityGuaranteeInsurer { get; set; }
        public OrganizationType OrganizationType { get; set; }
        public bool ShortTermInsurers { get; set; }
        public bool LongTermInsurers { get; set; }
        public string QuoteEMailBody { get; set; }
        public bool CaptureLeadTransferComment { get; set; }
        public CreateContactDetailDto ContactDetail { get; set; }
        public int ChannelId { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
