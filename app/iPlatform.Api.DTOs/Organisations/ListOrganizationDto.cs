﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.ContactDetail;

namespace iPlatform.Api.DTOs.Organisations
{
    public class ListOrganizationDto : Resource, ICultureAware
    {
        public ListOrganizationDto()
        {
        }
        public string Name { get; set; }
        public int PartyId { get; set; }
        public int PartyTypeId { get; set; }
        public string Code { get; set; }
        public string RegisteredName { get; set; }
        public string TradingName { get; set; }
        public DateTimeDto TradingSince { get; set; }
        public string Description { get; set; }
        public string RegNo { get; set; }
        public string FspNo { get; set; }
        public string VatNo { get; set; }
        public bool TemporaryFsp { get; set; }
        public string ProfessionalIndemnityInsurer { get; set; }
        public string PiCoverPolicyNo { get; set; }
        public string TypeOfAgency { get; set; }
        public string FgPolicyNo { get; set; }
        public string FidelityGuaranteeInsurer { get; set; }
        public int OrganizationTypeId { get; set; }
        public bool ShortTermInsurers { get; set; }
        public bool LongTermInsurers { get; set; }
        public string QuoteEMailBody { get; set; }
        public bool CaptureLeadTransferComment { get; set; }
        public ContactDetailDto ContactDetail { get; set; }

    }
}