﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.Quotes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Proposals
{
    public class ProposalQuotePolicyDto: ICultureAware
    {
        public ProposalQuotePolicyDto()
        {
            Items = new List<ProposalQuotePolicyItemDto>();
            State = new QuoteStateDto();
            UploadLog = new QuoteUploadLogDto();
        }

        public List<ProposalQuotePolicyItemDto> Items { get; set; }
        public QuoteStateDto State { get; set; }
        public ProposalQuoteProductDto Product { get; set; }
        public QuoteUploadLogDto UploadLog { get; set; }
        public string InsurerReference { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsPartialQuote { get; set; }
        public MoneyDto Fees { get; set; }
        public int QuoteId { get; set; }

        public bool Successfull
        {
            get { return Items.Count > 0 && Items.All(x => x.Successfull); }
        }
    }
}
