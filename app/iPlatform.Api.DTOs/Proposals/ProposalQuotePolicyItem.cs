﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Products;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Proposals
{
    public class ProposalQuotePolicyItemDto: ICultureAware
    {
        public ProposalQuotePolicyItemDto()
        {
            Benefits = new List<ProductBenefitDto>();
            State = new QuoteItemStateDto();
            BreakDown = new List<QuoteItemBreakDownDto>();
            AdditionalExcess = new List<ProductAdditionalExcessDto>();
            Clauses = new List<string>();
        }

        public List<ProductAdditionalExcessDto> AdditionalExcess { get; set; }
        public List<QuoteItemBreakDownDto> BreakDown { get; set; }
        public Cover Cover { get; set; }
        public CoverDefinitionDto CoverDefinition { get; set; }
        public QuoteItemStateDto State { get; set; }
        public string Description { get; set; }
        public string SumInsuredDescription { get; set; }
        public MoneyDto SumInsured { get; set; }

        public MoneyDto OriginalPremium { get; set; }
        public MoneyDto Premium { get; set; }
        public MoneyDto DiscountedPremium { get; set; }
        public MoneyDto DiscountedPercentage { get; set; }

        public MoneyDto Sasria { get; set; }
        public MoneyDto CalculatedSasria { get; set; }
        public MoneyDto SasriaShortfall { get; set; }

        public string FailureReason { get; set; }
        public string AssetDescription { get; set; }
        public int AssetNumber { get; set; }

        public QuoteItemExcessDto Excess { get; set; }
        public MoneyDto VoluntaryExcess { get; set; }
        public bool Successfull
        {
            // TODO: Removed until CRS / Webgate can differentiate between failures & warnings
            //get { return Premium > 0 && string.IsNullOrWhiteSpace(FailureReason); }
            get { return Premium.Value > 0; }
        }

        public IList<ProductBenefitDto> Benefits { get; set; }
        public List<ProductAdditionalExcessDto> AddtionalExcess { get; private set; }

        public decimal MaxAllowedDiscount { get; set; }
        public decimal MaxAllowedLoad { get; set; }
        public decimal QuoteMaxDiscount { get; set; }
        public decimal ItemMaxDiscount { get; set; }
        public bool HasPolicyBindingConfiguration { get; set; }
        public List<string> Clauses { get; set; }
    }
}
