﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Proposals
{
    public class ProposalQuoteLeadDto
    {
        public ProposalQuoteLeadDto() { }
        public string LeadName { get; set; }
        public int LeadId { get; set; }
        public int PartyId { get; set; }
    }
}
