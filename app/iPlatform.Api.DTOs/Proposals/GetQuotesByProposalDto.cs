﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Proposals
{
    public class GetQuotesByProposalDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public GetQuotesByProposalDto()
        {
            Context = DtoContext.NoContext();
            ReQuote = false;
        }

        public int Id { get; set; }
        public string Source { get; set; }
        public string CmpidSource { get; set; }
        public bool ReQuote { get; set; }

        public int ChannelId { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id <= 0)
                validation.Add(ProposalValidationMessages.ProposalIdRequired);

            return validation;
        }
    }
}
