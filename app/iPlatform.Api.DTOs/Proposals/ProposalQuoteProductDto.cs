﻿namespace iPlatform.Api.DTOs.Proposals
{
    public class ProposalQuoteProductDto
    {
        public string InsurerName { get; set; }

        public string InsurerCode { get; set; }

        public bool CaptureLeadTransferComment { get; set; }

        public string Description { get; set; }

        public string ProductName { get; set; }

        public string ProductCode { get; set; }

        public bool IsSelectedExcess { get; set; }

        public bool IsVoluntaryExcess { get; set; }

        public bool BenefitAcceptanceCheck { get; set; }

        public bool HideSasriaLine { get; set; }

        public bool HideCompareButton { get; set; }
    }
}
