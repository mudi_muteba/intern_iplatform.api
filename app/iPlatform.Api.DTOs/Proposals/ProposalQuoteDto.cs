﻿using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Party.Quotes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Proposals
{
    public class ProposalQuoteDto : ICultureAware
    {
        public ProposalQuoteDto()
        {
            Policies = new List<ProposalQuotePolicyDto>();
            State = new QuoteHeaderStateDto();
            Lead = new ProposalQuoteLeadDto();
        }

        public ProposalHeaderDto ProposalHeader { get; set; }
        public List<ProposalQuotePolicyDto> Policies { get; set; }
        public QuoteHeaderStateDto State { get; set; }
        public ProposalQuoteLeadDto Lead { get; set; }
        public bool IsPartialQuote { get; set; }
        public bool IsAccepted
        {
            get { return Policies.Any(x => x.UploadLog != null); }
        }

    }
}
