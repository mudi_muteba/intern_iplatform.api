﻿using iPlatform.Api.DTOs.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.DataView.DataViewLeadImport
{
    public class SearchDataViewLeadImportDto
    {
        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public ChannelDto Channel { get; set; }
    }
}
