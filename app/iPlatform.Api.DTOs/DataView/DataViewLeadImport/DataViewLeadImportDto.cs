﻿using iPlatform.Api.DTOs.Base.Culture;
using System;

namespace iPlatform.Api.DTOs.DataView.DataViewLeadImport
{
    public class DataViewLeadImportDto : ICultureAware
    {
        public string LeadSystem { get; set; }

        public string Channel { get; set; }

        public string Campaign { get; set; }

        public string LeadSource { get; set; }

        public string LeadPassedBy { get; set; }

        public string AgentName { get; set; }

        public DateTime? DateLeadImported { get; set; }

        public DateTime? DateOfSale { get; set; }

        public int LeadReference { get; set; }

        public string Product { get; set; }

        public string PolicyNo { get; set; }

        public string Client { get; set; }

        public string IDNumber { get; set; }

        public string CellNumber { get; set; }

        public double SoldPolicyPremium { get; set; }

        public string Asset { get; set; }

        public string LeadType { get; set; }

        public bool Quoted { get; set; }

        public bool Sale { get; set; }
    }
}
