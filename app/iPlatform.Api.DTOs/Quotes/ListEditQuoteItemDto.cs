﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Quotes
{
    public class ListEditQuoteItemDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public int Id { get; set; }

        public DtoContext Context { get; private set; }

        public int UserId { get; set; }

        public List<EditQuoteItemDto> QuoteItems { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        #region [ Validation ]

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            List<ValidationErrorMessage> errorMessages = new List<ValidationErrorMessage>();
            if (UserId == 0)
            {
                errorMessages.Add(QuoteItemValidationMessage.UserIdRequired);
            }
            foreach (EditQuoteItemDto editQuoteItemDto in QuoteItems)
            {
                if (editQuoteItemDto.QuoteId == 0)
                {
                    errorMessages.Add(QuoteItemValidationMessage.QuoteIdRequired);
                }
                if (editQuoteItemDto.AssetNo == 0)
                {
                    errorMessages.Add(QuoteItemValidationMessage.AssetNoRequired);
                }
                if (editQuoteItemDto.CoverDefinitionId == 0)
                {
                    errorMessages.Add(QuoteItemValidationMessage.CoverDefinitionIdRequired);
                }
            }

            return errorMessages;
        }

        #endregion
    }
}
