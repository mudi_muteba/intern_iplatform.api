﻿namespace iPlatform.Api.DTOs.Quotes
{
    public class QuoteDiscountResultDto
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}