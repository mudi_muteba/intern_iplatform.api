﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Quotes
{
    public class IntentToBuyQuoteDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public IntentToBuyQuoteDto()
        {
            Context = DtoContext.NoContext();
        }

        public int Id { get; set; }
        public string CmpidSource { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id <= 0)
                validation.Add(QuoteValidationMessages.QuoteIdRequired);

            return validation;
        }
    }
}
