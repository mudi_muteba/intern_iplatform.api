﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Quotes
{
    public class EditQuoteItemDto : Resource, ICultureAware
    {
        public int QuoteId { get; set; }

        public int AssetNo { get; set; }

        public decimal Percentage { get; set; }

        public int CoverDefinitionId { get; set; }
    }
}
