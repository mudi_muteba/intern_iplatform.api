﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SecondLevel.Questions
{
    public class SecondLevelQuestionGroupDto : Resource
    {
        public SecondLevelQuestionGroupDto()
        {
            
        }
        public SecondLevelQuestionGroupDto(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public string Name { get; set; }
    }
}