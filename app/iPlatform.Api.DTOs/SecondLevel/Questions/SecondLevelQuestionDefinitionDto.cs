﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.SecondLevel.Questions
{
    public class SecondLevelQuestionDefinitionDto : Resource
    {
        public CoverDefinitionDto CoverDefinition { get; set; }
        public SecondLevelQuestionDto SecondLevelQuestion { get; set; }
        public string DisplayName { get; set; }
        public string ToolTip { get; set; }
    }
}
