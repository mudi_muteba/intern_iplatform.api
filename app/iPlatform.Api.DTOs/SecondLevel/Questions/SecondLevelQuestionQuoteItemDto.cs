﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.SecondLevel.Questions
{
    public class SecondLevelQuestionQuoteItemDto : Resource
    {
        public SecondLevelQuestionQuoteItemDto()
        {
            Questions = new List<SecondLevelQuestionDto>();
        }

        public string Description { get; set; }
        public int QuoteItemId { get; set; }
        public decimal SumInsured { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Cover Cover { get; set; }
        public List<SecondLevelQuestionDto> Questions { get; set; }
    }
}
