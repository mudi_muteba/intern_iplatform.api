﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SecondLevel.Questions
{
    public class SecondLevelQuestionsForQuoteDto : Resource
    {
        public SecondLevelQuestionsForQuoteDto()
        {
            Questions = new List<SecondLevelQuestionDto>();
            QuoteItems = new List<SecondLevelQuestionQuoteItemDto>();
        }

        public List<SecondLevelQuestionDto> Questions { get; set; }
        public List<SecondLevelQuestionQuoteItemDto> QuoteItems { get; set; }
    }
}