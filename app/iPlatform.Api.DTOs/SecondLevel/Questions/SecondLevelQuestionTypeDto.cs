﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SecondLevel.Questions
{
    public class SecondLevelQuestionTypeDto : Resource
    {
        public SecondLevelQuestionTypeDto()
        {
        }

        public SecondLevelQuestionTypeDto(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public string Name { get; set; }
    }
}
