﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.SecondLevel.Answers;

namespace iPlatform.Api.DTOs.SecondLevel.Questions
{
    public class SecondLevelQuestionDto : Resource
    {
     
        public SecondLevelQuestionDto()
        {
            PossibleAnswers = new List<SecondLevelQuestionAnswerDto>();
        }

        public SecondLevelQuestionDto(int id, string name, SecondLevelQuestionTypeDto type, SecondLevelQuestionGroupDto group, List<SecondLevelQuestionAnswerDto> possibleAnswers = null)
        {
            Id = id;
            Name = name;
            Type = type;
            Group = group;
            PossibleAnswers = possibleAnswers ?? new List<SecondLevelQuestionAnswerDto>();
        }

        public void AddAnswer(string answer)
        {
            Answer = answer;
        }

        public SecondLevelQuestionTypeDto Type { get; set; }
        public SecondLevelQuestionGroupDto Group { get; set; }
        public List<SecondLevelQuestionAnswerDto> PossibleAnswers { get; set; }

        public string Name { get; set; }
        public string Answer { get; set; }
        public string ToolTip { get; set; }
    }
}
