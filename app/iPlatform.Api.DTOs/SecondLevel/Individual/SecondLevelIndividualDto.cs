﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.SecondLevel.Individual
{
    public class SecondLevelIndividualDto : Resource, IAuditableDto, ICultureAware
    {
        public SecondLevelIndividualDto()
        {
            Events = new List<AuditEventDto>();
        }

        public int IndvidualId { get; set; }
        public bool AnyJudgements { get; set; }
        public string AnyJudgementsReason { get; set; }
        public bool UnderAdministrationOrDebtReview { get; set; }
        public string UnderAdministrationOrDebtReviewReason { get; set; }
        public bool BeenSequestratedOrLiquidated { get; set; }
        public string BeenSequestratedOrLiquidatedReason { get; set; }

        public List<AuditEventDto> Events { get; private set; }
    }
}