﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.SecondLevel.Individual;

namespace iPlatform.Api.DTOs.SecondLevel.Quotes
{
    public class SecondLevelQuoteDto : IExecutionDto, ICultureAware //IAuditableDto
    {
        public SecondLevelQuoteDto()
        {
            Items = new List<QuoteItemDto>();
            Individual = new SecondLevelIndividualDto();
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public void SetIndividual(SecondLevelIndividualDto individual)
        {
            Individual = individual;
        }

        public List<QuoteItemDto> Items { get; set; }
        public string ProductCode { get; set; }
        public int ProductId { get; set; }
        public int PartyId { get; set; }
        public int QuoteId { get; set; }
        public SecondLevelIndividualDto Individual { get; set; }
        public DtoContext Context { get; private set; }
    }
}
