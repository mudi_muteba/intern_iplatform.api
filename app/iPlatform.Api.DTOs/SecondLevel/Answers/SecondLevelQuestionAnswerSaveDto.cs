﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SecondLevel.Answers
{
    public class SecondLevelQuestionAnswerSaveDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public int QuoteItemId { get; set; }
        public string Answer { get; set; }
        public int SecondLevelQuestionId { get; set; }
        public DtoContext Context { get; private set; }
    }
}
