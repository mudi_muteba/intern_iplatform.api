﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SecondLevel.Answers
{
    public class SecondLevelQuestionSavedAnswerDto : Resource, IExecutionDto, IAffectExistingEntity
    {

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
        public DtoContext Context { get; private set; }
    }
}
