﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SecondLevel.Answers
{
    public class SecondLevelQuestionsAnswersSaveDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public SecondLevelQuestionsAnswersSaveDto()
        {
            Items = new List<SecondLevelQuestionAnswerSaveDto>();
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public int QuoteId { get; set; }
        public List<SecondLevelQuestionAnswerSaveDto> Items { get; set; }
        public DtoContext Context { get; private set; }
    }
}
