﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SecondLevel.Answers
{
    public class SecondLevelQuestionAnswerDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public SecondLevelQuestionAnswerDto()
        {
            
        }
        public SecondLevelQuestionAnswerDto(int id, string name, string answer)
        {
            Id = id;
            Name = name;
            Answer = answer;
        }

        public string Name { get; set; }
        public string Answer { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }
}
