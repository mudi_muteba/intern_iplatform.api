﻿namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class SmartGetPdfResponseDto : BasicSmartPolicyResponseHeaderDto
    {
        public string PolicyNumber { get; set; }
        public string PdfUrl { get; set; }
    }
}