﻿namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class SmartDataDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string TelephoneNo { get; set; }
        public string TelephoneWorkNo { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string PolicyStartDate { get; set; }
        public string CarDeliveryDate { get; set; }
        public string CarRegistrationNumber { get; set; }
        public string CarModel { get; set; }
        public string CarManufacturer { get; set; }
        public string CarYearOfManufacture { get; set; }
        public string CarColour { get; set; }
        public string CarVinNumber { get; set; }
        public string PreDamagedOrNot { get; set; }
        public string PreDamagedText { get; set; }
        public string DurationInMonths { get; set; }
        public string PaymentDurationInMonths { get; set; }
        public string CarCondition { get; set; }
        public string CarPayment { get; set; }
        public string PaymentAmount { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string SortCode { get; set; }
        public decimal Price { get; set; }
        public string Status { get; set; }
        public string BodyType { get; set; }
        public string FirstReg { get; set; }
        public string CancelDate { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
    }
}