﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class GetSmartPolicyDto : BasicSmartPolicyHeaderDto, IExecutionDto
    {
        public string PolicyNumber { get; set; }
        public void SetContext(DtoContext context)
        {
            this.Context = context;
        }

        public DtoContext Context { get; set; }
    }
}
