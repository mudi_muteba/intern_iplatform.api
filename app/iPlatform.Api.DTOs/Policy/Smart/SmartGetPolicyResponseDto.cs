﻿namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class SmartGetPolicyResponseDto : BasicSmartPolicyResponseHeaderDto
    {
        public SmartDataDto Policy { get; set; }
    }
}