﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class SendSmartPdfPoliciesDto : BasicSmartPolicyHeaderDto, IExecutionDto
    {
        public List<string> PolicyNumber { get; set; }
        public void SetContext(DtoContext context)
        {
            this.Context = context;
        }

        public DtoContext Context { get; set; }
    }
}
