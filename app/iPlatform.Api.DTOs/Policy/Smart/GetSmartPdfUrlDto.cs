﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class GetSmartPdfUrlDto : BasicSmartPolicyHeaderDto, IExecutionDto
    {
        public string PolicyNumber { get; set; }
        public void SetContext(DtoContext context)
        {
            this.Context = context;
        }

        public DtoContext Context { get; set; }
        public string PathMap { get; set; }
    }
}
