﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class GetSmartSendPdfPolicyDto : BasicSmartPolicyHeaderDto
    {
        public List<string> Policies { get; set; }
    }
}
