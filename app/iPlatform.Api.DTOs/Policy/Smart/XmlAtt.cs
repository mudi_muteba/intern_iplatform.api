﻿using System;

namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class XmlAtt : Attribute
    {
        public XmlAtt(string Namespace = null, string ElementName = null)
        {
            this.Namespace = Namespace;
            this.ElementName = ElementName;
        }

        public string Namespace { get; set; }
        public string ElementName { get; set; }

    }
}