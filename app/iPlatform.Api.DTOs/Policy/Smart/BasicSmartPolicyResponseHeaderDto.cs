﻿namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class BasicSmartPolicyResponseHeaderDto
    {
        public string SuccessOrFail { get; set; }
        public string ErrorMessages { get; set; }
    }
}
