﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class UpdateSmartPolicyDto : SmartDataDto, IExecutionDto
    {
        public string PolicyNumber { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

    }
}