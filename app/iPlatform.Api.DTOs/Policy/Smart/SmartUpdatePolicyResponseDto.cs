﻿namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class SmartUpdatePolicyResponseDto : BasicSmartPolicyResponseHeaderDto
    {
        public string PolicyNumber { get; set; }
       
    }
}