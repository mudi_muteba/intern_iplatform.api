﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class RegisterSmartPolicyDto : SmartDataDto, IExecutionDto
    {
       
        public string PolicyAlternativeNumber { get; set; }
        public string EmailPDF { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}