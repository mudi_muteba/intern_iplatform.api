﻿namespace iPlatform.Api.DTOs.Policy.Smart
{
    public class BasicSmartPolicyHeaderDto 
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
