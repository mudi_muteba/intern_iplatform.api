﻿using System.Xml.Serialization;
using iPlatform.Api.DTOs.Policy.Smart;

[XmlRoot(ElementName = "result")]
public class GetPdfUrlResponseResult
{
    [XmlAtt(ElementName = "SuccessOrFail", Namespace = "https://api.smartinsurance.com/api/")]
    public string SuccessOrFail { get; set; }
    [XmlAtt(ElementName = "ErrorMessages", Namespace = "https://api.smartinsurance.com/api/")]
    public string ErrorMessages { get; set; }
    [XmlAtt(ElementName = "PolicyNumber", Namespace = "https://api.smartinsurance.com/api/")]
    public string PolicyNumber { get; set; }
    [XmlAtt(ElementName = "PdfUrl", Namespace = "https://api.smartinsurance.com/api/")]
    public string PdfUrl { get; set; }
}

[XmlRoot(ElementName = "GetPolicyResponse", Namespace = "https://api.smartinsurance.com/api/")]
public class GetPdfUrlResponseDto
{
    public GetPdfUrlResponseDto()
    {
        Result = new GetPdfUrlResponseResult();
    }
    [XmlAtt(ElementName = "result", Namespace = "")]
    public GetPdfUrlResponseResult Result { get; set; }
}

[XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
public class GetPdfUrlResponseResponseBody
{
    public GetPdfUrlResponseResponseBody()
    {
        GetPDFUrlResponse = new GetPdfUrlResponseDto();
    }
    [XmlAtt(ElementName = "GetPDFUrlResponse", Namespace = "https://api.smartinsurance.com/api/")]
    public GetPdfUrlResponseDto GetPDFUrlResponse { get; set; }
}

[XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
public class GetPdfUrlResponseEnvelope
{
    public GetPdfUrlResponseEnvelope()
    {
        Body = new GetPdfUrlResponseResponseBody();
    }

    [XmlAtt(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public GetPdfUrlResponseResponseBody Body { get; set; }
}