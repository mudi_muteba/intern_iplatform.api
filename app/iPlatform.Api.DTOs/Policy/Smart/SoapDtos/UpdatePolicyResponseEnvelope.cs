﻿using System.Xml.Serialization;
using iPlatform.Api.DTOs.Policy.Smart;

namespace iPlatform.Api.DTOs.Policy.Smart.SoapDtos
{

    [XmlRoot(ElementName = "result")]
    public class UpdatePolicyResponseResult
    {
        [XmlAtt(ElementName = "SuccessOrFail", Namespace = "https://api.smartinsurance.com/api/")]
        public string SuccessOrFail { get; set; }
        [XmlAtt(ElementName = "ErrorMessages", Namespace = "https://api.smartinsurance.com/api/")]
        public string ErrorMessages { get; set; }
        [XmlAtt(ElementName = "PolicyNumber", Namespace = "https://api.smartinsurance.com/api/")]
        public string PolicyNumber { get; set; }
    }

    [XmlRoot(ElementName = "UpdatePolicyResponseDto", Namespace = "https://api.smartinsurance.com/api/")]
    public class UpdatePolicyResponseDto
    {
        public UpdatePolicyResponseDto()
        {
            Result = new UpdatePolicyResponseResult();
        }
        [XmlAtt(ElementName = "result", Namespace = "")]
        public UpdatePolicyResponseResult Result { get; set; }
    }


    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class UpdatePolicyResponseBody
    {
        public UpdatePolicyResponseBody()
        {
            UpdatePolicyResponse = new UpdatePolicyResponseDto();
        }

        [XmlAtt(ElementName = "UpdatePolicyResponse", Namespace = "https://api.smartinsurance.com/api/")]
        public UpdatePolicyResponseDto UpdatePolicyResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class UpdatePolicyResponseEnvelope
    {
        public UpdatePolicyResponseEnvelope()
        {
            Body = new UpdatePolicyResponseBody();
        }

        [XmlAtt(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public UpdatePolicyResponseBody Body { get; set; }
    }
}