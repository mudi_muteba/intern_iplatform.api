﻿using System.Xml.Serialization;
using iPlatform.Api.DTOs.Policy.Smart;

namespace iPlatform.Api.DTOs.Policy.Smart.SoapDtos
{
    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class RegisterPolicyResponseBody
    {
        public RegisterPolicyResponseBody()
        {
            RegisterPolicyResponse = new GetPdfUrlResponseDto();
        }

        [XmlAtt(ElementName = "RegisterPolicyResponse", Namespace = "https://api.smartinsurance.com/api/")]
        public GetPdfUrlResponseDto RegisterPolicyResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class RegisterPolicyResponseEnvelope
    {
        public RegisterPolicyResponseEnvelope()
        {
            Body = new RegisterPolicyResponseBody();
        }

        [XmlAtt(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public RegisterPolicyResponseBody Body { get; set; }
    }
}