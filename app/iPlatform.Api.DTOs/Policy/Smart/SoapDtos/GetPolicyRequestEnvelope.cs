﻿using System.Xml.Serialization;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Policy.Smart.SoapDtos
{
    /* 
  Licensed under the Apache License, Version 2.0

  http://www.apache.org/licenses/LICENSE-2.0
  */

    [XmlRoot(ElementName = "data")]
    public class GetPolicyRequestData
    {
        [XmlElement(ElementName = "Username", Namespace = "https://api.smartinsurance.com/api/")]
        public string Username { get; set; }
        [XmlElement(ElementName = "Password", Namespace = "https://api.smartinsurance.com/api/")]
        public string Password { get; set; }
        [XmlElement(ElementName = "PolicyNumber", Namespace = "https://api.smartinsurance.com/api/")]
        public string PolicyNumber { get; set; }
    }

    [XmlRoot(ElementName = "GetPolicy", Namespace = "https://api.smartinsurance.com/api/")]
    public class GetPolicyRequestDto
    {
        [XmlElement(ElementName = "data", Namespace = "")]
        public GetPolicyRequestData Data { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class GetPolicyRequestBody
    {
        [XmlElement(ElementName = "GetPolicy", Namespace = "https://api.smartinsurance.com/api/")]
        public GetPolicyRequestDto GetPolicy { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]

    public class GetPolicyRequestEnvelope : IExecutionDto
    {
        [XmlElement(ElementName = "Header", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public string Header { get; set; }
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public GetPolicyRequestBody Body { get; set; }
        [XmlAttribute(AttributeName = "soapenv", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soapenv { get; set; }
        [XmlAttribute(AttributeName = "api", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Api { get; set; }

        public void SetContext(DtoContext context)
        {
            this.Context = context;
        }

        public DtoContext Context { get; set; }
    }



}