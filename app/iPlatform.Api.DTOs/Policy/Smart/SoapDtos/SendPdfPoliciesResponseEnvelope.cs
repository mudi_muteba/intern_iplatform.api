﻿using System.Xml.Serialization;
using iPlatform.Api.DTOs.Policy.Smart;

namespace iPlatform.Api.DTOs.Policy.Smart.SoapDtos
{

    [XmlRoot(ElementName = "result")]
    public class SendPdfPoliciesResponseResult
    {
        [XmlAtt(ElementName = "SuccessOrFail", Namespace = "https://api.smartinsurance.com/api/")]
        public string SuccessOrFail { get; set; }
        [XmlAtt(ElementName = "ErrorMessages", Namespace = "https://api.smartinsurance.com/api/")]
        public string ErrorMessages { get; set; }
    }

    [XmlRoot(ElementName = "UpdatePolicyResponseDto", Namespace = "https://api.smartinsurance.com/api/")]
    public class SendPdfPoliciesResponseDto
    {
        public SendPdfPoliciesResponseDto()
        {
            Result = new SendPdfPoliciesResponseResult();
        }
        [XmlAtt(ElementName = "result", Namespace = "")]
        public SendPdfPoliciesResponseResult Result { get; set; }
    }


    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class SendPdfPoliciesResponseBody
    {
        public SendPdfPoliciesResponseBody()
        {
            SendPdfPolicy = new SendPdfPoliciesResponseDto();
        }

        [XmlAtt(ElementName = "SendPdfPolicy", Namespace = "https://api.smartinsurance.com/api/")]
        public SendPdfPoliciesResponseDto SendPdfPolicy { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class SendPdfPoliciesResponseEnvelope
    {
        public SendPdfPoliciesResponseEnvelope()
        {
            Body = new SendPdfPoliciesResponseBody();
        }

        [XmlAtt(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public SendPdfPoliciesResponseBody Body { get; set; }
    }
}