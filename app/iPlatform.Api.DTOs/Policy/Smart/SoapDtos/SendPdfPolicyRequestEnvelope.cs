﻿using System.Collections.Generic;
using System.Xml.Serialization;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Policy.Smart;

namespace iPlatform.Api.DTOs.Policy.Smart.SoapDtos
{

    [XmlRoot(ElementName = "Policies", Namespace = "https://api.smartinsurance.com/api/")]
    public class Policies
    {
        [XmlElement(ElementName = "PolicyNumber", Namespace = "https://api.smartinsurance.com/api/")]
        public List<string> PolicyNumber { get; set; }
    }

    [XmlRoot(ElementName = "data")]
    public class SendPdfRequestData
    {
        [XmlElement(ElementName = "Username", Namespace = "https://api.smartinsurance.com/api/")]
        public string Username { get; set; }
        [XmlElement(ElementName = "Password", Namespace = "https://api.smartinsurance.com/api/")]
        public string Password { get; set; }
        [XmlElement(ElementName = "Policies", Namespace = "https://api.smartinsurance.com/api/")]
        public Policies Policies { get; set; }
    }

    [XmlRoot(ElementName = "SendPdfPolicy", Namespace = "https://api.smartinsurance.com/api/")]
    public class SendPdfPolicDto
    {
        [XmlElement(ElementName = "data", Namespace = "")]
        public SendPdfRequestData Data { get; set; }
    }


    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class SendPdfPolicyBody
    {
        [XmlElement(ElementName = "SendPdfPolicy", Namespace = "https://api.smartinsurance.com/api/")]
        public SendPdfPolicDto SendPdfPolicy { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class SendPdfPolicyRequestEnvelope : IExecutionDto
    {
        [XmlElement(ElementName = "Header", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public string Header { get; set; }
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public SendPdfPolicyBody Body { get; set; }
        [XmlAttribute(AttributeName = "soapenv", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soapenv { get; set; }
        [XmlAttribute(AttributeName = "api", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Api { get; set; }

        public void SetContext(DtoContext context)
        {
            this.Context = context;
        }

        public DtoContext Context { get; set; }
    }

}