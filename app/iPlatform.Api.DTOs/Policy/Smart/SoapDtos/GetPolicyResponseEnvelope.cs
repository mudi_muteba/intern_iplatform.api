﻿using System.Xml.Serialization;

namespace iPlatform.Api.DTOs.Policy.Smart.SoapDtos
{

    /* 
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */


    [XmlRoot(ElementName = "Policy", Namespace = "https://api.smartinsurance.com/api/")]
    public class GetPolicyResponsePolicy
    {
        [XmlAtt(ElementName = "Title", Namespace = "https://api.smartinsurance.com/api/")]
        public string Title { get; set; }
        [XmlAtt(ElementName = "FirstName", Namespace = "https://api.smartinsurance.com/api/")]
        public string FirstName { get; set; }
        [XmlAtt(ElementName = "LastName", Namespace = "https://api.smartinsurance.com/api/")]
        public string LastName { get; set; }
        [XmlAtt(ElementName = "DateOfBirth", Namespace = "https://api.smartinsurance.com/api/")]
        public string DateOfBirth { get; set; }
        [XmlAtt(ElementName = "TelephoneNo", Namespace = "https://api.smartinsurance.com/api/")]
        public string TelephoneNo { get; set; }
        [XmlAtt(ElementName = "TelephoneWorkNo", Namespace = "https://api.smartinsurance.com/api/")]
        public string TelephoneWorkNo { get; set; }
        [XmlAtt(ElementName = "MobileNo", Namespace = "https://api.smartinsurance.com/api/")]
        public string MobileNo { get; set; }
        [XmlAtt(ElementName = "Email", Namespace = "https://api.smartinsurance.com/api/")]
        public string Email { get; set; }
        [XmlAtt(ElementName = "PolicyStartDate", Namespace = "https://api.smartinsurance.com/api/")]
        public string PolicyStartDate { get; set; }
        [XmlAtt(ElementName = "CarDeliveryDate", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarDeliveryDate { get; set; }
        [XmlAtt(ElementName = "CarRegistrationNumber", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarRegistrationNumber { get; set; }
        [XmlAtt(ElementName = "CarModel", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarModel { get; set; }
        [XmlAtt(ElementName = "CarManufacturer", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarManufacturer { get; set; }
        [XmlAtt(ElementName = "CarYearOfManufacture", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarYearOfManufacture { get; set; }
        [XmlAtt(ElementName = "CarColour", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarColour { get; set; }
        [XmlAtt(ElementName = "CarVinNumber", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarVinNumber { get; set; }
        [XmlAtt(ElementName = "PreDamagedOrNot", Namespace = "https://api.smartinsurance.com/api/")]
        public string PreDamagedOrNot { get; set; }
        [XmlAtt(ElementName = "PreDamagedText", Namespace = "https://api.smartinsurance.com/api/")]
        public string PreDamagedText { get; set; }
        [XmlAtt(ElementName = "DurationInMonths", Namespace = "https://api.smartinsurance.com/api/")]
        public string DurationInMonths { get; set; }
        [XmlAtt(ElementName = "PaymentDurationInMonths", Namespace = "https://api.smartinsurance.com/api/")]
        public string PaymentDurationInMonths { get; set; }
        [XmlAtt(ElementName = "PaymentAmount", Namespace = "https://api.smartinsurance.com/api/")]
        public string PaymentAmount { get; set; }
        [XmlAtt(ElementName = "CarCondition", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarCondition { get; set; }
        [XmlAtt(ElementName = "CarPayment", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarPayment { get; set; }
        [XmlAtt(ElementName = "Address1", Namespace = "https://api.smartinsurance.com/api/")]
        public string Address1 { get; set; }
        [XmlAtt(ElementName = "Address2", Namespace = "https://api.smartinsurance.com/api/")]
        public string Address2 { get; set; }
        [XmlAtt(ElementName = "City", Namespace = "https://api.smartinsurance.com/api/")]
        public string City { get; set; }
        [XmlAtt(ElementName = "County", Namespace = "https://api.smartinsurance.com/api/")]
        public string County { get; set; }
        [XmlAtt(ElementName = "Postcode", Namespace = "https://api.smartinsurance.com/api/")]
        public string Postcode { get; set; }
        [XmlAtt(ElementName = "SortCode", Namespace = "https://api.smartinsurance.com/api/")]
        public string SortCode { get; set; }
        [XmlAtt(ElementName = "Price", Namespace = "https://api.smartinsurance.com/api/")]
        public string Price { get; set; }
        [XmlAtt(ElementName = "Status", Namespace = "https://api.smartinsurance.com/api/")]
        public string Status { get; set; }
        [XmlAtt(ElementName = "BodyType", Namespace = "https://api.smartinsurance.com/api/")]
        public string BodyType { get; set; }
        [XmlAtt(ElementName = "FirstReg", Namespace = "https://api.smartinsurance.com/api/")]
        public string FirstReg { get; set; }
        [XmlAtt(ElementName = "CancelDate", Namespace = "https://api.smartinsurance.com/api/")]
        public string CancelDate { get; set; }
        [XmlAtt(ElementName = "AccountNumber", Namespace = "https://api.smartinsurance.com/api/")]
        public string AccountNumber { get; set; }
        [XmlAtt(ElementName = "AccountName", Namespace = "https://api.smartinsurance.com/api/")]
        public string AccountName { get; set; }
    }

    [XmlRoot(ElementName = "result")]
    public class GetPolicyResponseResult
    {
        [XmlAtt(ElementName = "SuccessOrFail", Namespace = "https://api.smartinsurance.com/api/")]
        public string SuccessOrFail { get; set; }
        [XmlAtt(ElementName = "ErrorMessages", Namespace = "https://api.smartinsurance.com/api/")]
        public string ErrorMessages { get; set; }
        [XmlAtt(ElementName = "Policy", Namespace = "https://api.smartinsurance.com/api/")]
        public GetPolicyResponsePolicy Policy { get; set; }
    }

    [XmlRoot(ElementName = "GetPolicyResponse", Namespace = "https://api.smartinsurance.com/api/")]
    public class GetPolicyResponseDto
    {
        public GetPolicyResponseDto()
        {
            Result = new GetPolicyResponseResult();
        }
        [XmlAtt(ElementName = "result", Namespace = "")]
        public GetPolicyResponseResult Result { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class GetPolicyResponseBody
    {
        public GetPolicyResponseBody()
        {
            GetPolicyResponse = new GetPolicyResponseDto();
        }
        [XmlAtt(ElementName = "GetPolicyResponse", Namespace = "https://api.smartinsurance.com/api/")]
        public GetPolicyResponseDto GetPolicyResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class GetPolicyResponseEnvelope
    {
        public GetPolicyResponseEnvelope()
        {
            Body = new GetPolicyResponseBody();
        }

        [XmlAtt(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public GetPolicyResponseBody Body { get; set; }
    }
}