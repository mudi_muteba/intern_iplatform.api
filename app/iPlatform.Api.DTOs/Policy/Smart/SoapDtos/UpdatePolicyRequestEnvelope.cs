﻿using System.Xml.Serialization;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Policy.Smart.SoapDtos
{
    /* 
  Licensed under the Apache License, Version 2.0

  http://www.apache.org/licenses/LICENSE-2.0
  */

    public class UpdatePolicyData
    {
        [XmlElement(ElementName = "Username", Namespace = "https://api.smartinsurance.com/api/")]
        public string Username { get; set; }
        [XmlElement(ElementName = "Password", Namespace = "https://api.smartinsurance.com/api/")]
        public string Password { get; set; }
        [XmlElement(ElementName = "PolicyNumber", Namespace = "https://api.smartinsurance.com/api/")]
        public string PolicyNumber { get; set; }
        [XmlElement(ElementName = "Title", Namespace = "https://api.smartinsurance.com/api/")]
        public string Title { get; set; }
        [XmlElement(ElementName = "FirstName", Namespace = "https://api.smartinsurance.com/api/")]
        public string FirstName { get; set; }
        [XmlElement(ElementName = "LastName", Namespace = "https://api.smartinsurance.com/api/")]
        public string LastName { get; set; }
        [XmlElement(ElementName = "DateOfBirth", Namespace = "https://api.smartinsurance.com/api/")]
        public string DateOfBirth { get; set; }
        [XmlElement(ElementName = "TelephoneNo", Namespace = "https://api.smartinsurance.com/api/")]
        public string TelephoneNo { get; set; }
        [XmlElement(ElementName = "TelephoneWorkNo", Namespace = "https://api.smartinsurance.com/api/")]
        public string TelephoneWorkNo { get; set; }
        [XmlElement(ElementName = "MobileNo", Namespace = "https://api.smartinsurance.com/api/")]
        public string MobileNo { get; set; }
        [XmlElement(ElementName = "Email", Namespace = "https://api.smartinsurance.com/api/")]
        public string Email { get; set; }
        [XmlElement(ElementName = "PolicyStartDate", Namespace = "https://api.smartinsurance.com/api/")]
        public string PolicyStartDate { get; set; }
        [XmlElement(ElementName = "CarDeliveryDate", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarDeliveryDate { get; set; }
        [XmlElement(ElementName = "CarRegistrationNumber", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarRegistrationNumber { get; set; }
        [XmlElement(ElementName = "CarModel", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarModel { get; set; }
        [XmlElement(ElementName = "CarManufacturer", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarManufacturer { get; set; }
        [XmlElement(ElementName = "CarYearOfManufacture", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarYearOfManufacture { get; set; }
        [XmlElement(ElementName = "CarColour", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarColour { get; set; }
        [XmlElement(ElementName = "CarVinNumber", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarVinNumber { get; set; }
        [XmlElement(ElementName = "PreDamagedOrNot", Namespace = "https://api.smartinsurance.com/api/")]
        public string PreDamagedOrNot { get; set; }
        [XmlElement(ElementName = "PreDamagedText", Namespace = "https://api.smartinsurance.com/api/")]
        public string PreDamagedText { get; set; }
        [XmlElement(ElementName = "DurationInMonths", Namespace = "https://api.smartinsurance.com/api/")]
        public string DurationInMonths { get; set; }
        [XmlElement(ElementName = "PaymentDurationInMonths", Namespace = "https://api.smartinsurance.com/api/")]
        public string PaymentDurationInMonths { get; set; }
        [XmlElement(ElementName = "CarCondition", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarCondition { get; set; }
        [XmlElement(ElementName = "CarPayment", Namespace = "https://api.smartinsurance.com/api/")]
        public string CarPayment { get; set; }
        [XmlElement(ElementName = "Address1", Namespace = "https://api.smartinsurance.com/api/")]
        public string Address1 { get; set; }
        [XmlElement(ElementName = "Address2", Namespace = "https://api.smartinsurance.com/api/")]
        public string Address2 { get; set; }
        [XmlElement(ElementName = "City", Namespace = "https://api.smartinsurance.com/api/")]
        public string City { get; set; }
        [XmlElement(ElementName = "County", Namespace = "https://api.smartinsurance.com/api/")]
        public string County { get; set; }
        [XmlElement(ElementName = "Postcode", Namespace = "https://api.smartinsurance.com/api/")]
        public string Postcode { get; set; }
        [XmlElement(ElementName = "SortCode", Namespace = "https://api.smartinsurance.com/api/")]
        public string SortCode { get; set; }
        [XmlElement(ElementName = "Price", Namespace = "https://api.smartinsurance.com/api/")]
        public string Price { get; set; }
        [XmlElement(ElementName = "Status", Namespace = "https://api.smartinsurance.com/api/")]
        public string Status { get; set; }
        [XmlElement(ElementName = "BodyType", Namespace = "https://api.smartinsurance.com/api/")]
        public string BodyType { get; set; }
        [XmlElement(ElementName = "FirstReg", Namespace = "https://api.smartinsurance.com/api/")]
        public string FirstReg { get; set; }
        [XmlElement(ElementName = "CancelDate", Namespace = "https://api.smartinsurance.com/api/")]
        public string CancelDate { get; set; }
        [XmlElement(ElementName = "AccountNumber", Namespace = "https://api.smartinsurance.com/api/")]
        public string AccountNumber { get; set; }
        [XmlElement(ElementName = "AccountName", Namespace = "https://api.smartinsurance.com/api/")]
        public string AccountName { get; set; }
    }

    [XmlRoot(ElementName = "UpdatePolicy", Namespace = "https://api.smartinsurance.com/api/")]
    public class UpdatePolicyDto
    {
        [XmlElement(ElementName = "data", Namespace = "")]
        public UpdatePolicyData Data { get; set; }
    }


    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class UpdatePolicyBody
    {
        [XmlElement(ElementName = "UpdatePolicy", Namespace = "https://api.smartinsurance.com/api/")]
        public UpdatePolicyDto UpdatePolicy { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class UpdatePolicyRequestEnvelope : IExecutionDto
    {
        [XmlElement(ElementName = "Header", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public string Header { get; set; }
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public UpdatePolicyBody Body { get; set; }
        [XmlAttribute(AttributeName = "soapenv", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soapenv { get; set; }
        [XmlAttribute(AttributeName = "api", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Api { get; set; }

        public void SetContext(DtoContext context)
        {
            this.Context = context;
        }

        public DtoContext Context { get; set; }
    }



}