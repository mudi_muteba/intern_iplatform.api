﻿using System.Xml.Serialization;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Policy.Smart.SoapDtos
{
    /* 
  Licensed under the Apache License, Version 2.0

  http://www.apache.org/licenses/LICENSE-2.0
  */
    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class GetPdfUrlRequestBody
    {
        [XmlElement(ElementName = "GetPDFUrl", Namespace = "https://api.smartinsurance.com/api/")]
        public GetPolicyRequestDto GetPDFUrl { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class GetPdfUrlRequestEnvelope : IExecutionDto
    {
        [XmlElement(ElementName = "Header", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public string Header { get; set; }
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public GetPdfUrlRequestBody Body { get; set; }
        [XmlAttribute(AttributeName = "soapenv", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soapenv { get; set; }
        [XmlAttribute(AttributeName = "api", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Api { get; set; }

        public void SetContext(DtoContext context)
        {
            this.Context = context;
        }

        public DtoContext Context { get; set; }
        public string PathMap { get; set; }
    }



}