﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Policy.CimsSelfService
{
   
    public class SelfServiceGetPolicyInfoItem
    {
        public string CoverType { get; set; }
        public string Id { get; set; }
        public string RiskItemType { get; set; }
        public string ItemDescription1 { get; set; }
        public string ItemDescription2 { get; set; }
        public string ItemDescription3 { get; set; }
        public string ItemDescription4 { get; set; }
        public string ItemDescription5 { get; set; }
        public int SumInsured { get; set; }
        public double RiskPayment { get; set; }
        public string ExcessValue { get; set; }
        public string IsMotor { get; set; }
        public string IsProperty { get; set; }
        public string IsVAP { get; set; }
        public bool ShowPremium { get; set; }
    }

    public class SelfServiceGetPolicyData
    {
        public SelfServiceGetPolicyData()
        {
            PolicyItems = new List<SelfServiceGetPolicyInfoItem>();
        }

        public string Insurer { get; set; }
        public string Product { get; set; }
        public string ProductCode { get; set; }
        public string PolicyNumber { get; set; }
        public string AnnualRenewalDate { get; set; }
        public string OriginalInceptionDate { get; set; }
        public string PolicyType { get; set; }
        public string PolicyStatus { get; set; }
        public double TotalPayment { get; set; }
        public List<SelfServiceGetPolicyInfoItem> PolicyItems { get; set; }
    }

    public class SelfServiceGetPolicyInfoResponseDto
    {
        public bool Success { get; set; }
        public string LastErrorDescription { get; set; }
        public List<SelfServiceGetPolicyData> Data { get; set; }
    }
}