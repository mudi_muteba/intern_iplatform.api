﻿using iPlatform.Api.DTOs.Base;
using Newtonsoft.Json;

namespace iPlatform.Api.DTOs.Policy.CimsSelfService
{
    public class SelfServiceGetPolicyInfoInputDto : IExecutionDto
    {
        public SelfServiceGetPolicyInfoInputDto()
        {
            IdNumber = "";
            CellNumber = "";
            PolicyNumber = "";
        }

        [JsonProperty(PropertyName = "prmIDNumber")]
        public string IdNumber { get; set; }

        [JsonProperty(PropertyName = "prmCellNumber")]
        public string CellNumber { get; set; }

        [JsonProperty(PropertyName = "prmPolicyNumber")]
        public string PolicyNumber { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        [JsonIgnore]
        public DtoContext Context { get; set; }
    }
}