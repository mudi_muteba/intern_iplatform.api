﻿using System;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Policy.CimsSelfService
{
    public class SelfServiceGetPolicyScheduleResponseDto
    {
        public bool Success { get; set; }
        public string LastErrorDescription { get; set; }
        public List<SelfServiceGetPolicyScheduleDatum> Data { get; set; }
    }

    public class SelfServiceGetPolicyScheduleDatum
    {
        public DateTime eDate { get; set; }
        public string AttachtmentGUID { get; set; }
        public byte[] FileData { get; set; }
        public string Name { get; set; }
    }

   

}