﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.Policy
{
    public class BasicPolicyHeaderDto : Resource, ICultureAware
    {
        public string PolicyNo { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int InsurerId { get; set; }
        public string InsurerName { get; set; }
        public PaymentPlan PaymentPlan { get; set; }
        public PolicyStatus PolicyStatus { get; set; }
        public MoneyDto TotalPayment { get; set; }
        public Guid Reference { get; set; }
    }
}
