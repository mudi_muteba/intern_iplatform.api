﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Policy
{
    public class EditPolicyHeaderDto : IExecutionDto, IAffectExistingEntity
    {
        public EditPolicyHeaderDto()
        {
            Context = DtoContext.NoContext();
        }

        public DtoContext Context { get; private set; }

        public string PolicyNo { get; set; }
        public string PolicyMasterGuid { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int InsurerId { get; set; }
        public string InsurerName { get; set; }
        public string PaymentPlan { get; set; }
        public string PolicyStatus { get; set; }
        public MoneyDto TotalPayment { get; set; }
        public DateTimeDto InceptionDate { get; set; }
        public DateTimeDto RenewalDate { get; set; }
        public LeadActivityDto LeadActivity { get; set; }
        public List<PolicyItemDto> PolicyItems { get; set; }
        public DateTimeDto DateUpdated { get; set; }
        public DateTimeDto DateCreated { get; set; }
        public Guid Reference { get; set; }
        public int QuoteId { get; set; }

        public void SetContext(DtoContext context)
        {
        }

        public int Id { get; private set; }
    }
}
