﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Policy.FNOL
{
    public class FnolLodgeClaimResponse
    {
        public string ClaimNumber { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }

    }

    public class FnolLodgeClaimResponseDto : BaseResponseDto
    {
        public FnolLodgeClaimResponse Data { get; set; }

    }
}
