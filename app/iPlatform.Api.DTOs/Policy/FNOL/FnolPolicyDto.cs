﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Policy.FNOL
{
    public class FnolPolicyResponseDto : BaseResponseDto
    {
        public FnolPolicyResponseDto()
        {
            Data = new List<FnolPolicyDto>();
        }

        public List<FnolPolicyDto> Data { get; set; }
    }

    public class FnolPolicyDto
    {
        public Guid PolicyMasterGuid { get; set; }
        public string PolicyHolder { get; set; }
        public string PolicyNo { get; set; }
        public DateTime OnCoverFrom { get; set; }
        public DateTime OnCoverTo { get; set; }
        public Boolean IsVAP { get; set; }
        public DateTime AnnualRenewalDate { get; set; }
        public string PolicyStatus { get; set; }
        public string PolicyType { get; set; }
        public string PaymentMethod { get; set; }
    }
}
