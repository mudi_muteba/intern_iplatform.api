﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Policy.FNOL
{
    public class PolicyDataForFNOLDto : ICultureAware
    {
        public PolicyDataForFNOLDto()
        {
            Data = new List<PolicyDataFnolDto>();
        }

        public List<PolicyDataFnolDto> Data { get; set; }
    }

    public class PolicyDataFnolDto
    {
        public PolicyDataFnolDto()
        {
            InsurerProduct = new List<InsurerProductDto>();
        }

        public List<InsurerProductDto> InsurerProduct { get; set; }
    }

    public class InsurerProductDto : ICultureAware
    {
        public InsurerProductDto()
        {
            clientInfo = new List<ClientInfoDto>();
            policyInfo = new List<PolicyInfoDto>();
        }

        public String InsurerName { get; set; }
        public String ProductName { get; set; }
        public String ProductType { get; set; }
        public Boolean VAPProduct { get; set; }
        public Guid ProductGUID { get; set; }
        public List<ClientInfoDto> clientInfo { get; set; }
        public List<PolicyInfoDto> policyInfo { get; set; }
    }

    public class ClientInfoDto
    {
        public ClientInfoDto()
        {
            clientAddressInfo = new List<ClientAddressInfoDto>();
            clientBankInfo = new List<ClientBankInfoDto>();
        }

        public String ClientType { get; set; }
        public String Surname { get; set; }
        public String Firstnames { get; set; }
        public String Initials { get; set; }
        public String TradingName { get; set; }
        public String ID_Type { get; set; }
        public String IDNo { get; set; }
        public String PassportNo { get; set; }
        public String CoRegNo { get; set; }
        public String VATRegNo { get; set; }
        public String EmailAddress { get; set; }
        public String CellPhone { get; set; }
        public String WorkPhone { get; set; }
        public String HomePhone { get; set; }
        public String Fax { get; set; }
        public Guid EntityGUID { get; set; }
        public List<ClientAddressInfoDto> clientAddressInfo { get; set; }
        public List<ClientBankInfoDto> clientBankInfo { get; set; }
    }

    public class ClientAddressInfoDto
    {
        public ClientAddressInfoDto()
        {
        }

        public String AddressType { get; set; }
        public String AddressSubType { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String Address3 { get; set; }
        public String Address4 { get; set; }
        public String PostalCode { get; set; }
        public String TownshipSuburb { get; set; }
        public String City { get; set; }
        public String ProvinceName { get; set; }
        public String Country { get; set; }
        public Boolean DefaultAddress { get; set; }
        public Boolean ActiveAddress { get; set; }
        public Guid RiskAddressGUID { get; set; }
    }

    public class ClientBankInfoDto
    {
        public ClientBankInfoDto()
        {
        }

        public String BankAccountHolder { get; set; }
        public String BankName { get; set; }
        public String BranchName { get; set; }
        public String BranchCode { get; set; }
        public String BankAccountType { get; set; }
        public String AccountNumber { get; set; }
        public Guid BankDetailsGUID { get; set; }
    }

    public class PolicyInfoDto : ICultureAware
    {
        public PolicyInfoDto()
        {
            sectionInfo = new List<SectionInfoDto>();
        }

        public String PolicyNo { get; set; }
        public String InsurerPolicyNo { get; set; }
        public String PolicyNoOptional { get; set; }
        public String PolicyNoOther { get; set; }
        public Boolean PolicyNoOverridden { get; set; }
        public DateTimeDto OriginalInceptionDate { get; set; }
        public DateTimeDto AnnualRenewalDate { get; set; }
        public String PolicyStatus { get; set; }
        public String PolicyType { get; set; }
        public String PaymentMethod { get; set; }
        public MoneyDto Premium { get; set; }
        public MoneyDto Fees { get; set; }
        public MoneyDto Sasria { get; set; }
        public MoneyDto SasriaShortfall { get; set; }
        public Guid PolicyGUID { get; set; }
        public Guid PolicyMasterGUID { get; set; }
        public List<SectionInfoDto> sectionInfo { get; set; }
    }

    public class SectionInfoDto: ICultureAware
    {
        public SectionInfoDto()
        {
            riskItemInfo = new List<RiskItemInfoDto>();
        }

        public String CoverType { get; set; }
        public String CoverCode { get; set; }
        public Guid CoverGUID { get; set; }
        public List<RiskItemInfoDto> riskItemInfo { get; set; }
    }

    public class RiskItemInfoDto: ICultureAware
    {
        public RiskItemInfoDto()
        {
            riskItemExtensionInfo = new List<RiskItemExtensionInfoDto>();
            riskItemFuneralInfo = new List<RiskItemFuneralInfoDto>();
        }

        public String ItemDescription { get; set; }
        public MoneyDto Sum_Insured { get; set; }
        public MoneyDto Premium { get; set; }
        public MoneyDto Sasria { get; set; }
        public String Vehicle_Make { get; set; }
        public String Vehicle_Model { get; set; }
        public String Vehicle_Year { get; set; }
        public String Vehicle_ChassisNo { get; set; }
        public String Vehicle_EngineNo { get; set; }
        public String Vehicle_RegNo { get; set; }
        public Guid ItemMasterGUID { get; set; }
        public String VendorItemCode { get; set; }
        public Guid ItemGUID { get; set; }
        public Guid CoverGUID { get; set; }
        public String CoverCode { get; set; }
        public List<RiskItemExtensionInfoDto> riskItemExtensionInfo { get; set; }
        public List<RiskItemFuneralInfoDto> riskItemFuneralInfo { get; set; }
    }

    public class RiskItemExtensionInfoDto: ICultureAware
    {
        public RiskItemExtensionInfoDto()
        {
        }

        public String Extension { get; set; }
        public MoneyDto Sum_Insured { get; set; }
        public MoneyDto Premium { get; set; }
        public MoneyDto Sasria { get; set; }
        public Guid MainItemGUID { get; set; }
        public Guid MainCoverGUID { get; set; }
        public String MainCoverCode { get; set; }
        public Guid ItemMasterGUID { get; set; }
        public Guid ItemGUID { get; set; }
        public Guid CoverGUID { get; set; }
        public String CoverCode { get; set; }
    }

    public class RiskItemFuneralInfoDto: ICultureAware
    {
        public RiskItemFuneralInfoDto()
        {
        }

        public String PlanName { get; set; }
        public String IndividualsOnCover { get; set; }
        public String IDNo { get; set; }
        public DateTimeDto DateOnCover { get; set; }
        public Int32 AccidentalDeathBenefit { get; set; }
        public Int32 GroceryBenefit { get; set; }
        public Int32 MemorialBenefit { get; set; }
        public Int32 NaturalBenefit { get; set; }
        public Guid GridItemUniqueGUID { get; set; }
        public Guid ItemMasterGUID { get; set; }
        public Guid ItemGUID { get; set; }
        public Guid CoverGUID { get; set; }
        public String CoverCode { get; set; }
    }
}
