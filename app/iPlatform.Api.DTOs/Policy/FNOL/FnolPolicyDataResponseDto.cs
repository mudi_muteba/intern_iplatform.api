﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Policy.FNOL
{
    public class FnolPolicyDataResponseDto : BaseResponseDto
    {
        public FnolPolicyDataResponseDto()
        {
            Data = new List<FnolPolicyDataDto>();
        }

        public List<FnolPolicyDataDto> Data { get; set; }
    }

    public class FnolPolicyDataDto
    {
        public FnolPolicyDataDto()
        {
            InsurerProduct = new List<InsurerProduct>();
        }

        public List<InsurerProduct> InsurerProduct { get; set; }
    }

    public class InsurerProduct
    {
        public InsurerProduct()
        {
            clientInfo = new List<ClientInfo>();
            policyInfo = new List<PolicyInfo>();
        }

        public String InsurerName { get; set; }
        public String ProductName { get; set; }
        public String ProductType { get; set; }
        public Boolean VAPProduct { get; set; }
        public Guid ProductGUID { get; set; }
        public List<ClientInfo> clientInfo { get; set; }
        public List<PolicyInfo> policyInfo { get; set; }
    }

    public class ClientInfo
    {
        public ClientInfo()
        {
            clientAddressInfo = new List<ClientAddressInfo>();
            clientBankInfo = new List<ClientBankInfo>();
        }

        public String ClientType { get; set; }
        public String Surname { get; set; }
        public String Firstnames { get; set; }
        public String Initials { get; set; }
        public String TradingName { get; set; }
        public String ID_Type { get; set; }
        public String IDNo { get; set; }
        public String PassportNo { get; set; }
        public String CoRegNo { get; set; }
        public String VATRegNo { get; set; }
        public String EmailAddress { get; set; }
        public String CellPhone { get; set; }
        public String WorkPhone { get; set; }
        public String HomePhone { get; set; }
        public String Fax { get; set; }
        public Guid EntityGUID { get; set; }
        public List<ClientAddressInfo> clientAddressInfo { get; set; }
        public List<ClientBankInfo> clientBankInfo { get; set; }
    }

    public class ClientAddressInfo
    {
        public ClientAddressInfo()
        {
        }

        public String AddressType { get; set; }
        public String AddressSubType { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String Address3 { get; set; }
        public String Address4 { get; set; }
        public String PostalCode { get; set; }
        public String TownshipSuburb { get; set; }
        public String City { get; set; }
        public String ProvinceName { get; set; }
        public String Country { get; set; }
        public Boolean DefaultAddress { get; set; }
        public Boolean ActiveAddress { get; set; }
        public Guid RiskAddressGUID { get; set; }
    }

    public class ClientBankInfo
    {
        public ClientBankInfo()
        {
        }

        public String BankAccountHolder { get; set; }
        public String BankName { get; set; }
        public String BranchName { get; set; }
        public String BranchCode { get; set; }
        public String BankAccountType { get; set; }
        public String AccountNumber { get; set; }
        public Guid BankDetailsGUID { get; set; }
    }

    public class PolicyInfo
    {
        public PolicyInfo()
        {
            sectionInfo = new List<SectionInfo>();
        }

        public String PolicyNo { get; set; }
        public String InsurerPolicyNo { get; set; }
        public String PolicyNoOptional { get; set; }
        public String PolicyNoOther { get; set; }
        public Boolean PolicyNoOverridden { get; set; }
        public DateTime OriginalInceptionDate { get; set; }
        public DateTime AnnualRenewalDate { get; set; }
        public String PolicyStatus { get; set; }
        public String PolicyType { get; set; }
        public String PaymentMethod { get; set; }
        public Decimal Premium { get; set; }
        public Decimal Fees { get; set; }
        public Decimal Sasria { get; set; }
        public Decimal SasriaShortfall { get; set; }
        public Guid PolicyGUID { get; set; }
        public Guid PolicyMasterGUID { get; set; }
        public List<SectionInfo> sectionInfo { get; set; }
    }

    public class SectionInfo
    {
        public SectionInfo()
        {
            riskItemInfo = new List<RiskItemInfo>();
        }

        public String CoverType { get; set; }
        public String CoverCode { get; set; }
        public Guid CoverGUID { get; set; }
        public List<RiskItemInfo> riskItemInfo { get; set; }
    }

    public class RiskItemInfo
    {
        public RiskItemInfo()
        {
            riskItemExtensionInfo = new List<RiskItemExtensionInfo>();
            riskItemFuneralInfo = new List<RiskItemFuneralInfo>();
        }

        public String ItemDescription { get; set; }
        public Int32 Sum_Insured { get; set; }
        public Decimal Premium { get; set; }
        public Decimal Sasria { get; set; }
        public String Vehicle_Make { get; set; }
        public String Vehicle_Model { get; set; }
        public String Vehicle_Year { get; set; }
        public String Vehicle_ChassisNo { get; set; }
        public String Vehicle_EngineNo { get; set; }
        public String Vehicle_RegNo { get; set; }
        public Guid ItemMasterGUID { get; set; }
        public String VendorItemCode { get; set; }
        public Guid ItemGUID { get; set; }
        public Guid CoverGUID { get; set; }
        public String CoverCode { get; set; }
        public List<RiskItemExtensionInfo> riskItemExtensionInfo { get; set; }
        public List<RiskItemFuneralInfo> riskItemFuneralInfo { get; set; }
    }

    public class RiskItemExtensionInfo
    {
        public RiskItemExtensionInfo()
        {
        }

        public String Extension { get; set; }
        public Int32 Sum_Insured { get; set; }
        public Decimal Premium { get; set; }
        public Decimal Sasria { get; set; }
        public Guid MainItemGUID { get; set; }
        public Guid MainCoverGUID { get; set; }
        public String MainCoverCode { get; set; }
        public Guid ItemMasterGUID { get; set; }
        public Guid ItemGUID { get; set; }
        public Guid CoverGUID { get; set; }
        public String CoverCode { get; set; }
    }

    public class RiskItemFuneralInfo
    {
        public RiskItemFuneralInfo()
        {
        }

        public String PlanName { get; set; }
        public String IndividualsOnCover { get; set; }
        public String IDNo { get; set; }
        public DateTime DateOnCover { get; set; }
        public Int32 AccidentalDeathBenefit { get; set; }
        public Int32 GroceryBenefit { get; set; }
        public Int32 MemorialBenefit { get; set; }
        public Int32 NaturalBenefit { get; set; }
        public Guid GridItemUniqueGUID { get; set; }
        public Guid ItemMasterGUID { get; set; }
        public Guid ItemGUID { get; set; }
        public Guid CoverGUID { get; set; }
        public String CoverCode { get; set; }
    }
}
