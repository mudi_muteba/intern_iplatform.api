﻿using System;

namespace iPlatform.Api.DTOs.Policy
{
    public class CreatePolicyItemDto
    {
        public int AssetNumber { get; set; }
        public string ItemDescription { get; set; }
        public DateTime? DateEffective { get; set; }
        public DateTime? DateEnd { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}
