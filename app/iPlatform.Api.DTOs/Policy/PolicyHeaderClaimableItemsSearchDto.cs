﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Policy
{
    public class PolicyHeaderClaimableItemsSearchDto : BaseCriteria
    {
        public int? PartyId { get; set; }
        public string DateOfLoss { get; set; }
    }
}
