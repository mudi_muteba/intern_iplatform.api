﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Policy
{
    public class PolicyHeaderClaimableItemDto: Resource, ICultureAware
    {
        public virtual PolicyHeaderDto PolicyHeader { get; set; }
        public virtual Guid RequestId { get; set; }
        public virtual DateTimeDto DateOfLoss { get; set; }
        public virtual PolicyItemDto PolicyItem { get; set; }
        public virtual Guid ExternalItemId { get; set; }
        public virtual Guid ExternalItemMasterId { get; set; }
        public virtual Guid ExternalSubmitGuid { get; set; }
        public virtual string Description { get; set; }
    }
}
