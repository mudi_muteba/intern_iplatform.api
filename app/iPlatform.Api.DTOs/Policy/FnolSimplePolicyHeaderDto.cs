﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Policy
{
    public class FnolSimplePolicyHeader
    {
        public string PolicyMasterGuid { get; set; }
        public string PolicyHolder { get; set; }
        public string PolicyNo { get; set; }
        public DateTime OnCoverFrom { get; set; }
        public DateTime OnCoverTo { get; set; }
        public Boolean IsVAP { get; set; }
        public DateTime AnnualRenewalDate { get; set; }
        public string PolicyStatus { get; set; }
        public string PolicyType { get; set; }
        public string PaymentMethod { get; set; }
    }
}
