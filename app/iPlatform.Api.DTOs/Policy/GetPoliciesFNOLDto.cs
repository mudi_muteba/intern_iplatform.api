﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Individual;

namespace iPlatform.Api.DTOs.Policy
{
    public class GetPoliciesFNOLDto : AttributeValidationDto, IExecutionDto
    {
        public GetPoliciesFNOLDto()
        {
            Context = DtoContext.NoContext();
        }
        public int PartyId { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (PartyId <= 0)
                validation.Add(PartyValidationMessages.InvalidId.AddParameters(new[] { PartyId.ToString() }));

            return validation;
        }

    }
}
