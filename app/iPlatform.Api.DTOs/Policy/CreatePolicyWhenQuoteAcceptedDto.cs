﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Policy;
using Shared.Extentions;

namespace iPlatform.Api.DTOs.Policy
{
    public class CreatePolicyWhenQuoteAcceptedDto : AttributeValidationDto, IExecutionDto
    {
        public CreatePolicyWhenQuoteAcceptedDto()
        {
            Context = DtoContext.NoContext();
        }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public int QuoteId { get; set; }
        public Guid Reference { get; set; }
        public string PolicyNo { get; set; }
        public int QuoteAcceptedLeadActivityId { get; set; }


        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (QuoteId == 0)
                validation.Add(PolicyValidationMessages.EmptyQuoteId);


            if (Reference.IsEmpty())
                validation.Add(PolicyValidationMessages.EmptyReference);

            return validation;
        }
    }
}
