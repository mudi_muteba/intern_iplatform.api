﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Policy
{
    public class PolicyScheduleDto
    {
        public string eDate { get; set; }
        public string AttachtmentGUID { get; set; }
        public string FileData { get; set; }
        public string Name { get; set; }
    }
}
