﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Claims;
using ValidationMessages.Individual;
using ValidationMessages.Policy;

namespace iPlatform.Api.DTOs.Policy
{
    public class GetPolicyAvailableItemsForFNOLDto : AttributeValidationDto, IExecutionDto
    {
        public GetPolicyAvailableItemsForFNOLDto()
        {
            Context = DtoContext.NoContext();
        }

        public int Id { get; set; }
        public int PartyId { get; set; }
        public int ClaimsHeaderId { get; set; }
        public string DateOfLoss { get; set; }


        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (PartyId <= 0)
                validation.Add(PartyValidationMessages.InvalidId.AddParameters(new[] { PartyId.ToString() }));

            if (string.IsNullOrEmpty(DateOfLoss))
                validation.Add(PolicyValidationMessages.EmptyDateOfLoss);

            if (!string.IsNullOrEmpty(DateOfLoss))
            {
                DateTime parsedDate = new DateTime();
                if (!DateTime.TryParse(DateOfLoss, out parsedDate))
                    validation.Add(PolicyValidationMessages.InvalidDateOfLoss.AddParameters(new[] { DateOfLoss }));
            }
                

            return validation;
        }
    }
}
