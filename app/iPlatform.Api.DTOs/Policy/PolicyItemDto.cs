﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Policy
{
    public class PolicyItemDto : Resource, ICultureAware
    {
        public int AssetNumber { get; set; }
        public int PolicyHeaderClaimableItemId { get; set; }
        public string ItemDescription { get; set; }
        public string ItemMasterGuid { get; set; }
        public DateTimeDto DateEffective { get; set; }
        public DateTimeDto DateEnd { get; set; }
        public DateTimeDto DateCreated { get; set; }
        public DateTimeDto DateUpdated { get; set; }
        public string Cover { get; set; }
        public MoneyDto TotalPayment { get; set; }
        public MoneyDto ExcessBasic { get; set; }
        public MoneyDto VoluntaryExcess { get; set; }
        public MoneyDto SumInsured { get; set; }

    }
}
