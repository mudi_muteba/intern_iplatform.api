﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Policy
{
    public class PolicySearchDto : BaseCriteria, IExecutionDto
    {
        public string PolicyId { get; set; }
        public int? PartyId { get; set; }
        public string insurerCode { get; set; }
        public string IdNumber { get; set; }
        public string MobileNumber { get; set; }
        public bool IncludeItems { get; set; }
        public int QuoteId { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}
