﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Policy
{
    public class CreatePolicyHeaderDto : AttributeValidationDto, IExecutionDto
    {
        public CreatePolicyHeaderDto()
        {
            PolicyItems = new List<CreatePolicyItemDto>();
            Context = DtoContext.NoContext();
        }

        public int MasterPolicyId { get; set; }
        public int LeadActivityID { get; set;}
        public int PolicyId { get; set;}
        public string QuoteNo { get; set; }
        public string PolicyNo { get; set; }
        public int ProductId { get; set; }
        public int InsurerId { get; set; }
        public int PartyId { get; set; }
        public DateTime DateEffective { get; set; }
        public DateTime DateEnd { get; set; }
        public DateTime DateRenewal { get; set; }
        public int Frequency { get; set; }
        public bool Renewable { get; set; }
        public int PaymentPlanId { get; set; }
        public bool NewBusiness { get; set; }
        public bool VAP { get; set; }
        public PolicyStatus PolicyStatus { get; set; }
        public List<CreatePolicyItemDto> PolicyItems { get; set; }
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }

    }
}
