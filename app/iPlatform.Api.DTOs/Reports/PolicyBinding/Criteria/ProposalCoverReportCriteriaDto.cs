﻿namespace iPlatform.Api.DTOs.Reports.PolicyBinding.Criteria
{
    public class ProposalCoverReportCriteriaDto
    {
        public ProposalCoverReportCriteriaDto()
        {
        }

        public int QuoteId { get; set; }
    }
}