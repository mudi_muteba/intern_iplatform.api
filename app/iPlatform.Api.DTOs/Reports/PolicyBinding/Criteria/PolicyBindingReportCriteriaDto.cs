﻿namespace iPlatform.Api.DTOs.Reports.PolicyBinding.Criteria
{
    public class PolicyBindingReportCriteriaDto
    {
        public PolicyBindingReportCriteriaDto()
        {
        }

        public int ChannelId { get; set; }
        public int CurrentChannelId { get; set; }
        public int LeadId { get; set; }
        public int PartyId { get; set; }
        public int AgentId { get; set; }
        public int ProposalHeaderId { get; set; }
        public int QuoteId { get; set; }
    }
}