﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.PolicyBinding
{
    public class PolicyBindingReportChannelDto : Resource, ICultureAware
    {
        public PolicyBindingReportChannelDto()
        {
        }
        public string Name { get; set; }
    }
}
