﻿namespace iPlatform.Api.DTOs.Reports.PolicyBinding
{
    public class PolicyBindingReportAgentDto
    {
        public PolicyBindingReportAgentDto()
        {
        }

        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    }
}
