﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.PolicyBinding
{
    public class PolicyBindingReportContainerDto : Resource, ICultureAware
    {
        public PolicyBindingReportContainerDto()
        {
            QuestionAnswer = new List<PolicyBindingReportQuestionAnswerDto>();
            QuestionAnswerProposal = new List<ProposalCoverReportQuestionAnswerDto>();
        }

        public PolicyBindingReportChannelDto Channel { get; set; }
        public PolicyBindingReportClientDto Client { get; set; }
        public PolicyBindingReportAgentDto Agent { get; set; }
        public List<PolicyBindingReportQuestionAnswerDto> QuestionAnswer { get; set; }
        public List<ProposalCoverReportQuestionAnswerDto> QuestionAnswerProposal { get; set; }
        public string ProductName { get; set; }
        public string DateSubmitted { get; set; }
    }
}

