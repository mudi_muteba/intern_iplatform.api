﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.PolicyBinding
{
    public class PolicyBindingReportQuestionAnswerDto : Resource, ICultureAware
    {
        public PolicyBindingReportQuestionAnswerDto()
        {

        }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}