﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.PolicyBinding
{
    public class PolicyBindingReportClientDto : Resource, ICultureAware
    {
        public PolicyBindingReportClientDto()
        {
        }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    }
}
