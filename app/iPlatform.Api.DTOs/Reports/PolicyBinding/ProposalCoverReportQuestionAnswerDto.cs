﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.PolicyBinding
{
    public class ProposalCoverReportQuestionAnswerDto : Resource, ICultureAware
    {
        public ProposalCoverReportQuestionAnswerDto()
        {

        }
        public string CoverName { get; set; }
        public string Asset { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
