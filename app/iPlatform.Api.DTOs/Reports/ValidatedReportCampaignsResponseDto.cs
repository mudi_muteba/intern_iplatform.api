﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using ValidationMessages;
using ValidationMessages.Reports;

namespace iPlatform.Api.DTOs.Reports
{
    public class ValidatedReportCampaignsResponseDto
    {
        public ValidatedReportCampaignsResponseDto()
        {
        }

        public bool IsMultiChannel { get; set; }
        public bool IsValidated { get; set; }
        public string Message { get; set; }
    }
}
