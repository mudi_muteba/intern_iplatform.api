﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using ValidationMessages;
using ValidationMessages.Reports;

namespace iPlatform.Api.DTOs.Reports
{
    public class ValidateReportCampaignsDto : AttributeValidationDto, IExecutionDto
    {
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }

        public ValidateReportCampaignsDto()
        {
            Context = DtoContext.NoContext();
            Campaigns = new List<int>();
            Reports = new List<string>();
        }


        public List<int> Campaigns { get; set; }
        public List<string> Reports { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Reports.Count == 0)
                validation.Add(ReportValidationMessages.ReportListEmpty);

            if (Campaigns.Count == 0)
                validation.Add(ReportValidationMessages.CampaignListEmpty);

            return validation;
        }
    }
}
