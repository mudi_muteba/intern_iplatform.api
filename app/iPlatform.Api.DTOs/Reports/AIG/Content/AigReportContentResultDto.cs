﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Reports.AIG.Content
{
    public class AigReportContentResultDto : Resource
    {
        public AigReportContentResultDto()
        {
        }

        public string ExcelFileName;
    }
}
