﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Reports.AIG.Content
{
    public class AigReportContentDto
    {
        public AigReportContentDto()
        {
            Contents = new List<AigReportContentExtractDto>();

        }

        public List<AigReportContentExtractDto> Contents;
        public string Excel;
    }
}
