﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Reports.AIG.SalesForceIntegration
{
    public class AigReportSalesForceIntegrationLogExtractDto
    {

        public AigReportSalesForceIntegrationLogExtractDto()
        {
            
        }
        public virtual string JsonData { get; set; }
        public virtual string Response { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual int PartyId { get; set; }
        public virtual bool IsSuccess { get; set; }
        public virtual string Status { get; set; }
    }
}
