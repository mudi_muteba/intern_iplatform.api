﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Reports.AIG.SalesForceIntegration
{
    public class AigReportSalesForceIntegrationResultDto : Resource
    {
        public AigReportSalesForceIntegrationResultDto()
        {
        }

        public string ExcelFileName;
    }
}
