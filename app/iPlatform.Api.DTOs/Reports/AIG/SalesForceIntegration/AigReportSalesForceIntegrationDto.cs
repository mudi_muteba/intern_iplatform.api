﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Reports.AIG.Motor;

namespace iPlatform.Api.DTOs.Reports.AIG.SalesForceIntegration
{
    public class AigReportSalesForceIntegrationDto : ICultureAware
    {
        public AigReportSalesForceIntegrationDto()
        {
            SalesForceIntegration = new List<AigReportSalesForceIntegrationLogExtractDto>();

        }

        public List<AigReportSalesForceIntegrationLogExtractDto> SalesForceIntegration;
        public string Excel;
    }
}


