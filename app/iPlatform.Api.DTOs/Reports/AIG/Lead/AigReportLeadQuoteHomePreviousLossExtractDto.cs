﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.AIG.Lead
{
    public class AigReportLeadQuoteHomePreviousLossExtractDto 
    {
        public AigReportLeadQuoteHomePreviousLossExtractDto()
        {
        }

        public int PartyId { get; set; }
        public string Type { get; set; }
        public string DateOfLoss { get; set; }
        public string HomeLossType { get; set; }
        public string HomeLossClaim { get; set; }
        public string Location { get; set; }
    }
}
