﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.AIG.Lead
{
    public class AigReportLeadQuoteMotorPreviousLossExtractDto 
    {
        public AigReportLeadQuoteMotorPreviousLossExtractDto()
        {
        }

        public int PartyId { get; set; }
        public string Type { get; set; }
        public string DateOfLoss { get; set; }
        public string MotorLossType { get; set; }
        public string MotorLossClaim { get; set; }
        public string Location { get; set; }
    }
}
