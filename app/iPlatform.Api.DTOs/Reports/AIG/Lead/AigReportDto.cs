﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.Address;

namespace iPlatform.Api.DTOs.Reports.AIG.Lead
{
    public class AigReportDto : ICultureAware
    {
        public AigReportDto()
        {
            Leads = new List<AigReportLeadExtractDto>();
            Addresses = new List<BasicAddressDto>();
            MotorPreviousLoss = new List<AigReportLeadQuoteMotorPreviousLossExtractDto>();
            HomePreviousLoss = new List<AigReportLeadQuoteHomePreviousLossExtractDto>();


        }

        public List<AigReportLeadExtractDto> Leads;
        public List<BasicAddressDto> Addresses;
        public List<AigReportLeadQuoteMotorPreviousLossExtractDto> MotorPreviousLoss;
        public List<AigReportLeadQuoteHomePreviousLossExtractDto> HomePreviousLoss;
        public string Excel ;
    }
}
