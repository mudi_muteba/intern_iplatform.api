﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Party.Address;

namespace iPlatform.Api.DTOs.Reports.AIG.Lead
{
    public class AigReportLeadExcelDto
    {
        public AigReportLeadExcelDto()
        {
            Addresses = new List<BasicAddressDto>();
            MotorLosses = new List<AigReportLeadQuoteMotorPreviousLossExtractDto>();
            HomeLosses= new List<AigReportLeadQuoteHomePreviousLossExtractDto>();
        }

        public string AgentName { get; set; }
        public string Channel { get; set; }
        public string DateOfQuote { get; set; }
        public string QuoteExpired { get; set; }
        public string QuoteBound { get; set; }
        public string QuoteBindDate { get; set; }
        public string QuoteTotalPremium { get; set; }
        public decimal QuoteMotorPremium { get; set; }
        public decimal QuoteHomePremium { get; set; }
        public decimal QuoteFuneralPremium { get; set; }
        public string QuoteOtherPremium { get; set; }
        public string QuoteIncepptionDate { get; set; }
        public int QuotePolicyNumber { get; set; }
        public string QuoteCreateDate { get; set; }
        public string QuoteCreateTime { get; set; }
        public int LeadText { get; set; }
        public string IDNumber { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string Initials { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string ITCCreditScore { get; set; }
        public string ITCCreditScoreBand { get; set; }
        public string Occupation { get; set; }
        public string HomeLanguage { get; set; }
        public string PreferredLanguage { get; set; }
        public string ExternalReference { get; set; }
        public string EmailAddress { get; set; }
        public string CellPhoneNumber { get; set; }
        public string HomePhoneNumber { get; set; }
        public string WorkPhoneNumber { get; set; }
        public string DeclarationJudgements { get; set; }
        public string DeclarationDebtReview { get; set; }
        public string DeclarationSequestration { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string CurrentlyInsured { get; set; }
        public string UninterruptedCover { get; set; }
        public string CancelledInsurance { get; set; }
        public int PartyId { get; set; }
        public string ExcelFile { get; set; }
        public List<BasicAddressDto> Addresses { get; set; }
        public List<AigReportLeadQuoteMotorPreviousLossExtractDto> MotorLosses { get; set; }
        public List<AigReportLeadQuoteHomePreviousLossExtractDto> HomeLosses { get; set; }
    }
}
