﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Reports.AIG.Lead
{
    public class AigLeadExcelData
    {
        public AigLeadExcelData()
        {
            Leads = new List<AigReportLeadExtractDto>();
        }

        public List<AigReportLeadExtractDto> Leads;
    }
}
