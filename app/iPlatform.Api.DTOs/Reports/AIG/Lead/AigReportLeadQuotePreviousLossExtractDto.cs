﻿namespace iPlatform.Api.DTOs.Reports.AIG.Lead
{
    public class AigReportLeadQuotePreviousLossExtractDto
    {
        public AigReportLeadQuotePreviousLossExtractDto()
        {
        }

        public int PartyId { get; set; }
        public string Type { get; set; }
        public string DateOfLoss { get; set; }
        public string LossType { get; set; }
        public string LossClaim { get; set; }
        public string Location { get; set; }
    }
}
