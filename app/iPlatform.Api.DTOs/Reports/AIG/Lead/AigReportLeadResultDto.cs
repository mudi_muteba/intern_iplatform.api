﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Reports.AIG.Lead
{
    public class AigReportLeadResultDto : Resource
    {
        public AigReportLeadResultDto()
        {
        }

        public string ExcelFileName;
    }
}
