﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.AIG.Lead.Criteria
{
    public class AigReportLeadHomePreviousLossCriteriaDto : Resource, ICultureAware
    {
        public int PartyId { get; set; }
    }
}
