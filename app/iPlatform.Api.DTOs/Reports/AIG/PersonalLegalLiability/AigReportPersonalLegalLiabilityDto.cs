﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Reports.AIG.PersonalLegalLiability
{
    public class AigReportPersonalLegalLiabilityDto
    {
        public AigReportPersonalLegalLiabilityDto()
        {
            PersonalLegalLiabilities = new List<AigReportPersonalLegalLiabilityExtractDto>();

        }

        public List<AigReportPersonalLegalLiabilityExtractDto> PersonalLegalLiabilities;
        public string Excel;
    }
}
