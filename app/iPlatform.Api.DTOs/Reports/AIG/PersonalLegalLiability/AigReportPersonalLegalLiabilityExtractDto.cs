﻿using System;

namespace iPlatform.Api.DTOs.Reports.AIG.PersonalLegalLiability
{
    public class AigReportPersonalLegalLiabilityExtractDto
    {
        public AigReportPersonalLegalLiabilityExtractDto()
        {
        }

        public virtual string QuoteCreateDate { get; set; }
        public virtual string QuoteCreateTime { get; set; }
        public virtual string DateOfQuote { get; set; }
        public virtual string QuoteNumber { get; set; }
        public virtual int LeadNumber { get; set; }
        public virtual string AddressSuburb { get; set; }
        public virtual string AddressPostCode { get; set; }
        public virtual string AddressProvince { get; set; }
        public virtual string PersonalLiabilityLimit { get; set; }
        public virtual decimal Premium { get; set; }
    }
}