﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Reports.AIG.IdentityTheft
{
    public class AigReportIdentityTheftDto
    {
        public AigReportIdentityTheftDto()
        {
            IdentityThefts = new List<AigReportIdentityTheftExtractDto>();

        }

        public List<AigReportIdentityTheftExtractDto> IdentityThefts;
        public string Excel;
    }
}
