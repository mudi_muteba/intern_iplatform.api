﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Reports.AIG.DisasterCash
{
    public class AigReportDisasterCashDto
    {
        public AigReportDisasterCashDto()
        {
            DisasterCashes = new List<AigReportDisasterCashExtractDto>();

        }

        public List<AigReportDisasterCashExtractDto> DisasterCashes;
        public string Excel;
    }
}
