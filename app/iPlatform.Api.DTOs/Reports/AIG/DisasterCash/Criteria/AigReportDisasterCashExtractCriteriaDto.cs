﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.AIG.DisasterCash.Criteria
{
    public class AigReportDisasterCashExtractCriteriaDto : Resource, ICultureAware
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}