﻿using System;

namespace iPlatform.Api.DTOs.Reports.AIG.DisasterCash
{
    public class AigReportDisasterCashExtractDto
    {
        public AigReportDisasterCashExtractDto()
        {
        }

        public virtual string QuoteCreateDate { get; set; }
        public virtual string QuoteCreateTime { get; set; }
        public virtual string DateOfQuote { get; set; }
        public virtual string QuoteNumber { get; set; }
        public virtual int LeadNumber { get; set; }
        public virtual string AddressSuburb { get; set; }
        public virtual string AddressPostCode { get; set; }
        public virtual string AddressProvince { get; set; }
        public virtual string CashLimit { get; set; }
        public virtual decimal Premium { get; set; }
    }
}