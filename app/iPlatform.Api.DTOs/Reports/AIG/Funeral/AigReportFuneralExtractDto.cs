﻿using System;
using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.AdditionalMembers;

namespace iPlatform.Api.DTOs.Reports.AIG.Funeral
{
    public class AigReportFuneralExtractDto
    {
        public AigReportFuneralExtractDto()
        {
        }

        public virtual string AgentName { get; set; }
        public virtual string MortgageBank { get; set; }
        public virtual string VAPSWaterPumpingMachinery { get; set; }
        public virtual string BuildingsAdditionalExcess { get; set; }
        public virtual string Channel { get; set; }
        public virtual string DateOfQuote { get; set; }
        public virtual string QuoteExpired { get; set; }
        public virtual string QuoteBound { get; set; }
        public virtual string QuoteBindDate { get; set; }
        public virtual decimal QuoteTotalPremium { get; set; }
        public virtual decimal QuoteMotorPremium { get; set; }
        public virtual decimal QuoteHomePremium { get; set; }
        public virtual decimal QuoteFuneralPremium { get; set; }
        public virtual decimal QuoteOtherPremium { get; set; }
        public virtual string QuoteIncepptionDate { get; set; }
        public virtual string QuotePolicyNumber { get; set; }
        public virtual string QuoteCreateDate { get; set; }
        public virtual string QuoteCreateTime { get; set; }
        public virtual int LeadText { get; set; }
        public virtual int PropertyNumber { get; set; }
        public virtual string AddressSuburb { get; set; }
        public virtual string AddressPostCode { get; set; }
        public virtual string AddressProvince { get; set; }
        public virtual string CoverType { get; set; }
        public virtual string Occupation { get; set; }
        public virtual string BeneficiaryIdNumber { get; set; }
        public virtual string BeneficiaryFirstname { get; set; }
        public virtual string BeneficiarySurname { get; set; }
        public virtual string BeneficiaryContactNumber { get; set; }
        public virtual string BeneficiaryRelationship { get; set; }
        public virtual decimal SumAssured { get; set; }

        public virtual List<ListAdditionalMemberDto> AdditionalMembers { get; set; }
        public virtual int ProposalDefinitionId { get; set; }
    }
}