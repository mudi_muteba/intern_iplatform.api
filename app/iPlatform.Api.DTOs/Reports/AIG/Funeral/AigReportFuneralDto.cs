﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Reports.AIG.Funeral
{
    public class AigReportFuneralDto
    {
        public AigReportFuneralDto()
        {
            Funerals = new List<AigReportFuneralExtractDto>();

        }

        public List<AigReportFuneralExtractDto> Funerals;
        public string Excel;
    }
}
