﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Reports.AIG.Motor
{
    public class AigReportMotorResultDto : Resource
    {
        public AigReportMotorResultDto()
        {
        }

        public string ExcelFileName;
    }
}
