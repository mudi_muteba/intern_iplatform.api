﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.AIG.Motor
{
    public class AigReportMotorDto : ICultureAware
    {
        public AigReportMotorDto()
        {
            Motors = new List<AigReportMotorExtractDto>();

        }

        public List<AigReportMotorExtractDto> Motors;
        public string Excel;
    }
}

