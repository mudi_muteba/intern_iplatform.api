﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Reports.AIG.Lead;

namespace iPlatform.Api.DTOs.Reports.AIG.Motor
{
    public class AigReportMotorExtractDto
    {
        public AigReportMotorExtractDto()
        {

        }

        public string Channel { get; set; }
        public string QuoteExpired { get; set; }
        public string QuoteBound { get; set; }
        public string QuoteBindDate { get; set; }
        public string DateOfQuote { get; set; }
        public decimal QuoteTotalPremium { get; set; }
        public decimal QuoteMotorPremium { get; set; }
        public decimal QuoteHomePremium { get; set; }
        public decimal QuoteFuneralPremium { get; set; }
        public decimal QuoteOtherPremium { get; set; }
        public decimal QuoteFees { get; set; }
        public decimal QuoteSasria { get; set; }
        public string QuoteIncepptionDate { get; set; }
        public string QuotePolicyNumber { get; set; }
        public string QuoteCreateDate { get; set; }
        public string QuoteCreateTime { get; set; }
        public int QuoteId { get; set; }
        public string Vmsaid { get; set; }
        public int VehicleNumber { get; set; }
        public string NightAddressSuburb { get; set; }
        public string NightAddressPostCode { get; set; }
        public string NightAddressProvince { get; set; }
        public string NightAddressStorage { get; set; }
        public string DayAddressSuburb { get; set; }
        public string DayAddressPostCode { get; set; }
        public string DayAddressProvince { get; set; }
        public string DayAddressStorage { get; set; }
        public string VehicleType { get; set; }
        public string MotorExcess { get; set; }
        public string RegistrationNumber { get; set; }
        public string VehicleYear { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleModel { get; set; }
        public string VehicleMmCode { get; set; }
        public string VinNumber { get; set; }
        public string Colour { get; set; }
        public string MetallicPaint { get; set; }
        public string CoverType { get; set; }
        public string Modified { get; set; }
        public string VehicleStatus { get; set; }
        public string ClassOfUse { get; set; }
        public string Delivery { get; set; }
        public string TrackingDevice { get; set; }
        public string DiscountedTrackingDevice { get; set; }
        public string Immobiliser { get; set; }
        public string AntiHijack { get; set; }
        public string DataDot { get; set; }
        public decimal AccSoundEquipment { get; set; }
        public decimal AccMagWheels { get; set; }
        public decimal AccSunRoof { get; set; }
        public decimal AccXenonLights { get; set; }
        public decimal AccTowBar { get; set; }
        public decimal AccBodyKit { get; set; }
        public decimal AccAntiSmashAndGrab { get; set; }
        public decimal AccOther { get; set; }
        public string VapCarHire { get; set; }
        public string VapTyreAndRim { get; set; }
        public string VapScratchAndDent { get; set; }
        public string VapCreditShortfall { get; set; }
        public string DriverIdNumber { get; set; }
        public string DriverTitle { get; set; }
        public string DriverFirstName { get; set; }
        public string DriverSurname { get; set; }
        public string DriverGender { get; set; }
        public string DriverMaritalStatus { get; set; }
        public string DriverDateOfBirth { get; set; }
        public string DriverOccupation { get; set; }
        public string DriverRecentCoverIndicator { get; set; }
        public string DriverUninterruptedCover { get; set; }
        public string DriverLicenseType { get; set; }
        public string DriverLicenseDate { get; set; }
        public string DriverLicenseEndorsed { get; set; }
        public string LimitationAutomatic { get; set; }
        public string LimitationParaplegic { get; set; }
        public string LimitationAmputee { get; set; }
        public string LimitationGlasses { get; set; }
        public string Financed { get; set; }
        public string ValuationMethod { get; set; }
        public decimal SumInsured { get; set; }
        public decimal Sasria { get; set; }
        public int PartyId { get; set; }

        public List<AigReportLeadQuoteMotorPreviousLossExtractDto> MotorLosses { get; set; }
    }
}
