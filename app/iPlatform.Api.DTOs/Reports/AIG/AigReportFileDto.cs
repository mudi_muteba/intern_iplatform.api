﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Reports.AIG
{
    public class AigReportFileDto : Resource
    {
        public AigReportFileDto()
        {
        }

        public string ExcelFileName;
    }
}
