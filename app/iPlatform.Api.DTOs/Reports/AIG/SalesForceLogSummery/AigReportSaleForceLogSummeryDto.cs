﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceIntegration;


namespace iPlatform.Api.DTOs.Reports.AIG.SalesForceLogSummery
{
   public class AigReportSaleForceLogSummeryDto : ICultureAware
    {
        public AigReportSaleForceLogSummeryDto()
        {
            SalesForceLogSummery = new List<AigReportSalesForceLogSummeryExtractDto>();

        }

        public List<AigReportSalesForceLogSummeryExtractDto> SalesForceLogSummery;
        public string Excel;
    }
}

