﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.AIG.SalesForceLogSummery.Criteria
{
   public class AigReportSalesForceLogSummeryExtractCriteriaDto : Resource, ICultureAware
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
