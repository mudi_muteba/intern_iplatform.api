﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Reports.AIG.SalesForceLogSummery
{
   public class AigReportSalesForceLogSummeryResultDto : Resource
    {
        public AigReportSalesForceLogSummeryResultDto()
        {
        }

        public string ExcelFileName;
    }
}