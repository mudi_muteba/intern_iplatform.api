﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Reports.AIG.SalesForceLogSummery
{
    public class AigReportSalesForceLogSummeryExtractDto
    {

        public AigReportSalesForceLogSummeryExtractDto()
        {

        }
        public virtual int Counter { get; set; }
        public virtual string Description { get; set; }
        public virtual int Total { get; set; }
    }
}