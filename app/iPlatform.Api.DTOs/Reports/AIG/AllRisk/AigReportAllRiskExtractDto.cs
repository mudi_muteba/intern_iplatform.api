﻿using System;

namespace iPlatform.Api.DTOs.Reports.AIG.AllRisk
{
    public class AigReportAllRiskExtractDto
    {
        public AigReportAllRiskExtractDto()
        {
        }

        public virtual string AgentName { get; set; }
        public virtual string Channel { get; set; }
        public virtual string DateOfQuote { get; set; }
        public virtual string QuoteExpired { get; set; }
        public virtual string QuoteBound { get; set; }
        public virtual string QuoteBindDate { get; set; }
        public virtual decimal QuoteTotalPremium { get; set; }
        public virtual decimal QuoteMotorPremium { get; set; }
        public virtual decimal QuoteHomePremium { get; set; }
        public virtual decimal QuoteFuneralPremium { get; set; }
        public virtual decimal QuoteOtherPremium { get; set; }
        public virtual string QuoteIncepptionDate { get; set; }
        public virtual string QuotePolicyNumber { get; set; }
        public virtual string QuoteCreateDate { get; set; }
        public virtual string QuoteCreateTime { get; set; }
        public virtual int LeadText { get; set; }
        public virtual string PropertyNumber { get; set; }
        public virtual int ItemNumber { get; set; }
        public virtual string AddressSuburb { get; set; }
        public virtual string AddressPostCode { get; set; }
        public virtual string AddressProvince { get; set; }
        public virtual string ItemCategory { get; set; }
        public virtual string ItemDescripption { get; set; }
        public virtual string ItemJewellerySafe { get; set; }
        public virtual string ItemReceiptsForItems { get; set; }
        public virtual string ItemSerialNumber { get; set; }
        public virtual string ItemDiscount { get; set; }
        public virtual string ItemSumInsured { get; set; }
    }
}
