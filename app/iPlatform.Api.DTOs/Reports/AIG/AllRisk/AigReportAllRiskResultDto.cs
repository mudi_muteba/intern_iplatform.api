﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Reports.AIG.AllRisk
{
    public class AigReportAllRiskResultDto : Resource
    {
        public AigReportAllRiskResultDto()
        {
        }

        public string ExcelFileName;
    }
}
