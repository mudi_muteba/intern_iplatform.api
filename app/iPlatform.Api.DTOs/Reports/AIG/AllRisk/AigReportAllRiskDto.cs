﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Reports.AIG.AllRisk
{
    public class AigReportAllRiskDto
    {
        public AigReportAllRiskDto()
        {
            AllRisks = new List<AigReportAllRiskExtractDto>();

        }

        public List<AigReportAllRiskExtractDto> AllRisks;
        public string Excel;
    }
}
