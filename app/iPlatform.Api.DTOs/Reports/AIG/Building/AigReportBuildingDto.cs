﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Reports.AIG.Building
{
    public class AigReportBuildingDto
    {
        public AigReportBuildingDto()
        {
            Buildings = new List<AigReportBuildingExtractDto>();

        }

        public List<AigReportBuildingExtractDto> Buildings;
        public string Excel;
    }
}
