﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.AIG.Building.Criteria
{
    public class AigReportBuildingExtractCriteriaDto : Resource, ICultureAware
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}