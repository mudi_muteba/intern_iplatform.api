﻿namespace iPlatform.Api.DTOs.Reports.AIG.Building
{
    public class AigReportBuildingExtractDto
    {
        public AigReportBuildingExtractDto()
        {
        }

        public string Channel { get; set; }
        public string QuoteExpired { get; set; }
        public string QuoteBound { get; set; }
        public string QuoteBindDate { get; set; }
        public decimal QuoteTotalPremium { get; set; }
        public decimal QuoteMotorPremium { get; set; }
        public decimal QuoteHomePremium { get; set; }
        public decimal QuoteFuneralPremium { get; set; }
        public decimal QuoteOtherPremium { get; set; }
        public decimal QuoteFees { get; set; }
        public decimal QuoteSasria { get; set; }
        public string QuoteIncepptionDate { get; set; }
        public string QuotePolicyNumber { get; set; }
        public string QuoteCreateDate { get; set; }
        public string QuoteCreateTime { get; set; }
        public int QuoteId { get; set; }
        public string Vmsaid { get; set; }
        public int PropertyNumber { get; set; }
        public string CoverType { get; set; }
        public string AddressSuburb { get; set; }
        public string AddressPostCode { get; set; }
        public string AddressProvince { get; set; }
        public string HomeType { get; set; }
        public string RoofConstruction { get; set; }
        public string WallConstruction { get; set; }
        public string ThatchLapa { get; set; }
        public string ThatchLapaSize { get; set; }
        public string LightningConductor { get; set; }
        public string PropertyBorder { get; set; }
        public string ConstructionYear { get; set; }
        public string SquareMetresOfProperty { get; set; }
        public string NumberOfBathrooms { get; set; }
        public string PropertyUnderConstruction { get; set; }
        public string HomeUsage { get; set; }
        public string BusinessConducted { get; set; }
        public string Commune { get; set; }
        public string PeriodPropertyIsUnoccupied { get; set; }
        public string Ownership { get; set; }
        public string MortgageBank { get; set; }
        public string SecurityGatedCommunity { get; set; }
        public string SecuritySecurityComplex { get; set; }
        public string SecurityBurglarBars { get; set; }
        public string SecuritySecurityGates { get; set; }
        public string SecurityAlarmWithArmedResponse { get; set; }
        public string VapsWaterPumpingMachinery { get; set; }
        public string VapsAccidentalDamage { get; set; }
        public string VapsSubsidenceAndLandslip { get; set; }
        public string BuildingsAdditionalExcess { get; set; }
        public string SumInsured { get; set; }
        public string DiscretionaryDiscount { get; set; }
        public decimal Sasria { get; set; }
        public decimal PremiumRisk { get; set; }
        public decimal PremiumSubsidenceAndLandslip { get; set; }
        public decimal PremiumWaterMachinery { get; set; }
        public decimal PremiumTotal { get; set; }
    }
}