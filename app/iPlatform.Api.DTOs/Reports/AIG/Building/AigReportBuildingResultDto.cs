﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Reports.AIG.Building
{
    public class AigReportBuildingResultDto : Resource
    {
        public AigReportBuildingResultDto()
        {
        }

        public string ExcelFileName;
    }
}
