﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Base
{ 
    public class ReportScheduleParamDto : Resource, ICultureAware
    {
        public ReportParamDto ReportParam { get; set; }
        public string Value { get; set; }
    }
}
