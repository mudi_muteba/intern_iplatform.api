﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Individual;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ReportEmailDto : Resource, ICultureAware
    {
        public ReportEmailDto()
        {
            Recipients = new List<IndividualDto>();
            Sender = new IndividualDto();
        }

        public IndividualDto Sender;
        public List<IndividualDto> Recipients;
    }
}