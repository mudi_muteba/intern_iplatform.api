﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.ReportSchedules;

namespace iPlatform.Api.DTOs.Reports.Base
{ 
    public class ReportScheduleHistoryDto : Resource, ICultureAware
    {
        public ReportScheduleHistoryDto() { }

        public virtual ReportScheduleDto ReportSchedule { get; set; }

        public virtual ReportDto Report { get; set; }

        public virtual bool Success { get; set; }

        public virtual string Errors { get; set; }
    }
}
