﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.ReportSchedules;

namespace iPlatform.Api.DTOs.Reports.Base
{ 
    public class ReportScheduleCampaignDto : Resource, ICultureAware
    {
        public ReportScheduleDto ReportSchedule { get; set; }
        public CampaignDto Campaign { get; set; }
    }
}
