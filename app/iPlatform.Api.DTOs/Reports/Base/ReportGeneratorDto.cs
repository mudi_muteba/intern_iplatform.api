﻿using System.Collections.Generic;
using System.Linq;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Reports.ComparativeQuote;

using MasterData;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Reports.Base
{
    public class ReportGeneratorDto :  IExecutionDto, IValidationAvailableDto, ICultureAware
    {
        public ReportGeneratorDto()
        {
            Context = DtoContext.NoContext();
            Email = new ReportEmailDto();
            Format = ExportTypes.PDF;
            PathMap = string.Empty;
            Parameters = new Dictionary<string, object>();
            Reports = new List<ReportDto>();
        }

        public int ChannelId { get; set; }

        public List<ReportDto> Reports { get; set; }
        public string PathMap { get; set; }
        public Dictionary<string, object> Parameters { get; set; }
        public ExportType Format { get; set; }
        public DtoContext Context { get; set; }

        public ReportEmailDto Email { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();
            return list;
        }

    }
}