﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.ReportSchedules;

namespace iPlatform.Api.DTOs.Reports.Base
{
    public class ReportScheduleReportDto : Resource, ICultureAware
    {
        public ReportScheduleDto ReportSchedule { get; set; }
        public ReportDto Report { get; set; }
    }
}
