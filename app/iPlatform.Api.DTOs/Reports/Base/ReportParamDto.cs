﻿using MasterData;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Base
{
    public class ReportParamDto : Resource, ICultureAware
    {
        public ReportParamDto() { }

        public int ReportId { get; set; }

        public ReportDto Report { get; set; }

        public string Field { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DataType DataType { get; set; }
        public FontAwesomeIcon Icon { get; set; }
        public int VisibleIndex { get; set; }
        public bool IsVisibleInReportViewer { get; set; }
        public bool IsVisibleInScheduler { get; set; }
    }
}