﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.Reports.Base
{
    public class ReportDocumentDefinitionDto : Resource, ICultureAware
    {
        public ReportDocumentDefinitionDto()
        {
        }

        public int ReportId { get; set; }
        public DocumentDefinition DocumentDefinitions { get; set; }
    }
}