﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Base.Criteria
{
    public class ReportDocumentDefinitionCriteriaDto : Resource, ICultureAware
    {
        public int ReportId { get; set; }
    }
}
