﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;

using ValidationMessages;

namespace iPlatform.Api.DTOs.Reports.Base
{
    public class CreateMultipleReportsDto : AttributeValidationDto, IExecutionDto
    {
        public CreateMultipleReportsDto()
        {
            Reports = new List<CreateReportDto>();
        }

        public List<CreateReportDto> Reports { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
