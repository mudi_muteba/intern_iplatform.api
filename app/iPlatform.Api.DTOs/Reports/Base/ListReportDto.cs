﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Base
{
    public class ListReportDto : Resource, ICultureAware
    {
        public List<ReportDto> Reports {get; set;}
    }
}
