﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Base
{ 
    public class ReportEmailResponseDto : Resource, ICultureAware
    {
        public string Body { get; set; }
        public bool Success { get; set; }
    }
}
