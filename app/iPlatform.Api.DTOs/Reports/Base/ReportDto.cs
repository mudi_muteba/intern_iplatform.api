﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.Reports.Base
{ 
    public class ReportDto : Resource, ICultureAware
    {
        public ReportDto()
        {
            Parameters = new List<ReportParamDto>();
            Layouts = new List<ReportLayoutDto>();
            DocumentDefinitions = new List<ReportDocumentDefinitionDto>();
        }

        public IList<ReportParamDto> Parameters { get; set; }
        public IList<ReportLayoutDto> Layouts { get; set; }
        public IList<ReportDocumentDefinitionDto> DocumentDefinitions { get; set; }

        public ReportCategory ReportCategory { get; set; }
        public ReportType ReportType { get; set; }
        public ExportType ReportFormat { get; set; }

        public int ReportId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SourceFile { get; set; }
        public string Info { get; set; }

        public bool IsVisibleInReportViewer { get; set; }
        public bool IsVisibleInScheduler { get; set; }
        public bool IsActive { get; set; }
        public int VisibleIndex { get; set; }
        public string Icon { get; set; }
    }
}
