﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Base
{ 
    public class ReportEmailResultDto : Resource, ICultureAware
    {
        public bool Sent { get; set; }
    }
}
