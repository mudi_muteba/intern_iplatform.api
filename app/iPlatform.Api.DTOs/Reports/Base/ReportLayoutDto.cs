﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Base
{ 
    public class ReportLayoutDto : Resource, ICultureAware
    {
        public ReportDto Report { get; set; }

        public int ReportId { get; set; }

        public string Name { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }

        public bool IsVisible { get; set; }
    }
}
