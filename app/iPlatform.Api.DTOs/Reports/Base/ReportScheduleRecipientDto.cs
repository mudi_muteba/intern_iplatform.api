using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.ReportSchedules;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Reports.Base
{
    public class ReportScheduleRecipientDto : Resource, ICultureAware
    {
        public ReportScheduleDto ReportSchedule { get; set; }
        public BasicUserDto Recipient { get; set; }
    }
}