﻿using System;
using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;

using MasterData;

using ValidationMessages;

namespace iPlatform.Api.DTOs.Reports.Base
{
    public class CreateReportDto : AttributeValidationDto, IExecutionDto
    {
        public CreateReportDto()
        {
            ReportParameters = new List<ReportParamDto>();
            ReportDocumentDefinitions = new List<ReportDocumentDefinitionDto>();
            ReportLayouts = new List<ReportLayoutDto>();
        }

        public int ChannelId { get; set; }
        public Guid ChannelSystemId { get; set; }

        public List<ReportParamDto> ReportParameters { get; set; }
        public List<ReportDocumentDefinitionDto> ReportDocumentDefinitions { get; set; }
        public List<ReportLayoutDto> ReportLayouts { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string SourceFile { get; set; }
        public string Info { get; set; }
        public string Content { get; set; }
        public string ContentType { get; set; }
        public string Extension { get; set; }

        public bool IsVisibleInReportViewer { get; set; }
        public bool IsVisibleInScheduler { get; set; }
        public bool IsActive { get; set; }

        public int VisibleIndex { get; set; }
        public FontAwesomeIcon Icon { get; set; }

        public ReportCategory ReportCategory { get; set; }
        public ReportType ReportType { get; set; }
        public ExportType ExportType { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
