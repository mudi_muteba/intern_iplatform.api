﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Reports.Base
{ 
    public class ReportScheduleResponseDto : Resource, ICultureAware
    {
        public ReportScheduleResponseDto()
        {
            Reports = new List<string>();
        }

        public List<string> Reports { get; set; }
    }
}
