﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments
{
    public class DebitOrderReportDto : Resource, ICultureAware
    {
        public DebitOrderReportDto()
        {
            DebitOrderData = new DebitOrderReportExtractDto();
            HeaderData = new ReportHeaderExtractDto();
        }

        public DebitOrderReportExtractDto DebitOrderData { get; set; }
        public ReportHeaderExtractDto HeaderData { get; set; }
    }
}
