﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments
{
    public class PopiDocReportDto : Resource, ICultureAware
    {
        public PopiDocReportDto()
        {
            PopiDocData = new List<PopiDocReportExtractDto>();
            HeaderData = new ReportHeaderExtractDto();
        }

        public List<PopiDocReportExtractDto> PopiDocData { get; set; }
        public ReportHeaderExtractDto HeaderData { get; set; }
    }
}
