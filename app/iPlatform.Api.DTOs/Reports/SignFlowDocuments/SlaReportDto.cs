﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments
{
    public class SlaReportDto : Resource, ICultureAware
    {
        public SlaReportDto()
        {
            SlaData = new SlaReportExtractDto();
            HeaderData = new ReportHeaderExtractDto();
        }

        public SlaReportExtractDto SlaData { get; set; }
        public ReportHeaderExtractDto HeaderData { get; set; }
    }
}
