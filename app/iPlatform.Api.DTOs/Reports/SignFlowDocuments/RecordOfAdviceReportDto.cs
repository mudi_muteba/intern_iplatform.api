﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments
{
    public class RecordOfAdviceReportDto : Resource, ICultureAware
    {
        public RecordOfAdviceReportDto()
        {
            RecordOfAdviceData = new List<RecordOfAdviceReportExtractDto>();
            RecordOfAdviceContactData =  new RecordOfAdviceContactReportExtractDto();
            HeaderData = new ReportHeaderExtractDto();
        }

        public List<RecordOfAdviceReportExtractDto> RecordOfAdviceData { get; set; }

        public RecordOfAdviceContactReportExtractDto RecordOfAdviceContactData { get; set; }
        public ReportHeaderExtractDto HeaderData { get; set; }
    }
}
