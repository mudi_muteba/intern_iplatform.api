﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments
{
    public class RecordOfAdviceContactReportExtractDto : Resource, ICultureAware
    {
        public string InsuredName { get; set; }
        public string InsuredId { get; set; }
        public string Broker { get; set; }
        public string Occupation { get; set; }
        public string EmailAddress { get; set; }
        public string ContactWork { get; set; }
        public string ContactMobile { get; set; }
        public string ContactHome { get; set; }
        public string PostalAddress { get; set; }
        public string PhysicalAddress { get; set; }
    }
}
