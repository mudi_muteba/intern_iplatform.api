﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments
{
    public class DebitOrderReportExtractDto : Resource, ICultureAware
    {
        public string InsuredName { get; set; }
        public string InsuredId { get; set; }
        public string ContactWork { get; set; }
        public string ContactMobile { get; set; }
        public string ContactHome { get; set; }
        public string PostalAddress { get; set; }
        public string BankDetailAccountName { get; set; }
        public string BankDetailsBankName { get; set; }
        public string BankDetailBranchName { get; set; }
        public string BankDetailBranchCode { get; set; }
        public string BankDetailAccountNumber { get; set; }
        public string BankDetailAccountType { get; set; }
        public string QuotePolicyNo { get; set; }
    }
}
