﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments
{
    public class BrokerNoteReportDto : Resource, ICultureAware
    {
        public BrokerNoteReportDto()
        {
            BrokerNoteData = new BrokerNoteReportExtractDto();
            HeaderData = new ReportHeaderExtractDto();
        }

        public BrokerNoteReportExtractDto BrokerNoteData { get; set; }
        public ReportHeaderExtractDto HeaderData { get; set; }
    }
}
