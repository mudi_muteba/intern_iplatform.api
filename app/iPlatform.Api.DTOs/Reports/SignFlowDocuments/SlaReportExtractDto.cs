﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments
{
    public class SlaReportExtractDto : Resource, ICultureAware
    {
        public SlaReportExtractDto()
        {

        }
        public string BrokerName { get; set; }
        public string ClientName { get; set; }
        public string ClientIndentityNo { get; set; }
        public string PostalAddress { get; set; }
    }
}
