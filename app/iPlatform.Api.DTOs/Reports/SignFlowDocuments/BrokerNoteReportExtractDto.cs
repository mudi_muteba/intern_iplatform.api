﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments
{
    public class BrokerNoteReportExtractDto : Resource, ICultureAware
    {
        public BrokerNoteReportExtractDto()
        {

        }
        public string Insured { get; set; }
        public string Appointee { get; set; }
    }
}
