﻿namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria
{
    public class BrokerNoteReportCriteriaDto
    {
        public BrokerNoteReportCriteriaDto()
        {

        }

        public int Id { get; set; }

        public string InsurerCode { get; set; }
    }
}
