﻿namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria
{
    public class DebitOrderReportCriteriaDto
    {
        public DebitOrderReportCriteriaDto()
        {

        }

        public int Id { get; set; }
        public string InsurerCode { get; set; }
    }
}
