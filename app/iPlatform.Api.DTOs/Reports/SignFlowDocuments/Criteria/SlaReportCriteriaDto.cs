﻿namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria
{
    public class SlaReportCriteriaDto
    {
        public SlaReportCriteriaDto()
        {

        }

        public int Id { get; set; }
        public string InsurerCode { get; set; }
    }
}
