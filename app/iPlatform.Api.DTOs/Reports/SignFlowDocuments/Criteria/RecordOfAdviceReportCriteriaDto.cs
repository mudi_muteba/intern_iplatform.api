﻿namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria
{
    public class RecordOfAdviceReportCriteriaDto
    {
        public RecordOfAdviceReportCriteriaDto()
        {

        }

        public int Id { get; set; }
        public string InsurerCode { get; set; }
    }
}
