﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments
{
    public class PopiDocReportExtractDto : Resource, ICultureAware
    {
        public PopiDocReportExtractDto()
        {

        }
        public string Client { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }
}
