﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments
{
    public class ReportHeaderExtractDto : Resource, ICultureAware
    {
        public ReportHeaderExtractDto()
        {

        }
        public string CompanyAddress { get; set; }
        public string CompanyWorkTelephone { get; set; }
        public string CompanyMobileTelephone { get; set; }
    }
}
