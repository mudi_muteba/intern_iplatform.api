﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.SignFlowDocuments
{
    public class RecordOfAdviceReportExtractDto : Resource, ICultureAware
    {
        public RecordOfAdviceReportExtractDto()
        {

        }
        public string Name { get; set; }
        public string Suminsured { get; set; }
        public string Description { get; set; }
    }
}
