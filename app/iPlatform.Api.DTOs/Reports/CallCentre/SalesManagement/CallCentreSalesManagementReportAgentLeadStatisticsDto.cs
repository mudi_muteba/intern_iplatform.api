﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.SalesManagement
{
    public class CallCentreSalesManagementReportAgentLeadStatisticsDto : Resource, ICultureAware
    { 
        public string Agent { get; set; }
        public string Campaign { get; set; }

        public int DailySalesTarget { get; set; }
        public int ActualSales { get; set; }
        public int MTDSalesTarget { get; set; }
        public int MTDSales { get; set; }
        public int ProjectedSales { get; set; }
        public decimal AttainmentToSalesTarget { get; set; }
        public decimal PremiumTarget { get; set; }
        public decimal ActualPremium { get; set; }
        public decimal MTDPremiumTarget { get; set; }
        public decimal MTDPremium { get; set; }
        public decimal ProjectedPremium { get; set; }
        public decimal AttainmentToPremiumTarget { get; set; }
    }
}
