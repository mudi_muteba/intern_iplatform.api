﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.SalesManagement
{
    public class CallCentreSalesManagementReportHeaderDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
    }
}
