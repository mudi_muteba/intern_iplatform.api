﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.SalesManagement
{
    public class CallCentreSalesManagementReportContainerDto : Resource, ICultureAware
    { 
        public CallCentreSalesManagementReportHeaderDto Header { get; set; }
        public List<CallCentreSalesManagementReportAgentLeadStatisticsDto> Statistics { get; set; }
    }
}
