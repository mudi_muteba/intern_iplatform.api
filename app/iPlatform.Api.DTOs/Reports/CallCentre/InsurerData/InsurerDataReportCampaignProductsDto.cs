﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.InsurerData
{
    public class InsurerDataReportCampaignProductsDto : Resource, ICultureAware
    {
        public int ProductOwnerId { get; set; }
        public string ProductOwnerName { get; set; }
        public int ProductProviderId { get; set; }
        public string ProductProviderName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
    }
}
