﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.InsurerData
{
    public class InsurerDataReportGeneralStatisticsDto : Resource, ICultureAware
    {
        public decimal AverageNoClaimBonus { get; set; }
        public string AverageSumInsured { get; set; }
    }
}
