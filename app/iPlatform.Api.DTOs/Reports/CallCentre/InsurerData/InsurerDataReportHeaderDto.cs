﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.InsurerData
{
    public class InsurerDataReportHeaderDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
    }
}
