﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.InsurerData
{
    public class InsurerDataReportMaritalStatusStatisticsDto : Resource, ICultureAware
    {
        public string Label { get; set; }
        public string Category { get; set; }
        public decimal Total { get; set; }
        public decimal Percentage { get; set; }
    }
}
