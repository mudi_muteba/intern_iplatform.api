﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.Reports.CallCentre.InsurerData
{
    public class InsurerDataReportContainerDto : Resource, ICultureAware
    { 
        public InsurerDataReportContainerDto()
        {
            Header = new InsurerDataReportHeaderDto();
            GeneralStatistics = new InsurerDataReportGeneralStatisticsDto();

            Statistics = new List<InsurerDataReportInsurerQuoteStatisticsDto>();
            AgeStatistics = new List<InsurerDataReportAgeStatisticsDto>();
            ClassUseStatistics = new List<InsurerDataReportClassUseStatisticsDto>();
            CoverTypeStatistics = new List<InsurerDataReportCoverTypeStatisticsDto>();
            GenderStatistics = new List<InsurerDataReportGenderStatisticsDto>();
            InsuranceTypeStatistics = new List<InsurerDataReportInsuranceTypeStatisticsDto>();
            MaritalStatusStatistics = new List<InsurerDataReportMaritalStatusStatisticsDto>();
            ProvinceStatistics = new List<InsurerDataReportProvinceStatisticsDto>();
            StatisticsByProduct = new List<InsurerDataReportContainerDto>();
            VehicleColourStatistics = new List<InsurerDataReportVehicleColourStatisticsDto>();
            VehicleMakeStatistics = new List<InsurerDataReportVehicleMakeStatisticsDto>();
        }

        public InsurerDataReportHeaderDto Header { get; set; }
        public List<InsurerDataReportInsurerQuoteStatisticsDto> Statistics { get; set; }

        public InsurerDataReportCampaignProductsDto Product { get; set; }

        public List<InsurerDataReportAgeStatisticsDto> AgeStatistics { get; set; }
        public List<InsurerDataReportClassUseStatisticsDto> ClassUseStatistics { get; set; }
        public List<InsurerDataReportCoverTypeStatisticsDto> CoverTypeStatistics { get; set; }
        public List<InsurerDataReportGenderStatisticsDto> GenderStatistics { get; set; }
        public InsurerDataReportGeneralStatisticsDto GeneralStatistics { get; set; }
        public List<InsurerDataReportInsuranceTypeStatisticsDto> InsuranceTypeStatistics { get; set; }
        public List<InsurerDataReportMaritalStatusStatisticsDto> MaritalStatusStatistics { get; set; }
        public List<InsurerDataReportProvinceStatisticsDto> ProvinceStatistics { get; set; }
        public List<InsurerDataReportVehicleColourStatisticsDto> VehicleColourStatistics { get; set; }
        public List<InsurerDataReportVehicleMakeStatisticsDto> VehicleMakeStatistics { get; set; }

        public List<InsurerDataReportContainerDto> StatisticsByProduct { get; set; }
    }
}