﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.InsurerQuoteBreakdown
{
    public class InsurerQuoteBreakdownReportHeaderDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
    }
}
