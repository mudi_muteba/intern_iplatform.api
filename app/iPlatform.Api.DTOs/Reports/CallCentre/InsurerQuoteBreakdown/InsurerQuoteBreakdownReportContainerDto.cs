﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.InsurerQuoteBreakdown
{
    public class InsurerQuoteBreakdownReportContainerDto : Resource, ICultureAware
    { 
        public InsurerQuoteBreakdownReportContainerDto()
        {
            Statistics = new List<InsurerQuoteBreakdownReportInsurerQuoteStatisticsDto>();
        }

        public InsurerQuoteBreakdownReportHeaderDto Header { get; set; }
        public List<InsurerQuoteBreakdownReportInsurerQuoteStatisticsDto> Statistics { get; set; }
    }
}
