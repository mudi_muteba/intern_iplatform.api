﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.InsurerQuoteBreakdown
{
    public class InsurerQuoteBreakdownReportInsurerQuoteStatisticsDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
        public string Product { get; set; }
        public string Campaign { get; set; }

        public int Quotes { get; set; }
        public int Sales { get; set; }
        public decimal Closing { get; set; }
        public decimal AveragePremiumPerAcceptedQuote { get; set; }
        public decimal AveragePremiumPerQuote { get; set; }
    }
}