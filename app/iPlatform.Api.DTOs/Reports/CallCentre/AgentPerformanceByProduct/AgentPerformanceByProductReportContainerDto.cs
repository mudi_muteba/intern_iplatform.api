﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformanceByProduct
{
    public class AgentPerformanceByProductReportContainerDto : Resource, ICultureAware
    { 
        public AgentPerformanceByProductReportContainerDto()
        {
            Statistics = new List<AgentPerformanceByProductReportAgentQuoteStatisticsDto>();
        }

        public AgentPerformanceByProductReportHeaderDto Header { get; set; }
        public List<AgentPerformanceByProductReportAgentQuoteStatisticsDto> Statistics { get; set; }
    }
}
