﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformanceByProduct
{
    public class AgentPerformanceByProductReportAgentQuoteStatisticsDto : Resource, ICultureAware
    {
        public string Channel { get; set; }
        public string Campaign { get; set; }
        public string Agent { get; set; }
        public string Insurer { get; set; }
        public string Product { get; set; }

        public int Leads { get; set; }
        public int Quotes { get; set; }
        public decimal QuotesPerLead { get; set; }
        public int SecondLevelUnderwriting { get; set; }
        public int Sales { get; set; }

        public decimal Closing { get; set; }
        public decimal InsurerClosing { get; set; }
        public decimal InsurerDropOff { get; set; }
        public decimal AveragePremiumPerAcceptedQuote { get; set; }
    }
}
