﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformanceByProduct
{
    public class AgentPerformanceByProductReportHeaderDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
    }
}
