﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.ManagerLevelQuoteUpload
{
    public class ManagerLevelQuoteUploadReportContainerDto : Resource, ICultureAware
    { 
        public ManagerLevelQuoteUploadReportContainerDto()
        {
            Statistics = new List<ManagerLevelQuoteUploadReportUploadStatisticsDto>();
        }

        public ManagerLevelQuoteUploadReportHeaderDto Header { get; set; }
        public List<ManagerLevelQuoteUploadReportUploadStatisticsDto> Statistics { get; set; }
    }
}
