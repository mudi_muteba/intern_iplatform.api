﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.ManagerLevelQuoteUpload
{
    public class ManagerLevelQuoteUploadReportHeaderDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
    }
}
