﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.LeadManagement
{
    public class LeadManagementReportHeaderDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
    }
}
