﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.LeadManagement
{
    public class LeadManagementReportAgentLeadStatisticsDto : Resource, ICultureAware
    { 
        public string Agent { get; set; }
        public string Campaign { get; set; }

        public int Received { get; set; }
        public int Contacted { get; set; }
        public int Uncontactable { get; set; }
        public int Pending { get; set; }
        public int Completed { get; set; }
        public int Unactioned { get; set; }
    }
}
