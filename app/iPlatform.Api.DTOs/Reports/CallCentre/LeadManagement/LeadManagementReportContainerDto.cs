﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.LeadManagement
{
    public class LeadManagementReportContainerDto : Resource, ICultureAware
    { 
        public LeadManagementReportContainerDto()
        {
            Statistics = new List<LeadManagementReportAgentLeadStatisticsDto>();
        }

        public LeadManagementReportHeaderDto Header { get; set; }
        public List<LeadManagementReportAgentLeadStatisticsDto> Statistics { get; set; }
    }
}
