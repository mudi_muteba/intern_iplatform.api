﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.User
{
    public class UserReportContainerDto : Resource, ICultureAware
    { 
        public UserReportContainerDto()
        {
            Statistics = new List<UserReportUserStatisticsDto>();
        }

        public UserReportHeaderDto Header { get; set; }
        public List<UserReportUserStatisticsDto> Statistics { get; set; }
    }
}
