﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.User
{
    public class UserReportHeaderDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
    }
}
