﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.User
{
    public class UserReportUserStatisticsDto : Resource, ICultureAware
    {
        public string Channel { get; set; }
        public string Role { get; set; }
        public string Agent { get; set; }
        public string EMail { get; set; }
        public string Active { get; set; }
        public string Billable { get; set; }
    }
}
