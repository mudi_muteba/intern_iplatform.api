﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformance
{
    public class AgentPerformanceReportHeaderDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
    }
}
