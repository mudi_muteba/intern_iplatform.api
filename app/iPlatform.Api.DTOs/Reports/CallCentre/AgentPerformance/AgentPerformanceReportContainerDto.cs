﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformance
{
    public class AgentPerformanceReportContainerDto : Resource, ICultureAware
    { 
        public AgentPerformanceReportContainerDto()
        {
            Statistics = new List<AgentPerformanceReportAgentQuoteStatisticsDto>();
        }

        public AgentPerformanceReportHeaderDto Header { get; set; }
        public List<AgentPerformanceReportAgentQuoteStatisticsDto> Statistics { get; set; }
    }
}
