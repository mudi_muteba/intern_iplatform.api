﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.OutboundRatios
{
    public class CallCentreOutboundRatiosReportContainerDto : Resource, ICultureAware
    { 
        public CallCentreOutboundRatiosReportHeaderDto Header { get; set; }
        public List<CallCentreOutboundRatiosReportAgentLeadStatisticsDto> Statistics { get; set; }
    }
}
