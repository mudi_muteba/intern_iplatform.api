﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.OutboundRatios
{
    public class CallCentreOutboundRatiosReportHeaderDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
    }
}
