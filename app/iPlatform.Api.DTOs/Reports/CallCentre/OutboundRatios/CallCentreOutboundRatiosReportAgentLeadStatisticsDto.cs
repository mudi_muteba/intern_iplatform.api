﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.OutboundRatios
{
    public class CallCentreOutboundRatiosReportAgentLeadStatisticsDto : Resource, ICultureAware
    { 
        public string Agent { get; set; }
        public string Campaign { get; set; }

        public decimal LeadToSales { get; set; }
        public decimal ContactedToSales { get; set; }
        public decimal ContactedToLeadsWorked { get; set; }
        public decimal SalesToPresentation { get; set; }
        public decimal PresentationToLeads { get; set; }
    }
}
