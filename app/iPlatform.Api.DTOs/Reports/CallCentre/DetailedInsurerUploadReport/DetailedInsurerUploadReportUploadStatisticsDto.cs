﻿using System;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.DetailedInsurerUpload
{
    public class DetailedInsurerUploadReportUploadStatisticsDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
        public string Product { get; set; }
        public string Agent { get; set; }
        public string Client { get; set; }
        public string InsurerReference { get; set; }
        public DateTime UploadDate { get; set; }
        public decimal PremiumValue { get; set; }
    }
}
