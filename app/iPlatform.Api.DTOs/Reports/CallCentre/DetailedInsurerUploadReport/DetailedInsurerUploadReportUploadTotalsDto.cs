﻿using System;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.DetailedInsurerUpload
{
    public class DetailedInsurerUploadReportUploadTotalsDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
        public string Product { get; set; }
        public int Uploads { get; set; }
        public decimal AveragePremiumValue { get; set; }
        public decimal TotalPremiumValue { get; set; }
    }
}
