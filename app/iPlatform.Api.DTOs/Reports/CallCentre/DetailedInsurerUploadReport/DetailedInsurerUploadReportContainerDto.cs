﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.DetailedInsurerUpload
{
    public class DetailedInsurerUploadReportContainerDto : Resource, ICultureAware
    { 
        public DetailedInsurerUploadReportContainerDto()
        {
            Statistics = new List<DetailedInsurerUploadReportUploadStatisticsDto>();
        }

        public DetailedInsurerUploadReportHeaderDto Header { get; set; }
        public List<DetailedInsurerUploadReportUploadStatisticsDto> Statistics { get; set; }
        public List<DetailedInsurerUploadReportUploadTotalsDto> Totals { get; set; }
    }
}
