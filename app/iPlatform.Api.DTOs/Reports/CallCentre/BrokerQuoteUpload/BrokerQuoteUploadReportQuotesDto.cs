﻿using System;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.BrokerQuoteUpload
{
    public class BrokerQuoteUploadReportQuotesDto : Resource, ICultureAware
    {
        public string BrokerName { get; set; }
        public string Campaign { get; set; }
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
        public string QuoteNumber { get; set; }
        public string ContactCentreAgent { get; set; }
        public DateTime QuoteCreateDate { get; set; }
        public decimal QuotedPremium { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public decimal BrokerFee { get; set; }
    }
}
