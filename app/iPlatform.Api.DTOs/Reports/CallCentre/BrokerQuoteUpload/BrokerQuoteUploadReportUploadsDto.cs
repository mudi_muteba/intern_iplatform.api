﻿using System;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.BrokerQuoteUpload
{
    public class BrokerQuoteUploadReportUploadsDto : Resource, ICultureAware
    {
        public string BrokerName { get; set; }
        public string Campaign { get; set; }
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
        public string QuoteNumber { get; set; }
        public string ContactCentreAgent { get; set; }
        public DateTime DateTimeUploaded { get; set; }
        public decimal QuotedPremium { get; set; }
    }
}
