﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;


namespace iPlatform.Api.DTOs.Reports.CallCentre.BrokerQuoteUpload
{
    public class BrokerQuoteUploadReportContainerDto : Resource, ICultureAware
    { 
        public BrokerQuoteUploadReportContainerDto()
        {
            Summary = new List<BrokerQuoteUploadReportSummaryDto>();
            Quotes = new List<BrokerQuoteUploadReportQuotesDto>();
            Uploads = new List<BrokerQuoteUploadReportUploadsDto>();
        }

        public BrokerQuoteUploadReportHeaderDto Header { get; set; }
        public List<BrokerQuoteUploadReportSummaryDto> Summary { get; set; }
        public List<BrokerQuoteUploadReportQuotesDto> Quotes { get; set; }
        public List<BrokerQuoteUploadReportUploadsDto> Uploads { get; set; }
    }
}
