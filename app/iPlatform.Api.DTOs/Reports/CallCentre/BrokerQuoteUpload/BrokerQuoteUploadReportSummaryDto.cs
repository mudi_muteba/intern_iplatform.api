﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.BrokerQuoteUpload
{
    public class BrokerQuoteUploadReportSummaryDto : Resource, ICultureAware
    {
        public string BrokerName { get; set; }
        public string Agent { get; set; }
        public int LeadsCount { get; set; }
        public int NoOfQuotes { get; set; }
        public int NoOfQuotesUploaded { get; set; }
        public decimal Closing { get; set; }
        public decimal LeadsSPV { get; set; }
    }
}
