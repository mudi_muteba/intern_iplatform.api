﻿using System;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.CallCentre.Criteria
{
    public class CallCentreReportCriteriaDto : Resource, ICultureAware
    {
        public int UserId { get; set; }
        public int ChannelId { get; set; }
        public int CampaignId { get; set; }
        public string ChannelIds { get; set; }
        public string CampaignIds { get; set; }
        public string ProductIds { get; set; }
        public string InsurerIds { get; set; }
        public int CoverId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
