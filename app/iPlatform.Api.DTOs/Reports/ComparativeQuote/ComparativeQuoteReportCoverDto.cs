﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportCoverDto : Resource, ICultureAware
    {
        public ComparativeQuoteReportCoverDto()
        {
            Assets = new List<ComparativeQuoteReportInsurerCoverAssetDto>();
            All = new List<ComparativeQuoteReportInsurerCoverBenefitDto>();
            Header = new List<ComparativeQuoteReportInsurerCoverBenefitDto>();
            Totals = new List<ComparativeQuoteReportInsurerCoverBenefitDto>();
            CommonBenefits = new List<ComparativeQuoteReportInsurerCoverBenefitDto>();
            DifferingBenefits = new List<ComparativeQuoteReportInsurerCoverBenefitDto>();
            Organizations = new List<ComparativeQuoteReportOrganizationDto>();
            AssetList = new List<ComparativeQuoteReportInsurerAssetDto>();
        }

        public int CoverId { get; set; }
        public int CoverDefinitionId { get; set; }
        public string Description { get; set; }

        public int Count { get; set; }

        public List<ComparativeQuoteReportInsurerCoverAssetDto> Assets { get; set; }
        public List<ComparativeQuoteReportInsurerAssetDto> AssetList { get; set; }
        public List<ComparativeQuoteReportInsurerCoverBenefitDto> All { get; set; }
        public List<ComparativeQuoteReportInsurerCoverBenefitDto> Header { get; set; }
        public List<ComparativeQuoteReportInsurerCoverBenefitDto> Totals { get; set; }
        public List<ComparativeQuoteReportInsurerCoverBenefitDto> CommonBenefits { get; set; }
        public List<ComparativeQuoteReportInsurerCoverBenefitDto> DifferingBenefits { get; set; }
        public List<ComparativeQuoteReportOrganizationDto> Organizations { get; set; }

        public bool DisplayExcess { get; set; }
    }
}
