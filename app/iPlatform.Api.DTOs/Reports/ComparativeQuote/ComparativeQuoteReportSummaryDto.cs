﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportSummaryDto : Resource, ICultureAware
    {
        public ComparativeQuoteReportSummaryDto()
        {

        }
        public string Insurer { get; set; }
        public string Code { get; set; }
        public string Product { get; set; }
        public decimal BasicExcess { get; set; }
        public decimal Premium { get; set; }
        public decimal SASRIA { get; set; }
        public decimal Fees { get; set; }
        public decimal Total { get; set; }
        public decimal Commission { get; set; }
    }
}
