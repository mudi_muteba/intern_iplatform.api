﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportOrganizationDto : Resource, ICultureAware
    {
        public ComparativeQuoteReportOrganizationDto()
        {
            AdditionalExcessCategories = new List<ComparativeQuoteReportProductAdditionalExcessCategoryDto>();
        }
        public int OrganizationId { get; set; }
        public string TradingName { get; set; }
        public string Code { get; set; }
        public string CoverDefinitionIds { get; set; }
        public int Count { get; set; }

        public List<ComparativeQuoteReportProductAdditionalExcessCategoryDto> AdditionalExcessCategories { get; set; }
    }
}
