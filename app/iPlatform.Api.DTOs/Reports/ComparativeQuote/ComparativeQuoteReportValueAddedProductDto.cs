﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportValueAddedProductDto : Resource, ICultureAware
    {
        public ComparativeQuoteReportValueAddedProductDto()
        {

        }
        public string Code { get; set; }
        public string Product { get; set; }
        public string ValueAddedProducts { get; set; }
        public decimal Total { get; set; }
    }
}
