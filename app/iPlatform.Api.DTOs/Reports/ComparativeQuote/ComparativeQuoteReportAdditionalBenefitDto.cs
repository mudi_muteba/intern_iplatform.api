﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportAdditionalBenefitDto : Resource, ICultureAware
    {
        public ComparativeQuoteReportAdditionalBenefitDto() { }

        public string Name { get; set; }
        public string Value { get; set; }
    }
}
