﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportProductAdditionalExcessCategoryDto : Resource, ICultureAware
    {
        public ComparativeQuoteReportProductAdditionalExcessCategoryDto()
        {
            AdditionalExcess = new List<ComparativeQuoteReportProductAdditionalExcessDto>();
        }
        public string Category { get; set; }

        public bool ShouldApplySelectedExcess { get; set; }
        public bool ShouldDisplayExcessValues { get; set; }

        public List<ComparativeQuoteReportProductAdditionalExcessDto> AdditionalExcess { get; set; }
    }
}