﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportInsurerAssetDto : Resource, ICultureAware
    {
        public ComparativeQuoteReportInsurerAssetDto()
        {
            Details = new List<ComparativeQuoteReportInsurerCoverBenefitDto>();
        }
        public string Description { get; set; }
        public List<ComparativeQuoteReportInsurerCoverBenefitDto> Details { get; set; }
    }
}
