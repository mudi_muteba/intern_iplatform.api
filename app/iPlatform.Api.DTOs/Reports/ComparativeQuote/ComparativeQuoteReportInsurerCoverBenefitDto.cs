﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportInsurerCoverBenefitDto : Resource, ICultureAware
    {
        public ComparativeQuoteReportInsurerCoverBenefitDto()
        {

        }
        public string Description { get; set; }
        public string FirstSelectedInsurerBenefitValue { get; set; }
        public string SecondSelectedInsurerBenefitValue { get; set; }
        public string ThirdSelectedInsurerBenefitValue { get; set; }
    }
}
