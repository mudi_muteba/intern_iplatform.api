﻿using System.Collections.Generic;

using ValidationMessages;
using ValidationMessages.Reports;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportEmailDto :  IExecutionDto, IValidationAvailableDto, ICultureAware
    {
        public int ProposalHeaderId { get; set; }
        public string QuoteIds { get; set; }
        public string DocumentIds { get; set; }
        public string BaseUrl { get; set; }
        public string Email { get; set; }
        public string OrganizationCode { get; set; }
        public string QuoteUri { get; set; }
        public int PartyId { get; set; }
        public int ChannelId { get; set; }

        public string AgentName { get; set; }
        public string AgentEMail { get; set; }

        public string PathMap { get; set; }

        public DtoContext Context { get; set; }

        public ComparativeQuoteReportEmailDto() { }

        public ComparativeQuoteReportEmailDto(int proposalHeaderId, string email, string quoteUri, string organizationCode, int partyId)
        {
            ProposalHeaderId = proposalHeaderId;
            Email = email;
            QuoteUri = quoteUri;
            OrganizationCode = organizationCode;
            PartyId = partyId;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(ProposalHeaderId.ToString()))
                list.Add(ReportValidationMessages.ProposalHeaderIdRequired);

            if (string.IsNullOrWhiteSpace(PartyId.ToString()))
                list.Add(ReportValidationMessages.PartyIdRequired);

            if (string.IsNullOrWhiteSpace(QuoteUri.ToString()))
                list.Add(ReportValidationMessages.QuoteUriRequired);

            if (string.IsNullOrWhiteSpace(OrganizationCode))
                list.Add(ReportValidationMessages.OrganizationCodeRequired);

            if (string.IsNullOrWhiteSpace(Email))
                list.Add(ReportValidationMessages.EmailRequired);

            return list;
        }

    }
}