﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria
{
    public class ComparativeQuoteReportCriteriaDto
    {
        public ComparativeQuoteReportCriteriaDto()
        {
            QuestionIds = string.Empty;
            QuoteIds = string.Empty;
        }
        public int ChannelId { get; set; }
        public int ProposalHeaderId { get; set; }
        public string QuoteIds { get; set; }
        public string OrganizationCode { get; set; }
        public string ReportName { get; set; }
        public string ReportId { get; set; }
        public int CoverId { get; set; }
        public int CoverDefinitionId { get; set; }
        public string QuestionIds { get; set; }
    }
}
