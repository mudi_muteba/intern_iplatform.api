﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportProductAdditionalExcessDto : Resource, ICultureAware
    {
        public ComparativeQuoteReportProductAdditionalExcessDto() { }
        public int ProductAdditionalExcessId { get; set; }
        public int Index { get; set; }

        public string Category { get; set; }
        public string Description { get; set; }

        public decimal ActualExcess { get; set; }
        public decimal MinExcess { get; set; }
        public decimal MaxExcess { get; set; }

        public decimal Percentage { get; set; }

        public bool IsPercentageOfClaim { get; set; }
        public bool IsPercentageOfItem { get; set; }
        public bool IsPercentageOfSumInsured { get; set; }

        public bool ShouldApplySelectedExcess { get; set; }
        public bool ShouldDisplayExcessValues { get; set; }

    }
}
