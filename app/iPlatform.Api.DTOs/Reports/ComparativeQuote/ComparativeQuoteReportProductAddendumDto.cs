﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportProductAddendumDto : Resource, ICultureAware
    {
        public ComparativeQuoteReportProductAddendumDto()
        {

        }
        public string Insurer { get; set; }
        public string Product { get; set; }
        public int QuoteValidFor { get; set; }
        public string Addendum { get; set; }
    }
}
