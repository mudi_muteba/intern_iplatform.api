﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportContainerDto : Resource, ICultureAware
    {
        public ComparativeQuoteReportContainerDto()
        {
            Summary = new List<ComparativeQuoteReportSummaryDto>();
            ValueAddedProducts = new List<ComparativeQuoteReportValueAddedProductDto>();
            Covers = new List<ComparativeQuoteReportCoverDto>();
            Organizations = new List<ComparativeQuoteReportOrganizationDto>();
            ProductAddendums = new List<ComparativeQuoteReportProductAddendumDto>();
            AdditionalBenefits = new List<ComparativeQuoteReportAdditionalBenefitDto>();
        }

        public ComparativeQuoteReportSettingsDto Settings { get; set; }
        public ComparativeQuoteReportHeaderDto Header { get; set; }

        public List<ComparativeQuoteReportSummaryDto> Summary { get; set; }
        public List<ComparativeQuoteReportValueAddedProductDto> ValueAddedProducts { get; set; }
        public List<ComparativeQuoteReportCoverDto> Covers { get; set; }
        public List<ComparativeQuoteReportOrganizationDto> Organizations { get; set; }
        public List<ComparativeQuoteReportProductAddendumDto> ProductAddendums { get; set; }
        public List<ComparativeQuoteReportAdditionalBenefitDto> AdditionalBenefits { get; set; }

        public bool DisplayExcess { get; set; }
        public bool DisplayValueAddedProducts { get; set; }
    }
}
