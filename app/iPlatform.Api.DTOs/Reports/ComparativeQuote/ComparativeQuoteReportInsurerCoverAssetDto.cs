﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.ComparativeQuote
{
    public class ComparativeQuoteReportInsurerCoverAssetDto : Resource, ICultureAware
    {
        public ComparativeQuoteReportInsurerCoverAssetDto()
        {

        }
        public string Asset { get; set; }
        public decimal Value { get; set; }
    }
}
