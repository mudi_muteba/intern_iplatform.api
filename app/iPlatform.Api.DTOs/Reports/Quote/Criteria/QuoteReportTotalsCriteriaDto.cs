﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Quote.Criteria
{
    public class QuoteReportTotalsCriteriaDto : Resource, ICultureAware
    {
        public int ProposalHeaderId { get; set; }
        public string QuoteIds { get; set; }
        public string OrganizationCode { get; set; }
    }
}
