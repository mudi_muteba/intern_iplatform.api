﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Quote.Criteria
{
    public class QuoteReportCriteriaDto : Resource, ICultureAware
    {
        public int ChannelId { get; set; }
        public int ProposalHeaderId { get; set; }
        public string QuoteIds { get; set; }
        public string OrganizationCode { get; set; }
        public string ReportName { get; set; }
        public string ReportId { get; set; }
    }
}
