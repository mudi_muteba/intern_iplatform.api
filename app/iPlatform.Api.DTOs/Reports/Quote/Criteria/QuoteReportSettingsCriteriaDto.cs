﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Quote.Criteria
{
    public class QuoteReportSettingsCriteriaDto : Resource, ICultureAware
    {
        public int ChannelId { get; set; }
        public string ReportName { get; set; }
    }
}
