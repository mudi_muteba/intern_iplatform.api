﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Quote
{
    public class QuoteReportSettingsDto : Resource, ICultureAware
    {
        #region General
        public string QuoteCode { get; set; }
        public string QuoteFooter { get; set; }
        #endregion

        #region Labels
        public string QuoteTitleLabel { get; set; }
        public string QuoteSummaryLabel { get; set; }
        public string QuoteAssetsLabel { get; set; }
        public string QuoateDateLabel { get; set; }
        public string QuoteExpiresLabel { get; set; }
        public string QuoteNumberLabel { get; set; }

        public string ClientHeaderLabel { get; set; }
        public string ClientGeneralNameLabel { get; set; }
        public string ClientGeneralIdentityLabel { get; set; }
        public string ClientAddressPhysicalLabel { get; set; }
        public string ClientAddressPostalLabel { get; set; }
        public string ClientContactWorkLabel { get; set; }
        public string ClientContactHomeLabel { get; set; }
        public string ClientContactCellLabel { get; set; }
        public string ClientContactFaxLabel { get; set; }
        public string ClientContactEmailLabel { get; set; }
        public string ClientContactIdentityLabel { get; set; }

        public string CompanyHeaderLabel { get; set; }
        public string CompanyGeneralNameLabel { get; set; }
        public string CompanyAddressPhysicalLabel { get; set; }
        public string CompanyAddressPostalLabel { get; set; }
        public string CompanyContactWorkLabel { get; set; }
        public string CompanyContactFaxLabel { get; set; }
        public string CompanyContactEmailLabel { get; set; }
        public string CompanyAuthRegistrationLabel { get; set; }
        public string CompanyAuthTaxLabel { get; set; }
        public string CompanyAuthLicenseLabel { get; set; }
        public string CompanyContactWebsiteLabel { get; set; }
        #endregion

        #region Values
        public string CompanyHeaderValue { get; set; }
        public string CompanyGeneralNameValue { get; set; }
        public string CompanyAddressPhysicalValue { get; set; }
        public string CompanyAddressPostalValue { get; set; }
        public string CompanyContactWorkValue { get; set; }
        public string CompanyContactCellValue { get; set; }
        public string CompanyContactFaxValue { get; set; }
        public string CompanyContactEmailValue { get; set; }
        public string CompanyAuthRegistrationValue { get; set; }
        public string CompanyAuthTaxValue { get; set; }
        public string CompanyAuthLicenseValue { get; set; }
        public string CompanyContactWebsiteValue { get; set; }
        #endregion

        #region Visiblity
        public bool QuoteTitleVisibility { get; set; }
        public bool QuoteSummaryVisibility { get; set; }
        public bool QuoteAssetsVisibility { get; set; }
        public bool QuoateDateVisibility { get; set; }
        public bool QuoteExpiresVisibility { get; set; }
        public bool QuoteNumberVisibility { get; set; }

        public bool ClientHeaderVisibility { get; set; }
        public bool ClientGeneralNameVisibility { get; set; }
        public bool ClientGeneralIdentityVisibility { get; set; }
        public bool ClientAddressPhysicalVisibility { get; set; }
        public bool ClientAddressPostalVisibility { get; set; }
        public bool ClientContactWorkVisibility { get; set; }
        public bool ClientContactHomeVisibility { get; set; }
        public bool ClientContactCellVisibility { get; set; }
        public bool ClientContactFaxVisibility { get; set; }
        public bool ClientContactEmailVisibility { get; set; }

        public bool CompanyHeaderVisibility { get; set; }
        public bool CompanyGeneralNameVisibility { get; set; }
        public bool CompanyAddressPhysicalVisibility { get; set; }
        public bool CompanyAddressPostalVisibility { get; set; }
        public bool CompanyContactWorkVisibility { get; set; }
        public bool CompanyContactFaxVisibility { get; set; }
        public bool CompanyContactEmailVisibility { get; set; }
        public bool CompanyAuthRegistrationVisibility { get; set; }
        public bool CompanyAuthTaxVisibility { get; set; }
        public bool CompanyAuthLicenseVisibility { get; set; }
        public bool CompanyContactWebsiteVisibility { get; set; }

        public bool QuoteSummaryFeesVisibility { get; set; }
        public bool QuoteSummaryTotalVisibility { get; set; }
        public bool CostFooterVisiblity { get; set; }
        #endregion
    }
}
