﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Quote
{
    public class QuoteReportAssetDto : Resource, ICultureAware
    {
        public int AssetNo { get; set; }
        public string Asset { get; set; }
        public string CoverType { get; set; }
        public decimal SumInsured { get; set; }
    }
}
