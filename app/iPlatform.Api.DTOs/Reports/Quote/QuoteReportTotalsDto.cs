﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Quote
{
    public class QuoteReportTotalsDto : Resource, ICultureAware
    {
        public decimal SASRIA { get; set; }
        public decimal Fees { get; set; }
        public decimal Total { get; set; }
    }
}
