﻿using System;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Quote
{
    public class QuoteReportHeaderDto : Resource, ICultureAware
    {
        public string Client { get; set; }
        public string PhysicalAddress { get; set; }
        public string PostalAddress { get; set; }
        public string WorkNo { get; set; }
        public string HomeNo { get; set; }
        public string FaxNo { get; set; }
        public string CellNo { get; set; }
        public string EmailAddress { get; set; }
        public string IdentityNo { get; set; }
        public string QuoteNo { get; set; }
        public int QuoteHeaderId { get; set; }
        public int QuoteExpiration { get; set; }
        public string EMailBody { get; set; }
        public string DefaultPhysical { get; set; }
        public string DefaultPostal { get; set; }
        public string NoDefaultPhysical { get; set; }
        public string NoDefaultPostal { get; set; }
        public DateTime Date { get; set; }
        public decimal Fees { get; set; }
        public decimal Sasria { get; set; }
        public decimal Total { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }
}
