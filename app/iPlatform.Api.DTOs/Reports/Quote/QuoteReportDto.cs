﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Quote
{
    public class QuoteReportDto : Resource, ICultureAware
    {
        public QuoteReportSettingsDto Layout;
        public QuoteReportHeaderDto Header;
        public QuoteReportTotalsDto Totals;

        public IList<QuoteReportSummaryDto> Summary;
        public IList<QuoteReportAssetDto> Assets;
        public IList<QuoteReportValueAddedProductDto> ValueAddedProducts;
    }
}
