﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Quote
{
    public class QuoteReportContainerDto : Resource, ICultureAware
    {
        public QuoteReportSettingsDto Settings { get; set; }
        public QuoteReportHeaderDto Header { get; set; }
    }
}
