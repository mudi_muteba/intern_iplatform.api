﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Quote
{
    public class QuoteReportValueAddedProductDto : Resource, ICultureAware
    {
        public string Cover { get; set; }
        public string Asset { get; set; }
        public string Product { get; set; }
        public string Definition { get; set; }
    }
}
