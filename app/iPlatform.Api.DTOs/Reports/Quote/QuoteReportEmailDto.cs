﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using System.IO;
using System.Net.Mail;

namespace iPlatform.Api.DTOs.Reports.Quote
{
    public class QuoteReportEmailDto : Resource, ICultureAware, IExecutionDto
    {
        public int ProposalHeaderId { get; set; }
        public string QuoteIds { get; set; }
        public string BaseUrl { get; set; }
        public string Email { get; set; }
        public string OrganizationCode { get; set; }
        public string QuoteUri { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

    }
}