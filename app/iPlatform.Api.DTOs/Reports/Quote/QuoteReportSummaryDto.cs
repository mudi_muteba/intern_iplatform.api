﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Reports.Quote
{
    public class QuoteReportSummaryDto : Resource, ICultureAware
    {
        public string Insurer { get; set; }
        public string Product { get; set; }
        public decimal BasicExcess { get; set; }
        public decimal Premium { get; set; }
        public decimal SASRIA { get; set; }
        public decimal Fees { get; set; }
        public decimal Total { get; set; }
    }
}
