﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Statistics
{
    public class StatisticsDto
    {
        public int NewLeads { get; set; }
        public int QuotesAccepted { get; set; }

    }
}
