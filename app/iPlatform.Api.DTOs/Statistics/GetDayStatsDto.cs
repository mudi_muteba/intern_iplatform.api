﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Statistics
{
    public class GetDayStatsDto : AttributeValidationDto, IExecutionDto
    {
        public GetDayStatsDto()
        {
            Context = DtoContext.NoContext();
        }

        public int AgentId { get; set; }
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var list = new List<ValidationErrorMessage>();

            if (AgentId <= 0)
            {
                list.Add(StatisticsValidationMessages.AgentIdRequired);
            }

            return list;
        }
    }
}
