﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Statistics
{
    public class GetMonthlyStatsDto : AttributeValidationDto, IExecutionDto
    {
        public GetMonthlyStatsDto()
        {
            Context = DtoContext.NoContext();
        }

        public int AgentId { get; set; }
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}
