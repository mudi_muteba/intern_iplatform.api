﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.SectionsPerChannel
{
    public class GetSectionsPerChannelDto : AttributeValidationDto, IExecutionDto
    {

        public GetSectionsPerChannelDto()
        {
            Context = DtoContext.NoContext();
            
        }
       public int ChannelId { get; set; }
       public string Sections { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context; 
        }

        public DtoContext Context { get; private set; }
       
    }
}
