﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.SectionsPerChannel
{
    public class ChannelSectionDto : ICultureAware
    {
       public int ChannelId { get; set; }
       public int Sections { get; set; }

    }
}
