﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.SectionsPerChannel
{
    public class ResultChannelSectionDto : ICultureAware
    {
       public int ChannelId { get; set; }
       public List<string> Sections { get; set; }

    }
}
