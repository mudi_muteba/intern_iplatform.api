﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.QuoteAcceptance
{
    public class QuoteNotificationQuestionGroupDto
    {
        public QuoteNotificationQuestionGroupDto()
        {
            QuestionAnswers = new List<QuoteNotificationQuestionAnswerDto>();
        }
        public QuestionGroup QuestionGroup { get; set; }
        public List<QuoteNotificationQuestionAnswerDto> QuestionAnswers { get; set; }
    }
}
