﻿using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;
using System.Collections.Generic;
using System.Globalization;

namespace iPlatform.Api.DTOs.QuoteAcceptance
{
    public class QuoteNotificationDto
    {
        public QuoteNotificationDto()
        {
            Covers = new List<QuoteNotificationCoverDto>();
            PaymentPlan = PaymentPlans.Monthly;
        }

        public string ProductCode { get; set; }
        public string InsurerCode { get; set; }
        public string InsurerName { get; set; }
        public string ProductName { get; set; }
        public string InsurerReference { get; set; }
        public decimal Fees { get; set; }
        public NumberFormatInfo NumberFormatInfo { get; set; }
        public RatingRequestPersonDto Person { get; set; }
        public InsuredInfoDto InsuredInfo { get; set; }
        public RatingResultPolicyDto Policy { get; set; }
        public List<QuoteNotificationCoverDto> Covers { get; set; }
        public PaymentPlan PaymentPlan { get; set; }
        public string Environment { get; set; }
        public AgentDetailDto AgentDetail { get; set; }
        public EmailCommunicationSettingDto EmailCommunicationSetting { get; set; }
    }
}
