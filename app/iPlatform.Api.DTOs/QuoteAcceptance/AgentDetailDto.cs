﻿namespace iPlatform.Api.DTOs.QuoteAcceptance
{
    public class AgentDetailDto
    {
        public int UserId { get; set; }
        public string EmailAddress { get; set; }
        public string Name { get; set; }
        public string Comments { get; set; }

        public AgentDetailDto() { }

        public AgentDetailDto(int userId, string email, string name = "")
        {
            UserId = userId;
            EmailAddress = email;
            Name = name;
        }
    }
}
