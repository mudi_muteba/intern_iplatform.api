﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.QuoteAcceptance
{
    public class QuoteNotificationQuestionAnswerDto
    {
        public QuoteNotificationQuestionAnswerDto() { }
        public Question Question { get; set; }
        public string Answer { get; set; }
        public bool IsDecimal { get; set; }
        public decimal DecimalAnswer {get; set; }
    }
}
