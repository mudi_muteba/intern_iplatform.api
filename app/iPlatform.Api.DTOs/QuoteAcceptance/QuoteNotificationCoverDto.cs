﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.QuoteAcceptance
{
    public class QuoteNotificationCoverDto
    {
        public QuoteNotificationCoverDto()
        {
            QuestionGroups = new List<QuoteNotificationQuestionGroupDto>();
        }

        public Cover Cover { get; set; }
        public decimal Premium { get; set; }
        public decimal Sasria { get; set; }
        public decimal Excess { get; set; }
        public int AssetNo { get; set; }
        public List<QuoteNotificationQuestionGroupDto> QuestionGroups { get; set; }

    }
}
