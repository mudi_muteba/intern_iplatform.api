﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.QuoteAcceptance
{
    public class EchoTCFDto
    {
        public EchoTCFDto()
        {

        }
        public EchoTCFDto(string baseurl, string token, string byPass )
        {
            Token = token;
            Baseurl = baseurl;
            ByPass = byPass;
        }
        public string Token { get; set; }
        public string Baseurl { get; set; }
        public string ByPass { get; set; }
    }
}
