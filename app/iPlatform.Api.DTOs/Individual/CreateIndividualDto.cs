﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Party.ContactDetail;
using MasterData;
using iPlatform.Api.DTOs.Occupation;

namespace iPlatform.Api.DTOs.Individual
{
    public class CreateIndividualDto : AttributeValidationDto, IExecutionDto, IChannelAwareDto
    {
        public CreateIndividualDto()
        {
            SimpleAudit = new AuditDto();
            Events = new List<AuditEventDto>();
            Campaigns = new List<CampaignInfoDto>();
        }

        public Title Title { get; set; }
        public Gender Gender { get; set; }
        public OccupationDto Occupation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string IdentityNo { get; set; }
        public string PassportNo { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public Language Language { get; set; }
        public CreateContactDetailDto ContactDetail { get; set; }
        public string PinNo { get; set; }
        public string PreferredName { get; set; }
        public List<CampaignInfoDto> Campaigns { get; set; }

        public virtual bool AnyJudgements { get; set; }
        public virtual string AnyJudgementsReason { get; set; }
        public virtual bool UnderAdministrationOrDebtReview { get; set; }
        public virtual string UnderAdministrationOrDebtReviewReason { get; set; }
        public virtual bool BeenSequestratedOrLiquidated { get; set; }
        public virtual string BeenSequestratedOrLiquidatedReason { get; set; }
        public virtual bool InsurancePolicyCancelledOrRenewalRefused { get; set; }
        public virtual string InsurancePolicyCancelledOrRenewalRefusedReason { get; set; }

        public virtual string LeadExternalReference { get; set; }
        public int Userid { get; set; }
        public string ContactOnlyIntendedProduct { get; set; }

        //[JsonIgnore]
        public string DisplayName
        {
            get { return string.Format("{0}, {1}", FirstName, Surname); }
            set { _diplayName = value;  }
        }

        private string _diplayName;

        public AuditDto SimpleAudit { get; private set; }
        public List<AuditEventDto> Events { get; private set; }
        public int ChannelId { get; set; }
        public DtoContext Context { get; set; }

        public string Source { get; set; }
        public string CmpidSource { get; set; }

        public string ExternalReference { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}