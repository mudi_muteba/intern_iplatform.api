﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.ContactDetail;
using MasterData;
using System;

namespace iPlatform.Api.DTOs.Individual
{
    public class IndividualSimplifiedDto : Resource, IAuditableDto, ICultureAware
    {
        public IndividualSimplifiedDto()
        {
        }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string DisplayName { get; set; }

        public DateTimeDto DateOfBirth { get; set; }

        public MaritalStatus MaritalStatus { get; set; }

        public Language Language { get; set; }

        public ContactDetailDto ContactDetail { get; set; }

        public List<AuditEventDto> Events { get; private set; }

        public string IdentityNo { get; set; }

        public string IdentityNoMasked
        {
            set { IdentityNo = value;  }
            get
            {
                if(IdentityNo == string.Empty)
                {
                    return string.Empty;
                }

                double showedLength = Math.Ceiling(IdentityNo.Length * 0.45);
                string maskedIdNumber = IdentityNo.Substring(0, (int)showedLength);
                return maskedIdNumber.PadRight(IdentityNo.Length, 'X');
            }
        }
       
    }
}
