﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Individual
{
    public class DeclarationInfoDto
    {
        public DeclarationInfoDto() { }
        public virtual bool AnyJudgements { get; set; }
        public virtual string AnyJudgementsReason { get; set; }
        public virtual bool UnderAdministrationOrDebtReview { get; set; }
        public virtual string UnderAdministrationOrDebtReviewReason { get; set; }
        public virtual bool BeenSequestratedOrLiquidated { get; set; }
        public virtual string BeenSequestratedOrLiquidatedReason { get; set; }
        public virtual bool InsurancePolicyCancelledOrRenewalRefused { get; set; }
        public virtual string InsurancePolicyCancelledOrRenewalRefusedReason { get; set; }
    }
}
