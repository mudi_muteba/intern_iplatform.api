﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Party.Bank;
using iPlatform.Api.DTOs.Party.ContactDetail;
using MasterData;
using iPlatform.Api.DTOs.Occupation;
using iPlatform.Api.DTOs.Party.Contacts;

namespace iPlatform.Api.DTOs.Individual
{
    public class IndividualDto : Resource, IAuditableDto, ICultureAware
    {
        public IndividualDto()
        {
            SimpleAudit = new AuditDto();
            Events = new List<AuditEventDto>();
            BankDetails = new List<BankDetailsDto>();
            Addresses = new List<AddressDto>();
            DeclarationInfo = new DeclarationInfoDto();
            Contacts = new List<ContactDto>();
        }

        public int PartyId { get; set; }
        public int ChannelId { get; set; }
        public Title Title { get; set; }
        public Gender Gender { get; set; }
        public OccupationDto Occupation { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string PreferredName { get; set; }

        public DateTimeDto DateOfBirth { get; set; }

        public string IdentityNo { get; set; }

        public string PassportNo { get; set; }

        public string ExternalReference { get; set; }

        public MaritalStatus MaritalStatus { get; set; }

        public Language Language { get; set; }

        public ContactDetailDto ContactDetail { get; set; }
        public List<AddressDto> Addresses { get; set; }
        public List<BankDetailsDto> BankDetails { get; set; }
        public List<ContactDto> Contacts { get; set; }
        public int LeadId { get; set; }

        public string DisplayName { get; set; }
        public DateTimeDto DateCreated { get; set; }
        public DateTimeDto DateUpdated { get; set; }

        public DeclarationInfoDto DeclarationInfo { get; set; }
        public string PinNo { get; set; }

        public string GetPrimaryContactNumber
        {
            get
            {
                if (ContactDetail == null)
                    return string.Empty;

                if (!string.IsNullOrEmpty(ContactDetail.Cell))
                    return ContactDetail.Cell;
                if (!string.IsNullOrEmpty(ContactDetail.Home))
                    return ContactDetail.Home;
                if (!string.IsNullOrEmpty(ContactDetail.Work))
                    return ContactDetail.Work;
                if (!string.IsNullOrEmpty(ContactDetail.Direct))
                    return ContactDetail.Direct;
                return string.Empty;
            }
        }


        public AuditDto SimpleAudit { get; private set; }

        public List<AuditEventDto> Events { get; private set; }
    }
}