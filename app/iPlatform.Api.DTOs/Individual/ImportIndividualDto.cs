﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.ContactDetail;
using MasterData;
using iPlatform.Api.DTOs.Occupation;

namespace iPlatform.Api.DTOs.Individual
{
   public class ImportIndividualDto :Resource, IChannelAwareDto, IExecutionDto
   {
       public ImportIndividualDto()
       {
           ContactDetail = new ContactDetailDto();
           SimpleAudit = new AuditDto();
           Events = new List<AuditEventDto>();
       }

       public Title Title { get; set; }
       public Gender Gender { get; set; }

       public string FirstName { get; set; }
       public string MiddleName { get; set; }
       public string Surname { get; set; }
       public DateTime? DateOfBirth { get; set; }
       public string IdentityNo { get; set; }
       public string PassportNo { get; set; }
       public MaritalStatus MaritalStatus { get; set; }
       public Language Language { get; set; }

       public OccupationDto Occupation { get; set; }
        public ContactDetailDto ContactDetail { get; set; }

       public int ChannelId { get; set; }

       public AuditDto SimpleAudit { get; private set; }
       public List<AuditEventDto> Events { get; private set; }
       public DtoContext Context { get; protected internal set; }

       public string ExternalReference { get; set; }

       public void SetContext(DtoContext context)
       {
           Context = context;
       }

       public string Source { get; set; }
   }

   public enum IndividualSourceEnum
    {
        API,
        WEB
    }

}
