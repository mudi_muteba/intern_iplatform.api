﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Occupation;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Individual
{
    public class BasicIndividualDto: Resource, ICultureAware
    {
        public int PartyId { get; set; }
        public Title Title { get; set; }
        public Gender Gender { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public DateTimeDto DateOfBirth { get; set; }

        public string IdentityNo { get; set; }

        public string PassportNo { get; set; }
        public string ExternalReference { get; set; }

        public MaritalStatus MaritalStatus { get; set; }
        public Language Language { get; set; }
        public OccupationDto Occupation { get; set; }

        public string DisplayName { get; set; }
        public DateTimeDto DateCreated { get; set; }
        public DateTimeDto DateUpdated { get; set; }

        public List<UserChannelDto> UserChannel { get; set; }
    }
}
