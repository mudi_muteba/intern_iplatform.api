﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Individual
{
    public class IndividualSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public string PolicyNumber { get; set; }
        public string CellNumber { get; set; }
        public string Email { get; set; }
        public string IdNumber { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string ExternalReference { get; set; }
        public int LeadId { get; set; }

        public int CampaignId { get; set; }
        public int AgentId { get; set; }
        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
