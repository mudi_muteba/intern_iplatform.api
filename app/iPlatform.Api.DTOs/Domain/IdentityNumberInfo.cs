﻿using System;
using System.Linq;
using MasterData;

namespace iPlatform.Api.DTOs.Domain
{
    public class IdentityNumberInfo
    {
        public IdentityNumberInfo(string identityNumber)
        {
            BirthDate = null;
            Gender = Genders.Unknown;
            Initialize(identityNumber);
        }

        public string IdentityNumber { get; private set; }

        public DateTime? BirthDate { get; private set; }

        public bool IsForeignCitizenship { get; private set; }

        public bool IsValid { get; private set; }

        public Gender Gender { get; private set; }

        private void Initialize(string identityNumber)
        {
            IdentityNumber = (identityNumber ?? string.Empty).Replace(" ", "");
            if (IdentityNumber.Length != 13) return;

            var digits = new int[13];
            for (var i = 0; i < 13; i++)
            {
                if (!int.TryParse(IdentityNumber.Substring(i, 1), out digits[i])) return;
            }

            var control1 = digits.Where((v, i) => i % 2 == 0 && i < 12).Sum();
            var second = string.Empty;
            digits
                .Where((v, i) => i % 2 != 0 && i < 12)
                .ToList()
                .ForEach(v => second += v.ToString());

            var string2 = (int.Parse(second) * 2).ToString();
            var control2 = string2.Select((t, i) => int.Parse(string2.Substring(i, 1))).Sum();
            var control = (10 - ((control1 + control2) % 10)) % 10;
            if (digits[12] != control) return;

            BirthDate = DateTime.ParseExact(IdentityNumber.Substring(0, 6), "yyMMdd", null);
            Gender = digits[6] < 5 ? Genders.Female : Genders.Male;
            IsForeignCitizenship = digits[10] != 0;
            IsValid = true;
        }
    }
}
