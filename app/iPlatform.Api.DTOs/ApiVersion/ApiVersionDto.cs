﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.ApiVersion
{
    public class ApiVersionDto
    {
        public string EnvironmentRelease { get; set; }
        public string PreviousPackageVersion { get; set; }
        public string NugetPackageVersion { get; set; }
        public string PackageVersion { get; set; }
        public string VersionApi { get; set; }

        public ApiVersionDto()
        {
        }
    }
}
