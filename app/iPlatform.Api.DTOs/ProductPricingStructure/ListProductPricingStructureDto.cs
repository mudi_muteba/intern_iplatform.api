﻿using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.ProductPricingStructure
{
    public class ListProductPricingStructureDto : Resource
    {
        public Cover Cover { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public decimal Monthly1YearDefault { get; set; }

        public decimal Monthly1YearMin { get; set; }

        public decimal Monthly1YearMax { get; set; }

        public decimal Monthly2YearDefault { get; set; }

        public decimal Monthly2YearMin { get; set; }

        public decimal Monthly2YearMax { get; set; }

        public decimal Monthly3YearDefault { get; set; }

        public decimal Monthly3YearMin { get; set; }

        public decimal Monthly3YearMax { get; set; }

        public decimal Annual1YearDefault { get; set; }

        public decimal Annual1YearMin { get; set; }

        public decimal Annual1YearMax { get; set; }

        public decimal Annual2YearDefault { get; set; }

        public decimal Annual2YearMin { get; set; }

        public decimal Annual2YearMax { get; set; }

        public decimal Annual3YearDefault { get; set; }

        public decimal Annual3YearMin { get; set; }

        public decimal Annual3YearMax { get; set; }

        public ProductInfoDto Product { get; set; }

        public ChannelInfoDto Channel { get; set; }
    }
}
