﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions
{
    public class DisableComponentQuestionDefinitionDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {

        public DtoContext Context { get; private set; }

        public int Id { get; set; }
    

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
