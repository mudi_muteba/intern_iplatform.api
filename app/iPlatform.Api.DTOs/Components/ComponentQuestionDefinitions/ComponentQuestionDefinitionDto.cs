﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions
{
    public class ComponentQuestionDefinitionDto : Resource, IAuditableDto, ICultureAware
    {
        public ComponentQuestionDefinitionDto()
        {
            Events = new List<AuditEventDto>();
        }

        public List<AuditEventDto> Events { get; set; }

        public string DisplayName { get; set; }
        public int ChannelId { get; set; }
        public ComponentQuestion ComponentQuestion { get; set; }
        public ComponentQuestionGroup ComponentQuestionGroup { get; set; }
        public ComponentType ComponentType { get; set; }
        public int VisibleIndex { get; set; }
        public bool IsRequieredForQuote { get; set; }
        public bool IsRatingFactor { get; set; }
        public bool IsReadOnly { get; set; }
        public string ToolTip { get; set; }
        public string DefaultValue { get; set; }
        public string RegexPattern { get; set; }

    }
}
