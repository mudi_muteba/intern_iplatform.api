﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions
{
    public class EditComponentQuestionDefinitionDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public string DisplayName { get; set; }
        public int ChannelId { get; set; }
        public int ComponentQuestionId { get; set; }
        public int ComponentQuestionGroupId { get; set; }
        public int ComponentTypeId { get; set; }
        public int VisibleIndex { get; set; }
        public bool IsRequieredForQuote { get; set; }
        public bool IsRatingFactor { get; set; }
        public bool IsReadOnly { get; set; }
        public string ToolTip { get; set; }
        public string DefaultValue { get; set; }
        public string RegexPattern { get; set; }

        public DtoContext Context { get; private set; }

        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
