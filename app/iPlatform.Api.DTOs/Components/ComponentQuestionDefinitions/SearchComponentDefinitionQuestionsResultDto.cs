﻿using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions
{
    public class SearchComponentDefinitionQuestionsResultDto
    {
        public int QuestionDefinitionId { get; set; }
        public string DisplayName { get; set; }
        public int QuestionGoupId { get; set; }
        public string QuestionGroupName{ get; set; }
        public int ComponentTypeId { get; set; }
        public string ComponentTypeName { get; set; }
        public int VisibleIndex { get; set; }
        public bool IsRequieredForQuote { get; set; }
        public bool IsRatingFactor { get; set; }
        public bool IsReadOnly { get; set; }
        public string ToolTip { get; set; }
        public string DefaultValue { get; set; }
        public string RegexPattern { get; set; }
        public int QuestionTypeId { get; set; }
        public string QuestionTypeName { get; set; }

        public List<SearchComponentQuestionAnswerResultDto> ComponentAnswerOptions { get; set; }

        public SearchComponentDefinitionQuestionsResultDto()
        {
            ComponentAnswerOptions = new List<SearchComponentQuestionAnswerResultDto>();
        }
    }
}
