﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions
{
    public class SearchComponentDefinitionsDto : BaseCriteria, IValidationAvailableDto
    {
        public string ComponentTypeId { get; set; }
        public string ComponentQuestionGroupId { get; set; }
        public int PartyId { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }

        public static implicit operator SearchComponentDefinitionsDto(SearchComponentDefinitionAnswersDto v)
        {
            throw new NotImplementedException();
        }
    }
}
