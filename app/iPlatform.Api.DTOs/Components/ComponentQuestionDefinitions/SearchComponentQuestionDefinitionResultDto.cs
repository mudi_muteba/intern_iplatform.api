﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Components.ComponentHeaders;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterData;
using iPlatform.Api.DTOs.Components.ComponentDefinitions;

namespace iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions
{
    public class SearchComponentQuestionDefinitionResultDto 
    {
        public SearchComponentDefinitionQuestionsResultDto ComponentQuestionDefinition { get; set; }
    }
}