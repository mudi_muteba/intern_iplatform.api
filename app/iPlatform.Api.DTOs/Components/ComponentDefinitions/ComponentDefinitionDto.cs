﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Components.ComponentDefinitions
{
    public class ComponentDefinitionDto : Resource, IAuditableDto, ICultureAware
    {
        public ComponentDefinitionDto()
        {
            Events = new List<AuditEventDto>();
        }

        public List<AuditEventDto> Events { get; set; }

        public int ComponentHeaderId { get; set; }
        public int PartyId { get; set; }
        public int ChannelId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime DateModified { get; set; }
    }
}
