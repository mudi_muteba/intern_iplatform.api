﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterData;

namespace iPlatform.Api.DTOs.Components.ComponentDefinitions.Questions
{
    public class ComponentDefinitionQuestionDto
    {
        public string DisplayName { get; set; }

        public ComponentQuestion Question { get; set; }

        public List<ComponentAnswer> Answers { get; set; }

        public bool IsRatingFactor { get; set; }

        public bool IsRequieredForQuote { get; set; }

        public bool IsReadOnly { get; set; }

        public string ToolTip { get; set; }

        public string DefaultValue { get; set; }

        public string RegexPattern { get; set; }
    }
}
