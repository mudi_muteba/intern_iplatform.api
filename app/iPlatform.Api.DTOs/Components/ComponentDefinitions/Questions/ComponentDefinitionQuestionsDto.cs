﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Components.ComponentDefinitions.Questions
{
    public class ComponentDefinitionQuestionsDto: Resource
    {
        public ComponentDefinitionDto ComponentDefinition { get; set; }
        
        public List<ComponentDefinitionQuestionDto> QuestionDefinitions { get; set; }

        public ComponentDefinitionQuestionsDto()
        {
            QuestionDefinitions = new List<ComponentDefinitionQuestionDto>();
            ComponentDefinition = new ComponentDefinitionDto();
        }
    }
}
