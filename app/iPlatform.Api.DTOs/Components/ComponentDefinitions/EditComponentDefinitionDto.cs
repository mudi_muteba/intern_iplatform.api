﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Components.ComponentDefinitions
{
    public class EditComponentDefinitionDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public int Id { get; set; }
        public int ComponentHeaderId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime DateModified { get; set; }
        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
