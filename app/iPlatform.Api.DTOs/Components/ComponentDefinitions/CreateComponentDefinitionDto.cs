﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Components;

namespace iPlatform.Api.DTOs.Components.ComponentDefinitions
{
    public class CreateComponentDefinitionDto : AttributeValidationDto, IExecutionDto
    {
        public int ComponentHeaderId { get; set; }
        public int ComponentTypeId { get; set; }
        public int PartyId { get; set; }
        
        public DateTime CreatedDate { get; set; }
        public DateTime DateModified { get; set; }

        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ComponentHeaderId <= 0)
                validation.Add(ComponentDefinitionValidationMessages.ComponentHeaderIdInvalid);

            if (PartyId <= 0)
                validation.Add(ComponentDefinitionValidationMessages.PartyIdInvalid);

            if (ComponentTypeId <= 0)
                validation.Add(ComponentDefinitionValidationMessages.ComponentTypeIdInvalId);

            return validation;
        }

    }
}
