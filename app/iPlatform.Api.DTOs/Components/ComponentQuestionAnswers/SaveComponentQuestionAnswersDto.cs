﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Components.ComponentQuestionAnswers
{
    public class SaveComponentQuestionAnswersDto : Resource, IExecutionDto, IAffectExistingEntity
    {
        public int ComponentDefinitionId { get; set; }
        
        public IList<CreateComponentQuestionAnswerDto> Answers { get; set; }

        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }
                
        public SaveComponentQuestionAnswersDto()
        {
            Answers = new List<CreateComponentQuestionAnswerDto>();
            Context = DtoContext.NoContext();
        }

    }
}
