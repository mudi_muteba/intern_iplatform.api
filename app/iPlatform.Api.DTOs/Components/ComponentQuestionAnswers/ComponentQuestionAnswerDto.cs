﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using System;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Components.ComponentQuestionAnswers
{
    public class ComponentQuestionAnswerDto : Resource, IAuditableDto, ICultureAware
    {
        public ComponentQuestionAnswerDto()
        {
            Events = new List<AuditEventDto>();
        }

        public List<AuditEventDto> Events { get; set; }

        public int ComponentDefinitionId { get; set; }
        public int ComponentQuestionDefinitionId { get; set; }
        public string Answer { get; set; }
        public int ComponentQuestionTypeId { get; set; }

    }
}
