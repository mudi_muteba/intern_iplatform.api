﻿using iPlatform.Api.DTOs.Components.ComponentDefinitions;
using iPlatform.Api.DTOs.Components.ComponentHeaders;
using MasterData;

namespace iPlatform.Api.DTOs.Components.ComponentQuestionAnswers
{
    public class SearchComponentQuestionDefinitionAnswerResultDto
    {
        public ComponentHeaderDto Header { get; set; }
        public ComponentDefinitionDto Definition { get; set; }
        public ComponentQuestionAnswerDto Answer{ get; set; }
        public QuestionType QuetionType { get; set; }
    }
}
