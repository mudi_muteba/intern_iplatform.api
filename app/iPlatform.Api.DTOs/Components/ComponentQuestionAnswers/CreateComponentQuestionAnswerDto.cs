﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Components;

namespace iPlatform.Api.DTOs.Components.ComponentQuestionAnswers
{
    public class CreateComponentQuestionAnswerDto : AttributeValidationDto, IExecutionDto
    {
        public int ComponentDefinitionId { get; set; }
        public int ComponentQuestionDefinitionId { get; set; }
        public string Answer { get; set; }
        public int ComponentQuestionTypeId { get; set; }

        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ComponentDefinitionId <= 0)
                validation.Add(ComponentQuestionAnswerValidationMessages.ComponentDefinitionInvalId);

            if (ComponentQuestionDefinitionId <= 0)
                validation.Add(ComponentQuestionAnswerValidationMessages.ComponentQuestionDefinitionIdInvalId);

            if (ComponentQuestionTypeId <= 0)
                validation.Add(ComponentQuestionAnswerValidationMessages.ComponentQuestionTypeIdInvalId);
            return validation;
        }

    }
}
