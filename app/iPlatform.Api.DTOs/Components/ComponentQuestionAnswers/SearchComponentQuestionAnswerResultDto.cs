﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Components.ComponentQuestionAnswers
{
    public class SearchComponentQuestionAnswerResultDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Answer { get; set; }
        public int VisibleIndex { get; set; }
        public int QuestionId { get; set; }
    }
}
