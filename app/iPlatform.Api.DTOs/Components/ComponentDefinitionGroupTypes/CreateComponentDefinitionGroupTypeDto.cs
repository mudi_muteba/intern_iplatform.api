﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Components.ComponentDefinitionGroupTypes
{
    public class CreateComponentDefinitionGroupTypeDto : AttributeValidationDto, IExecutionDto
    {
        public string Name { get; set; }
        public int VisibleIndex { get; set; }

        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public CreateComponentDefinitionGroupTypeDto()
        {
            Context = DtoContext.NoContext();
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
