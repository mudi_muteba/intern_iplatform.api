﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Components.ComponentDefinitionGroupTypes
{
    public class ComponentDefinitionGroupTypeDto : Resource, IAuditableDto, ICultureAware
    {
        public ComponentDefinitionGroupTypeDto()
        {
            Events = new List<AuditEventDto>();
        }
        public List<AuditEventDto> Events { get; set; }

        public string Name { get; set; }
        public int VisibleIndex { get; set; }

    }
}
