﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Components.ComponentHeaders
{
    public class ComponentHeaderDto : Resource, IAuditableDto, ICultureAware
    {
        public ComponentHeaderDto()
        {
            Events = new List<AuditEventDto>();
        }
        public List<AuditEventDto> Events { get; set; }

        public int ActivityId { get; set; }
        public int ActivityTypeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime DateModified { get; set; }
        public string Source { get; set; }
        public int PartyId { get; set; }
    }
}
