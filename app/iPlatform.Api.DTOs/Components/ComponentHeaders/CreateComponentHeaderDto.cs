﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Components;

namespace iPlatform.Api.DTOs.Components.ComponentHeaders
{
    public class CreateComponentHeaderDto : AttributeValidationDto, IExecutionDto
    {
        public int ActivityId { get; set; }
        public int ActivityTypeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime DateModified { get; set; }
        public string Source { get; set; }
        public int PartyId { get; set; }

        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (PartyId <= 0)
                validation.Add(ComponentHeaderValidationMessages.PartyIdInvalid);

            return validation;
        }

    }
}
