﻿using System;

namespace iPlatform.Api.DTOs.Monitoring
{
    public class MonitoringDto
    {
        public MonitoringDto()
        {
            
        }


        public MonitoringDto(Guid parentId, string serverName, Status status, Priority priority, Guid channelId, string error,
            string category, string subCategory, string parameters, string result)
        {
            ParentId = parentId;
            DateOfEvent = DateTime.UtcNow;
            ServerName = serverName;
            Status = status.ToString();
            Priority = priority.ToString();
            ChannelId = channelId;
            Error = error;
            Category = category;
            SubCategory = subCategory;
            Parameters = parameters;
            Result = result;
        }
        
        public Guid? ParentId { get; set; }
        public DateTime? DateOfEvent { get; set; }
        public string ServerName { get; set; }
        public string Status { get; set; }
        public string Priority { get; set; }
        public Guid? ChannelId { get; set; }
        public string Error { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string Parameters { get; set; }
        public string Result { get; set; }
    }

    public enum Priority
    {
        Critical,
        High,
        Normal,
        Low
    }

    public enum Status
    {
        Success,
        Failure
    }
}