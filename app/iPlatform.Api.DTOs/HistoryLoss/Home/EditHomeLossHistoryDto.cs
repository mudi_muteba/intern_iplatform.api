﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using MasterData;
using ValidationMessages;

namespace iPlatform.Api.DTOs.HistoryLoss.Home
{
    public class EditHomeLossHistoryDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public EditHomeLossHistoryDto()
        {
            Context = DtoContext.NoContext();
        }
        public int Id { get; set; }
        public int PartyId { get; set; }
        public int HomeTypeOfLossId { get; set; }
        public string DateOfLoss { get; set; }
        public int HomeClaimAmountId { get; set; }
        public int HomeClaimLocationId { get; set; }
        public int CurrentlyInsuredId { get; set; }
        public int UninterruptedPolicyId { get; set; }
        public bool InsurerCancel { get; set; }
        public string Why { get; set; }
        public string OtherLossType { get; set; }
    
        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (PartyId <= 0)
                validation.Add(MotorLossHistoryValidationMessages.PartyIdInvalid);

            //if (!new HomeTypeOfLosses().Any(x => x.Id == HomeTypeOfLossId))
            //    validation.Add(HomeLossHistoryValidationMessages.HomeTypeOfLossIdInvalid.AddParameters(new[] { HomeTypeOfLossId.ToString() }));

            //if (!new HomeClaimAmounts().Any(x => x.Id == HomeClaimAmountId))
            //    validation.Add(HomeLossHistoryValidationMessages.HomeClaimAmountIdInvalid.AddParameters(new[] { HomeClaimAmountId.ToString() }));

            //if (!new UninterruptedPolicies().Any(x => x.Id == UninterruptedPolicyId))
            //    validation.Add(HomeLossHistoryValidationMessages.UninterruptedPolicyIdInvalid.AddParameters(new[] { UninterruptedPolicyId.ToString() }));

            //if (!new HomeClaimLocations().Any(x => x.Id == HomeClaimLocationId))
            //    validation.Add(HomeLossHistoryValidationMessages.HomeClaimLocationIdInvalid.AddParameters(new[] { HomeClaimLocationId.ToString() }));

            //if (!new CurrentlyInsureds().Any(x => x.Id == CurrentlyInsuredId))
            //    validation.Add(HomeLossHistoryValidationMessages.CurrentlyInsuredIdInvalid.AddParameters(new[] { CurrentlyInsuredId.ToString() }));

            //if (string.IsNullOrEmpty(DateOfLoss))
            //    validation.Add(HomeLossHistoryValidationMessages.DateOfLossEmpty);

            //if (string.IsNullOrEmpty(Why))
            //    validation.Add(HomeLossHistoryValidationMessages.WhyEmpty);


            return validation;
        }
    }
}
