﻿using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.HistoryLoss.Home
{
    public class ListHomeLossHistoryDto : Resource
    {
        public ListHomeLossHistoryDto()
        {
            
        }

        public int PartyId { get; set; }
        public HomeTypeOfLoss HomeTypeOfLoss { get; set; }
        public string DateOfLoss { get; set; }
        public HomeClaimAmount HomeClaimAmount { get; set; }
        public HomeClaimLocation HomeClaimLocation { get; set; }
        public CurrentlyInsured CurrentlyInsured { get; set; }
        public UninterruptedPolicy UninterruptedPolicy { get; set; }
        public bool InsurerCancel { get; set; }
        public string Why { get; set; }
        public string OtherLossType { get; set; }
    }
}
