﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.HistoryLoss.Home
{
    public class DisableHomeLossHistoryDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {

        public DisableHomeLossHistoryDto()
        {
            Context = DtoContext.NoContext();
        }

        public int Id { get; set; }
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {

        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                validation.Add(HomeLossHistoryValidationMessages.IdRequired);
            }

            return validation;
        }

    }
}
