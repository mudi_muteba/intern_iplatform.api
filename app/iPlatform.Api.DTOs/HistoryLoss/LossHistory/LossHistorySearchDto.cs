﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.HistoryLoss.LossHistory
{
    public class LossHistorySearchDto : BaseCriteria, IValidationAvailableDto
    {
        public int ContactId { get; set; }
        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
