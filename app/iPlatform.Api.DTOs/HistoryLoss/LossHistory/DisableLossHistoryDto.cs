﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.HistoryLoss.LossHistory
{
    public class DisableLossHistoryDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DisableLossHistoryDto()
        {
            Context = DtoContext.NoContext();
        }

        public int Id { get; set; }
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {

        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                validation.Add(LossHistoryValidationMessages.IdRequired);
            }

            return validation;
        }
    }
}
