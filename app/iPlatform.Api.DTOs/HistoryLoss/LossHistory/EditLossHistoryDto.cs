﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.HistoryLoss.LossHistory
{
    public class EditLossHistoryDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public EditLossHistoryDto()
        {
            Context = DtoContext.NoContext();
        }

        public int Id { get; set; }
        public int PartyId { get; set; }
        public int CurrentlyInsuredId { get; set; }
        public int UninterruptedPolicyId { get; set; }
        public bool InsurerCancel { get; set; }
        public bool PreviousComprehensiveInsurance { get; set; }
        public int UninterruptedPolicyNoClaimId { get; set; }
        public bool InterruptedPolicyClaim { get; set; }
        public int ContactId { get; set; }
        public bool MissedPremiumLast6Months { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (PartyId <= 0)
                validation.Add(LossHistoryValidationMessages.PartyIdInvalid);

            return validation;
        }
    }
}
