﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.HistoryLoss.LossHistory
{
    public class CreateLossHistoryDto : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public int PartyId { get; set; }
        public int CurrentlyInsuredId { get; set; }
        public int UninterruptedPolicyId { get; set; }
        public bool InsurerCancel { get; set; }
        public bool PreviousComprehensiveInsurance { get; set; }
        public int UninterruptedPolicyNoClaimId { get; set; }
        public bool InterruptedPolicyClaim { get; set; }
        public int ContactId { get; set; }
        public bool MissedPremiumLast6Months { get; set; }


        public CreateLossHistoryDto()
        {
            Context = DtoContext.NoContext();
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
