﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.HistoryLoss.LossHistory
{
    public class LossHistoryDto : Resource, IAuditableDto
    {
        public List<AuditEventDto> Events { get; private set; }

        public int PartyId { get; set; }
        public int CurrentlyInsuredId { get; set; }
        public int UninterruptedPolicyId { get; set; }
        public bool InsurerCancel { get; set; }
        public bool PreviousComprehensiveInsurance { get; set; }
        public int UninterruptedPolicyNoClaimId { get; set; }
        public bool InterruptedPolicyClaim { get; set; }
        public int ContactId { get; set; }
        public bool MissedPremiumLast6Months { get; set; }

        public LossHistoryDto()
        {
            Events = new List<AuditEventDto>();
        }
    }
}
