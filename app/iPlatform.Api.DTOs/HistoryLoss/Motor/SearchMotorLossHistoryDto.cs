﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;
using ValidationMessages;

namespace iPlatform.Api.DTOs.HistoryLoss.Motor
{
    public class SearchMotorLossHistoryDto : BaseCriteria, IValidationAvailableDto
    {
        public int PartyId { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
