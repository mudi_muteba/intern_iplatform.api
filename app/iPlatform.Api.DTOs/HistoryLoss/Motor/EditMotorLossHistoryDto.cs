﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using MasterData;
using ValidationMessages;

namespace iPlatform.Api.DTOs.HistoryLoss.Motor
{
    public class EditMotorLossHistoryDto: AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {

        public DtoContext Context { get; private set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public EditMotorLossHistoryDto()
        {
            Context = DtoContext.NoContext();
        }

        public int Id { get; set; }
        public int PartyId { get; set; }
        public int MotorTypeOfLossId { get; set; }
        public string DateOfLoss { get; set; }
        public int MotorClaimAmountId { get; set; }
        public int CurrentlyInsuredId { get; set; }
        public int UninterruptedPolicyId { get; set; }
        public bool InsurerCancel { get; set; }
        public int MotorCurrentTypeOfCoverId { get; set; }
        public bool PreviousComprehensiveInsurance { get; set; }
        public int MotorUninterruptedPolicyNoClaimId { get; set; }
        public string Why { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (PartyId <= 0)
                validation.Add(MotorLossHistoryValidationMessages.PartyIdInvalid);

            //if (!new MotorTypeOfLosses().Any(x => x.Id == MotorTypeOfLossId))
            //    validation.Add(MotorLossHistoryValidationMessages.MotorTypeOfLossIdInvalid.AddParameters(new[] { MotorTypeOfLossId.ToString() }));

            //if (!new MotorClaimAmounts().Any(x => x.Id == MotorClaimAmountId))
            //    validation.Add(MotorLossHistoryValidationMessages.MotorClaimAmountIdInvalid.AddParameters(new[] { MotorClaimAmountId.ToString() }));

            //if (!new UninterruptedPolicies().Any(x => x.Id == UninterruptedPolicyId))
            //    validation.Add(MotorLossHistoryValidationMessages.UninterruptedPolicyIdInvalid.AddParameters(new[] { UninterruptedPolicyId.ToString() }));

            //if (!new MotorCurrentTypeOfCovers().Any(x => x.Id == MotorCurrentTypeOfCoverId))
            //    validation.Add(MotorLossHistoryValidationMessages.MotorCurrentTypeOfCoverIdInvalid.AddParameters(new[] { MotorCurrentTypeOfCoverId.ToString() }));

            //if (!new MotorUninterruptedPolicyNoClaims().Any(x => x.Id == MotorUninterruptedPolicyNoClaim))
            //    validation.Add(MotorLossHistoryValidationMessages.MotorUninterruptedPolicyNoClaimIdInvalid.AddParameters(new[] { MotorUninterruptedPolicyNoClaim.ToString() }));

            //if (string.IsNullOrEmpty(DateOfLoss))
            //    validation.Add(MotorLossHistoryValidationMessages.DateOfLossEmpty);

            //if (string.IsNullOrEmpty(Why))
            //    validation.Add(MotorLossHistoryValidationMessages.WhyEmpty);


            return validation;
        }
    }
}
