﻿using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.HistoryLoss.Motor
{
    public class ListMotorLossHistoryDto : Resource
    {
        public ListMotorLossHistoryDto()
        {
            
        }
        public int PartyId { get; set; }
        public MotorTypeOfLoss MotorTypeOfLoss { get; set; }
        public string DateOfLoss { get; set; }
        public MotorClaimAmount MotorClaimAmount { get; set; }
        public CurrentlyInsured CurrentlyInsured { get; set; }
        public UninterruptedPolicy UninterruptedPolicy { get; set; }
        public bool InsurerCancel { get; set; }
        public MotorCurrentTypeOfCover MotorCurrentTypeOfCover { get; set; }
        public bool PreviousComprehensiveInsurance { get; set; }
        public MotorUninterruptedPolicyNoClaim MotorUninterruptedPolicyNoClaim { get; set; }
        public string Why { get; set; }
    }
}
