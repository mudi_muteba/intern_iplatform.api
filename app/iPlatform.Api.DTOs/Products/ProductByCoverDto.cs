﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Organisations;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Products
{
    public class ProductByCoverDto : Resource, ICultureAware
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string DisplayName
        {
            get
            {
                return string.Format("{0} ({1})", Name, Code);
            }
        }
        public DateTimeDto StartDate { get; set; }
        public DateTimeDto EndDate { get; set; }
        public int CoverDefinitionId { get; set; }
        public ListOrganizationDto ProductProvider { get; set; }
        public ListOrganizationDto ProductOwner { get; set; }
    }
}