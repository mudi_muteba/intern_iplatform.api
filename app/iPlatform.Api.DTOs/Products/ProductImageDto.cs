namespace iPlatform.Api.DTOs.Products
{
    public class ProductImageDto
    {
        public string Url { get; set; }
    }
}