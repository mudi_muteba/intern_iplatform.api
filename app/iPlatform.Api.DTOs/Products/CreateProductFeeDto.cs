﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Products
{
    public class CreateProductFeeDto : AttributeValidationDto, IExecutionDto
    {
        public CreateProductFeeDto()
        {

        }
        public int ProductId { get; set; }
        public ProductFeeType ProductFeeType { get; set; }
        public PaymentPlan PaymentPlan { get; set; }
        public int? BrokerageId { get; set; }
        public decimal Value { get; set; }
        public decimal MinValue { get; set; }
        public decimal MaxValue { get; set; }
        public bool IsOnceOff { get; set; }
        public bool IsPercentage { get; set; }
        public bool AllowRefund { get; set; }
        public bool AllowProRata { get; set; }
        public int ChannelId { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
