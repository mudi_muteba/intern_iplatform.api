using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Products
{
    public class ProductInfoDto : Resource, ICultureAware
    {
        public ProductInfoDto()
        {
        }

        public string Name { get; set; }
        public string DisplayName
        {
            get
            {
                return string.Format("{0} ({1})", Name, ProductCode);
            }
        }
        public string ProductCode { get; set; }
        public DateTimeDto StartDate { get; set; }
        public DateTimeDto EndDate { get; set; }
        public int ChannelId { get; set; }
    }
}