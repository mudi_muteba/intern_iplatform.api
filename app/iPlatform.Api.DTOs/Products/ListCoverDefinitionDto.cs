using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Products
{
    public class ListCoverDefinitionDto : Resource
    {
        public string DisplayName { get; set; }
        public string CoverLevelWording { get; set; }
        public Cover Cover { get; set; }
        public CoverDefinitionType CoverDefinitionType { get; set; }
        public int VisibleIndex { get; set; }
        public ProductInfoDto Product { get; set; }
    }
}