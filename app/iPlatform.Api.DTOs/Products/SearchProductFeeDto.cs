﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Products
{
    public class SearchProductFeeDto : BaseCriteria, IValidationAvailableDto
    {
        public string Channel { get; set; }
        public string Product { get; set; }

        public SearchProductFeeDto()
        {

        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();


            return validation;
        }
    }
}