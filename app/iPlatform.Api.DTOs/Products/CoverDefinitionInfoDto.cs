using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Products
{
    public class CoverDefinitionInfoDto : Resource
    {
        public CoverDefinitionInfoDto()
        {

        }

        public string DisplayName { get; set; }
        public Cover Cover { get; set; }
        public ProductInfoDto Product { get; set; }
        public int VisibleIndex { get; set; }
        public CoverDefinitionType CoverDefinitionType { get; set; }

    }
}