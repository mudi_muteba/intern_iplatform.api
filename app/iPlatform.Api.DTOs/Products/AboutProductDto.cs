using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Products
{
    public class AboutProductDto
    {
        public AboutProductDto()
        {
            Images = new List<ProductImageDto>();
            Provider = new ProductProviderDto();
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public string DisplayName
        {
            get
            {
                return string.Format("{0} ({1})", Name, Code);
            }
        }
        public string ProductType { get; set; }
        public ProductProviderDto Provider { get; set; }
        public List<ProductImageDto> Images { get; set; }
    }
}