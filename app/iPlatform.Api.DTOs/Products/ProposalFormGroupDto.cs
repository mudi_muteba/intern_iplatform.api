using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Products
{
    public class ProposalFormGroupDto : Resource
    {
        public ProposalFormGroupDto()
        {
            Questions = new List<QuestionDefinitionDto>();
        }

        public string Name { get; set; }
        public int VisibleIndex { get; set; }
        public string QuestionGroupTranslationKey { get; set; }
        public List<QuestionDefinitionDto> Questions { get; set; }
    }
}