﻿using System.Collections.Generic;
using MasterData;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Products
{
    public class ListProductByCoverDto: ICultureAware
    {
        public List<ProductByCoverDto> Products { get; set; }
        public Cover Cover { get; set; }
    
    }
}
