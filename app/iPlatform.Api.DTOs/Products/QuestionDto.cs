﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;

namespace iPlatform.Api.DTOs.Products
{
    public class QuestionDto : Resource
    {
        public string Name { get; set; }

        public QuestionGroupDto QuestionGroup { get; set; }

        public QuestionType QuestionType { get; set; }
    }
}
