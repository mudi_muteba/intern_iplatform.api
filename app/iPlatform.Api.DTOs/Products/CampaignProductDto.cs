using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Products
{
    public class CampaignProductDto : Resource, ICultureAware
    {
        public CampaignProductDto(){}

        public ProductInfoDto Product { get; set; }
    }
}