using System.Collections.Generic;
using MasterData;
using iPlatform.Api.DTOs.Base;
using Newtonsoft.Json;

namespace iPlatform.Api.DTOs.Products
{
    public class QuestionDefinitionDto: Resource
    {
        public QuestionDefinitionDto()
        {
            Children = new List<QuestionDefinitionDto>();
            PossibleAnswers = new List<QuestionAnswer>();
        }

        public string DisplayName { get; set; }
        public string DisplayNameTranslationKey { get; set; }
        public string QuestionGroupTranslationKey { get; set; }
        public Question Question { get; set; }
        public List<QuestionAnswer> PossibleAnswers { get; set; }        
        public QuestionDefinitionDto Parent { get; set; }
        public List<QuestionDefinitionDto> Children { get; set; }
        public QuestionDefinitionType QuestionDefinitionType { get; set; }
        public QuestionDefinitionGroupType QuestionDefinitionGroupType { get; set; }
        public int VisibleIndex { get; set; }
        public int MasterId { get; set; }
        public bool RequiredForQuote { get; set; }
        public bool RatingFactor { get; set; }
        public bool ReadOnly { get; set; }
        public string ToolTip { get; set; }
        public string TooltipTranslationKey { get; set; }
        public string DefaultValue { get; set; }
        public string RegexPattern { get; set; }
        public int GroupIndex { get; set; }
    }
}