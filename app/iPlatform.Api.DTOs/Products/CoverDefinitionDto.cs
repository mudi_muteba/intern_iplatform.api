using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.Products
{
    public class CoverDefinitionDto : Resource, ICultureAware
    {
        public CoverDefinitionDto()
        {
            QuestionDefinitions = new List<QuestionDefinitionDto>();
            Benefits = new List<ProductBenefitDto>();
        }

        public string DisplayName { get; set; }
        public Cover Cover { get; set; }
        public ListProductDto Product { get; set; }
        public int VisibleIndex { get; set; }
        public CoverDefinitionType CoverDefinitionType { get; set; }
        public List<QuestionDefinitionDto> QuestionDefinitions { get; set; }
        public List<ProductBenefitDto> Benefits { get; set; }
    }
}