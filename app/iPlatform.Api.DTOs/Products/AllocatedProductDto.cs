using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Organisations;
using MasterData;

namespace iPlatform.Api.DTOs.Products
{
    public class AllocatedProductDto : Resource, ICultureAware
    {
        public AllocatedProductDto()
        {
            Fees = new List<ProductFeeDto>();
            Covers = new List<ListCoverDefinitionDto>();
        }
        
        public string Name { get; set; }
        public string DisplayName
        {
            get
            {
                return string.Format("{0} ({1})", Name, ProductCode);
            }
        }
        public string ProductCode { get; set; }
        public DateTimeDto StartDate { get; set; }
        public DateTimeDto EndDate { get; set; }
        public ListOrganizationDto ProductProvider { get; set; }
        public ListOrganizationDto ProductOwner { get; set; }
        public virtual ProductType ProductType { get; set; }
        public virtual string AgencyNumber { get; set; }
        public List<ProductFeeDto> Fees { get; set; }
        public List<ListCoverDefinitionDto> Covers { get; set; }
        public bool IsSelectedExcess { get; set; }
        public bool IsVoluntaryExcess { get; set; }
        public bool ShowInReporting { get; set; }
        public bool BenefitAcceptanceCheck { get; set; }
        public bool HideSasriaLine { get; set; }
        public bool HideCompareButton { get; set; }
    }
}