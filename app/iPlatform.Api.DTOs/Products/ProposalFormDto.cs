using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Products
{
    public class ProposalFormDto : Resource, ICultureAware
    {
        public ProposalFormDto()
        {
            Groups = new List<ProposalFormGroupDto>();
        }

        public AllocatedProductDto Product { get; set; }
        public List<ProposalFormGroupDto> Groups { get; set; }
    }
}