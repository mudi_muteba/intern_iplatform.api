﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Products
{
    public class BenefitDto : Resource
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public bool ShowToClient { get; set; }

        public int VisibleIndex { get; set; }
    }
}
