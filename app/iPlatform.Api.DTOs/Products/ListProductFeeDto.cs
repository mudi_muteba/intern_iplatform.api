﻿using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Organisations;
using MasterData;

namespace iPlatform.Api.DTOs.Products
{
    public class ListProductFeeDto : Resource, ICultureAware
    {
        public ListProductFeeDto()
        {
        }

        public ProductInfoDto Product { get; set; }
        public ProductFeeType ProductFeeType { get; set; }
        public PaymentPlan PaymentPlan { get; set; }
        public OrganisationDto Brokerage { get; set; }
        public MoneyDto Value { get; set; }
        public MoneyDto MinValue { get; set; }
        public MoneyDto MaxValue { get; set; }
        public bool IsOnceOff { get; set; }
        public bool IsPercentage { get; set; }
        public bool AllowRefund { get; set; }
        public bool AllowProRata { get; set; }
        public ChannelInfoDto Channel { get; set; }
    }
}