using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Products
{
    public class ProductBenefitDto
    {
        public ProductBenefitDto()
        {

        }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool ShowToClient { get; set; }
        public int VisibleIndex { get; set; }
    }
}