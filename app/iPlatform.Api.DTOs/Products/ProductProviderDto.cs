namespace iPlatform.Api.DTOs.Products
{
    public class ProductProviderDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string TradingName { get; set; }
        public string RegisteredName { get; set; }
        public string Description { get; set; }
    }
}