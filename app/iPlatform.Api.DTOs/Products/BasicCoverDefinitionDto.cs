using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Products
{
    public class BasicCoverDefinitionDto : Resource
    {
        public BasicCoverDefinitionDto()
        {

        }

        public string DisplayName { get; set; }
        public Cover Cover { get; set; }
        public ListProductDto Product { get; set; }
        public int VisibleIndex { get; set; }
        public CoverDefinitionType CoverDefinitionType { get; set; }

    }
}