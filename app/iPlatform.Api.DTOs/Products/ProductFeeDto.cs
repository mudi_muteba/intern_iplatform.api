using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.Products
{
    public class ProductFeeDto: Resource, ICultureAware
    {
        public ProductFeeDto()
        {
        }

        public ProductFeeType ProductFeeType { get; set; }
        public PaymentPlan PaymentPlan { get; set; }
        public MoneyDto Value { get; set; }
        public MoneyDto MinValue { get; set; }
        public MoneyDto MaxValue { get; set; }
        public bool IsOnceOff { get; set; }
        public bool IsPercentage { get; set; }
        public bool AllowRefund { get; set; }
        public bool AllowProRata { get; set; }
        public int ProductId { get; set; }
        public int ChannelId { get; set; }
    }
}