﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Party.Assets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Sales
{
    public class SalesDetailDto: Resource
    {
        public SalesDetailDto() { }

        public string PolicyNumber { get; set; }
        public string IdNumber { get; set; }
        public AssetDto Asset { get; set; }
        public string VehicleRetailPrice { get; set; }
        public string CurrentDate { get; set; }
        public SalesFNIDto Agent { get; set; }
        public LeadBasicDto Lead { get; set; }
        public SalesStructureDto SalesStructure { get; set; }
    }
}
