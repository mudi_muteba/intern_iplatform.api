﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Sales
{
    public class CreateSalesDetailsDto : AttributeValidationDto, IExecutionDto
    {
        public CreateSalesDetailsDto()
        {
            SalesDetails = new List<SalesAssetDto>();
            Context = DtoContext.NoContext();
        }
        public List<SalesAssetDto> SalesDetails { get; set; }

        public virtual string PolicyNumber { get; set; }
        public virtual string IdNumber { get; set; }
        public virtual int AssetId { get; set; }
        public virtual string VehicleRetailPrice { get; set; }
        public virtual string CurrentDate { get; set; }
        public virtual int SalesFNIId { get; set; }
        public virtual int LeadId { get; set; }
        public virtual int SaleStructureId { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }
        public DtoContext Context { get; private set; }
    }
}
