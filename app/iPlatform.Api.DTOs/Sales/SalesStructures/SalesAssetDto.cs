﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Sales
{
    public class SalesAssetDto: Resource
    {
        public SalesAssetDto()
        {

        }
        public SalesAssetDto(int assetId, string vehicleRetailPrice)
        {
            AssetId = assetId;
            VehicleRetailPrice = vehicleRetailPrice;
        }
        public int AssetId { get; set; }
        public string VehicleRetailPrice { get; set; }
        public int SalesDetailId { get; set; }
    }
}
