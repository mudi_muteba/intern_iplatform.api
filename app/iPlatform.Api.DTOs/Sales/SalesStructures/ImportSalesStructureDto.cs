﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Admin;

namespace iPlatform.Api.DTOs.Sales
{
    public class ImportSalesStructureDto : AttributeValidationDto
    {
        public DtoContext Context { get; private set; }

        public ImportSalesStructureDto()
        {
            Context = DtoContext.NoContext();
            Assets = new List<SalesAssetDto>();
        }

        public string SystemMode { get; set; }
        public string CurrentDate { get; set; }
        public string PolicyNumber { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string FinanceCompanyCode { get; set; }
        public string FinanceCompanyName { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string CompanyCode { get; set; }
        public int SaleFNIId { get; set; }
        public int LeadId { get; set; }
        public ChannelInfoDto Channel { get; set; }
        public List<SalesAssetDto> Assets { get; set; }


        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}
