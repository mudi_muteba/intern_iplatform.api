﻿using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Sales
{
    public class SalesStructureDto : Resource
    {
        public SalesStructureDto() { }
        public SalesTagDto BranchSalesTag { get; set; }
        public SalesTagDto CompanySalesTag { get; set; }
        public SalesTagDto GroupSalesTag { get; set; }
        public virtual int TagLeft { get; set; }
        public virtual int TagRight { get; set; }
        public virtual int Level { get; set; }
        public ChannelInfoDto Channel { get; set; }
    }
}
