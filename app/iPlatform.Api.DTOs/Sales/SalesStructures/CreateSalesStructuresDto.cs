﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Sales
{
    public class CreateSalesStructuresDto : AttributeValidationDto, IExecutionDto, IChannelAwareDto 
    {

        public DtoContext Context { get; private set; }
        public CreateSalesStructuresDto()
        {
            Context = DtoContext.NoContext();
            SalesStructures = new List<SalesStructureDto>();
        }

        public List<SalesStructureDto> SalesStructures { get; set; }
        public int ChannelId { get; set; }




        public void SetContext(DtoContext context)
        {
            Context = context;
        }


        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if(ChannelId == 0)
                validation.Add(SalesValidationMessages.ChannelIdRequired);

            if (!SalesStructures.Any())
                validation.Add(SalesValidationMessages.SaleStructuresRequired);

            int count = 1;
            SalesStructures.ForEach(x => {

                if (x.GroupSalesTag.Id == 0)
                    validation.Add(SalesValidationMessages.SaleStructureGroupSalesTagIdRequired.AddParameters(new[] { count.ToString() }));
                if (x.CompanySalesTag.Id == 0)
                    validation.Add(SalesValidationMessages.SaleStructureParentSalesTagIdRequired.AddParameters(new[] { count.ToString() }));
                if (x.BranchSalesTag.Id == 0)
                    validation.Add(SalesValidationMessages.SaleStructureSalesTagIdRequired.AddParameters(new[] { count.ToString() }));
                if (x.Level == 0)
                    validation.Add(SalesValidationMessages.SaleStructureLevelRequired.AddParameters(new[] { count.ToString() }));
                if (x.Channel.Id == 0)
                    validation.Add(SalesValidationMessages.SaleStructureChannelIdRequired.AddParameters(new[] { count.ToString() }));

                count++;
            });

            return validation;
        }

    }
}
