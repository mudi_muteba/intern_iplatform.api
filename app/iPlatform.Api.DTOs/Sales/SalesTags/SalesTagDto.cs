﻿using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Sales
{
    public class SalesTagDto : Resource
    {
        public DtoContext Context { get; private set; }

        public SalesTagDto()
        {
            
        }

        public SalesTagType SalesTagType { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

    }
}
