﻿using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Sales
{
    public class CreateSalesTagDtoOLD : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public CreateSalesTagDtoOLD()
        {
            Context = DtoContext.NoContext();
        }

        public string SystemMode { get; set; }
        public string CurrentDate { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string FinanceCompanyCode { get; set; }
        public string FinanceCompanyName { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string CompanyCode { get; set; }
        public SalesTagType SalesTagType { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}
