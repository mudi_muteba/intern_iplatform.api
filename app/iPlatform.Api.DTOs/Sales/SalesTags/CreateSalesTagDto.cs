﻿using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Sales
{
    public class CreateSalesTagDto : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public CreateSalesTagDto()
        {
            Context = DtoContext.NoContext();
        }

        public SalesTagType SalesTagType { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}
