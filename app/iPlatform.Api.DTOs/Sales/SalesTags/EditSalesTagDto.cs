﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Sales
{
    public class EditSalesTagDto : IExecutionDto, IAffectExistingEntity
    {
        public EditSalesTagDto()
        {
            Context = DtoContext.NoContext();
        }

        public DtoContext Context { get; private set; }

        public string SystemMode { get; set; }
        public string CurrentDate { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string FinanceCompanyCode { get; set; }
        public string FinanceCompanyName { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string CompanyCode { get; set; }

        public void SetContext(DtoContext context)
        {

        }

        public int Id { get; set; }
    }
}
