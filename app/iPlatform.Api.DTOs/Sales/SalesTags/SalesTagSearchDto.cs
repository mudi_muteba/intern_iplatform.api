﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Sales
{
    public class SalesTagSearchDto : BaseCriteria, IValidationAvailableDto
    {

        public string Code { get; set; }
        public string Name { get; set; }
        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            var valid = !string.IsNullOrEmpty(Code) || !string.IsNullOrEmpty(Name);

            if (valid)
                return validation;

            validation.Add(SalesTagValidationMessages.InvalidSearchCriteria);

            return validation;
        }
    }
}
