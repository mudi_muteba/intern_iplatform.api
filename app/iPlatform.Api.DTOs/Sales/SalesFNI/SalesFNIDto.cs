﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.Sales
{
    public class SalesFNIDto : Resource, IAuditableDto, ICultureAware
    {
        public SalesFNIDto()
        {
            Events = new List<AuditEventDto>();
            PartyType = PartyTypes.FNI;
        }

        public int ChannelId { get; set; }
        public int PartyId { get; set; }
        public PartyType PartyType { get; set; }
        public string UserName { get; set; }
        public string UserWorkTelephoneCode { get; set; }
        public string UserWorkTelephoneNumber { get; set; }
        public string UserEmailAddress { get; set; }
        public string UserFaxCode { get; set; }
        public string UserFaxNumber { get; set; }
        public string EmployeeNumber { get; set; }
        public string FiFirstName { get; set; }
        public string FiLastName { get; set; }
        public string FiidNumber { get; set; }
        public string DisplayName { get; set; }
        public DateTime? DateCreated { get; set; }
        public List<AuditEventDto> Events { get; private set; }
    }
}
