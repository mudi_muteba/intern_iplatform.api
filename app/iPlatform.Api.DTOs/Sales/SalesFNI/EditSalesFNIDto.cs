﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Sales
{
    public class EditSalesFNIDto : IExecutionDto, IAffectExistingEntity
    {
        public EditSalesFNIDto()
        {
            Context = DtoContext.NoContext();
        }

        public DtoContext Context { get; private set; }

        public string UserName { get; set; }
        public string UserWorkTelephoneCode { get; set; }
        public string UserWorkTelephoneNumber { get; set; }
        public string UserEmailAddress { get; set; }
        public string UserFaxCode { get; set; }
        public string UserFaxNumber { get; set; }
        public string EmployeeNumber { get; set; }
        public string FiFirstName { get; set; }
        public string FiLastName { get; set; }
        public string FiidNumber { get; set; }

        public void SetContext(DtoContext context)
        {

        }

        public int Id { get; set; }
    }
}
