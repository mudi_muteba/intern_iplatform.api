﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;
using ValidationMessages;
using ValidationMessages.Sales;

namespace iPlatform.Api.DTOs.Sales
{
    public class CreateSalesFNIDto : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public CreateSalesFNIDto()
        {
            Context = DtoContext.NoContext();
            PartyType = PartyTypes.FNI;
        }

        public int ChannelId { get; set; }
        public int PartyId { get; set; }
        public PartyType PartyType { get; set; }
        public string UserName { get; set; }
        public string UserWorkTelephoneCode { get; set; }
        public string UserWorkTelephoneNumber { get; set; }
        public string UserEmailAddress { get; set; }
        public string UserFaxCode { get; set; }
        public string UserFaxNumber { get; set; }
        public string EmployeeNumber { get; set; }
        public string FiFirstName { get; set; }
        public string FiLastName { get; set; }
        public string FiidNumber { get; set; }

        public string DisplayName { get; set; }

        public DateTime? DateCreated { get; set; }


        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(UserName))
            {
                validation.Add(SalesFNIValidationMessages.UserNameRequired);
            }

            if (string.IsNullOrWhiteSpace(UserWorkTelephoneNumber))
            {
                validation.Add(SalesFNIValidationMessages.WorkTelephoneNumberRequired);
            }

            if (string.IsNullOrWhiteSpace(UserEmailAddress))
            {
                validation.Add(SalesFNIValidationMessages.EmailAddressRequired);
            }

            if (string.IsNullOrWhiteSpace(EmployeeNumber))
            {
                validation.Add(SalesFNIValidationMessages.EmployeeNumberRequired);
            }

            if (string.IsNullOrWhiteSpace(FiFirstName))
            {
                validation.Add(SalesFNIValidationMessages.FirstNameRequired);
            }

            if (string.IsNullOrWhiteSpace(FiLastName))
            {
                validation.Add(SalesFNIValidationMessages.LastNameRequired);
            }

            if (string.IsNullOrWhiteSpace(FiidNumber))
            {
                validation.Add(SalesFNIValidationMessages.IdNumberRequired);
            }

            return validation;
        }
    }
}
