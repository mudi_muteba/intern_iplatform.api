﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.CimsDataSync
{
    public class CimsDataSyncProductPricingStructureStoredProcedureDto : ICultureAware
    {
        public int RowsAffected { get; set; }

        public string Message { get; set; }
    }
}
