﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.CimsDataSync
{
    public class CimsDataSyncChannelUserDto
    {
        public string ProductCodes { get; set; }

        public string DatabaseName { get; set; }

        public string Client { get; set; }
    }
}
