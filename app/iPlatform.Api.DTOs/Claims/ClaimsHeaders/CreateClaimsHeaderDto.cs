﻿using iPlatform.Api.DTOs.Base;
using System;

namespace iPlatform.Api.DTOs.Claims.ClaimsHeaders
{
    public class CreateClaimsHeaderDto : AttributeValidationDto, IExecutionDto, IChannelAwareDto
    {
        public CreateClaimsHeaderDto()
        {
            Context = DtoContext.NoContext();
            claimDto = new ClaimsItemDtos();
        }

        public DtoContext Context { get; private set; }
        public int PartyId {get; set;}

        public int PolicyHeaderId {get; set;}

        public ClaimsItemDtos claimDto { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public int ChannelId { get; set; }

        public DateTime? DateOfLoss { get; set; }
    }
}
