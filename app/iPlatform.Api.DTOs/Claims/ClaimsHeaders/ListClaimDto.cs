﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Party;
using iPlatform.Api.DTOs.Policy;

namespace iPlatform.Api.DTOs.Claims.ClaimsHeaders
{
    public class ListClaimDto : Resource, ICultureAware
    {
        public PartyDto Party { get; set; }
        public string ExternalReference { get; set; }
        public PolicyHeaderDto Proposal { get; set; }
    }
}