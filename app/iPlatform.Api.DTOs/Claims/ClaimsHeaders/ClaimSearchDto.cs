﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Claims;

namespace iPlatform.Api.DTOs.Claims.ClaimsHeaders
{
    public class ClaimSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public int? PolicyHeaderId { get; set; }
        public int? PartyId { get; set; }
        public string ExternalReference { get; set; }
        public string IdNumber { get; set; }
        public string InsurerCode { get; set; }
        public string ContactNumber { get; set; }

        public ClaimSearchDto()
        {
           
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            var valid = PolicyHeaderId.HasValue ||
                        PartyId.HasValue ||
                        !string.IsNullOrEmpty(ExternalReference) ||
                        !string.IsNullOrEmpty(IdNumber) ||
                        !string.IsNullOrEmpty(InsurerCode) ||
                        !string.IsNullOrEmpty(ContactNumber);

            if (valid)
                return validation;

            validation.Add(ClaimsHeaderValidationMessages.InvalidSearchCriteria);

            return validation;
        }
    }
}