﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Claims;

namespace iPlatform.Api.DTOs.Claims.ClaimsHeaders
{

    public class AcceptClaimResponseDto : Resource
    {
        public bool IsSuccess { get; set; }

        public AcceptClaimResponseDto(int id)
        {
            this.Id = id;
            IsSuccess = false;
        }

        public AcceptClaimResponseDto Success()
        {
            this.IsSuccess = true;
            return this;
        }
    }
}
