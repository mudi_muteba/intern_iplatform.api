﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Claims.ClaimsItems;

namespace iPlatform.Api.DTOs.Claims.ClaimsHeaders
{
    public class ClaimsItemDtos
    {
        public ClaimsItemDtos()
        {
            PolicyItems = new List<ClaimsItemDto>();
        }
        public List<ClaimsItemDto> PolicyItems { get; set; }
    }
}
