﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Claims;

namespace iPlatform.Api.DTOs.Claims.ClaimsHeaders
{

    public class DisableClaimDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity//, IChannelAwareDto
    {
        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                list.Add(ClaimsHeaderValidationMessages.InvalidId);
            }

            return list;
        }
    }
}
