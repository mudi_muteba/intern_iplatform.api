﻿namespace iPlatform.Api.DTOs.Claims.ClaimsHeaders
{

    public class EditClaimDto//: IChannelAwareDto
    {
        public int PolicyItemId { get; set; }
        public int ClaimTypeId { get; set; }
        public string Description { get; set; }
    }
}
