﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using iPlatform.Api.DTOs.Party;
using iPlatform.Api.DTOs.Policy;

namespace iPlatform.Api.DTOs.Claims.ClaimsHeaders
{

    public class ClaimsHeaderDto : Resource, IAuditableDto, ICultureAware
    {
        public ClaimsHeaderDto()
        {
            SimpleAudit = new AuditDto();
            Events = new List<AuditEventDto>();
            ClaimsItems = new List<BasicClaimsItemDto>();
            PolicyHeader = new BasicPolicyHeaderDto();

        }

        public string ClaimNumber { get; set; }
        public string Description { get; set; }
        public decimal ClaimAmount { get; set; }
        public PartyDto Party { get; set; }
        public BasicPolicyHeaderDto PolicyHeader { get; set; }
        public List<BasicClaimsItemDto> ClaimsItems { get; set; }
        public DateTimeDto CreatedAt { get; set; }
        public DateTimeDto ModifiedAt { get; set; }
        public DateTimeDto DateOfLoss { get; set; }
        public string ClaimStatus { get; set; }
        public string ClaimType { get; set; }
        public AuditDto SimpleAudit { get; private set; }
        public List<AuditEventDto> Events { get; private set; }


    }
}
