﻿using iPlatform.Api.DTOs.Claims.Questions;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims
{
    public class ProductClaimsQuestionDefinitionDto
    {
        public ProductClaimsQuestionDefinitionDto() { }

        public int Id { get; set; }
        public string Name { get; set; }
        public int ClaimsTypeId { get; set; }
        public int VisibleIndex { get; set; }
        public string ToolTip { get; set; }
        public string DefaultValue { get; set; }
        public string RegexPattern { get; set; }
        public string DisplayName { get; set; }
        public bool? Required { get; set; }
        public bool? FirstPhase { get; set; }
        public bool? SecondPhase { get; set; }
        public ClaimsQuestion ClaimsQuestion { get; set; }
        public ClaimsQuestionGroup ClaimsQuestionGroup { get; set; }
        public int ParentProductClaimsQuestionDefinitionId { get; set; }
    }
}
