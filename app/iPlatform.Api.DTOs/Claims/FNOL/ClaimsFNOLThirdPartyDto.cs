﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims.FNOL
{
    public class ClaimsFnolThirdPartyDto
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string ContactDetails { get; set; }

        public string DriverFirstName { get; set; }
        public string DriverSurname { get; set; }
        public string DriverID { get; set; }
        public string DriverLicenceNumber { get; set; }

        public string VehicleMake { get; set; }
        public string VehicleModel { get; set; }
        public string VehicleRegistration { get; set; }
    }
}
