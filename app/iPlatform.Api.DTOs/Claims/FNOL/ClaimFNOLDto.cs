﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims.FNOL
{
    public class ClaimFnolDto
    {
        public ClaimFnolDto()
        {
            WitnessDetails = new List<ClaimFnolWitnessDto>();
            ThirdPartyDetails = new List<ClaimsFnolThirdPartyDto>();
        }

        public Guid Id { get; set; }
        public string PolicyNo { get; set; }
        public DateTime? ReportingDateTime { get; set; }

        public string ExternalItemIdentifier { get; set; }
        public string Cover { get; set; }


        public string Benefit { get; set; }
        public string DescriptionOfLoss { get; set; }
        public string EventType { get; set; }
        public int EventTypeId { get; set; }
        public string CauseCode { get; set; }
        public int CauseCodeId { get; set; }
        public bool IsSpecified { get; set; } // applies to all risk cover only 

        public string ItemDescription { get; set; }

        public string SAPSCaseNumber { get; set; }
        public bool SAPSOnScene { get; set; }
        public bool DrugAlcoholTest { get; set; }

        public DateTime? DateTimeOfLoss { get; set; }
        public string AreaOfLoss { get; set; }
        public string EstimatedLoss { get; set; }

        /// <summary>
        /// driver
        /// </summary>
        public string DriverFirstName { get; set; }
        public string DriverSurname { get; set; }
        public string DriverID { get; set; }
        public string DriverLicenceNumber { get; set; }

        public bool VehicleDriveable { get; set; }
        public bool RoadsideAssitanceContacted { get; set; }
        public string PreferredRepairLocation { get; set; }
        public bool VehicleUnderWarranty { get; set; }

        /// <summary>
        /// Towing
        /// </summary>
        public bool VehicleTowed { get; set; }
        public string TowingCompany { get; set; }
        public string TowingContactDetails { get; set; }

        public bool Witness { get; set; }
        public List<ClaimFnolWitnessDto> WitnessDetails { get; set; }
        public List<ClaimsFnolThirdPartyDto> ThirdPartyDetails { get; set; }

        /// <summary>
        /// Funeral
        /// </summary>
        public string FuneralCauseOfDeath { get; set; }
        public string FuneralCallerFirstName { get; set; }
        public string FuneralCallerSurname { get; set; }
        public string FuneralCallerMobileNumber { get; set; }
        public string FuneralCallerEmailAddress { get; set; }
        public string OriginalRequest { get; set; }
    }
}
