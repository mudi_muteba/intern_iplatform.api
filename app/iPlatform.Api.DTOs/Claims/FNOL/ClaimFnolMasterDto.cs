﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims.FNOL
{
    public class ClaimFnolMasterDto
    {
        public ClaimFnolMasterDto()
        {
            ClaimDetails = new List<ClaimFnolDto>();
        }

        [System.Xml.Serialization.XmlArray]
        public List<ClaimFnolDto> ClaimDetails { get; set; }
        public string OriginalRequest { get; set; }
    }
}
