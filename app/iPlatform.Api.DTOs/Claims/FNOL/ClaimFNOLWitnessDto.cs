﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims.FNOL
{
    public class ClaimFnolWitnessDto
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string ContactDetails { get; set; }
    }
}
