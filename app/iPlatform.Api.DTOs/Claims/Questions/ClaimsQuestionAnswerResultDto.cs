﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims.Questions
{
    public class ClaimsQuestionAnswerResultDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public ClaimsQuestionAnswerResultDto() { }

        public ClaimsQuestionAnswerResultDto(int id, string text)
        {
            Id = id;
            Text = text;

        }
    }
}
