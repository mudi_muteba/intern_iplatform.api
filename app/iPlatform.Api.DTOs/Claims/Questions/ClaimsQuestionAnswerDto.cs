﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims.Questions
{
    public class ClaimsQuestionAnswerDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Answer { get; set; }

        public ClaimsQuestionAnswerDto() { }

        public ClaimsQuestionAnswerDto(int id, string name, string answer)
        {
            Id = id;
            Name = name;
            Answer = answer;
        }
    }

}
