﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Claims.ClaimTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims.Questions
{
    public class ClaimsTypeDetailDto : Resource
    {
        public ClaimsPhasesRequiredDto PhasesRequired { get; set; }
        public  ClaimsTypeDto Type { get; set; }
        public ClaimsQuestionStructureDto Structure { get; set; }

        public ClaimsTypeDetailDto()
        {
            
        }

        public ClaimsTypeDetailDto(ClaimsTypeDto type, ClaimsQuestionStructureDto structure)
        {
            Type = type;
            Structure = structure;
        }
        public ClaimsTypeDetailDto(ClaimsTypeDto type, ClaimsQuestionStructureDto structure, ClaimsPhasesRequiredDto phaseRequired)
        {
            Type = type;
            Structure = structure;
            PhasesRequired = phaseRequired;
        }
    }
}
