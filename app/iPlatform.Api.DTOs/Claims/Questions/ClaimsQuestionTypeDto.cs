﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims.Questions
{
    public class ClaimsQuestionTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ClaimsQuestionTypeDto()
        {
        }

        public ClaimsQuestionTypeDto(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
