﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Claims.ClaimTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims.Questions
{
    public class ClaimsPhasesRequiredDto
    {
        public bool FirstPhase { get; set; }
        public bool SecondPhase { get; set; }

        public ClaimsPhasesRequiredDto()
        {
            FirstPhase = false;
            SecondPhase = false;
        }

        public ClaimsPhasesRequiredDto(bool firstPhase, bool secondPhase)
        {
            FirstPhase = firstPhase;
            SecondPhase = secondPhase;
        }
    }
}
