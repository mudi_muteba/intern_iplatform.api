﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims.Questions
{
    public class ClaimsQuestionStructureDto
    {
        public List<ClaimsQuestionGroupDto> Groups { get; set; }

        public ClaimsQuestionStructureDto()
        {
        }

        public ClaimsQuestionStructureDto(List<ClaimsQuestionGroupDto> groups)
        {
            Groups = groups;
        }
    }
}
