﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims.Questions
{
    public class ClaimsQuestionGroupDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ClaimsQuestionDto> Questions { get; set; }

        public ClaimsQuestionGroupDto()
        {
            
        }
        public ClaimsQuestionGroupDto(int id, string name, List<ClaimsQuestionDto> questions)
        {
            Id = id;
            Name = name;
            Questions = questions;
        }
    }
}
