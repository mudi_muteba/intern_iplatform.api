﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Claims.Questions
{
    public class ClaimsQuestionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public ClaimsQuestionTypeDto Type { get; set; }
        public ClaimsQuestionTypeDto MobileType { get; set; }
        public List<ClaimsQuestionAnswerDto> PossibleAnswers { get; set; }
        public bool? Required { get; set; }
        public bool? FirstPhase { get; set; }
        public bool? SecondPhase { get; set; }
        public int VisibleIndex { get; set; }
        public int ParentClaimsQuestionId { get; set; }
        public ClaimsQuestionDto ParentClaimsQuestion { get; set; }
        public ClaimsQuestionDto()
        {
        }

        public ClaimsQuestionDto(int id, string name, string displayName, ClaimsQuestionTypeDto type, ClaimsQuestionTypeDto mobileType, int visibleIndex, bool? required, bool? firstPhase, bool? secondPhase, List<ClaimsQuestionAnswerDto> possibleAnswers = null, int parentClaimsQuestionId = 0)
        {
            Id = id;
            Name = name;
            DisplayName = displayName;
            Type = type;
            PossibleAnswers = possibleAnswers ?? new List<ClaimsQuestionAnswerDto>();
            VisibleIndex = visibleIndex;
            Required = required ?? false;
            FirstPhase = firstPhase ?? false;
            SecondPhase = secondPhase ?? false;
            ParentClaimsQuestionId = parentClaimsQuestionId;
            MobileType = mobileType;
        }
    }
}
