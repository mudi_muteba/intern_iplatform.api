﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Claims;

namespace iPlatform.Api.DTOs.Claims.ClaimTypes
{
    public class ClaimTypeSearchDto : IExecutionDto
    {
        public int? OrganizationId { get; set; }

        public ClaimTypeSearchDto()
        {
            Context = DtoContext.NoContext();
        }

        public ClaimTypeSearchDto(int? organizationId)
        {
            this.OrganizationId = organizationId;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}