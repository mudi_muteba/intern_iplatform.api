﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Claims.ClaimTypes
{
    public class ClaimsTypeDto : Resource
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public ClaimsTypeCategoryDto ClaimsTypeCategory { get; set; }
        public int VisibleIndex { get; set; }
        public ClaimsTypeDto()
        {
        }
        public ClaimsTypeDto(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
