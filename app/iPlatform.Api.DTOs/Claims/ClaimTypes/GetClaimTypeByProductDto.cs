﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Claims;

namespace iPlatform.Api.DTOs.Claims.ClaimTypes
{
    public class GetClaimTypeByProductDto : IExecutionDto
    {
        public int? ProductId { get; set; }
        public int Id { get; set; }
        public GetClaimTypeByProductDto()
        {
            Context = DtoContext.NoContext();
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
    }
}