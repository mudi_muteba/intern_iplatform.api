﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Policy;
using MasterData;
using ValidationMessages;
using ValidationMessages.Claims;

namespace iPlatform.Api.DTOs.Claims.ClaimsItems
{
    public class ClaimsItemDto : Resource, IExecutionDto, IValidationAvailableDto, IAffectExistingEntity//, ICultureAware
    {
        public ClaimsItemDto()
        {

        }

        public int PolicyItemId { get; set; }
        public PolicyItemDto PolicyItem { get; set; }
        public int ClaimTypeId { get; set; }
        public ClaimsType ClaimsType { get; set; }
        public int ClaimsHeaderId { get; set; }
        public ClaimsHeaderDto ClaimsHeader { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public List<ClaimsItemQuestionAnswerDto> Answers { get; set; }


        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id <= 0)
                list.Add(ClaimsItemValidationMessages.InvalidId.AddParameters(new[] { Id.ToString() }));

            if (ClaimsHeaderId <= 0)
                list.Add(ClaimsItemValidationMessages.InvalidClaimsHeaderId);

            return list;
        }
    }
}
