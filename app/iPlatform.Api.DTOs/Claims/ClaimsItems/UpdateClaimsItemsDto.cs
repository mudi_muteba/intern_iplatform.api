﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using ValidationMessages;
using ValidationMessages.Claims;

namespace iPlatform.Api.DTOs.Claims.ClaimsItems
{
    public class UpdateClaimsItemsDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity//, IChannelAwareDto
    {

        public UpdateClaimsItemsDto()
        {
            Context = DtoContext.NoContext();
            claimDto = new ClaimsItemDtos();
        }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public int Id { get; set; }
        public int PartyId { get; set; } 
        public int ClaimsHeaderId { get; set; }
        public ClaimsItemDtos claimDto { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (PartyId <= 0)
            {
                validation.Add(ClaimsItemValidationMessages.InvalidPartyId);
            }

            if (ClaimsHeaderId <= 0)
                validation.Add(ClaimsItemValidationMessages.InvalidClaimsHeaderId);
            else
                Id = ClaimsHeaderId;

            return validation;
        }


    }
}
