﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.Claims.ClaimsItems
{
    public class BasicClaimsItemDto : Resource, ICultureAware
    {
        public BasicClaimsItemDto()
        {

        }

        public string Description { get; set; }
        public string PolicyDescription { get; set; }
        public ClaimsType ClaimsType { get; set; }
        public DateTimeDto CreatedAt { get; set; }
        public DateTimeDto ModifiedAt { get; set; }
        public int TotalQuestions { get; set; }
        public int TotalQuestionsAnswered { get; set; }
    }
}
