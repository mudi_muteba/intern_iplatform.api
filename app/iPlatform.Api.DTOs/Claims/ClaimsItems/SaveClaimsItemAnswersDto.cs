﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Claims;

namespace iPlatform.Api.DTOs.Claims.ClaimsItems
{
    public class SaveClaimsItemAnswersDto : Resource, IExecutionDto, IValidationAvailableDto, IAffectExistingEntity//, IChannelAwareDto
    {
        public SaveClaimsItemAnswersDto()
        {
            Answers = new List<SaveClaimsItemAnswerDto>();
        }

        public int PartyId { get; set; }

        public int ClaimsHeaderId { get; set; }
        public List<SaveClaimsItemAnswerDto> Answers { get; set; }


        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }
        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id <= 0)
                list.Add(ClaimsItemValidationMessages.InvalidId.AddParameters(new[] { Id.ToString() }));

            if (ClaimsHeaderId <= 0)
                list.Add(ClaimsItemValidationMessages.InvalidClaimsHeaderId);

            return list;
        }
    }
}
