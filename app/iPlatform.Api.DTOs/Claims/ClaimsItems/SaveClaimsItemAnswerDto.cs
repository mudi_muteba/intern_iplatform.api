﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Claims.ClaimsItems
{
    public class SaveClaimsItemAnswerDto : Resource
    {
        public string Answer { get; set; }
    }
}
