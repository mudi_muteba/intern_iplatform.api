﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Claims;

namespace iPlatform.Api.DTOs.Claims.ClaimsItems
{
    public class DisableClaimItemDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity//, IChannelAwareDto
    {
        public int Id { get; set; }
        public int PartyId { get; set; }
        public int ClaimsHeaderId { get; set; }

        public DisableClaimItemDto()
        {
        }
        public DisableClaimItemDto(int claimsHeaderId, int id)
        {
            this.Id = id;
            this.ClaimsHeaderId = claimsHeaderId;
        }


        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id <= 0)
                list.Add(ClaimsItemValidationMessages.InvalidId.AddParameters(new[] { Id.ToString() }));

            if (ClaimsHeaderId <= 0)
                list.Add(ClaimsItemValidationMessages.InvalidClaimsHeaderId.AddParameters(new[] { ClaimsHeaderId.ToString() }));

            return list;
        }
    }
}
