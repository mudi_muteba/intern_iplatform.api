﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Claims.ClaimsItems
{
    public class ClaimsItemQuestionAnswerDto : Resource
    {
        public ClaimsItemQuestionAnswerDto()
        {

        } 
        public string Answer { get; set; }
        public ClaimsQuestionDefinition ClaimsQuestionDefinition { get; set; }
        public List<ClaimsQuestionAnswer> Options { get; set; }

    }
}
