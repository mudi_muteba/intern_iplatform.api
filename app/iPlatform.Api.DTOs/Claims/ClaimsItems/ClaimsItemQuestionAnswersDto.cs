﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Claims.ClaimsItems
{
    public class ClaimsItemQuestionAnswersDto 
    {
        public ClaimsItemQuestionAnswersDto()
        {
            Answers = new List<ClaimsItemAnswerDto>();
        }

        public List<ClaimsItemAnswerDto> Answers { get; set; }
    }
}
