﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Claims.ClaimsItems
{
    public class ClaimsItemAnswerDto : Resource
    {
        public ClaimsItemAnswerDto()
        {

        }
        public string Answer { get; set; }
    }
}
