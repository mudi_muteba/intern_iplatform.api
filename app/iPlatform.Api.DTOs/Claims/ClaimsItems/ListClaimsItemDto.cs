﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Claims.ClaimsItems
{
    public class ListClaimsItemDto : Resource
    {
        public ListClaimsItemDto()
        {

        }

        public int ClaimsHeaderId { get; set; }

        public ClaimsType ClaimsType { get; set; }
        public string Description { get; set; }
        public string PolicyDescription { get; set; }
        public int TotalQuestions { get; set; }
        public int TotalQuestionsAnswered { get; set; }

        public virtual List<ClaimsItemQuestionAnswerDto> Answers { get; protected set; }

    }
}
