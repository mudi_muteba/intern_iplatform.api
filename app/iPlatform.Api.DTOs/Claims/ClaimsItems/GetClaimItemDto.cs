﻿namespace iPlatform.Api.DTOs.Claims.ClaimsItems
{
    public class GetClaimItemDto
    {
        public int PartyId { get; set; }
        public int ClaimHeaderId { get; set; }
        public int ClaimItemId { get; set; }
    }
}
