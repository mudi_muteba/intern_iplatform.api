﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace iPlatform.Api.DTOs.Extensions
{
    public static class SerializerExtensions
    {
        private static readonly ILog _Log = LogManager.GetLogger(typeof(SerializerExtensions));

        private static readonly Dictionary<string, XmlSerializer> SerializerDictionary = new Dictionary<string, XmlSerializer>();
        public static string Serialize<T>(T value, params Type[] extraTypes)
        {
            try
            {
                var sw = new StringWriter();
                using (var xw = System.Xml.XmlWriter.Create(sw))
                {
                    XmlSerializer serializer;
                    if (!SerializerDictionary.TryGetValue(typeof(T).FullName, out serializer))
                    {
                        serializer = new XmlSerializer(typeof(T), extraTypes); ; // HACK 
                        SerializerDictionary.Add(typeof(T).FullName, serializer);
                    }

                    serializer.Serialize(xw, value);
                }
                return sw.ToString();
            }
            catch (Exception ex)
            {
                _Log.Error(String.Format("An error occurred whilst trying to serialize object. Message: {0}", ex.Message), ex);
                return "";
            }
        }
    }
}
