﻿namespace iPlatform.Api.DTOs.Extensions
{
    public static class BooleanExtensions
    {
        public static string ToYesNoStringShort(this bool value)
        {
            return value ? "Y" : "N";
        }
        public static string ToYesNoStringLong(this bool value)
        {
            return value ? "Yes" : "No";
        }
    }
}
