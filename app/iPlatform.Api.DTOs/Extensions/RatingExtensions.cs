﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;
using Newtonsoft.Json;
using System.Xml.Serialization;

namespace iPlatform.Api.DTOs.Extensions
{
    public static class RatingExtensions
    {
        public static List<Cover> GetCovers(this RatingRequestDto request)
        {
            return request.Items.Select(r => r.Cover).Distinct().ToList();
        }

        public static void Validate(this PublishQuoteUploadMessageDto quote)
        {
            if (quote == null)
                throw new ArgumentNullException("quote", "The 'Quote' cannot be null");

            quote.Request.Validate();
            quote.Policy.Validate();
            quote.InsuredInfo.Validate();
        }

        public static void Validate(this RatingRequestDto request)
        {
            if (request == null)
                throw new ArgumentNullException("request", "The 'Request' cannot be null");

            if (request.Items == null || request.Items.Count == 0)
                throw new Exception("The request contains no items");

            foreach (var item in request.Items)
            {
                if (item.Answers.Count == 0)
                    throw new Exception(string.Format("Item {0} contains no answers", item.AssetNo));

                item.Answers.EnsureDeserialization();
            }
        }

        public static void Validate(this InsuredInfoDto insured)
        {
            if (insured == null)
                throw new ArgumentNullException("insured info", "The 'Insured Info' cannot be null");
        }

        public static void Validate(this ImportLeadDto lead)
        {
            if (lead == null)
                throw new ArgumentNullException("lead", "The 'Lead' cannot be null");

            if (lead.Request == null || lead.Request.Items == null || lead.Request.Items.Count <= 0)
                return;

            foreach (var item in lead.Request.Items)
            {
                item.Answers.EnsureDeserialization();
            }
        }

        public static void Validate(this RatingResultPolicyDto policy)
        {
            if (policy == null)
                throw new ArgumentNullException("policy", "The 'Policy' cannot be null");

            if (policy.Items.Count == 0)
                throw new Exception("The policy contains no items");
        }

        public static void EnsureDeserialization(this List<RatingRequestItemAnswerDto> answers)
        {
            answers.ForEach(EnsureDeserialization);
        }

        public static void EnsureDeserialization(this RatingRequestItemAnswerDto answer)
        {
            //Hander Year Of Manufacture as text //TODO redo this part to differentiate between xml and json
            if (answer.Question.Id == Questions.YearOfManufacture.Id && !answer.QuestionAnswer.ToString().Contains("XmlNode"))
                return;

            if (answer.Question.QuestionType.Id.Equals(QuestionTypes.Dropdown.Id) && answer.QuestionAnswer != null)
                answer.QuestionAnswer = answer.QuestionAnswer.CastAsQuestionAnswer();
            else
                answer.QuestionAnswer = answer.QuestionAnswer.CastAsString();
        }

        public static QuestionAnswer CastAsQuestionAnswer(this object answer)
        {
            var questionAnswer = answer as QuestionAnswer;

            if (answer != null && answer.ToString().Contains("XmlNode"))
                return DeserializeXMLAsQuestionAnswer((XmlNode[])answer);

            if (questionAnswer == null)
                return JsonConvert.DeserializeObject<QuestionAnswer>(answer.ToString());
            

            return questionAnswer;
        }

        public static string CastAsString(this object answer)
        {
            if (answer.ToString().Contains("XmlNode"))
                return DeserializeXMLAsString((XmlNode[])answer);

            return answer.ToString();
        }

        public static string GetAnswer(this object answer)
        {
            if (answer == null)
                return string.Empty;

            if (!(answer is QuestionAnswer))
            {
                return answer.ToString();
            }

            var questionAnswer = answer.CastAsQuestionAnswer();

            if (questionAnswer == null)
                return answer.ToString();

            return questionAnswer.Answer;

        }

        public static decimal GetSumInsured(this IEnumerable<RatingRequestItemAnswerDto> answers)
        {
            var sumInsured = 0m;

            if (answers == null)
                return sumInsured;

            var answer = answers.FirstOrDefault(x => x.Question.Id == Questions.SumInsured.Id);

            if (answer == null)
                return sumInsured;

            return decimal.TryParse(answer.QuestionAnswer as string, out sumInsured) ? sumInsured : sumInsured;
        }


        private static QuestionAnswer DeserializeXMLAsQuestionAnswer(XmlNode[] data)
        {
            if (data == null)
                return null;

            foreach(var element in data)
            {
                var newElem = (XmlElement) element;
                if (newElem.Name == "Id")
                    return new QuestionAnswers().First(x => x.Id == Convert.ToInt32(newElem.InnerXml));
            }
            throw new NullReferenceException();
        }

        private static string DeserializeXMLAsString(XmlNode[] data)
        {
            if (data == null)
                return string.Empty;

            return data.First().Value;
        }
    }
}
