﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using iPlatform.Api.DTOs.Upload;
using MasterData;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.PolicyBindings;

namespace iPlatform.Api.DTOs.Extensions
{
    public static class XmlSerializationExtensions
    {

        private static readonly Dictionary<string, XmlSerializer> SerializerDictionary = new Dictionary<string, XmlSerializer>();

        public static string SerializeAsXml(this RatingRequestDto request)
        {
            return Serialize(request);
        }

        public static string SerializeAsXml(this RatingResultDto result)
        {
            return Serialize(result);
        }

        public static string SerializeAsXml(this PublishQuoteUploadMessageDto quote)
        {
            return Serialize(quote);
        }

        public static string SerializeAsXml(this ImportLeadDto lead)
        {
            return Serialize(lead);
        }

        public static string SerializeAsXml(this PolicyUploadDto policyupload)
        {
            return Serialize(policyupload);
        }


        public static string SerializeAsXml(this PolicyBindingRequestDto policyBindingRequestDto)
        {
            return Serialize(policyBindingRequestDto);
        }
        

        private static string Serialize(this object obj)
        {
            if (obj == null)
                return null;

            using (var writer = new StringWriter())
            {
                var xns = new XmlSerializerNamespaces();
                xns.Add(string.Empty, string.Empty);
                XmlSerializer serializer;
                if (!SerializerDictionary.TryGetValue(obj.GetType().FullName, out serializer))
                {
                    serializer = new XmlSerializer(obj.GetType(), new[] { typeof(QuestionAnswer) }); // HACK 
                    SerializerDictionary.Add(obj.GetType().FullName, serializer);
                }
                serializer.Serialize(writer, obj, xns);

                return writer.ToString().Replace(@" encoding=""utf-16""", string.Empty);
            }
        }
    }
}
