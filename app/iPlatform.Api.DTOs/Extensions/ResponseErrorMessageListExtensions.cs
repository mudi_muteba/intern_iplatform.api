﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace iPlatform.Api.DTOs.Extensions
{
    public static class ResponseErrorMessageListExtensions
    {
        public static void AddIfValid(this List<ResponseErrorMessage> list, ResponseErrorMessage value)
        {
            if(!list.Any(e => e.MessageKey.Equals(value.MessageKey)))
            {
                list.Add(value);
            }
        }

        public static void AddRangeIfValid(this List<ResponseErrorMessage> list, IEnumerable<ResponseErrorMessage> values)
        {
            foreach (var value in values)
            {
                if (!list.Any(e => e.MessageKey.Equals(value.MessageKey)))
                {
                    list.Add(value);
                }
            }
        }

    }
}
