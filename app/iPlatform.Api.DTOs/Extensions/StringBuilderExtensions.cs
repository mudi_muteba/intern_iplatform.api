﻿using System.Text;

namespace iPlatform.Api.DTOs.Extensions
{
    public static class StringBuilderExtensions
    {
        public static StringBuilder AppendHeader(this StringBuilder stringBuilder, string header)
        {
            return stringBuilder.AppendLine(header);
        }

        public static StringBuilder AppendLineAnswer(this StringBuilder stringBuilder, string question, string answer)
        {
            return stringBuilder.AppendLine(question + " " + answer + ";");
        }
    }
}
