﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.FuneralDualCover
{
    public class FuneralDualCoverDto: Resource
    {
        public FuneralDualCoverDto()
        {
        }

        public decimal Value { get; set; }
    }
}
