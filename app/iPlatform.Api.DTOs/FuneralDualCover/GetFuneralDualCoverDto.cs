﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.FuneralDualCover
{
    public class GetFuneralDualCoverDto: AttributeValidationDto, IExecutionDto
    {
        public GetFuneralDualCoverDto()
        {
            Context = DtoContext.NoContext();
        }
        public string IdNumber { get; set; }

        public decimal SumInsured { get; set; }

        public DtoContext Context { get; protected internal set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrWhiteSpace(IdNumber))
                validation.Add(FuneralDualCoverValidationMessages.IdNumberRequired);

            if (SumInsured <= 0)
                validation.Add(FuneralDualCoverValidationMessages.SumInsuredRequired);


            return validation;
        }
    }
}
