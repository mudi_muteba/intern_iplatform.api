﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.ChannelSettings
{
    public class CreateChannelSettingDto : AttributeValidationDto, IExecutionDto
    {
        public CreateChannelSettingDto()
        {

        }
        public int ChannelSettingTypeId { get; set; }
        public string ChannelSettingValue { get; set; }
        public int ChannelId { get; set; }
        public string ChannelSettingName { get; set; }
        public string ChannelSettingDescription { get; set; }
        public int ChannelSettingDataTypeId { get; set; }
        public int? ChannelEnvironmentId { get; set; }
        public int ChannelSettingOwnerId { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
