﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.ChannelSettings
{
    public class DeleteChannelSettingDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DeleteChannelSettingDto()
        {
        }
        public int Id { get; set; }


        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
