﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;

namespace iPlatform.Api.DTOs.Admin.ChannelSettings
{
    public class ListChannelSettingDto : Resource, ICultureAware
    {
        public ListChannelSettingDto()
        {
        }
        
        public ChannelInfoDto Channel { get; set; }
        public ChannelSettingType ChannelSettingType { get; set; }
        public string ChannelSettingValue { get; set; }
        public string ChannelSettingName { get; set; }
        public string ChannelSettingDescription { get; set; }
        public DataType ChannelSettingDataType { get; set; }
        public ChannelSettingOwner ChannelSettingOwner { get; set; }
    }
}
