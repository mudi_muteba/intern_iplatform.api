﻿using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Admin.ChannelSettings
{
    public class ChannelSettingDto : Resource
    {
        public ChannelSettingDto()
        {

        }

        public virtual ChannelInfoDto Channel { get; set; }
        public virtual ChannelSettingType ChannelSettingType { get; set; }
        public virtual string ChannelSettingValue { get; set; }
        public virtual string ChannelSettingName { get; set; }
        public virtual string ChannelSettingDescription { get; set; }
        public virtual DataType ChannelSettingDataType { get; set; }
        public virtual ChannelSettingOwner ChannelSettingOwner { get; set; }
    }
}
