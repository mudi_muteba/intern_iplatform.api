﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.ChannelSettings
{
    public class UpdateChannelSettingDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public UpdateChannelSettingDto()
        {

        }
        public int ChannelSettingTypeId { get; set; }
        public string ChannelSettingValue { get; set; }
        public int ChannelId { get; set; }
        public string ChannelSettingName { get; set; }
        public string ChannelSettingDescription { get; set; }
        public int ChannelSettingDataTypeId { get; set; }
        public int? ChannelEnvironmentId { get; set; }
        public int ChannelSettingOwnerId { get; set; }
        public DtoContext Context { get; private set; }

        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
