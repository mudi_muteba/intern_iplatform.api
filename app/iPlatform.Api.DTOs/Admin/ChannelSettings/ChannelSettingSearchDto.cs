﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.ChannelSettings
{
    public class ChannelSettingSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public int ChannelId { get; set; }
        public int ChannelEnvironmentId { get; set; }
        public string ChannelSettingName { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
