﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.ChannelSettings
{
    public class SearchChannelSettingDto : BaseCriteria, IValidationAvailableDto
    {
        public int ChannelSettingTypeId { get; set; }
        public string ChannelSettingValue { get; set; }
        public int ChannelId { get; set; }
        public string ChannelSettingName { get; set; }
        public string ChannelSettingDescription { get; set; }
        public int DataTypeId { get; set; }
        public int ChannelEnvironmentId { get; set; }
        public int ChannelSettingOwnerId { get; set; }

        public SearchChannelSettingDto()
        {
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();


            return validation;
        }
    }
}