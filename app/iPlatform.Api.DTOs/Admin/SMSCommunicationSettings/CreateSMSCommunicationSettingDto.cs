﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SMSCommunicationSettings
{
    public class CreateSMSCommunicationSettingDto : AttributeValidationDto, IExecutionDto
    {
        public CreateSMSCommunicationSettingDto()
        {

        }

        public int ChannelId { get; set; }

        public string SMSProviderName { get; set; }
        public string SMSUserName { get; set; }
        public string SMSPassword { get; set; }
        public string SMSUrl { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}