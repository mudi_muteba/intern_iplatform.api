﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SMSCommunicationSettings
{
    public class SearchSMSCommunicationSettingDto : BaseCriteria, IValidationAvailableDto
    {
        public SearchSMSCommunicationSettingDto()
        {

        }
        public string SMSProviderName { get; set; }
        public string SMSUserName { get; set; }
        public string SMSUrl { get; set; }

        public ChannelInfoDto Channel { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();


            return validation;
        }
    }
}