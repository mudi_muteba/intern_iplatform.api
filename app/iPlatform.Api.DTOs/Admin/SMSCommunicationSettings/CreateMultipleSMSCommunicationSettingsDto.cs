﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;

using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SMSCommunicationSettings
{
    public class CreateMultipleSMSCommunicationSettingsDto : AttributeValidationDto, IExecutionDto
    {
        public CreateMultipleSMSCommunicationSettingsDto()
        {
            SMSCommunicationSettings = new List<CreateSMSCommunicationSettingDto>();
        }

        public List<CreateSMSCommunicationSettingDto> SMSCommunicationSettings { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
