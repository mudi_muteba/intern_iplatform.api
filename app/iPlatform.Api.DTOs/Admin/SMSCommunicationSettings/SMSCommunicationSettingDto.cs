﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Admin.SMSCommunicationSettings
{
    public class SMSCommunicationSettingDto : Resource
    {
        public SMSCommunicationSettingDto()
        {
        }

        public ChannelInfoDto Channel { get; set; }
        public string SMSProviderName { get; set; }
        public string SMSUserName { get; set; }
        public string SMSPassword { get; set; }
        public string SMSUrl { get; set; }
    }
}