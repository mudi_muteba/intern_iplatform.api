﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Campaigns;
using System;
using ValidationMessages.Channels;

namespace iPlatform.Api.DTOs.Admin.ChannelPermissions
{
    public class ChannelPermissionSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public ChannelPermissionSearchDto()
        { }

        public string ChannelId { get; set; }
        public string ProductCode { get; set; }
        public string InsurerCode { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            Guid _channelId;

            if(!string.IsNullOrEmpty(ChannelId) && !Guid.TryParse(ChannelId,out _channelId))
                validation.Add(ChannelValidationMessages.ChannelIdInvalid);

            return validation;
        }
    }
}