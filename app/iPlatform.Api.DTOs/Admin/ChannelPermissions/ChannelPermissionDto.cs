﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Admin.ChannelPermissions
{
    public class ChannelPermissionDto: Resource
    {
        public ChannelPermissionDto()
        {
            AllowProposalCreation = new PermissionDto();
            AllowQuoting = new PermissionDto();
            AllowSecondLevelUnderwriting = new PermissionDto();
        }

        public Guid ChannelId { get; set; }
        public string InsurerCode { get; set; }
        public string ProductCode { get; set; }

        public PermissionDto AllowProposalCreation { get; set; }
        public PermissionDto AllowQuoting { get; set; }
        public PermissionDto AllowSecondLevelUnderwriting { get; set; }
    }
}
