﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Admin.ChannelPermissions
{
    public class PermissionDto
    {

        public PermissionDto()
        {
            Allowed = false;
        }

        public PermissionDto(bool allowed)
        {
            Allowed = allowed;
        }
        public bool Allowed { get; set; }
    }
}
