﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Channels;

namespace iPlatform.Api.DTOs.Admin.ChannelPermissions
{
    public class DisableChannelPermissionDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
         public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        public DisableChannelPermissionDto()
        {
            Context = DtoContext.NoContext();
        }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id <= 0)
            {
                list.Add(ChannelValidationMessages.IdRequired);
            }

            return list;
        }
    }
}
