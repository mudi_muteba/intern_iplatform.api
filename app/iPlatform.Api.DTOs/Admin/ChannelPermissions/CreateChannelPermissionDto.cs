﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Channels;

namespace iPlatform.Api.DTOs.Admin.ChannelPermissions
{
    public class CreateChannelPermissionDto : AttributeValidationDto, IExecutionDto
    {
        public CreateChannelPermissionDto()
        {
            AllowProposalCreation = false;
            AllowQuoting = false;
            AllowSecondLevelUnderwriting = false;
            Context = DtoContext.NoContext();
        }

        public DtoContext Context { get; private set; }
        public Guid ChannelId { get; set; }
        public string InsurerCode { get; set; }
        public string ProductCode { get; set; }
        public int ProductId { get; set; }
        public bool AllowProposalCreation { get; set; }
        public bool AllowQuoting { get; set; }
        public bool AllowSecondLevelUnderwriting { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Guid.Empty == ChannelId)
                validation.Add(ChannelValidationMessages.ChannelIdRequired);

            if (string.IsNullOrWhiteSpace(InsurerCode))
                validation.Add(ChannelValidationMessages.InsurerCodeRequired);

            if (string.IsNullOrWhiteSpace(ProductCode))
                validation.Add(ChannelValidationMessages.ProductCodeRequired);

            return validation;
        }
    }
}
