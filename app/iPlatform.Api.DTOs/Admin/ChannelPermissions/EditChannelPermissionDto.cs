﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;
using ValidationMessages.Channels;

namespace iPlatform.Api.DTOs.Admin.ChannelPermissions
{
    public class EditChannelPermissionDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public EditChannelPermissionDto()
        {
            AllowProposalCreation = false;
            AllowQuoting = false;
            AllowSecondLevelUnderwriting = false;
            Context = DtoContext.NoContext();
        }
        public int Id { get; set; }
        public DtoContext Context { get; private set; }
        public Guid ChannelId { get; set; }
        public string InsurerCode { get; set; }
        public string ProductCode { get; set; }

        public int ProductId { get; set; }

        public bool AllowProposalCreation { get; set; }
        public bool AllowQuoting { get; set; }
        public bool AllowSecondLevelUnderwriting { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id <= 0)
                validation.Add(ChannelValidationMessages.IdRequired);

            if (Guid.Empty == ChannelId)
                validation.Add(ChannelValidationMessages.ChannelIdRequired);

            if (string.IsNullOrWhiteSpace(InsurerCode))
                validation.Add(ChannelValidationMessages.InsurerCodeRequired);

            if (string.IsNullOrWhiteSpace(ProductCode))
                validation.Add(ChannelValidationMessages.ProductCodeRequired);

            return validation;
        }
    }
}
