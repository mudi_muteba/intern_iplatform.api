﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsiRateUser
{ 
    public class SearchSettingsiRateUserDto : BaseCriteria, IValidationAvailableDto
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public string RatingPassword { get; set; }
        public string RatingUsername { get; set; }
        public string User { get; set; }
        public string Product { get; set; }

        public SearchSettingsiRateUserDto()
        {

        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}