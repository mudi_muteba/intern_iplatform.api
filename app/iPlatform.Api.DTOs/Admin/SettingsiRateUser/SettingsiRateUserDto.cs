﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Admin.SettingsiRateUser
{
    public class SettingsiRateUserDto :  Resource, ICultureAware
    {
        public SettingsiRateUserDto()
        {

        }
        public UserInfoDto User { get; set; }
        public ProductInfoDto Product { get; set; }
        public string RatingPassword { get; set; }
        public string RatingUsername { get; set; }
    }
}
