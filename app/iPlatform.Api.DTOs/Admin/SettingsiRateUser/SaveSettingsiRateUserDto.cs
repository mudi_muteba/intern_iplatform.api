﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Admin.SettingsiRateUser
{
    public class SaveSettingsiRateUserDto 
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public string RatingPassword { get; set; }
        public string RatingUsername { get; set; }
    }
}
