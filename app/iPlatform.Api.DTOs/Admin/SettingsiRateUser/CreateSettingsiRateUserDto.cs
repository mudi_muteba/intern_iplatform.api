﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsiRateUser
{
    public class CreateSettingsiRateUserDto : AttributeValidationDto, IExecutionDto
    {
        public CreateSettingsiRateUserDto()
        {

        }
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public string RatingPassword { get; set; }
        public string RatingUsername { get; set; }



        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (UserId == 0)
            { validation.Add(SettingsiRateUserValidationMessages.UserIdRequired); }
            if (ProductId == 0)
            { validation.Add(SettingsiRateUserValidationMessages.ProductIdRequired); }


            return validation;
        }
    }
}
