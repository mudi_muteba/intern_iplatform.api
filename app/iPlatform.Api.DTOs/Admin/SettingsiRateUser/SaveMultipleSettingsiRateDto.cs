﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsiRateUser
{
    public class SaveMultipleSettingsiRateUserDto : AttributeValidationDto, IExecutionDto
    {
        public SaveMultipleSettingsiRateUserDto()
        {
            SettingsiRateUser = new List<SaveSettingsiRateUserDto>();
        }
   
        public virtual List<SaveSettingsiRateUserDto> SettingsiRateUser { get; set; }

        public virtual int ProductId { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
