﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsiRateUser
{
    public class DisableSettingsiRateUserDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
        public DisableSettingsiRateUserDto()
        {

        }
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public string RatingPassword { get; set; }
        public string RatingUsername { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (UserId == 0)
            { validation.Add(SettingsiRateUserValidationMessages.UserIdRequired); }
            if (ProductId == 0)
            { validation.Add(SettingsiRateUserValidationMessages.ProductIdRequired); }
            if (Id == 0)
            { validation.Add(SettingsiRateUserValidationMessages.IdRequiredForDelete); }

            return validation;
        }
    }
}
