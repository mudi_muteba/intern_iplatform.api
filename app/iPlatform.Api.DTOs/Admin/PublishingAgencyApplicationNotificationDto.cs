﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;

namespace iPlatform.Api.DTOs.Admin
{
    public class PublishingAgencyApplicationNotificationDto : IExecutionDto
    {
        public PublishingAgencyApplicationNotificationDto()
        {
            Context = DtoContext.NoContext();
        }

        public string Email { get; set; }
        public string OrganizationCode { get; set; }
        public int ChannelId { get; set; }
        public string Body { get; set; }
        public string FromEmail { get; set; }
        public string Subject { get; set; }
        public BrokerageData BrokerageData { get; set; }

        public DtoContext Context { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }

    public class BrokerageData
    {
        public BrokerageData()
        {
            UserBrokerages = new List<UserBrokerage>();
        }

        public string Name { get; set; }
        public AgencyApplicationStatus AgencyApplicationStatus { get; set; }
        public List<UserBrokerage> UserBrokerages { get; set; }
    }

    public class UserBrokerage
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}