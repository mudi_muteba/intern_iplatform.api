﻿
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Admin.EmailCommunicationSettings
{
    public class EmailCommunicationSettingDto: Resource
    {
        public EmailCommunicationSettingDto()
        {

        }
        public ChannelInfoDto Channel { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool UseSSL { get; set; }
        public bool UseDefaultCredentials { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string DefaultFrom { get; set; }
        public string DefaultBCC { get; set; }
        public string DefaultContactNumber { get; set; }
        public string SubAccountID { get; set; }
    }
}