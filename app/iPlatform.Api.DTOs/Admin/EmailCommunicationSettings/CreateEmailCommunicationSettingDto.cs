﻿
using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.EmailCommunicationSettings
{
    public class CreateEmailCommunicationSettingDto : AttributeValidationDto, IExecutionDto
    {
        public CreateEmailCommunicationSettingDto()
        {

        }
        public int ChannelId { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool UseSSL { get; set; }
        public bool UseDefaultCredentials { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string DefaultFrom { get; set; }
        public string DefaultBCC { get; set; }
        public string DefaultContactNumber { get; set; }
        public string SubAccountID { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId == 0)
                validation.Add(EmailCommunicationValidationMessages.ChannelIdRequired);

            return validation;
        }
    }
}