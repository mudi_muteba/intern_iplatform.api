﻿
using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.EmailCommunicationSettings
{
    public class SearchEmailCommunicationsDto : BaseCriteria, IValidationAvailableDto
    {
        public SearchEmailCommunicationsDto()
        {

        }
        public string Channel { get; set; }

        public string host { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();


            return validation;
        }
    }
}