﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;

using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.EmailCommunicationSettings
{
    public class CreateMultipleEmailCommunicationSettingsDto : AttributeValidationDto, IExecutionDto
    {
        public CreateMultipleEmailCommunicationSettingsDto()
        {
            EmailCommunicationSettings = new List<CreateEmailCommunicationSettingDto>();
        }

        public List<CreateEmailCommunicationSettingDto> EmailCommunicationSettings { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
