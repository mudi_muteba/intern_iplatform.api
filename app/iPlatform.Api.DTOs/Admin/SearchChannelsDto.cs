﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using System;

namespace iPlatform.Api.DTOs.Admin
{
    public class SearchChannelsDto : BaseCriteria, IValidationAvailableDto
    {
        public string Name { get; set; }
        public string Code { get; set; }
        //public Guid SystemId { get; set; }

        public SearchChannelsDto()
        {

        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();
            

            return validation;
        }
    }
}