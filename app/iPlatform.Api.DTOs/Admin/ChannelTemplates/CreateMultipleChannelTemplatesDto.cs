﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;

using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.ChannelTemplates
{
    public class CreateMultipleChannelTemplatesDto : AttributeValidationDto, IExecutionDto
    {
        public CreateMultipleChannelTemplatesDto()
        {
            ChannelTemplates = new List<CreateChannelTemplateDto>();
        }

        public List<CreateChannelTemplateDto> ChannelTemplates { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
