﻿using System;
using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Admin.ChannelTemplateAttachments;
using iPlatform.Api.DTOs.Admin.ChannelTemplateVariables;

using MasterData;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Admin.ChannelTemplates
{
    public class ChannelTemplateDto : Resource, ICultureAware
    {
        public ChannelTemplateDto()
        {
            Attachments = new List<ChannelTemplateAttachmentDto>();
            Variables = new List<ChannelTemplateVariableDto>();
        }

        public int ChannelMasterId { get; set; }
        public int ChannelId { get; set; }
        public Guid ChannelSystemId { get; set; }
        public TemplateType TemplateType { get; set; }
        public TemplateContextType TemplateContextType { get; set; }

        public ChannelInfoDto Channel { get; set; }

        public List<ChannelTemplateAttachmentDto> Attachments { get; set; }
        public List<ChannelTemplateVariableDto> Variables { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Extension { get; set; }
        public string ContentType { get; set; }

        public bool IsDefault { get; set; }
        public bool IsTemplate { get; set; }
    }
}
