﻿using System;
using System.Collections.Generic;

using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Admin.ChannelTemplateAttachments;
using iPlatform.Api.DTOs.Admin.ChannelTemplateVariables;

using MasterData;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.ChannelTemplates
{
    public class CreateChannelTemplateDto : AttributeValidationDto, IExecutionDto
    {
        public CreateChannelTemplateDto()
        {
            Attachments = new List<ChannelTemplateAttachmentDto>();
            Variables = new List<ChannelTemplateVariableDto>();
        }

        public int ChannelMasterId { get; set; }
        public int ChannelId { get; set; }
        public Guid ChannelSystemId { get; set; }
        public Guid SystemId { get; set; }
        public TemplateType TemplateType { get; set; }
        public TemplateContextType TemplateContextType { get; set; }

        public List<ChannelTemplateAttachmentDto> Attachments { get; set; }
        public List<ChannelTemplateVariableDto> Variables { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Extension { get; set; }
        public string ContentType { get; set; }

        public bool IsDefault { get; set; }
        public bool IsTemplate { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
