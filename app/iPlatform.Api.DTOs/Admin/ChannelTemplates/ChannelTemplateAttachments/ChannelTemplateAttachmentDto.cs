﻿using System;

using iPlatform.Api.DTOs.Admin.ChannelTemplates;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Admin.ChannelTemplateAttachments
{
    public class ChannelTemplateAttachmentDto : Resource
    {
        public ChannelTemplateAttachmentDto() { }

        public int ChannelTemplateId { get; set; }
        public Guid ChannelTemplateSystemId { get; set; }
        public ChannelTemplateDto ChannelTemplate { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Extension { get; set; }
        public string Attachment { get; set; }
        public string ContentType { get; set; }

        public bool IsTemplate { get; set; }
    }
}
