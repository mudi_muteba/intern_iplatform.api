﻿using System;

using iPlatform.Api.DTOs.Admin.ChannelTemplates;
using iPlatform.Api.DTOs.Base;

using MasterData;

namespace iPlatform.Api.DTOs.Admin.ChannelTemplateVariables
{
    public class ChannelTemplateVariableDto : Resource
    {
        public ChannelTemplateVariableDto() { }

        public int ChannelTemplateId { get; set; }
        public Guid ChannelTemplateSystemId { get; set; }
        public ChannelTemplateDto ChannelTemplate { get; set; }

        public TemplateVariableType TemplateVariableType { get; set; }

        public bool IsTemplate { get; set; }
    }
}
