﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using Shared.Extentions;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsiRates
{
    public class EditSettingsiRateDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public EditSettingsiRateDto()
        {

        }
        public virtual int ProductId { get; set; }
        public virtual string Password { get; set; }
        public virtual string AgentCode { get; set; }
        public virtual string BrokerCode { get; set; }
        public virtual string AuthCode { get; set; }
        public virtual string SchemeCode { get; set; }
        public virtual string Token { get; set; }
        public virtual int ChannelId { get; set; }
        public virtual string Environment { get; set; }
        public virtual string UserId { get; set; }
        public virtual string SubscriberCode { get; set; }
        public virtual string CompanyCode { get; set; }
        public virtual string UwCompanyCode { get; set; }
        public virtual string UwProductCode { get; set; }
        public virtual string ProductCode { get; set; }

        public DtoContext Context { get; private set; }

        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id == 0)
                validation.Add(SettingsiRateValidationMessages.IdRequired);
            if (ChannelId == 0)
                validation.Add(SettingsiRateValidationMessages.ChannelIdRequired);
            if (ProductId == 0)
                validation.Add(SettingsiRateValidationMessages.ProductIdRequired);
            if (Environment == null)
                validation.Add(SettingsiRateValidationMessages.EnvironmentRequired);

            return validation;
        }
    }
}
