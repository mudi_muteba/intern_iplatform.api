﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.Admin.SettingsiRates
{
    public class SettingsiRateDto : Resource, ICultureAware
    {
        public SettingsiRateDto()
        {

        }
        public virtual ProductInfoDto Product { get; set; }
        public virtual string Password { get; set; }
        public virtual string AgentCode { get; set; }
        public virtual string BrokerCode { get; set; }
        public virtual string AuthCode { get; set; }
        public virtual string SchemeCode { get; set; }
        public virtual string Token { get; set; }
        public virtual ChannelInfoDto Channel { get; set; }
        public virtual string Environment { get; set; }
        public virtual string UserId { get; set; }
        public virtual string SubscriberCode { get; set; }
        public virtual string CompanyCode { get; set; }
        public virtual string UwCompanyCode { get; set; }
        public virtual string UwProductCode { get; set; }
        public virtual string ProductCode { get; set; }
    }
}
