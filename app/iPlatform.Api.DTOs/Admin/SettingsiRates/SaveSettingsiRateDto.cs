﻿using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsiRates
{
    public class SaveSettingsiRateDto
    {
        public SaveSettingsiRateDto()
        {
        }

        public int ProductId { get; set; }
        public string Password { get; set; }
        public string AgentCode { get; set; }
        public string BrokerCode { get; set; }
        public string AuthCode { get; set; }
        public string SchemeCode { get; set; }
        public string Token { get; set; }
        public int ChannelId { get; set; }
        public string Environment { get; set; }
        public string UserId { get; set; }
        public string SubscriberCode { get; set; }
        public string CompanyCode { get; set; }
        public string UwCompanyCode { get; set; }
        public string UwProductCode { get; set; }
        public string ProductCode { get; set; }

        protected List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId == 0)
                validation.Add(SettingsiRateValidationMessages.ChannelIdRequired);
            if (ProductId == 0)
                validation.Add(SettingsiRateValidationMessages.ProductIdRequired);
            if (string.IsNullOrEmpty(Environment))
                validation.Add(SettingsiRateValidationMessages.EnvironmentRequired);

            return validation;
        }
    }
}
