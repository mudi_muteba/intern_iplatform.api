﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsiRates
{
    public class EditMultipleSettingsiRateDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DtoContext Context { get; private set; }

        public EditMultipleSettingsiRateDto()
        {
            Context = DtoContext.NoContext();
            EditSettingsiRateDtos = new List<EditSettingsiRateDto>();
        }

        public List<EditSettingsiRateDto> EditSettingsiRateDtos { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }

        public int Id { get; }
    }
}