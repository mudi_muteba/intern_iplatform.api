﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using System;

namespace iPlatform.Api.DTOs.Admin.SettingsiRates
{ 
    public class SearchSettingsiRateDto : BaseCriteria, IValidationAvailableDto
    {
        public string Name { get; set; }
        public string Channel { get; set; }
        public string Product { get; set; }
        public string Environment { get; set; }
        public string SchemeCode { get; set; }
        public string BrokerCode { get; set; }
        public string AgentCode { get; set; }
        public string SubscriberCode { get; set; }
        public int ChannelId { get; set; }
        public int ProductId { get; set; }
        public Guid ChannelSystemId { get; set; }

        public SearchSettingsiRateDto()
        {

        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();
            

            return validation;
        }
    }
}