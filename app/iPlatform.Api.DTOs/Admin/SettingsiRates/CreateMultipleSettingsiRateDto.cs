﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsiRates
{
    public class CreateMultipleSettingsiRateDto : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public CreateMultipleSettingsiRateDto()
        {
            Context = DtoContext.NoContext();
            CreateSettingsiRateDtos = new List<CreateSettingsiRateDto>();
        }

        public List<CreateSettingsiRateDto> CreateSettingsiRateDtos { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
