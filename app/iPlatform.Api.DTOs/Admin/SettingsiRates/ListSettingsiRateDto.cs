﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.Admin.SettingsiRates
{
    public class ListSettingsiRateDto : Resource, ICultureAware
    {
        public ListSettingsiRateDto()
        {
        }

        public ProductInfoDto Product { get; set; }
        public string Password { get; set; }
        public string AgentCode { get; set; }
        public string BrokerCode { get; set; }
        public string AuthCode { get; set; }
        public string SchemeCode { get; set; }
        public string Token { get; set; }
        public ChannelInfoDto Channel { get; set; }
        public string Environment { get; set; }
        public string UserId { get; set; }
        public string SubscriberCode { get; set; }
        public string UwCompanyCode { get; set; }
        public string UwProductCode { get; set; }
        public string ProductCode { get; set; }
    }
}