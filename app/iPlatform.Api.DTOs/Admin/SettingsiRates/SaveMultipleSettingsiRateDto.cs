﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsiRates
{
    public class SaveMultipleSettingsiRateDto : AttributeValidationDto, IExecutionDto
    {
        public SaveMultipleSettingsiRateDto()
        {
            SettingsiRate = new List<SaveSettingsiRateDto>();
        }
   
        public virtual List<SaveSettingsiRateDto> SettingsiRate { get; set; }

        public virtual int ChannelId { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
