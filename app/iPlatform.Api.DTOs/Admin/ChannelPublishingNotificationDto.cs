﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Organisations;
using MasterData;
using System;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Admin
{
    public class ChannelPublishingNotificationDto : IExecutionDto
    {
        public ChannelPublishingNotificationDto()
        {
            Context = DtoContext.NoContext();
            Logs = new List<Publishlog>();
        }

        public string Email { get; set; }
        public string OrganizationCode { get; set; }
        public int ChannelId { get; set; }
        public string Body { get; set; }
        public string FromEmail { get; set; }
        public string Subject { get; set; }
        public List<Publishlog> Logs { get; set; }

        public DtoContext Context { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

    }

    public class Publishlog
    {
        public string Message { get; set; }
        public string Date { get; set; }
        public string Status { get; set; }
        public string Setting { get; set; }
    }
}
