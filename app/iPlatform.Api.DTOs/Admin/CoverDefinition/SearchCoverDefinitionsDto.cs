﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Admin.CoverDefinition
{
    public class SearchCoverDefinitionsDto : BaseCriteria
    {
        public string DisplayName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string CoverName { get; set; }
        public int CoverId { get; set; }

        public SearchCoverDefinitionsDto()
        {
        }
    }
}
