﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using MasterData;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.CoverDefinition
{
    public class CreateCoverDefinitionDto : AttributeValidationDto, IExecutionDto
    {
        public CreateCoverDefinitionDto()
        {
        }

        public string DisplayName { get; set; }
        public Cover Cover { get; set; }
        public ListProductDto Product { get; set; }
        public int VisibleIndex { get; set; }
        public CoverDefinitionType CoverDefinitionType { get; set; }


        public DtoContext Context { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            return validation;
        }
    }
}
