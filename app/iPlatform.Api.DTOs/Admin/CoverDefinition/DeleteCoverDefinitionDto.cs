﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.CoverDefinition
{
    public class DeleteCoverDefinitionDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DeleteCoverDefinitionDto()
        {
            Context = DtoContext.NoContext();
        }
        public DeleteCoverDefinitionDto(int id)
        {
            Id = id;
            Context = DtoContext.NoContext();
        }
        public DtoContext Context { get; set; }

        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
