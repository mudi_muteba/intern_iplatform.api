﻿using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;
using iPlatform.Api.DTOs.Organisations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Admin
{
    public class ChannelInfoDto
    {
        public ChannelInfoDto()
        { }
        public int Id { get; set; }
        public Guid SystemId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public ListOrganizationDto Organization { get; set; }
        public string ExternalReference { get; set; }
        public bool RequestBroker { get; set; }
        public bool RequestAccountExecutive { get; set; }
        public bool HasPolicyBinding { get; set; }
    }
}
