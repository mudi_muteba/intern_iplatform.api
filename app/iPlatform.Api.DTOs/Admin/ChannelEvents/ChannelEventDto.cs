﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Products;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Admin.ChannelEvents
{
    public class ChannelEventDto : Resource
    {
        public ChannelEventDto()
        {
            Tasks = new List<ChannelEventTaskDto>();
        }

        public string EventName { get; set; }
        public string ProductCode { get; set; }
        public ChannelInfoDto Channel { get; set; }
        public List<ChannelEventTaskDto> Tasks { get; set; }
    }
}
