﻿
namespace iPlatform.Api.DTOs.Admin.ChannelEvents
{
    public class ChannelEventInfoDto 
    {
        public ChannelEventInfoDto()
        {
        }

        public int Id { get; set; }
        public string EventName { get; set; }
        public string ProductCode { get; set; }
    }
}
