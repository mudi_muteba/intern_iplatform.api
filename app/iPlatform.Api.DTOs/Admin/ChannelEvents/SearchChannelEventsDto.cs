﻿using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Admin.ChannelEvents
{
    public class SearchChannelEventsDto : BaseCriteria
    {
        public string EventName { get; set; }
        public string ProductCode { get; set; }
        public int ChannelId { get; set; }

        public SearchChannelEventsDto()
        {

        }

    }
}
