﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.ChannelEvents
{
    public class EditChannelEventDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public EditChannelEventDto()
        {
            Tasks = new List<ChannelEventTaskDto>();
            Context = DtoContext.NoContext();
        }

        public int Id { get; set; }
        public string EventName { get; set; }
        public string ProductCode { get; set; }
        public int ChannelId { get; set; }
        public List<ChannelEventTaskDto> Tasks { get; set; }

        public DtoContext Context { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            if (Id == 0)
                validation.Add(ChannelEventValidationMessages.IdRequired);
            if (ChannelId == 0)
                validation.Add(ChannelEventValidationMessages.ChannelIdRequired);
            if (string.IsNullOrEmpty(EventName))
                validation.Add(ChannelEventValidationMessages.EventNameRequired);
            if (Tasks.Count() == 0)
                validation.Add(ChannelEventValidationMessages.TasksRequired);
            if (string.IsNullOrEmpty(ProductCode))
                validation.Add(ChannelEventValidationMessages.ProductCodeRequired);
            if (Tasks.Any(x => string.IsNullOrEmpty(x.TaskName)))
                validation.Add(ChannelEventValidationMessages.TaskNameRequired);

            return validation;
        }
    }
}
