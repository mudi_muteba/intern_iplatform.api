﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using System.Linq;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.ChannelEvents
{
    public class CreateChannelEventDto : AttributeValidationDto, IExecutionDto
    {
        public CreateChannelEventDto()
        {
            Tasks = new List<ChannelEventTaskDto>();
        }

        public string EventName { get; set; }
        public string ProductCode { get; set; }
        public int ChannelId { get; set; }
        public List<ChannelEventTaskDto> Tasks { get; set; }
        public DtoContext Context { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId == 0)
                validation.Add(ChannelEventValidationMessages.ChannelIdRequired);
            if (string.IsNullOrEmpty(EventName))
                validation.Add(ChannelEventValidationMessages.EventNameRequired);
            if (Tasks.Count() == 0)
                validation.Add(ChannelEventValidationMessages.TasksRequired);
            if (string.IsNullOrEmpty(ProductCode))
                validation.Add(ChannelEventValidationMessages.ProductCodeRequired);
            if(Tasks.Any(x => string.IsNullOrEmpty(x.TaskName)))
                validation.Add(ChannelEventValidationMessages.TaskNameRequired);

            return validation;
        }

    }
}
