﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.ChannelEvents
{
    public class DeleteChannelEventDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DeleteChannelEventDto()
        {
            Context = DtoContext.NoContext();
        }
        public DeleteChannelEventDto(int id)
        {
            Id = id;
            Context = DtoContext.NoContext();
        }
        public DtoContext Context { get; set; }

        public int Id { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id == 0)
                validation.Add(ChannelEventValidationMessages.IdRequired);

            return validation;
        }
    }
}
