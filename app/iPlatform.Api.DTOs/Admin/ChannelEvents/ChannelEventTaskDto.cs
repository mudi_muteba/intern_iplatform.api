﻿
namespace iPlatform.Api.DTOs.Admin.ChannelEvents
{
    public class ChannelEventTaskDto 
    {
        public ChannelEventTaskDto()
        {

        }

        public int Id { get; set; }
        public ChannelEventInfoDto ChannelEvent { get; set; }
        public string TaskName { get; set; }
    }
}
