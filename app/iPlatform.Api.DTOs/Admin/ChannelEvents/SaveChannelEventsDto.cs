﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using System.Linq;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.ChannelEvents
{
    public class SaveChannelEventsDto : AttributeValidationDto, IExecutionDto
    {
        public SaveChannelEventsDto()
        {

        }

        public List<CreateChannelEventDto> ChannelEvents { get; set; }
        public int ChannelId { get; set; }
        public DtoContext Context { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId == 0)
                validation.Add(ChannelEventValidationMessages.ChannelIdRequired);

            ChannelEvents.ForEach(x => {
                var messages = x.Validate();
                validation.AddRange(messages);
            });

            return validation;
        }

    }
}
