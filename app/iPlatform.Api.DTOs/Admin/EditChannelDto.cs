﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using MasterData;
using ValidationMessages;
using ValidationMessages.Channels;

namespace iPlatform.Api.DTOs.Admin
{
    public class EditChannelDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
        public bool PasswordStrengthEnabled { get; set; }
        public DateTimeDto ActivatedOn { get; set; }
        public DateTimeDto DeactivatedOn { get; set; }
        public Currency Currency { get; set; }
        public Country Country { get; set; }
        public string DateFormat { get; set; }
        public Language Language { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int OrganizationId { get; set; }
        public string ExternalReference { get; set; }
        public bool RequestBroker { get; set; }
        public bool RequestAccountExecutive { get; set; }
        public int ParentChannelId { get; set; }
        public bool HasPolicyBinding { get; set; }
        public DtoContext Context { get; private set; }

        public EditChannelDto()
        {
            Context = DtoContext.NoContext();
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Currency.Id <= 0)
                validation.Add(ChannelValidationMessages.CurrencyIdRequired);

            if (string.IsNullOrWhiteSpace(DateFormat))
                validation.Add(ChannelValidationMessages.DateFormatRequired);

            if (string.IsNullOrWhiteSpace(Name))
                validation.Add(ChannelValidationMessages.ChannelNameRequired);

            if (Language.Id <= 0)
                validation.Add(ChannelValidationMessages.LanguageIdRequired);

            return validation;
        }
    }
}