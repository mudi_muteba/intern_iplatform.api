﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Admin.SettingsiPersons
{
    public class SettingsiPersonDto : Resource
    {
        public SettingsiPersonDto()
        {

        }
        public ChannelInfoDto Channel { get; set; }
        public ProductInfoDto Product { get; set; }
        public bool CheckRequired { get; set; }
        public string SubscriberCode { get; set; }
        public string BranchNumber { get; set; }
        public string BatchNumber { get; set; }
        public string SecurityCode { get; set; }
        public string EnquirerContactName { get; set; }
        public string EnquirerContactPhoneNo { get; set; }
        public EnvironmentType EnvironmentType { get; set; }
    }
}
