﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Campaigns;

namespace iPlatform.Api.DTOs.Admin.SettingsiPersons
{
    public class SearchSettingsiPersonDto : BaseCriteria, IValidationAvailableDto
    {
        public string SubscriberCode { get; set; }
        public string Channel { get; set; }
        public string Product { get; set; }
        public string Environment { get; set; }
        public string Name { get; set; }

        public SearchSettingsiPersonDto()
        {

        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();
            

            return validation;
        }
    }
}