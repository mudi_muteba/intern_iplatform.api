﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using System;
using System.Collections.Generic;
using Shared.Extentions;
using ValidationMessages;
using MasterData;

namespace iPlatform.Api.DTOs.Admin.SettingsiPersons
{
    public class SaveMultipleSettingsiPersonDto : AttributeValidationDto, IExecutionDto
    {
        public SaveMultipleSettingsiPersonDto()
        {
            Settings = new List<EditSettingsiPersonDto>();
        }

        public int ChannelId { get; set; }

        public IList<EditSettingsiPersonDto> Settings { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
