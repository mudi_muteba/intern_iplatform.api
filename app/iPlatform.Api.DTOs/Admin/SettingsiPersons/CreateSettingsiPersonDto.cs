﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using System;
using System.Collections.Generic;
using Shared.Extentions;
using ValidationMessages;
using MasterData;

namespace iPlatform.Api.DTOs.Admin.SettingsiPersons
{
    public class CreateSettingsiPersonDto : AttributeValidationDto, IExecutionDto
    {
        public CreateSettingsiPersonDto()
        {

        }
        public int ChannelId { get; set; }
        public int ProductId { get; set; }
        public bool CheckRequired { get; set; }
        public string SubscriberCode { get; set; }
        public string BranchNumber { get; set; }
        public string BatchNumber { get; set; }
        public string SecurityCode { get; set; }
        public string EnquirerContactName { get; set; }
        public string EnquirerContactPhoneNo { get; set; }
        public EnvironmentType EnvironmentType { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId == 0)
                validation.Add(SettingsiPersonValidationMessages.ChannelIdRequired);
            if (ProductId == 0)
                validation.Add(SettingsiPersonValidationMessages.ProductIdRequired);
            if (EnvironmentType == null)
                validation.Add(SettingsiPersonValidationMessages.EnvironmentRequired);

            return validation;
        }
    }
}
