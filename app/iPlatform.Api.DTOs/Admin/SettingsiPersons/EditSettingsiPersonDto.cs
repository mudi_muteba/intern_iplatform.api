﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using Shared.Extentions;
using ValidationMessages;
using MasterData;

namespace iPlatform.Api.DTOs.Admin.SettingsiPersons
{
    public class EditSettingsiPersonDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public EditSettingsiPersonDto()
        {

        }
        public int Id { get; set; }
        public int ChannelId { get; set; }
        public int ProductId { get; set; }
        public bool CheckRequired { get; set; }
        public string SubscriberCode { get; set; }
        public string BranchNumber { get; set; }
        public string BatchNumber { get; set; }
        public string SecurityCode { get; set; }
        public string EnquirerContactName { get; set; }
        public string EnquirerContactPhoneNo { get; set; }
        public EnvironmentType EnvironmentType { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id == 0)
                validation.Add(SettingsiPersonValidationMessages.IdRequired);
            if (ChannelId == 0)
                validation.Add(SettingsiPersonValidationMessages.ChannelIdRequired);
            if (ProductId == 0)
                validation.Add(SettingsiPersonValidationMessages.ProductIdRequired);
            if (EnvironmentType == null)
                validation.Add(SettingsiPersonValidationMessages.EnvironmentRequired);

            return validation;
        }
    }
}
