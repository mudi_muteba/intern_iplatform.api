﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Products;
using MasterData;

namespace iPlatform.Api.DTOs.Admin.SettingsiPersons
{
    public class ListSettingsiPersonDto : Resource, ICultureAware
    {
        public ListSettingsiPersonDto()
        {
        }

        public ChannelInfoDto Channel { get; set; }
        public ProductInfoDto Product { get; set; }
        public bool CheckRequired { get; set; }
        public string SubscriberCode { get; set; }
        public string BranchNumber { get; set; }
        public string BatchNumber { get; set; }
        public string SecurityCode { get; set; }
        public string EnquirerContactName { get; set; }
        public string EnquirerContactPhoneNo { get; set; }
        public EnvironmentType EnvironmentType { get; set; }
    }
}