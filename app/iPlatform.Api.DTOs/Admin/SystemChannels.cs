﻿using System.Collections.Generic;
using System.Linq;

namespace iPlatform.Api.DTOs.Admin
{
    public class SystemChannels
    {
        public IEnumerable<ChannelDto> Channels { get; set; }

        public bool IsAllowed(int channelId, string channelCode)
        {
            return (channelId == Channels.First(a => a.Code == channelCode.ToUpper()).Id);
        }
        public bool IsAllowed(int channelId, string[] channelCodes)
        {
            return (Channels.Where(a => channelCodes.Contains(a.Code)).Any(a=>a.Id == channelId));
        }
    }
}