﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.DisplaySetting
{
    public class EditDisplaySettingDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public int Id { get; set; }

        public int ChannelId { get; set; }

        public int ProductId { get; set; }

        public bool ThirdPartyDisplay { get; set; }

        public string PolicyEndPointUrl { get; set; }

        public string PolicyEndPointUserName { get; set; }

        public string PolicyEndPointPassword { get; set; }

        public string PolicyScheduleEndPointUrl { get; set; }

        public string PolicyScheduleEndPointUserName { get; set; }

        public string PolicyScheduleEndPointPassword { get; set; }

        public string ClaimEndPointUrl { get; set; }

        public string ClaimEndPointUserName { get; set; }

        public string ClaimEndPointPassword { get; set; }

        public string FinanceEndPointUrl { get; set; }

        public string FinanceEndPointUserName { get; set; }

        public string FinanceEndPointPassword { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        #region [ Validation ] 

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if(Id==0)
                validation.Add(DisplaySettingValidationMessages.IdRequired);
            if (ChannelId == 0)
                validation.Add(DisplaySettingValidationMessages.ChannelIdRequired);
            if (ProductId == 0)
                validation.Add(DisplaySettingValidationMessages.ProductIdRequired);

            return validation;
        }

        #endregion
    }
}
