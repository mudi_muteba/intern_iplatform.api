﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.DisplaySetting
{
    public class DeleteDisplaySettingDto : IExecutionDto, IValidationAvailableDto, IAffectExistingEntity
    {
        public int Id { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        #region [ Validation ] 

        public List<ValidationErrorMessage> Validate()
        {
            List<ValidationErrorMessage> errorMessages = new List<ValidationErrorMessage>();
            if (Id == 0)
                errorMessages.Add(DisplaySettingValidationMessages.IdRequired);

            return errorMessages;
        }

        #endregion
    }
}
