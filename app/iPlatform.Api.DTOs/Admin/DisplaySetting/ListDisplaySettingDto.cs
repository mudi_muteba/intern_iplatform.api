﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.Admin.DisplaySetting
{
    public class ListDisplaySettingDto : Resource, ICultureAware
    {
        public ChannelInfoDto Channel { get; set; }

        public ProductInfoDto Product { get; set; }

        public bool ThirdPartyDisplay { get; set; }

        public string PolicyEndPointUrl { get; set; }

        public string PolicyScheduleEndPointUrl { get; set; }

        public string ClaimEndPointUrl { get; set; }

        public string FinanceEndPointUrl { get; set; }
    }
}
