﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.DisplaySetting
{
    public class SearchDisplaySettingDto : BaseCriteria, IValidationAvailableDto
    {
        public string ChannelName { get; set; }

        public string ProductName { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            List<ValidationErrorMessage> validationErrorMessages = new List<ValidationErrorMessage>();
            return validationErrorMessages;
        }
    }
}
