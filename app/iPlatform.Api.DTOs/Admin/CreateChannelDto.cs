﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Party;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Tags;
using ValidationMessages;
using ValidationMessages.Campaigns;
using MasterData;
using ValidationMessages.Channels;

namespace iPlatform.Api.DTOs.Admin
{
    public class CreateChannelDto : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public CreateChannelDto()
        {
            Context = DtoContext.NoContext();
            SystemId = Guid.NewGuid();
        }

        public int Id { get; set; }
        public Guid SystemId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
        public Currency Currency { get; set; }
        public string DateFormat { get; set; }
        public Language Language { get; set; }
        public string Name { get; set; }
        public Country Country { get; set; }
        public int PasswordStrengthEnabled { get; set; }
        public string Code { get; set; }
        public string ExternalReference { get; set; }
        public bool RequestBroker { get; set; }
        public bool RequestAccountExecutive { get; set; }
        public int ParentChannelId { get; set; }
        public bool HasPolicyBinding { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }


        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Currency.Id <= 0)
                validation.Add(ChannelValidationMessages.CurrencyIdRequired);

            if (string.IsNullOrWhiteSpace(DateFormat))
                validation.Add(ChannelValidationMessages.DateFormatRequired);

            if (string.IsNullOrWhiteSpace(Name))
                validation.Add(ChannelValidationMessages.IdRequired);

            if (Language.Id <= 0)
                validation.Add(ChannelValidationMessages.LanguageIdRequired);

            return validation;
        }
    }
}