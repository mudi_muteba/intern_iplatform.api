﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using System.Linq;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsQuoteUploads
{
    public class DeleteSettingsQuoteUploadDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DeleteSettingsQuoteUploadDto()
        {
        }
        public int Id { get; set; }


        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (Id == 0)
                validation.Add(SettingsQuoteUploadValidationMessages.IdRequired);

            return validation;
        }
    }
}
