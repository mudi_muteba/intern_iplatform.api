﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using System.Linq;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsQuoteUploads
{
    public class EditSettingsQuoteUploadsDto : AttributeValidationDto, IExecutionDto
    {
        public EditSettingsQuoteUploadsDto()
        {
            Settings = new List<EditSettingsQuoteUploadDetailDto>();
        }
        public int ProductId { get; set; }
        public string Environment { get; set; }
        public int ChannelId { get; set; }
        public List<EditSettingsQuoteUploadDetailDto> Settings { get; set; }

        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId == 0)
                validation.Add(SettingsQuoteUploadValidationMessages.ChannelIdRequired);
            if (ProductId == 0)
                validation.Add(SettingsQuoteUploadValidationMessages.ProductIdRequired);
            if (string.IsNullOrEmpty(Environment))
                validation.Add(SettingsQuoteUploadValidationMessages.EnvironmentRequired);
            if (!Settings.Any())
                validation.Add(SettingsQuoteUploadValidationMessages.NoSettings);
            else
                Settings.ForEach(x => validation.AddRange(x.Validate()));

            return validation;
        }
    }
}
