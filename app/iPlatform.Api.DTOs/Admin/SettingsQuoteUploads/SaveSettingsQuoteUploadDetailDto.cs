﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using System;
using System.Collections.Generic;
using Shared.Extentions;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsQuoteUploads
{
    public class SaveSettingsQuoteUploadDetailDto
    {
        public SaveSettingsQuoteUploadDetailDto()
        {

        }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
        public int ProductId { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (string.IsNullOrEmpty(SettingName))
                validation.Add(SettingsQuoteUploadValidationMessages.SettingNameRequired);
            if (string.IsNullOrEmpty(SettingValue))
                validation.Add(SettingsQuoteUploadValidationMessages.SettingValueRequired);
            if (ProductId== 0)
                validation.Add(SettingsQuoteUploadValidationMessages.ProductIdRequired);

            return validation;
        }
    }
}
