﻿using iPlatform.Api.DTOs.Base;
using System.Linq;
using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsQuoteUploads
{
    public class CreateSettingsQuoteUploadDto : AttributeValidationDto, IExecutionDto
    {
        public CreateSettingsQuoteUploadDto()
        {
            
        }
        public virtual int ProductId { get; set; }
        public virtual string Environment { get; set; }
        public virtual int ChannelId { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
        public bool IsPolicyBinding { get; set; }
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId == 0)
                validation.Add(SettingsQuoteUploadValidationMessages.ChannelIdRequired);
            if (ProductId == 0)
                validation.Add(SettingsQuoteUploadValidationMessages.ProductIdRequired);
            if (string.IsNullOrEmpty(Environment))
                validation.Add(SettingsQuoteUploadValidationMessages.EnvironmentRequired);
            if (string.IsNullOrEmpty(SettingName))
                validation.Add(SettingsQuoteUploadValidationMessages.SettingNameRequired);
            if (string.IsNullOrEmpty(SettingValue))
                validation.Add(SettingsQuoteUploadValidationMessages.SettingValueRequired);

            return validation;
        }
    }
}
