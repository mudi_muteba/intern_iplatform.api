﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Campaigns;
using System;

namespace iPlatform.Api.DTOs.Admin.SettingsQuoteUploads
{
    public class SearchSettingsQuoteUploadDto : BaseCriteria, IValidationAvailableDto
    {
        public string SettingName { get; set; }
        public string Channel { get; set; }
        public string Product { get; set; }
        public string Environment { get; set; }
        public int ChannelId { get; set; }
        public int ProductId { get; set; }
        public Guid ChannelSystemId { get; set; }
        public bool IsPoliciBinding { get; set; }
        public SearchSettingsQuoteUploadDto()
        {

        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();
            

            return validation;
        }
    }
}