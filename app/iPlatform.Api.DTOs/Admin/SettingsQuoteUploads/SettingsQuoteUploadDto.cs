﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Admin.SettingsQuoteUploads
{
    public class SettingsQuoteUploadDto : Resource
    {
        public SettingsQuoteUploadDto()
        {

        }

        public SettingsQuoteUploadDto(string name, string value)
        {
            SettingName = name;
            SettingValue = value;
        }

        public ProductInfoDto Product { get; set; }
        public ChannelDto Channel { get; set; }
        public string Environment { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
        public bool IsPolicyBinding { get; set; }
    }
}
