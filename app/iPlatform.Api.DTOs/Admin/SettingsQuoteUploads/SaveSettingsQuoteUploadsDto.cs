﻿using iPlatform.Api.DTOs.Base;
using System.Linq;
using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Admin.SettingsQuoteUploads
{
    public class SaveSettingsQuoteUploadsDto : AttributeValidationDto, IExecutionDto
    {
        public SaveSettingsQuoteUploadsDto()
        {
            Settings = new List<SaveSettingsQuoteUploadDetailDto>();
        }
        public string Environment { get; set; }
        public List<SaveSettingsQuoteUploadDetailDto> Settings { get; set; }
        public int ChannelId { get; set; }
        public bool IsPolicyBinding { get; set; }
        public DtoContext Context { get; private set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId == 0)
                validation.Add(SettingsQuoteUploadValidationMessages.ChannelIdRequired);
            if (string.IsNullOrEmpty(Environment))
                validation.Add(SettingsQuoteUploadValidationMessages.EnvironmentRequired);
            if (!Settings.Any())
                validation.Add(SettingsQuoteUploadValidationMessages.NoSettings);
            else
                Settings.ForEach(x => validation.AddRange(x.Validate()));

            return validation;
        }
    }
}
