using MasterData;

namespace iPlatform.Api.DTOs.Admin
{
    public class ChannelReferenceDto
    {
        public int Id { get; set; }
        public string ChannelName { get; set; }
        public bool IsDefault { get; set; }
        public Country Country { get; set; }//add this so userinfo in web has access to country per channel
    }
}