﻿using System;
using iPlatform.Api.DTOs.Base;
using MasterData;
using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;
using iPlatform.Api.DTOs.Organisations;

namespace iPlatform.Api.DTOs.Admin
{
    public class ChannelDto
    {
        public ChannelDto()
        {

        }
        public int Id { get; set; }
        public Guid SystemId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
        public DateTimeDto ActivatedOn { get; set; }
        public DateTimeDto DeactivatedOn { get; set; }
        public Currency Currency { get; set; }
        public string DateFormat { get; set; }
        public Language Language { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public EmailCommunicationSettingDto EmailCommunicationSetting { get; set; }
        public ListOrganizationDto Organization { get; set; }
        public string ExternalReference { get; set; }
        public bool RequestBroker { get; set; }
        public bool RequestAccountExecutive { get; set; }
        public bool HasPolicyBinding { get; set; }
        
    }
}