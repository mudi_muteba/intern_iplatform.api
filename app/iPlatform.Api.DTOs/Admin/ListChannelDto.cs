﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Organisations;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Admin
{
    public class ListChannelDto : Resource, ICultureAware
    {
        public ListChannelDto() { }
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
        public DateTimeDto ActivatedOn { get; set; }
        public DateTimeDto DeactivatedOn { get; set; }
        public Currency Currency { get; set; }
        public Country Country { get; set; }
        public string DateFormat { get; set; }
        public Language Language { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public ListOrganizationDto Organization { get; set; }
        public string ExternalReference { get; set; }
        public bool RequestBroker { get; set; }
        public bool RequestAccountExecutive { get; set; }
        public bool HasPolicyBinding { get; set; }

        public ChannelInfoDto ParentChannel { get; set; }
    }
}
