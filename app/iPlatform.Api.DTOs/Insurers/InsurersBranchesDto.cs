﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Insurers
{
    public class InsurersBranchesDto : ICultureAware
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual DateTimeDto StartDate { get; set; }
        public virtual DateTimeDto EndDate { get; set; }
        public virtual int OrganizationId { get; set; }
        public virtual InsurersBranchTypes InsurersBranchType { get; set; }
    }
}
