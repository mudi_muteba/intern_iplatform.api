﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Insurers
{
    public class InsurerSearchDto : BaseCriteria, IValidationAvailableDto
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public InsurerSearchDto()
        {
        }

        public List<ValidationErrorMessage> Validate()
        {
            var validation = new List<ValidationErrorMessage>();

            var valid = Id.HasValue ||
                   !string.IsNullOrEmpty(Code) ||
                   !string.IsNullOrEmpty(Name);

            if (valid)
                return validation;

            validation.Add(InsurerValidationMessages.InvalidSearchCriteria);

            return validation;
        }
    }
}