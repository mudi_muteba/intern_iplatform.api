﻿using iPlatform.Api.DTOs.Base;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Insurers
{
    public class CreateInsurersBranchDto: AttributeValidationDto, IExecutionDto
    {

        public CreateInsurersBranchDto()
        {
            Context = DtoContext.NoContext();
        }

        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual int OrganizationId { get; set; }
        public virtual InsurersBranchTypes InsurersBranchType { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            return validation;
        }
    }
}
