﻿using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using MasterData;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Insurers
{
    public class ListInsurerDto : Resource, ICultureAware
    {
        public ListInsurerDto()
        {
            InsurersBranches = new List<InsurersBranchesDto>();
        }

        public string Code { get; set; }
        public string TradingName { get; set; }
        public DateTimeDto TradingSince { get; set; }
        public string RegisteredName { get; set; }
        public string Description { get; set; }
        public string RegNo { get; set; }
        public string FspNo { get; set; }
        public string VatNo { get; set; }
        public DateTimeDto DateCreated { get; set; }
        public DateTimeDto DateUpdated { get; set; }
        public virtual bool TemporaryFsp { get; set; }
        public virtual string ProfessionalIndemnityInsurer { get; set; }
        public virtual string PiCoverPolicyNo { get; set; }
        public virtual string TypeOfAgency { get; set; }
        public virtual string FgPolicyNo { get; set; }
        public virtual string FidelityGuaranteeInsurer { get; set; }
        public virtual OrganizationType OrganizationType { get; set; }
        public virtual bool ShortTermInsurers { get; set; }
        public virtual bool LongTermInsurers { get; set; }
        public IList<InsurersBranchesDto> InsurersBranches { get; set; }

    }
}