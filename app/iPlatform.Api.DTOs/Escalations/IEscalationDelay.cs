﻿using System;
using iPlatform.Enums.Escalations;

namespace iPlatform.Api.DTOs.Escalations
{
    public interface IEscalationDelay
    {
        DelayType DelayType { get; }
        IntervalType IntervalType { get; }
        int IntervalDelay { get; }
        DateTime? DateTimeDelay { get; }
    }
}