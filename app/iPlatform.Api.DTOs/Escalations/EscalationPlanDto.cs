using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using ValidationMessages;
using ValidationMessages.Escalations;

namespace iPlatform.Api.DTOs.Escalations
{
    public class EscalationPlanDto : Pagination, ICultureAware, IValidationAvailableDto, IExecutionDto
    {
        public string EventType { get; set; }
        public IEnumerable<int> Channels { get; set; }
        public IEnumerable<int> Campaigns { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public DtoContext Context { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (!Channels.Any())
                list.Add(EscalationPlanValidationMessages.ChannelIdRequired);

            return list;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}