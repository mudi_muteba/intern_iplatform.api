﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Escalations;

namespace iPlatform.Api.DTOs.Escalations
{
    public class CreateEscalationDto : AttributeValidationDto, IExecutionDto
    {
        public Guid InitialCorrelationId { get; set; }
        public int EscalationPlanStepId { get; set; }
        public int UserId { get; set; }
        public string EventType { get; set; }
        public int ChannelId { get; set; }
        public DateTime LeadCallBackDate { get; set; }
        public string JsonData { get; set; }
        public DtoContext Context { get; private set; }

        /// <summary>
        /// NB Required for Json.net to create the instance
        /// </summary>
        public CreateEscalationDto() { }

        public CreateEscalationDto(int userId, string eventType, int channelId, string jsonData)
        {
            UserId = userId;
            EventType = eventType;
            ChannelId = channelId;
            JsonData = jsonData;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();

            if (ChannelId <= 0)
                validation.Add(EscalationPlanValidationMessages.ChannelIdRequired);

            return validation;
        }
    }
}