﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Enums.Escalations;

namespace iPlatform.Api.DTOs.Escalations
{
    public class EscalationHistoryDto : Resource
    {
        public int EscalationPlanStepWorkflowMessageId { get; set; }
        public int UserId { get; set; }
        public EscalationWorkflowMessageStatus EscalationWorkflowMessageStatus { get; set; } 
    }
}