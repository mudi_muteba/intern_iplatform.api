using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Enums.Escalations;

namespace iPlatform.Api.DTOs.Escalations
{
    public class EscalationPlanStepDto : Pagination, IExecutionDto, IEscalationDelay
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }
        public DelayType DelayType { get; set; }
        public IntervalType IntervalType { get; set; }
        public int IntervalDelay { get; set; }
        public DateTime? DateTimeDelay { get; set; }
        public bool IsDeleted { get; set; }
        public IEnumerable<string> WorkflowMessageTypes { get; set; }
        public int EscalationPlanId { get; set; }
        public DtoContext Context { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}