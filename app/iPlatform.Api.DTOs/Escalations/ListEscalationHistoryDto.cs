﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Enums.Escalations;
using iPlatform.Enums.Workflows;

namespace iPlatform.Api.DTOs.Escalations
{
    public class ListEscalationHistoryDto : Resource, ICultureAware
    {
        public WorkflowMessageType WorkflowMessageEnumType { get; set; }
        public int EntityId { get; set; }
        public EscalationWorkflowMessageStatus EscalationWorkflowMessageStatusEnumType { get; protected internal set; }
        public string EscalationPlanStepName { get; set; }
        public int EscalationPlanStepSequence { get; set; }
        public int EscalationPlanStepDelay { get; set; }
    }
}