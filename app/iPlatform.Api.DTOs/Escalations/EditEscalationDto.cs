﻿using iPlatform.Api.DTOs.Base;
using iPlatform.Enums.Escalations;

namespace iPlatform.Api.DTOs.Escalations
{
    public class EditEscalationDto : IExecutionDto, IAffectExistingEntity
    {
        public int Id { get; set; }
        public EscalationWorkflowMessageStatus Status { get; set; }
        public DtoContext Context { get; private set; }

        public EditEscalationDto() { } // NB Required for Json.net to create the instance

        public EditEscalationDto(int id, EscalationWorkflowMessageStatus status)
        {
            Id = id;
            Status = status;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}