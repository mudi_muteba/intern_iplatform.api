﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.OverrideRatingQuestionMapVapQuestionDefinition
{
    public class CreateOverrideRatingQuestionMapVapQuestionDefinitionDto : AttributeValidationDto, IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public int OverrideRatingQuestionId { get; set; }

        public int MapVapQuestionDefinitionId { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        #region [ Validation ]

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            List<ValidationErrorMessage> errorMessages = new List<ValidationErrorMessage>();
            if (OverrideRatingQuestionId == 0)
                errorMessages.Add(OverrideRatingQuestionMapVapQuestionDefinitionValidationMessage.OverrideRatingQuestionIdRequired);
            if (MapVapQuestionDefinitionId == 0)
                errorMessages.Add(OverrideRatingQuestionMapVapQuestionDefinitionValidationMessage.MapVapQuestionDefinitionIdIdRequired);

            return errorMessages;
        }

        #endregion
    }
}
