﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;

namespace iPlatform.Api.DTOs.OverrideRatingQuestionMapVapQuestionDefinition
{
    public class EditOverrideRatingQuestionMapVapQuestionDefinitionDto: AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DtoContext Context { get; private set; }

        public int Id { get; set; }

        public int OverrideRatingQuestionId { get; set; }

        public int MapVapQuestionDefinitionId { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        #region [ Validation ]

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            List<ValidationErrorMessage> errorMessages = new List<ValidationErrorMessage>();
            if(Id==0)
                errorMessages.Add(OverrideRatingQuestionMapVapQuestionDefinitionValidationMessage.IdRequired);
            if (OverrideRatingQuestionId == 0)
                errorMessages.Add(OverrideRatingQuestionMapVapQuestionDefinitionValidationMessage.OverrideRatingQuestionIdRequired);
            if (MapVapQuestionDefinitionId == 0)
                errorMessages.Add(OverrideRatingQuestionMapVapQuestionDefinitionValidationMessage.MapVapQuestionDefinitionIdIdRequired);

            return errorMessages;
        }

        #endregion
    }
}
