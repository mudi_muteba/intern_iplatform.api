﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using Newtonsoft.Json;

namespace iPlatform.Api.DTOs.OverrideRatingQuestionMapVapQuestionDefinition
{
    public class OverrideRatingQuestionMapVapQuestionDefinitionDto
    {
        [JsonIgnore]
        public OverrideRatingQuestionDto OverrideRatingQuestion { get; set; }

        public MapVapQuestionDefinitionDto MapVapQuestionDefinition { get; set; }
    }
}
