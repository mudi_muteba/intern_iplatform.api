﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using iPlatform.Api.DTOs.Base;
using ValidationMessages;
using ValidationMessages.Policy;

namespace iPlatform.Api.DTOs.Upload
{
    [XmlRoot("PolicyUploadDto")]
    public class PolicyUploadDto : IExecutionDto, IValidationAvailableDto
    {
        public PolicyUploadDto()
        {
            Id = Guid.Empty;
        }

        public Guid Id { get; set; }
        public string ExternalPolicyNo { get; set; }
        public DateTime? PolicyCommenceDate { get; set; }
        public DateTime? PolicyAnniversaryDate { get; set; }
        public bool PolicyActive { get; set; }

        public PolicyUploadValidation ValidResult()
        {
            var result = new PolicyUploadValidation();

            if (Id == Guid.Empty)
                result.AddError("Invalid ID provided");

            if (string.IsNullOrEmpty(ExternalPolicyNo))
                result.AddError("No Policy Number provided");

            return result;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        public List<ValidationErrorMessage> Validate()
        {
            var list = new List<ValidationErrorMessage>();

            if (Id == Guid.Empty)
                list.Add(PolicyUploadConfirmationValidationMessages.InvalidId);

            if (string.IsNullOrEmpty(ExternalPolicyNo))
                list.Add(PolicyUploadConfirmationValidationMessages.EmptyPolicyNo);

            return list;
        }
    }
}
