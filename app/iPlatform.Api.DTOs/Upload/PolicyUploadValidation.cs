﻿using System.Collections.Generic;
using System.Linq;

namespace iPlatform.Api.DTOs.Upload
{
    public class PolicyUploadValidation
    {
        public PolicyUploadValidation()
        {
            Errors = new List<string>();
        }

        public IList<string> Errors { get; private set; }

        public bool Success
        {
            get
            {
                return !Errors.Any();
            }
        }

        public void AddError(string error)
        {
            Errors.Add(error);
        }
    }
}
