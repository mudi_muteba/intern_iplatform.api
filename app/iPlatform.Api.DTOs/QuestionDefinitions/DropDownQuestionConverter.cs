﻿using System.Linq;
using MasterData;

namespace iPlatform.Api.DTOs.QuestionDefinitions
{
    public interface IQuestionConverter
    {
        QuestionAnswer Convert(Question question, object givenAnswer);
    }

    public class DropDownQuestionConverter : IQuestionConverter
    {
        public QuestionAnswer Convert(Question question, object givenAnswer)
        {
            string answer = givenAnswer.ToString().ToLower();

            if (question.Equals(Questions.MainDriverGender))
            {
                if (new[] { "female", "f", "vrou" }.Any(answer.Contains))
                    // add to string array if more possible values comes up
                    return QuestionAnswers.MainDriverGenderFemale;
                if (new[] { "male", "m", "man" }.Any(answer.Contains))
                    // add to string array if more possible values comes up
                    return QuestionAnswers.MainDriverGenderMale;
                return QuestionAnswers.MainDriverGenderUnknown;
            }

            if (question.Equals(Questions.RelationshipToInsured))
            {
                if (new[] { "self" }.Any(answer.Contains))
                    return QuestionAnswers.RelationshipToInsuredInsured;

                if (new[] { "spouse" }.Any(answer.Contains))
                    return QuestionAnswers.RelationshipToInsuredSpouse;

                if (new[] { "child" }.Any(answer.Contains))
                    return QuestionAnswers.RelationshipToInsuredChild;

                if (new[] { "family" }.Any(answer.Contains))
                    return QuestionAnswers.RelationshipToInsuredResidingFamilyMember;

                if (new[] { "domestic" }.Any(answer.Contains))
                    return QuestionAnswers.RelationshipToInsuredDomesticWorker;

                return QuestionAnswers.RelationshipToInsuredOther;
            }

            if (question.Equals(Questions.ClassOfUse))
            {
                if (new[] { "private", "work" }.Any(answer.Contains))
                    return QuestionAnswers.ClassOfUsePrivateIncludingWork;
                if (new[] { "business" }.Any(answer.Contains))
                    return QuestionAnswers.ClassOfUseBusiness;
                if (new[] { "private" }.Any(answer.Contains))
                    return QuestionAnswers.ClassOfUseStrictlyPrivate;
                if (new[] { "farm" }.Any(answer.Contains))
                    return QuestionAnswers.ClassOfUseFarming;
                if (new[] { "multiple" }.Any(answer.Contains))
                    return QuestionAnswers.ClassOfUseCompanyMultipleDrivers;
                if (new[] { "taxi" }.Any(answer.Contains))
                    return QuestionAnswers.ClassOfUseTaxi;
                if (new[] { "goods", "cargo" }.Any(answer.Contains))
                    return QuestionAnswers.ClassOfUseBusinessGoodsCarrying;
                return QuestionAnswers.ClassOfUseUnknown;
            }

            if (question.Equals(Questions.VehicleType))
            {
                if (new[] { "MOTORCYCLE" }.Any(answer.ToUpper().Contains))
                    return QuestionAnswers.VehicleTypeMotorcycle;

                if (new[] { "sedan","HATCHBACK", "SUV", "SEDAN","TRUCK", "VAN", "COUPE", "WAGON", "CONVERTIBLE", "DIESEL",
                            "CROSSOVER", "HYBRID", "HYBRID/ELETRIC", "ELECTRIC", "SPORTS", "LUXURY", "SPORTS CAR", "LUXURY CAR",
                            "PRE-OWNED" }.Any(answer.ToUpper().Contains))
                    return QuestionAnswers.VehicleTypeSedan;

                return QuestionAnswers.VehicleTypeSedan;
            }

            if (question.Equals(Questions.VehicleImmobiliser))
            {
                if (new[] { "yes", "factory" }.Any(answer.Contains))
                    return QuestionAnswers.VehicleImmobiliserFactoryFitted;
                if (new[] { "vesa4" }.Any(answer.Contains))
                    return QuestionAnswers.VehicleImmobiliserVESA4SecureDevice;
                if (new[] { "vesa3" }.Any(answer.Contains))
                    return QuestionAnswers.VehicleImmobiliserVESA3SecureDevice;
                if (new[] { "hijack" }.Any(answer.Contains))
                    return QuestionAnswers.VehicleImmobiliserVESA4withAntiHiJack;
                return QuestionAnswers.VehicleImmobiliserNone;
            }

            if (question.Equals(Questions.VehicleTrackingDevice))
            {
                // there are so many options here, we cannot match a simple "Yes"to any of them
                return QuestionAnswers.VehicleTrackingDeviceNone;
            }

            return null;
        }
    }
}
