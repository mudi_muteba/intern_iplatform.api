﻿using MasterData;

namespace iPlatform.Api.DTOs.QuestionDefinitions
{
    public static class QuestionConverterFactory
    {
        public static IQuestionConverter GetConverter(Question question)
        {
            if (question.QuestionType.Equals(QuestionTypes.Dropdown))
                return new DropDownQuestionConverter();

            return null;
        }
    }
}
