﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Escalations;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class EscalationPlanExecutionHistoryRoutes : IDefineRoutes<EscalationHistoryDto>
    {
        private const string IdPlaceholder = "{id:int}";
        private const string PageNumberPlaceHolder = "{pageNumber:int}";
        private const string PageSizePlaceHolder = "{pageSize:int}";
        private const string EventIdPlaceholer = "{eventType}";
        private const string ChannelIdPlaceholer = "{channelId:int}";

        public string SingularRoutePrefix
        {
            get { return "/EscalationPlanExecutionHistory"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/EscalationPlanExecutionHistories"; }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Get an escalation by Id", typeof(CreateEscalationDto));
            }
        }

        public RouteDefinition GetByIds
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", SingularRoutePrefix),
                    "Get an escalation plan by EventId, ChannelId, ProductId", typeof(CreateEscalationDto));
            }
        }

        public RouteDefinition GetAllWithPagination
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/{2}", PluralRoutePrefix, PageNumberPlaceHolder, PageSizePlaceHolder),
                    "List of EscalationPlanExecutionHistories", typeof(PagedResultDto<ListUserDto>));
            }
        }

        public string CreateGetAllWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetAllWithPagination.Route
                .Replace(PageNumberPlaceHolder, pageNumber.ToString())
                .Replace(PageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Used for creating an escalation plan execution history item", typeof(CreateEscalationDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Used for updating escalation plan execution history status", typeof(EditEscalationDto));
            }
        }

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(IdPlaceholder, id.ToString());
        }

        public string CreateGetEscalationPlanByIds(CreateEscalationDto dto)
        {
            var route = GetByIds.Route.Replace(EventIdPlaceholer, dto.EventType);
            route = route.Replace(ChannelIdPlaceholer, dto.ChannelId.ToString());
            return route;
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "EscalationPlanExecutionHistory", "The entry point for escalation plan execution history", GetById.Route);
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetAllWithPagination,
                Post,
                Put
            };
        }

        public void CreateLinks(EscalationHistoryDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
    }
}