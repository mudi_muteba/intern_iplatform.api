﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.JsonDataStores;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class JsonDataStoresRoutes : IDefineRoutes<JsonDataStoreDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string searchPlaceholder = "{search}";

        public string SingularRoutePrefix
        {
            get { return "/jsondatastore"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/jsondatastores"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "JsonDataStore", "The entry point for JsonStorage", GetById.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "JsonDataStoreDocumentation");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a JsonDataStore entry by Id", typeof(JsonDataStoreDto));
            }
        }

        public RouteDefinition GetByPartyId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/party/{1}", PluralRoutePrefix, idPlaceholder),
                    "Get a JsonDataStore entry by PartyId", typeof(ListResultDto<JsonDataStoreDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", SingularRoutePrefix),
                    "Save a JsonStorage entry", typeof(JsonDataStoreDto));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), 
                    "Search a JsonStorage entry by SearchJsonDataStoreDto", typeof(JsonDataStoreSearchDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetByPartyId,
                Post,
                PostSearch
            };
        }

        public void CreateLinks(JsonDataStoreDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByPartyId(int id)
        {
            var idPart = GetByPartyId.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
    }
}
