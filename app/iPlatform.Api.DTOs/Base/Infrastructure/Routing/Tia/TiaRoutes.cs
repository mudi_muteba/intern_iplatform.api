using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Tia.Parties;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Tia
{
    public class TiaRoutes : IDefineRoutes<TiaPartiesSearchDto>
    {

        public string PluralRoutePrefix
        {
            get { return "tia/parties"; }
        }


        public string CreateGetById(int id)
        {
            throw new System.NotImplementedException();
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "Tia Parties Search", "The entry point for Tia Parties Search",
                    GetByTiaPartiesSearchDto.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format(PluralRoutePrefix),
                    "Tia Parties");
            }
        }

        public RouteDefinition GetByTiaPartiesSearchDto
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search/", PluralRoutePrefix),
                    "Get Tia Parties by Search Dto", typeof (TiaPartiesSearchDto));
            }
        }
 

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
            };
        }



        public void CreateLinks(TiaPartiesSearchDto resource)
        {
        }

       

    }
}