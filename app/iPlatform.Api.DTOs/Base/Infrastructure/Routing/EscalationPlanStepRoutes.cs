using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Authentication;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class EscalationPlanStepRoutes : IDefineRoutes
    {
        private const string idPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string SingularRoutePrefix
        {
            get { return "/escalationPlanStep"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/escalationPlanSteps"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "EscalationPlanStep", "Escalation Plan Step", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix), "Escalation Plan Step documentation");
            }
        }

        public RouteDefinition Get
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", SingularRoutePrefix) + "{id}", "Escalation Plan Step"); }
        }

        public RouteDefinition GetAll
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "Get a list of escalation plan steps"); }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), "Escalation plan step search", typeof(ListUserDto));
            }
        }

        public RouteDefinition SearchWithPagination
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder),
                    "List of escalation plans with pagination", typeof(PagedResultDto<ListUserDto>));
            }
        }

        public RouteDefinition GetByName
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", SingularRoutePrefix) + "{name}", "Get Escalation Plan Step by name"); }
        }

        public RouteDefinition Post
        {
            get { return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix), "Creates a Escalation Plan Step", typeof(CreateUserDto)); }
        }

        public RouteDefinition Put
        {
            get { return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder), "Edit a Escalation Plan Step's information", typeof(AuthenticationRequestDto)); }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Escalation Plan Step by Id", typeof(UserDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Post,
                Get,
                GetById,
                GetByName,
                Put
            };
        }

        public string CreateGetAllWithPaginationUrl(int pageNumber, int pageSize)
        {
            return SearchWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateEditUrl(int userId)
        {
            var idPart = Put.Route.Replace(idPlaceholder, userId.ToString());

            return idPart;
        }

        public void CreateLinks(Resource resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
    }
}