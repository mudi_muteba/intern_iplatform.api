﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class BankBranchRoutes : IDefineRoutes<ListBankBranchesDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return "/bankbranches"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/bankbranch"; }
        }

        public void CreateLinks(ListBankBranchesDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Bank branches by Id", typeof(ListBankBranchesDto));
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Bank branches documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the bank branches", typeof(PagedResultDto<ListBankBranchesDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the bank branches with pagination", typeof(PagedResultDto<ListBankBranchesDto>));
            }
        }




        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Bank branches search", typeof(BankBranchesSearchDto));
            }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "BankBranch", "The entry point for bank branch", Get.Route); }
        }





        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetWithPagination,
                GetById,
                PostSearch
            };
        }
    }
}
