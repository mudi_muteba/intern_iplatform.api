using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class MotorLossHistoryRoutes : IDefineRoutes<ListMotorLossHistoryDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string SingularRoutePrefix
        {
            get { return "motorlosshistory"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/motorlosshistories"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "MotorLossHistory documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "MotorLossHistory", "The entry point for MotorLossHistories", Get.Route);
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of MotorLossHistory", typeof(PagedResultDto<ListMotorLossHistoryDto>));
        }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a MotorLossHistory by Id", typeof(ListMotorLossHistoryDto));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of MotorLossHistories with pagination", typeof(PagedResultDto<ListMotorLossHistoryDto>));
            }
        }

        public RouteDefinition GetAll
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the MotorLossHistories", typeof(PagedResultDto<ListMotorLossHistoryDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a new MotorLossHistory", typeof(CreateMotorLossHistoryDto));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search motor loss history by search DTO", typeof(PagedResultDto<ListMotorLossHistoryDto>));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, idPlaceholder),
                    "Update a MotorLossHistory", typeof(EditMotorLossHistoryDto));
            }
        }

        public RouteDefinition PutDisable
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                    "Disable a MotorLossHistory");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                Get,
                GetWithPagination,
                GetAll,
                Post,
                PostSearch,
                PutDisable
            };
        }

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(idPlaceholder, id.ToString());
        }

        public void CreateLinks(Resource resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
        
        public void CreateLinks(ListMotorLossHistoryDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string PutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string DisableById(int id)
        {
            var idPart = PutDisable.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

    }
}