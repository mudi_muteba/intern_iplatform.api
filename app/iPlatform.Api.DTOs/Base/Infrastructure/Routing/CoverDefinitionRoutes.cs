using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class CoverDefinitionRoutes 
    {
        private const string idPlaceholder = "{id:int}";
        private const string productIdPlaceholder = "{product:int}";


        public string SingularRoutePrefix
        {
            get { return String.Format("/product/{0}/cover/{1}", productIdPlaceholder, idPlaceholder); }
        }

        public string PluralRoutePrefix
        {
            get { return String.Format("/product/{0}/covers", productIdPlaceholder); }
        }

        public RouteCategoryDefinition Definition { get; set; }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", SingularRoutePrefix, "Get cover by id for product");
            }
        }

        public RouteDefinition GetByProductId
        {
            get
            {
                return new RouteDefinition("GET", PluralRoutePrefix, "Get covers for a product");
            }
        }

        public RouteDefinition GetBenefitsById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/benefits", SingularRoutePrefix),
                    "Get benefits by product Id and cover Id", typeof(List<BenefitDto>));
            }
        }


        public string CreateCoversByIdUrl(int productId, int coverId)
        {
            return GetById.Route
                .Replace(productIdPlaceholder, productId.ToString())
                .Replace(idPlaceholder, coverId.ToString());
        }

        public string CreateCoversByProductIdUrl(int productId)
        {
            return GetByProductId.Route
                .Replace(productIdPlaceholder, productId.ToString());
        }

        public string CreateBenefitsByCoverIdUrl(int productId, int coverId)
        {
            return GetBenefitsById.Route
                .Replace(productIdPlaceholder, productId.ToString())
                .Replace(idPlaceholder, coverId.ToString());
        }

        public void CreateLinks(ListProductDto resource)
        {
            throw new NotImplementedException();
        }

        public string CreateGetById(int productId, int coverId)
        {
            return GetById.Route
                .Replace(productIdPlaceholder, productId.ToString())
                .Replace(idPlaceholder, coverId.ToString());
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            throw new NotImplementedException();
        }

        public void CreateLinks(int productId, ListCoverDefinitionDto cover)
        {
            cover.AddLink("self", CreateGetById(productId, cover.Id));
            cover.AddLink("parent", SystemRoutes.Products.CreateGetById(productId));
        }

        public void CreateLinks(CoverDefinitionDto cover)
        {
            cover.AddLink("self", CreateGetById(cover.Product.Id, cover.Id));
            cover.AddLink("parent", SystemRoutes.Products.CreateGetById(cover.Product.Id));
        }

    }
}