using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Insurers;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class InsurersRoutes : IDefineRoutes<ListInsurerDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string idInsurerPlaceholder = "{insurerId:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";
        public string PluralRoutePrefix
        {
            get { return "/insurers"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/insurer"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", SingularRoutePrefix),
                    "Insurers documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Insurers", "The entry point for Insurers", Get.Route); }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Insurer by Id", typeof(ListInsurerDto));
            }
        }
        
        public RouteDefinition GetAll
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the Insurers", typeof(PagedResultDto<ListInsurerDto>));
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", SingularRoutePrefix),
                    "List of the Insurers", typeof(PagedResultDto<ListInsurerDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the Insurers with pagination", typeof(PagedResultDto<ListInsurerDto>));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Insurer search", typeof(InsurerSearchDto));
            }
        }
        
        #region Insurers Branches
        public RouteDefinition GetInsurerBranches
        {
            get
            {
                return new RouteDefinition("Get", string.Format("{0}/{1}/branches", SingularRoutePrefix, idPlaceholder),
                    "Get Insurer branch", typeof(InsurersBranchesDto));
            }
        }

        public RouteDefinition PostInsurerBranch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/branches", SingularRoutePrefix, idPlaceholder),
                    "Create Insurer branch", typeof(CreateInsurersBranchDto));
            }
        }

        public RouteDefinition PutInsurerBranch
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/branch/{2}", SingularRoutePrefix, idInsurerPlaceholder, idPlaceholder),
                    "Edit Insurer branch", typeof(EditInsurersBranchDto));
            }
        }

        public RouteDefinition DeleteInsurerBranch
        {
            get
            {
                return new RouteDefinition("DELETE", string.Format("{0}/{1}/branch{2}", SingularRoutePrefix, idInsurerPlaceholder, idPlaceholder),
                    "Edit Insurer branch", typeof(EditInsurersBranchDto));
            }
        }

        public RouteDefinition GetInsurerBranchById
        {
            get
            {
                return new RouteDefinition("Get", string.Format("{0}/{1}/branch{2}", SingularRoutePrefix, idInsurerPlaceholder, idPlaceholder),
                    "Get Insurer branch", typeof(InsurersBranchesDto));
            }
        }

        public string CreateGetBranchById(int insurerId, int id)
        {
            var idPart = GetInsurerBranchById.Route
                .Replace(idInsurerPlaceholder, id.ToString());

            return idPart;
        }
        #endregion

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetAll,
                GetById,
                GetWithPagination,
                PostSearch
            };
        }

        public string CreatePaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public void CreateLinks(ListInsurerDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

    }
}