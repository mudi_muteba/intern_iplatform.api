﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class RatingRuleHeaderCalculationRoutes : IDefineRoutes<RatingRuleHeaderCalculationDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string productIdPlaceholder = "{productId:int}";
        private const string channelIdPlaceholder = "{channelId:int}";
        private const string enabledPlaceholder = "{enabled:bool}";

        public string PluralRoutePrefix
        {
            get { return string.Format("/RatingRuleHeaderCalculations"); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("/RatingRuleHeaderCalculation"); }
        }

        public void CreateLinks(ListRatingRuleHeaderCalculationDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(RatingRuleHeaderCalculationDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        #region [ Create URL's ]

        public string GetListByCriteria(int productId, int channelId)
        {
            var idPart = GetList.Route.Replace(productIdPlaceholder, productId.ToString()).Replace(channelIdPlaceholder, channelId.ToString());
            return idPart;
        }

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(idPlaceholder, id.ToString());
        }

        public string CreatePutById(int id)
        {
            return Put.Route.Replace(idPlaceholder, id.ToString());
        }

        public string CreateDeleteById(int id)
        {
            return PutDelete.Route.Replace(idPlaceholder, id.ToString());
        }

        #endregion

        #region [ Route Definitions ]

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetList,
                Put,
                Post,
                PutDelete,
                GetById,
                Get,
                Search
            };
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}", SingularRoutePrefix),
                    "RatingRuleHeaderCalculation() documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "RatingRuleHeaderCalculation", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Save Rating Rule Header Calculation", typeof(CreateRatingRuleHeaderCalculationDto));
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("/{0}", PluralRoutePrefix),
                    "Get all Rating Rule Header Calculations");
            }
        }

        public RouteDefinition GetList
        {
            get
            {
                return new RouteDefinition("GET", string.Format("/{0}", PluralRoutePrefix),
                    "Get List Rating Rule Header Calculations");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("/{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Rating Rule Header Calculation");
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search Rating Rule Header Calculations", typeof(PagedResultDto<ListRatingRuleHeaderCalculationDto>));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Edit Rating Rule Header Calculation", typeof(EditRatingRuleHeaderCalculationDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/delete/{1}", SingularRoutePrefix, idPlaceholder),
                    "Delete Rating Rule Header Calculation", typeof(DeleteRatingRuleHeaderCalculationDto));
            }
        }

        #endregion
    }
}
