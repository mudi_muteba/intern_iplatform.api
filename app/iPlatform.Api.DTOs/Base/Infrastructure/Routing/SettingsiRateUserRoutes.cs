﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class SettingsiRateUserRoutes : IDefineRoutes<SettingsiRateUserDto>
    {
        private const string SettingsiRateUserIdPlaceholder = "{SettingsiRateUserId:int}";
        private const string IdPlaceholder = "{id:int}";
        private const string PageNumberPlaceHolder = "{pageNumber:int}";
        private const string PageSizePlaceHolder = "{pageSize:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("SettingsiRateUsers", SettingsiRateUserIdPlaceholder); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("SettingsiRateUser/{0}", IdPlaceholder); }
        }
        public string SingularRoutePrefixCreate
        {
            get { return "SettingsiRateUser/"; }
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePutById(int id)
        {
            var idPart = Put.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(PageNumberPlaceHolder, pageNumber.ToString())
                .Replace(PageSizePlaceHolder, pageSize.ToString());
        }

        public string CreateDeleteById(int id)
        {
            var idPart = PutDelete.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get settings iRate User details by Id", typeof(SettingsiRateUserDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", SingularRoutePrefixCreate,
                    "Create settings iRate User details", typeof(CreateSettingsiRateUserDto));
            }
        }

        public RouteDefinition PostMultiple
        {
            get
            {
                return new RouteDefinition("POST", "SettingsiRateUser/multiple",
                    "Save multiple settings iRate User details", typeof(SaveMultipleSettingsiRateUserDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/", SingularRoutePrefix), "Update a setting iRate User", typeof(EditSettingsiRateUserDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/delete", SingularRoutePrefixCreate, IdPlaceholder), "Delete an iRate User setting");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Settings iRate Users", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Settings iRate User Documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "List of the settings iRate User details", typeof(PagedResultDto<ListSettingsiRateUserDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, PageNumberPlaceHolder, PageSizePlaceHolder), "List of the settings iRate Users", typeof(PagedResultDto<ListSettingsiRateUserDto>));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search iRate Users Setting ", typeof(SearchSettingsiRateUserDto));
            }
        }

        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix), "List of the settings iRate Users", typeof(PagedResultDto<ListSettingsiRateUserDto>));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {

                GetById,
                Options,
                Post,
                Get,
                Put,
                PutDelete,
                GetWithPagination,
                Search,
                GetNoPagination,
                PostMultiple
            };
        }

        public void CreateLinks(SettingsiRateUserDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }

        public void CreateLinks(ListSettingsiRateUserDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

    }
}
