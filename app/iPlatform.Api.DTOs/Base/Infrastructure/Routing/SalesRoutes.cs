﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Sales;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class SalesRoutes : IDefineRoutes
    {
        private const string IdPlaceholder = "{id:int}";
        private const string m_LeadIdPlaceholder = "{leadId:int}";

        public string PluralRoutePrefix
        {
            get { return "/Sales"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/Sale"; }
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetSalesDetailByLeadId(int leadId)
        {
            var idPart = GetSalesDetailByLeadId.Route.Replace(m_LeadIdPlaceholder, leadId.ToString());
            return idPart;
        }


        public string CreateGetBySalesStructureId(int id)
        {
            var idPart = GetSalesStructureById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetBySalesDetailId(int id)
        {
            var idPart = GetSalesDetailById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/fni/{1}", PluralRoutePrefix, IdPlaceholder),
                    "Get salesFNI by Id", typeof(SalesFNIDto));
            }
        }

        public RouteDefinition GetSalesStructureById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/salesstructure/{1}", PluralRoutePrefix, IdPlaceholder),
                    "Get Sales Structure by Id", typeof(SalesStructureDto));
            }
        }
        public RouteDefinition GetSalesDetailById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/salesdetail/{1}", PluralRoutePrefix, IdPlaceholder),
                    "Get Sales Detail by Id", typeof(SalesDetailDto));
            }
        }

        public RouteDefinition CreateSalesFNI
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/fni", PluralRoutePrefix),
                    "Create salesFNI", typeof(CreateSalesFNIDto));
            }
        }

        public RouteDefinition SaveSalesStructure
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/salesstructure", PluralRoutePrefix),
                    "Create salesFNI", typeof(CreateSalesFNIDto));
            }
        }
        public RouteDefinition SaveSalesDetails
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/salesdetail", PluralRoutePrefix),
                    "Create Sales Details", typeof(CreateSalesDetailsDto));
            }
        }

        public RouteDefinition GetSalesDetailByLeadId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/salesdetail{1}", SingularRoutePrefix, m_LeadIdPlaceholder),
                    "Get Sales Detail by Lead Id", typeof(SalesDetailDto));
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Sales", "The entry point for sales", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Sales documentation");
            }
        }


        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                CreateSalesFNI,
                GetById,
                Options,
                SaveSalesStructure,
                Options,
                GetSalesStructureById,
                SaveSalesDetails,
                GetSalesDetailByLeadId
            };
        }
    }
}
