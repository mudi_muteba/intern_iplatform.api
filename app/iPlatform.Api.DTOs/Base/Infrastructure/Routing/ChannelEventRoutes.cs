﻿
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using System;
using iPlatform.Api.DTOs.Admin.ChannelEvents;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ChannelEventRoutes : IDefineRoutes<ChannelEventDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string channelIdPlaceholder = "{channelid:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string PluralRoutePrefix
        {
            get { return "/channelevents"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/channelevent"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "ChannelEvents", "The entry point for Channel Permissions", Get.Route); }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "List of the Channel Events", typeof(ListResultDto<ChannelEventDto>));
            }
        }
        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix),
                    "List of the Channel Events", typeof(ListResultDto<ChannelEventDto>));
            }
        }
        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder),
                    "List of the channels with pagination", typeof(PagedResultDto<ChannelEventDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Channel Event by Id", typeof(ChannelEventDto));
            }
        }

        public RouteDefinition GetByChannelId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", PluralRoutePrefix, channelIdPlaceholder),
                    "List of the Channel Events by channelId", typeof(ListResultDto<ChannelEventDto>));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Update a Channel Event by Id", typeof(EditChannelEventDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a Channel Event", typeof(CreateChannelEventDto));
            }
        }
        public RouteDefinition SaveMultiple
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/multiple", PluralRoutePrefix),
                    "Save Multiple Channel Event", typeof(SaveChannelEventsDto));
            }
        }
        public RouteDefinition Delete
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                    "Disable a Channel Permission by Id", typeof(DeleteChannelEventDto));
            }
        }    

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search a Channel Permission", typeof(ListResultDto<ChannelEventDto>));
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Channels documentation");
            }
        }
        
        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetById,
                GetByChannelId,
                Post,
                Search,
                Delete,
                Put,
                GetNoPagination,
                GetWithPagination,
                SaveMultiple
            };
        }

        public void CreateLinks(ChannelEventDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateDeleteById(int id)
        {
            var idPart = Delete.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByChannelId(int channelId)
        {
            var idPart = GetByChannelId.Route.Replace(channelIdPlaceholder, channelId.ToString());

            return idPart;
        }
        public string CreateGetWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString());
        }


    }
}
