using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Users.Discount;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class UserDiscountsRoutes : IDefineRoutes<DiscountDto>
    {
        private const string userIdPlaceholder = "{userId:int}";
        private const string channelIdPlaceholder = "{channelId:int}";
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("user/{0}/Discounts", userIdPlaceholder); }
        }


        public string SingularRoutePrefix
        {
            get { return string.Format("/user/{0}/Discount/{1}", userIdPlaceholder, idPlaceholder); }
        }

        public string SingularSearchRoutePrefix
        {
            get { return string.Format("/user/{0}/Discount", userIdPlaceholder); }
        }

        public RouteDefinition GetByCriteria
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularSearchRoutePrefix),
                    "Get an Discount by Id", typeof(DiscountDto));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get an Discount by Id", typeof (DiscountDto));
            }
        }

        public RouteDefinition GetDiscounts
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get an Discount by Id", typeof(ListDiscountDto));
            }
        }

        public RouteDefinition GetDiscountsByUserAndChannel
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", PluralRoutePrefix, channelIdPlaceholder),
                    "Get a Discount by User and ChannelId", typeof(ListDiscountDto));
            }
        }

        public RouteDefinition PostCreateDiscount
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create an Discount", typeof (CreateDiscountDto));
            }
        }


        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("UserDiscounts", PluralRoutePrefix),
                    "User Discount documentation");
            }
        }


        public string CreateGetById(int id)
        {
            return PluralRoutePrefix
                .Replace(userIdPlaceholder, id.ToString());
        }

        public string CreateGetByUserIdAndChannelId(int userId, int channelId)
        {
            return GetDiscountsByUserAndChannel.Route
                .Replace(userIdPlaceholder, userId.ToString())
                .Replace(channelIdPlaceholder, channelId.ToString());
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "UsersDiscounts", "The entry point for users",
                    GetById.Route);
            }
        }


        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                PostCreateDiscount
            };
        }

        public void CreateLinks(DiscountDto resource)
        {
        }

        public string CreateGetById(int userId, int discountId)
        {
            return SingularRoutePrefix
                .Replace(userIdPlaceholder, userId.ToString())
                .Replace(idPlaceholder, discountId.ToString());
        }

        public string SearchByCriteria(int userId)
        {
            return SingularSearchRoutePrefix
                .Replace(userIdPlaceholder, userId.ToString());
        }

        public string GetDiscountsUrl(int userId)
        {
            return PluralRoutePrefix
                .Replace(userIdPlaceholder, userId.ToString());
        }

        public void CreateLinks(ListDiscountDto resource)
        {
        }
    }
}