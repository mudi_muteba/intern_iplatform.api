﻿
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;


namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class MapVapQuestionDefinitionRoutes : IDefineRoutes<ListMapVapQuestionDefinitionDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string productIdPlaceholder = "{productId:int}";
        private const string channelIdPlaceholder = "{channelId:int}";
        private const string enabledPlaceholder = "{enabled:bool}";

        public string PluralRoutePrefix
        {
            get { return string.Format("/MapVapQuestionDefinitions"); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("/MapVapQuestionDefinition"); }
        }

        public void CreateLinks(ListMapVapQuestionDefinitionDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(MapVapQuestionDefinitionDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        #region [ Create URL's ]

        public string GetListByCriteria(int productId, int channelId)
        {
            var idPart = GetList.Route.Replace(productIdPlaceholder, productId.ToString()).Replace(channelIdPlaceholder, channelId.ToString());
            return idPart;
        }

        public string CreateGetListByEnabledUrl(int productId, int channelId, bool enabled)
        {
            var idPart = GetListByEnabled.Route
                .Replace(productIdPlaceholder, productId.ToString())
                .Replace(channelIdPlaceholder, channelId.ToString())
                .Replace(enabledPlaceholder, enabled.ToString());

            return idPart;
        }

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(idPlaceholder, id.ToString());
        }

        public string CreatePutById(int id)
        {
            return Put.Route.Replace(idPlaceholder, id.ToString());
        }

        public string CreateDeleteById(int id)
        {
            return PutDelete.Route.Replace(idPlaceholder, id.ToString());
        }

        #endregion

        #region [ Route Definitions ]

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetList,
                Put,
                Post,
                PutDelete,
                GetById,
                Get,
                GetListByEnabled,
                Search
            };
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}", SingularRoutePrefix),
                    "MapVapQuestionDefinition() documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "MapVapQuestionDefinition", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Save Map Vap Question Definition ", typeof(CreateMapVapQuestionDefinitionDto));
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("/{0}", PluralRoutePrefix),
                    "Get all Map Vap Question Definition ");
            }
        }

        public RouteDefinition GetList
        {
            get
            {
                return new RouteDefinition("GET", string.Format("/{0}/{1}/{2}", PluralRoutePrefix, productIdPlaceholder, channelIdPlaceholder),
                    "Get List Map Vap Question Definition");
            }
        }

        public RouteDefinition GetListByEnabled
        {
            get
            {
                return new RouteDefinition("GET", string.Format("/{0}/{1}/{2}/{3}", PluralRoutePrefix, productIdPlaceholder, channelIdPlaceholder, enabledPlaceholder),
                    "Get List Map Vap Question Definition ");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("/{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Map Vap Question Definition ");
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), "Search MapVapQuestionDefinition", typeof(PagedResultDto<ListMapVapQuestionDefinitionDto>));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Edit Map Vap Question Definition ", typeof(EditMapVapQuestionDefinitionDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/delete/{1}", SingularRoutePrefix, idPlaceholder),
                    "Delete Map Vap Question Definition ", typeof(DeleteMapVapQuestionDefinitionDto));
            }
        }

        #endregion
    }
}
