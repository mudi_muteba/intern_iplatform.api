﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class EmailCommunicationSettingRoutes : IDefineRoutes<EmailCommunicationSettingDto>
    {
        private const string IdPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string PluralRoutePrefix
        {
            get { return "/EmailCommunicationSettings"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/EmailCommunicationSetting"; }
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateDeleteById(int id)
        {
            var idPart = PutDelete.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }
        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Get Email Communication by Id", typeof(EmailCommunicationSettingDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/createmultiple", PluralRoutePrefix),
                    "Create multiple E-Mail Communication Settings", typeof(CreateMultipleEmailCommunicationSettingsDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/delete", SingularRoutePrefix, IdPlaceholder), 
                    "Delete an Email Communication");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Email Communication", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Email Communication documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), 
                    "List of the Email Communication", typeof(PagedResultDto<EmailCommunicationSettingDto>));
            }
        }
        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), 
                    "Search Email Communication", typeof(PagedResultDto<EmailCommunicationSettingDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder), 
                    "List of the Email Communication", typeof(PagedResultDto<EmailCommunicationSettingDto>));
            }
        }

        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix), 
                    "List of the Email Communication", typeof(PagedResultDto<EmailCommunicationSettingDto>));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetById,
                Options,
                Post,
                Get,
                PutDelete,
                GetWithPagination,
                GetNoPagination,
                Search
            };
        }

        public string CreateGetWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString());
        }

        public void CreateLinks(EmailCommunicationSettingDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }


    }
}
