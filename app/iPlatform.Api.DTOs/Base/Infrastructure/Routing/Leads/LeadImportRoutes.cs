using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Leads;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Leads
{
    public class LeadImportRoutes : IDefineRoutes<ListLeadImportDto>
    {
        private const string idPlaceholder = "{id:guid}";

        public string SingularRoutePrefix
        {
            get { return string.Format("/import/lead"); }
        }

        public string PluralRoutePrefix
        {
            get { return string.Format("/import/leads"); }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "ImportLead", "The entry point for imported lead",
                    GetById.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format(SingularRoutePrefix),
                    "Lead import documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get imported leads from excel by reference");
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the campaigns with pagination", typeof(PagedResultDto<ListLeadImportDto>));
            }
        }

        public RouteDefinition GetAll
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get all imported leads from excel", typeof(PagedResultDto<ListLeadImportDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Lead imported by Id", typeof(ListLeadImportDto));
            }
        }

        public RouteDefinition PostSeriti
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}", SingularRoutePrefix),
                    "Import Lead Seriti", typeof(LeadImportXMLDto));
            }
        }

        public RouteDefinition SubmitLead
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/submit", SingularRoutePrefix),
                    "Import Lead", typeof(SubmitLeadDto));
            }
        }

        public RouteDefinition PostExcel
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}", PluralRoutePrefix),
                    "Import Leads Excel");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetWithPagination,
                GetAll,
                GetById,
                SubmitLead,
                PostSeriti,
                PostExcel
            };
        }

        public void CreateLinks(ListLeadImportDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.LeadImportReference));
        }

        public string CreateGetById(int id = 0)
        {
            return SingularRoutePrefix;
        }

        public string CreateGetById(Guid id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

    }
}