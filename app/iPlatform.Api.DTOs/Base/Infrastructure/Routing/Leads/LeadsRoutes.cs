using System.Collections.Generic;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Campaign;
using iPlatform.Api.DTOs.Leads.Quality;
using iPlatform.Api.DTOs.SectionsPerChannel;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Leads
{
    public class LeadsRoutes : IDefineRoutes<LeadDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string campaignIdPlaceholder = "{campaignid:int}";
        public string PluralRoutePrefix
        {
            get { return "/leads"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/lead"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "LeadActivities", "The entry point for LeadActivity", Get.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Leads documentation");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a lead by Id", typeof(LeadDto));
            }
        }

        public RouteDefinition GetSectionsByActiveChannelId
        {
            get
            {
                return new RouteDefinition("GET", 
                    string.Format("{0}/GetSectionsPerChannel", SingularRoutePrefix),
                    "Get a lead by Id", typeof(GetSectionsPerChannelDto));
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the Leads", typeof(PagedResultDto<LeadDto>));
            }
        }

        public RouteDefinition GetFullLeadById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/complete", SingularRoutePrefix, idPlaceholder),
                    "Get a lead by Id", typeof(LeadDto));
            }
        }

        public RouteDefinition GetByIdDetailed
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/detailed", SingularRoutePrefix, idPlaceholder),
                    "Get a lead by Id", typeof(ListLeadAdvanceDto));
            }
        }

        public RouteDefinition GetByIdDetailedSimplified
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/detailedsimplified", SingularRoutePrefix, idPlaceholder),
                    "Get a lead by Id", typeof(ListLeadSimplifiedDto));
            }
        }

        public RouteDefinition GetByPartyId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/byparty", SingularRoutePrefix, idPlaceholder),
                    "Get a lead by party Id", typeof(LeadDto));
            }
        }

        public RouteDefinition GetFullLeadByPartyId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/fullleadbyparty", SingularRoutePrefix, idPlaceholder),
                    "Get a lead by party Id", typeof(LeadDto));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the leads with pagination", typeof(PagedResultDto<LeadDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Save a Lead", typeof(CreateLeadDto));
            }
        }

        public RouteDefinition Import
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/import", PluralRoutePrefix),
                    "Import a Lead", typeof (SubmitLeadDto));
            }
        }

        public RouteDefinition Dead
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/dead", SingularRoutePrefix, idPlaceholder),
                    "Save a lead dead by Id", typeof(DeadLeadActivityDto));
            }
        }

        public RouteDefinition Loss
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/loss", SingularRoutePrefix, idPlaceholder),
                    "Save a lead loss by Id", typeof(LossLeadActivityDto));
            }
        }

        public RouteDefinition Delay
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/delay", SingularRoutePrefix, idPlaceholder),
                    "Save a lead delay by Id",typeof(DelayLeadActivityDto));
            }
        }

        public RouteDefinition Imported
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/imported", SingularRoutePrefix, idPlaceholder),
                    "Save a lead delay by Id", typeof(ImportedLeadActivityDto));
            }
        }
        public RouteDefinition PolicyBindingCreated
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/policybindingcreated", SingularRoutePrefix, idPlaceholder),
                    "Save a lead policy binding created by Id", typeof(PolicyBindingCreatedLeadActivityDto));
            }
        }
        public RouteDefinition Quality
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/quality", SingularRoutePrefix, idPlaceholder),
                    "Get a lead quality by LeadId", typeof(LeadQualityDto));
            }
        }

        public RouteDefinition PostQuality
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/quality", SingularRoutePrefix, idPlaceholder),
                    "Save a lead quality by LeadId", typeof(CreateLeadQualityDto));
            }
        }

        public RouteDefinition PutQuality
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/quality", SingularRoutePrefix, idPlaceholder),
                    "Save a lead quality by LeadId", typeof(EditLeadQualityDto));
            }
        }


        public RouteDefinition PutLeadStatus
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/status", SingularRoutePrefix, idPlaceholder),
                    "Update Lead Status related to campaign using campaign statuses", typeof(UpdateLeadStatusDto));
            }
        }

        public RouteDefinition RejectedAtUnderwriting
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/rejectedatunderwriting", SingularRoutePrefix, idPlaceholder),
                    "Save a lead rejected at underwriting by Id", typeof(RejectedAtUnderwritingLeadActivityDto));
            }
        }

        public RouteDefinition GetRegOwnerIdContacts
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/getregowneridcontacts", SingularRoutePrefix),"Get Registered Owner Contacts",typeof(RegOwnerContactDto));
            }
        }

        public RouteDefinition Sold
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/sold", SingularRoutePrefix, idPlaceholder),
                    "Save a lead sold by Id", typeof(SoldLeadActivityDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetById,
                GetFullLeadById,
                GetByIdDetailed,
                GetByIdDetailedSimplified,
                GetByPartyId,
                GetFullLeadByPartyId,
                GetWithPagination,
                Post,
                Import,
                Dead,
                Loss,
                Delay,
                Quality,
                PostQuality,
                PutQuality,
                PutLeadStatus,
                RejectedAtUnderwriting,
                GetRegOwnerIdContacts,
                Sold,
                Imported,
                PolicyBindingCreated
            };
        }

        public void CreateLinks(ImportLeadDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(LeadDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(LeadQualityDto resource)
        {
            resource.AddLink("self", CreateQualityGetById(resource.LeadId));
        }

        public void CreateLinks(ListLeadAdvanceDto resource)
        {
            resource.AddLink("self", CreateQualityGetById(resource.Id));
        }

        public void CreateLinks(ListLeadSimplifiedDto resource)
        {
            resource.AddLink("self", CreateQualityGetById(resource.Id));
        }

        public string CreateQualityGetById(int id)
        {
            var idPart = Quality.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string GetActiveSectionsChannelId(int id)
        {
            var idPart = GetSectionsByActiveChannelId.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByIdDetailed(int id)
        {
            var idPart = GetByIdDetailed.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByIdDetailedSimplified(int id)
        {
            var idPart = GetByIdDetailedSimplified.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByPartyId(int id)
        {
            var idPart = GetByPartyId.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetFullByPartyId(int id)
        {
            var idPart = GetFullLeadByPartyId.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetFullById(int id)
        {
            var idPart = GetFullLeadById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string DeadById(int id)
        {
            var idPart = Dead.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string LossById(int id)
        {
            var idPart = Loss.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string DelayById(int id)
        {
            var idPart = Delay.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string ImportedById(int id)
        {
            var idPart = Imported.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string QualityById(int id)
        {
            var idPart = Quality.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string UpdateLeadStatus(int id)
        {
            var idPart = PutLeadStatus.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string RejectedAtUnderwritingById(int id)
        {
            var idPart = RejectedAtUnderwriting.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string SoldById(int id)
        {
            var idPart = Sold.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string PolicBindingCreatedById(int id)
        {
            var idPart = PolicyBindingCreated.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
    }
}