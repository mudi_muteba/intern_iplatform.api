using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Leads;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Leads
{
    public class IndividualUploadDetailRoutes : IDefineRoutes<IndividualUploadDetailDto>
    {
        private const string m_LeadImportReference = "{leadImportReference:guid}";
        private const string m_PageNumberPlaceHolder = "{pageNumber:int}";
        private const string m_PageSizePlaceHolder = "{pageSize:int}";

        public string SingularRoutePrefix
        {
            get { return string.Format("/IndividualDetailUpload"); }
        }

        public string PluralRoutePrefix
        {
            get { return string.Format("/IndividualDetailUploads"); }
        }

        public void CreateLinks(IndividualUploadDetailDto resource)
        {
            resource.AddLink("self", CreateGetByLeadImportReference(resource.LeadImportReference));
        }

        #region [ Create URL's ]

        public string CreateGetById(int id = 0)
        {
            return SingularRoutePrefix;
        }

        public string CreateGetByLeadImportReference(Guid id)
        {
            return GetByLeadImportReference.Route.Replace(m_LeadImportReference, id.ToString());
        }

        public string EditGetById(Guid leadImportReference)
        {
            return Edit.Route.Replace(m_LeadImportReference, leadImportReference.ToString());
        }

        public string GetAllWithPaginationUrl(Guid leadImportReference, int pageNumber, int pageSize)
        {
            return GetAllWithPagination.Route
                .Replace(m_LeadImportReference, leadImportReference.ToString())
                .Replace(m_PageNumberPlaceHolder, pageNumber.ToString())
                .Replace(m_PageSizePlaceHolder, pageSize.ToString());
        }

        #endregion

        #region [ Route Definitions ]

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
               Create,
               Edit,
               GetByLeadImportReference,
               GetAllWithPagination,
               GetAllDetailsWithPagination
            };
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "IndividualUploadDetailRoutes", "The entry point for Individual Upload DetailRoutes", GetByLeadImportReference.Route); }
        }

        public RouteDefinition Create
        {
            get
            {
                return new RouteDefinition("POST", PluralRoutePrefix, "Create detail record", typeof(IndividualUploadDetailDto));
            }
        }

        public RouteDefinition Edit
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, m_LeadImportReference), "Create detail record", typeof(EditIndividualUploadDetailDto));
            }
        }

        public RouteDefinition GetByLeadImportReference
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, m_LeadImportReference),
                    "Get a Lead detail by Id", typeof(IndividualUploadDetailDto));
            }
        }

        public RouteDefinition GetAllWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", PluralRoutePrefix, m_LeadImportReference), "Get all imported lead details from excel", typeof(PagedResultDto<IndividualUploadDetailDto>));
            }
        }

        public RouteDefinition GetAllDetailsWithPagination
        {
            get
            {
                return new RouteDefinition("GET", PluralRoutePrefix, "Get all imported lead details from excel", typeof(PagedResultDto<IndividualUploadDetailDto>));
            }
        }

        #endregion
    }
}