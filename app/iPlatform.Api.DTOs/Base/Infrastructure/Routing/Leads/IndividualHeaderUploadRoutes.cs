using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Leads;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Leads
{
    public class IndividualHeaderUploadRoutes : IDefineRoutes<CreateIndividualUploadHeaderDto>
    {
        private const string IdPlaceholder = "{id:guid}";
        private const string PageNumberPlaceHolder = "{pageNumber:int}";
        private const string PageSizePlaceHolder = "{pageSize:int}";
        public string SingularRoutePrefix
        {
            get { return string.Format("/IndividualHeaderUpload"); }
        }

        public string PluralRoutePrefix
        {
            get { return string.Format("/IndividualHeaderUploads"); }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Get a Lead imported by Id", typeof(CreateIndividualUploadHeaderDto));
            }
        }

        public RouteDefinition Create
        {
            get
            {
                return new RouteDefinition("POST", PluralRoutePrefix, "Create header record", typeof(CreateIndividualUploadHeaderDto));
            }
        }

        public RouteDefinition Edit
        {
            get
            {
                return new RouteDefinition("PUT", SingularRoutePrefix, "Consolidate header record", typeof(EditIndividualUploadHeaderDto));
            }
        }

        public RouteDefinition GetAllWithPagination
        {
            get
            {
                return new RouteDefinition("GET", PluralRoutePrefix, "Get all imported lead headers from excel", typeof(PagedResultDto<CreateIndividualUploadHeaderDto>));
            }
        }

        public RouteDefinition GetAllDetailsWithPagination
        {
            get
            {
                return new RouteDefinition("GET", PluralRoutePrefix, "Get all imported lead headers from excel", typeof(PagedResultDto<CreateIndividualUploadHeaderDto>));
            }
        }

        public string GetAllWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetAllWithPagination.Route
                .Replace(PageNumberPlaceHolder, pageNumber.ToString())
                .Replace(PageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public void CreateLinks(CreateIndividualUploadHeaderDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.LeadImportReference));
        }

        public string CreateGetById(int id = 0)
        {
            return SingularRoutePrefix;
        }

        public string CreateGetById(Guid id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "IndividualHeaderUploadRoutes", "The entry point for Individual Header UploadRoutes", GetById.Route); }
        }

        public IEnumerable<RouteDefinition> Routes()
        {

            return new List<RouteDefinition>
            {
                GetById,
                Create,
                Edit,
                GetAllWithPagination,
                GetAllDetailsWithPagination,
            };
        }
    }
}