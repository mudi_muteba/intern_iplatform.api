using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Party.Payments;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class PaymentDetailsRoutes : IDefineRoutes<PaymentDetailsDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string partyIdPlaceHolder = "{partyId:int}";
        public string PluralRoutePrefix
        {
            get { return "/paymentdetails"; }
        }
        public string SingularRoutePrefix
        {
            get { return "/paymentdetail"; }
        }


        #region Route Definition
        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Payment Detail by Id", typeof(PaymentDetailsDto));
            }
        }


        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Payment Details documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of Payment Details", typeof(PagedResultDto<PaymentDetailsDto>));
            }
        }


        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Payment Detail search", typeof(PaymentDetailSearchDto));
            }
        }

        #endregion

        #region Links
        public void CreateLinks(PaymentDetailsDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        #endregion

        #region Connector 
        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
        #endregion

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "PaymentDetails", "The entry point for Payment Details", Get.Route); }
        }
        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {

            };
        }

    }
}