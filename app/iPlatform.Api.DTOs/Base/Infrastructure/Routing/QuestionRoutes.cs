﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class QuestionRoutes : IDefineRoutes<QuestionDefinitionDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string coverIdPlaceholder = "{coverId:int}";
        private const string productIdPlaceholder = "{productId:int}";

        public string PluralRoutePrefix
        {
            get { return "/questions"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/question"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Questions", "The entry point for Questions", Get.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Questions documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the Questions", typeof(PagedResultDto<ListBankDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Question by Id", typeof(QuestionDefinitionDto));
            }
        }

        public RouteDefinition GetByCoverId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/cover/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Question by Id", typeof(PagedResultDto<QuestionDefinitionDto>));
            }
        }
        public RouteDefinition GetByCoverIdNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/covernopagination/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Question by Id", typeof(ListResultDto<QuestionDefinitionDto>));
            }
        }

        public RouteDefinition GetByCoverIdAndProductId
        {
            get
            {

                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, coverIdPlaceholder, productIdPlaceholder),
                    "Get a Question by CoverId and QuestionId", typeof(ListResultDto<QuestionDto>));
            }
        }

        public RouteDefinition GetByCoverIdWithPagination

        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/cover/{1}/{{pageNumber:int}}/{{pageSize:int}}", SingularRoutePrefix, idPlaceholder),
                    "Get a Question by Id", typeof(PagedResultDto<QuestionDefinitionDto>));

            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the Questions with pagination", typeof(PagedResultDto<ListBankDto>));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetById,
                GetByCoverId,
                GetByCoverIdWithPagination,
                GetWithPagination,
                GetByCoverIdAndProductId
            };
        }

        public void CreateLinks(QuestionDefinitionDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateQuestionDtoLinks(QuestionDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByCoverId(int id)
        {
            var idPart = GetByCoverId.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByCoverIdNopagination(int id)
        {
            var idPart = GetByCoverIdNoPagination.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByCoverIdAndProductId(int coverId, int productId)
        {
            var idPart = GetByCoverIdAndProductId.Route.Replace(coverIdPlaceholder, coverId.ToString()).Replace(productIdPlaceholder, productId.ToString());

            return idPart;
        }
    }
}
