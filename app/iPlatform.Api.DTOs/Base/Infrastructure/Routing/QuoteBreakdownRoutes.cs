using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Proposals;
using iPlatform.Api.DTOs.QuoteBreakdown;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class QuoteBreakdownRoutes : IDefineRoutes<EditQuoteItemBreakDownDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string campaignIdPlaceholder = "{campaignid:int}";
        public string PluralRoutePrefix
        {
            get { return "/leads"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/quotebreakdown"; }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get QuoteBreakdoqn by Id", typeof(EditQuoteItemBreakDownDto));
            }
        }

        public void CreateLinks(EditQuoteItemBreakDownDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "QuoteBreakdown", "The entry point for editing Qoutebreakdown", GetById.Route); }

        }

        public RouteDefinition EditQuoteBreakdown
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/edit", SingularRoutePrefix),
                    "Save edited qoutebreakdown Id", typeof(EditQuoteItemBreakDownDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
               EditQuoteBreakdown,
               GetById
            };
        }

        public string EditBreakDownString(int id)
        {
            var idPart = EditQuoteBreakdown.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

    }
}