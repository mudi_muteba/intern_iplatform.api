using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class HomeRoutes : IDefineRoutes
    {
        public string SingularRoutePrefix
        {
            get { return "/"; }
        }
        
        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Home", "The entry point into the system", Options.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", SingularRoutePrefix),
                    "API documentation");
            }
        }

        public RouteDefinition GetSiteMap
        {
            get { return new RouteDefinition("GET", "/sitemap", "Sitemap"); }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                GetSiteMap,
                Options
            };
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public void CreateLinks(Resource resource)
        {
            throw new NotImplementedException();
        }
    }
}