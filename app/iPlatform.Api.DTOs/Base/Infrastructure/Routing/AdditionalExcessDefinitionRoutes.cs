using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class AdditionalExcessDefinitionRoutes
    {
        private const string coverIdPlaceholder = "{id:int}";
        private const string productIdPlaceholder = "{product:int}";

        public string SingularRoutePrefix
        {
            get { return String.Format("/product/{0}/cover/{1}", productIdPlaceholder, coverIdPlaceholder); }
        }

        public RouteCategoryDefinition Definition { get; set; }

        public RouteDefinition GetAdditionalExcessById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/excess/", SingularRoutePrefix),
                    "Get additional excess by Product Id and Cover Id", typeof(List<ProductAdditionalExcessDto>));
            }
        }

        public string CreateAdditionalExcessByCoverIdUrl(int productId, int coverId)
        {
            return GetAdditionalExcessById.Route
                .Replace(productIdPlaceholder, productId.ToString())
                .Replace(coverIdPlaceholder, coverId.ToString());
        }

        public void CreateLinks(ListProductDto resource)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            throw new NotImplementedException();
        }
    }
}