﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class BankRoutes : IDefineRoutes<ListBankDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return "/banks"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/bank"; }
        }

        public void CreateLinks(ListBankDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Bank by Id", typeof(ListBankDto));
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Banks documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the banks", typeof(PagedResultDto<ListBankDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the banks with pagination", typeof(PagedResultDto<ListBankDto>));
            }
        }

        public RouteDefinition ValidateBankDetails
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/validate", PluralRoutePrefix),
                    "Bank Account Validation", typeof(BankSearchDto));
            }
        }
        public RouteDefinition ValidateBankDetailsLegacy
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/validatelegacy", PluralRoutePrefix),
                    "Bank Account Validation", typeof(BankSearchDto));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Bank search", typeof(BankSearchDto));
            }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Banks", "The entry point for banks", Get.Route); }
        }





        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetWithPagination,
                GetById,
                PostSearch,
                ValidateBankDetails,
                ValidateBankDetailsLegacy
            };
        }
    }
}
