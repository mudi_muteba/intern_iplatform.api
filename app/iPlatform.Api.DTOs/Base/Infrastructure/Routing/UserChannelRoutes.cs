﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class UserChannelRoutes : IDefineRoutes<UserChannelDto>
    {
        private const string IdPlaceholder = "{userid:int}";

        public string PluralRoutePrefix
        {
            get { return "userchannels/"; }
        }

        public string SingularRoutePrefix
        {
            get { return "userchannel/"; }
        }

        public string CreateGetById(int id)
        {
            var idPart = Get.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "User channel", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix), "User channel documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "List of user channel", typeof(PagedResultDto<ListUserChannelDto>));
            }
        }

        public RouteDefinition GetByUserId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Get user channel by user id", typeof(ListUserChannelDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", SingularRoutePrefix, "Create user channel", typeof(CreateUserChannelDto));
            }
        }
        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), "Search user channel", typeof(UserChannelSearchDto));
            }
        }
        public RouteDefinition Save
        {
            get
            {
                return new RouteDefinition("POST", PluralRoutePrefix, "Save user channels", typeof(CreateUserChannelDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Post,
                Get,
                GetByUserId,
                Options,
                Save,
                Search
            };
        }

        public void CreateLinks(UserChannelDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }

        public void CreateLinks(ListUserChannelDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

    }
}
