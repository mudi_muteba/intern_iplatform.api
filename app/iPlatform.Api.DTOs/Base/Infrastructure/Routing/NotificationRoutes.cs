﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.ItcQuestion;
using iPlatform.Api.DTOs.Notifications;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class NotificationRoutes : IDefineRoutes<NotificationDto>
    {
        private const string NotificationIdPlaceholder = "{id:int}";
        private const string RecipientId = "{recipientId}";
        private const string SenderId = "{senderId}";
        private const string NotificationUserId = "{notificationUserId}";


        public string PluralRoutePrefix { get { return "/notifications"; } }

        public string SingularRoutePrefix { get { return "/notification"; } }

       
        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Notifiactions", "The entry point for iPlatform Notifications", GetById.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                var path = string.Format("{0}/", PluralRoutePrefix);
                var description = "iPlatform Notifications documentation";
                return new RouteDefinition("OPTIONS", path, description);
            }
        }

        public RouteDefinition Post
        {
            get
            {
                var path = string.Format("{0}/", SingularRoutePrefix);
                var description = "Create a new iPlatform Notification.";
                return new RouteDefinition("POST", path, description, typeof(NotificationDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                var path = string.Format("{0}/markasread/{1}", SingularRoutePrefix, NotificationUserId);
                var description = "Update an iPlatform Notification marking it as read by NotificationUserId";
                return new RouteDefinition("PUT", path, description, typeof(NotificationDto));

            }
        }

        public RouteDefinition GetById
        {
            get
            {
                var path = string.Format("{0}/{1}", SingularRoutePrefix, NotificationIdPlaceholder);
                var description = "Get iPlatform Notification by the id";
                return new RouteDefinition("GET", path, description);
            }
        }

        public RouteDefinition GetUnreadByRecipientId
        {
            get
            {
                var path = string.Format("{0}/recipient/{1}", PluralRoutePrefix, RecipientId);
                var description = "Get all unread iPlatform Notifications by the Recipients user id";
                return new RouteDefinition("GET", path, description, typeof(NotificationDto));
            }
        }

        public RouteDefinition GetAllByRecipientId
        {
            get
            {
                var path = string.Format("{0}/all/{1}", PluralRoutePrefix, RecipientId);
                var description = "Get all iPlatform Notifications by the Recipients user id";
                return new RouteDefinition("GET", path, description, typeof(NotificationDto));
            }
        }

        public RouteDefinition GetBySenderId
        {
            get
            {
                var path = string.Format("{0}/sender/{1}", PluralRoutePrefix, SenderId);
                var description = "Get all iPlatform Notifications by the Sender's User Id id";
                return new RouteDefinition("GET", path, description);
            }
        }

        public string CreateGetBySenderId(int userId)
        {
            return GetBySenderId.Route.Replace(SenderId, userId.ToString());
        }

        public string CreateGetUnreadByRecipientId(int userId)
        {
            return GetUnreadByRecipientId.Route.Replace(RecipientId, userId.ToString());
        }

        public string CreateGetAllByRecipientId(int userId)
        {
            return GetAllByRecipientId.Route.Replace(RecipientId, userId.ToString());
        }

        public string CreateMarkNotificationAsRead(int id)
        {
            return Put.Route.Replace(NotificationUserId, id.ToString());
        }

        public string CreatePost()
        {
            return Post.Route;
        }

        public void CreateLinks(NotificationDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(NotificationIdPlaceholder, id.ToString());
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Post,
                Put,
                GetById,
                GetBySenderId,
                GetUnreadByRecipientId,
                GetAllByRecipientId
            };
        }
    }
}
