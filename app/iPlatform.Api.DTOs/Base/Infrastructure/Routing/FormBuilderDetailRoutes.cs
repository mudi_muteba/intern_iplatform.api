﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.FormBuilder;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class FormBuilderDetailRoutes : IDefineRoutes<FormBuilderDetailDto>
    {

        private const string IdPlaceholder = "{id:int}";
        private const string IdFormDetailPlaceholder = "{idformdetail:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string PluralRoutePrefix
        {
            get { return "/formbuilderdetails"; }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("/formbuilderdetail/{0}", IdFormDetailPlaceholder); }
        }


        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdFormDetailPlaceholder, id.ToString());

            return idPart;
        }


        public string CreatePutById(int id, int formBuilderDetailId)
        {
            var idPart = Put.Route.Replace(IdPlaceholder, id.ToString()).Replace(IdFormDetailPlaceholder, formBuilderDetailId.ToString());

            return idPart;
        }


        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix), "Get form builder detail by Id", typeof(FormBuilderDetailDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", PluralRoutePrefix, "Create form", typeof(CreateFormBuilderDetailDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder), "Update a form builder", typeof(EditFormBuilderDetailDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/delete", SingularRoutePrefix, IdPlaceholder), "Delete a form builder");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Form Builder", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix), "Form Builder documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "List of the form builders", typeof(PagedResultDto<FormBuilderDetailDto>));
            }
        }



        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {

                GetById,
                Options,
                Post,
                Get,
                Put,
                PutDelete
            };
        }

        public void CreateLinks(FormBuilderDetailDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }
    }
}
