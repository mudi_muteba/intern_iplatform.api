﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.HistoryLoss.LossHistory
{
    public class LossHistoryRoutes : IDefineRoutes<LossHistoryDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string SingularRoutePrefix
        {
            get { return "/losshistory"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/losshistories"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Loss History documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "LossHistory", "The entry point for LossHistory", GetByPartyId.Route);
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Loss History entry by Id", typeof(LossHistoryDto));
            }
        }

        public RouteDefinition GetByPartyId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/byparty/{1}", PluralRoutePrefix, idPlaceholder),
                    "Get Loss History by partyId", typeof(ListResultDto<LossHistoryDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a new loss history entry", typeof(CreateLossHistoryDto));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search loss history by search DTO", typeof(LossHistorySearchDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, idPlaceholder),
                    "Update a LossHistory Entry by Id", typeof(EditLossHistoryDto));
            }
        }

        public RouteDefinition PutDisable
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                                    "Disable a LossHistory entry");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetByPartyId,
                PostSearch,
                Post,
                Put,
                PutDisable
            };
        }

        public void CreateLinks(LossHistoryDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByPartyId(int id)
        {
            var idPart = GetByPartyId.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string PutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string DisableById(int id)
        {
            var idPart = PutDisable.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
    }
}
