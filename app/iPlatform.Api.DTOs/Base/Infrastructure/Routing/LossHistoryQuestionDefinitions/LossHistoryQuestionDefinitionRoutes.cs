﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.LossHistoryQuestionDefinitions;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.LossHistoryQuestionDefinitions
{
    public class LossHistoryQuestionDefinitionRoutes : IDefineRoutes<LossHistoryQuestionDefinitionDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string SingularRoutePrefix
        {
            get { return "/LossHistoryQuestionDefinition"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/LossHistoryQuestionDefinitions"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Loss history question definitions documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "LossHistoryquestiondefinitions", "The entry point for Loss history question definitions", GetById.Route);
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a loss history question definition entry by Id", typeof(LossHistoryQuestionDefinitionDto));
            }
        }

        public RouteDefinition PostSearchQuestions
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search Loss History Question Definitions by LossHistoryQuestionDefinitionSearchDto", typeof(LossHistoryQuestionDefinitionSearchDto));
            }
        }

        public RouteDefinition PostSearchQuestion
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", SingularRoutePrefix),
                    "Search Loss History Question Definition by LossHistoryQuestionDefinitionSearchDto", typeof(LossHistoryQuestionDefinitionSearchDto));
            }
        }

        public RouteDefinition PostSearchQuestionWithAnswers
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/Answers/search", PluralRoutePrefix),
                    "Search Loss History Question Definition by LossHistoryQuestionDefinitionSearchDto", typeof(LossHistoryQuestionDefinitionSearchDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                PostSearchQuestions,
                PostSearchQuestion,
                PostSearchQuestionWithAnswers
            };
        }

        public void CreateLinks(LossHistoryQuestionDefinitionDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

    }
}
