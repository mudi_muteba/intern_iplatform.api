﻿﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using System.Collections.Generic;
﻿using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class CustomerSatisfactionSurveyRoutes : IDefineRoutes<CustomerSatisfactionSurveyDto>
    {
        private const string IdPlaceholder = "{customersatisfactionId:int}";


        public string PluralRoutePrefix
        {
            get { return "/customersatisfactionsurveys"; }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("customersatisfactionsurvey/{0}", IdPlaceholder); }
        }



        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "CustomerSatisfactionSurveys", "The entry point for Customer Satisfaction Surveys", Get.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("proposaldefinitionquestions/", PluralRoutePrefix),
                    "Customer Satisfaction Surveys documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get an ProposalDefinitionQuestions by proposalDefinition Id", typeof(CustomerSatisfactionSurveyDto));
            }
        }

        public RouteDefinition Save
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}", PluralRoutePrefix),
                    "Save Customer SatisfactionSurvey from lightstone", typeof(CreateCustomerSatisfactionSurveyDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/post", PluralRoutePrefix),
                    "Post a request for survey to lightstone", typeof(CustomerSatisfactionSurveyRequestDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                Save,
                Post
            };
        }

        public string CreateGetById(int customersatisfactionId)
        {
            string idpart = Get.Route.Replace(IdPlaceholder, customersatisfactionId.ToString());
            return idpart;
        }

        


        public string GetLinks(CustomerSatisfactionSurveyDto resource)
        {
            return CreateGetById(resource.Id);
        }

        public void CreateLinks(CustomerSatisfactionSurveyDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
    }
}