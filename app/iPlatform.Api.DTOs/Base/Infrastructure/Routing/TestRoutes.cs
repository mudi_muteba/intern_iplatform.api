using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class TestRoutes : IDefineRoutes
    {
        public string SingularRoutePrefix
        {
            get { return "/test"; }
        }

        public string CreateGetById(int id)
        {
            return string.Empty;
        }
        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", SingularRoutePrefix),
                    "Authentication documentation");
            }
        }

        public RouteCategoryDefinition Definition {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Testing", "Testing. Does nothing", Options.Route);
            }
        }

        public RouteDefinition Get
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", SingularRoutePrefix), "Testing route"); }
        }

        public RouteDefinition Failure
        {
            get { return new RouteDefinition("GET", string.Format("{0}/fail", SingularRoutePrefix), "Failure testing route"); }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Get,
                Failure
            };
        }
    }
}