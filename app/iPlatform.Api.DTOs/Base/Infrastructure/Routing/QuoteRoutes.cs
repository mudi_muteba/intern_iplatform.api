using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class QuoteRoutes : IDefineRoutes
    {
        private const string externalReferencePlaceholder = "{externalReference}";
        private const string quoteIdPlaceholder = "{quoteId}";
        private const string campaignIdPlaceholder = "{campaignId}";
        public string PluralRoutePrefix
        {
            get { return "quotes"; }
        }

        public string SingluarRoutePrefix
        {
            get { return "quote"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "Quote", "The entry point for Quotes",
                    Get.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Quotes documentation");
            }
        }

        public RouteDefinition Get
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "List of quotes"); }
        }

        public RouteDefinition GetRating
        {
            get
            {
                return new RouteDefinition("GET", PluralRoutePrefix, "Get ratings for the allocated products",
                    typeof(RatingRequestDto));
            }
        }

        public RouteDefinition Accept
        {
            get
            {
                var path = string.Format("{0}/accept", PluralRoutePrefix);

                return new RouteDefinition("POST", path, "Accepts a quote",
                    typeof(PublishQuoteUploadMessageDto));
            }
        }

        public RouteDefinition AcceptExternalQuote
        {
            get
            {
                var path = string.Format("{0}/acceptexternalquote", PluralRoutePrefix);

                return new RouteDefinition("POST", path, "Accepts a quote",
                    typeof(RatingResultDto));
            }
        }

        public RouteDefinition AcceptQuote
        {
            get
            {
                var path = string.Format("{0}/{1}/accept", SingluarRoutePrefix, quoteIdPlaceholder);

                return new RouteDefinition("POST", path, "Accepts a quote by Id");
            }
        }
        public RouteDefinition DeleteQuoteAccept
        {
            get
            {
                var path = string.Format("{0}/{1}/deletequoteaccept", SingluarRoutePrefix, quoteIdPlaceholder);

                return new RouteDefinition("POST", path, "Delete QuoteAcceptedLeadActivty");
            }
        }

        public RouteDefinition DeletePolicyBindingCompleted
        {
            get
            {
                var path = string.Format("{0}/{1}/deletepolicybindingcompleted", SingluarRoutePrefix, quoteIdPlaceholder);

                return new RouteDefinition("POST", path, "Delete DeletePolicyBindingCompleted Lead Activity");
            }
        }
        public RouteDefinition LogQuoteUploadSuccess
        {
            get
            {
                var path = string.Format("{0}/{1}/quoteupload", SingluarRoutePrefix, quoteIdPlaceholder);

                return new RouteDefinition("POST", path, "quote upload is a success", typeof(PublishQuoteUploadMessageDto));
            }
        }

        public RouteDefinition GetQuoteUploadLog
        {
            get
            {
                var path = string.Format("{0}/{1}/quoteupload", SingluarRoutePrefix, quoteIdPlaceholder);

                return new RouteDefinition("GET", path, "get quote upload log");
            }
        }

        public RouteDefinition IntentToBuyQuote
        {
            get
            {
                var path = string.Format("{0}/{1}/intenttobuy", SingluarRoutePrefix, quoteIdPlaceholder);

                return new RouteDefinition("POST", path, "Accepts a quote by Id");
            }
        }

        public RouteDefinition Create
        {
            get
            {
                var path = string.Format("{0}", PluralRoutePrefix);

                return new RouteDefinition("POST", path, "Creates a quote",
                    typeof(PublishQuoteUploadMessageDto));
            }
        }

        public RouteDefinition GetByExternalReference
        {
            get
            {
                var path = string.Format("{0}/{1}", SingluarRoutePrefix, externalReferencePlaceholder);
                return new RouteDefinition("GET", path, "Gets quote by external reference",
                    typeof(QuoteHeaderDto));
            }
        }

        public RouteDefinition DistributeQuoteUsingExternalReference
        {
            get
            {
                var path = string.Format("{0}/{1}/distribute", SingluarRoutePrefix, externalReferencePlaceholder);

                return new RouteDefinition("POST", path, "Distributes a quote for an external reference",
                    typeof(DistributeQuoteDto));
            }
        }

        public RouteDefinition TrackQuoteDistribution
        {
            get
            {
                var path = string.Format("{0}/{1}/track-distribution", SingluarRoutePrefix, quoteIdPlaceholder);

                return new RouteDefinition("POST", path, "Tracks the distribution of quotes",
                    typeof(TrackQuoteDistributionDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Get,
                GetRating,
                Accept,
                AcceptQuote,
                IntentToBuyQuote,
                Create,
                GetByExternalReference,
                DistributeQuoteUsingExternalReference,
                TrackQuoteDistribution,
                LogQuoteUploadSuccess,
                GetQuoteUploadLog,
                DeleteQuoteAccept,
                AcceptExternalQuote,
                DeletePolicyBindingCompleted

            };
        }

        public string CreateGetById(int id)
        {
            return "NA";
        }

        public string CreateGetById(QuoteAcceptanceResponseDto result)
        {
            return GetRating.Route;
        }

        public string CreateCreateQuote(RatingResultDto ratingResult)
        {
            return Create.Route;
        }

        public string CreateAcceptQuote(string quoteId)
        {
            return AcceptQuote.Route.Replace(quoteIdPlaceholder, quoteId);
        }

        public string CreateDeleteQuoteAccept(string quoteId)
        {
            return DeleteQuoteAccept.Route.Replace(quoteIdPlaceholder, quoteId);
        }

        public string CreateDeletePolicyBindingCompleted(string quoteId)
        {
            return DeletePolicyBindingCompleted.Route.Replace(quoteIdPlaceholder, quoteId);
        }

        public string CreateIntentToBuyQuote(string quoteId)
        {
            return AcceptQuote.Route.Replace(quoteIdPlaceholder, quoteId);
        }

        public string CreateLogQuoteUploadSuccess(string quoteId)
        {
            return LogQuoteUploadSuccess.Route.Replace(quoteIdPlaceholder, quoteId);
        }

        public string CreateRatingRequestUrl()
        {
            return GetRating.Route;
        }

        public string CreateGetByExternalReferenceUrl(string externalReference)
        {
            return GetByExternalReference.Route
                .Replace(externalReferencePlaceholder, externalReference);
        }

        public string CreateGetQuoteUploadLog(string quoteId)
        {
            return GetQuoteUploadLog.Route
                .Replace(quoteIdPlaceholder, quoteId);
        }
    }
}