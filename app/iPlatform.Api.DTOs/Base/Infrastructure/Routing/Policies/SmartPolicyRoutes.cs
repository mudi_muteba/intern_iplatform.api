using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Policy;
using iPlatform.Api.DTOs.Policy.FNOL;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Policies
{
    public class SmartPolicyRoutes : IDefineRoutes<PolicyHeaderDto>
    {

        private const string GetPolicy = "GetPolicy";
        private const string RegisterPolicy = "RegisterPolicy";
        private const string SendPdfPolicy = "SendPdfPolicy";
        private const string UpdatePolicy = "UpdatePolicy";
        private const string GetPdfUrl = "GetPDFUrl";


        public string SingularRoutePrefix
        {
            get { return "/smartpolicy"; }
        }
        #region PolicyHeaders
        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", SingularRoutePrefix),
                    "Policy headers documentation");
            }
        }
        public RouteDefinition GetSmartPolicy
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}", SingularRoutePrefix, GetPolicy),
                    "Returns all the policy's details");
            }
        }

        public RouteDefinition GetSmartPolicyPdf
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}", SingularRoutePrefix, GetPdfUrl),
                   "Returns the pdf url for the policy");
            }
        }

        public RouteDefinition RegisterSmartPolicy
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}", SingularRoutePrefix, RegisterPolicy),
                   "Register a new policy with the policy's details");
            }
        }
        public RouteDefinition UpdateSmartPolicy 
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}", SingularRoutePrefix, UpdatePolicy),
                  "Register a new policy with the policy's new details");
            }
        }

        public RouteDefinition SendPdfSmartPolicy
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}", SingularRoutePrefix, SendPdfPolicy),
                 "Sends all the policies");
            }
        }


        #endregion
    

      
        #region Misc

        public void CreateLinks(PolicyHeaderDto resource)
        {
            throw new System.NotImplementedException();
        }

        public string CreateGetById(int id)
        {
            throw new System.NotImplementedException();
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "SMART Policies ", "The entry point for SMART policies", GetSmartPolicy.Route); }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
             
            };
        }
        #endregion
    }
}