using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Policy;
using iPlatform.Api.DTOs.Policy.FNOL;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Policies
{
    public class PolicyHeaderRoutes : IDefineRoutes<PolicyHeaderDto>
    {
        private const string IdPlaceholder = "{id:int}";
        private const string partyIdPlaceholder = "{partyId:int}";
        private const string insurerCodePlaceholder = "{insurerCode:string}";
        private const string idNumberPlaceholder = "{idNumber:string}";
        private const string mobileNumberPlaceholder = "{mobileNumber:string}";
        private const string includeItemsPlaceholder = "{includeItems:bool}";
        private const string dateOfLossPlaceHolder = "{dateOfLoss:string}";
        private const string iPlatformIdPlaceholder = "{iplatformid:string}";
        public string PluralRoutePrefix
        {
            get { return "/policyheaders"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/policyheader"; }
        }
        #region PolicyHeaders
        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Policy headers documentation");
            }
        }
        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the Policies", typeof(PagedResultDto<PolicyHeaderDto>));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "List of the Policies by PartyId", typeof(PagedResultDto<PolicyHeaderDto>));
            }
        }

        public RouteDefinition SearchWithoutPagination
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/searchnopagination/", PluralRoutePrefix),
                    "Get Policies by filters", typeof(ListResultDto<PolicyHeaderDto>));

            }
        }
        public RouteDefinition GetByPartyIdWithPagination 
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix, partyIdPlaceholder),
                    "List of the Policies with pagination", typeof(PagedResultDto<PolicyHeaderDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the Policies with pagination", typeof(PagedResultDto<PolicyHeaderDto>));
            }
        }


        public RouteDefinition GetByFilters
        {
            get
            {
                //"policies/{insurerCode}/{idNumber}/{mobileNumber}/{includeItems}"
                return new RouteDefinition("GET", string.Format("{0}/filter/", PluralRoutePrefix),
                    "Get Policies by filters", typeof(PolicyHeaderDto));

            }
        }
        
        public RouteDefinition GetScheduledDocuments
        {
            get
            {
                //"policyschedules/{insurerCode}/{idNumber}/{mobileNumber}"
                return new RouteDefinition("GET", string.Format("{0}/schedules/", PluralRoutePrefix),
                    "Get Policies schedule documents", typeof(PagedResultDto<PolicyHeaderDto>));
            }
        }
        public RouteDefinition Getfnol
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/fnol", PluralRoutePrefix, partyIdPlaceholder),
                    "Get Policies FNOL by Party Id", typeof(ListResultDto<PolicyHeaderDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create PolicyHeader", typeof(CreatePolicyHeaderDto));
            }
        }
        public RouteDefinition PostQuoteAccepted
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/quoteaccepted", PluralRoutePrefix),
                    "Create Policy when quote is accepted", typeof(CreatePolicyHeaderDto));
            }
        }

        public string CreateGetFNOLByPartyId(int partyid)
        {
            var idPart = Getfnol.Route.Replace(partyIdPlaceholder, partyid.ToString());

            return idPart;
        }


        #endregion
        #region PolicyHeader
        public string CreateGetById(int id)
        {
            var idPart = GetByPolicyId.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public RouteDefinition GetByPolicyId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Get a Policy by Id", typeof(PolicyHeaderDto));
            }
        }

        public RouteDefinition GetByPartyAndPolicyIds
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", SingularRoutePrefix, IdPlaceholder, partyIdPlaceholder),
                    "Get a Policy by Id & Party Id", typeof(PolicyHeaderDto));
            }
        }

        public RouteDefinition GetDataForFNOL
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/fnoldata", SingularRoutePrefix),
                    "Get a PolicyData for FNOL", typeof(GetPolicyDataForFNOLDto));
            }
        }

        public RouteDefinition GetAvailableItemsForFNOL
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/availablefnolitems", SingularRoutePrefix),
                    "Get a Policy avaliable items for FNOL", typeof(GetPolicyAvailableItemsForFNOLDto));
            }
        }

        public RouteDefinition UpdateByQuoteId
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}", SingularRoutePrefix),
                    "Update policy header by quote id", typeof(EditPolicyHeaderDto));
            }
        }

        #endregion
        #region Misc
        public void CreateLinks(PolicyHeaderDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(BasicPolicyHeaderDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }


        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "PolicyHeaders", "The entry point for policy headers", Get.Route); }
        }
        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetWithPagination,
                Post,
                GetByPolicyId,
                GetDataForFNOL,
                Getfnol,
                GetDataForFNOL,
                GetAvailableItemsForFNOL,
                UpdateByQuoteId,
                PostQuoteAccepted
            };
        }
        #endregion
    }
}