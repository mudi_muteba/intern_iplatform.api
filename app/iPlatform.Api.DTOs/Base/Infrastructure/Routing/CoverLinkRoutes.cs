using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.CoverDefinition;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.CoverLinks;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class CoverLinkRoutes : IDefineRoutes<ListCoverLinkDto>
    {
        private const string channelIdPlaceholder = "{channelId:int}";
        private const string idPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";
        public string PluralRoutePrefix
        {
            get { return "/coverLinks"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/coverLink"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Cover Links Documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Cover Links", "The entry point for cover links", GetByChannelId.Route); }
        }

        public RouteDefinition GetByChannelId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", PluralRoutePrefix, channelIdPlaceholder),
                    "Get a Cover Links by ChannelId", typeof(ListCoverLinkDto));
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "List of the Cover Link", typeof(ListResultDto<ListCoverLinkDto>));
            }
        }
        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix),
                    "List of the Cover Link", typeof(ListResultDto<ListCoverLinkDto>));
            }
        }
        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder),
                    "List of the Cover Link with pagination", typeof(PagedResultDto<ListCoverLinkDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Cover Link by Id", typeof(ListCoverLinkDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a Cover Link", typeof(CreateCoverLinkDto));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search a Cover Link", typeof(ListResultDto<ListCoverLinkDto>));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Update a Cover Linkby Id", typeof(EditCoverLinkDto));
            }
        }

        public RouteDefinition Delete
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                    "Disable a Cover Link by Id", typeof(DeleteCoverLinkDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetByChannelId,
                GetById,
                GetWithPagination,
                GetNoPagination,
                Get,
                Post,
                Search,
                Put,
                Delete
            };
        }

        public void CreateLinks(ListCoverLinkDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.ChannelId));
        }


        public string CreateGetById(int channelId)
        {
            var idPart = GetByChannelId.Route.Replace(channelIdPlaceholder, channelId.ToString());

            return idPart;
        }

    }
}