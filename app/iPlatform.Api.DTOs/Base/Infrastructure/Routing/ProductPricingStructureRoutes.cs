﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.ProductPricingStructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ProductPricingStructureRoutes : IDefineRoutes<ListProductPricingStructureDto>
    {
        private const string m_IdPlaceholder = "{id:int}";
        private const string m_ChannelIdPlaceholder = "{channelId:int}";
        private const string m_ProductIdPlaceholder = "{productId:int}";
        private const string m_CoverIdPlaceholder = "{coverId:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("/ProductPricingStructures"); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("/ProductPricingStructure"); }
        }

        public void CreateLinks(ListProductPricingStructureDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(ProductPricingStructureDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        #region [ Create URL's ]

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(m_IdPlaceholder, id.ToString());
        }

        public string CreateGetByChannelIdProductIdAndCoverId(int channelId, int productId, int coverId)
        {
            return GetByChannelIdProductIdAndCoverId.Route
                .Replace(m_ChannelIdPlaceholder, channelId.ToString())
                .Replace(m_ProductIdPlaceholder, productId.ToString())
                .Replace(m_CoverIdPlaceholder, coverId.ToString());
        }

        #endregion

        #region [ Route Definitions ]

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Options,
                GetById,
                GetByChannelIdProductIdAndCoverId
            };
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}", SingularRoutePrefix),
                    "ProductPricingStructure documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "ProductPricingStructure", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, m_IdPlaceholder),
                    "Get a Product Pricing Structure ");
            }
        }

        public RouteDefinition GetByChannelIdProductIdAndCoverId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}/{3}", SingularRoutePrefix, m_ChannelIdPlaceholder, m_ProductIdPlaceholder, m_CoverIdPlaceholder),
                    "Get a Product Pricing Structure by Channel, Product and Cover");
            }
        }

        #endregion
    }
}
