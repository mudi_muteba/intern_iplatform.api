using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.Bank;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using System;
using iPlatform.Api.DTOs.Party.Payments;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Individual
{
    public class IndividualPaymentDetailsRoute : IDefineRoutes<PaymentDetailsDto>
    {
        private const string individualIdPlaceholder = "{individualId:int}";
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("individual/{0}/paymentdetails", individualIdPlaceholder); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("/individual/{0}/paymentdetail/{1}", individualIdPlaceholder, idPlaceholder); }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "Payment Details", "The entry point for Payment Details",
                    GetById.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("individual/paymentdetails", PluralRoutePrefix),
                    "Payment Details documentation");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get a Payment Detail by Id", typeof(PaymentDetailsDto));
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get an Payment Details by PartyId", typeof(PagedResultDto<PaymentDetailsDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create Payment Detail", typeof(CreatePaymentDetailsDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}", SingularRoutePrefix),
                    "Edit Payment Detail", typeof(EditPaymentDetailsDto));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", "paymentdetails/search",
                    "Search Payment Details");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                Get,
                Post,
                Put,
                Search,

            };
        }

        public string CreateGetById(int id)
        {
            return PluralRoutePrefix
                .Replace(individualIdPlaceholder, id.ToString());
        }

        public string CreateGetById(int partyId, int id)
        {
            return SingularRoutePrefix
                .Replace(individualIdPlaceholder, partyId.ToString())
                .Replace(idPlaceholder, id.ToString());
        }

        public void CreateLinks(PaymentDetailsDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.PartyId, resource.Id));
        }

    }
}