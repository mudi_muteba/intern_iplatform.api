using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Individual
{
    public class IndividualAssetVehicleRoutes : IDefineRoutes<AssetVehicleDto>
    {
        private const string individualIdPlaceholder = "{individualId:int}";
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("individual/{0}/AssetVehicles", individualIdPlaceholder); }
        }


        public string SingularRoutePrefix
        {
            get { return string.Format("/individual/{0}/AssetVehicle/{1}", individualIdPlaceholder, idPlaceholder); }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "IndividualsAsset", "The entry point for individual Assets",
                    GetById.Route);
            }
        }
        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("individual/AssetVehicles", PluralRoutePrefix),
                    "Individuals Asset");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get an AssetVehicle by Id", typeof (AssetVehicleDto));
            }
        }

        public RouteDefinition GetAssetVehicles
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get an AssetVehicle by Id", typeof (ListAssetVehicleDto));
            }
        }

        public RouteDefinition PostCreateAssetVehicle
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create Bank Details");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetAssetVehicles,
                PostCreateAssetVehicle
            };
        }

        public string CreateGetById(int id)
        {
            return PluralRoutePrefix
                .Replace(individualIdPlaceholder, id.ToString());
        }

        public void CreateLinks(AssetVehicleDto resource)
        {
        }

        public string CreateGetById(int individualId, int AssetVehicleId)
        {
            return SingularRoutePrefix
                .Replace(individualIdPlaceholder, individualId.ToString())
                .Replace(idPlaceholder, AssetVehicleId.ToString());
        }

        public void CreateLinks(ListAssetVehicleDto resource)
        {
        }
    }
}