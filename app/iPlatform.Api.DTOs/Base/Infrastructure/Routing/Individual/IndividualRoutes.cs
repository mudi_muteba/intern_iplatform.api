using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Individual;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Individual
{
    public class IndividualRoutes :  IDefineRoutes<IndividualDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return "/individuals"; }
        }
        public string Search
        {
            get { return "/Search"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/individual"; }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Individual by Id", typeof(IndividualDto));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/", PluralRoutePrefix,Search),
                    "Search individuals", typeof(IndividualSearchDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create an individual", typeof(CreateIndividualDto));
            }
        }
        
        public RouteDefinition ProposalPost
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", SingularRoutePrefix),
                    "Create a new individual", typeof(CreateIndividualDto));
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Individuals Documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Individuals", "The entry point for individuals", GetById.Route); }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                Post,
                PostSearch,
            };
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
        public void CreateLinks(IndividualDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
        public void CreateLinks(ListIndividualDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
    }
}