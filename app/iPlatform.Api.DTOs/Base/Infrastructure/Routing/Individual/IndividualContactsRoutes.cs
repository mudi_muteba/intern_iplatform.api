using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.Contacts;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Individual
{
    public class IndividualContactsRoutes : IDefineRoutes<ContactDto>
    {
        private const string individualIdPlaceholder = "{individualId:int}";
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("individual/{0}/Contacts", individualIdPlaceholder); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("/individual/{0}/Contact/{1}", individualIdPlaceholder, idPlaceholder); }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "Contacts", "The entry point for Contacts",
                    GetById.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("individual/Contacts", PluralRoutePrefix),
                    "Contact documentation");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get an Contact by Id", typeof(ContactDto));
            }
        }

        public RouteDefinition GetContacts
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get an Contacts by partyId", typeof(ListContactDto));
            }
        }

        public RouteDefinition PostCreateContact
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create Contact");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetContacts,
                PostCreateContact
            };
        }

        public string CreateGetById(int id)
        {
            return PluralRoutePrefix
                .Replace(individualIdPlaceholder, id.ToString());
        }

        public void CreateLinks(ContactDto resource)
        {
        }

        public string CreateGetById(int individualId, int contactId)
        {
            return SingularRoutePrefix
                .Replace(individualIdPlaceholder, individualId.ToString())
                .Replace(idPlaceholder, contactId.ToString());
        }

        public void CreateLinks(ListContactDto resource)
        {
        }
    }
}