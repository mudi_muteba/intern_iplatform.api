using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.Address;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Individual
{
    public class IndividualAddressRoutes : IDefineRoutes<AddressDto>
    {
        private const string individualIdPlaceholder = "{individualId:int}";
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("individual/{0}/addresses", individualIdPlaceholder); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("/individual/{0}/address/{1}", individualIdPlaceholder, idPlaceholder); }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "IndividualsAddresses", "The entry point for individual Addresses",
                    GetById.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("individual/addresses", PluralRoutePrefix),
                    "Individuals Addresses");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get an Address by Id", typeof (AddressDto));
            }
        }

        public RouteDefinition GetAddresses
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get all address for an individual", typeof(ListAddressDto));
            }
        }

        public RouteDefinition PostCreateAddress
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create an address", typeof (CreateAddressDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetAddresses,
                PostCreateAddress
            };
        }

        public string CreateGetById(int id)
        {
            return PluralRoutePrefix
                .Replace(individualIdPlaceholder, id.ToString());
        }

        public void CreateLinks(AddressDto resource)
        {
        }

        public string CreateGetById(int individualId, int addressId)
        {
            return SingularRoutePrefix
                .Replace(individualIdPlaceholder, individualId.ToString())
                .Replace(idPlaceholder, addressId.ToString());
        }

        public void CreateLinks(ListAddressDto resource)
        {
        }

        public void CreateLinks(Products.ListProductByCoverDto c)
        {
        }
    }
}