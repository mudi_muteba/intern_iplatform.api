using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.Bank;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Individual
{
    public class IndividualBankDetailRoutes : IDefineRoutes<BankDetailsDto>
    {
        private const string individualIdPlaceholder = "{individualId:int}";
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("individual/{0}/BankDetails", individualIdPlaceholder); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("/individual/{0}/BankDetail/{1}", individualIdPlaceholder, idPlaceholder); }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "IndividualBankDetails", "The entry point for individual Bank Details",
                    GetById.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("individual/BankDetails", PluralRoutePrefix),
                    "Bank Details documentation");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get an BankDetail by Id", typeof (BankDetailsDto));
            }
        }

        public RouteDefinition GetBankDetails
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get an BankDetail by Id", typeof (ListBankDetailsDto));
            }
        }

        public RouteDefinition PostCreateBankDetail
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create Bank Details");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetBankDetails,
                PostCreateBankDetail
            };
        }

        public string CreateGetById(int id)
        {
            return PluralRoutePrefix
                .Replace(individualIdPlaceholder, id.ToString());
        }

        public void CreateLinks(BankDetailsDto resource)
        {
        }

        public string CreateGetById(int individualId, int BankDetailId)
        {
            return SingularRoutePrefix
                .Replace(individualIdPlaceholder, individualId.ToString())
                .Replace(idPlaceholder, BankDetailId.ToString());
        }

        public void CreateLinks(ListBankDetailsDto resource)
        {
        }
    }
}