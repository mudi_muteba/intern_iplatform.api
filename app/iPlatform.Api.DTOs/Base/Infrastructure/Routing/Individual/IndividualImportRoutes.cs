using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Individual;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Individual
{
    public class IndividualImportRoutes : IDefineRoutes<ImportIndividualDto>
    {

        public string SingularRoutePrefix
        {
            get { return string.Format("/import/individual"); }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}", SingularRoutePrefix),
                    "Import an Individual", typeof (ImportIndividualDto));
            }
        }


        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format(SingularRoutePrefix),
                    "Individual import documentation");
            }
        }


        public string CreateGetById(int id = 0)
        {
            return SingularRoutePrefix;
        }
        public string CreateGetById(Guid id)
        {
            return SingularRoutePrefix;
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("POST", "ImportIndividual", "The entry point for import individual",
                    Post.Route);
            }
        }


        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Post,
            };
        }

        public void CreateLinks(ImportIndividualDto resource)
        {
        }
    }
}