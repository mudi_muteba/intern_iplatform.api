using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Ratings.Quoting;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Individual
{
    public class IndividualProposalHeadersRoutes : IDefineRoutes<ProposalHeaderDto>
    {
        private const string IndividualIdPlaceholder = "{IndividualId:int}";
        private const string IdPlaceholder = "{id:int}";
        private const string ExternalReferencePlaceholder = "{externalReference}";

        public string PluralRoutePrefix
        {
            get { return string.Format("Individual/{0}/ProposalHeaders", IndividualIdPlaceholder); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("/Individual/{0}/ProposalHeader/{1}", IndividualIdPlaceholder, IdPlaceholder); }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "IndividualProposalHeaders", "The entry point for Individual ProposalHeaders",
                    GetById.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("Individual/ProposalHeaders", PluralRoutePrefix),
                    "Individual ProposalHeaders documentation");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get a ProposalHeader by Id", typeof (ProposalHeaderDto));
            }
        }

        public RouteDefinition GetDetailedById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("/Individual/{0}/ProposalHeaderDetailed/{1}", IndividualIdPlaceholder, IdPlaceholder),
                    "Get a ProposalHeader by Id", typeof(ProposalHeaderDto));
            }
        }

        public RouteDefinition GetQuoteByProposalId
        {
            get
            {
                return new RouteDefinition("POST", "Individual/ProposalHeader/Quote",
                    "Create a Quote by PoposalHeader Id and Individual Id", typeof (CreateQuoteDto));
            }
        }

        public RouteDefinition GetByExternalReference
        {
            get
            {
                return new RouteDefinition("GET", string.Format("proposal/{0}", ExternalReferencePlaceholder),
                    "Get a ProposalHeader by external reference", typeof (ProposalHeaderDto));
            }
        }

        public RouteDefinition GetContainsVehilceType
        {
            get
            {
                return new RouteDefinition("POST", string.Format("Individual/{0}/proposal/{1}/ContainsVehilceType", IndividualIdPlaceholder, IdPlaceholder),
                    "POST check if proposal contain vehicle type", typeof (ContainsVehicleTypesDto));
            }
        }

        public RouteDefinition GetProposalHeaders
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get a ProposalHeader by Id", typeof (ListProposalHeaderDto));
            }
        }

        public RouteDefinition PostCreateProposalHeader
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a proposal header",typeof(CreateProposalHeaderDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetById,
                GetDetailedById,
                GetQuoteByProposalId,
                GetByExternalReference,
                GetProposalHeaders,
                PostCreateProposalHeader,
            };
        }

        public string CreateGetById(int id)
        {
            return PluralRoutePrefix
                .Replace(IndividualIdPlaceholder, id.ToString());
        }

        public void CreateLinks(ProposalHeaderDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.PartyId, resource.Id));
        }

        public string CreateLinks(int partyId, int proposalId)
        {
            return SingularRoutePrefix
                      .Replace(IndividualIdPlaceholder, partyId.ToString())
                      .Replace(IdPlaceholder, proposalId.ToString());
        }
        public void CreateLinks(ProposalHeaderDetailedDto resource)
        {
            resource.AddLink("self", CreateGetDetailedById(resource.PartyId, resource.Id));
        }

        public string CreateGetById(int individualId, int proposalHeaderId)
        {
            return SingularRoutePrefix
                .Replace(IndividualIdPlaceholder, individualId.ToString())
                .Replace(IdPlaceholder, proposalHeaderId.ToString());
        }

        public string GetContainsVehilceTypeById(int individualId, int proposalHeaderId)
        {
            return GetContainsVehilceType.Route
                .Replace(IndividualIdPlaceholder, individualId.ToString())
                .Replace(IdPlaceholder, proposalHeaderId.ToString());
        }
        public string CreateGetDetailedById(int individualId, int proposalHeaderId)
        {
            return GetDetailedById.Route
                .Replace(IndividualIdPlaceholder, individualId.ToString())
                .Replace(IdPlaceholder, proposalHeaderId.ToString());
        }

        public void CreateLinks(ListProposalHeaderDto resource)
        {
        }

        public string CreateLinks(DeleteProposalDefinitionResponseDto response)
        {
            return CreateGetById(response.PartyId, response.ProposalHeaderId);
        }

        public string CreateGetByExternalReferenceUrl(string externalReference)
        {
            return GetByExternalReference.Route
                .Replace(ExternalReferencePlaceholder, externalReference);
        }
    }
}