using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.Bank;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using System;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Individual
{
    public class IndividualCorrespondencePreferenceRoutes : IDefineRoutes<PartyCorrespondencePreferenceDto>
    {
        private const string individualIdPlaceholder = "{individualId:int}";
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("individual/{0}/correspondencepreference", individualIdPlaceholder); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("/individual/{0}/correspondencepreference/{1}", individualIdPlaceholder, idPlaceholder); }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "CorrespondencePreferences", "The entry point for Correspondence Preferences",
                    GetById.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("individual/correspondencepreferences", PluralRoutePrefix),
                    "Correspondence Preferences documentation");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get an Correspondence Preference by Id", typeof(PartyCorrespondencePreferenceDto));
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get an Correspondence Preference by Id", typeof(PartyCorrespondencePreferenceDto));
            }
        }

        public RouteDefinition PostCorrespondencePreference
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create Correspondence Preferences");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                Get,
                PostCorrespondencePreference
            };
        }

        public string CreateGetById(int id)
        {
            return PluralRoutePrefix
                .Replace(individualIdPlaceholder, id.ToString());
        }

        public string CreateGetById(int partyId, int id)
        {
            return SingularRoutePrefix
                .Replace(individualIdPlaceholder, partyId.ToString())
                .Replace(idPlaceholder, id.ToString());
        }

        public void CreateLinks(PartyCorrespondencePreferenceDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.PartyId, resource.Id));
        }

    }
}