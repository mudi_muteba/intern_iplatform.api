using System.Collections.Generic;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.Correspondence;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Individual
{
    public class IndividualCorrespondenceRoutes : IDefineRoutes<PartyCorrespondenceDto>
    {
        private const string individualIdPlaceholder = "{individualId:int}";
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("individual/{0}/correspondences", individualIdPlaceholder); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("/individual/{0}/correspondence/{1}", individualIdPlaceholder, idPlaceholder); }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "Correspondence", "The entry point for Correspondence", GetById.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("individual/correspondence", PluralRoutePrefix),
                    "Correspondence documentation");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get an Correspondence Preference by Id", typeof(PartyCorrespondenceDto));
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get an Correspondence Preference by Id", typeof(ListPartyCorrespondenceDto));
            }
        }

        public RouteDefinition PostCorrespondence
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create Correspondence");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                Get,
                PostCorrespondence
            };
        }

        public string CreateGetById(int id)
        {
            return PluralRoutePrefix
                .Replace(individualIdPlaceholder, id.ToString());
        }

        public string CreateGetById(int partyId, int id)
        {
            return SingularRoutePrefix
                .Replace(individualIdPlaceholder, partyId.ToString())
                .Replace(idPlaceholder, id.ToString());
        }

        public void CreateLinks(PartyCorrespondenceDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id, resource.Id));
        }
    }
}