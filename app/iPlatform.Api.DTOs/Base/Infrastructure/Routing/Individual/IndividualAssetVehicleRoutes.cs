using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Individual
{
    public class IndividualAssetRiskItemRoutes : IDefineRoutes<AssetRiskItemDto>
    {
        private const string individualIdPlaceholder = "{individualId:int}";
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("individual/{0}/AssetRiskItems", individualIdPlaceholder); }
        }


        public string SingularRoutePrefix
        {
            get { return string.Format("/individual/{0}/AssetRiskItem/{1}", individualIdPlaceholder, idPlaceholder); }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "IndividualAssetRiskItems", "The entry point for individual Asset Risk Items",
                    GetById.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("individual/AssetRiskItems", PluralRoutePrefix),
                    "individual Asset Risk Items Documentation");
            }
        }     

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get an AssetRiskItem by Id", typeof(AssetRiskItemDto));
            }
        }

        public RouteDefinition GetAssetRiskItems
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get an AssetRiskItem by Id", typeof(ListAssetRiskItemDto));
            }
        }

        public RouteDefinition PostCreateAssetRiskItem
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create an AssetRiskItem", typeof(CreateAssetRiskItemDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetAssetRiskItems,
                PostCreateAssetRiskItem
            };
        }

        public string CreateGetById(int id)
        {
            return PluralRoutePrefix
                .Replace(individualIdPlaceholder, id.ToString());
        }

        public void CreateLinks(AssetRiskItemDto resource)
        {
        }

        public string CreateGetById(int individualId, int AssetRiskItemId)
        {
            return SingularRoutePrefix
                .Replace(individualIdPlaceholder, individualId.ToString())
                .Replace(idPlaceholder, AssetRiskItemId.ToString());
        }

        public void CreateLinks(ListAssetRiskItemDto resource)
        {
        }
    }
}