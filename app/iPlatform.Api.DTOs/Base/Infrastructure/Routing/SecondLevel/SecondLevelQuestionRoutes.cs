﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.SecondLevel
{
    public class SecondLevelQuestionRoutes
    {
        private readonly SecondLevelRoutes _secondLevelRoutes;

        public SecondLevelQuestionRoutes(SecondLevelRoutes secondLevelRoutes)
        {
            _secondLevelRoutes = secondLevelRoutes;
        }

        public string CreateGetQuestionsByQuoteId(int quoteId)
        {
            return GetForQuoteId.Route.Replace(SecondLevelRouteConstants.QuoteIdPlaceholder, quoteId.ToString());
        }

        public RouteDefinition GetForQuoteId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/questions/{1}", _secondLevelRoutes.RoutePrefix, SecondLevelRouteConstants.QuoteIdPlaceholder),
                    "Gets Second Level Questions for Quote");
            }
        }
    }
}
