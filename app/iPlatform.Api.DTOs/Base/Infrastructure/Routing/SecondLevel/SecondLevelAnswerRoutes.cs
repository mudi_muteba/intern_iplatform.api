﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.SecondLevel.Answers;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.SecondLevel
{
    public class SecondLevelAnswerRoutes
    {
        private readonly SecondLevelRoutes _secondLevelRoutes;

        public SecondLevelAnswerRoutes(SecondLevelRoutes secondLevelRoutes)
        {
            _secondLevelRoutes = secondLevelRoutes;
        }

        public string CreateSaveAnswersForQuoteId(int quoteId)
        {
            return SaveForQuote.Route.Replace(SecondLevelRouteConstants.QuoteIdPlaceholder, quoteId.ToString());
        }

        public RouteDefinition SaveForQuote
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/answers/save/{1}", _secondLevelRoutes.RoutePrefix, SecondLevelRouteConstants.QuoteIdPlaceholder),
                    "Save Answers From Second Level Questions", typeof(SecondLevelQuestionsAnswersSaveDto));
            }
        }
    }
}
