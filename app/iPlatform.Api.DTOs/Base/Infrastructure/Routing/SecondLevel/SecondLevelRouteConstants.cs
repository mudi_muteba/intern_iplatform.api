﻿namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.SecondLevel
{
    public class SecondLevelRouteConstants
    {
        public static readonly string QuoteIdPlaceholder = "{quoteId}";
    }
}
