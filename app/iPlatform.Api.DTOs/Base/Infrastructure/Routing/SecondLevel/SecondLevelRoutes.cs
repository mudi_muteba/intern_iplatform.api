﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.SecondLevel
{
    public class SecondLevelRoutes : IDefineRoutes
    {
        public SecondLevelRoutes()
        {
            Questions = new SecondLevelQuestionRoutes(this);
            Answers = new SecondLevelAnswerRoutes(this);
        }

        public string RoutePrefix
        {
            get { return "/secondlevel"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("Options", "SecondlevelQuestionsAnswers", "The entry point for Secondlevel Questions/Answers",
                    Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", RoutePrefix), "Secondlevel Questions/Answers Documentation");
            }
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Questions.GetForQuoteId,
                Answers.SaveForQuote
            };
        }

        public SecondLevelQuestionRoutes Questions { get; private set; }
        public SecondLevelAnswerRoutes Answers { get; private set; }
    }
}
