﻿using System;
using System.Collections.Generic;

using iPlatform.Api.DTOs.Admin.ChannelTemplates;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ChannelTemplateRoutes : IDefineRoutes<ChannelTemplateDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string systemIdPlaceHolder = "{systemId:int}";

        public string PluralRoutePrefix
        {
            get { return "/ChannelTemplates"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/ChannelTemplate"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Channel Templates", "The entry point for Channel Templates", Get.Route); }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/get/all", PluralRoutePrefix),
                    "List of Channel Templates", typeof(PagedResultDto<ChannelTemplateDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/get/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Channel Template by Id", typeof(ChannelTemplateDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/createmultiple", PluralRoutePrefix),
                    "Create multiple Channel Templates", typeof(CreateMultipleChannelTemplatesDto));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search/page", PluralRoutePrefix),
                    "Search Channel Templates Using Pagination", typeof(SearchChannelTemplatesDto));
            }
        }

        public RouteDefinition SearchNoPagination
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search Channel Templates Using No Pagination", typeof(SearchChannelTemplatesDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetById,
                Post,
                Search,
                SearchNoPagination
            };
        }

        public void CreateLinks(ChannelTemplateDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
            resource.AddLink("self", CreateGetBySystemGuid(resource.SystemId));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetBySystemGuid(Guid systemGuid)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, systemGuid.ToString());

            return idPart;
        }
    }
}
