﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Components
{
    public class ComponentQuestionAnswerRoutes : IDefineRoutes<ComponentQuestionAnswerDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string SingularRoutePrefix
        {
            get { return "/ComponentQuestionAnswer"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/ComponentQuestionAnswers"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Component Question Answer Documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "ComponentQuestionAnswer", "3rd tier of components containing routes for CRUD operations on Component Question Answer", GetById.Route);
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/id/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Component Question Answer entry by Id", typeof(ComponentQuestionAnswerDto));
            }
        }


        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", SingularRoutePrefix),
                    "Create a new Component Question Answer entry", typeof(CreateComponentQuestionAnswerDto));
            }
        }

        public RouteDefinition PostMultiple
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Creates a new Component Question Answer per CreateComponentQuestionAnswerDto node", typeof(SaveComponentQuestionAnswersDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, idPlaceholder),
                    "Update a Component Question Answer", typeof(EditComponentQuestionAnswerDto));
            }
        }

        public RouteDefinition PutDisable
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                                    "Disable a Component Answer");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                Post,
                PostMultiple,
                Put,
                PutDisable
            };
        }

        public void CreateLinks(ComponentQuestionAnswerDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateComponentSaveQuestionAnswersById()
        {
            return PostMultiple.Route;
        }

        public string CreateComponentSaveQuestionAnswerById()
        {
            return Post.Route;
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string PutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string DisableById(int id)
        {
            var idPart = PutDisable.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
    }
}
