﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Components.ComponentHeaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Components
{
    public class ComponentHeaderRoutes : IDefineRoutes<ComponentHeaderDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string SingularRoutePrefix
        {
            get { return "/ComponentHeader"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/ComponentHeaders"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Component Header Documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "ComponentHeader", "1st tier of components containing routes for CRUD operations on component Headers", GetById.Route);
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/id/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Component Header entry by Id", typeof(ComponentHeaderDto));
            }
        }


        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a new Component Header entry", typeof(CreateComponentHeaderDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, idPlaceholder),
                    "Update a Component Header", typeof(EditComponentHeaderDto));
            }
        }

        public RouteDefinition PutDisable
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                                    "Disable a Component Header");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                Post,
                Put,
                PutDisable
            };
        }

        public void CreateLinks(ComponentHeaderDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string PutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string DisableById(int id)
        {
            var idPart = PutDisable.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
    }
}
