﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Components.ComponentDefinitions;
using iPlatform.Api.DTOs.Components.ComponentDefinitions.Questions;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Components
{
    public class ComponentDefinitionRoutes : IDefineRoutes<ComponentDefinitionDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string SingularRoutePrefix
        {
            get { return "/ComponentDefinition"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/ComponentDefinitions"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Component Definition Documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "ComponentDefinition", "2nd tier of components containing routes for CRUD operations on component Definition", GetById.Route);
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/id/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Component Definition entry by Id", typeof(ComponentDefinitionDto));
            }
        }

        public RouteDefinition GetComponentDefinitionQuestions
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/componentquestiondefinitions/id/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Component Definition Questions ", typeof(ComponentDefinitionDto));
            }
        }


        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a new Component Definition entry", typeof(CreateComponentDefinitionDto));
            }
        }

        public RouteDefinition SearchDefinitionsAnswers
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/searchdefinitionanswers", PluralRoutePrefix),
                    "Returns Component Question Definitions with saved answers searched by component criteria", typeof(SearchComponentDefinitionAnswersDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, idPlaceholder),
                    "Update a Component Definition", typeof(EditComponentDefinitionDto));
            }
        }

        public RouteDefinition PutDisable
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                                    "Disable a Component Definition");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetComponentDefinitionQuestions,
                Post,
                SearchDefinitionsAnswers,
                Put,
                PutDisable
            };
        }

        public void CreateLinks(ComponentDefinitionDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string PutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string GetLinks(ComponentDefinitionQuestionsDto resource)
        {
            return CreateGetById(resource.Id);
        }

        public string DisableById(int id)
        {
            var idPart = PutDisable.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
    }
}
