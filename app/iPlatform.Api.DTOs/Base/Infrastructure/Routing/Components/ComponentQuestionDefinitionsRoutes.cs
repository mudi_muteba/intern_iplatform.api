﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;
using System.Collections.Generic;
using System;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Components
{
    public class ComponentQuestionDefinitionsRoutes : IDefineRoutes<ComponentQuestionDefinitionDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string SingularRoutePrefix
        {
            get { return "/ComponentQuestionDefinition"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/ComponentQuestionDefinitions"; }
        }
        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Component Question Definition Documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "ComponentQuestionDefinition", "Contains all the routes for the component question definitions", SearchDefinitions.Route);
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/id/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Component Question Definition entry by Id", typeof(ComponentQuestionDefinitionDto));
            }
        }


        public RouteDefinition SearchDefinitions
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/searchdefinitions", PluralRoutePrefix),
                    "Returns Component Question Definitions with all possible answers per question", typeof(SearchComponentDefinitionsDto));
            }
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                SearchDefinitions
            };
        }

        public void CreateLinks(SearchComponentQuestionDefinitionAnswerResultDto c)
        {
            throw new NotImplementedException();
        }

        public void CreateLinks(ComponentQuestionDefinitionDto resource)
        {
            throw new NotImplementedException();
        }
    }
}
