﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.LossHistoryDefinitions;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.LossHistoryDefinitions
{
    public class LossHistoryDefinitionsRoutes : IDefineRoutes<LossHistoryDefinitionDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string SingularRoutePrefix
        {
            get { return "/LossHistoryDefinition"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/LossHistoryDefinitions"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Loss history definitions Loss  documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "LossHistorydefinitions", "The entry point for Loss history definitions", GetByPartyId.Route);
            }
        }

        public RouteDefinition GetByPartyId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get Loss history definitions by partyId", typeof(ListResultDto<LossHistoryDefinitionDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/id/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a loss history definition entry by Id", typeof(LossHistoryDefinitionDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a new loss history definition entry", typeof(CreateLossHistoryDefinitionDto));
            }
        }

        public RouteDefinition PutDisable
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                                    "Disable a loss history definition entry");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetByPartyId,
                Post,
                PutDisable
            };
        }

        public void CreateLinks(LossHistoryDefinitionDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByPartyId(int id)
        {
            var idPart = GetByPartyId.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string DisableById(int id)
        {
            var idPart = PutDisable.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
    }
}
