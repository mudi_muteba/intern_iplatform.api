using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.ProposalDefinition;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ProposalDefinitionsRoutes : IDefineRoutes<ProposalDefinitionDto>
    {
        private const string ProposalHeaderIdPlaceholder = "{proposalHeaderId:int}";
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("ProposalHeader/{0}/ProposalDefinitions", ProposalHeaderIdPlaceholder); }
        }

        public string SingularRoutePrefix
        {
            get
            {
                return string.Format("/ProposalHeader/{0}/ProposalDefinition/{1}", ProposalHeaderIdPlaceholder,
                    idPlaceholder);
            }
        }


        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "ProposalDefinition", "The entry point for ProposalDefinition",
                    GetById.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS",
                    string.Format("ProposalHeader/ProposalDefinitions", PluralRoutePrefix),
                    "ProposalDefinition documentation");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}", SingularRoutePrefix),
                    "Save a ProposalDefinition", typeof(ProposalDefinitionDto));
            }
        }

        public RouteDefinition GetProposalDefinitions
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get an ProposalDefinition by Id", typeof(ListProposalDefinitionDto));
            }
        }

        public RouteDefinition PostCreateProposalDefinition
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a proposal Definition", typeof(CreateProposalDefinitionDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}", SingularRoutePrefix),
                    "Edit a ProposalDefinition", typeof(EditProposalDefinitionDto));
            }
        }


        public RouteDefinition Delete
        {
            get
            {
                return new RouteDefinition("Delete", string.Format("{0}", SingularRoutePrefix),
                    "Delete a ProposalDefinition", typeof(DeleteProposalDefinitionDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetProposalDefinitions,
                PostCreateProposalDefinition,
                Put,
                Delete
            };
        }

        public string CreateGetById(int id)
        {
            return GetProposalDefinitions.Route.Replace(ProposalHeaderIdPlaceholder, id.ToString());
        }

        public void CreateLinks(ProposalDefinitionDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id, resource.ProposalHeaderId));
        }

        public string CreateGetById(int proposalHeaderId, int proposalDefinitionId)
        {
            return SingularRoutePrefix
                .Replace(ProposalHeaderIdPlaceholder, proposalHeaderId.ToString())
                .Replace(idPlaceholder, proposalDefinitionId.ToString());
        }

        public void CreateLinks(ListProposalDefinitionDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.ProposalHeaderId, resource.Id));
        }
    }
}