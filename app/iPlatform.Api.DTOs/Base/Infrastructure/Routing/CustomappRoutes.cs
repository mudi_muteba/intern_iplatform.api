using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.CustomApp;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class CustomappRoutes : IDefineRoutes
    {
        public string SingularRoutePrefix
        {
            get { return "Customapp"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Customapp", "The entry point for customapp calls", Options.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", SingularRoutePrefix),
                    "API documentation");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Options
            };
        }

        public RouteDefinition Register
        {
            get
            {
                return new RouteDefinition("POST", String.Format("{0}/Register", SingularRoutePrefix),
                    "Find a matching member", typeof(List<RegisterDto>));
            }
        }

        public RouteDefinition VerifyMember
        {
            get
            {
                return new RouteDefinition("POST", String.Format("{0}/VerifyMember", SingularRoutePrefix),
                    "Verify member is still an active policyholder with provider", typeof(List<VerifyMemberDto>));
            }
        }

        public RouteDefinition GetPolicyInfo
        {
            get
            {
                return new RouteDefinition("POST", String.Format("{0}/PolicyInfo", SingularRoutePrefix), 
                    "Get policy and risk information", typeof(List<GetPolicyInfoDto>));
            }
        }

        public RouteDefinition GetMemberDetails
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/MemberDetails", SingularRoutePrefix),
                    "Get MemberDetails", typeof(List<GetMemberDetailsDto>));
            }
        }

        public RouteDefinition UpdateMemberDetails
        {
            get
            {
                return new RouteDefinition("POST", String.Format("{0}/UpdateMemberDetails", SingularRoutePrefix),
                    "Update the member details", typeof(List<UpdateMemberDetailsDto>));
            }
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public void CreateLinks(Resource resource)
        {
            throw new NotImplementedException();
        }
    }
}