﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.CoverDefinition;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class CoverDefinitionsRoutes : IDefineRoutes<CoverDefinitionDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string PluralRoutePrefix
        {
            get { return "/coverdefinitions"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/coverdefinition"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "CoverDefinitions", "The entry point for Channel Permissions", Get.Route); }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "List of the Channel Events", typeof(ListResultDto<CoverDefinitionDto>));
            }
        }
        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix),
                    "List of the Channel Events", typeof(ListResultDto<CoverDefinitionDto>));
            }
        }
        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder),
                    "List of the channels with pagination", typeof(PagedResultDto<CoverDefinitionDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Channel Event by Id", typeof(CoverDefinitionDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Update a Channel Event by Id", typeof(EditCoverDefinitionDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a Channel Event", typeof(CreateCoverDefinitionDto));
            }
        }

        public RouteDefinition Delete
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                    "Disable a Channel Permission by Id", typeof(DeleteCoverDefinitionDto));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search a Channel Permission", typeof(ListResultDto<CoverDefinitionDto>));
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Channels documentation");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetById,
                Post,
                Search,
                Delete,
                Put,
                GetNoPagination,
                GetWithPagination
            };
        }

        public void CreateLinks(CoverDefinitionDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateDeleteById(int id)
        {
            var idPart = Delete.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
  
        public string CreateGetWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString());
        }
    }
}
