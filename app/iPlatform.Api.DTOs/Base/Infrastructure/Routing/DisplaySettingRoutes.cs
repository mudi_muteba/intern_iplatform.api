﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class DisplaySettingRoutes : IDefineRoutes<DisplaySettingDto>
    {
        private const string IdPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string SingularRoutePrefix
        {
            get { return "displaySetting"; }
        }

        public string PluralRoutePrefix
        {
            get { return "displaySettings"; }
        }

        public void CreateLinks(DisplaySettingDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(ListDisplaySettingDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        #region [ Create URL's ]

        public string CreateGetById(int id)
        {
            string url = GetById.Route.Replace(IdPlaceholder, id.ToString());
            return url;
        }

        public string CreateGetWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString());
        }

        public string CreatePutById(int id)
        {
            string url = Put.Route.Replace(IdPlaceholder, id.ToString());
            return url;
        }

        public string CreateDeleteById(int id)
        {
            string url = PutDelete.Route.Replace(IdPlaceholder, id.ToString());
            return url;
        }

        #endregion

        #region [ Route Definitions ]

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Options,
                Post,
                Get,
                GetWithPagination,
                GetById,
                Put,
                PutDelete
            };
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "DisplaySetting Documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "DisplaySetting", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", SingularRoutePrefix,
                    "Create DisplaySetting", typeof(CreateDisplaySettingDto));
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", String.Format("{0}/", PluralRoutePrefix), "List of DisplaySettings",
                    typeof(ListResultDto<ListDisplaySettingDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder),
                    "List of DisplaySettings", typeof(PagedResultDto<ListDisplaySettingDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", String.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Get DisplaySetting by Id", typeof(DisplaySettingDto));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), "Search DisplaySetting", typeof(PagedResultDto<ListDisplaySettingDto>));
            }
        }

        public RouteDefinition GetRequiresThirdPartyIntegration
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/requiresThirdParty", PluralRoutePrefix), "Get DisplaySettings that require third party integration", typeof(ListResultDto<ListDisplaySettingDto>));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Update a DisplaySetting", typeof(EditDisplaySettingDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/delete", SingularRoutePrefix, IdPlaceholder),
                    "Delete an DisplaySetting by Id", typeof(DeleteDisplaySettingDto));
            }
        }

        #endregion
    }
}
