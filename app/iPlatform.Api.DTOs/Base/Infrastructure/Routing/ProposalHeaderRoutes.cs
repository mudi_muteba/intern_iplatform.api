using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Proposals;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ProposalHeaderRoutes : IDefineRoutes<ProposalHeaderDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string reQuotePlaceholder = "{reQuote:bool}";
        private const string channelIdPlaceholer = "{channelId:int}";
        public string PluralRoutePrefix
        {
            get { return "/proposalheaders"; }
        }

        public string SingularRoutePrefix
        {
            get
            {
                return string.Format("/proposalheader/{0}", idPlaceholder);
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "ProposalHeaders", "The entry point for ProposalHeaders",
                    GetById.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix), "ProposalHeaders documentation");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get an ProposalDefinition by Id", typeof(ProposalDefinitionDto));
            }
        }

        public RouteDefinition GetRatingsById
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/GetQuote", SingularRoutePrefix),
                    "Get an Quotes by proposal header Id", typeof(GetQuotesByProposalDto));
            }
        }

        public RouteDefinition EditShouldQuote
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/EditShouldQuote", SingularRoutePrefix),
                    "Updates the should quote property of proposal header", typeof(EditProposalHeaderShouldQuoteDto));
            }
        }

        public RouteDefinition EditValidateProposal
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/EditValidateProposal", SingularRoutePrefix),
                    "Updates the should quote property of proposal header", typeof(EditProposalHeaderValidateProposalDto));
            }
        }

        public RouteDefinition ImportProposals
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/import", PluralRoutePrefix),
                    "Import lead Proposals", typeof(SubmitLeadDto));
            }
        }


        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetRatingsById,
                ImportProposals,
                EditShouldQuote,
                EditValidateProposal
            };
        }

        public string CreateGetById(int id)
        {
            return SingularRoutePrefix.Replace(idPlaceholder, id.ToString());
        }

        public string CreateGetRatingsById(GetQuotesByProposalDto dto)
        {
            return GetRatingsById.Route
                .Replace(idPlaceholder, dto.Id.ToString());
        }

        public string EditProposalHeaderShouldQuote(int id)
        {
            var idPart = EditShouldQuote.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string EditProposalHeaderValidateProposal(int id)
        {
            var idPart = EditValidateProposal.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }


        public void CreateLinks(ProposalHeaderDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

    }
}