using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.WorkflowRetries;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class WorkflowRetryLogRoutes : IDefineRoutes<ListWorkflowRetryLogDto>
    {
        private const string IdPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return "/WorkflowRetryLogs"; }
        }
        public string Search
        {
            get { return "/Search"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/WorkflowRetryLog"; }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Get a workflow retry log entry by Id", typeof(IndividualDto));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/", PluralRoutePrefix,Search),
                    "Search workflow retry log entries", typeof(SearchWorkflowRetryLogDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create an workflow retry log entry", typeof(CreateIndividualDto));
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Workflow retries documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "WorkFlowRetryLogs", "The entry point for workflow retry logs", GetById.Route); }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                Post,
                PostSearch,
            };
        }

        public void CreateLinks(ListWorkflowRetryLogDto resource)
        {
            throw new System.NotImplementedException();
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public void CreateLinks(ListIndividualDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
    }
}