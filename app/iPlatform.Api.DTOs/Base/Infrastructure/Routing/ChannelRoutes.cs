﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Admin;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ChannelRoutes : IDefineRoutes<ListChannelDto>
    {
        private const string idPlaceholder = "{id:int}";
        public string PluralRoutePrefix
        {
            get { return "/channels"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/channel"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Channels", "The entry point for Channels", Get.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Channels documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the Channels", typeof(PagedResultDto<ListChannelDto>));
            }
        }
        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search List of the Channels", typeof(PagedResultDto<ListChannelDto>));
            }
        }

        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/all", PluralRoutePrefix),
                    "List of the Channels", typeof(ListResultDto<ListBankDto>));
            }
        }

        public RouteDefinition GetAllWithNoVehicleGuideSetting
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/AllWithNoVehicleGuideSetting", PluralRoutePrefix),
                    "List of the Channels that do not have a vehicle guide setting configured", typeof(ListResultDto<ListChannelDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Channel by Id", typeof(ListChannelDto));
            }
        }
        public RouteDefinition PublishChannelNotification
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/publishemail", SingularRoutePrefix, idPlaceholder),
                    "Send a Notification for new channel being published.", typeof(ChannelPublishingNotificationDto));
            }
        }

        public RouteDefinition PublishAgencyApplicationNotification
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/publishemailagencyapplication", SingularRoutePrefix, idPlaceholder),
                    "Send a Notification for agency application approved.", typeof(PublishingAgencyApplicationNotificationDto));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the channels with pagination", typeof(PagedResultDto<ListBankDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a new channel", typeof(CreateChannelDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, idPlaceholder),
                    "Update a channel", typeof(EditChannelDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetWithPagination,
                GetById,
                Post,
                Put,
                GetNoPagination,
                Search,
                GetAllWithNoVehicleGuideSetting
            };
        }

        public void CreateLinks(ListChannelDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePublishChannelNotification(int id)
        {
            var idPart = PublishChannelNotification.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePublishAgencyApplicationNotification(int id)
        {
            var idPart = PublishAgencyApplicationNotification.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
    }
}
