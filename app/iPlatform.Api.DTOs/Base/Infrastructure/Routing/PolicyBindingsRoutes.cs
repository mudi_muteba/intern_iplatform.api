using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.PolicyBindings;
using iPlatform.Api.DTOs.Reports.PolicyBinding.Criteria;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class PolicyBindingsRoutes : IDefineRoutes<PolicyBindingListDto>, IDefineRoutes<PolicyBindingDto>
    {
        private const string IdPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return "/policybindings"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/policybinding"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Policy Binding", "The entry point for Policy Bindings", Get.Route); }
        }
        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix), "Policy Bindings documentation");
            }
        }
        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "List of the Policy Bindings", typeof(PagedResultDto<PolicyBindingListDto>));
            }
        }
        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder), "Get a Policy Binding by Id", typeof(PolicyBindingDto));
            }
        }

        public RouteDefinition GetByQuoteId
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/quoteid", PluralRoutePrefix), "Get a Policy Binding first asset by Quote Id", typeof(GetPolicyBindingByQuoteDto));
            }
        }

        public RouteDefinition ValidateConfiguration
        {
            get
            {
                return new RouteDefinition("Post", string.Format("{0}/validate", PluralRoutePrefix), "Validate if configuration exists");
            }
        }

        public RouteDefinition GetNavigation
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/navigation", PluralRoutePrefix), "Get Navigation by Quote Id & Channel Id", typeof(GetPolicyBindingNavigationDto));
            }
        }

        public RouteDefinition GetFirstAsset
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/firstasset", PluralRoutePrefix), "Get a Policy Binding first asset by Quote Id", typeof(GetPolicyBindingFirstAssetDto));
            }
        }
        public RouteDefinition Save
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix), "Save Policy Binding", typeof(SavePolicyBindingDto));
            }
        }
        public RouteDefinition SaveAnswers
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/saveanswers", PluralRoutePrefix), "Save Policy Binding", typeof(SavePolicyBindingAnswerDto));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the Policy Bindings with pagination", typeof(PagedResultDto<PolicyBindingListDto>));
            }
        }
        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), "Policy Bindings search", typeof(SearchPolicyBindingDto));
            }
        }
        public RouteDefinition Update
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, IdPlaceholder), "Update a campaign", typeof(UpdatePolicyBindingDto));
            }
        }

        public RouteDefinition Submit
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/submit", PluralRoutePrefix), "submit policy binding", typeof(SubmitPolicyBindingDto));
            }
        }

        public RouteDefinition GetPolicyBindingReport
        {
            get
            {
                return new RouteDefinition("POST", string.Concat(SingularRoutePrefix, "/report"), "Get Policy Binding Report", typeof(PolicyBindingReportCriteriaDto));
            }
        }

        public RouteDefinition PolicyBindingNotification
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/sendnotificationemail", SingularRoutePrefix),
                    "Send a Notification for sucessfull binding with PDF report.", typeof(PolicyBindingNotificationDto));
            }
        }


        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetById,
                Search,
                GetWithPagination,
                Save,
                Update,
                SaveAnswers,
                GetNavigation,
                ValidateConfiguration,
                Submit,
                GetPolicyBindingReport,
                PolicyBindingNotification
            };
        }
        

        public void CreateLinks(PolicyBindingListDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
        public void CreateLinks(PolicyBindingDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
            resource.AddLink("update", CreateUpdateById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateUpdateById(int id)
        {
            var idPart = Update.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }
    }
}