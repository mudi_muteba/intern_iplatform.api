using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ProductsRoutes : IDefineRoutes<ListProductDto>, IDefineRoutes<CampaignDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string channelIdPlaceholder = "{channelId:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";
        private const string codePlaceholder = "{code}";
        private const string coverIdPlaceholder = "{coverIdLint}";

        private CoverDefinitionRoutes coverRoutes = new CoverDefinitionRoutes();
        private ProductFeeDefinitionRoutes feeRoutes = new ProductFeeDefinitionRoutes();

        private AdditionalExcessDefinitionRoutes additionalExcessRoutes = new AdditionalExcessDefinitionRoutes();

        public string PluralRoutePrefix

        {
            get { return "/products"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/product"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Products", "The entry point for products", Get.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Products Documentation");
            }
        }

        public RouteDefinition GetProducts
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/list", PluralRoutePrefix), "Get a list of products", typeof(List<AllocatedProductDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a product by Id", typeof(AllocatedProductDto));
            }
        }

        public RouteDefinition GetByCode
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, codePlaceholder),
                    "Get a product by Id", typeof(AllocatedProductDto));
            }
        }

        public RouteDefinition ProposalForm
        {
            get
            {
                return new RouteDefinition("GET",
                    string.Format("{0}/{1}/proposalform", SingularRoutePrefix, idPlaceholder),
                    "Get proposal form for product", typeof(AllocatedProductDto));
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of products", typeof(PagedResultDto<AllocatedProductDto>));
            }
        }

        public RouteDefinition GetProductsByCover
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", PluralRoutePrefix, channelIdPlaceholder),
                    "List of products", typeof(PagedResultDto<AllocatedProductDto>));
            }
        }

        public RouteDefinition GetAll
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "Get a list of allocated products");
            }
        }
        public RouteDefinition GetAllForReporting
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/reporting", PluralRoutePrefix),
                    "Get a list of products that are set to show in reporting");
            }
        }

        public RouteDefinition GetAllWithPagination
        {
            get
            {
                return new RouteDefinition("GET",
                    string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder),
                    "List of users with pagination", typeof(PagedResultDto<ListProductDto>));
            }
        }

        public RouteDefinition GetAllNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/all", PluralRoutePrefix),
                    "Get a list of allocated products");
            }
        }

        public RouteDefinition GetAllocatedProduct
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/allocatedproducts/{1}", PluralRoutePrefix, idPlaceholder),
                    "Get a list of allocated products by ChannelId");
            }
        }
        public RouteDefinition GetAllAllocatedProduct
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/allocatedproducts", PluralRoutePrefix),
                    "Get a list of all allocated products with channel Ids");
            }
        }
        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a new campaign", typeof(CreateCampaignDto));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Campaign search", typeof(CampaignSearchDto));
            }
        }

        public RouteDefinition Edit
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, idPlaceholder),
                    "Update a campaign", typeof(EditCampaignDto));
            }
        }

        public RouteDefinition Create
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/", SingularRoutePrefix, "createproductfee"),
                    "Create product fee", typeof(CreateProductFeeDto));
            }
        }

        public RouteDefinition EditProductFee
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", "/editproductfee", idPlaceholder),
                    "Edit product fee", typeof(EditProductFeeDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/delete", "/productfee", idPlaceholder), "Delete a product fee");
            }
        }

        public string DisableById(int id)
        {
            var idPart = PutDelete.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string GetProductFeebyId(int productFeeId)
        {
            var idPart = GetProductFeeById.Route.Replace(idPlaceholder, productFeeId.ToString());

            return idPart;
        }

        public string CreateEditProductFeeById(int id)
        {
            var idPart = EditProductFee.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public RouteDefinition CoversById
        {
            get { return coverRoutes.GetById; }
        }

        public RouteDefinition CoversByProductId
        {
            get { return coverRoutes.GetByProductId; }
        }

        public RouteDefinition Fees
        {
            get { return feeRoutes.Get; }
        }

        public RouteDefinition FeesById
        {
            get { return feeRoutes.GetById; }
        }

        public RouteDefinition FeesByChannelId
        {
            get { return feeRoutes.GetByChannelId; }
        }

        public RouteDefinition FeesByIdChannelId
        {
            get { return feeRoutes.GetByIdChannelId; }
        }

        public RouteDefinition GetAllFees
        {
            get { return feeRoutes.GetAll; }
        }

        public RouteDefinition SearchFees
        {
            get { return feeRoutes.Search; }
        }

        public RouteDefinition GetProductFeeById
        {
            get { return feeRoutes.GetProductFeeById; }
        }

        public RouteDefinition BenefitsByCoverId
        {
            get { return coverRoutes.GetBenefitsById; }
        }

        public RouteDefinition AdditionalExcessByCoverId
        {
            get { return additionalExcessRoutes.GetAdditionalExcessById; }
        }
        public string CreateGetAllocatedByChannelId(int channelid)
        {
            return GetAllocatedProduct.Route
                .Replace(idPlaceholder, channelid.ToString())
                ;
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetById,
                GetByCode,
                ProposalForm,
                CoversById,
                CoversByProductId,
                Fees,
                FeesById,
                BenefitsByCoverId,
                AdditionalExcessByCoverId,
                Get,
                GetProductsByCover,
                GetAll,
                GetAllWithPagination,
                Post,
                Search,
                Edit,
                GetProducts,         
                Create,
                EditProductFee,
                PutDelete,
                GetAllNoPagination,
                GetAllAllocatedProduct,
                GetAllocatedProduct,
                GetAllForReporting
            };
        }

        public void CreateLinks(CampaignDto resource)
        {
            throw new NotImplementedException();
        }

        public void CreateLinks(ListProductFeeDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(ListProductDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
            SystemRoutes.Organisations.CreateLinks(resource.ProductOwner);
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string GetProductsByCoverUrl(int id)
        {
            var idPart = GetProductsByCover.Route.Replace(channelIdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateCoversByIdUrl(int productId, int coverId)
        {
            return coverRoutes.CreateCoversByIdUrl(productId, coverId);
        }

        public string CreateFeesUrl(int productId)
        {
            return feeRoutes.CreateFeesUrl(productId);
        }

        public string CreateFeesByChannelIdUrl(int productId, int channelId)
        {
            return feeRoutes.CreateFeesByChannelIdUrl(productId, channelId);
        }

        public string CreateFeeByIdUrl(int productId, int feeId)
        {
            return feeRoutes.CreatefeesByIdUrl(productId, feeId);
        }
    
        public string CreateFeeByIdChannelIdUrl(int productId, int feeId, int channelId)
        {
            return feeRoutes.CreatefeesByIdChannelIdUrl(productId, feeId, channelId);
        }

        public string CreateBenefitsByCoverIdUrl(int productId, int coverId)
        {
            return coverRoutes.CreateBenefitsByCoverIdUrl(productId, coverId);
        }

        public string CreateProposalFormUrl(int productId)
        {
            return ProposalForm.Route.Replace(idPlaceholder, productId.ToString());
        }

        public void CreateLinks(CoverDefinitionDto c)
        {
            coverRoutes.CreateLinks(c);
        }

        public void CreateLinks(int productid, List<ListCoverDefinitionDto> covers)
        {
            foreach (var cover in covers)
            {
                coverRoutes.CreateLinks(productid, cover);
            }
        }

        public void CreateLinks(ListAllocatedProductDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
            SystemRoutes.Organisations.CreateLinks(resource.ProductOwner);
            SystemRoutes.Organisations.CreateLinks(resource.ProductProvider);
        }

        public void CreateLinks(ProductInfoDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(ProductFeeDto resource)
        {
            feeRoutes.CreateLinks(resource);
        }

        public string CreateEdit(int id)
        {
            var idPart = Edit.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetAllWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetAllWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public string CreateGetAllFeesWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetAllFees.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public void CreateLinks(AllocatedProductDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
            SystemRoutes.Organisations.CreateLinks(resource.ProductOwner);
            SystemRoutes.Organisations.CreateLinks(resource.ProductProvider);

            foreach (var cover in resource.Covers)
            {
                coverRoutes.CreateLinks(resource.Id, cover);
            }
        }

        public void CreateLinks(ProposalFormDto resource)
        {
            CreateLinks(resource.Product);
        }

        public string CreateGetByCode(string productCode)
        {
            return GetByCode.Route
                .Replace(codePlaceholder, productCode);
        }
    }
}