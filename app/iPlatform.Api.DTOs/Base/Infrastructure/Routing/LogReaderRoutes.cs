using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Logs;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class LogReaderRoutes : IDefineRoutes
    {
        public string RoutePrefix
        {
            get { return "logs"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("POST", "Logs", "The entry point for retreiving system logs",
                    Post.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", RoutePrefix), "Logs reader Documentation");
            }
        }

        public RouteDefinition Post
        {
            get { return new RouteDefinition("Post", string.Format("{0}/GetApiLogs", RoutePrefix), "List of logs per file and log type", typeof(GetLogsDto)); }
        }



        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Post,
             };
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public string CreatePost()
        {
            return Post.Route;
        }

    }
}