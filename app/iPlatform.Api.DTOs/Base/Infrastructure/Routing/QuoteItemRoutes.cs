﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Quotes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class QuoteItemRoutes : IDefineRoutes<EditQuoteItemDto>
    {
        private const string m_IdPlaceholder = "{id:int}";
        private const string m_UserIdPlaceholder = "{userId:int}";
        private const string m_pageNumberPlaceHolder = "{pageNumber:int}";
        private const string m_pageSizePlaceHolder = "{pageSize:int}";

        public string SingularRoutePrefix
        {
            get { return "quoteItem"; }
        }

        public string PluralRoutePrefix
        {
            get { return "quoteItems"; }
        }

        public void CreateLinks(EditQuoteItemDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        #region [ Create URL's ]

        public string CreateGetById(int id)
        {
            return SingularRoutePrefix.Replace(m_IdPlaceholder, id.ToString());
        }

        public string CreatePutByUserId(int userId)
        {
            string url = Put.Route.Replace(m_UserIdPlaceholder, userId.ToString());
            return url;
        }

        #endregion

        #region [ Route Definitions ]

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Options,
                GetById,
                Put
            };
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "QuoteItem Documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "QuoteItem", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", String.Format("{0}/{1}", SingularRoutePrefix, m_IdPlaceholder),
                    "Get QuoteItem by Id", typeof(EditQuoteItemDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", PluralRoutePrefix, m_UserIdPlaceholder),
                    "Update QuoteItems", typeof(List<EditQuoteItemDto>));
            }
        }

        #endregion
    }
}
