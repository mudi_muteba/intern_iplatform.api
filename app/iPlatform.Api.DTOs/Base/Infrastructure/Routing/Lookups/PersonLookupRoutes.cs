using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Lookups.Person;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    public class PersonLookupRoutes : IDefineRoutes
    {
        private const string idNumberPlaceholder = "{idNumber}";
        private const string phoneNumberPlaceholder = "{phoneNumber}";
        private const string emailPlaceholder = "{email}";
        private const string sourcePlaceholder = "{source}";
        private const string firstNamePlaceHolder = "{firstName}";
        private const string surnamePlaceHolder = "{surname}";
        private const string propertyIdPlaceHolder = "{propertyId}";

        public string RoutePrefix
        {
            get { return "/lookups/people"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "PersonLookup", "Provides personal information lookup via ID number, phone number or email"
                    , RoutePrefix);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", RoutePrefix, "Person Documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                var path = string.Format("{0}/{1}/{2}/{3}"
                    , RoutePrefix
                    , idNumberPlaceholder
                    , phoneNumberPlaceholder
                    , emailPlaceholder);

                return new RouteDefinition("GET", path, "Get personal information based on ID number, phone number and/or email", typeof(PersonLookupRequest));
            }
        }

        public RouteDefinition GetBySource
        {
            get
            {
                var path = string.Format("{0}/{1}/{2}/{3}/{4}/{5}/{6}"
                    , RoutePrefix
                    , idNumberPlaceholder
                    , firstNamePlaceHolder
                    , surnamePlaceHolder
                    , phoneNumberPlaceholder
                    , emailPlaceholder
                    , sourcePlaceholder);

                return new RouteDefinition("GET", path, "Get personal information based on ID number, phone number and/or email", typeof(PersonLookupRequest));
            }
        }

        public RouteDefinition GetByIdNumber
        {
            get
            {
                var path = string.Format("{0}/{1}"
                    , RoutePrefix
                    , idNumberPlaceholder);

                return new RouteDefinition("GET", path, "Get personal information based on ID number", typeof(PersonLookupRequest));
            }
        }

        public RouteDefinition GetByPhoneNumber
        {
            get
            {
                var path = string.Format("{0}/phoneNumber/{1}"
                    , RoutePrefix
                    , phoneNumberPlaceholder);

                return new RouteDefinition("GET", path, "Get personal information based on phone number", typeof(PersonLookupRequest));
            }
        }

        public RouteDefinition GetByEmail
        {
            get
            {
                var path = string.Format("{0}/email/{1}"
                    , RoutePrefix
                    , emailPlaceholder);

                return new RouteDefinition("GET", path, "Get personal information based on email", typeof(PersonLookupRequest));
            }
        }

        public RouteDefinition GetByPropertyId
        {
            get
            {
                var path = string.Format("{0}/property/{1}/{2}"
                    , RoutePrefix
                    , idNumberPlaceholder
                    , propertyIdPlaceHolder);

                return new RouteDefinition("GET", path, "Get the property valuation based on the propertyId", typeof(PersonLookupRequest));
            }
        }

        public RouteDefinition GetPersonLeadById
        {
            get
            {
                var path = string.Format("{0}/personlead/{1}"
                    , RoutePrefix
                    , idNumberPlaceholder);

                return new RouteDefinition("GET", path, "Gets the existing lead details if it exists else gets the personal information based on the identity number", typeof(PersonLookupRequest));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Options,
                Get,
                GetByIdNumber,
                GetByPhoneNumber,
                GetByEmail,
                GetPersonLeadById,
                GetBySource,
                GetByPropertyId
            };
        }

        public string CreateGetUrl(PersonLookupRequest request)
        {
            return Get.Route
                .Replace(idNumberPlaceholder, request.IdNumber)
                .Replace(phoneNumberPlaceholder, request.PhoneNumber)
                .Replace(emailPlaceholder, request.Email)
                .Replace(propertyIdPlaceHolder, request.PropertyId)
                .Replace(firstNamePlaceHolder, request.FirstName)
                .Replace(surnamePlaceHolder, request.Surname)
                .Replace(sourcePlaceholder, request.Source);
        }

        public string CreateGetBySourceUrl(PersonLookupRequest request)
        {
            return GetBySource.Route
                .Replace(idNumberPlaceholder, request.IdNumber)
                .Replace(phoneNumberPlaceholder, request.PhoneNumber)
                .Replace(emailPlaceholder, request.Email)
                .Replace(firstNamePlaceHolder, request.FirstName)
                .Replace(surnamePlaceHolder, request.Surname)
                .Replace(sourcePlaceholder, request.Source);
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public void CreateLinks(Resource resource)
        {
            throw new NotImplementedException();
        }
    }
}