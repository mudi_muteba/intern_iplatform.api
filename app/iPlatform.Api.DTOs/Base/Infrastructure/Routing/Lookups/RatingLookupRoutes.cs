using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Lookups.Person;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    public class RatingLookupRoutes : IDefineRoutes
    {
        private const string quoteIdPlaceholder = "{quoteId:int}";

        public string RoutePrefix
        {
            get { return "/lookups/quotes"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "RatingLookup", "Provides rating xml for testers"
                    , RoutePrefix);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", RoutePrefix, "Rating Xml");
            }
        }

        public RouteDefinition GetRequest
        {
            get
            {
              
                var path = string.Format("{0}/request/{1}"
                    , RoutePrefix
                    , quoteIdPlaceholder);

                return new RouteDefinition("GET", path, "Returns the external rating request xml", typeof(string));
            }
        }

        public RouteDefinition GetResponse
        {
            get
            {
                var path = string.Format("{0}/response/{1}"
                    , RoutePrefix
                    , quoteIdPlaceholder);

                return new RouteDefinition("GET", path, "Returns the external rating response xml", typeof(string));
            }
        }



        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Options,
                GetRequest,
                GetResponse
            };
        }

        public string CreateGetRequestUrl(int quoteId)
        {
            return GetRequest.Route.Replace(quoteIdPlaceholder, quoteId.ToString());
        }

        public string CreateGetResponseUrl(int quoteId)
        {
            return GetResponse.Route.Replace(quoteIdPlaceholder, quoteId.ToString());
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public void CreateLinks(Resource resource)
        {
            throw new NotImplementedException();
        }
    }
}