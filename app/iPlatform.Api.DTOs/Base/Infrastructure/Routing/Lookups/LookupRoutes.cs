namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    public class LookupRoutes
    {
        public LookupRoutes()
        {
            PostalCodes = new PostalCodesRoutes();
            VehicleGuide = new VehicleGuideRoutes();
            Person = new PersonLookupRoutes();
            Address = new AddressLookupRoutes();
            Occupation = new OccupationLookupRoutes();
            Company = new CompanyLookupRoutes();
            Ratings = new RatingLookupRoutes();
        }
        
        public PersonLookupRoutes Person { get; private set; }
        public PostalCodesRoutes PostalCodes { get; private set; }
        public VehicleGuideRoutes VehicleGuide { get; private set; }
        public AddressLookupRoutes Address { get; private set; }
        public OccupationLookupRoutes Occupation { get; private set; }
        public CompanyLookupRoutes Company { get; set; }
        public RatingLookupRoutes Ratings { get; set; }
    }
}