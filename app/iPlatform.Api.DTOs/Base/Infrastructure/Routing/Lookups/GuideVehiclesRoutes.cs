using iGuide.DTOs.Details;
using iGuide.DTOs.Specifications;
using iGuide.DTOs.Values;
using iGuide.DTOs.Years;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    public class GuideVehiclesRoutes
    {
        private readonly VehicleGuideRoutes vehicleGuideRoutes;

        public GuideVehiclesRoutes(VehicleGuideRoutes vehicleGuideRoutes)
        {
            this.vehicleGuideRoutes = vehicleGuideRoutes;
        }

        public RouteDefinition GetValues
        {
            get
            {
                var path = string.Format("{0}/vehicle/{1}/{2}/value",
                    vehicleGuideRoutes.RoutePrefix,
                    VehicleGuideRoutesConstants.MMCodePlaceholder,
                    VehicleGuideRoutesConstants.YearPlaceholder);

                return new RouteDefinition("GET", path, "Returns values for a vehicles", typeof(VehicleValuesDto));
            }
        }

        public RouteDefinition GetAbout
        {
            get
            {
                var path = string.Format("{0}/vehicle/{1}/{2}/about",
                    vehicleGuideRoutes.RoutePrefix,
                    VehicleGuideRoutesConstants.MMCodePlaceholder,
                    VehicleGuideRoutesConstants.YearPlaceholder);

                return new RouteDefinition("GET", path, "Returns information about a vehicle", typeof(VehicleDetailsDto));
            }
        }

        public RouteDefinition GetExtras
        {
            get
            {
                var path = string.Format("{0}/vehicle/{1}/{2}/extras",
                    vehicleGuideRoutes.RoutePrefix,
                    VehicleGuideRoutesConstants.MMCodePlaceholder,
                    VehicleGuideRoutesConstants.YearPlaceholder);

                return new RouteDefinition("GET", path, "Returns extras for a vehicle", typeof(VehiclesOptionalExtrasDto));
            }
        }

        public RouteDefinition GetByLicensePlateNumber
        {
            get
            {
                var path = string.Format("{0}/vehicle/specifications/{1}/licensePlateNumber", vehicleGuideRoutes.RoutePrefix,
                    VehicleGuideRoutesConstants.LicensePlateNumberPlaceHolder);
                return new RouteDefinition("GET", path, "Returns specifications for a vehicle using a license plate", typeof(VehicleSpecsResultsDto));
            }
        }
        public RouteDefinition GetByLicensePlateNumberAndIdNumber
        {
            get
            {
                var path = string.Format("{0}/vehicle/specifications/{1}/{2}/licensePlateAndIdNumber", vehicleGuideRoutes.RoutePrefix,
                    VehicleGuideRoutesConstants.LicensePlateNumberPlaceHolder,
                    VehicleGuideRoutesConstants.IdNumberPlaceHolder);
                return new RouteDefinition("GET", path, "Returns specifications for a vehicle using a license plate and Id number", typeof(VehicleSpecsResultsDto));
            }
        }

        public RouteDefinition GetByRegistrationNumber
        {
            get
            {
                var path = string.Format("{0}/vehicle/specifications/{1}/registrationNumber", vehicleGuideRoutes.RoutePrefix,
                    VehicleGuideRoutesConstants.RegistrationNumberPlaceHolder);
                return new RouteDefinition("GET", path, "Returns specifications for a vehicle using a registration number", typeof(VehicleSpecsResultsDto));
            }
        }

        public RouteDefinition GetYears
        {
            get
            {
                var path = string.Format("{0}/vehicle/{1}/years",
                    vehicleGuideRoutes.RoutePrefix,
                    VehicleGuideRoutesConstants.MMCodePlaceholder);

                return new RouteDefinition("GET", path, "Returns the years in which a vehicle is available", typeof(VehicleYearsDto));
            }
        }

        public string CreateYearsUrl(string mmCode)
        {
            return GetYears.Route
                .Replace(VehicleGuideRoutesConstants.MMCodePlaceholder, mmCode);
        }

        public string CreateValuesUrl(string mmCode, int year)
        {
            return GetValues.Route
                .Replace(VehicleGuideRoutesConstants.YearPlaceholder, year.ToString())
                .Replace(VehicleGuideRoutesConstants.MMCodePlaceholder, mmCode);
        }

        public string CreateAboutUrl(string mmCode, int year)
        {
            return GetAbout.Route
                .Replace(VehicleGuideRoutesConstants.YearPlaceholder, year.ToString())
                .Replace(VehicleGuideRoutesConstants.MMCodePlaceholder, mmCode);
        }

        public string CreateGetByLicensePlateNumberUrl(string licensePlateNumber)
        {
            return GetByLicensePlateNumber.Route.Replace(VehicleGuideRoutesConstants.LicensePlateNumberPlaceHolder, licensePlateNumber);
        }

        public string CreateGetByRegistrationNumberUrl(string licensePlateNumber)
        {
            return GetByLicensePlateNumber.Route.Replace(VehicleGuideRoutesConstants.RegistrationNumberPlaceHolder, licensePlateNumber);
        }

        public string CreateGetByLicensePlateAndIdNumberUrl(string licensePlateNumber, string idNumber)
        {
            return GetByLicensePlateNumberAndIdNumber.Route.Replace(VehicleGuideRoutesConstants.LicensePlateNumberPlaceHolder, licensePlateNumber)
                .Replace(VehicleGuideRoutesConstants.IdNumberPlaceHolder, idNumber);
        }

        public string CreateExtrasUrl(string mmCode, int year)
        {
            return GetExtras.Route
                .Replace(VehicleGuideRoutesConstants.YearPlaceholder, year.ToString())
                .Replace(VehicleGuideRoutesConstants.MMCodePlaceholder, mmCode);
        }
    }
}