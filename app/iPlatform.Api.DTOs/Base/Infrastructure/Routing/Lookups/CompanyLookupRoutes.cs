using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Lookups.Person;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    public class CompanyLookupRoutes : IDefineRoutes
    {

        private const string search = "{search}";

        public string RoutePrefix
        {
            get { return "/lookups"; }
        }

        public string PluralPrefix
        {
            get { return "companies"; }
        }


        public string SingularPrefix
        {
            get { return "company"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "CompanyLookup", "Provides company information lookup via company name or number"
                    , RoutePrefix);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", RoutePrefix, "Company Documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                var path = string.Format("{0}/{1}/{2}"
                    , RoutePrefix
                    , SingularPrefix
                    , search);
                return new RouteDefinition("GET", path, "Get company information based on company number", typeof(PersonLookupRequest));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                var path = string.Format("{0}/{1}/{2}"
                   , RoutePrefix
                   , PluralPrefix
                   , search);

                return new RouteDefinition("GET", path, "Gets all the companies with a particular name based search text provided", typeof(PersonLookupRequest));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Options,
                Get,
                Search
            };
        }

        public string CreateGetUrl(PersonLookupRequest request)
        {
            return Get.Route.Replace(search, request.Search);

        }

        public string CreateGetBySourceUrl(PersonLookupRequest request)
        {
            return Search.Route.Replace(search, request.Search);

        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public void CreateLinks(Resource resource)
        {
            throw new NotImplementedException();
        }
    }
}