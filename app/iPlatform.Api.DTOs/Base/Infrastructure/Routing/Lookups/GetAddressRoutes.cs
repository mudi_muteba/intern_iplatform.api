﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Lookups.Address.GetAddress;
using iPlatform.Api.DTOs.Party.Address;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    public class GetAddressRoutes : IDefineRoutes<GetAddressDto>
    {
        private const string RoutePrefix = "/LookUps/GetAddress/";
        private const string PostCodePlaceHolder = "{postCode}";
        public void CreateLinks(GetAddressDto resource)
        {
            throw new NotImplementedException();
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public string CreateGetAddressesByPostCodeUrl(string postCode)
        {
            var idPart = GetAddressesByPostCode.Route.Replace(PostCodePlaceHolder, postCode);
            return idPart;
        }

        #region [ Route Definitions ]

        public RouteDefinition GetAddressesByPostCode
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}{1}", RoutePrefix, PostCodePlaceHolder), "List of address", typeof(List<ListAddressDto>));
            }
        }
        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}", RoutePrefix),
                    "getAddress() documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "GetAddress", "Authentication before using the API", Options.Route);
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                GetAddressesByPostCode
            };
        }

        #endregion
    }
}
