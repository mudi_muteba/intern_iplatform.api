using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Lookups.Person;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    public class OccupationLookupRoutes : IDefineRoutes
    {
        private const string searchPlaceholder = "{search}";

        public string RoutePrefix
        {
            get { return "/lookups/occupation"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("OPTIONS", "Occupation lookup documentation", "Provides Occupation information lookup via search text", RoutePrefix); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", RoutePrefix, "Occupations Documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                var path = string.Format("{0}/{1}"
                    , RoutePrefix
                    , searchPlaceholder);

                return new RouteDefinition("GET", path, "Get occupations based on search text");
            }
        }


        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Options,
                Get,
            };
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public string CreateGetUrl(string searcTerm)
        {
            return Get.Route.Replace(searchPlaceholder, searcTerm);
        }
    }
}