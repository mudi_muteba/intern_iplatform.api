using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Lookups.PostalCodes;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    public class PostalCodesRoutes : IDefineRoutes
    {
        private const string PageNumberPlaceHolder = "{pageNumber:int}";
        private const string PageSizePlaceHolder = "{pageSize:int}";
        private const string SearchTermPlaceHolder = "{searchTerm}";
        private const string SearchPlaceHolder = "{search}";
        public string RoutePrefix
        {
            get { return "/lookups/postalcodes"; }
        }

        public string SimplifiedRoutePrefix
        {
            get { return "/lookups/postalcodes/simple"; }
        }

        public string RoutePrefixV2
        {
            get { return "/lookups/postalcodes/v2"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "PostalCodeLookups", "The entry point postal code lookups",
                    Get.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", RoutePrefix, "Postal Code Documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", RoutePrefix),
                    "List of postal codes", typeof(PagedResultDto<PostalCodeDto>));
            }
        }

        public RouteDefinition GetWithSearchTerm
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", RoutePrefix, SearchTermPlaceHolder),
                    "List of postal codes by search term", typeof(PagedResultDto<PostalCodeDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET",
                    string.Format("{0}/{1}/{2}", RoutePrefix, PageNumberPlaceHolder, PageSizePlaceHolder),
                    "List of postal codes with pagination", typeof(PagedResultDto<PostalCodeDto>));
            }
        }

        public RouteDefinition GetWithSearchTermAndPagination
        {
            get
            {
                return new RouteDefinition("GET",
                    string.Format("{0}/{1}/{2}/{3}", RoutePrefix, SearchTermPlaceHolder, PageNumberPlaceHolder,
                        PageSizePlaceHolder),
                    "List of postal codes with pagination by search term", typeof(PagedResultDto<PostalCodeDto>));
            }
        }

        public RouteDefinition GetSimplified
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", SimplifiedRoutePrefix),
                    "List of postal codes", typeof(PagedResultDto<PostalCodeDto>));
            }
        }

        public RouteDefinition GetSimplifiedWithSearchTerm
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SimplifiedRoutePrefix, SearchTermPlaceHolder),
                    "List of postal codes by search term", typeof(PagedResultDto<PostalCodeDto>));
            }
        }

        public RouteDefinition GetSimplifiedWithPagination
        {
            get
            {
                return new RouteDefinition("GET",
                    string.Format("{0}/{1}/{2}", SimplifiedRoutePrefix, PageNumberPlaceHolder, PageSizePlaceHolder),
                    "List of postal codes with pagination", typeof(PagedResultDto<PostalCodeDto>));
            }
        }

        public RouteDefinition GetSimplifiedWithSearchTermAndPagination
        {
            get
            {
                return new RouteDefinition("GET",
                    string.Format("{0}/{1}/{2}/{3}", SimplifiedRoutePrefix, SearchTermPlaceHolder, PageNumberPlaceHolder,
                        PageSizePlaceHolder),
                    "List of postal codes with pagination by search term", typeof(PagedResultDto<PostalCodeDto>));
            }
        }

        public RouteDefinition SearchSuburbsAndPostalCodes
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", RoutePrefixV2, SearchPlaceHolder),
                    "List of postal codes", typeof(ListResultDto<PostalCodeDto>));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Options,
                Get,
                GetWithPagination,
                GetWithSearchTerm,
                GetWithSearchTermAndPagination,
                GetSimplified,
                GetSimplifiedWithPagination,
                GetSimplifiedWithSearchTerm,
                GetSimplifiedWithSearchTermAndPagination,
                SearchSuburbsAndPostalCodes
            };
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public string CreatePaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(PageNumberPlaceHolder, pageNumber.ToString())
                .Replace(PageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public string CreateGetWithSearchTerm(string searchTerm)
        {
            return GetWithSearchTerm.Route
                .Replace(SearchTermPlaceHolder, searchTerm)
                ;
        }

        public string CreateSearchTermPaginationUrl(string searchTerm, int pageNumber, int pageSize)
        {
            return GetWithSearchTermAndPagination.Route
                .Replace(SearchTermPlaceHolder, searchTerm)
                .Replace(PageNumberPlaceHolder, pageNumber.ToString())
                .Replace(PageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public string CreateSimplifiedPaginationUrl(int pageNumber, int pageSize)
        {
            return GetSimplifiedWithPagination.Route
                .Replace(PageNumberPlaceHolder, pageNumber.ToString())
                .Replace(PageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public string CreateGetSimplifiedWithSearchTerm(string searchTerm)
        {
            return GetSimplifiedWithSearchTerm.Route
                .Replace(SearchTermPlaceHolder, searchTerm)
                ;
        }

        public string CreateSimplifiedSearchTermPaginationUrl(string searchTerm, int pageNumber, int pageSize)
        {
            return GetSimplifiedWithSearchTermAndPagination.Route
                .Replace(SearchTermPlaceHolder, searchTerm)
                .Replace(PageNumberPlaceHolder, pageNumber.ToString())
                .Replace(PageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public string CreateSearchSuburbsAndPostalCodes(string search)
        {
            return SearchSuburbsAndPostalCodes.Route.Replace(SearchPlaceHolder, search);
        }
    }
}