namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    internal class VehicleGuideRoutesConstants
    {
        public static readonly string YearPlaceholder = "{year:int}";
        public static readonly string MakePlaceholder = "{make}";
        public static readonly string ModelPlaceholder = "{model}";
        public static readonly string MMCodePlaceholder = "{mmCode}";
        public static readonly string LicensePlateNumberPlaceHolder = "{licensePlateNumber}";
        public static readonly string RegistrationNumberPlaceHolder = "{registrationNumber}";
        public static readonly string IdNumberPlaceHolder = "{idNumber}";
    }
}