using iGuide.DTOs.Makes;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    public class GuideMakesRoutes
    {
        private readonly VehicleGuideRoutes vehicleGuideRoutes;

        public GuideMakesRoutes(VehicleGuideRoutes vehicleGuideRoutes)
        {
            this.vehicleGuideRoutes = vehicleGuideRoutes;
        }

        public RouteDefinition GetAll
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/makes", vehicleGuideRoutes.RoutePrefix), "Returns all makes", typeof(MakeSearchResultsDto));
            }
        }

        public RouteDefinition ByYear
        {
            get
            {
                var path = string.Format("{0}/makes/{1}", vehicleGuideRoutes.RoutePrefix, VehicleGuideRoutesConstants.YearPlaceholder);
                return new RouteDefinition("GET", path, "Returns all makes by year", typeof(MakeSearchResultsDto));
            }
        }

        public RouteDefinition BySearch
        {
            get
            {
                var path = string.Format("{0}/makes/{1}", vehicleGuideRoutes.RoutePrefix, VehicleGuideRoutesConstants.MakePlaceholder);
                return new RouteDefinition("GET", path, "Returns all makes by search criteria", typeof(MakeSearchResultsDto));
            }
        }

        public RouteDefinition BySearchAndYear
        {
            get
            {
                var path = string.Format("{0}/makes/{1}/{2}",
                    vehicleGuideRoutes.RoutePrefix,
                    VehicleGuideRoutesConstants.YearPlaceholder,
                    VehicleGuideRoutesConstants.MakePlaceholder);

                return new RouteDefinition("GET", path, "Returns all makes for year and search criteria", typeof(MakeSearchResultsDto));
            }
        }

        public string CreateByYearUrl(int year)
        {
            return ByYear.Route.Replace(VehicleGuideRoutesConstants.YearPlaceholder, year.ToString());
        }

        public string CreateSearchUrl(string search)
        {
            return BySearch.Route.Replace(VehicleGuideRoutesConstants.MakePlaceholder, search);
        }

        public string CreateSearchByYearUrl(string search, int year)
        {
            return BySearch.Route
                .Replace(VehicleGuideRoutesConstants.YearPlaceholder, search)
                .Replace(VehicleGuideRoutesConstants.MakePlaceholder, search);
        }

    }
}