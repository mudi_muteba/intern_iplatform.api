using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Lookups.Address;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    public class AddressLookupRoutes : IDefineRoutes<AddressLookupDto>
    {
        private const string postCodePlaceholder = "{postCode}";
        private const string countryCodePlaceholder = "{countryCode}";

        public string RoutePrefix
        {
            get { return "/lookups/province/"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("OPTIONS", "address lookup", "Provides personal information lookup via ID number, phone number or email", RoutePrefix); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", RoutePrefix, "Address Lookup Documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                throw new NotImplementedException();
            }

        }

        public RouteDefinition GetProvinceByPostCode
        {
            get
            {
                var path = string.Format("{0}bypostcode/{1}"
                    , RoutePrefix
                    , postCodePlaceholder);

                return new RouteDefinition("GET", path, "Get province by postcode", typeof(AddressLookupDto));
            }
        }

        public RouteDefinition GetProvinceByPostCodeAndCountryCode
        {
            get
            {
                var path = string.Format("{0}bypostcode/{1}/{2}"
                    , RoutePrefix
                    , postCodePlaceholder
                    , countryCodePlaceholder
                    );

                return new RouteDefinition("GET", path, "Get province by postcode and country", typeof(AddressLookupDto));
            }
        }

        public RouteDefinition GetProvinceBySuburb
        {
            get
            {
                var path = string.Format("{0}bysuburb/{1}"
                    , RoutePrefix
                    , postCodePlaceholder);

                return new RouteDefinition("GET", path, "Get province by suburb", typeof(AddressLookupDto));
            }
        }

        public RouteDefinition GetProvinceBySuburbAndCountryCode
        {
            get
            {
                var path = string.Format("{0}bysuburb/{1}/{2}"
                    , RoutePrefix
                    , postCodePlaceholder
                    , countryCodePlaceholder
                    );

                return new RouteDefinition("GET", path, "Get province by suburb and country", typeof(AddressLookupDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Options,
                Get,
                GetProvinceByPostCode,
                GetProvinceByPostCodeAndCountryCode,
                GetProvinceBySuburb,
                GetProvinceBySuburbAndCountryCode
            };
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public string CreateGetProvinceByPostCode(string postCode)
        {
            return GetProvinceByPostCode.Route
                .Replace(postCodePlaceholder, postCode);
        }

        public string CreateGetProvinceByPostCodeAndCountryCode(string postCode, string countryCode)
        {
            return GetProvinceByPostCodeAndCountryCode.Route
                .Replace(postCodePlaceholder, postCode)
                .Replace(countryCodePlaceholder, countryCode);
        }

        public string CreateGetProvinceBySuburb(string suburb)
        {
            return GetProvinceBySuburb.Route
                .Replace(postCodePlaceholder, suburb);
        }

        public string CreateGetProvinceBySuburbAndCountryCode(string suburb, string countryCode)
        {
            return GetProvinceBySuburbAndCountryCode.Route
                .Replace(postCodePlaceholder, suburb)
                .Replace(countryCodePlaceholder, countryCode);
        }

        public void CreateLinks(AddressLookupDto resource)
        {
            throw new NotImplementedException();
        }

    }
}