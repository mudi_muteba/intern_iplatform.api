using iGuide.DTOs.Models;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    public class GuideModelsRoutes
    {
        private readonly VehicleGuideRoutes vehicleGuideRoutes;

        public GuideModelsRoutes(VehicleGuideRoutes vehicleGuideRoutes)
        {
            this.vehicleGuideRoutes = vehicleGuideRoutes;
        }

        public RouteDefinition GetByMake
        {
            get
            {
                var path = string.Format("{0}/models/{1}", vehicleGuideRoutes.RoutePrefix, VehicleGuideRoutesConstants.MakePlaceholder);
                return new RouteDefinition("GET", path, "Returns all models for a make",typeof(ModelSearchResultsDto));
            }
        }

        public RouteDefinition GetByMakeAndYear
        {
            get
            {
                var path = string.Format("{0}/models/{1}/{2}",
                    vehicleGuideRoutes.RoutePrefix,
                    VehicleGuideRoutesConstants.YearPlaceholder,
                    VehicleGuideRoutesConstants.MakePlaceholder)
                    ;
                return new RouteDefinition("GET", path, "Returns all models for a make and year", typeof(ModelSearchResultsDto));
            }
        }

        public RouteDefinition SearchByMake
        {
            get
            {
                var path = string.Format("{0}/models/{1}/{2}",
                    vehicleGuideRoutes.RoutePrefix,
                    VehicleGuideRoutesConstants.MakePlaceholder,
                    VehicleGuideRoutesConstants.ModelPlaceholder)
                    ;

                return new RouteDefinition("GET", path, "Returns all models for a make that matches the search criteria", typeof(ModelSearchResultsDto));
            }
        }

        public RouteDefinition SearchByMakeAndYear
        {
            get
            {
                var path = string.Format("{0}/models/{1}/{2}/{3}",
                    vehicleGuideRoutes.RoutePrefix,
                    VehicleGuideRoutesConstants.YearPlaceholder,
                    VehicleGuideRoutesConstants.MakePlaceholder,
                    VehicleGuideRoutesConstants.ModelPlaceholder)
                    ;

                return new RouteDefinition("GET", path, "Returns all models for a make and year that matches the search criteria", typeof(ModelSearchResultsDto));
            }
        }

        public string CreateByMakeUrl(string make)
        {
            return GetByMake.Route
                .Replace(VehicleGuideRoutesConstants.MakePlaceholder, make);
        }

        public string CreateByMakeAndYearUrl(string make, int year)
        {
            return GetByMakeAndYear.Route
                .Replace(VehicleGuideRoutesConstants.YearPlaceholder, year.ToString())
                .Replace(VehicleGuideRoutesConstants.MakePlaceholder, make);
        }

        public string CreateSearchByMakeUrl(string make, string model)
        {
            return SearchByMake.Route
                .Replace(VehicleGuideRoutesConstants.ModelPlaceholder, model)
                .Replace(VehicleGuideRoutesConstants.MakePlaceholder, make);
        }

        public string CreateSearchByMakeAndYearUrl(string make, int year, string model)
        {
            return SearchByMakeAndYear.Route
                .Replace(VehicleGuideRoutesConstants.YearPlaceholder, year.ToString())
                .Replace(VehicleGuideRoutesConstants.ModelPlaceholder, model)
                .Replace(VehicleGuideRoutesConstants.MakePlaceholder, make);
        }

    }
}