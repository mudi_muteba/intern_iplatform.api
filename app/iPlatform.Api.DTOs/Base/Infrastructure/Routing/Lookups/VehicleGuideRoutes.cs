using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups
{
    public class VehicleGuideRoutes : IDefineRoutes
    {
        public string RoutePrefix
        {
            get { return "/lookups/vehicles"; }
        }

        public VehicleGuideRoutes()
        {
            Makes = new GuideMakesRoutes(this);
            Models = new GuideModelsRoutes(this);
            Vehicles = new GuideVehiclesRoutes(this);
        }

        public GuideMakesRoutes Makes { get; private set; }
        public GuideModelsRoutes Models { get; private set; }
        public GuideVehiclesRoutes Vehicles { get; private set; }

        public RouteCategoryDefinition Definition { get; private set; }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", RoutePrefix, "Vehicle Lookup Documentation");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Makes.GetAll,
                Makes.ByYear,
                Makes.BySearch,
                Makes.BySearchAndYear,
                Models.GetByMake,
                Models.GetByMakeAndYear,
                Models.SearchByMake,
                Models.SearchByMakeAndYear,
                Vehicles.GetYears,
                Vehicles.GetAbout,
                Vehicles.GetExtras,
                Vehicles.GetValues,
                Vehicles.GetByLicensePlateNumber,
                Vehicles.GetByRegistrationNumber,
                Vehicles.GetByLicensePlateNumberAndIdNumber
            };
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }
    }


}