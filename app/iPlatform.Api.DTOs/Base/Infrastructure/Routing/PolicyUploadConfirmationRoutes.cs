using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Upload;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class PolicyUploadConfirmationRoutes : IDefineRoutes
    {
        private const string IdPlaceholder = "{id:int}";

        public string SingularRoutePrefix
        {
            get { return "/PolicyUploadConfirmation"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/PolicyUploadConfirmations"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "PolicyUploadConfirmation", "Policy Upload Confirmation", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix), "PolicyUploadConfirmation documentation");
            }
        }

        public RouteDefinition Post
        {
            get { return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix), "Creates a Policy Upload Confirmation", typeof(PolicyUploadDto)); }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Get a Policy Upload Confirmation by Id", typeof(UserDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Post,
            };
        }

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(IdPlaceholder, id.ToString());
        }
        
        public void CreateLinks(Resource resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
    }
}