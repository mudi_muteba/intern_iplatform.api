﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.ChannelSettings;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ChannelSettingRoutes : IDefineRoutes<ListChannelSettingDto>
    {
        private const string idPlaceholder = "{id:int}";

        private const string channelIdPlaceholder = "{channelId:int}";

        public string PluralRoutePrefix
        {
            get { return "/channelsettings"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/channelsetting"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Channel settings", "The entry point for Channel settings", Get.Route); }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the Channel settings", typeof(PagedResultDto<ListChannelSettingDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Channel setting by Id", typeof(ListChannelSettingDto));
            }
        }

        public RouteDefinition GetByChannelId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", PluralRoutePrefix, channelIdPlaceholder),
                    "Get channel settings by Channel Id", typeof(ListChannelSettingDto));
            }
        }


        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", SingularRoutePrefix),
                    "Create a new channel setting", typeof(CreateChannelSettingDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder), "Update a channel setting", typeof(UpdateChannelSettingDto));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search Channel Setting", typeof(SearchChannelSettingDto));
            }
        }

        public RouteDefinition SearchNoPagination
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/searchnopagination", PluralRoutePrefix),
                    "Search Channel Setting no pagination", typeof(SearchChannelSettingDto));
            }
        }

        public RouteDefinition Delete
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/delete", SingularRoutePrefix, idPlaceholder),
                    "Disable a channel Setting by Id", typeof(DeleteChannelSettingDto));
            }
        }


        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetById,
                Post,
                GetByChannelId,
                Put,
                Search,
                SearchNoPagination,
                Delete

            };
        }

        public void CreateLinks(ListChannelSettingDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
            resource.AddLink("self", CreateGetChannelSettingByChannelId(resource.Channel.Id));
        }

        public void CreateLinks(ChannelSettingDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
            resource.AddLink("self", CreateGetChannelSettingByChannelId(resource.Channel.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetChannelSettingByChannelId(int id)
        {
            var idPart = GetByChannelId.Route.Replace(channelIdPlaceholder, id.ToString());

            return idPart;
        }
    }
}
