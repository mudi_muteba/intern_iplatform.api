﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.DataView.DataViewLeadImport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class DataViewSystemRoutes : IDefineRoutes
    {
        public string SingularRoutePrefix
        {
            get
            {
                return "/DataView";
            }
        }

        #region [ Create URL's]

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region [ Route Definitions ]

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Options
            };
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}", SingularRoutePrefix),
                    "Data Views documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "DataView", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition LeadImportDataView
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/LeadImport", SingularRoutePrefix),
                    "Get Lead import data view.", typeof(SearchDataViewLeadImportDto));
            }
        }

        #endregion
    }
}
