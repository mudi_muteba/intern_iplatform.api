﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.FormBuilder;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class FormBuilderRoutes : IDefineRoutes<FormBuilderDto>
    {
 
        private const string IdPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string PluralRoutePrefix
        {
            get { return "/formbuilders"; }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("/formbuilder/{0}", IdPlaceholder); }
        }
      

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePutById(int id)
        {
            var idPart = Put.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }



        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix), "Get form by Id", typeof(FormBuilderDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/", SingularRoutePrefix), "Update a form builder", typeof(EditFormBuilderDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/delete", SingularRoutePrefix, IdPlaceholder), "Delete a form builder");
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", PluralRoutePrefix, "Create form", typeof(CreateFormBuilderDto));
            }
        }


        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix), "List of the form builders", typeof(PagedResultDto<FormBuilderDto>));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search form builders", typeof(SearchFormBuilderDto));
            }
        }


        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Form Builder", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix), "Form Builder documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "List of the form builders", typeof(PagedResultDto<FormBuilderDto>));
            }
        }

        

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {

                GetById,
                Options,
                Post,
                Get,
                GetNoPagination,
                Search,
                Put,
                PutDelete
            };
        }

        public void CreateLinks(FormBuilderDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }
    }
}
