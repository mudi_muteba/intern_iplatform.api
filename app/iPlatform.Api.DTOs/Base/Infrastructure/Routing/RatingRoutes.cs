using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class RatingRoutes : IDefineRoutes
    {
        public string RoutePrefix
        {
            get { return "rating"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "Ratings", "The entry point for external system ratings",
                    Get.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", RoutePrefix), "Ratings Documentation");
            }
        }

        public RouteDefinition Get
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", RoutePrefix), "List of products",typeof(RatingResultDto)); }
        }

        public RouteDefinition GetRating
        {
            get
            {
                return new RouteDefinition("POST", RoutePrefix, "Get ratings for the allocated products",
                    typeof(RatingRequestDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetRating
            };
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public string CreateGetById(RatingResultDto result)
        {
            return GetRating.Route;
        }

        public string CreateRatingRequestUrl()
        {
            return GetRating.Route;
        }
    }
}