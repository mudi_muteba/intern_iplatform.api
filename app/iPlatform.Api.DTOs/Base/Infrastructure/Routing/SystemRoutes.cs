﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Claims;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Components;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.HistoryLoss.LossHistory;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Individual;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Leads;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Lookups;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.LossHistoryDefinitions;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.LossHistoryQuestionDefinitions;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Policies;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Reports;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.SecondLevel;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Tia;


namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public static class SystemRoutes
    {
        public static readonly HomeRoutes Home = new HomeRoutes();
        public static readonly CustomappRoutes Customapp = new CustomappRoutes();
        public static readonly AuthenticateRoutes Authenticate = new AuthenticateRoutes();
        public static readonly CampaignsRoutes Campaigns = new CampaignsRoutes();
        public static readonly LookupRoutes Lookups = new LookupRoutes();
        public static readonly ProductsRoutes Products = new ProductsRoutes();
        public static readonly OrganisationsRoutes Organisations = new OrganisationsRoutes();
        public static readonly IndividualRoutes Individuals = new IndividualRoutes();
        public static readonly UserRoutes Users = new UserRoutes();
        public static readonly RatingRoutes Rating = new RatingRoutes();
        public static readonly IndividualAddressRoutes IndividualAddresses = new IndividualAddressRoutes();
        public static readonly IndividualNoteRoutes IndividualNotes = new IndividualNoteRoutes();
        public static readonly IndividualBankDetailRoutes IndividualBankDetails = new IndividualBankDetailRoutes();
        public static readonly TestRoutes Test = new TestRoutes();
        public static readonly QuoteRoutes Quotes = new QuoteRoutes();
        public static readonly IndividualProposalHeadersRoutes IndividualProposalHeaders = new IndividualProposalHeadersRoutes();
        public static readonly ProposalDefinitionsRoutes ProposalDefinitions = new ProposalDefinitionsRoutes();
        public static readonly IndividualAssetVehicleRoutes IndividualAssetVehicle = new IndividualAssetVehicleRoutes();
        public static readonly IndividualAssetRiskItemRoutes IndividualAssetRiskItem = new IndividualAssetRiskItemRoutes();
        public static readonly IndividualContactsRoutes IndividualContacts = new IndividualContactsRoutes();
        public static readonly UserDiscountsRoutes UserDiscounts = new UserDiscountsRoutes();
        public static readonly IndividualImportRoutes IndividualImport = new IndividualImportRoutes();
        public static readonly SecondLevelRoutes SecondLevel = new SecondLevelRoutes();
        public static readonly ClaimsHeaderRoutes ClaimsHeader = new ClaimsHeaderRoutes();
        public static readonly ClaimsItemRoutes ClaimsItem = new ClaimsItemRoutes();
        public static readonly LeadsRoutes Leads = new LeadsRoutes();
        public static readonly LeadImportRoutes LeadImport = new LeadImportRoutes();
        public static readonly IndividualHeaderUploadRoutes IndividualHeaderUpload = new IndividualHeaderUploadRoutes();
        public static readonly IndividualUploadDetailRoutes IndividualUploadDetail = new IndividualUploadDetailRoutes();
        public static readonly PolicyHeaderRoutes PolicyHeaders = new PolicyHeaderRoutes();
        public static readonly LeadActivitiesRoutes LeadActivities = new LeadActivitiesRoutes();
        public static readonly LeadConversionStatisticsRoutes LeadConversionStatistics = new LeadConversionStatisticsRoutes();
        public static readonly TeamRoutes Teams = new TeamRoutes();
        public static readonly FuneralMemberRoutes FuneralMembers = new FuneralMemberRoutes();
        public static readonly MotorLossHistoryRoutes MotorLossHistory = new MotorLossHistoryRoutes();
        public static readonly HomeLossHistoryRoutes HomeLossHistory = new HomeLossHistoryRoutes();
        public static readonly RelationshipsRoutes Relationships = new RelationshipsRoutes();
        public static readonly BankRoutes Bank = new BankRoutes();
        public static readonly BankBranchRoutes BankBranch = new BankBranchRoutes();
        public static readonly InsurersRoutes Insurers = new InsurersRoutes();
        public static readonly ClaimTypeRoutes ClaimType = new ClaimTypeRoutes();
        public static readonly ChannelRoutes Channels = new ChannelRoutes();
        public static readonly ChannelSettingRoutes ChannelSettings = new ChannelSettingRoutes();
        public static readonly ChannelPermissionRoutes Permissions = new ChannelPermissionRoutes();
        public static readonly QuestionRoutes Questions = new QuestionRoutes();
        public static readonly ItcQuestionRoutes ItcQuestions = new ItcQuestionRoutes();
        public static readonly IndividualCorrespondencePreferenceRoutes CorrespondencePreferences = new IndividualCorrespondencePreferenceRoutes();
        public static readonly IndividualPaymentDetailsRoute PaymentDetails = new IndividualPaymentDetailsRoute();
        public static readonly ProposalHeaderRoutes ProposalHeaders = new ProposalHeaderRoutes();
        public static readonly QuoteBreakdownRoutes QuoteBreakdown = new QuoteBreakdownRoutes();
        public static readonly ProposalDefinitionQuestionsRoutes ProposalDefinitionQuestions = new ProposalDefinitionQuestionsRoutes();
        public static readonly FuneralDualCoverRoutes FuneralDualCover = new FuneralDualCoverRoutes();
        public static readonly ReportRoutes Reports = new ReportRoutes();
        public static readonly ReportScheduleRoutes ReportSchedules = new ReportScheduleRoutes();
        public static readonly QuoteReportRoutes QuoteReports = new QuoteReportRoutes();
        public static readonly ComparativeQuoteReportRoutes ComparativeQuoteReports = new ComparativeQuoteReportRoutes();
        public static readonly AuditRoutes Audit = new AuditRoutes();
        public static readonly JsonDataStoresRoutes JsonDataStores = new JsonDataStoresRoutes();
        public static readonly LossHistoryRoutes LossHistory = new LossHistoryRoutes();
        public static readonly SalesForceRoutes SalesForce = new SalesForceRoutes();
        public static readonly AdditionalMemberRoutes AdditionalMembers = new AdditionalMemberRoutes();
        public static readonly IndividualCorrespondenceRoutes IndividualCorrespondence = new IndividualCorrespondenceRoutes();
        public static readonly ProposalDeclineRoutes ProposalDecline = new ProposalDeclineRoutes();
        public static readonly WorkflowRetryLogRoutes WorkflowRetryLogs = new WorkflowRetryLogRoutes();
        public static readonly CustomerSatisfactionSurveyRoutes CustomerSatisfactionSurvey = new CustomerSatisfactionSurveyRoutes();
        public static readonly EventRoutes Events = new EventRoutes();
        public static readonly EscalationPlanRoutes EscalationPlans = new EscalationPlanRoutes();
        public static readonly EscalationPlanStepRoutes EscalationPlanSteps = new EscalationPlanStepRoutes();
        public static readonly EscalationPlanExecutionHistoryRoutes EscalationPlanExecutionHistory = new EscalationPlanExecutionHistoryRoutes();
        public static readonly CanaryRoutes Canaries = new CanaryRoutes();
        public static readonly SignFlowRoutes SignFlow = new SignFlowRoutes();
        public static readonly PolicyUploadConfirmationRoutes PolicyUploadConfirmations = new PolicyUploadConfirmationRoutes();
        public static readonly SalesRoutes Sales = new SalesRoutes();
        public static readonly LossHistoryDefinitionsRoutes LossHistoryDefinitions = new LossHistoryDefinitionsRoutes();
        public static readonly SalesTagRoutes SalesTag = new SalesTagRoutes();
        public static readonly LossHistoryQuestionDefinitionRoutes LossHistoryQuestionDefinition = new LossHistoryQuestionDefinitionRoutes();
        public static readonly AigReportRoutes AigReport = new AigReportRoutes();
        public static readonly CallCentreReportRoutes CallCentreReports = new CallCentreReportRoutes();
        public static readonly SettingsiRateRoutes SettingsiRate = new SettingsiRateRoutes();
        public static readonly SettingsQuoteUploadRoutes SettingsQuoteUploads = new SettingsQuoteUploadRoutes();
        public static readonly TiaRoutes Tia = new TiaRoutes();
        public static readonly CoverLinkRoutes CoverLinks = new CoverLinkRoutes();
        public static readonly SettingsiPersonRoutes SettingsiPerson = new SettingsiPersonRoutes();
        public static readonly UserChannelRoutes UserChannel = new UserChannelRoutes();
        public static readonly DocumentManagementRoutes DocumentManagementRoutes = new DocumentManagementRoutes();
        public static readonly VehicleGuideSettingRoutes VehicleGuideSetting = new VehicleGuideSettingRoutes();
        public static readonly ChannelEventRoutes ChannelEvent = new ChannelEventRoutes();
        public static readonly SMSCommunicationSettingRoutes CommunicationSetting = new SMSCommunicationSettingRoutes();
        public static readonly GetAddressRoutes GetAddress = new GetAddressRoutes();
        public static readonly MapVapQuestionDefinitionRoutes MapVapQuestionDefinition = new MapVapQuestionDefinitionRoutes();
        public static readonly UserAuthorisationRoutes UserAuthorisation = new UserAuthorisationRoutes();
        public static readonly CoverDefinitionsRoutes CoverDefinitions = new CoverDefinitionsRoutes();
        public static readonly DisplaySettingRoutes DisplaySettings = new DisplaySettingRoutes();
        public static readonly ChannelTemplateRoutes ChannelTemplates = new ChannelTemplateRoutes();
		public static readonly ComponentDefinitionRoutes ComponentDefinitionRoutes = new ComponentDefinitionRoutes();
        public static readonly ComponentQuestionAnswerRoutes ComponentQuestionAnswerRoutes = new ComponentQuestionAnswerRoutes();
        public static readonly ComponentQuestionDefinitionsRoutes ComponentQuestionDefinitionsRoutes = new ComponentQuestionDefinitionsRoutes();
		public static readonly ComponentHeaderRoutes ComponentHeaderRoutes = new ComponentHeaderRoutes();        
		public static readonly OverrideRatingQuestionRoutes OverrideRatingQuestions = new OverrideRatingQuestionRoutes();
        
        public static readonly EmailCommunicationSettingRoutes EmailCommunicationSettings = new EmailCommunicationSettingRoutes();
        public static readonly SMSCommunicationSettingRoutes SMSCommunicationSettings = new SMSCommunicationSettingRoutes();

        public static readonly RatingRuleHeaderRoutes RatingRuleHeaders = new RatingRuleHeaderRoutes();
        public static readonly RatingRuleHeaderCalculationRoutes RatingRuleHeaderCalculations = new RatingRuleHeaderCalculationRoutes();
        public static readonly SmartPolicyRoutes SmartPolicy = new SmartPolicyRoutes();
        public static readonly QuoteItemRoutes QuoteItems = new QuoteItemRoutes();
        public static readonly FormBuilderRoutes FormBuilder = new FormBuilderRoutes();
        public static readonly FormBuilderDetailRoutes FormBuilderDetail = new FormBuilderDetailRoutes();
        public static readonly CimsDataSyncRoutes CimsDataSync = new CimsDataSyncRoutes();
        public static readonly ProductPricingStructureRoutes ProductPricingStructure = new ProductPricingStructureRoutes();
        public static readonly LogReaderRoutes LogReader = new LogReaderRoutes();
        public static readonly DataViewSystemRoutes DataView = new DataViewSystemRoutes();
        public static readonly SettingsiRateUserRoutes SettingsiRateUser = new SettingsiRateUserRoutes();
        public static readonly NotificationRoutes Notification = new NotificationRoutes();



        public static readonly PolicyBindingsRoutes PolicyBinding = new PolicyBindingsRoutes();
        public static IDefineRoutes[] Routes
        {
            get
            {
                return new IDefineRoutes[]
                {
                    Home,
                    Customapp,
                    Authenticate,
                    Campaigns,
                    Users,
                    Individuals,
                    Products,
                    Organisations,
                    Rating,
                    Lookups.PostalCodes,
                    Quotes,
                    IndividualProposalHeaders,
                    IndividualBankDetails,
                    IndividualAddresses,
                    IndividualNotes,
                    IndividualUploadDetail,
                    ProposalDefinitions,
                    IndividualAssetVehicle,
                    IndividualAssetRiskItem,
                    IndividualContacts,
                    UserDiscounts,
                    IndividualImport,
                    SecondLevel,
                    ClaimsHeader,
                    ClaimsItem,
                    Leads,
                    PolicyHeaders,
                    LeadImport,
                    LeadActivities,
                    LeadConversionStatistics,
                    Teams,
                    FuneralMembers,
                    MotorLossHistory,
                    HomeLossHistory,
                    Relationships,
                    Bank,
                    BankBranch,
                    Insurers,
                    ClaimType,
                    Channels,
                    ChannelSettings,
                    Permissions,
                    Questions,
                    ItcQuestions,
                    CorrespondencePreferences,
                    PaymentDetails,
                    ProposalHeaders,
                    ProposalDefinitionQuestions,
                    FuneralDualCover,
                    Reports,
                    ReportSchedules,
                    QuoteReports,
                    ComparativeQuoteReports,
                    Audit,
                    JsonDataStores,
                    LossHistory,
                    AdditionalMembers,
                    IndividualCorrespondence,
                    ProposalDecline,
                    WorkflowRetryLogs,
                    CustomerSatisfactionSurvey,
                    Events,
                    EscalationPlans,
                    EscalationPlanSteps,
                    EscalationPlanExecutionHistory,
                    Canaries,
                    Sales,
                    PolicyUploadConfirmations,
                    LossHistoryDefinitions,
                    SalesTag,
                    LossHistoryQuestionDefinition,
                    AigReport,
                    CallCentreReports,
                    SettingsiRate,
                    SettingsQuoteUploads,
                    Tia,
                    CoverLinks,
                    SettingsiPerson,
                    DocumentManagementRoutes,
                    UserChannel,
                    SettingsiPerson,
                    VehicleGuideSetting,
                    ChannelEvent,
                    CommunicationSetting,
                    GetAddress,
                    UserAuthorisation,
                    MapVapQuestionDefinition,
                    CoverDefinitions,
                    DisplaySettings,
   					OverrideRatingQuestions,
                    ComponentDefinitionRoutes,
                    ComponentQuestionAnswerRoutes,
                    ComponentQuestionDefinitionsRoutes,
                    ChannelTemplates,
                    EmailCommunicationSettings,
                    SMSCommunicationSettings,
                    RatingRuleHeaders,
                    RatingRuleHeaderCalculations,
                    SmartPolicy,
                    FormBuilder,
                    FormBuilderDetail,
                    CimsDataSync,
                    QuoteItems,
                    ProductPricingStructure,
                    LogReader,
                    DataView,
                    QuoteItems,
                    PolicyBinding,
                    SettingsiRateUser,
                    Notification
                };
            }
        }
    }
}