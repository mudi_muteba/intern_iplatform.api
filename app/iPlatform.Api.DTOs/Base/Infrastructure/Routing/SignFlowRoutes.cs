﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;
using iPlatform.Api.DTOs.SignFlow;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class SignFlowRoutes : IDefineRoutes<ListSignFlowDocumentDto>, IDefineRoutes<SignFlowDocumentDto>
    {

        private const string IdPlaceholder = "{id:int}";


        private const string ReferencePlaceholder = "{reference:string}";

        public void CreateLinks(ListSignFlowDocumentDto resource)
        {
            throw new NotImplementedException();
        }

        public void CreateLinks(SignFlowDocumentDto resource)
        {
             resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string GetSignFlowDocumentByDocId(int id)
        {
            var idPart = GetSignFlowDocument.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public RouteDefinition GetSignFlowDocument
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/signflowbydocid", IdPlaceholder),
                    "Get signflow by document id");
            }
        }

        public RouteCategoryDefinition Definition { get; set; }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", "signflow", IdPlaceholder),
                    "Get a digital document by Id", typeof(SignFlowDocumentDto));
            }
        }

        public RouteDefinition GetByDocId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", "signflow", IdPlaceholder),
                    "Get a digital document by Id", typeof(SignFlowDocumentDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", "signflow", 
                    "Initiate ditial document signature with signflow", typeof(CreateSignFlowDocumentDto));
            }
        }

        public RouteDefinition PostMultiple
        {
            get
            {
                return new RouteDefinition("POST", "signflowmultiple",
                    "Initiate PostMultiple ditial documents signature with signflow", typeof(CreateMultipleSignFlowDocumentDto));
            }
        }

        public RouteDefinition GetBrokerNoteReport
        {
            get
            {
                return new RouteDefinition("POST", string.Concat("signflow/getbrokernotereport/"),
                    "Get Broker note report", typeof(BrokerNoteReportCriteriaDto));
            }
        }

        public RouteDefinition GetPopiDocReport
        {
            get
            {
                return new RouteDefinition("POST", string.Concat("signflow/getpopidocreport/"),
                    "Get Popi Doc report", typeof(PopiDocReportCriteriaDto));
            }
        }

        public RouteDefinition GetDebitOrderReport
        {
            get
            {
                return new RouteDefinition("POST", string.Concat("signflow/getdebitorderreport/"),
                    "Get debit order report", typeof(DebitOrderReportCriteriaDto));
            }
        }
      
        public RouteDefinition GetSlaReport
        {
            get
            {
                return new RouteDefinition("POST", string.Concat("signflow/getslareport/"),
                    "Get sla report", typeof(SlaReportCriteriaDto));
            }
        }
        
        public RouteDefinition GetRecordOfAdviceReport
        {
            get
            {
                return new RouteDefinition("POST", string.Concat("signflow/getrecordofadvicereport/"),
                    "Get sla report", typeof(RecordOfAdviceReportCriteriaDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}", "signflow"),
                    "Update digital signature", typeof(EditSignFlowDocumentDto));
            }
        }

        public RouteDefinition PutUpdateStatus
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}", "signflowstatus"),
                    "Update digital signature status", typeof(UpdateSignFlowDocumentDto));
            }
        }

        public RouteDefinition GetDigitalSignatureDocumentUpdateStatus
        {
            get
            {
                return new RouteDefinition("POST", "/signflowdocumentstatus",
                    "Update digital signature status", typeof(UpdateEventHandlerSignFlowDocumentDto));
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/", "signflow", IdPlaceholder),
                    "List of signflow", typeof(PagedResultDto<SignFlowDocumentDto>));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Post,
                Put,
                GetById,
                Get,
                GetByDocId,
                GetSignFlowDocument,
                PutUpdateStatus,
                GetDigitalSignatureDocumentUpdateStatus,
                GetBrokerNoteReport,
                GetPopiDocReport,
                GetDebitOrderReport,
                GetSlaReport,
                GetRecordOfAdviceReport,
                PostMultiple
            };
        }
    }
}
