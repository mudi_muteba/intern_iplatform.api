using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Users.Authentication;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class AuthenticateRoutes : IDefineRoutes
    {
        public string RoutePrefix
        {
            get { return "/authenticate"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", RoutePrefix, "Authentication documentation");
            }
        }

        public RouteDefinition Post
        {
            get { return new RouteDefinition("POST", RoutePrefix, "Authenticates a caller", typeof(AuthenticationRequestDto)); }
        }

        public RouteDefinition ByToken
        {
            get { return new RouteDefinition("POST", string.Format("{0}/token", RoutePrefix), "Authenticates a caller by API token, & updates the active channel id", typeof(AuthenticationRequestDto)); }
        }

        public RouteDefinition GetIsAlive
        {
            get { return new RouteDefinition("GET", "/IsAlive", "Is Alive"); }
        }
        public RouteDefinition Get
        {
            get { return new RouteDefinition("GET", "/", "Authentication"); }
        }

        public RouteDefinition GetApiVersions
        {
            get { return new RouteDefinition("GET","/apiversions", "Retrieves api version deployed"); }
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Authentication", "Authentication before using the API", Options.Route);
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Post
            };
        }

        public void CreateLinks(Resource resource)
        {
            throw new NotImplementedException();
        }
    }
}