﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using System;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class SettingsQuoteUploadRoutes : IDefineRoutes<SettingsQuoteUploadDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string channelIdPlaceholder = "{channelid:int}";
        public string PluralRoutePrefix
        {
            get { return "/settingsquoteuploads"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/settingsquoteupload"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Quote Upload Settings", "The entry point for Quote Upload Settings", Get.Route); }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "List of the Quote Upload Settings", typeof(ListResultDto<SettingsQuoteUploadDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Quote Upload Setting by Id", typeof(SettingsQuoteUploadDto));
            }
        }

        public RouteDefinition GetByChannelId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", PluralRoutePrefix, channelIdPlaceholder),
                    "List of the Quote Upload Settings by channelId", typeof(ListResultDto<SettingsQuoteUploadDto>));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Update a Quote Upload Setting", typeof(EditSettingsQuoteUploadDto));
            }
        }
        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a Quote Upload Setting", typeof(CreateSettingsQuoteUploadDto));
            }
        }
        public RouteDefinition SaveMultiple
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/multiple", PluralRoutePrefix),
                    "Save Multiple Quote Upload Settings", typeof(SaveSettingsQuoteUploadsDto));
            }
        }

        public RouteDefinition Delete
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/delete", SingularRoutePrefix, idPlaceholder),
                    "Disable a Quote Upload Setting by Id", typeof(DeleteSettingsQuoteUploadDto));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search Quote Upload Setting", typeof(SearchSettingsQuoteUploadDto));
            }
        }
        public RouteDefinition SearchNoPagination
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/searchnopagination", PluralRoutePrefix),
                    "Search Quote Upload Setting", typeof(SearchSettingsQuoteUploadDto));
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Channels documentation");
            }
        }

        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix), "List of the settings Quote Upload", typeof(PagedResultDto<SettingsQuoteUploadDto>));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetById,
                GetByChannelId,
                Post,
                SaveMultiple,
                Delete,
                Put,
                GetNoPagination,
                SearchNoPagination
            };
        }

        public void CreateLinks(SettingsQuoteUploadDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateDeleteById(int id)
        {
            var idPart = Delete.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByChannelId(int channelId)
        {
            var idPart = GetByChannelId.Route.Replace(channelIdPlaceholder, channelId.ToString());

            return idPart;
        }
    }
}
