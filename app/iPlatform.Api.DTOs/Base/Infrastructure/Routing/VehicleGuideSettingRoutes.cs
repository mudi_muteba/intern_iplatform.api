﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Lookups.VehicleGuide.VehicleGuideSetting;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class VehicleGuideSettingRoutes : IDefineRoutes<VehicleGuideSettingDto>
    {
        private const string vehicleGuidSettingIdPlaceholder = "{vehicleGuidSettingId:int}";
        private const string IdPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string PluralRoutePrefix
        {
            get { return "vehicleGuidSettings"; }
        }

        public string SingularRoutePrefix
        {
            get { return "vehicleGuidSetting"; }
        }

        public void CreateLinks(VehicleGuideSettingDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }

        public void CreateLinks(ListVehicleGuideSettingDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString());
        }

        public string CreateDeleteById(int id)
        {
            var idPart = Delete.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePutById(int id)
        {
            var idPart = Put.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        #region [ Route Definitions ]

        public RouteDefinition Get
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", SingularRoutePrefix) + "{id}", "Get Vehicle Guide Setting"); }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Get Vehicle Guide Setting by Id", typeof(VehicleGuideSettingDto));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder), "List of the Vehicle Guide Settings", typeof(PagedResultDto<ListVehicleGuideSettingDto>));
            }
        }

        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix), "List of the Vehicle Guide Settings", typeof(PagedResultDto<ListVehicleGuideSettingDto>));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), "Search vehicle guide setting", typeof(PagedResultDto<VehicleGuideSettingDto>));
            }
        }

        public RouteDefinition PostMultiple
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/savemultiple", PluralRoutePrefix), "save multiple vehicle guide setting", typeof(SaveMultipleVehicleGuideSettingDto));
            }
        }

        public RouteDefinition Delete
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/delete", SingularRoutePrefix, IdPlaceholder),
                    "Disable a Quote Upload Setting by Id", typeof(DisableVehicleGuideSettingDto));
            }
        }
        
        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", SingularRoutePrefix,
                    "Create Vehicle Guide Setting", typeof(CreateVehicleGuideSettingDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Update a Vehicle Guide Setting", typeof(EditVehicleGuideSettingDto));
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "vehicle Guide Setting documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Vehicle Guide Setting", "Authentication before using the API", Options.Route);
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>()
            {
                Get,
                GetById,
                GetWithPagination,
                GetNoPagination,
                Delete,
                Post,
                Put,
                Search,
                PostMultiple
            };
        }

        #endregion
    }
}
