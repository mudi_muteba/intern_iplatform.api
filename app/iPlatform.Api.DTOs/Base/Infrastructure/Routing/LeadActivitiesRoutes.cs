using System.Collections.Generic;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class LeadActivitiesRoutes : IDefineRoutes<ListLeadActivityDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return "/leadactivities"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/leadactivity"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "LeadActivities", "The entry point for LeadActivity", Get.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "LeadActivities");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the LeadActivities", typeof(PagedResultDto<ListLeadActivityDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a LeadActivity by Id", typeof(ListLeadActivityDto));
            }
        }

        public RouteDefinition GetCount
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/count", PluralRoutePrefix),
                    "List of the LeadActivities");
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the LeadActivities with pagination", typeof(PagedResultDto<ListLeadActivityDto>));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "LeadActivity search", typeof(LeadActivitySearchDto));
            }
        }
        public RouteDefinition PostSearchNoPagination
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/searchnopagination", PluralRoutePrefix),
                    "LeadActivity search", typeof(LeadActivitySearchDto));
            }
        }
        public RouteDefinition PutDisable
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                    "Disable a Lead activity");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetById,
                GetCount,
                GetWithPagination,
                PostSearch,
                PutDisable,
                PostSearchNoPagination

            };
        }

        public void CreateLinks(ListLeadActivityDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(LeadActivityDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
    }
}