﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.ProposalDeclines;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ProposalDeclineRoutes : IDefineRoutes<ProposalDeclineDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string searchPlaceholder = "{search}";

        public string SingularRoutePrefix
        {
            get { return "/proposaldecline"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/proposaldeclines"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "ProposalDecline", "The entry point for JsonStorage", GetById.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "ProposalDecline");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a proposal decline entry by Id", typeof(ProposalDeclineDto));
            }
        }

        public RouteDefinition GetByPartyId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/party/{1}", PluralRoutePrefix, idPlaceholder),
                    "Get a Proposal decline entry by PartyId", typeof(ListResultDto<ProposalDeclineDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", SingularRoutePrefix),
                    "Save a proposal decline entry", typeof(ProposalDeclineDto));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search a proposal decline entry by ProposalDeclineSearchDto", typeof(ProposalDeclineSearchDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetByPartyId,
                Post,
                PostSearch
            };
        }

        public void CreateLinks(ProposalDeclineDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByPartyId(int id)
        {
            var idPart = GetByPartyId.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

    }
}
