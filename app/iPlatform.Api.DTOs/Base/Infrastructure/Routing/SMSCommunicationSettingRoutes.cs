﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Admin.SMSCommunicationSettings;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class SMSCommunicationSettingRoutes : IDefineRoutes<SMSCommunicationSettingDto>
    {
        private const string IdPlaceholder = "{id:int}";
        private const string PageNumberPlaceHolder = "{pageNumber:int}";
        private const string PageSizePlaceHolder = "{pageSize:int}";

        public string PluralRoutePrefix
        {
            get { return "SMSCommunicationSettings"; }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("SMSCommunicationSetting/{0}", IdPlaceholder); }
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateDeleteById(int id)
        {
            var idPart = PutDelete.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }
        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get Communication setting by Id", typeof(SMSCommunicationSettingDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/createmultiple", PluralRoutePrefix),
                    "Create multiple SMS Communication Settings", typeof(CreateMultipleSMSCommunicationSettingsDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/delete", SingularRoutePrefix), "Delete an Communication setting");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Communication setting", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Communication setting documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "List of the Communication setting", typeof(PagedResultDto<SMSCommunicationSettingDto>));
            }
        }
        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), "Search Communication setting", typeof(PagedResultDto<SMSCommunicationSettingDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, PageNumberPlaceHolder, PageSizePlaceHolder), "List of the Communication setting", typeof(PagedResultDto<SMSCommunicationSettingDto>));
            }
        }

        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix), "List of the Communication setting", typeof(PagedResultDto<SMSCommunicationSettingDto>));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetById,
                Options,
                Post,
                Get,
                PutDelete,
                GetWithPagination,
                GetNoPagination,
                Search
            };
        }

        public string CreateGetWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(PageNumberPlaceHolder, pageNumber.ToString())
                .Replace(PageSizePlaceHolder, pageSize.ToString());
        }

        public void CreateLinks(SMSCommunicationSettingDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }
    }
}