using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Users.UserAuthorisationGroups;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class UserAuthorisationRoutes : IDefineRoutes
    {
        private const string IdPlaceholder = "{id:int}";
        private const string UserIdPlaceholder = "{userid:int}";
        private const string ChannelIdPlaceholder = "{channelid:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string SingularRoutePrefix
        {
            get { return "/userauthorisations"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/userauthorisation"; }
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePutById(int id)
        {
            var idPart = Put.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateDeleteById(int id)
        {
            var idPart = PutDelete.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }
        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get settings iPerson by Id", typeof(ListUserAuthorisationGroupDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", PluralRoutePrefix,
                    "Create settings iPerson", typeof(CreateUserAuthorisationGroupDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", PluralRoutePrefix, IdPlaceholder), "Update a User Authorisation Group", typeof(EditUserAuthorisationGroupDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/delete", SingularRoutePrefix, IdPlaceholder), "Delete an User Authorisation Group", typeof(DeleteUserAuthorisationGroupDto));
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "User Authorisation Group", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "User Authorisation Groups documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "List of the User Authorisation Group", typeof(PagedResultDto<ListUserAuthorisationGroupDto>));
            }
        }
        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), "Searchsettings iPerson", typeof(PagedResultDto<ListUserAuthorisationGroupDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder), "List of the settings iPerson", typeof(PagedResultDto<ListUserAuthorisationGroupDto>));
            }
        }

        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix), "List of the settings iPerson", typeof(PagedResultDto<ListUserAuthorisationGroupDto>));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {

                GetById,
                Options,
                Post,
                Get,
                Put,
                PutDelete,
                GetWithPagination,
                GetNoPagination,
                Search
            };
        }

        public string CreateGetWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString());
        }

        public void CreateLinks(ListUserAuthorisationGroupDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }

    }
}