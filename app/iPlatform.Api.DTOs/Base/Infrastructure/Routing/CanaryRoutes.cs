using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Escalations;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class CanaryRoutes : IDefineRoutes<EscalationHistoryDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";
        
        public string PluralRoutePrefix
        {
            get { return "/Canaries"; }
        }

        public RouteDefinition GetAllWithPagination
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder),
                    "List of Canaries", typeof(PagedResultDto<ListUserDto>));
            }
        }

        public string CreateGetAllWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetAllWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Used for creating an escalation plan execution history item", typeof(CreateEscalationDto));
            }
        }

        public void CreateLinks(EscalationHistoryDto resource)
        {
            throw new System.NotImplementedException();
        }

        public string CreateGetById(int id)
        {
            throw new System.NotImplementedException();
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Canaries", "The entry point for canaries", GetAllWithPagination.Route); }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetAllWithPagination,
                Post
            };
        }
    }
}