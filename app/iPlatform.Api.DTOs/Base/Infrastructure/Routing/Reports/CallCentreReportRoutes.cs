using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Reports
{
    public class CallCentreReportRoutes : IDefineRoutes<CallCentreReportCriteriaDto>
    {
        public string SingularRoutePrefix
        {
            get { return "/reports/callcentre/"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", "ReportsCallCenter", "Call Centre Lead Management Report API Documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "ReportsCallCenter", "Entry Point for Call Centre Lead Management Reports", Options.Route);
            }
        }

        public RouteDefinition GetCallCentreLeadManagementReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}{1}", SingularRoutePrefix, "leadmanagement"), "Get Call Centre Lead Management Report", typeof(CallCentreReportCriteriaDto));
            }
        }

        public RouteDefinition GetCallCentreOutboundRatiosReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}{1}", SingularRoutePrefix, "outboundratios"), "Get Call Centre Outbound Ratios Report", typeof(CallCentreReportCriteriaDto));
            }
        }

        public RouteDefinition GetCallCentreSalesManagementReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}{1}", SingularRoutePrefix, "salesmanagement"), "Get Call Centre Sales Management Report", typeof(CallCentreReportCriteriaDto));
            }
        }

        public RouteDefinition GetCallCentreAgentPerformanceReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}{1}", SingularRoutePrefix, "agentperformance"), "Get Call Centre Agent Performance Report", typeof(CallCentreReportCriteriaDto));
            }
        }

        public RouteDefinition GetCallCentreAgentPerformanceByProductReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}{1}", SingularRoutePrefix, "agentperformancebyproduct"), "Get Call Centre Agent Performance By Product Report", typeof(CallCentreReportCriteriaDto));
            }
        }

        public RouteDefinition GetCallCentreInsurerQuoteBreakdownReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}{1}", SingularRoutePrefix, "insurerquotebreakdown"), "Get Call Centre Insurer Quote Breakdown Report", typeof(CallCentreReportCriteriaDto));
            }
        }

        public RouteDefinition GetCallCentreInsurerDataReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}{1}", SingularRoutePrefix, "insurerdata"), "Get Call Centre Insurer Data Report", typeof(CallCentreReportCriteriaDto));
            }
        }

        public RouteDefinition GetCallCentreDetailedInsurerUploadReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}{1}", SingularRoutePrefix, "detailedinsurerupload"), "Get Detailed Insurer Upload Report", typeof(CallCentreReportCriteriaDto));
            }
        }

        public RouteDefinition GetCallCentreUserReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}{1}", SingularRoutePrefix, "user"), "Get Detailed Insurer Upload Report", typeof(CallCentreReportCriteriaDto));
            }
        }

        public RouteDefinition GetCallCentreBrokerQuoteUploadsReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}{1}", SingularRoutePrefix, "brokerquoteuploads"), "Get Detailed Insurer Upload Report", typeof(CallCentreReportCriteriaDto));
            }
        }

        public RouteDefinition GetCallCentreManagerLevelQuoteUploadsReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}{1}", SingularRoutePrefix, "managerlevelquoteuploads"), "Get Detailed Insurer Upload Report", typeof(CallCentreReportCriteriaDto));
            }
        }


        public string CreateGetById(int id)
        {
            return string.Empty;
        }


        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetCallCentreLeadManagementReport,
                GetCallCentreOutboundRatiosReport,
                GetCallCentreSalesManagementReport,
                GetCallCentreAgentPerformanceReport,
                GetCallCentreInsurerQuoteBreakdownReport,
                GetCallCentreInsurerDataReport
            };
        }

        public void CreateLinks(CallCentreReportCriteriaDto resource)
        {
            
        }
    }
}