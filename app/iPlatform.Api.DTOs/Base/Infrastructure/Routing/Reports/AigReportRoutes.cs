﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Reports.AIG.AllRisk.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Building.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Content.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Lead;
using iPlatform.Api.DTOs.Reports.AIG.Lead.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Motor.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.ReportCriteria;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceIntegration;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceLogSummery.Criteria;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Reports
{
    public class AigReportRoutes : IDefineRoutes<AigReportLeadResultDto>
    {
        public string SingularRoutePrefix
        {
            get { return "/reports/aig"; }
        }

        public void CreateLinks(AigReportLeadResultDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }


        public string CreateGetByFileName(string fileName)
        {
            throw new NotImplementedException();
        }


        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public RouteDefinition GetReportList
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/list/", SingularRoutePrefix),
                    "List of the reports");
            }
        }

        public RouteDefinition GetAigReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/extract/", SingularRoutePrefix),
                    "Get AIG Report", typeof(AigReportCriteriaDto));
            }
        }

        public RouteDefinition GetAigReportLeadExtract
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/leadextract/", SingularRoutePrefix), 
                    "Get AIG Report Lead Extract", typeof(AigReportLeadExtractCriteriaDto));
            }
        }

        public RouteDefinition GetAigReportMotorExtract
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/motorextract/", SingularRoutePrefix),
                    "Get AIG Report Motor Extract", typeof(AigReportMotorExtractCriteriaDto));
            }
        }

        public RouteDefinition GetAigReportSalesForceIntegrationExtract
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/salesforcextract/", SingularRoutePrefix),
                    "Get AIG Report Sales Force Extract", typeof(AigReportSalesForceIntegrationExtractCriteriaDto));
            }
        }

        public RouteDefinition GetAigReportSalesForceLogSummeryExtract
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/salesforcelogsummeryextract/", SingularRoutePrefix),
                    "Get AIG Report Sales Force Log Summery Extract", typeof(AigReportSalesForceLogSummeryExtractCriteriaDto));
            }
        }

        public RouteDefinition GetAigReportAllRiskExtract
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/allriskextract/", SingularRoutePrefix),
                    "Get AIG Report all risk Extract", typeof(AigReportAllRiskExtractCriteriaDto));
            }
        }

        public RouteDefinition GetAigReportContentExtract
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/contentextract/", SingularRoutePrefix),
                    "Get AIG Report content Extract", typeof(AigReportContentExtractCriteriaDto));
            }
        }

        public RouteDefinition GetAigReportBuildingExtract
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/buildingextract/", SingularRoutePrefix),
                    "Get AIG Report building Extract", typeof(AigReportBuildingExtractCriteriaDto));
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "AIGReports", "The entry point for ASIGReports", GetReportList.Route);
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetReportList,
                GetAigReport,
                GetAigReportLeadExtract,
                GetAigReportMotorExtract,
                GetAigReportAllRiskExtract,
                GetAigReportContentExtract,
                GetAigReportBuildingExtract
            };
        }
    }
}
