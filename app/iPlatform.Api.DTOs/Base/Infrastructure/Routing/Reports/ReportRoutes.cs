using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Reports.Base.Criteria;
using iPlatform.Api.DTOs.Reports;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Reports
{
    public class ReportRoutes : IDefineRoutes<ReportDto>
    {
        public string PluralRoutePrefix
        {
            get { return "/reports"; }
        }

        public RouteDefinition GetReports
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix), "Get a list of all available reports", typeof(IList<ReportDto>));
            }
        }

        public RouteDefinition GetReportsByCriteria
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/criteria", PluralRoutePrefix), "Get a list of all available reports by channel", typeof(ReportCriteriaDto));
            }
        }

        public RouteDefinition GenerateReports
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/generate", PluralRoutePrefix), "Generates reports and returns their URIs", typeof(ReportGeneratorResponseDto));
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", "Reports", "Reports API Documentation");
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/createmultiple", PluralRoutePrefix),
                    "Create multiple Reports", typeof(CreateMultipleReportsDto));
            }
        }

        public RouteDefinition ValidateReportCampaigns
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/validatereportcampaigns", PluralRoutePrefix),
                    "Validate Campaigns and report list", typeof(ValidateReportCampaignsDto));
            }
        }

        

        public string CreateGetById(int id)
        {
            return string.Empty;
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "Reports", "The entry point for Reports", GetReports.Route);
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetReports,
                GetReportsByCriteria,
                GenerateReports,
                Post
            };
        }

        public void CreateLinks(ReportDto resource)
        {
            
        }
    }
}