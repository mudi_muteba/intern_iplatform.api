using System.Collections.Generic;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.ContactUs;
using iPlatform.Api.DTOs.Reports.ComparativeQuote;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Reports
{
    public class ComparativeQuoteReportRoutes : IDefineRoutes<ComparativeQuoteReportContainerDto>
    {
        public string SingularRoutePrefix
        {
            get { return "/reports/comparativequote"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", "ReportsComparativeQuote", "Comparative Quote Report API Documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "ReportsComparativeQuote", "Entry Point for Comparative Quote Report", GetComparativeQuoteReport.Route);
            }
        }

        public RouteDefinition GetComparativeQuoteReport
        {
            get
            {
                return new RouteDefinition("POST", SingularRoutePrefix, "Get Comparative Quote Report", typeof(ComparativeQuoteReportCriteriaDto));
            }
        }

        public RouteDefinition EmailComparativeQuoteReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/email", SingularRoutePrefix), "Send a Comparative Quote Schedule to an e-mail address.", typeof(ComparativeQuoteReportEmailDto));
            }
        }

        public RouteDefinition ContactUsEmail
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/contactus", SingularRoutePrefix), "Send a contact us e-mail address.", typeof(ContactUsEmailDto));
            }
        }


        public string CreateGetById(int id)
        {
            return string.Empty;
        }


        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetComparativeQuoteReport,
                EmailComparativeQuoteReport,
                ContactUsEmail
            };
        }

        public void CreateLinks(ComparativeQuoteReportContainerDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
    }
}