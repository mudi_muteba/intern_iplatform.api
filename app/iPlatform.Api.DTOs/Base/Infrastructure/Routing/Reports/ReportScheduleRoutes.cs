using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.ReportSchedules;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Reports
{
    public class ReportScheduleRoutes : IDefineRoutes<ReportScheduleDto>
    {
        private const string IdPlaceholder = "{id:int}";

        public string Route
        {
            get { return "/ReportSchedule"; }
        }

        public RouteDefinition GetById
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", Route) + IdPlaceholder, "Report Schedule"); }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", Route), "Searches for report schedules and returns a result", typeof(SearchReportScheduleDto));
            }
        }

        public RouteDefinition Save
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}", Route), "Saves a report schedule and returns a result", typeof(SaveReportScheduleDto));
            }
        }

        public RouteDefinition Invoke
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/invoke", Route), "Invokes a report schedule", typeof(InvokeReportScheduleDto));
            }
        }

        public RouteDefinition Delete
        {
            get
            {
                return new RouteDefinition("DELETE", string.Format("{0}/", Route) + IdPlaceholder, "Deletes a report schedule", typeof(DeleteReportScheduleDto));
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", "Reports", "Reports API Documentation");
            }
        }

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(IdPlaceholder, id.ToString());
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "Reports", "The entry point for Reports", "");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetById,
                Search,
                Save,
                Invoke,
                Delete
            };
        }

        public void CreateLinks(ReportScheduleDto resource)
        {

        }
    }
}