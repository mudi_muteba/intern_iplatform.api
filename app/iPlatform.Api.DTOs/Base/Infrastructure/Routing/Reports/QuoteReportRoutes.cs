using System.Collections.Generic;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Reports.Quote;
using iPlatform.Api.DTOs.Reports.Quote.Criteria;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Reports
{
    public class QuoteReportRoutes : IDefineRoutes<QuoteReportDto>
    {
        public string SingularRoutePrefix
        {
            get { return "/reports/quote"; }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", "ReportsQuote", "Quote Report API Documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "ReportsQuote", "Entry Point for Quote Report", GetQuoteReport.Route);
            }
        }

        public RouteDefinition GetQuoteReport
        {
            get
            {
                return new RouteDefinition("POST", SingularRoutePrefix, "Get Quote Report", typeof(QuoteReportCriteriaDto));
            }
        }

        public RouteDefinition GetQuoteReportHeader
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/header/", SingularRoutePrefix), "Get Quote Report Header", typeof (QuoteReportCriteriaDto));
            }
        }

        public RouteDefinition GetQuoteReportSettings
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/settings/", SingularRoutePrefix), "Get Quote Report Header", typeof(QuoteReportCriteriaDto));
            }
        }

        public RouteDefinition GetQuoteReportSummary
        { 
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/summary/", SingularRoutePrefix), "Get Quote Report Header", typeof(QuoteReportCriteriaDto));
            }
        }

        public RouteDefinition GetQuoteReportAssets
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/assets/", SingularRoutePrefix), "Get Quote Report Header", typeof(QuoteReportCriteriaDto));
            }
        }

        public RouteDefinition GetQuoteReportTotals
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/totals/", SingularRoutePrefix), "Get Quote Report Header", typeof(QuoteReportCriteriaDto));
            }
        }

        public RouteDefinition GetQuoteReportValueAddedProducts
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/valueaddedproducts/", SingularRoutePrefix), "Get Quote Report Header", typeof(QuoteReportCriteriaDto));
            }
        }


        public RouteDefinition EmailQuoteReport
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/email", SingularRoutePrefix), "Send a Quote Schedule to an e-mail address.", typeof(QuoteReportEmailDto));
            }
        }

        public string CreateGetById(int id)
        {
            return string.Empty;
        }


        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                GetQuoteReport,
                GetQuoteReportHeader,
                GetQuoteReportSettings,
                GetQuoteReportSummary,
                GetQuoteReportAssets,
                GetQuoteReportTotals,
                GetQuoteReportValueAddedProducts,
                EmailQuoteReport
            };
        }

        public void CreateLinks(QuoteReportDto resource)
        {
            
        }
    }
}