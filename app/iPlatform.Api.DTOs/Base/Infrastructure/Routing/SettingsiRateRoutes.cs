﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class SettingsiRateRoutes : IDefineRoutes<SettingsiRateDto>
    {
        private const string settingsiRateIdPlaceholder = "{settingsiRateId:int}";
        private const string IdPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("settingsiRates", settingsiRateIdPlaceholder); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("settingsiRate/{0}", IdPlaceholder); }
        }
        public string SingularRoutePrefixCreate
        {
            get { return "settingsiRate/"; }
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePutById(int id)
        {
            var idPart = Put.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString());
        }

        public string CreateDeleteById(int id)
        {
            var idPart = PutDelete.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get settings iRate by Id", typeof(SettingsiRateDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", SingularRoutePrefixCreate,
                    "Create settings iRate", typeof(CreateSettingsiRateDto));
            }
        }

        public RouteDefinition PostMultiple
        {
            get
            {
                return new RouteDefinition("POST", "settingsirate/multiple",
                    "Save multiple settings iRate", typeof(SaveMultipleSettingsiRateDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/", SingularRoutePrefix), "Update a setting iRate", typeof(EditSettingsiRateDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/delete", SingularRoutePrefixCreate, IdPlaceholder), "Delete an iRate setting");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Settings Ireate", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "settings iRate documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "List of the settings iRate", typeof(PagedResultDto<ListSettingsiRateDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder), "List of the settings iRate", typeof(PagedResultDto<ListSettingsiRateDto>));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search Quote iRate Setting", typeof(SearchSettingsiRateDto));
            }
        }

        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix), "List of the settings iRate", typeof(PagedResultDto<ListSettingsiRateDto>));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {

                GetById,
                Options,
                Post,
                Get,
                Put,
                PutDelete,
                GetWithPagination,
                Search,
                GetNoPagination,
                PostMultiple
            };
        }

        public void CreateLinks(SettingsiRateDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }

        public void CreateLinks(ListSettingsiRateDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

    }
}
