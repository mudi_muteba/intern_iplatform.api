using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Products;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ProductFeeDefinitionRoutes 
    {
        private const string idPlaceholder = "{id:int}";
        private const string productIdPlaceholder = "{productid:int}";
        private const string channelIdPlaceholder = "{channelId:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";


        public string SingularRoutePrefix
        {
            get { return String.Format("/product/{0}/fee/{1}", productIdPlaceholder, idPlaceholder); }
        }

        public string PluralRoutePrefix
        {
            get { return String.Format("/product/{0}/fees", productIdPlaceholder); }
        }

        public string SingularRoutePrefixChannel
        {
            get { return String.Format("/product/{0}/fee/{1}/channel/{2}", productIdPlaceholder, idPlaceholder, channelIdPlaceholder); }
        }

        public string PluralRoutePrefixChannel
        {
            get { return String.Format("/product/{0}/fees/Channel/{1}", productIdPlaceholder, channelIdPlaceholder); }
        }

        public RouteCategoryDefinition Definition { get; set; }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", "productfees"), "Search product fees", typeof(PagedResultDto<ListProductFeeDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", SingularRoutePrefix, "Get fee by id for product");
            }
        }

        public RouteDefinition GetByIdChannelId
        {
            get
            {
                return new RouteDefinition("GET", SingularRoutePrefixChannel, "Get fee by id for product by channel id");
            }
        }

        public RouteDefinition GetAll
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", "productfees", pageNumberPlaceHolder, pageSizePlaceHolder), "Get fees");
            }
        }

        public RouteDefinition GetProductFeeById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", "productfees", idPlaceholder), "Get fee by id");
            }
        }


        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", PluralRoutePrefix, "Get fees for a product");
            }
        }

        public RouteDefinition GetByChannelId
        {
            get
            {
                return new RouteDefinition("GET", PluralRoutePrefixChannel, "Get fees for a product by channel id");
            }
        }


        public string CreatefeesByIdUrl(int productId, int feeId)
        {
            return GetById.Route
                .Replace(productIdPlaceholder, productId.ToString())
                .Replace(idPlaceholder, feeId.ToString());
        }

        public string CreatefeesByIdChannelIdUrl(int productId, int feeId, int channelId)
        {
            return GetByIdChannelId.Route
                .Replace(productIdPlaceholder, productId.ToString())
                .Replace(idPlaceholder, feeId.ToString())
                .Replace(channelIdPlaceholder, channelId.ToString());
        }

        public string CreateFeesUrl(int productId)
        {
            return Get.Route.Replace(productIdPlaceholder, productId.ToString());
        }

        public string CreateFeesByChannelIdUrl(int productId, int channelId)
        {
            return GetByChannelId.Route.Replace(productIdPlaceholder, productId.ToString()).Replace(channelIdPlaceholder, channelId.ToString());
        }

        public string CreateGetById(int productId, int feeId)
        {
            return SingularRoutePrefix
                .Replace(productIdPlaceholder, productId.ToString())
                .Replace(idPlaceholder, feeId.ToString());
        }


        public void CreateLinks(int productId, ProductFeeDto fee)
        {
            fee.AddLink("self", CreateGetById(productId, fee.Id));
            fee.AddLink("parent", SystemRoutes.Products.CreateGetById(productId));
        }

        public void CreateLinks(ProductFeeDto fee)
        {
            fee.AddLink("self", CreateGetById(fee.ProductId, fee.Id));
            fee.AddLink("parent", SystemRoutes.Products.CreateGetById(fee.ProductId));
        }

    }
}