﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class SettingsiPersonRoutes : IDefineRoutes<SettingsiPersonDto>,  IDefineRoutes<ListSettingsiPersonDto>
    {
        private const string IdPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string PluralRoutePrefix
        {
            get { return "settingsiPersons"; }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("settingsiPerson/{0}", IdPlaceholder); }
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePutById(int id)
        {
            var idPart = Put.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateDeleteById(int id)
        {
            var idPart = PutDelete.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }
        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get settings iPerson by Id", typeof(SettingsiPersonDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", PluralRoutePrefix,
                    "Create settings iPerson", typeof(CreateSettingsiPersonDto));
            }
        }
        public RouteDefinition SaveMultiple
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/multiple",PluralRoutePrefix),
                    "Save multiple settings iPerson", typeof(SaveMultipleSettingsiPersonDto));
            }
        }
        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", PluralRoutePrefix, IdPlaceholder), "Update a setting iPerson", typeof(EditSettingsiPersonDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/delete", SingularRoutePrefix, IdPlaceholder), "Delete an iPerson setting");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Settings iPerson", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "settings iPerson documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "List of the settings iPerson", typeof(PagedResultDto<ListSettingsiPersonDto>));
            }
        }
        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), "Searchsettings iPerson", typeof(PagedResultDto<ListSettingsiPersonDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder), "List of the settings iPerson", typeof(PagedResultDto<ListSettingsiPersonDto>));
            }
        }

        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix), "List of the settings iPerson", typeof(PagedResultDto<ListSettingsiPersonDto>));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {

                GetById,
                Options,
                Post,
                Get,
                Put,
                PutDelete,
                GetWithPagination,
                GetNoPagination,
                Search
            };
        }

        public string CreateGetWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString());
        }

        public void CreateLinks(SettingsiPersonDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }

        public void CreateLinks(ListSettingsiPersonDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

    }
}
