using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Escalations;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class EventRoutes : IDefineRoutes
    {
        private const string idPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string SingularRoutePrefix
        {
            get { return "/event"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/events"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Event", "Event", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix), "Event documentation");
            }
        }

        public RouteDefinition Get
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", SingularRoutePrefix) + "{id}", "Event"); }
        }

        public RouteDefinition GetAll
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "Get a list of event"); }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), "Event search", typeof(EscalationPlanDto));
            }
        }

        public RouteDefinition SearchWithPagination
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder),
                    "List of events with pagination", typeof(PagedResultDto<EscalationPlanDto>));
            }
        }

        public RouteDefinition GetByName
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", SingularRoutePrefix) + "{name}", "Get events by name"); }
        }

        public RouteDefinition Post
        {
            get { return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix), "Creates an event", typeof(EscalationPlanDto)); }
        }

        public RouteDefinition Put
        {
            get { return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder), "Edit an event's information", typeof(EscalationPlanDto)); }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get an event by Id", typeof(UserDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Post,
                Get,
                GetById,
                GetByName,
                Put
            };
        }

        public string CreateGetAllWithPaginationUrl(int pageNumber, int pageSize)
        {
            return SearchWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateEditUrl(int userId)
        {
            var idPart = Put.Route.Replace(idPlaceholder, userId.ToString());

            return idPart;
        }

        public void CreateLinks(Resource resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
    }
}