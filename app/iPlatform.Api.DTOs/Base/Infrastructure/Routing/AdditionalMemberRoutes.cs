using System.Collections.Generic;
using iPlatform.Api.DTOs.AdditionalMembers;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class AdditionalMemberRoutes : IDefineRoutes<ListAdditionalMemberDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string SingularRoutePrefix
        {
            get { return "additionalmember"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/additionalmembers"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "AdditionalMembers", "The entry point for additional members", Get.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Additional Member");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of additional members", typeof(PagedResultDto<ListAdditionalMemberDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a additional member by Id", typeof(ListAdditionalMemberDto));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of additional members with pagination", typeof(PagedResultDto<ListAdditionalMemberDto>));
            }
        }

        public RouteDefinition GetByProposalDefinitionId
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}", PluralRoutePrefix, idPlaceholder),
                    "Get additional members by proposaldefinitionid");
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a new additional member", typeof(CreateAdditionalMemberDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, idPlaceholder),
                    "Update a additional member", typeof(EditAdditionalMemberDto));
            }
        }

        public RouteDefinition PutDisable
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                    "Disable a additional members");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Get,
                GetById,
                GetWithPagination,
                GetByProposalDefinitionId,
                Post,
                PutDisable,
                Options
            };
        }

        public void CreateLinks(ListAdditionalMemberDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(idPlaceholder, id.ToString());
        }

        public void CreateLinks(Resource resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string PutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string DisableById(int id)
        {
            var idPart = PutDisable.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string GetByProposalDefId(int id)
        {
            var idPart = GetByProposalDefinitionId.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string UpdateById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

    }
}