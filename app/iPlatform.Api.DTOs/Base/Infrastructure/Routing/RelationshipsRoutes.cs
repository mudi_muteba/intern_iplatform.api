using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.Relationships;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class RelationshipsRoutes : IDefineRoutes<RelationshipDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return "/relationships"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/relationship"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Relationships", "The entry point for Relationships", Get.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", SingularRoutePrefix),
                    "Relationships documentation");
            }
        }      

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", SingularRoutePrefix),
                    "List of the Relationships", typeof(PagedResultDto<RelationshipDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Relationship by Id", typeof(RelationshipDto));
            }
        }

        public RouteDefinition GetByPartyId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/Party/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Relationship by Id", typeof(RelationshipDto));
            }
        }
        
        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", SingularRoutePrefix),
                    "Create a new Relationship", typeof(CreateRelationshipDto));
            }
        }

        public RouteDefinition PutUpdate
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Update a Relationship", typeof(EditRelationshipDto));
            }
        }

        public RouteDefinition PutDeleteByPartyId
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/delete", SingularRoutePrefix, idPlaceholder),
                    "Delete a Relationship");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetById,
                GetByPartyId,
                PutDeleteByPartyId,
                Post,
                PutUpdate,
            };
        }

        public void CreateLinks(RelationshipDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByPartyId(int id)
        {
            var idPart = GetByPartyId.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string DeleteByPartyId(int partyid)
        {
            var idPart = PutDeleteByPartyId.Route.Replace(idPlaceholder, partyid.ToString());

            return idPart;
        }

        public string UpdateById(int id)
        {
            var idPart = PutUpdate.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

    }
}