﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.SalesForce;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class SalesForceRoutes : IDefineRoutes<SalesForceLeadDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string SingularRoutePrefix
        {
            get { return "/salesforce"; }
        }

        public string PostByPartyId(int id)
        {
            return PostStatus.Route.Replace(idPlaceholder, id.ToString());
        }
        public string CreateGetById(int id)
        {
            return GetStatus.Route.Replace(idPlaceholder, id.ToString());
        }
        public string CreateUpdateById(int id)
        {
            return PutStatus.Route.Replace(idPlaceholder, id.ToString());
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "SalesForce", "The entry point for SalesForce", Post.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", SingularRoutePrefix),
                    "SalesForce documentation");
            }
        }


        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", SingularRoutePrefix),
                    "Create or Update a SalesForce entry", typeof(SalesForceLeadDto));
            }
        }
        public RouteDefinition PostStatus
        {
            get
            {
                return new RouteDefinition("POST", String.Format("{0}/{1}/status", SingularRoutePrefix, idPlaceholder), "Post Lead to sales force using party Id",
                    typeof(SalesForceLeadDto));
            }
        }        public RouteDefinition PostQuoteStatus
        {
            get
            {
                return new RouteDefinition("POST", String.Format("{0}/{1}/QuoteStatus", SingularRoutePrefix, idPlaceholder), "Post Lead to sales force using party Id",
                    typeof(SalesForceLeadDto));
            }
        }
        public RouteDefinition PostRetry
        {
            get
            {
                return new RouteDefinition("POST", String.Format("{0}/retry", SingularRoutePrefix), "Post retry to sales force using party Id",
                    typeof(SalesForceLeadDto));
            }
        }
        public RouteDefinition PostSalesForceEntry
        {
            get
            {
                return new RouteDefinition("POST", String.Format("{0}/SalesForceEntry", SingularRoutePrefix), "Post retry to sales force using party Id",
                    typeof(SalesForceLeadDto));
            }
        }
        public RouteDefinition PostExternal
        {
            get
            {
                return new RouteDefinition("POST", String.Format("{0}/external", SingularRoutePrefix), "Post external to sales force using party Id",
                    typeof(SalesForceLeadDto));
            }
        }
        public RouteDefinition PutStatus
        {
            get
            {
                return new RouteDefinition("PUT", String.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder), "Post Lead to sales force using party Id",
                    typeof(SalesForceLeadDto));
            }
        }
        public RouteDefinition GetStatus
        {
            get
            {
                return new RouteDefinition("GET", String.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder), "Post Lead to sales force using party Id",
                    typeof(SalesForceLeadDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Post,

            };
        }

        public void CreateLinks(SalesForceEntryDto resource)
        {
            resource.AddLink("self", Post.Route);
            resource.AddLink("self", PostStatus.Route);
        }
        public void CreateLinks(SalesForceLeadDto resource)
        {
            resource.AddLink("self", Post.Route);
            resource.AddLink("self", PostStatus.Route);
        }
    }
}