using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Claims.ClaimsItems;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Claims
{
    public class ClaimsItemRoutes : IDefineRoutes<ClaimsItemDto>, IDefineRoutes<ListClaimsItemDto>
    {
        private const string ClaimsHeaderIdPlaceholder = "{claimsHeaderId:int}";
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("claims/{0}/claimsitems", ClaimsHeaderIdPlaceholder); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("claims/{0}/claimsitems/{1}", ClaimsHeaderIdPlaceholder, idPlaceholder); }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("GET", "ClaimsItems", "The entry point for Claims Items",
                    GetById.Route);
            }

        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS",
                    string.Format("ClaimsHeader/ClaimsItems", PluralRoutePrefix),
                    "Claims items documentation");
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get an ClaimsItem by Id", typeof(ClaimsItemDto));
            }
        }

        public RouteDefinition GetClaimsItem
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get a ClaimsItem", typeof(ClaimsItemDto));
            }
        }

        public RouteDefinition GetClaimsItems
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "Get all ClaimsItems", typeof(ListClaimsItemDto));
            }
        }

        public RouteDefinition CreateClaimsItems
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}", PluralRoutePrefix),
                    "Create ClaimsItems", typeof(CreateClaimsItemsDto));
            }
        }

        public RouteDefinition SaveClaimsItemAnswers
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/SaveAnswers", SingularRoutePrefix),
                    "Save a ClaimsItem Answers", typeof(ClaimsItemDto));
            }
        }

        public RouteDefinition UpdateClaimsItems
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}", PluralRoutePrefix),
                    "Update ClaimsItems", typeof(CreateClaimsItemsDto));
            }
        }

        public RouteDefinition DeleteClaimsItem
        {
            get
            {
                return new RouteDefinition("Delete", string.Format("{0}/Delete", SingularRoutePrefix),
                    "Delete a ClaimsItem");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                GetClaimsItem,
                GetClaimsItems,
                CreateClaimsItems,
                SaveClaimsItemAnswers,
                UpdateClaimsItems,
                DeleteClaimsItem
            };
        }

        public void CreateLinks(ClaimsItemDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.ClaimsHeader.Id, resource.Id));
        }

        public void CreateLinks(ListClaimsItemDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.ClaimsHeaderId, resource.Id));
        }

        public string CreateGetById(int id)
        {
            return PluralRoutePrefix
                .Replace(ClaimsHeaderIdPlaceholder, id.ToString());
        }

        public string CreateGetById(int claimsHeaderId, int claimsItemId)
        {
            return SingularRoutePrefix
                .Replace(ClaimsHeaderIdPlaceholder, claimsHeaderId.ToString())
                .Replace(idPlaceholder, claimsItemId.ToString());
        }

        public string DeleteGetById(int claimsHeaderId, int claimsItemId)
        {
            return SingularRoutePrefix
                .Replace(ClaimsHeaderIdPlaceholder, claimsHeaderId.ToString())
                .Replace(idPlaceholder, claimsItemId.ToString()) + "/Delete";
        }

        public string SaveAnswersById(int claimsHeaderId, int claimsItemId)
        {
            return SingularRoutePrefix
                .Replace(ClaimsHeaderIdPlaceholder, claimsHeaderId.ToString())
                .Replace(idPlaceholder, claimsItemId.ToString()) + "/SaveAnswers";
        }

    }
}