using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Claims.ClaimTypes;
using iPlatform.Api.DTOs.Claims.Questions;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Claims
{
    public class ClaimTypeRoutes : IDefineRoutes<ClaimsTypeDto>
    {
        private const string organizationIdPlaceholder = "{organizationId:int}";
        private const string IdPlaceholder = "{id:int}";
        private const string ProductIdPlaceholder = "{productId:int}";

        public string PluralRoutePrefix
        {
            get { return "/claims/types"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/claims/type"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "ClaimsTypes" , "The entry point for claims types", Get.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "ClaimsTypes");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the claims", typeof(PagedResultDto<ClaimsTypeDto>));
            }
        }

        public RouteDefinition GetByOrganisationId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", PluralRoutePrefix, organizationIdPlaceholder),
                    "Get a Claim by OrganisationId", typeof(ClaimsTypeDto));
            }
        }

        public RouteDefinition GetClaimTypeByProductId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", SingularRoutePrefix, IdPlaceholder, ProductIdPlaceholder),
                    "Get a Claim by OrganisationId", typeof(ClaimsTypeDetailDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetByOrganisationId,
                GetClaimTypeByProductId
            };
        }

        public void CreateLinks(ClaimsTypeDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(List<ClaimsTypeDto> list)
        {
            foreach (var resource in list)
            {
                resource.AddLink("self", CreateGetById(resource.Id));
            }
        }

        public string CreateGetByOrganisationId(int? organizationId)
        {
            var idPart = GetByOrganisationId.Route.Replace(organizationIdPlaceholder, organizationId.ToString());

            return idPart;
        }
        public string CreateGetByProductId(int id, int? productId)
        {
            var idPart = GetClaimTypeByProductId.Route
                .Replace(ProductIdPlaceholder, productId.ToString())
                .Replace(IdPlaceholder, id.ToString());

            return idPart;
        }
        public string CreateGetById(int organizationId)
        {
            var idPart = GetByOrganisationId.Route.Replace(organizationIdPlaceholder, organizationId.ToString());

            return idPart;
        }
    }
}