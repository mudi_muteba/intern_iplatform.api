using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Claims
{
    public class ClaimsHeaderRoutes : IDefineRoutes<ListClaimDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return "/claims/headers"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/claims/header"; }
        }

        public void CreateLinks(ListClaimDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(ClaimsHeaderDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "ClaimsHeaders", "The entry point for claims headers", Get.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Claims Headers Documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the claims", typeof(PagedResultDto<ListClaimDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Claims Header by Id", typeof(ClaimsHeaderDto));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the claims with pagination", typeof(PagedResultDto<ListClaimDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create ClaimHeader", typeof(CreateClaimsHeaderDto));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "ClaimHeader search", typeof(ClaimSearchDto));
            }
        }

        public RouteDefinition PostAccept
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/accept", PluralRoutePrefix),
                    "Accept Claim for workflow use", typeof(AcceptClaimDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, idPlaceholder),
                    "Update a ClaimHeader", typeof(EditClaimDto));
            }
        }

        public RouteDefinition PutDisable
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                    "Disable a claim");
            }
        }

        public RouteDefinition PutAccept
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/accept", SingularRoutePrefix, idPlaceholder),
                    "accept a claim");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetById,
                GetWithPagination,
                Post,
                PostSearch,
                Put,
                PutDisable,
                PutAccept
            };
        }

        public string DisableById(int id)
        {
            var idPart = PutDisable.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string AcceptById(int id)
        {
            var idPart = PutAccept.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string PutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
    }
}