﻿using System.Collections.Generic;

using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.ItcQuestion;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ItcQuestionRoutes : IDefineRoutes<ItcQuestionDefinitionDto>
    {
        private const string M_IdPlaceholder = "{id:int}";
        private const string M_ChannelCodePlaceholder = "{channelCode}";
        private const string M_ChannelPathPlaceholder = "channel";
        private const string M_ChannelCodePathPlaceholder = "channelcode";

        public string PluralRoutePrefix { get { return "/itcquestions"; } }

        public string SingularRoutePrefix { get { return "/itcquestion"; } }
        
        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "ItcQuestions", "The entry point for ITC Questions", Get.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                var path = string.Format("{0}/", PluralRoutePrefix);
                var description = "ITC Questions documentation";
                return new RouteDefinition("OPTIONS", path, description);
            }
        }

        public RouteDefinition Get
        {
            get
            {
                var path = string.Format("{0}/", PluralRoutePrefix);
                var description = "List of the ITC Questions";
                return new RouteDefinition("GET", path, description, typeof(PagedResultDto<ItcQuestionDefinitionDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                var path = string.Format("{0}/", SingularRoutePrefix);
                var description = "Create a new channel ITC Question and disable an existing version.";
                return new RouteDefinition("POST", path, description, typeof(ItcQuestionDefinitionDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                var path = string.Format("{0}/{1}", SingularRoutePrefix, M_IdPlaceholder);
                var description = "Update an existing ITC Question";
                return new RouteDefinition("PUT", path, description, typeof(ItcQuestionDefinitionDto));

            }
        }

        public RouteDefinition Delete
        {
            get
            {
                var path = string.Format("{0}/{1}", SingularRoutePrefix, M_IdPlaceholder);
                var description = "Delete an existing ITC Question";
                return new RouteDefinition("DELETE", path, description, typeof(ItcQuestionDefinitionDto));

            }
        }

        public RouteDefinition GetById
        {
            get
            {
                var path = string.Format("{0}/{1}", SingularRoutePrefix, M_IdPlaceholder);
                var description = "Get an ITC Question by id";
                return new RouteDefinition("GET", path, description, typeof(ItcQuestionDefinitionDto));
            }
        }

        public RouteDefinition GetByChannelId
        {
            get
            {
                var path = string.Format("{0}/{1}/{2}", SingularRoutePrefix, M_ChannelPathPlaceholder, M_IdPlaceholder);
                var description = "Get an ITC Question by channel id";
                return new RouteDefinition("GET", path, description, typeof(ItcQuestionDefinitionDto));
            }
        }

        public RouteDefinition GetByChannelCode
        {
            get
            {
                var path = string.Format("{0}/{1}/{2}", SingularRoutePrefix, M_ChannelCodePathPlaceholder, M_ChannelCodePlaceholder);
                var description = "Get an ITC Question by channel code";
                return new RouteDefinition("GET", path, description, typeof(ItcQuestionDefinitionDto));
            }
        }

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(M_IdPlaceholder, id.ToString());
        }

        public string CreateGetByChannelCode(string channelCode)
        {
            return GetByChannelCode.Route.Replace(M_ChannelCodePlaceholder, channelCode); 
        }

        public string CreateGetByChannelId(int channelId)
        {
            return GetByChannelId.Route.Replace(M_IdPlaceholder, channelId.ToString());
        }

        public string CreateUpdate(int id)
        {
            return Put.Route.Replace(M_IdPlaceholder, id.ToString());
        }

        public string CreatePost()
        {
            return Post.Route;
        }

        public string CreateDelete(int id)
        {
            return Delete.Route.Replace(M_IdPlaceholder, id.ToString());
        }

        public void CreateLinks(ItcQuestionDefinitionDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                Post,
                Put,
                Delete,
                GetById,
                GetByChannelId,
                GetByChannelCode
            };
        }
    }
}
