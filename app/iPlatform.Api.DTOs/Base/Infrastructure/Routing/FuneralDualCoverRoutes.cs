using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.FuneralDualCover;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class FuneralDualCoverRoutes : IDefineRoutes<FuneralDualCoverDto>
    {
        private const string idNumberPlaceholder = "{idNumber}";
        private const string sumInsuredPlaceholder = "{sumInsured}";

        public string SingularRoutePrefix
        {
            get { return "funeraldualcover"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "FuneralDualCover", "The entry point for funeraldualcover", Get.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", SingularRoutePrefix),
                    "Funeraldualcover documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", SingularRoutePrefix, idNumberPlaceholder, sumInsuredPlaceholder),
                    "Get funeraldualcover", typeof(FuneralDualCoverDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
            };
        }

        public void CreateLinks(Resource resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            throw new NotImplementedException();
        }

        public string CreateGet(string idNumber, decimal sumInsured)
        {
            return Get.Route
                .Replace(idNumberPlaceholder, idNumber)
                .Replace(sumInsuredPlaceholder, sumInsured.ToString());
        }

        public void CreateLinks(FuneralDualCoverDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
    }
}