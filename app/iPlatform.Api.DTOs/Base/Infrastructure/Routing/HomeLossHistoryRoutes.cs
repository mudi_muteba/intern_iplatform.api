using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.HistoryLoss.Home;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class HomeLossHistoryRoutes : IDefineRoutes<ListHomeLossHistoryDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string SingularRoutePrefix
        {
            get { return "homelosshistory"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/homelosshistories"; }
        }

        public void CreateLinks(ListHomeLossHistoryDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "HomeLossHistory documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "HomeLossHistory", "The entry point for HomeLossHistories", Get.Route);
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a HomeLossHistory by Id", typeof(ListHomeLossHistoryDto));
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of HomeLossHistory", typeof(PagedResultDto<ListHomeLossHistoryDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of HomeLossHistory with pagination", typeof(PagedResultDto<ListHomeLossHistoryDto>));
            }
        }

        public RouteDefinition GetAll
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the HomeLossHistories", typeof(PagedResultDto<ListHomeLossHistoryDto>));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search home loss history by search DTO", typeof(PagedResultDto<ListHomeLossHistoryDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a new HomeLossHistory", typeof(CreateHomeLossHistoryDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, idPlaceholder),
                    "Update a HomeLossHistory", typeof(EditHomeLossHistoryDto));
            }
        }

        public RouteDefinition PutDisable
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                    "Disable a HomeLossHistory");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetById,
                GetWithPagination,
                Post,
                PostSearch,
                PutDisable
            };
        }

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(idPlaceholder, id.ToString());
        }

        public void CreateLinks(Resource resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string PutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string DisableById(int id)
        {
            var idPart = PutDisable.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string UpdateById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }
    }
}