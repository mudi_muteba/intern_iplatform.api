﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.CimsDataSync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class CimsDataSyncRoutes : IDefineRoutes
    {
        private const string m_IdPlaceholder = "{id:int}";
        private const string m_DbName = "{dbName}";

        public string SingularRoutePrefix
        {
            get { return string.Format("/CimsDataSync"); }
        }



        #region [ Create URL's ]

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(m_IdPlaceholder, id.ToString());
        }

        #endregion

        #region [ Route Definitions ]

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                CimsDataSyncProductPricingStructure
            };
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}", SingularRoutePrefix),
                    "ProductPricingStructure() documentation");
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "CimsDataSync", "Authentication before using the API", Options.Route);
            }
        }
        
        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, m_IdPlaceholder),
                    "Get a Product Pricing Structure ");
            }
        }

        public RouteDefinition CimsDataSyncProductPricingStructure
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/ProductPricingStructure/{1}", SingularRoutePrefix, m_DbName),
                    "Sync CIMS3 pricing structure with iPlatform.");
            }
        }

        public RouteDefinition CimsDataSyncChannelUser
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/ChannelUser", SingularRoutePrefix),
                    "Sync CIMS3 channels and users with iPlatform.", typeof(CimsDataSyncChannelUserDto));
            }
        }

        #endregion
    }
}
