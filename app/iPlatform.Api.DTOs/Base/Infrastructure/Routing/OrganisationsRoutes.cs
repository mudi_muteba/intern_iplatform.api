using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Organisations;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class OrganisationsRoutes : IDefineRoutes<ListOrganizationDto>, IDefineRoutes<OrganisationDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string PluralRoutePrefix
        {
            get { return "/organisations"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/organisation"; }
        }

      
        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetAllWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a organization by Id", typeof(ListOrganizationDto));
            }
        }


        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Organizations Documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of Organizations", typeof(PagedResultDto<ListOrganizationDto>));
            }
        }

        public RouteDefinition GetOrganizations
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of Organizations", typeof(PagedResultDto<ListOrganizationDto>));
            }
        }

        public RouteDefinition GetAll
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/all", PluralRoutePrefix),
                    "List of Organizations", typeof(ListResultDto<ListOrganizationDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the organizations with pagination", typeof(PagedResultDto<ListOrganizationDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a new organization", typeof(CreateOrganizationDto));
            }
        }

        //public RouteDefinition Search
        //{
        //    get
        //    {
        //        return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
        //            "organization search", typeof(CampaignSearchDto));
        //    }
        //}

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Organizations", "The entry point for organizations", Get.Route); }
        }

        public RouteDefinition Edit
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, idPlaceholder),
                    "Update a organizations", typeof(EditOrganizationDto));
            }
        }

        public string CreateEditUrl(int id)
        {
            var idPart = Edit.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetWithPagination,
                Post,
                GetById,
                Edit,
                //Search,
                //PostOrganisation,
                GetOrganizations
            };
        }

        public void CreateLinks(ListOrganizationDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(OrganisationDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
    }
}