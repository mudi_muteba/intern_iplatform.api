using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Leads;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class CampaignsRoutes : IDefineRoutes<ListCampaignDto>, IDefineRoutes<CampaignDto>
    {
        private const string IdPlaceholder = "{id:int}";
        private const string AgentIdPlaceholder = "{agentid:int}";
        private const string LeadIdPlaceholder = "{leadid:int}";
        private const string ChannelIdPlaceholder = "{channelid:int}";
        private const string PartyIdPlaceholder = "{partyid:int}";

        public string PluralRoutePrefix
        {
            get { return "/campaigns"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/campaign"; }
        }

        public void CreateLinks(ListCampaignDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByPartyId(int id)
        {
            var idPart = GetByPartyId.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByChannelPartyId(int channelId, int partyId)
        {
            var idPart = GetByChannelPartyId.Route.Replace(ChannelIdPlaceholder, channelId.ToString()).Replace(PartyIdPlaceholder, partyId.ToString());

            return idPart;
        }

        public string CreateGetByChannelId(int id)
        {
            var idPart = GetByChannelId.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Campaigns", "The entry point for campaigns", Get.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix), "Campaigns documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "List of the campaigns", typeof(PagedResultDto<ListCampaignDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder), "Get a campaign by Id", typeof(CampaignDto));
            }
        }

        public RouteDefinition GetByPartyId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/party/{1}", PluralRoutePrefix, IdPlaceholder), "List of the campaigns by partyId", typeof(ListCampaignDto));
            }
        }

        public RouteDefinition GetByChannelPartyId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/channelparty/{1}/{2}", PluralRoutePrefix, ChannelIdPlaceholder, PartyIdPlaceholder), "List of the campaigns by channel & party Id", typeof(ListCampaignDto));
            }
        }

        public RouteDefinition GetAcrossChannelPartyId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/acrosschannelbyparty/{1}", PluralRoutePrefix, PartyIdPlaceholder), "List of the campaigns by channel & party Id", typeof(ListCampaignDto));
            }
        }

        public RouteDefinition GetByChannelId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/channel/{1}", PluralRoutePrefix, IdPlaceholder), "List of the campaigns by channelId", typeof(ListCampaignDto));
            }
        }

        public RouteDefinition PostCampaignReference
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/campaignreference", PluralRoutePrefix), "Save CampaignReference", typeof(CreateCampaignReferenceDto));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of the campaigns with pagination", typeof(PagedResultDto<ListCampaignDto>));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix), "Create a new campaign", typeof(CreateCampaignDto));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), "Campaign search", typeof(CampaignSearchDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, IdPlaceholder), "Update a campaign", typeof(EditCampaignDto));
            }
        }

        public RouteDefinition PutDisable
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, IdPlaceholder), "Disable a campaign");
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/delete", SingularRoutePrefix, IdPlaceholder), "Delete a campaign");
            }
        }

        public RouteDefinition AssignLeads
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/assignleads", SingularRoutePrefix, IdPlaceholder), "Assign Leads to a campaign", typeof(AssignLeadToCampaignDto));
            }
        }

        public RouteDefinition GetLeads
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/leadsbycampaign", SingularRoutePrefix, IdPlaceholder), "Get Leads by a campaign");
            }
        }

        public RouteDefinition GetLeadsByAgent
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/leadsbyagent/{2}", SingularRoutePrefix, IdPlaceholder, AgentIdPlaceholder), "Get Leads by a campaign and agent");
            }
        }

        public RouteDefinition GetACampaignLeadBucket
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/campaignleadbucket/{2}", SingularRoutePrefix, IdPlaceholder, LeadIdPlaceholder), "Get Lead by a campaign and leadid", typeof(CampaignLeadBucketDto));
            }
        }

        public RouteDefinition PutNextLead
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/nextlead", SingularRoutePrefix, IdPlaceholder), "Get Next Lead, assign Lead to agent", typeof(GetNextLeadDto));
            }
        }

        public RouteDefinition GetNextSerityImportLead
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/nextseritilead", SingularRoutePrefix, IdPlaceholder), "Get Nextseriti import Lead, assign Lead to agent", typeof(GetSeritiImportNextLeadDto));
            }
        }

        public RouteDefinition PostAssignLeadAgent
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/assignleadagent", SingularRoutePrefix, IdPlaceholder), "Assign Lead to agent", typeof(AssignLeadToAgentDto));
            }
        }
        public RouteDefinition PostAssignLeadCampaigns
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/assignleadcampaigns", PluralRoutePrefix), "Assign Lead to a campaign list", typeof(AssignLeadCampaignsDto));
            }
        }

        public string DisableById(int id)
        {
            var idPart = PutDisable.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string DeleteById(int id)
        {
            var idPart = PutDelete.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string PutById(int id)
        {
            var idPart = Put.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string AssignLeadsById(int id)
        {
            var idPart = AssignLeads.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string GetLeadByCampaignId(int id)
        {
            var idPart = GetLeads.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string GetLeadsByAgentId(int id, int agentid)
        {
            var idPart = GetLeadsByAgent.Route.Replace(IdPlaceholder, id.ToString()).Replace(AgentIdPlaceholder, agentid.ToString());

            return idPart;
        }

        public string GetCampaignLeadBucket(int id, int leadid)
        {
            var idPart = GetACampaignLeadBucket.Route.Replace(IdPlaceholder, id.ToString()).Replace(LeadIdPlaceholder, leadid.ToString());

            return idPart;
        }

        public string GetNextLead(int id)
        {
            var idPart = PutNextLead.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string GetAgentNextSerityImportLead(int id)
        {
            var idPart = GetNextSerityImportLead.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }
        public string PostAssignLeadToAgent(int id)
        {
            var idPart = PostAssignLeadAgent.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetAcrossChannelPartyId(int partyId)
        {
            var idPart = GetAcrossChannelPartyId.Route.Replace(PartyIdPlaceholder, partyId.ToString());

            return idPart;
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetById,
                GetWithPagination,
                Post,
                PostSearch,
                Put,
                GetByPartyId,
                GetByChannelPartyId,
                GetByChannelId,
                AssignLeads,
                GetLeads,
                GetLeadsByAgent,
                GetACampaignLeadBucket,
                PutNextLead,
                PostAssignLeadCampaigns,
                PostAssignLeadAgent,
                GetNextSerityImportLead,
                GetAcrossChannelPartyId
            };
        }

        public void CreateLinks(CampaignDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
            resource.AddLink("update", PutById(resource.Id));
        }
    }
}