using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.FuneralMembers;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class FuneralMemberRoutes : IDefineRoutes<ListFuneralMemberDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string SingularRoutePrefix
        {
            get { return "funeralmember"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/funeralmembers"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "FuneralMembers", "The entry point for funeralmembers", Get.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "FuneralMember");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of funeral members", typeof(PagedResultDto<ListFuneralMemberDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a funeral member by Id", typeof(ListFuneralMemberDto));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{{pageNumber:int}}/{{pageSize:int}}", PluralRoutePrefix),
                    "List of funeral members with pagination", typeof(PagedResultDto<ListFuneralMemberDto>));
            }
        }

        public RouteDefinition GetByProposalDefinitionId
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}", PluralRoutePrefix, idPlaceholder),
                    "Get  Funeralmembers by proposaldefinitionid");
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a new FuneralMember", typeof(CreateFuneralMemberDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/", SingularRoutePrefix, idPlaceholder),
                    "Update a funeral member", typeof(EditFuneralMemberDto));
            }
        }

        public RouteDefinition PutDisable
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                    "Disable a funeral members");
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Get,
                GetById,
                GetWithPagination,
                GetByProposalDefinitionId,
                Post,
                PutDisable
            };
        }

        public void CreateLinks(ListFuneralMemberDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            return GetById.Route.Replace(idPlaceholder, id.ToString());
        }

        public void CreateLinks(Resource resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string PutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string DisableById(int id)
        {
            var idPart = PutDisable.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string GetByProposalDefId(int id)
        {
            var idPart = GetByProposalDefinitionId.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string UpdateById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

    }
}