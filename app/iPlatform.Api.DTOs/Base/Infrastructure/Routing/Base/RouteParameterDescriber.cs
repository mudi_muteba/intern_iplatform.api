using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base
{
    public class RouteParameterDescriber
    {
        public readonly string ParameterType;
        public readonly List<ParameterDescription> Parameters = new List<ParameterDescription>();

        private RouteParameterDescriber()
        {
        }

        public RouteParameterDescriber(Type type) : this(type, string.Empty)
        {
        }

        // builds up the parameter information from a type
        public RouteParameterDescriber(Type type, string route): this(route)
        {
            ParameterType = type.Name;

            var parameters = GetAvailableProperties(type)
                .Select(p => new ParameterDescription(p.Name, p.PropertyType));

            foreach (var parameter in parameters)
                if (!Parameters.Any(p => p.Name.Equals(parameter.Name, StringComparison.InvariantCultureIgnoreCase)))
                    Parameters.Add(parameter);
        }

        private IEnumerable<PropertyInfo> GetAvailableProperties(Type type)
        {
            var availableProperties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(p => p.PropertyType != typeof(DtoContext));

            return availableProperties;
        }

        // builds up the parameter information from the route
        public RouteParameterDescriber(string route)
        {
            if (string.IsNullOrEmpty(route))
                return;

            if (!(route.Contains("{") && route.Contains("}")))
                return;

            ParameterType = "QueryString";

            var expression = new Regex(@"\{(.*?)\}");
            var matches = expression.Matches(route);

            foreach (var match in matches)
            {
                // ugly as sin
                var cleanedParameter = match.ToString().Replace("{", string.Empty).Replace("}", string.Empty);
                var parameterParts = cleanedParameter.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                if (!Parameters.Any(p => p.Name.Equals(parameterParts[0], StringComparison.InvariantCultureIgnoreCase)))
                    Parameters.Add(new ParameterDescription(parameterParts[0], parameterParts.Length > 2 ? parameterParts[1] : ""));
            }
        }

        public static RouteParameterDescriber NoParameter()
        {
            return new RouteParameterDescriber();
        }
    }
}