using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base
{
    public class ParameterDescription
    {
        public readonly string Name;
        public readonly string Type;
        public List<PropertyDescription> Properties { get; set; }

        public ParameterDescription()
        {
            Properties = new List<PropertyDescription>();
        }

        public ParameterDescription(string name, string type) : this()
        {
            Name = name;
            Type = type;
        }

        public ParameterDescription(string name, Type type) : this()
        {
            var isPrimitive = type.Namespace.StartsWith("System");
            var isGeneric = type.IsGenericType;

            Name = name;
            Type = !isGeneric
                ? type.Name 
                : string.Format("{0}[{1}]", type.Name, BuildGenericTypeName(type));

            if (isPrimitive && !isGeneric)
                return;

            if(!isGeneric)
            {
                AddProperties(type);
                return;
            }

            var genericTypes = type.GetGenericArguments();

            foreach (var genericType in genericTypes.Where(t => !t.Namespace.StartsWith("System")))
            {
                AddProperties(genericType);
            }

        }

        private void AddProperties(Type type)
        {
            var excludeProperties = new List<string>()
            {
                "Context"
            };

            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var property in properties.Where(p => !excludeProperties.Any(ex => ex.Equals(p.Name))))
            {
                Properties.Add(new PropertyDescription(property.Name, property.PropertyType));
            }
        }

        private string BuildGenericTypeName(Type type)
        {
            var name = type.GetGenericArguments()
                .Aggregate(string.Empty, (current, genericArgument) => current + (genericArgument.Name + ", "));

            return name.Substring(0, name.Length - 2);
        }
    }
}