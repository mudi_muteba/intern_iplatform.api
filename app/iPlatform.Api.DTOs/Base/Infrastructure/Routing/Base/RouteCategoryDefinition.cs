namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base
{
    public class RouteCategoryDefinition
    {
        public readonly string Verb;
        public readonly string Description;
        public readonly string EntryPoint;
        public readonly string Name;

        public RouteCategoryDefinition(string verb, string name, string description, string entryPoint)
        {
            Verb = verb;
            Name = name;
            Description = description;
            EntryPoint = entryPoint;
        }
    }
}