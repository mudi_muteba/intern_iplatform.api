using System;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base
{
    public class RouteDefinition
    {
        public readonly string Description;
        public readonly RouteParameterDescriber Parameter;
        public readonly string Route;
        public readonly string Verb;

        public RouteDefinition(string verb, string route, string description)
            : this(verb, route, description, null)
        {
        }

        public RouteDefinition(string verb, string route, string description, Type type)
        {
            Verb = verb;
            Route = route;
            Description = description;

            Parameter = type != null
                ? new RouteParameterDescriber(type, route)
                : new RouteParameterDescriber(route);
        }
    }
}