using System;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base
{
    public class PropertyDescription
    {
        public string Name { get; set; }
        public string Type { get; set; }

        protected PropertyDescription()
        {
        }

        public PropertyDescription(string name, Type type)
        {
            Name = name;
            this.Type = type.Name;
        }
    }
}