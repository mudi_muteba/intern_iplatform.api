using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base
{
    public interface IDefineRoutes
    {
        string CreateGetById(int id);
        RouteCategoryDefinition Definition { get; }
        IEnumerable<RouteDefinition> Routes();
    }

    public interface IDefineRoutes<in TDto> : IDefineRoutes
        where TDto : Resource
    {
        void CreateLinks(TDto resource);
    }

}