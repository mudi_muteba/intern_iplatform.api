﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Sales;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class SalesTagRoutes : IDefineRoutes<SalesTagDto>
    {
        private const string saleIdPlaceholder = "{saleid:int}";
        private const string IdPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return string.Format("saletags", saleIdPlaceholder); }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("saletag/{0}", IdPlaceholder); }
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get Sales Tag by Id", typeof(SalesTagDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/tag", PluralRoutePrefix),
                    "Create Sales Tag", typeof(CreateSalesTagDto));
            }
        }

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Sale Tag search", typeof(SalesTagSearchDto));
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "SalesTags", "The entry point for sales tags", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Sales Tags documentation");
            }
        }


        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {

                GetById,
                Options,
                Post,
                Options
            };
        }

        public void CreateLinks(SalesTagDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }

    }
}
