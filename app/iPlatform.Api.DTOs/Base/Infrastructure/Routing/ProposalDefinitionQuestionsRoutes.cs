﻿﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using iPlatform.Api.DTOs.Proposals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ProposalDefinitionQuestionsRoutes : IDefineRoutes<ProposalDefinitionQuestionsDto>
    {
        private const string ProposalDefinitionIdPlaceholder = "{proposalDefinitionId:int}";
        public string RoutePrefix
        {
            get { return string.Format("proposaldefinitionquestions/{0}", ProposalDefinitionIdPlaceholder); }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "ProposalDefinitionQuestions", "The entry point for Proposal Definition Questions", Get.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("proposaldefinitionquestions/", RoutePrefix),
                    "Proposal Definition Questions documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", RoutePrefix),
                    "Get a ProposalDefinitionQuestions by proposalDefinition Id", typeof(ProposalDefinitionQuestionsDto));
            }
        }

        public RouteDefinition Save
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}", RoutePrefix),
                    "Saves a ProposalDefinitionQuestions by proposalDefinition Id", typeof(SaveProposalDefinitionAnswersDto));
            }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", RoutePrefix),
                    "Search a ProposalDefinitionQuestions by proposalDefinition Id", typeof(ProposalDefinitionQuestionsCriteria));
            }
        }

        public RouteDefinition SaveAnswer
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/saveanswer", RoutePrefix),
                    "save a answer from policy binding", typeof(UpdateProposalAnswerFromPolicyBindingDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                Save,
                Search,
                SaveAnswer
            };
        }

        public string CreateGetById(int proposaldefinitionId)
        {
            string idpart = Get.Route.Replace(ProposalDefinitionIdPlaceholder, proposaldefinitionId.ToString());
            return idpart;
        }

        public string CreateSaveAnswer(int proposaldefinitionId)
        {
            string idpart = SaveAnswer.Route.Replace(ProposalDefinitionIdPlaceholder, proposaldefinitionId.ToString());
            return idpart;
        }

        public string CreateSearchById(int proposaldefinitionId)
        {
            string idpart = Search.Route.Replace(ProposalDefinitionIdPlaceholder, proposaldefinitionId.ToString());
            return idpart;
        }

        public string CreateSaveQuestionAnswersById(int proposaldefinitionId)
        {
            string idpart = Save.Route.Replace(ProposalDefinitionIdPlaceholder, proposaldefinitionId.ToString());
            return idpart;
        }

        public void CreateLinks(ProposalDefinitionQuestionsDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string GetLinks(ProposalDefinitionQuestionsDto resource)
        {
            return CreateGetById(resource.Id);
        }


        public string GetLinks(ProposalDefinitionDto resource)
        {
            return CreateGetById(resource.Id);
        }
    }
}