using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Audit;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class AuditRoutes : IDefineRoutes<ListAuditDto>
    {
        private const string idPlaceholder = "{id:int}";

        public string PluralRoutePrefix
        {
            get { return "/audits"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/audit"; }
        }


        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder), "Get a Audit by Id", typeof(ListAuditDto));
            }
        }

        public void CreateLinks(ListAuditDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }
        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", SingularRoutePrefix), "Audit documentation");
            }
        }
        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "Audit", "The entry point for Audit", Get.Route); }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix),
                    "List of the Audits");
            }
        }

        public RouteDefinition Save
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Audit search", typeof(CreateAuditLogDto));
            }
        }




        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetById,
                Save
            };
        }

    }
}