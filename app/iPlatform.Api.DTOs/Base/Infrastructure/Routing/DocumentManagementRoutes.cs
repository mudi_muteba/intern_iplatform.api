﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.DocumentManagement;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class DocumentManagementRoutes : IDefineRoutes<DocumentDto>
    {
        private const string IdPlaceholder = "{id:int}";
        private const string DocumentIdPlaceholder = "{document:int}";
        private const string SearchPlaceholder = "{document:string}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string PluralRoutePrefix
        {
            get { return "documents"; }
        }

        public string SingularRoutePrefix
        {
            get { return string.Format("document"); }
        }

        #region Get

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByCeatorId(int id)
        {
            var idPart = GetByCreatorId.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByPartyId(int id)
        {
            var idPart = GetByPartyId.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByDocumentId(int id)
        {
            var idPart = GetByDocumentId.Route.Replace(DocumentIdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetBySearch(string searchTerm)
        {
            var idPart = GetByDocumentsBySearch.Route.Replace(SearchPlaceholder, searchTerm);

            return idPart;
        }

        public string CreatePutDeleted()
        {
            var idPart = PutDelete.Route;

            return idPart;
        }

        #endregion


        public string CreatePutById(int id)
        {
            var idPart = Put.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePost()
        {
            var idPart = Post.Route;

            return idPart;
        }

        public string CreateDeleteById(int id)
        {
            var idPart = PutDelete.Route.Replace(IdPlaceholder, id.ToString());

            return idPart;
        }
        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", SingularRoutePrefix),
                    "Get document by Id");
            }
        }

        public RouteDefinition GetByCreatorId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/creator/{1}", PluralRoutePrefix, IdPlaceholder),
                    "Get document by Creator Id");
            }
        }

        public RouteDefinition GetByPartyId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/party/{1}", PluralRoutePrefix, IdPlaceholder),
                    "Get document by Party Id");
            }
        }

        public RouteDefinition GetByDocumentId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", PluralRoutePrefix, DocumentIdPlaceholder),
                    "Get document by Document Id");
            }
        }

        public RouteDefinition GetByDocumentsBySearch
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/search/{1}", PluralRoutePrefix, DocumentIdPlaceholder),
                    "Get document by search term");
            }
        }


        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", PluralRoutePrefix,
                    "Create document", typeof(CreateDocumentDto));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", PluralRoutePrefix, IdPlaceholder), "Update a document", typeof(DocumentDto));
            }
        }

        public RouteDefinition PutDelete
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/delete", SingularRoutePrefix), "Delete document", typeof(DeleteDocumentDto));
            }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Documents", "Authentication before using the API", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Document Management documentation");
            }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "List of the documents", typeof(PagedResultDto<DocumentDto>));
            }
        }
        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix), "Documents Search", typeof(PagedResultDto<DocumentDto>));
            }
        }

        public RouteDefinition GetWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder), "List of the documents", typeof(PagedResultDto<DocumentDto>));
            }
        }

        public RouteDefinition GetNoPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/nopagination", PluralRoutePrefix), "List of the documents", typeof(PagedResultDto<DocumentDto>));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {

                GetById,
                Options,
                Post,
                Get,
                Put,
                PutDelete,
                GetWithPagination,
                GetNoPagination,
                Search,
                GetByDocumentId,
                PutDelete
            };
        }

        public string CreateGetWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString());
        }

        public void CreateLinks(DocumentDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id)); ;
        }
    }
}
