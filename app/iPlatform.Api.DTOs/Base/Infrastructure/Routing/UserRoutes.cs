using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Authentication;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class UserRoutes : IDefineRoutes
    {
        private const string idPlaceholder = "{id:int}";
        private const string channelIdPlaceholder = "{channelId:int}";
        private const string UserNamePlaceholder = "{username:string}";
        private const string pageNumberPlaceHolder = "{pageNumber:int}";
        private const string pageSizePlaceHolder = "{pageSize:int}";

        public string SingularRoutePrefix
        {
            get { return "/user"; }
        }

        public string PluralRoutePrefix
        {
            get { return "/users"; }
        }

        public RouteCategoryDefinition Definition
        {
            get
            {
                return new RouteCategoryDefinition("OPTIONS", "Users", "User Documentation", Options.Route);
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "User Documentation");
            }
        }

        public RouteDefinition Get
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", SingularRoutePrefix) + "{id}", "User"); }
        }

        public RouteDefinition GetAll
        {
            get { return new RouteDefinition("GET", string.Format("{0}/", PluralRoutePrefix), "Get a list of users"); }
        }

        public RouteDefinition GetAllUsers
        {
            get { return new RouteDefinition("GET", string.Format("{0}/all", PluralRoutePrefix), "Get a list of users"); }
        }

        public RouteDefinition GetAllBrokerUsers
        {
            get { return new RouteDefinition("GET", string.Format("{0}/allBrokers/{1}", PluralRoutePrefix, channelIdPlaceholder), "Get a list of users where IsBroker is true"); }
        }

        public RouteDefinition GetAllAccountExecutiveUsers
        {
            get { return new RouteDefinition("GET", string.Format("{0}/allAccountExecutives/{1}", PluralRoutePrefix, channelIdPlaceholder), "Get a list of users where IsAccountExecutive is true"); }
        }

        public RouteDefinition GetAllUsersByChannelId
        {
            get { return new RouteDefinition("GET", string.Format("{0}/all/{1}", PluralRoutePrefix, channelIdPlaceholder), "Get a list of users by channelId"); }
        }

        public RouteDefinition Search
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "User search", typeof(ListUserDto));
            }
        }

        public RouteDefinition SearchNoDefaultChannel
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/searchnodefaultchannel", PluralRoutePrefix),
                    "User search no default channel", typeof(ListUserDto));
            }
        }

        public RouteDefinition GetAllWithPagination
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/{2}", PluralRoutePrefix, pageNumberPlaceHolder, pageSizePlaceHolder),
                    "List of users with pagination", typeof(PagedResultDto<ListUserDto>));
            }
        }

        public RouteDefinition GetByName
        {
            get { return new RouteDefinition("POST", string.Format("{0}/username", SingularRoutePrefix, UserNamePlaceholder), "Get user by name"); }
        }

        public RouteDefinition Post
        {
            get { return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix), "Creates a user", typeof(CreateUserDto)); }
        }

        public RouteDefinition Put
        {
            get { return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder), "Edit a user's information", typeof(AuthenticationRequestDto)); }
        }

        public RouteDefinition Register
        {
            get { return new RouteDefinition("POST", string.Format("{0}/register", PluralRoutePrefix), "Register a user", typeof(RegisterUserDto)); }
        }

        public RouteDefinition RegisterAPIUser
        {
            get { return new RouteDefinition("POST", string.Format("{0}/register-api-user", PluralRoutePrefix), "Register an API user", typeof(RegisterApiUserDto)); }
        }

        public RouteDefinition Approve
        {
            get { return new RouteDefinition("PUT", string.Format("{0}/{1}/Approve", SingularRoutePrefix, idPlaceholder), "Approve a user", typeof(AuthenticationRequestDto)); }
        }

        public RouteDefinition Disable
        {
            get { return new RouteDefinition("PUT", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder), "Disables a user"); }
        }
        
        public RouteDefinition ValidatePasswordResetToken
        {
            get { return new RouteDefinition("PUT", string.Format("{0}/ValidatePasswordResetToken", PluralRoutePrefix), "Request password reset token validation", typeof(RequestPasswordResetDto)); }
        }

        public RouteDefinition RequestPasswordReset
        {
            get { return new RouteDefinition("PUT", string.Format("{0}/RequestPasswordReset", PluralRoutePrefix), "Request password reset", typeof(RequestPasswordResetDto)); }
        }

        public RouteDefinition ResetPassword
        {
            get { return new RouteDefinition("PUT", string.Format("{0}/ResetPassword", PluralRoutePrefix), "Reset a user's password", typeof(ResetPasswordDto)); }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a user by Id", typeof(UserDto));
            }
        }

        public RouteDefinition GetCampaignUsers
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/campaignusers", PluralRoutePrefix), "Retrieve campaign Users", typeof(GetCampaignUsersDto));
            }
        }
        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Post,
                Get,
                GetById,
                GetByName,
                Approve,
                Disable,
                ResetPassword,
                Put,
                GetAllBrokerUsers,
                GetAllAccountExecutiveUsers,
                SearchNoDefaultChannel,
                GetCampaignUsers
            };
        }

        public string CreateGetAllWithPaginationUrl(int pageNumber, int pageSize)
        {
            return GetAllWithPagination.Route
                .Replace(pageNumberPlaceHolder, pageNumber.ToString())
                .Replace(pageSizePlaceHolder, pageSize.ToString())
                ;
        }

        public string CreateApproveUrl(int userId)
        {
            var idPart = Approve.Route.Replace(idPlaceholder, userId.ToString());

            return idPart;
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateEditUrl(int userId)
        {
            var idPart = Put.Route.Replace(idPlaceholder, userId.ToString());

            return idPart;
        }

        public string CreateDisableUserUrl(int userId)
        {
            var idPart = Disable.Route.Replace(idPlaceholder, userId.ToString());

            return idPart;
        }
        
        public void CreateLinks(Resource resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public void CreateLinks(UserInfoDto dto)
        {
            dto.AddLink("self", CreateGetById(dto.Id));
        }
        public string CreateGetByUserName(string username)
        {
            var idPart = GetByName.Route.Replace(UserNamePlaceholder, username);
            return idPart;
        }

        public string CreateGetAllBrokerUsersUrl(int channelId)
        {
            var idPart = GetAllBrokerUsers.Route.Replace(channelIdPlaceholder, channelId.ToString());
            return idPart;
        }

        public string CreateGetAllAccountExecutiveUsersUrl(int channelId)
        {
            var idPart = GetAllAccountExecutiveUsers.Route.Replace(channelIdPlaceholder, channelId.ToString());
            return idPart;
        }

        public string CreateGetAllByChannelIdUrl(int channelId)
        {
            var idPart = GetAllUsersByChannelId.Route.Replace(channelIdPlaceholder, channelId.ToString());
            return idPart;
        }
    }
}