﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using System;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class ChannelPermissionRoutes : IDefineRoutes<ChannelPermissionDto>
    {
        private const string idPlaceholder = "{id:int}";
        private const string channelIdPlaceholder = "{channelid:guid}";
        public string PluralRoutePrefix
        {
            get { return "/permissions"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/permission"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "ChannelPermissions", "The entry point for Channel Permissions", Get.Route); }
        }

        public RouteDefinition Get
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}", PluralRoutePrefix),
                    "List of the Channel Permissions", typeof(ListResultDto<ChannelPermissionDto>));
            }
        }

        public RouteDefinition GetById
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Get a Channel Permission by Id", typeof(ChannelPermissionDto));
            }
        }

        public RouteDefinition GetByChannelId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", PluralRoutePrefix, channelIdPlaceholder),
                    "List of the Channel Permissions by channelId", typeof(ListResultDto<ChannelPermissionDto>));
            }
        }

        public RouteDefinition Put
        {
            get
            {
                return new RouteDefinition("PUT", string.Format("{0}/{1}", SingularRoutePrefix, idPlaceholder),
                    "Update a Channel Permission by Id", typeof(EditChannelPermissionDto));
            }
        }

        public RouteDefinition Post
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/", PluralRoutePrefix),
                    "Create a Channel Permission", typeof(CreateChannelPermissionDto));
            }
        }

        public RouteDefinition Disable
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/{1}/disable", SingularRoutePrefix, idPlaceholder),
                    "Disable a Channel Permission by Id", typeof(DisableChannelPermissionDto));
            }
        }    

        public RouteDefinition PostSearch
        {
            get
            {
                return new RouteDefinition("POST", string.Format("{0}/search", PluralRoutePrefix),
                    "Search a Channel Permission", typeof(ListResultDto<ChannelPermissionDto>));
            }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Channels documentation");
            }
        }
        
        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                Get,
                GetById,
                GetByChannelId,
                Post,
                PostSearch,
                Disable,
                Put
            };
        }

        public void CreateLinks(ChannelPermissionDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int id)
        {
            var idPart = GetById.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreatePutById(int id)
        {
            var idPart = Put.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateDisableById(int id)
        {
            var idPart = Disable.Route.Replace(idPlaceholder, id.ToString());

            return idPart;
        }

        public string CreateGetByChannelId(Guid channelId)
        {
            var idPart = GetByChannelId.Route.Replace(channelIdPlaceholder, channelId.ToString());

            return idPart;
        }
    }
}
