using System.Collections.Generic;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Statistics;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Routing
{
    public class LeadConversionStatisticsRoutes : IDefineRoutes<ListLeadActivityDto>
    {
        private const string IdPlaceholder = "{agentId:int}";

        public string PluralRoutePrefix
        {
            get { return "/LeadConversionStatistics"; }
        }

        public string SingularRoutePrefix
        {
            get { return "/LeadConversionStatistic"; }
        }

        public RouteCategoryDefinition Definition
        {
            get { return new RouteCategoryDefinition("GET", "LeadConversationStatistics", "The entry point for Lead Conversation Statistics", GetByAgentId.Route); }
        }

        public RouteDefinition Options
        {
            get
            {
                return new RouteDefinition("OPTIONS", string.Format("{0}/", PluralRoutePrefix),
                    "Lead Conversation Statistics Documentation");
            }
        }

        public RouteDefinition GetByAgentId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}", SingularRoutePrefix, IdPlaceholder),
                    "Get LeadActivity statistics by AgentId", typeof(StatisticsDto));
            }
        }

        public RouteDefinition GetMonthlyByAgentId
        {
            get
            {
                return new RouteDefinition("GET", string.Format("{0}/{1}/monthly", SingularRoutePrefix, IdPlaceholder),
                    "Get LeadActivity statistics by AgentId", typeof(StatisticsDto));
            }
        }

        public IEnumerable<RouteDefinition> Routes()
        {
            return new List<RouteDefinition>
            {
                Options,
                GetByAgentId,
                GetMonthlyByAgentId
            };
        }

        public void CreateLinks(ListLeadActivityDto resource)
        {
            resource.AddLink("self", CreateGetById(resource.Id));
        }

        public string CreateGetById(int agentId)
        {
            return GetByAgentId.Route.Replace(IdPlaceholder, agentId.ToString());
        }

        public string CreateGetMonthlyById(int agentId)
        {
            return GetMonthlyByAgentId.Route.Replace(IdPlaceholder, agentId.ToString());
        }
    }
}