namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    public enum ResponseCode
    {
        Success = 0,
        NotFound = -1,
        ErrorSaving = -2,
        IdIsPopulated = -3,
        IdNotPopulated = -4,
        AssociatedItemNotFound = -5,
        IdentityNumberAllReadyInUse = -6
    }
}