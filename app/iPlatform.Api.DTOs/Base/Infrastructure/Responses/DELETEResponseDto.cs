﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    [DataContract]
    public class DELETEResponseDto<TResponse> : BaseResponseDto
    {
        public DELETEResponseDto()
        {
            StatusCode = HttpStatusCode.Accepted;

            OnValidationFailure = () =>
            {
                StatusCode = HttpStatusCode.BadRequest;
                RemoveEntityReferences();
            };

            OnAuthorisationFailure = () =>
            {
                StatusCode = HttpStatusCode.Unauthorized;
                RemoveEntityReferences();
            };

            OnExecutionFailure = () =>
            {
                StatusCode = HttpStatusCode.InternalServerError;
                RemoveEntityReferences();
            };
        }

        public DELETEResponseDto(TResponse response, string link)
            : this()
        {
            Response = response;
            Link = link;
        }

        [DataMember]
        public TResponse Response { get; private set; }
        [DataMember]
        public string Link { get; private set; }

        protected override List<HttpStatusCode> SuccessStatusCodes
        {
            get
            {
                return new List<HttpStatusCode>()
                {
                    HttpStatusCode.OK,
                    HttpStatusCode.Accepted,
                };
            }
        }

        private void RemoveEntityReferences()
        {
            Link = string.Empty;
            Response = default(TResponse);
        }

        public DELETEResponseDto<TResponse> CreateLinks(string link)
        {
            if (Errors.Any())
                return this;

            Link = link;
            return this;
        }
    }
}