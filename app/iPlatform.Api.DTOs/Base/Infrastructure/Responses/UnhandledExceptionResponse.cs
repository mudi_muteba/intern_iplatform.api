﻿using System.Collections.Generic;
using System.Net;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    public class UnhandledExceptionResponse : BaseResponseDto
    {
        public UnhandledExceptionResponse()
        {
            StatusCode = HttpStatusCode.InternalServerError;
        }

        protected override List<HttpStatusCode> SuccessStatusCodes
        {
            get { return new List<HttpStatusCode>(); }
        }
    }
}