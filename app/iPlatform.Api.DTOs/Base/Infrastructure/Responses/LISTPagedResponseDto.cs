﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    [DataContract]
    public class LISTPagedResponseDto<TResponse> : BaseResponseDto
    {
        public LISTPagedResponseDto() { }

        public LISTPagedResponseDto(PagedResultDto<TResponse> response)
        {
            Response = response;
            StatusCode = HttpStatusCode.OK;
        }

        [DataMember]
        public PagedResultDto<TResponse> Response { get; set; }
        [DataMember]
        protected override List<HttpStatusCode> SuccessStatusCodes
        {
            get
            {
                return new List<HttpStatusCode>
                {
                    HttpStatusCode.OK
                };
            }
        }

        public LISTPagedResponseDto<TResponse> WithResponse(PagedResultDto<TResponse> response)
        {
            Response = response;
            return this;
        }

        public LISTPagedResponseDto<TResponse> CreateLinks(Action<TResponse> createLink)
        {
            if (!(typeof (Resource).IsAssignableFrom(typeof (TResponse))))
                return this;

            if (Response == null)
                return this;

            foreach (var r in Response.Results)
                createLink(r);

            return this;
        }
    }
}