﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    [DataContract]
    public class PUTResponseDto<TResponse> : BaseResponseDto
    {
        public PUTResponseDto()
        {
            StatusCode = HttpStatusCode.OK;
        }

        public PUTResponseDto(TResponse response, string link)
        {
            Response = response;
            Link = link;
            StatusCode = HttpStatusCode.OK;
            OnValidationFailure = RemoveEntityReferences;
        }
        [DataMember]
        public TResponse Response { get; private set; }
        [DataMember]
        public string Link { get; private set; }

        protected override List<HttpStatusCode> SuccessStatusCodes
        {
            get
            {
                return new List<HttpStatusCode>()
                {
                    HttpStatusCode.OK,
                    HttpStatusCode.Accepted

                };
            }
        }

        private void RemoveEntityReferences()
        {
            Link = string.Empty;
            Response = default(TResponse);
            StatusCode = HttpStatusCode.BadRequest;
        }

        public PUTResponseDto<TResponse> CreateLinks(string link)
        {
            if (Errors.Any())
                return this;

            Link = link;
            return this;
        }
    }
}