namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    public class Status
    {

        public Status()
        {
        }
        public Status(ResponseCode id)
        {
            ResponseCode = id;
        }

        public ResponseCode ResponseCode { get; set; }
        public string Message { get; set; }
    }
}