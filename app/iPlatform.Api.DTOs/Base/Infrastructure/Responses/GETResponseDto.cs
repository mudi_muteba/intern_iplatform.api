﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    [DataContract]
    public class GETResponseDto<TResponse> : BaseResponseDto
    {
        public GETResponseDto()
        {
            StatusCode = HttpStatusCode.OK;
        }

        public GETResponseDto(TResponse response)
        {
            Response = response;
            StatusCode = response == null ? HttpStatusCode.NotFound : HttpStatusCode.OK;

            OnAuthorisationFailure = () => ChangeStatusCode(HttpStatusCode.Unauthorized);
            OnExecutionFailure = () => ChangeStatusCode(HttpStatusCode.InternalServerError);
            OnValidationFailure = () => ChangeStatusCode(HttpStatusCode.BadRequest);
        }

        private void ChangeStatusCode(HttpStatusCode newStatusCode)
        {
            if (StatusCode == HttpStatusCode.OK)
            {
                StatusCode = newStatusCode;
                return;
            }

            if (StatusCode == HttpStatusCode.NotFound && newStatusCode == HttpStatusCode.Unauthorized)
            {
                StatusCode = newStatusCode;
                return;
            }
        }

        [DataMember]
        public TResponse Response { get; set; }

        protected override List<HttpStatusCode> SuccessStatusCodes
        {
            get
            {
                return new List<HttpStatusCode>
                {
                    HttpStatusCode.OK
                };
            }
        }

        public GETResponseDto<TResponse> CreateLinks(Action<TResponse> createLink)
        {
            if (!(typeof (Resource).IsAssignableFrom(typeof (TResponse))))
                return this;

            if (Response == null)
                return this;

            createLink(Response);

            return this;
        }
    }
}