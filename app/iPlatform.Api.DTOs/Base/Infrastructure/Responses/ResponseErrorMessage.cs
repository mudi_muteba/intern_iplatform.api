﻿namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    public class ResponseErrorMessage
    {
        public ResponseErrorMessage()
        {
        }

        public ResponseErrorMessage(string defaultMessage, string messageKey)
        {
            DefaultMessage = defaultMessage;
            MessageKey = messageKey;
        }

        public string DefaultMessage { get; set; }
        public string MessageKey { get; set; }
    }
}