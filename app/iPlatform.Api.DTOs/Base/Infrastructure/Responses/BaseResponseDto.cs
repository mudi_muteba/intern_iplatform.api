﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using iPlatform.Api.DTOs.Extensions;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    [DataContract]
    public class BaseResponseDto
    {
        private readonly List<ResponseErrorMessage> errors = new List<ResponseErrorMessage>();

        public BaseResponseDto()
        {
            OnValidationFailure = () => { StatusCode = HttpStatusCode.BadRequest; };
            OnAuthorisationFailure = () => { StatusCode = HttpStatusCode.Unauthorized; };
            OnExecutionFailure = () => { StatusCode = HttpStatusCode.InternalServerError; };
            OnMissingEntity = () => { StatusCode = HttpStatusCode.NotFound; };
            Errors = new List<ResponseErrorMessage>();
        }

        [DataMember]
        public HttpStatusCode StatusCode { get; set; }
        [DataMember]
        public List<ResponseErrorMessage> Errors
        {
            get { return errors; }
            private set
            {
                foreach (var message in value)
                {
                    errors.AddIfValid(message);
                    OnValidationFailure();
                }
            }
        }

        protected virtual Action OnAuthorisationFailure { get; set; }
        protected virtual Action OnExecutionFailure { get; set; }
        protected virtual Action OnValidationFailure { get; set; }
        protected virtual Action OnMissingEntity { get; set; }

        public bool IsSuccess
        {
            get { return SuccessStatusCodes.Any(s => s.Equals(StatusCode)); }
        }

        protected virtual List<HttpStatusCode> SuccessStatusCodes {
            get
            {
                return new List<HttpStatusCode>();
            }
        }

        public void AuthorisationFailed(IEnumerable<ResponseErrorMessage> messages)
        {
            Errors.AddRangeIfValid(messages);
            OnAuthorisationFailure();
        }

        public void ValidationFailed(IEnumerable<ResponseErrorMessage> messages)
        {
            Errors.AddRangeIfValid(messages);
            OnValidationFailure();
        }

        public void ExecutionFailed(IEnumerable<ResponseErrorMessage> messages)
        {
            Errors.AddRangeIfValid(messages);
            OnExecutionFailure();
        }

        public void MissingEntity(IEnumerable<ResponseErrorMessage> messages)
        {
            Errors.AddRangeIfValid(messages);
            OnMissingEntity();
        }

        public string PrintErrors()
        {
            if (IsSuccess)
                return string.Empty;

            var errorMessage = string.Format("Error: Status code was {0}", StatusCode);

            foreach (var message in errors)
                errorMessage += string.Format("Response error: {0}{1}", message, Environment.NewLine);

            return errorMessage;
        }
    }
}