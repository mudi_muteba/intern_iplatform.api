﻿using System.Collections.Generic;
using System.Net;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    public class ResponseDto<TDTO> where TDTO : class
    {
        public ResponseDto(TDTO responseData, HttpStatusCode httpStatusCode)
        {
            Data = responseData;
            StatusCode = httpStatusCode;
            FailureInfo = new Failure();
        }

        public TDTO Data { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public Failure FailureInfo { get; set; }

        public bool IsSuccess
        {
            get { return StatusCode == HttpStatusCode.OK; }
        }

        public int Id { get; set; }

        public static ResponseDto<TDTO> Success(TDTO responseData)
        {
            return new ResponseDto<TDTO>(responseData, HttpStatusCode.OK);
        }
    }

    public class FailureMessage
    {
        public string PropertyName { get; set; }
        public string DefaultMessage { get; set; }
        public string MessageKey { get; set; }

        public FailureMessage(string message)
        {
            DefaultMessage = message;
        }

        public FailureMessage(string propertyName, string defaultMessage, string messageKey)
        {
            PropertyName = propertyName;
            DefaultMessage = defaultMessage;
            MessageKey = messageKey;
        }
    }

    public class Failure
    {
        public Failure()
        {
            ErrorsList = new List<FailureMessage>();
            Status = ResponseStatuses.Success;
        }

        public Status Status { get; set; }
        public List<FailureMessage> ErrorsList { get; set; }
    }
}