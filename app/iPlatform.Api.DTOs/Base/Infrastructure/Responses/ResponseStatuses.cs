namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    public static class ResponseStatuses
    {
        public static readonly Status Success = new Status(ResponseCode.Success) { Message = "Success" };
        public static readonly Status NotFound = new Status(ResponseCode.NotFound) { Message = "The item was not found" };
        public static readonly Status AssociatedItemNotFound = new Status(ResponseCode.AssociatedItemNotFound) { Message = "One or more of the associated Items was not found" };
        public static readonly Status ErrorSaving = new Status(ResponseCode.ErrorSaving) { Message = "An error occurred while trying to save" };
        public static readonly Status IdIsPopulated = new Status(ResponseCode.IdIsPopulated) { Message = "The ID field can not be populated when creating a new entity" };
        public static readonly Status IdNotPopulated = new Status(ResponseCode.IdNotPopulated) { Message = "The ID field must be populated" };
        public static readonly Status IdentityNumberAllReadyInUse = new Status(ResponseCode.IdentityNumberAllReadyInUse) { Message = "Can not create entity. The Identity Number is allready in use" };

    }
}