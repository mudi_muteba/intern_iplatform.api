﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    [DataContract]
    public class CustomAppResponseDto<TId>
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public TId Data { get; set; }
        [DataMember]
        public string LastErrorDescription { get; set; }
    }
}
