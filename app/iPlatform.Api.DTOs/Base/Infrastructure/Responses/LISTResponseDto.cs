﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;

namespace iPlatform.Api.DTOs.Base.Infrastructure.Responses
{
    [DataContract]
    public class LISTResponseDto<TResponse> : BaseResponseDto
    {
        public LISTResponseDto() { }

        public LISTResponseDto(ListResultDto<TResponse> response)
        {
            Response = response;
            StatusCode = HttpStatusCode.OK;
        }

        [DataMember]
        public ListResultDto<TResponse> Response { get; set; }

        protected override List<HttpStatusCode> SuccessStatusCodes
        {
            get
            {
                return new List<HttpStatusCode>
                {
                    HttpStatusCode.OK
                };
            }
        }

        public LISTResponseDto<TResponse> WithResponse(ListResultDto<TResponse> response)
        {
            Response = response;
            return this;
        }

        public LISTResponseDto<TResponse> CreateLinks(Action<TResponse> createLink)
        {
            if (!(typeof(Resource).IsAssignableFrom(typeof(TResponse))))
            {
                return this;
            }

            if (Response == null || Response.Results == null)
                return this;

            foreach (var r in Response.Results)
            {
                createLink(r);
            }

            return this;
        }
    }
}