﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Base
{
    public class ListResultDto<TDto> : IListResultDto
    {
        public ListResultDto()
        {
            Results = new List<TDto>();
        }

        public List<TDto> Results { get; set; }
    }
}