﻿namespace iPlatform.Api.DTOs.Base
{
    public class Pagination
    {
        private int? pageNumber;
        private int? pageSize;

        public Pagination() : this(1, 20)
        {
        }

        public Pagination(int pageNumber, int pageSize)
        {
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
        }

        public int PageNumber
        {
            get
            {
                return pageNumber.HasValue 
                    ? (pageNumber.Value <= 0 ? 1 : pageNumber.Value)
                    : 1;
            }
            set { pageNumber = value; }
        }

        public int PageSize
        {
            get
            {
                return pageSize.HasValue 
                    ? (pageSize.Value <= 0 ? 1 : pageSize.Value) 
                    : 20;
            }
            set { pageSize = value; }
        }
    }
}