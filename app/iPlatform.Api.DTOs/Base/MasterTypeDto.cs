﻿namespace iPlatform.Api.DTOs.Base
{
    public class MasterTypeDto : Resource
    {

        public string Name { get; set; }

        public string Code { get; set; }

        public int VisibleIndex { get; set; }
    }
}