﻿using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Base
{
    public abstract class AttributeValidationDto : IValidationAvailableDto
    {
        public List<ValidationErrorMessage> Validate()
        {
            var innerValidation = InnerValidate();

            // to be implemented to get validation based on attributes

            return innerValidation;
        }

        protected virtual List<ValidationErrorMessage> InnerValidate()
        {
            return new List<ValidationErrorMessage>();
        }
    }
}