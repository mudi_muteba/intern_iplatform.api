using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.AdditionalMembers
{
    public class WorkflowRetryLogManagement
    {
        public WorkflowRetryLogsConnector WorkflowRetryLogs { get; private set; }

        public WorkflowRetryLogManagement(IRestClient client, ApiToken token)
        {
            WorkflowRetryLogs = new WorkflowRetryLogsConnector(client, token);
        }
    }
}