using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.AdditionalMembers
{
    public class AdditionalMemberManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public AdditionalMemberManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            this.AdditionalMembers = new AdditionalMembersConnector(client, token);

        }

        public AdditionalMembersConnector AdditionalMembers { get; private set; }
        public AdditionalMemberConnector AdditionalMember(int id)
        {
            return new AdditionalMemberConnector(id, client, token);
        }


    }
}