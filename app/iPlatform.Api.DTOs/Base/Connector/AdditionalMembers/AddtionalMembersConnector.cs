﻿using iPlatform.Api.DTOs.AdditionalMembers;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.AdditionalMembers
{
    public class AdditionalMembersConnector : BaseConnector
    {
        public AdditionalMembersConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.AdditionalMembers.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateAdditionalMember(CreateAdditionalMemberDto createAdditionalMemberDto)
        {
            return Post<CreateAdditionalMemberDto, int>(createAdditionalMemberDto, SystemRoutes.AdditionalMembers.Post.Route);
        }

        public LISTResponseDto<ListAdditionalMemberDto> RetrieveByProposalDefinitionId(int id)
        {
            return GetList<ListAdditionalMemberDto>(SystemRoutes.AdditionalMembers.GetByProposalDefId(id));
        }
    }
}