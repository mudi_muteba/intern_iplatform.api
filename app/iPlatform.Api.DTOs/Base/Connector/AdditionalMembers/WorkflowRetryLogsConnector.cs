﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.WorkflowRetries;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.AdditionalMembers
{
    public class WorkflowRetryLogsConnector : BaseConnector
    {
        public WorkflowRetryLogsConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.WorkflowRetryLogs.CreateGetById(id);
        }

        public POSTResponseDto<int> Create(CreateWorkflowRetryLogDto dto)
        {
            return Post<CreateWorkflowRetryLogDto, int>(dto, SystemRoutes.WorkflowRetryLogs.Post.Route);
        }

        public POSTResponseDto<ListResultDto<ListWorkflowRetryLogDto>> Search(SearchWorkflowRetryLogDto searchDto)
        {
            var searchUrl = SystemRoutes.WorkflowRetryLogs.PostSearch.Route;

            return Post<SearchWorkflowRetryLogDto, ListResultDto<ListWorkflowRetryLogDto>>(searchDto, searchUrl);
        }
    }
}