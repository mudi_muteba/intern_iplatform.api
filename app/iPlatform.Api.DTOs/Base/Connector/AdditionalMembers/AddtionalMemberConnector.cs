﻿using iPlatform.Api.DTOs.AdditionalMembers;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.AdditionalMembers
{
    public class AdditionalMemberConnector : BaseConnector
    {
        private readonly int _Id;

        public AdditionalMemberConnector(int funeralMemberId, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _Id = funeralMemberId;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.AdditionalMembers.CreateGetById(_Id);
        }

        public GETResponseDto<ListAdditionalMemberDto> Get()
        {
            return base.Get<ListAdditionalMemberDto>(GetByIdUrl(_Id));
        }

        public PUTResponseDto<int> DeleteAdditionalMember(DisableAdditionalMemberDto disableAdditionalMemberDto)
        {
            return Put<DisableAdditionalMemberDto, int>(disableAdditionalMemberDto, SystemRoutes.AdditionalMembers.DisableById(_Id));
        }

        public PUTResponseDto<int> EditAdditionalMember(EditAdditionalMemberDto editAdditionalMemberDto)
        {
            return Put<EditAdditionalMemberDto, int>(editAdditionalMemberDto, SystemRoutes.AdditionalMembers.UpdateById(_Id));
        }

        public PUTResponseDto<int> Disable()
        {
            return Put<int>(SystemRoutes.AdditionalMembers.DisableById(_Id));
        }
    }
}