﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Statistics;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Statistics
{
    public class LeadConversionStatsConnector : BaseConnector
    {
        public LeadConversionStatsConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.LeadConversionStatistics.CreateGetById(id);
        }

        protected string GetMonthlyByIdUrl(int id)
        {
            return SystemRoutes.LeadConversionStatistics.CreateGetMonthlyById(id);
        }

        public GETResponseDto<StatisticsDto> GetSameDayConversionByAgent(int agentId)
        {
            return base.Get<StatisticsDto>(GetByIdUrl(agentId));
        }

        public GETResponseDto<StatisticsDto> GetMonthlyConversionByAgent(int agentId)
        {
            return base.Get<StatisticsDto>(GetMonthlyByIdUrl(agentId));
        }

    }
}