using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Statistics
{
    public class LeadConversionStatsManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public LeadConversionStatsManagement(IRestClient client, ApiToken token = null)
        {
            this.client = client;
            this.token = token;
            Statistics = new LeadConversionStatsConnector(client, token);
        }

        public LeadConversionStatsConnector Statistics { get; private set; }


    }
}