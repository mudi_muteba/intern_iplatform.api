﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Notifications
{
    public class NotificationManagement
    {
        private readonly IRestClient m_Client;
        private readonly ApiToken m_Token;

        public NotificationManagement(IRestClient client, ApiToken token)
        {
            m_Client = client;
            m_Token = token;
            Notification = new NotificationConnector(client, token);
        }

        public NotificationConnector Notification { get; set; }

        public NotificationConnector Notifications()
        {
            return new NotificationConnector(m_Client, m_Token);
        }
    }
}
