﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Notifications;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Notifications
{
    public class NotificationConnector : BaseConnector
    {
        public NotificationConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {}

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Notification.CreateGetById(id);
        }

        public GETResponseDto<NotificationDto> Get(int id)
        {
            return base.Get<NotificationDto>(GetByIdUrl(id));
        }

        public PUTResponseDto<bool> MarkNotificationAsRead(int notificationUserId)
        {
            var route = SystemRoutes.Notification.CreateMarkNotificationAsRead(notificationUserId);
            return base.Put<bool>(route);
        }

        public POSTResponseDto<int> CreateNotification(CreateNotificationDto dto)
        {
            var route = SystemRoutes.Notification.CreatePost();
            return base.Post<CreateNotificationDto, int>(dto, route);
        }

        public GETResponseDto<ListResultDto<NotificationDto>> GetUnreadNotificationsByRecipientId(int userId)
        {
            return base.Get<ListResultDto<NotificationDto>>(SystemRoutes.Notification.CreateGetUnreadByRecipientId(userId));
        }

        public GETResponseDto<ListResultDto<NotificationDto>> GetNotificationsBySenderId(int userId)
        {
            return base.Get<ListResultDto<NotificationDto>>(SystemRoutes.Notification.CreateGetBySenderId(userId));
        }

        public GETResponseDto<ListResultDto<NotificationDto>> GetAllNotificationsByRecipientId(int userId)
        {
            return base.Get<ListResultDto<NotificationDto>>(SystemRoutes.Notification.CreateGetAllByRecipientId(userId));
        }
    }
}
