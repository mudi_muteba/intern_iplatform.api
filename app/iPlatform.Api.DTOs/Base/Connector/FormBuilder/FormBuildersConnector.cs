﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.FormBuilder;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.FormBuilder
{
    public class FormBuildersConnector : BaseConnector
    {
        public FormBuildersConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.FormBuilder.CreateGetById(id);
        }

        public POSTResponseDto<int> Create(CreateFormBuilderDto createFormBuilderDto)
        {
            return Post<CreateFormBuilderDto, int>(createFormBuilderDto, SystemRoutes.FormBuilder.Post.Route);
        }

        public POSTResponseDto<int> CreateFormBuilderDetail(CreateFormBuilderDetailDto createFormBuilderDetailDto)
        {
            return Post<CreateFormBuilderDetailDto, int>(createFormBuilderDetailDto, SystemRoutes.FormBuilderDetail.Post.Route);
        }

        public POSTResponseDto<PagedResultDto<FormBuilderDto>> Search(SearchFormBuilderDto searchFormBuilderDto)
        {
            return Post<SearchFormBuilderDto, PagedResultDto<FormBuilderDto>>(searchFormBuilderDto, SystemRoutes.FormBuilder.Search.Route);
        }

        public GETResponseDto<ListResultDto<FormBuilderDto>> GetAllNoPagination()
        {
            return Get<ListResultDto<FormBuilderDto>>(SystemRoutes.FormBuilder.GetNoPagination.Route);
        }
    }
}
