﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.FormBuilder
{
    public class FormBuilderManagement
    {
        private readonly IRestClient m_Client;
        private readonly ApiToken m_Token;

        public FormBuilderManagement(IRestClient client, ApiToken token)
        {
            m_Client = client;
            m_Token = token;
            FormBuilders = new FormBuildersConnector(client, token);

        }

        public FormBuildersConnector FormBuilders { get; private set; }

        public FormBuilderConnector FormBuilder(int id)
        {
            return new FormBuilderConnector(id, m_Client, m_Token);
        }
    }
}