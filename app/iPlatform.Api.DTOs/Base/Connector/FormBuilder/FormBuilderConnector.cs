﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.FormBuilder;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.FormBuilder
{
    public class FormBuilderConnector : BaseConnector
    {
        private readonly int m_Id;

        public FormBuilderConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            m_Id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.FormBuilder.CreateGetById(id);
        }

        public GETResponseDto<FormBuilderDto> Get()
        {
            return base.Get<FormBuilderDto>(GetByIdUrl(m_Id));
        }

        public PUTResponseDto<int> EditForm(EditFormBuilderDto editFormBuilderDto)
        {
            return Put<EditFormBuilderDto, int>(editFormBuilderDto, SystemRoutes.FormBuilder.CreatePutById(editFormBuilderDto.Id));
        }

        public PUTResponseDto<int> EditFormDetail(EditFormBuilderDetailDto editFormBuilderDetailDto)
        {
            return Put<EditFormBuilderDetailDto, int>(editFormBuilderDetailDto, SystemRoutes.FormBuilderDetail.CreatePutById(editFormBuilderDetailDto.Id, editFormBuilderDetailDto.IdForm));
        }
    }
}
