﻿using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.ApiVersion;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.SectionsPerChannel;
using iPlatform.Api.DTOs.Users.Authentication;
using iPlatform.Enums;
using Newtonsoft.Json;
using RestSharp;
using Shared.Extentions;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace iPlatform.Api.DTOs.Base.Connector.Authentication
{
    public class AuthenticationConnector : BaseConnector
    {
        public AuthenticationConnector(IRestClient client) : base(client) { }

        public AuthenticateResponseDto Authenticate(string userName, string password, string ipAddress, string system, ApiRequestType apiRequestType)
        {
            var dto = new AuthenticationRequestDto
            {
                Email = userName,
                Password = password,
                IPAddress = ipAddress,
                System = system,
                RequestType = apiRequestType
            };

            return AuthenticateBase(SystemRoutes.Authenticate.Post.Route, dto);
        }

        public AuthenticateResponseDto AuthenticateByToken(string token, int activeChannelId)
        {
            return AuthenticateBase(SystemRoutes.Authenticate.ByToken.Route, new AuthenticationRequestDto {Token = token, ActiveChannelId = activeChannelId});
        }


        /// <summary>
        /// Used to update the the js web token which is used to set the ExecutionContext in the api.
        /// This is intended for iWorkflow use to be able to audit with these parameters as the workflow
        /// impersonates the root user
        /// </summary>
        /// <param name="token"></param>
        /// <param name="activeChannelId"></param>
        /// <param name="channelIds"></param>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public AuthenticateResponseDto AuthenticateByToken(string token, int activeChannelId, int userId, string userName)
        {
            var dto = new AuthenticationRequestDto
            {
                Token = token,
                ActiveChannelId = activeChannelId,
                UserId = userId,
                Email = userName
            };
            return AuthenticateBase(SystemRoutes.Authenticate.ByToken.Route, dto);
        }

        public AuthenticateResponseDto Authenticate(AuthenticationRequestDto dto)
        {
            return AuthenticateBase(SystemRoutes.Authenticate.Post.Route, dto);
        }

        private AuthenticateResponseDto AuthenticateBase(string url, AuthenticationRequestDto authenticationRequest)
        {
            var response = Create(url, authenticationRequest);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
                return AuthenticateResponseDto.NotAuthenticated();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var token = response.Headers.FirstOrDefault(h => h.Name.EqualsIgnoreCase("Authorization"));
                if (token == null)
                    return AuthenticateResponseDto.NotAuthenticated();

                SetToken(token.Value.ToString());

                return string.IsNullOrEmpty(token.Value.ToString())
                    ? AuthenticateResponseDto.NotAuthenticated()
                    : BuildResponse(response);
            }

            return AuthenticateResponseDto.NotAuthenticated();
        }

        private AuthenticateResponseDto BuildResponse(IRestResponse response)
        {
            return JsonConvert.DeserializeObject<AuthenticateResponseDto>(response.Content);
        }

        protected override string GetByIdUrl(int id)
        {
            return string.Empty;
        }

        public GETResponseDto<ApiVersionDto> GetApiVersion()
        {
            return base.Get<ApiVersionDto>(SystemRoutes.Authenticate.GetApiVersions.Route);
        }
    }
}