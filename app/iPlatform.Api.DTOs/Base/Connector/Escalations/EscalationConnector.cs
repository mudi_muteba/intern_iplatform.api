﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Escalations;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Escalations
{
    public class EscalationConnector : BaseConnector
    {
        private readonly int _id;

        public EscalationConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            _id = id;
        }

        public PUTResponseDto<int> Edit(EditEscalationDto dto)
        {
            return Put<EditEscalationDto, int>(dto, GetByIdUrl(_id));
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.EscalationPlanExecutionHistory.CreateGetById(id);
        }
    }
}