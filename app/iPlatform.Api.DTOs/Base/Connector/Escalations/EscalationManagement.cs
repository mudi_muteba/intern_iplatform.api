using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Escalations
{
    public class EscalationManagement
    {
        private readonly IRestClient _client;
        private readonly ApiToken _token;

        public EscalationManagement(IRestClient client, ApiToken token = null)
        {
            _client = client;
            _token = token;
            Escalations = new EscalationsConnector(client, token);
        }

        public EscalationsConnector Escalations { get; private set; }

        public EscalationConnector Escalation(int id)
        {
            return new EscalationConnector(id, _client, _token);
        }
    }
}