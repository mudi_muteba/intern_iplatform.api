﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Escalations;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Escalations
{
    public class EscalationsConnector : BaseConnector
    {
        public EscalationsConnector(IRestClient client, ApiToken token = null) : base(client, token) { }

        protected override string GetByIdUrl(int id)
        {
            return "";
        }

        public POSTResponseDto<PagedResultDto<EscalationPlanDto>> SearchPlans(EscalationPlanDto dto)
        {
            return Post<EscalationPlanDto, PagedResultDto<EscalationPlanDto>>(dto, SystemRoutes.EscalationPlans.CreateGetAllWithPaginationUrl(dto.PageNumber, dto.PageSize));
        }

        public POSTResponseDto<PagedResultDto<EscalationPlanStepDto>> SearchSteps(EscalationPlanStepDto dto)
        {
            return Post<EscalationPlanStepDto, PagedResultDto<EscalationPlanStepDto>>(dto, SystemRoutes.EscalationPlanSteps.CreateGetAllWithPaginationUrl(dto.PageNumber, dto.PageSize));
        }

        public POSTResponseDto<PagedResultDto<EscalationHistoryDto>> SearchHistory(EscalationHistorySearchDto dto)
        {
            return Post<EscalationHistorySearchDto, PagedResultDto<EscalationHistoryDto>>(dto, SystemRoutes.EscalationPlanExecutionHistory.CreateGetAllWithPaginationUrl(dto.PageNumber, dto.PageSize));
        }

        public POSTResponseDto<int> SavePlan(EscalationPlanDto dto)
        {
            return Post<EscalationPlanDto, int>(dto, SystemRoutes.EscalationPlans.Post.Route);
        }

        public POSTResponseDto<int> SaveStep(EscalationPlanStepDto dto)
        {
            return Post<EscalationPlanStepDto, int>(dto, SystemRoutes.EscalationPlanSteps.Post.Route);
        }

        public POSTResponseDto<int> CreateEscalation(CreateEscalationDto dto)
        {
            return Post<CreateEscalationDto, int>(dto, SystemRoutes.EscalationPlanExecutionHistory.Post.Route);
        }
    }
}