using iPlatform.Api.DTOs.Base.Connector.Lookups.Banks;
using iPlatform.Api.DTOs.Base.Connector.Lookups.Occupations;
using iPlatform.Api.DTOs.Base.Connector.Lookups.PostalCodes;
using iPlatform.Api.DTOs.Base.Connector.Lookups.Ratings;
using iPlatform.Api.DTOs.Base.Connector.Lookups.VehicleGuide;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Lookups
{
    public class LookupsManagement
    {
        public LookupsManagement(IRestClient client, ApiToken token)
        {
            PostalCodes = new PostalCodeLookupConnector(client, token);
            VehicleGuide = new VehicleGuideConnector(client, token);
            People = new PersonInformationConnector(client, token);
            Bank = new BanksLookupConnector(client, token);
            Address = new AddressLookupConnector(client, token);
            Occupation = new OccupationLookupConnector(client, token);
            Rating = new RatingConnector(client, token);
        }


        public PostalCodeLookupConnector PostalCodes { get; set; }
        public VehicleGuideConnector VehicleGuide { get; set; }
        public PersonInformationConnector People { get; set; }

        public BanksLookupConnector Bank { get; set; }
        public AddressLookupConnector Address { get; set; }

        public OccupationLookupConnector Occupation { get; set; }
        public RatingConnector Rating { get; set; }

    }
}