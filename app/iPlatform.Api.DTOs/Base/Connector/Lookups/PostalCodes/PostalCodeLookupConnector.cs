﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Lookups.PostalCodes;
using RestSharp;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Base.Connector.Lookups.PostalCodes
{
    public class PostalCodeLookupConnector : BaseConnector
    {
        public PostalCodeLookupConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public LISTPagedResponseDto<MasterTypeDto> GetSimplifiedPostalCodes(string searchTerm = "")
        {
            var url = string.IsNullOrWhiteSpace(searchTerm)
                ? SystemRoutes.Lookups.PostalCodes.GetSimplified.Route
                : SystemRoutes.Lookups.PostalCodes.CreateGetSimplifiedWithSearchTerm(searchTerm);

            return GetPagedList<MasterTypeDto>(url);
        }

        public LISTPagedResponseDto<MasterTypeDto> GetSimplifiedPostalCodes(int pageNumber, int pageSize, string searchTerm = "")
        {
            var url = string.IsNullOrWhiteSpace(searchTerm)
                ? SystemRoutes.Lookups.PostalCodes.CreateSimplifiedPaginationUrl(pageNumber, pageSize)
                : SystemRoutes.Lookups.PostalCodes.CreateSimplifiedSearchTermPaginationUrl(searchTerm, pageNumber, pageSize);

            return GetPagedList<MasterTypeDto>(url);
        }

        public LISTPagedResponseDto<PostalCodeDto> Get(string searchTerm = "")
        {
            var url = string.IsNullOrWhiteSpace(searchTerm)
                ? SystemRoutes.Lookups.PostalCodes.Get.Route
                : SystemRoutes.Lookups.PostalCodes.CreateGetWithSearchTerm(searchTerm);

            return GetPagedList<PostalCodeDto>(url);
        }

        public LISTPagedResponseDto<PostalCodeDto> Get(int pageNumber, int pageSize, string searchTerm = "")
        {
            var url = string.IsNullOrWhiteSpace(searchTerm)
                ? SystemRoutes.Lookups.PostalCodes.CreatePaginationUrl(pageNumber, pageSize)
                : SystemRoutes.Lookups.PostalCodes.CreateSearchTermPaginationUrl(searchTerm, pageNumber, pageSize);

            return GetPagedList<PostalCodeDto>(url);
        }

        public GETResponseDto<List<PostalCodeDto>> SearchSuburbsAndPostalCodes(string searchTerm = "")
        {
            var url = SystemRoutes.Lookups.PostalCodes.CreateSearchSuburbsAndPostalCodes(searchTerm);

            return Get<List<PostalCodeDto>>(url);
        }

    }
}