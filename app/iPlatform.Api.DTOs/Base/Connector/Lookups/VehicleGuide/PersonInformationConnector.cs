using System;
using iPerson.Api.DTOs.PersonInformation;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Lookups.Person;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Lookups.VehicleGuide
{
    public class PersonInformationConnector : BaseConnector
    {
        public PersonInformationConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        public GETResponseDto<PersonInformationResponsesDto> Get(PersonLookupRequest request)
        {
            var url = SystemRoutes.Lookups.Person.CreateGetUrl(request);

            return Get<PersonInformationResponsesDto>(url);
        }

        public GETResponseDto<PersonInformationResponsesDto> GetBySource(PersonLookupRequest request)
        {
            var url = SystemRoutes.Lookups.Person.CreateGetBySourceUrl(request);

            return Get<PersonInformationResponsesDto>(url);
        }

        public GETResponseDto<PersonInformationResponsesDto> GetByIdNumber(string idNumber)
        {
            return Get(new PersonLookupRequest()
            {
                IdNumber = idNumber
            });
        }

        public GETResponseDto<PersonInformationResponsesDto> GetByPhoneNumber(string phoneNumber)
        {
            return Get(new PersonLookupRequest()
            {
                PhoneNumber = phoneNumber
            });
        }

        public GETResponseDto<PersonInformationResponsesDto> GetByEmail(string email)
        {
            return Get(new PersonLookupRequest()
            {
                Email = email
            });
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }
    }
}