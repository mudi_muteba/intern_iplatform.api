﻿using System;
using iGuide.DTOs.Details;
using iGuide.DTOs.Makes;
using iGuide.DTOs.Models;
using iGuide.DTOs.Specifications;
using iGuide.DTOs.Values;
using iGuide.DTOs.Years;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Lookups.VehicleGuide
{
    public class VehicleGuideConnector : BaseConnector
    {
        public VehicleGuideConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        public GETResponseDto<MakeSearchResultsDto> GetMakes()
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Makes.GetAll.Route;
            return Get<MakeSearchResultsDto>(url);
        }

        public GETResponseDto<MakeSearchResultsDto> GetMakes(int year)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Makes.CreateByYearUrl(year);
            return Get<MakeSearchResultsDto>(url);
        }

        public GETResponseDto<MakeSearchResultsDto> SearchMakes(string search)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Makes.CreateSearchUrl(search);
            return Get<MakeSearchResultsDto>(url);
        }

        public GETResponseDto<MakeSearchResultsDto> SearchMakes(int year, string search)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Makes.CreateSearchByYearUrl(search, year);
            return Get<MakeSearchResultsDto>(url);
        }

        public GETResponseDto<ModelSearchResultsDto> GetModels(string make)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Models.CreateByMakeUrl(make);

            return Get<ModelSearchResultsDto>(url);
        }


        public GETResponseDto<ModelSearchResultsDto> SearchModels(int year, string make, string search)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Models.CreateSearchByMakeAndYearUrl(make, year, search);

            return Get<ModelSearchResultsDto>(url);
        }

        public GETResponseDto<ModelSearchResultsDto> GetModels(string make, int year)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Models.CreateByMakeAndYearUrl(make, year);

            return Get<ModelSearchResultsDto>(url);
        }

        public GETResponseDto<ModelSearchResultsDto> SearchModels(string make, string model)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Models.CreateSearchByMakeUrl(make, model);

            return Get<ModelSearchResultsDto>(url);
        }

        public GETResponseDto<ModelSearchResultsDto> SearchModels(string make, int year, string model)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Models.CreateSearchByMakeAndYearUrl(make, year, model);

            return Get<ModelSearchResultsDto>(url);
        }

        public GETResponseDto<VehicleValuesDto> GetVehicleValue(string mmCode, int year)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Vehicles.CreateValuesUrl(mmCode, year);

            return Get<VehicleValuesDto>(url);
        }

        public GETResponseDto<VehicleDetailsDto> GetVehicleInfo(string mmCode, int year)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Vehicles.CreateAboutUrl(mmCode, year);

            return Get<VehicleDetailsDto>(url);
        }

        public GETResponseDto<VehiclesOptionalExtrasDto> GetVehicleExtras(string mmCode, int year)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Vehicles.CreateExtrasUrl(mmCode, year);

            return Get<VehiclesOptionalExtrasDto>(url);
        }

        public GETResponseDto<VehicleSpecsResultsDto> GetVehicleByLicensePlate(string licensePlateNumber)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Vehicles.CreateGetByLicensePlateNumberUrl(licensePlateNumber);
            return Get<VehicleSpecsResultsDto>(url);
        }

        public GETResponseDto<VehicleSpecsResultsDto> GetVehicleByLicensePlateAndId(string licensePlateNumber, string idNumber)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Vehicles.CreateGetByLicensePlateAndIdNumberUrl(licensePlateNumber, idNumber);
            return Get<VehicleSpecsResultsDto>(url);
        }

        public GETResponseDto<VehicleSpecsResultsDto> GetVehicleByRegistrationNumber(string registrationNumber)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Vehicles.CreateGetByRegistrationNumberUrl(registrationNumber);
            return Get<VehicleSpecsResultsDto>(url);
        }

        public GETResponseDto<VehicleYearsDto> GetVehicleYears(string mmCode)
        {
            var url = SystemRoutes.Lookups.VehicleGuide.Vehicles.CreateYearsUrl(mmCode);

            return Get<VehicleYearsDto>(url);
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }
    }
}