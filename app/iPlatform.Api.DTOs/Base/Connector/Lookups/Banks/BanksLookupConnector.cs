﻿using System;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Lookups.Banks
{
    public class BanksLookupConnector : BaseConnector
    {
        public BanksLookupConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public POSTResponseDto<PagedResultDto<ListBankDto>> SearchBanks(
            BankSearchDto bankSearchDto)
        {
            return Post<BankSearchDto, PagedResultDto<ListBankDto>>(bankSearchDto,
                SystemRoutes.Bank.PostSearch.Route);
        }

        public POSTResponseDto<PagedResultDto<ListBankBranchesDto>> SearchBankBranches(
            BankBranchesSearchDto dto)
        {
            return Post<BankBranchesSearchDto, PagedResultDto<ListBankBranchesDto>>(dto,
                SystemRoutes.BankBranch.PostSearch.Route);
        }
    }
}