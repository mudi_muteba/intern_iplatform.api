﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Lookups.PostalCodes;
using RestSharp;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Occupation;

namespace iPlatform.Api.DTOs.Base.Connector.Lookups.Occupations
{
    public class OccupationLookupConnector : BaseConnector
    {
        public OccupationLookupConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }



        public GETResponseDto<List<OccupationDto>> SearchOccupations(string searchTerm = "")
        {
            var url = string.IsNullOrEmpty(searchTerm) ? SystemRoutes.Lookups.Occupation.CreateGetUrl("NOOCCUPATION") : SystemRoutes.Lookups.Occupation.CreateGetUrl(searchTerm);

            return Get<List<OccupationDto>>(url);
        }

    }
}