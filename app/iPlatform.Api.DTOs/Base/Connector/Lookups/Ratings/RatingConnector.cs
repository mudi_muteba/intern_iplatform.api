using System;
using iPerson.Api.DTOs.PersonInformation;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing.Base;
using iPlatform.Api.DTOs.Lookups.Person;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Lookups.Ratings
{
    public class RatingConnector : BaseConnector
    {
        public RatingConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        public GETResponseDto<string> GetRequest(int quoteId)
        {
            var url = SystemRoutes.Lookups.Ratings.CreateGetRequestUrl(quoteId);
            return Get<string>(url);
        }

        public GETResponseDto<string> GetResponse(int quoteId)
        {
            var url = SystemRoutes.Lookups.Ratings.CreateGetResponseUrl(quoteId);
            return Get<string>(url);
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }
    }
}