﻿using System;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;
using iPlatform.Api.DTOs.Lookups.Address;

namespace iPlatform.Api.DTOs.Base.Connector.Lookups.Banks
{
    public class AddressLookupConnector : BaseConnector
    {
        public AddressLookupConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public GETResponseDto<AddressLookupDto> GetProvinceByPostCode(string postCode)
        {
            return base.Get<AddressLookupDto>(SystemRoutes.Lookups.Address.CreateGetProvinceByPostCode(postCode));
        }

        public GETResponseDto<AddressLookupDto> GetProvinceByPostCodeAndCountryCode(string postCode, string countryCode)
        {
            return base.Get<AddressLookupDto>(SystemRoutes.Lookups.Address.CreateGetProvinceByPostCodeAndCountryCode(postCode, countryCode));
        }

        public GETResponseDto<AddressLookupDto> GetProvinceBySuburb(string suburb)
        {
            return base.Get<AddressLookupDto>(SystemRoutes.Lookups.Address.CreateGetProvinceBySuburb(suburb));
        }

        public GETResponseDto<AddressLookupDto> GetProvinceBySuburbAndCountryCode(string suburb, string countryCode)
        {
            return base.Get<AddressLookupDto>(SystemRoutes.Lookups.Address.CreateGetProvinceBySuburbAndCountryCode(suburb, countryCode));
        }
    }
}