﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Lookups.Address.GetAddress
{
    public class GetAddressManagement
    {
        private readonly IRestClient _RestClient;
        private readonly ApiToken _ApiToken;
        public GetAddressConnector GetAddresses { get; private set; }

        public GetAddressManagement(IRestClient restClient, ApiToken apiToken)
        {
            _RestClient = restClient;
            _ApiToken = apiToken;
            GetAddresses = new GetAddressConnector(_RestClient, _ApiToken);
        }

        public GetAddressConnector GetAddress()
        {
            return new GetAddressConnector(_RestClient, _ApiToken);
        }
    }
}
