﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Lookups.Address.GetAddress;
using iPlatform.Api.DTOs.Party.Address;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Lookups.Address.GetAddress
{
    public class GetAddressConnector : BaseConnector
    {
        public GetAddressConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public GETResponseDto<List<ListAddressDto>> GetAddressByPostCode(string postCode)
        {
            return Get<List<ListAddressDto>>(SystemRoutes.GetAddress.CreateGetAddressesByPostCodeUrl(postCode));
        }
    }
}
