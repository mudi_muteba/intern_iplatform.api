﻿using System;
using iPlatform.Api.DTOs.Base.Connector.Handlers;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Search;
using iPlatform.Api.DTOs.Users.UserAuthorisationGroups;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Users
{
    public class UsersConnector : BaseConnector
    {
        public UsersConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        public POSTResponseDto<int> RegisterUser(RegisterUserDto user)
        {
            return Post<RegisterUserDto, int>(user, SystemRoutes.Users.Register.Route);
        }

        public POSTResponseDto<int> CreateUser(CreateUserDto user)
        {
            return Post<CreateUserDto, int>(user, SystemRoutes.Users.Post.Route);
        }

        public POSTResponseDto<int> CreateUserAuthorisation(CreateUserAuthorisationGroupDto createUserAuthorisationGroupDto)
        {
            return Post<CreateUserAuthorisationGroupDto, int>(createUserAuthorisationGroupDto, SystemRoutes.UserAuthorisation.Post.Route);
        }

        public POSTResponseDto<int> CreateUserChannel(CreateUserChannelDto createUserChannelDto)
        {
            return Post<CreateUserChannelDto, int>(createUserChannelDto, SystemRoutes.UserChannel.Post.Route);
        }


        public POSTResponseDto<bool> SaveMultipleUserChannels(SaveUserChannelsDto saveUserChannelsDto)
        {
            return Post<SaveUserChannelsDto, bool>(saveUserChannelsDto, SystemRoutes.UserChannel.Save.Route);
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Users.CreateGetById(id);
        }

        public LISTPagedResponseDto<ListUserDto> Get()
        {
            return GetPagedList<ListUserDto>(SystemRoutes.Users.GetAll.Route);
        }

        public LISTResponseDto<ListUserDto> GetAll()
        {
            return GetList<ListUserDto>(SystemRoutes.Users.GetAllUsers.Route);
        }

        public LISTResponseDto<ListUserDto> GetAllBrokers(int channelId)
        {
            return GetList<ListUserDto>(SystemRoutes.Users.CreateGetAllBrokerUsersUrl(channelId));
        }

        public LISTResponseDto<ListUserDto> GetAllAccountExecutives(int channelId)
        {
            return GetList<ListUserDto>(SystemRoutes.Users.CreateGetAllAccountExecutiveUsersUrl(channelId));
        }

        public LISTResponseDto<ListUserDto> GetAllByChannelId(int channelId)
        {
            return GetList<ListUserDto>(SystemRoutes.Users.CreateGetAllByChannelIdUrl(channelId));
        }

        public LISTPagedResponseDto<ListUserDto> Get(int pageNumber, int pageSize)
        {
            var url = SystemRoutes.Users.CreateGetAllWithPaginationUrl(pageNumber, pageSize);
            return GetPagedList<ListUserDto>(url);
        }

        public GETResponseDto<UserDto> Get(int userId)
        {
            return base.Get<UserDto>(userId);
        }

        public POSTResponseDto<PagedResultDto<ListUserDto>> Search(UserSearchDto searchQuery)
        {
            var searchUrl = SystemRoutes.Users.Search.Route;

            return Post<UserSearchDto, PagedResultDto<ListUserDto>>(searchQuery, searchUrl);
        }

        public POSTResponseDto<PagedResultDto<ListUserDto>> SearchNoDefaultChannel(UserSearchDto searchQuery)
        {
            var searchUrl = SystemRoutes.Users.SearchNoDefaultChannel.Route;

            return Post<UserSearchDto, PagedResultDto<ListUserDto>>(searchQuery, searchUrl);
        }

        public PUTResponseDto<RequestPasswordResetResponseDto> ValidatePasswordResetToken(Guid resetToken)
        {
            return new PUTHandler<RequestPasswordResetResponseDto>(Client).Execute(new ValidatePasswordResetTokenDto(resetToken), SystemRoutes.Users.ValidatePasswordResetToken.Route, false);
        }

        public PUTResponseDto<RequestPasswordResetResponseDto> RequestPasswordReset(string userName)
        {
            return new PUTHandler<RequestPasswordResetResponseDto>(Client).Execute(new RequestPasswordResetDto(userName), SystemRoutes.Users.RequestPasswordReset.Route, false);
        }

        public PUTResponseDto<int> ResetPassword(Guid resetToken, string password)
        {
            return new PUTHandler<int>(Client).Execute(new ResetPasswordDto(password, resetToken), SystemRoutes.Users.ResetPassword.Route, false);
        }


        public POSTResponseDto<ListResultDto<UserInfoDto>> GetCampaignUsers (GetCampaignUsersDto getCampaignUsersDto)
        {
            return Post<GetCampaignUsersDto, ListResultDto<UserInfoDto>>(getCampaignUsersDto, SystemRoutes.Users.GetCampaignUsers.Route);
        }
    }
}