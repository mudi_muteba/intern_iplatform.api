﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Discount;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Users
{
    public class UserConnector : BaseConnector
    {
        private readonly int _userId;

        public UserConnector(int userId, IRestClient client, ApiToken token = null) : base(client, token)
        {
            _userId = userId;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Users.CreateGetById(id);
        }

        public PUTResponseDto<int> Approve()
        {
            return Put<ApproveUserDto, int>(new ApproveUserDto
            {
                Id = _userId
            }, SystemRoutes.Users.CreateApproveUrl(_userId));
        }

        public PUTResponseDto<int> Disable()
        {
            return Put<DisableUserDto, int> (new DisableUserDto
            {
                Id = _userId
            }, SystemRoutes.Users.CreateDisableUserUrl(_userId));
        }

        public PUTResponseDto<int> Edit(EditUserDto editUser)
        {
            editUser.Id = _userId;
            return Put<EditUserDto, int>(editUser, SystemRoutes.Users.CreateEditUrl(_userId));
        }

        #region Discount

        public GETResponseDto<DiscountDto> GetDiscount(int id)
        {
            return Get<DiscountDto>(SystemRoutes.UserDiscounts.CreateGetById(_userId, id));
        }

        public POSTResponseDto<DiscountDto> GetDiscountByCriteria(SearchDiscountDto searchDto)
        {
            return Post<SearchDiscountDto, DiscountDto>(searchDto, SystemRoutes.UserDiscounts.SearchByCriteria(_userId));
        }

        public LISTResponseDto<ListDiscountDto> GetDiscounts(int channelId)
        {
            return GetList<ListDiscountDto>(SystemRoutes.UserDiscounts.CreateGetByUserIdAndChannelId(_userId, channelId));
        }

        public POSTResponseDto<int> CreateDiscount(CreateDiscountDto createDiscountDto)
        {
            return Post<CreateDiscountDto, int>(createDiscountDto, SystemRoutes.UserDiscounts.CreateGetById(_userId));
        }

        public PUTResponseDto<int> EditDiscount(EditDiscountDto updateDiscountDto)
        {
            return Put<EditDiscountDto, int>(updateDiscountDto, SystemRoutes.UserDiscounts.CreateGetById(_userId, updateDiscountDto.Id));
        }

        public DELETEResponseDto<int> DeleteDiscount(int id)
        {
            var deleteDto = new DeleteDiscountDto
            {
                Id = id,
                UserId = _userId
            };

            return Delete<DeleteDiscountDto, int>(deleteDto, SystemRoutes.UserDiscounts.CreateGetById(_userId, id));
        }

        #endregion
    }
}