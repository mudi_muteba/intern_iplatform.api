using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Users
{
    public class UserManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public UserManagement(IRestClient client, ApiToken token = null)
        {
            this.client = client;
            this.token = token;
            Users = new UsersConnector(client, token);
        }

        public UsersConnector Users { get; private set; }

        public UserConnector User(int id)
        {
            return new UserConnector(id, client, token);
        }
    }
}