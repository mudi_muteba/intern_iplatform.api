﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments;
using iPlatform.Api.DTOs.Reports.SignFlowDocuments.Criteria;
using iPlatform.Api.DTOs.SignFlow;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SignFlow
{
    public class SignFlowConnector : BaseConnector
    {
        public SignFlowConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        public PUTResponseDto<int> Update(EditSignFlowDocumentDto dto)
        {
            return Put<EditSignFlowDocumentDto, int>(dto, SystemRoutes.SignFlow.Put.Route);
        }

        public PUTResponseDto<int> UpdateStatus(UpdateSignFlowDocumentDto dto)
        {
            return Put<UpdateSignFlowDocumentDto, int>(dto, SystemRoutes.SignFlow.PutUpdateStatus.Route);
        }

        public POSTResponseDto<int> SendSignFlowDocument(CreateSignFlowDocumentDto criteria)
        {
            return Post<CreateSignFlowDocumentDto, int>(criteria, SystemRoutes.SignFlow.Post.Route);
        }

        public POSTResponseDto<bool> SendMultipleSignFlowDocument(CreateMultipleSignFlowDocumentDto criteria)
        {
            return Post<CreateMultipleSignFlowDocumentDto, bool>(criteria, SystemRoutes.SignFlow.PostMultiple.Route);
        }

        public POSTResponseDto<BrokerNoteReportDto> GetDataForBrokerNoteReport(BrokerNoteReportCriteriaDto criteria)
        {
            return Post<BrokerNoteReportCriteriaDto, BrokerNoteReportDto>(criteria, SystemRoutes.SignFlow.GetBrokerNoteReport.Route);
        }

        public POSTResponseDto<SlaReportDto> GetDataForSlaReport(SlaReportCriteriaDto criteria)
        {
            return Post<SlaReportCriteriaDto, SlaReportDto>(criteria, SystemRoutes.SignFlow.GetSlaReport.Route);
        }

        public POSTResponseDto<PopiDocReportDto> GetDataForPopiDocReport(PopiDocReportCriteriaDto criteria)
        {
            return Post<PopiDocReportCriteriaDto, PopiDocReportDto>(criteria, SystemRoutes.SignFlow.GetPopiDocReport.Route);
        }

        public POSTResponseDto<DebitOrderReportDto> GetDataForDebitOrderReport(DebitOrderReportCriteriaDto criteria)
        {
            return Post<DebitOrderReportCriteriaDto, DebitOrderReportDto>(criteria, SystemRoutes.SignFlow.GetDebitOrderReport.Route);
        }

        public POSTResponseDto<RecordOfAdviceReportDto> GetDataForRecordOfAdviceReport(RecordOfAdviceReportCriteriaDto criteria)
        {
            return Post<RecordOfAdviceReportCriteriaDto, RecordOfAdviceReportDto>(criteria, SystemRoutes.SignFlow.GetRecordOfAdviceReport.Route);
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }
    }
}
