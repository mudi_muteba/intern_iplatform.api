﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SignFlow
{
    public class SignFlowManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public SignFlowManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            SignFlow = new SignFlowConnector(client, token);

        }

        public SignFlowConnector SignFlow { get; private set; }
    }
}
