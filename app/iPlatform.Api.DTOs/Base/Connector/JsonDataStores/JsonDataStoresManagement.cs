﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.JsonDataStores
{
    public class JsonDataStoresManagement
    {
        private readonly IRestClient _client;
        private readonly ApiToken _token;

        public JsonDataStoresConnector JsonDataStores { get; private set; }

        public JsonDataStoresManagement(IRestClient client, ApiToken token = null)
        {
            this._client = client;
            this._token = token;
            JsonDataStores = new JsonDataStoresConnector(client, token);
        }

        public JsonDataStoreConnector JsonDataStore(int id)
        {
            return new JsonDataStoreConnector(id, _client, _token);
        }

    }
}
