﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Connector.Handlers;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.JsonDataStores;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.JsonDataStores
{
    public class JsonDataStoresConnector : BaseConnector
    {
        public JsonDataStoresConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        public GETResponseDto<ListResultDto<JsonDataStoreDto>> GetByPartyId(int id)
        {
            var url = SystemRoutes.JsonDataStores.CreateGetByPartyId(id);

            var foo = Get<ListResultDto<JsonDataStoreDto>>(url);
            return foo;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.JsonDataStores.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateJsonDataStore(CreateJsonDataStoreDto createJsonDataStoreDto)
        {
            return Post<CreateJsonDataStoreDto, int>(createJsonDataStoreDto, SystemRoutes.JsonDataStores.Post.Route);
        }

        public POSTResponseDto<ListResultDto<JsonDataStoreDto>> SearchJsonDataStores(JsonDataStoreSearchDto jsonDataStoreSearchDto)
        {
            return Post<JsonDataStoreSearchDto, ListResultDto<JsonDataStoreDto>>(jsonDataStoreSearchDto,
                SystemRoutes.JsonDataStores.PostSearch.Route);
        }
    }
}
