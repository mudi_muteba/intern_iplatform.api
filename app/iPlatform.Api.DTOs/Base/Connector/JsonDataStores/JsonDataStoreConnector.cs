﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Connector.Handlers;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.JsonDataStores;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.JsonDataStores
{
    public class JsonDataStoreConnector : BaseConnector
    {
        private readonly int _Id;

        public JsonDataStoreConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            _Id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.JsonDataStores.CreateGetById(id);
        }

        public GETResponseDto<JsonDataStoreDto> Get()
        {
            return base.Get<JsonDataStoreDto>(GetByIdUrl(_Id));
        }
    }
}
