using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Canaries;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Canaries
{
    public class CanariesConnector : BaseConnector
    {
        public CanariesConnector(IRestClient client, ApiToken token = null) : base(client, token) { }

        protected override string GetByIdUrl(int id)
        {
            return "";
        }

        public POSTResponseDto<int> Create(CreateCanaryConsumerResultDto dto)
        {
            return Post<CreateCanaryConsumerResultDto, int>(dto, SystemRoutes.Canaries.Post.Route);
        }
    }
}