using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Canaries
{
    public class CanaryManagement
    {
        private readonly IRestClient _client;
        private readonly ApiToken _token;

        public CanaryManagement(IRestClient client, ApiToken token = null)
        {
            _client = client;
            _token = token;
            Canaries = new CanariesConnector(client, token);
        }

        public CanariesConnector Canaries { get; private set; }
    }
}