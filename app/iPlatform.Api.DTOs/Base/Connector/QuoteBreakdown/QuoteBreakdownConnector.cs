﻿using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base.Connector.Individuals.ProposalDefinitionQuestions;
using iPlatform.Api.DTOs.Base.Connector.Individuals.ProposalHeaders;
using iPlatform.Api.DTOs.Base.Connector.Individuals.SalesForce;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using iPlatform.Api.DTOs.Party.Bank;
using iPlatform.Api.DTOs.Party.Contacts;
using iPlatform.Api.DTOs.Party.Correspondence;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.Note;
using iPlatform.Api.DTOs.Party.Payments;
using iPlatform.Api.DTOs.QuoteBreakdown;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.QuoteBreakdown
{
    public class QuoteBreakdownConnector : BaseConnector
    {
        private readonly int _quotebreakdownId;

        public QuoteBreakdownConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _quotebreakdownId = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.QuoteBreakdown.CreateGetById(id);
        }
        
        public PUTResponseDto<int> EditQuoteBreakdown(EditQuoteItemBreakDownDto updateQuotebreakdownDto)
        {
            return Put<EditQuoteItemBreakDownDto, int>(updateQuotebreakdownDto,
                SystemRoutes.QuoteBreakdown.EditBreakDownString(updateQuotebreakdownDto.Id));
        }

    }
}