using iPlatform.Api.DTOs.Base.Connector.Individuals;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.QuoteBreakdown
{
    public class QuoteBreakdownManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public QuoteBreakdownManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
           

        }
        
        public QuoteBreakdownConnector Quotebreakdown(int id)
        {
            return new QuoteBreakdownConnector(id, client, token);
        }
    }
}