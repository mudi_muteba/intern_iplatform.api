﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Party.LossHistoryDefinitions;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.LossHistoryDefinitions
{
    public class LossHistoryDefinitionConnector : BaseConnector
    {
        public LossHistoryDefinitionConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _Id = id;
        }

        private readonly int _Id;

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.LossHistoryDefinitions.CreateGetById(id);
        }

        public GETResponseDto<LossHistoryDefinitionDto> Get()
        {
            return base.Get<LossHistoryDefinitionDto>(GetByIdUrl(_Id));
        }
        
    }
}
