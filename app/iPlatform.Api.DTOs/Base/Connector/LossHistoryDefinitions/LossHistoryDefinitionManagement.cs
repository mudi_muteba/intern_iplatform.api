﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.LossHistoryDefinitions
{
    public class LossHistoryDefinitionManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public LossHistoryDefinitionManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            this.LossHistoryDefinition = new LossHistoryDefinitionsConnector(client, token);
        }

        public LossHistoryDefinitionsConnector LossHistoryDefinition { get; private set; }

        public LossHistoryDefinitionConnector LossHistoryDefinitions(int id)
        {
            return new LossHistoryDefinitionConnector(id, client, token);
        }
    }
}
