﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.LossHistoryDefinitions;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.LossHistoryDefinitions
{
    public class LossHistoryDefinitionsConnector : BaseConnector
    {
        public LossHistoryDefinitionsConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.LossHistoryDefinitions.CreateGetById(id);
        }

        public GETResponseDto<ListResultDto<LossHistoryDefinitionDto>> GetByPartyId(int id)
        {
            var url = SystemRoutes.LossHistoryDefinitions.CreateGetByPartyId(id);

            var foo = Get<ListResultDto<LossHistoryDefinitionDto>>(url);
            return foo;
        }
    }
}
