﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector.VehicleGuideSetting;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.DisplaySetting
{
    public class DisplaySettingManagement
    {
        private readonly IRestClient _Client;
        private readonly ApiToken _Token;

        public DisplaySettingsConnector DisplaySettings { get; private set; }

        public DisplaySettingManagement(IRestClient client, ApiToken token)
        {
            _Client = client;
            _Token = token;
            DisplaySettings = new DisplaySettingsConnector(_Client, _Token);
        }

        public DisplaySettingConnector DisplaySetting(int id)
        {
            return new DisplaySettingConnector(id, _Client, _Token);
        }
    }
}
