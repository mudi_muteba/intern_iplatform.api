﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Individual;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.DisplaySetting
{
    public class DisplaySettingsConnector : BaseConnector
    {
        public DisplaySettingsConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.DisplaySettings.CreateGetById(id);
        }

        /// <summary>
        /// Create a new DisplaySetting.
        /// </summary>
        /// <param name="createDisplaySettingDto"></param>
        /// <returns></returns>
        public POSTResponseDto<int> Create(CreateDisplaySettingDto createDisplaySettingDto)
        {
            return Post<CreateDisplaySettingDto, int>(createDisplaySettingDto, SystemRoutes.DisplaySettings.Post.Route);
        }

        /// <summary>
        /// Get all DisplaySettings that have not been deleted.
        /// </summary>
        /// <returns></returns>
        public GETResponseDto<ListResultDto<ListDisplaySettingDto>> GetAll()
        {
            return Get<ListResultDto<ListDisplaySettingDto>>(SystemRoutes.DisplaySettings.Get.Route);
        }

        /// <summary>
        /// Get all DisplaySettings with pagination that have not been deleted.
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public GETResponseDto<PagedResultDto<ListDisplaySettingDto>> GetAllWithPagination(int pageNumber, int pageSize)
        {
            string url = SystemRoutes.DisplaySettings.CreateGetWithPaginationUrl(pageNumber, pageSize);
            return Get<PagedResultDto<ListDisplaySettingDto>>(url);
        }

        /// <summary>
        /// Get all DisplaySettings with the passed search criteria(ChannelName, ProductName) that have not been deleted.
        /// </summary>
        /// <param name="searchDisplaySettingDto"></param>
        /// <returns></returns>
        public POSTResponseDto<PagedResultDto<ListDisplaySettingDto>> Search(SearchDisplaySettingDto searchDisplaySettingDto)
        {
            return Post<SearchDisplaySettingDto, PagedResultDto<ListDisplaySettingDto>>(searchDisplaySettingDto,
                SystemRoutes.DisplaySettings.Search.Route);
        }

        /// <summary>
        /// Gets all DisplaySettings by PartyId, ChannelId and ThirdPartyDispolay is true
        /// </summary>
        /// <param name="individualDto"></param>
        /// <returns></returns>
        public POSTResponseDto<ListResultDto<ListDisplaySettingDto>> GetRequiresThirdPartyIntegration(
            IndividualDto individualDto)
        {
            return Post<IndividualDto, ListResultDto<ListDisplaySettingDto>>(individualDto,
                SystemRoutes.DisplaySettings.GetRequiresThirdPartyIntegration.Route);
        }
    }
}