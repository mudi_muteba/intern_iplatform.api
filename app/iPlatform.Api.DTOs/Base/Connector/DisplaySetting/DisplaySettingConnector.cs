﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.DisplaySetting
{
    public class DisplaySettingConnector:BaseConnector
    {
        private readonly int _Id;

        public DisplaySettingConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            _Id = id;
        }
        
        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.DisplaySettings.CreateGetById(id);
        }

        /// <summary>
        /// Get a DisplaySetting by it's unique identifier
        /// </summary>
        /// <returns></returns>
        public GETResponseDto<DisplaySettingDto> Get()
        {
            return Get<DisplaySettingDto>(_Id);
        }

        /// <summary>
        /// Updates a DisplaySetting by it's unique identifier
        /// </summary>
        /// <param name="editDisplaySettingDto"></param>
        /// <returns></returns>
        public PUTResponseDto<int> Update(EditDisplaySettingDto editDisplaySettingDto)
        {
            return Put<EditDisplaySettingDto, int>(editDisplaySettingDto, SystemRoutes.DisplaySettings.CreatePutById(_Id));
        }

        /// <summary>
        /// Deletes a DisplaySetting by it's unique identifier
        /// </summary>
        /// <param name="deleteDisplaySettingDto"></param>
        /// <returns></returns>
        public PUTResponseDto<int> Delete()
        {
            return Put<int>(SystemRoutes.DisplaySettings.CreateDeleteById(_Id));
        }
    }
}
