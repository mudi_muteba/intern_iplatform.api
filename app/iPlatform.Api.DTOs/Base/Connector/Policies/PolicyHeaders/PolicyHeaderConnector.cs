﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Policy;
using iPlatform.Api.DTOs.Policy.FNOL;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Policies.PolicyHeaders
{
    public class PolicyHeaderConnector : BaseConnector
    {
        public PolicyHeaderConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _PolicyHeaderId = id;
        }
        private readonly int _PolicyHeaderId;
        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.PolicyHeaders.CreateGetById(id);
        }

        public GETResponseDto<PolicyHeaderDto> Get()
        {
            return base.Get<PolicyHeaderDto>(GetByIdUrl(_PolicyHeaderId));
        }


        public POSTResponseDto<PolicyDataFnolDto> GetDataForFNOL(GetPolicyDataForFNOLDto dto)
        {
            dto.Id = _PolicyHeaderId;
            return base.Post<GetPolicyDataForFNOLDto, PolicyDataFnolDto>(dto, SystemRoutes.PolicyHeaders.GetDataForFNOL.Route);
        }

        public POSTResponseDto<ListResultDto<PolicyHeaderClaimableItemDto>> GetAvailableItemForFNOL(GetPolicyAvailableItemsForFNOLDto dto)
        {
            dto.Id = _PolicyHeaderId;
            return base.Post<GetPolicyAvailableItemsForFNOLDto, ListResultDto<PolicyHeaderClaimableItemDto>>(dto, SystemRoutes.PolicyHeaders.GetAvailableItemsForFNOL.Route);
        }

        public PUTResponseDto<int> UpdatePolicyByQuoteId(EditPolicyHeaderDto dto)
        {
            return Put<EditPolicyHeaderDto, int>(dto, SystemRoutes.PolicyHeaders.UpdateByQuoteId.Route);
        }

    }
}