using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Policies.PolicyHeaders
{
    public class PolicyHeaderManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public PolicyHeaderManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            PolicyHeaders = new PolicyHeadersConnector(client, token);

        }

        public PolicyHeadersConnector PolicyHeaders { get; private set; }

        public PolicyHeaderConnector PolicyHeader(int id)
        {
            return new PolicyHeaderConnector(id, client, token);
        }
    }
}