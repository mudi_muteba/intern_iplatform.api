﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Policy;
using iPlatform.Api.DTOs.Upload;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Policies.PolicyHeaders
{
    public class PolicyHeadersConnector : BaseConnector
    {
        public PolicyHeadersConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.PolicyHeaders.CreateGetById(id);
        }

        private string GetFNOLByPartyIdUrl(int partyid)
        {
            return SystemRoutes.PolicyHeaders.CreateGetFNOLByPartyId(partyid);
        }

        public POSTResponseDto<int> CreatePolicyWhenQuoteAccepted(CreatePolicyWhenQuoteAcceptedDto createPolicyWhenQuoteAcceptedDto)
        {
            return Post<CreatePolicyWhenQuoteAcceptedDto, int>(createPolicyWhenQuoteAcceptedDto, SystemRoutes.PolicyHeaders.PostQuoteAccepted.Route);
        }

        public POSTResponseDto<int> CreatePolicyHeader(CreatePolicyHeaderDto createPolicyHeaderDto)
        {
            return Post<CreatePolicyHeaderDto, int>(createPolicyHeaderDto, SystemRoutes.PolicyHeaders.Post.Route);
        }

        public POSTResponseDto<PagedResultDto<PolicyHeaderDto>> SearchPolicyHeader(PolicySearchDto policySearchDto)
        {
            return Post<PolicySearchDto, PagedResultDto<PolicyHeaderDto>>(policySearchDto, SystemRoutes.PolicyHeaders.Search.Route);
        }

        public POSTResponseDto<ListResultDto<PolicyHeaderDto>> SearchPolicyHeaderNoPagination(PolicySearchDto policySearchDto)
        {
            return Post<PolicySearchDto, ListResultDto<PolicyHeaderDto>>(policySearchDto, SystemRoutes.PolicyHeaders.SearchWithoutPagination.Route);
        }

        public POSTResponseDto<Guid> PolicyConfirmation(PolicyUploadDto policyUploadDto)
        {
            return Post<PolicyUploadDto, Guid>(policyUploadDto, SystemRoutes.PolicyUploadConfirmations.Post.Route);
        }

        public GETResponseDto<ListResultDto<PolicyHeaderDto>> GetPoliciesForFNOL(int partyid)
        {
            return base.Get<ListResultDto<PolicyHeaderDto>>(GetFNOLByPartyIdUrl(partyid));
        }
    }
}