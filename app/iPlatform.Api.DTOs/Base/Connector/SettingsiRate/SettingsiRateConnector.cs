﻿using System;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Campaigns;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SettingsiRate
{
    public class SettingsiRateConnector : BaseConnector
    {
        private readonly int _Id;

        public SettingsiRateConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            _Id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.SettingsiRate.CreateGetById(id);
        }

        public GETResponseDto<SettingsiRateDto> Get()
        {
            return base.Get<SettingsiRateDto>(GetByIdUrl(_Id));
        }

        public PUTResponseDto<int> Edit(EditSettingsiRateDto editSettingsiRateDto)
        {
            return Put<EditSettingsiRateDto, int>(editSettingsiRateDto, SystemRoutes.SettingsiRate.CreatePutById(editSettingsiRateDto.Id));
        }

        public PUTResponseDto<int> Delete()
        {
            return Put<int>(SystemRoutes.SettingsiRate.CreateDeleteById(_Id));
        }
    }
}
