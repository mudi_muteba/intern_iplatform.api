﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SettingsiRate
{
    public class SettingsiRateManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public SettingsiRateManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            SettingsiRates = new SettingsiRatesConnector(client, token);

        }

        public SettingsiRatesConnector SettingsiRates { get; private set; }

        public SettingsiRateConnector SettingsiRate(int id)
        {
            return new SettingsiRateConnector(id, client, token);
        }
    }
}