﻿using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SettingsiRate
{
    public class SettingsiRatesConnector : BaseConnector
    {
        public SettingsiRatesConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.SettingsiRate.CreateGetById(id);
        }

        public POSTResponseDto<int> Create(CreateSettingsiRateDto createSettingsiRateDto)
        {
            return Post<CreateSettingsiRateDto, int>(createSettingsiRateDto, SystemRoutes.SettingsiRate.Post.Route);
        }

        public POSTResponseDto<bool> SaveMultiple(SaveMultipleSettingsiRateDto saveMultipleSettingsiRateDto)
        {
            return Post<SaveMultipleSettingsiRateDto, bool>(saveMultipleSettingsiRateDto, SystemRoutes.SettingsiRate.PostMultiple.Route);
        }

        public PagedResultDto<ListSettingsiRateDto> GetAll()
        {
            var result = Get<PagedResultDto<ListSettingsiRateDto>>(SystemRoutes.SettingsiRate.Get.Route);
            return result.Response;
        }

        public GETResponseDto<PagedResultDto<ListSettingsiRateDto>> GetAllPaginated(int pageNumber, int pageSize)
        {
            var url = SystemRoutes.SettingsiRate.CreateGetWithPaginationUrl(pageNumber, pageSize);
            return Get<PagedResultDto<ListSettingsiRateDto>>(url);
        }

        public POSTResponseDto<PagedResultDto<ListSettingsiRateDto>> Search(SearchSettingsiRateDto searchSettingsiRateDto)
        {
            return Post<SearchSettingsiRateDto, PagedResultDto<ListSettingsiRateDto>>(searchSettingsiRateDto, SystemRoutes.SettingsiRate.Search.Route);
        }
    }
}
