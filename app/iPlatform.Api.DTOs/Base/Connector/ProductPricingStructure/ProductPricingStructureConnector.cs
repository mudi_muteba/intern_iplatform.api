﻿using RestSharp;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.ProductPricingStructure;

namespace iPlatform.Api.DTOs.Base.Connector.ProductPricingStructure
{
    public class ProductPricingStructureConnector : BaseConnector
    {
        private readonly int m_Id;

        public ProductPricingStructureConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            m_Id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ProductPricingStructure.CreateGetById(id);
        }

        public GETResponseDto<ProductPricingStructureDto> GetByChannelIdProductIdAndCoverId(int channelId, int productId, int coverId)
        {
            return Get<ProductPricingStructureDto>(SystemRoutes.ProductPricingStructure.CreateGetByChannelIdProductIdAndCoverId(channelId, productId, coverId));
        }
    }
}
