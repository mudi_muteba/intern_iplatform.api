﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.ProductPricingStructure;

namespace iPlatform.Api.DTOs.Base.Connector.ProductPricingStructure
{
    public class ProductPricingStructureManagement
    {
        private readonly IRestClient m_Client;
        private readonly ApiToken m_Token;

        public ProductPricingStructureManagement(IRestClient client, ApiToken token)
        {
            m_Client = client;
            m_Token = token;
        }

        public ProductPricingStructureConnector ProductPricingStructure(int id = 0)
        {
            return new ProductPricingStructureConnector(id, m_Client, m_Token);
        }
    }
}
