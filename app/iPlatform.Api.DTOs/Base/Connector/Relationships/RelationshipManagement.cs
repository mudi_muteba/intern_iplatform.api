using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Relationships
{
    public class RelationshipManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public RelationshipManagement(IRestClient client, ApiToken token = null)
        {
            this.client = client;
            this.token = token;
            Relationship = new RelationshipConnector(client, token);
        }

        public RelationshipConnector Relationship { get; private set; }


    }
}