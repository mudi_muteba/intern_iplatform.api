﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Relationships;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Relationships
{
    public class RelationshipConnector : BaseConnector
    {
        public RelationshipConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Relationships.CreateGetById(id);
        }

        protected string GetByPartyIdUrl(int id)
        {
            return SystemRoutes.Relationships.CreateGetByPartyId(id);
        }

        public GETResponseDto<RelationshipDto> Get(int id)
        {
            return base.Get<RelationshipDto>(GetByIdUrl(id));
        }

        public GETResponseDto<ListResultDto<RelationshipDto>> GetByPartyId(int partyid)
        {
            return base.Get < ListResultDto<RelationshipDto>>(GetByPartyIdUrl(partyid));
        }

        public POSTResponseDto<int> CreateRelationship(CreateRelationshipDto dto)
        {
            return Post<CreateRelationshipDto, int>(dto, SystemRoutes.Relationships.Post.Route);
        }

        public PUTResponseDto<int> EditRelationship(EditRelationshipDto dto)
        {
            return Put<EditRelationshipDto, int>(dto, SystemRoutes.Relationships.UpdateById(dto.Id));
        }

        public PUTResponseDto<int> DeleteRelationship(int partyid)
        {
            return Put<int>(SystemRoutes.Relationships.DeleteByPartyId(partyid));
        }


    }
}