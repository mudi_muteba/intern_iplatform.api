﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.HistoryLoss.LossHistory
{
    public class LossHistoriesConnector : BaseConnector
    {
        public LossHistoriesConnector(IRestClient client, ApiToken token = null) 
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.LossHistory.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateLossHisotry(CreateLossHistoryDto createLossHistoryDto)
        {
            return Post<CreateLossHistoryDto, int>(createLossHistoryDto, SystemRoutes.LossHistory.Post.Route);
        }

        public GETResponseDto<ListResultDto<LossHistoryDto>> GetByPartyId(int id)
        {
            var url = SystemRoutes.LossHistory.CreateGetByPartyId(id);

            var foo = Get<ListResultDto<LossHistoryDto>>(url);
            return foo;
        }

        public POSTResponseDto<PagedResultDto<LossHistoryDto>> SearchLossHistory(LossHistorySearchDto searchDto)
        {
            return Post<LossHistorySearchDto, PagedResultDto<LossHistoryDto>>(searchDto,
                SystemRoutes.LossHistory.PostSearch.Route);
        }
    }
}
