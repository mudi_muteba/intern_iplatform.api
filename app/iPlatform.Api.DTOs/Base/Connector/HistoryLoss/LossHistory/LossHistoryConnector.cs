﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.HistoryLoss.LossHistory
{
    public class LossHistoryConnector : BaseConnector
    {
        public LossHistoryConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _Id = id;
        }

        private readonly int _Id;

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.LossHistory.CreateGetById(id);
        }

        public GETResponseDto<LossHistoryDto> Get()
        {
            return base.Get<LossHistoryDto>(GetByIdUrl(_Id));
        }

        public PUTResponseDto<int> Disable()
        {
            return Put<int>(SystemRoutes.LossHistory.DisableById(_Id));
        }

        public PUTResponseDto<int> EditLossHistory(EditLossHistoryDto editLossHistoryDto)
        {
            return Put<EditLossHistoryDto, int>(editLossHistoryDto, SystemRoutes.LossHistory.PutById(editLossHistoryDto.Id));
        }
    }
}
