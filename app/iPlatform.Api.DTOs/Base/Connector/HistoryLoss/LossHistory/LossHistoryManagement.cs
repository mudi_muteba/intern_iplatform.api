﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.HistoryLoss.LossHistory
{
    public class LossHistoryManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public LossHistoryManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            this.LossHistories = new LossHistoriesConnector(client, token);
        }

        public LossHistoriesConnector LossHistories { get; private set; }

        public LossHistoryConnector LossHistory(int id)
        {
            return new LossHistoryConnector(id, client, token);
        }
    }
}
