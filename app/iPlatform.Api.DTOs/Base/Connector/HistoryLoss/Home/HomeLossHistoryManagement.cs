using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.HistoryLoss.Home
{
    public class HomeLossHistoryManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public HomeLossHistoryManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            this.HomeLossHistories = new HomeLossHistoriesConnector(client, token);

        }

        public HomeLossHistoriesConnector HomeLossHistories { get; private set; }

        public HomeLossHistoryConnector HomeLossHistory(int id)
        {
            return new HomeLossHistoryConnector(id, client, token);
        }
    }
}