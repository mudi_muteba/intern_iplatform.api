﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.HistoryLoss.Home
{
    public class HomeLossHistoryConnector : BaseConnector
    {
        public HomeLossHistoryConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _Id = id;
        }

        private readonly int _Id;

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.HomeLossHistory.CreateGetById(id);
        }

        public GETResponseDto<ListHomeLossHistoryDto> Get()
        {
            return base.Get<ListHomeLossHistoryDto>(GetByIdUrl(_Id));
        }

        public PUTResponseDto<int> Disable()
        {
            return Put<int>(SystemRoutes.HomeLossHistory.DisableById(_Id));
        }

        public PUTResponseDto<int> EditHomeLossHistory(EditHomeLossHistoryDto editHomeLossHistoryDto)
        {
            return Put<EditHomeLossHistoryDto, int>(editHomeLossHistoryDto, SystemRoutes.HomeLossHistory.PutById(editHomeLossHistoryDto.Id));
        }
    }
}