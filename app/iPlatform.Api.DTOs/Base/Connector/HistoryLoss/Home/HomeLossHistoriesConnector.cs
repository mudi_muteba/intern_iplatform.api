﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.HistoryLoss.Home;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.HistoryLoss.Home
{
    public class HomeLossHistoriesConnector : BaseConnector
    {
        public HomeLossHistoriesConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.HomeLossHistory.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateHomeLossHistory(CreateHomeLossHistoryDto createHomeLossHistoryDto)
        {
            return Post<CreateHomeLossHistoryDto, int>(createHomeLossHistoryDto, SystemRoutes.HomeLossHistory.Post.Route);
        }

        public PagedResultDto<ListHomeLossHistoryDto> GetAllHomeLossHistory()
        {
            var result = Get<PagedResultDto<ListHomeLossHistoryDto>>(SystemRoutes.HomeLossHistory.GetAll.Route);
            return result.Response;
        }

        public PagedResultDto<ListHomeLossHistoryDto> SearchLossHistory(SearchHomeLossHistoryDto searchDto)
        {
            var result = Post<SearchHomeLossHistoryDto, PagedResultDto<ListHomeLossHistoryDto>>(searchDto,
                SystemRoutes.HomeLossHistory.PostSearch.Route);

            return result.Response;
        }
    }
}