﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.HistoryLoss.Motor
{
    public class MotorLossHistoryConnector : BaseConnector
    {
        public MotorLossHistoryConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _Id = id;
        }

        private readonly int _Id;

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.MotorLossHistory.CreateGetById(id);
        }

        public GETResponseDto<ListMotorLossHistoryDto> Get()
        {
            return base.Get<ListMotorLossHistoryDto>(GetByIdUrl(_Id));
        }

        public PUTResponseDto<int> Disable()
        {
            return Put<int>(SystemRoutes.MotorLossHistory.DisableById(_Id));
        }

        public PUTResponseDto<int> EditMotorLossHistory(EditMotorLossHistoryDto editMotorLossHistoryDto)
        {
            return Put<EditMotorLossHistoryDto, int>(editMotorLossHistoryDto, SystemRoutes.MotorLossHistory.PutById(editMotorLossHistoryDto.Id));
        }
    }
}