using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.HistoryLoss.Motor
{
    public class MotorLossHistoryManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public MotorLossHistoryManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            this.MotorLossHistories = new MotorLossHistoriesConnector(client, token);

        }

        public MotorLossHistoriesConnector MotorLossHistories { get; private set; }

        public MotorLossHistoryConnector MotorLossHistory(int id)
        {
            return new MotorLossHistoryConnector(id, client, token);
        }


    }
}