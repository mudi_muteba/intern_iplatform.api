﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.HistoryLoss.Motor;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.HistoryLoss.Motor
{
    public class MotorLossHistoriesConnector : BaseConnector
    {
        public MotorLossHistoriesConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.MotorLossHistory.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateMotorLossHistory(CreateMotorLossHistoryDto createMotorLossHistoryDto)
        {
            return Post<CreateMotorLossHistoryDto, int>(createMotorLossHistoryDto, SystemRoutes.MotorLossHistory.Post.Route);
        }

        public PagedResultDto<ListMotorLossHistoryDto> GetAllMotorLossHistory()
        {
            var result = Get<PagedResultDto<ListMotorLossHistoryDto>>(SystemRoutes.MotorLossHistory.GetAll.Route);
            return result.Response;
        }
        public PagedResultDto<ListMotorLossHistoryDto> SearchLossHistory(SearchMotorLossHistoryDto searchDto)
        {
            var result = Post<SearchMotorLossHistoryDto, PagedResultDto<ListMotorLossHistoryDto>>(searchDto,
                SystemRoutes.MotorLossHistory.PostSearch.Route);

            return result.Response;
        }
    }
}