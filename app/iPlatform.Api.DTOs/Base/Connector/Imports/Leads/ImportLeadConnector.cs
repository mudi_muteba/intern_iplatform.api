﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Excel;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Imports.Leads
{
    public class ImportLeadConnector : BaseConnector
    {
        public ImportLeadConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public POSTResponseDto<Guid> ImportLeadSeriti(LeadImportXMLDto leadImportXMLDto)
        {
            return Post<LeadImportXMLDto, Guid>(leadImportXMLDto, SystemRoutes.LeadImport.PostSeriti.Route);
        }

        public POSTResponseDto<Guid> ImportLeadExcel(LeadImportExcelDto leadImportExcelDto)
        {
            return Post<LeadImportExcelDto, Guid>(leadImportExcelDto, SystemRoutes.LeadImport.PostExcel.Route);
        }

        public POSTResponseDto<int> CreateIndividualUploadHeader(CreateIndividualUploadHeaderDto dto)
        {
            return Post<CreateIndividualUploadHeaderDto, int>(dto, SystemRoutes.IndividualHeaderUpload.Create.Route);
        }

        public PUTResponseDto<int> EditIndividualUploadHeader(EditIndividualUploadHeaderDto dto)
        {
            return Put<EditIndividualUploadHeaderDto, int>(dto, SystemRoutes.IndividualHeaderUpload.Edit.Route);
        }

        public POSTResponseDto<int> CreateIndividualUploadDetail(IndividualUploadDetailDto dto)
        {
            return Post<IndividualUploadDetailDto, int>(dto, SystemRoutes.IndividualUploadDetail.Create.Route);
        }

        public PUTResponseDto<Guid> EditIndividualUploadDetail(EditIndividualUploadDetailDto dto)
        {
            return Put<EditIndividualUploadDetailDto, Guid>(dto, SystemRoutes.IndividualUploadDetail.EditGetById(dto.LeadImportReference));
        }

        public LISTPagedResponseDto<CreateIndividualUploadHeaderDto> GetAllHeaderWithPagination(int pageNumber, int pageSize)
        {
            var url = SystemRoutes.IndividualHeaderUpload.GetAllWithPaginationUrl(pageNumber, pageSize);
            return GetPagedList<CreateIndividualUploadHeaderDto>(url);
        }

        public LISTPagedResponseDto<IndividualUploadDetailDto> GetAllDetailWithPagination(Guid reference, int pageNumber, int pageSize)
        {
            var url = SystemRoutes.IndividualUploadDetail.GetAllWithPaginationUrl(reference, pageNumber, pageSize);
            return GetPagedList<IndividualUploadDetailDto>(url);
        }

        public POSTResponseDto<string> SubmitLead(SubmitLeadDto submitLeadDto)
        {
            return Post<SubmitLeadDto, string>(submitLeadDto, SystemRoutes.LeadImport.SubmitLead.Route);
        }

        public GETResponseDto<IndividualUploadDetailDto> GetByLeadImportReference(Guid leadImportReference)
        {
            string url = SystemRoutes.IndividualUploadDetail.CreateGetByLeadImportReference(leadImportReference);
            return Get<IndividualUploadDetailDto>(url);
        }
    }
}