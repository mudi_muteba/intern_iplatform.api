using iPlatform.Api.DTOs.Base.Connector.Imports.Individuals;
using iPlatform.Api.DTOs.Base.Connector.Imports.Leads;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Imports
{
    public class ImportManagement
    {
        public ImportManagement(IRestClient client, ApiToken token)
        {
            Individual = new ImportIndividualConnector(client, token);
            Lead = new ImportLeadConnector(client, token);
        }
        public ImportIndividualConnector Individual { get; set; }
        public ImportLeadConnector Lead { get; set; }
    }
}