﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Individual;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Imports.Individuals
{
    public class ImportIndividualConnector : BaseConnector
    {
        public ImportIndividualConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public POSTResponseDto<Guid> Import(ImportIndividualDto importIndividualDto)
        {
            string url = SystemRoutes.IndividualImport.CreateGetById();
            return Post<ImportIndividualDto, Guid>(importIndividualDto, url);
        }
    }
}