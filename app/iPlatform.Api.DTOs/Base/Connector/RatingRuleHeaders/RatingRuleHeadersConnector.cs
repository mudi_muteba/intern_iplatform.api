﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.RatingRuleHeaders
{
    public class RatingRuleHeadersConnector : BaseConnector
    {
        public RatingRuleHeadersConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }
        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.RatingRuleHeaders.CreateGetById(id);
        }

        public POSTResponseDto<int> Create(CreateRatingRuleHeaderDto dto)
        {
            return Post<CreateRatingRuleHeaderDto, int>(dto, SystemRoutes.RatingRuleHeaders.Post.Route);
        }

        public PUTResponseDto<int> Edit(EditRatingRuleHeaderDto dto)
        {
            return Put<EditRatingRuleHeaderDto, int>(dto, SystemRoutes.RatingRuleHeaders.CreatePutById(dto.Id));
        }
        
        public GETResponseDto<ListResultDto<ListRatingRuleHeaderDto>> GetAll()
        {
            return Get<ListResultDto<ListRatingRuleHeaderDto>>(SystemRoutes.RatingRuleHeaders.Get.Route);
        }
    }
}
