﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.RatingRuleHeaders
{
    public class RatingRuleHeaderManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public RatingRuleHeadersConnector RatingRuleHeaders { get; private set; }

        public RatingRuleHeaderManagement(IRestClient client, ApiToken token = null)
        {
            this.client = client;
            this.token = token;
            this.RatingRuleHeaders = new RatingRuleHeadersConnector(client, token);
        }
       
        public RatingRuleHeaderConnector RatingRuleHeader(int id)
        {
            return new RatingRuleHeaderConnector(id, client, token);
        }
    }
}
