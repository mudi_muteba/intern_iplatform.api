﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.RatingRuleHeaders
{
    public class RatingRuleHeaderConnector: BaseConnector
    {
        private readonly int id;

        public RatingRuleHeaderConnector(int id, IRestClient client, ApiToken token = null): base(client, token)
        {
            this.id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.RatingRuleHeaders.CreateGetById(id);
        }

        public POSTResponseDto<int> Create(CreateRatingRuleHeaderDto dto)
        {
            return Post<CreateRatingRuleHeaderDto, int>(dto, SystemRoutes.RatingRuleHeaders.Post.Route);
        }

        public PUTResponseDto<int> Edit(EditRatingRuleHeaderDto dto)
        {
            return Put<EditRatingRuleHeaderDto, int>(dto, SystemRoutes.RatingRuleHeaders.CreatePutById(dto.Id));
        }

        public PUTResponseDto<int> Delete()
        {
            return Put<int>(SystemRoutes.RatingRuleHeaders.CreateDeleteById(id));
        }

        public GETResponseDto<RatingRuleHeaderDto> Get()
        {
            return base.Get<RatingRuleHeaderDto>(id);
        }
    }
}
