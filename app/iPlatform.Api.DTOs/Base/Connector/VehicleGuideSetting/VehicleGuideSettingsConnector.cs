﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Lookups.VehicleGuide.VehicleGuideSetting;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.VehicleGuideSetting
{
    public class VehicleGuideSettingsConnector: BaseConnector
    {
        public VehicleGuideSettingsConnector(IRestClient client, ApiToken token = null) 
            : base(client, token)
        {
        }

        public POSTResponseDto<int> Create(CreateVehicleGuideSettingDto createVehicleGuideSettingDto)
        {
            return Post<CreateVehicleGuideSettingDto, int>(createVehicleGuideSettingDto, SystemRoutes.VehicleGuideSetting.Post.Route);
        }

        public POSTResponseDto<bool> SaveMultiple(SaveMultipleVehicleGuideSettingDto saveMultipleVehicleGuideSettingDto)
        {
            return Post<SaveMultipleVehicleGuideSettingDto, bool>(saveMultipleVehicleGuideSettingDto, SystemRoutes.VehicleGuideSetting.PostMultiple.Route);
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.VehicleGuideSetting.CreateGetById(id);
        }

        public GETResponseDto<PagedResultDto<VehicleGuideSettingDto>> GetAll()
        {
            return Get<PagedResultDto<VehicleGuideSettingDto>>(SystemRoutes.VehicleGuideSetting.Get.Route);
        }

        public GETResponseDto<PagedResultDto<ListVehicleGuideSettingDto>> GetAllPaginated(int pageNumber, int pageSize)
        {
            var url = SystemRoutes.VehicleGuideSetting.CreateGetWithPaginationUrl(pageNumber, pageSize);
            return Get<PagedResultDto<ListVehicleGuideSettingDto>>(url);
        }        
    }
}
