﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.VehicleGuideSetting
{
    public class VehicleGuideSettingManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public VehicleGuideSettingsConnector VehicleGuideSettings { get; private set; }

        public VehicleGuideSettingManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            VehicleGuideSettings = new VehicleGuideSettingsConnector(client, token);
        }

        public VehicleGuideSettingConnector VehicleGuideSetting(int id)
        {
            return new VehicleGuideSettingConnector(id, client, token);
        }
    }
}
