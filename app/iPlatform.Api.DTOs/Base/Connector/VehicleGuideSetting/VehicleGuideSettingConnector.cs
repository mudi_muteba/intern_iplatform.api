﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.VehicleGuideSetting
{
    public class VehicleGuideSettingConnector : BaseConnector
    {
        private readonly int id;
        public VehicleGuideSettingConnector(int id, IRestClient client, ApiToken token = null) 
            : base(client, token)
        {
            this.id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.VehicleGuideSetting.CreateGetById(id);
        }

        public GETResponseDto<VehicleGuideSettingDto> Get()
        {
            return base.Get<VehicleGuideSettingDto>(id);
        }

        public PUTResponseDto<int> Edit(EditVehicleGuideSettingDto dto)
        {
            return Put<EditVehicleGuideSettingDto, int>(dto, SystemRoutes.VehicleGuideSetting.CreatePutById(dto.Id));
        }

        public PUTResponseDto<int> Delete()
        {
            return Put<int>(SystemRoutes.VehicleGuideSetting.CreateDeleteById(id));
        }
    }
}
