﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.SecondLevel.Answers;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SecondLevel
{
    public class SecondLevelConnector : BaseConnector
    {
        public SecondLevelConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {

        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public GETResponseDto<SecondLevelQuestionsForQuoteDto> GetSecondLevelQuestionsByQuoteId(int quoteId)
        {
            var url = SystemRoutes.SecondLevel.Questions.CreateGetQuestionsByQuoteId(quoteId);
            return Get<SecondLevelQuestionsForQuoteDto>(url);
        }

        public PUTResponseDto<bool> SaveSecondLevelAnswersForQuoteId(SecondLevelQuestionsAnswersSaveDto secondLevelQuestionsAnswersSave, int quoteId)
        {
            var url = SystemRoutes.SecondLevel.Answers.CreateSaveAnswersForQuoteId(quoteId);
            return Put<SecondLevelQuestionsAnswersSaveDto, bool>(secondLevelQuestionsAnswersSave, url);
        }
    }
}
