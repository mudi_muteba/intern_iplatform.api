﻿using System;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SettingsiPerson
{
    public class SettingsiPersonConnector : BaseConnector
    {
        private readonly int _settingsiPersonId;

        public SettingsiPersonConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            _settingsiPersonId = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.SettingsiPerson.CreateGetById(id);
        }

        public GETResponseDto<SettingsiPersonDto> Get()
        {
            return base.Get<SettingsiPersonDto>(GetByIdUrl(_settingsiPersonId));
        }
        public PUTResponseDto<int> Delete()
        {
            return Put<int>(SystemRoutes.SettingsiPerson.CreateDeleteById(_settingsiPersonId));
        }

        public PUTResponseDto<int> Edit(EditSettingsiPersonDto editSettingsiPersonDto)
        {
            return Put<EditSettingsiPersonDto, int>(editSettingsiPersonDto, SystemRoutes.SettingsiPerson.CreatePutById(editSettingsiPersonDto.Id));
        }

    }
}
