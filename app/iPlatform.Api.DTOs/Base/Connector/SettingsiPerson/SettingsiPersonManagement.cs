﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SettingsiPerson
{
    public class SettingsiPersonManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public SettingsiPersonManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            SettingsiPersons = new SettingsiPersonsConnector(client, token);

        }

        public SettingsiPersonsConnector SettingsiPersons { get; private set; }

        public SettingsiPersonConnector SettingsiPerson(int id)
        {
            return new SettingsiPersonConnector(id, client, token);
        }
    }
}