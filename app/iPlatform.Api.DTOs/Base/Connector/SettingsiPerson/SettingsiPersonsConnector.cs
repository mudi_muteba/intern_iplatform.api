﻿using System;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SettingsiPerson
{
    public class SettingsiPersonsConnector : BaseConnector
    {
        public SettingsiPersonsConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
           return SystemRoutes.SettingsiPerson.CreateGetById(id);
        }


        public POSTResponseDto<PagedResultDto<ListSettingsiPersonDto>> Search(SearchSettingsiPersonDto searchSettingsiPersonDto)
        {
            return Post<SearchSettingsiPersonDto, PagedResultDto<ListSettingsiPersonDto>>(searchSettingsiPersonDto, SystemRoutes.SettingsiPerson.Search.Route);
        }

        public POSTResponseDto<int> Create(CreateSettingsiPersonDto createSettingsiPersonDto)
        {
            return Post<CreateSettingsiPersonDto, int>(createSettingsiPersonDto, SystemRoutes.SettingsiPerson.Post.Route);
        }

        public GETResponseDto<PagedResultDto<ListSettingsiPersonDto>> Get()
        {
            return Get<PagedResultDto<ListSettingsiPersonDto>>(SystemRoutes.SettingsiPerson.Get.Route);
        }

        public GETResponseDto<PagedResultDto<ListSettingsiPersonDto>> GetAllPaginated(int pageNumber, int pageSize)
        {
            var url = SystemRoutes.SettingsiPerson.CreateGetWithPaginationUrl(pageNumber, pageSize);
            return Get<PagedResultDto<ListSettingsiPersonDto>>(url);
        }

        public GETResponseDto<ListResultDto<ListSettingsiPersonDto>> GetAll()
        {
            return Get<ListResultDto<ListSettingsiPersonDto>>(SystemRoutes.SettingsiPerson.GetNoPagination.Route);
        }

        public POSTResponseDto<bool> SaveMultiple(SaveMultipleSettingsiPersonDto saveMultipleSettingsiPersonDto)
        {
            return Post<SaveMultipleSettingsiPersonDto, bool>(saveMultipleSettingsiPersonDto, SystemRoutes.SettingsiPerson.SaveMultiple.Route);
        }
    }
}
