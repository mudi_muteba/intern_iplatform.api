﻿using iPlatform.Api.DTOs.Base.Connector.AdditionalMembers;
using iPlatform.Api.DTOs.Base.Connector.Admin;
using iPlatform.Api.DTOs.Base.Connector.Authentication;
using iPlatform.Api.DTOs.Base.Connector.Campaigns;
using iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsItems;
using iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsTypes;
using iPlatform.Api.DTOs.Base.Connector.FuneralDualCover;
using iPlatform.Api.DTOs.Base.Connector.FuneralMembers;
using iPlatform.Api.DTOs.Base.Connector.HistoryLoss.Home;
using iPlatform.Api.DTOs.Base.Connector.HistoryLoss.LossHistory;
using iPlatform.Api.DTOs.Base.Connector.HistoryLoss.Motor;
using iPlatform.Api.DTOs.Base.Connector.Imports;
using iPlatform.Api.DTOs.Base.Connector.Individuals;
using iPlatform.Api.DTOs.Base.Connector.Insurers;
using iPlatform.Api.DTOs.Base.Connector.JsonDataStores;
using iPlatform.Api.DTOs.Base.Connector.Leads;
using iPlatform.Api.DTOs.Base.Connector.Lookups;
using iPlatform.Api.DTOs.Base.Connector.Payment;
using iPlatform.Api.DTOs.Base.Connector.Policies.PolicyHeaders;
using iPlatform.Api.DTOs.Base.Connector.Products;
using iPlatform.Api.DTOs.Base.Connector.Proposals;
using iPlatform.Api.DTOs.Base.Connector.Questions;
using iPlatform.Api.DTOs.Base.Connector.Quotes;
using iPlatform.Api.DTOs.Base.Connector.Ratings;
using iPlatform.Api.DTOs.Base.Connector.Relationships;
using iPlatform.Api.DTOs.Base.Connector.SecondLevel;
using iPlatform.Api.DTOs.Base.Connector.Teams;
using iPlatform.Api.DTOs.Base.Connector.Users;
using iPlatform.Api.DTOs.Base.Connector.Statistics;
using iPlatform.Api.DTOs.Base.Connector.Audit;
using iPlatform.Api.DTOs.Base.Connector.Canaries;
using iPlatform.Api.DTOs.Base.Connector.Commercials;
using iPlatform.Api.DTOs.Base.Connector.CoverLinksConnector;
using iPlatform.Api.DTOs.Base.Connector.Escalations;
using iPlatform.Api.DTOs.Base.Connector.Reports;
using iPlatform.Api.DTOs.Base.Connector.SignFlow;
using iPlatform.Api.DTOs.Base.Connector.Organizations;
using iPlatform.Api.DTOs.Base.Connector.SettingsiPerson;
using iPlatform.Api.DTOs.Base.Connector.SettingsiRate;
using iPlatform.Api.DTOs.Base.Connector.SettingsQuoteUploads;
using iPlatform.Api.DTOs.Base.Connector.VehicleGuideSetting;
using iPlatform.Api.DTOs.Base.Connector.ChannelEvents;
using iPlatform.Api.DTOs.Base.Connector.DisplaySetting;
using iPlatform.Api.DTOs.Base.Connector.Lookups.Address.GetAddress;
using iPlatform.Api.DTOs.Base.Connector.QuoteBreakdown;
using iPlatform.Api.DTOs.Base.Connector.Components.HeadersConnector;
using iPlatform.Api.DTOs.Base.Connector.Components.DefinitionsConnector;
using iPlatform.Api.DTOs.Base.Connector.Components.QuestionAnswersConnector;
using iPlatform.Api.DTOs.Base.Connector.Components.QuestionDefinitionConnector;
using iPlatform.Api.DTOs.Base.Connector.OverrideRatingQuestion;
using iPlatform.Api.DTOs.Base.Connector.Admin.EmailCommunicationSettings;
using iPlatform.Api.DTOs.Base.Connector.Admin.ChannelTemplates;
using iPlatform.Api.DTOs.Base.Connector.Admin.SMSCommunicationSettings;
using iPlatform.Api.DTOs.Base.Connector.FormBuilder;
using iPlatform.Api.DTOs.Base.Connector.Logs;
using iPlatform.Api.DTOs.Base.Connector.ProductPricingStructure;
using iPlatform.Api.DTOs.Base.Connector.DataView;
using iPlatform.Api.DTOs.Base.Connector.PolicyBindings;
using iPlatform.Api.DTOs.Base.Connector.SettingsiRateUser;
using iPlatform.Api.DTOs.Base.Connector.ItcQuestionConnector;
using iPlatform.Api.DTOs.Base.Connector.Notifications;

namespace iPlatform.Api.DTOs.Base.Connector
{
    public interface IConnector
    {
        Connector Reset(string token);
        void SetToken(string token);
        AuthenticationConnector Authentication { get; }
        CampaignManagement CampaignManagement { get; }
        ChannelManagement ChannelManagement { get; }
        ClaimsHeaderManagement ClaimsHeaderManagement { get; }
        ClaimsItemManagement ClaimsItemManagement { get; }
        ComponentHeaderManagement ComponentHeaderManagement { get; }
        ComponentDefinitionManagement ComponentDefinitionManagement { get; }
        ComponentQuestionAnswerManagement ComponentQuestionAnswerManagement { get; }
        ComponentQuestionDefinitionManagement ComponentQuestionDefinitionManagement { get; }
        FuneralMemberManagement FuneralMemberManagement { get; }
        HomeLossHistoryManagement HomeLossHistoryManagement { get; }
        ImportManagement ImportManagement { get; }
        IndividualManagement IndividualManagement { get; }
        InsurerManagement InsurerManagement { get; }
        LeadManagement LeadManagement { get; }
        LookupsManagement Lookups { get; }
        MotorLossHistoryManagement MotorLossHistoryManagement { get; }
        PaymentDetailsManagement PaymentDetailsManagement { get; }
        PolicyHeaderManagement PolicyHeaderManagement { get; }
        ProductManagement ProductManagement { get; }
        ProposalManagement ProposalManagement { get; }
        QuestionManagement QuestionManagement { get; }
        RatingsConnector Ratings { get; }
        RelationshipManagement RelationshipManagement { get; }
        SecondLevelConnector SecondLevel { get; }
        TeamManagement TeamManagement { get; }
        UserManagement UserManagement { get; }
        LeadConversionStatsManagement LeadConversionStatsManagement { get; }
        QuotesManagement QuotesManagement { get; }
        QuoteBreakdownManagement QuoteBreakdownManagement { get; }
        FuneralDualCoverManagement FuneralDualCoverManagement { get; }
        AuditManagement AuditManagement { get; }
        ReportManagement ReportManagement { get; }
        JsonDataStoresManagement JsonDataStoresManagement { get; }
        ClaimsTypeManagement ClaimsTypeManagement { get; }
        LossHistoryManagement LossHistoryManagement { get; }
        AdditionalMemberManagement AdditionalMemberManagement { get; }
        WorkflowRetryLogManagement WorkflowRetryLogManagement { get; }
        CustomerSatisfactionSurveyManagement CustomerSatisfactionSurveyManagement { get; }
        EscalationManagement EscalationManagement { get; }
        CanaryManagement CanaryManagement { get; }
        SignFlowManagement SignFlowManagement { get; }
        SalesManagement SalesManagement { get; }
        CoverLinksManagement CoverLinksManagement { get; }
        OrganizationsManagement OrganizationsManagement { get; }
        SettingsiPersonManagement SettingsiPersonManagement { get; }
        SettingsiRateManagement SettingsiRateManagement { get; }
        SettingsQuoteUploadManagement SettingsQuoteUploadManagement { get; }
        Documents.DocumentManagement DocumentManagement { get; }
        ChannelEventManagement ChannelEventManagement { get; }
        VehicleGuideSettingManagement VehicleGuideSetting { get; }
        GetAddressManagement GetAddressManagement { get; }

        DisplaySettingManagement DisplaySettingManagement { get; }

        OverrideRatingQuestionManagement OverrideRatingQuestionManagement { get; }



        ChannelTemplateManagement ChannelTemplateManagement { get; }
        EmailCommunicationSettingManagement EmailCommunicationSettingManagement { get; }
        SMSCommunicationSettingManagement SMSCommunicationSettingManagement { get; }
        FormBuilderManagement FormBuilderManagement { get; }

        ProductPricingStructureManagement ProductPricingStructureManagement { get; }
        LogFileManagement LogFileManagement { get; }
        DataViewManagement DataViewManagement { get; }
        PolicyBindingsManagement PolicyBindingsManagement { get; }
        SettingsiRateUserManagement SettingsiRateUserManagement { get; }

        ItcQuestionManagement ItcQuestionManagement { get; }
        NotificationManagement NotificationManagement { get; }
    }
}