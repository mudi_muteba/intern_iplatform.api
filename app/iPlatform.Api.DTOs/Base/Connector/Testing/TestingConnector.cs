﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Testing
{
    public class TestingConnector : BaseConnector
    {
        public TestingConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public BaseResponseDto Failure()
        {
            return Get<BaseResponseDto>(SystemRoutes.Test.Failure.Route);
        }

        public BaseResponseDto InvalidRoute()
        {
            return Get<BaseResponseDto>(SystemRoutes.Test.Failure.Route + "_never");
        }
    }
}