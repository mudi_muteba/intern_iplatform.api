using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.FuneralMembers
{
    public class FuneralMemberManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public FuneralMemberManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            this.FuneralMembers = new FuneralMembersConnector(client, token);

        }

        public FuneralMembersConnector FuneralMembers { get; private set; }
        public FuneralMemberConnector FuneralMember(int id)
        {
            return new FuneralMemberConnector(id, client, token);
        }


    }
}