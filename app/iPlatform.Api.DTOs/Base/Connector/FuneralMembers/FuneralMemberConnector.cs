﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.FuneralMembers;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.FuneralMembers
{
    public class FuneralMemberConnector : BaseConnector
    {
        private readonly int _Id;

        public FuneralMemberConnector(int funeralMemberId, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _Id = funeralMemberId;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.FuneralMembers.CreateGetById(_Id);
        }

        public GETResponseDto<ListFuneralMemberDto> Get()
        {
            return base.Get<ListFuneralMemberDto>(GetByIdUrl(_Id));
        }

        public PUTResponseDto<int> DeleteFuneralMember(DisableFuneralMemberDto disableFuneralMemberDto)
        {
            return Put<DisableFuneralMemberDto, int>(disableFuneralMemberDto, SystemRoutes.FuneralMembers.DisableById(_Id));
        }

        public PUTResponseDto<int> EditFuneralMember(EditFuneralMemberDto editFuneralMemberDto)
        {
            return Put<EditFuneralMemberDto, int>(editFuneralMemberDto, SystemRoutes.FuneralMembers.UpdateById(_Id));
        }

        public PUTResponseDto<int> Disable()
        {
            return Put<int>(SystemRoutes.FuneralMembers.DisableById(_Id));
        }
    }
}