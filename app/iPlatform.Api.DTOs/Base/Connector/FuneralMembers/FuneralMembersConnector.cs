﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.FuneralMembers;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.FuneralMembers
{
    public class FuneralMembersConnector : BaseConnector
    {
        public FuneralMembersConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.FuneralMembers.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateFuneralMember(CreateFuneralMemberDto createFuneralMemberDto)
        {
            return Post<CreateFuneralMemberDto, int>(createFuneralMemberDto, SystemRoutes.FuneralMembers.Post.Route);
        }

        public LISTResponseDto<ListFuneralMemberDto> RetrieveByProposalDefinitionId(int id)
        {
            return GetList<ListFuneralMemberDto>(SystemRoutes.FuneralMembers.GetByProposalDefId(id));
        }
    }
}