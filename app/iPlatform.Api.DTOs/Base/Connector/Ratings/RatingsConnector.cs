using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Ratings
{
    public class RatingsConnector : BaseConnector
    {
        public RatingsConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public POSTResponseDto<RatingResultDto> GetRating(RatingRequestDto request)
        {
            var url = SystemRoutes.Rating.CreateRatingRequestUrl();

            return Post<RatingRequestDto, RatingResultDto>(request, url);
        }

        public POSTResponseDto<int> CreateQuote(RatingResultDto ratingResult)
        {
            var url = SystemRoutes.Quotes.CreateCreateQuote(ratingResult);

            return Post<RatingResultDto, int>(ratingResult, url);
        }

        public GETResponseDto<QuoteHeaderDto> GetQuoteByExternalReference(string externalReference)
        {
            var url = SystemRoutes.Quotes.CreateGetByExternalReferenceUrl(externalReference);
            return Get<QuoteHeaderDto>(url);
        }

        public GETResponseDto<ProposalHeaderDto> GetProposalByExternalReference(string externalReference)
        {
            var url = SystemRoutes.IndividualProposalHeaders.CreateGetByExternalReferenceUrl(externalReference);
            return Get<ProposalHeaderDto>(url);
        }

        public POSTResponseDto<QuoteHeaderDto> GetQuoteByProposalId(CreateQuoteDto createQuote)
        {
            var url = SystemRoutes.IndividualProposalHeaders.GetQuoteByProposalId.Route;
            return Post<CreateQuoteDto,QuoteHeaderDto>(createQuote,url);
        }
    }
}