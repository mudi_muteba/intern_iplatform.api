﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Payments;
using iPlatform.Api.DTOs.Products;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Payments
{
    public class PaymentDetailsConnector : BaseConnector
    {
        public PaymentDetailsConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.PaymentDetails.CreateGetById(id);
        }

        public POSTResponseDto<PagedResultDto<PaymentDetailsDto>> SearchPaymentDetails(
            PaymentDetailSearchDto paymentDetailSearchDto)
        {
            return Post<PaymentDetailSearchDto, PagedResultDto<PaymentDetailsDto>>(paymentDetailSearchDto,
                SystemRoutes.PaymentDetails.Search.Route);
        }
    }
}