using iPlatform.Api.DTOs.Base.Connector.Payments;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Payment
{
    public class PaymentDetailsManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public PaymentDetailsManagement(IRestClient client, ApiToken token = null)
        {
            this.client = client;
            this.token = token;
            PaymentDetails = new PaymentDetailsConnector(client, token);
        }

        public PaymentDetailsConnector PaymentDetails { get; private set; }
    }
}