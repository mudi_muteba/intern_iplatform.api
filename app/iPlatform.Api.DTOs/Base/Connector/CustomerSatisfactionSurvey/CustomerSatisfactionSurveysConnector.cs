﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Party.CustomerSatisfactionSurvey;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Campaigns
{
    public class CustomerSatisfactionSurveysConnector : BaseConnector
    {
        public CustomerSatisfactionSurveysConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.CustomerSatisfactionSurvey.CreateGetById(id);
        }

        public POSTResponseDto<int> Create(CreateCustomerSatisfactionSurveyDto dto)
        {
            return Post<CreateCustomerSatisfactionSurveyDto, int>(dto, SystemRoutes.CustomerSatisfactionSurvey.Save.Route);
        }

    }
}