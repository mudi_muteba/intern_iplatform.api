using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Campaigns
{
    public class CustomerSatisfactionSurveyManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public CustomerSatisfactionSurveyManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            Surveys = new CustomerSatisfactionSurveysConnector(client, token);

        }

        public CustomerSatisfactionSurveysConnector Surveys { get; private set; }

    }
}