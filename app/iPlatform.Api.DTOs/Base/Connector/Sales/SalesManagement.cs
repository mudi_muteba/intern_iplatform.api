﻿using iPlatform.Api.DTOs.Base.Connector.Sales;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Commercials
{
    public class SalesManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public SalesManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            Sales = new SalesConnector(client, token);
            SalesTag = new SalesTagsConnector(client, token);
        }

        public SalesConnector Sales { get; private set; }
        public SalesTagsConnector SalesTag { get; private set; }

    }
}
