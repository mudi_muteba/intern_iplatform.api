﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Sales;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Sales
{
    public class SalesTagsConnector : BaseConnector
    {
        public SalesTagsConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }


        public POSTResponseDto<int> CreateSalesTag(CreateSalesTagDto createSalesTagDto)
        {
            return Post<CreateSalesTagDto, int>(createSalesTagDto, SystemRoutes.SalesTag.Post.Route);
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.SalesTag.CreateGetById(id);
        }

        public POSTResponseDto<PagedResultDto<SalesTagDto>> SearchSalesTags(
            SalesTagSearchDto salesTagSearchDto)
        {
            return Post<SalesTagSearchDto, PagedResultDto<SalesTagDto>>(salesTagSearchDto,
                SystemRoutes.SalesTag.PostSearch.Route);
        }
    }
}
