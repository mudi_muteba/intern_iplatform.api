﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Sales;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Sales
{
    public class SalesConnector : BaseConnector
    {
        public SalesConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        public POSTResponseDto<int> CreateSalesFni(CreateSalesFNIDto createSalesFniDto)
        {
            return Post<CreateSalesFNIDto, int>(createSalesFniDto, SystemRoutes.Sales.CreateSalesFNI.Route);
        }


        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public POSTResponseDto<int> CreateSalesStructures(CreateSalesStructuresDto dto)
        {
            return Post<CreateSalesStructuresDto, int>(dto, SystemRoutes.Sales.SaveSalesStructure.Route);
        }

        public POSTResponseDto<bool> CreateSalesDetails(CreateSalesDetailsDto dto)
        {
            return Post<CreateSalesDetailsDto, bool>(dto, SystemRoutes.Sales.SaveSalesDetails.Route);
        }

        public GETResponseDto<SalesDetailDto> GetSalesDetailByLeadId(int leadId)
        {
            return Get<SalesDetailDto>(SystemRoutes.Sales.CreateGetSalesDetailByLeadId(leadId));
        }
    }
}
