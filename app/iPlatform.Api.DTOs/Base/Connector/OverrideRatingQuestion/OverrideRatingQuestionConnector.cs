﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.OverrideRatingQuestion
{
    public class OverrideRatingQuestionConnector : BaseConnector
    {
        private readonly int _Id;

        public OverrideRatingQuestionConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            _Id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.OverrideRatingQuestions.CreateGetById(id);
        }

        /// <summary>
        /// Get a OverrideRatingQuestion by it's unique identifier
        /// </summary>
        /// <returns></returns>
        public GETResponseDto<OverrideRatingQuestionDto> Get()
        {
            return Get<OverrideRatingQuestionDto>(_Id);
        }

        /// <summary>
        /// Updates a OverrideRatingQuestion by it's unique identifier
        /// </summary>
        /// <param name="editOverrideRatingQuestionDto"></param>
        /// <returns></returns>
        public PUTResponseDto<int> Update(EditOverrideRatingQuestionDto editOverrideRatingQuestionDto)
        {
            return Put<EditOverrideRatingQuestionDto, int>(editOverrideRatingQuestionDto, SystemRoutes.OverrideRatingQuestions.CreatePutById(_Id));
        }

        /// <summary>
        /// Deletes a OverrideRatingQuestion by it's unique identifier
        /// </summary>
        /// <param name="deleteOverrideRatingQuestionDto"></param>
        /// <returns></returns>
        public PUTResponseDto<int> Delete()
        {
            return Put<int>(SystemRoutes.OverrideRatingQuestions.CreateDeleteById(_Id));
        }
    }
}
