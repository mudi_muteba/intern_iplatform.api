﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Individual;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.OverrideRatingQuestion
{
    public class OverrideRatingQuestionsConnector : BaseConnector
    {
        public OverrideRatingQuestionsConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.OverrideRatingQuestions.CreateGetById(id);
        }

        /// <summary>
        /// Create a new OverrideRatingQuestion.
        /// </summary>
        /// <param name="createOverrideRatingQuestionDto"></param>
        /// <returns></returns>
        public POSTResponseDto<int> Create(CreateOverrideRatingQuestionDto createOverrideRatingQuestionDto)
        {
            return Post<CreateOverrideRatingQuestionDto, int>(createOverrideRatingQuestionDto, SystemRoutes.OverrideRatingQuestions.Post.Route);
        }

        /// <summary>
        /// Get all OverrideRatingQuestions that have not been deleted.
        /// </summary>
        /// <returns></returns>
        public GETResponseDto<ListResultDto<ListOverrideRatingQuestionDto>> GetAll()
        {
            return Get<ListResultDto<ListOverrideRatingQuestionDto>>(SystemRoutes.OverrideRatingQuestions.Get.Route);
        }

        /// <summary>
        /// Get all OverrideRatingQuestions with pagination that have not been deleted.
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public GETResponseDto<PagedResultDto<ListOverrideRatingQuestionDto>> GetAllWithPagination(int pageNumber, int pageSize)
        {
            string url = SystemRoutes.OverrideRatingQuestions.CreateGetWithPaginationUrl(pageNumber, pageSize);
            return Get<PagedResultDto<ListOverrideRatingQuestionDto>>(url);
        }

        /// <summary>
        /// Get all OverrideRatingQuestions with the passed search criteria(ChannelName, ProductName) that have not been deleted.
        /// </summary>
        /// <param name="searchOverrideRatingQuestionDto"></param>
        /// <returns></returns>
        public POSTResponseDto<PagedResultDto<ListOverrideRatingQuestionDto>> Search(SearchOverrideRatingQuestionDto searchOverrideRatingQuestionDto)
        {
            return Post<SearchOverrideRatingQuestionDto, PagedResultDto<ListOverrideRatingQuestionDto>>(searchOverrideRatingQuestionDto,
                SystemRoutes.OverrideRatingQuestions.Search.Route);
        }
    }
}
