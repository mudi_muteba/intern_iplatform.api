﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.OverrideRatingQuestion
{
    public class OverrideRatingQuestionManagement
    {
        private readonly IRestClient _Client;
        private readonly ApiToken _Token;

        public OverrideRatingQuestionsConnector OverrideRatingQuestions { get; private set; }

        public OverrideRatingQuestionManagement(IRestClient client, ApiToken token)
        {
            _Client = client;
            _Token = token;

            OverrideRatingQuestions = new OverrideRatingQuestionsConnector(_Client, _Token);
        }

        public OverrideRatingQuestionConnector OverrideRatingQuestion(int id)
        {
            return new OverrideRatingQuestionConnector(id, _Client, _Token);
        }
    }
}
