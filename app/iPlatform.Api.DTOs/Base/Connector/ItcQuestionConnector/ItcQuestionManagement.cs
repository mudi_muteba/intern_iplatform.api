﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.ItcQuestionConnector
{
    public class ItcQuestionManagement
    {
        private readonly IRestClient m_Client;
        private readonly ApiToken m_Token;

        public ItcQuestionManagement(IRestClient client, ApiToken token)
        {
            m_Client = client;
            m_Token = token;
            ItcQuestion = new ItcQuestionConnector(client, token);
        }

        public ItcQuestionConnector ItcQuestion { get; set; }

        public ItcQuestionConnector ItcQuestions()
        {
            return new ItcQuestionConnector(m_Client, m_Token);
        }
    }
}
