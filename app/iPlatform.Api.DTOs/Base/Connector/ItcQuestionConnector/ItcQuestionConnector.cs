﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.ItcQuestion;
using RestSharp;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace iPlatform.Api.DTOs.Base.Connector.ItcQuestionConnector
{
    public class ItcQuestionConnector : BaseConnector
    {
        public ItcQuestionConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {}

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ItcQuestions.CreateGetById(id);
        }

        public GETResponseDto<ItcQuestionDefinitionDto> GetByChannelIdUrl(int channelId)
        {
            var route = SystemRoutes.ItcQuestions.CreateGetByChannelId(channelId);
            return base.Get<ItcQuestionDefinitionDto>(route);
        }

        public GETResponseDto<ItcQuestionDefinitionDto> GetByChannelCodeUrl(string channelCode)
        {
            var route = SystemRoutes.ItcQuestions.CreateGetByChannelCode(channelCode);
            return base.Get<ItcQuestionDefinitionDto>(route);
        }

        public GETResponseDto<ItcQuestionDefinitionDto> Get(int id)
        {
            return base.Get<ItcQuestionDefinitionDto>(GetByIdUrl(id));
        }

        public PUTResponseDto<ItcQuestionDefinitionDto> Update(int id)
        {
            var route = SystemRoutes.ItcQuestions.CreateUpdate(id);
            return base.Put<ItcQuestionDefinitionDto>(route);
        }

        public POSTResponseDto<ItcQuestionDefinitionDto> Create(ItcQuestionDefinitionDto dto)
        {
            var route = SystemRoutes.ItcQuestions.CreatePost();
            return base.Post<ItcQuestionDefinitionDto, ItcQuestionDefinitionDto>(dto, route);
        }

        public DELETEResponseDto<ItcQuestionDefinitionDto> Delete(ItcQuestionDefinitionDto dto)
        {
            var route = SystemRoutes.ItcQuestions.CreateDelete(dto.Id);
            return base.Delete<ItcQuestionDefinitionDto, ItcQuestionDefinitionDto>(dto, route);
        }
    }
}
