﻿using iPlatform.Api.DTOs.Base.Connector.AdditionalMembers;
using iPlatform.Api.DTOs.Base.Connector.Admin;
using iPlatform.Api.DTOs.Base.Connector.Audit;
using iPlatform.Api.DTOs.Base.Connector.Authentication;
using iPlatform.Api.DTOs.Base.Connector.Campaigns;
using iPlatform.Api.DTOs.Base.Connector.Canaries;
using iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsItems;
using iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsTypes;
using iPlatform.Api.DTOs.Base.Connector.Commercials;
using iPlatform.Api.DTOs.Base.Connector.CoverLinksConnector;
using iPlatform.Api.DTOs.Base.Connector.Escalations;
using iPlatform.Api.DTOs.Base.Connector.FuneralDualCover;
using iPlatform.Api.DTOs.Base.Connector.FuneralMembers;
using iPlatform.Api.DTOs.Base.Connector.HistoryLoss.Home;
using iPlatform.Api.DTOs.Base.Connector.HistoryLoss.LossHistory;
using iPlatform.Api.DTOs.Base.Connector.HistoryLoss.Motor;
using iPlatform.Api.DTOs.Base.Connector.Imports;
using iPlatform.Api.DTOs.Base.Connector.Individuals;
using iPlatform.Api.DTOs.Base.Connector.Insurers;
using iPlatform.Api.DTOs.Base.Connector.JsonDataStores;
using iPlatform.Api.DTOs.Base.Connector.Leads;
using iPlatform.Api.DTOs.Base.Connector.Lookups;
using iPlatform.Api.DTOs.Base.Connector.Payment;
using iPlatform.Api.DTOs.Base.Connector.Policies.PolicyHeaders;
using iPlatform.Api.DTOs.Base.Connector.Products;
using iPlatform.Api.DTOs.Base.Connector.Proposals;
using iPlatform.Api.DTOs.Base.Connector.Questions;
using iPlatform.Api.DTOs.Base.Connector.Quotes;
using iPlatform.Api.DTOs.Base.Connector.Ratings;
using iPlatform.Api.DTOs.Base.Connector.Relationships;
using iPlatform.Api.DTOs.Base.Connector.Reports;
using iPlatform.Api.DTOs.Base.Connector.SecondLevel;
using iPlatform.Api.DTOs.Base.Connector.SettingsiRate;
using iPlatform.Api.DTOs.Base.Connector.SignFlow;
using iPlatform.Api.DTOs.Base.Connector.Statistics;
using iPlatform.Api.DTOs.Base.Connector.Teams;
using iPlatform.Api.DTOs.Base.Connector.Testing;
using iPlatform.Api.DTOs.Base.Connector.Users;
using Infrastructure.Configuration;
using RestSharp;
using iPlatform.Api.DTOs.Base.Connector.CustomApp;
using iPlatform.Api.DTOs.Base.Connector.Organizations;
using iPlatform.Api.DTOs.Base.Connector.SettingsiPerson;
using iPlatform.Api.DTOs.Base.Connector.SettingsQuoteUploads;
using iPlatform.Api.DTOs.Base.Connector.VehicleGuideSetting;
using iPlatform.Api.DTOs.Base.Connector.ChannelEvents;
using iPlatform.Api.DTOs.Base.Connector.DisplaySetting;
using iPlatform.Api.DTOs.Base.Connector.Lookups.Address.GetAddress;
using iPlatform.Api.DTOs.Base.Connector.QuoteBreakdown;
using iPlatform.Api.DTOs.Base.Connector.Components.DefinitionsConnector;
using iPlatform.Api.DTOs.Base.Connector.Components.HeadersConnector;
using iPlatform.Api.DTOs.Base.Connector.Components.QuestionAnswersConnector;
using iPlatform.Api.DTOs.Base.Connector.Components.QuestionDefinitionConnector;
using iPlatform.Api.DTOs.Base.Connector.OverrideRatingQuestion;
using iPlatform.Api.DTOs.Base.Connector.Admin.ChannelTemplates;
using iPlatform.Api.DTOs.Base.Connector.Admin.EmailCommunicationSettings;
using iPlatform.Api.DTOs.Base.Connector.Admin.SMSCommunicationSettings;
using iPlatform.Api.DTOs.Base.Connector.FormBuilder;
using iPlatform.Api.DTOs.Base.Connector.Logs;
using iPlatform.Api.DTOs.Base.Connector.RatingRuleHeaders;
using iPlatform.Api.DTOs.Base.Connector.RatingRuleHeaderCalculations;
using iPlatform.Api.DTOs.Base.Connector.ProductPricingStructure;
using iPlatform.Api.DTOs.Base.Connector.DataView;
using iPlatform.Api.DTOs.Base.Connector.PolicyBindings;
using iPlatform.Api.DTOs.Base.Connector.SettingsiRateUser;
using iPlatform.Api.DTOs.Base.Connector.ItcQuestionConnector;
using iPlatform.Api.DTOs.Base.Connector.Notifications;

namespace iPlatform.Api.DTOs.Base.Connector
{
    public class Connector : IConnector
    {
        private readonly IRestClient _client;

        public Connector(ApiToken token = null) : this(new RestClient(ConfigurationReader.Connector.BaseUrl), token) { }

        public Connector(IRestClient client, ApiToken token = null)
        {
            _client = client;
            Authentication = new AuthenticationConnector(client);
            UserManagement = new UserManagement(client, token);
            IndividualManagement = new IndividualManagement(client, token);
            CampaignManagement = new CampaignManagement(client, token);
            ProductManagement = new ProductManagement(client, token);
            Ratings = new RatingsConnector(client, token);
            Lookups = new LookupsManagement(client, token);
            Testing = new TestingConnector(client, token);
            ImportManagement = new ImportManagement(client, token);
            SecondLevel = new SecondLevelConnector(client, token);
            LeadManagement = new LeadManagement(client, token);
            PolicyHeaderManagement = new PolicyHeaderManagement(client, token);
            ClaimsHeaderManagement = new ClaimsHeaderManagement(client, token);
            ClaimsItemManagement = new ClaimsItemManagement(client, token);
            TeamManagement = new TeamManagement(client, token);
            FuneralMemberManagement = new FuneralMemberManagement(client, token);
            MotorLossHistoryManagement = new MotorLossHistoryManagement(client, token);
            HomeLossHistoryManagement = new HomeLossHistoryManagement(client, token);
            RelationshipManagement = new RelationshipManagement(client, token);
            InsurerManagement = new InsurerManagement(client, token);
            ChannelManagement = new ChannelManagement(client, token);
            QuestionManagement = new QuestionManagement(client, token);
            PaymentDetailsManagement = new PaymentDetailsManagement(client, token);
            ProposalManagement = new ProposalManagement(client, token);
            LeadConversionStatsManagement = new LeadConversionStatsManagement(client, token);
            QuotesManagement = new QuotesManagement(client, token);
            QuoteBreakdownManagement = new QuoteBreakdownManagement(client, token);
            FuneralDualCoverManagement = new FuneralDualCoverManagement(client, token);
            AuditManagement = new AuditManagement(client, token);
            ReportManagement = new ReportManagement(client, token);
            JsonDataStoresManagement = new JsonDataStoresManagement(client, token);
            ClaimsTypeManagement = new ClaimsTypeManagement(client, token);
            LossHistoryManagement = new LossHistoryManagement(client, token);
            AdditionalMemberManagement = new AdditionalMemberManagement(client, token);
            WorkflowRetryLogManagement = new WorkflowRetryLogManagement(client, token);
            CustomerSatisfactionSurveyManagement = new CustomerSatisfactionSurveyManagement(client, token);
            EscalationManagement = new EscalationManagement(client, token);
            CanaryManagement = new CanaryManagement(client, token);
            SignFlowManagement = new SignFlowManagement(client, token);
            SalesManagement = new SalesManagement(client, token);
            SettingsiRateManagement = new SettingsiRateManagement(client, token);
            CoverLinksManagement = new CoverLinksManagement(client, token);
            CustomAppManagement = new CustomAppManagement(client, token);
            OrganizationsManagement = new OrganizationsManagement(client, token);
            SettingsiPersonManagement = new SettingsiPersonManagement(client, token);
            DocumentManagement = new Documents.DocumentManagement(client, token);
            SettingsQuoteUploadManagement = new SettingsQuoteUploadManagement(client, token);
            VehicleGuideSetting = new VehicleGuideSettingManagement(client, token);
            ChannelEventManagement = new ChannelEventManagement(client, token);
            GetAddressManagement = new GetAddressManagement(client, token);
            DisplaySettingManagement = new DisplaySettingManagement(client, token);

            ComponentQuestionDefinitionManagement = new ComponentQuestionDefinitionManagement(client, token);

            OverrideRatingQuestionManagement = new OverrideRatingQuestionManagement(client, token);
            ComponentHeaderManagement = new ComponentHeaderManagement(client, token);
            ComponentDefinitionManagement = new ComponentDefinitionManagement(client, token);
            ComponentQuestionAnswerManagement = new ComponentQuestionAnswerManagement(client, token);
            ComponentQuestionDefinitionManagement = new ComponentQuestionDefinitionManagement(client, token);

            ChannelTemplateManagement = new ChannelTemplateManagement(client, token);
            EmailCommunicationSettingManagement = new EmailCommunicationSettingManagement(client, token);
            SMSCommunicationSettingManagement = new SMSCommunicationSettingManagement(client, token);
            RatingRuleHeaderManagement = new RatingRuleHeaderManagement(client, token);
            RatingRuleHeaderCalculationManagement = new RatingRuleHeaderCalculationManagement(client, token);
            FormBuilderManagement = new FormBuilderManagement(client, token);
            ProductPricingStructureManagement = new ProductPricingStructureManagement(client, token);
            LogFileManagement = new LogFileManagement(client, token);
            DataViewManagement = new DataViewManagement(client, token);
            PolicyBindingsManagement = new PolicyBindingsManagement(client, token);
            SettingsiRateUserManagement = new SettingsiRateUserManagement(client, token);
            ItcQuestionManagement = new ItcQuestionManagement(client, token);
            NotificationManagement = new NotificationManagement(client, token);
        }

        public TestingConnector Testing { get; private set; }

        public void SetToken(string token)
        {
            BaseConnector.ExistingToken = token;
        }

        public ImportManagement ImportManagement { get; private set; }
        public UserManagement UserManagement { get; private set; }
        public IndividualManagement IndividualManagement { get; private set; }
        public CampaignManagement CampaignManagement { get; private set; }
        public ProductManagement ProductManagement { get; private set; }
        public AuthenticationConnector Authentication { get; private set; }
        public RatingsConnector Ratings { get; private set; }
        public LookupsManagement Lookups { get; private set; }
        public SecondLevelConnector SecondLevel { get; private set; }
        public LeadManagement LeadManagement { get; private set; }
        public PolicyHeaderManagement PolicyHeaderManagement { get; private set; }
        public ClaimsHeaderManagement ClaimsHeaderManagement { get; private set; }
        public ClaimsItemManagement ClaimsItemManagement { get; private set; }
        public TeamManagement TeamManagement { get; private set; }
        public FuneralMemberManagement FuneralMemberManagement { get; private set; }
        public MotorLossHistoryManagement MotorLossHistoryManagement { get; private set; }
        public HomeLossHistoryManagement HomeLossHistoryManagement { get; private set; }
        public RelationshipManagement RelationshipManagement { get; private set; }
        public InsurerManagement InsurerManagement { get; private set; }
        public ChannelManagement ChannelManagement { get; private set; }
        public QuestionManagement QuestionManagement { get; private set; }
        public PaymentDetailsManagement PaymentDetailsManagement { get; private set; }
        public JsonDataStoresManagement JsonDataStoresManagement { get; private set; }
        public LossHistoryManagement LossHistoryManagement { get; set; }
        public LeadConversionStatsManagement LeadConversionStatsManagement { get; private set; }
        public ProposalManagement ProposalManagement { get; private set; }
        public QuotesManagement QuotesManagement { get; private set; }
        public QuoteBreakdownManagement QuoteBreakdownManagement { get; private set; }
        public FuneralDualCoverManagement FuneralDualCoverManagement { get; private set; }
        public AuditManagement AuditManagement { get; private set; }
        public ReportManagement ReportManagement { get; private set; }
        public ClaimsTypeManagement ClaimsTypeManagement { get; private set; }
        public AdditionalMemberManagement AdditionalMemberManagement { get; private set; }
        public WorkflowRetryLogManagement WorkflowRetryLogManagement { get; private set; }
        public CustomerSatisfactionSurveyManagement CustomerSatisfactionSurveyManagement { get; private set; }
        public EscalationManagement EscalationManagement { get; private set; }
        public CanaryManagement CanaryManagement { get; private set; }
        public SignFlowManagement SignFlowManagement { get; private set; }
        public SalesManagement SalesManagement { get; private set; }
        public SettingsiRateManagement SettingsiRateManagement { get; set; }
        public CoverLinksManagement CoverLinksManagement { get; set; }
        public CustomAppManagement CustomAppManagement { get; set; }
        public OrganizationsManagement OrganizationsManagement { get; set; }

        public SettingsiPersonManagement SettingsiPersonManagement { get; set; }

        public Documents.DocumentManagement DocumentManagement { get; set; }
        public VehicleGuideSettingManagement VehicleGuideSetting { get; set; }
        public GetAddressManagement GetAddressManagement { get; set; }


        public SettingsQuoteUploadManagement SettingsQuoteUploadManagement { get; set; }
        public ChannelEventManagement ChannelEventManagement { get; set; }
        public DisplaySettingManagement DisplaySettingManagement { get; set; }
        public OverrideRatingQuestionManagement OverrideRatingQuestionManagement { get; set; }

        public ComponentDefinitionManagement ComponentDefinitionManagement { get; set; }
        public ComponentHeaderManagement ComponentHeaderManagement { get; set; }
        public ComponentQuestionAnswerManagement ComponentQuestionAnswerManagement { get; set; }
        public ComponentQuestionDefinitionManagement ComponentQuestionDefinitionManagement { get; set; }

        public ChannelTemplateManagement ChannelTemplateManagement { get; set; }
        public EmailCommunicationSettingManagement EmailCommunicationSettingManagement { get; set; }
        public SMSCommunicationSettingManagement SMSCommunicationSettingManagement { get; set; }
        public RatingRuleHeaderManagement RatingRuleHeaderManagement { get; set; }
        public RatingRuleHeaderCalculationManagement RatingRuleHeaderCalculationManagement { get; set; }

        public FormBuilderManagement FormBuilderManagement { get; set; }
        public PolicyBindingsManagement PolicyBindingsManagement { get; set; }

        public ProductPricingStructureManagement ProductPricingStructureManagement { get; set; }
        public LogFileManagement LogFileManagement { get; set; }
        public DataViewManagement DataViewManagement { get; set; }
        public SettingsiRateUserManagement SettingsiRateUserManagement { get; set; }

        public ItcQuestionManagement ItcQuestionManagement { get; set; }
        public NotificationManagement NotificationManagement { get; set; }

        public Connector Reset(string token)
        {
            return new Connector(_client, new ApiToken(token));
        }
    }
}