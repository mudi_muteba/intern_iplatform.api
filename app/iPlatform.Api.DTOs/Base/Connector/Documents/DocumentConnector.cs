﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.DocumentManagement;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Documents
{
    public class DocumentConnector : BaseConnector
    {

        public DocumentConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        public POSTResponseDto<int> StoreDocument(CreateDocumentDto document)
        {
            return Post<CreateDocumentDto, int>(document, SystemRoutes.DocumentManagementRoutes.CreatePost());
        }

        public PUTResponseDto<int> DeleteDocument(DeleteDocumentDto document)
        {
            return Put<DeleteDocumentDto, int>(document, SystemRoutes.DocumentManagementRoutes.CreatePutDeleted());
        }

        public GETResponseDto<ListResultDto<DocumentDto>> GetDocumentsByCreatorId(int creatorId)
        {
            return
                base.Get<ListResultDto<DocumentDto>>(SystemRoutes.DocumentManagementRoutes.CreateGetByCeatorId(creatorId));
        }


        public GETResponseDto<ListResultDto<DocumentDto>> GetDocumentsByPartyId(int partyId)
        {
            return
                base.Get<ListResultDto<DocumentDto>>(SystemRoutes.DocumentManagementRoutes.CreateGetByPartyId(partyId));
        }

        public GETResponseDto<DocumentResultDto> GetDocumentById(int documentId)
        {
            return
                base.Get<DocumentResultDto>(SystemRoutes.DocumentManagementRoutes.CreateGetByDocumentId(documentId));
        }


        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }
    }
}
