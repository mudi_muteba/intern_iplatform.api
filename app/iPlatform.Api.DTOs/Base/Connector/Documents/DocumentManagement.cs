﻿using iPlatform.Api.DTOs.Base.Connector.SettingsiPerson;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Documents
{
    public class DocumentManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public DocumentManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            Documents = new DocumentConnector(client, token);

        }

        public DocumentConnector Documents { get; private set; }
       
    }
}