﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Quotes;
using RestSharp;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.Ratings.Response;


namespace iPlatform.Api.DTOs.Base.Connector.Quotes
{
    public class QuoteConnector : BaseConnector
    {
        public QuoteConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {

        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public POSTResponseDto<QuoteAcceptanceResponseDto> Accept(PublishQuoteUploadMessageDto quoteDto)
        {
            return Post<PublishQuoteUploadMessageDto, QuoteAcceptanceResponseDto>(quoteDto, SystemRoutes.Quotes.Accept.Route);
        }

        public POSTResponseDto<bool> ExternalAcceptQuote(RatingResultDto ratingResultDto)
        {
            return Post<RatingResultDto, bool>(ratingResultDto, SystemRoutes.Quotes.AcceptExternalQuote.Route);
        }

        public POSTResponseDto<bool> DeleteQuoteAccept(DeleteQuoteAcceptDto dto)
        {
            return Post<DeleteQuoteAcceptDto, bool>(dto, SystemRoutes.Quotes.CreateDeleteQuoteAccept(dto.Id.ToString()));
        }


        public POSTResponseDto<bool> DeletePolicyBindingCompleted(DeletePolicyBindingCompletedDto dto)
        {
            return Post<DeletePolicyBindingCompletedDto, bool>(dto, SystemRoutes.Quotes.CreateDeletePolicyBindingCompleted(dto.Id.ToString()));
        }

        public POSTResponseDto<int> Create(RatingResultDto ratingResultDto)
        {
            return Post<RatingResultDto, int>(ratingResultDto, SystemRoutes.Quotes.Create.Route);
        }

        public POSTResponseDto<QuoteAcceptanceResponseDto> Accept(int quoteId, string comments = "")
        {
            var dto = new AcceptQuoteDto { QuoteId = quoteId, Comments = comments };
            return Post<AcceptQuoteDto, QuoteAcceptanceResponseDto>(dto, SystemRoutes.Quotes.CreateAcceptQuote(quoteId.ToString()));
        }

        public POSTResponseDto<QuoteAcceptanceResponseDto> Accept(int quoteId, string comments, int brokerUserId, int accountExecutiveUserId, int channelId)
        {
            var dto = new AcceptQuoteDto { QuoteId = quoteId, Comments = comments, BrokerUserId = brokerUserId, AccountExecutiveUserId = accountExecutiveUserId, ChannelId = channelId};
            return Post<AcceptQuoteDto, QuoteAcceptanceResponseDto>(dto, SystemRoutes.Quotes.CreateAcceptQuote(quoteId.ToString()));
        }

        public POSTResponseDto<int> IntentToBuy(int quoteId)
        {
            IntentToBuyQuoteDto dto = new IntentToBuyQuoteDto() { Id = quoteId };
            return Post<IntentToBuyQuoteDto, int>(dto, SystemRoutes.Quotes.CreateIntentToBuyQuote(quoteId.ToString()));
        }

        public POSTResponseDto<int> LogQuoteUploadSuccess(CreateQuoteUploadLogDto dto)
        {
            return Post<CreateQuoteUploadLogDto, int>(dto, SystemRoutes.Quotes.CreateLogQuoteUploadSuccess(dto.QuoteId.ToString()));
        }

        public GETResponseDto<QuoteUploadLogDto> GetQuoteUploadLog(int quoteId)
        {
            return base.Get<QuoteUploadLogDto>(SystemRoutes.Quotes.CreateGetQuoteUploadLog(quoteId.ToString()));
        }
    }
}
