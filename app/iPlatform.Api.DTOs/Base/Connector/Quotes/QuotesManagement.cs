using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Quotes
{
    public class QuotesManagement
    {
        public QuotesManagement(IRestClient client, ApiToken token = null)
        {
            Quote = new QuoteConnector(client, token);
            QuoteItems = new QuoteItemsConnector(client, token);
        }

        public QuoteConnector Quote { get; private set; }

        public QuoteItemsConnector QuoteItems { get; private set; }

    }
}