﻿using RestSharp;
using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Quotes;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;

namespace iPlatform.Api.DTOs.Base.Connector.Quotes
{
    public class QuoteItemsConnector : BaseConnector
    {
        public QuoteItemsConnector(IRestClient client, ApiToken token) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public PUTResponseDto<QuoteDiscountResultDto> SaveQuoteItemDiscounts(int userId, ListEditQuoteItemDto quoteItems)
        {
            return Put<ListEditQuoteItemDto, QuoteDiscountResultDto>(quoteItems, SystemRoutes.QuoteItems.CreatePutByUserId(userId));
        }
    }
}
