using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Audit
{
    public class AuditManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public AuditManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            Audit = new AuditConnector(client, token);

        }

        public AuditConnector Audit { get; private set; }

    }
}