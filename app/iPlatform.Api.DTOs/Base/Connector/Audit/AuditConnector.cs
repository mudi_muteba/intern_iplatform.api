﻿using iPlatform.Api.DTOs.Audit;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Campaigns;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Audit
{
    public class AuditConnector : BaseConnector
    {

        public AuditConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Audit.CreateGetById(id);
        }

        public POSTResponseDto<int> Save(CreateAuditLogDto createAuditLogDto)
        {
            return Post<CreateAuditLogDto, int>(createAuditLogDto, SystemRoutes.Audit.Save.Route);
        }
    }
}