using System;
using System.Collections.Generic;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using RestSharp;
using Shared;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Base.Connector.Handlers
{
    internal class GETHandler<TResponse> : AbstractVERBHandler<GETResponseDto<TResponse>>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(GETHandler<TResponse>));

        public GETHandler(IRestClient client, string existingToken = "") : base(client, existingToken)
        {
        }

        public GETResponseDto<TResponse> Execute(string getUrl)
        {
            return ExecuteRequest(getUrl, Method.GET, null, OnError(getUrl));
        }

        private static Func<Exception, GETResponseDto<TResponse>> OnError(string getUrl)
        {
            Func<Exception, GETResponseDto<TResponse>> onError = (e) =>
            {
                var errorMessage = string.Format("Failed to execute {0}. The exception message is {1}",
                    getUrl, new ExceptionPrettyPrinter().Print(e));

                log.ErrorFormat(errorMessage);

                var getResponse = new GETResponseDto<TResponse>();
                var generalFailure = SystemErrorMessages.GeneralFailure(errorMessage);

                getResponse.ExecutionFailed(new List<ResponseErrorMessage>
                {
                    new ResponseErrorMessage(generalFailure.Message, generalFailure.MessageKey)
                });

                return getResponse;
            };
            return onError;
        }
    }
}