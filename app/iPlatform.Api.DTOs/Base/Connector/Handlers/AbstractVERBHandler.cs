using System;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Connector.Plumbing;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Newtonsoft.Json;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Handlers
{
    internal abstract class AbstractVERBHandler<TApiResponse> where TApiResponse : BaseResponseDto, new()
    {
        private readonly IRestClient _client;
        private readonly string _existingToken;
        private static readonly ILog Log = LogManager.GetLogger(typeof(AbstractVERBHandler<TApiResponse>));

        protected AbstractVERBHandler(IRestClient client, string existingToken)
        {
            _client = client;
            _existingToken = existingToken;
        }

        protected IRestResponse<TResponse> Execute<TResponse>(RestRequest request) where TResponse : BaseResponseDto, new()
        {
            AddHeaders(request, _existingToken);

            var restResponse = _client.Execute<TResponse>(request);
            if (restResponse != null)
            {
                restResponse.Data = restResponse.Data ?? new TResponse();
                restResponse.Data.StatusCode = restResponse.StatusCode;
            }
            return restResponse;
        }

        protected TApiResponse ExecuteRequest(string url, 
            Method method,
            object body,
            Func<Exception, TApiResponse> onError, bool authenticate = true)
        {
            var request = new RestRequest(url, method)
            {
                RequestFormat = DataFormat.Json
            };

            if (method == Method.POST || method == Method.PUT || method == Method.DELETE)
                request.AddBody(body);

            var apiResponse = Execute<TApiResponse>(request);

            var context = new CallContext(_client.BuildUri(request), method);
            var handler = new DefaultResponseHandler(context);

            Log.InfoFormat("Called {0}, the response was {1}", context.UrlInformation(), apiResponse.StatusCode);

            try
            {
                handler.Handle(apiResponse);
            }
            catch (Exception e)
            {
                return onError(e);
            }

            return apiResponse.Data;
        }

        protected TApiResponse ExecuteRequest(string url, 
            Method method,
            Func<Exception, TApiResponse> onError)
        {
            var request = new RestRequest(url, method)
            {
                RequestFormat = DataFormat.Json
            };

            var apiResponse = Execute<TApiResponse>(request);
            var context = new CallContext(_client.BuildUri(request), method);

            var handler = new DefaultResponseHandler(context);
            Log.InfoFormat("Called {0}, the response was {1}", context.UrlInformation(), apiResponse.StatusCode);

            try
            {
                handler.Handle(apiResponse);
            }
            catch (Exception e)
            {
                return onError(e);
            }

            return apiResponse.Data;
        }

        private static void AddHeaders(RestRequest request, string existingToken)
        {
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("Authorization", existingToken, ParameterType.HttpHeader);
        }
    }
}