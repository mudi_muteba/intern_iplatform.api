using System;
using System.Collections.Generic;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using RestSharp;
using Shared;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Base.Connector.Handlers
{
    internal class LISTPagedHandler<TResponse> : AbstractVERBHandler<LISTPagedResponseDto<TResponse>>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof (GETHandler<TResponse>));

        public LISTPagedHandler(IRestClient client, string existingToken = "") : base(client, existingToken)
        {
        }

        public LISTPagedResponseDto<TResponse> Execute(string url)
        {
            return ExecuteRequest(url, Method.GET, OnError(url));
        }

        public LISTPagedResponseDto<TResponse> Execute<TDto>(TDto dto, string url)
        {
            return ExecuteRequest(url, dto == null ? Method.GET : Method.POST, dto, OnError(dto != null ? dto.GetType().ToString() : url));
        }

        private static Func<Exception, LISTPagedResponseDto<TResponse>> OnError(string val)
        {
            Func<Exception, LISTPagedResponseDto<TResponse>> onError = (e) =>
            {
                var errorMessage = string.Format("Failed to execute {0}. The exception message is {1}",
                    val, new ExceptionPrettyPrinter().Print(e));

                log.ErrorFormat(errorMessage);

                var response = new LISTPagedResponseDto<TResponse>();
                var statusCode = response.StatusCode;
                var generalFailure = SystemErrorMessages.GeneralFailure(errorMessage);

                response.ExecutionFailed(new List<ResponseErrorMessage>
                {
                    new ResponseErrorMessage(generalFailure.Message, generalFailure.MessageKey)
                });
                response.StatusCode = statusCode;
                return response;
            };
            return onError;
        }
    }
}