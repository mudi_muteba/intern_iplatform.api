using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using RestSharp;
using Shared;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Base.Connector.Handlers
{
    internal class DELETEHandler<TResponse> : AbstractVERBHandler<DELETEResponseDto<TResponse>>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DELETEHandler<TResponse>));

        public DELETEHandler(IRestClient client, string existingToken = "")
            : base(client, existingToken)
        {
        }

        public DELETEResponseDto<TResponse> Execute<TDto>(TDto dto, string postUrl)
        {
            if (dto is IValidationAvailableDto)
            {
                var validationMessages = (dto as IValidationAvailableDto).Validate();

                if(validationMessages.Any())
                {
                    var response = new DELETEResponseDto<TResponse>();
                    response.ValidationFailed(validationMessages.Select(m => new ResponseErrorMessage(m.DefaultMessage, m.MessageKey)));
                    return response;
                }
            }

            return ExecuteRequest(postUrl, Method.DELETE, dto, OnError(dto));
        }

        private static Func<Exception, DELETEResponseDto<TResponse>> OnError<TDto>(TDto dto)
        {
            Func<Exception, DELETEResponseDto<TResponse>> onError = (e) =>
            {
                var errorMessage = string.Format("Failed to execute {0}. The exception message is {1}",
                    dto.GetType(), new ExceptionPrettyPrinter().Print(e));

                log.ErrorFormat(errorMessage);

                var response = new DELETEResponseDto<TResponse>();
                var generalFailure = SystemErrorMessages.GeneralFailure(errorMessage);

                response.ExecutionFailed(new List<ResponseErrorMessage>()
                {
                    new ResponseErrorMessage(generalFailure.Message, generalFailure.MessageKey)
                });

                return response;
            };
            return onError;
        }
    }
}