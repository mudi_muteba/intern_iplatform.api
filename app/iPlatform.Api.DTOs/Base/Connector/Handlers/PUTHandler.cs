using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using RestSharp;
using Shared;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Base.Connector.Handlers
{
    internal class PUTHandler<TResponse> : AbstractVERBHandler<PUTResponseDto<TResponse>>
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(PUTHandler<TResponse>));

        public PUTHandler(IRestClient client, string existingToken = "")
            : base(client, existingToken)
        {
        }

        public PUTResponseDto<TResponse> Execute(string putUrl)
        {
            return ExecuteRequest(putUrl, Method.PUT, OnError(putUrl));
        }

        public PUTResponseDto<TResponse> Execute<TDto>(TDto dto, string putUrl, bool authenticate = true)
        {
            if (dto is IValidationAvailableDto)
            {
                var validationMessages = (dto as IValidationAvailableDto).Validate();

                if (validationMessages.Any())
                {
                    var response = new PUTResponseDto<TResponse>();
                    response.ValidationFailed(
                        validationMessages.Select(m => new ResponseErrorMessage(m.DefaultMessage, m.MessageKey)));
                    return response;
                }
            }

            return ExecuteRequest(putUrl, Method.PUT, dto, OnError(dto.GetType().ToString()), authenticate);
        }

        private static Func<Exception, PUTResponseDto<TResponse>> OnError(string putUrl)
        {
            Func<Exception, PUTResponseDto<TResponse>> onError = (e) =>
            {
                var errorMessage = string.Format("Failed to execute {0}. The exception message is {1}",
                    putUrl,
                    new ExceptionPrettyPrinter().Print(e));

                Log.ErrorFormat(errorMessage);

                var response = new PUTResponseDto<TResponse>();
                var generalFailure = SystemErrorMessages.GeneralFailure(errorMessage);

                response.ExecutionFailed(new List<ResponseErrorMessage>()
                {
                    new ResponseErrorMessage(generalFailure.Message, generalFailure.MessageKey)
                });

                return response;
            };
            return onError;
        }
    }
}