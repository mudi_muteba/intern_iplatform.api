using System;
using System.Collections.Generic;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using RestSharp;
using Shared;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Base.Connector.Handlers
{
    internal class LISTHandler<TResponse> : AbstractVERBHandler<LISTResponseDto<TResponse>>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(GETHandler<TResponse>));

        public LISTHandler(IRestClient client, string existingToken = "") : base(client, existingToken)
        {
        }

        public LISTResponseDto<TResponse> Execute(string url)
        {
            return ExecuteRequest(url, Method.GET, OnError(url));
        }

        public LISTResponseDto<TResponse> Execute<TDto>(TDto dto, string url)
        {
            return ExecuteRequest(url, dto == null ? Method.GET : Method.POST, dto, OnError(dto != null ? dto.GetType().ToString() : url));
        }

        private static Func<Exception, LISTResponseDto<TResponse>> OnError(string val)
        {
            Func<Exception, LISTResponseDto<TResponse>> onError = (e) =>
            {
                var errorMessage = string.Format("Failed to execute {0}. The exception message is {1}",
                    val, new ExceptionPrettyPrinter().Print(e));

                log.ErrorFormat(errorMessage);

                var response = new LISTResponseDto<TResponse>();
                var statusCode = response.StatusCode;
                var generalFailure = SystemErrorMessages.GeneralFailure(errorMessage);

                response.ExecutionFailed(new List<ResponseErrorMessage>
                {
                    new ResponseErrorMessage(generalFailure.Message, generalFailure.MessageKey)
                });
                response.StatusCode = statusCode;
                return response;
            };
            return onError;
        }
    }
}