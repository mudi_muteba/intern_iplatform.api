﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using RestSharp;
using Shared;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Base.Connector.Handlers
{
    internal class POSTCustomAppHandler<TResponse> : AbstractVERBCustomAppHandler<CustomAppResponseDto<TResponse>>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(POSTHandler<TResponse>));

        public POSTCustomAppHandler(IRestClient client, string existingToken) : base(client, existingToken)
        {
        }

        public CustomAppResponseDto<TResponse> Execute<TDto>(TDto dto, string postUrl)
        {
            if (dto is IValidationAvailableDto)
            {
                var validationMessages = (dto as IValidationAvailableDto).Validate();

                if (validationMessages.Any())
                {
                    CustomAppResponseDto<TResponse> response = new CustomAppResponseDto<TResponse>();
                    response.LastErrorDescription = validationMessages.Select(vm => vm.DefaultMessage).ToString();
                    return response;
                }
            }

            Func<Exception, CustomAppResponseDto<TResponse>> onError = (e) =>
            {
                var errorMessage = String.Format("Failed to execute {0}. The exception message is {1}",
                    dto.GetType(), new ExceptionPrettyPrinter().Print(e));

                log.ErrorFormat(errorMessage);

                CustomAppResponseDto<TResponse> response = new CustomAppResponseDto<TResponse>();
                var generalFailure = SystemErrorMessages.GeneralFailure(errorMessage);
                response.LastErrorDescription = generalFailure.Message;
                return response;
            };

            return ExecuteRequest(postUrl, Method.POST, dto, onError);
        }
    }
}
