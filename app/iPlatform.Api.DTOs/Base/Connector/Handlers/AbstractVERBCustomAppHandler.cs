﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Connector.Plumbing;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Handlers
{
    internal abstract class AbstractVERBCustomAppHandler<TApiResponse> where TApiResponse : new()
    {
        private readonly IRestClient _client;
        private readonly string _existingToken;
        private static readonly ILog Log = LogManager.GetLogger(typeof(AbstractVERBCustomAppHandler<TApiResponse>));

        protected AbstractVERBCustomAppHandler(IRestClient client, string existingToken)
        {
            _client = client;
            _existingToken = existingToken;
        }

        protected IRestResponse<TResponse> Execute<TResponse>(RestRequest request)
            where TResponse : new()
        {
            AddHeaders(request, _existingToken);

            var restResponse = _client.Execute<TResponse>(request);
            return restResponse;
        }

        protected TApiResponse ExecuteRequest(string url,
            Method method,
            object body,
            Func<Exception,TApiResponse> onError, bool authenticate = true )
        {
            var request = new RestRequest(url, method)
            {
                RequestFormat = DataFormat.Json
            };

            if (method == Method.POST || method == Method.PUT)
                request.AddBody(body);

            var apiResponse = Execute<TApiResponse>(request);
            var context = new CallContext(_client.BuildUri(request), method);
            var handler = new DefaultResponseHandler(context);

            Log.InfoFormat("Called {0}, the response was {1}", context.UrlInformation(), apiResponse.StatusCode);

            try
            {
                handler.Handle(apiResponse);
            }
            catch (Exception e)
            {
                return onError(e);
            }

            return apiResponse.Data;
        }

        private static void AddHeaders(RestRequest request, string existingToken)
        {
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("Authorization", existingToken, ParameterType.HttpHeader);
        }
    }
}
