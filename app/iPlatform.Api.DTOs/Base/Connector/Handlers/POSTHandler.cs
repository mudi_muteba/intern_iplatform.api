using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using RestSharp;
using Shared;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Base.Connector.Handlers
{
    internal class POSTHandler<TResponse> : AbstractVERBHandler<POSTResponseDto<TResponse>>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(POSTHandler<TResponse>));

        public POSTHandler(IRestClient client, string existingToken = "") : base(client, existingToken)
        {
        }

        public POSTResponseDto<TResponse> Execute<TDto>(TDto dto, string postUrl)
        {
            if (dto is IValidationAvailableDto)
            {
                var validationMessages = (dto as IValidationAvailableDto).Validate();

                if(validationMessages.Any())
                {
                    var response = new POSTResponseDto<TResponse>();
                    response.ValidationFailed(validationMessages.Select(m => new ResponseErrorMessage(m.DefaultMessage, m.MessageKey)));
                    return response;
                }
            }

            Func<Exception, POSTResponseDto<TResponse>> onError = (e) =>
            {
                var errorMessage = string.Format("Failed to execute {0}. The exception message is {1}",
                    dto.GetType(), new ExceptionPrettyPrinter().Print(e));

                log.ErrorFormat(errorMessage);

                var response = new POSTResponseDto<TResponse>();
                var statusCode = response.StatusCode;
                var generalFailure = SystemErrorMessages.GeneralFailure(errorMessage);

                response.ExecutionFailed(new List<ResponseErrorMessage>()
                {
                    new ResponseErrorMessage(generalFailure.Message, generalFailure.MessageKey)
                });
                response.StatusCode = statusCode;
                return response;
            };

            return ExecuteRequest(postUrl, Method.POST, dto, onError);
        }
    }
}