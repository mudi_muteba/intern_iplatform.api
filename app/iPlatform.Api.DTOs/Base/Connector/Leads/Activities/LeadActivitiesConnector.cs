﻿using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Leads.Activities
{
    public class LeadActivitiesConnector : BaseConnector
    {
        public LeadActivitiesConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.LeadActivities.CreateGetById(id);
        }


        public POSTResponseDto<PagedResultDto<LeadActivityDto>> SearchLeadActivities(
            LeadActivitySearchDto leadActivitySearchDto)
        {
            return Post<LeadActivitySearchDto, PagedResultDto<LeadActivityDto>>(leadActivitySearchDto,
                SystemRoutes.LeadActivities.PostSearch.Route);
        }

        public POSTResponseDto<ListResultDto<LeadActivityDto>> SearchLeadActivitiesNoPagination(
            LeadActivitySearchDto leadActivitySearchDto)
        {
            return Post<LeadActivitySearchDto, ListResultDto<LeadActivityDto>>(leadActivitySearchDto,
                SystemRoutes.LeadActivities.PostSearchNoPagination.Route);
        }

        public POSTResponseDto<LeadActivityCountDto> GetLeadActivitiesCount(LeadActivitySearchDto LeadActivitySearchDto)
        {
            return Post<LeadActivitySearchDto, LeadActivityCountDto>(LeadActivitySearchDto, SystemRoutes.LeadActivities.GetCount.Route);
        }
    }
}