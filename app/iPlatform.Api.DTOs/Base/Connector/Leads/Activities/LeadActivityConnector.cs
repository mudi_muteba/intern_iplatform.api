﻿using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Leads.Activities
{
    public class LeadActivityConnector : BaseConnector
    {
        public LeadActivityConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.LeadActivities.CreateGetById(id);
        }

        public GETResponseDto<LeadActivityDto> GetById(int id)
        {
            return base.Get<LeadActivityDto>(GetByIdUrl(id));
        }

        public PUTResponseDto<int> DisableLeadActivity(DisableLeadActivityDto disableLeadActivityDto)
        {
            return Put<DisableLeadActivityDto, int>(disableLeadActivityDto, SystemRoutes.LeadActivities.CreateGetById(disableLeadActivityDto.Id));
        }
    }
}