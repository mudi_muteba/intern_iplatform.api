﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Campaign;
using iPlatform.Api.DTOs.Leads.Quality;
using iPlatform.Api.DTOs.Party.Contacts;
using iPlatform.Api.DTOs.SectionsPerChannel;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Leads
{
    public class LeadConnector : BaseConnector
    {
        public LeadConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Leads.CreateGetById(id);
        }

        protected string GetByChannelIdUrl(int id)
        {
            return SystemRoutes.Leads.GetActiveSectionsChannelId(id);
        }

        protected string GetByIdPartyUrl(int id)
        {
            return SystemRoutes.Leads.CreateGetByPartyId(id);
        }

        protected string GetByIdUrlDetailed(int id)
        {
            return SystemRoutes.Leads.CreateGetByIdDetailed(id);
        }

        protected string GetByIdUrlDetailedSimplified(int id)
        {
            return SystemRoutes.Leads.CreateGetByIdDetailedSimplified(id);
        }


        public GETResponseDto<LeadDto> Get(int leadId)
        {
            return base.Get<LeadDto>(GetByIdUrl(leadId));
        }


        public GETResponseDto<ResultChannelSectionDto> GetSections(int leadId)
        {
            return base.Get<ResultChannelSectionDto>(GetByChannelIdUrl(leadId));
        }

        public GETResponseDto<ListLeadAdvanceDto> GetDetailed(int leadId)
        {
            return base.Get<ListLeadAdvanceDto>(GetByIdUrlDetailed(leadId));
        }

        public POSTResponseDto<List<ContactDto>> GetRegOwnerContacts(RegOwnerContactDto dto)
        {
            return base.Post<RegOwnerContactDto, List<ContactDto>>(dto,SystemRoutes.Leads.GetRegOwnerIdContacts.Route);
        }

        public GETResponseDto<ListLeadSimplifiedDto> GetDetailedSimplified(int leadId)
        {
            return base.Get<ListLeadSimplifiedDto>(GetByIdUrlDetailedSimplified(leadId));
        }

        public GETResponseDto<LeadDto> GetByPartyId(int leadId)
        {
            return base.Get<LeadDto>(GetByIdPartyUrl(leadId));
        }

        public PUTResponseDto<int> DisableLeadActivity(EditLeadQualityDto editLeadQualityDto)
        {
            return Put<EditLeadQualityDto, int>(editLeadQualityDto, SystemRoutes.Leads.QualityById(editLeadQualityDto.Id));
        }

        public POSTResponseDto<int> SaveLead(CreateLeadDto createLeadDto)
        {
            return Post<CreateLeadDto, int>(createLeadDto, SystemRoutes.Leads.Post.Route);
        }

        public POSTResponseDto<LeadImportResponseDto> ImportLead(SubmitLeadDto submitLeadDto)
        {
            return Post<SubmitLeadDto, LeadImportResponseDto>(submitLeadDto, SystemRoutes.Leads.Import.Route);
        }

        public PUTResponseDto<int> DeadLeadActivity(DeadLeadActivityDto deadLeadActivityDto)
        {
            return Put<DeadLeadActivityDto, int>(deadLeadActivityDto, SystemRoutes.Leads.DeadById(deadLeadActivityDto.Id));
        }

        public PUTResponseDto<int> ImportedLeadActivity(ImportedLeadActivityDto importedLeadActivity)
        {
            return Put<ImportedLeadActivityDto, int>(importedLeadActivity, SystemRoutes.Leads.ImportedById(importedLeadActivity.Id));
        }

        public PUTResponseDto<int> LossLeadActivity(LossLeadActivityDto lossLeadActivityDto)
        {
            return Put<LossLeadActivityDto, int>(lossLeadActivityDto, SystemRoutes.Leads.LossById(lossLeadActivityDto.Id));
        }

        public PUTResponseDto<int> DelayLeadActivity(DelayLeadActivityDto delayLeadActivityDto)
        {
            return Put<DelayLeadActivityDto, int>(delayLeadActivityDto, SystemRoutes.Leads.DelayById(delayLeadActivityDto.Id));
        }


        public PUTResponseDto<bool> UpdateLeadStatus(UpdateLeadStatusDto updateLeadStatusDto)
        {
            return Put<UpdateLeadStatusDto, bool>(updateLeadStatusDto, SystemRoutes.Leads.UpdateLeadStatus(updateLeadStatusDto.Id));
        }

        public PUTResponseDto<int> RejectedAtUnderwriting(RejectedAtUnderwritingLeadActivityDto dto)
        {
            return Put<RejectedAtUnderwritingLeadActivityDto, int>(dto, SystemRoutes.Leads.RejectedAtUnderwritingById(dto.Id));
        }

        public PUTResponseDto<int> Sold(SoldLeadActivityDto dto)
        {
            return Put<SoldLeadActivityDto, int>(dto, SystemRoutes.Leads.SoldById(dto.Id));
        }

        public PUTResponseDto<int> PolicyBindingCreated(PolicyBindingCreatedLeadActivityDto dto)
        {
            return Put<PolicyBindingCreatedLeadActivityDto, int>(dto, SystemRoutes.Leads.PolicBindingCreatedById(dto.Id));
        }
    }
}