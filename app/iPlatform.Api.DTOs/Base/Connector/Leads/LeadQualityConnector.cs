﻿using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Leads.Quality;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Leads
{
    public class LeadQualityConnector : BaseConnector
    {
        public LeadQualityConnector(int leadId, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _leadId = leadId;
        }

        private readonly int _leadId;
        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Leads.CreateQualityGetById(id);
        }

        public GETResponseDto<LeadQualityDto> Get()
        {
            return base.Get<LeadQualityDto>(GetByIdUrl(_leadId));
        }


        public POSTResponseDto<int> SaveLead(CreateLeadDto createLeadDto)
        {
            return Post<CreateLeadDto, int>(createLeadDto, SystemRoutes.Leads.Post.Route);
        }

        public POSTResponseDto<int> SaveLeadQuality(CreateLeadQualityDto createLeadQualityDto)
        {
            return Post<CreateLeadQualityDto, int>(createLeadQualityDto, SystemRoutes.Leads.QualityById(_leadId));
        }

        public PUTResponseDto<int> EditLeadQuality(EditLeadQualityDto editLeadQualityDto)
        {
            return Put<EditLeadQualityDto, int>(editLeadQualityDto, SystemRoutes.Leads.QualityById(_leadId));
        }

    }
}