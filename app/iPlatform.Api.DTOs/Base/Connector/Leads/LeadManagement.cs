using iPlatform.Api.DTOs.Base.Connector.FuneralMembers;
using iPlatform.Api.DTOs.Base.Connector.Leads.Activities;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Leads
{
    public class LeadManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;
        public LeadManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            LeadActivities = new LeadActivitiesConnector(client, token);
            LeadActivity = new LeadActivityConnector(client, token);
            Lead = new LeadConnector(client, token);
        }

        public LeadActivitiesConnector LeadActivities { get; private set; }
        public LeadActivityConnector LeadActivity { get; private set; }

        public LeadConnector Lead { get; private set; }


        public LeadQualityConnector LeadQuality(int leadId)
        {
            return new LeadQualityConnector(leadId, client, token);
        }
    }
}