using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Individuals
{
    public class IndividualManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public IndividualManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            Individuals = new IndividualsConnector(client, token);

        }

        public IndividualsConnector Individuals { get; private set; }

        public IndividualConnector Individual(int id)
        {
            return new IndividualConnector(id, client, token);
        }
    }
}