﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Individuals.ProposalDefinitionQuestions
{
    public class ProposalDefinitionQuestionConnector : BaseConnector
    {
        private readonly int proposaldefinitionId;

        public ProposalDefinitionQuestionConnector(int proposaldefinitionId, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            this.proposaldefinitionId = proposaldefinitionId;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ProposalDefinitionQuestions.CreateGetById(id);
        }

        private string GetSaveByIdUrl(int id)
        {
            return SystemRoutes.ProposalDefinitionQuestions.CreateSaveQuestionAnswersById(id);
        }

        private string GetSearchByIdUrl(int id)
        {
            return SystemRoutes.ProposalDefinitionQuestions.CreateSearchById(id);
        }

        public GETResponseDto<ProposalDefinitionQuestionsDto> GetQuestionAnswer()
        {
            return base.Get<ProposalDefinitionQuestionsDto>(GetByIdUrl(proposaldefinitionId));
        }

        public POSTResponseDto<ProposalDefinitionQuestionsDto> GetByCriteria(ProposalDefinitionQuestionsCriteria criteria)
        {
            criteria.Id = proposaldefinitionId;
            return Post<ProposalDefinitionQuestionsCriteria, ProposalDefinitionQuestionsDto>(criteria, GetSearchByIdUrl(proposaldefinitionId));
        }

        public POSTResponseDto<bool> SaveQuestionAnswer(SaveProposalDefinitionAnswersDto dto)
        {
            dto.Id = proposaldefinitionId;
            return Post<SaveProposalDefinitionAnswersDto, bool>(dto, GetSaveByIdUrl(proposaldefinitionId));
        }

        public POSTResponseDto<bool> UpdateQuestionAnswer(UpdateProposalAnswerFromPolicyBindingDto dto)
        {
            return Post<UpdateProposalAnswerFromPolicyBindingDto, bool>(dto, SystemRoutes.ProposalDefinitionQuestions.CreateSaveAnswer(dto.ProposalDefinitionId));
        }
    }
}