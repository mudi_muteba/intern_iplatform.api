﻿using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base.Connector.Individuals.ProposalDefinitionQuestions;
using iPlatform.Api.DTOs.Base.Connector.Individuals.ProposalHeaders;
using iPlatform.Api.DTOs.Base.Connector.Individuals.SalesForce;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using iPlatform.Api.DTOs.Party.Bank;
using iPlatform.Api.DTOs.Party.Contacts;
using iPlatform.Api.DTOs.Party.Correspondence;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.Note;
using iPlatform.Api.DTOs.Party.Payments;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Individuals
{
    public class IndividualConnector : BaseConnector
    {
        private readonly int _individualId;

        public IndividualConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _individualId = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Individuals.CreateGetById(id);
        }

        protected string AddressGetByIdUrl(int individualId, int id)
        {
            return SystemRoutes.IndividualAddresses.CreateGetById(individualId, id);
        }

        protected string BankDetailGetByIdUrl(int individualId, int id)
        {
            return SystemRoutes.IndividualBankDetails.CreateGetById(individualId, id);
        }


        protected string AddressDeleteByIdUrl(int individualId, int id)
        {
            return SystemRoutes.IndividualAddresses.CreateGetById(individualId, id);
        }

        protected string BankDetailsDeleteByIdUrl(int individualId,int id)
        {
            return SystemRoutes.IndividualBankDetails.CreateGetById(individualId, id);
        }

        protected string CorrespondencePreferenceGetByIdUrl(int individualId, int id)
        {
            return SystemRoutes.CorrespondencePreferences.CreateGetById(individualId, id);
        }

        protected string CorrespondencePreferenceDeleteByIdUrl(int individualId, int id)
        {
            return SystemRoutes.CorrespondencePreferences.CreateGetById(individualId, id);
        }

        protected string PaymentDetailGetByIdUrl(int individualId, int id)
        {
            return SystemRoutes.PaymentDetails.CreateGetById(individualId, id);
        }

        public GETResponseDto<IndividualDto> Get()
        {
            return base.Get<IndividualDto>(GetByIdUrl(_individualId));
        }

        public GETResponseDto<AddressDto> GetAddress(int id)
        {
            return base.Get<AddressDto>(AddressGetByIdUrl(_individualId, id));
        }


        public LISTResponseDto<ListAddressDto> GetAddresses()
        {
            return base.GetList<ListAddressDto>(SystemRoutes.IndividualAddresses.CreateGetById(_individualId));
        }


        public POSTResponseDto<int> CreateAddress(CreateAddressDto createAddressDto)
        {
            return Post<CreateAddressDto, int>(createAddressDto,
                SystemRoutes.IndividualAddresses.CreateGetById(_individualId));
        }

        public PUTResponseDto<int> EditAddress(EditAddressDto updateAddressDto)
        {
            return Put<EditAddressDto, int>(updateAddressDto,
                SystemRoutes.IndividualAddresses.CreateGetById(_individualId, updateAddressDto.Id));
        }


        public DELETEResponseDto<int> DeleteAddress(int id)
        {
            var deleteDto = new DeleteAddressDto
            {
                Id = id,
                PartyId = _individualId
            };

            return Delete<DeleteAddressDto, int>(deleteDto, AddressDeleteByIdUrl(_individualId, id));
        }

        #region Bank Details
        public POSTResponseDto<int> CreateBankDetails(CreateBankDetailsDto createBankDetailsDto)
        {
            return Post<CreateBankDetailsDto, int>(createBankDetailsDto,
                SystemRoutes.IndividualBankDetails.CreateGetById(_individualId));
        }

        public POSTResponseDto<bool> ValidateBankDetails(BankDetailValidationDto bankDetailvalidationDto)
        {
            return Post<BankDetailValidationDto, bool>(bankDetailvalidationDto,
                SystemRoutes.Bank.ValidateBankDetails.Route);
        }

        public POSTResponseDto<bool> ValidateBankDetailsLegacy(BankDetailValidationLegacyDto bankDetailValidationLegacyDto)
        {
            return Post<BankDetailValidationLegacyDto, bool>(bankDetailValidationLegacyDto,
                SystemRoutes.Bank.ValidateBankDetailsLegacy.Route);
        }

        public DELETEResponseDto<int> DeleteBankDetails(int id)
        {
            var deleteDto = new DeleteBankDetailsDto
            {
                Id = id,
                PartyId = _individualId
            };

            return Delete<DeleteBankDetailsDto, int>(deleteDto, BankDetailsDeleteByIdUrl(_individualId, id));
        }


        public LISTResponseDto<ListBankDetailsDto> GetBankDetails()
        {
            return base.GetList<ListBankDetailsDto>(SystemRoutes.IndividualBankDetails.CreateGetById(_individualId));
        }

        public PUTResponseDto<int> EditBankDetails(EditBankDetailsDto updateBankDetailsDto)
        {
            return Put<EditBankDetailsDto, int>(updateBankDetailsDto,
                SystemRoutes.IndividualBankDetails.CreateGetById(_individualId, updateBankDetailsDto.Id));
        }


        public GETResponseDto<BankDetailsDto> GetBankDetail(int id)
        {
            return base.Get<BankDetailsDto>(BankDetailGetByIdUrl(_individualId, id));
        }

        #endregion

        #region Correspondence
        public POSTResponseDto<int> CreateCorrespondence(CreatePartyCorrespondenceDto createPartyCorrespondenceDto)
        {
            return Post<CreatePartyCorrespondenceDto, int>(createPartyCorrespondenceDto,
                SystemRoutes.IndividualCorrespondence.CreateGetById(_individualId));
        }

        public LISTResponseDto<PartyCorrespondenceDto> GetCorrespondence()
        {
            return base.GetList<PartyCorrespondenceDto>(SystemRoutes.IndividualCorrespondence.CreateGetById(_individualId));
        }
        #endregion

        #region Notes

        public POSTResponseDto<int> CreateNote(CreateNoteDto createNoteDto)
        {
            return Post<CreateNoteDto, int>(createNoteDto, SystemRoutes.IndividualNotes.CreateGetById(_individualId));
        }

        protected string NoteGetByIdUrl(int id, int individualId)
        {
            return SystemRoutes.IndividualNotes.CreateGetById(id, individualId);
        }

        protected string NoteDeleteByIdUrl(int id, int individualId)
        {
            return SystemRoutes.IndividualNotes.CreateGetById(id, individualId);
        }

        public GETResponseDto<NoteDto> GetNote(int id)
        {
            return base.Get<NoteDto>(NoteGetByIdUrl(_individualId, id));
        }

        public DELETEResponseDto<int> DeleteNote(int id)
        {
            var deleteDto = new DeleteNoteDto
            {
                Id = id,
                PartyId = _individualId
            };

            return Delete<DeleteNoteDto, int>(deleteDto, NoteDeleteByIdUrl(_individualId, id));
        }

        public LISTResponseDto<ListNoteDto> GetNotes()
        {
            return base.GetList<ListNoteDto>(SystemRoutes.IndividualNotes.CreateGetById(_individualId));
        }

        #endregion

        #region ProposalHeaders

        public SalesForceConnector SalesForce()
        {
            return new SalesForceConnector(_individualId, Client);
        }
        public ProposalHeaderConnector Proposal(int proposalHeaderId)
        {
            return new ProposalHeaderConnector(_individualId, proposalHeaderId, Client);
        }

        public ProposalHeadersConnector Proposals()
        {
            return new ProposalHeadersConnector(_individualId, Client);
        }

        #endregion

        #region Proposal Definition Question

        public ProposalDefinitionQuestionConnector ProposalDefinitionQuestion(int proposalDefinitionId)
        {
            return new ProposalDefinitionQuestionConnector(proposalDefinitionId, Client);
        }
        #endregion

        #region Assets Vehicle

        public POSTResponseDto<int> CreateAssetVehicle(CreateAssetVehicleDto createAssetVehicleDto)
        {
            return Post<CreateAssetVehicleDto, int>(createAssetVehicleDto,
                SystemRoutes.IndividualAssetVehicle.CreateGetById(_individualId));
        }

        public DELETEResponseDto<int> DeleteAssetVehicle(int id)
        {
            var deleteDto = new DeleteAssetVehicleDto
            {
                Id = id,
                PartyId = _individualId
            };

            string url = SystemRoutes.IndividualAssetVehicle.CreateGetById(_individualId, id);
            return Delete<DeleteAssetVehicleDto, int>(deleteDto, url);
        }


        public LISTResponseDto<ListAssetVehicleDto> GetAssetVehicles()
        {
            return base.GetList<ListAssetVehicleDto>(SystemRoutes.IndividualAssetVehicle.CreateGetById(_individualId));
        }

        public GETResponseDto<AssetVehicleDto> GetAssetVehicle(int id)
        {
            string url = SystemRoutes.IndividualAssetVehicle.CreateGetById(_individualId, id);
            return base.Get<AssetVehicleDto>(url);
        }

        public PUTResponseDto<int> EditAssetVehicle(EditAssetVehicleDto updateAssetVehicleDto)
        {
            return Put<EditAssetVehicleDto, int>(updateAssetVehicleDto,
                SystemRoutes.IndividualAssetVehicle.CreateGetById(_individualId, updateAssetVehicleDto.Id));
        }

        #endregion

        #region Assets RiskItem

        public POSTResponseDto<int> CreateAssetRiskItem(CreateAssetRiskItemDto createAssetRiskItemDto)
        {
            return Post<CreateAssetRiskItemDto, int>(createAssetRiskItemDto,
                SystemRoutes.IndividualAssetRiskItem.CreateGetById(_individualId));
        }

        public DELETEResponseDto<int> DeleteAssetRiskItem(int id)
        {
            var deleteDto = new DeleteAssetRiskItemDto
            {
                Id = id,
                PartyId = _individualId
            };

            string url = SystemRoutes.IndividualAssetRiskItem.CreateGetById(_individualId, id);
            return Delete<DeleteAssetRiskItemDto, int>(deleteDto, url);
        }


        public LISTResponseDto<ListAssetRiskItemDto> GetAssetRiskItems()
        {
            return base.GetList<ListAssetRiskItemDto>(SystemRoutes.IndividualAssetRiskItem.CreateGetById(_individualId));
        }

        public GETResponseDto<AssetRiskItemDto> GetAssetRiskItem(int id)
        {
            string url = SystemRoutes.IndividualAssetRiskItem.CreateGetById(_individualId, id);
            return base.Get<AssetRiskItemDto>(url);
        }

        public PUTResponseDto<int> EditAssetRiskItem(EditAssetRiskItemDto updateAssetRiskItemDto)
        {
            return Put<EditAssetRiskItemDto, int>(updateAssetRiskItemDto,
                SystemRoutes.IndividualAssetRiskItem.CreateGetById(_individualId, updateAssetRiskItemDto.Id));
        }

        #endregion

        #region Contacts

        public POSTResponseDto<int> CreateContact(CreateContactDto newContact)
        {
            return Post<CreateContactDto, int>(newContact, SystemRoutes.IndividualContacts.CreateGetById(_individualId));
        }
        public GETResponseDto<ContactDto> GetContact(int id)
        {
            return base.Get<ContactDto>(SystemRoutes.IndividualContacts.CreateGetById(_individualId, id));
        }

        public LISTResponseDto<ListContactDto> GetContacts()
        {
            return base.GetList<ListContactDto>(SystemRoutes.IndividualContacts.CreateGetById(_individualId));
        }

        public PUTResponseDto<int> EditContact(EditContactDto updateContactDto)
        {
            return Put<EditContactDto, int>(updateContactDto,
                SystemRoutes.IndividualContacts.CreateGetById(_individualId, updateContactDto.Id));
        }

        public DELETEResponseDto<int> DeleteContact(int id)
        {
            var deleteDto = new DeleteContactDto
            {
                Id = id,
            };

            return Delete<DeleteContactDto, int>(deleteDto,
                SystemRoutes.IndividualContacts.CreateGetById(_individualId, id));
        }
        #endregion

        #region Correspondence Preference
        public POSTResponseDto<int> EditCorrespondencePreference(EditCorrespondencePreferenceDto createCorrespondencePrefDto)
        {
            return Post<EditCorrespondencePreferenceDto, int>(createCorrespondencePrefDto,
                SystemRoutes.CorrespondencePreferences.CreateGetById(_individualId));
        }

        public LISTResponseDto<PartyCorrespondencePreferenceDto> GetCorrespondencePreferences()
        {
            return base.GetList<PartyCorrespondencePreferenceDto>(SystemRoutes.CorrespondencePreferences.CreateGetById(_individualId));
        }

        public GETResponseDto<PartyCorrespondencePreferenceDto> GetCorrespondencePreference(int id)
        {
            return base.Get<PartyCorrespondencePreferenceDto>(CorrespondencePreferenceGetByIdUrl(_individualId, id));
        }
        #endregion

        #region Payment Details

        public GETResponseDto<PaymentDetailsDto> GetPaymentDetailById(int id)
        {
            return base.Get<PaymentDetailsDto>(PaymentDetailGetByIdUrl(_individualId, id));
        }

        public POSTResponseDto<int> CreatePaymentDetail(CreatePaymentDetailsDto createPaymentDetailsDto)
        {
            return Post<CreatePaymentDetailsDto, int>(createPaymentDetailsDto,
                SystemRoutes.PaymentDetails.CreateGetById(_individualId));
        }

        public PUTResponseDto<int> EditPaymentDetail(EditPaymentDetailsDto editPaymentDetailsDto)
        {
            return Put<EditPaymentDetailsDto, int>(editPaymentDetailsDto,
                SystemRoutes.PaymentDetails.CreateGetById(_individualId, editPaymentDetailsDto.Id));
        }

        public LISTResponseDto<PaymentDetailsDto> GetPaymentDetails()
        {
            return base.GetList<PaymentDetailsDto>(SystemRoutes.PaymentDetails.CreateGetById(_individualId));
        }

        #endregion

        public PUTResponseDto<int> EditIndividual(EditIndividualDto individualDto)
        {
            return Put<EditIndividualDto, int>(individualDto,
                SystemRoutes.Individuals.CreateGetById(_individualId));
        }
    }
}