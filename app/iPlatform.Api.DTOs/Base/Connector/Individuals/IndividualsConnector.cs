﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Individual;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Individuals
{
    public class IndividualsConnector : BaseConnector
    {
        public IndividualsConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Individuals.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateIndividual(CreateIndividualDto createIndividualDto)
        {
            return Post<CreateIndividualDto, int>(createIndividualDto, SystemRoutes.Individuals.Post.Route);
        }

        public POSTResponseDto<PagedResultDto<ListIndividualDto>> SearchIndividual(
            IndividualSearchDto individualSearchDto)
        {
            return Post<IndividualSearchDto, PagedResultDto<ListIndividualDto>>(individualSearchDto,
                SystemRoutes.Individuals.PostSearch.Route);
        }
    }
}