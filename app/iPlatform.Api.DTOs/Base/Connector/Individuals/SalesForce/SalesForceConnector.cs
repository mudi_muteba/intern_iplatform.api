﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.SalesForce;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Individuals.SalesForce
{
    public class SalesForceConnector : BaseConnector
    {
        private readonly int _individualId;

        public SalesForceConnector(int individualId, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _individualId = individualId;
        }

        protected string PostByIdUrl(int id)
        {
            return SystemRoutes.SalesForce.CreateGetById(id);
        }

        public POSTResponseDto<int> Post()
        {
            var dto = new SalesForceLeadStatusDto
            {
                PartyId = _individualId
            };

            return Post<SalesForceLeadStatusDto, int>(dto, SystemRoutes.SalesForce.PostByPartyId(_individualId));
        }
        public POSTResponseDto<int> Post(SalesForceLeadStatusDto dto)
        {
            dto.PartyId = _individualId;
            return Post<SalesForceLeadStatusDto, int>(dto, SystemRoutes.SalesForce.PostByPartyId(_individualId));
        }

        public GETResponseDto<SalesForceEntryDto> GetSfEntryByPartyId(int id)
        {
            return Get<SalesForceEntryDto>(SystemRoutes.SalesForce.CreateGetById(id));
        }

        public PUTResponseDto<int> UpdateSfEntry(EditSalesForceEntryDto dto)
        {
            return Put<EditSalesForceEntryDto,int>(dto, SystemRoutes.SalesForce.CreateUpdateById(dto.Id));
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }
    }
}