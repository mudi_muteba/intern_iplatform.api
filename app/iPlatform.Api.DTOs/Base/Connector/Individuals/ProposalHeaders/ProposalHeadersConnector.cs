﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Individuals.ProposalHeaders
{
    public class ProposalHeadersConnector : BaseConnector
    {
        private readonly int _individualId;

        public ProposalHeadersConnector(int individualId, IRestClient client, ApiToken token = null)
            : base(client, token)
        {

            _individualId = individualId;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Individuals.CreateGetById(id);
        }
        public POSTResponseDto<int> CreateProposalHeader(CreateProposalHeaderDto createProposalHeaderDto)
        {
            return Post<CreateProposalHeaderDto, int>(createProposalHeaderDto, SystemRoutes.IndividualProposalHeaders.CreateGetById(_individualId));
        }

        public LISTResponseDto<ListProposalHeaderDto> Get()
        {
            return base.GetList<ListProposalHeaderDto>(SystemRoutes.IndividualProposalHeaders.CreateGetById(_individualId));
        }
    }
}