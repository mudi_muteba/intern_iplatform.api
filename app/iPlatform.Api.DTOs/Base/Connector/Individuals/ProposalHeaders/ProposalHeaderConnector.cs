﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Individuals.ProposalHeaders
{
    public class ProposalHeaderConnector : BaseConnector
    {
        private readonly int _individualId;
        private readonly int _proposalHeaderId;

        public ProposalHeaderConnector(int individualId, int proposalHeaderId, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _proposalHeaderId = proposalHeaderId;
            _individualId = individualId;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.IndividualProposalHeaders.CreateGetById(id);
        }

        #region ProposalHeaders

        private string ProposalHeaderGetByIdUrl(int individualId, int proposalHeaderId)
        {
            return SystemRoutes.IndividualProposalHeaders.CreateGetById(individualId, proposalHeaderId);
        }

        private string ProposalHeaderDeleteByIdUrl(int individualId, int proposalHeaderId)
        {
            return SystemRoutes.IndividualProposalHeaders.CreateGetById(individualId, proposalHeaderId);
        }

        private string ProposalHeaderGetDetailedByIdUrl(int individualId, int proposalHeaderId)
        {
            return SystemRoutes.IndividualProposalHeaders.CreateGetDetailedById(individualId, proposalHeaderId);
        }

        public GETResponseDto<ProposalHeaderDto> Get()
        {
            return base.Get<ProposalHeaderDto>(ProposalHeaderGetByIdUrl(_individualId, _proposalHeaderId));
        }

        public GETResponseDto<ProposalHeaderDetailedDto> GetDetailed()
        {
            return base.Get<ProposalHeaderDetailedDto>(ProposalHeaderGetDetailedByIdUrl(_individualId, _proposalHeaderId));
        }


        public DELETEResponseDto<int> Delete()
        {
            var deleteDto = new DeleteProposalHeaderDto
            {
                Id = _proposalHeaderId,
                PartyId = _individualId
            };

            return Delete<DeleteProposalHeaderDto, int>(deleteDto,
                ProposalHeaderDeleteByIdUrl(_individualId, _proposalHeaderId));
        }

        public POSTResponseDto<int> GetVehicleTypes(ContainsVehicleTypesDto dto)
        {
            var url  = SystemRoutes.IndividualProposalHeaders.GetContainsVehilceTypeById(dto.IndividualId, dto.ProposalId);
            return Post<ContainsVehicleTypesDto, int>(dto, url);
        }
        #endregion

        #region ProposalDefinition

        public POSTResponseDto<int> CreateProposalDefinition(CreateProposalDefinitionDto newProposalDefinition)
        {
            return Post<CreateProposalDefinitionDto, int>(newProposalDefinition,
                SystemRoutes.ProposalDefinitions.CreateGetById(_proposalHeaderId));
        }


        public DELETEResponseDto<DeleteProposalDefinitionResponseDto> DeleteProposalDefinition(int proposalDefinitionId)
        {
            var deleteDto = new DeleteProposalDefinitionDto
            {
                Id = proposalDefinitionId,
                ProposalHeaderId = _proposalHeaderId
            };

            string link = SystemRoutes.ProposalDefinitions.CreateGetById(_proposalHeaderId, proposalDefinitionId);
            return Delete<DeleteProposalDefinitionDto, DeleteProposalDefinitionResponseDto>(deleteDto, link);
        }


        public LISTResponseDto<ListProposalDefinitionDto> GetProposalDefinitions()
        {
            return
                base.GetList<ListProposalDefinitionDto>(
                    SystemRoutes.ProposalDefinitions.CreateGetById(_proposalHeaderId));
        }


        public GETResponseDto<ProposalDefinitionDto> GetProposalDefinition(int id)
        {
            return
                base.Get<ProposalDefinitionDto>(SystemRoutes.ProposalDefinitions.CreateGetById(_proposalHeaderId,
                    id));
        }

        #endregion

        public PUTResponseDto<ProposalDefinitionDto> UpdateProposalDefinition(EditProposalDefinitionDto editProposalDefinition)
        {
            return
                base.Put<EditProposalDefinitionDto, ProposalDefinitionDto>(editProposalDefinition, SystemRoutes.ProposalDefinitions.CreateGetById(_proposalHeaderId,
                    editProposalDefinition.Id));
        }
        
    }
}