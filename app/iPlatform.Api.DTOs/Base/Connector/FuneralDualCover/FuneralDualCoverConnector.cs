﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.FuneralMembers;
using RestSharp;
using iPlatform.Api.DTOs.FuneralDualCover;

namespace iPlatform.Api.DTOs.Base.Connector.FuneralDualCover
{
    public class FuneralDualCoverConnector : BaseConnector
    {

        public FuneralDualCoverConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {

        }

        protected override string GetByIdUrl(int Id)
        {
            throw new NotImplementedException();
        }

        protected string GetByIdUrl(string idNumber, decimal sumInsured)
        {
            return SystemRoutes.FuneralDualCover.CreateGet(idNumber, sumInsured);
        }

        public GETResponseDto<FuneralDualCoverDto> Get(string idNumber, decimal sumInsured)
        {
            return base.Get<FuneralDualCoverDto>(GetByIdUrl(idNumber, sumInsured));
        }

    }
}