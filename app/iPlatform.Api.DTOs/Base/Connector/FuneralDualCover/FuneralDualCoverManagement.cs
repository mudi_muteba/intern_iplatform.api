using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.FuneralDualCover
{
    public class FuneralDualCoverManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public FuneralDualCoverManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            this.FuneralDualCover = new FuneralDualCoverConnector(client, token);

        }

        public FuneralDualCoverConnector FuneralDualCover { get; private set; }
    }
}