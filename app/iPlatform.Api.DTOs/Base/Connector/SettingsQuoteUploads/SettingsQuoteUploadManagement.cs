﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SettingsQuoteUploads
{
    public class SettingsQuoteUploadManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public SettingsQuoteUploadManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            SettingsQuoteUploads = new SettingsQuoteUploadsConnector(client, token);

        }

        public SettingsQuoteUploadsConnector SettingsQuoteUploads { get; private set; }

        public SettingsQuoteUploadConnector SettingsQuoteUpload(int id)
        {
            return new SettingsQuoteUploadConnector(id, client, token);
        }
    }
}