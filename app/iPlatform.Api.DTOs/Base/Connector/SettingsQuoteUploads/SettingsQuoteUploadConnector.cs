﻿using iPlatform.Api.DTOs.Admin.ChannelPermissions;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SettingsQuoteUploads
{
    public class SettingsQuoteUploadConnector : BaseConnector
    {
        private readonly int id;

        public SettingsQuoteUploadConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            this.id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.SettingsQuoteUploads.CreateGetById(id);
        }

        public GETResponseDto<SettingsQuoteUploadDto> Get()
        {
            return base.Get<SettingsQuoteUploadDto>(id);
        }

        public PUTResponseDto<int> Edit(EditSettingsQuoteUploadDto dto)
        {
            return Put<EditSettingsQuoteUploadDto, int>(dto, SystemRoutes.SettingsQuoteUploads.CreatePutById(dto.Id));
        }

        public PUTResponseDto<int> Delete()
        {
            return Put<int>(SystemRoutes.SettingsQuoteUploads.CreateDeleteById(id));
        }
    }
}