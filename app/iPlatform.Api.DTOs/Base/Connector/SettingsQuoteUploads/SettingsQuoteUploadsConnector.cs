﻿using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;
using System;

namespace iPlatform.Api.DTOs.Base.Connector.SettingsQuoteUploads
{
    public class SettingsQuoteUploadsConnector : BaseConnector
    {
        public SettingsQuoteUploadsConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.SettingsQuoteUploads.CreateGetById(id);
        }

        public POSTResponseDto<int> Create(CreateSettingsQuoteUploadDto dto)
        {
            return Post<CreateSettingsQuoteUploadDto, int>(dto, SystemRoutes.SettingsQuoteUploads.Post.Route);
        }

        public POSTResponseDto<bool> SaveMultiple(SaveSettingsQuoteUploadsDto dto)
        {
            return Post<SaveSettingsQuoteUploadsDto, bool>(dto, SystemRoutes.SettingsQuoteUploads.SaveMultiple.Route);
        }


        public GETResponseDto<ListResultDto<SettingsQuoteUploadDto>> GetByChannelId(int ChannelId)
        {
            return Get<ListResultDto<SettingsQuoteUploadDto>>(SystemRoutes.SettingsQuoteUploads.CreateGetByChannelId(ChannelId));
        }

        public GETResponseDto<PagedResultDto<SettingsQuoteUploadDto>> GetAll()
        {
            return Get<PagedResultDto<SettingsQuoteUploadDto>>(SystemRoutes.SettingsQuoteUploads.Get.Route);
        }

        public POSTResponseDto<PagedResultDto<SettingsQuoteUploadDto>> Search(SearchSettingsQuoteUploadDto searchSettingsQuoteUploadDto)
        {
            return Post<SearchSettingsQuoteUploadDto, PagedResultDto<SettingsQuoteUploadDto>>(searchSettingsQuoteUploadDto, SystemRoutes.SettingsQuoteUploads.Search.Route);
        }

        public POSTResponseDto<ListResultDto<SettingsQuoteUploadDto>> SearchNoPagination(SearchSettingsQuoteUploadDto searchSettingsQuoteUploadDto)
        {
            return Post<SearchSettingsQuoteUploadDto, ListResultDto<SettingsQuoteUploadDto>>(searchSettingsQuoteUploadDto, SystemRoutes.SettingsQuoteUploads.SearchNoPagination.Route);
        }

    }
}