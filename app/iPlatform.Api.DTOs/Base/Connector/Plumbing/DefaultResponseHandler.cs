﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Plumbing
{
    internal class DefaultResponseHandler : IHandleRestClientResponse
    {
        private readonly IHandleRestClientResponse[] _handlers;

        public DefaultResponseHandler(CallContext context)
        {
            _handlers = new IHandleRestClientResponse[]
            {
                new RestClientResponseNullHandler(context),
                new RestClientFailureHandler(context),
                new RestClientExceptionHandler(context),
                new HttpStatusCodeHandler(context)
            };
        }

        public void Handle(IRestResponse response)
        {
            foreach (var handler in _handlers)
                handler.Handle(response);
        }
    }
}