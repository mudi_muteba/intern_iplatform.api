﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Plumbing
{
    internal interface IHandleRestClientResponse
    {
        void Handle(IRestResponse response);
    }
}