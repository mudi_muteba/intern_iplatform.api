﻿using System;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Plumbing
{
    internal class CallContext
    {
        private readonly Uri apiUrl;
        private readonly Method method;

        public CallContext(Uri apiUrl, Method method)
        {
            this.apiUrl = apiUrl;
            this.method = method;
        }

        public string UrlInformation()
        {
            return string.Format("{0} ({1})", apiUrl, method);
        }
    }
}