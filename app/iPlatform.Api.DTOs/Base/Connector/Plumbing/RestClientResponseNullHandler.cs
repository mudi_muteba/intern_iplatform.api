﻿using System;
using Common.Logging;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Plumbing
{
    internal class RestClientResponseNullHandler : IHandleRestClientResponse
    {
        private static readonly ILog log = LogManager.GetLogger<RestClientResponseNullHandler>();
        private readonly CallContext _context;

        public RestClientResponseNullHandler(CallContext context)
        {
            _context = context;
        }

        public void Handle(IRestResponse response)
        {
            if (response == null)
            {
                var errorMessage = string.Format("NULL was returned from the API call. Called {0}", _context.UrlInformation());

                log.ErrorFormat(errorMessage);
                throw new Exception(errorMessage);
            }
        }
    }
}