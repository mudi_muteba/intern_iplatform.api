﻿using System;
using System.Linq;
using System.Net;
using Common.Logging;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Plumbing
{
    internal class HttpStatusCodeHandler : IHandleRestClientResponse
    {
        private static readonly ILog log = LogManager.GetLogger<HttpStatusCodeHandler>();
        private readonly CallContext _context;

        public HttpStatusCodeHandler(CallContext context)
        {
            _context = context;
        }

        public void Handle(IRestResponse response)
        {
            var allowedStatusCodes = new[]
            {
                HttpStatusCode.OK, HttpStatusCode.Accepted, HttpStatusCode.Created, HttpStatusCode.BadRequest, HttpStatusCode.Conflict,
                HttpStatusCode.NotFound
            };

            if (allowedStatusCodes.Any(c => c == response.StatusCode))
                return;

            var errorMessage =
                string.Format("Unexpected HTTP status code whilst calling API ({0}). The status code was {1}",
                    _context.UrlInformation(), response.StatusCode);

            log.ErrorFormat(errorMessage);

            throw new Exception(errorMessage);
        }
    }
}