﻿using System;
using Common.Logging;
using RestSharp;
using Shared;

namespace iPlatform.Api.DTOs.Base.Connector.Plumbing
{
    internal class RestClientExceptionHandler : IHandleRestClientResponse
    {
        private static readonly ILog log = LogManager.GetLogger<RestClientExceptionHandler>();
        private readonly CallContext _context;

        public RestClientExceptionHandler(CallContext context)
        {
            _context = context;
        }

        public void Handle(IRestResponse response)
        {
            if (response.ErrorException == null)
                return;

            var errorMessage = string.Format("Exception whilst calling API ({0}). The exception was {1}",
                _context.UrlInformation(), new ExceptionPrettyPrinter().Print(response.ErrorException));

            log.ErrorFormat(errorMessage);
            throw new Exception(errorMessage);
        }
    }
}