﻿using System;
using Common.Logging;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Plumbing
{
    internal class RestClientFailureHandler : IHandleRestClientResponse
    {
        private static readonly ILog log = LogManager.GetLogger<RestClientFailureHandler>();
        private readonly CallContext _context;

        public RestClientFailureHandler(CallContext context)
        {
            _context = context;
        }

        public void Handle(IRestResponse response)
        {
            if (response.ResponseStatus == ResponseStatus.Completed || response.ResponseStatus == ResponseStatus.None)
                return;

            var errorMessage = string.Format("Call to API {2} caused client failure. The status code is {0} with message {1}",
                response.ResponseStatus,
                response.ErrorMessage,
                _context.UrlInformation());

            log.ErrorFormat(errorMessage);

            throw new Exception(errorMessage);
        }
    }
}