﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Proposals;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Proposals
{
    public class ProposalsConnector : BaseConnector
    {

        public ProposalsConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {

        }

        protected override string GetByIdUrl(int id)
        {
            throw new System.NotImplementedException();
        }

        public POSTResponseDto<int> ImportProposals(SubmitLeadDto submitLeadDto)
        {
            return Post<SubmitLeadDto, int>(submitLeadDto, SystemRoutes.ProposalHeaders.ImportProposals.Route);
        }

    }
}