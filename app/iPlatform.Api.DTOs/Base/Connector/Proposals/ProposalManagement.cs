using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Proposals
{
    public class ProposalManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;
        public ProposalsConnector Proposals;
        public ProposalManagement(IRestClient client, ApiToken token = null)
        {
            this.client = client;
            this.token = token;
            Proposals = new ProposalsConnector(client, token);
        }



        public ProposalConnector Proposal(int id)
        {
            return new ProposalConnector(id, client, token);
        }
    }
}