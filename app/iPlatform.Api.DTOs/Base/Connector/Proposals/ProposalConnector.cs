﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Leads;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Proposals;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Proposals
{
    public class ProposalConnector : BaseConnector
    {
        private readonly int proposalId;

        public ProposalConnector(int proposalId, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            this.proposalId = proposalId;
        }

        protected override string GetByIdUrl(int id)
        {
            throw new System.NotImplementedException();
        }

        public POSTResponseDto<ProposalQuoteDto> GetQuotes(GetQuotesByProposalDto dto)
        {
            dto.Id = proposalId;
            var url = SystemRoutes.ProposalHeaders.CreateGetRatingsById(dto);
            return Post<GetQuotesByProposalDto, ProposalQuoteDto>(dto, url);
        }

        public PUTResponseDto<int> EditShouldQuote(EditProposalHeaderShouldQuoteDto editDto)
        {
            return Put<EditProposalHeaderShouldQuoteDto, int>(editDto, SystemRoutes.ProposalHeaders.EditProposalHeaderShouldQuote(editDto.Id));
        }

        public PUTResponseDto<int> EditValidateProposal(EditProposalHeaderValidateProposalDto editDto)
        {
            return Put<EditProposalHeaderValidateProposalDto, int>(editDto, SystemRoutes.ProposalHeaders.EditProposalHeaderValidateProposal(editDto.Id));
        }
    }
}