﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Questions
{
    public class QuestionConnector : BaseConnector
    {
        private readonly int id;

        public QuestionConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            this.id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Questions.CreateGetById(id);
        }

        public GETResponseDto<QuestionDefinitionDto> Get()
        {
            return base.Get<QuestionDefinitionDto>(GetByIdUrl(id));
        }
        
    }
}