﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Questions
{
   public  class MapVapQuestionDefinitionsConnector : BaseConnector
    {
       public MapVapQuestionDefinitionsConnector(IRestClient client, ApiToken token = null) : base(client, token)
       {
       }

       protected override string GetByIdUrl(int id)
       {
            return SystemRoutes.MapVapQuestionDefinition.CreateGetById(id);
       }

        public POSTResponseDto<int> Create(CreateMapVapQuestionDefinitionDto dto)
        {
            return Post<CreateMapVapQuestionDefinitionDto, int>(dto, SystemRoutes.MapVapQuestionDefinition.Post.Route);
        }
        public LISTResponseDto<ListMapVapQuestionDefinitionDto> GetList(int productId, int channelId)
        {
            string route = SystemRoutes.MapVapQuestionDefinition.GetListByCriteria(productId, channelId);
            return GetList<ListMapVapQuestionDefinitionDto>(route);
        }

        public LISTResponseDto<ListMapVapQuestionDefinitionDto> GetListByEnabled(int productId, int channelId, bool enabled)
        {
            string route = SystemRoutes.MapVapQuestionDefinition.CreateGetListByEnabledUrl(productId, channelId, enabled);
            return GetList<ListMapVapQuestionDefinitionDto>(route);
        }

        public POSTResponseDto<PagedResultDto<ListMapVapQuestionDefinitionDto>> Search(SearchMapVapQuestionDefinitionDto searchMapVapQuestionDefinitionDto)
        {
            return Post<SearchMapVapQuestionDefinitionDto, PagedResultDto<ListMapVapQuestionDefinitionDto>>(searchMapVapQuestionDefinitionDto,
                SystemRoutes.MapVapQuestionDefinition.Search.Route);
        }
    }
}
