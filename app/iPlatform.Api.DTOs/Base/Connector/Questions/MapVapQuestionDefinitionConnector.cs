﻿
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Questions
{
   public  class MapVapQuestionDefinitionConnector : BaseConnector
    {
        private readonly int id;

        public MapVapQuestionDefinitionConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            this.id = id;
        }

       protected override string GetByIdUrl(int id)
       {
            return SystemRoutes.MapVapQuestionDefinition.CreateGetById(id);
       }

        public GETResponseDto<MapVapQuestionDefinitionDto> Get()
        {
            return base.Get<MapVapQuestionDefinitionDto>(id);
        }

        public PUTResponseDto<int> Edit(EditMapVapQuestionDefinitionDto dto)
        {
            return Put<EditMapVapQuestionDefinitionDto, int>(dto, SystemRoutes.MapVapQuestionDefinition.CreatePutById(dto.Id));
        }

        public PUTResponseDto<bool> Delete()
        {
            return Put<bool>(SystemRoutes.MapVapQuestionDefinition.CreateDeleteById(id));
        }
    }
}
