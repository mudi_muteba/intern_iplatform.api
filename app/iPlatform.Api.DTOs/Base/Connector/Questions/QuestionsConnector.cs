﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Questions
{
    public class QuestionsConnector : BaseConnector
    {
        public QuestionsConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected string GetByCoverIdUrl(int id)
        {
            return SystemRoutes.Questions.CreateGetByCoverId(id);
        }

        protected override string GetByIdUrl(int id)
        {
            throw new System.NotImplementedException();
        }

        public GETResponseDto<PagedResultDto<QuestionDefinitionDto>> GetByCoverId(int coverid)
        {
            return base.Get<PagedResultDto<QuestionDefinitionDto>>(GetByCoverIdUrl(coverid));
        }

        public GETResponseDto<ListResultDto<QuestionDefinitionDto>> GetNoPaginationByCoverId(int coverid)
        {
            var result = Get<ListResultDto<QuestionDefinitionDto>>(SystemRoutes.Questions.CreateGetByCoverIdNopagination(coverid));
            return result;
        }

        public GETResponseDto<ListResultDto<QuestionDto>> GetByCoverIdAndProductId(int coverId, int productId)
        {
            var result = Get<ListResultDto<QuestionDto>>(SystemRoutes.Questions.CreateGetByCoverIdAndProductId(coverId, productId));
            return result;
        }
    }
}