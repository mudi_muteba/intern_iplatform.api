using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Questions
{
    public class QuestionManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public QuestionManagement(IRestClient client, ApiToken token = null)
        {
            this.client = client;
            this.token = token;
            Questions = new QuestionsConnector(client, token);
            MapVapQuestionDefinitions = new MapVapQuestionDefinitionsConnector(client, token);
        }

        public QuestionsConnector Questions { get; private set; }

        public QuestionConnector Question(int id)
        {
            return new QuestionConnector(id, client, token);
        }


        public MapVapQuestionDefinitionsConnector MapVapQuestionDefinitions { get; private set; }

        public MapVapQuestionDefinitionConnector MapVapQuestionDefinition(int id)
        {
            return new MapVapQuestionDefinitionConnector(id, client, token);
        }
    }
}