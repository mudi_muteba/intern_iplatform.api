﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Campaigns;
using RestSharp;
using iPlatform.Api.DTOs.Admin;

namespace iPlatform.Api.DTOs.Base.Connector.Campaigns
{
    public class CampaignsConnector : BaseConnector
    {
        public CampaignsConnector(IRestClient client, ApiToken token = null) : base(client, token) { }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Campaigns.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateCampaign(CreateCampaignDto createCampaignDto)
        {
            return Post<CreateCampaignDto, int>(createCampaignDto, SystemRoutes.Campaigns.Post.Route);
        }

        public POSTResponseDto<PagedResultDto<ListCampaignDto>> SearchCampaign(CampaignSearchDto CampaignSearchDto)
        {
            return Post<CampaignSearchDto, PagedResultDto<ListCampaignDto>>(CampaignSearchDto, SystemRoutes.Campaigns.PostSearch.Route);
        }

        public PagedResultDto<ListCampaignDto> GetAllCampaigns()
        {
            var result = Get<PagedResultDto<ListCampaignDto>>(SystemRoutes.Campaigns.Get.Route);
            return result.Response;
        }

        public List<ListCampaignDto> GetCampaignsByPartyId(int partyId)
        {
            var result = Get<List<ListCampaignDto>>(SystemRoutes.Campaigns.CreateGetByPartyId(partyId));
            return result.Response;
        }

        public List<ListCampaignDto> GetCampaignsByChannelPartyId(int channelId, int partyId)
        {
            var result = Get<List<ListCampaignDto>>(SystemRoutes.Campaigns.CreateGetByChannelPartyId(channelId, partyId));
            return result.Response;
        }

        public List<ListCampaignDto> GetCampaignsAcrossChannelPartyId(int partyId)
        {
            var result = Get<List<ListCampaignDto>>(SystemRoutes.Campaigns.CreateGetAcrossChannelPartyId(partyId));
            return result.Response;
        }

        public List<ListCampaignDto> GetCampaignsByChannelId(int channelId)
        {
            var result = Get<List<ListCampaignDto>>(SystemRoutes.Campaigns.CreateGetByChannelId(channelId));
            return result.Response;
        }

        public POSTResponseDto<bool> AssignLeadCampaigns(AssignLeadCampaignsDto assignLeadCampaignsDto)
        {
            return Post<AssignLeadCampaignsDto, bool>(assignLeadCampaignsDto, SystemRoutes.Campaigns.PostAssignLeadCampaigns.Route);
        }
    }
}