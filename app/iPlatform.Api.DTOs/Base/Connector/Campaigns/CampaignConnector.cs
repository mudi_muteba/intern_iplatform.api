﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Individual;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Campaigns
{
    public class CampaignConnector : BaseConnector
    {
        private readonly int _CampaignId;

        public CampaignConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _CampaignId = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Campaigns.CreateGetById(id);
        }

        public GETResponseDto<CampaignDto> Get()
        {
            return base.Get<CampaignDto>(GetByIdUrl(_CampaignId));
        }

        public PUTResponseDto<int> Disable()
        {
            return Put<int>(SystemRoutes.Campaigns.DisableById(_CampaignId));
        }

        public PUTResponseDto<int> Delete()
        {
            return Put<int>(SystemRoutes.Campaigns.DeleteById(_CampaignId));
        }

        public PUTResponseDto<int> EditCampaign(EditCampaignDto editCampaignDto)
        {
            return Put<EditCampaignDto, int>(editCampaignDto, SystemRoutes.Campaigns.PutById(editCampaignDto.Id));
        }

        public POSTResponseDto<bool> AssignLeads(AssignLeadToCampaignDto dto)
        {
            return Post<AssignLeadToCampaignDto, bool>(dto, SystemRoutes.Campaigns.AssignLeadsById(dto.Id));
        }


        public GETResponseDto<ListResultDto<ListIndividualDto>> GetLeads()
        {
            return base.Get<ListResultDto<ListIndividualDto>>(SystemRoutes.Campaigns.GetLeadByCampaignId(_CampaignId));
        }

        public GETResponseDto<ListResultDto<ListIndividualDto>> GetLeadsbyAgentId(int agentId)
        {
            return base.Get<ListResultDto<ListIndividualDto>>(SystemRoutes.Campaigns.GetLeadsByAgentId(_CampaignId, agentId));
        }

        public GETResponseDto<CampaignLeadBucketDto> GetCampaignLeadBucket(int leadId)
        {
            return base.Get<CampaignLeadBucketDto>(SystemRoutes.Campaigns.GetCampaignLeadBucket(_CampaignId, leadId));
        }

        public PUTResponseDto<NextLeadResponseDto> GetNextLead()
        {
            return Put<NextLeadResponseDto>(SystemRoutes.Campaigns.GetNextLead(_CampaignId));
        }

        public POSTResponseDto<NextLeadResponseDto> GetNextSerityImportLead(GetSeritiImportNextLeadDto criteria)
        {
            return Post<GetSeritiImportNextLeadDto, NextLeadResponseDto>(criteria, SystemRoutes.Campaigns.GetAgentNextSerityImportLead(_CampaignId));
        }
        
        public POSTResponseDto<NextLeadResponseDto> AssignLeadToAgent(AssignLeadToAgentDto dto)
        {
            return Post<AssignLeadToAgentDto, NextLeadResponseDto>(dto, SystemRoutes.Campaigns.PostAssignLeadToAgent(_CampaignId));
        }
    }
}