using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Campaigns
{
    public class CampaignManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public CampaignManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            Campaigns = new CampaignsConnector(client, token);

        }

        public CampaignsConnector Campaigns { get; private set; }

        public CampaignConnector Campaign(int id)
        {
            return new CampaignConnector(id, client, token);
        }
    }
}