﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using RestSharp;
using iPlatform.Api.DTOs.CustomApp;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace iPlatform.Api.DTOs.Base.Connector.CustomApp
{
    public class CustomAppConnector : BaseConnector
    {
        public CustomAppConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public CustomAppResponseDto<List<RegisterDto>> Register(RegisterSearchDto registerSearchDto)
        {
            return PostCustomApp<RegisterSearchDto, List<RegisterDto>>(registerSearchDto, SystemRoutes.Customapp.Register.Route);
        }

        public CustomAppResponseDto<List<VerifyMemberDto>> VerifyMember(VerifyMemberSearchDto verifyMemberSearchDto)
        {
            return PostCustomApp<VerifyMemberSearchDto, List<VerifyMemberDto>>(verifyMemberSearchDto, SystemRoutes.Customapp.VerifyMember.Route);
        }

        public CustomAppResponseDto<List<GetPolicyInfoDto>> GetPolicyInfo(GetPolicyInfoSearchDto getPolicyInfoSearchDto)
        {
            return PostCustomApp<GetPolicyInfoSearchDto, List<GetPolicyInfoDto>>(getPolicyInfoSearchDto, SystemRoutes.Customapp.GetPolicyInfo.Route);
        }

        public CustomAppResponseDto<List<GetMemberDetailsDto>> GetMemberDetails(GetMemberDetailsSearchDto getMemberDetailsSearchDto)
        {
            return PostCustomApp<GetMemberDetailsSearchDto, List<GetMemberDetailsDto>>(getMemberDetailsSearchDto, SystemRoutes.Customapp.GetMemberDetails.Route);
        }

        public CustomAppResponseDto<List<UpdateMemberDetailsDto>> UpdateMemberDetails(UpdateMemberDetailsSearchDto updateMemberDetailsSearchDto)
        {
            return PostCustomApp<UpdateMemberDetailsSearchDto, List<UpdateMemberDetailsDto>>(updateMemberDetailsSearchDto, SystemRoutes.Customapp.UpdateMemberDetails.Route);
        }
    }
}
