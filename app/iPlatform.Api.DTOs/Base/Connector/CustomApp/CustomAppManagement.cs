﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Connector.CustomApp
{   
    public class CustomAppManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public CustomAppConnector CustomApp { get; private set; }

        public CustomAppManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            CustomApp = new CustomAppConnector(client, token);
        }
    }
}
