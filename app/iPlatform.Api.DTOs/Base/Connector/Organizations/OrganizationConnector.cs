﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Organisations;
using iPlatform.Api.DTOs.Party.Payments;
using iPlatform.Api.DTOs.Products;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Organizations
{
    public class OrganizationConnector : BaseConnector
    {
        private readonly int _OrgId;
        public OrganizationConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        public OrganizationConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _OrgId = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Organisations.CreateGetById(id);
        }

        public GETResponseDto<OrganisationDto> Get()
        {
            return base.Get<OrganisationDto>(GetByIdUrl(_OrgId));
        }

        public PUTResponseDto<int> Edit(EditOrganizationDto editDto)
        {
            return Put<EditOrganizationDto, int>(editDto, SystemRoutes.Organisations.CreateEditUrl(editDto.Id));
        }
    }
}