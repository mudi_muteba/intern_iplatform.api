﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Organisations;
using iPlatform.Api.DTOs.Party.Payments;
using iPlatform.Api.DTOs.Products;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Organizations
{
    public class OrganizationsConnector : BaseConnector
    {
        public OrganizationsConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Organisations.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateOrganization(CreateOrganizationDto createOrganizationDto)
        {
            return Post<CreateOrganizationDto, int>(createOrganizationDto, SystemRoutes.Organisations.Post.Route);
        }

        public LISTPagedResponseDto<ListOrganizationDto> Get(int pageNumber, int pageSize)
        {
            var url = SystemRoutes.Organisations.CreateGetAllWithPaginationUrl(pageNumber, pageSize);
            return GetPagedList<ListOrganizationDto>(url);
        }
        public LISTPagedResponseDto<ListOrganizationDto> Get()
        {
            return GetPagedList<ListOrganizationDto>(SystemRoutes.Organisations.Get.Route);
        }

        public LISTResponseDto<ListOrganizationDto> GetAll()
        {
            return GetList<ListOrganizationDto>(SystemRoutes.Organisations.GetAll.Route);
        }
    }
}