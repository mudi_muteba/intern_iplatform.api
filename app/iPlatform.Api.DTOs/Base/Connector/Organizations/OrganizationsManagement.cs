
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Organizations
{
    public class OrganizationsManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public OrganizationsManagement(IRestClient client, ApiToken token = null)
        {
            this.client = client;
            this.token = token;
            Organizations = new OrganizationsConnector(client, token);
        }

        public OrganizationsConnector Organizations { get; private set; }

        public OrganizationConnector Organization(int id)
        {
            return new OrganizationConnector(id, client, token);
        }
    }
}