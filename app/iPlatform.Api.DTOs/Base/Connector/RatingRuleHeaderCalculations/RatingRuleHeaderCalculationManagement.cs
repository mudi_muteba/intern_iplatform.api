﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.RatingRuleHeaderCalculations
{
   public class RatingRuleHeaderCalculationManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;
        public RatingRuleHeaderCalculationsConnector RatingRuleHeaderCalculations { get; private set; }

        public RatingRuleHeaderCalculationManagement(IRestClient client, ApiToken token = null)
        {
            this.client = client;
            this.token = token;
            this.RatingRuleHeaderCalculations = new RatingRuleHeaderCalculationsConnector(client, token);
        }

        
        public RatingRuleHeaderCalculationConnector RatingRuleHeaderCalculation(int id)
        {
            return new RatingRuleHeaderCalculationConnector(id, client, token);
        }
    }
}
