﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.RatingRuleHeaderCalculations
{
    public class RatingRuleHeaderCalculationConnector : BaseConnector
    {
        private readonly int id;

        public RatingRuleHeaderCalculationConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            this.id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.RatingRuleHeaderCalculations.CreateGetById(id);
        }

        public POSTResponseDto<int> Create(CreateRatingRuleHeaderCalculationDto dto)
        {
            return Post<CreateRatingRuleHeaderCalculationDto, int>(dto, SystemRoutes.RatingRuleHeaderCalculations.Post.Route);
        }

        public PUTResponseDto<int> Edit(EditRatingRuleHeaderCalculationDto dto)
        {
            return Put<EditRatingRuleHeaderCalculationDto, int>(dto, SystemRoutes.RatingRuleHeaderCalculations.CreatePutById(dto.Id));
        }

        public PUTResponseDto<int> Delete()
        {
            return Put<int>(SystemRoutes.RatingRuleHeaderCalculations.CreateDeleteById(id));
        }

        public GETResponseDto<RatingRuleHeaderCalculationDto> Get()
        {
            return base.Get<RatingRuleHeaderCalculationDto>(id);
        }
    }
}
