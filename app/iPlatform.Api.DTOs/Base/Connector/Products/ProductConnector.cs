﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Products
{
    public class ProductConnector : BaseConnector
    {
        private readonly int productId;

        public ProductConnector(int productId, IRestClient client, ApiToken token = null) : base(client, token)
        {
            this.productId = productId;
        }

        protected override string GetByIdUrl(int id)
        {
            throw new System.NotImplementedException();
        }

        public GETResponseDto<CoverDefinitionDto> Cover(int coverId)
        {
            var url = SystemRoutes.Products.CreateCoversByIdUrl(productId, coverId);
            return base.Get<CoverDefinitionDto>(url);
        }

        public GETResponseDto<ProposalFormDto> ProposalForm()
        {
            var url = SystemRoutes.Products.CreateProposalFormUrl(productId);

            return base.Get<ProposalFormDto>(url);
        }
    }
}