using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Products
{
    public class ProductManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public ProductManagement(IRestClient client, ApiToken token = null)
        {
            this.client = client;
            this.token = token;
            Products = new ProductsConnector(client, token);
        }

        public ProductsConnector Products { get; private set; }

        public ProductConnector Product(int id)
        {
            return new ProductConnector(id, client, token);
        }
    }
}