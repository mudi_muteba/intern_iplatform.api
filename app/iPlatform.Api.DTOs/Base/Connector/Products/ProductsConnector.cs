﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Products;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Products
{
    public class ProductsConnector : BaseConnector
    {
        public ProductsConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Products.CreateGetById(id);
        }

        public LISTPagedResponseDto<ListAllocatedProductDto> Get()
        {
            return GetPagedList<ListAllocatedProductDto>(SystemRoutes.Products.GetAll.Route);
        }
        public LISTResponseDto<ListProductDto> GetAllNoPagination()
        {
            return GetList<ListProductDto>(SystemRoutes.Products.GetAllNoPagination.Route);
        }

        public GETResponseDto<AllocatedProductDto> Get(int productId)
        {
            return base.Get<AllocatedProductDto>(productId);
        }

        public GETResponseDto<AllocatedProductDto> Get(string productCode)
        {
            var url = SystemRoutes.Products.CreateGetByCode(productCode);
            return base.Get<AllocatedProductDto>(url);
        }

        public LISTPagedResponseDto<ListAllocatedProductDto> Get(int pageNumber, int pageSize)
        {
            var url = SystemRoutes.Products.CreateGetAllWithPaginationUrl(pageNumber, pageSize);
            return GetPagedList<ListAllocatedProductDto>(url);
        }

        public LISTResponseDto<ListProductByCoverDto> GetByCover(int channelId)
        {
            var url = SystemRoutes.Products.GetProductsByCoverUrl(channelId);
            return base.GetList<ListProductByCoverDto>(url);
        }

        public LISTResponseDto<ProductBenefitDto> GetBenefitsByCover(int productId, int coverId)
        {
            var url = SystemRoutes.Products.CreateBenefitsByCoverIdUrl(productId, coverId);
            return base.GetList<ProductBenefitDto>(url);
        }

        public LISTResponseDto<ProductFeeDto> GetFees(int productId)
        {
            var url = SystemRoutes.Products.CreateFeesUrl(productId);
            return base.GetList<ProductFeeDto>(url);
        }

        public GETResponseDto<ProductFeeDto> GetFeeById(int productId, int feeId)
        {
            var url = SystemRoutes.Products.CreateFeeByIdUrl(productId, feeId);
            return base.Get<ProductFeeDto>(url);
        }

        public LISTResponseDto<ProductFeeDto> GetFeesByChannelId(int productId, int channelId)
        {
            var url = SystemRoutes.Products.CreateFeesByChannelIdUrl(productId, channelId);
            return base.GetList<ProductFeeDto>(url);
        }

        public GETResponseDto<ProductFeeDto> GetFeeByIdChannelId(int productId, int feeId, int channelId)
        {
            var url = SystemRoutes.Products.CreateFeeByIdChannelIdUrl(productId, feeId, channelId);
            return base.Get<ProductFeeDto>(url);
        }

        public POSTResponseDto<int> CreateProductFee(CreateProductFeeDto createProductFeeDto)
        {
            return Post<CreateProductFeeDto, int>(createProductFeeDto, SystemRoutes.Products.Create.Route);
        }

        public PUTResponseDto<int> EditProductFee(EditProductFeeDto editProductFeeDto)
        {
            return Put<EditProductFeeDto, int>(editProductFeeDto, SystemRoutes.Products.CreateEditProductFeeById(editProductFeeDto.Id));
        }

        public PUTResponseDto<int> Delete(int productFeeId)
        {
            return Put<int>(SystemRoutes.Products.DisableById(productFeeId));
        }

        public GETResponseDto<PagedResultDto<ListProductFeeDto>> GetAllProductFeePaginated(int pageNumber, int pageSize)
        {
            var url = SystemRoutes.Products.CreateGetAllFeesWithPaginationUrl(pageNumber, pageSize);
            return Get<PagedResultDto<ListProductFeeDto>>(url);
        }

        public GETResponseDto<ListProductFeeDto> GetProductFeeById(int productId)
        {
            return base.Get<ListProductFeeDto>(SystemRoutes.Products.GetProductFeebyId(productId));
        }

        public POSTResponseDto<PagedResultDto<ListProductFeeDto>> Search(SearchProductFeeDto searchProductFeeDto)
        {
            return Post<SearchProductFeeDto, PagedResultDto<ListProductFeeDto>>(searchProductFeeDto, SystemRoutes.Products.SearchFees.Route);
        }

        public LISTResponseDto<ProductInfoDto> GetAllocatedByChannelId(int channelid)
        {
            return GetList<ProductInfoDto>(SystemRoutes.Products.CreateGetAllocatedByChannelId(channelid));
        }
        public LISTResponseDto<ProductInfoDto> GetAllAllocatedProducts()
        {
            return GetList<ProductInfoDto>(SystemRoutes.Products.GetAllAllocatedProduct.Route);
        }

        public LISTResponseDto<ListProductDto> GetAllForReporting()
        {
            return GetList<ListProductDto>(SystemRoutes.Products.GetAllForReporting.Route);
        }
    }
}