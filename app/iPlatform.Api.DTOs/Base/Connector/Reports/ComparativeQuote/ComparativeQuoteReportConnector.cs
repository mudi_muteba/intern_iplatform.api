﻿using System.Collections.Generic;

using RestSharp;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.ContactUs;
using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Reports.ComparativeQuote;
using iPlatform.Api.DTOs.Reports.ComparativeQuote.Criteria;

namespace iPlatform.Api.DTOs.Base.Connector.Reports.Quote
{
    public class ComparativeQuoteReportConnector : BaseConnector
    {
        public ComparativeQuoteReportConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {

        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Reports.CreateGetById(id);
        }

        public POSTResponseDto<ComparativeQuoteReportContainerDto> GetComparativeQuoteReport(ComparativeQuoteReportCriteriaDto criteria)
        {
            return Post<ComparativeQuoteReportCriteriaDto, ComparativeQuoteReportContainerDto>(criteria, SystemRoutes.ComparativeQuoteReports.GetComparativeQuoteReport.Route);
        }

        public POSTResponseDto<ReportEmailResponseDto> EmailComparativeQuoteReport(ComparativeQuoteReportEmailDto criteria)
        {
            return Post<ComparativeQuoteReportEmailDto, ReportEmailResponseDto>(criteria, SystemRoutes.ComparativeQuoteReports.EmailComparativeQuoteReport.Route);
        }

        public POSTResponseDto<ReportEmailResponseDto> ContactUsEmail(ContactUsEmailDto criteria)
        {
            return Post<ContactUsEmailDto, ReportEmailResponseDto>(criteria, SystemRoutes.ComparativeQuoteReports.ContactUsEmail.Route);
        }
    }
}