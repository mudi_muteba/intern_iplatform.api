﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.PolicyBinding;
using iPlatform.Api.DTOs.Reports.PolicyBinding.Criteria;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Reports.PolicyBinding
{
    public class PolicyBindingReportConnector : BaseConnector
    {
        public PolicyBindingReportConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {

        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Reports.CreateGetById(id);
        }

        public POSTResponseDto<PolicyBindingReportContainerDto> GetPolicyBindingReport(PolicyBindingReportCriteriaDto criteria)
        {
            return Post<PolicyBindingReportCriteriaDto, PolicyBindingReportContainerDto>(criteria, SystemRoutes.PolicyBinding.GetPolicyBindingReport.Route);
        }
    }
}