﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.ReportSchedules;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Reports
{
    public class ReportScheduleConnector : BaseConnector
    {
        public ReportScheduleConnector(IRestClient client, ApiToken token = null) : base(client, token) { }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ReportSchedules.CreateGetById(id);
        }

        public GETResponseDto<ReportScheduleDto> Get(int id)
        {
            return base.Get<ReportScheduleDto>(id);
        }

        public POSTResponseDto<PagedResultDto<ReportScheduleDto>> Search(SearchReportScheduleDto dto)
        {
            return Post<SearchReportScheduleDto, PagedResultDto<ReportScheduleDto>>(dto, SystemRoutes.ReportSchedules.Search.Route);
        }

        public POSTResponseDto<int> Save(SaveReportScheduleDto dto)
        {
            return Post<SaveReportScheduleDto, int>(dto, SystemRoutes.ReportSchedules.Save.Route);
        }

        public POSTResponseDto<int> Invoke(InvokeReportScheduleDto dto)
        {
            return Post<InvokeReportScheduleDto, int>(dto, SystemRoutes.ReportSchedules.Invoke.Route);
        }

        public DELETEResponseDto<int> Delete(DeleteReportScheduleDto dto)
        {
            return Delete<DeleteReportScheduleDto, int>(dto, SystemRoutes.ReportSchedules.CreateGetById(dto.Id));
        }
    }
}