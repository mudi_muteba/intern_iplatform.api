﻿using System.Collections.Generic;

using RestSharp;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Reports.Quote;
using iPlatform.Api.DTOs.Reports.Quote.Criteria;

namespace iPlatform.Api.DTOs.Base.Connector.Reports.Quote
{
    public class QuoteReportConnector : BaseConnector
    {
        public QuoteReportConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {

        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Reports.CreateGetById(id);
        }

        public POSTResponseDto<QuoteReportDto> GetQuoteReport(QuoteReportCriteriaDto criteria)
        {
            return Post<QuoteReportCriteriaDto, QuoteReportDto>(criteria, SystemRoutes.QuoteReports.GetQuoteReport.Route);
        }

        public POSTResponseDto<List<QuoteReportHeaderDto>> GetQuoteReportHeader(QuoteReportCriteriaDto criteria)
        {
            return Post<QuoteReportCriteriaDto, List<QuoteReportHeaderDto>>(criteria, SystemRoutes.QuoteReports.GetQuoteReportHeader.Route);
        }

        public POSTResponseDto<QuoteReportSettingsDto> GetQuoteReportSettings(QuoteReportCriteriaDto criteria)
        {
            return Post<QuoteReportCriteriaDto, QuoteReportSettingsDto>(criteria, SystemRoutes.QuoteReports.GetQuoteReportSettings.Route);
        }

        public POSTResponseDto<List<QuoteReportSummaryDto>> GetQuoteReportSummary(QuoteReportCriteriaDto criteria)
        {
            return Post<QuoteReportCriteriaDto, List<QuoteReportSummaryDto>>(criteria, SystemRoutes.QuoteReports.GetQuoteReportSummary.Route);
        }

        public POSTResponseDto<List<QuoteReportAssetDto>> GetQuoteReportAssets(QuoteReportCriteriaDto criteria)
        {
            return Post<QuoteReportCriteriaDto, List<QuoteReportAssetDto>>(criteria, SystemRoutes.QuoteReports.GetQuoteReportAssets.Route);
        }

        public POSTResponseDto<List<QuoteReportTotalsDto>> GetQuoteReportTotals(QuoteReportCriteriaDto criteria)
        {
            return Post<QuoteReportCriteriaDto, List<QuoteReportTotalsDto>>(criteria, SystemRoutes.QuoteReports.GetQuoteReportTotals.Route);
        }

        public POSTResponseDto<List<QuoteReportValueAddedProductDto>> GetQuoteReportValueAddedProducts(QuoteReportCriteriaDto criteria)
        {
            return Post<QuoteReportCriteriaDto, List<QuoteReportValueAddedProductDto>>(criteria, SystemRoutes.QuoteReports.GetQuoteReportValueAddedProducts.Route);
        }

        public POSTResponseDto<ReportEmailResponseDto> EmailQuoteReport(QuoteReportEmailDto criteria)
        {
            return Post<QuoteReportEmailDto, ReportEmailResponseDto>(criteria, SystemRoutes.QuoteReports.EmailQuoteReport.Route);
        }
    }
}