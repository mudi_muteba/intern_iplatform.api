﻿using RestSharp;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Reports.Base.Criteria;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Reports;

namespace iPlatform.Api.DTOs.Base.Connector.Reports
{
    public class ReportsConnector : BaseConnector
    {
        public ReportsConnector(IRestClient client, ApiToken token = null) : base(client, token) { }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Reports.CreateGetById(id);
        }

        public GETResponseDto<List<ReportDto>> GetReports()
        {
            return Get<List<ReportDto>>(SystemRoutes.Reports.GetReports.Route);
        }

        public POSTResponseDto<List<ReportDto>> GetReportsByCriteria(ReportCriteriaDto criteria)
        {
            return Post<ReportCriteriaDto, List<ReportDto>>(criteria, SystemRoutes.Reports.GetReportsByCriteria.Route);
        }

        public POSTResponseDto<ReportGeneratorResponseDto> GenerateReport(ReportGeneratorDto criteria)
        {
            return Post<ReportGeneratorDto, ReportGeneratorResponseDto>(criteria, SystemRoutes.Reports.GenerateReports.Route);
        }

        public POSTResponseDto<int> CreateMultiple(CreateMultipleReportsDto dto)
        {
            return Post<CreateMultipleReportsDto, int>(dto, SystemRoutes.Reports.Post.Route);
        }

        public POSTResponseDto<ValidatedReportCampaignsResponseDto> ValidateReportCampaigns(ValidateReportCampaignsDto dto)
        {
            return Post<ValidateReportCampaignsDto, ValidatedReportCampaignsResponseDto>(dto, SystemRoutes.Reports.ValidateReportCampaigns.Route);
        }
    }
}