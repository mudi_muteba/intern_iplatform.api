﻿using RestSharp;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;

using iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformance;
using iPlatform.Api.DTOs.Reports.CallCentre.AgentPerformanceByProduct;
using iPlatform.Api.DTOs.Reports.CallCentre.Criteria;
using iPlatform.Api.DTOs.Reports.CallCentre.DetailedInsurerUpload;
using iPlatform.Api.DTOs.Reports.CallCentre.InsurerData;
using iPlatform.Api.DTOs.Reports.CallCentre.InsurerQuoteBreakdown;
using iPlatform.Api.DTOs.Reports.CallCentre.LeadManagement;
using iPlatform.Api.DTOs.Reports.CallCentre.OutboundRatios;
using iPlatform.Api.DTOs.Reports.CallCentre.SalesManagement;
using iPlatform.Api.DTOs.Reports.CallCentre.User;
using iPlatform.Api.DTOs.Reports.CallCentre.ManagerLevelQuoteUpload;
using iPlatform.Api.DTOs.Reports.CallCentre.BrokerQuoteUpload;

namespace iPlatform.Api.DTOs.Base.Connector.Reports.Quote
{
    public class CallCentreReportConnector : BaseConnector
    {
        public CallCentreReportConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {

        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Reports.CreateGetById(id);
        }

        public POSTResponseDto<LeadManagementReportContainerDto> GetLeadManagementReport(CallCentreReportCriteriaDto criteria)
        {
            return Post<CallCentreReportCriteriaDto, LeadManagementReportContainerDto>(criteria, SystemRoutes.CallCentreReports.GetCallCentreLeadManagementReport.Route);
        }

        public POSTResponseDto<CallCentreOutboundRatiosReportContainerDto> GetOutboundRatiosReport(CallCentreReportCriteriaDto criteria)
        {
            return Post<CallCentreReportCriteriaDto, CallCentreOutboundRatiosReportContainerDto>(criteria, SystemRoutes.CallCentreReports.GetCallCentreOutboundRatiosReport.Route);
        }

        public POSTResponseDto<CallCentreSalesManagementReportContainerDto> GetSalesManagementReport(CallCentreReportCriteriaDto criteria)
        {
            return Post<CallCentreReportCriteriaDto, CallCentreSalesManagementReportContainerDto>(criteria, SystemRoutes.CallCentreReports.GetCallCentreSalesManagementReport.Route);
        }

        public POSTResponseDto<AgentPerformanceReportContainerDto> GetAgentPerformanceReport(CallCentreReportCriteriaDto criteria)
        {
            return Post<CallCentreReportCriteriaDto, AgentPerformanceReportContainerDto>(criteria, SystemRoutes.CallCentreReports.GetCallCentreAgentPerformanceReport.Route);
        }

        public POSTResponseDto<AgentPerformanceByProductReportContainerDto> GetAgentPerformanceByProductReport(CallCentreReportCriteriaDto criteria)
        {
            return Post<CallCentreReportCriteriaDto, AgentPerformanceByProductReportContainerDto>(criteria, SystemRoutes.CallCentreReports.GetCallCentreAgentPerformanceByProductReport.Route);
        }

        public POSTResponseDto<InsurerQuoteBreakdownReportContainerDto> GetInsurerQuoteBreakdownReport(CallCentreReportCriteriaDto criteria)
        {
            return Post<CallCentreReportCriteriaDto, InsurerQuoteBreakdownReportContainerDto>(criteria, SystemRoutes.CallCentreReports.GetCallCentreInsurerQuoteBreakdownReport.Route);
        }

        public POSTResponseDto<InsurerDataReportContainerDto> GetInsurerDataReport(CallCentreReportCriteriaDto criteria)
        {
            return Post<CallCentreReportCriteriaDto, InsurerDataReportContainerDto>(criteria, SystemRoutes.CallCentreReports.GetCallCentreInsurerDataReport.Route);
        }

        public POSTResponseDto<DetailedInsurerUploadReportContainerDto> GetDetailedInsurerUploadReport(CallCentreReportCriteriaDto criteria)
        {
            return Post<CallCentreReportCriteriaDto, DetailedInsurerUploadReportContainerDto>(criteria, SystemRoutes.CallCentreReports.GetCallCentreDetailedInsurerUploadReport.Route);
        }

        public POSTResponseDto<UserReportContainerDto> GetUserReport(CallCentreReportCriteriaDto criteria)
        {
            return Post<CallCentreReportCriteriaDto, UserReportContainerDto>(criteria, SystemRoutes.CallCentreReports.GetCallCentreUserReport.Route);
        }

        public POSTResponseDto<BrokerQuoteUploadReportContainerDto> GetBrokerQuoteUploadsReport(CallCentreReportCriteriaDto criteria)
        {
            return Post<CallCentreReportCriteriaDto, BrokerQuoteUploadReportContainerDto>(criteria, SystemRoutes.CallCentreReports.GetCallCentreBrokerQuoteUploadsReport.Route);
        }

        public POSTResponseDto<ManagerLevelQuoteUploadReportContainerDto> GetManagerLevelQuoteUploadsReport(CallCentreReportCriteriaDto criteria)
        {
            return Post<CallCentreReportCriteriaDto, ManagerLevelQuoteUploadReportContainerDto>(criteria, SystemRoutes.CallCentreReports.GetCallCentreManagerLevelQuoteUploadsReport.Route);
        }
    }
}