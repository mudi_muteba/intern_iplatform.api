using iPlatform.Api.DTOs.Base.Connector.Reports.AIGReports;
using iPlatform.Api.DTOs.Base.Connector.Reports.PolicyBinding;
using RestSharp;
using iPlatform.Api.DTOs.Base.Connector.Reports.Quote;

namespace iPlatform.Api.DTOs.Base.Connector.Reports
{
    public class ReportManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public ReportManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;

            Reports = new ReportsConnector(client, token);
            ReportSchedules = new ReportScheduleConnector(client, token);
            QuoteReport = new QuoteReportConnector(client, token);
            ComparativeQuoteReport = new ComparativeQuoteReportConnector(client, token);
            CallCentreReportConnector = new CallCentreReportConnector(client, token);
            AigReport = new AigReportConnector(client, token);
            ComparativeQuoteReport = new ComparativeQuoteReportConnector(client, token);
            PolicyBindingReport =  new PolicyBindingReportConnector(client, token);
        }

        public ReportsConnector Reports { get; private set; }

        public ReportScheduleConnector ReportSchedules { get; private set; }

        public QuoteReportConnector QuoteReport { get; private set; }

        public ComparativeQuoteReportConnector ComparativeQuoteReport { get; private set; }

        public CallCentreReportConnector CallCentreReportConnector { get; private set; }

        public AigReportConnector AigReport { get; private set; }
        public PolicyBindingReportConnector PolicyBindingReport { get; private set; }
    }
}