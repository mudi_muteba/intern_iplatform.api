﻿using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.AIG;
using iPlatform.Api.DTOs.Reports.AIG.AllRisk;
using iPlatform.Api.DTOs.Reports.AIG.AllRisk.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Building;
using iPlatform.Api.DTOs.Reports.AIG.Building.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Content;
using iPlatform.Api.DTOs.Reports.AIG.Content.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Lead;
using iPlatform.Api.DTOs.Reports.AIG.Lead.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.Motor;
using iPlatform.Api.DTOs.Reports.AIG.Motor.Criteria;
using iPlatform.Api.DTOs.Reports.AIG.ReportCriteria;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceIntegration;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceLogSummery;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceLogSummery.Criteria;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Reports.AIGReports
{
    public class AigReportConnector : BaseConnector
    {
        public AigReportConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }
        public GETResponseDto<List<string>> GetReportList()
        {
            return base.Get<List<string>>(SystemRoutes.AigReport.GetReportList.Route);
        }

        public POSTResponseDto<AigReportFileDto> GetReport(AigReportCriteriaDto criteria)
        {
            return Post<AigReportCriteriaDto, AigReportFileDto>(criteria, SystemRoutes.AigReport.GetAigReport.Route);
        }

        public POSTResponseDto<AigReportLeadResultDto> GetReportForAigLeadExtract(AigReportLeadExtractCriteriaDto criteria)
        {
            return Post<AigReportLeadExtractCriteriaDto, AigReportLeadResultDto>(criteria, SystemRoutes.AigReport.GetAigReportLeadExtract.Route);
        }

        public POSTResponseDto<AigReportMotorResultDto> GetReportForAigMotorExtract(AigReportMotorExtractCriteriaDto criteria)
        {
            return Post<AigReportMotorExtractCriteriaDto, AigReportMotorResultDto>(criteria, SystemRoutes.AigReport.GetAigReportMotorExtract.Route);
        }

        public POSTResponseDto<AigReportAllRiskResultDto> GetReportForAigAllRiskExtract(AigReportAllRiskExtractCriteriaDto criteria)
        {
            return Post<AigReportAllRiskExtractCriteriaDto, AigReportAllRiskResultDto>(criteria, SystemRoutes.AigReport.GetAigReportAllRiskExtract.Route);
        }

        public POSTResponseDto<AigReportContentResultDto> GetReportForAigContentExtract(AigReportContentExtractCriteriaDto criteria)
        {
            return Post<AigReportContentExtractCriteriaDto, AigReportContentResultDto>(criteria, SystemRoutes.AigReport.GetAigReportContentExtract.Route);
        }

        public POSTResponseDto<AigReportBuildingResultDto> GetReportForAigBuildingExtract(AigReportBuildingExtractCriteriaDto criteria)
        {
            return Post<AigReportBuildingExtractCriteriaDto, AigReportBuildingResultDto>(criteria, SystemRoutes.AigReport.GetAigReportBuildingExtract.Route);
        }

        public POSTResponseDto<AigReportSalesForceIntegrationResultDto> GetReportForAigSalesForceIntergationExtract(AigReportSalesForceIntegrationExtractCriteriaDto criteria)
        {
            return Post<AigReportSalesForceIntegrationExtractCriteriaDto, AigReportSalesForceIntegrationResultDto>(criteria, SystemRoutes.AigReport.GetAigReportSalesForceIntegrationExtract.Route);
        }

        public POSTResponseDto<AigReportSalesForceLogSummeryResultDto> GetReportForAigSalesForceLogSummeryExtract(AigReportSalesForceLogSummeryExtractCriteriaDto criteria)
        {
            return Post<AigReportSalesForceLogSummeryExtractCriteriaDto, AigReportSalesForceLogSummeryResultDto>(criteria, SystemRoutes.AigReport.GetAigReportSalesForceLogSummeryExtract.Route);
        }
    }
}
