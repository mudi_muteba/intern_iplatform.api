﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Logs
{
    public class LogFileManagement
    {
        private readonly IRestClient m_Client;
        private readonly ApiToken m_Token;

        public LogFileManagement(IRestClient client, ApiToken token)
        {
            m_Client = client;
            m_Token = token;
        }

        public LogFileConnector LogFileConnector()
        {
            return new LogFileConnector(m_Client, m_Token);
        }
    }
}
