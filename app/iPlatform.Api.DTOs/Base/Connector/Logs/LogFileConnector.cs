﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Logs;
using iPlatform.Api.DTOs.ProductPricingStructure;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Logs
{
    public class LogFileConnector : BaseConnector
    {
        private readonly int m_Id;

        public LogFileConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {

        }

        public POSTResponseDto<LogsResultDto> GetLogFiles(GetLogsDto logsRequest)
        {
            return Post<GetLogsDto, LogsResultDto>(logsRequest, SystemRoutes.LogReader.CreatePost());
        }

        protected override string GetByIdUrl(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
