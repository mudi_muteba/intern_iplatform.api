﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.DataView.DataViewLeadImport;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;

namespace iPlatform.Api.DTOs.Base.Connector.DataView
{
    public class DataViewConnector : BaseConnector
    {
        public DataViewConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public POSTResponseDto<IList<DataViewLeadImportDto>> GetLeadImportDataView(SearchDataViewLeadImportDto searchDataViewLeadImportDto)
        {
            return Post<SearchDataViewLeadImportDto, IList<DataViewLeadImportDto>>(searchDataViewLeadImportDto, SystemRoutes.DataView.LeadImportDataView.Route);
        }
    }
}
