﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Connector.DataView
{
    public class DataViewManagement
    {
        public DataViewConnector DataView { get; set; }

        public DataViewManagement(IRestClient client, ApiToken token)
        {
            DataView = new DataViewConnector(client, token);
        }
    }
}
