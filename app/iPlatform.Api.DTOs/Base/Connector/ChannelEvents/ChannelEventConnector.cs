﻿
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;
using iPlatform.Api.DTOs.Admin.ChannelEvents;

namespace iPlatform.Api.DTOs.Base.Connector.ChannelEvents
{
    public class ChannelEventConnector : BaseConnector
    {
        private readonly int _channelEventId;

        public ChannelEventConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            _channelEventId = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ChannelEvent.CreateGetById(id);
        }

        public GETResponseDto<ChannelEventDto> Get()
        {
            return base.Get<ChannelEventDto>(GetByIdUrl(_channelEventId));
        }
        public PUTResponseDto<int> Delete()
        {
            return Put<int>(SystemRoutes.ChannelEvent.CreateDeleteById(_channelEventId));
        }

        public PUTResponseDto<int> Edit(EditChannelEventDto editChannelEventDto)
        {
            return Put<EditChannelEventDto, int>(editChannelEventDto, SystemRoutes.ChannelEvent.CreatePutById(editChannelEventDto.Id));
        }

    }
}
