﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.ChannelEvents
{
    public class ChannelEventManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public ChannelEventManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            ChannelEvents = new ChannelEventsConnector(client, token);

        }

        public ChannelEventsConnector ChannelEvents { get; private set; }

        public ChannelEventConnector ChannelEvent(int id)
        {
            return new ChannelEventConnector(id, client, token);
        }
    }
}