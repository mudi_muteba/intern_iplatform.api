﻿
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;
using iPlatform.Api.DTOs.Admin.ChannelEvents;

namespace iPlatform.Api.DTOs.Base.Connector.ChannelEvents
{
    public class ChannelEventsConnector : BaseConnector
    {
        public ChannelEventsConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
           return SystemRoutes.ChannelEvent.CreateGetById(id);
        }


        public POSTResponseDto<PagedResultDto<ChannelEventDto>> Search(SearchChannelEventsDto searchChannelEventsDto)
        {
            return Post<SearchChannelEventsDto, PagedResultDto<ChannelEventDto>>(searchChannelEventsDto, SystemRoutes.ChannelEvent.Search.Route);
        }

        public POSTResponseDto<int> Create(CreateChannelEventDto createChannelEventDto)
        {
            return Post<CreateChannelEventDto, int>(createChannelEventDto, SystemRoutes.ChannelEvent.Post.Route);
        }

        public POSTResponseDto<bool> SaveMultiple(SaveChannelEventsDto saveChannelEventsDto)
        {
            return Post<SaveChannelEventsDto, bool>(saveChannelEventsDto, SystemRoutes.ChannelEvent.SaveMultiple.Route);
        }

        public GETResponseDto<PagedResultDto<ChannelEventDto>> Get()
        {
            return Get<PagedResultDto<ChannelEventDto>>(SystemRoutes.ChannelEvent.Get.Route);
        }

        public GETResponseDto<PagedResultDto<ChannelEventDto>> GetAllPaginated(int pageNumber, int pageSize)
        {
            var url = SystemRoutes.ChannelEvent.CreateGetWithPaginationUrl(pageNumber, pageSize);
            return Get<PagedResultDto<ChannelEventDto>>(url);
        }

        public GETResponseDto<ListResultDto<ChannelEventDto>> GetAll()
        {
            return Get<ListResultDto<ChannelEventDto>>(SystemRoutes.ChannelEvent.GetNoPagination.Route);
        }
    }
}
