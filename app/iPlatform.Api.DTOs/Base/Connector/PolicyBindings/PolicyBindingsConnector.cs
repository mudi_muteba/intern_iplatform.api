﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.PolicyBindings;
using RestSharp;
using iPlatform.Api.DTOs.Reports.Base;

namespace iPlatform.Api.DTOs.Base.Connector.PolicyBindings
{
    public class PolicyBindingsConnector : BaseConnector
    {
        public PolicyBindingsConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.PaymentDetails.CreateGetById(id);
        }

        public POSTResponseDto<PagedResultDto<PolicyBindingListDto>> Search(SearchPolicyBindingDto searchPolicyBindingDto)
        {
            return Post<SearchPolicyBindingDto, PagedResultDto<PolicyBindingListDto>>(searchPolicyBindingDto,
                SystemRoutes.PolicyBinding.Search.Route);
        }

        public POSTResponseDto<int> Save(SavePolicyBindingDto savePolicyBindingDto)
        {
            return Post<SavePolicyBindingDto, int>(savePolicyBindingDto, SystemRoutes.PolicyBinding.Save.Route);
        }

        public POSTResponseDto<PolicyBindingDetailsDto> GetByQuote(GetPolicyBindingByQuoteDto getPolicyBindingByQuoteDto)
        {
            return Post<GetPolicyBindingByQuoteDto, PolicyBindingDetailsDto>(getPolicyBindingByQuoteDto, SystemRoutes.PolicyBinding.GetByQuoteId.Route);
        }

        public POSTResponseDto<int> GetFirstAsset(GetPolicyBindingFirstAssetDto getPolicyBindingFirstAssetDto)
        {
            return Post<GetPolicyBindingFirstAssetDto, int>(getPolicyBindingFirstAssetDto, SystemRoutes.PolicyBinding.GetFirstAsset.Route);
        }
        public POSTResponseDto<bool> SaveAnswers(SavePolicyBindingAnswerDto savePolicyBindingAnswerDto)
        {
            return Post<SavePolicyBindingAnswerDto, bool>(savePolicyBindingAnswerDto, SystemRoutes.PolicyBinding.SaveAnswers.Route);
        }
        public POSTResponseDto<PolicyBindingNavigationInfoDto> GetNavigation(GetPolicyBindingNavigationDto getPolicyBindingNavigationDto)
        {
            return Post<GetPolicyBindingNavigationDto, PolicyBindingNavigationInfoDto>(getPolicyBindingNavigationDto, SystemRoutes.PolicyBinding.GetNavigation.Route);
        }

        public POSTResponseDto<bool> ValidateConfiguration(ValidatePolicyBindingConfigurationDto validatePolicyBindingConfigurationDto)
        {
            return Post<ValidatePolicyBindingConfigurationDto, bool>(validatePolicyBindingConfigurationDto, SystemRoutes.PolicyBinding.ValidateConfiguration.Route);
        }

        public POSTResponseDto<SubmitPolicyBindingResponseDto> Submit(SubmitPolicyBindingDto submitPolicyBindingDto)
        {
            return Post<SubmitPolicyBindingDto, SubmitPolicyBindingResponseDto>(submitPolicyBindingDto, SystemRoutes.PolicyBinding.Submit.Route);
        }

        public POSTResponseDto<ReportEmailResponseDto> SendEmailNotification(PolicyBindingNotificationDto policyBindingNotificationDto)
        {
            return Post<PolicyBindingNotificationDto, ReportEmailResponseDto>(policyBindingNotificationDto, SystemRoutes.PolicyBinding.PolicyBindingNotification.Route);
        }
    }
}