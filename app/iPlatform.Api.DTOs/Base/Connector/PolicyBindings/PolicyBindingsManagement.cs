using iPlatform.Api.DTOs.Base.Connector.Payments;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.PolicyBindings
{
    public class PolicyBindingsManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public PolicyBindingsManagement(IRestClient client, ApiToken token = null)
        {
            this.client = client;
            this.token = token;
            PolicyBindings = new PolicyBindingsConnector(client, token);
        }

        public PolicyBindingsConnector PolicyBindings { get; private set; }
    }
}