﻿using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;

using RestSharp;


namespace iPlatform.Api.DTOs.Base.Connector.Admin.EmailCommunicationSettings
{
    public class EmailCommunicationSettingsConnector : BaseConnector
    {
        private readonly int id;

        public EmailCommunicationSettingsConnector(IRestClient client, ApiToken token = null) : base(client, token) { }

        public EmailCommunicationSettingsConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            this.id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.EmailCommunicationSettings.CreateGetById(id);
        }

        public GETResponseDto<ListResultDto<EmailCommunicationSettingDto>> GetAll()
        {
            return Get<ListResultDto<EmailCommunicationSettingDto>>(SystemRoutes.EmailCommunicationSettings.Get.Route);
        }

        public GETResponseDto<ListResultDto<EmailCommunicationSettingDto>> GetById()
        {
            return Get<ListResultDto<EmailCommunicationSettingDto>>(SystemRoutes.EmailCommunicationSettings.GetById.Route);
        }

        public POSTResponseDto<int> CreateMultiple(CreateMultipleEmailCommunicationSettingsDto dto)
        {
            return Post<CreateMultipleEmailCommunicationSettingsDto, int>(dto, SystemRoutes.EmailCommunicationSettings.Post.Route);
        }
    }
}
