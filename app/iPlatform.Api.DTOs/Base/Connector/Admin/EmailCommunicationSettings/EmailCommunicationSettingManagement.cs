﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Admin.EmailCommunicationSettings
{
    public class EmailCommunicationSettingManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public EmailCommunicationSettingManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;

            EmailCommunicationSettings = new EmailCommunicationSettingsConnector(client, token);
        }

        public EmailCommunicationSettingsConnector EmailCommunicationSettings { get; private set; }
    }
}