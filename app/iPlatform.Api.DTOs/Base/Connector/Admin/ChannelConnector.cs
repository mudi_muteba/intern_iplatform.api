﻿using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Reports.Base;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Admin
{
    public class ChannelConnector : BaseConnector
    {
        private readonly int _ChannelId;

        public ChannelConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _ChannelId = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Channels.CreateGetById(id);
        }

        public GETResponseDto<ListChannelDto> Get()
        {
            return base.Get<ListChannelDto>(_ChannelId);
        }

        public PUTResponseDto<int> Edit(EditChannelDto editChannelDto)
        {
            return Put<EditChannelDto, int>(editChannelDto, SystemRoutes.Channels.CreatePutById(editChannelDto.Id));
        }

        public POSTResponseDto<ReportEmailResponseDto> NotifyChannelNotification(ChannelPublishingNotificationDto criteria)
        {
            return Post<ChannelPublishingNotificationDto, ReportEmailResponseDto>(criteria, SystemRoutes.Channels.CreatePublishChannelNotification(criteria.ChannelId));
        }

        public POSTResponseDto<ReportEmailResponseDto> NotifyAgencyApplicationNotification(PublishingAgencyApplicationNotificationDto criteria)
        {
            return Post<PublishingAgencyApplicationNotificationDto, ReportEmailResponseDto>(criteria, SystemRoutes.Channels.CreatePublishAgencyApplicationNotification(criteria.ChannelId));
        }
    }
}