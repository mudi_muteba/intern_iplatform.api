﻿using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Campaigns;
using RestSharp;
using System;

namespace iPlatform.Api.DTOs.Base.Connector.Admin
{
    public class ChannelPermissionsConnector : BaseConnector
    {
        public ChannelPermissionsConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Permissions.CreateGetById(id);
        }

        public POSTResponseDto<int> Create(CreateChannelPermissionDto dto)
        {
            return Post<CreateChannelPermissionDto, int>(dto, SystemRoutes.Permissions.Post.Route);
        }

        public GETResponseDto<ListResultDto<ChannelPermissionDto>> GetByChannelId(Guid ChannelId)
        {
            return Get<ListResultDto<ChannelPermissionDto>>(SystemRoutes.Permissions.CreateGetByChannelId(ChannelId));
        }

        public GETResponseDto<PagedResultDto<ChannelPermissionDto>> GetAll()
        {
            return Get<PagedResultDto<ChannelPermissionDto>>(SystemRoutes.Permissions.Get.Route);
        }
    }
}