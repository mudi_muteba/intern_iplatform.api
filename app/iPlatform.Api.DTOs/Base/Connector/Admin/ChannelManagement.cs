using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Admin
{
    public class ChannelManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public ChannelManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;

            Channels = new ChannelsConnector(client, token);
            Permissions = new ChannelPermissionsConnector(client, token);
        }

        public ChannelsConnector Channels { get; private set; }
        public ChannelPermissionsConnector Permissions { get; private set; }

        public ChannelConnector Channel(int id)
        {
            return new ChannelConnector(id, client, token);
        }

        public ChannelPermissionConnector Permission(int id)
        {
            return new ChannelPermissionConnector(id, client, token);
        }
    }
}