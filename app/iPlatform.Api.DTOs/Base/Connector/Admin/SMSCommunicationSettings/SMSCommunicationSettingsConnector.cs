﻿using iPlatform.Api.DTOs.Admin.SMSCommunicationSettings;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;

using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Admin.SMSCommunicationSettings
{
    public class SMSCommunicationSettingsConnector : BaseConnector
    {
        private readonly int id;

        public SMSCommunicationSettingsConnector(IRestClient client, ApiToken token = null) : base(client, token) { }

        public SMSCommunicationSettingsConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            this.id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.SMSCommunicationSettings.CreateGetById(id);
        }

        public GETResponseDto<ListResultDto<SMSCommunicationSettingDto>> GetAll()
        {
            return Get<ListResultDto<SMSCommunicationSettingDto>>(SystemRoutes.SMSCommunicationSettings.Get.Route);
        }

        public GETResponseDto<ListResultDto<SMSCommunicationSettingDto>> GetById()
        {
            return Get<ListResultDto<SMSCommunicationSettingDto>>(SystemRoutes.SMSCommunicationSettings.GetById.Route);
        }

        public POSTResponseDto<int> CreateMultiple(CreateMultipleSMSCommunicationSettingsDto dto)
        {
            return Post<CreateMultipleSMSCommunicationSettingsDto, int>(dto, SystemRoutes.SMSCommunicationSettings.Post.Route);
        }
    }
}
