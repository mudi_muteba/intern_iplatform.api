﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Admin.SMSCommunicationSettings
{
    public class SMSCommunicationSettingManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public SMSCommunicationSettingManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;

            SMSCommunicationSettings = new SMSCommunicationSettingsConnector(client, token);
        }

        public SMSCommunicationSettingsConnector SMSCommunicationSettings { get; private set; }
    }
}