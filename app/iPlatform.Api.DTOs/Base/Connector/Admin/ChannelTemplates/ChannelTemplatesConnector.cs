﻿using iPlatform.Api.DTOs.Admin.ChannelTemplates;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;

using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Admin.ChannelTemplates
{
    public class ChannelTemplatesConnector : BaseConnector
    {
        private readonly int id;

        public ChannelTemplatesConnector(IRestClient client, ApiToken token = null) : base(client, token) { }

        public ChannelTemplatesConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            this.id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ChannelTemplates.CreateGetById(id);
        }

        public GETResponseDto<ListResultDto<ChannelTemplateDto>> GetAll()
        {
            return Get<ListResultDto<ChannelTemplateDto>>(SystemRoutes.ChannelTemplates.Get.Route);
        }

        public GETResponseDto<ListResultDto<ChannelTemplateDto>> GetById()
        {
            return Get<ListResultDto<ChannelTemplateDto>>(SystemRoutes.ChannelTemplates.GetById.Route);
        }

        public POSTResponseDto<int> CreateMultiple(CreateMultipleChannelTemplatesDto dto)
        {
            return Post<CreateMultipleChannelTemplatesDto, int>(dto, SystemRoutes.ChannelTemplates.Post.Route);
        }
    }
}