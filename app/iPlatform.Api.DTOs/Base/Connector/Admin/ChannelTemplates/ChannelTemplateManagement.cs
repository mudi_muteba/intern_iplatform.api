﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Admin.ChannelTemplates
{
    public class ChannelTemplateManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public ChannelTemplateManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;

            ChannelTemplates = new ChannelTemplatesConnector(client, token);
        }

        public ChannelTemplatesConnector ChannelTemplates { get; private set; }
    }
}