﻿using iPlatform.Api.DTOs.Admin.ChannelPermissions;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Admin
{
    public class ChannelPermissionConnector : BaseConnector
    {
        private readonly int id;

        public ChannelPermissionConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            this.id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Permissions.CreateGetById(id);
        }

        public GETResponseDto<ChannelPermissionDto> Get()
        {
            return base.Get<ChannelPermissionDto>(id);
        }

        public PUTResponseDto<int> Edit(EditChannelPermissionDto dto)
        {
            return Put<EditChannelPermissionDto, int>(dto, SystemRoutes.Permissions.CreatePutById(dto.Id));
        }

        public PUTResponseDto<int> Disable()
        {
            return Put<int>(SystemRoutes.Permissions.CreateDisableById(id));
        }
    }
}