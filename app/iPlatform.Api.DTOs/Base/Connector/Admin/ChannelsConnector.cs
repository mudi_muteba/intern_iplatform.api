﻿using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Admin
{
    public class ChannelsConnector : BaseConnector
    {
        public ChannelsConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Channels.CreateGetById(id);
        }

        public POSTResponseDto<int> Create(CreateChannelDto createChannelDto)
        {
            return Post<CreateChannelDto, int>(createChannelDto, SystemRoutes.Channels.Post.Route);
        }

        public GETResponseDto<PagedResultDto<ListChannelDto>> GetAll()
        {
            return Get<PagedResultDto<ListChannelDto>>(SystemRoutes.Channels.Get.Route);
        }

        public GETResponseDto<ListResultDto<ListChannelDto>> GetAllNoPagination()
        {
            return Get<ListResultDto<ListChannelDto>>(SystemRoutes.Channels.GetNoPagination.Route);
        }

        public POSTResponseDto<PagedResultDto<ListChannelDto>> Search(SearchChannelsDto searchChannelsDto)
        {
            return Post<SearchChannelsDto, PagedResultDto<ListChannelDto>>(searchChannelsDto, SystemRoutes.Channels.Search.Route);
        }

        public GETResponseDto<ListResultDto<ListChannelDto>> GetAllWithNoVehicleGuideSetting()
        {
            return Get<ListResultDto<ListChannelDto>>(SystemRoutes.Channels.GetAllWithNoVehicleGuideSetting.Route);
        }
    }
}