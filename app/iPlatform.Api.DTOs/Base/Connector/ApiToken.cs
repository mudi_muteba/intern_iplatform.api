﻿namespace iPlatform.Api.DTOs.Base.Connector
{
    public class ApiToken
    {
        public string Token { get; private set; }

        public ApiToken(string token)
        {
            Token = token;
        }
    }
}