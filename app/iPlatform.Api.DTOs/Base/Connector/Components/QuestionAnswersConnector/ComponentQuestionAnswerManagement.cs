﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Connector.Components.QuestionAnswersConnector
{
    public class ComponentQuestionAnswerManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public ComponentQuestionAnswerManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            this.ComponentQuestionAnswers = new ComponentQuestionAnswersConnector(client, token);
        }

        public ComponentQuestionAnswersConnector ComponentQuestionAnswers { get; private set; }

        public ComponentQuestionAnswerConnector ComponentQuestionAnswer(int id)
        {
            return new ComponentQuestionAnswerConnector(id, client, token);
        }
    }
}
