﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;

namespace iPlatform.Api.DTOs.Base.Connector.Components.QuestionAnswersConnector
{
    public class ComponentQuestionAnswersConnector : BaseConnector
    {
        public ComponentQuestionAnswersConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ComponentQuestionAnswerRoutes.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateComponentQuestionAnswer(CreateComponentQuestionAnswerDto createComponentQuestionAnswersDto)
        {
            return Post<CreateComponentQuestionAnswerDto, int>(createComponentQuestionAnswersDto, SystemRoutes.ComponentQuestionAnswerRoutes.Post.Route);
        }

        public POSTResponseDto<int> CreateComponentQuestionAnswers(SaveComponentQuestionAnswersDto saveComponentQuestionAnswersDto)
        {
            return Post<SaveComponentQuestionAnswersDto, int>(saveComponentQuestionAnswersDto, SystemRoutes.ComponentQuestionAnswerRoutes.PostMultiple.Route);
        }

		public POSTResponseDto<int> SaveComponentQuestionAnswers(SaveComponentQuestionAnswersDto saveComponentQuestionAnswersDto)
		{
		  return Post<SaveComponentQuestionAnswersDto, int>(saveComponentQuestionAnswersDto, SystemRoutes.ComponentQuestionAnswerRoutes.Post.Route);
		}

    }
}
