﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Components.QuestionAnswersConnector
{
    public class ComponentQuestionAnswerConnector : BaseConnector
    {
        public ComponentQuestionAnswerConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _Id = id;
        }

        private readonly int _Id;

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ComponentQuestionAnswerRoutes.CreateGetById(id);
        }

        public PUTResponseDto<int> EditComponentQuestionAnswer(EditComponentQuestionAnswerDto editComponentQuestionAnswerDto)
        {
            return Put<EditComponentQuestionAnswerDto, int>(editComponentQuestionAnswerDto, SystemRoutes.ComponentQuestionAnswerRoutes.PutById(editComponentQuestionAnswerDto.Id));
        }

        public PUTResponseDto<int> Disable()
        {
            return Put<int>(SystemRoutes.ComponentQuestionAnswerRoutes.DisableById(_Id));
        }
    }
}
