﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Connector.Components.DefinitionsConnector
{
    public class ComponentDefinitionManagement 
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public ComponentDefinitionManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            this.ComponentDefinitions = new ComponentDefinitionsConnector(client, token);
        }

        public ComponentDefinitionsConnector ComponentDefinitions { get; private set; }

        public ComponentDefinitionConnector ComponentDefinitionConnector(int id)
        {
            return new ComponentDefinitionConnector(id, client, token);
        }
    }
}
