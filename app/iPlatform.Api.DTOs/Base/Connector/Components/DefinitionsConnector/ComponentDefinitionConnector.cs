﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Components.ComponentDefinitions;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Components.DefinitionsConnector
{
    public class ComponentDefinitionConnector : BaseConnector
    {
        public ComponentDefinitionConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _Id = id;
        }

        private readonly int _Id;

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ComponentDefinitionRoutes.CreateGetById(id);
        }

        public PUTResponseDto<int> Disable()
        {
            return Put<int>(SystemRoutes.ComponentDefinitionRoutes.DisableById(_Id));
        }

        public PUTResponseDto<int> EditComponentDefinition(EditComponentDefinitionDto editComponentDefinitionDto)
        {
            return Put<EditComponentDefinitionDto, int>(editComponentDefinitionDto, SystemRoutes.ComponentDefinitionRoutes.PutById(editComponentDefinitionDto.Id));
        }
    }
}
