﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentDefinitions;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;

namespace iPlatform.Api.DTOs.Base.Connector.Components.DefinitionsConnector
{
    public class ComponentDefinitionsConnector : BaseConnector
    {
        public ComponentDefinitionsConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ComponentDefinitionRoutes.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateComponentDefinition(CreateComponentDefinitionDto createComponentDefinitionsDto)
        {
            return Post<CreateComponentDefinitionDto, int>(createComponentDefinitionsDto, SystemRoutes.ComponentDefinitionRoutes.Post.Route);
        }

        public POSTResponseDto<ListResultDto<SearchComponentQuestionDefinitionAnswerResultDto>> SearchDefinitionsAnswers(SearchComponentDefinitionAnswersDto searchComponentDefinitionAnswersDto)
        {
            return Post<SearchComponentDefinitionAnswersDto, ListResultDto<SearchComponentQuestionDefinitionAnswerResultDto>>(searchComponentDefinitionAnswersDto, SystemRoutes.ComponentDefinitionRoutes.SearchDefinitionsAnswers.Route);
        }
    }
}
