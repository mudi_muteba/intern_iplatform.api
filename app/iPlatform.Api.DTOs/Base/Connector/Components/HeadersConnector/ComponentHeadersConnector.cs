﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentHeaders;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Components.HeadersConnector
{
    public class ComponentHeadersConnector : BaseConnector
    {
        public ComponentHeadersConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ComponentHeaderRoutes.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateComponentHeader(CreateComponentHeaderDto createComponentHeaderDto)
        {
            return Post<CreateComponentHeaderDto, int>(createComponentHeaderDto, SystemRoutes.ComponentHeaderRoutes.Post.Route);
        }
    }
}
