﻿using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentHeaders;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Connector.Components.HeadersConnector
{
    public class ComponentHeaderConnector : BaseConnector
    {
        public ComponentHeaderConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _Id = id;
        }

        private readonly int _Id;

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ComponentHeaderRoutes.CreateGetById(id);
        }

        public PUTResponseDto<int> Disable()
        {
            return Put<int>(SystemRoutes.ComponentHeaderRoutes.DisableById(_Id));
        }

        public PUTResponseDto<int> EditComponentHeader(EditComponentHeaderDto editComponentHeaderDto)
        {
            return Put<EditComponentHeaderDto, int>(editComponentHeaderDto, SystemRoutes.ComponentHeaderRoutes.PutById(editComponentHeaderDto.Id));
        }
    }
}
