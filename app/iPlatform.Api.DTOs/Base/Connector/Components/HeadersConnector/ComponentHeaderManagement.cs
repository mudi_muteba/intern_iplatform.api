﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Connector.Components.HeadersConnector
{
    public class ComponentHeaderManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public ComponentHeaderManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            this.ComponentHeaders = new ComponentHeadersConnector(client, token);
        }

        public ComponentHeadersConnector ComponentHeaders { get; private set; }

        public ComponentHeaderConnector ComponentHeader(int id)
        {
            return new ComponentHeaderConnector(id, client, token);
        }
    }
}
