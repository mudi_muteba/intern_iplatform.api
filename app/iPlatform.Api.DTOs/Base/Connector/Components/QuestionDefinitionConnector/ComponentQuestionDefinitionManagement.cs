﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Connector.Components.QuestionDefinitionConnector
{
    public class ComponentQuestionDefinitionManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public ComponentQuestionDefinitionManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            this.ComponentQuestionDefinitions = new ComponentQuestionDefinitionsConnector(client, token);
 
        }

        public ComponentQuestionDefinitionsConnector ComponentQuestionDefinitions { get; private set; }
    

        public ComponentQuestionDefinitionConnector ComponentQuestionDefinition(int id)
        {
            return new ComponentQuestionDefinitionConnector(id, client, token);
        }
    }
}
