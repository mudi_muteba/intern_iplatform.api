﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using iPlatform.Api.DTOs.Components.ComponentQuestionAnswers;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;

namespace iPlatform.Api.DTOs.Base.Connector.Components.QuestionDefinitionConnector
{
    public class ComponentQuestionDefinitionConnector : BaseConnector
    {
        public ComponentQuestionDefinitionConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _Id = id;
        }

        private readonly int _Id;

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ComponentQuestionDefinitionsRoutes.CreateGetById(id);
        }

        public POSTResponseDto<ListResultDto<SearchComponentQuestionDefinitionResultDto>> CreateComponentHeader(SaveComponentQuestionAnswersDto saveComponentQuestionAnswersDto)
        {
            return Post<SaveComponentQuestionAnswersDto, ListResultDto<SearchComponentQuestionDefinitionResultDto>>(saveComponentQuestionAnswersDto, SystemRoutes.ComponentQuestionDefinitionsRoutes.SearchDefinitions.Route);
        }
    }
}
