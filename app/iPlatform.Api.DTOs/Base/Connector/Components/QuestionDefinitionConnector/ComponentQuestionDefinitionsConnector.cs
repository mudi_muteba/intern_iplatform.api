﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Components.ComponentQuestionDefinitions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Connector.Components.QuestionDefinitionConnector
{
    public class ComponentQuestionDefinitionsConnector : BaseConnector
    {
        public ComponentQuestionDefinitionsConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
            
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ComponentQuestionDefinitionsRoutes.CreateGetById(id);
        }

        public POSTResponseDto<ListResultDto<SearchComponentQuestionDefinitionResultDto>> SearchDefinitions(SearchComponentDefinitionsDto searchComponentDefinitionsDto)
        {
            return Post<SearchComponentDefinitionsDto, ListResultDto<SearchComponentQuestionDefinitionResultDto>>(searchComponentDefinitionsDto, SystemRoutes.ComponentQuestionDefinitionsRoutes.SearchDefinitions.Route);
        }
    }
}
