﻿using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SettingsiRateUser
{
    public class SettingsiRateUserConnector : BaseConnector
    {
        private readonly int m_Id;

        public SettingsiRateUserConnector(int id, IRestClient client, ApiToken token = null) : base(client, token)
        {
            m_Id = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.SettingsiRateUser.CreateGetById(id);
        }

        public GETResponseDto<SettingsiRateUserDto> Get()
        {
            return base.Get<SettingsiRateUserDto>(GetByIdUrl(m_Id));
        }

        public PUTResponseDto<int> Edit(EditSettingsiRateUserDto editSettingsiRateUserDto)
        {
            return Put<EditSettingsiRateUserDto, int>(editSettingsiRateUserDto, SystemRoutes.SettingsiRateUser.CreatePutById(editSettingsiRateUserDto.Id));
        }

        public PUTResponseDto<int> Delete()
        {
            return Put<int>(SystemRoutes.SettingsiRateUser.CreateDeleteById(m_Id));
        }
    }
}
