﻿using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SettingsiRateUser
{
    public class SettingsiRateUsersConnector : BaseConnector
    {
        public SettingsiRateUsersConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.SettingsiRateUser.CreateGetById(id);
        }

        public POSTResponseDto<int> Create(CreateSettingsiRateUserDto createSettingsiRateUserDto)
        {
            return Post<CreateSettingsiRateUserDto, int>(createSettingsiRateUserDto, SystemRoutes.SettingsiRateUser.Post.Route);
        }

        public POSTResponseDto<bool> SaveMultiple(SaveMultipleSettingsiRateUserDto saveMultipleSettingsiRateUserDto)
        {
            return Post<SaveMultipleSettingsiRateUserDto, bool>(saveMultipleSettingsiRateUserDto, SystemRoutes.SettingsiRateUser.PostMultiple.Route);
        }

        public PagedResultDto<ListSettingsiRateUserDto> GetAll()
        {
            var result = Get<PagedResultDto<ListSettingsiRateUserDto>>(SystemRoutes.SettingsiRateUser.Get.Route);
            return result.Response;
        }

        public GETResponseDto<PagedResultDto<ListSettingsiRateUserDto>> GetAllPaginated(int pageNumber, int pageSize)
        {
            var url = SystemRoutes.SettingsiRateUser.CreateGetWithPaginationUrl(pageNumber, pageSize);
            return Get<PagedResultDto<ListSettingsiRateUserDto>>(url);
        }

        public POSTResponseDto<PagedResultDto<ListSettingsiRateUserDto>> Search(SearchSettingsiRateUserDto searchSettingsiRateUserDto)
        {
            return Post<SearchSettingsiRateUserDto, PagedResultDto<ListSettingsiRateUserDto>>(searchSettingsiRateUserDto, SystemRoutes.SettingsiRateUser.Search.Route);
        }
    }
}
