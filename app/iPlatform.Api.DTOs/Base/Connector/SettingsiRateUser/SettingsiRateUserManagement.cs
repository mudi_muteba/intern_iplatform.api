﻿using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.SettingsiRateUser
{
    public class SettingsiRateUserManagement
    {
        private readonly IRestClient m_Client;
        private readonly ApiToken m_Token;

        public SettingsiRateUserManagement(IRestClient client, ApiToken token)
        {
            this.m_Client = client;
            this.m_Token = token;
            SettingsiRateUsers = new SettingsiRateUsersConnector(client, token);

        }

        public SettingsiRateUsersConnector SettingsiRateUsers { get; private set; }

        public SettingsiRateUserConnector SettingsiRateUser(int id)
        {
            return new SettingsiRateUserConnector(id, m_Client, m_Token);
        }
    }
}