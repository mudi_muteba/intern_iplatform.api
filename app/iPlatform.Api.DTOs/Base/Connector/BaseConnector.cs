using System;
using System.Net;
using Common.Logging;
using iPlatform.Api.DTOs.Base.Connector.Handlers;
using iPlatform.Api.DTOs.Base.Connector.Plumbing;
using iPlatform.Api.DTOs.Base.Helper;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users.Authentication;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector
{
    public abstract class BaseConnector
    {
        private static readonly ILog log = LogManager.GetLogger<BaseConnector>();

        [ThreadStatic] internal static string ExistingToken;
        protected readonly IRestClient Client;

        public void SetToken(string apiToken)
        {
            ExistingToken = apiToken;
        }

        protected BaseConnector(IRestClient client, ApiToken token = null)
        {
            Client = client;
            var contentType = "application/json";
            (Client as RestClient).RemoveHandler(contentType);
            (Client as RestClient).AddHandler(contentType, new RestSharpDataContractJsonDeserializer());
            if (string.IsNullOrEmpty(ExistingToken))
                ExistingToken = token == null ? string.Empty : token.Token;
        }

        protected IRestResponse Create<TRequest>(string postUrl, TRequest request)
        {
            var restRequest = new RestRequest(postUrl, Method.POST);
            var authenticaterequest = (object)request as AuthenticationRequestDto;
            if (authenticaterequest != null)
                log.InfoFormat("email : {0}", authenticaterequest.Email);

            AddHeaders(restRequest);
            restRequest.AddBody(request);

            var context = new CallContext(Client.BuildUri(restRequest), Method.POST);

            var response = Client.Execute(restRequest);

            log.InfoFormat(response.StatusDescription);
            log.InfoFormat(response.ErrorMessage);
            log.InfoFormat("Creating a POST to '{0}', response was {1}", context.UrlInformation(), response.StatusCode);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
                return response;

            new DefaultResponseHandler(context)
                .Handle(response);

            return response;
        }

        private static void AddHeaders(RestRequest request)
        {
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
        }

        protected POSTResponseDto<TReturn> Post<TDto, TReturn>(TDto dto, string postUrl)
        {
            var handler = new POSTHandler<TReturn>(Client, ExistingToken);
            return handler.Execute(dto, postUrl);
        }
        
        protected CustomAppResponseDto<TReturn> PostCustomApp<TDto, TReturn>(TDto dto, string postUrl)
        {
            var handler = new POSTCustomAppHandler<TReturn>(Client, ExistingToken);
            return handler.Execute(dto, postUrl);
        }
        
        protected DELETEResponseDto<TReturn> Delete<TDto, TReturn>(TDto dto, string postUrl)
        {
            var handler = new DELETEHandler<TReturn>(Client, ExistingToken);
            return handler.Execute(dto, postUrl);
        }
        
        protected PUTResponseDto<TReturn> Put<TDto, TReturn>(TDto dto,  string putUrl)
        {
            var handler = new PUTHandler<TReturn>(Client, ExistingToken);
            var result =handler.Execute(dto, putUrl);
            return result;
        }

        protected PUTResponseDto<TResponse> Put<TResponse>(TResponse dto, string putUrl)
        {
            var handler = new PUTHandler<TResponse>(Client, ExistingToken);
            return handler.Execute(dto, putUrl);
        }

        protected PUTResponseDto<TResponse> Put<TResponse>(string putUrl)
        {
            var handler = new PUTHandler<TResponse>(Client, ExistingToken);
            return handler.Execute(putUrl);
        }

        protected virtual GETResponseDto<TResponse> Get<TResponse>(int id)
        {
            var handler = new GETHandler<TResponse>(Client, ExistingToken);
            return handler.Execute(GetByIdUrl(id));
        }

        protected virtual GETResponseDto<TResponse> Get<TResponse>(string url)
        {
            var handler = new GETHandler<TResponse>(Client, ExistingToken);
            return handler.Execute(url);
        }

        protected LISTPagedResponseDto<TResponse> GetPagedList<TDto, TResponse>(TDto dto, string url)
        {
            var handler = new LISTPagedHandler<TResponse>(Client, ExistingToken);
            return handler.Execute(dto, url);
        }

        protected LISTPagedResponseDto<TResponse> GetPagedList<TResponse>(string url)
        {
            var handler = new LISTPagedHandler<TResponse>(Client, ExistingToken);
            return handler.Execute(url);
        }

        protected LISTResponseDto<TResponse> GetList<TDto, TResponse>(TDto dto, string url)
        {
            var handler = new LISTHandler<TResponse>(Client, ExistingToken);
            return handler.Execute(dto, url);
        }

        protected LISTResponseDto<TResponse> GetList<TResponse>(string url)
        {
            var handler = new LISTHandler<TResponse>(Client, ExistingToken);
            return handler.Execute(url);
        }

        protected abstract string GetByIdUrl(int id);
    }
}