﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Teams;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Teams
{
    public class TeamsConnector : BaseConnector
    {
        public TeamsConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Teams.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateTeam(CreateTeamDto createTeamDto)
        {
            return Post<CreateTeamDto, int>(createTeamDto, SystemRoutes.Teams.Post.Route);
        }

        public POSTResponseDto<PagedResultDto<ListTeamDto>> Search(SearchTeamDto searchTeamDto)
        {
            return Post<SearchTeamDto, PagedResultDto<ListTeamDto>>(searchTeamDto, SystemRoutes.Teams.PostSearch.Route);
        }
    }
}