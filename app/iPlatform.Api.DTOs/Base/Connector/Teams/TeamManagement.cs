using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Teams
{
    public class TeamManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public TeamManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            this.Teams = new TeamsConnector(client, token);

        }

        public TeamsConnector Teams { get; private set; }
        public TeamConnector Team(int id)
        {
            return new TeamConnector(id, client, token);
        }


    }
}