﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Teams;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Teams
{
    public class TeamConnector : BaseConnector
    {
        public TeamConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _TeamId = id;
        }

        private readonly int _TeamId;

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.Teams.CreateGetById(id);
        }

        public GETResponseDto<ListTeamDto> Get()
        {
            return base.Get<ListTeamDto>(GetByIdUrl(_TeamId));
        }

        public PUTResponseDto<int> Disable()
        {
            return Put<int>(SystemRoutes.Teams.DisableById(_TeamId));
        }

        public PUTResponseDto<int> EditTeam(EditTeamDto editTeamDto)
        {
            return Put<EditTeamDto, int>(editTeamDto, SystemRoutes.Teams.PutById(editTeamDto.Id));
        }
    }
}