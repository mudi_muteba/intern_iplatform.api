﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsHeaders
{
    public class ClaimsHeadersConnector : BaseConnector
    {
        public ClaimsHeadersConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ClaimsHeader.CreateGetById(id);
        }

        public POSTResponseDto<int> CreateClaimHeader(CreateClaimsHeaderDto createClaimsHeaderDto)
        {
            return Post<CreateClaimsHeaderDto, int>(createClaimsHeaderDto, SystemRoutes.ClaimsHeader.Post.Route);
        }

        public POSTResponseDto<PagedResultDto<ClaimsHeaderDto>> SearchClaims(
            ClaimSearchDto claimSearchDto)
        {
            return Post<ClaimSearchDto, PagedResultDto<ClaimsHeaderDto>>(claimSearchDto,
                SystemRoutes.ClaimsHeader.PostSearch.Route);
        }

        public POSTResponseDto<AcceptClaimResponseDto> AcceptClaim(AcceptClaimDto acceptClaimDto)
        {
            return Post<AcceptClaimDto, AcceptClaimResponseDto>(acceptClaimDto, SystemRoutes.ClaimsHeader.PostAccept.Route);
        }
    }
}