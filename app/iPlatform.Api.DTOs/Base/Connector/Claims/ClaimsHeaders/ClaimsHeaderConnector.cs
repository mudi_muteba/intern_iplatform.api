﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsHeaders
{
    public class ClaimsHeaderConnector : BaseConnector
    {
        private readonly int _ClaimsHeaderId;

        public ClaimsHeaderConnector(int id, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _ClaimsHeaderId = id;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ClaimsHeader.CreateGetById(id);
        }

        public GETResponseDto<ClaimsHeaderDto> Get()
        {
            return base.Get<ClaimsHeaderDto>(GetByIdUrl(_ClaimsHeaderId));
        }

        public PUTResponseDto<int> SubmitClaimsHeader(AcceptClaimDto dto)
        {
            return Put<AcceptClaimDto, int>(dto, SystemRoutes.ClaimsHeader.AcceptById(_ClaimsHeaderId));
        }

        public PUTResponseDto<int> DisableClaimsHeader(DisableClaimDto dto)
        {
            return Put<DisableClaimDto, int>(dto, SystemRoutes.ClaimsHeader.DisableById(_ClaimsHeaderId));
        }
    }
}