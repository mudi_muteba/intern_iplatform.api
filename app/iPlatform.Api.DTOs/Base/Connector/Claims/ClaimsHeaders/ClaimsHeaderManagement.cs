using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsHeaders
{
    public class ClaimsHeaderManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public ClaimsHeaderManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            ClaimsHeaders = new ClaimsHeadersConnector(client, token);

        }

        public ClaimsHeadersConnector ClaimsHeaders { get; private set; }

        public ClaimsHeaderConnector ClaimsHeader(int id)
        {
            return new ClaimsHeaderConnector(id, client, token);
        }
    }
}