﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsItems
{
    public class ClaimsItemsConnector : BaseConnector
    {
        public ClaimsItemsConnector(int claimHeaderId, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _claimHeaderId = claimHeaderId;
        }

        private readonly int _claimHeaderId;
        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ClaimsItem.CreateGetById(id);
        }

        public LISTResponseDto<ListClaimsItemDto> GetClaimsItems()
        {
            return
                base.GetList<ListClaimsItemDto>(
                    SystemRoutes.ClaimsItem.CreateGetById(_claimHeaderId));
        }

        public POSTResponseDto<int> CreateClaimsItem(CreateClaimsItemsDto createClaimsItemsDto)
        {
            return Post<CreateClaimsItemsDto, int>(createClaimsItemsDto,
                SystemRoutes.ClaimsItem.CreateGetById(_claimHeaderId));
        }

        public PUTResponseDto<int> UpdateClaimsItems(UpdateClaimsItemsDto updateClaimsItemsDto)
        {
            return Put<UpdateClaimsItemsDto, int>(updateClaimsItemsDto,
                SystemRoutes.ClaimsItem.CreateGetById(_claimHeaderId));
        }
    }
}