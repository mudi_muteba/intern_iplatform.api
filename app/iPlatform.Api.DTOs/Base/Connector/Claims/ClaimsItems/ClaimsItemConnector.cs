﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsItems
{
    public class ClaimsItemConnector : BaseConnector
    {
        private readonly int _ClaimsHeaderId;
        private readonly int _ClaimsItemId;

        public ClaimsItemConnector(int claimsHeaderId, int claimsItemId, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _ClaimsHeaderId = claimsHeaderId;
            _ClaimsItemId = claimsItemId;
        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ClaimsItem.CreateGetById(_ClaimsHeaderId, id);
        }

        public GETResponseDto<ClaimsItemDto> Get()
        {
            return base.Get<ClaimsItemDto>(GetByIdUrl(_ClaimsItemId));
        }

        public PUTResponseDto<int> DeleteClaimsItem(DisableClaimItemDto disableClaimItemDto)
        {
            return Put<DisableClaimItemDto, int>(disableClaimItemDto, SystemRoutes.ClaimsItem.DeleteGetById(_ClaimsHeaderId, _ClaimsItemId));
        }

        public POSTResponseDto<int> SaveClaimItemAnswers(SaveClaimsItemAnswersDto saveClaimsItemAnswersDto)
        {
            return Post<SaveClaimsItemAnswersDto, int>(saveClaimsItemAnswersDto, SystemRoutes.ClaimsItem.SaveAnswersById(_ClaimsHeaderId, _ClaimsItemId));
        }
    }
}