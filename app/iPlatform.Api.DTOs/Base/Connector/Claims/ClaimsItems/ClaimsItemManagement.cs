using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsItems
{
    public class ClaimsItemManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;
        private int _claimHeaderId;

        public ClaimsItemManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
        }

        public ClaimsItemManagement SetClaimHeaderId(int claimHeaderId)
        {
            _claimHeaderId = claimHeaderId;
            ClaimsItems = new ClaimsItemsConnector(_claimHeaderId, client, token);
            return this;
        }

        public ClaimsItemsConnector ClaimsItems { get; private set; }

        public ClaimsItemConnector ClaimsItem(int id)
        {
            return new ClaimsItemConnector(_claimHeaderId, id, client, token);
        }
    }
}