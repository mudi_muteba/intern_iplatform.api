using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsTypes
{
    public class ClaimsTypeManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public ClaimsTypeManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            ClaimsTypes = new ClaimsTypesConnector(client, token);
        }


        public ClaimsTypesConnector ClaimsTypes { get; private set; }

        public ClaimsTypeConnector ClaimsType(int id)
        {
            return new ClaimsTypeConnector(id, client, token);
        }
    }
}