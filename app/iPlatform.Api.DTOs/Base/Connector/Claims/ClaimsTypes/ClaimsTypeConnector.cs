﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using iPlatform.Api.DTOs.Claims.Questions;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsTypes
{
    public class ClaimsTypeConnector : BaseConnector
    {
        private readonly int _ClaimsTypeId;

        public ClaimsTypeConnector(int claimsTypeId, IRestClient client, ApiToken token = null)
            : base(client, token)
        {
            _ClaimsTypeId = claimsTypeId;
        }


        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ClaimType.CreateGetById(id);
        }

        public GETResponseDto<ClaimsTypeDetailDto> GetClaimTypeDetailsByProductId(int? productId = 0)
        {
            return Get<ClaimsTypeDetailDto>(SystemRoutes.ClaimType.CreateGetByProductId(_ClaimsTypeId, productId));
        }
    }
}