﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using iPlatform.Api.DTOs.Claims.ClaimTypes;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Claims.ClaimsTypes
{
    public class ClaimsTypesConnector : BaseConnector
    {
        public ClaimsTypesConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {

        }

        protected override string GetByIdUrl(int id)
        {
            return SystemRoutes.ClaimType.CreateGetById(id);
        }

        public GETResponseDto<List<ClaimsTypeDto>> GetClaimTypesByOrganisationId(int? id = 0)
        {
            return base.Get<List<ClaimsTypeDto>>(SystemRoutes.ClaimType.CreateGetByOrganisationId(id));
        }
    }
}