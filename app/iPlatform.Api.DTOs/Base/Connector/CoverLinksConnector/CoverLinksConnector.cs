﻿using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.CoverLinks;
using iPlatform.Api.DTOs.Insurers;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.CoverLinksConnector
{
    public class CoverLinksConnector : BaseConnector
    {
        public CoverLinksConnector(IRestClient client, ApiToken token = null) : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new System.NotImplementedException();
        }

        public GETResponseDto<ListResultDto<ListCoverLinkDto>> GetCoverLinksByChannelId(int channelId)
        {
            var url = SystemRoutes.CoverLinks.CreateGetById(channelId);
            return Get<ListResultDto<ListCoverLinkDto>>(url);
        }
     
    }

}