﻿using iPlatform.Api.DTOs.Base.Connector.Escalations;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.CoverLinksConnector
{
    public class CoverLinksManagement
    {
        private IRestClient _client;
        private ApiToken _token;

        public CoverLinksManagement(IRestClient client, ApiToken token = null)
        {
            _client = client;
            _token = token;
            CoverLinksConnector = new CoverLinksConnector(client, token);
        }

        public CoverLinksConnector CoverLinksConnector { get; set; }


    }
}