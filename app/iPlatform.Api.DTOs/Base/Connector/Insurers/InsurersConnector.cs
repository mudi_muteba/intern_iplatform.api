﻿using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Base.Infrastructure.Routing;
using iPlatform.Api.DTOs.Insurers;
using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Insurers
{
    public class InsurersConnector : BaseConnector
    {
        public InsurersConnector(IRestClient client, ApiToken token = null)
            : base(client, token)
        {
        }

        protected override string GetByIdUrl(int id)
        {
            throw new NotImplementedException();
        }

        public POSTResponseDto<PagedResultDto<ListInsurerDto>> SearchInsurers(
            InsurerSearchDto insurerSearchDto)
        {
            return Post<InsurerSearchDto, PagedResultDto<ListInsurerDto>>(insurerSearchDto,
                SystemRoutes.Insurers.PostSearch.Route);
        }


        public LISTPagedResponseDto<ListInsurerDto> GetAllInsurers()
        {
            return GetPagedList<ListInsurerDto>(SystemRoutes.Insurers.GetAll.Route);
        }

        public LISTPagedResponseDto<ListInsurerDto> GetAllInsurers(Pagination pagination)
        {
            return GetPagedList<ListInsurerDto>
                (SystemRoutes.Insurers.CreatePaginationUrl(pagination.PageNumber, pagination.PageSize));
        }
    }
}