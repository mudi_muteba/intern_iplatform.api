using RestSharp;

namespace iPlatform.Api.DTOs.Base.Connector.Insurers
{
    public class InsurerManagement
    {
        private readonly IRestClient client;
        private readonly ApiToken token;

        public InsurerManagement(IRestClient client, ApiToken token)
        {
            this.client = client;
            this.token = token;
            Insurers = new InsurersConnector(client, token);

        }

        public InsurersConnector Insurers { get; private set; }

    }
}