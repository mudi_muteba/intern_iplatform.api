﻿using System;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Base
{
    public abstract class BaseCriteria
    {
        protected BaseCriteria()
        {
            PageNumber = 1;
            PageSize = 20;
            OrderBy = new List<OrderByField>();
        }

        public int? Id;
        public Guid SystemId;
        public int PageNumber;
        public int PageSize;

        public Pagination CreatePagination()
        {
            return new Pagination(PageNumber, PageSize);
        }

        public List<OrderByField> OrderBy { get; set; }
    }
}