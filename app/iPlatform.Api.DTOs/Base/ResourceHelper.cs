﻿using System.Collections.Generic;
using System.Net;

namespace iPlatform.Api.DTOs.Base
{
    internal class ResourceHelper
    {
        public static readonly Dictionary<string, List<HttpStatusCode>> ExpectedResponses = new Dictionary
            <string, List<HttpStatusCode>>
        {
            {
                "GET", new List<HttpStatusCode>
                {
                    HttpStatusCode.OK,
                    HttpStatusCode.NotFound,
                    HttpStatusCode.InternalServerError,
                    HttpStatusCode.Unauthorized
                }
            },
            {
                "OPTIONS", new List<HttpStatusCode>
                {
                    HttpStatusCode.OK,
                    HttpStatusCode.NotFound,
                    HttpStatusCode.InternalServerError,
                    HttpStatusCode.Unauthorized
                }
            },
            {
                "POST", new List<HttpStatusCode>
                {
                    HttpStatusCode.OK,
                    HttpStatusCode.BadRequest,
                    HttpStatusCode.Conflict,
                    HttpStatusCode.InternalServerError,
                    HttpStatusCode.Unauthorized
                }
            },
            {
                "PUT", new List<HttpStatusCode>
                {
                    HttpStatusCode.OK,
                    HttpStatusCode.BadRequest,
                    HttpStatusCode.Conflict,
                    HttpStatusCode.InternalServerError,
                    HttpStatusCode.Unauthorized
                }
            },
            {
                "DELETE", new List<HttpStatusCode>
                {
                    HttpStatusCode.OK,
                    HttpStatusCode.BadRequest,
                    HttpStatusCode.InternalServerError,
                    HttpStatusCode.Unauthorized
                }
            },
        };

    }
}