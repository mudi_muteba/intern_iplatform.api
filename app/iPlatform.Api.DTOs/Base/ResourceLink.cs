﻿namespace iPlatform.Api.DTOs.Base
{
    public class ResourceLink
    {
        public ResourceLink()
        {
        }

        public ResourceLink(string rel, string href)
        {
            Rel = rel;
            Href = href;
        }

        public string Rel { get; set; }
        public string Href { get; set; }
    }
}