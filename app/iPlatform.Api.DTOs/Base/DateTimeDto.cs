﻿using iPlatform.Api.DTOs.Base.Culture;
using System;

namespace iPlatform.Api.DTOs.Base
{
    public class DateTimeDto : ICultureVisitable
    {
        public DateTimeDto()
        {
        }

        public DateTimeDto(DateTime date)
        {
            Value = date;
        }

        public DateTime? Value { get; set; }

        #region Culture Setup
        public CultureVisitor Visitor { get; set; }

        public void AcceptVisitor(CultureVisitor visitor)
        {
            this.Visitor = visitor;
        }

        public override string ToString()
        {
            return Visitor.Visit(this);
        }
        #endregion
    }
}
