using System;
using iPlatform.Api.DTOs.Base.Culture;

namespace iPlatform.Api.DTOs.Base
{
    public class AuditEventDto : ICultureAware
    {
        public AuditEventDto(){}

        public string EventName { get; set; }
        public DateTimeDto Date { get; set; }
        public string UserName { get; set; }
        public int Version { get; set; }
    }
}