﻿using System.Collections.Generic;
using ValidationMessages;

namespace iPlatform.Api.DTOs.Base
{
    public interface IValidationAvailableDto
    {
        List<ValidationErrorMessage> Validate();
    }
}