﻿using System;
using iPlatform.Api.DTOs.Base.Culture;
using iPlatform.Api.DTOs.Users;

namespace iPlatform.Api.DTOs.Base
{
    public class AuditDto : ICultureAware
    {
        public AuditDto()
        {
            CreatedBy = new UserDto();
            ModifiedBy = new UserDto();
        }

        public UserDto CreatedBy { get; set; }
        public UserDto ModifiedBy { get; set; }
        public DateTimeDto CreatedOn { get; set; }
        public DateTimeDto ModifiedOn { get; set; }
    }
}