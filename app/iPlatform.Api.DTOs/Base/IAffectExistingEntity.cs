﻿namespace iPlatform.Api.DTOs.Base
{
    public interface IAffectExistingEntity
    {
        int Id { get; }
    }
}