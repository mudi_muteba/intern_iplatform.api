﻿namespace iPlatform.Api.DTOs.Base
{
    public interface IExecutionDto
    {
        void SetContext(DtoContext context);
        DtoContext Context { get; }
    }
}