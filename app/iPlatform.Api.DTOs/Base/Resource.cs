﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace iPlatform.Api.DTOs.Base
{
    public class Resource
    {
        private List<ResourceLink> links = new List<ResourceLink>();

        public int Id { get; set; }
        public Guid SystemId { get; set; }

        public List<ResourceLink> Links
        {
            get { return links; }
            set { links = value; }
        }

        public void AddLink(ResourceLink link)
        {
            links.Add(link);
        }

        public void AddLink(string rel, string href)
        {
            if (links.Any(l => l.Rel.Equals(rel) && l.Href.Equals(href)))
                return;

            links.Add(new ResourceLink(rel, href));
        }
    }
}