﻿
namespace iPlatform.Api.DTOs.Base
{
    public class DtoContext
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public string RequestSource { get; set; }

        private DtoContext() { }

        public DtoContext(int userId, string username, string token)
        {
            UserId = userId;
            Username = username;
            Token = token;
        }

        public static DtoContext NoContext()
        {
            return new DtoContext();
        }
    }
}