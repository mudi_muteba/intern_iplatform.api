﻿using iPlatform.Api.DTOs.Base.Culture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base
{
    public class MoneyDto : ICultureVisitable
    {
        public MoneyDto() { }

        public MoneyDto(decimal value)
        {
            this.Value = value;
        }

        public MoneyDto(decimal value, int currencyid)
        {
            this.Value = value;
        }

        public MoneyDto(decimal value, CultureVisitor visitor)
        {
            this.Value = value;
            this.Visitor = visitor;
        }

        public decimal Value { get; set; }




        #region Culture Setup
        public CultureVisitor Visitor { get; set; }

        public void AcceptVisitor(CultureVisitor visitor)
        {
            this.Visitor = visitor;
        }

        public override string ToString()
        {
            return Visitor.Visit(this);
        }
        #endregion

    }
}
