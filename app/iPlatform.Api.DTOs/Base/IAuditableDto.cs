﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Base
{
    public interface IAuditableDto
    {
        List<AuditEventDto> Events { get; }
    }
}