﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Base
{
    public interface IMultiChannelDto
    {
        List<int> Channels { get; set; }
    }
}