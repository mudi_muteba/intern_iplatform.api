﻿using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Base
{
    public class PagedResultDto<TDto> : IPagedResultDto
    {
        public PagedResultDto()
        {
            Results = new List<TDto>();
        }

        public List<TDto> Results { get; set; }
        public int PageNumber { get; set; }
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
    }
}