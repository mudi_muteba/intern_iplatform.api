﻿namespace iPlatform.Api.DTOs.Base
{
    public enum OrderByDirection
    {
        Ascending = 0,
        Descending = 1
    }

    public class OrderByField
    {
        public OrderByField()
        {
        }

        public OrderByField(string fieldName)
        {
            FieldName = fieldName;
        }

        public string FieldName { get; set; }
        public OrderByDirection Direction { get; set; }
    }
}