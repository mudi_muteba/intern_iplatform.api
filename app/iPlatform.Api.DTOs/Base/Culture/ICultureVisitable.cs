﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Culture
{
    public interface ICultureVisitable
    {
        void AcceptVisitor(CultureVisitor visitor);

        CultureVisitor Visitor { get; }
    }
}
