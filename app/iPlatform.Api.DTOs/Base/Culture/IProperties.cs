﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Culture
{
    public interface IProperties
    {
        string Name { get; }
        List<string> VisitableProperties { get; }
        List<string> AwareProperties { get; }
        List<string> GenericProperties { get; }
    }
}
