﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Culture
{
    public class CultureParameter
    {
        public CultureParameter() {}

        public CultureParameter(int currencyId, string dateFormat, int languageId, int countryId)
        {
            this.CurrencyId = currencyId;
            this.DateFormat = dateFormat;
            this.LanguageId = LanguageId;
            this.CountryId = countryId;
        }

        public int CurrencyId { get; set; }
        public string DateFormat { get; set; }
        public int LanguageId { get; set; }
        public int CountryId { get; set; }

        public static CultureParameter CreateDefault()
        {
            return new CultureParameter(1, "dd/MM/yyyy", 2, 197);
        }
    }
}
