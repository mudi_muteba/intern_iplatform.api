﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Culture
{
    public static class CultureExtensions
    {
        public static void Accept(this ICultureAware Culture, CultureParameter cultureInfo)
        {
            CultureVisitor cultureVisitor = new CultureVisitor(cultureInfo);

            SetCulture(Culture, cultureVisitor);
        }
        private static void SetCulture(ICultureAware Culture, CultureVisitor cultureVisitor)
        {
            IProperties property = DtoProperties.Properties.FirstOrDefault(x => x.Name == Culture.GetType().Name);

            if (property != null)
            {
                //Set Culture for ICultureVisitable properties
                SetVisitableProperties(Culture, property.VisitableProperties, cultureVisitor);

                //Set Culture for ICultureAware Properties
                SetAwareProperties(Culture, property.AwareProperties, cultureVisitor);

                //Set Generic Properties
                SetGenericProperties(Culture, property.GenericProperties, cultureVisitor);
            }
        }
        private static void SetVisitableProperties(ICultureAware Culture, List<string> properties, CultureVisitor cultureVisitor)
        {
            foreach (string p in properties)
            {
                var prop = (ICultureVisitable)Culture.GetType().GetProperty(p).GetValue(Culture, null);

                if (prop != null)
                    prop.AcceptVisitor(cultureVisitor);
            }
        }
        private static void SetAwareProperties(ICultureAware Culture, List<string> properties, CultureVisitor cultureVisitor)
        {
            foreach (string p in properties)
            {
                var culture = (ICultureAware)Culture.GetType().GetProperty(p).GetValue(Culture, null);

                //Set Culture for ICultureVisitable properties
                if (culture !=  null)
                SetCulture(culture, cultureVisitor);
            }
        }
        private static void SetGenericProperties(ICultureAware Culture, List<string> properties, CultureVisitor cultureVisitor)
        {
            foreach (string p in properties)
            {
                var propertyInfo = Culture.GetType().GetProperty(p);
                if (propertyInfo == null) continue;
                var listProperties = propertyInfo.GetValue(Culture, null);

                if (listProperties is IEnumerable)
                {
                    foreach (var property in ((IEnumerable)listProperties))
                    {
                        //Set Culture for ICultureVisitable properties
                        if (property is ICultureVisitable)
                            ((ICultureVisitable)property).AcceptVisitor(cultureVisitor);


                        //Set Culture for ICultureAware Properties
                        if (property is ICultureAware)
                            SetCulture((ICultureAware)property, cultureVisitor);

                        //Set Generic Properties
                        //var genericProperties = val.GetType().GetProperties().Where(x => x.PropertyType.IsGenericType);
                        //SetGenericProperties(Culture, genericProperties, cultureVisitor);
                    }
                }

            }
        }
    }
}
