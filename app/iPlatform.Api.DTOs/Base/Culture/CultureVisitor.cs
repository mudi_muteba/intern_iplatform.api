﻿using MasterData;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.Base.Culture
{
    public class CultureVisitor : ICultureVisitor
    {
        public CultureVisitor(){}

        public CultureVisitor(CultureParameter cultureInfo)
        {
            SetCurrency(cultureInfo.CurrencyId);
            SetLanguage(cultureInfo.LanguageId);
            SetCountry(cultureInfo.CountryId);
            this.CultureParams = cultureInfo;
        }

        public Currency Currency { get; set; }
        public Language Language { get; set; }
        public Country Country { get; set; }
        public CultureParameter CultureParams { get; set; }

        #region Money
        public void SetCurrency(int CurrencyId)
        {
            Currency = new Currencies().FirstOrDefault(x => x.Id == CurrencyId);
        }


        public string Visit(MoneyDto money)
        {
            NumberFormatInfo numberFormatInfo = GetCurrencyFormatProviderSymbolDecimals(this.Currency.Code);
            return money.Value.ToString("C", numberFormatInfo);
        }


        public static NumberFormatInfo GetCurrencyFormatProviderSymbolDecimals(string currencyCode)
        {
            if (String.IsNullOrWhiteSpace(currencyCode))
                return NumberFormatInfo.CurrentInfo;


            var currencyNumberFormat = (from culture in CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                                        let region = new RegionInfo(culture.LCID)
                                        where String.Equals(region.ISOCurrencySymbol, currencyCode,
                                                            StringComparison.InvariantCultureIgnoreCase)
                                        select culture.NumberFormat).First();

            //Need to Clone() a shallow copy here, because GetInstance() returns a read-only NumberFormatInfo
            var desiredNumberFormat = (NumberFormatInfo)NumberFormatInfo.GetInstance(CultureInfo.CurrentCulture).Clone();
            desiredNumberFormat.CurrencyDecimalDigits = currencyNumberFormat.CurrencyDecimalDigits;
            desiredNumberFormat.CurrencySymbol = currencyNumberFormat.CurrencySymbol;

            return desiredNumberFormat;
        }
        #endregion
        #region Date Time

        public void SetLanguage(int languageId)
        {
            Language = new Languages().FirstOrDefault(x => x.Id == languageId);
        }

        public void SetCountry(int countryId)
        {
            Country = new Countries().FirstOrDefault(x => x.Id == countryId);
        }

        public string Visit(DateTimeDto date)
        {
            return date.Value.HasValue 
                ? date.Value.Value.ToString(CultureParams.DateFormat,CultureInfo.CreateSpecificCulture(this.Language.ISO))
                : string.Empty;
        }
        #endregion
    }
}
