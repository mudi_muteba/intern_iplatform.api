﻿namespace iPlatform.Api.DTOs.Base
{
    public class PaginationDto
    {
        public int PageNumber { get; set; }
    }
}
