﻿namespace iPlatform.Api.DTOs.Base
{
    public interface IChannelAwareDto
    {
        int ChannelId { get; set; }
    }
}