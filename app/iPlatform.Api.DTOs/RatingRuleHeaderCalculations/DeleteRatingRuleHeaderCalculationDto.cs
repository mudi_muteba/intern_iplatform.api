﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using ValidationMessages;
using ValidationMessages.RatingRuleHeaderCalculation;

namespace iPlatform.Api.DTOs.RatingRuleHeaderCalculations
{
    public class DeleteRatingRuleHeaderCalculationDto : AttributeValidationDto, IExecutionDto, IAffectExistingEntity
    {
        public DeleteRatingRuleHeaderCalculationDto()
        {
        }
        public int Id { get; set; }
        public void SetContext(DtoContext context)
        {
            Context = context;
        }
        public DtoContext Context { get; set; }
        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            if (Id == 0)
                validation.Add(RatingRuleHeaderCalculationValidationMessages.IDRequired);
            return validation;
        }
    }
}
