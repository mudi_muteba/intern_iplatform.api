﻿using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Admin;
using MasterData;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.RatingRuleHeaders;
namespace iPlatform.Api.DTOs.RatingRuleHeaderCalculations
{
    public class RatingRuleHeaderCalculationDto : Resource
    {
        public RatingRuleHeaderCalculationDto()
        {
        }

        public RatingRuleHeaderDto RatingRuleHeader { get; set; }
        public ProductInfoDto Product { get; set; }
        public ChannelInfoDto Channel { get; set; }
        public Cover Cover { get; set; }

        public Question Question { get; set; }
        public string QuestionAnswer { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public decimal Premium { get; set; }
        public decimal Percentage { get; set; }
        public int Rank { get; set; }
        public RateCalculatorType RateCalculatorType { get; set; }
        public bool IsPercentage { get; set; }
        public decimal MinPremium { get; set; }
        public decimal MaxPremium { get; set; }
        public string WarningMessage { get; set; }
        public string FailureMessage { get; set; }
    }

    public enum RateCalculatorType
    {
        Fixed = 1,
        RatingBand = 2,
        LookupBand = 3
    }
}
