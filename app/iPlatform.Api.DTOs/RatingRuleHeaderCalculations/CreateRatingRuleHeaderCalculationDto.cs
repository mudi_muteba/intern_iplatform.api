﻿using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;
using ValidationMessages;
using ValidationMessages.RatingRuleHeaderCalculation;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Admin;
using MasterData;
using iPlatform.Api.DTOs.RatingRuleHeaders;

namespace iPlatform.Api.DTOs.RatingRuleHeaderCalculations
{
    public class CreateRatingRuleHeaderCalculationDto : AttributeValidationDto, IExecutionDto
    {
        public CreateRatingRuleHeaderCalculationDto()
        {
        }

        public RatingRuleHeaderDto RatingRuleHeader { get; set; }
        public ProductInfoDto Product { get; set; }
        public ChannelInfoDto Channel { get; set; }
        public Cover Cover { get; set; }

        public Question Question { get; set; }
        public string QuestionAnswer { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public decimal Premium { get; set; }
        public decimal Percentage { get; set; }
        public int Rank { get; set; }
        public RateCalculatorType RateCalculatorType { get; set; }
        public bool IsPercentage { get; set; }
        public decimal MinPremium { get; set; }
        public decimal MaxPremium { get; set; }
        public string WarningMessage { get; set; }
        public string FailureMessage { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; set; }

        protected override List<ValidationErrorMessage> InnerValidate()
        {
            var validation = new List<ValidationErrorMessage>();
            if (Channel == null)
                validation.Add(RatingRuleHeaderCalculationValidationMessages.ChannelIdRequired);
            if (Product == null)
                validation.Add(RatingRuleHeaderCalculationValidationMessages.ProductIdRequired);
            if (Cover == null)
                validation.Add(RatingRuleHeaderCalculationValidationMessages.CoverIdRequired);
            if (Question == null)
                validation.Add(RatingRuleHeaderCalculationValidationMessages.QuestionIdRequired);
            return validation;
        }

    }
}
