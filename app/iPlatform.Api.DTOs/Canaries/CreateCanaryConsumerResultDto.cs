﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Canaries
{
    public class CreateCanaryConsumerResultDto : IExecutionDto
    {
        public string TextField { get; set; }
        public DateTime DateField { get; set; }
        public int IntField { get; set; }
        public bool BoolField { get; set; }
        public DtoContext Context { get; set; }

        public CreateCanaryConsumerResultDto() { }

        public CreateCanaryConsumerResultDto(string textField, DateTime dateField, int intField, bool boolField)
        {
            TextField = textField;
            DateField = dateField;
            IntField = intField;
            BoolField = boolField;
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }
    }
}