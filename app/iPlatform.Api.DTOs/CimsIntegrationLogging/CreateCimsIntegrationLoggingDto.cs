﻿using iPlatform.Api.DTOs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPlatform.Api.DTOs.CimsIntegrationLogging
{
    public class CreateCimsIntegrationLoggingDto : IExecutionDto
    {
        public DtoContext Context { get; private set; }

        public CreateCimsIntegrationLoggingDto()
        {
            Context = DtoContext.NoContext();
        }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public virtual Guid RequestId { get; set; }
        public virtual string RequestData { get; set; }
        public virtual string ResponseData { get; set; }
    }
}
