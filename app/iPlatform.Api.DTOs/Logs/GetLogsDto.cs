﻿using System;
using iPlatform.Api.DTOs.Base;

namespace iPlatform.Api.DTOs.Logs
{
    public class GetLogsDto : AttributeValidationDto, IExecutionDto
    {

        public GetLogsDto()
        {
            Context = DtoContext.NoContext();

        }

        public FileType FileType { get; set; }
        public LogType LogType { get; set; }

        public void SetContext(DtoContext context)
        {
            Context = context;
        }

        public DtoContext Context { get; private set; }
    }

    public enum LogType
    {
        Debug = 1,
        Error = 2,
        Info = 3,
        All = 4
    }

    public enum FileType
    {
        Api = 1,
        Web = 2,
        Engine = 3,
    }
}