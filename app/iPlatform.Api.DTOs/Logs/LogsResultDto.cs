﻿using System;
using System.Collections.Generic;

namespace iPlatform.Api.DTOs.Logs
{
    public class LogsResultDto
    {
        public LogsResultDto()
        {
            Logs = new List<LogDto>();
        }
        public List<LogDto> Logs { get; set; }
        public string FileName { get; set; }
        public int FileSize { get; set; }
        public string FilePath { get; set; }
    }

    public class LogDto
    {
        public string Type { get; set; }
        public DateTime DateTime { get; set; }
        public string Message { get; set; }

    }
   

}