﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Reports.AIG.AllRisk;
using iPlatform.Api.DTOs.Reports.AIG.Building;
using iPlatform.Api.DTOs.Reports.AIG.Content;
using iPlatform.Api.DTOs.Reports.AIG.DisasterCash;
using iPlatform.Api.DTOs.Reports.AIG.Funeral;
using iPlatform.Api.DTOs.Reports.AIG.IdentityTheft;
using iPlatform.Api.DTOs.Reports.AIG.Lead;
using iPlatform.Api.DTOs.Reports.AIG.Motor;
using iPlatform.Api.DTOs.Reports.AIG.PersonalLegalLiability;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceIntegration;
using iPlatform.Api.DTOs.Reports.AIG.SalesForceLogSummery;

using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace ExcelGenerator
{
    public class CreateExcel
    {
        private const string Storage = @"Reports/Storage";
        private const string BaseUrl = @"iPlatform/serviceLocator/iPlatform/baseUrl";

        private string BasePath = string.Empty;

        public CreateExcel(string path)
        {
            BasePath = path;
        }

        public string GenerateDisasterCashExtractExcelFile(List<AigReportDisasterCashExtractDto> disasterCashes)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("AIG_DisasterCash_Extract");

            var rowHeader = sheet.CreateRow(0);
            rowHeader.CreateCell(0).SetCellValue("Quote-CreateDate");
            rowHeader.CreateCell(1).SetCellValue("Quote-CreateTime");
            rowHeader.CreateCell(2).SetCellValue("Date of Quote");
            rowHeader.CreateCell(3).SetCellValue("QuoteNumber");
            rowHeader.CreateCell(5).SetCellValue("Address-Suburb");
            rowHeader.CreateCell(6).SetCellValue("Address-PostCode");
            rowHeader.CreateCell(7).SetCellValue("Address-Province");
            rowHeader.CreateCell(8).SetCellValue("CashLimit");
            rowHeader.CreateCell(9).SetCellValue("Premium");

            var itemCounter = 0;

            if (disasterCashes != null && disasterCashes.Count > 0)
            {

                for (var i = 1; i < disasterCashes.Count + 1; i++)
                {
                    var row = sheet.CreateRow(i);
                    for (var j = 0; j < 9;)
                    {
                        row.CreateCell(j).SetCellValue(disasterCashes[itemCounter].QuoteCreateDate);
                        j++;
                        row.CreateCell(j).SetCellValue(disasterCashes[itemCounter].QuoteCreateTime);
                        j++;
                        row.CreateCell(j).SetCellValue(disasterCashes[itemCounter].DateOfQuote);
                        j++;
                        row.CreateCell(j).SetCellValue(disasterCashes[itemCounter].QuoteNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(disasterCashes[itemCounter].LeadNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(disasterCashes[itemCounter].AddressSuburb);
                        j++;
                        row.CreateCell(j).SetCellValue(disasterCashes[itemCounter].AddressPostCode);
                        j++;
                        row.CreateCell(j).SetCellValue(disasterCashes[itemCounter].AddressProvince);
                        j++;
                        row.CreateCell(j).SetCellValue(disasterCashes[itemCounter].CashLimit);
                        j++;
                        row.CreateCell(j).SetCellValue(disasterCashes[itemCounter].Premium.ToString(CultureInfo.InvariantCulture));
                        j++;

                        itemCounter++;
                    }
                }
            }

            var fileName = string.Format("AIG_DisasterCash_Extract_{0}.xlsx", DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));
            return Store(workbook, fileName);
        }

        public string GeneratePersonalLegalLiabilityExtractExcelFile(List<AigReportPersonalLegalLiabilityExtractDto> personalLegalLiabilities)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("AIG_PersonalLegalLiability_Extract");

            var rowHeader = sheet.CreateRow(0);
            rowHeader.CreateCell(0).SetCellValue("Quote-CreateDate");
            rowHeader.CreateCell(1).SetCellValue("Quote-CreateTime");
            rowHeader.CreateCell(2).SetCellValue("Date of Quote");
            rowHeader.CreateCell(3).SetCellValue("QuoteNumber");
            rowHeader.CreateCell(4).SetCellValue("LeadNumber");
            rowHeader.CreateCell(5).SetCellValue("Address-Suburb");
            rowHeader.CreateCell(6).SetCellValue("Address-PostCode");
            rowHeader.CreateCell(7).SetCellValue("Address-Province");
            rowHeader.CreateCell(8).SetCellValue("Personal Liability Limit");
            rowHeader.CreateCell(9).SetCellValue("Premium");

            var itemCounter = 0;

            if (personalLegalLiabilities != null && personalLegalLiabilities.Count > 0)
            {

                for (var i = 1; i < personalLegalLiabilities.Count + 1; i++)
                {
                    var row = sheet.CreateRow(i);
                    for (var j = 0; j < 9;)
                    {
                        row.CreateCell(j).SetCellValue(personalLegalLiabilities[itemCounter].QuoteCreateDate);
                        j++;
                        row.CreateCell(j).SetCellValue(personalLegalLiabilities[itemCounter].QuoteCreateTime);
                        j++;
                        row.CreateCell(j).SetCellValue(personalLegalLiabilities[itemCounter].DateOfQuote);
                        j++;
                        row.CreateCell(j).SetCellValue(personalLegalLiabilities[itemCounter].QuoteNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(personalLegalLiabilities[itemCounter].LeadNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(personalLegalLiabilities[itemCounter].AddressSuburb);
                        j++;
                        row.CreateCell(j).SetCellValue(personalLegalLiabilities[itemCounter].AddressPostCode);
                        j++;
                        row.CreateCell(j).SetCellValue(personalLegalLiabilities[itemCounter].AddressProvince);
                        j++;
                        row.CreateCell(j).SetCellValue(personalLegalLiabilities[itemCounter].PersonalLiabilityLimit);
                        j++;
                        row.CreateCell(j).SetCellValue(personalLegalLiabilities[itemCounter].Premium.ToString(CultureInfo.InvariantCulture));
                        j++;

                        itemCounter++;
                    }
                }
            }

            var fileName = string.Format("AIG_PersonalLegalLiability_Extract_{0}.xlsx", DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));
            return Store(workbook, fileName);
        }

        public string GenerateIdentityTheftExtractExcelFile(List<AigReportIdentityTheftExtractDto> identityThefts)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("AIG_IdentityTheft_Extract");

            var rowHeader = sheet.CreateRow(0);
            rowHeader.CreateCell(0).SetCellValue("Quote-CreateDate");
            rowHeader.CreateCell(1).SetCellValue("Quote-CreateTime");
            rowHeader.CreateCell(2).SetCellValue("Date of Quote");
            rowHeader.CreateCell(3).SetCellValue("QuoteNumber");
            rowHeader.CreateCell(4).SetCellValue("LeadNumber");
            rowHeader.CreateCell(5).SetCellValue("Address-Suburb");
            rowHeader.CreateCell(6).SetCellValue("Address-PostCode");
            rowHeader.CreateCell(7).SetCellValue("Address-Province");
            rowHeader.CreateCell(8).SetCellValue("ID Theft Value");
            rowHeader.CreateCell(9).SetCellValue("Premium");

            var itemCounter = 0;

            if (identityThefts != null && identityThefts.Count > 0)
            {

                for (var i = 1; i < identityThefts.Count + 1; i++)
                {
                    var row = sheet.CreateRow(i);
                    for (var j = 0; j < 9;)
                    {
                        row.CreateCell(j).SetCellValue(identityThefts[itemCounter].QuoteCreateDate);
                        j++;
                        row.CreateCell(j).SetCellValue(identityThefts[itemCounter].QuoteCreateTime);
                        j++;
                        row.CreateCell(j).SetCellValue(identityThefts[itemCounter].DateOfQuote);
                        j++;
                        row.CreateCell(j).SetCellValue(identityThefts[itemCounter].QuoteNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(identityThefts[itemCounter].LeadNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(identityThefts[itemCounter].AddressSuburb);
                        j++;
                        row.CreateCell(j).SetCellValue(identityThefts[itemCounter].AddressPostCode);
                        j++;
                        row.CreateCell(j).SetCellValue(identityThefts[itemCounter].AddressProvince);
                        j++;
                        row.CreateCell(j).SetCellValue(identityThefts[itemCounter].IdTheftvalue);
                        j++;
                        row.CreateCell(j).SetCellValue(identityThefts[itemCounter].Premium.ToString(CultureInfo.InvariantCulture));
                        j++;

                        itemCounter++;
                    }
                }
            }

            var fileName = string.Format("AIG_IdentityTheft_Extract_{0}.xlsx", DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));
            return Store(workbook, fileName);
        }

        public string GenerateFuneralExtractExcelFile(List<AigReportFuneralExtractDto> funerals)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("AIG_Funeral_Extract");

            var rowHeader = sheet.CreateRow(0);
            //rowHeader.RowStyle.Alignment = HorizontalAlignment.Center;
            rowHeader.CreateCell(0).SetCellValue("AgentName");
            rowHeader.CreateCell(1).SetCellValue("Channel");
            rowHeader.CreateCell(2).SetCellValue("Date of Quote (Rated date)");
            rowHeader.CreateCell(3).SetCellValue("Quote-Expired");
            rowHeader.CreateCell(4).SetCellValue("Quote-Bound");
            rowHeader.CreateCell(5).SetCellValue("Quote-BindDate");
            rowHeader.CreateCell(6).SetCellValue("Quote-TotalPremium");
            rowHeader.CreateCell(7).SetCellValue("Quote-MotorPremium");
            rowHeader.CreateCell(8).SetCellValue("Quote-HomePremium");
            rowHeader.CreateCell(9).SetCellValue("Quote-FuneralPremium");
            rowHeader.CreateCell(10).SetCellValue("Quote-OtherPremium");
            rowHeader.CreateCell(11).SetCellValue("Quote-InceptionDate");
            rowHeader.CreateCell(12).SetCellValue("Quote-PolicyNumber");
            rowHeader.CreateCell(13).SetCellValue("Quote-CreateDate");
            rowHeader.CreateCell(14).SetCellValue("Quote-CreateTime");
            rowHeader.CreateCell(15).SetCellValue("LeadText");
            rowHeader.CreateCell(16).SetCellValue("PropertyNumber");
            rowHeader.CreateCell(17).SetCellValue("Address-Suburb");
            rowHeader.CreateCell(18).SetCellValue("Address-PostCode");
            rowHeader.CreateCell(19).SetCellValue("Address-Province");
            rowHeader.CreateCell(20).SetCellValue("CoverType");
            rowHeader.CreateCell(21).SetCellValue("Occupation");
            rowHeader.CreateCell(22).SetCellValue("BeneficiaryIDNumber");
            rowHeader.CreateCell(23).SetCellValue("BeneficiaryFirstname");
            rowHeader.CreateCell(24).SetCellValue("Beneficiary Surname");
            rowHeader.CreateCell(25).SetCellValue("BeneficiaryContactNumber");
            rowHeader.CreateCell(26).SetCellValue("BeneficiaryRelationship");
            rowHeader.CreateCell(27).SetCellValue("SumAssured");

            var globalRowHeaderCounter = 28;
            var c = 0;
            if (funerals != null && funerals.Count> 0)
            {
                var maxAdditionalMembers = funerals.Select(item => item.AdditionalMembers.Count).ToList();

                if (maxAdditionalMembers != null)
                {
                    c = maxAdditionalMembers.Max();
                }

                for (var i = 1; i <= c; i++) //for (int i = 1; i < 5; i++)
                {
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Member{0}-FirstName", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Member{0}-Surname", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Member{0}-Relationship", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Member{0}-Student", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Member{0}-IDNumber", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Member{0}-Gender", i));
                }
            }
  

            var itemCounter = 0;

            if (funerals != null && funerals.Count > 0)
            {
                for (var i = 1; i < funerals.Count + 1; i++)
                {
                    var row = sheet.CreateRow(i);
                    for (var j = 0; j < globalRowHeaderCounter;)
                    {
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].AgentName);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].Channel);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].DateOfQuote);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].QuoteExpired);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].QuoteBound);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].QuoteBindDate);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].QuoteTotalPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].QuoteMotorPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].QuoteHomePremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].QuoteFuneralPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].QuoteOtherPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].QuoteIncepptionDate);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].QuotePolicyNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].QuoteCreateDate);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].QuoteCreateTime);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].LeadText);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].PropertyNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].AddressSuburb);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].AddressPostCode);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].AddressProvince);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].CoverType);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].Occupation);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].BeneficiaryIdNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].BeneficiaryFirstname);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].BeneficiarySurname);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].BeneficiaryContactNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].BeneficiaryRelationship);
                        j++;
                        row.CreateCell(j).SetCellValue(funerals[itemCounter].SumAssured.ToString(CultureInfo.InvariantCulture));
                        j++;

                        for (var k = 0; k < c; k++)
                        {
                            var dataAdditionalmember = funerals[itemCounter].AdditionalMembers.ElementAtOrDefault(k);

                            var isStudent = "N";
                            if (dataAdditionalmember != null && dataAdditionalmember.IsStudent)
                            {
                                isStudent = "Y";
                            }

                            row.CreateCell(j).SetCellValue(dataAdditionalmember == null ? "" : dataAdditionalmember.Initials);
                            j++;
                            row.CreateCell(j).SetCellValue(dataAdditionalmember == null ? "" : dataAdditionalmember.Surname);
                            j++;
                            row.CreateCell(j).SetCellValue(dataAdditionalmember == null ? "" : dataAdditionalmember.MemberRelationship.Name);
                            j++;
                            row.CreateCell(j).SetCellValue(dataAdditionalmember == null ? "" : isStudent);
                            j++;
                            row.CreateCell(j).SetCellValue(dataAdditionalmember == null ? "" : dataAdditionalmember.IdNumber);
                            j++;
                            row.CreateCell(j).SetCellValue(dataAdditionalmember == null ? "" : dataAdditionalmember.Gender.Name);
                            j++;
                        }

                        itemCounter++;
                    }
                }
            }

            var fileName = string.Format("AIG_Funeral_Extract_{0}.xlsx", DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));
            return Store(workbook, fileName);
        }

        public string GenerateBuildingExtractExcelFile(List<AigReportBuildingExtractDto> buildings)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("AIG_Building_Extract");

            var rowHeader = sheet.CreateRow(0);
            rowHeader.CreateCell(0).SetCellValue("Channel");
            rowHeader.CreateCell(1).SetCellValue("QuoteExpired");
            rowHeader.CreateCell(2).SetCellValue("QuoteBound");
            rowHeader.CreateCell(3).SetCellValue("QuoteBindDate");
            rowHeader.CreateCell(4).SetCellValue("QuoteTotalPremium");
            rowHeader.CreateCell(5).SetCellValue("QuoteMotorPremium");
            rowHeader.CreateCell(6).SetCellValue("QuoteHomePremium");
            rowHeader.CreateCell(7).SetCellValue("QuoteFuneralPremium");
            rowHeader.CreateCell(8).SetCellValue("QuoteOtherPremium");
            rowHeader.CreateCell(9).SetCellValue("QuoteFees");
            rowHeader.CreateCell(10).SetCellValue("QuoteSasria");
            rowHeader.CreateCell(11).SetCellValue("QuoteInceptionDate");
            rowHeader.CreateCell(12).SetCellValue("QuotePolicyNumber");
            rowHeader.CreateCell(13).SetCellValue("QuoteCreateDate");
            rowHeader.CreateCell(14).SetCellValue("QuoteCreateTime");
            rowHeader.CreateCell(15).SetCellValue("QuoteId");
            rowHeader.CreateCell(16).SetCellValue("Vmsaid");
            rowHeader.CreateCell(17).SetCellValue("PropertyNumber");
            rowHeader.CreateCell(18).SetCellValue("CoverType");
            rowHeader.CreateCell(19).SetCellValue("AddressSuburb");
            rowHeader.CreateCell(20).SetCellValue("AddressPostCode");
            rowHeader.CreateCell(21).SetCellValue("AddressProvince");
            rowHeader.CreateCell(22).SetCellValue("HomeType");
            rowHeader.CreateCell(23).SetCellValue("RoofConstruction");
            rowHeader.CreateCell(24).SetCellValue("WallConstruction");
            rowHeader.CreateCell(25).SetCellValue("ThatchLapa");
            rowHeader.CreateCell(26).SetCellValue("ThatchLapaSize");
            rowHeader.CreateCell(27).SetCellValue("LightningConductor");
            rowHeader.CreateCell(28).SetCellValue("PropertyBorder");
            rowHeader.CreateCell(29).SetCellValue("ConstructionYear");
            rowHeader.CreateCell(30).SetCellValue("SquareMetresOfProperty");
            rowHeader.CreateCell(31).SetCellValue("NumberOfBathrooms");
            rowHeader.CreateCell(32).SetCellValue("PropertyUnderConstruction");
            rowHeader.CreateCell(33).SetCellValue("HomeUsage");
            rowHeader.CreateCell(34).SetCellValue("BusinessConducted");
            rowHeader.CreateCell(35).SetCellValue("Commune");
            rowHeader.CreateCell(36).SetCellValue("PeriodPropertyIsUnoccupied");
            rowHeader.CreateCell(37).SetCellValue("Ownership");
            rowHeader.CreateCell(38).SetCellValue("MortgageBank");
            rowHeader.CreateCell(39).SetCellValue("SecurityGatedCommunity");
            rowHeader.CreateCell(40).SetCellValue("SecuritySecurityComplex");
            rowHeader.CreateCell(41).SetCellValue("SecurityBurglarBars");
            rowHeader.CreateCell(42).SetCellValue("SecuritySecurityGates");
            rowHeader.CreateCell(43).SetCellValue("SecurityAlarmWithArmedResponse");
            rowHeader.CreateCell(44).SetCellValue("VapsWaterPumpingMachinery");
            rowHeader.CreateCell(45).SetCellValue("VapsAccidentalDamage");
            rowHeader.CreateCell(46).SetCellValue("VapsSubsidenceAndLandslip");
            rowHeader.CreateCell(47).SetCellValue("BuildingsAdditionalExcess");
            rowHeader.CreateCell(48).SetCellValue("SumInsured");
            rowHeader.CreateCell(49).SetCellValue("DiscretionaryDiscount");
            rowHeader.CreateCell(50).SetCellValue("Sasria");
            rowHeader.CreateCell(51).SetCellValue("PremiumRisk");
            rowHeader.CreateCell(52).SetCellValue("PremiumSubsidenceAndLandslip");
            rowHeader.CreateCell(53).SetCellValue("PremiumWaterMachinery");
            rowHeader.CreateCell(54).SetCellValue("PremiumTotal");

            var itemCounter = 0;

            if (buildings != null && buildings.Count > 0)
            {

                for (var i = 1; i < buildings.Count + 1; i++)
                {
                    var row = sheet.CreateRow(i);
                    for (var j = 0; j < 54;)
                    {
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].Channel);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteExpired);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteBound);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteBindDate);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteTotalPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteMotorPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteHomePremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteFuneralPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteOtherPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteFees.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteSasria.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteIncepptionDate);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuotePolicyNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteCreateDate);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteCreateTime);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].QuoteId);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].Vmsaid);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].PropertyNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].CoverType);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].AddressSuburb);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].AddressPostCode);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].AddressProvince);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].HomeType);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].RoofConstruction);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].WallConstruction);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].ThatchLapa);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].ThatchLapaSize);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].LightningConductor);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].PropertyBorder);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].ConstructionYear);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].SquareMetresOfProperty);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].NumberOfBathrooms);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].PropertyUnderConstruction);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].HomeUsage);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].BusinessConducted);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].Commune);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].PeriodPropertyIsUnoccupied);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].Ownership);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].MortgageBank);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].SecurityGatedCommunity);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].SecuritySecurityComplex);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].SecurityBurglarBars);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].SecuritySecurityGates);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].SecurityAlarmWithArmedResponse);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].VapsWaterPumpingMachinery);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].VapsAccidentalDamage);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].VapsSubsidenceAndLandslip);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].BuildingsAdditionalExcess);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].SumInsured);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].DiscretionaryDiscount);
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].Sasria.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].PremiumRisk.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].PremiumSubsidenceAndLandslip.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].PremiumWaterMachinery.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(buildings[itemCounter].PremiumTotal.ToString(CultureInfo.InvariantCulture));
                        j++;

                        itemCounter++;
                    }
                }
            }

            var fileName = string.Format("AIG_Building_Extract_{0}.xlsx", DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));
            return Store(workbook, fileName);
        }

        public string GenerateContentExtractExcelFile(List<AigReportContentExtractDto> contents)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("AIG_Content_Extract");

            // Build Excel headers
            var rowHeader = sheet.CreateRow(0);
            rowHeader.CreateCell(0).SetCellValue("Channel");
            rowHeader.CreateCell(1).SetCellValue("QuoteExpired");
            rowHeader.CreateCell(2).SetCellValue("QuoteBound");
            rowHeader.CreateCell(3).SetCellValue("QuoteBindDate");
            //rowHeader.CreateCell(4).SetCellValue("DateOfQuote");
            rowHeader.CreateCell(4).SetCellValue("QuoteTotalPremium");
            rowHeader.CreateCell(5).SetCellValue("QuoteMotorPremium");
            rowHeader.CreateCell(6).SetCellValue("QuoteHomePremium");
            rowHeader.CreateCell(7).SetCellValue("QuoteFuneralPremium");
            rowHeader.CreateCell(8).SetCellValue("QuoteOtherPremium");
            rowHeader.CreateCell(9).SetCellValue("QuoteFees");
            rowHeader.CreateCell(10).SetCellValue("QuoteSasria");
            rowHeader.CreateCell(11).SetCellValue("QuoteInceptionDate");
            rowHeader.CreateCell(12).SetCellValue("QuotePolicyNumber");
            rowHeader.CreateCell(13).SetCellValue("QuoteCreateDate");
            rowHeader.CreateCell(14).SetCellValue("QuoteCreateTime");
            rowHeader.CreateCell(15).SetCellValue("QuoteId");
            rowHeader.CreateCell(16).SetCellValue("Vmsaid");
            rowHeader.CreateCell(17).SetCellValue("PropertyNumber");
            rowHeader.CreateCell(18).SetCellValue("CoverType");
            rowHeader.CreateCell(19).SetCellValue("AddressSuburb");
            rowHeader.CreateCell(20).SetCellValue("AddressPostCode");
            rowHeader.CreateCell(21).SetCellValue("AddressProvince");
            rowHeader.CreateCell(22).SetCellValue("HomeType");
            rowHeader.CreateCell(23).SetCellValue("WallConstruction");
            rowHeader.CreateCell(24).SetCellValue("RoofConstruction");
            rowHeader.CreateCell(25).SetCellValue("ThatchLapa");
            rowHeader.CreateCell(26).SetCellValue("ExcludingTheft");
            rowHeader.CreateCell(27).SetCellValue("ThatchLapaSize");
            rowHeader.CreateCell(28).SetCellValue("LightningConductor");
            rowHeader.CreateCell(29).SetCellValue("PropertyBorder");
            rowHeader.CreateCell(30).SetCellValue("ConstructionYear");
            rowHeader.CreateCell(31).SetCellValue("SquareMetresOfProperty");
            rowHeader.CreateCell(32).SetCellValue("NumberOfBathrooms");
            rowHeader.CreateCell(33).SetCellValue("PropertyUnderConstruction");
            rowHeader.CreateCell(34).SetCellValue("HomeUsage");
            rowHeader.CreateCell(35).SetCellValue("BusinessConducted");
            rowHeader.CreateCell(36).SetCellValue("Commune");
            rowHeader.CreateCell(37).SetCellValue("PeriodPropertyIsUnoccupied");
            rowHeader.CreateCell(38).SetCellValue("Ownership");
            rowHeader.CreateCell(39).SetCellValue("SecurityGatedCommunity");
            rowHeader.CreateCell(40).SetCellValue("SecuritySecurityComplex");
            rowHeader.CreateCell(41).SetCellValue("SecurityBurglarBars");
            rowHeader.CreateCell(42).SetCellValue("SecuritySecurityGates");
            rowHeader.CreateCell(43).SetCellValue("SecurityAlarmWithArmedResponse");
            rowHeader.CreateCell(44).SetCellValue("VapsAccidentalDamage");
            rowHeader.CreateCell(45).SetCellValue("VapsSubsidenceAndLandslip");
            rowHeader.CreateCell(46).SetCellValue("ContentsExcess");
            rowHeader.CreateCell(47).SetCellValue("SumInsured");
            rowHeader.CreateCell(48).SetCellValue("DiscretionaryDiscount");
            rowHeader.CreateCell(49).SetCellValue("Sasria");
            rowHeader.CreateCell(50).SetCellValue("PremiumRisk");
            rowHeader.CreateCell(51).SetCellValue("PremiumSubsidenceAndLandslip");
            rowHeader.CreateCell(52).SetCellValue("PremiumTotal");

            var itemCounter = 0;

            if (contents != null && contents.Count > 0)
            {

                for (var i = 1; i < contents.Count + 1; i++)
                {
                    var row = sheet.CreateRow(i);
                    for (var j = 0; j < 52;)
                    {
                        row.CreateCell(j).SetCellValue(contents[itemCounter].Channel);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteExpired);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteBound);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteBindDate);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteTotalPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteMotorPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteHomePremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteFuneralPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteOtherPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteFees.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteSasria.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteIncepptionDate);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuotePolicyNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteCreateDate);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteCreateTime);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].QuoteId);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].Vmsaid);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].PropertyNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].CoverType);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].AddressSuburb);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].AddressPostCode);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].AddressProvince);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].HomeType);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].WallConstruction);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].RoofConstruction);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].ThatchLapa);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].ExcludingTheft);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].ThatchLapaSize);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].LightningConductor);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].PropertyBorder);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].ConstructionYear);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].SquareMetresOfProperty);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].NumberOfBathrooms);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].PropertyUnderConstruction);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].HomeUsage);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].BusinessConducted);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].Commune);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].PeriodPropertyIsUnoccupied);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].Ownership);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].SecurityGatedCommunity);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].SecuritySecurityComplex);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].SecurityBurglarBars);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].SecuritySecurityGates);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].SecurityAlarmWithArmedResponse);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].VapsAccidentalDamage);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].VapsSubsidenceAndLandslip);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].ContentsExcess);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].SumInsured);
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].DiscretionaryDiscount.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].Sasria.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].PremiumRisk.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].PremiumSubsidenceAndLandslip.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(contents[itemCounter].PremiumTotal.ToString(CultureInfo.InvariantCulture));
                        j++;

                        itemCounter++;
                    }
                }
            }

            var fileName = string.Format("AIG_Content_Extract_{0}.xlsx", DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));
            return Store(workbook, fileName);
        }

        public string GenerateAllRiskExtractExcelFile(List<AigReportAllRiskExtractDto> allRisks)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("AIG_AllRisk_Extract");

            // Build Excel headers
            var rowHeader = sheet.CreateRow(0);
            rowHeader.CreateCell(0).SetCellValue("AgentName");
            rowHeader.CreateCell(1).SetCellValue("Channel");
            rowHeader.CreateCell(2).SetCellValue("Date of Quote (Rated date)");
            rowHeader.CreateCell(3).SetCellValue("Quote-Expired");
            rowHeader.CreateCell(4).SetCellValue("Quote-Bound");
            rowHeader.CreateCell(5).SetCellValue("Quote-BindDate");
            rowHeader.CreateCell(6).SetCellValue("Quote-TotalPremium");
            rowHeader.CreateCell(7).SetCellValue("Quote-MotorPremium");
            rowHeader.CreateCell(8).SetCellValue("Quote-HomePremium");
            rowHeader.CreateCell(9).SetCellValue("Quote-FuneralPremium");
            rowHeader.CreateCell(10).SetCellValue("Quote-OtherPremium");
            rowHeader.CreateCell(11).SetCellValue("Quote-InceptionDate");
            rowHeader.CreateCell(12).SetCellValue("Quote-PolicyNumber");
            rowHeader.CreateCell(13).SetCellValue("Quote-CreateDate");
            rowHeader.CreateCell(14).SetCellValue("Quote-CreateTime");
            rowHeader.CreateCell(15).SetCellValue("LeadText");
            rowHeader.CreateCell(16).SetCellValue("PropertyNumber");
            rowHeader.CreateCell(17).SetCellValue("ItemNumber");
            rowHeader.CreateCell(18).SetCellValue("Address - Suburb");
            rowHeader.CreateCell(19).SetCellValue("Address - PostCode");
            rowHeader.CreateCell(20).SetCellValue("Address - Province");
            rowHeader.CreateCell(21).SetCellValue("Item - Category");
            rowHeader.CreateCell(22).SetCellValue("Item - Description");
            rowHeader.CreateCell(23).SetCellValue("Item - Jewellery - Safe");
            rowHeader.CreateCell(24).SetCellValue("Item - ReceiptsForItems");
            rowHeader.CreateCell(25).SetCellValue("Item - SerialNumber");
            rowHeader.CreateCell(26).SetCellValue("Item - Discount");
            rowHeader.CreateCell(27).SetCellValue("Item - SumInsured");

            var itemCounter = 0;

            if (allRisks != null && allRisks.Count > 0)
            {

                for (var i = 1; i < allRisks.Count + 1; i++)
                {
                    var row = sheet.CreateRow(i);
                    for (var j = 0; j < 27;)
                    {
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].AgentName);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].Channel);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].DateOfQuote);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].QuoteExpired);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].QuoteBound);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].QuoteBindDate);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].QuoteTotalPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].QuoteMotorPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].QuoteHomePremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].QuoteFuneralPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].QuoteOtherPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].QuoteIncepptionDate);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].QuotePolicyNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].QuoteCreateDate);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].QuoteCreateTime);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].LeadText);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].PropertyNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].ItemNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].AddressSuburb);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].AddressPostCode);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].AddressProvince);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].ItemCategory);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].ItemDescripption);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].ItemJewellerySafe);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].ItemReceiptsForItems);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].ItemSerialNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].ItemDiscount);
                        j++;
                        row.CreateCell(j).SetCellValue(allRisks[itemCounter].ItemSumInsured.ToString());
                        j++;

                        itemCounter++;
                    }
                }
            }

            var fileName = string.Format("AIG_AllRisk_Extract_{0}.xlsx", DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));
            return Store(workbook, fileName);

        }

        public string GenerateMotorExtractExcelFile(List<AigReportMotorExtractDto> motors)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("AIG_Motor_Extract");

            // Build Excel headers
            var rowHeader = sheet.CreateRow(0);
            rowHeader.CreateCell(0).SetCellValue("Channel");
            rowHeader.CreateCell(1).SetCellValue("QuoteExpired");
            rowHeader.CreateCell(2).SetCellValue("QuoteBound");
            rowHeader.CreateCell(3).SetCellValue("QuoteBindDate");
            //rowHeader.CreateCell(4).SetCellValue("DateOfQuote");
            rowHeader.CreateCell(4).SetCellValue("QuoteTotalPremium");
            rowHeader.CreateCell(5).SetCellValue("QuoteMotorPremium");
            rowHeader.CreateCell(6).SetCellValue("QuoteHomePremium");
            rowHeader.CreateCell(7).SetCellValue("QuoteFuneralPremium");
            rowHeader.CreateCell(8).SetCellValue("QuoteOtherPremium");
            rowHeader.CreateCell(9).SetCellValue("QuoteFees");
            rowHeader.CreateCell(10).SetCellValue("QuoteSasria");
            rowHeader.CreateCell(11).SetCellValue("QuoteInceptionDate");
            rowHeader.CreateCell(12).SetCellValue("QuotePolicyNumber");
            rowHeader.CreateCell(13).SetCellValue("QuoteCreateDate");
            rowHeader.CreateCell(14).SetCellValue("QuoteCreateTime");
            rowHeader.CreateCell(15).SetCellValue("QuoteId");
            rowHeader.CreateCell(16).SetCellValue("Vmsaid");
            rowHeader.CreateCell(17).SetCellValue("VehicleNumber");
            rowHeader.CreateCell(18).SetCellValue("NightAddressSuburb");
            rowHeader.CreateCell(19).SetCellValue("NightAddressPostCode");
            rowHeader.CreateCell(20).SetCellValue("NightAddressProvince");
            rowHeader.CreateCell(21).SetCellValue("NightAddressStorage");
            rowHeader.CreateCell(22).SetCellValue("DayAddressSuburb");
            rowHeader.CreateCell(23).SetCellValue("DayAddressPostCode");
            rowHeader.CreateCell(24).SetCellValue("DayAddressProvince");
            rowHeader.CreateCell(25).SetCellValue("DayAddressStorage");
            rowHeader.CreateCell(26).SetCellValue("VehicleType");
            rowHeader.CreateCell(27).SetCellValue("MotorExcess");
            rowHeader.CreateCell(28).SetCellValue("RegistrationNumber");
            rowHeader.CreateCell(29).SetCellValue("VehicleYear");
            rowHeader.CreateCell(30).SetCellValue("VehicleMake");
            rowHeader.CreateCell(31).SetCellValue("VehicleModel");
            rowHeader.CreateCell(32).SetCellValue("VehicleMmCode");
            rowHeader.CreateCell(33).SetCellValue("VinNumber");
            rowHeader.CreateCell(34).SetCellValue("Colour");
            rowHeader.CreateCell(35).SetCellValue("MetallicPaint");
            rowHeader.CreateCell(36).SetCellValue("CoverType");
            rowHeader.CreateCell(37).SetCellValue("Modified");
            rowHeader.CreateCell(38).SetCellValue("VehicleStatus");
            rowHeader.CreateCell(39).SetCellValue("ClassOfUse");
            rowHeader.CreateCell(40).SetCellValue("Delivery");
            rowHeader.CreateCell(41).SetCellValue("TrackingDevice");
            rowHeader.CreateCell(42).SetCellValue("DiscountedTrackingDevice");
            rowHeader.CreateCell(43).SetCellValue("Immobiliser");
            rowHeader.CreateCell(44).SetCellValue("AntiHijack");
            rowHeader.CreateCell(45).SetCellValue("DataDot");
            rowHeader.CreateCell(46).SetCellValue("AccSoundEquipment");
            rowHeader.CreateCell(47).SetCellValue("AccMagWheels");
            rowHeader.CreateCell(48).SetCellValue("AccSunRoof");
            rowHeader.CreateCell(49).SetCellValue("AccXenonLights");
            rowHeader.CreateCell(50).SetCellValue("AccTowBar");
            rowHeader.CreateCell(51).SetCellValue("AccBodyKit");
            rowHeader.CreateCell(52).SetCellValue("AccAntiSmashAndGrab");
            rowHeader.CreateCell(53).SetCellValue("AccOther");
            rowHeader.CreateCell(54).SetCellValue("VapCarHire");
            rowHeader.CreateCell(55).SetCellValue("VapTyreAndRim");
            rowHeader.CreateCell(56).SetCellValue("VapScratchAndDent");
            rowHeader.CreateCell(57).SetCellValue("VapCreditShortfall");
            rowHeader.CreateCell(58).SetCellValue("DriverIdNumber");
            rowHeader.CreateCell(59).SetCellValue("DriverTitle");
            rowHeader.CreateCell(60).SetCellValue("DriverFirstName");
            rowHeader.CreateCell(61).SetCellValue("DriverSurname");
            rowHeader.CreateCell(62).SetCellValue("DriverGender");
            rowHeader.CreateCell(63).SetCellValue("DriverMaritalStatus");
            rowHeader.CreateCell(64).SetCellValue("DriverDateOfBirth");
            rowHeader.CreateCell(65).SetCellValue("DriverOccupation");
            rowHeader.CreateCell(66).SetCellValue("DriverRecentCoverIndicator");
            rowHeader.CreateCell(67).SetCellValue("DriverUnerruptedCover");
            rowHeader.CreateCell(68).SetCellValue("DriverLicenseType");
            rowHeader.CreateCell(69).SetCellValue("DriverLicenseDate");
            rowHeader.CreateCell(70).SetCellValue("DriverLicenseEndorsed");
            rowHeader.CreateCell(71).SetCellValue("LimitationAutomatic");
            rowHeader.CreateCell(72).SetCellValue("LimitationParaplegic");
            rowHeader.CreateCell(73).SetCellValue("LimitationAmputee");
            rowHeader.CreateCell(74).SetCellValue("LimitationGlasses");


            // Driver Losses


            var globalRowHeaderCounter = 75;
            var c = 0;
            if (motors!= null && motors.Count > 0)
            {
                var maxDriverLosses = motors.Select(item => item.MotorLosses.Count).ToList();

                if (maxDriverLosses != null)
                {
                    c = maxDriverLosses.Max();
                }

                for (var i = 1; i <= c; i++) //for (int i = 1; i < 5; i++)
                {
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("DriverLosses{0}-Line", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("DriverLosses{0}-Type", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("DriverLosses{0}-Date", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("DriverLosses{0}-Amount", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("DriverLosses{0}-Location", i));
                }
            }

            rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("Financed");
            rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("ValuationMethod");
            rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("SumInsured");
            rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("SASRIA");

            var itemCounter = 0;

            if (motors != null && motors.Count > 0)
            {

                for (var i = 1; i < motors.Count + 1; i++)
                {
                    var row = sheet.CreateRow(i);
                    for (var j = 0; j < globalRowHeaderCounter;)
                    {
                        row.CreateCell(j).SetCellValue(motors[itemCounter].Channel);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteExpired);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteBound);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteBindDate);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteTotalPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteMotorPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteHomePremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteFuneralPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteOtherPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteFees.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteSasria.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteIncepptionDate);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuotePolicyNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteCreateDate);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteCreateTime);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].QuoteId);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].Vmsaid);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].VehicleNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].NightAddressSuburb);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].NightAddressPostCode);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].NightAddressProvince);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].NightAddressStorage);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DayAddressSuburb);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DayAddressPostCode);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DayAddressProvince);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DayAddressStorage);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].VehicleType);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].MotorExcess.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].RegistrationNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].VehicleYear);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].VehicleMake);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].VehicleModel);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].VehicleMmCode);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].VinNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].Colour);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].MetallicPaint);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].CoverType);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].Modified);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].VehicleStatus);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].ClassOfUse);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].Delivery);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].TrackingDevice);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DiscountedTrackingDevice);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].Immobiliser);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].AntiHijack);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DataDot);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].AccSoundEquipment.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].AccMagWheels.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].AccSunRoof.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].AccXenonLights.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].AccTowBar.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].AccBodyKit.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].AccAntiSmashAndGrab.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].AccOther.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].VapCarHire);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].VapTyreAndRim);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].VapScratchAndDent);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].VapCreditShortfall);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverIdNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverTitle);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverFirstName);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverSurname);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverGender);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverMaritalStatus);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverDateOfBirth);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverOccupation);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverRecentCoverIndicator);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverUninterruptedCover);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverLicenseType);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverLicenseDate);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].DriverLicenseEndorsed);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].LimitationAutomatic);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].LimitationParaplegic);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].LimitationAmputee);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].LimitationGlasses);
                        j++;

                        for (var k = 0; k < c; k++) 
                        {
                            var dataMotorLoss = motors[itemCounter].MotorLosses.ElementAtOrDefault(k);
                  
                            row.CreateCell(j).SetCellValue(dataMotorLoss == null ? "" : dataMotorLoss.Type);
                            j++;
                            row.CreateCell(j).SetCellValue(dataMotorLoss == null ? "" : dataMotorLoss.MotorLossType);
                            j++;
                            row.CreateCell(j).SetCellValue(dataMotorLoss == null ? "" : dataMotorLoss.DateOfLoss);
                            j++;
                            row.CreateCell(j).SetCellValue(dataMotorLoss == null ? "" : dataMotorLoss.MotorLossClaim);
                            j++;
                            row.CreateCell(j).SetCellValue("");
                            j++;
                        }

                        row.CreateCell(j).SetCellValue(motors[itemCounter].Financed);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].ValuationMethod);
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].SumInsured.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(motors[itemCounter].Sasria.ToString(CultureInfo.InvariantCulture));
                        j++;

                        itemCounter++;
                    }
                }
            }

            var fileName = string.Format("AIG_Motor_Extract_{0}.xlsx", DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));
            return Store(workbook, fileName);
        }

        public string GenerateSalesForceIntegrationExtractExcelFile(
            List<AigReportSalesForceIntegrationLogExtractDto> salesForceIntegration)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("AIG_SalesForceIntegration_Extract");

            // Build Excel headers
            var rowHeader = sheet.CreateRow(0);
            rowHeader.CreateCell(0).SetCellValue("JsonData");
            rowHeader.CreateCell(1).SetCellValue("Response");
            rowHeader.CreateCell(2).SetCellValue("CreatedOn");
            rowHeader.CreateCell(3).SetCellValue("PartyId");
            rowHeader.CreateCell(4).SetCellValue("IsSuccess");
            rowHeader.CreateCell(5).SetCellValue("Status");
            var itemCounter = 0;

            if (salesForceIntegration != null && salesForceIntegration.Count > 0)
            {

                for (var i = 1; i < salesForceIntegration.Count + 1; i++)
                {
                    var row = sheet.CreateRow(i);
                    for (var j = 0; j < 6;)
                    {
                        row.CreateCell(j).SetCellValue(salesForceIntegration[itemCounter].JsonData);
                        j++;
                        row.CreateCell(j).SetCellValue(salesForceIntegration[itemCounter].Response);
                        j++;
                        row.CreateCell(j).SetCellValue(salesForceIntegration[itemCounter].CreatedOn.ToString("yyyy/MM/dd HH:mm:ss"));
                        j++;
                        row.CreateCell(j).SetCellValue(salesForceIntegration[itemCounter].PartyId);
                        j++;
                        row.CreateCell(j).SetCellValue(salesForceIntegration[itemCounter].IsSuccess);
                        j++;
                        row.CreateCell(j).SetCellValue(salesForceIntegration[itemCounter].Status);
                        j++;

                        itemCounter++;
                    }
                }
            }

            var fileName = string.Format("AIG_SalesForceIntegration_Extract_{0}.xlsx", DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));
            return Store(workbook, fileName);
        }

        public string GenerateSalesForceLogSummeryExtractExcelFile(
         List<AigReportSalesForceLogSummeryExtractDto> salesForceIntegration)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("AIG_SalesForceIntegrationLogSummery_Extract");

            // Build Excel headers
            var rowHeader = sheet.CreateRow(0);
            //rowHeader.CreateCell(0).SetCellValue("Counter");
            rowHeader.CreateCell(0).SetCellValue("Description");
            rowHeader.CreateCell(1).SetCellValue("Total");
            var itemCounter = 0;

            if (salesForceIntegration != null && salesForceIntegration.Count > 0)
            {

                for (var i = 1; i < salesForceIntegration.Count + 1; i++)
                {
                    var row = sheet.CreateRow(i);
                    for (var j = 0; j < 2;)
                    {
                        //row.CreateCell(j).SetCellValue(salesForceIntegration[itemCounter].Counter);
                        //j++;
                        row.CreateCell(j).SetCellValue(salesForceIntegration[itemCounter].Description);
                        j++;
                        row.CreateCell(j).SetCellValue(salesForceIntegration[itemCounter].Total);
                        j++;
                        itemCounter++;
                    }
                }
            }

            var fileName = string.Format("AIG_SalesForceIntegrationLogSummery_Extract_{0}.xlsx", DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));
            return Store(workbook, fileName);
        }

        public string GenerateLeadExtractExcelFile(AigLeadExcelData data)
        {
            IWorkbook workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("AIG_Lead_Extract");

            var rowHeader = sheet.CreateRow(0);
            rowHeader.CreateCell(0).SetCellValue("Channel");
            rowHeader.CreateCell(1).SetCellValue("QuoteExpired");
            rowHeader.CreateCell(2).SetCellValue("QuoteBound");
            rowHeader.CreateCell(3).SetCellValue("QuoteBindDate");
            rowHeader.CreateCell(4).SetCellValue("QuoteTotalPremium");
            rowHeader.CreateCell(5).SetCellValue("QuoteMotorPremium");
            rowHeader.CreateCell(6).SetCellValue("QuoteHomePremium");
            rowHeader.CreateCell(7).SetCellValue("QuoteFuneralPremium");
            rowHeader.CreateCell(8).SetCellValue("QuoteOtherPremium");
            rowHeader.CreateCell(9).SetCellValue("QuoteFees");
            rowHeader.CreateCell(10).SetCellValue("QuoteSasria");
            rowHeader.CreateCell(11).SetCellValue("QuoteInceptionDate");
            rowHeader.CreateCell(12).SetCellValue("QuotePolicyNumber");
            rowHeader.CreateCell(13).SetCellValue("QuoteCreateDate");
            rowHeader.CreateCell(14).SetCellValue("QuoteCreateTime");
            rowHeader.CreateCell(15).SetCellValue("QuoteId");
            rowHeader.CreateCell(16).SetCellValue("Vmsaid");
            rowHeader.CreateCell(17).SetCellValue("IdNumber");
            rowHeader.CreateCell(18).SetCellValue("Title");
            rowHeader.CreateCell(19).SetCellValue("FirstName");
            rowHeader.CreateCell(20).SetCellValue("Initials");
            rowHeader.CreateCell(21).SetCellValue("Surname");
            rowHeader.CreateCell(22).SetCellValue("DateOfBirth");
            rowHeader.CreateCell(23).SetCellValue("Age");
            rowHeader.CreateCell(24).SetCellValue("Gender");
            rowHeader.CreateCell(25).SetCellValue("MaritalStatus");
            rowHeader.CreateCell(26).SetCellValue("ItcCreditScoreBand");
            rowHeader.CreateCell(27).SetCellValue("Occupation");
            rowHeader.CreateCell(28).SetCellValue("HomeLanguage");
            rowHeader.CreateCell(29).SetCellValue("PreferredLanguage");
            rowHeader.CreateCell(30).SetCellValue("EmailAddress");
            rowHeader.CreateCell(31).SetCellValue("CellPhoneNumber");
            rowHeader.CreateCell(32).SetCellValue("HomePhoneNumber");
            rowHeader.CreateCell(33).SetCellValue("WorkPhoneNumber");
            rowHeader.CreateCell(34).SetCellValue("CommPreferencePolicy");
            rowHeader.CreateCell(35).SetCellValue("CommPreferenceMarketing");
            rowHeader.CreateCell(36).SetCellValue("DeclarationJudgements");
            rowHeader.CreateCell(37).SetCellValue("DeclarationDebtReview");
            rowHeader.CreateCell(38).SetCellValue("DeclarationSequestration");

            var globalRowHeaderCounter = 39;

            if (data.Leads != null && data.Leads.Count > 0)
            {
                var maxAddress = data.Leads.Select(item => item.Addresses.Count).ToList();
                var c = 0;
                if (maxAddress != null)
                {
                    c = maxAddress.Max();
                }

                for (var i = 1; i <= c; i++) 
                {
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Address{0}-Line1", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Address{0}-Line2", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Address{0}-Line3", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Address{0}-Suburb", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Address{0}-PostCode", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Address{0}-Province", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Address{0}-Type", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Address{0}-Default", i));
                    //rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("Address{0}-Active", i));
                }

                rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("AccountHolderName");
                rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("AccountNumber");
                rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("AccountType");
                rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("BankName");
                rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("BranchName");
                rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("BranchCode");
                rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("CurrentlyInsured");
                rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("UninterruptedCover");
                rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue("CancelledInsurance");

                var m = 0;
                var maxLoss = data.Leads.Select(item => item.Losses.Count).ToList();
                if (maxLoss != null)
                {
                    m = maxLoss.Max();
                }

                var counterLossItems = 0;
                for (var i = 1; i <= m; i++)
                {
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("PreviousLosses{0}-Line", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("PreviousLosses{0}-Type", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("PreviousLosses{0}-Date", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("PreviousLosses{0}-Amount", i));
                    rowHeader.CreateCell(globalRowHeaderCounter++).SetCellValue(string.Format("PreviousLosses{0}-Location", i));
                    counterLossItems++;
                }


                var itemCounter = 0;
                for (var i = 1; i < data.Leads.Count + 1; i++)
                {
                    var row = sheet.CreateRow(i);
                    for (var j = 0; j < globalRowHeaderCounter;)
                    {
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].Channel);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteExpired);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteBound);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteBindDate);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteTotalPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteMotorPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteHomePremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteFuneralPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteOtherPremium.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteFees.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteSasria.ToString(CultureInfo.InvariantCulture));
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteIncepptionDate);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuotePolicyNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteCreateDate);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteCreateTime);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].QuoteId);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].Vmsaid);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].IdNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].Title);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].FirstName);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].Initials);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].Surname);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].DateOfBirth);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].Age);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].Gender);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].MaritalStatus);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].ItcCreditScoreBand);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].Occupation);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].HomeLanguage);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].PreferredLanguage);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].EmailAddress);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].CellPhoneNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].HomePhoneNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].WorkPhoneNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].CommPreferencePolicy);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].CommPreferenceMarketing);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].DeclarationJudgements);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].DeclarationDebtReview);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].DeclarationSequestration);
                        j++;
                  
                        for (var k = 0; k < c; k++) 
                        {
                            var dataAddress = data.Leads[itemCounter].Addresses.ElementAtOrDefault(k);

                            var isDefaultAddress = "N";

                            if (dataAddress == null)
                            {
                                isDefaultAddress = "N";
                            }
                            else if (dataAddress.IsDefault)
                            {
                                isDefaultAddress = "Y";
                            }
                            row.CreateCell(j).SetCellValue(dataAddress == null ? "" : dataAddress.Line1);
                            j++;
                            row.CreateCell(j).SetCellValue(dataAddress == null ? "" : dataAddress.Line2);
                            j++;
                            row.CreateCell(j).SetCellValue(dataAddress == null ? "" : dataAddress.Line3);
                            j++;
                            row.CreateCell(j).SetCellValue(dataAddress == null ? "" : dataAddress.Line4);
                            j++;
                            row.CreateCell(j).SetCellValue(dataAddress == null ? "" : dataAddress.Code);
                            j++;
                            row.CreateCell(j).SetCellValue(dataAddress == null ? "" : dataAddress.StateProvince);
                            j++;
                            row.CreateCell(j).SetCellValue(dataAddress == null ? "" : dataAddress.AddressType);
                            j++;
                            row.CreateCell(j).SetCellValue(isDefaultAddress);
                            j++;
                        }

                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].AccountHolderName);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].AccountNumber);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].AccountType);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].BankName);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].BranchName);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].BranchCode);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].CurrentlyInsured);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].UninterruptedCover);
                        j++;
                        row.CreateCell(j).SetCellValue(data.Leads[itemCounter].CancelledInsurance);
                        j++;

                        for (var k = 0; k < m; k++) 
                        {
                            var dataLosses = data.Leads[itemCounter].Losses.ElementAtOrDefault(k);
                          
                            row.CreateCell(j).SetCellValue(dataLosses == null ? "" : dataLosses.Type);
                            j++;
                            row.CreateCell(j).SetCellValue(dataLosses == null ? "" : dataLosses.LossType);
                            j++;
                            row.CreateCell(j).SetCellValue(dataLosses == null ? "" : dataLosses.DateOfLoss);
                            j++;
                            row.CreateCell(j).SetCellValue(dataLosses == null ? "" : dataLosses.LossClaim);
                            j++;
                            row.CreateCell(j).SetCellValue(dataLosses == null ? "" : dataLosses.Location);
                            j++;
                        }

                        itemCounter++;
                    }
                }
            }

            var fileName = string.Format("AIG_Lead_Extract_{0}.xlsx", DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));
            return Store(workbook, fileName);
        }

        public string Store(IWorkbook workbook, string name)
        {
            var baseUrl = ConfigurationManager.AppSettings[BaseUrl].ToString();
            var storage = ConfigurationManager.AppSettings[Storage].ToString();

            var path = Path.Combine(BasePath, storage, name);

            var fileStream = File.Create(path);
            workbook.Write(fileStream);
            fileStream.Close();

            var uri = Path.Combine(baseUrl, storage, name).Replace(@"\", "/");

            return uri;
        }
    }
}