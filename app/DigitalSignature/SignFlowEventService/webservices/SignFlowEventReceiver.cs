﻿using System;
using System.Linq;
using System.Web;
using Domain.SignFlow.Message;
using MasterData;
using SignFlow;
using Workflow.Messages;


namespace SignFlowEventService.webservices
{
    public class SignFlowEventReceiver : ISignFlowEventReceiver
    {
        private readonly IWorkflowRouter _router;
        public SignFlowEventReceiver(IWorkflowRouter router)
        {
            _router = router;
        }

        public string Receiver(string sfs, string et, string di, string ui, string ue, string fi, string fn, DateTime ed)
        {
            var recieverResult = "Suc: Event Handled";

            var secretKey = Infrastructure.Configuration.ConfigurationReader.SignFlowSetting.SignFlowEventReceiverSecretKey;
            if (string.IsNullOrEmpty(secretKey))
            {
                recieverResult = "Err: SignFlowSecret missing from Application Settings";
                return recieverResult;
            }

            if (sfs != secretKey)
            {
                recieverResult = "Err: Access Denied";
                return recieverResult;
            }

            var signFlowConnector = new SignFlowConnector();

            var loginResult = signFlowConnector.Login();
            if (loginResult.Result == "Success")
            {
                var getDocStatusResult = signFlowConnector.GetDocStatus(loginResult, int.Parse(di));

                if (getDocStatusResult.DocStatus == "Completed")
                {
                    var status = new DocumentStatuses().FirstOrDefault(c => c.Name == getDocStatusResult.DocStatus);
                    signFlowConnector.GetDoc(getDocStatusResult, loginResult, int.Parse(di));

                    // Raise message for updating Singflow row in database
                    var routingMessage = new WorkflowRoutingMessage();
                    routingMessage.AddMessage(new SignFlowEventReceiverWorkflowExecutionMessage(new SignFlowEventReceiverTaskMetaData(int.Parse(di), status, getDocStatusResult, loginResult)));
                    _router.Publish(routingMessage);

                    recieverResult = "Suc: Event Handled";
                }
            }

            HttpContext.Current.Response.Output.WriteLine(recieverResult);
            HttpContext.Current.Response.End();
            return recieverResult;
        }
    }
}