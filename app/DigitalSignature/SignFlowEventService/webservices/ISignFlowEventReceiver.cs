﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace SignFlowEventService.webservices
{
    [ServiceContract]
    public interface ISignFlowEventReceiver
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Xml, UriTemplate = "receiver")]
        string Receiver(string sfs, string et, string di, string ui, string ue, string fi, string fn, DateTime ed);
    }
}
