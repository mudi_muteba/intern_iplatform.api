﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using EasyNetQ;
using SignFlowEventService.webservices;
using Workflow.Messages;
using Workflow.Publisher;

namespace SignFlowEventService
{
    public class Global : System.Web.HttpApplication
    {
        IWindsorContainer _container;

        protected void Application_Start(object sender, EventArgs e)
        {
            var bus = BusBuilder.CreateRouterBus();
            _container = new WindsorContainer();
            _container.AddFacility<WcfFacility>().Register
                (
                    Component.For<ISignFlowEventReceiver>().ImplementedBy<SignFlowEventReceiver>().Named("SignFlowEventReceiver"),
                    Component.For<IBus>().Instance(bus).LifestyleSingleton().Named("RouterBus"),
                    Component.For<IWorkflowBus>().ImplementedBy<WorkflowBus>(),
                    Component.For<IWorkflowRouter>().UsingFactoryMethod(CreateWorkflowRouter)
                );
        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (_container != null)
                _container.Dispose();
        }

        private IWorkflowRouter CreateWorkflowRouter(IKernel kernel)
        {
            var bus = kernel.Resolve<IBus>("RouterBus");
            kernel.ReleaseComponent(bus);
            return new WorkflowRouter(new WorkflowBus(bus));
        }
    }
}