﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MasterData;
using SignFlow.Model;
using SignFlowAPI;

namespace SignFlow
{
    public class SignFlowConnector
    {
        public object SignFlowInitialiser(SignFlowWebMethod methodName, object request)
        {
            var signFlowApiFactory = new SignFlowAPIFactory();
            var signFlowApiUrl = Infrastructure.Configuration.ConfigurationReader.SignFlowSetting.SignFlowApiServiceUrl;
            var signFlowApiClient = (ISignFlowAPIService)signFlowApiFactory.CreateClient(signFlowApiUrl);

            switch (methodName)
            {
                case SignFlowWebMethod.Login:
                    return signFlowApiClient.Login((LoginRequest)request);

                case SignFlowWebMethod.CreateWorkflow:
                    return signFlowApiClient.CreateWorkflow((AddDocRequest)request);

                case SignFlowWebMethod.AddWorkFlowStep:
                    return signFlowApiClient.AddWorkFlowStep((WorkflowStepRequest)request);

                case SignFlowWebMethod.InitiateFlow:
                    return signFlowApiClient.InitiateFlow((InitiateFlowRequest)request);

                case SignFlowWebMethod.GetDocStatus:
                    return signFlowApiClient.GetDocStatus((GetDocStatusRequest)request);

                case SignFlowWebMethod.GetDoc:
                    return signFlowApiClient.GetDoc((GetDocRequest)request);

                case SignFlowWebMethod.DocPrepperAddFields:
                    return signFlowApiClient.DocPrepperAddFields((DocPrepperAddFieldRequest)request);

                case SignFlowWebMethod.AddUser:
                    return signFlowApiClient.Add_User((AddUserRequest)request);
            }

            return null;
        }

        public LoginResult Login()
        {
            var loginResult = new LoginResult();

            var request = new LoginRequest
            {
                Password = Infrastructure.Configuration.ConfigurationReader.SignFlowSetting.SignFlowApiPassword,
                UserName = Infrastructure.Configuration.ConfigurationReader.SignFlowSetting.SignFlowApiUserName
            };

            var result = (LoginResponse)SignFlowInitialiser(SignFlowWebMethod.Login, request);

            loginResult.Token = result.Token.Token;
            loginResult.TokenExpiry = result.Token.TokenExpiry;
            loginResult.Result = result.Result;

            return loginResult;
        }

        public CreateWorkflowResult CreateWorkflow(LoginResult loginResult, string fileToUpload, string messageToUser, string uploadDocName, DateTime dueDate)
        {
            var createWorkflowResult = new CreateWorkflowResult();
            AddDocRequest addDocRequest;

            if (loginResult == null) return createWorkflowResult;

            if (!string.IsNullOrEmpty(fileToUpload) && !string.IsNullOrEmpty(uploadDocName))
            {
                var path = fileToUpload;

                var filteredPath = path.Contains("%20") ? path.Replace("%20", " ") : path;

                var bytes = File.ReadAllBytes(filteredPath);
                var fileBase64 = Convert.ToBase64String(bytes);

                var token = new Token
                {
                    Token = loginResult.Token,
                    TokenExpiry = loginResult.TokenExpiry
                };

                addDocRequest = new AddDocRequest
                {
                    Priority = DocPriority.Normal,
                    Extension = ExtensionManager(fileToUpload),
                    DueDate = dueDate, // new DateTime(2017, 07, 11, 17, 00, 00)
                    SLA = 1,
                    AutoRemind = DocAutoRemind.No,
                    Message = messageToUser,
                    DocName = uploadDocName,
                    Doc = fileBase64,
                    Token = token
                };
            }
            else
            {
                return createWorkflowResult;
            }

            var result = (AddDocResponse)SignFlowInitialiser(SignFlowWebMethod.CreateWorkflow, addDocRequest);

            createWorkflowResult.DocId = result.DocID;
            createWorkflowResult.Result = result.Result;

            return createWorkflowResult;
        }

        public List<AddWorkFlowStepResult> AddWorkFlowStep(CreateWorkflowResult createWorkflowResult, LoginResult loginResult, List<SignFlowUserData> sfUserData)
        {
            var addWorkFlowStepResults = new List<AddWorkFlowStepResult>();

            if (createWorkflowResult == null) return addWorkFlowStepResults;
            if (loginResult == null) return addWorkFlowStepResults;

            var token = new Token
            {
                Token = loginResult.Token,
                TokenExpiry = loginResult.TokenExpiry
            };

            var result = new WorkflowStepResponse();

            foreach (var workflowStepRequest in sfUserData.Select(item => new WorkflowStepRequest
            {
                DocID = createWorkflowResult.DocId.ToString(),
                Email = item.Email,
                Name = item.Name,
                Cell = item.Cell,
                Action = Actions.SignDocument,
                Token = token
            }))
            {
                result = (WorkflowStepResponse)SignFlowInitialiser(SignFlowWebMethod.AddWorkFlowStep, workflowStepRequest);
            }

            var flowStepsResults = new List<FlowStepsResult>();
            var flowStepsResult = new FlowStepsResult();

            foreach (var itemFlowStep in result.FlowSteps)
            {
                flowStepsResult.Email = itemFlowStep.Email;
                flowStepsResult.Action = itemFlowStep.Action;
                flowStepsResult.FlowID = itemFlowStep.FlowID;
                flowStepsResults.Add(flowStepsResult);
            }

            var addWorkFlowStepResult = new AddWorkFlowStepResult
            {
                LoginResult = loginResult,
                CreateWorkflowResult = createWorkflowResult,
                DocId = result.DocID,
                FlowStepsResult = flowStepsResults,
                Result = result.Result
            };

            addWorkFlowStepResults.Add(addWorkFlowStepResult);

            return addWorkFlowStepResults;
        }

        public List<InitiateFlowResult> InitiateFlow(List<AddWorkFlowStepResult> addWorkFlowStepResults, LoginResult loginResult)
        {
            var initiateFlowResults = new List<InitiateFlowResult>();
            var initiateFlowResult = new InitiateFlowResult();

            if (loginResult == null) return initiateFlowResults;
            if (addWorkFlowStepResults == null) return initiateFlowResults;

            var token = new Token
            {
                Token = loginResult.Token,
                TokenExpiry = loginResult.TokenExpiry
            };

            foreach (var result in addWorkFlowStepResults.Select(item => new InitiateFlowRequest()
            {
                DocID = item.DocId.ToString(),
                Token = token
            }).Select(request => (InitiateFlowResponse)SignFlowInitialiser(SignFlowWebMethod.InitiateFlow, request)))
            {
                initiateFlowResult.DocID = result.DocID;
                initiateFlowResult.Result = result.Result;
                initiateFlowResults.Add(initiateFlowResult);
            }

            return initiateFlowResults;
        }

        public GetDocStatusResult GetDocStatus(LoginResult loginResult, int docId)
        {
            var getDocStatusResult = new GetDocStatusResult();

            if (docId <= 0) return getDocStatusResult;
            if (loginResult == null) return getDocStatusResult;

            var token = new Token
            {
                Token = loginResult.Token,
                TokenExpiry = loginResult.TokenExpiry
            };

            var request = new GetDocStatusRequest
            {
                DocID = docId,
                Token = token
            };

            var result = (GetDocStatusResponse)SignFlowInitialiser(SignFlowWebMethod.GetDocStatus, request);

            getDocStatusResult.Result = result.Result;
            getDocStatusResult.DocStatus = result.DocStatus;

            return getDocStatusResult;
        }

        public GetDocResult GetDoc(GetDocStatusResult getDocStatusResult, LoginResult loginResult, int docId)
        {
            var getDocResult = new GetDocResult();

            if (loginResult == null) return getDocResult;
            if (getDocStatusResult == null) return getDocResult;
            if (docId <= 0) return getDocResult;

            if (getDocStatusResult.DocStatus != "Completed") return getDocResult;

            var token = new Token
            {
                Token = loginResult.Token,
                TokenExpiry = loginResult.TokenExpiry
            };

            var request = new GetDocRequest
            {
                DocID = docId,
                Token = token
            };

            var result = (GetDocResponse)SignFlowInitialiser(SignFlowWebMethod.GetDoc, request);

            getDocResult.Result = result.Result;
            getDocResult.Doc = result.Doc;
            getDocResult.DocName = result.DocName;
            getDocResult.Extension = result.Extension;

            var storage = Infrastructure.Configuration.ConfigurationReader.SignFlowSetting.SignFlowCompletePath;

            var folderPath = storage;

            var bytes = Convert.FromBase64String(getDocResult.Doc);
            var path = string.Format(@"{0}\{1}.{2}", folderPath, string.Concat(getDocResult.DocName, "_", docId), getDocResult.Extension);

            var directoryInfo = new DirectoryInfo(folderPath);
            if (directoryInfo.Exists)
            {
                if (!File.Exists(path))
                {
                    var stream = new FileStream(path, FileMode.CreateNew);
                    var writer = new BinaryWriter(stream);
                    writer.Write(bytes, 0, bytes.Length);
                    writer.Close();
                }
                else
                {
                    File.Delete(path);
                    var stream = new FileStream(path, FileMode.CreateNew);
                    var writer = new BinaryWriter(stream);
                    writer.Write(bytes, 0, bytes.Length);
                    writer.Close();
                }
            }
            else
            {
                directoryInfo.Create();
            }

            return getDocResult;
        }

        public DocPrepperAddFieldRequest.FieldType GetFieldType(string fieldTypeName)
        {
            switch (fieldTypeName)
            {
                case "Signature":
                    return DocPrepperAddFieldRequest.FieldType.Signature;

                case "PlainText":
                    return DocPrepperAddFieldRequest.FieldType.PlainText;

                case "PlainTextOptional":
                    return DocPrepperAddFieldRequest.FieldType.PlainTextOptional;
            }

            return new DocPrepperAddFieldRequest.FieldType();
        }

        public List<DocPrepperAddFieldResult> DocPrepperAddFields(LoginResult loginResult, int docId, List<SignFlowUserData> sfUserData)
        {
            var docPrepperAddFieldResults = new List<DocPrepperAddFieldResult>();
            var docPrepperAddFieldResult = new DocPrepperAddFieldResult();

            if (loginResult == null) return docPrepperAddFieldResults;
            if (docId <= 0) return docPrepperAddFieldResults;

            var token = new Token
            {
                Token = loginResult.Token,
                TokenExpiry = loginResult.TokenExpiry
            };


            foreach (var item in sfUserData)
            {
                var docPrepperAddFieldRequest = new DocPrepperAddFieldRequest
                {
                    DocID = docId,
                    Token = token,
                    UserEmail = item.Email, //sfUserData.FirstOrDefault().Email,
                    //TypeField = DocPrepperAddFieldRequest.FieldType.Signature
                };
                // senderEmail, //Infrastructure.Configuration.ConfigurationReader.SignFlowSetting.SignFlowApiUserName,

                var docDefinitionNames = item.DocDefinitions.Select(y => new DocumentDefinitions().FirstOrDefault(x => x.Id == y.Id)).ToList();

                foreach (var doc in docDefinitionNames)
                {
                    docPrepperAddFieldRequest.XCoordinate = doc.XCoordinate.ToString();
                    docPrepperAddFieldRequest.YCoordinate = doc.YCoordinate.ToString();
                    docPrepperAddFieldRequest.FieldWidth = doc.FieldWidth.ToString();
                    docPrepperAddFieldRequest.FieldHeight = doc.FieldHeight.ToString();
                    docPrepperAddFieldRequest.PageNumber = doc.PageNumber;
                    docPrepperAddFieldRequest.TypeField = GetFieldType(doc.DocumentFieldType.Name);

                    var result = (DocPrepperAddFieldResponse)SignFlowInitialiser(SignFlowWebMethod.DocPrepperAddFields, docPrepperAddFieldRequest);

                    docPrepperAddFieldResult.PrepperFieldId = result.PrepperFieldID;
                    docPrepperAddFieldResult.DocId = result.DocID;
                    docPrepperAddFieldResult.UserId = result.UserID;
                    docPrepperAddFieldResult.FieldType = result.FieldType;
                    docPrepperAddFieldResult.XCoordinate = result.XCoordinate;
                    docPrepperAddFieldResult.YCoordinate = result.YCoordinate;
                    docPrepperAddFieldResult.FieldWidth = result.FieldWidth;
                    docPrepperAddFieldResult.FieldHeight = result.FieldHeight;
                    docPrepperAddFieldResult.PageNumber = result.PageNumber;
                    docPrepperAddFieldResult.Result = result.Result;
                    docPrepperAddFieldResults.Add(docPrepperAddFieldResult);

                }
            }

            return docPrepperAddFieldResults;
        }

        public AddUserResult AddUser(LoginResult loginResult)
        {
            var addUserResult = new AddUserResult();

            if (loginResult == null) return addUserResult;

            var token = new Token
            {
                Token = loginResult.Token,
                TokenExpiry = loginResult.TokenExpiry
            };

            var request = new AddUserRequest
            {
                UserFirstName = "Taariq",
                UserFullName = "Gh",
                UserLastName = "Gh",
                UserEmail = "taariqh.gh@gmail.com",
                UserMobile = "2305913572",
                UserIDType = IDType.Passport,
                UserIdentificationNumber = "13351881111",
                Token = token
            };

            var result = (AddUserResponse)SignFlowInitialiser(SignFlowWebMethod.AddUser, request);

            addUserResult.Result = result.Result;
            addUserResult.UserId = result.UserID;

            return addUserResult;
        }

        public DocExtension ExtensionManager(string file)
        {
            if (file.Contains(".doc"))
            {
                return DocExtension.doc;
            }

            if (file.Contains(".docx"))
            {
                return DocExtension.docx;
            }

            return DocExtension.pdf;
        }
    }
}

