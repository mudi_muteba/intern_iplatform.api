﻿using AutoMapper;
using SignFlow.Model;

namespace SignFlow.Mappings
{
    public class SignFlowConnectorMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<SignFlowAPI.LoginResponse, LoginResult>();

            Mapper.CreateMap<SignFlowAPI.AddDocResponse, CreateWorkflowResult>();

            Mapper.CreateMap<SignFlowAPI.WorkflowStepResponse, AddWorkFlowStepResult>();

            Mapper.CreateMap<SignFlowAPI.InitiateFlowResponse, InitiateFlowResult>();

            Mapper.CreateMap<SignFlowAPI.GetDocStatusResponse, GetDocStatusResult>();

            Mapper.CreateMap<SignFlowAPI.GetDocResponse, GetDocResult>();

            Mapper.CreateMap<SignFlowAPI.DocPrepperAddFieldResponse, DocPrepperAddFieldResult>(); 
        }
    }
}
