﻿namespace SignFlow.Model
{
    public enum DocPrepperField
    {
        Signature,
        PlainText,
        PlainTextOptional
    }
}
