﻿namespace SignFlow.Model
{
    public class CreateWorkflowResult
    {
        public int DocId { get; set; }
        public string Result { get; set; }
    }
}
