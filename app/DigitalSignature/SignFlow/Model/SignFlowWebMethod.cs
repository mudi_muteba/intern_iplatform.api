﻿namespace SignFlow.Model
{
    public enum SignFlowWebMethod
    {
        Login,
        CreateWorkflow,
        AddWorkFlowStep,
        InitiateFlow,
        GetDocStatus,
        GetDoc,
        DocPrepperAddFields,
        AddUser
    }
}
