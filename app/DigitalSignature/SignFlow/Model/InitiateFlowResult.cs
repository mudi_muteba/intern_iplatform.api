﻿namespace SignFlow.Model
{
    public class InitiateFlowResult
    {
        public int DocID { get; set; }

        public string Result { get; set; }
    }
}
