﻿namespace SignFlow.Model
{
    public class GetDocResult
    {
        public string DocName { get; set; }
        public string Extension { get; set; }
        public string Doc { get; set; }
        public string Result { get; set; }
    }
}
