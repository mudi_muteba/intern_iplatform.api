﻿namespace SignFlow.Model
{
    public class AddUserResult
    {
        public string Result { get; set; }
        public string UserId { get; set; }
    }
}
