﻿namespace SignFlow.Model
{
    public class GetDocStatusResult
    {
        public string DocStatus { get; set; }

        public string Result { get; set; }
    }
}
