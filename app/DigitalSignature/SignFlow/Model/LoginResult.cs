﻿using System;

namespace SignFlow.Model
{
    public class LoginResult
    {
        public string Token { get; set; }
        public DateTime TokenExpiry { get; set; }
        public string Result { get; set; }
    }
}
