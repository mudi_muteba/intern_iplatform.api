﻿using System.Collections.Generic;

namespace SignFlow.Model
{
    public class AddWorkFlowStepResult
    {
        public CreateWorkflowResult CreateWorkflowResult { get; set; }

        public LoginResult LoginResult { get; set; }

        public int DocId { get; set; }

        public List<FlowStepsResult> FlowStepsResult { get; set; }

        public string Result { get; set; }
    }
}
