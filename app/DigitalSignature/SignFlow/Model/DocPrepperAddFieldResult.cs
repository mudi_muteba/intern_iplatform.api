﻿namespace SignFlow.Model
{
    public class DocPrepperAddFieldResult
    {
        public int PrepperFieldId { get; set; }
        public int DocId { get; set; }
        public int UserId { get; set; }
        public string FieldType { get; set; }
        public string XCoordinate { get; set; }
        public string YCoordinate { get; set; }
        public string FieldWidth { get; set; }
        public string FieldHeight { get; set; }
        public int PageNumber { get; set; }
        public string Result { get; set; }
    }
}
