﻿namespace SignFlow.Model
{
    public class FlowStepsResult
    {
        public int FlowID { get; set; }

        public string Email { get; set; }

        public string Action { get; set; }
    }
}
