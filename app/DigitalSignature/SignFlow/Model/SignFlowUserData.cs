﻿using System.Collections.Generic;
using MasterData;

namespace SignFlow.Model
{
    public class SignFlowUserData
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Cell { get; set; }
        public List<DocumentDefinition> DocDefinitions { get; set; }
    }
}
