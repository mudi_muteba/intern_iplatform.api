﻿using System;
using System.Threading;
using Acceptance.UI.Tests.UI.Helpers.Models;
using OpenQA.Selenium;
using TestStack.Seleno.PageObjects;

namespace Acceptance.UI.Tests.UI.Helpers
{
    public class LoginPage : PageBase
    {
        protected internal string Username { set { Execute.ActionOnLocator(By.Id("Username"), e => { e.Clear(); e.SendKeys(value); }); } }
        protected internal string Password { set { Execute.ActionOnLocator(By.Id("Password"), e => { e.Clear(); e.SendKeys(value); }); } }

        public LoginPage WithModel(LoginViewModel model)
        {
            
            //delay for fancy but annoying video
            Thread.Sleep(TimeSpan.FromSeconds(20));
            WaitFor. AjaxCallsToComplete(TimeSpan.FromSeconds(120));

            Username = model.Username;
            Password = model.Password;
            return this;
        }

        public LandingPage<T> Login<T>() where T : PageBase, IPropsalPage, new()
        {
            return Navigate.To<LandingPage<T>>(By.Id("login-btn"));
        }

        public Page Login()
        {
            return Navigate.To<LandingPage<PropsalPage>>(By.Id("login-btn"));
        }
    }
}