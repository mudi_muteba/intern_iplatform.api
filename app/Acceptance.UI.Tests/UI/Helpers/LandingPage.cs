﻿using System;
using OpenQA.Selenium;
using TestStack.Seleno.PageObjects;

namespace Acceptance.UI.Tests.UI.Helpers
{
    public class LandingPage<T> : PageBase where T : UiComponent, IPropsalPage, new()
    {
        protected internal string leadSearch { set { Execute.ActionOnLocator(By.Id("leadSearch"), e => { e.SendKeys(value); }); } }





        public E GoToCreateLead<E>() where E : UiComponent, new()
        {
            return Navigate.To<E>(By.Id("btnCreateLead"));
        }



        public bool Search(string searchTerm)
        {
            try
            {
                leadSearch = searchTerm;
                var element = Execute.ActionOnLocator(By.PartialLinkText(searchTerm), e => { e.Click(); });
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}
