﻿using Acceptance.UI.Tests.UI.Helpers.Models;

namespace Acceptance.UI.Tests.UI.Helpers.TestData
{
    public static class LoginData
    {
        public static LoginViewModel GetAdminLogin()
        {
            var model = new LoginViewModel();
            model.Username = "admin@ibroker.co.za";
            model.Password = "password";
            return model;
        }
        public static LoginViewModel GetAutoTestLogin()
        {
            var model = new LoginViewModel();
            model.Username = "autotest@iplatform.co.za";
            model.Password = ">B8WH_d~AYqg]k$Y";
            return model;
        }
        public static LoginViewModel GetTestLogin()
        {
            var model = new LoginViewModel();
            model.Username = "Test@Iplatform.Co.Za";
            model.Password = "password";
            return model;
        }
    }
}
