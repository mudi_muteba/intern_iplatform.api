﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TestStack.Seleno.PageObjects;

namespace Acceptance.UI.Tests.UI.Helpers
{
    public abstract class PageBase : Page
    {
        public bool IsLinkTextOnPage(string text)
        {
            WaitFor.AjaxCallsToComplete(TimeSpan.FromSeconds(120));
            var item = Find.OptionalElement(By.LinkText(text));
            return item != null;
        }
        public bool IsTextOnPage(string text)
        {
            var list = Find.OptionalElement(By.XPath("//*[contains(text(),'" + text + "')]"));
            if (list == null)
                return false;
            return true;
        }

        public void SetDate(string id, string date)
        {
            var datearray = date.Split('-');
            var strmonth = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(datearray[1]));
            //Select date picker
            Find.Element(By.Id(id)).Click();

            //Get datepicker
            IWebElement datepicker = Find.Element(By.ClassName("datepicker"));
            datepicker.FindElement(By.ClassName("datepicker-switch")).Click();//will give month
            datepicker.FindElement(By.ClassName("datepicker-months")).FindElement(By.ClassName("datepicker-switch")).Click();//will give year
            Find.Element(By.XPath("//span[contains(text(), '" + datearray[2] + "')]")).Click(); //get year
            Find.Element(By.XPath("//span[contains(text(), '" + strmonth + "')]")).Click(); //get month
            Find.Element(By.XPath("//td[contains(text(), '" + datearray[0] + "')]")).Click(); //get day
        }
        public void Select2Options(string id, List<string> values)
        {
            IWebElement element = Find.Element(By.Id(id));
            element.Click();
            var select = Find.Element(By.ClassName("select2-choices"));


            for (int i = 0; i < values.Count(); i++)
            {
                if (i > 0)
                    element.Click();

                var item = select.FindElement(By.XPath(String.Format("//li[contains(translate(., '{0}', '{1}'), '{1}')]", values[i].ToUpper(), values[i].ToLower())));
                item.Click();
            }
        }
        public void SelectOption(string id, string value)
        {
            IWebElement comboBox = Find.Element(By.Id(id));
            SelectElement selector = new SelectElement(comboBox);
            selector.SelectByText(value);
        }
        public void Select2First(string id)
        {
            IWebElement element = Find.Element(By.Id(id));
            element.Click();

            var selectedItem = Find.Elements(By.ClassName("select2-result")).First();
            selectedItem.Click();
        }
        public void SelectFirst(string id)
        {
            IWebElement comboBox = Find.Element(By.Id(id));
            SelectElement selector = new SelectElement(comboBox);
            selector.SelectByIndex(1);
        }

        public void SelectByDescription(string id, string description)
        {
            IWebElement comboBox = Find.Element(By.Id(id));
            SelectElement selector = new SelectElement(comboBox);
            selector.SelectByText(description);
        }

        public bool Checked(string id)
        {
            return Find.Element(By.Id(id)).Selected;
        }
    }
}
