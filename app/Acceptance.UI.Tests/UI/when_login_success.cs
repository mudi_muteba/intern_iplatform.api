using System;
using Acceptance.UI.Tests.UI.Helpers;
using Acceptance.UI.Tests.UI.Helpers.TestData;
using TestHelper;
using TestStack.Seleno.PageObjects;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.UI.Tests.UI
{
    public class when_login_success : Specification, IDisposable
    {

        private LandingPage<PropsalPage> _landingPage;
        private Page _result;
        private WebHost _host;

        public when_login_success()
        {
            _host = new WebHost();
        }

        public override void Observe()
        {
            _result = _host.SelenoInstance.NavigateToInitialPage<LoginPage>()
             .WithModel(LoginData.GetAutoTestLogin())
             .Login();

        }

        //[Observation]
        protected void assert_all()
        {
            Assert.True(_result.Title.ToLower().Equals("iplatform"));
        }

        public void the_login_succeeded()
        {

        }

        public void Dispose()
        {
            _host.Dispose();
        }
    }
}