﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Drawing.Imaging;
using Castle.Core.Logging;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using TestStack.Seleno.Configuration;
using TestStack.Seleno.Configuration.WebServers;

namespace Acceptance.UI.Tests.UI
{
    public class WebHost : IDisposable
    {

        public SelenoHost SelenoInstance = new SelenoHost();

        public WebHost()
        {
            //var webHost = ConfigurationManager.AppSettings[@"SelenoURL"];
            var browserFactory = ConfigurationManager.AppSettings[@"BrowserFactory"];
            var webHost = "http://staging.iplatform.co.za/account/login";
            //var webHost = "http://dev.iplatform.web/";
            //var webHost = "http://demo.iplatform.co.za/account/login?ReturnUrl=%2F";


           // var browser = browserFactory == "PhantomJS" ? BrowserFactory.PhantomJS : BrowserFactory.Chrome)
            ;

                SelenoInstance.Run(configure => configure
                    .WithRemoteWebDriver(() => browserFactory == "PhantomJS" ? (RemoteWebDriver) BrowserFactory.PhantomJS() : BrowserFactory.Chrome())
                    .UsingLoggerFactory(new ConsoleFactory())
                    .WithMinimumWaitTimeoutOf(new TimeSpan())
                    .WithWebServer(new InternetWebServer(webHost)));

                SelenoInstance.Application.Browser.Manage().Window.Maximize();
        }


        public void Refresh()
        {
            SelenoInstance.Application.Browser.Navigate().Refresh();
        }

        public void TakeScreenShot(string filename)
        {
            var dateTime = string.Format("{0}{1}{2}{3}{4}{5}",
                DateTime.Now.Year,
                DateTime.Now.Month.ToString().PadLeft(2,'0')
                , DateTime.Now.Day.ToString().PadLeft(2, '0'),
                DateTime.Now.Hour.ToString().PadLeft(2, '0'),
                DateTime.Now.Minute.ToString().PadLeft(2, '0'),
                DateTime.Now.Second.ToString().PadLeft(2, '0'));
            Screenshot image = SelenoInstance.Application.Camera.ScreenshotTaker.GetScreenshot();
            image.SaveAsFile(string.Format("{0} - {1}.png", dateTime, filename), ImageFormat.Png);

        }

        public void Dispose()
        {
            try
            {
                SelenoInstance.Application.Browser.Quit();

                foreach (var process in Process.GetProcessesByName("chromedriver.exe"))
                    process.Kill();
                SelenoInstance.Dispose();
            }
            catch (Exception)
            {
                
            }
        }
    }
}