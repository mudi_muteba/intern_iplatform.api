﻿using System;

namespace iPlatform.OutboundCommunication.SMS.CellFind
{
    public interface ICellFindClient : IDisposable
    {
        long SendSMSMessage(string Token, string RefNo, string PhoneNo, string MsgType, string Message, string CostClass, long Priority);

        string Url { get; set; }

        string Login(string Username, string Password);

        string GetMessageStatus(string Token, long MessageID);

        string Logout(string Token);
    }
}