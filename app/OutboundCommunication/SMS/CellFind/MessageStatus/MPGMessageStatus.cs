using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace iPlatform.OutboundCommunication.SMS.CellFind.MessageStatus
{
    /// <remarks/>
    [Serializable()]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public partial class MPGMessageStatus
    {
        /// <remarks/>
        public MPGMessageStatusMsgStat MsgStat { get; set; }

        public override string ToString()
        {
            return string.Format("StatTime: {0}, StatText: {1}, Status: {2}"
                , MsgStat.StatTime
                , MsgStat.StatText
                , MsgStat.Status);
        }

        public string TranslateStatus()
        {
            if (MsgStat == null)
            {
                return "Unknown status";
            }

            return string.Format("{0} ({1})"
                , MsgStat.StatText
                , MsgStat.Status);
        }
    }
}