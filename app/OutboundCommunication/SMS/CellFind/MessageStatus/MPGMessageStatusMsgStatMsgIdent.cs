using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace iPlatform.OutboundCommunication.SMS.CellFind.MessageStatus
{
    /// <remarks/>
    [Serializable()]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public partial class MPGMessageStatusMsgStatMsgIdent
    {
        private string batchNoField;

        private string clientAppField;

        private uint msgIDField;

        private string protField;

        private ulong rcptField;

        private string refField;

        private string templateNameField;

        /// <remarks/>
        [XmlAttribute()]
        public uint MsgID
        {
            get { return this.msgIDField; }
            set { this.msgIDField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public string ClientApp
        {
            get { return this.clientAppField; }
            set { this.clientAppField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public string BatchNo
        {
            get { return this.batchNoField; }
            set { this.batchNoField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public string Prot
        {
            get { return this.protField; }
            set { this.protField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public ulong Rcpt
        {
            get { return this.rcptField; }
            set { this.rcptField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public string Ref
        {
            get { return this.refField; }
            set { this.refField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public string TemplateName
        {
            get { return this.templateNameField; }
            set { this.templateNameField = value; }
        }
    }
}