﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace iPlatform.OutboundCommunication.SMS.CellFind.MessageStatus
{
    /// <remarks/>
    [Serializable()]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public partial class MPGMessageStatusMsgStat
    {
        private MPGMessageStatusMsgStatMsgIdent msgIdentField;

        private string statTextField;

        private string statTimeField;

        private byte statusField;

        /// <remarks/>
        public MPGMessageStatusMsgStatMsgIdent MsgIdent
        {
            get { return this.msgIdentField; }
            set { this.msgIdentField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public string StatTime
        {
            get { return this.statTimeField; }
            set { this.statTimeField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public string StatText
        {
            get { return this.statTextField; }
            set { this.statTextField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public byte Status
        {
            get { return this.statusField; }
            set { this.statusField = value; }
        }
    }
}