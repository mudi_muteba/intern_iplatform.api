﻿using System;
using System.IO;
using System.Xml.Serialization;
using Common.Logging;
using iPlatform.OutboundCommunication.SMS.CellFind.MessageStatus;
using Shared;
using Shared.Retry;

namespace iPlatform.OutboundCommunication.SMS.CellFind
{
    internal class CellFindStatus
    {
        private readonly ICellFindClient client;
        private static readonly ILog log = LogManager.GetLogger<CellFindStatus>();

        public CellFindStatus(ICellFindClient client)
        {
            this.client = client;
        }

        public MPGMessageStatus GetStatus(string token, long messageId)
        {
            MPGMessageStatus status = null;
            var statusAvailable = false;

            RepeatAction
                .While(() => !statusAvailable)
                .RetryAfter(2.Seconds())
                .UpTo(10.Times())
                .Do(() =>
                {
                    status = QueryStatus(token, messageId);
                    statusAvailable = IsStatusAvailable(status);
                });

            return status;
        }

        private bool IsStatusAvailable(MPGMessageStatus status)
        {
            if (status == null)
                return false;

            return status.MsgStat != null;
        }

        private MPGMessageStatus QueryStatus(string token, long messageId)
        {
            MPGMessageStatus status = null;
            try
            {
                var statusText = client.GetMessageStatus(token, messageId);
                var serializer = new XmlSerializer(typeof(MPGMessageStatus));
                using (var reader = new StringReader(statusText))
                {
                    status = serializer.Deserialize(reader) as MPGMessageStatus;
                }
            }
            catch (Exception e)
            {
                log.ErrorFormat("Failed to deserialise status response. The exception was {0}"
                    , new ExceptionPrettyPrinter().Print(e));
            }

            return status;
        }
    }
}