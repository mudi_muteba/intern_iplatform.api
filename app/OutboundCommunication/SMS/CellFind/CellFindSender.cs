﻿using System;
using Common.Logging;
using iPlatform.OutboundCommunication.SMS.CellFind.MessageStatus;
using Shared;
using Shared.Retry;

namespace iPlatform.OutboundCommunication.SMS.CellFind
{
    public class CellFindSender : ISendSMS
    {
        private static readonly ILog log = LogManager.GetLogger<CellFindSender>();
        private readonly ICellFindClient client;

        public CellFindSender(ICellFindClient client)
        {
            this.client = client;
        }

        public SMSSendingResult Send(SMSProviderConfiguration configuration, SMSMessage message)
        {
            using (client)
            {
                ConfigureClient(configuration);

                var token = Login(configuration);
                if (string.IsNullOrWhiteSpace(token))
                    return new SMSSendingResult(false, string.Empty, "Login Failed");

                var messageId = SendMessage(token, message);

                var status = GetMessageStatus(token, messageId);

                LogOff(token);

                return new SMSSendingResult(true, messageId.ToString()
                    , status == null ? "Unknown status" : status.TranslateStatus());
            }
        }

        private void LogOff(string token)
        {
            var response = TryCall(() => client.Logout(token));
            if (string.IsNullOrWhiteSpace(response))
            {
                log.ErrorFormat("Failed to logout. Check logs for more information");
            }
        }

        private void ConfigureClient(SMSProviderConfiguration configuration)
        {
            client.Url = configuration.Url;
        }

        private MPGMessageStatus GetMessageStatus(string token, long messageId)
        {
            return new CellFindStatus(client).GetStatus(token, messageId);
        }

        private long SendMessage(string token, SMSMessage message)
        {
            var sendingResult = TryCall(() => client.SendSMSMessage(token
                , message.Reference
                , message.PhoneNumber
                , "1"
                , message.Text
                , string.Empty
                , 1));

            if (sendingResult == 0)
            {
                log.ErrorFormat("Failed to send message. Check logs for more information");
            }

            return sendingResult;
        }

        private string Login(SMSProviderConfiguration configuration)
        {
            var token = TryCall(() => client.Login(configuration.UserName, configuration.Password));
            if (string.IsNullOrWhiteSpace(token))
            {
                log.ErrorFormat("Failed to login into CallFind. Check logs for more information");
                return string.Empty;
            }


            return token;
        }

        private TReturn TryCall<TReturn>(Func<TReturn> call)
        {
            var completed = false;
            var returnValue = default(TReturn);

            RepeatAction
                .While(() => !completed)
                .UpTo(10.Times())
                .RetryAfter(5.Seconds())
                .Do(() =>
                {
                    try
                    {
                        returnValue = call();
                        completed = true;
                    }
                    catch (Exception e)
                    {
                        log.ErrorFormat("Failed to complete call the CallFind. The exception was: {0}"
                            , new ExceptionPrettyPrinter().Print(e));

                        completed = false;
                    }
                });

            return returnValue;
        }
    }
}