﻿namespace iPlatform.OutboundCommunication.SMS
{
    public class SMSMessage
    {
        public SMSMessage(string phoneNumber, string text, string reference)
        {
            PhoneNumber = phoneNumber;
            Text = text;
            Reference = reference;
        }

        public string PhoneNumber { get; private set; }
        public string Text { get; private set; }
        public string Reference { get; private set; }
    }
}