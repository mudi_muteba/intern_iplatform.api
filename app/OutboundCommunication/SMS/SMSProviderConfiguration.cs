namespace iPlatform.OutboundCommunication.SMS
{
    public class SMSProviderConfiguration
    {
        public SMSProviderConfiguration()
        {
        }

        public SMSProviderConfiguration(string providerName, string userName, string password, string url)
        {
            ProviderName = providerName;
            UserName = userName;
            Password = password;
            Url = url;
        }

        public string ProviderName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Url { get; set; }
    }
}