namespace iPlatform.OutboundCommunication.SMS
{
    public interface ICreateSMSSenders
    {
        ISendSMS Create(SMSProviderConfiguration provider);
    }
}