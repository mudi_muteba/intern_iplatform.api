namespace iPlatform.OutboundCommunication.SMS
{
    public interface ISendSMS
    {
        SMSSendingResult Send(SMSProviderConfiguration configuration, SMSMessage message);
    }
}