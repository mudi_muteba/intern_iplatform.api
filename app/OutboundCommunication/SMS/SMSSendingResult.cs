﻿using Shared;
using System;

namespace iPlatform.OutboundCommunication.SMS
{
    public class SMSSendingResult
    {
        public SMSSendingResult(bool completed, string messageId, string status)
        {
            Completed = completed;
            MessageId = messageId;
            Status = status;
            CompletedOn = SystemTime.Now();
        }

        public bool Completed { get; private set; }
        public string MessageId { get; private set; }
        public string Status { get; private set; }
        public DateTime CompletedOn { get; private set; }

        public override string ToString()
        {
            if (!Completed)
            {
                return string.Format("CellFind SMS sending did not complete at {0}. Status was {1}"
                    , CompletedOn
                    , Status);
            }

            return string.Format("MessageID: {0} sent at {1} with status {2}"
                , MessageId
                , CompletedOn
                , Status);
        }
    }
}