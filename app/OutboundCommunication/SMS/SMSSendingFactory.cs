using System;
using Common.Logging;
using iPlatform.OutboundCommunication.SMS.CellFind;
using iPlatform.OutboundCommunication.za.co.cellfind.cfwinqa;

namespace iPlatform.OutboundCommunication.SMS
{
    public class SMSSendingFactory : ICreateSMSSenders
    {
        private static readonly ILog log = LogManager.GetLogger<SMSSendingFactory>();

        public ISendSMS Create(SMSProviderConfiguration provider)
        {
            if (provider == null)
            {
                var message = "Could not decide on SMS provider as the provider is NULL";
                log.ErrorFormat(message);
                throw new Exception(message);
            }

            if("CellFind".Equals(provider.ProviderName, StringComparison.InvariantCultureIgnoreCase))
                return new CellFindSender(new IMPGWS());

            var notFoundMessage = string.Format("Could not find an SMS provider called {0}", provider.ProviderName);
            log.ErrorFormat(notFoundMessage);
            throw new Exception(notFoundMessage);
        }
    }
}