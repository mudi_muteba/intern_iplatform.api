﻿using CustomerSatisfactionSurvey.Domain.Data;

namespace CustomerSatisfactionSurvey.Domain
{
    public interface ICustomerSatisfactionSurveyService
    {
        void Send(CustomerSatisfactionSurveyRequest request);
    }
}
