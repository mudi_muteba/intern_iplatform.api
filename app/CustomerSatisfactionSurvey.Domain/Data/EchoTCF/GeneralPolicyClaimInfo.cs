﻿using System;
using System.Xml.Serialization;

namespace CustomerSatisfactionSurvey.Domain.Data.EchoTCF
{
    public class GeneralPolicyClaimInfo
    {
        /*
           <GeneralPolicyClaimInfo>
            <TriggerEvent>1</TriggerEvent>
            <EntityType>1</EntityType>
            <EntityCode><![CDATA[IEDF]]></EntityCode>
            <EntityName><![CDATA[IEDF Brokers]]></EntityName>
            <PolicyHolder><![CDATA[James Earl]]></PolicyHolder>
            <ClientType><![CDATA[1]]></ClientType>
            <IDCoRegNo><![CDATA[7608088374081]]></IDCoRegNo>
            <PolicyClaimNumber><![CDATA[ACME-00S454335]]></PolicyClaimNumber>
            <IsPolicy>1</IsPolicy>
            <IsClaim>0</IsClaim>
            <PolicyHolderCell><![CDATA[27821234567]]></PolicyHolderCell>
            <PolicyHolderEmail><![CDATA[john@email.com]]></PolicyHolderEmail>
            <InsurerName><![CDATA[Mutual & Federal]]></InsurerName>
            <InsuranceType>1</InsuranceType>
            <EffectiveDate><![CDATA[2015-04-29]]></EffectiveDate>
          </GeneralPolicyClaimInfo>
         */

        public GeneralPolicyClaimInfo()
        {
            
        }

        public int TriggerEvent { get; set; }
        public int EntityType { get; set; }
        public CData EntityCode { get; set; }
        public CData EntityName { get; set; }
        public CData PolicyHolder { get; set; }
        public int ClientType { get; set; }
        public CData IDCoRegNo { get; set; }
        public CData PolicyClaimNumber { get; set; }
        public int IsPolicy { get; set; }
        public int IsClaim { get; set; }
        public CData PolicyHolderCell { get; set; }
        public CData PolicyHolderEmail { get; set; }
        public CData InsurerName { get; set; }
        public int InsuranceType { get; set; }
        public CData EffectiveDate { get; set; }
    }
}
