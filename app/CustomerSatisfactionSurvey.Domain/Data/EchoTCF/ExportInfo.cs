﻿using System;
using System.Globalization;
using System.Xml.Serialization;

namespace CustomerSatisfactionSurvey.Domain.Data.EchoTCF
{
    public class ExportInfo
    {
        public ExportInfo()
        {
            VendorSystem = "iPlatform";
        }

        public CData DateExported { get; set; }
        public CData TimeExported { get; set; }
        public CData VendorSystem { get; set; }
        public CData SystemUser { get; set; }
        public CData GeneralNotes { get; set; }

        /*
           <ExportInfo>
            <DateExported><![CDATA[17-Jul-2014]]></DateExported>
            <TimeExported><![CDATA[14:47:06 AM]]></TimeExported>
            <VendorSystem><![CDATA[Cims3]]></VendorSystem>
            <SystemUser><![CDATA[User A]]></SystemUser>
            <GeneralNotes><![CDATA[test phase]]></GeneralNotes>
          </ExportInfo>
         */


        public ExportInfo SetDateTime(DateTime ExportedDate)
        {
            DateExported = ExportedDate.ToString("dd-MMM-yyyy");
            TimeExported = ExportedDate.ToString("hh:mm:ss tt", CultureInfo.InvariantCulture);
            return this;
        }
    }
}
