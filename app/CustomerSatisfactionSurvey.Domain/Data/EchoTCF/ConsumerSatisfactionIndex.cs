﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerSatisfactionSurvey.Domain.Data.EchoTCF
{
    public class ConsumerSatisfactionIndex
    {
        public ConsumerSatisfactionIndex()
        {
            
        }

        public ExportInfo ExportInfo { get; set; }
        public GeneralPolicyClaimInfo GeneralPolicyClaimInfo { get; set; }
        
    }
}
