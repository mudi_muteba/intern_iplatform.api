﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Common.Logging;

namespace CustomerSatisfactionSurvey.Domain.Data
{
    public class CustomerSatisfactionSurveyRequest
    {
        protected static readonly ILog Log = LogManager.GetLogger<CustomerSatisfactionSurveyRequest>();
        public CustomerSatisfactionSurveyRequest()
        {
            _messages= new List<string>();
        }

        public CustomerSatisfactionSurveyRequest (string xmlRequest)
        {
            _messages = new List<string>();
            XmlRequest = xmlRequest;
        }

        public CustomerSatisfactionSurveyRequest(string xmlRequest, string token)
        {
            _messages = new List<string>();
            XmlRequest = xmlRequest;
            Token = token;
        }

        public CustomerSatisfactionSurveyRequest(string xmlRequest, string userName, string password)
        {
            _messages = new List<string>();
            XmlRequest = xmlRequest;
            Username = userName;
            Password = password;
        }

        public string XmlRequest { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }

        private readonly List<string> _messages;

        public bool Validate()
        {
            if (string.IsNullOrEmpty(XmlRequest))
                _messages.Add("XmlRequest cannot be empty");
            else
            {
                try
                {
                    var xml = new XmlDocument();
                    xml.LoadXml(XmlRequest);
                }
                catch
                {
                    _messages.Add("Unable to convert Xmlrequest to XmlDocument");
                }
            }

            Guid guid;
            if (string.IsNullOrEmpty(Token) && Guid.TryParse(Token, out guid))
                _messages.Add("invalid token");

            return !_messages.Any();
        }

        public void LogErrors()
        {
            foreach (var message in _messages)
            {
                Log.ErrorFormat(message);   
            }
        }

    }
}
