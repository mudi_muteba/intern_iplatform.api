﻿using System;
using System.ServiceModel;
using iPlatform.Api.DTOs.Policy.CimsSelfService;
using iPlatform.Integration.Services.CimsSelfServiceWebService;
using Newtonsoft.Json;

namespace iPlatform.Integration.Services.CimsSelfService
{
    public class CimsSelfServices
    {
        private readonly SelfServiceSoapClient m_Client;

        public CimsSelfServices()
        {
            m_Client = new SelfServiceSoapClient();
        }

        public CimsSelfServices(string endPointurl) : this()
        {
            m_Client.Endpoint.Address = new EndpointAddress(endPointurl);
        }

        public string Authenticate(SecuredWebServiceHeader request)
        {
            var result = m_Client.Authenticate(request);
            return result;
        }

        public string Register(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.Register(request, input);
            return result;
        }

        public string VerifyMember(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.VerifyMember(request, input);
            return result;
        }

        public string GetMemberDetails(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.GetMemberDetails(request, input);
            return result;
        }

        public SelfServiceGetPolicyInfoResponseDto GetPolicyInfo(SecuredWebServiceHeader request, SelfServiceGetPolicyInfoInputDto serviceInput)
        {
            return ExcecuteSelfServiceMethod<SelfServiceGetPolicyInfoInputDto, SelfServiceGetPolicyInfoResponseDto>(m_Client.GetPolicyInfo, request, serviceInput);
        }

        public string GetPolicies(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.GetPolicies(request, input);
            return result;
        }

        public SelfServiceGetPolicyScheduleResponseDto GetPolicySchedule(SecuredWebServiceHeader request, SelfServiceGetPolicyInfoInputDto serviceInput)
        {
            //Temporary untill we have the Idnumber
            serviceInput.IdNumber = serviceInput.PolicyNumber;
            return ExcecuteSelfServiceMethod<SelfServiceGetPolicyInfoInputDto, SelfServiceGetPolicyScheduleResponseDto>(m_Client.GetPolicySchedule, request, serviceInput);
        }

        public string GetClaims(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.GetClaims(request, input);
            return result;
        }

        public string GetFinancials(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.GetFinancials(request, input);
            return result;
        }

        public string GetClientCorrespondenceEmails(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.GetClientCorrespondenceEmails(request, input);
            return result;
        }

        public string GetClientCorrespondenceEmailAttachments(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.GetClientCorrespondenceEmailAttachments(request, input);
            return result;
        }

        public string GetClientCorrespondenceSms(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.GetClientCorrespondenceSMS(request, input);
            return result;
        }

        public string GetFnolPolicyList(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.GetClientCorrespondenceSMS(request, input);
            return result;
        }

        public string GetFnolEventTypeList(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.GetClientCorrespondenceSMS(request, input);
            return result;
        }

        public string GetFnolCoverPerilList(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.GetClientCorrespondenceSMS(request, input);
            return result;
        }

        public string LodgeClaim(SecuredWebServiceHeader request, string input)
        {
            var result = m_Client.GetClientCorrespondenceSMS(request, input);
            return result;
        }

        private T ExcecuteSelfServiceMethod<TIn1, T>(Func<SecuredWebServiceHeader, string, string> cimsSelfServiceMethod, SecuredWebServiceHeader header, TIn1 input1)
        {
            var stringCimsSelfServiceInput = JsonConvert.SerializeObject(input1);
            header.AuthenticatedToken = Authenticate(header);
            string response = cimsSelfServiceMethod(header, stringCimsSelfServiceInput);
            return JsonConvert.DeserializeObject<T>(response);
        }
    }
}