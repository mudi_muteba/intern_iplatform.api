﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using iPlatform.Integration.Services.SmartServiceReference;

namespace iPlatform.Integration.Services.SmartBinding
{
    public class SmartPolicyService
    {
        private smartPortTypeClient m_Client;

        public SmartPolicyService()
        {
            m_Client = new smartPortTypeClient();
            //m_Client.Endpoint.Address = new EndpointAddress("https://test.api.smartinsurance.com/v1/policyadmin2");
        }

        public Response GetPdfUrl(GetPDFUrlRequest request)
        {
            var result = m_Client.GetPDFUrl(request);
            return result;
        }

        public GetPolicyResponse GetPolicy(GetPolicyRequest request)
        {
            var result = m_Client.GetPolicy(request);
            return result;
        }

        public Response SendPdfPolicy(SendPdfPolicyRequest request)
        {
            var result = m_Client.SendPdfPolicy(request);
            return result;
        }

        public Response RegisterPolicy(RegisterPolicyRequest request)
        {
            var result = m_Client.RegisterPolicy(request);
            return result;
        }

        public Response UpdatePolicy(UpdatePolicyRequest request)
        {
            var result = m_Client.UpdatePolicy(request);
            return result;
        }



    }
}