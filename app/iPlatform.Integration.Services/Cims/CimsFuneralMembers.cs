﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Integration.Services.CimsServiceReference;

namespace iPlatform.Integration.Services.Cims
{
    public class CimsFuneralMembers
    {
        private CimsConnectClient _client;

        public CimsFuneralMembers()
        {
            _client = new CimsConnectClient();
        }

        public decimal Get(string idNumber, decimal sumInsured)
        {
            var dto = new MembersDto
            {
                IdNumber = idNumber,
                SumInsured = sumInsured
            };

            MembersDto[] list = new MembersDto[1];
            list[0] = dto;

            MembersDto[] result
                = _client.GetDualCovers(list);

            return result[0].SumInsured;
        }
    }
}
