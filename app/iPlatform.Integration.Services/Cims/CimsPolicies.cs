﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using iPlatform.Integration.Services.CimsServiceReference;
using iPlatform.Api.DTOs.Policy.FNOL;
using iPlatform.Api.DTOs.Claims.FNOL;

namespace iPlatform.Integration.Services.Cims
{
    public class CimsPolicies
    {
        private CimsConnectClient _client;
        private static readonly ILog log = LogManager.GetLogger<CimsConnectClient>();

        public CimsPolicies()
        {
            _client = new CimsConnectClient();
        }

        public FnolPolicyResponseDto GetSimplePoliciesForIDNumber(string idNumber, Guid requestGuid)
        {
            FnolPolicyResponseDto response = new FnolPolicyResponseDto()
            {
                Success = false
            };

            try
            {
                List<FnolPolicyDto> data = new List<FnolPolicyDto>();
                string result = _client.Fnol_PolicyList(idNumber, SearchTypesSearchType.ID_No, requestGuid);

                data = XmlHelper.XmlToObject<List<FnolPolicyDto>>(result);

                if (!data.Any())
                {
                    log.ErrorFormat("No policies returned from IDS");
                    log.ErrorFormat("IDS CimsConnectClient response: {0}",result);
                }
                log.InfoFormat("IDS CimsConnectClient response: {0}", result);

                response.Data = data;
                response.RawResponse = result;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Errors.Add(ex.Message);
                throw;
            }


            return response;
        }

        public FnolPolicyDataResponseDto GetDetailForPolicyGuid(Guid policyGuid, DateTime dateOfLoss, Guid requestGuid)
        {
            FnolPolicyDataResponseDto response = new FnolPolicyDataResponseDto()
            {
                Success = false
            };

            try
            {
                List<FnolPolicyDataDto> data = new List<FnolPolicyDataDto>();
                string result = _client.Fnol_PolicyData(policyGuid, dateOfLoss, requestGuid);

                data = XmlHelper.XmlToObject<List<FnolPolicyDataDto>>(result);

                response.Data = data;
                response.RawResponse = result;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Errors.Add(ex.Message);
                throw;
            }

            return response;
        }

        public FnolLodgeClaimResponseDto SubmitFnolClaim(ClaimFnolMasterDto submitDto, Guid requestGuid)
        {
            FnolLodgeClaimResponseDto response = new FnolLodgeClaimResponseDto()
            {
                Success = false
            };

            try
            {
                string claimMessage = XmlHelper.ObjectToXml(submitDto);

                string result = _client.Fnol_LodgeClaim(claimMessage, requestGuid);

                FnolLodgeClaimResponse data = XmlHelper.XmlToObject<FnolLodgeClaimResponse>(result);

                if (data != null)
                {
                    response.Success = true;
                    response.RawResponse = result;
                    response.Data = data;
                }
                else
                {
                    response.Errors.Add("No data was returned implying the claim was not submitted.");
                }

            }
            catch (Exception ex)
            {
                response.Errors.Add(ex.Message);
                throw;
            }

            return response;
        }

        //public FnolLodgeClaimResponseDto SubmitFnolClaim(ClaimFnolDto submitDto, Guid requestGuid)
        //{
        //    FnolLodgeClaimResponseDto response = new FnolLodgeClaimResponseDto()
        //    {
        //        Success = false
        //    };

        //    try
        //    {
        //        string result = _client.Fnol_LodgeClaim(XmlHelper.ObjectToXml(submitDto), requestGuid);

        //        FnolLodgeClaimResponse data = XmlHelper.XmlToObject<FnolLodgeClaimResponse>(result);

        //        if (data != null)
        //        {
        //            response.Success = true;
        //            response.RawResponse = result;
        //            response.Data = data;
        //        }
        //        else
        //        {
        //            response.Errors.Add("No data was returned implying the claim was not submitted.");
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        response.Errors.Add(ex.Message);
        //        throw;
        //    }

        //    return response;
        //}
    }
}
