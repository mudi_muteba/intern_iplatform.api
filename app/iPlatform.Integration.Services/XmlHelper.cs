﻿using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace iPlatform.Integration.Services
{
    public static class XmlHelper
    {
        public static string ObjectToXml<T>(T obj)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stringWriter = new StringWriter())
            {
                try
                {
                    serializer.Serialize(stringWriter, obj);
                    return stringWriter.ToString();
                }
                catch
                {
                    return null;
                }
            }
        }

        public static T XmlToObject<T>(string xml) where T : new()
        {
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                using (var stringReader = new StringReader(xml))
                {
                    using (var xmlTextReader = new XmlTextReader(stringReader))
                    {
                        try
                        {
                            var o = (T)serializer.Deserialize(xmlTextReader);
                            return o;
                        }
                        catch
                        {
                            return default(T);
                        }
                    }
                }
            }
            catch
            {
                return default(T);
            }
        }

        public static XElement ObjectToXElement<T>(T obj)
        {
            string xml = ObjectToXml(obj);
            if (!string.IsNullOrEmpty(xml))
            {
                return XElement.Parse(xml);
            }
            return null;
        }

        public static T XElementToObject<T>(XElement xElement) where T : new()
        {
            if (xElement == null)
                return default(T);
            string xml = xElement.ToString();
            return XmlToObject<T>(xml);
        }
    }
}
