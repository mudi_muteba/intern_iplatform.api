using Acceptance.Tests.Api.AssetBuilders;
using Acceptance.Tests.Api.Individual.AssetRiskItems;
using Acceptance.Tests.Api.Individual.Bank;
using Acceptance.Tests.Api.Leads;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api
{
    public class LocalDb_Tests_AssetBuilders : BaseTest
    {
        public LocalDb_Tests_AssetBuilders()
            : base(ApiUserObjectMother.AsAdmin())
        {
        }

        public override void Observe()
        {
        }

        [Observation]
        protected override void assert_all()
        {
            new LeadsTests(Connector).run_all();

        }
    }
}