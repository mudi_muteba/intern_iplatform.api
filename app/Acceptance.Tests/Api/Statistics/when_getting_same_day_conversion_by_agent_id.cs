﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Statistics;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Statistics
{
    public class when_getting_same_day_conversion_by_agent_id : BaseTest
    {
        private readonly GETResponseDto<StatisticsDto> _response;

        public when_getting_same_day_conversion_by_agent_id() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.LeadConversionStatsManagement.Statistics.GetSameDayConversionByAgent(86);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            ratings_are_returned();
        }

        public void ratings_are_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.ShouldNotBeNull();
        }
    }
}