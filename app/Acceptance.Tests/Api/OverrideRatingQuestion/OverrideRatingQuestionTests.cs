﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.OverrideRatingQuestion
{
    public class OverrideRatingQuestionTests
    {
        private readonly Connector _Connector;

        public OverrideRatingQuestionTests(Connector connector)
        {
            _Connector = connector;
            string token = new ConnectorTestData(_Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _Connector.SetToken(token);
        }

        public void run_all()
        {
            //new WhenCreatingOverrideRatingQuestionBddfy().Run(_Connector);
            new WhenEditingOverrideRatingQuestionBddfy().Run(_Connector);
            new WhenDeletingOverrideRatingQuestionBddfy().Run(_Connector);
            new WhenGettingAOverrideRatingQuestionBddfy().Run(_Connector);
            new WhenGettingAOverrideRatingQuestionListBddfy().Run(_Connector);
            new WhengettingAPaginatedOverrideRatingQuestionListBddfy().Run(_Connector);
        }
    }
}
