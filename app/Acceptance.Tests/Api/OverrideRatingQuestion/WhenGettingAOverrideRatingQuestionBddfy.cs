﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using TestObjects.Mothers.OverrideRatingQuestion;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.OverrideRatingQuestion
{
    public class WhenGettingAOverrideRatingQuestionBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private POSTResponseDto<int> _PostResponse;
        private GETResponseDto<OverrideRatingQuestionDto> _GetResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            CreateOverrideRatingQuestionDto createOverrideRatingQuestionDto = OverrideRatingQuestionObjectMother.ValidCreateOverrideRatingQuestionDto();
            _PostResponse = Connector.OverrideRatingQuestionManagement.OverrideRatingQuestions.Create(createOverrideRatingQuestionDto);
        }

        public void When_action()
        {
            _PostResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _PostResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_PostResponse.Response > 0);

            _GetResponse = Connector.OverrideRatingQuestionManagement.OverrideRatingQuestion(_PostResponse.Response).Get();
        }

        public void Then_result()
        {
            _GetResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _GetResponse.IsSuccess.ShouldBeTrue();

            _GetResponse.Response.ShouldNotBeNull();
        }
    }
}
