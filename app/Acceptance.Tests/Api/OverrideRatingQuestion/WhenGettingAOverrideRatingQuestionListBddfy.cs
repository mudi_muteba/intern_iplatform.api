﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.OverrideRatingQuestion
{
    public class WhenGettingAOverrideRatingQuestionListBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        public GETResponseDto<ListResultDto<ListOverrideRatingQuestionDto>> _GetResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
            _GetResponse = Connector.OverrideRatingQuestionManagement.OverrideRatingQuestions.GetAll();
        }

        public void Then_result()
        {
            _GetResponse.IsSuccess.ShouldBeTrue();
            _GetResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);

            List<ListOverrideRatingQuestionDto> result = _GetResponse.Response.Results;
            Assert.True(result.Count > 0);
        }
    }
}
