﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using TestObjects.Mothers.OverrideRatingQuestion;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.OverrideRatingQuestion
{
    public class WhenEditingOverrideRatingQuestionBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private POSTResponseDto<int> _PostResponse;
        private PUTResponseDto<int> _PutResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            CreateOverrideRatingQuestionDto createOverrideRatingQuestionDto = OverrideRatingQuestionObjectMother.ValidCreateOverrideRatingQuestionDto();
            _PostResponse = Connector.OverrideRatingQuestionManagement.OverrideRatingQuestions.Create(createOverrideRatingQuestionDto);
        }

        public void When_action()
        {
            _PostResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _PostResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_PostResponse.Response > 0);

            EditOverrideRatingQuestionDto editOverrideRatingQuestionDto =
                OverrideRatingQuestionObjectMother.ValidEditOverrideRatingQuestionDto(_PostResponse.Response);

            _PutResponse = Connector.OverrideRatingQuestionManagement.OverrideRatingQuestion(editOverrideRatingQuestionDto.Id)
                    .Update(editOverrideRatingQuestionDto);
        }

        public void Then_result()
        {
            _PutResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _PutResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_PutResponse.Response > 0);
        }
    }
}
