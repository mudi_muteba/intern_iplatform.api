﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.OverrideRatingQuestion;
using TestObjects.Mothers.OverrideRatingQuestion;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.OverrideRatingQuestion
{
    public class WhenCreatingOverrideRatingQuestionBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private CreateOverrideRatingQuestionDto _CreateOverrideRatingQuestionDto;
        private POSTResponseDto<int> _PostResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _CreateOverrideRatingQuestionDto = OverrideRatingQuestionObjectMother.ValidCreateOverrideRatingQuestionDto();
        }

        public void When_action()
        {
            _PostResponse = Connector.OverrideRatingQuestionManagement.OverrideRatingQuestions.Create(_CreateOverrideRatingQuestionDto);
        }

        public void Then_result()
        {
            _PostResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _PostResponse.IsSuccess.ShouldBeTrue();

            Assert.True(_PostResponse.Response > 0);
        }
    }
}
