﻿using Acceptance.Tests.Api.Admin;
using Acceptance.Tests.Api.AssetBuilders;
using Acceptance.Tests.Api.Campaigns;
using Acceptance.Tests.Api.Components.Headers;
using Acceptance.Tests.Api.CustomApp;
using Acceptance.Tests.Api.DocumentManagement;
using Acceptance.Tests.Api.Individual.AssetRiskItems;
using Acceptance.Tests.Api.Individual.Bank;
using Acceptance.Tests.Api.Individual.ProposalDefinitionQuestion;
using Acceptance.Tests.Api.Leads;
using Acceptance.Tests.Api.Users;
using Acceptance.Tests.Api.Users.Authentication;
using Acceptance.Tests.Api.Users.Discounts;
using Acceptance.Tests.PersistenceSpecifications;
using Acceptance.Tests.Queries.Escalations;
using TestHelper;
using Xunit.Extensions;
using Acceptance.Tests.Api.RatingRuleHeaders;
using Acceptance.Tests.Api.RatingRuleHeaderCalculations;

namespace Acceptance.Tests.Api
{
    public class InMemory_Tests : BaseTest
    {
        public InMemory_Tests()
            : base(ApiUserObjectMother.AsAdmin())
        {
        }

        public override void Observe()
        {
        }

        [Observation]
        protected override void assert_all()
        {
            new WhenQueryingEscalationPlansBddfy(Session, Container).Run(Connector);
            new DocumentTests(Connector).run_all();
            new LeadsTests(Connector).run_all();
            new AdminTests(Connector).run_all();
            new BankTests(Connector).run_all();
            new AssetRiskItemsTests(Connector).run_all();
            new UsersTests(Connector).run_all();
            // TODO: Andrew - Investigate why this test fails 
            //new DiscountsTests(Connector).run_all(); 
            new CampaignTests(Connector).run_all();
            new AssetBuildersTests(Connector).run_all();
            new ProposalDefinitionQuestionTests(Connector).run_all();
            new AuthenticationTests(Connector).run_all();

            new CustomAppTests(Connector).run_all();

            //new OverrideRatingQuestionTests(Connector).run_all();
            //new MapVapQuestionDefinitionCoverTests(Connector).run_all();


            new RatingRuleHeaderTests(Connector).run_all();
            new RatingRuleHeaderCalculationTests(Connector).run_all();

            new ComponentHeadersTest(Connector).run_all();

            new PersistenceSpecificationsTests(Connector, Session, Container).run_all();



        }
    }
}