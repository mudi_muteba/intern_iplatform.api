﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.DisplaySetting;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.DisplaySetting
{
    public class WhenCreatingDisplaySettingBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private CreateDisplaySettingDto _CreateDisplaySettingDto;
        private POSTResponseDto<int> _Response;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _CreateDisplaySettingDto = DisplaySettingObjectMother.ValidCreateDisplaySettingDto();
        }

        public void When_action()
        {
            _Response = Connector.DisplaySettingManagement.DisplaySettings.Create(_CreateDisplaySettingDto);
        }

        public void Then_result()
        {
            _Response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _Response.IsSuccess.ShouldBeTrue();

            Assert.True(_Response.Response > 0);
        }
    }
}
