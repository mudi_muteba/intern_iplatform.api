﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.DisplaySetting;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.DisplaySetting
{
    public class WhenEditingDisplaySettingBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private POSTResponseDto<int> _PostResponse;
        private EditDisplaySettingDto _EditDisplaySettingDto;
        private PUTResponseDto<int> _PutResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            CreateDisplaySettingDto createDisplaySettingDto = DisplaySettingObjectMother.ValidCreateDisplaySettingDto();
            _PostResponse = Connector.DisplaySettingManagement.DisplaySettings.Create(createDisplaySettingDto);
        }

        public void When_action()
        {
            _PostResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _PostResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_PostResponse.Response > 0);

            _EditDisplaySettingDto = DisplaySettingObjectMother.ValidEditDisplaySettingDto(_PostResponse.Response);
            _PutResponse = Connector.DisplaySettingManagement.DisplaySetting(_EditDisplaySettingDto.Id)
                    .Update(_EditDisplaySettingDto);
        }

        public void Then_result()
        {
            _PutResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _PutResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_PutResponse.Response > 0);
        }
    }
}
