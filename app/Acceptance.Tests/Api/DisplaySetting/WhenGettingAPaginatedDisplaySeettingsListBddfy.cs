﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.DisplaySetting
{
    public class WhenGettingAPaginatedDisplaySeettingsListBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private GETResponseDto<PagedResultDto<ListDisplaySettingDto>> _GetResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
            _GetResponse = Connector.DisplaySettingManagement.DisplaySettings.GetAllWithPagination(1, 10);
        }

        public void Then_result()
        {
            _GetResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _GetResponse.IsSuccess.ShouldBeTrue();
            _GetResponse.Response.ShouldNotBeNull();

            Assert.True(_GetResponse.Response.Results.Count > 0);
        }
    }
}
