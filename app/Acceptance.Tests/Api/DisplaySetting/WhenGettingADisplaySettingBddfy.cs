﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.DisplaySetting;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.DisplaySetting
{
    public class WhenGettingADisplaySettingBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private POSTResponseDto<int> _PostResponse;
        private GETResponseDto<DisplaySettingDto> _GetResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            CreateDisplaySettingDto createDisplaySettingDto = DisplaySettingObjectMother.ValidCreateDisplaySettingDto();
            _PostResponse = Connector.DisplaySettingManagement.DisplaySettings.Create(createDisplaySettingDto);
        }

        public void When_action()
        {
            _PostResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _PostResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_PostResponse.Response > 0);

            _GetResponse = Connector.DisplaySettingManagement.DisplaySetting(_PostResponse.Response).Get();
        }

        public void Then_result()
        {
            _GetResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _GetResponse.IsSuccess.ShouldBeTrue();

            DisplaySettingDto displaySettingDto = _GetResponse.Response;
            displaySettingDto.ShouldNotBeNull();
        }
    }
}
