﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.DisplaySetting
{
    public class DisplaySettingTests
    {
        private readonly Connector _Connector;

        public DisplaySettingTests(Connector connector)
        {
            _Connector = connector;
            string token = new ConnectorTestData(_Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _Connector.SetToken(token);
        }

        public void run_all()
        {
            new WhenCreatingDisplaySettingBddfy().Run(_Connector);
            new WhenEditingDisplaySettingBddfy().Run(_Connector);
            new WhenDeletingDisplaySettingBddfy().Run(_Connector);
            new WhenGettingADisplaySettingBddfy().Run(_Connector);
            new WhenGettingADisplaySettingsList().Run(_Connector);
            new WhenGettingAPaginatedDisplaySeettingsListBddfy().Run(_Connector);
        }
    }
}
