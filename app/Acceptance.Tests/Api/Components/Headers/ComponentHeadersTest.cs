﻿using iPlatform.Api.DTOs.Base.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestHelper.Helpers;

namespace Acceptance.Tests.Api.Components.Headers
{
    public class ComponentHeadersTest
    {
        private readonly Connector _connector;
        private int _indivdualId;
        public ComponentHeadersTest(Connector connector)
        {
            _connector = connector;
            getIndividualId();
        }

        private void getIndividualId()
        {
            _indivdualId = new ConnectorTestData(_connector).Individuals.Create();
        }

        public void run_all()
        {
            new when_creating_component_header(_indivdualId).Run(_connector);
            new when_updating_component_header(_indivdualId).Run(_connector);
            //new when_disabling_component_header(_indivdualId).Run(_connector);
        }
    }
}
