﻿using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using TestStack.BDDfy;
using iPlatform.Api.DTOs.Components.ComponentHeaders;
using TestObjects.Mothers.Components.ComponentHeadersObjectMothers;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Components.Headers
{
    internal class when_updating_component_header : IBddfyTest
    {
        private int _indivdualId;
        private EditComponentHeaderDto _createComponentHeaderDto;
        private POSTResponseDto<int> _response;
        

        public Connector Connector { get; set; }

        public when_updating_component_header(int indivdualId)
        {
            _indivdualId = indivdualId;
        }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            //_createComponentHeaderDto = NewComponentHeaderObjectMother.ValidUpdateComponentHeaderDto(_indivdualId);
        }

        public void Then_result()
        {
            //throw new NotImplementedException();
        }

        public void When_action()
        {
            //throw new NotImplementedException();
        }
    }
}