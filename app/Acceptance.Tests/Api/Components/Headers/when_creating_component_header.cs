﻿using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using TestStack.BDDfy;
using iPlatform.Api.DTOs.Components.ComponentHeaders;
using TestObjects.Mothers.Components.ComponentHeadersObjectMothers;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Components.Headers
{
    public class when_creating_component_header : IBddfyTest
    {
        public Connector Connector { get; set; }

        private CreateComponentHeaderDto _createComponentHeaderDto;
        private POSTResponseDto<int> _response;
        private int _indivdualId;

        public when_creating_component_header(int indivdualId) {
            _indivdualId = indivdualId;
        }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createComponentHeaderDto = NewComponentHeaderObjectMother.ValidComponentHeaderDto(_indivdualId);
        }

        public void When_action()
        {
            _response = Connector.ComponentHeaderManagement.ComponentHeaders.CreateComponentHeader(_createComponentHeaderDto);
        }

        public void Then_result()
        {
            _response.IsSuccess.ShouldBeTrue();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.ShouldNotBeNull();
        }
    }
}
