using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.FuneralMembers;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.FuneralMembers
{
    public class when_retrieving_funeralmember_by_id : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_retrieving_funeralmember_by_id() : base(ApiUserObjectMother.AsAdmin())
        {
            var createFuneralMemberDto = NewFuneralMemberDtoObjectMother.ValidFuneralMemberDto();
            _response = Connector.FuneralMemberManagement.FuneralMembers.CreateFuneralMember(createFuneralMemberDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_funeralmember_is_retrieved_by_id();
        }

        private void the_funeralmember_is_retrieved_by_id()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var result = Connector.FuneralMemberManagement.FuneralMember(_response.Response).Get();
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }
    }
}