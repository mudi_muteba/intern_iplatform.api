using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.FuneralMembers;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.FuneralMembers
{
    public class when_creating_a_new_funeralmember : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_creating_a_new_funeralmember() : base(ApiUserObjectMother.AsAdmin())
        {
            var createFuneralMemberDto = NewFuneralMemberDtoObjectMother.ValidFuneralMemberDto();
            _response = Connector.FuneralMemberManagement.FuneralMembers.CreateFuneralMember(createFuneralMemberDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_funeralmember_is_created();
        }

        private void the_funeralmember_is_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}