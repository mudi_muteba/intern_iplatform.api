using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.FuneralMembers;
using TestHelper;
using Xunit.Extensions;
using TestObjects.Mothers.FuneralMembers;
using Xunit;
using System.Linq;

namespace Acceptance.Tests.Api.FuneralMembers
{
    public class when_retrieving_funeralmembers_by_proposaldefinitionid : BaseTest
    {
        private readonly CreateFuneralMemberDto _createFuneralMemberDto;
        private readonly POSTResponseDto<int> response;

        public when_retrieving_funeralmembers_by_proposaldefinitionid() : base(ApiUserObjectMother.AsAdmin())
        {
            _createFuneralMemberDto = NewFuneralMemberDtoObjectMother.ValidFuneralMemberDto();
            response = Connector.FuneralMemberManagement.FuneralMembers.CreateFuneralMember(_createFuneralMemberDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_funeralmember_is_retrieved();
        }

        public void the_funeralmember_is_retrieved()
        {
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.IsSuccess.ShouldBeTrue();

            var result = Connector.FuneralMemberManagement.FuneralMembers.RetrieveByProposalDefinitionId(_createFuneralMemberDto.ProposalDefinitionId);
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
            Assert.True(result.Response.Results.Count > 0);
            Assert.True(result.Response.Results.Any(x => x.ProposalDefinitionId == _createFuneralMemberDto.ProposalDefinitionId));
        }
    }
}