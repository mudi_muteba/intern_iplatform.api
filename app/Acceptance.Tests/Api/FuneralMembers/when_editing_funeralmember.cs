using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.FuneralMembers;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.FuneralMembers
{
    public class when_editing_funeralmember : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_editing_funeralmember() : base(ApiUserObjectMother.AsAdmin())
        {
            var createFuneralMemberDto = NewFuneralMemberDtoObjectMother.ValidFuneralMemberDto();
            _response = Connector.FuneralMemberManagement.FuneralMembers.CreateFuneralMember(createFuneralMemberDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_funeralmember_is_edited();
        }

        private void the_funeralmember_is_edited()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var dto = NewFuneralMemberDtoObjectMother.ValidEditFuneralMemberDto();
            dto.Id = _response.Response;

            var result = Connector.FuneralMemberManagement.FuneralMember(_response.Response).EditFuneralMember(dto);
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();

            var editresult = Connector.FuneralMemberManagement.FuneralMember(_response.Response).Get();
            editresult.Response.Surname.ShouldEqual(dto.Surname);
            editresult.Response.SumInsured.Value.ToString().ShouldEqual("250,0");
        }
    }
}