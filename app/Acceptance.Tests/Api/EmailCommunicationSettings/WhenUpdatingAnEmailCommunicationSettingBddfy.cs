﻿using TestStack.BDDfy;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;
using Xunit;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using TestObjects.Mothers.EmailCommunicationSettings;
using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;

namespace Acceptance.Tests.Api.EmailCommunicationSettings
{
    public class WhenUpdatingAnEmailCommunicationSettingBddfy : IBddfyTest
    {
        private CreateEmailCommunicationSettingDto _createDto;
        private POSTResponseDto<int> _response;

        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createDto = EmailCommunicationSettingObjectMother.ValidDto();
        }

        public void When_action()
        {
            //_response = Connector.EmailCommunicationSettingManagement.EmailCommunicationSettings.Create(_createDto);
        }

        public void Then_result()
        {
            the_record_has_been_created();
            the_record_is_updated();
            the_record_should_be_retrieve_by_id_and_validated();
        }
        private void the_record_has_been_created()
        {
            Assert.True(true);
            //_response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            //_response.IsSuccess.ShouldBeTrue();
            //Assert.True(_response.Response > 0);
        }

        private void the_record_is_updated()
        {
            Assert.True(true);

            //var response = Connector.EmailCommunicationSettingManagement.EmailCommunicationSetting(_response.Response).Edit(GetEditDto());
            //response.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }
        private void the_record_should_be_retrieve_by_id_and_validated()
        {
            Assert.True(true);

            //var response = Connector.EmailCommunicationSettingManagement.EmailCommunicationSetting(_response.Response).Get();
            //response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            //response.Response.Username.ShouldEqual("root@iplatform.co.za");
            //response.Response.DefaultFrom.ShouldEqual("Admin<Root@platform.co.za>");
        }

        //private EditEmailCommunicationSettingDto GetEditDto()
        //{
        //    return new EditEmailCommunicationSettingDto
        //    {
        //        Id = _response.Response,
        //        ChannelId = _createDto.ChannelId,
        //        Host = "smtp.gmail.com",
        //        Port = 587,
        //        DefaultContactNumber = "+27 11 300 1100",
        //        Username = "root@iplatform.co.za",
        //        Password = "Password",
        //        DefaultFrom = "Admin<Root@platform.co.za>",
        //        SubAccountID = "QuoteComparison",
        //        UseDefaultCredentials = true,
        //        UseSSL = true,
        //        DefaultBCC = "ootam@iplatform.co.za"
        //    };
        //}
    }
}
