﻿using System.Linq;
using TestHelper;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;
using Xunit;
using System.Net;
using TestObjects.Mothers.EmailCommunicationSettings;
using iPlatform.Api.DTOs.Base.Connector;
using TestStack.BDDfy;

namespace Acceptance.Tests.Api.EmailCommunicationSettings
{
    public class WhenCreatingNewEmailCommunicationSettingBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createDto = EmailCommunicationSettingObjectMother.ValidDto();
            //_response = Connector.EmailCommunicationSettingManagement.EmailCommunicationSettings.Create(createDto);
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            the_record_has_been_created();
            the_record_should_be_retrieve_by_id();
            the_record_should_be_deleted();
        }

        private void the_record_has_been_created()
        {
            Assert.True(true);

            //_response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            //_response.IsSuccess.ShouldBeTrue();
            //Assert.True(_response.Response > 0);
        }

        private void the_record_should_be_retrieve_by_id()
        {
            Assert.True(true);

            //var response = Connector.EmailCommunicationSettingManagement.EmailCommunicationSetting(_response.Response).Get();
            //response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            //response.Response.Channel.Id.ShouldEqual(1);
        }

        private void the_record_should_be_deleted()
        {
            Assert.True(true);

            //var response = Connector.EmailCommunicationSettingManagement.EmailCommunicationSetting(_response.Response).Delete();
            //response.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }
    }
}
