using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.EmailCommunicationSettings
{
    public class EmailCommunicationSettingTests
    {
        private readonly Connector _connector;

        public EmailCommunicationSettingTests(Connector connector)
        {
            _connector = connector;
            var token = new ConnectorTestData(_connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _connector.SetToken(token);
        }

        public void run_all()
        {
            new WhenCreatingNewEmailCommunicationSettingBddfy().Run(_connector);
            new WhenUpdatingAnEmailCommunicationSettingBddfy().Run(_connector);
        }
    }
}