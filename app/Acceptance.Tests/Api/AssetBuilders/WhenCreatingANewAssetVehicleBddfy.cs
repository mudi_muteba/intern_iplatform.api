using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalDefinition;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.AssetBuilders
{
    public class WhenCreatingANewAssetVehicleBddfy : IBddfyTest
    {
        private int _indivdualId;
        private int _proposalHeaderId;
        private POSTResponseDto<int> _proposalDefinitionResponse;
        private PUTResponseDto<ProposalDefinitionDto> _updateResult;
        private ProposalDefinitionAnswerDto _vehicleMmCode;
        private ProposalDefinitionAnswerDto _vehicleMake;
        private EditProposalDefinitionDto _editProposalDefinition;
        private ProposalDefinitionAnswerDto _vehicleRegistrationNumber;
        private ProposalDefinitionAnswerDto _vehicleModel;
        private ProposalDefinitionAnswerDto _yearOfManufacture;
        private ProposalDefinitionAnswerDto _vehicleType;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
      {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();

            _proposalHeaderId = new ConnectorTestData(Connector).ProposalHeaders.Create(_indivdualId);
            var newProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_MOTOR(_proposalHeaderId);

            _proposalDefinitionResponse =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .CreateProposalDefinition(newProposalDefinition);

            SetUp();

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _proposalDefinitionResponse.ShouldNotBeNull();
            _proposalDefinitionResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _proposalDefinitionResponse.IsSuccess.ShouldBeTrue();

            _updateResult.ShouldNotBeNull();
            _updateResult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _updateResult.IsSuccess.ShouldBeTrue();

            LISTResponseDto<ListAssetVehicleDto> assetResponse =
                Connector.IndividualManagement.Individual(_indivdualId).GetAssetVehicles();

            assetResponse.Response.Results.ShouldNotBeEmpty();

            assetResponse.Response.Results.Any(a => a.VehicleMMCode == _vehicleMmCode.Answer).ShouldBeTrue();
            assetResponse.Response.Results.Any(a => a.VehicleMake == _vehicleMake.Answer).ShouldBeTrue();
            assetResponse.Response.Results.Any(a => a.VehicleModel == _vehicleModel.Answer).ShouldBeTrue();
            assetResponse.Response.Results.Any(a => a.VehicleRegistrationNumber == _vehicleRegistrationNumber.Answer).ShouldBeTrue();
            assetResponse.Response.Results.Any(a => a.YearOfManufacture.ToString().Equals(_yearOfManufacture.Answer)).ShouldBeTrue();
            assetResponse.Response.Results.Any(a => a.VehicleType.Id.ToString().Equals(_vehicleType.Answer)).ShouldBeTrue();

            GETResponseDto<ProposalDefinitionDto> result =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .GetProposalDefinition(_proposalDefinitionResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            assetResponse.Response.Results.Any(a => a.Id == result.Response.Asset.Id).ShouldBeTrue(); 
        }

        private void SetUp()
        {
            GETResponseDto<ProposalDefinitionDto> result =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .GetProposalDefinition(_proposalDefinitionResponse.Response);

            _editProposalDefinition =
                NewProposalDefinitionDtoObjectMother.ValidEditProposalDefinitionDto(_proposalDefinitionResponse.Response,
                    _proposalHeaderId);

            _vehicleRegistrationNumber = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.VehicleRegistrationNumber.Id).Select(a => a.AnswerDto).First();

            _vehicleMake = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.VehicleMake.Id).Select(a => a.AnswerDto).First();

            _vehicleModel = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.VehicleModel.Id).Select(a => a.AnswerDto).First();

            _vehicleMmCode = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.VehicleMMCode.Id).Select(a => a.AnswerDto).First();

            _yearOfManufacture = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.YearOfManufacture.Id).Select(a => a.AnswerDto).First();

            _vehicleType = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.VehicleType.Id).Select(a => a.AnswerDto).First();

            _vehicleMmCode.Answer = "123654";
            _vehicleRegistrationNumber.Answer = "DG46DJ79";
            _vehicleMake.Answer = "BMW";
            _vehicleModel.Answer = "X3 2.0";
            _yearOfManufacture.Answer = "2012";
            _vehicleType.Answer = VehicleTypes.Motorcycle.Id.ToString();

            _editProposalDefinition.Answers.Add(_vehicleMmCode);
            _editProposalDefinition.Answers.Add(_vehicleRegistrationNumber);
            _editProposalDefinition.Answers.Add(_vehicleMake);
            _editProposalDefinition.Answers.Add(_vehicleModel);
            _editProposalDefinition.Answers.Add(_yearOfManufacture);
            _editProposalDefinition.Answers.Add(_vehicleType);

            _updateResult =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .UpdateProposalDefinition(_editProposalDefinition);
        }

    }
}