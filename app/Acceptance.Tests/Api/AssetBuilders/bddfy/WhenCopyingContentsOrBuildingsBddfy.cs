using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalDefinition;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.AssetBuilders.bddfy
{
    public class WhenCopyingContentsOrBuildingsBddfy : IBddfyTest
    {
        private int _indivdualId;
        private POSTResponseDto<int> _proposalDefinitionResponse;
        private int _proposalHeaderId;
        private int _addressId;
        private EditProposalDefinitionDto _editProposalDefinition;
        private ProposalDefinitionAnswerDto _riskAddress;
        private ProposalDefinitionAnswerDto _usage;
        private ProposalDefinitionAnswerDto _financedOrPaidoff;
        private ProposalDefinitionAnswerDto _homeType;
        private ProposalDefinitionAnswerDto _roofConstruction;
        private ProposalDefinitionAnswerDto _wallConstruction;
        private ProposalDefinitionAnswerDto _thatchLapa;
        private ProposalDefinitionAnswerDto _homeBoarder;
        private ProposalDefinitionAnswerDto _constructionYear;
        private ProposalDefinitionAnswerDto _sizeOfProperty;
        private ProposalDefinitionAnswerDto _numberOfBathroom;
        private ProposalDefinitionAnswerDto _propertyUnoccupied;
        private ProposalDefinitionAnswerDto _bussinessConducted;
        private ProposalDefinitionAnswerDto _mortgageBank;
        private PUTResponseDto<ProposalDefinitionDto> _updateResult;
        private POSTResponseDto<int> _copyProposal;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();

            _proposalHeaderId = new ConnectorTestData(Connector).ProposalHeaders.Create(_indivdualId);
            var createProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_BUILDINGS(_proposalHeaderId);

            _proposalDefinitionResponse =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .CreateProposalDefinition(createProposalDefinition);
        }

        public void When_action()
        {
            var createProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_BUILDINGS(_proposalHeaderId);

            GETResponseDto<ProposalDefinitionDto> result =
                 Connector.IndividualManagement.Individual(_indivdualId)
                      .Proposal(_proposalHeaderId)
                     .GetProposalDefinition(_proposalDefinitionResponse.Response);

            _editProposalDefinition =
                NewProposalDefinitionDtoObjectMother.ValidEditProposalDefinitionDto(_proposalDefinitionResponse.Response,
                    _proposalHeaderId);

            _addressId = new ConnectorTestData(Connector).Individuals.CreateAddress(_indivdualId);

            _riskAddress = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.RiskAddress.Id).Select(a => a.AnswerDto).First();

            _usage = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGUsage.Id).Select(a => a.AnswerDto).First();

            _financedOrPaidoff = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGOwnership.Id).Select(a => a.AnswerDto).First();

            _homeType = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGHomeType.Id).Select(a => a.AnswerDto).First();

            _roofConstruction = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGRoofConstruction.Id).Select(a => a.AnswerDto).First();

            _wallConstruction = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGWallConstruction.Id).Select(a => a.AnswerDto).First();

            _thatchLapa = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGThatchLapa.Id).Select(a => a.AnswerDto).First();

            _homeBoarder = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGPropertyBorder.Id).Select(a => a.AnswerDto).First();

            _constructionYear = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGConstructionYear.Id).Select(a => a.AnswerDto).First();

            _sizeOfProperty = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGSquareMetresOfProperty.Id).Select(a => a.AnswerDto).First();

            _numberOfBathroom = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGNumberOfBathrooms.Id).Select(a => a.AnswerDto).First();

            _propertyUnoccupied = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGPeriodPropertyIsUnoccupied.Id)
                .Select(a => a.AnswerDto)
                .First();

            _bussinessConducted = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGBusiness.Id).Select(a => a.AnswerDto).First();

            _mortgageBank = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
               .Where(a => a.Question.Id == Questions.AIGMortgageBank.Id).Select(a => a.AnswerDto).First();

            _riskAddress.Answer = _addressId.ToString();
            _usage.Answer = "3227";
            _financedOrPaidoff.Answer = "Yes - Mortgaged";
            _homeType.Answer = "Free Standing House";
            _roofConstruction.Answer = "Standard";
            _wallConstruction.Answer = "Standard";
            _thatchLapa.Answer = "Attached";
            _homeBoarder.Answer = "Vacant land";
            _constructionYear.Answer = "2011";
            _sizeOfProperty.Answer = "12000";
            _numberOfBathroom.Answer = "2";
            _propertyUnoccupied.Answer = "0 days -30 days";
            _bussinessConducted.Answer = "None";
            _mortgageBank.Answer = "ABSA";

            _editProposalDefinition.Answers.Add(_riskAddress);
            _editProposalDefinition.Answers.Add(_usage);
            _editProposalDefinition.Answers.Add(_financedOrPaidoff);
            _editProposalDefinition.Answers.Add(_homeType);
            _editProposalDefinition.Answers.Add(_roofConstruction);
            _editProposalDefinition.Answers.Add(_wallConstruction);
            _editProposalDefinition.Answers.Add(_thatchLapa);
            _editProposalDefinition.Answers.Add(_homeBoarder);
            _editProposalDefinition.Answers.Add(_constructionYear);
            _editProposalDefinition.Answers.Add(_sizeOfProperty);
            _editProposalDefinition.Answers.Add(_numberOfBathroom);
            _editProposalDefinition.Answers.Add(_propertyUnoccupied);
            _editProposalDefinition.Answers.Add(_bussinessConducted);
            _editProposalDefinition.Answers.Add(_mortgageBank);

            _updateResult =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .UpdateProposalDefinition(_editProposalDefinition);

            _copyProposal =
                               Connector.IndividualManagement.Individual(_indivdualId)
                                   .Proposal(_proposalHeaderId)
                                   .CreateProposalDefinition(new CreateProposalDefinitionDto
                                   {
                                       Id = _proposalHeaderId,
                                       CoverId = 78,
                                       ProductId = createProposalDefinition.ProductId,
                                       DefaultFromProposalId = _proposalDefinitionResponse.Response
                                   });
        }

        public void Then_result()
        {
            _proposalDefinitionResponse.ShouldNotBeNull();
            _proposalDefinitionResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _proposalDefinitionResponse.IsSuccess.ShouldBeTrue();

            _updateResult.ShouldNotBeNull();
            _updateResult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _updateResult.IsSuccess.ShouldBeTrue();

            _copyProposal.ShouldNotBeNull();
            _copyProposal.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _copyProposal.IsSuccess.ShouldBeTrue();

            GETResponseDto<ProposalDefinitionDto> result =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .GetProposalDefinition(_proposalDefinitionResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            var copy = Connector.IndividualManagement.Individual(_indivdualId).Proposal(_proposalHeaderId).GetProposalDefinition(_copyProposal.Response);

            copy.Response.ShouldNotBeNull();

            var riskaddress = copy.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.RiskAddress.Id).Select(a => a.AnswerDto).First();

            _riskAddress.Answer.ShouldEqual(riskaddress.Answer);

            var generalinformation = copy.Response.QuestionGroups.FirstOrDefault(a => a.Id == QuestionGroups.GeneralInformation.Id);

            generalinformation.ShouldNotBeNull();

            var usage = generalinformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.AIGUsage.Id);

            _usage.Answer.ShouldEqual(usage.AnswerDto.Answer);

            var financed = generalinformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.AIGOwnership.Id);

            _financedOrPaidoff.Answer.ShouldEqual(financed.AnswerDto.Answer);

            var homeType = generalinformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.AIGHomeType.Id);

            _homeType.Answer.ShouldEqual(homeType.AnswerDto.Answer);

            var riskinformation = copy.Response.QuestionGroups.FirstOrDefault(a => a.Id == QuestionGroups.RiskInformation.Id);

            generalinformation.ShouldNotBeNull();

            var thatchLapa =
                riskinformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.AIGThatchLapa.Id);

            _thatchLapa.Answer.ShouldEqual(thatchLapa.AnswerDto.Answer);

            var propertyBoarder = riskinformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.AIGPropertyBorder.Id);

            _homeBoarder.Answer.ShouldEqual(propertyBoarder.AnswerDto.Answer);

            var constructionYear = riskinformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.AIGConstructionYear.Id);

            _constructionYear.Answer.ShouldEqual(constructionYear.AnswerDto.Answer);

            var propertySize = riskinformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.AIGSquareMetresOfProperty.Id);

            _sizeOfProperty.Answer.ShouldEqual(propertySize.AnswerDto.Answer);

            var numberOfBathrooms = riskinformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.AIGNumberOfBathrooms.Id);

            _numberOfBathroom.Answer.ShouldEqual(numberOfBathrooms.AnswerDto.Answer);

            var bussinessConducted = riskinformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.AIGBusiness.Id);

            _bussinessConducted.Answer.ShouldEqual(bussinessConducted.AnswerDto.Answer);

            var propertyUnoccupied = riskinformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.AIGPeriodPropertyIsUnoccupied.Id);

            _propertyUnoccupied.Answer.ShouldEqual(propertyUnoccupied.AnswerDto.Answer);

        }
    }
}