using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Address;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalDefinition;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.AssetBuilders
{
    public class WhenCreatingANewAssetAddressBddfy : IBddfyTest
    {
        private int _indivdualId;
        private POSTResponseDto<int> _proposalDefinitionResponse;
        private int _proposalHeaderId;
        private int _addressId;
        private EditProposalDefinitionDto _editProposalDefinition;
        private ProposalDefinitionAnswerDto _riskAddress;
        private PUTResponseDto<ProposalDefinitionDto> _updateResult;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();

            _proposalHeaderId = new ConnectorTestData(Connector).ProposalHeaders.Create(_indivdualId);
            var createProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_CONTENTS(_proposalHeaderId);

            _proposalDefinitionResponse =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .CreateProposalDefinition(createProposalDefinition);

            SetUp();
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _proposalDefinitionResponse.ShouldNotBeNull();
            _proposalDefinitionResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _proposalDefinitionResponse.IsSuccess.ShouldBeTrue();

            _updateResult.ShouldNotBeNull();
            _updateResult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _updateResult.IsSuccess.ShouldBeTrue();

            GETResponseDto<ProposalDefinitionDto> result =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .GetProposalDefinition(_proposalDefinitionResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            AddressDto address = Connector.IndividualManagement.Individual(_indivdualId)
                .GetAddress(_addressId).Response;

            result.Response.Asset.ShouldNotBeNull();
            (result.Response.Asset.Id > 0).ShouldBeTrue();
            (result.Response.Asset.Description.Equals(address.Description)).ShouldBeTrue();
        }

        private void SetUp()
        {
            GETResponseDto<ProposalDefinitionDto> result =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .GetProposalDefinition(_proposalDefinitionResponse.Response);

            _editProposalDefinition =
                NewProposalDefinitionDtoObjectMother.ValidEditProposalDefinitionDto(_proposalDefinitionResponse.Response,
                    _proposalHeaderId);

            _addressId = new ConnectorTestData(Connector).Individuals.CreateAddress(_indivdualId);

            _riskAddress = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.RiskAddress.Id).Select(a => a.AnswerDto).First();


            _riskAddress.Answer = _addressId.ToString();
            _editProposalDefinition.Answers.Add(_riskAddress);

            _updateResult =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .UpdateProposalDefinition(_editProposalDefinition);
        }
    }
}