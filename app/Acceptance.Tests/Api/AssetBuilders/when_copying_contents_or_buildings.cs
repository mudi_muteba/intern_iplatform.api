using Acceptance.Tests.Api.AssetBuilders.bddfy;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.AssetBuilders
{
    public class when_copying_contents_or_buildings : BaseTest
    {
        public when_copying_contents_or_buildings()
            : base(ApiUserObjectMother.AsAdmin(), ConnectionType.SqlServer)
        {
        }

        public override void Observe()
        {

        }

       
        [Observation]
        protected override void assert_all()
        {
            new WhenCopyingContentsOrBuildingsBddfy().Run(Connector);
        }

    }
}