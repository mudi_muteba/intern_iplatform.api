using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.AssetBuilders
{
    public class AssetBuildersTests
    {
        private readonly Connector _connector;

        public AssetBuildersTests(Connector connector)
        {
            _connector = connector;
        }

        public void run_all()
        {
            new WhenCreatingANewAssetVehicleBddfy().Run(_connector);
            new WhenCreatingANewAssetRiskItemBddfy().Run(_connector);
            new WhenCreatingANewAssetAddressBddfy().Run(_connector);
        }
    }
}