using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using MasterData;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalDefinition;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.AssetBuilders
{
    public class WhenCreatingANewAssetRiskItemBddfy : IBddfyTest
    {
        private int _indivdualId;
        private POSTResponseDto<int> _proposalDefinitionResponse;
        private int _proposalHeaderId;
        private EditProposalDefinitionDto _editProposalDefinition;
        private PUTResponseDto<ProposalDefinitionDto> _updateResult;
        private ProposalDefinitionAnswerDto _itemDescription;
        private ProposalDefinitionAnswerDto _serialImeiNumber;
        private ProposalDefinitionAnswerDto _allRiskCategory;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _proposalHeaderId = new ConnectorTestData(Connector).ProposalHeaders.Create(_indivdualId);
            var newProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_RISK_ITEM(_proposalHeaderId);


            _proposalDefinitionResponse =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .CreateProposalDefinition(newProposalDefinition);

            Setup();
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _proposalDefinitionResponse.ShouldNotBeNull();
            _proposalDefinitionResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _proposalDefinitionResponse.IsSuccess.ShouldBeTrue();

            _updateResult.ShouldNotBeNull();
            _updateResult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _updateResult.IsSuccess.ShouldBeTrue();

            LISTResponseDto<ListAssetRiskItemDto> assetResponse =
                Connector.IndividualManagement.Individual(_indivdualId).GetAssetRiskItems();

            assetResponse.Response.Results.ShouldNotBeEmpty();

            assetResponse.Response.Results.Any(a => a.Description.Contains(_itemDescription.Answer)).ShouldBeTrue();
            assetResponse.Response.Results.Any(a => a.SerialIMEINumber == _serialImeiNumber.Answer).ShouldBeTrue();
            assetResponse.Response.Results.Any(a => a.AllRiskCategory == int.Parse(_allRiskCategory.Answer)).ShouldBeTrue();

            GETResponseDto<ProposalDefinitionDto> result =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .GetProposalDefinition(_proposalDefinitionResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            assetResponse.Response.Results.Any(a => a.Id == result.Response.Asset.Id).ShouldBeTrue(); 
        }
        private void Setup()
        {
            GETResponseDto<ProposalDefinitionDto> result =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .GetProposalDefinition(_proposalDefinitionResponse.Response);

            _editProposalDefinition =
                NewProposalDefinitionDtoObjectMother.ValidEditProposalDefinitionDto(_proposalDefinitionResponse.Response,
                    _proposalHeaderId);

            _itemDescription = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.ItemDescription.Id).Select(a => a.AnswerDto).First();

            _serialImeiNumber = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.SerialIMEINumber.Id).Select(a => a.AnswerDto).First();

            _allRiskCategory = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AllRiskCategory.Id).Select(a => a.AnswerDto).First();

            _itemDescription.Answer = "This is a description";
            _serialImeiNumber.Answer = "3468434168434";
            _allRiskCategory.Answer = 10.ToString();

            _editProposalDefinition.Answers.Add(_itemDescription);
            _editProposalDefinition.Answers.Add(_serialImeiNumber);
            _editProposalDefinition.Answers.Add(_allRiskCategory);

            _updateResult =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .UpdateProposalDefinition(_editProposalDefinition);
        }
    }
}