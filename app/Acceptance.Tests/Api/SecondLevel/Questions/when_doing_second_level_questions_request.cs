﻿using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.SecondLevel.Questions
{
    public class when_doing_second_level_questions_request : BaseTest
    {
        private readonly GETResponseDto<SecondLevelQuestionsForQuoteDto> _response;

        public when_doing_second_level_questions_request() : base(ApiUserObjectMother.AsAdmin())
        {
            new TestHelper.Helpers.Workflow().Start();
            _response = Connector.SecondLevel.GetSecondLevelQuestionsByQuoteId(2006);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            then_second_level_questions_are_returned();
        }

        private void then_second_level_questions_are_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.ShouldNotBeNull();
            _response.IsSuccess.ShouldBeTrue();

            _response.Response.Questions.Count.ShouldEqual(4);

            _response.Response.Questions.FirstOrDefault(w => w.Name == "Policy cancelled or refused renewal").ShouldNotBeNull();
            _response.Response.Questions.FirstOrDefault(w => w.Name == "Under debt review or administration").ShouldNotBeNull();
            _response.Response.Questions.FirstOrDefault(w => w.Name == "Any defaults or judgements").ShouldNotBeNull();
            _response.Response.Questions.FirstOrDefault(w => w.Name == "Insolvent, sequestrated or liquidated").ShouldNotBeNull();

            _response.Response.QuoteItems.Count.ShouldEqual(1);
        }
    }
}
