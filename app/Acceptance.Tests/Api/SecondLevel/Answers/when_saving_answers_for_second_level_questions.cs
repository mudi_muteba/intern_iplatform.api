﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.SecondLevel;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.SecondLevel.Answers
{
    public class when_saving_answers_for_second_level_questions : BaseTest
    {
        private readonly PUTResponseDto<bool> _response;

        public when_saving_answers_for_second_level_questions() : base(ApiUserObjectMother.AsAdmin())
        {
            new TestHelper.Helpers.Workflow().Start();
            var dto = SecondLevelQuestionDefinitionMother.ForSecondLevelQuestionsSaveDto();
            _response = Connector.SecondLevel.SaveSecondLevelAnswersForQuoteId(dto, dto.QuoteId);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            then_second_level_answers_for_questions_should_be_saved();
        }

        private void then_second_level_answers_for_questions_should_be_saved()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            _response.Response.ShouldNotBeNull();
            _response.Response.ShouldEqual(true);
        }
    }
}
