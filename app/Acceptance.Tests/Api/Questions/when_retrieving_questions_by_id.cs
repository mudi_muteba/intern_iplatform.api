using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Products;
using TestHelper;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Question
{
    public class when_retrieving_questions_by_id : BaseTest
    {
        private readonly GETResponseDto<QuestionDefinitionDto> _response;

        public when_retrieving_questions_by_id() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.QuestionManagement.Question(1).Get();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_questions_are_retrieved_by_id();
        }

        private void the_questions_are_retrieved_by_id()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            Assert.True(_response.Response != null);
        }
    }
}