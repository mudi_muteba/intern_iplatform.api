using System.Net;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Products;
using TestHelper;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Question
{
    public class when_retrieving_questions_by_coverid : BaseTest
    {
        private readonly GETResponseDto<PagedResultDto<QuestionDefinitionDto>> _response;

        public when_retrieving_questions_by_coverid() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.QuestionManagement.Questions.GetByCoverId(1);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_questions_are_retrieved_by_cover_id();
        }
        
        private void the_questions_are_retrieved_by_cover_id()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            Assert.True(_response.Response.Results.Count > 0);
        }
    }
}