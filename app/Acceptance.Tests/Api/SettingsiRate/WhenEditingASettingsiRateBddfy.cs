﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.SettingsiRate;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.SettingsiRate
{
    public class WhenEditingASettingsiRateBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private EditSettingsiRateDto _EditSettingsiRateDto;
        private POSTResponseDto<int> _SaveResponse;
        private PUTResponseDto<int> _EditResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            CreateSettingsiRateDto createSettingsiRateDto = SettingsiRateObjectMother.ValidSettingsiRateDto();
            _SaveResponse = Connector.SettingsiRateManagement.SettingsiRates.Create(createSettingsiRateDto);
        }

        public void When_action()
        {
            _SaveResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _SaveResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_SaveResponse.Response > 0);
            
            _EditSettingsiRateDto = SettingsiRateObjectMother.ValidEditSettingsiRateDto(_SaveResponse.Response);
            _EditResponse = Connector.SettingsiRateManagement.SettingsiRate(_SaveResponse.Response).Edit(_EditSettingsiRateDto);
        }

        public void Then_result()
        {
            _EditResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _EditResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_EditResponse.Response > 0);
        }
    }
}