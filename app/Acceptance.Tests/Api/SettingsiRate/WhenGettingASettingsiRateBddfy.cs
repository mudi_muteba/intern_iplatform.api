﻿using System.Net;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.SettingsiRate;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.SettingsiRate
{
    public class WhenGettingASettingsiRateBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private POSTResponseDto<int> _SaveResponse;
        private GETResponseDto<SettingsiRateDto> _GetResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            CreateSettingsiRateDto createSettingsiRateDto = SettingsiRateObjectMother.ValidSettingsiRateDto();
            _SaveResponse = Connector.SettingsiRateManagement.SettingsiRates.Create(createSettingsiRateDto);
        }

        public void When_action()
        {
            _SaveResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _SaveResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_SaveResponse.Response > 0);
            
            _GetResponse = Connector.SettingsiRateManagement.SettingsiRate(_SaveResponse.Response).Get();
        }

        public void Then_result()
        {
            _GetResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _GetResponse.IsSuccess.ShouldBeTrue();

            SettingsiRateDto settingsiRateDto = _GetResponse.Response;
            settingsiRateDto.ShouldNotBeNull();
        }
    }
}
