﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.SettingsiRate
{
    public class SettingsiRateTests
    {
        private readonly Connector _Connector;

        public SettingsiRateTests(Connector connector)
        {
            _Connector = connector;
            string token = new ConnectorTestData(_Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _Connector.SetToken(token);
        }

        public void run_all()
        {
            //Valid data tests
            new WhenCreatingASettingsiRateBddfy().Run(_Connector);
            new WhenEditingASettingsiRateBddfy().Run(_Connector);
            
            //Invalid data tests
            new WhenCreatingASettingsiRateWithInvalidChannelSystemIdBddfy().Run(_Connector);
            new WhenCreatingASettingsiRateWithInvalidProductIdBddfy().Run(_Connector);

            //Get tests
            new WhenGettingASettingsiRateBddfy().Run(_Connector);
            new WhenGettingASettingsiRateListBddfy().Run(_Connector);
        }
    }
}
