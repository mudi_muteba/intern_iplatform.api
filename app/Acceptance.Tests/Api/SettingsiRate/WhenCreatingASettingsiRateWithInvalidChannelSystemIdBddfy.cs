﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.SettingsiRate;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.SettingsiRate
{
    public class WhenCreatingASettingsiRateWithInvalidChannelSystemIdBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private CreateSettingsiRateDto _CreateSettingsiRateDto;
        private POSTResponseDto<int> _Response;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _CreateSettingsiRateDto = SettingsiRateObjectMother.InvalidChannelSystemIdSettingsiRateDto();
        }

        public void When_action()
        {
            _Response = Connector.SettingsiRateManagement.SettingsiRates.Create(_CreateSettingsiRateDto);
        }

        public void Then_result()
        {
            _Response.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            _Response.IsSuccess.ShouldBeFalse();
        }
    }
}
