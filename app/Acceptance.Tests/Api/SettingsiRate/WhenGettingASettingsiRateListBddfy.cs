﻿using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Connector;
using TestStack.BDDfy;
using Xunit;

namespace Acceptance.Tests.Api.SettingsiRate
{
    public class WhenGettingASettingsiRateListBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private PagedResultDto<ListSettingsiRateDto> _Response;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _Response = Connector.SettingsiRateManagement.SettingsiRates.GetAll();
        }

        public void When_action()
        {
            _Response = Connector.SettingsiRateManagement.SettingsiRates.GetAll();
        }

        public void Then_result()
        {
            Assert.True(_Response.Results.Count > 0);
        }
    }
}
