using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual
{
    public class when_retrieving_an_individual : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_retrieving_an_individual() : base(ApiUserObjectMother.AsAdmin())
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_individual_is_created();
            the_individual_is_retrieved_by_id();
        }

        private void the_individual_is_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }

        private void the_individual_is_retrieved_by_id()
        {
            var result = Connector.IndividualManagement.Individual(_response.Response).Get();
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }
    }
}