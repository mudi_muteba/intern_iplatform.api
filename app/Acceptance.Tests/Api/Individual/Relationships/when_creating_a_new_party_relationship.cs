using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Individual;
using TestHelper;
using Xunit.Extensions;
using TestObjects.Mothers.Individual;

namespace Acceptance.Tests.Api.Individual.Relationships
{
    public class when_creating_a_new_party_relationship : BaseTest
    {
        private CreateIndividualDto _createIndividualDto;
        private POSTResponseDto<int> _response;
        private int _partyId;
        private int _childPartyId;

        public when_creating_a_new_party_relationship() : base(ApiUserObjectMother.AsAdmin())
        {
            _createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualOne();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(_createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_relationship_is_created();
        }

        private void the_relationship_is_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            _partyId = _response.Response;

            _createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualTwo();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(_createIndividualDto);

            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            _childPartyId = _response.Response;

            var dto = NewRelationshipDtoObjectMother.ValidRelationshipDto(_partyId, _childPartyId);
            _response = Connector.RelationshipManagement.Relationship.CreateRelationship(dto);

            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}