using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit.Extensions;
using TestObjects.Mothers.Individual;

namespace Acceptance.Tests.Api.Individual.Relationships
{
    public class when_creating_a_new_party_relationship_invalid_parties : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_creating_a_new_party_relationship_invalid_parties() : base(ApiUserObjectMother.AsAdmin())
        {
            var createRelationshipDto = NewRelationshipDtoObjectMother.ValidRelationshipDto(9999, 10000);
            _response = Connector.RelationshipManagement.Relationship.CreateRelationship(createRelationshipDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_relationship_with_invalid_parties();
        }

        private void the_relationship_with_invalid_parties()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            _response.IsSuccess.ShouldBeFalse();
        }
    }
}