using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Individual;
using TestHelper;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Relationships
{
    public class when_updating_a_party_relationship_invalid_parties : BaseTest
    {
        private CreateIndividualDto _createIndividualDto;
        private POSTResponseDto<int> _response;
        private int _partyId;
        private int _childPartyId;

        public when_updating_a_party_relationship_invalid_parties() : base(ApiUserObjectMother.AsAdmin())
        {
            _createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualOne();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(_createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_relationship_is_updated();
        }

        private void the_relationship_is_updated()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            _partyId = _response.Response;

            _createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualTwo();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(_createIndividualDto);

            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            _childPartyId = _response.Response;


            //create relationship
            var dto = NewRelationshipDtoObjectMother.ValidRelationshipDto(_partyId, _childPartyId);
            _response = Connector.RelationshipManagement.Relationship.CreateRelationship(dto);

            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            //update relationship
            var updateDto = NewRelationshipDtoObjectMother.ValidEditRelationshipWithInvalidPartiesDto(_response.Response, dto);
            var result = Connector.RelationshipManagement.Relationship.EditRelationship(updateDto);

            result.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            result.IsSuccess.ShouldBeFalse();
        }
    }
}