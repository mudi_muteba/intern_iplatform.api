using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.SalesForce
{
    public class when_posting_to_sales_force : BaseTest
    {
        private POSTResponseDto<int> _response;
        private POSTResponseDto<int> sfResponse;

        public when_posting_to_sales_force() : base(ApiUserObjectMother.AsAdmin())
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
            // sfResponse = connector.IndividualManagement.Individual(response.Response).SalesForce().Post();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "This uses the work flow and the result can not be test here")]
        protected override void assert_all()
        {
            the_internal_salesforce_id_is_returned();
        }

        private void the_internal_salesforce_id_is_returned()
        {
          //  sfResponse.StatusCode.ShouldEqual(HttpStatusCode.Created);
           // sfResponse.IsSuccess.ShouldBeTrue();
           // Assert.True(sfResponse.Response > 0); 
        }
    }
}