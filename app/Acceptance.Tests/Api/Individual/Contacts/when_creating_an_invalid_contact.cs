using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual;
using ValidationMessages.Individual.Contacts;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Contacts
{
    public class when_creating_an_invalid_Contact : BaseTest
    {
        private readonly POSTResponseDto<int> _getResponse;

        public when_creating_an_invalid_Contact() : base(ApiUserObjectMother.AsAdmin())
        {
            var newContact = NewContactDtoObjectMother.InvalidContactDto();
            var indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _getResponse = Connector.IndividualManagement.Individual(indivdualId).CreateContact(newContact);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_contact_is_not_saved();
        }

        private void the_contact_is_not_saved()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            _getResponse.IsSuccess.ShouldBeFalse();

            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(ContactValidationMessages.IdentityNoRequired.MessageKey)).ShouldNotBeNull();
            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(ContactValidationMessages.RelationshipTypeRequired.MessageKey)).ShouldNotBeNull();
        }
    }
}