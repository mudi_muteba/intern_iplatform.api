using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Contacts;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Contacts
{
    public class when_getting_Contactes_by_individual : BaseTest
    {
        private readonly LISTResponseDto<ListContactDto> _result;

        public when_getting_Contactes_by_individual() : base(ApiUserObjectMother.AsAdmin())
        {
            var indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var responseContact1 = Connector.IndividualManagement.Individual(indivdualId)
                .CreateContact(NewContactDtoObjectMother.ValidContactDto());
            var responseContact2 = Connector.IndividualManagement.Individual(indivdualId)
                .CreateContact(NewContactDtoObjectMother.ValidContactDto());

            responseContact1.ShouldNotBeNull();
            responseContact1.StatusCode.ShouldEqual(HttpStatusCode.OK);
            responseContact1.IsSuccess.ShouldBeTrue();

            responseContact2.ShouldNotBeNull();
            responseContact2.StatusCode.ShouldEqual(HttpStatusCode.OK);
            responseContact2.IsSuccess.ShouldBeTrue();

            _result = Connector.IndividualManagement.Individual(indivdualId).GetContacts();
        }

        public override void Observe()
        {
           
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            fetching_contacts_by_individual();
        }

        private void fetching_contacts_by_individual()
        {
            _result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _result.Response.ShouldNotBeNull();

            Assert.True(_result.Response.Results.Count == 2);
        }
    }
}