using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Contacts;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Contacts
{
    public class when_creating_a_new_contact : BaseTest
    {
        private readonly int _indivdualId;
        private readonly CreateContactDto _newContact;
        private readonly POSTResponseDto<int> _postResponse;

        // given
        public when_creating_a_new_contact() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _newContact = NewContactDtoObjectMother.ValidContactDto();
            _postResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateContact(_newContact);
        }

        // when
        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_individual_is_created();
            the_contact_is_saved();
        }

        private void the_individual_is_created()
        {
            _postResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postResponse.IsSuccess.ShouldBeTrue();
        }

        private void the_contact_is_saved()
        {
            _postResponse.ShouldNotBeNull();
            _postResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).GetContact(_postResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            var contact = result.Response;
            contact.ShouldNotBeNull();
            contact.FirstName.ShouldEqual(_newContact.FirstName);
            contact.Surname.ShouldEqual(_newContact.Surname);
            contact.IdentityNo.ShouldEqual(_newContact.IdentityNo);
            contact.Gender.Code.ShouldEqual(_newContact.Gender.Code);
            contact.MaritalStatus.Code.ShouldEqual(_newContact.MaritalStatus.Code);
            contact.RelationshipType.Id.ShouldEqual(_newContact.RelationshipType.Id);
        }
    }
}