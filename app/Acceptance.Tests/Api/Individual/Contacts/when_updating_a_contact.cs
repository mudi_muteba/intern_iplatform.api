using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Contacts;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Contacts
{
    public class when_updating_an_Contact : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _contactResponse;

        public when_updating_an_Contact() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newContact = NewContactDtoObjectMother.ValidContactDto();
            _contactResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateContact(newContact);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_contact_is_updated();
        }

        private void the_contact_is_updated()
        {
            //Check if Contact is saved
            _contactResponse.ShouldNotBeNull();
            _contactResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _contactResponse.IsSuccess.ShouldBeTrue();

            //get saved Contact
            var result = Connector.IndividualManagement.Individual(_indivdualId).GetContact(_contactResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            //update Contact
            var contactDto = new EditContactDto
            {
                Id = result.Response.Id,
                IdentityNo = result.Response.IdentityNo,
                PassportNo = result.Response.PassportNo,
                Surname = result.Response.Surname,
                MiddleName = result.Response.MiddleName,
                Language = result.Response.Language,
                MaritalStatus = result.Response.MaritalStatus,
                Gender = result.Response.Gender,
                Title = result.Response.Title,
                DateOfBirth = result.Response.DateOfBirth.Value,
                RelationshipType = result.Response.RelationshipType,
                FirstName = "this is an update"
            };

            var updateresult = Connector.IndividualManagement.Individual(_indivdualId).EditContact(contactDto);

            updateresult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            updateresult.Response.ShouldNotBeNull();

            //Get updated Contact
            var updatedResult = Connector.IndividualManagement.Individual(_indivdualId).GetContact(_contactResponse.Response);

            updatedResult.Response.FirstName.ShouldEqual(contactDto.FirstName);
            updatedResult.Response.IdentityNo.ShouldEqual(contactDto.IdentityNo);
            updatedResult.Response.Language.Id.ShouldEqual(contactDto.Language.Id);
        }
    }
}