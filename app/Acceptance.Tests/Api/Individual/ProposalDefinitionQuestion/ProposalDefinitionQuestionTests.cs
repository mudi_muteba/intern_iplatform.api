using Acceptance.Tests.Api.Individual.Bank;
using iPlatform.Api.DTOs.Base.Connector;

namespace Acceptance.Tests.Api.Individual.ProposalDefinitionQuestion
{
    public class ProposalDefinitionQuestionTests
    {
        private readonly Connector _connector;

        public ProposalDefinitionQuestionTests(Connector connector)
        {
            _connector = connector;
        }

        public void run_all()
        {
            new WhenRetrievingProposaldefinitionquestionByCriteriaBddfy().Run(_connector);
        }
    }
}