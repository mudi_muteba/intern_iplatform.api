using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalDefinition;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.ProposalDefinitionQuestion
{
    public class WhenRetrievingProposaldefinitionquestionByCriteriaBddfy : IBddfyTest
    {
        private POSTResponseDto<ProposalDefinitionQuestionsDto> _response;
        private int _proposalHeaderId;
        private POSTResponseDto<int> _proposalDefinitionResponse;
        private int _indivdualId; public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _proposalHeaderId = new ConnectorTestData(Connector).ProposalHeaders.Create(_indivdualId);
            var newProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_MOTOR(_proposalHeaderId);

            _proposalDefinitionResponse =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .CreateProposalDefinition(newProposalDefinition);


        }

        public void When_action()
        {
            _response = Connector.IndividualManagement.Individual(_indivdualId).ProposalDefinitionQuestion(_proposalDefinitionResponse.Response).GetByCriteria(
                new ProposalDefinitionQuestionsCriteria
                {
                    GroupIndex = 0,
                    GroupType = 0,

                });

        }

        public void Then_result()
        {
            _response.IsSuccess.ShouldBeTrue();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.ShouldNotBeNull();
            _response.Response.QuestionDefinitions.ShouldNotBeEmpty();
            _response.Response.QuestionDefinitions.Count.ShouldEqual(75);
            _response.Response.ProposalDefinition.Description.ShouldNotBeEmpty();
        }
    }
}