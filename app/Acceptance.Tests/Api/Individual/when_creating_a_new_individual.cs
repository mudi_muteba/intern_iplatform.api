using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Individual;
using TestHelper;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual
{
    public class when_creating_a_new_individual : BaseTest
    {
        private readonly CreateIndividualDto _createIndividualDto;
        private readonly POSTResponseDto<int> _response;

        public when_creating_a_new_individual() : base(ApiUserObjectMother.AsAdmin())
        {
            _createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(_createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_individual_is_created();
            the_individual_can_be_retrieved_by_id();
        }

        private void the_individual_is_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }

        private void the_individual_can_be_retrieved_by_id()
        {
            var getResponse = Connector.IndividualManagement.Individual(_response.Response).Get();

            getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            getResponse.IsSuccess.ShouldBeTrue();

            var individual = getResponse.Response;
            individual.ShouldNotBeNull();
            individual.FirstName.ShouldEqual(_createIndividualDto.FirstName);
            individual.Surname.ShouldEqual(_createIndividualDto.Surname);
            individual.IdentityNo.ShouldEqual(_createIndividualDto.IdentityNo);
            individual.Gender.Code.ShouldEqual(_createIndividualDto.Gender.Code);
            individual.MaritalStatus.Code.ShouldEqual(_createIndividualDto.MaritalStatus.Code);
            individual.ContactDetail.Cell.ShouldEqual(_createIndividualDto.ContactDetail.Cell);
            individual.Occupation.Id.ShouldEqual(_createIndividualDto.Occupation.Id);
            individual.ExternalReference.ShouldEqual(_createIndividualDto.ExternalReference);
        }
    }
}