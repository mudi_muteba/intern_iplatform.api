using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Note;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Note;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Note
{
    public class when_creating_a_new_note : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _getResponse;
        private readonly CreateNoteDto _newNote;

        public when_creating_a_new_note() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _newNote = NewCreateNoteDtoObjectMother.ValidCreateNoteDto();
            _getResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateNote(_newNote);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_note_is_saved();
        }

        private void the_note_is_saved()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).GetNote(_getResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            result.Response.Notes.ShouldEqual(_newNote.Notes);
            result.Response.Confidential.ShouldEqual(_newNote.Confidential);
        }
    }
}