using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Note;
using ValidationMessages.Individual.Note;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Note
{
    public class when_creating_an_invalid_note : BaseTest
    {
        private readonly POSTResponseDto<int> _getResponse;

        public when_creating_an_invalid_note() : base(ApiUserObjectMother.AsAdmin())
        {
            var indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newNote = NewCreateInvalidNoteDtoObjectMother.InvalidCreateNoteDto();
            _getResponse = Connector.IndividualManagement.Individual(indivdualId).CreateNote(newNote);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_note_is_not_saved();
        }

        private void the_note_is_not_saved()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            _getResponse.IsSuccess.ShouldBeFalse();
            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(NoteValidationMessages.NotesRequired.MessageKey)).ShouldNotBeNull();
        }
    }
}