using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Note;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Note
{
    public class when_deleteing_a_note : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _getResponse;

        public when_deleteing_a_note() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newNote = NewCreateNoteDtoObjectMother.ValidCreateNoteDto();
            _getResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateNote(newNote);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_note_is_deleted();
        }

        private void the_note_is_deleted()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).DeleteNote(_getResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.Accepted);
            result.Response.ShouldNotBeNull();

            result.Response.ShouldEqual(_getResponse.Response);
        }
    }
}