using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Note;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Note
{
    public class when_getting_notes_by_individual : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _responseNote1;
        private readonly POSTResponseDto<int> _responseNote2;

        public when_getting_notes_by_individual() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _responseNote1 =
               Connector.IndividualManagement.Individual(_indivdualId)
                   .CreateNote(NewCreateNoteDtoObjectMother.ValidCreateNoteDto());
            _responseNote2 =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .CreateNote(NewCreateNoteDtoObjectMother.ValidCreateNoteDto());
        }

        public override void Observe()
        {
           
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            fetching_notes_by_individual();
        }

        private void fetching_notes_by_individual()
        {
            _responseNote1.ShouldNotBeNull();
            _responseNote1.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseNote1.IsSuccess.ShouldBeTrue();

            _responseNote2.ShouldNotBeNull();
            _responseNote2.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseNote2.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).GetNotes();

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            Assert.True(result.Response.Results.Count == 2);
        }
    }
}