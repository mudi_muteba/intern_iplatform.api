using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using TestHelper;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual
{
    public class when_creating_a_new_individual_with_invalid_campaigns : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_creating_a_new_individual_with_invalid_campaigns() : base(ApiUserObjectMother.AsAdmin())
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualWithCampaignDto();
            createIndividualDto.Campaigns.Add(new CampaignInfoDto { Id = 999999 });
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_new_campaigns_is_created();
        }

        private void the_new_campaigns_is_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            _response.IsSuccess.ShouldBeFalse();
        }
    }
}