using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalHeader;
using Xunit.Extensions;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.Individual.ProposalHeaders
{
    public class when_creating_a_new_Proposal_header : BaseTest
    {
        private readonly int _indivdualId;
        private readonly CreateProposalHeaderDto _newProposalHeader;
        private readonly POSTResponseDto<int> _getResponse;

        public when_creating_a_new_Proposal_header() : base(ApiUserObjectMother.AsAdmin())
        {
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());

            Connector = new Connector(new ApiToken(token)); 

            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _newProposalHeader = NewCreateProposalHeaderDtoObjectMother.ValidCreateProposalHeaderDto();

            _getResponse =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposals()
                    .CreateProposalHeader(_newProposalHeader);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_proposal_header_is_saved();
        }

        private void the_proposal_header_is_saved()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).Proposal(_getResponse.Response).Get();

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            result.Response.Description.ShouldEqual(_newProposalHeader.Description);
        }
    }
}