using System.Net;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalHeader;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.ProposalHeaders
{
    public class when_creating_an_invalid_ProposalHeader : BaseTest
    {
        private readonly int _indivdualId;
        private readonly CreateProposalHeaderDto _newProposalHeader;

        public when_creating_an_invalid_ProposalHeader() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _newProposalHeader = NewCreateInvalidProposalHeaderDtoObjectMother.InvalidCreateProposalHeaderDto();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "")]
        protected override void assert_all()
        {
            the_proposal_header_is_not_saved();
        }

        private void the_proposal_header_is_not_saved()
        {
            var getResponse =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposals()
                    .CreateProposalHeader(_newProposalHeader);

            getResponse.ShouldNotBeNull();
            getResponse.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            getResponse.IsSuccess.ShouldBeFalse();
            //getResponse.Errors.FirstOrDefault(
            //    a => a.MessageKey.Equals(ProposalHeaderValidationMessages.ProposalHeadersRequired.MessageKey))
            //    .ShouldNotBeNull();
        }
    }
}