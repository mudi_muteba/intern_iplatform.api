using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalHeader;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.ProposalHeaders
{
    public class when_deleteing_a_ProposalHeader : BaseTest
    {
        private readonly int _proposalHeaderId;
        private readonly DELETEResponseDto<int> _result;

        public when_deleteing_a_ProposalHeader() : base(ApiUserObjectMother.AsAdmin())
        {
            var indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newProposalHeader = NewCreateProposalHeaderDtoObjectMother.ValidCreateProposalHeaderDto();
            _proposalHeaderId =
                Connector.IndividualManagement.Individual(indivdualId)
                    .Proposals()
                    .CreateProposalHeader(newProposalHeader)
                    .Response;

            _result = Connector.IndividualManagement.Individual(indivdualId).Proposal(_proposalHeaderId).Delete();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_proposal_header_is_deleted();
        }

        private void the_proposal_header_is_deleted()
        {
            _result.StatusCode.ShouldEqual(HttpStatusCode.Accepted);
            _result.Response.ShouldNotBeNull();

            _result.Response.ShouldEqual(_proposalHeaderId);
        }
    }
}