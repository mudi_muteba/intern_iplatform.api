using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalHeader;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.ProposalHeaders
{
    public class when_getting_proposal_header_by_individual : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _responseProposalHeader1;
        private readonly POSTResponseDto<int> _responseProposalHeader2;

        public when_getting_proposal_header_by_individual() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();

            _responseProposalHeader1 =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposals()
                    .CreateProposalHeader(NewCreateProposalHeaderDtoObjectMother.ValidCreateProposalHeaderDto());
            _responseProposalHeader2 =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposals()
                    .CreateProposalHeader(NewCreateProposalHeaderDtoObjectMother.ValidCreateProposalHeaderDto());
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            fetching_proposal_headers_by_individual();
        }

        private void fetching_proposal_headers_by_individual()
        {
            _responseProposalHeader1.ShouldNotBeNull();
            _responseProposalHeader1.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseProposalHeader1.IsSuccess.ShouldBeTrue();

            _responseProposalHeader2.ShouldNotBeNull();
            _responseProposalHeader2.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseProposalHeader2.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).Proposals().Get();
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            Assert.True(result.Response.Results.Count == 2);
        }
    }
}