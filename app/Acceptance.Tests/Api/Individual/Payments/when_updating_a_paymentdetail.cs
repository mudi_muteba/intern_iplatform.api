using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Bank;
using TestObjects.Mothers.Individual.Payments;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Payments
{
    public class when_updating_a_paymentdetail : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _getResponse;
        private readonly int _bankDetailId;

        public when_updating_a_paymentdetail() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();

            var newBankDetails = NewCreateBankDetailsDtoObjectMother.ValidCreateBankDetailsDto();

            _getResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateBankDetails(newBankDetails);
            _bankDetailId = _getResponse.Response;

            var newPaymentDetail = NewCreatePaymentDetailDtoObjectMother.ValidCreatePaymentDetailDto(_getResponse.Response, _indivdualId);

            _getResponse = Connector.IndividualManagement.Individual(_indivdualId).CreatePaymentDetail(newPaymentDetail);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_Paymentdetail_is_updated();
        }

        private void the_Paymentdetail_is_updated()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var editDto = NewCreatePaymentDetailDtoObjectMother.ValidEditPaymentDetailDto(_bankDetailId, _indivdualId);
            editDto.Id = _getResponse.Response;

            var response = Connector.IndividualManagement.Individual(_indivdualId).EditPaymentDetail(editDto);

            response.ShouldNotBeNull();
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.IsSuccess.ShouldBeTrue();
        }
    }
}