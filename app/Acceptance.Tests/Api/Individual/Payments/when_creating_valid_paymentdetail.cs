using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Bank;
using Xunit.Extensions;
using TestObjects.Mothers.Individual.Payments;

namespace Acceptance.Tests.Api.Individual.Payments
{
    public class when_creating_valid_paymentdetail : BaseTest
    {
        private readonly POSTResponseDto<int> _getResponse;

        public when_creating_valid_paymentdetail() : base(ApiUserObjectMother.AsAdmin())
        {
            var indivdualId = new ConnectorTestData(Connector).Individuals.Create();

            var newBankDetails = NewCreateBankDetailsDtoObjectMother.ValidCreateBankDetailsDto();

            _getResponse = Connector.IndividualManagement.Individual(indivdualId).CreateBankDetails(newBankDetails);

            var newPaymentDetail = NewCreatePaymentDetailDtoObjectMother.ValidCreatePaymentDetailDto(_getResponse.Response, indivdualId);

            _getResponse = Connector.IndividualManagement.Individual(indivdualId).CreatePaymentDetail(newPaymentDetail);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_Paymentdetail_is_saved();
        }

        private void the_Paymentdetail_is_saved()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();
        }
    }
}