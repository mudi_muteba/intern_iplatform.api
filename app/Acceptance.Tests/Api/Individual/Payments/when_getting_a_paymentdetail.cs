using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Bank;
using TestObjects.Mothers.Individual.Payments;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Payments
{
    public class when_getting_a_paymentdetail : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _getResponse;

        public when_getting_a_paymentdetail()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();

            var newBankDetails = NewCreateBankDetailsDtoObjectMother.ValidCreateBankDetailsDto();

            _getResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateBankDetails(newBankDetails);

            var newPaymentDetail = NewCreatePaymentDetailDtoObjectMother.ValidCreatePaymentDetailDto(_getResponse.Response, _indivdualId);

            _getResponse = Connector.IndividualManagement.Individual(_indivdualId).CreatePaymentDetail(newPaymentDetail);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_Paymentdetail_is_retrieved();
        }

        private void the_Paymentdetail_is_retrieved()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_getResponse.Response > 0);

            var response = Connector.IndividualManagement.Individual(_indivdualId).GetPaymentDetailById(_getResponse.Response);

            response.ShouldNotBeNull();
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.IsSuccess.ShouldBeTrue();
            Assert.True(response.Response != null);
        }
    }
}