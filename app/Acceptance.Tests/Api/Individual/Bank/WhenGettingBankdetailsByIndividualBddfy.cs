using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Bank;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Bank
{
    public class WhenGettingBankdetailsByIndividualBddfy : IBddfyTest
    {
        private int _indivdualId;
        private POSTResponseDto<int> _responseBankDetails1;
        private POSTResponseDto<int> _responseBankDetails2;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _responseBankDetails1 =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .CreateBankDetails(NewCreateBankDetailsDtoObjectMother.ValidCreateBankDetailsDto());
            _responseBankDetails2 =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .CreateBankDetails(NewCreateBankDetailsDtoObjectMother.ValidCreateBankDetailsDto());

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _responseBankDetails1.ShouldNotBeNull();
            _responseBankDetails1.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseBankDetails1.IsSuccess.ShouldBeTrue();

            _responseBankDetails2.ShouldNotBeNull();
            _responseBankDetails2.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseBankDetails2.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).GetBankDetails();

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            Assert.True(result.Response.Results.Count == 2);
        }
    }
}