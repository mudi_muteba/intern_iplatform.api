using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Bank;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Bank
{
    public class WhenCreatingNewBankdetailsBddfy : IBddfyTest
    {
        private int _indivdualId;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            var newBankDetails = NewCreateBankDetailsDtoObjectMother.ValidCreateBankDetailsDto();

            var getResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateBankDetails(newBankDetails);

            getResponse.ShouldNotBeNull();
            getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            getResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).GetBankDetail(getResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            result.Response.AccountNo.ShouldEqual(newBankDetails.AccountNo);
            result.Response.Bank.ShouldEqual(newBankDetails.Bank);
            result.Response.BankAccHolder.ShouldEqual(newBankDetails.BankAccHolder);
            result.Response.BankBranch.ShouldEqual(newBankDetails.BankBranch);
            result.Response.BankBranchCode.ShouldEqual(newBankDetails.BankBranchCode);
        }
    }
}