using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Bank;
using TestStack.BDDfy;
using ValidationMessages.Individual.AssetVehicle;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Bank
{
    public class WhenCreatingInvalidBankdetailsBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _getResponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newBankDetails = NewCreateBankDetailsDtoObjectMother.InvalidCreateBankDetailsDto();
            _getResponse = Connector.IndividualManagement.Individual(indivdualId).CreateBankDetails(newBankDetails);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            _getResponse.IsSuccess.ShouldBeFalse();

            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(BankDetailsValidationMessages.AccountNoRequired.MessageKey)).ShouldNotBeNull();
            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(BankDetailsValidationMessages.BankBranchCodeRequired.MessageKey)).ShouldNotBeNull();
            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(BankDetailsValidationMessages.BankBranchRequired.MessageKey)).ShouldNotBeNull();
            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(BankDetailsValidationMessages.BankRequired.MessageKey)).ShouldNotBeNull();
            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(BankDetailsValidationMessages.TypeAccount.MessageKey)).ShouldNotBeNull();

        }
    }
}