using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Bank;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Bank;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Bank
{
    public class WhenUpdatingBankdetailsBddfy : IBddfyTest
    {
        private int _indivdualId;
        private POSTResponseDto<int> _bankDetailsResponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newBankDetails = NewCreateBankDetailsDtoObjectMother.ValidCreateBankDetailsDto();
            _bankDetailsResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateBankDetails(newBankDetails);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            //Check if details was saved
            _bankDetailsResponse.ShouldNotBeNull();
            _bankDetailsResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _bankDetailsResponse.IsSuccess.ShouldBeTrue();

            //get saved details
            var result = Connector.IndividualManagement.Individual(_indivdualId).GetBankDetail(_bankDetailsResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            //update details
            var newDetails = new EditBankDetailsDto
            {
                Id = result.Response.Id,
                AccountNo = result.Response.AccountNo,
                PartyId = result.Response.PartyId,
                Bank = result.Response.Bank,
                BankBranchCode = result.Response.BankBranchCode,
                BankBranch = result.Response.BankBranch,
                BankAccHolder = result.Response.BankAccHolder,
                TypeAccount = result.Response.TypeAccount
            };
            newDetails.BankAccHolder = "this is the account holder";

            var updateresult = Connector.IndividualManagement.Individual(_indivdualId).EditBankDetails(newDetails);

            updateresult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            updateresult.Response.ShouldNotBeNull();

            //Get updated details

            var updatedResult = Connector.IndividualManagement.Individual(_indivdualId).GetBankDetail(_bankDetailsResponse.Response);

            updatedResult.Response.AccountNo.ShouldEqual(newDetails.AccountNo);
            updatedResult.Response.PartyId.ShouldEqual(newDetails.PartyId);
            updatedResult.Response.Bank.ShouldEqual(newDetails.Bank);
            updatedResult.Response.BankBranchCode.ShouldEqual(newDetails.BankBranchCode);
            updatedResult.Response.BankBranch.ShouldEqual(newDetails.BankBranch);
            updatedResult.Response.BankAccHolder.ShouldEqual(newDetails.BankAccHolder);
            updatedResult.Response.TypeAccount.ShouldEqual(newDetails.TypeAccount);
        }
    }
}