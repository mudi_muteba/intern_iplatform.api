using Acceptance.Tests.Api.Individual.AssetRiskItems;
using iPlatform.Api.DTOs.Base.Connector;

namespace Acceptance.Tests.Api.Individual.Bank
{
    public class BankTests
    {
        private readonly Connector _connector;

        public BankTests(Connector connector)
        {
            _connector = connector;
        }

        public void run_all()
        {
            new WhenCreatingInvalidBankdetailsBddfy().Run(_connector);
            new WhenCreatingNewBankdetailsBddfy().Run(_connector);
            new WhenDeleteingBankdetailsBddfy().Run(_connector);
            new WhenGettingBankdetailsByIndividualBddfy().Run(_connector);
            new WhenUpdatingBankdetailsBddfy().Run(_connector);

        }
    }
}