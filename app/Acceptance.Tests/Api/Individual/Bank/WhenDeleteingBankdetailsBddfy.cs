using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Bank;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Bank
{
    public class WhenDeleteingBankdetailsBddfy : IBddfyTest
    {
        private int indivdualId;
        private POSTResponseDto<int> _getResponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newBankDetails = NewCreateBankDetailsDtoObjectMother.ValidCreateBankDetailsDto();
            _getResponse = Connector.IndividualManagement.Individual(indivdualId).CreateBankDetails(newBankDetails);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(indivdualId).DeleteBankDetails(_getResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.Accepted);
            result.Response.ShouldNotBeNull();

            result.Response.ShouldEqual(_getResponse.Response);
        }
    }
}