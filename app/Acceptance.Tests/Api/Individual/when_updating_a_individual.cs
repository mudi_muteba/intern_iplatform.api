using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Individual;
using TestHelper;
using TestHelper.Helpers;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual
{
    public class when_updating_a_individual : BaseTest
    {
        private readonly int _indivdualId;
        private readonly PUTResponseDto<int> _updateresult;
        private readonly EditIndividualDto _individualDto;

        public when_updating_a_individual() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var result = Connector.IndividualManagement.Individual(_indivdualId).Get();

            _individualDto = new EditIndividualDto
            {
                Id = result.Response.Id,
                IdentityNo = result.Response.IdentityNo,
                PassportNo = result.Response.PassportNo,
                Surname = result.Response.Surname,
                MiddleName = result.Response.MiddleName,
                Language = result.Response.Language,
                MaritalStatus = result.Response.MaritalStatus,
                Gender = result.Response.Gender,
                Title = result.Response.Title,
                DateOfBirth = result.Response.DateOfBirth.Value,
                ChannelId = 1,
                Occupation = result.Response.Occupation,
                ExternalReference = result.Response.ExternalReference,
                FirstName = "this is an update"
            };

            _updateresult = Connector.IndividualManagement.Individual(_indivdualId).EditIndividual(_individualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_Individual_is_updated();
        }

        private void the_Individual_is_updated()
        {
            _updateresult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _updateresult.Response.ShouldNotBeNull();

            //Get updated Individual
            var updatedResult = Connector.IndividualManagement.Individual(_indivdualId).Get();

            updatedResult.Response.FirstName.ShouldEqual(_individualDto.FirstName);
            updatedResult.Response.IdentityNo.ShouldEqual(_individualDto.IdentityNo);
            updatedResult.Response.Language.Id.ShouldEqual(_individualDto.Language.Id);
            updatedResult.Response.Occupation.Id.ShouldEqual(_individualDto.Occupation.Id);
            updatedResult.Response.ExternalReference.ShouldEqual(_individualDto.ExternalReference);
        }
    }
}