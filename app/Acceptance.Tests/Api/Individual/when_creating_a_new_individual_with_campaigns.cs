using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using TestHelper;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;
using TestObjects.Mothers.Campaigns;

namespace Acceptance.Tests.Api.Individual
{
    public class when_creating_a_new_individual_with_campaigns : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_creating_a_new_individual_with_campaigns() : base(ApiUserObjectMother.AsAdmin())
        {
            var createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_new_campaigns_is_created();
            the_individual_is_created_with_campaigns();
        }

        private void the_new_campaigns_is_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }

        private void the_individual_is_created_with_campaigns()
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualWithCampaignDto();
            createIndividualDto.Campaigns.Add(new CampaignInfoDto() { Id = _response.Response });
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);

            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var getResponse = Connector.IndividualManagement.Individual(_response.Response).Get();

            getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            getResponse.IsSuccess.ShouldBeTrue();

            var individual = getResponse.Response;
            individual.ShouldNotBeNull();
            individual.FirstName.ShouldEqual(createIndividualDto.FirstName);
            individual.Surname.ShouldEqual(createIndividualDto.Surname);
            individual.IdentityNo.ShouldEqual(createIndividualDto.IdentityNo);
            individual.Gender.Code.ShouldEqual(createIndividualDto.Gender.Code);
            individual.MaritalStatus.Code.ShouldEqual(createIndividualDto.MaritalStatus.Code);
            individual.ContactDetail.Cell.ShouldEqual(createIndividualDto.ContactDetail.Cell);
            individual.Occupation.Id.ShouldEqual(createIndividualDto.Occupation.Id);
            individual.ExternalReference.ShouldEqual(createIndividualDto.ExternalReference);
        }
    }
}