using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.CorrespondencePreference;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.CorrespondencePreferencences
{
    public class when_updating_a_correspondence_preference : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _getResponse;

        public when_updating_a_correspondence_preference() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newCorrespondencePref = NewCreateCorrespondencePrefDtoObjectMother.ValidCreateCorrespondencePrefDto();
            _getResponse = Connector.IndividualManagement.Individual(_indivdualId).EditCorrespondencePreference(newCorrespondencePref);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_correspondencepreference_is_updated();
        }

        private void the_correspondencepreference_is_updated()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var editdto = NewCreateCorrespondencePrefDtoObjectMother.ValidEditCorrespondencePrefDto(_indivdualId);

            var updateResponse = Connector.IndividualManagement.Individual(_indivdualId).EditCorrespondencePreference(editdto);

            updateResponse.ShouldNotBeNull();
            updateResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            updateResponse.IsSuccess.ShouldBeTrue();
        }
    }
}