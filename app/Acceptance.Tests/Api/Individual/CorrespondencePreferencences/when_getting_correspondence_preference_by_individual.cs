using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.CorrespondencePreference;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.CorrespondencePreferencences
{
    public class when_getting_correspondence_preference_by_individual : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _getResponse;

        public when_getting_correspondence_preference_by_individual() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newCorrespondencePref = NewCreateCorrespondencePrefDtoObjectMother.ValidCreateCorrespondencePrefDto();
            _getResponse = Connector.IndividualManagement.Individual(_indivdualId).EditCorrespondencePreference(newCorrespondencePref);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            fetching_correspondence_preference_by_individual();
        }

        private void fetching_correspondence_preference_by_individual()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).GetCorrespondencePreferences();

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();
            Assert.True(result.Response.Results.Count > 0);
        }
    }
}