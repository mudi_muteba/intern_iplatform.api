using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.CorrespondencePreference;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.CorrespondencePreferencences
{
    public class when_creating_valid_correspondence_preference : BaseTest
    {
        private readonly POSTResponseDto<int> _getResponse;

        public when_creating_valid_correspondence_preference() : base(ApiUserObjectMother.AsAdmin())
        {
            var indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newCorrespondencePref = NewCreateCorrespondencePrefDtoObjectMother.ValidCreateCorrespondencePrefDto();
            _getResponse = Connector.IndividualManagement.Individual(indivdualId).EditCorrespondencePreference(newCorrespondencePref);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_correspondencepreference_is_saved();
        }

        private void the_correspondencepreference_is_saved()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();
        }
    }
}