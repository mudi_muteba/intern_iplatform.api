using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Individual.CorrespondencePreference;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.CorrespondencePreferencences
{
    public class when_creating_correspondence_preference_invalid_partyid : BaseTest
    {
        private readonly POSTResponseDto<int> _getResponse;

        public when_creating_correspondence_preference_invalid_partyid() : base(ApiUserObjectMother.AsAdmin())
        {
            var indivdualId = 999;
            var newCorrespondencePref = NewCreateCorrespondencePrefDtoObjectMother.ValidCreateCorrespondencePrefDto();
            _getResponse = Connector.IndividualManagement.Individual(indivdualId).EditCorrespondencePreference(newCorrespondencePref);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_correspondencepreference_is_saved();
        }

        private void the_correspondencepreference_is_saved()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            _getResponse.IsSuccess.ShouldBeFalse();
        }
    }
}