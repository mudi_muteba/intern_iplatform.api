using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.AssetRiskItems;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.AssetRiskItems
{
    public class WhenDeleteingAssetRiskItemBddfy : IBddfyTest
    {
        private int _indivdualId;
        private int _assetId;
        private POSTResponseDto<int> _getResponse; 
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newAssetRiskItem = NewAssetRiskItemDtoObjectMother.ValidCreateAssetRiskItemDto(_indivdualId);
            _getResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateAssetRiskItem(newAssetRiskItem);
            _assetId = _getResponse.Response;

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).DeleteAssetRiskItem(_assetId);

            result.StatusCode.ShouldEqual(HttpStatusCode.Accepted);
            result.Response.ShouldNotBeNull();

            result.Response.ShouldEqual(_getResponse.Response);
        }
    }
}