using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.AssetRiskItems;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.AssetRiskItems
{
    public class WhenGettingAssetRiskItemByIndividualBddfy : IBddfyTest
    {
        private int _indivdualId;
        private POSTResponseDto<int> _responseAssetRiskItem1;
        private POSTResponseDto<int> _responseAssetRiskItem2;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _responseAssetRiskItem1 =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .CreateAssetRiskItem(NewAssetRiskItemDtoObjectMother.ValidCreateAssetRiskItemDto(_indivdualId));
            _responseAssetRiskItem2 =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .CreateAssetRiskItem(NewAssetRiskItemDtoObjectMother.ValidCreateAssetRiskItemDto(_indivdualId));

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _responseAssetRiskItem1.ShouldNotBeNull();
            _responseAssetRiskItem1.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseAssetRiskItem1.IsSuccess.ShouldBeTrue();

            _responseAssetRiskItem2.ShouldNotBeNull();
            _responseAssetRiskItem2.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseAssetRiskItem2.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).GetAssetRiskItems();

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            Assert.True(result.Response.Results.Count == 2);
        }
    }
}