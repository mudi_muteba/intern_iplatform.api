using iPlatform.Api.DTOs.Base.Connector;

namespace Acceptance.Tests.Api.Individual.AssetRiskItems
{
    public class AssetRiskItemsTests
    {
        private readonly Connector _connector;

        public AssetRiskItemsTests(Connector connector)
        {
            _connector = connector;
        }

        public void run_all()
        {
            new WhenCreatingNewAssetRiskItemBddfy().Run(_connector);
            new WhenDeleteingAssetRiskItemBddfy().Run(_connector);
            new WhenGettingAssetRiskItemByIndividualBddfy().Run(_connector);
            new WhenUpdatingAssetRiskItemBddfy().Run(_connector);

        }
    }
}