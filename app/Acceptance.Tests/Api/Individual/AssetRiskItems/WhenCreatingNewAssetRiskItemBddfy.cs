using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.AssetRiskItems;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.AssetRiskItems
{
    public class WhenCreatingNewAssetRiskItemBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private int _indivdualId;
        private int _assetId;
        private POSTResponseDto<int> _getResponse;
        private CreateAssetRiskItemDto _newAssetRiskItem;
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _newAssetRiskItem = NewAssetRiskItemDtoObjectMother.ValidCreateAssetRiskItemDto(_indivdualId);
            _getResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateAssetRiskItem(_newAssetRiskItem);
            _assetId = _getResponse.Response;
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).GetAssetRiskItem(_assetId);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            result.Response.PartyId.ShouldEqual(_newAssetRiskItem.PartyId);
            result.Response.AllRiskCategory.ShouldEqual(_newAssetRiskItem.AllRiskCategory);
            result.Response.SerialIMEINumber.ShouldEqual(_newAssetRiskItem.SerialIMEINumber);
        }
    }
}