using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Assets.AssetRiskItems;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.AssetRiskItems;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.AssetRiskItems
{
    public class WhenUpdatingAssetRiskItemBddfy : IBddfyTest
    {
        private int _indivdualId;
        private CreateAssetRiskItemDto _newAssetRiskItem;
        private POSTResponseDto<int> _assetRiskItemResponse; 
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _newAssetRiskItem = NewAssetRiskItemDtoObjectMother.ValidCreateAssetRiskItemDto(_indivdualId);
            _assetRiskItemResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateAssetRiskItem(_newAssetRiskItem);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            //Check if details was saved
            _assetRiskItemResponse.ShouldNotBeNull();
            _assetRiskItemResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _assetRiskItemResponse.IsSuccess.ShouldBeTrue();

            //get saved details
            var result = Connector.IndividualManagement.Individual(_indivdualId).GetAssetRiskItem(_assetRiskItemResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            //update details
            var newDetails = new EditAssetRiskItemDto
            {
                Id = result.Response.Id,
                PartyId = result.Response.PartyId,
                AllRiskCategory = result.Response.AllRiskCategory,
                SerialIMEINumber = "this a different Serial number"
            };

            var updateresult = Connector.IndividualManagement.Individual(_indivdualId).EditAssetRiskItem(newDetails);

            updateresult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            updateresult.Response.ShouldNotBeNull();

            //Get updated details

            var updatedResult = Connector.IndividualManagement.Individual(_indivdualId).GetAssetRiskItem(_assetRiskItemResponse.Response);

            result.Response.PartyId.ShouldEqual(_newAssetRiskItem.PartyId);
            result.Response.PartyId.ShouldEqual(_newAssetRiskItem.PartyId);
            result.Response.AllRiskCategory.ShouldEqual(_newAssetRiskItem.AllRiskCategory);
            result.Response.SerialIMEINumber.ShouldEqual(_newAssetRiskItem.SerialIMEINumber);
        }
    }
}