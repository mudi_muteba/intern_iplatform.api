using System;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Address;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Address;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Address
{
    public class when_creating_a_new_address : BaseTest
    {
        private readonly int _indivdualId;
        private readonly CreateAddressDto _newAddress;
        private readonly POSTResponseDto<int> _postResponse;

        // given
        public when_creating_a_new_address() : base(ApiUserObjectMother.AsAdmin())
        {
            _newAddress = NewCreateAddressDtoObjectMother.ValidCreateAddressDto();
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _postResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateAddress(_newAddress);
        }

        // when
        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_individual_is_created();
            the_address_is_saved();
        }

        private void the_individual_is_created()
        {
            _postResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postResponse.IsSuccess.ShouldBeTrue();
        }

        private void the_address_is_saved()
        {
            _postResponse.ShouldNotBeNull();
            _postResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).GetAddress(_postResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            result.Response.Code.ShouldEqual(_newAddress.Code);
            result.Response.Complex.ShouldEqual(_newAddress.Complex);
            result.Response.IsComplex.ShouldEqual(_newAddress.IsComplex);
            result.Response.Line1.ShouldEqual(_newAddress.Line1);
            result.Response.Line2.ShouldEqual(_newAddress.Line2);
            result.Response.Line3.ShouldEqual(_newAddress.Line3);
            result.Response.Line4.ShouldEqual(_newAddress.Line4);
            Convert.ToDouble(result.Response.Latitude).ShouldEqual(0);
            Convert.ToDouble(result.Response.Longitude).ShouldEqual(0);
        }
    }
}