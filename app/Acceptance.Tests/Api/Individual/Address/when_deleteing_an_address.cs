using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Address;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Address
{
    public class when_deleteing_an_address : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _getResponse;

        public when_deleteing_an_address() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newAddress = NewCreateAddressDtoObjectMother.ValidCreateAddressDto();
            _getResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateAddress(newAddress);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_address_is_deleted();
        }

        private void the_address_is_deleted()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).DeleteAddress(_getResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.Accepted);
            result.Response.ShouldNotBeNull();

            result.Response.ShouldEqual(_getResponse.Response);
        }
    }
}