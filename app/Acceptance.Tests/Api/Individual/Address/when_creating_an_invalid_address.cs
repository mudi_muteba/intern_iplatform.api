using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Address;
using ValidationMessages.Individual.Address;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Address
{
    public class when_creating_an_invalid_address : BaseTest
    {
        private readonly POSTResponseDto<int> _getResponse;

        public when_creating_an_invalid_address() : base(ApiUserObjectMother.AsAdmin())
        {
            var newAddress = NewCreateAddressDtoObjectMother.InvalidCreateAddressDto();
            var indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _getResponse = Connector.IndividualManagement.Individual(indivdualId).CreateAddress(newAddress);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_address_is_not_saved();
        }

        public void the_address_is_not_saved()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            _getResponse.IsSuccess.ShouldBeFalse();
            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(AddressValidationMessages.Line1Required.MessageKey)).ShouldNotBeNull();
            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(AddressValidationMessages.Line4Required.MessageKey)).ShouldNotBeNull();
            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(AddressValidationMessages.CodeRequired.MessageKey)).ShouldNotBeNull();
        }
    }
}