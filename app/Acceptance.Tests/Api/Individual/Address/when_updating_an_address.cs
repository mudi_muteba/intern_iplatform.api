using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Address;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Address;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Address
{
    public class when_updating_an_address : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _addressResponse;

        public when_updating_an_address() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newAddress = NewCreateAddressDtoObjectMother.ValidCreateAddressDto();
            _addressResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateAddress(newAddress);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_address_is_updated();
        }

        private void the_address_is_updated()
        {
            //Check if address is saved
            _addressResponse.ShouldNotBeNull();
            _addressResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _addressResponse.IsSuccess.ShouldBeTrue();

            //get saved address
            var result = Connector.IndividualManagement.Individual(_indivdualId).GetAddress(_addressResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            //update address
            var newAddress = new EditAddressDto
            {
                Id = result.Response.Id,
                Code = result.Response.Code,
                Complex = result.Response.Complex,
                Line2 = result.Response.Line2,
                Line4 = result.Response.Line4,
                PartyId = result.Response.PartyId,
                IsComplex = result.Response.IsComplex,
                IsDefault = result.Response.IsDefault,
                Latitude = result.Response.Latitude,
                Line1 = result.Response.Line1,
                Line3 = result.Response.Line3,
                AddressType = result.Response.AddressType,
                StateProvince = result.Response.StateProvince,
                Country = result.Response.Country,
                Longitude = result.Response.Longitude
            };
            newAddress.Line1 = "this is an update";

            var updateresult = Connector.IndividualManagement.Individual(_indivdualId).EditAddress(newAddress);

            updateresult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            updateresult.Response.ShouldNotBeNull();

            //Get updated address
            var updatedResult = Connector.IndividualManagement.Individual(_indivdualId).GetAddress(_addressResponse.Response);

            updatedResult.Response.Code.ShouldEqual(newAddress.Code);
            updatedResult.Response.Complex.ShouldEqual(newAddress.Complex);
            updatedResult.Response.IsComplex.ShouldEqual(newAddress.IsComplex);
            updatedResult.Response.Line1.ShouldEqual(newAddress.Line1);
            updatedResult.Response.Line2.ShouldEqual(newAddress.Line2);
            updatedResult.Response.Line3.ShouldEqual(newAddress.Line3);
            updatedResult.Response.Line4.ShouldEqual(newAddress.Line4);
            updatedResult.Response.Latitude.ShouldEqual(newAddress.Latitude);
            updatedResult.Response.Longitude.ShouldEqual(newAddress.Longitude);
        }
    }
}