using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.Address;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Address
{
    public class when_getting_addresses_by_individual : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _responseAddress1;
        private readonly POSTResponseDto<int> _responseAddress2;

        public when_getting_addresses_by_individual() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _responseAddress1 =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .CreateAddress(NewCreateAddressDtoObjectMother.ValidCreateAddressDto());
            _responseAddress2 =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .CreateAddress(NewCreateAddressDtoObjectMother.ValidCreateAddressDto());
        }

        public override void Observe()
        {

        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            fetching_addresses_by_individual();
        }

        private void fetching_addresses_by_individual()
        {
            _responseAddress1.ShouldNotBeNull();
            _responseAddress1.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseAddress1.IsSuccess.ShouldBeTrue();

            _responseAddress2.ShouldNotBeNull();
            _responseAddress2.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseAddress2.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).GetAddresses();

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            Assert.True(result.Response.Results.Count == 2);
        }
    }
}