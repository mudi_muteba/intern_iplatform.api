using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.AssetVehicles;
using ValidationMessages.Individual.AssetVehicle;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.AssetVehicles
{
    public class when_creating_new_invalid_asset_vehicle : BaseTest
    {
        private readonly POSTResponseDto<int> _getResponse;

        public when_creating_new_invalid_asset_vehicle() : base(ApiUserObjectMother.AsAdmin())
        {
            var indivdualId = new ConnectorTestData(Connector).Individuals.Create();

            var newAssetVehicle = NewAssetVehicleDtoObjectMother.InvalidCreateAssetVehicleDto(indivdualId);

            _getResponse = Connector.IndividualManagement.Individual(indivdualId).CreateAssetVehicle(newAssetVehicle);

            var assetId = _getResponse.Response;
        }

        public override void Observe()
        {
      
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_asset_vehicle_is_saved();
        }

        private void the_asset_vehicle_is_saved()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            _getResponse.IsSuccess.ShouldBeFalse();

            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(AssetVehicleValidationMessages.VehicleModelRequired.MessageKey)).ShouldNotBeNull();
            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(AssetVehicleValidationMessages.VehicleMakeRequired.MessageKey)).ShouldNotBeNull();
            _getResponse.Errors.FirstOrDefault(a => a.MessageKey.Equals(AssetVehicleValidationMessages.VehicleMMCodeRequired.MessageKey)).ShouldNotBeNull();
        }
    }
}