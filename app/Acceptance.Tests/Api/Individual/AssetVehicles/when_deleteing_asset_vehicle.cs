using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.AssetVehicles;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.AssetVehicles
{
    public class when_deleteing_asset_vehicle : BaseTest
    {
        private readonly int _indivdualId;
        private readonly int _assetId;
        private readonly POSTResponseDto<int> _response;

        public when_deleteing_asset_vehicle() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var newAssetVehicle = NewAssetVehicleDtoObjectMother.ValidCreateAssetVehicleDto(_indivdualId);
            _response = Connector.IndividualManagement.Individual(_indivdualId).CreateAssetVehicle(newAssetVehicle);
            _assetId = _response.Response;
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_asset_vehicle_is_deleted();
        }

        private void the_asset_vehicle_is_deleted()
        {
            _response.ShouldNotBeNull();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).DeleteAssetVehicle(_assetId);

            result.StatusCode.ShouldEqual(HttpStatusCode.Accepted);
            result.Response.ShouldNotBeNull();

            result.Response.ShouldEqual(_response.Response);
        }
    }
}