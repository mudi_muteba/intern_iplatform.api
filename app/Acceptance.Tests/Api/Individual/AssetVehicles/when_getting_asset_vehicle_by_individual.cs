using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.AssetVehicles;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.AssetVehicles
{
    public class when_getting_asset_vehicle_by_individual : BaseTest
    {
        private readonly int _indivdualId;
        private readonly POSTResponseDto<int> _responseAssetVehicle1;
        private readonly POSTResponseDto<int> _responseAssetVehicle2;

        public when_getting_asset_vehicle_by_individual() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _responseAssetVehicle1 =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .CreateAssetVehicle(NewAssetVehicleDtoObjectMother.ValidCreateAssetVehicleDto(_indivdualId));
            _responseAssetVehicle2 =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .CreateAssetVehicle(NewAssetVehicleDtoObjectMother.ValidCreateAssetVehicleDto(_indivdualId));
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            fetching_asset_vehicle_by_individual();
        }

        private void fetching_asset_vehicle_by_individual()
        {
            _responseAssetVehicle1.ShouldNotBeNull();
            _responseAssetVehicle1.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseAssetVehicle1.IsSuccess.ShouldBeTrue();

            _responseAssetVehicle2.ShouldNotBeNull();
            _responseAssetVehicle2.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseAssetVehicle2.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).GetAssetVehicles();

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            Assert.True(result.Response.Results.Count == 2);
        }
    }
}