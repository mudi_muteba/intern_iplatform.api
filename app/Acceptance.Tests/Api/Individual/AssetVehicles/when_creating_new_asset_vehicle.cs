using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.AssetVehicles;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.AssetVehicles
{
    public class when_creating_new_asset_vehicle : BaseTest
    {
        private readonly int _indivdualId;
        private readonly int _assetId;
        private readonly POSTResponseDto<int> _getResponse;
        private readonly CreateAssetVehicleDto _newAssetVehicle;

        public when_creating_new_asset_vehicle() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();

            _newAssetVehicle = NewAssetVehicleDtoObjectMother.ValidCreateAssetVehicleDto(_indivdualId);

            _getResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateAssetVehicle(_newAssetVehicle);

            _assetId = _getResponse.Response;
        }

        public override void Observe()
        {
           
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_asset_vehicle_is_saved();
        }

        private void the_asset_vehicle_is_saved()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).GetAssetVehicle(_assetId);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            result.Response.PartyId.ShouldEqual(_newAssetVehicle.PartyId);
            result.Response.VehicleMMCode.ShouldEqual(_newAssetVehicle.VehicleMMCode);
            result.Response.VehicleMake.ShouldEqual(_newAssetVehicle.VehicleMake);
            result.Response.VehicleModel.ShouldEqual(_newAssetVehicle.VehicleModel);
            result.Response.YearOfManufacture.ShouldEqual(_newAssetVehicle.YearOfManufacture);
            result.Response.VehicleRegistrationNumber.ShouldEqual(_newAssetVehicle.VehicleRegistrationNumber);
        }
    }
}