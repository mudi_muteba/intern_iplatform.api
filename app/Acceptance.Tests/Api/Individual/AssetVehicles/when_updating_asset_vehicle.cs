using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Assets.AssetVehicles;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.AssetVehicles;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.AssetVehicles
{
    public class when_updating_asset_vehicle : BaseTest
    {
        private readonly int _indivdualId;
        private readonly CreateAssetVehicleDto _newAssetVehicle;
        private readonly POSTResponseDto<int> _assetVehicleResponse;

        public when_updating_asset_vehicle() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _newAssetVehicle = NewAssetVehicleDtoObjectMother.ValidCreateAssetVehicleDto(_indivdualId);
            _assetVehicleResponse = Connector.IndividualManagement.Individual(_indivdualId).CreateAssetVehicle(_newAssetVehicle);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_asset_vehicle_is_updated();
        }

        private void the_asset_vehicle_is_updated()
        {
            //Check if details was saved
            _assetVehicleResponse.ShouldNotBeNull();
            _assetVehicleResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _assetVehicleResponse.IsSuccess.ShouldBeTrue();

            //get saved details
            var result = Connector.IndividualManagement.Individual(_indivdualId).GetAssetVehicle(_assetVehicleResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            //update details
            var newDetails = new EditAssetVehicleDto
            {
                Id = result.Response.Id,
                PartyId = result.Response.PartyId,
                VehicleMMCode = result.Response.VehicleMMCode,
                VehicleMake = result.Response.VehicleMake,
                VehicleModel = result.Response.VehicleModel,
                VehicleRegistrationNumber = result.Response.VehicleRegistrationNumber,
                VehicleType = result.Response.VehicleType,
                YearOfManufacture = result.Response.YearOfManufacture
            };
            newDetails.VehicleMMCode = "this a different MMCode";

            var updateresult = Connector.IndividualManagement.Individual(_indivdualId).EditAssetVehicle(newDetails);

            updateresult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            updateresult.Response.ShouldNotBeNull();

            //Get updated details
            var updatedResult = Connector.IndividualManagement.Individual(_indivdualId).GetAssetVehicle(_assetVehicleResponse.Response);

            result.Response.PartyId.ShouldEqual(_newAssetVehicle.PartyId);
            result.Response.VehicleMMCode.ShouldEqual(_newAssetVehicle.VehicleMMCode);
            result.Response.VehicleMake.ShouldEqual(_newAssetVehicle.VehicleMake);
            result.Response.VehicleModel.ShouldEqual(_newAssetVehicle.VehicleModel);
            result.Response.YearOfManufacture.ShouldEqual(_newAssetVehicle.YearOfManufacture);
            result.Response.VehicleRegistrationNumber.ShouldEqual(_newAssetVehicle.VehicleRegistrationNumber);
        }
    }
}