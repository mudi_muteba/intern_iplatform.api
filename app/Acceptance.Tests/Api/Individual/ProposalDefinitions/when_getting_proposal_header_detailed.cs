using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.ProposalHeaders;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalDefinition;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.ProposalDefinitions
{
    public class when_getting_proposal_header_detailed : BaseTest
    {
        private readonly POSTResponseDto<int> _responseProposalDefinition1;
        private readonly POSTResponseDto<int> _responseProposalDefinition2;
        private readonly GETResponseDto<ProposalHeaderDetailedDto> _result;

        public when_getting_proposal_header_detailed() : base(ApiUserObjectMother.AsAdmin())
        {
            var indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            var proposalHeaderId = new ConnectorTestData(Connector).ProposalHeaders.Create(indivdualId);

            _responseProposalDefinition1 =
                Connector.IndividualManagement.Individual(indivdualId)
                    .Proposal(proposalHeaderId)
                    .CreateProposalDefinition(
                        NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_MOTOR(proposalHeaderId));
            _responseProposalDefinition2 =
                Connector.IndividualManagement.Individual(indivdualId)
                    .Proposal(proposalHeaderId)
                    .CreateProposalDefinition(
                        NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_MOTOR(proposalHeaderId));

            _result =
                Connector.IndividualManagement.Individual(indivdualId)
                    .Proposal(proposalHeaderId)
                    .GetDetailed();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            fetching_proposal_definitions_by_individual();
        }

        private void fetching_proposal_definitions_by_individual()
        {
            _responseProposalDefinition1.ShouldNotBeNull();
            _responseProposalDefinition1.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseProposalDefinition1.IsSuccess.ShouldBeTrue();

            _responseProposalDefinition2.ShouldNotBeNull();
            _responseProposalDefinition2.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseProposalDefinition2.IsSuccess.ShouldBeTrue();

            _result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _result.Response.ShouldNotBeNull();

            Assert.True(_result.Response.ProposalDefinitions.Count == 2);
        }
    }
}