using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalDefinition;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.ProposalDefinitions
{
    public class when_deleteing_a_ProposalDefinition : BaseTest
    {
        private readonly int _proposalHeaderId;
        private readonly DELETEResponseDto<DeleteProposalDefinitionResponseDto> _result;

        public when_deleteing_a_ProposalDefinition()
        {
            var indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _proposalHeaderId = new ConnectorTestData(Connector).ProposalHeaders.Create(indivdualId);

            var newProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_MOTOR(_proposalHeaderId);

            var proposalDefinitionId = Connector.IndividualManagement.Individual(indivdualId)
                .Proposal(_proposalHeaderId)
                .CreateProposalDefinition(newProposalDefinition)
                .Response;

            _result =
              Connector.IndividualManagement.Individual(indivdualId)
                  .Proposal(_proposalHeaderId)
                  .DeleteProposalDefinition(proposalDefinitionId);
        }

        public override void Observe()
        {
          
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_proposal_definition_is_deleted();
        }

        private void the_proposal_definition_is_deleted()
        {
            _result.StatusCode.ShouldEqual(HttpStatusCode.Accepted);
            _result.Response.ShouldNotBeNull();

            _result.Response.ProposalHeaderId.ShouldEqual(_proposalHeaderId);
        }
    }
}