using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalDefinition;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.ProposalDefinitions
{
    public class when_update_a_proposal_definition : BaseTest
    {
        private readonly int _indivdualId;
        private readonly int _proposalHeaderId;
        private int _addressId;
        private int _proposalDefinitionId;
        private readonly EditProposalDefinitionDto _editProposalDefinition;
        private readonly POSTResponseDto<int> _getResponse;

        public when_update_a_proposal_definition() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _proposalHeaderId = new ConnectorTestData(Connector).ProposalHeaders.Create(_indivdualId);
            var newProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_MOTOR(_proposalHeaderId);

            _addressId = new ConnectorTestData(Connector).Individuals.CreateAddress(_indivdualId);

            _getResponse =
            Connector.IndividualManagement.Individual(_indivdualId)
                .Proposal(_proposalHeaderId)
                .CreateProposalDefinition(newProposalDefinition);

            _proposalDefinitionId = _getResponse.Response;

            _editProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidEditProposalDefinitionDto(_getResponse.Response, _proposalHeaderId);
        }

        public override void Observe()
        {
        
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_proposal_definition_is_saved();
        }

        private void the_proposal_definition_is_saved()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var result =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .GetProposalDefinition(_getResponse.Response);

            var answerA = result.Response.QuestionGroups[0].QuestionDefinitions[1].AnswerDto;
            var answerB = result.Response.QuestionGroups[0].QuestionDefinitions[2].AnswerDto;
            var answerC = result.Response.QuestionGroups[1].QuestionDefinitions[1].AnswerDto;

            answerA.Answer = "Somerset";
            answerB.Answer = "4321";
            answerC.Answer = "2010";

            _editProposalDefinition.Answers.Add(answerA);
            _editProposalDefinition.Answers.Add(answerB);
            _editProposalDefinition.Answers.Add(answerC);

            var updateResult =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .UpdateProposalDefinition(_editProposalDefinition);

            updateResult.ShouldNotBeNull();
            updateResult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            updateResult.IsSuccess.ShouldBeTrue();

            var updatedResult =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .GetProposalDefinition(_getResponse.Response);

            var answerAA = updatedResult.Response.QuestionGroups[0].QuestionDefinitions[1].AnswerDto;
            var answerBB = updatedResult.Response.QuestionGroups[0].QuestionDefinitions[2].AnswerDto;
            var answerCC = updatedResult.Response.QuestionGroups[1].QuestionDefinitions[1].AnswerDto;

            answerA.Answer.ShouldEqual(answerAA.Answer);
            answerB.Answer.ShouldEqual(answerBB.Answer);
            answerC.Answer.ShouldEqual(answerCC.Answer);
        }
    }
}