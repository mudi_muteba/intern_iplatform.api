using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using MasterData;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalDefinition;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.ProposalDefinitions
{
    public class when_creating_a_new_Proposal_Definition : BaseTest
    {
        private readonly int _addressId;
        private readonly int _indivdualId;
        private readonly int _proposalHeaderId;
        private readonly CreateProposalDefinitionDto _newProposalDefinition;
        private readonly POSTResponseDto<int> _response;

        public when_creating_a_new_Proposal_Definition() : base(ApiUserObjectMother.AsAdmin())
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _proposalHeaderId = new ConnectorTestData(Connector).ProposalHeaders.Create(_indivdualId);
            _newProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_MOTOR(_proposalHeaderId);

            _addressId = new ConnectorTestData(Connector).Individuals.CreateAddress(_indivdualId);
            _response = Connector.IndividualManagement.Individual(_indivdualId).Proposal(_proposalHeaderId).CreateProposalDefinition(_newProposalDefinition);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_proposal_definition_is_saved();
        }

        private void the_proposal_definition_is_saved()
        {
            _response.ShouldNotBeNull();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var result = Connector.IndividualManagement.Individual(_indivdualId).Proposal(_proposalHeaderId).GetProposalDefinition(_response.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            result.Response.ProductId.ShouldEqual(_newProposalDefinition.ProductId);

            var riskInformation = result.Response.QuestionGroups.FirstOrDefault(a => a.Id == QuestionGroups.RiskInformation.Id);
            riskInformation.ShouldNotBeNull();

            var yearOfManufacture = riskInformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.YearOfManufacture.Id);
            yearOfManufacture.Options.Count().ShouldEqual(35);

            var driverInformation = result.Response.QuestionGroups.FirstOrDefault(a => a.Id == QuestionGroups.DriverInformation.Id);
            riskInformation.ShouldNotBeNull();

            var mainDriverIDNumber = driverInformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.MainDriverIDNumber.Id);
            var options = mainDriverIDNumber.Options.Select(a => a.Name).ToList();
            //bool idNumberFound = options.Contains(NewIndividualDtoObjectMother.ValidIndividualDto().IdentityNo);
            //Assert.True(idNumberFound);

            var generalInformation = result.Response.QuestionGroups.FirstOrDefault(a => a.Id == QuestionGroups.GeneralInformation.Id);
            generalInformation.ShouldNotBeNull();

            var riskAddress = generalInformation.QuestionDefinitions.FirstOrDefault(a => a.Question.Id == Questions.OvernightAddress.Id);

            var riskAddressOptions = riskAddress.Options.Select(a => a.Id).ToList();
            var riskAddressFound = riskAddressOptions.Contains(_addressId.ToString());
            Assert.True(riskAddressFound);
        }
    }
}