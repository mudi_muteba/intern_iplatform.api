using System;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Individual.Import;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Individual.Import
{
    public class when_importing_individual : BaseTest
    {
        private readonly POSTResponseDto<Guid> _postResponse;

        // given
        public when_importing_individual() : base(ApiUserObjectMother.AsAdmin())
        {
            var importIndividualDto = NewImportIndividualDtoObjectMother.ValidImportIndividualDto();
            _postResponse = Connector.ImportManagement.Individual.Import(importIndividualDto);
        }

        // when
        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_imported_individual_is_created();
        }

        private void the_imported_individual_is_created()
        {
            _postResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postResponse.IsSuccess.ShouldBeTrue();
        }
    }
}