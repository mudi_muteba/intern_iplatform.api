﻿using iPlatform.Api.DTOs.Base.Connector;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.Authentication
{
  
    public class AuthenticationTests
    {
        private readonly Connector _connector;

        public AuthenticationTests(Connector connector)
        {
            _connector = connector;
        }

        public void run_all()
        {
            new WhenAuthenticatingAValidTokenIsReturnedBddfy().Run(_connector);
            new WhenAuthenticatingAValidUserBddfy().Run(_connector);
            new WhenAuthenticatingAValidUserWithTheWrongPasswordBddfy().Run(_connector);
            new WhenAuthenticatingAValidUserWithTheWrongUsernameBddfy().Run(_connector);
            new WhenAuthenticatingAnUnapprovedUserBddfy().Run(_connector);
        }
    }
}

