using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Authentication;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.Authentication
{
    public class WhenAuthenticatingAValidTokenIsReturnedBddfy : IBddfyTest
    {
        private AuthenticationRequestDto _authenticationRequest;
        private AuthenticateResponseDto _response;
        private int _createUserResult;
        private CreateUserDto _validUser;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            Connector.SetToken(token);
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _validUser = UserDtoObjectMother.ValidApprovedNewUser();
            _authenticationRequest = AuthenticationObjectMother.ValidAuthenticationRequest(_validUser);
        }

        public void When_action()
        {
            _createUserResult = new ConnectorTestData(Connector).Users.CreateUser(_validUser);

            _response = Connector.Authentication.Authenticate(_authenticationRequest);

        }

        public void Then_result()
        {
            _createUserResult.ShouldNotEqual(0);
            string.IsNullOrWhiteSpace(_response.Token).ShouldBeFalse();
        }
    }
}