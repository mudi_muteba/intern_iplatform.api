using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Authentication;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.Authentication
{
    public class WhenAuthenticatingAValidUserWithTheWrongUsernameBddfy : IBddfyTest
    {
        public  Connector Connector { get; set; }
        private AuthenticateResponseDto _response;
        private CreateUserDto _validUser;


        public void Run(Connector connector)
        {
            Connector = connector;
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            Connector.SetToken(token);

            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _validUser = UserDtoObjectMother.ValidApprovedNewUser();
            new ConnectorTestData(Connector).Users.CreateUser(_validUser);
        }

        public void When_action()
        {
            var authenticationRequest = new AuthenticationRequestDto
            {
                Email = _validUser.UserName + "MAKE IT WRONG",
                Password = _validUser.Password
            };

            _response = Connector.Authentication.Authenticate(authenticationRequest);
        }

        public void Then_result()
        {
            _response.IsAuthenticated.ShouldBeFalse();
            string.IsNullOrWhiteSpace(_response.Token).ShouldBeTrue();

        }
    }
}