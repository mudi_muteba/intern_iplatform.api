using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Users.Authentication;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.Authentication
{
    public class WhenAuthenticatingAValidUserBddfy : IBddfyTest
    {
        private AuthenticationRequestDto _authenticationRequest;
        private AuthenticateResponseDto _response;
        private int _userId;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            Connector.SetToken(token);

            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var validUser = UserDtoObjectMother.ValidApprovedNewUser();
            _userId = new ConnectorTestData(Connector).Users.CreateUser(validUser);

            _authenticationRequest = AuthenticationObjectMother.ValidAuthenticationRequest(validUser);
            _response = Connector.Authentication.Authenticate(_authenticationRequest);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            the_user_is_authenticated();
            an_authorisation_token_is_returned();
            user_information_is_returned();
        }

        private void the_user_is_authenticated()
        {
            _response.IsAuthenticated.ShouldBeTrue();
            Connector.IndividualManagement.Individual(137);
        }
        private void an_authorisation_token_is_returned()
        {
            string.IsNullOrWhiteSpace(_response.Token).ShouldBeFalse();
        }
        private void the_user_is_marked_as_logged_in()
        {
            var getUserResponse = Connector.UserManagement.Users.Get(_userId);

            getUserResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            var user = getUserResponse.Response;

            user.IsLoggedIn.ShouldBeTrue();
            user.LoggedInFromSystem.ShouldEqual(_authenticationRequest.System);
        }
        private void user_information_is_returned()
        {
            _response.User.ShouldNotBeNull();

            // all groups are returned
            var user = _response.User;

            var allocatedGroups = user.Groups.FirstOrDefault(g => g.ChannelIds.Contains(1)).Groups;
            allocatedGroups.All(g => g.Id == 1).ShouldBeTrue();

            allocatedGroups = user.Groups.FirstOrDefault(g => g.ChannelIds.Contains(1)).Groups;
            allocatedGroups.All(g => g.Id == 1 || g.Id == 2).ShouldBeTrue();

            // the users to which the user belongs is returned
            user.Channels
                .All(c => c.Id == 1 || c.Id == 2)
                .ShouldBeTrue();

            // the system authorisation for the user is available
            var channel1Authorisation = user.SystemAuthorisation.Channels.FirstOrDefault(c => c.ChannelId == 1);
            channel1Authorisation.Points.ShouldNotBeEmpty();

            var channel2Authorisation = user.SystemAuthorisation.Channels.FirstOrDefault(c => c.ChannelId == 2);
            channel2Authorisation.Points.ShouldNotBeEmpty();
        }
    }
}