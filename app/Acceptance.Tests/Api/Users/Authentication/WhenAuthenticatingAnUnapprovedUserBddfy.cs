using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Users.Authentication;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.Authentication
{
    public class WhenAuthenticatingAnUnapprovedUserBddfy : IBddfyTest
    {
        private AuthenticateResponseDto _response; 
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            Connector.SetToken(token);

            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var invalidUser = UserDtoObjectMother.UnapprovedUser();
            new ConnectorTestData(Connector).Users.CreateUser(invalidUser);

            var authenticationRequest = new AuthenticationRequestDto
            {
                Email = invalidUser.UserName,
                Password = invalidUser.Password
            };

            _response = Connector.Authentication.Authenticate(authenticationRequest);
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _response.IsAuthenticated.ShouldBeFalse();

            string.IsNullOrWhiteSpace(_response.Token).ShouldBeTrue();

        }
    }
}