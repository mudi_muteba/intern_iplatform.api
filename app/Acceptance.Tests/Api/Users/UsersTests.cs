using Acceptance.Tests.Api.Users.bddfy;
using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.Users
{
    public class UsersTests
    {
        private readonly Connector _connector;

        public UsersTests(Connector connector)
        {
            _connector = connector;
            var token = new ConnectorTestData(_connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _connector.SetToken(token);
        }

        public void run_all()
        {
            new WhenDisablingAUserBddfy().Run(_connector);
            new WhenCreatingANewUserBddfy().Run(_connector);
            new WhenEditingAnExistingUserBddfy().Run(_connector);
            new WhenPaginatingThroughAListOfUsersBddfy().Run(_connector);
            new WhenRegisteringANewUserBddfy().Run(_connector);
            new WhenRemovingAUserFromAChannelTheAuthorisationForTheChannelIsAlsoRemovedBddfy().Run(_connector);
            new WhenRequestingAListOfUsersBddfy().Run(_connector);
            new WhenRequestingANonExistingUserBddfy().Run(_connector);
            new WhenSearchingForUsersBddfy().Run(_connector);
            new WhenResettingAPasswordForAUserBddfy().Run(_connector);
            new WhenApprovingAUserBddfy().Run(_connector);

        }
    }
}