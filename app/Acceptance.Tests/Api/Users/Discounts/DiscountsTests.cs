﻿using Acceptance.Tests.Api.Users.bddfy;
using Acceptance.Tests.Api.Users.Discounts.bddfy;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.Discounts
{

    using bddfy;
    using iPlatform.Api.DTOs.Base.Connector;

    public class DiscountsTests
    {
        private readonly Connector _connector;

        public DiscountsTests(Connector connector)
        {
            _connector = connector;
            var token = new ConnectorTestData(_connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _connector.SetToken(token);
        }

        public void run_all()
        {
            new WhenCreatingADuplicateDiscountBddfy().Run(_connector);
            new WhenCreatingANewDiscountBddfy().Run(_connector);
            new WhenCreatingAnInvalidDiscountBddfy().Run(_connector);
            new WhenDeleteingAnDiscountBddfy().Run(_connector);
            new WhenGettingDiscountByCriteriaBddfy().Run(_connector);
            new WhenGettingDiscountsByUserBddfy().Run(_connector);
            new WhenUpdatingAnDiscountBddfy().Run(_connector);
        }
    }

}


