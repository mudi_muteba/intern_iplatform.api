using System.Net;
using Domain.Products;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper.Helpers;
using TestObjects.Mothers.Users.Discounts;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.Discounts.bddfy
{
    public class WhenDeleteingAnDiscountBddfy : IBddfyTest
    {
        private int _userId;
        private POSTResponseDto<int> _getResponse;
        private DELETEResponseDto<int> _result;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _userId = new ConnectorTestData(Connector).Users.Create();

            var newDiscount = NewCreateDiscountDtoObjectMother.ValidCreateDiscountDto(_userId, 3);
            _getResponse = Connector.UserManagement.User(_userId).CreateDiscount(newDiscount);

        }

        public void When_action()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            _result = Connector.UserManagement.User(_userId).DeleteDiscount(_getResponse.Response);
        }

        public void Then_result()
        {

            _result.StatusCode.ShouldEqual(HttpStatusCode.Accepted);
            _result.Response.ShouldNotBeNull();
            _result.Response.ShouldEqual(_getResponse.Response);

        }
    }
}