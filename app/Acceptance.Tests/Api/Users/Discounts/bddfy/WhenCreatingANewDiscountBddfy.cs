using System.Net;
using Domain.Products;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users.Discount;
using TestHelper.Helpers;
using TestHelper.TestEntities;
using TestObjects.Mothers.Users;
using TestObjects.Mothers.Users.Discounts;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.Discounts.bddfy
{
    public class WhenCreatingANewDiscountBddfy : IBddfyTest
    {
        private CreateDiscountDto _newDiscount;
        private int _userId;
        private POSTResponseDto<int> _postResponse;
        private CoverDefinition _coverDefinition;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _userId = new ConnectorTestData(Connector).Users.CreateUser(UserDtoObjectMother.ValidApprovedNewUser());
            _coverDefinition = CoverDefinitions.CoverDefinition3;

            _newDiscount = NewCreateDiscountDtoObjectMother.ValidCreateDiscountDto(_userId, 3);
            _postResponse = Connector.UserManagement.User(_userId).CreateDiscount(_newDiscount);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            the_User_is_created();
            the_discount_is_saved();

        }
        private void the_User_is_created()
        {
            _postResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postResponse.IsSuccess.ShouldBeTrue();
        }

        private void the_discount_is_saved()
        {
            _postResponse.ShouldNotBeNull();
            _postResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.UserManagement.User(_userId).GetDiscount(_postResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            result.Response.CoverDefinition.Id.ShouldEqual(_newDiscount.CoverDefinitionId);
            result.Response.Discount.ShouldEqual(_newDiscount.Discount);
            result.Response.User.Id.ShouldEqual(_newDiscount.UserId);
        }
    }
}