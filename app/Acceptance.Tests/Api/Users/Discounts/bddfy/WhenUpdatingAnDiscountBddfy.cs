using System.Net;
using Domain.Products;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users.Discount;
using TestHelper.Helpers;
using TestHelper.TestEntities;
using TestObjects.Mothers.Users.Discounts;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.Discounts.bddfy
{
    public class WhenUpdatingAnDiscountBddfy : IBddfyTest
    {
        private int _userId;
        private POSTResponseDto<int> _discountResponse;
        private PUTResponseDto<int> _updateresult;
        private EditDiscountDto _newDiscount;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _userId = new ConnectorTestData(Connector).Users.Create();

            var newDiscount = NewCreateDiscountDtoObjectMother.ValidCreateDiscountDto(_userId, 3);
            _discountResponse = Connector.UserManagement.User(_userId).CreateDiscount(newDiscount);

        }

        public void When_action()
        {
            //Check if Discount is saved
            _discountResponse.ShouldNotBeNull();
            _discountResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _discountResponse.IsSuccess.ShouldBeTrue();

            //get saved Discount
            var result = Connector.UserManagement.User(_userId).GetDiscount(_discountResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();

            //update Discount
            _newDiscount = new EditDiscountDto
            {
                Id = result.Response.Id,
                CoverDefinitionId = result.Response.CoverDefinition.Id,
                UserId = result.Response.User.Id
            };
            _newDiscount.Discount = (decimal)40.4;

            _updateresult = Connector.UserManagement.User(_userId).EditDiscount(_newDiscount);
        }

        public void Then_result()
        {
            _updateresult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _updateresult.Response.ShouldNotBeNull();

            //Get updated Discount
            var updatedResult = Connector.UserManagement.User(_userId).GetDiscount(_discountResponse.Response);

            updatedResult.Response.Discount.ShouldEqual(_newDiscount.Discount);
            updatedResult.Response.CoverDefinition.Id.ShouldEqual(_newDiscount.CoverDefinitionId);
        }
    }
}