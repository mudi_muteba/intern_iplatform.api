using System.Net;
using Domain.Products;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users.Discount;
using TestHelper.Helpers;
using TestHelper.TestEntities;
using TestObjects.Mothers.Users.Discounts;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.Discounts.bddfy
{
    public class WhenGettingDiscountByCriteriaBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _responseDiscount1;
        private int _userId;
        private CoverDefinition _coverDefinition;
        private CreateDiscountDto _discount;
        private POSTResponseDto<DiscountDto> _result;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _userId = new ConnectorTestData(Connector).Users.Create();
            _coverDefinition = CoverDefinitions.CoverDefinition3;
            _discount = NewCreateDiscountDtoObjectMother.ValidCreateDiscountDto(_userId, 3);
            _responseDiscount1 = Connector.UserManagement.User(_userId).CreateDiscount(_discount);

        }

        public void When_action()
        {
            _responseDiscount1.ShouldNotBeNull();
            _responseDiscount1.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseDiscount1.IsSuccess.ShouldBeTrue();


            _result = Connector.UserManagement.User(_userId).GetDiscountByCriteria(new SearchDiscountDto
            {
                CoverId = _coverDefinition.Cover.Id,
                ProductId = _coverDefinition.Product.Id,
                UserId = _userId
            });
        }

        public void Then_result()
        {


            _result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _result.Response.ShouldNotBeNull();

            Assert.True(_result.Response.Discount == _discount.Discount);
            Assert.True(_result.Response.CoverDefinition.Id == _coverDefinition.Id);
        }
    }
}