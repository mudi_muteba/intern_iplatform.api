using System.Net;
using Domain.Products;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users.Discount;
using TestHelper.Helpers;
using TestHelper.TestEntities;
using TestObjects.Mothers.Users.Discounts;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.Discounts.bddfy
{
    public class WhenGettingDiscountsByUserBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _responseDiscount1;
        private POSTResponseDto<int> _responseDiscount2;
        private int _userId;
        private LISTResponseDto<ListDiscountDto> _result;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _userId = new ConnectorTestData(Connector).Users.Create();
            _responseDiscount1 = Connector.UserManagement.User(_userId).CreateDiscount(NewCreateDiscountDtoObjectMother.ValidCreateDiscountDto(_userId, 3));

            var discount1 = NewCreateDiscountDtoObjectMother.ValidCreateDiscountDto(_userId, 3);
            discount1.CoverDefinitionId = 2;
            discount1.ChannelId = 1;
            _responseDiscount2 = Connector.UserManagement.User(_userId).CreateDiscount(discount1);

        }

        public void When_action()
        {
            _responseDiscount1.ShouldNotBeNull();
            _responseDiscount1.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseDiscount1.IsSuccess.ShouldBeTrue();

            _responseDiscount2.ShouldNotBeNull();
            _responseDiscount2.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseDiscount2.IsSuccess.ShouldBeTrue();

            _result = Connector.UserManagement.User(_userId).GetDiscounts(1);
        }

        public void Then_result()
        {
            _result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _result.Response.ShouldNotBeNull();
            _result.Response.Results.Count.ShouldEqual(2);
        }
    }
}