using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users.Discount;
using TestHelper.Helpers;
using TestObjects.Mothers.Users.Discounts;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.Discounts.bddfy
{
    public class WhenCreatingADuplicateDiscountBddfy : IBddfyTest
    {
        private int _userId;
        private POSTResponseDto<int> _getResponse;
        private CreateDiscountDto _newDiscount;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _newDiscount = NewCreateDiscountDtoObjectMother.InvalidCreateDiscountDto(_userId);
            _userId = new ConnectorTestData(Connector).Users.Create();

        }

        public void When_action()
        {
            _getResponse = Connector.UserManagement.User(_userId).CreateDiscount(_newDiscount);

        }

        public void Then_result()
        {
            _getResponse.ShouldNotBeNull();
            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.NotFound);
            _getResponse.IsSuccess.ShouldBeFalse();

        }
    }
}