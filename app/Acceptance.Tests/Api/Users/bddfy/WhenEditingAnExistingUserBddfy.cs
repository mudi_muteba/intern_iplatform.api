using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users;
using MasterData;
using TestHelper.Helpers;
using TestHelper.Helpers.Extensions;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.bddfy
{
    public class WhenEditingAnExistingUserBddfy : IBddfyTest
    {
        private EditUserDto _editUser;
        private CreateUserDto _createUser;
        private PUTResponseDto<int> _response;
        private int _existingUserId;
        private GETResponseDto<UserDto> _getResponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }


        public void Given_data()
        {
            _createUser = UserDtoObjectMother.ValidApprovedNewUser();
            _existingUserId = new ConnectorTestData(Connector).Users.CreateUser(_createUser);
            _editUser = UserDtoObjectMother.EditExistingUser(_existingUserId);

        }
        public void When_action()
        {
            _response = Connector.UserManagement.User(_existingUserId).Edit(_editUser);
        }
        public void Then_result()
        {
            the_change_is_successful();
            the_user_can_be_retrieved_by_id();
        }


        private void the_change_is_successful()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }

        private void the_user_can_be_retrieved_by_id()
        {
            _getResponse = Connector.UserManagement.Users.Get(_existingUserId);

            _getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getResponse.IsSuccess.ShouldBeTrue();

            var user = _getResponse.Response;

            user.ShouldNotBeNull();
            user.UserName.ShouldEqual(_createUser.UserName); // username can not be changed
            user.IsActive.ShouldBeTrue();
            user.IsApproved.ShouldBeTrue();

            user.Channels.ShouldNotBeEmpty();

            foreach (var expectedChannel in _editUser.Channels)
            {
                var actualChannel = user.Channels.FirstOrDefault(a => a.Id == expectedChannel);

                actualChannel.ShouldNotBeNull();
            }

            user.Groups.ShouldNotBeEmpty();

            foreach (UserAuthorisationGroupDto expectedChannel in _editUser.Groups)
            {
                var actualChannel = user.Groups.FirstOrDefault(g => g.ChannelIds.Contains(expectedChannel.ChannelIds[0]));

                actualChannel.ShouldNotBeNull();

                actualChannel.Groups.IsEqual<AuthorisationGroup>(expectedChannel.Groups, (a, e) => a.Id == e.Id);
            }
        }

    }
}