using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.bddfy
{
    public class WhenRegisteringANewUserBddfy : IBddfyTest
    {
        private RegisterUserDto _newUser;
        private POSTResponseDto<int> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _newUser = UserDtoObjectMother.NewUserRegistration2();
            _response = Connector.UserManagement.Users.RegisterUser(_newUser);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            the_user_is_created();
            the_user_can_be_retrieved_by_id();
            an_individual_is_created_for_the_registered_user();

        }


        private void the_user_is_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }

        private void the_user_can_be_retrieved_by_id()
        {
            var getResponse = Connector.UserManagement.Users.Get(_response.Response);

            getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            getResponse.IsSuccess.ShouldBeTrue();

            var user = getResponse.Response;

            user.ShouldNotBeNull();
            user.UserName.ShouldEqual(_newUser.UserName);
            user.IsActive.ShouldBeFalse();
            user.IsApproved.ShouldBeFalse();
            user.IsLoggedIn.ShouldBeFalse();
            string.IsNullOrWhiteSpace(user.LoggedInFromSystem).ShouldBeTrue();
            string.IsNullOrWhiteSpace(user.LoggedInFromIP).ShouldBeTrue();
            user.LastLoggedInDate.Value.HasValue.ShouldBeFalse();

            user.Groups.ShouldBeEmpty();
        }

        private void an_individual_is_created_for_the_registered_user()
        {
            var getResponse = Connector.UserManagement.Users.Get(_response.Response);
            var user = getResponse.Response;

            user.Individuals.Count().ShouldEqual(1);
            user.Individuals.First().FirstName.ShouldEqual(_newUser.Name);
            user.Individuals.First().Surname.ShouldEqual(_newUser.Surname);
            user.Individuals.First().IdentityNo.ShouldEqual(_newUser.IdNumber);
            user.Individuals.First().DisplayName.ShouldEqual(_newUser.Surname + ", " + _newUser.Name);
            user.Individuals.First().GetPrimaryContactNumber.ShouldEqual(_newUser.PhoneNumber);
            user.Individuals.First().ContactDetail.Cell.ShouldEqual(_newUser.PhoneNumber);
            user.Individuals.First().ContactDetail.Email.ShouldEqual(_newUser.UserName);
        }
    }
}