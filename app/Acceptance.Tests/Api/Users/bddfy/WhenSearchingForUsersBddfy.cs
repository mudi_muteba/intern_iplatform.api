using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Search;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.bddfy
{
    public class WhenSearchingForUsersBddfy : IBddfyTest
    {
        private List<CreateUserDto> _allUsers = new List<CreateUserDto>();
        private List<Tuple<CreateUserDto, int>> _idMapping = new List<Tuple<CreateUserDto, int>>();
        private POSTResponseDto<PagedResultDto<ListUserDto>> _userSearchResponse;
        private UserSearchDto _searchQuery;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _searchQuery = new UserSearchDto
            {
                UserName = "mary",
                OrderBy = new List<OrderByField>
                {
                    new OrderByField
                    {
                        FieldName = "UserName",
                        Direction = OrderByDirection.Descending
                    }
                }
            };

        }

        public void When_action()
        {
            _userSearchResponse = Connector.UserManagement.Users.Search(_searchQuery);
        }

        public void Then_result()
        {
            a_list_of_users_are_returned();

        }

        private void a_list_of_users_are_returned()
        {
            _userSearchResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _userSearchResponse.IsSuccess.ShouldBeTrue();

            var userList = _userSearchResponse.Response;
            userList.PageNumber.ShouldEqual(1);
            userList.TotalPages.ShouldEqual(1);
            userList.TotalCount.ShouldEqual(4);
            userList.Results.Count.ShouldEqual(4);

            var expectedUsers = _idMapping
                .Where(u => u.Item1.UserName.StartsWith("mary"))
                .ToList();

            foreach (var expectedUser in expectedUsers)
                userList.Results.FirstOrDefault(r => r.Id == expectedUser.Item2).ShouldNotBeNull();

            var actualUserName = userList.Results.First().UserName;
            foreach (var actualUser in userList.Results)
            {
                var greater = (actualUserName.CompareTo(actualUser.UserName));
                (greater >= 0).ShouldBeTrue();
            }
        }

    }
}