using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.bddfy
{
    public class WhenRequestingAListOfUsersBddfy : IBddfyTest
    {
        private List<CreateUserDto> _allUsers = new List<CreateUserDto>();
        private List<Tuple<CreateUserDto, int>> _idMapping = new List<Tuple<CreateUserDto, int>>();
        private LISTPagedResponseDto<ListUserDto> _userListResponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var johnChannel12Users = UserDtoObjectMother.GenerateValidApprovedNewUsers(4, String.Format("{0}john{1}", new Random().Next(100, 999), new Random().Next(100, 999)), new List<int> { 1, 2 }).ToList();
            var maryChannel12Users = UserDtoObjectMother.GenerateValidApprovedNewUsers(4, String.Format("{0}mary{1}", new Random().Next(100, 999), new Random().Next(100, 999)), new List<int> { 1, 2 }).ToList();
            var richardChannel3Users = UserDtoObjectMother.GenerateValidApprovedNewUsers(4, String.Format("{0}richard{1}", new Random().Next(100, 999), new Random().Next(100, 999)), new List<int> { 1,2 }).ToList();

            _allUsers.AddRange(johnChannel12Users);
            _allUsers.AddRange(maryChannel12Users);
            _allUsers.AddRange(richardChannel3Users);


        }

        public void When_action()
        {
            var count = 99;
            foreach (var newUserDto in _allUsers)
            {
                newUserDto.ExternalReference = newUserDto.ExternalReference + count.ToString();
                var userId = new ConnectorTestData(Connector).Users.CreateUser(newUserDto);
                _idMapping.Add(new Tuple<CreateUserDto, int>(newUserDto, userId));
                count++;
            }

            _userListResponse = Connector.UserManagement.Users.Get();

        }

        public void Then_result()
        {
            a_list_of_users_are_returned();
        }


        private void a_list_of_users_are_returned()
        {
            _userListResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _userListResponse.IsSuccess.ShouldBeTrue();

            var userList = _userListResponse.Response;

            //these results are affected by users created in previous test
            userList.Results.ShouldNotBeEmpty();
            userList.PageNumber.ShouldEqual(1);
            userList.TotalPages.ShouldEqual(2);
            userList.TotalCount.ShouldEqual(29);

            foreach (var expectedUser in _idMapping)
                userList.Results.FirstOrDefault(r => r.Id == expectedUser.Item2).ShouldNotBeNull();
        }

    }
}