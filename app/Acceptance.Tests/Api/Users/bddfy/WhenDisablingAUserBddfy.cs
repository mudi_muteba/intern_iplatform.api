using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Authentication;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.bddfy
{
    public class WhenDisablingAUserBddfy : IBddfyTest
    {
        private PUTResponseDto<int> _response;
        private CreateUserDto _userData;

        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _userData = UserDtoObjectMother.ValidApprovedNewUser();
            var userId = new ConnectorTestData(Connector).Users.CreateUser(_userData);
            _response = Connector.UserManagement.User(userId).Disable();

        }
        public void When_action()
        {
            user_can_not_login();
        }
        public void Then_result()
        {
            the_user_is_disabled();
        }

        private void the_user_is_disabled()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }

        private void user_can_not_login()
        {
            var authenticationRequest = new AuthenticationRequestDto()
            {
                Password = _userData.Password,
                Email = _userData.UserName
            };
            Connector.Authentication.Authenticate(authenticationRequest).IsAuthenticated.ShouldBeFalse();
        }
    }
}