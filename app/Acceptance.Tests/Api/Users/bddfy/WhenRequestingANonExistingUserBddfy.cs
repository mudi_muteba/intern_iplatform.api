using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.bddfy
{
    public class WhenRequestingANonExistingUserBddfy : IBddfyTest
    {
        private GETResponseDto<UserDto> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {

        }

        public void When_action()
        {
            _response = Connector.UserManagement.Users.Get(500);
        }

        public void Then_result()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.NotFound);
            _response.IsSuccess.ShouldBeFalse();

        }
    }
}