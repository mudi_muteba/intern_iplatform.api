using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.bddfy
{
    public class WhenPaginatingThroughAListOfUsersBddfy : IBddfyTest
    {
        private List<CreateUserDto> _allUsers = new List<CreateUserDto>();
        private List<Tuple<CreateUserDto, int>> _idMapping = new List<Tuple<CreateUserDto, int>>();
        private LISTPagedResponseDto<ListUserDto> _userListResponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var johnChannel12Users = UserDtoObjectMother.GenerateValidApprovedNewUsers(4, "john", new List<int> { 1, 2 }).ToList();
            var maryChannel12Users = UserDtoObjectMother.GenerateValidApprovedNewUsers(4, "mary", new List<int> { 1, 2 }).ToList();
            var richardChannel3Users = UserDtoObjectMother.GenerateValidApprovedNewUsers(4, "richard", new List<int> { 1, 2 }).ToList();

            _allUsers.AddRange(johnChannel12Users);
            _allUsers.AddRange(maryChannel12Users);
            _allUsers.AddRange(richardChannel3Users);


            int count = 0;
            foreach (var newUserDto in _allUsers)
            {
                newUserDto.ExternalReference = newUserDto.ExternalReference + count.ToString();
                var newUserResponse = Connector.UserManagement.Users.CreateUser(newUserDto);
                var userId = newUserResponse.Response;
                _idMapping.Add(new Tuple<CreateUserDto, int>(newUserDto, userId));
                count++;
            }

            _userListResponse = Connector.UserManagement.Users.Get(2, 5);

        }
        public void When_action()
        {
        }
        public void Then_result()
        {
            a_list_of_users_are_returned();
        }

        private void a_list_of_users_are_returned()
        {
            _userListResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _userListResponse.IsSuccess.ShouldBeTrue();

            var userList = _userListResponse.Response;

            //these results are affected by users created in previous test
            userList.PageNumber.ShouldEqual(2);
            userList.TotalPages.ShouldEqual(3);
            userList.TotalCount.ShouldEqual(15); // 12 users from this test plus 3 from previous user tests
            userList.Results.Count.ShouldEqual(5);

            //var expectedUsers = _idMapping
            //    .OrderBy(u => u.Item1.UserName)
            //    .Skip(5)
            //    .Take(5)
            //    .ToList();

        }
    }
}