using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users;
using MasterData;
using TestHelper.Helpers.Extensions;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.bddfy
{
    public class WhenCreatingANewUserBddfy : IBddfyTest
    {
        private CreateUserDto _createUser;
        private POSTResponseDto<int> _response;

        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createUser = UserDtoObjectMother.ValidApprovedNewUser();
            _response = Connector.UserManagement.Users.CreateUser(_createUser);

        }
        public void When_action()
        {
            the_user_is_created();
        }
        public void Then_result()
        {
            the_user_can_be_retrieved_by_id();
        }


        private void the_user_is_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }

        private void the_user_can_be_retrieved_by_id()
        {
            var getResponse = Connector.UserManagement.Users.Get(_response.Response);

            getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            getResponse.IsSuccess.ShouldBeTrue();

            var user = getResponse.Response;

            user.ShouldNotBeNull();
            user.UserName.ShouldEqual(_createUser.UserName);
            user.IsActive.ShouldBeTrue();
            user.IsApproved.ShouldBeTrue();
            user.IsLoggedIn.ShouldBeFalse();
            string.IsNullOrWhiteSpace(user.LoggedInFromSystem).ShouldBeTrue();
            string.IsNullOrWhiteSpace(user.LoggedInFromIP).ShouldBeTrue();
            user.LastLoggedInDate.Value.HasValue.ShouldBeFalse();

            user.Groups.ShouldNotBeEmpty();

            foreach (UserAuthorisationGroupDto expectedChannel in _createUser.Groups)
            {


                var actualChannel = user.Groups.FirstOrDefault(g => g.ChannelIds.Contains(expectedChannel.ChannelIds[0]));

                actualChannel.ShouldNotBeNull();

                actualChannel.Groups.IsEqual<AuthorisationGroup>(expectedChannel.Groups, (a, e) => a.Id == e.Id);
            }

            user.Channels.ShouldNotBeEmpty();

            foreach (var expectedChannel in _createUser.Channels)
                user.Channels.FirstOrDefault(ac => ac.Id == expectedChannel).ShouldNotBeNull();

            user.Individuals.ShouldNotBeEmpty();

            foreach (var individualDto in user.Individuals)
                individualDto.FirstName.ShouldNotBeEmpty();
        }

    }
}