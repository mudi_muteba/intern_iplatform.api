using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Authentication;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.bddfy
{
    public class WhenResettingAPasswordForAUserBddfy : IBddfyTest
    {
        private const string NewPassword = "!This is the new password12345";
        private PUTResponseDto<int> _response;
        private CreateUserDto _validNewUser;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _validNewUser = UserDtoObjectMother.ValidApprovedNewUser();
            _validNewUser.ExternalReference = "new reference7889798";
            var passwordResetRequestResponse = new ConnectorTestData(Connector).Users.RequestPasswordReset(_validNewUser);
            _response = Connector.UserManagement.Users.ResetPassword(passwordResetRequestResponse.ResetToken, NewPassword);

        }

        public void When_action()
        {
            the_password_is_reset();
        }

        public void Then_result()
        {
            user_can_login_with_new_password();
        }

        private void the_password_is_reset()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }

        private void user_can_login_with_new_password()
        {
            var authenticationRequest = new AuthenticationRequestDto
            {
                Password = NewPassword,
                Email = _validNewUser.UserName
            };

            var _connector = new Connector();
            var response = _connector.Authentication.Authenticate(authenticationRequest);
            response.IsAuthenticated.ShouldBeTrue();
        }
    }
}