using System.Linq;
using System.Net;
using System.Net.Sockets;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Users;
using iPlatform.Api.DTOs.Users.Authentication;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.bddfy
{
    public class WhenApprovingAUserBddfy : IBddfyTest
    {
        private PUTResponseDto<int> _response;
        private GETResponseDto<UserDto> _user;
        private CreateUserDto _userData;

        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            
            // reauthenticateprevious test changes the context of the logedin user 
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            Connector.SetToken(token);

            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _userData = UserDtoObjectMother.ValidApprovedNewUser();
            _userData.ExternalReference = "new reference 09099890";
            var userId = new ConnectorTestData(Connector).Users.CreateUser(_userData);
            _response = Connector.UserManagement.User(userId).Approve();
            _user = Connector.UserManagement.Users.Get(userId);
        }
        public void When_action()
        {
            user_can_login();
        }
        public void Then_result()
        {
            the_user_is_approved();
        }

        private void the_user_is_approved()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _user.Response.IsApproved.ShouldBeTrue();
            _user.Response.IsActive.ShouldBeTrue();
        }

        private void user_can_login()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            var ip = host.AddressList.FirstOrDefault(a => a.AddressFamily == AddressFamily.InterNetwork);
            var authenticationRequest = new AuthenticationRequestDto
            {
                Password = _userData.Password,
                Email = _userData.UserName,
                IPAddress = ip == null ? "?" : ip.ToString(),
                System = "Testing"
            };

            var _connector = new Connector();
            _connector.Authentication.Authenticate(authenticationRequest).IsAuthenticated.ShouldBeTrue();
        }
    }
}