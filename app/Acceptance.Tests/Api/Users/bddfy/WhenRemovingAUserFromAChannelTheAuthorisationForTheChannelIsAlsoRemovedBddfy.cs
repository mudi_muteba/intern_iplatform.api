using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Users.bddfy
{
    public class WhenRemovingAUserFromAChannelTheAuthorisationForTheChannelIsAlsoRemovedBddfy : IBddfyTest
    {
        private PUTResponseDto<int> _response;
        private int _existingUserId;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createUser = UserDtoObjectMother.ValidApprovedNewUser();
            _existingUserId = new ConnectorTestData(Connector).Users.CreateUser(createUser);

        }

        public void When_action()
        {
            var editUser = UserDtoObjectMother.EditExistingUserWithMismatchedChannelAuthorisation(_existingUserId);
            _response = Connector.UserManagement.User(_existingUserId).Edit(editUser);
        }

        public void Then_result()
        {
            the_change_is_successful();
            the_user_only_has_authorisation_in_allocated_channels();

        }

        private void the_change_is_successful()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }

        private void the_user_only_has_authorisation_in_allocated_channels()
        {
            var getResponse = Connector.UserManagement.Users.Get(_existingUserId);

            getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            getResponse.IsSuccess.ShouldBeTrue();

            var user = getResponse.Response;

            user.Channels.ShouldNotBeEmpty();

            var authorisationGroupChannels = user.SystemAuthorisation.Channels.Select(c => c.ChannelId).ToList();

            user.Channels.Count.ShouldEqual(authorisationGroupChannels.Count); // same number of channels as authorisation in channels

            // get each authorisation and make sure the channel for the authorisation 
            // is in the allocated channels
            foreach (var channelAuthorisation in user.SystemAuthorisation.Channels)
                user.Channels.Any(c => c.Id == channelAuthorisation.ChannelId).ShouldBeTrue();
        }
    }
}