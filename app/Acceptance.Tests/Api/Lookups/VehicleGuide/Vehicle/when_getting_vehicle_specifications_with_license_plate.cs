﻿using System.Net;
using iGuide.DTOs.Specifications;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Vehicle
{
    public class when_getting_vehicle_specifications_with_license_plate : BaseTest
    {
        private readonly GETResponseDto<VehicleSpecsResultsDto> _response;

        public when_getting_vehicle_specifications_with_license_plate() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.VehicleGuide.GetVehicleByLicensePlate("XMC167GP");
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "Don't run automatically. Dependent on iGuide running and authorised user")]
        protected override void assert_all()
        {
            then_the_specifications_should_exist();
        }
        
        private void then_the_specifications_should_exist()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.Specifications.ShouldNotBeNull();
        }
    }
}
