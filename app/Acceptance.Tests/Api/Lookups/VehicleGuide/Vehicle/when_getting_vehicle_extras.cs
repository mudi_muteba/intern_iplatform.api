using System.Net;
using iGuide.DTOs.Values;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;
using TestHelper;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Vehicle
{
    public class when_getting_vehicle_extras : BaseTest
    {
        private readonly GETResponseDto<VehiclesOptionalExtrasDto> _response;

        public when_getting_vehicle_extras() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.VehicleGuide.GetVehicleExtras("02077400", 2012);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "Don't run automatically. Dependent on iGuide running and authorised user")]
        protected override void assert_all()
        {
            the_extras_are_returned();
        }

        private void the_extras_are_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);

            var availableExtras = _response.Response.AvailableExtras;
            availableExtras.ShouldNotBeEmpty();

            foreach (var extras in availableExtras)
                extras.Extras.ShouldNotBeEmpty();
        }
    }
}