using System.Net;
using iGuide.DTOs.Values;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;
using TestHelper;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Vehicle
{
    public class when_getting_vehicle_values : BaseTest
    {
        private readonly GETResponseDto<VehicleValuesDto> _response;

        public when_getting_vehicle_values() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.VehicleGuide.GetVehicleValue("02081285", 2004);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "Don't run automatically. Dependent on iGuide running and authorised user")]
        protected override void assert_all()
        {
            the_extras_are_returned();
        }
        
        private void the_extras_are_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);

            var availableValues = _response.Response.AvailableValues;
            availableValues.ShouldNotBeEmpty();
        }
    }
}