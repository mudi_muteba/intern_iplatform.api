using System.Net;
using iGuide.DTOs.Years;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;
using TestHelper;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Vehicle
{
    public class when_getting_available_years_for_vehicle : BaseTest
    {
        private readonly GETResponseDto<VehicleYearsDto> _response;

        public when_getting_available_years_for_vehicle() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.VehicleGuide.GetVehicleYears("02081285");
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "Don't run automatically. Dependent on iGuide running and authorised user")]
        protected override void assert_all()
        {
            the_extras_are_returned();
        }

        private void the_extras_are_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);

            var years = _response.Response.Years;
            years.ShouldNotBeEmpty();

            foreach (var year in years)
                year.AvailableYears.ShouldNotBeEmpty();
        }
    }
}