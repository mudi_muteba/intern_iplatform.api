using System.Net;
using iGuide.DTOs.Details;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;
using TestHelper;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Vehicle
{
    public class when_getting_vehicle_info : BaseTest
    {
        private readonly GETResponseDto<VehicleDetailsDto> _response;

        public when_getting_vehicle_info() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.VehicleGuide.GetVehicleInfo("02081285", 2004);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "Don't run automatically. Dependent on iGuide running and authorised user")]
        protected override void assert_all()
        {
            the_extras_are_returned();
        }

        private void the_extras_are_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);

            var availableInfo = _response.Response.AvailableInfo;
            availableInfo.ShouldNotBeEmpty();
        }
    }
}