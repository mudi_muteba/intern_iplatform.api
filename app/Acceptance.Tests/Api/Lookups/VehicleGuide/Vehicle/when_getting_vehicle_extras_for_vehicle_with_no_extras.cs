using System.Net;
using iGuide.DTOs.Values;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Vehicle
{
    public class when_getting_vehicle_extras_for_vehicle_with_no_extras : BaseTest
    {
        private readonly GETResponseDto<VehiclesOptionalExtrasDto> _response;

        public when_getting_vehicle_extras_for_vehicle_with_no_extras() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.VehicleGuide.GetVehicleExtras("02081285", 2004);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "Don't run automatically. Dependent on iGuide running and authorised user")]
        protected override void assert_all()
        {
            no_extras_are_returned();
        }

        private void no_extras_are_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.NotFound);
        }
    }
}