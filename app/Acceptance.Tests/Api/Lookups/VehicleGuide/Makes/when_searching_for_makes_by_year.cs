using System.Net;
using iGuide.DTOs.Makes;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;
using TestHelper;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Makes
{
    public class when_searching_for_makes_by_year : BaseTest
    {
        private readonly GETResponseDto<MakeSearchResultsDto> _response;

        public when_searching_for_makes_by_year() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.VehicleGuide.SearchMakes(2012, "Au");
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "Don't run automatically. Dependent on iGuide running and authorised user")]
        protected override void assert_all()
        {
            the_makes_are_returned();
        }

        private void the_makes_are_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.AvailableMakes.ShouldNotBeEmpty();

            foreach (var availableMakes in _response.Response.AvailableMakes)
                availableMakes.Makes.ShouldNotBeEmpty();
        }
    }
}