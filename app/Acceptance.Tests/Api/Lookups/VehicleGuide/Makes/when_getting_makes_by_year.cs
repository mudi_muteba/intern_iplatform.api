using System.Net;
using iGuide.DTOs.Makes;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Makes
{
    public class when_getting_makes_by_year : BaseTest
    {
        private readonly GETResponseDto<MakeSearchResultsDto> _response;

        public when_getting_makes_by_year() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.VehicleGuide.GetMakes(2012);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "Don't run automatically. Dependent on iGuide running and authorised user")]
        protected override void assert_all()
        {
            the_makes_are_returned();
        }

        private void the_makes_are_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.AvailableMakes.ShouldNotBeEmpty();

            foreach (var availableMakes in _response.Response.AvailableMakes)
                availableMakes.Makes.ShouldNotBeEmpty();
        }
    }
}