﻿using System.Net;
using iGuide.DTOs.Makes;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;
using TestHelper;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Makes
{
    public class when_getting_all_makes : BaseTest
    {
        private readonly GETResponseDto<MakeSearchResultsDto> _response;

        public when_getting_all_makes() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.VehicleGuide.GetMakes();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "Don't run automatically. Dependent on iGuide running and authorized user")]
        protected override void assert_all()
        {
            the_makes_are_returned();
        }

        private void the_makes_are_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.AvailableMakes.ShouldNotBeEmpty();

            foreach (var availableMakes in _response.Response.AvailableMakes)
                availableMakes.Makes.ShouldNotBeEmpty();
        }
    }
}