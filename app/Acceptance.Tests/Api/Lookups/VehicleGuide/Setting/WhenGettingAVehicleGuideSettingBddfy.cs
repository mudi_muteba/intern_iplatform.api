﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using TestObjects.Mothers.Lookups.VehicleGuideSettings;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Setting
{
    public class WhenGettingAVehicleGuideSettingBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private POSTResponseDto<int> _SaveResponse;
        private GETResponseDto<VehicleGuideSettingDto> _GetResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            CreateVehicleGuideSettingDto createVehicleGuideSettingDto =
                VehicleGuideSettingObjectMother.ValidCreateVehicleGuideSettingDto();

            _SaveResponse = Connector.VehicleGuideSetting.VehicleGuideSettings.Create(createVehicleGuideSettingDto);
        }

        public void When_action()
        {
            _SaveResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _SaveResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_SaveResponse.Response > 0);

            _GetResponse = Connector.VehicleGuideSetting.VehicleGuideSetting(_SaveResponse.Response).Get();
        }

        public void Then_result()
        {
            _GetResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _GetResponse.IsSuccess.ShouldBeTrue();

            VehicleGuideSettingDto vehicleGuideSettingDto = _GetResponse.Response;
            vehicleGuideSettingDto.ShouldNotBeNull();
        }
    }
}
