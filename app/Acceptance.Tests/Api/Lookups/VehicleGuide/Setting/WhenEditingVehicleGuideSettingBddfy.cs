﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using TestObjects.Mothers.Lookups.VehicleGuideSettings;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Setting
{
    public class WhenEditingVehicleGuideSettingBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private POSTResponseDto<int> _SaveResponse;
        private EditVehicleGuideSettingDto _EditVehicleGuideSettingDto;
        private PUTResponseDto<int> _EditResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            CreateVehicleGuideSettingDto createVehicleGuideSettingDto =
                VehicleGuideSettingObjectMother.ValidCreateVehicleGuideSettingDto();

            _SaveResponse = Connector.VehicleGuideSetting.VehicleGuideSettings.Create(createVehicleGuideSettingDto);
        }

        public void When_action()
        {
            _SaveResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _SaveResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_SaveResponse.Response > 0);

            _EditVehicleGuideSettingDto = VehicleGuideSettingObjectMother.ValidEditVehicleGuideSettingDto(_SaveResponse.Response);
            _EditResponse = Connector.VehicleGuideSetting.VehicleGuideSetting(_SaveResponse.Response).Edit(_EditVehicleGuideSettingDto);
        }

        public void Then_result()
        {
            _EditResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _EditResponse.IsSuccess.ShouldBeTrue();

            Assert.True(_EditResponse.Response > 0);
        }
    }
}
