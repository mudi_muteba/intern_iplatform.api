﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using TestObjects.Mothers.Lookups.VehicleGuideSettings;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Setting
{
    public class WhenCreatingVehicleGuideSettingBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private CreateVehicleGuideSettingDto _CreateVehicleGuideSettingDto;
        private POSTResponseDto<int> _Response;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _CreateVehicleGuideSettingDto = VehicleGuideSettingObjectMother.ValidCreateVehicleGuideSettingDto();
        }

        public void When_action()
        {
            _Response = Connector.VehicleGuideSetting.VehicleGuideSettings.Create(_CreateVehicleGuideSettingDto);
        }

        public void Then_result()
        {
            _Response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _Response.IsSuccess.ShouldBeTrue();

            Assert.True(_Response.Response > 0);
        }
    }
}
