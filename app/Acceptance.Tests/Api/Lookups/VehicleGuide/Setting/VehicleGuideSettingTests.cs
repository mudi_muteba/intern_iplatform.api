﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Setting
{
    public class VehicleGuideSettingTests
    {
        private readonly Connector _Connector;

        public VehicleGuideSettingTests(Connector connector)
        {
            _Connector = connector;
            string token = new ConnectorTestData(_Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _Connector.SetToken(token);
        }

        public void run_all()
        {
            new WhenCreatingVehicleGuideSettingBddfy().Run(_Connector);
            new WhenEditingVehicleGuideSettingBddfy().Run(_Connector);
            new WhenGettingAVehicleGuideSettingBddfy().Run(_Connector);
            new WhenGettingAVehicleGuideSettingListbddfy().Run(_Connector);
        }
    }
}
