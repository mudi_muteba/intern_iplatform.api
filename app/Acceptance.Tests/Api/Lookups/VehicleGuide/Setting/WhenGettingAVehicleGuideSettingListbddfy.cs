﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.VehicleGuide.VehicleGuideSetting;
using TestStack.BDDfy;
using Xunit;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Setting
{
    public class WhenGettingAVehicleGuideSettingListbddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private GETResponseDto<PagedResultDto<VehicleGuideSettingDto>> _Response;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
            _Response = Connector.VehicleGuideSetting.VehicleGuideSettings.GetAll();
        }

        public void Then_result()
        {
            Assert.True(_Response.Response.TotalCount > 0);
        }
    }
}
