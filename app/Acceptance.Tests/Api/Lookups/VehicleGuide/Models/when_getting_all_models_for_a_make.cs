﻿using System.Net;
using iGuide.DTOs.Models;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;
using TestHelper;

namespace Acceptance.Tests.Api.Lookups.VehicleGuide.Models
{
    public class when_getting_all_models_for_a_make : BaseTest
    {
        private readonly GETResponseDto<ModelSearchResultsDto> _response;

        public when_getting_all_models_for_a_make() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.VehicleGuide.GetModels("Mazda");
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "Don't run automatically. Dependent on iGuide running and authorised user")]
        protected override void assert_all()
        {
            the_models_are_returned();
        }

        private void the_models_are_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);

            var availableModels = _response.Response.AvailableModels;
            availableModels.ShouldNotBeEmpty();

            foreach (var models in availableModels)
                models.Models.ShouldNotBeEmpty();
        }
    }
}