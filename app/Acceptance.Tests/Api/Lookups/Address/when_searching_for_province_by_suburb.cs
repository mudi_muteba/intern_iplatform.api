﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Lookups.Address;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Lookups.Address
{
    public class when_searching_for_province_by_suburb : BaseTest
    {
        private readonly GETResponseDto<AddressLookupDto> _response;

        public when_searching_for_province_by_suburb() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.Address.GetProvinceBySuburb("JOHANNESBURG");
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_request_completes();
        }

        private void the_request_completes()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            _response.Response.Name.ShouldEqual("Gauteng");
        }
    }
}