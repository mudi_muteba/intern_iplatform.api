using System.Collections.Generic;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Occupation;
using TestHelper;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Lookups.Occupations
{
    public class when_searching_for_occupation : BaseTest
    {
        private readonly GETResponseDto<List<OccupationDto>> _response;

        public when_searching_for_occupation() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.Occupation.SearchOccupations("pilot");
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_request_completes();
        }

        private void the_request_completes()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            Assert.True(_response.Response.Count > 0);
        }
    }
}