using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Lookups.PostalCodes.Simplified
{
    public class when_getting_all_postcodes_with_pagination : BaseTest
    {
        private readonly LISTPagedResponseDto<MasterTypeDto> _postCodeResponse;

        public when_getting_all_postcodes_with_pagination() : base(ApiUserObjectMother.AsAdmin())
        {
            _postCodeResponse = Connector.Lookups.PostalCodes.GetSimplifiedPostalCodes(2, 50);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_request_completes();
            pagination_info_is_returned();
        }

        private void the_request_completes()
        {
            _postCodeResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postCodeResponse.IsSuccess.ShouldBeTrue();
        }

        private void pagination_info_is_returned()
        {
            var postCodeResult = _postCodeResponse.Response;

            postCodeResult.PageNumber.ShouldEqual(2);
            (postCodeResult.TotalCount > 0).ShouldBeTrue();
            (postCodeResult.TotalPages > 0).ShouldBeTrue();

            postCodeResult.Results.ShouldNotBeEmpty();

            postCodeResult.Results.All(IsValidPostalCodeInfo)
                .ShouldBeTrue();
        }

        private bool IsValidPostalCodeInfo(MasterTypeDto postalCode)
        {
            return !string.IsNullOrWhiteSpace(postalCode.Name)
                   && !string.IsNullOrWhiteSpace(postalCode.Code)
                   ;
        }
    }
}