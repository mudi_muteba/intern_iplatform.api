using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Lookups.PostalCodes;
using Xunit.Extensions;
using TestHelper;

namespace Acceptance.Tests.Api.Lookups.PostalCodes
{
    public class when_getting_all_postcodes_with_pagination : BaseTest
    {
        private readonly LISTPagedResponseDto<PostalCodeDto> _postCodeResponse;

        public when_getting_all_postcodes_with_pagination() : base(ApiUserObjectMother.AsAdmin())
        {
            _postCodeResponse = Connector.Lookups.PostalCodes.Get(2, 50);
        } 

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_request_completes();
            pagination_info_is_returned();
        }

        private void the_request_completes()
        {
            _postCodeResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postCodeResponse.IsSuccess.ShouldBeTrue();
        }

        private void pagination_info_is_returned()
        {
            var postCodeResult = _postCodeResponse.Response;

            postCodeResult.PageNumber.ShouldEqual(2);
            (postCodeResult.TotalCount > 0).ShouldBeTrue();
            (postCodeResult.TotalPages > 0).ShouldBeTrue();

            postCodeResult.Results.ShouldNotBeEmpty();

            postCodeResult.Results.All(IsValidPostalCodeInfo)
                .ShouldBeTrue();
        }

        private bool IsValidPostalCodeInfo(PostalCodeDto postalCode)
        {
            return !string.IsNullOrWhiteSpace(postalCode.City)
                   && !string.IsNullOrWhiteSpace(postalCode.Town)
                   && !string.IsNullOrWhiteSpace(postalCode.Country.Code)
                   //&& !string.IsNullOrWhiteSpace(postalCode.Code)
                   ;
        }
    }
}