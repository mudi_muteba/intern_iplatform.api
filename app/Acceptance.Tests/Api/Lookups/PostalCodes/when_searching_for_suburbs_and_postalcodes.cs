using System.Collections.Generic;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Lookups.PostalCodes;
using Xunit.Extensions;
using TestHelper;
using Xunit;

namespace Acceptance.Tests.Api.Lookups.PostalCodes
{
    public class when_searching_for_suburbs_and_postalcodes : BaseTest
    {
        private readonly GETResponseDto<List<PostalCodeDto>> _postCodeResponse;

        public when_searching_for_suburbs_and_postalcodes() : base(ApiUserObjectMother.AsAdmin())
        {
            _postCodeResponse = Connector.Lookups.PostalCodes.SearchSuburbsAndPostalCodes("2194");
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_request_completes();
        }

        private void the_request_completes()
        {
            _postCodeResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postCodeResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_postCodeResponse.Response.Count > 0);
        }
    }
}