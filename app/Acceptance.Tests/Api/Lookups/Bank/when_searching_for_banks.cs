﻿using System.Net;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Lookups.Banks
{
    public class when_searching_for_banks : BaseTest
    {
        private readonly POSTResponseDto<PagedResultDto<ListBankDto>> _response;

        public when_searching_for_banks() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.Bank.SearchBanks(new BankSearchDto { Name = "absa" });
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_request_completes();
        }

        private void the_request_completes()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}