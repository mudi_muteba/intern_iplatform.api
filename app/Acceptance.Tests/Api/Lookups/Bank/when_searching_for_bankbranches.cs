﻿using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Banks;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Lookups.Banks
{
    public class when_searching_for_bankbranches : BaseTest
    {
        private readonly POSTResponseDto<PagedResultDto<ListBankDto>> _response;

        public when_searching_for_bankbranches() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Lookups.Bank.SearchBanks(new BankSearchDto { Name = "absa" });
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_request_completes();
            the_branches_retrieved();
        }

        private void the_request_completes()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            Assert.True(_response.Response.Results.Count() > 0);
        }

        private void the_branches_retrieved()
        {
            var bankid = _response.Response.Results.First().Id;
            var result = Connector.Lookups.Bank.SearchBankBranches(new BankBranchesSearchDto { BankId = bankid });

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }
    }
}