using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.Campaigns
{
    public class CampaignTests
    {
        private readonly Connector _connector;

        public CampaignTests(Connector connector)
        {
            _connector = connector;
            var token = new ConnectorTestData(_connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _connector.SetToken(token);
        }

        public void run_all()
        {
            new WhenCreatingADuplicateCampaignBddfy().Run(_connector);
            new WhenCreatingANewCampaignBddfy().Run(_connector);
            new WhenCreatingANewCampaignWithInvalidAgentsBddfy().Run(_connector);
            new WhenCreatingANewCampaignWithInvalidProductsBddfy().Run(_connector);
            new WhenCreatingANewCampaignWithInvalidReferencesBddfy().Run(_connector);
            new WhenCreatingANewCampaignWithValidAgentsBddfy().Run(_connector);
            new WhenCreatingANewCampaignWithValidProductsBddfy().Run(_connector);
            new WhenCreatingANewCampaignWithValidReferenceBddfy().Run(_connector);
            new WhenCreatingANewCampaignWithValidSimilarCampaignBddfy().Run(_connector);
            new WhenDisablingACampaignBddfy().Run(_connector);
            new WhenFetchingACampaignBddfy().Run(_connector);
            new WhenRetrievingAllCampaignsBddfy().Run(_connector);
            new WhenSearchingCampaignsBddfy().Run(_connector);
            new WhenUpdatingACampaignBddfy().Run(_connector);
            
            //Missing testing data
            //new WhenUpdatingACampaignWithInvalidAgentsBddfy().Run(_connector);
        }
    }
}