using System;
using System.Collections.Generic;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Party;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Tags;
using TestObjects.Mothers.Campaigns;
using TestObjects.Mothers.Individual;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Campaigns
{
    public class WhenUpdatingACampaignWithInvalidAgentsBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createcampaignDto = NewCampaignDtoObjectMother.ValidTagCampaignDto();
            createcampaignDto.Agents.Add(new PartyDto
            {
                Id = _response.Response
            });

            var createResponse = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);

            createResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            createResponse.IsSuccess.ShouldBeTrue();

            var createDto = Connector.CampaignManagement.Campaign(createResponse.Response).Get().Response;

            var product = new List<ProductInfoDto>();
            foreach (var p in createDto.Products)
                product.Add(new ProductInfoDto { Id = p.Product.Id });

            var tags = new List<TagDto>();
            foreach (var p in createDto.Tags)
                tags.Add(new TagDto { Id = p.Tag.Id, Name = p.Tag.Name });

            var agents = new List<PartyDto> { new PartyDto { Id = 9999999 } };
            var editDto = new EditCampaignDto
            {
                ChannelId = createDto.ChannelId,
                Id = createDto.Id,
                StartDate = createDto.StartDate.Value.Value,
                Name = createDto.Name,
                DefaultCampaign = createDto.DefaultCampaign,
                EndDate = createDto.EndDate.Value,
                Reference = createDto.Reference,
                Products = product,
                Tags = tags,
                Agents = agents
            };

            var newDate = DateTime.UtcNow.AddDays(20);

            editDto.EndDate = newDate;

            var editResponse = Connector.CampaignManagement.Campaign(createResponse.Response).EditCampaign(editDto);

            editResponse.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            editResponse.IsSuccess.ShouldBeFalse();
        }
    }
}