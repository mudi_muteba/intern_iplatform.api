using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using TestObjects.Mothers.Campaigns;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Campaigns
{
    public class WhenRetrievingAllCampaignsBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        private CreateCampaignDto _createcampaignDto;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();

        }

        public void When_action()
        {
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(_createcampaignDto);

        }

        public void Then_result()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var result = Connector.CampaignManagement.Campaigns.GetAllCampaigns();
            Assert.True(result.Results.Count > 0);

        }
    }
}