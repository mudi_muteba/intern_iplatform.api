using System;
using System.Collections.Generic;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Products;
using TestObjects.Mothers.Campaigns;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Campaigns
{
    public class WhenUpdatingACampaignBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
        }

        public void When_action()
        {
        }

        public void Then_result()
      {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createDto = Connector.CampaignManagement.Campaign(_response.Response).Get().Response;
            var editDto = new EditCampaignDto
            {
                ChannelId = createDto.ChannelId,
                Id = createDto.Id,
                StartDate = createDto.StartDate.Value != null ? createDto.StartDate.Value.Value : DateTime.UtcNow,
                Name = createDto.Name,
                DefaultCampaign = createDto.DefaultCampaign,
                EndDate = createDto.EndDate.Value,
                Reference = createDto.Reference,
                Products = new List<ProductInfoDto>()
            };

            var newDate = DateTime.UtcNow.AddDays(20);

            editDto.EndDate = newDate;

            var editResponse = Connector.CampaignManagement.Campaign(_response.Response).EditCampaign(editDto);

            editResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);

            var result = Connector.CampaignManagement.Campaign(_response.Response).Get();
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();

            var updateDto = result.Response;
            updateDto.EndDate.Value.ToString().ShouldEqual(editDto.EndDate.ToString());
        }
    }
}