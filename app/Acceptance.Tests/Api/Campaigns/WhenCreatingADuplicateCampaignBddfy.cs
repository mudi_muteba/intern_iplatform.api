using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using TestObjects.Mothers.Campaigns;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Campaigns
{
    public class WhenCreatingADuplicateCampaignBddfy : IBddfyTest
    {
        private CreateCampaignDto _createcampaignDto;
        private POSTResponseDto<int> _response;
        private POSTResponseDto<int> _responseDuplicate;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(_createcampaignDto);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            _responseDuplicate = Connector.CampaignManagement.Campaigns.CreateCampaign(_createcampaignDto);
            _responseDuplicate.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);

        }
    }
}