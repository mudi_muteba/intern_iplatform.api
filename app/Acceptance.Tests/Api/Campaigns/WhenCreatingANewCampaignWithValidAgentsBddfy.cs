using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party;
using TestObjects.Mothers.Campaigns;
using TestObjects.Mothers.Individual;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Campaigns
{
    public class WhenCreatingANewCampaignWithValidAgentsBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _response.Errors.ShouldBeEmpty();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createcampaignDto = NewCampaignDtoObjectMother.ValidTagCampaignDto();
            createcampaignDto.Agents.Add(new PartyDto
            {
                Id = _response.Response
            });

            var createResponse = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);

            createResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            createResponse.IsSuccess.ShouldBeTrue();
        }
    }
}