using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Campaigns;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Campaigns
{
    public class when_creating_a_new_campaign_with_valid_tags : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_creating_a_new_campaign_with_valid_tags() : base(ApiUserObjectMother.AsAdmin(),ConnectionType.SqlServer)
        {
        }

        public override void Observe()
        {
            var createcampaignDto = NewCampaignDtoObjectMother.ValidTagCampaignDto();
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
        }

        [Observation(Skip = "tags not used any more, campaign references")]
        protected override void assert_all()
        {
            the_campaign_is_created_with_valid_tags();
        }

        private void the_campaign_is_created_with_valid_tags()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}