using System;
using System.Collections.Generic;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Tags;
using TestHelper;
using TestObjects.Mothers.Campaigns;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Campaigns
{
    public class when_updating_a_campaign_with_invalid_references : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_updating_a_campaign_with_invalid_references() : base(ApiUserObjectMother.AsAdmin())
        {
           // var createcampaignDto = NewCampaignDtoObjectMother.ValidReferenceCampaignDto();
          //  _response = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
          //  the_campaign_is_updated_with_invalid_references();
        }

        private void the_campaign_is_updated_with_invalid_references()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createDto = Connector.CampaignManagement.Campaign(_response.Response).Get().Response;

            var product = new List<ProductInfoDto>();
            foreach (var p in createDto.Products)
                product.Add(new ProductInfoDto {Id = p.Product.Id});

            var tags = new List<TagDto>();
            foreach (var p in createDto.Tags)
                tags.Add(new TagDto {Id = p.Tag.Id, Name = p.Tag.Name});

            var references = new List<CampaignReferenceDto>();
            foreach (var p in createDto.References)
                references.Add(new CampaignReferenceDto {Id = p.Id, Name = ""});

            var editDto = new EditCampaignDto
            {
                ChannelId = createDto.ChannelId,
                Id = createDto.Id,
                StartDate = createDto.StartDate.Value != null ? createDto.StartDate.Value.Value : DateTime.UtcNow,
                Name = createDto.Name,
                DefaultCampaign = createDto.DefaultCampaign,
                EndDate = createDto.EndDate.Value,
                Reference = createDto.Reference,
                Products = product,
                Tags = tags,
                References = references
            };

            var editResponse = Connector.CampaignManagement.Campaign(_response.Response).EditCampaign(editDto);

            editResponse.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            editResponse.IsSuccess.ShouldBeFalse();
        }
    }
}