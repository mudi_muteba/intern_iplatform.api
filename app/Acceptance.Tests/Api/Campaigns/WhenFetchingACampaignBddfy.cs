using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using TestObjects.Mothers.Campaigns;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Campaigns
{
    public class WhenFetchingACampaignBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        private CreateCampaignDto _createcampaignDto;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
        }

        public void When_action()
        {
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(_createcampaignDto);

        }

        public void Then_result()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var getResponse = Connector.CampaignManagement.Campaign(_response.Response).Get();
            //getResponse.Response.SetCulture(user.Culture);
            getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            getResponse.IsSuccess.ShouldBeTrue();

            var campaign = getResponse.Response;

            campaign.ShouldNotBeNull();
            campaign.Reference.ShouldEqual(_createcampaignDto.Reference);
            campaign.StartDate.Value.Value.ToString("yyyy-MM-dd").ShouldEqual(_createcampaignDto.StartDate.ToString("yyyy-MM-dd"));
            campaign.EndDate.Value.Value.ToString("yyyy-MM-dd").ShouldEqual(_createcampaignDto.EndDate.Value.ToString("yyyy-MM-dd"));
        }
    }
}