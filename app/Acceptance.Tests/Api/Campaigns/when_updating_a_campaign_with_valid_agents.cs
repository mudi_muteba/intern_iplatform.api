using System;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Party;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Tags;
using TestHelper;
using TestObjects.Mothers.Campaigns;
using Xunit.Extensions;
using System.Collections.Generic;
using TestObjects.Mothers.Individual;

namespace Acceptance.Tests.Api.Campaigns
{
    public class when_updating_a_campaign_with_valid_agents : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_updating_a_campaign_with_valid_agents() : base(ApiUserObjectMother.AsAdmin())
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_campaign_is_updated_with_valid_agents();
        }

        private void the_campaign_is_updated_with_valid_agents()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createcampaignDto = NewCampaignDtoObjectMother.ValidTagCampaignDto();
            createcampaignDto.Agents.Add(new PartyDto
            {
                Id = _response.Response
            });

            var createResponse = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);

            createResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            createResponse.IsSuccess.ShouldBeTrue();

            var createDto = Connector.CampaignManagement.Campaign(createResponse.Response).Get().Response;

            var product = new List<ProductInfoDto>();
            foreach (var p in createDto.Products)
                product.Add(new ProductInfoDto {Id = p.Product.Id});

            var tags = new List<TagDto>();
            foreach (var p in createDto.Tags)
                tags.Add(new TagDto {Id = p.Tag.Id, Name = p.Tag.Name});

            var agents = new List<PartyDto>();
            foreach (var p in createDto.Agents)
                agents.Add(new PartyDto {Id = p.Party.Id});

            var editDto = new EditCampaignDto
            {
                ChannelId = createDto.ChannelId,
                Id = createDto.Id,
                StartDate = createDto.StartDate.Value != null ? createDto.StartDate.Value.Value : DateTime.UtcNow,
                Name = createDto.Name,
                DefaultCampaign = createDto.DefaultCampaign,
                EndDate = createDto.EndDate.Value,
                Reference = createDto.Reference,
                Products = product,
                Tags = tags,
                Agents = agents
            };

            var newDate = DateTime.UtcNow.AddDays(20);

            editDto.EndDate = newDate;

            var editResponse = Connector.CampaignManagement.Campaign(_response.Response).EditCampaign(editDto);

            editResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);

            var updateDto = Connector.CampaignManagement.Campaign(createResponse.Response).Get().Response;

            updateDto.EndDate.Value.ToString().ShouldEqual(editDto.EndDate.Value.ToString());
        }
    }
}