using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.Campaigns;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Campaigns
{
    public class WhenCreatingANewCampaignWithInvalidProductsBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createcampaignDto = NewCampaignDtoObjectMother.InValidProductIdCampaignDto();
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            //Getting different and in correct results when using in memory
            _response.Errors.ShouldNotBeEmpty(); 
            
            //_response.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            //_response.IsSuccess.ShouldBeFalse();

        }
    }
}