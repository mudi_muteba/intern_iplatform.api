using System;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Products;
using TestHelper;
using TestObjects.Mothers.Campaigns;
using Xunit.Extensions;
using System.Collections.Generic;

namespace Acceptance.Tests.Api.Campaigns
{
    public class when_updating_a_campaign_with_invalid_products : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_updating_a_campaign_with_invalid_products() : base(ApiUserObjectMother.AsAdmin())
        {
            var createcampaignDto = NewCampaignDtoObjectMother.ValidProductCampaignDto();
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_campaign_is_updated_with_invalid_products();
        }

        private void the_campaign_is_updated_with_invalid_products()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createDto = Connector.CampaignManagement.Campaign(_response.Response).Get().Response;

            var product = new List<ProductInfoDto> {new ProductInfoDto {Id = 9999}};
            var editDto = new EditCampaignDto
            {
                ChannelId = createDto.ChannelId,
                Id = createDto.Id,
                StartDate = createDto.StartDate.Value != null ? createDto.StartDate.Value.Value : DateTime.UtcNow,
                Name = createDto.Name,
                DefaultCampaign = createDto.DefaultCampaign,
                EndDate = createDto.EndDate.Value,
                Reference = createDto.Reference,
                Products = product
            };

            var newDate = DateTime.UtcNow.AddDays(20);

            editDto.EndDate = newDate;

            var editResponse = Connector.CampaignManagement.Campaign(_response.Response).EditCampaign(editDto);

            editResponse.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            editResponse.IsSuccess.ShouldBeFalse();
        }
    }
}