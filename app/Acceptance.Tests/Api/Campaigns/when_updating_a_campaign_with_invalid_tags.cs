using System;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Tags;
using TestHelper;
using TestObjects.Mothers.Campaigns;
using Xunit.Extensions;
using System.Linq;
using System.Collections.Generic;

namespace Acceptance.Tests.Api.Campaigns
{
    public class when_updating_a_campaign_with_invalid_tags : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_updating_a_campaign_with_invalid_tags() : base(ApiUserObjectMother.AsAdmin())
        {
            var createcampaignDto = NewCampaignDtoObjectMother.ValidTagCampaignDto();
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_campaign_is_updated_with_invalid_tags();
        }

        private void the_campaign_is_updated_with_invalid_tags()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createDto = Connector.CampaignManagement.Campaign(_response.Response).Get().Response;

            var product = new List<ProductInfoDto>();
            foreach (var p in createDto.Products)
                product.Add(new ProductInfoDto {Id = p.Product.Id});

            var tag = new List<TagDto> {new TagDto {Id = 999999, Name = "INVALID"}};
            var editDto = new EditCampaignDto
            {
                ChannelId = createDto.ChannelId,
                Id = createDto.Id,
                StartDate = createDto.StartDate.Value.Value,
                Name = createDto.Name,
                DefaultCampaign = createDto.DefaultCampaign,
                EndDate = createDto.EndDate.Value,
                Reference = createDto.Reference,
                Products = product,
                Tags = tag
            };

            var newDate = DateTime.UtcNow.AddDays(20);

            editDto.EndDate = newDate;

            var editResponse = Connector.CampaignManagement.Campaign(_response.Response).EditCampaign(editDto);

            editResponse.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            editResponse.IsSuccess.ShouldBeFalse();
        }
    }
}