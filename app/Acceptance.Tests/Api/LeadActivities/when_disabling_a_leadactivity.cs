using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.LeadActivites
{
    public class when_disabling_a_leadactivity : BaseTest
    {
        private readonly POSTResponseDto<PagedResultDto<LeadActivityDto>> _response;

        public when_disabling_a_leadactivity() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.LeadManagement.LeadActivities.SearchLeadActivities(new LeadActivitySearchDto());
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_leadactivity_is_disabled();
        }

        private void the_leadactivity_is_disabled()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            Assert.True(_response.Response.TotalCount > 0);

            var result = Connector.LeadManagement.LeadActivity.DisableLeadActivity(
                new DisableLeadActivityDto { 
                    Id = _response.Response.TotalCount > 0 ? _response.Response.Results.First().Id : 1
                });
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}