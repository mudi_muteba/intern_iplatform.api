using System.Net;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.LeadActivites
{
    public class when_searching_leadActivities : BaseTest
    {
        private readonly POSTResponseDto<PagedResultDto<LeadActivityDto>> _response;
        
        public when_searching_leadActivities() : base(ApiUserObjectMother.AsAdmin())
        {
            var criteria = new LeadActivitySearchDto
            {
                // MaxItems = 100,
                //ActivityType = MasterData.ActivityTypes.Created,
                //AgentId = CurrentUser.Criteria.AgentId,
            };

            _response = Connector.LeadManagement.LeadActivities.SearchLeadActivities(criteria);
        }

        public override void Observe()
        {
           
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_lead_activities_search();
        }

        private void the_lead_activities_search()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            Assert.True(_response.Response.TotalCount >= 0);
        }
    }
}