using System.Net;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.LeadActivites
{
    public class when_calling_leadactivities_count : BaseTest
    {
        private readonly POSTResponseDto<LeadActivityCountDto> _response;

        public when_calling_leadactivities_count() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.LeadManagement.LeadActivities.GetLeadActivitiesCount(new LeadActivitySearchDto());
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            get_lead_activities_count();
        }

        private void get_lead_activities_count()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            Assert.True(_response.Response.Count > 0);
        }
    }
}