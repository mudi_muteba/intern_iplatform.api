using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.LeadActivites
{
    public class when_fetching_a_leadactivity : BaseTest
    {
        private readonly POSTResponseDto<PagedResultDto<LeadActivityDto>> _response;

        public when_fetching_a_leadactivity() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.LeadManagement.LeadActivities.SearchLeadActivities(new LeadActivitySearchDto());
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_leadactivity_is_received();
        }

        private void the_leadactivity_is_received()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            Assert.True(_response.Response.TotalCount > 0);

            if(_response.Response.TotalCount > 0)
            {
                var result = Connector.LeadManagement.LeadActivity.GetById(_response.Response.TotalCount > 0 ? _response.Response.Results.First().Id : 1);

                result.StatusCode.ShouldEqual(HttpStatusCode.OK);
                result.IsSuccess.ShouldBeTrue();
                Assert.True(_response.Response.Results.First().Id == result.Response.Id);
            }
        }
    }
}