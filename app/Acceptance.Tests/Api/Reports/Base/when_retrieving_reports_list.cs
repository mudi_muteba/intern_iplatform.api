﻿using System.Collections.Generic;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Reports.Base.Criteria;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Reports.Base
{
    public class when_retrieving_reports_list : BaseTest
    {
        private readonly GETResponseDto<List<ReportDto>> _reportListResponse;
        private readonly POSTResponseDto<List<ReportDto>> _reportListByCriteriaResponse;

        public when_retrieving_reports_list()
        {
            new TestHelper.Helpers.Workflow().Start();
            _reportListResponse = Connector.ReportManagement.Reports.GetReports();
            _reportListByCriteriaResponse = Connector.ReportManagement.Reports.GetReportsByCriteria(new ReportCriteriaDto
            {
                ChannelId = 1
            });
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            report_list_is_returned();
            report_list_by_criteria_is_returned();
        }

        private void report_list_is_returned()
        {
            _reportListResponse.ShouldNotBeNull();
            _reportListResponse.IsSuccess.ShouldBeTrue();
            _reportListResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            //reportListResponse.Response.ShouldNotBeEmpty();
        }

        private void report_list_by_criteria_is_returned()
        {
            _reportListByCriteriaResponse.ShouldNotBeNull();
            _reportListByCriteriaResponse.IsSuccess.ShouldBeTrue();
            _reportListByCriteriaResponse.StatusCode.ShouldEqual(HttpStatusCode.Created);
            //reportListByCriteriaResponse.Response.ShouldNotBeEmpty();
        }
    }
}