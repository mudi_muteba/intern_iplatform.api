using System.Net;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using TestHelper;
using TestObjects.Mothers.Claims;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Policy;
using Xunit.Extensions;
using iPlatform.Api.DTOs.Reports.Base;
using System.Collections.Generic;

namespace Acceptance.Tests.Api.Reports
{
    public class when_fetching_a_report : BaseTest
    {
        private readonly GETResponseDto<List<ReportDto>> _response;

        public when_fetching_a_report() : base(ApiUserObjectMother.AsAdmin(), ConnectionType.SqlServer)
        {
            _response = Connector.ReportManagement.Reports.GetReports();
            
        }

        public override void Observe()
        {
            
        }

        [Observation (Skip ="to be fixed")]
        protected override void assert_all()
        {
            _response.IsSuccess.ShouldBeTrue();
        }

    }
} 