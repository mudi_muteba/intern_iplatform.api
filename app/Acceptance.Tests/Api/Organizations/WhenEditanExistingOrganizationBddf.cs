using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit;
using Xunit.Extensions;
using TestObjects.Mothers.Organizations;
using iPlatform.Api.DTOs.Base.Connector;
using TestStack.BDDfy;

namespace Acceptance.Tests.Api.Organizations
{
    public class WhenEditanExistingOrganizationBddf : IBddfyTest
    {
        private POSTResponseDto<int> _saveresponse;
        private PUTResponseDto<int> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createOrganizationDto = OrganizationObjectMother.ValidDto();
            _saveresponse = Connector.OrganizationsManagement.Organizations.CreateOrganization(createOrganizationDto);

        }

        public void When_action()
        {
            _saveresponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _saveresponse.IsSuccess.ShouldBeTrue();

            Assert.True(_saveresponse.Response > 0);

            var editDto = OrganizationObjectMother.ValidEditDto(_saveresponse.Response);
            _response = Connector.OrganizationsManagement.Organization(_saveresponse.Response).Edit(editDto);

        }

        public void Then_result()
        {

            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            Assert.True(_response.Response > 0);
        }
    }

}