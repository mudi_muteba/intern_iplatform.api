using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.Organizations
{
    public class OrganizationTests
    {
        private readonly Connector _connector;

        public OrganizationTests(Connector connector)
        {
            _connector = connector;
            var token = new ConnectorTestData(_connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _connector.SetToken(token);
        }

        public void run_all()
        {
            new WhenCreatingANewOrganizationBddfy().Run(_connector);
            //new WhenEditanExistingOrganizationBddf().Run(_connector);
        }
    }
}