using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Products;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Products
{
    public class when_paginating_a_list_of_allocated_products : BaseTest
    {
        private readonly LISTPagedResponseDto<ListAllocatedProductDto> _allocatedProductsResponse;

        public when_paginating_a_list_of_allocated_products() : base(ApiUserObjectMother.AsAdmin())
        {
            _allocatedProductsResponse = Connector.ProductManagement.Products.Get(3, 1);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_allocated_products_are_returned();
            a_paginated_list_of_allocated_products_are_returned();
        }

        private void the_allocated_products_are_returned()
        {
            _allocatedProductsResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _allocatedProductsResponse.IsSuccess.ShouldBeTrue();
        }

        private void a_paginated_list_of_allocated_products_are_returned()
        {
            var allocatedProducts = _allocatedProductsResponse.Response;

            allocatedProducts.PageNumber.ShouldEqual(3);
            allocatedProducts.TotalPages.ShouldEqual(26);
            allocatedProducts.TotalCount.ShouldEqual(26);

            allocatedProducts.Results.First().ProductCode.ShouldEqual("ALLIN");
        }
    }
}