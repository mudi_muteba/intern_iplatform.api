using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Products;
using MasterData;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Products
{
    public class when_getting_the_covers_for_an_allocated_product : BaseTest
    {
        private const int ProductId = 33;
        private readonly GETResponseDto<CoverDefinitionDto> _getCoverResponse;

        public when_getting_the_covers_for_an_allocated_product() : base(ApiUserObjectMother.AsAdmin())
        {
            var actualProduct = Connector.ProductManagement.Products.Get(ProductId);
            var coverId = actualProduct.Response.Covers.First().Id;
            _getCoverResponse = Connector.ProductManagement.Product(ProductId).Cover(coverId);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_cover_information_is_returned();
            information_about_the_cover_is_available();
        }

        private void the_cover_information_is_returned()
        {
            _getCoverResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getCoverResponse.IsSuccess.ShouldBeTrue();
        }

        private void information_about_the_cover_is_available()
        {
            var cover = _getCoverResponse.Response;

            cover.DisplayName.ShouldEqual("Motor");
            cover.Cover.Id.ShouldEqual(Covers.Motor.Id);
            cover.CoverDefinitionType.Id.ShouldEqual(CoverDefinitionTypes.MainCover.Id);
            cover.QuestionDefinitions.Count.ShouldEqual(23);
            cover.Benefits.Count.ShouldEqual(20);
        }
    }
}