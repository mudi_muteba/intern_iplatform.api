﻿using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Products;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Products
{
    public class when_getting_a_list_of_products_by_cover : BaseTest
    {
        private readonly LISTResponseDto<ListProductByCoverDto> _allocatedProductsResponse;

        public when_getting_a_list_of_products_by_cover() : base(ApiUserObjectMother.AsAdmin())
        {
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.Channel2User());
            Connector = new Connector(new ApiToken(token));
            _allocatedProductsResponse = Connector.ProductManagement.Products.GetByCover(2);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_allocated_products_are_returned();
            only_channel_allocated_products_are_returned();
        }
        
        private void the_allocated_products_are_returned()
        {
            _allocatedProductsResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _allocatedProductsResponse.IsSuccess.ShouldBeTrue();
        }

        private void only_channel_allocated_products_are_returned()
        {
            var actualProducts = _allocatedProductsResponse.Response;
            actualProducts.Results.ShouldNotBeEmpty();
            Assert.True(actualProducts.Results.Count > 0);
        }
    }
}