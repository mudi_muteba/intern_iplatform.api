using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Products;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Products
{
    public class when_getting_an_allocated_product_by_id_not_assigned_to_the_users_channels : BaseTest
    {
        private readonly GETResponseDto<AllocatedProductDto> _getAllocatedProductResponse;

        public when_getting_an_allocated_product_by_id_not_assigned_to_the_users_channels() : base(ApiUserObjectMother.AsAdmin())
        {
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.Channel2User());
            Connector = new Connector(new ApiToken(token));
            _getAllocatedProductResponse = Connector.ProductManagement.Products.Get(13);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_allocated_product_is_not_returned();
        }
        
        private void the_allocated_product_is_not_returned()
        {
            _getAllocatedProductResponse.StatusCode.ShouldEqual(HttpStatusCode.NotFound);
        }
    }
}