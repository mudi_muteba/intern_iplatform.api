using System.Collections.Generic;
using System.Net;
using Domain.Products;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Products;
using TestHelper;
using TestHelper.Helpers.Extensions;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Products
{
    public class when_getting_the_proposal_form_for_the_multi_quote_product : BaseTest
    {
        private readonly GETResponseDto<ProposalFormDto> _getProposalFormResponse;
        private readonly int _productId = Product.MultiQuoteProductId;

        public when_getting_the_proposal_form_for_the_multi_quote_product() : base(ApiUserObjectMother.AsAdmin())
        {
            _getProposalFormResponse = Connector.ProductManagement.Product(_productId).ProposalForm();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_multi_quote_product_is_returned();
            the_proposal_form_structure_for_the_multi_quote_product_is_returned();
        }

        
        private void the_multi_quote_product_is_returned()
        {
            _getProposalFormResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getProposalFormResponse.IsSuccess.ShouldBeTrue();
        }

        private void the_proposal_form_structure_for_the_multi_quote_product_is_returned()
        {
            var proposalForm = _getProposalFormResponse.Response;
            proposalForm.Product.ProductCode.ShouldEqual("MUL");

            var expectedGroups = new List<string>
            {
                "QUESTION_GROUP_MOTOR_RISKINFORMATION",
                "QUESTION_GROUP_MOTOR_SECURITYINFORMATION",
                "QUESTION_GROUP_MOTOR_FINANCEINFORMATION",
                "QUESTION_GROUP_MOTOR_ADDITIONALOPTIONS",
                "QUESTION_GROUP_MOTOR_DRIVERINFORMATION",
                "QUESTION_GROUP_MOTOR_GENERALINFORMATION",
                "QUESTION_GROUP_MOTOR_INSURANCEHISTORY"
            };

            expectedGroups.IsEqual(proposalForm.Groups, (s, dto) => s.Equals(dto.QuestionGroupTranslationKey)).ShouldBeTrue();

            foreach (var @group in proposalForm.Groups)
                @group.Questions.ShouldNotBeEmpty();
        }
    }
}