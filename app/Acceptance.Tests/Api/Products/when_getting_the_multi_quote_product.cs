using System.Net;
using Domain.Products;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Products;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Products
{
    public class when_getting_the_multi_quote_product : BaseTest
    {
        private readonly GETResponseDto<AllocatedProductDto> _getAllocatedProductResponse;
        private readonly int _multiQuoteProductId = Product.MultiQuoteProductId;

        public when_getting_the_multi_quote_product() : base(ApiUserObjectMother.AsAdmin())
        {
            _getAllocatedProductResponse = Connector.ProductManagement.Products.Get(_multiQuoteProductId);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_allocated_product_is_returned();
        }

        private void the_allocated_product_is_returned()
        {
            _getAllocatedProductResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getAllocatedProductResponse.IsSuccess.ShouldBeTrue();
        }
    }
}