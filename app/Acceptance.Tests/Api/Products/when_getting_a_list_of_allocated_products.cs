﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Products;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Products
{
    public class when_getting_a_list_of_allocated_products : BaseTest
    {
        private readonly LISTPagedResponseDto<ListAllocatedProductDto> _allocatedProductsResponse;

        public when_getting_a_list_of_allocated_products() : base(ApiUserObjectMother.AsAdmin())
        {
            //var token = new ConnectorTestData(connector)
            //    .Authentication.Authenticate(AuthenticationObjectMother.Channel2User());

            //connector = new Connector(new ApiToken(token));

            _allocatedProductsResponse = Connector.ProductManagement.Products.Get();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_allocated_products_are_returned();
            only_channel_allocated_products_are_returned();
        }
        
        private void the_allocated_products_are_returned()
        {
            _allocatedProductsResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _allocatedProductsResponse.IsSuccess.ShouldBeTrue();
        }

        private void only_channel_allocated_products_are_returned()
        {
            var actualProducts = _allocatedProductsResponse.Response;
            //var expectedProducts = new List<string>()
            //{
            //    "PREMIER", "PERFORMER", "OAKHURST", "DOTSURE", "MUL","DOMESTIC_ARMOUR","AA","MULMOT"
            //};

            //actualProducts.TotalCount.ShouldEqual(expectedProducts.Count);
            actualProducts.TotalCount.ShouldEqual(26);

            //actualProducts.Results.IsEqual(expectedProducts, (dto, s) => dto.ProductCode.Equals(s))
            //    .ShouldBeTrue();
        }
    }
}