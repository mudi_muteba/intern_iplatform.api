using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Products;
using MasterData;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Products
{
    public class when_getting_an_allocated_product_by_code : BaseTest
    {
        private readonly GETResponseDto<AllocatedProductDto> _getAllocatedProductResponse;
        private const string ProductCode = "DOTSURE";

        public when_getting_an_allocated_product_by_code() : base(ApiUserObjectMother.AsAdmin())
        {
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.Channel2User());
            Connector = new Connector(new ApiToken(token));
            _getAllocatedProductResponse = Connector.ProductManagement.Products.Get(ProductCode);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_allocated_product_is_returned();
            the_allocted_products_info_is_returned();
        }

        private void the_allocated_product_is_returned()
        {
            _getAllocatedProductResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _getAllocatedProductResponse.IsSuccess.ShouldBeTrue();
        }

        private void the_allocted_products_info_is_returned()
        {
            var allocatedProduct = _getAllocatedProductResponse.Response;

            allocatedProduct.Id.ShouldEqual(33);
            allocatedProduct.Name.ShouldEqual("Dotsure");
            allocatedProduct.ProductCode.ShouldEqual(ProductCode);

            allocatedProduct.ProductOwner.Name.ShouldEqual("Oakhurst Insurance Company Limited");
            allocatedProduct.ProductProvider.Name.ShouldEqual("Oakhurst Insurance Company Limited");

            allocatedProduct.ProductType.Id.ShouldEqual(ProductTypes.PersonalPackage.Id);

            allocatedProduct.Covers.Count.ShouldEqual(1);
            allocatedProduct.Covers.First().Cover.Id.ShouldEqual(Covers.Motor.Id);
        }
    }
}