﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Products;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Products
{
    public class when_getting_a_product_fee_by_id : BaseTest
    {
        private readonly GETResponseDto<ProductFeeDto> _response;

        public when_getting_a_product_fee_by_id() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.ProductManagement.Products.GetFeeById(13, 1);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_benefits_are_retrieved();
        }
        
        private void the_benefits_are_retrieved()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}