﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Products;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Products
{
    public class when_getting_a_list_of_benefits_by_cover_id : BaseTest
    {
        private readonly LISTResponseDto<ProductBenefitDto> _response;

        public when_getting_a_list_of_benefits_by_cover_id() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.ProductManagement.Products.GetBenefitsByCover(5, 11);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_benefits_are_retrieved();
        }
        
        private void the_benefits_are_retrieved()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}