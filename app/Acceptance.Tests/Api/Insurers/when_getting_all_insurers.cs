﻿using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Insurers;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Insurers
{
    public class when_getting_all_insurers : BaseTest
    {
        private readonly LISTPagedResponseDto<ListInsurerDto> _response;

        public when_getting_all_insurers() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.InsurerManagement.Insurers.GetAllInsurers();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_request_completes();
        }

        private void the_request_completes()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            _response.Response.Results.Any().ShouldBeTrue();
        }
    }
}