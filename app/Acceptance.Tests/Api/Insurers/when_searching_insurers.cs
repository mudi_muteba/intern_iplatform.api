﻿using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Insurers;
using TestHelper;
using Xunit.Extensions;
using Xunit;

namespace Acceptance.Tests.Api.Insurers
{
    public class when_searching_insurers : BaseTest
    {
        private readonly LISTPagedResponseDto<ListInsurerDto> _response;

        public when_searching_insurers() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.InsurerManagement.Insurers.GetAllInsurers();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_search_completes();
        }

        private void the_search_completes()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            Assert.True(_response.Response.Results.Any());

            var insurer = _response.Response.Results.First();

            var result = Connector.InsurerManagement.Insurers.SearchInsurers(new InsurerSearchDto { Name = insurer.TradingName });

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
            Assert.True(result.Response.Results.First().TradingName.Contains(insurer.TradingName));
        }
    }
}