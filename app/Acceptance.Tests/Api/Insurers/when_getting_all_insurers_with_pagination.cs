using System.Net;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Insurers;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Insurers
{
    public class when_getting_all_insurers_with_pagination : BaseTest
    {
        private readonly LISTPagedResponseDto<ListInsurerDto> _response;

        public when_getting_all_insurers_with_pagination() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.InsurerManagement.Insurers.GetAllInsurers(new Pagination(2, 20));
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_request_completes();
            pagination_info_is_returned();
        }

        private void the_request_completes()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }

        private void pagination_info_is_returned()
        {
            var Result = _response.Response;

            Result.PageNumber.ShouldEqual(2);
            (Result.TotalCount > 0).ShouldBeTrue();
            (Result.TotalPages > 0).ShouldBeTrue();

            Result.Results.ShouldNotBeEmpty();
        }
    }
}