using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit.Extensions;
using Xunit;
using iPlatform.Api.DTOs.Proposals;

namespace Acceptance.Tests.Api.Quote
{
    public class when_retrieving_existing_quote_by_proposal_id : BaseTest
    {
        private POSTResponseDto<ProposalQuoteDto> _response;

        public when_retrieving_existing_quote_by_proposal_id() : base(ApiUserObjectMother.AsAdmin())
        {
            //new TestHelper.Helpers.Workflow().Start();
        }

        public override void Observe()
        {
            _response = Connector.ProposalManagement.Proposal(86).GetQuotes(new GetQuotesByProposalDto { ChannelId = 2, ReQuote = false });
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_quotes_are_retrieved();
        }

        private void the_quotes_are_retrieved()
        {
            _response.IsSuccess.ShouldBeTrue();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.ShouldNotBeNull();
            Assert.True(_response.Response.Policies.Count > 0);
        }
    }
}