﻿using System.Net;
using Acceptance.Tests.Api.Quote.bddfy;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Proposals;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Quote
{
    public class Quote_breakdown_tests : BaseTest
    {
        public Quote_breakdown_tests()
            : base(ApiUserObjectMother.AsAdmin(), ConnectionType.SqlServer)
        {
        }

        public override void Observe()
        {

        }


        [Observation]
        protected override void assert_all()
        {
            new WhenCheckingTheBuildingsBreakDownAgainstThePremiumsBddfy().Run(Connector);
        }
    }
}
