﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.Quotes;
using TestHelper;
using TestObjects.Mothers.Proposals;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Party.Quote
{
    public class when_getting_quote_from_proposal_header_id : BaseTest
    {
        private readonly POSTResponseDto<QuoteHeaderDto> _response;

        public when_getting_quote_from_proposal_header_id() : base(ApiUserObjectMother.AsAdmin())
        {
            var dto = CreateQuoteDtoObjectMother.ForNewQuote();
            //var json = JsonConvert.SerializeObject(dto);
            new TestHelper.Helpers.Workflow().Start();
            _response = Connector.Ratings.GetQuoteByProposalId(dto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            then_quote_for_proposal_should_be_created();
        }
        
        private void then_quote_for_proposal_should_be_created()
        {
            _response.IsSuccess.ShouldBeTrue();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.ShouldNotBeNull();
            _response.Response.Quotes.Count.ShouldEqual(1);
        }
    }
}
