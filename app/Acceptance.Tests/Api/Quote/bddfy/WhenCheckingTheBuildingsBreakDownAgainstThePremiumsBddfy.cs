﻿using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Party.ProposalDefinition;
using iPlatform.Api.DTOs.Party.ProposalDefinition.Questions;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using MasterData;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual.ProposalDefinition;
using TestObjects.Mothers.Ratings;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Quote.bddfy
{
    public class WhenCheckingTheBuildingsBreakDownAgainstThePremiumsBddfy : IBddfyTest
    {
        private int _indivdualId;
        private POSTResponseDto<int> _proposalDefinitionResponse;
        private int _proposalHeaderId;
        private int _addressId;
        private EditProposalDefinitionDto _editProposalDefinition;
        private ProposalDefinitionAnswerDto _riskAddress;
        private ProposalDefinitionAnswerDto _usage;
        private ProposalDefinitionAnswerDto _financedOrPaidoff;
        private ProposalDefinitionAnswerDto _homeType;
        private ProposalDefinitionAnswerDto _roofConstruction;
        private ProposalDefinitionAnswerDto _wallConstruction;
        private ProposalDefinitionAnswerDto _thatchLapa;
        private ProposalDefinitionAnswerDto _homeBoarder;
        private ProposalDefinitionAnswerDto _constructionYear;
        private ProposalDefinitionAnswerDto _sizeOfProperty;
        private ProposalDefinitionAnswerDto _numberOfBathroom;
        private ProposalDefinitionAnswerDto _propertyUnoccupied;
        private ProposalDefinitionAnswerDto _bussinessConducted;
        private ProposalDefinitionAnswerDto _mortgageBank;
        private PUTResponseDto<ProposalDefinitionDto> _updateResult;
        private ProposalDefinitionAnswerDto _suburb;
        private ProposalDefinitionAnswerDto _postalCode;
        private ProposalDefinitionAnswerDto _province;
        private ProposalDefinitionAnswerDto _accidentalDamage;
        private ProposalDefinitionAnswerDto _waterPumpingMachinery;
        private ProposalDefinitionAnswerDto _additionalExcess;
        private RatingRequestDto _request;
        private POSTResponseDto<RatingResultDto> _ratingResponse;
        public Connector Connector { get; set; }
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();

            _proposalHeaderId = new ConnectorTestData(Connector).ProposalHeaders.Create(_indivdualId);
            var createProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_BUILDINGS(_proposalHeaderId);

            _proposalDefinitionResponse =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .CreateProposalDefinition(createProposalDefinition);
        }

        public void When_action()
        {
           GETResponseDto<ProposalDefinitionDto> result =
                 Connector.IndividualManagement.Individual(_indivdualId)
                      .Proposal(_proposalHeaderId)
                     .GetProposalDefinition(_proposalDefinitionResponse.Response);

            _editProposalDefinition =
                NewProposalDefinitionDtoObjectMother.ValidEditProposalDefinitionDto(_proposalDefinitionResponse.Response,
                    _proposalHeaderId);

            _addressId = new ConnectorTestData(Connector).Individuals.CreateAddress(_indivdualId);

            _riskAddress = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.RiskAddress.Id).Select(a => a.AnswerDto).First();

            _suburb = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.Suburb.Id).Select(a => a.AnswerDto).First();

            _province = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.Province.Id).Select(a => a.AnswerDto).First();

            _postalCode = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.PostalCode.Id).Select(a => a.AnswerDto).First();

            _usage = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGUsage.Id).Select(a => a.AnswerDto).First();

            _financedOrPaidoff = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGOwnership.Id).Select(a => a.AnswerDto).First();

            _homeType = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGHomeType.Id).Select(a => a.AnswerDto).First();

            _roofConstruction = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGRoofConstruction.Id).Select(a => a.AnswerDto).First();

            _wallConstruction = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGWallConstruction.Id).Select(a => a.AnswerDto).First();

            _thatchLapa = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGThatchLapa.Id).Select(a => a.AnswerDto).First();

            _homeBoarder = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGPropertyBorder.Id).Select(a => a.AnswerDto).First();

            _constructionYear = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGConstructionYear.Id).Select(a => a.AnswerDto).First();

            _sizeOfProperty = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGSquareMetresOfProperty.Id).Select(a => a.AnswerDto).First();

            _numberOfBathroom = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGNumberOfBathrooms.Id).Select(a => a.AnswerDto).First();

            _propertyUnoccupied = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGPeriodPropertyIsUnoccupied.Id)
                .Select(a => a.AnswerDto)
                .First();

            _bussinessConducted = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGBusiness.Id).Select(a => a.AnswerDto).First();

            _mortgageBank = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
               .Where(a => a.Question.Id == Questions.AIGMortgageBank.Id).Select(a => a.AnswerDto).First();

            _accidentalDamage = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGAccidentalDamage.Id).Select(a => a.AnswerDto).First();

            _waterPumpingMachinery = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGWaterPumpingMachinery.Id).Select(a => a.AnswerDto).First();

            _additionalExcess = result.Response.QuestionGroups.SelectMany(a => a.QuestionDefinitions)
                .Where(a => a.Question.Id == Questions.AIGBuildingAdditionalExcess.Id).Select(a => a.AnswerDto).First();

            _riskAddress.Answer = _addressId.ToString();
            _suburb.Answer = "";
            _postalCode.Answer = ContentsAndBuildingsConstants.PostalCode;
            _province.Answer = ContentsAndBuildingsConstants.Province;
            _usage.Answer = ContentsAndBuildingsConstants.Usage;
            _financedOrPaidoff.Answer = ContentsAndBuildingsConstants.FinancedOrPaidoff;
            _homeType.Answer = ContentsAndBuildingsConstants.HomeType;
            _roofConstruction.Answer = ContentsAndBuildingsConstants.RoofConstruction;
            _wallConstruction.Answer = ContentsAndBuildingsConstants.WallConstruction;
            _thatchLapa.Answer = ContentsAndBuildingsConstants.ThatchLapa;
            _homeBoarder.Answer = ContentsAndBuildingsConstants.HomeBoarder;
            _constructionYear.Answer = ContentsAndBuildingsConstants.ConstructionYear;
            _sizeOfProperty.Answer = ContentsAndBuildingsConstants.SizeOfProperty;
            _numberOfBathroom.Answer = ContentsAndBuildingsConstants.NumberOfBathroom;
            _propertyUnoccupied.Answer = ContentsAndBuildingsConstants.PropertyUnoccupied;
            _bussinessConducted.Answer = ContentsAndBuildingsConstants.BussinessConducted;
            _mortgageBank.Answer = ContentsAndBuildingsConstants.MortgageBank;
            _accidentalDamage.Answer = ContentsAndBuildingsConstants.AccidentalDamage;
            _waterPumpingMachinery.Answer = ContentsAndBuildingsConstants.WaterPumpingMachinery;
            _additionalExcess.Answer = ContentsAndBuildingsConstants.AdditionalExcess;

            _editProposalDefinition.Answers.Add(_riskAddress);
            _editProposalDefinition.Answers.Add(_suburb);
            _editProposalDefinition.Answers.Add(_postalCode);
            _editProposalDefinition.Answers.Add(_province);
            _editProposalDefinition.Answers.Add(_usage);
            _editProposalDefinition.Answers.Add(_financedOrPaidoff);
            _editProposalDefinition.Answers.Add(_homeType);
            _editProposalDefinition.Answers.Add(_roofConstruction);
            _editProposalDefinition.Answers.Add(_wallConstruction);
            _editProposalDefinition.Answers.Add(_thatchLapa);
            _editProposalDefinition.Answers.Add(_homeBoarder);
            _editProposalDefinition.Answers.Add(_constructionYear);
            _editProposalDefinition.Answers.Add(_sizeOfProperty);
            _editProposalDefinition.Answers.Add(_numberOfBathroom);
            _editProposalDefinition.Answers.Add(_propertyUnoccupied);
            _editProposalDefinition.Answers.Add(_bussinessConducted);
            _editProposalDefinition.Answers.Add(_mortgageBank);
            _editProposalDefinition.Answers.Add(_accidentalDamage);
            _editProposalDefinition.Answers.Add(_waterPumpingMachinery);
            _editProposalDefinition.Answers.Add(_additionalExcess);
            

            _updateResult =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .UpdateProposalDefinition(_editProposalDefinition);

            _request = ContentsAndBuildingsRatingRequestObjectMother.RatingRequestTestData();

            //Connector.ProposalManagement.Proposal(1).GetQuotes(new iPlatform.Api.DTOs.Proposals.GetQuotesByProposalDto
            //{
            //    ChannelId = 2,
            //    Source = "widget",
            //    ReQuote = true,
            //    Id = _updateResult.Response.Id
            //});

            _ratingResponse = Connector.Ratings.GetRating(_request);
            
        }

        public void Then_result()
        {
            _proposalDefinitionResponse.ShouldNotBeNull();
            _proposalDefinitionResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _proposalDefinitionResponse.IsSuccess.ShouldBeTrue();

            _updateResult.ShouldNotBeNull();
            _updateResult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _updateResult.IsSuccess.ShouldBeTrue();

            GETResponseDto<ProposalDefinitionDto> result =
               Connector.IndividualManagement.Individual(_indivdualId)
                   .Proposal(_proposalHeaderId)
                   .GetProposalDefinition(_proposalDefinitionResponse.Response);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.Response.ShouldNotBeNull();
        }
    }
}
