﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Proposals;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Party.Quote
{
    public class when_creating_quote_upload_log : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_creating_quote_upload_log() : base(ApiUserObjectMother.AsAdmin())
        {
            var dto = CreateQuoteDtoObjectMother.CreateUploadLog();
            _response = Connector.QuotesManagement.Quote.LogQuoteUploadSuccess(dto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            then_quote__for_proposal_should_be_created();
        }
        
        private void then_quote__for_proposal_should_be_created()
        {
            _response.IsSuccess.ShouldBeTrue();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.ShouldNotBeNull();
            Assert.True(_response.Response > 0); 
        }
    }
}
