﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.Models;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Home
{
    public class when_retrieving_sitemap : BaseTest
    {
        private List<SiteMapBaseNode> _nodes;
        public when_retrieving_sitemap() : base(ApiUserObjectMother.AsAdmin(),ConnectionType.SqlServer)
        {
            _nodes = new global::Api.Models.SiteMap().GetAllSiteNodes();
        }

        
        public override void Observe()
        {
            
        }
        //If this test breaks check that all routes have a 
        //description and that it is not the default description{get;set}
        //see other routes for examples
        [Observation]//(Skip = "Speed up the build for testing")
        protected override void assert_all()
        {
            _nodes.ShouldNotBeNull();
            _nodes[0].Description.ShouldNotBeSameAs("Error");

        }
    }
}
