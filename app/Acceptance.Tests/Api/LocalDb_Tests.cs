﻿using Acceptance.Tests.Api.AdditionalMembers;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api
{
    public class LocalDb_Tests : BaseTest
    {
        public LocalDb_Tests()
            : base(ApiUserObjectMother.AsAdmin())
        {
            CreateBlankDb = true;
        }

        public override void Observe()
        {
        }

        [Observation]
        protected override void assert_all()
        {
            new AdditionalMemberTests(Connector).run_all();

        }
    }
}
