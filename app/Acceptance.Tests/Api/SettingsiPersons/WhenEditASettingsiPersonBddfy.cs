using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.Admin;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;
using TestObjects.Mothers.SettingsiPerson;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;

namespace Acceptance.Tests.Api.Admin
{
    public class WhenEditASettingsiPersonBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _saveResponse;
        private PUTResponseDto<int> _editResponse;
        private EditSettingsiPersonDto _editDto;

        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createDto = SettingsiPersonObjectMother.ValidSettingsiPersonDto();
            _saveResponse = Connector.SettingsiPersonManagement.SettingsiPersons.Create(createDto);

        }

        public void When_action()
        {
            _saveResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _saveResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_saveResponse.Response > 0);

            _editDto = SettingsiPersonObjectMother.ValidEditSettingsiPersonDto(_saveResponse.Response);
            _editResponse = Connector.SettingsiPersonManagement.SettingsiPerson(_saveResponse.Response).Edit(_editDto);

        }

        public void Then_result()
        {
            _editResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _editResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_editResponse.Response > 0);

        }
    }
}