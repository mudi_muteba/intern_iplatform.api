using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.Admin
{
    public class SettingsiPersonTests
    {
        private readonly Connector _connector;

        public SettingsiPersonTests(Connector connector)
        {
            _connector = connector;
            var token = new ConnectorTestData(_connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _connector.SetToken(token);
        }

        public void run_all()
        {
            new WhenCreatingASettingsiPersonBddfy().Run(_connector);
            new WhenEditASettingsiPersonBddfy().Run(_connector);
        }
    }
}