﻿using System;
using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Claims.ClaimTypes;
using MasterData;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Claims.ClaimsType
{
    public class when_requesting_the_available_claim_types: BaseTest
    {
        private readonly GETResponseDto<List<ClaimsTypeDto>> _response;

        public when_requesting_the_available_claim_types() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.ClaimsTypeManagement.ClaimsTypes.GetClaimTypesByOrganisationId();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_list_of_claim_types_are_returned();
        }

        private void the_list_of_claim_types_are_returned()
        {
            var expectedClaimTypes = new ClaimsTypes();

            _response.Response.ShouldNotBeEmpty();
            _response.Response.Count.ShouldEqual(expectedClaimTypes.Count);

            Func<MasterData.ClaimsType, ClaimsTypeDto, bool> isEqual =
                (ect, act) => ect.Id == act.Id && ect.Name.Equals(act.Name, StringComparison.InvariantCultureIgnoreCase);

            foreach (var expectedClaimType in expectedClaimTypes)
                _response.Response.FirstOrDefault(act => isEqual(expectedClaimType, act)).ShouldNotBeNull();
        }
    }
}
