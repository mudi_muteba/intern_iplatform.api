﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Claims.Questions;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Claims.ClaimsType
{
    public class when_requesting_claims_questions_for_a_non_existing_type: BaseTest
    {
        private readonly GETResponseDto<ClaimsTypeDetailDto> _response;

        public when_requesting_claims_questions_for_a_non_existing_type() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.ClaimsTypeManagement.ClaimsType(0).GetClaimTypeDetailsByProductId();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            a_null_is_returned();
        }

        private void a_null_is_returned()
        {
            _response.Response.ShouldBeNull();
        }
    }
}
