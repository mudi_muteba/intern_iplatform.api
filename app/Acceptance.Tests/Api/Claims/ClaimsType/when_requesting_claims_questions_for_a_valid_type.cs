﻿using System.Collections.Generic;
using System.Linq;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Claims.Questions;
using MasterData;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Claims.ClaimsType
{
    public class when_requesting_claims_questions_for_a_valid_type: BaseTest
    {
        private readonly GETResponseDto<ClaimsTypeDetailDto> _response;

        public when_requesting_claims_questions_for_a_valid_type() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.ClaimsTypeManagement.ClaimsType(ClaimsTypes.MotorAccident.Id).GetClaimTypeDetailsByProductId();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_claim_type_is_available();
            the_questions_for_the_type_is_returned();
            the_questions_for_the_claim_type_is_returned();
        }

        private void the_questions_for_the_claim_type_is_returned()
        {
            _response.Response.ShouldNotBeNull();
            _response.Response.Type.ShouldNotBeNull();
        }

        private void the_claim_type_is_available()
        {
            _response.Response.Type.Id.ShouldEqual(ClaimsTypes.MotorAccident.Id);
            _response.Response.Type.Name.ShouldEqual(ClaimsTypes.MotorAccident.Name);
        }

        private void the_questions_for_the_type_is_returned()
        {
            _response.Response.Structure.Groups.ShouldNotBeEmpty();
            foreach (var actualGroup in _response.Response.Structure.Groups)
            {
                QuestionsForMotorAccidentClaims.FirstOrDefault(q => q.ClaimsQuestionGroup.Id == actualGroup.Id).ShouldNotBeNull();
                foreach (var actualQuestion in actualGroup.Questions)
                    QuestionsForMotorAccidentClaims.FirstOrDefault(eq => eq.Id == actualQuestion.Id).ShouldNotBeNull();
            }
        }

        private List<ClaimsQuestion> QuestionsForMotorAccidentClaims
        {
            get
            {
                return
                    new ClaimsQuestions().Join(new ClaimsQuestionDefinitions().Where(a => a.ClaimsTypeId == ClaimsTypes.MotorAccident.Id), question => question.Id,
                        definition => definition.ClaimsQuestion.Id, (question, definition) => question).ToList();
            }
        }
    }
}
