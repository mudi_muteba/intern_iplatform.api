using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Claims;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Policy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Claims.ClaimsItem
{
    public class when_creating_new_claimsitems : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_creating_new_claimsitems() : base(ApiUserObjectMother.AsAdmin())
        {
            var createPolicyHeaderDto = NewPolicyDtoObjectMother.ValidPolicyDto();
            _response = Connector.PolicyHeaderManagement.PolicyHeaders.CreatePolicyHeader(createPolicyHeaderDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_claim_item_is_created();
        }

        private void the_claim_item_is_created()
        {
            var createClaimsHeaderDto = NewClaimsHeaderDtoObjectMother.ValidCreateClaimsHeaderDto();

            //Set PolicyHeaderId
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            createClaimsHeaderDto.PolicyHeaderId = _response.Response;


            //Create Party
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);

            //Set PartyId
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            createClaimsHeaderDto.PartyId = _response.Response;

            //Create ClaimHeader
            _response = Connector.ClaimsHeaderManagement.ClaimsHeaders.CreateClaimHeader(createClaimsHeaderDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();


            var policyheader = Connector.PolicyHeaderManagement.PolicyHeader(createClaimsHeaderDto.PolicyHeaderId).Get();
            policyheader.StatusCode.ShouldEqual(HttpStatusCode.OK);
            policyheader.IsSuccess.ShouldBeTrue();

            var createClaimsItemsDto = NewClaimsItemDtoObjectMother.ValidCreateClaimsItemsDto(_response.Response, createClaimsHeaderDto.PartyId, policyheader.Response.PolicyItems, 1);

            var result = Connector.ClaimsItemManagement.SetClaimHeaderId(_response.Response).ClaimsItems.CreateClaimsItem(createClaimsItemsDto);
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }
    }
}