using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using TestHelper;
using TestObjects.Mothers.Claims;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Policy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Claims.ClaimsItem
{
    public class when_disabling_a_claims_item : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_disabling_a_claims_item() : base(ApiUserObjectMother.AsAdmin())
        {
            var createPolicyHeaderDto = NewPolicyDtoObjectMother.ValidPolicyDto();
            _response = Connector.PolicyHeaderManagement.PolicyHeaders.CreatePolicyHeader(createPolicyHeaderDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_claim_item_is_disabled();
        }

        private void the_claim_item_is_disabled()
        {
            var createClaimsHeaderDto = NewClaimsHeaderDtoObjectMother.ValidCreateClaimsHeaderDto();

            //Set PolicyHeaderId
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            createClaimsHeaderDto.PolicyHeaderId = _response.Response;

            //Create Party
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);

            //Set PartyId
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            createClaimsHeaderDto.PartyId = _response.Response;

            //Create ClaimHeader
            _response = Connector.ClaimsHeaderManagement.ClaimsHeaders.CreateClaimHeader(createClaimsHeaderDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            var claimHeaderId = _response.Response;

            //Retrieve Policy Items
            var policyheader = Connector.PolicyHeaderManagement.PolicyHeader(createClaimsHeaderDto.PolicyHeaderId).Get();
            policyheader.StatusCode.ShouldEqual(HttpStatusCode.OK);
            policyheader.IsSuccess.ShouldBeTrue();

            //Create Claim Items
            var createClaimsItemsDto = NewClaimsItemDtoObjectMother.ValidCreateClaimsItemsDto(_response.Response, createClaimsHeaderDto.PartyId, policyheader.Response.PolicyItems, 1);
            _response = Connector.ClaimsItemManagement.SetClaimHeaderId(_response.Response).ClaimsItems.CreateClaimsItem(createClaimsItemsDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            
            //Retrieve Claim Items
            var claimItems = Connector.ClaimsItemManagement.SetClaimHeaderId(claimHeaderId).ClaimsItems.GetClaimsItems();
            claimItems.StatusCode.ShouldEqual(HttpStatusCode.OK);
            claimItems.IsSuccess.ShouldBeTrue();

            //disable a claim item
            var disableClaimItemDto = new DisableClaimItemDto() { ClaimsHeaderId = claimHeaderId, Id = claimItems.Response.Results[0].Id };
            var result = Connector.ClaimsItemManagement.SetClaimHeaderId(claimHeaderId).ClaimsItem(claimItems.Response.Results[0].Id).DeleteClaimsItem(disableClaimItemDto);
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }
    }
}