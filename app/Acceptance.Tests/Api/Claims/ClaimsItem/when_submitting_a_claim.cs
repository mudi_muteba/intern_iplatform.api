using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using TestHelper;
using TestObjects.Mothers.Claims;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Policy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Claims.ClaimsItem
{
    public class when_submitting_a_claim : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_submitting_a_claim() : base(ApiUserObjectMother.AsAdmin())
        {
            var createPolicyHeaderDto = NewPolicyDtoObjectMother.ValidPolicyDto();
            _response = Connector.PolicyHeaderManagement.PolicyHeaders.CreatePolicyHeader(createPolicyHeaderDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_claim_item_is_disabled();
        }

        private void the_claim_item_is_disabled()
        {
            var createClaimsHeaderDto = NewClaimsHeaderDtoObjectMother.ValidCreateClaimsHeaderDto();

            //Set PolicyHeaderId
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            createClaimsHeaderDto.PolicyHeaderId = _response.Response;

            //Create Party
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);

            //Set PartyId
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            createClaimsHeaderDto.PartyId = _response.Response;

            //Create ClaimHeader
            _response = Connector.ClaimsHeaderManagement.ClaimsHeaders.CreateClaimHeader(createClaimsHeaderDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            var claimHeaderId = _response.Response;

            //Retrieve Policy Items
            var policyheader = Connector.PolicyHeaderManagement.PolicyHeader(createClaimsHeaderDto.PolicyHeaderId).Get();
            policyheader.StatusCode.ShouldEqual(HttpStatusCode.OK);
            policyheader.IsSuccess.ShouldBeTrue();

            //Create Claim Items
            var createClaimsItemsDto = NewClaimsItemDtoObjectMother.ValidCreateClaimsItemsDto(_response.Response, createClaimsHeaderDto.PartyId, policyheader.Response.PolicyItems, 1);
            _response = Connector.ClaimsItemManagement.SetClaimHeaderId(_response.Response).ClaimsItems.CreateClaimsItem(createClaimsItemsDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            
            //Retrieve Claim Items
            var claimItems = Connector.ClaimsItemManagement.SetClaimHeaderId(claimHeaderId).ClaimsItems.GetClaimsItems();
            claimItems.StatusCode.ShouldEqual(HttpStatusCode.OK);
            claimItems.IsSuccess.ShouldBeTrue();
            var claimItemId = claimItems.Response.Results.FirstOrDefault().Id;

            //save answers
            var answers = NewClaimsItemDtoObjectMother.ValidSaveAnswers(claimHeaderId, claimItemId, claimItems.Response.Results.FirstOrDefault());
            var result = Connector.ClaimsItemManagement.SetClaimHeaderId(claimHeaderId).ClaimsItem(claimItemId).SaveClaimItemAnswers(answers);
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();


            var accept = Connector.ClaimsHeaderManagement.ClaimsHeader(_response.Response).SubmitClaimsHeader(new AcceptClaimDto { Id = _response.Response });
            accept.StatusCode.ShouldEqual(HttpStatusCode.OK);
            accept.IsSuccess.ShouldBeTrue();
        }
    }
}