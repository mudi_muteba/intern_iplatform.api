using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Claims;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Policy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Claims.ClaimsHeader
{
    public class when_creating_a_new_claimheader : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_creating_a_new_claimheader() : base(ApiUserObjectMother.AsAdmin())
        {
            var createPolicyHeaderDto = NewPolicyDtoObjectMother.ValidPolicyDto();
            _response = Connector.PolicyHeaderManagement.PolicyHeaders.CreatePolicyHeader(createPolicyHeaderDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_claim_header_is_created();
        }

        private void the_claim_header_is_created()
        {
            var createClaimsHeaderDto = NewClaimsHeaderDtoObjectMother.ValidCreateClaimsHeaderDto();

            //Set PolicyHeaderId
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            createClaimsHeaderDto.PolicyHeaderId = _response.Response;

            //Create Party
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);

            //Set PartyId
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            createClaimsHeaderDto.PartyId = _response.Response;
                 
            //Create ClaimHeader
            _response = Connector.ClaimsHeaderManagement.ClaimsHeaders.CreateClaimHeader(createClaimsHeaderDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}