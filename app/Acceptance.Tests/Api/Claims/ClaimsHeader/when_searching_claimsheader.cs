using System.Net;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using TestHelper;
using TestObjects.Mothers.Claims;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Policy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Claims.ClaimsHeader
{
    public class when_searching_claimsheader : BaseTest
    {
        private readonly POSTResponseDto<int> _response;
        private readonly POSTResponseDto<PagedResultDto<ClaimsHeaderDto>> _claimsResponse;

        public when_searching_claimsheader() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.PolicyHeaderManagement.PolicyHeaders.CreatePolicyHeader(NewPolicyDtoObjectMother.ValidPolicyDto());
            var createClaimsHeaderDto = NewClaimsHeaderDtoObjectMother.ValidCreateClaimsHeaderDto();

            //Set PolicyHeaderId
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            createClaimsHeaderDto.PolicyHeaderId = _response.Response;

            //Create Party
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);

            //Set PartyId
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            createClaimsHeaderDto.PartyId = _response.Response;

            //Create ClaimHeader
            _response = Connector.ClaimsHeaderManagement.ClaimsHeaders.CreateClaimHeader(createClaimsHeaderDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            _claimsResponse = Connector.ClaimsHeaderManagement.ClaimsHeaders.SearchClaims(NewClaimsHeaderDtoObjectMother.ValidClaimSearchDto());
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_claim_header_is_searched();
        }

        private void the_claim_header_is_searched()
        {
            _claimsResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _claimsResponse.IsSuccess.ShouldBeTrue();
        }
    }
} 