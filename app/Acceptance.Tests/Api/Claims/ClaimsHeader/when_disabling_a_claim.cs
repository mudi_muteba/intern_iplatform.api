using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using TestHelper;
using TestObjects.Mothers.Claims;
using Xunit.Extensions;
using TestObjects.Mothers.Policy;
using TestObjects.Mothers.Individual;

namespace Acceptance.Tests.Api.Claims.ClaimsHeader
{
    public class when_disabling_a_claim : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_disabling_a_claim() : base(ApiUserObjectMother.AsAdmin())
        {
            var createPolicyHeaderDto = NewPolicyDtoObjectMother.ValidPolicyDto();
            _response = Connector.PolicyHeaderManagement.PolicyHeaders.CreatePolicyHeader(createPolicyHeaderDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_claim_header_is_disabled();
        }

        private void the_claim_header_is_disabled()
        {
            var createClaimsHeaderDto = NewClaimsHeaderDtoObjectMother.ValidCreateClaimsHeaderDto();

            //Set PolicyHeaderId
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            createClaimsHeaderDto.PolicyHeaderId = _response.Response;

            //Create Party
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);

            //Set PartyId & Channel
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            createClaimsHeaderDto.PartyId = _response.Response;

            //Create ClaimsHeader
            _response = Connector.ClaimsHeaderManagement.ClaimsHeaders.CreateClaimHeader(createClaimsHeaderDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            //disable Claim
            var result = Connector.ClaimsHeaderManagement.ClaimsHeader(_response.Response).DisableClaimsHeader(new DisableClaimDto { Id = _response.Response });
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }
    }
}