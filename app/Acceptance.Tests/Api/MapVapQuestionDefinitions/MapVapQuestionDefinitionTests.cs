using iPlatform.Api.DTOs.Base.Connector;

namespace Acceptance.Tests.Api.MapVapQuestionDefinitions
{ 
    public class MapVapQuestionDefinitionTests
    {
        private readonly Connector _connector;

        public MapVapQuestionDefinitionTests(Connector connector)
        {
            _connector = connector;
        }

        public void run_all()
        {
            new WhenCreatingMapVapQuestionDefinitionBddfy().Run(_connector);
            //new WhenListMapVapQuestionDefinitionBddfy().Run(_connector);
            new WhenEditingMapVapQuestionDefinitionBddfy().Run(_connector);
        }
    }
}