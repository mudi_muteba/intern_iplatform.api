using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using TestObjects.Mothers.Individual;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.MapVapQuestionDefinitions
{
    public class WhenListMapVapQuestionDefinitionBddfy : IBddfyTest
    {
        private CreateMapVapQuestionDefinitionDto _saveDto;
        private LISTResponseDto<ListMapVapQuestionDefinitionDto> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _saveDto = MapVapQuestionDefinitionDtoObjectMother.ValidCreateDto();
            this.Connector.QuestionManagement.MapVapQuestionDefinitions.Create(_saveDto);
            this.Connector.QuestionManagement.MapVapQuestionDefinitions.Create(_saveDto);
            this.Connector.QuestionManagement.MapVapQuestionDefinitions.Create(_saveDto);
        }

        public void When_action()
        {
            _response = this.Connector.QuestionManagement.MapVapQuestionDefinitions.GetList(_saveDto.ProductId,_saveDto.ChannelId);
        }

        public void Then_result()
        {
            _response.IsSuccess.ShouldBeTrue();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.ShouldNotBeNull();
            _response.Response.Results.Count.ShouldEqual(4);
        }
    }
}