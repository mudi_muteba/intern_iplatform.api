﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.SettingsiRate;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using TestObjects.Mothers.Individual;

namespace Acceptance.Tests.Api.MapVapQuestionDefinitions
{
    public class WhenEditingMapVapQuestionDefinitionBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private EditMapVapQuestionDefinitionDto _EditDto;
        private POSTResponseDto<int> _SaveResponse;
        private PUTResponseDto<int> _EditResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            CreateMapVapQuestionDefinitionDto createDto = MapVapQuestionDefinitionDtoObjectMother.ValidCreateDto();
            _SaveResponse = Connector.QuestionManagement.MapVapQuestionDefinitions.Create(createDto);
        }

        public void When_action()
        {
            _SaveResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _SaveResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_SaveResponse.Response > 0);

            _EditDto = MapVapQuestionDefinitionDtoObjectMother.ValidEditDto(_SaveResponse.Response);
            _EditResponse = Connector.QuestionManagement.MapVapQuestionDefinition(_SaveResponse.Response).Edit(_EditDto);
        }

        public void Then_result()
        {
            _EditResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _EditResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_EditResponse.Response > 0);

            var getReponse = Connector.QuestionManagement.MapVapQuestionDefinition(_SaveResponse.Response).Get();
            getReponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            getReponse.IsSuccess.ShouldBeTrue();
            getReponse.Response.Enabled.ShouldEqual(_EditDto.Enabled);
            getReponse.Response.Premium.ShouldEqual(_EditDto.Premium);
        }
    }
}