﻿using TestStack.BDDfy;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;
using Xunit;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using TestObjects.Mothers.ChannelEvents;
using iPlatform.Api.DTOs.Admin.ChannelEvents;

namespace Acceptance.Tests.Api.ChannelEvents
{
    public class WhenUpdatingAChannelEventBddfy : IBddfyTest
    {
        private CreateChannelEventDto _createDto;
        private POSTResponseDto<int> _response;

        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createDto = ChannelEventObjectMother.ValidDto();
        }

        public void When_action()
        {
            _response = Connector.ChannelEventManagement.ChannelEvents.Create(_createDto);
        }

        public void Then_result()
        {
            the_record_has_been_created();
            the_record_is_updated();
            the_record_should_be_retrieve_by_id_and_validated();
        }
        private void the_record_has_been_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            Assert.True(_response.Response > 0);
        }

        private void the_record_is_updated()
        {
            var response = Connector.ChannelEventManagement.ChannelEvent(_response.Response).Edit(GetEditDto());
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }
        private void the_record_should_be_retrieve_by_id_and_validated()
        {
            var response = Connector.ChannelEventManagement.ChannelEvent(_response.Response).Get();
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.Response.EventName.ShouldEqual(GetEditDto().EventName);
        }

        private EditChannelEventDto GetEditDto()
        {
            return new EditChannelEventDto
            {
                Id = _response.Response,
                ChannelId = _createDto.ChannelId,
                EventName = "This is Updated EventName",
                ProductCode = _createDto.ProductCode,
                Tasks = _createDto.Tasks
            };
        }
    }
}
