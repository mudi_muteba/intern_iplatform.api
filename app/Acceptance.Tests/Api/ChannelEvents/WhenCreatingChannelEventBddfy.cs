﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;
using Xunit;
using System.Net;
using TestObjects.Mothers.ChannelEvents;
using iPlatform.Api.DTOs.Base.Connector;
using TestStack.BDDfy;

namespace Acceptance.Tests.Api.ChannelEvents
{
    public class WhenCreatingChannelEventBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createDto = ChannelEventObjectMother.ValidDto();
            _response = Connector.ChannelEventManagement.ChannelEvents.Create(createDto);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            the_record_has_been_created();
            the_record_should_be_retrieve_by_id();
            the_record_should_be_deleted();
        }

        private void the_record_has_been_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            Assert.True(_response.Response > 0);
        }

        private void the_record_should_be_retrieve_by_id()
        {
            var response = Connector.ChannelEventManagement.ChannelEvent(_response.Response).Get();
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.Response.Channel.Id.ShouldEqual(1);
            response.Response.EventName.ShouldEqual(ChannelEventObjectMother.ValidDto().EventName);
        }

        private void the_record_should_be_deleted()
        {
            var response = Connector.ChannelEventManagement.ChannelEvent(_response.Response).Delete();
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }
    }
}
