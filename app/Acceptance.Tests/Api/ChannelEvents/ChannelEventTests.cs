using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.ChannelEvents
{
    public class ChannelEventTests
    {
        private readonly Connector _connector;

        public ChannelEventTests(Connector connector)
        {
            _connector = connector;
            var token = new ConnectorTestData(_connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _connector.SetToken(token);
        }

        public void run_all()
        {
            new WhenCreatingChannelEventBddfy().Run(_connector);
            new WhenUpdatingAChannelEventBddfy().Run(_connector);
        }
    }
}