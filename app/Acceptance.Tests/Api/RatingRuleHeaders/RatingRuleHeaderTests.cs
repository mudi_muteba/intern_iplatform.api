﻿using iPlatform.Api.DTOs.Base.Connector;

namespace Acceptance.Tests.Api.RatingRuleHeaders
{
    public class RatingRuleHeaderTests
    {
        private readonly Connector _connector;
        public RatingRuleHeaderTests(Connector connector)
        {
            _connector = connector;
        }

        public void run_all()
        {
           new WhenCreatingRatingRuleHeaderBddfy().Run(_connector);
           new WhenEditingRatingRuleHeaderBddfy().Run(_connector);
           new WhenGettingARatingRuleHeaderBddfy().Run(_connector);
           new WhenGettingAllRatingRuleHeaderBddfy().Run(_connector);
           new WhenDeletingRatingRuleHeaderBddfy().Run(_connector);
        }
    }
}
