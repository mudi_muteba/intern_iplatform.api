﻿using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using TestObjects.Mothers.RatingRuleHeaders;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.RatingRuleHeaders
{
   public class WhenEditingRatingRuleHeaderBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private POSTResponseDto<int> _PostResponse;
        private PUTResponseDto<int> _PutResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createRatingRuleHeaderDto = RatingRuleHeaderDtoMother.ValidCreateDto();
            _PostResponse = Connector.RatingRuleHeaderManagement.RatingRuleHeaders.Create(createRatingRuleHeaderDto);
        }

        public void When_action()
        {
            _PostResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _PostResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_PostResponse.Response > 0);

            var editRatingRuleHeaderDto  = RatingRuleHeaderDtoMother.ValidEditDto(_PostResponse.Response);
            _PutResponse = Connector.RatingRuleHeaderManagement.RatingRuleHeader(editRatingRuleHeaderDto.Id).Edit(editRatingRuleHeaderDto);
        }

        public void Then_result()
        {
            _PutResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _PutResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_PutResponse.Response > 0);
        }
    }
}
