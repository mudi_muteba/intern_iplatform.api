﻿using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using TestObjects.Mothers.RatingRuleHeaders;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.RatingRuleHeaders
{
    public class WhenCreatingRatingRuleHeaderBddfy : IBddfyTest
    {
        private CreateRatingRuleHeaderDto _saveDto;
        private POSTResponseDto<int> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _saveDto = RatingRuleHeaderDtoMother.ValidCreateDto();
        }

        public void When_action()
        {
            _response = Connector.RatingRuleHeaderManagement.RatingRuleHeaders.Create(_saveDto);
        }

        public void Then_result()
        {
            _response.IsSuccess.ShouldBeTrue();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.ShouldNotBeNull();
            //_response.Response.ShouldBeInRange(1,100);
        }
    }
}
