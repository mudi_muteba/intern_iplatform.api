﻿using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using TestObjects.Mothers.RatingRuleHeaders;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.RatingRuleHeaders
{
    public class WhenGettingARatingRuleHeaderBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private POSTResponseDto<int> _PostResponse;
        private GETResponseDto<RatingRuleHeaderDto> _GetResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            CreateRatingRuleHeaderDto createRatingRuleHeaderDto = RatingRuleHeaderDtoMother.ValidCreateDto();
            _PostResponse = Connector.RatingRuleHeaderManagement.RatingRuleHeaders.Create(createRatingRuleHeaderDto);
        }

        public void When_action()
        {
            _PostResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _PostResponse.IsSuccess.ShouldBeTrue();
            Assert.True(_PostResponse.Response > 0);

            _GetResponse = Connector.RatingRuleHeaderManagement.RatingRuleHeader(_PostResponse.Response).Get();
        }

        public void Then_result()
        {
            _GetResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _GetResponse.IsSuccess.ShouldBeTrue();
            _GetResponse.Response.ShouldNotBeNull();
        }
    }
}
