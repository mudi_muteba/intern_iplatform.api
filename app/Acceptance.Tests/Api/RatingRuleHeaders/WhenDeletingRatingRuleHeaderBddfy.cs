﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using TestObjects.Mothers.RatingRuleHeaders;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.RatingRuleHeaders
{
   public  class WhenDeletingRatingRuleHeaderBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private POSTResponseDto<int> _PostResponse;
        private PUTResponseDto<int> _PutResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            CreateRatingRuleHeaderDto createRatingRuleHeaderDto = RatingRuleHeaderDtoMother.ValidCreateDto();
            _PostResponse = Connector.RatingRuleHeaderManagement.RatingRuleHeaders.Create(createRatingRuleHeaderDto);
        }

        public void When_action()
        {
            _PostResponse.IsSuccess.ShouldBeTrue();
            _PostResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            Assert.True(_PostResponse.Response > 0);

            _PutResponse = Connector.RatingRuleHeaderManagement.RatingRuleHeader(_PostResponse.Response).Delete();
        }

        public void Then_result()
        {
            _PutResponse.IsSuccess.ShouldBeTrue();
            _PutResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            Assert.True(_PutResponse.Response > 0);
        }
    }
}
