﻿using System.Collections.Generic;
using System.Net;
using Acceptance.Tests.Api.Individual.Payments;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.JsonDataStores;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.JsonDataStores;
using TestObjects.Mothers.Users;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.JsonDataStores
{
    public class when_retrieving_jsondatastore_by_searchDto : BaseTest
    {
        private List<CreateJsonDataStoreDto> _createJsonDataStoreDtoList;
        private List<POSTResponseDto<int>> _postResponseList;

        private JsonDataStoreSearchDto _searchDto;
        private JsonDataStoreSearchDto _searchDtoWithLatest;
        private int _indivdualId;

        public when_retrieving_jsondatastore_by_searchDto() : base(ApiUserObjectMother.AsAdmin(),ConnectionType.SqlServer)
        {
        }

        public override void Observe()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();

            _createJsonDataStoreDtoList = JsonDataStoreDtoMother.ListJsonCreateDataStoreDtos;
            _searchDto = JsonDataStoreDtoMother.JsonDataStoreSearchDto;
            _searchDtoWithLatest = JsonDataStoreDtoMother.JsonDataStoreSearchDtoWithLatest;
            _postResponseList = new List<POSTResponseDto<int>>();

            foreach (var dto in _createJsonDataStoreDtoList)
            {
                dto.PartyId = _indivdualId;
                _postResponseList.Add(Connector.JsonDataStoresManagement.JsonDataStores.CreateJsonDataStore(dto));
            }
        }

        [Observation(Skip = "Speed up the build for testing")]
        protected override void assert_all()
        {
            when_storedata_created_successfully();
            when_searching();
            when_searching_with_latest();
        }

        private void when_storedata_created_successfully()
        {
            foreach (var response in _postResponseList)
            {
                response.StatusCode.ShouldEqual(HttpStatusCode.OK);
                response.IsSuccess.ShouldBeTrue();
            }
        }

        private void when_searching()
        {
            var searchResult =
                Connector.JsonDataStoresManagement.JsonDataStores.SearchJsonDataStores(_searchDto);

            searchResult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            searchResult.IsSuccess.ShouldBeTrue();

            searchResult.ShouldNotBeNull();
            searchResult.Response.Results.Count.ShouldEqual(_createJsonDataStoreDtoList.Count);
        }

        private void when_searching_with_latest()
        {
            var searchWithLatest =
                Connector.JsonDataStoresManagement.JsonDataStores.SearchJsonDataStores(_searchDtoWithLatest);
            
            searchWithLatest.StatusCode.ShouldEqual(HttpStatusCode.OK);
            searchWithLatest.IsSuccess.ShouldBeTrue();
            searchWithLatest.Response.Results.ShouldNotBeNull();
        }
    }
}
