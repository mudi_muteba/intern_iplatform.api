﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.JsonDataStores;
using TestHelper;
using TestObjects.Mothers.JsonDataStores;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.JsonDataStores
{
    public class when_creating_a_new_jsondatastore : BaseTest
    {
        private CreateJsonDataStoreDto _createJsonDataStoreDto;
        private POSTResponseDto<int> _postResponse;

        public when_creating_a_new_jsondatastore() : base(ApiUserObjectMother.AsAdmin(),ConnectionType.SqlServer)
        {
        }

        public override void Observe()
        {
            _createJsonDataStoreDto = JsonDataStoreDtoMother.CreateJsonDataStoreDto;
            _postResponse = Connector.JsonDataStoresManagement.JsonDataStores.CreateJsonDataStore(_createJsonDataStoreDto);
        }

        [Observation] //(Skip = "Speed up the build for testing")
        protected override void assert_all()
        {
            the_jsondatastore_is_created();
            the_jsondatastore_enrty_can_be_retrieved_by_id();
        }

        private void the_jsondatastore_is_created()
        {
            _postResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postResponse.IsSuccess.ShouldBeTrue();
        }

        private void the_jsondatastore_enrty_can_be_retrieved_by_id()
        {
            var getResponse = Connector.JsonDataStoresManagement.JsonDataStore(_postResponse.Response).Get();

            getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            getResponse.IsSuccess.ShouldBeTrue();

            var jsonDataStoreEntry = getResponse.Response;
            jsonDataStoreEntry.ShouldNotBeNull();
            jsonDataStoreEntry.SavePoint.ShouldEqual(_createJsonDataStoreDto.SavePoint);
            jsonDataStoreEntry.PartyId.ShouldEqual(_createJsonDataStoreDto.PartyId);
            jsonDataStoreEntry.Section.ShouldEqual(_createJsonDataStoreDto.Section);
            jsonDataStoreEntry.SessionId.ShouldEqual(_createJsonDataStoreDto.SessionId);
            jsonDataStoreEntry.Store.ShouldEqual(_createJsonDataStoreDto.Store);
        }
    }
}
