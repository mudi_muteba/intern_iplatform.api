﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.JsonDataStores;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.JsonDataStores;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.JsonDataStores
{
    public class when_retrieving_jsondatastore_by_partyid : BaseTest
    {
        private List<CreateJsonDataStoreDto> _createJsonDataStoreDtoList;

        private List<POSTResponseDto<int>> _postResponseList;
        private int _indivdualId;

        public when_retrieving_jsondatastore_by_partyid() : base(ApiUserObjectMother.AsAdmin(),ConnectionType.SqlServer)
        {
        }

        public override void Observe()
        {
            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();

            _createJsonDataStoreDtoList = JsonDataStoreDtoMother.ListJsonCreateDataStoreDtos;
            _createJsonDataStoreDtoList.ForEach(a=>a.PartyId = _indivdualId);

            _postResponseList = new List<POSTResponseDto<int>>();
            foreach (var dto in _createJsonDataStoreDtoList)
            {
                _postResponseList.Add(Connector.JsonDataStoresManagement.JsonDataStores.CreateJsonDataStore(dto));
            }
        }

        [Observation]//(Skip = "Speed up the build for testing")
        protected override void assert_all()
        {
            the_storedata_created_successfully();
            the_storedata_can_be_retrieved_by_partyId();
        }

        private void the_storedata_created_successfully()
        {
            foreach (var response in _postResponseList)
            {
                response.StatusCode.ShouldEqual(HttpStatusCode.OK);
                response.IsSuccess.ShouldBeTrue();
            }
        }

        private void the_storedata_can_be_retrieved_by_partyId()
        {
            var getResponse =
                Connector.JsonDataStoresManagement.JsonDataStores.GetByPartyId(_createJsonDataStoreDtoList.FirstOrDefault().PartyId);

            getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            getResponse.IsSuccess.ShouldBeTrue();
            getResponse.Response.Results.Count.ShouldEqual(_createJsonDataStoreDtoList.Count);
        }
    }
}
