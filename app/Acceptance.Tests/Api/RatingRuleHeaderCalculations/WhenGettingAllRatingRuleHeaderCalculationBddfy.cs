﻿using System.Net;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.RatingRuleHeaderCalculations
{
    public class WhenGettingAllRatingRuleHeaderCalculationBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        public GETResponseDto<ListResultDto<ListRatingRuleHeaderCalculationDto>> _GetResponse;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
            _GetResponse = Connector.RatingRuleHeaderCalculationManagement.RatingRuleHeaderCalculations.GetAll();
        }

        public void Then_result()
        {
            _GetResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _GetResponse.IsSuccess.ShouldBeTrue();
            _GetResponse.Response.ShouldNotBeNull();
        }
    }
}
