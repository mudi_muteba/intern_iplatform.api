﻿using iPlatform.Api.DTOs.Base.Connector;
namespace Acceptance.Tests.Api.RatingRuleHeaderCalculations
{
   public class RatingRuleHeaderCalculationTests
    {
        private readonly Connector _connector;
        public RatingRuleHeaderCalculationTests(Connector connector)
        {
            _connector = connector;
        }

        public void run_all()
        {
            new WhenCreatingRatingRuleHeaderCalculationBddfy().Run(_connector);
            new WhenEditingRatingRuleHeaderCalculationBddfy().Run(_connector);
            new WhenGettingARatingRuleHeaderCalculationBddfy().Run(_connector);
            new WhenGettingAllRatingRuleHeaderCalculationBddfy().Run(_connector);
            new WhenDeletingRatingRuleHeaderCalculationBddfy().Run(_connector);
        }
    }
}
