﻿using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;
using TestObjects.Mothers.RatingRuleHeaderCalculations;
using TestObjects.Mothers.RatingRuleHeaders;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.RatingRuleHeaderCalculations
{
    public class WhenEditingRatingRuleHeaderCalculationBddfy : IBddfyTest
    {
        private CreateRatingRuleHeaderCalculationDto _saveDto;
        private POSTResponseDto<int> _response;
        private PUTResponseDto<int> _putresponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createRatingRuleHeaderDto = RatingRuleHeaderDtoMother.ValidCreateDto();
            _response = Connector.RatingRuleHeaderManagement.RatingRuleHeaders.Create(createRatingRuleHeaderDto);

            _saveDto = RatingRuleHeaderCalculationDtoMother.ValidCreateDto(new RatingRuleHeaderDto()
            {
                Id = _response.Response,
                StartDate = createRatingRuleHeaderDto.StartDate.Value,
                EndDate = createRatingRuleHeaderDto.EndDate.Value
            });
            _response = Connector.RatingRuleHeaderCalculationManagement.RatingRuleHeaderCalculations.Create(_saveDto);
        }

        public void When_action()
        {
            _response.IsSuccess.ShouldBeTrue();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            Assert.True(_response.Response > 0);

            var editRatingRuleHeaderCalculation = RatingRuleHeaderCalculationDtoMother.ValidEditDto(_response.Response);
            _putresponse = Connector.RatingRuleHeaderCalculationManagement.RatingRuleHeaderCalculation(editRatingRuleHeaderCalculation.Id).Edit(editRatingRuleHeaderCalculation);
        }

        public void Then_result()
        {
            _response.IsSuccess.ShouldBeTrue();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.Response.ShouldNotBeNull();
        }
    }
}
