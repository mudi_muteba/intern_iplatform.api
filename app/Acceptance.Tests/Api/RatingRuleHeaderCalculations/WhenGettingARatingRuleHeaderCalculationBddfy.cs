﻿using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.RatingRuleHeaders;
using iPlatform.Api.DTOs.RatingRuleHeaderCalculations;
using TestObjects.Mothers.RatingRuleHeaderCalculations;
using TestObjects.Mothers.RatingRuleHeaders;
using TestStack.BDDfy;
using Xunit.Extensions;
using Xunit;

namespace Acceptance.Tests.Api.RatingRuleHeaderCalculations
{
    public class WhenGettingARatingRuleHeaderCalculationBddfy : IBddfyTest
    {
        private CreateRatingRuleHeaderCalculationDto _saveDto;
        private POSTResponseDto<int> _response;
        private GETResponseDto<RatingRuleHeaderCalculationDto> _GetResponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createRatingRuleHeaderDto = RatingRuleHeaderDtoMother.ValidCreateDto();
            _response = Connector.RatingRuleHeaderManagement.RatingRuleHeaders.Create(createRatingRuleHeaderDto);

            _saveDto = RatingRuleHeaderCalculationDtoMother.ValidCreateDto(new RatingRuleHeaderDto()
            {
                Id = _response.Response,
                StartDate = createRatingRuleHeaderDto.StartDate.Value,
                EndDate = createRatingRuleHeaderDto.EndDate.Value
            });
            _response = Connector.RatingRuleHeaderCalculationManagement.RatingRuleHeaderCalculations.Create(_saveDto);
        }

        public void When_action()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            Assert.True(_response.Response > 0);

            _GetResponse = Connector.RatingRuleHeaderCalculationManagement.RatingRuleHeaderCalculation(_response.Response).Get();
        }

        public void Then_result()
        {
            _GetResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _GetResponse.IsSuccess.ShouldBeTrue();
            _GetResponse.Response.ShouldNotBeNull();
        }
    }
}
