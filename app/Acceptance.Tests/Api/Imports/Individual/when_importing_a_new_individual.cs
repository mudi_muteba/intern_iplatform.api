using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;
using System;
using Xunit;

namespace Acceptance.Tests.Api.Imports.Individual
{
    public class when_importing_a_new_individual : BaseTest
    {
        private readonly POSTResponseDto<Guid> _response;

        public when_importing_a_new_individual() : base(ApiUserObjectMother.AsAdmin())
        {
            var importedIndividual = NewIndividualDtoObjectMother.NewImportIndividual();
            _response = Connector.ImportManagement.Individual.Import(importedIndividual);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_new_individual_is_imported();
        }

        private void the_new_individual_is_imported()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            Guid guidOutput;
            Assert.True(Guid.TryParse(_response.Response.ToString(), out guidOutput));
        }
    }
}