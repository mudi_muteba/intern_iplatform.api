using System.Net;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.Admin;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Admin
{
    public class WhenRetrievingAChannelsByIdBddfy : IBddfyTest
    {
        private CreateChannelDto _createChannelDto;
        private POSTResponseDto<int> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createChannelDto = NewChannelDtoObjectMother.ValidChannelDto();
            _response = Connector.ChannelManagement.Channels.Create(_createChannelDto);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
        }


        private void the_channel_is_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            Assert.True(_response.Response > 0);
        }
        private void the_channel_is_retrieved()
        {
            Assert.True(_response.Response > 0);

            var result = Connector.ChannelManagement.Channel(_response.Response).Get();

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();

            result.Response.Currency.Id.ShouldEqual(_createChannelDto.Currency.Id);
            result.Response.Language.Id.ShouldEqual(_createChannelDto.Language.Id);
            result.Response.Name.ShouldEqual(_createChannelDto.Name);
            result.Response.DateFormat.ShouldEqual(_createChannelDto.DateFormat);
            result.Response.IsActive.ShouldEqual(_createChannelDto.IsActive);
            result.Response.IsDefault.ShouldEqual(_createChannelDto.IsDefault);
            result.Response.Code.ShouldEqual(_createChannelDto.Code);
        }

        private void the_channel_is_updated()
        {
            Assert.True(_response.Response > 0);

            var editChannelDto = NewChannelDtoObjectMother.ValidEditChannelDto(_createChannelDto);

            var resultEdit = Connector.ChannelManagement.Channel(_response.Response).Edit(editChannelDto);

            var result = Connector.ChannelManagement.Channel(_response.Response).Get();

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();

            result.Response.Currency.Id.ShouldEqual(editChannelDto.Currency.Id);
            result.Response.Language.Id.ShouldEqual(editChannelDto.Language.Id);
            result.Response.Name.ShouldEqual(editChannelDto.Name);
            result.Response.DateFormat.ShouldEqual(editChannelDto.DateFormat);
            result.Response.IsActive.ShouldEqual(editChannelDto.IsActive);
            result.Response.IsDefault.ShouldEqual(editChannelDto.IsDefault);
            result.Response.Code.ShouldEqual(editChannelDto.Code);
        }

        private void the_channel_code_must_be_unique_when_updated()
        {
            Assert.True(_response.Response > 0);

            //do not update the same entity we are working with 
            var editChannelDto = NewChannelDtoObjectMother.ValidEditChannelUniqueCodeDto(_createChannelDto.Id - 1);
            editChannelDto.Code = _createChannelDto.Code;
            var resultEdit = Connector.ChannelManagement.Channel(_response.Response).Edit(editChannelDto);

            //BadRequest because editChannelDto.Code = CodeA already exist in initial TestData Chnannels
            resultEdit.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            resultEdit.IsSuccess.ShouldBeFalse();
        }
    }
}