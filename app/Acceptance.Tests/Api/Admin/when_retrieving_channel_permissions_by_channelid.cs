using System.Net;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Admin;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Admin
{
    public class when_retrieving_channel_permissions_by_channelid : BaseTest
    {
        private readonly CreateChannelPermissionDto _createChannelPermissionDto;
        private readonly POSTResponseDto<int> _response;

        public when_retrieving_channel_permissions_by_channelid() : base(ApiUserObjectMother.AsAdmin())
        {
            _createChannelPermissionDto = NewChannelDtoObjectMother.ValidChannelPermissionDto4();
            _response = Connector.ChannelManagement.Permissions.Create(_createChannelPermissionDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_channels_are_retrieved();
        }
        
        public void the_channels_are_retrieved()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            Assert.True(_response.Response > 0);

            var result = Connector.ChannelManagement.Permissions.GetByChannelId(_createChannelPermissionDto.ChannelId);
            
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
            Assert.True(result.Response.Results.Count > 0);
        }
    }
}