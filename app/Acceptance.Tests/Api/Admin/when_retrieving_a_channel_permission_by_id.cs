using System.Net;
using iPlatform.Api.DTOs.Admin.ChannelPermissions;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Admin;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Admin
{
    public class when_retrieving_a_channel_permission_by_id : BaseTest
    {
        private readonly CreateChannelPermissionDto _createChannelPermissionDto;
        private readonly POSTResponseDto<int> _response;

        public when_retrieving_a_channel_permission_by_id() : base(ApiUserObjectMother.AsAdmin())
        {
            _createChannelPermissionDto = NewChannelDtoObjectMother.ValidChannelPermissionDto3();
            _response = Connector.ChannelManagement.Permissions.Create(_createChannelPermissionDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_channel_permission_is_retrieved();
        }
        
        public void the_channel_permission_is_retrieved()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            Assert.True(_response.Response > 0);

            var result = Connector.ChannelManagement.Permission(_response.Response).Get();
            
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();

            result.Response.AllowProposalCreation.Allowed.ShouldEqual(_createChannelPermissionDto.AllowProposalCreation);
            result.Response.AllowSecondLevelUnderwriting.Allowed.ShouldEqual(_createChannelPermissionDto.AllowSecondLevelUnderwriting);
            result.Response.AllowQuoting.Allowed.ShouldEqual(_createChannelPermissionDto.AllowQuoting);
            result.Response.ChannelId.ShouldEqual(_createChannelPermissionDto.ChannelId);
            result.Response.InsurerCode.ShouldEqual(_createChannelPermissionDto.InsurerCode);
            result.Response.ProductCode.ShouldEqual(_createChannelPermissionDto.ProductCode);
        }
    }
}