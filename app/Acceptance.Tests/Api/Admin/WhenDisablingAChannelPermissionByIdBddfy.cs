using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.Admin;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Admin
{
    public class WhenDisablingAChannelPermissionByIdBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createChannelPermissionDto = NewChannelDtoObjectMother.ValidChannelPermissionDto2();
            _response = Connector.ChannelManagement.Permissions.Create(createChannelPermissionDto);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            Assert.True(_response.Response > 0);

            var result = Connector.ChannelManagement.Permission(_response.Response).Disable();

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();

        }
    }
}