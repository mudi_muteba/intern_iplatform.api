using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.Admin
{
    public class AdminTests
    {
        private readonly Connector _connector;

        public AdminTests(Connector connector)
        {
            _connector = connector;
            var token = new ConnectorTestData(_connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _connector.SetToken(token);
        }

        public void run_all()
        {
            new WhenCreatingAChannelPermissionBddfy().Run(_connector);
            new WhenDisablingAChannelPermissionByIdBddfy().Run(_connector);
            new WhenRetrievingAChannelsByIdBddfy().Run(_connector);
            new WhenRetrievingAllChannelsBddfy().Run(_connector);
            new WhenCreatingAChannelBddfy().Run(_connector);
            new WhenEditingAChannelBddfy().Run(_connector);
        }
    }
}