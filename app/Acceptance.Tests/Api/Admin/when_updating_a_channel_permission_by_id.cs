using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Admin;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Admin
{
    public class when_updating_a_channel_permission_by_id : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_updating_a_channel_permission_by_id() : base(ApiUserObjectMother.AsAdmin())
        {
            var createChannelPermissionDto = NewChannelDtoObjectMother.ValidChannelPermissionDto();
            _response = Connector.ChannelManagement.Permissions.Create(createChannelPermissionDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_channel_permission_is_updated();
        }

        public void the_channel_permission_is_updated()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            Assert.True(_response.Response > 0);

            var editChannelPermissionDto = NewChannelDtoObjectMother.ValidChannelPermissionDto(_response.Response);

            var resultEdit = Connector.ChannelManagement.Permission(_response.Response).Edit(editChannelPermissionDto);
            
            resultEdit.StatusCode.ShouldEqual(HttpStatusCode.OK);
            resultEdit.IsSuccess.ShouldBeTrue();

            var result = Connector.ChannelManagement.Permission(_response.Response).Get();

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();

            result.Response.AllowProposalCreation.Allowed.ShouldEqual(editChannelPermissionDto.AllowProposalCreation);
            result.Response.AllowSecondLevelUnderwriting.Allowed.ShouldEqual(editChannelPermissionDto.AllowSecondLevelUnderwriting);
            result.Response.AllowQuoting.Allowed.ShouldEqual(editChannelPermissionDto.AllowQuoting);
            result.Response.ChannelId.ShouldEqual(editChannelPermissionDto.ChannelId);
            result.Response.InsurerCode.ShouldEqual(editChannelPermissionDto.InsurerCode);
            result.Response.ProductCode.ShouldEqual(editChannelPermissionDto.ProductCode);
        }
    }
}