using System.Net;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Admin
{
    public class WhenRetrievingAllChannelsBddfy : IBddfyTest
    {
        private GETResponseDto<PagedResultDto<ListChannelDto>> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
            _response = Connector.ChannelManagement.Channels.GetAll();

        }

        public void Then_result()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            Assert.True(_response.Response.Results.Count > 0);

        }
    }
}