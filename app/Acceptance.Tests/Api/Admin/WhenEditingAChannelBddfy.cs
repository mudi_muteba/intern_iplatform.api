using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.Admin;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;
using iPlatform.Api.DTOs.Admin;

namespace Acceptance.Tests.Api.Admin
{
    public class WhenEditingAChannelBddfy : IBddfyTest
    {
        private EditChannelDto editChannelDto;
        private POSTResponseDto<int> _response;
        private PUTResponseDto<int> _putresponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createChannelDto = NewChannelDtoObjectMother.ValidChannelDto();
            _response = Connector.ChannelManagement.Channels.Create(createChannelDto);
            
            editChannelDto = NewChannelDtoObjectMother.ValidEditChannelDto(_response.Response);

        }

        public void When_action()
        {
            _putresponse = Connector.ChannelManagement.Channel(_response.Response).Edit(editChannelDto);
        }

        public void Then_result()
        {
            _putresponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _putresponse.IsSuccess.ShouldBeTrue();

            Assert.True(_putresponse.Response > 0);

        }
    }
}