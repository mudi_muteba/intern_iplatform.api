using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Campaigns;
using Xunit.Extensions;
using TestObjects.Mothers.HistoryLoss;
using TestObjects.Mothers.Individual;
using Xunit;

namespace Acceptance.Tests.Api.HistoryLoss.Home
{
    public class when_creating_a_new_homelosshistory : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_creating_a_new_homelosshistory() : base(ApiUserObjectMother.AsAdmin(),ConnectionType.SqlServer)
        {
        }

        public override void Observe()
        {
            //var individualId = new ConnectorTestData(Connector).Individuals.Create(NewIndividualDtoObjectMother.ValidIndividualDto());
            var createValidCampaign = NewCampaignDtoObjectMother.ValidCampaignDto();
            var foo = Connector.CampaignManagement.Campaigns.CreateCampaign(createValidCampaign);
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        [Observation]//(Skip = "Speed up the build for testing")
        protected override void assert_all()
        {
            the_homelosshistory_is_created();
        }

        private void the_homelosshistory_is_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createHomeLossHistoryDto = NewHomeLossHistoryDtoObjectMother.ValidHomeLossHistoryDto();
            createHomeLossHistoryDto.PartyId = _response.Response;
            _response = Connector.HomeLossHistoryManagement.HomeLossHistories.CreateHomeLossHistory(createHomeLossHistoryDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var result = Connector.HomeLossHistoryManagement.HomeLossHistories.GetAllHomeLossHistory();
            Assert.True(result.Results.Count > 0);
        }
    }
}