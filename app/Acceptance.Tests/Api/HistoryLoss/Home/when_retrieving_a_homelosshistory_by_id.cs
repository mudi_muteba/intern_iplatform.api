using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.HistoryLoss;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.HistoryLoss.Home
{
    public class when_retrieving_a_homelosshistory_by_id : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_retrieving_a_homelosshistory_by_id() : base(ApiUserObjectMother.AsAdmin())
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_homelosshistory_is_retrieved_by_id();
        }

        private void the_homelosshistory_is_retrieved_by_id()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createHomeLossHistoryDto = NewHomeLossHistoryDtoObjectMother.ValidHomeLossHistoryDto();
            createHomeLossHistoryDto.PartyId = _response.Response;
            _response = Connector.HomeLossHistoryManagement.HomeLossHistories.CreateHomeLossHistory(createHomeLossHistoryDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var result = Connector.HomeLossHistoryManagement.HomeLossHistory(_response.Response).Get();
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }
    }
}