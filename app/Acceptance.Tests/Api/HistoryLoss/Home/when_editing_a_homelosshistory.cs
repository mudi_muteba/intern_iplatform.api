using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.HistoryLoss;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.HistoryLoss.Home
{
    public class when_editing_a_homelosshistory : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_editing_a_homelosshistory() : base(ApiUserObjectMother.AsAdmin())
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_homelosshistory_is_edited();
        }

        public void the_homelosshistory_is_edited()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createHomeLossHistoryDto = NewHomeLossHistoryDtoObjectMother.ValidHomeLossHistoryDto();
            createHomeLossHistoryDto.PartyId = _response.Response;
            _response = Connector.HomeLossHistoryManagement.HomeLossHistories.CreateHomeLossHistory(createHomeLossHistoryDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var ediDto = NewHomeLossHistoryDtoObjectMother.ValidEditHomeLossHistoryDto();
            ediDto.PartyId = createHomeLossHistoryDto.PartyId;
            ediDto.Id = _response.Response;
            var result = Connector.HomeLossHistoryManagement.HomeLossHistory(_response.Response).EditHomeLossHistory(ediDto);
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();

            var getresult = Connector.HomeLossHistoryManagement.HomeLossHistory(_response.Response).Get();
            getresult.Response.CurrentlyInsured.Id.ShouldEqual(ediDto.CurrentlyInsuredId);
            getresult.Response.DateOfLoss.ShouldEqual(ediDto.DateOfLoss);
            getresult.Response.InsurerCancel.ShouldEqual(ediDto.InsurerCancel);
            getresult.Response.HomeClaimAmount.Id.ShouldEqual(ediDto.HomeClaimAmountId);
            getresult.Response.HomeClaimLocation.Id.ShouldEqual(ediDto.HomeClaimLocationId);
            getresult.Response.HomeTypeOfLoss.Id.ShouldEqual(ediDto.HomeTypeOfLossId);
            getresult.Response.PartyId.ShouldEqual(ediDto.PartyId);
        }
    } 
}