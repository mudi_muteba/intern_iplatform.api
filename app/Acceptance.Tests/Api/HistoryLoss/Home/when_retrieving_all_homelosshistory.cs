using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit.Extensions;
using TestObjects.Mothers.HistoryLoss;
using TestObjects.Mothers.Individual;

namespace Acceptance.Tests.Api.HistoryLoss.Motor
{
    public class when_retrieving_all_homelosshistory : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_retrieving_all_homelosshistory() : base(ApiUserObjectMother.AsAdmin())
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_homelosshistories_is_retrieved();
        }

        private void the_homelosshistories_is_retrieved()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createHomeLossHistoryDto = NewHomeLossHistoryDtoObjectMother.ValidHomeLossHistoryDto();
            createHomeLossHistoryDto.PartyId = _response.Response;
            _response = Connector.HomeLossHistoryManagement.HomeLossHistories.CreateHomeLossHistory(createHomeLossHistoryDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}