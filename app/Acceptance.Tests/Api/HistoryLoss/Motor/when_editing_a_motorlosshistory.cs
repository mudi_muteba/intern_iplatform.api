using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.HistoryLoss;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.HistoryLoss.Motor
{
    public class when_editing_a_motorlosshistory : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_editing_a_motorlosshistory() : base(ApiUserObjectMother.AsAdmin())
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_motorlosshistory_is_edited();
        }

        private void the_motorlosshistory_is_edited()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createMotorLossHistoryDto = NewMotorLossHistoryDtoObjectMother.ValidMotorLossHistoryDto();
            createMotorLossHistoryDto.PartyId = _response.Response;
            _response = Connector.MotorLossHistoryManagement.MotorLossHistories.CreateMotorLossHistory(createMotorLossHistoryDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var ediDto = NewMotorLossHistoryDtoObjectMother.ValidEditMotorLossHistoryDto();
            ediDto.PartyId = createMotorLossHistoryDto.PartyId;
            ediDto.Id = _response.Response;
            var result = Connector.MotorLossHistoryManagement.MotorLossHistory(_response.Response).EditMotorLossHistory(ediDto);
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();

            var getresult = Connector.MotorLossHistoryManagement.MotorLossHistory(_response.Response).Get();

            getresult.Response.CurrentlyInsured.Id.ShouldEqual(ediDto.CurrentlyInsuredId);
            getresult.Response.DateOfLoss.ShouldEqual(ediDto.DateOfLoss);
            getresult.Response.InsurerCancel.ShouldEqual(ediDto.InsurerCancel);
            getresult.Response.MotorClaimAmount.Id.ShouldEqual(ediDto.MotorClaimAmountId);
            getresult.Response.MotorCurrentTypeOfCover.Id.ShouldEqual(ediDto.MotorCurrentTypeOfCoverId);
            getresult.Response.MotorTypeOfLoss.Id.ShouldEqual(ediDto.MotorTypeOfLossId);
            getresult.Response.MotorUninterruptedPolicyNoClaim.Id.ShouldEqual(ediDto.MotorUninterruptedPolicyNoClaimId);
            getresult.Response.PartyId.ShouldEqual(ediDto.PartyId);
            getresult.Response.PreviousComprehensiveInsurance.ShouldEqual(ediDto.PreviousComprehensiveInsurance);
        }
    } 
}