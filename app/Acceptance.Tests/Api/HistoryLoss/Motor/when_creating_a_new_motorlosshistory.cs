using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.HistoryLoss;
using TestObjects.Mothers.Individual;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.HistoryLoss.Motor
{
    public class when_creating_a_new_motorlosshistory : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_creating_a_new_motorlosshistory() : base(ApiUserObjectMother.AsAdmin())
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_motorlosshistory_is_created();
        }

        private void the_motorlosshistory_is_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createMotorLossHistoryDto = NewMotorLossHistoryDtoObjectMother.ValidMotorLossHistoryDto();
            createMotorLossHistoryDto.PartyId = _response.Response;
            _response = Connector.MotorLossHistoryManagement.MotorLossHistories.CreateMotorLossHistory(createMotorLossHistoryDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}