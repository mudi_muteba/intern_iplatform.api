using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.HistoryLoss;
using TestObjects.Mothers.Individual;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.HistoryLoss.Motor
{
    public class when_retrieving_all_motorlosshistory : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_retrieving_all_motorlosshistory() : base(ApiUserObjectMother.AsAdmin())
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_motorlosshistories_is_retrieved();
        }

        private void the_motorlosshistories_is_retrieved()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createMotorLossHistoryDto = NewMotorLossHistoryDtoObjectMother.ValidMotorLossHistoryDto();
            createMotorLossHistoryDto.PartyId = _response.Response;
            _response = Connector.MotorLossHistoryManagement.MotorLossHistories.CreateMotorLossHistory(createMotorLossHistoryDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var result = Connector.MotorLossHistoryManagement.MotorLossHistories.GetAllMotorLossHistory();
            Assert.True(result.Results.Count > 0);
        }
    }
}