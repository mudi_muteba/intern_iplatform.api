﻿using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.HistoryLoss;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.HistoryLoss.LossHistory
{
    public class when_updating_a_losshistory : BaseTest
    {
        private readonly CreateLossHistoryDto _newLossHistoryDto;
        private readonly EditLossHistoryDto _editLossHistoryDto;
        private POSTResponseDto<int> _postResponse;

        public when_updating_a_losshistory() : base(ApiUserObjectMother.AsAdmin())
        {
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());

            Connector.SetToken(token);

            _newLossHistoryDto = new NewLossHistoryDtoObjectMother().CreateLossHistoryDto;
            _editLossHistoryDto = new NewLossHistoryDtoObjectMother().EditLossHistoryDto;

            _postResponse = Connector.LossHistoryManagement.LossHistories.CreateLossHisotry(_newLossHistoryDto);
        }

        public override void Observe()
        {
            
        }

        //[Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_losshistory_is_updated();
        }

        private void the_losshistory_is_updated()
        {
            //_postResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            //_postResponse.IsSuccess.ShouldBeTrue();

            //_editLossHistoryDto.Id = _postResponse.Response;
            //_editLossHistoryDto.PartyId = _newLossHistoryDto.PartyId;

            //var updateRequestResult =
            //    connector.LossHistoryManagement.LossHistory(_postResponse.Response).EditLossHistory(_editLossHistoryDto);
            //updateRequestResult.StatusCode.ShouldEqual(HttpStatusCode.OK);
            //updateRequestResult.IsSuccess.ShouldBeTrue();


            //var getRequestResult =
            //    connector.LossHistoryManagement.LossHistory(_postResponse.Response).Get();

            //Update retruns 200 but response is did not update
            //getRequestResult.Response.PartyId.ShouldEqual(_editLossHistoryDto.PartyId);
            //getRequestResult.Response.CurrentlyInsuredId.ShouldEqual(_editLossHistoryDto.CurrentlyInsuredId);
            //getRequestResult.Response.InsurerCancel.ShouldEqual(_editLossHistoryDto.InsurerCancel);
            //getRequestResult.Response.InterruptedPolicyClaim.ShouldEqual(_editLossHistoryDto.InterruptedPolicyClaim);
            //getRequestResult.Response.PreviousComprehensiveInsurance.ShouldEqual(_editLossHistoryDto.PreviousComprehensiveInsurance);
            //getRequestResult.Response.UninterruptedPolicyId.ShouldEqual(_editLossHistoryDto.UninterruptedPolicyId);
            //getRequestResult.Response.UninterruptedPolicyNoClaimId.ShouldEqual(_editLossHistoryDto.UninterruptedPolicyNoClaimId);
        }
        
    }
}
