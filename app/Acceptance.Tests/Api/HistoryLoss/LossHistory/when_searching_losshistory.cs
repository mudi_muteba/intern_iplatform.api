﻿using System.Net;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.HistoryLoss;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.HistoryLoss.LossHistory
{
    public class when_searching_losshistory : BaseTest
    {
        private readonly POSTResponseDto<int> _postCreateResponse;
        private readonly POSTResponseDto<PagedResultDto<LossHistoryDto>> _postSearchResponse;

        public when_searching_losshistory() : base(ApiUserObjectMother.AsAdmin())
        {
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());

            Connector.SetToken(token);

            var searchDto = new LossHistorySearchDto();
            var newLossHistoryDto = new NewLossHistoryDtoObjectMother().CreateLossHistoryDto;

            _postCreateResponse = Connector.LossHistoryManagement.LossHistories.CreateLossHisotry(newLossHistoryDto);
            _postSearchResponse = Connector.LossHistoryManagement.LossHistories.SearchLossHistory(searchDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_losshistory_is_created();
            get_the_losshistory();
        }

        private void the_losshistory_is_created()
        {
            _postCreateResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postCreateResponse.IsSuccess.ShouldBeTrue();
        }

        private void get_the_losshistory()
        {
            _postSearchResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postSearchResponse.IsSuccess.ShouldBeTrue();
            _postSearchResponse.ShouldNotBeNull();
        }
    }
}
