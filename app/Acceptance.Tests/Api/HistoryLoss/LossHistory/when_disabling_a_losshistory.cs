﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.HistoryLoss;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.HistoryLoss.LossHistory
{
    public class when_disabling_a_losshistory : BaseTest
    {
        private readonly POSTResponseDto<int> _postResponse;

        public when_disabling_a_losshistory() : base(ApiUserObjectMother.AsAdmin())
        {
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            Connector.SetToken(token);

            var newLossHistoryDto = new NewLossHistoryDtoObjectMother().CreateLossHistoryDto;
            _postResponse = Connector.LossHistoryManagement.LossHistories.CreateLossHisotry(newLossHistoryDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_losshistory_is_disabled_by_id();
        }

        private void the_losshistory_is_disabled_by_id()
        {
            _postResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.LossHistoryManagement.LossHistory(_postResponse.Response).Disable();
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }
    }
}
