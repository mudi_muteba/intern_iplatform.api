﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.HistoryLoss.LossHistory;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.HistoryLoss;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.HistoryLoss.LossHistory
{
    public class when_retrieving_a_losshistory_by_partyId : BaseTest
    {
        private readonly CreateLossHistoryDto _newLossHistoryDto;
        private readonly POSTResponseDto<int> _postResponse;

        public when_retrieving_a_losshistory_by_partyId() : base(ApiUserObjectMother.AsAdmin())
        {
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            Connector.SetToken(token);

            _newLossHistoryDto = new NewLossHistoryDtoObjectMother().CreateLossHistoryDto;
            _postResponse = Connector.LossHistoryManagement.LossHistories.CreateLossHisotry(_newLossHistoryDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_losshistory_is_retrieved_by_partyid();
        }

        private void the_losshistory_is_retrieved_by_partyid()
        {
            _postResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _postResponse.IsSuccess.ShouldBeTrue();

            var result = Connector.LossHistoryManagement.LossHistories.GetByPartyId(_newLossHistoryDto.PartyId);
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }
    }
}
