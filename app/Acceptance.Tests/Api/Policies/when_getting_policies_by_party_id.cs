﻿using System.Net;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Policy;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Policies
{
    public class when_getting_policies_by_party_id : BaseTest
    {
        private readonly POSTResponseDto<ListResultDto<PolicyHeaderDto>> _response;

        public when_getting_policies_by_party_id() : base(ApiUserObjectMother.AsAdmin())
        {
            //connector = new Connector();

            //var token = new ConnectorTestData(connector)
            //    .Authentication.Authenticate(AuthenticationObjectMother.Channel2User());

            //connector = new Connector(new ApiToken(token));

            _response = Connector.PolicyHeaderManagement.PolicyHeaders.SearchPolicyHeaderNoPagination(new PolicySearchDto{PartyId = 1258});
        }

        public override void Observe()
        {
           
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_allocated_products_are_returned();
        }
        
        private void the_allocated_products_are_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}
