﻿using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Failures
{
    public class when_an_unhandled_exception_occurs : Specification
    {
        private readonly Connector connector = new Connector();
        private BaseResponseDto response;

        public override void Observe()
        {
            response = connector.Testing.Failure();
        }

        [Observation(Skip = "To be fixed")]
        public void the_correct_status_code_is_returned()
        {
            response.StatusCode.ShouldEqual(HttpStatusCode.InternalServerError);
        }
    }
}