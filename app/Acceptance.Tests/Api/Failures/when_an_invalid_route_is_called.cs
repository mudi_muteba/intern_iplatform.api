﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Failures
{
    public class when_an_invalid_route_is_called : BaseTest
    {
        private readonly BaseResponseDto _response;

        public when_an_invalid_route_is_called() : base(ApiUserObjectMother.AsAdmin())
        {
            _response = Connector.Testing.InvalidRoute();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_correct_status_code_is_returned();
        }

        private void the_correct_status_code_is_returned()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.NotFound);
        }
    }
}