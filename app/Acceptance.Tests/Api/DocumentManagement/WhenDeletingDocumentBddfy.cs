
using System.Net;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.DocumentManagement;
using TestObjects.Mothers.DocumentManagement;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.DocumentManagement
{
    public class WhenDeletingDocumentBddfy : IBddfyTest
    {
     
        private DeleteDocumentDto deleteDocumentDto;
        private PUTResponseDto<int> _response;
        public Connector Connector { get; set; }
     

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createDto = DocumentManagementObjectMother.ValidDocumentManagementDto();
            var x = Connector.DocumentManagement.Documents.StoreDocument(createDto);
            deleteDocumentDto = DocumentManagementObjectMother.ValidDeleteDocumentDto(x.Response);
        }

        public void When_action()
        {
            _response = Connector.DocumentManagement.Documents.DeleteDocument(deleteDocumentDto);
        }

        public void Then_result()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            Assert.True(_response.Response > 0);
        }
    }
}