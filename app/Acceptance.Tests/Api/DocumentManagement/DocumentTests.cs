using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.DocumentManagement
{
    public class DocumentTests
    {
        private readonly Connector _connector;

        public DocumentTests(Connector connector)
        {
            _connector = connector;
            var token = new ConnectorTestData(_connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _connector.SetToken(token);
        }

        public void run_all()
        {
            new WhenCreatingDocumentBddfy().Run(_connector);
            new WhenGettingDocumentsByCreatorIdBddfy().Run(_connector);
            new WhenGettingDocumentsByPartyIdBddfy().Run(_connector);
            new WhenGettingDocumentBddfy().Run(_connector);
            new WhenDeletingDocumentBddfy().Run(_connector);
        }
    }
}