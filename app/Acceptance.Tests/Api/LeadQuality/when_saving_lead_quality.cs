﻿using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Leads.Quality;
using TestHelper;
using TestObjects.Mothers.Campaigns;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Lead;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.LeadQuality
{
    public class when_saving_lead_quality : BaseTest
    {
        private POSTResponseDto<int> _response;
        private readonly int _leadId;
        private EditLeadQualityDto _editLeadQualityDto;

        public when_saving_lead_quality() : base(ApiUserObjectMother.AsAdmin())
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();

            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);

            //Individual
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            var partyId = _response.Response;

            //Campaign
            var createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            //Lead
            var createLeadDto = LeadsDtoObjectMother.ValidLeadDto(partyId, _response.Response);
            _response = Connector.LeadManagement.Lead.SaveLead(createLeadDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            _leadId = _response.Response;
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            lead_quality_is_saved();
            lead_quality_is_updated();
            lead_quality_is_retrieved();
        }

        private void lead_quality_is_saved()
        {
            var createLeadQualityDto = LeadsDtoObjectMother.ValidLeadQualityDto();
            _response = Connector.LeadManagement.LeadQuality(_leadId).SaveLeadQuality(createLeadQualityDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }

        private void lead_quality_is_updated()
        {
            _editLeadQualityDto = LeadsDtoObjectMother.UpdatedvalidLeadQualityDto(_leadId);
            var result = Connector.LeadManagement.LeadQuality(_leadId).EditLeadQuality(_editLeadQualityDto);
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }

        
        private void lead_quality_is_retrieved()
        {
            lead_quality_is_saved();
            var result = Connector.LeadManagement.LeadQuality(_leadId).Get();
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }
    }
}
