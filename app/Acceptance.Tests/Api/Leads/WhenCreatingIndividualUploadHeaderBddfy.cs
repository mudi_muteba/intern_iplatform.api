using System;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Leads;
using TestObjects.Mothers.Campaigns;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Leads
{
    public class WhenCreatingIndividualUploadHeaderBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        private POSTResponseDto<int> _campaignResponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            _campaignResponse = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _campaignResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _campaignResponse.IsSuccess.ShouldBeTrue();

            var individualUploadHeaderDto = new CreateIndividualUploadHeaderDto(Guid.NewGuid(), 1, 1, _campaignResponse.Response, "Test lead", 1000, 800, 100, 100, 10, "Submitted");
            _response = Connector.ImportManagement.Lead.CreateIndividualUploadHeader(individualUploadHeaderDto);

            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}