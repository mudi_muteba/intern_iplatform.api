using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Campaigns;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Lead;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Leads
{
    public class when_creating_lead : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_creating_lead() : base(ApiUserObjectMother.AsAdmin())
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            should_create();
        }

        private void should_create()
        {
            //Individual
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            var PartyId = _response.Response;

            //Campaign
            var createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            //Lead
            var createLeadDto = LeadsDtoObjectMother.ValidLeadDto(PartyId, _response.Response);
            _response = Connector.LeadManagement.Lead.SaveLead(createLeadDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}