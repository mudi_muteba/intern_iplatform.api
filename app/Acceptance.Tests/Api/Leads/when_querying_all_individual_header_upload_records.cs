using System;
using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Leads;
using TestHelper;
using TestObjects.Mothers.Campaigns;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Leads
{
    public class when_querying_all_individual_header_upload_records : BaseTest
    {
        private readonly LISTPagedResponseDto<CreateIndividualUploadHeaderDto> _individualUploadHeaders;
        readonly Guid _leadImportReference = Guid.NewGuid();

        public when_querying_all_individual_header_upload_records() : base(ApiUserObjectMother.AsAdmin())
        {
            var createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            var campaignResponse = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
            var campaignId = campaignResponse.Response;

            Connector.ImportManagement.Lead.CreateIndividualUploadHeader(new CreateIndividualUploadHeaderDto(_leadImportReference, 1, 1, campaignId, "Leads1.xlsx", 1000, 800, 100, 100, 10, "Submitted"));
            Connector.ImportManagement.Lead.CreateIndividualUploadHeader(new CreateIndividualUploadHeaderDto(Guid.NewGuid(), 2, 2, campaignId, "Leads2.xlsx", 1001, 801, 101, 101, 11, "Submitted"));

            _individualUploadHeaders = Connector.ImportManagement.Lead.GetAllHeaderWithPagination(1, 10);
        }

        public override void Observe()
        {

        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_results_are_returned();
            a_paginated_list_is_returned();
        }

        private void the_results_are_returned()
        {
            _individualUploadHeaders.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _individualUploadHeaders.IsSuccess.ShouldBeTrue();
        }
        
        private void a_paginated_list_is_returned()
        {
            var headerRecords = _individualUploadHeaders.Response;

            headerRecords.PageNumber.ShouldEqual(1);
            headerRecords.TotalPages.ShouldEqual(1);
            headerRecords.TotalCount.ShouldEqual(2);

            var individualUploadHeaderDto = headerRecords.Results.First();
            individualUploadHeaderDto.ChannelId.ShouldEqual(1);
            individualUploadHeaderDto.DuplicateCount.ShouldEqual(100);
            individualUploadHeaderDto.DuplicateFileRecordCount.ShouldEqual(100);
            individualUploadHeaderDto.FailureCount.ShouldEqual(10);
            individualUploadHeaderDto.FileName.ShouldEqual("Leads1.xlsx");
            individualUploadHeaderDto.FileRecordCount.ShouldEqual(1000);
            individualUploadHeaderDto.LeadImportReference.ShouldEqual(_leadImportReference);
            individualUploadHeaderDto.NewCount.ShouldEqual(800);
            individualUploadHeaderDto.DateUpdated.Value.Date.ShouldEqual(DateTime.UtcNow.Date);
        }
    }
}