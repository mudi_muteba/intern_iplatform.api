using System;
using System.IO;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper.Helpers;
using TestObjects.Mothers.Lead;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Leads
{
    public class WhenSubmittingAValidSeritiLeadWithExistingAndValidCampaignBddfy : IBddfyTest
    {
        private POSTResponseDto<Guid> _response;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            Connector.SetToken(token);

            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Api", "Leads", "files",
                "seriti_with_existing_and_valid_campaign.xml");

            var actualXml = string.Empty;
            using (var reader = new StreamReader(filePath))
            {
                actualXml = reader.ReadToEnd();
            }

            var leadImportXmlDto = LeadsDtoObjectMother.ValidLeadImportXMLDtoWithCampaign(actualXml);

            _response = Connector.ImportManagement.Lead.ImportLeadSeriti(leadImportXmlDto);
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}