using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Leads
{
    public class when_retrieving_lead_by_id : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_retrieving_lead_by_id()
        {
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            Connector.SetToken(token);

            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            when_retrieved();
        }

        private void when_retrieved()
        {
            //Individual
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            var PartyId = _response.Response;


            //Retrieve Lead by PartyId
            var result1 = Connector.LeadManagement.Lead.GetByPartyId(PartyId);
            result1.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result1.IsSuccess.ShouldBeTrue();

            //Retrieve Lead By Id
            var result = Connector.LeadManagement.Lead.Get(result1.Response.Id);
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }
    }
}