using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestObjects.Mothers.Campaigns;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Lead;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Leads
{
    public class WhenCreatingLeadBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        private POSTResponseDto<int> _responseA;
        private POSTResponseDto<int> _responseB;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            _response = Connector.IndividualManagement.Individuals.CreateIndividual(createIndividualDto);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            //Individual
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            var PartyId = _response.Response;


            _response = null;
            //Campaign
            var createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            _responseA = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
            _responseA.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _responseA.IsSuccess.ShouldBeTrue();

            //Lead
            //var createLeadDto = LeadsDtoObjectMother.ValidLeadDto(PartyId, _response.Response);
            //_responseB = Connector.LeadManagement.Lead.SaveLead(createLeadDto);
            //_responseB.StatusCode.ShouldEqual(HttpStatusCode.OK);
            //_responseB.IsSuccess.ShouldBeTrue();
        }
    }
}