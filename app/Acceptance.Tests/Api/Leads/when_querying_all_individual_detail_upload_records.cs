using System;
using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Leads;
using TestHelper;
using TestObjects.Mothers.Campaigns;
using Xunit.Extensions;
using MasterData;

namespace Acceptance.Tests.Api.Leads
{
    public class when_querying_all_individual_detail_upload_records : BaseTest
    {
        private readonly LISTPagedResponseDto<IndividualUploadDetailDto> _individualUploadDetails;
        readonly Guid _leadImportReference = Guid.NewGuid();

        public when_querying_all_individual_detail_upload_records() : base(ApiUserObjectMother.AsAdmin())
        {
            var createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            var campaignResponse = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
            var campaignId = campaignResponse.Response;

            Connector.ImportManagement.Lead.CreateIndividualUploadDetail(new IndividualUploadDetailDto(_leadImportReference, 1, 1, campaignId, "Leads1.xlsx", "0821234567", 100, LeadImportStatuses.Submitted, true));
            Connector.ImportManagement.Lead.CreateIndividualUploadDetail(new IndividualUploadDetailDto(Guid.NewGuid(), 2, 2, campaignId, "Leads2.xlsx", "0821234568", 101, LeadImportStatuses.Submitted, true));

            _individualUploadDetails = Connector.ImportManagement.Lead.GetAllDetailWithPagination(_leadImportReference, 1, 10);
        }

        public override void Observe()
        {

        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_results_are_returned();
            a_paginated_list_is_returned();
        }

        private void the_results_are_returned()
        {
            _individualUploadDetails.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _individualUploadDetails.IsSuccess.ShouldBeTrue();
        }

        private void a_paginated_list_is_returned()
        {
            var detailRecords = _individualUploadDetails.Response;

            detailRecords.PageNumber.ShouldEqual(1);
            detailRecords.TotalPages.ShouldEqual(1);
            detailRecords.TotalCount.ShouldEqual(1);

            var individualUploadDetailDto = detailRecords.Results.First();
            individualUploadDetailDto.ChannelId.ShouldEqual(1);
            individualUploadDetailDto.ContactNumber.ShouldEqual("0821234567");
            individualUploadDetailDto.DuplicateFileRecordCount.ShouldEqual(100);
            individualUploadDetailDto.FileName.ShouldEqual("Leads1.xlsx");
            individualUploadDetailDto.LeadImportReference.ShouldEqual(_leadImportReference);
        }
    }
}