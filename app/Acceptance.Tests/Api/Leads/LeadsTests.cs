﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Lead;
using TestObjects.Mothers.Users;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Leads
{
    public class LeadsTests
    {
            private readonly Connector _connector;

            public LeadsTests(Connector connector)
        {
            _connector = connector;
        }

        public void run_all()
        {

            new WhenCreatingLeadBddfy().Run(_connector);
            new WhenCreatingIndividualUploadDetailBddfy().Run(_connector);
            new WhenSubmittingAValidSeritiLeadWithExistingAndValidCampaignBddfy().Run(_connector);
        
            //new WhenCreatingIndividualUploadHeaderBddfy().Run(_connector); //Master Data Issue
        }
    }

   
}


