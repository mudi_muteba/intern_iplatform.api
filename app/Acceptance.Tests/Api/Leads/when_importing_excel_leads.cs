﻿using System.Linq;
using System.Threading;
using System;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Leads;
using TestHelper;
using TestObjects.Mothers.Lead;
using Xunit.Extensions;
using System.Net;
using TestObjects.Mothers.Campaigns;

namespace Acceptance.Tests.Api.Leads
{
    public class when_importing_excel_leads : BaseTest
    {
        private readonly POSTResponseDto<Guid> _response;
        private readonly LISTPagedResponseDto<CreateIndividualUploadHeaderDto> _individualUploadHeaders;
        private readonly Guid _leadImportReference;

        public when_importing_excel_leads() : base(ApiUserObjectMother.AsAdmin())
        {
            var createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            var campaignResponse = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);

            var leadImportExcelDto = LeadsDtoObjectMother.ValidLeadImportExcelDtoWithCampaign(campaignResponse.Response, "leads.xlsx", LeadExcelFileMother.ValidFile);

            _response = Connector.ImportManagement.Lead.ImportLeadExcel(leadImportExcelDto);

            Thread.Sleep(1000);

            _individualUploadHeaders = Connector.ImportManagement.Lead.GetAllHeaderWithPagination(1, 10);

            _leadImportReference = _individualUploadHeaders.Response.Results.First().LeadImportReference;
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            imported_excel_leads();
            created_individuals();
            created_individual_upload_header();
            created_individual_upload_details();
        }

        private void imported_excel_leads()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }

        private void created_individuals()
        {
            var getResponse = Connector.IndividualManagement.Individuals.SearchIndividual(new IndividualSearchDto());

            getResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            getResponse.IsSuccess.ShouldBeTrue();

            var individuals = getResponse.Response;

            individuals.PageNumber.ShouldEqual(1);
            individuals.TotalPages.ShouldEqual(1);
            individuals.TotalCount.ShouldEqual(18);

            var individual = individuals.Results.Last();

            individual.FirstName.ShouldEqual("John20");
            individual.Surname.ShouldEqual("Doe");
        }

        private void created_individual_upload_header()
        {
            _individualUploadHeaders.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _individualUploadHeaders.IsSuccess.ShouldBeTrue();

            var headerRecords = _individualUploadHeaders.Response;

            headerRecords.PageNumber.ShouldEqual(1);
            headerRecords.TotalPages.ShouldEqual(1);
            headerRecords.TotalCount.ShouldEqual(1);

            var individualUploadHeaderDto = headerRecords.Results.First();
            individualUploadHeaderDto.ChannelId.ShouldEqual(2);
            individualUploadHeaderDto.DuplicateCount.ShouldEqual(0);
            individualUploadHeaderDto.DuplicateFileRecordCount.ShouldEqual(2);
            individualUploadHeaderDto.FailureCount.ShouldEqual(0);
            individualUploadHeaderDto.FileName.ShouldEqual("leads.xlsx");
            individualUploadHeaderDto.FileRecordCount.ShouldEqual(22);
            individualUploadHeaderDto.LeadImportReference.ShouldNotEqual(Guid.Empty);
            individualUploadHeaderDto.NewCount.ShouldEqual(17);
            individualUploadHeaderDto.DateUpdated.Value.Date.ShouldEqual(DateTime.UtcNow.Date);
        }
        
        private void created_individual_upload_details()
        {
            Thread.Sleep(4000);

            var response = Connector.ImportManagement.Lead.GetAllDetailWithPagination(_leadImportReference, 1, 10);

            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.IsSuccess.ShouldBeTrue();

            var detailRecords = response.Response;

            detailRecords.PageNumber.ShouldEqual(1);
            detailRecords.TotalPages.ShouldEqual(1);
            detailRecords.TotalCount.ShouldEqual(17);

            var individualUploadDetailDto = detailRecords.Results.First();
            individualUploadDetailDto.ChannelId.ShouldEqual(2);
            individualUploadDetailDto.ContactNumber.ShouldEqual("0821234567");
            individualUploadDetailDto.DuplicateFileRecordCount.ShouldEqual(4);
            individualUploadDetailDto.FileName.ShouldEqual("leads.xlsx");
            individualUploadDetailDto.LeadImportReference.ShouldEqual(_leadImportReference);
        }
    }
}
