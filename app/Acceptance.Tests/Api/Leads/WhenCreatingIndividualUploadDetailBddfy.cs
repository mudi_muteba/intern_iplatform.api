using System;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Leads;
using MasterData;
using TestHelper.Helpers;
using TestObjects.Mothers.Campaigns;
using TestObjects.Mothers.Users;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Leads
{
    public class WhenCreatingIndividualUploadDetailBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        private POSTResponseDto<int> _campaignResponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            var token = new ConnectorTestData(connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            connector.SetToken(token);

            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            createcampaignDto.Name = new Random().Next(100, 999) + "This is a test campaign " + new Random().Next(100, 999);
            createcampaignDto.Reference = "REF#" + new Random().Next(100, 999);

            _campaignResponse = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
        }

        public void When_action()
        {
            _response = Connector.ImportManagement.Lead.CreateIndividualUploadDetail(new IndividualUploadDetailDto(Guid.NewGuid(), 1, 1, _campaignResponse.Response, "Test lead", "0821234567", 100, LeadImportStatuses.Submitted, true));
        }

        public void Then_result()
        {
            _campaignResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _campaignResponse.IsSuccess.ShouldBeTrue();


            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}