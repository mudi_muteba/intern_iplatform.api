﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.LossHistoryDefinitions
{
    public class when_creating_a_new_definition : BaseTest
    {
        private readonly POSTResponseDto<int> _postResponse;

        public when_creating_a_new_definition() : base(ApiUserObjectMother.AsAdmin())
        {
            var token = new ConnectorTestData(Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());

            Connector.SetToken(token);


        }

        public override void Observe()
        {
            
        }

        protected override void assert_all()
        {
            
        }
    }
}
