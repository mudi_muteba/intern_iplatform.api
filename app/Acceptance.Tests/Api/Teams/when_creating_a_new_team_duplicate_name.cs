using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Teams;
using TestHelper;
using TestObjects.Mothers.Team;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Teams
{
    public class when_creating_a_new_team_duplicate_name : BaseTest
    {
        private readonly CreateTeamDto createTeamDto;
        private POSTResponseDto<int> response;

        public when_creating_a_new_team_duplicate_name() : base(ApiUserObjectMother.AsAdmin())
        {
            createTeamDto = NewTeamDtoObjectMother.ValidTeamDto();
            createTeamDto.Name = "TEST123";
            response = Connector.TeamManagement.Teams.CreateTeam(createTeamDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_team_is_created_duplicate_name();
        }

        private void the_team_is_created_duplicate_name()
        {
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.IsSuccess.ShouldBeTrue();

            response = Connector.TeamManagement.Teams.CreateTeam(createTeamDto);
            response.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            response.IsSuccess.ShouldBeFalse();
        }
    }
}