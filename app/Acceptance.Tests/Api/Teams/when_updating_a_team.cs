using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Teams;
using iPlatform.Api.DTOs.Users;
using TestHelper;
using TestObjects.Mothers.Team;
using Xunit.Extensions;
using TestObjects.Mothers.Campaigns;
using System;

namespace Acceptance.Tests.Api.Teams
{
    public class when_updating_a_team : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_updating_a_team() : base(ApiUserObjectMother.AsAdmin())
        {
            var createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_team_is_updated();
        }
        
        private void the_team_is_updated()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
            var campaignId = _response.Response;

            //Create Team
            var createTeamDto = NewTeamDtoObjectMother.ValidTeamDto();
            _response = Connector.TeamManagement.Teams.CreateTeam(createTeamDto);
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            //Edit team
            var editTeamDto = new EditTeamDto
            {
                Id = _response.Response,
                Name = "TEST" + DateTime.UtcNow
            };
            editTeamDto.Campaigns.Add(new CampaignInfoDto { Id = campaignId });
            editTeamDto.Users.Add(new UserInfoDto { Id = 1 });

            var editResponse = Connector.TeamManagement.Team(_response.Response).EditTeam(editTeamDto);

            editResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);

            //Verify Update & Verify Get by Id
            var result = Connector.TeamManagement.Team(_response.Response).Get();
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();

            var listTeamDto = result.Response;
            editTeamDto.Name.ShouldEqual(listTeamDto.Name);
        }
    }
}