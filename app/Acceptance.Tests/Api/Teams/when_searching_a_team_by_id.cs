using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Teams;
using TestHelper;
using TestObjects.Mothers.Team;
using Xunit;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Teams
{
    public class when_searching_a_team_by_id : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_searching_a_team_by_id() : base(ApiUserObjectMother.AsAdmin())
        {
            var createTeamDto = NewTeamDtoObjectMother.ValidTeamWithValidUserDto();
            _response = Connector.TeamManagement.Teams.CreateTeam(createTeamDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_team_searched_by_id();
        }

        private void the_team_searched_by_id()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var searchTeamDto = new SearchTeamDto { TeamId = _response.Response };
            var result = Connector.TeamManagement.Teams.Search(searchTeamDto);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
            Assert.True(result.Response.Results.Count > 0 );
        }
    }
}