using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Teams;
using TestHelper;
using TestObjects.Mothers.Team;
using Xunit.Extensions;
using Xunit;

namespace Acceptance.Tests.Api.Teams
{
    public class when_searching_a_team_by_name : BaseTest
    {
        private readonly CreateTeamDto _createTeamDto;
        private readonly POSTResponseDto<int> _response;

        public when_searching_a_team_by_name()
        {
            _createTeamDto = NewTeamDtoObjectMother.ValidTeamWithValidUserDto();
            _response = Connector.TeamManagement.Teams.CreateTeam(_createTeamDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_team_searched_by_name();
        }
        
        private void the_team_searched_by_name()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var searchTeamDto = new SearchTeamDto { Name = _createTeamDto.Name };
            var result = Connector.TeamManagement.Teams.Search(searchTeamDto);

            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
            Assert.True(result.Response.Results.Count > 0 );
        }
    }
}