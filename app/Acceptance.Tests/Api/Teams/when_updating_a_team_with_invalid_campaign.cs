using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Teams;
using TestHelper;
using TestObjects.Mothers.Team;
using Xunit.Extensions;
using System;

namespace Acceptance.Tests.Api.Teams
{
    public class when_updating_a_team_with_invalid_campaign : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_updating_a_team_with_invalid_campaign() : base(ApiUserObjectMother.AsAdmin())
        {
            var createTeamDto = NewTeamDtoObjectMother.ValidTeamDto();
            _response = Connector.TeamManagement.Teams.CreateTeam(createTeamDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_team_is_updated_with_invalid_campaign();
        }
        
        private void the_team_is_updated_with_invalid_campaign()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var editTeamDto = new EditTeamDto
            {
                Id = _response.Response,
                Name = "TEST" + DateTime.UtcNow
            };
            editTeamDto.Campaigns.Add(new CampaignInfoDto { Id = 999999 });

            var editResponse = Connector.TeamManagement.Team(_response.Response).EditTeam(editTeamDto);

            editResponse.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
            editResponse.IsSuccess.ShouldBeFalse();
        }
    }
}