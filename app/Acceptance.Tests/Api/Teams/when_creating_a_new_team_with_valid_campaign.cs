using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;
using TestHelper;
using TestObjects.Mothers.Team;
using Xunit.Extensions;
using TestObjects.Mothers.Campaigns;

namespace Acceptance.Tests.Api.Teams
{
    public class when_creating_a_new_team_with_valid_campaign : BaseTest
    {
        private POSTResponseDto<int> _response;

        public when_creating_a_new_team_with_valid_campaign() : base(ApiUserObjectMother.AsAdmin())
        {
            var createcampaignDto = NewCampaignDtoObjectMother.ValidCampaignDto();
            _response = Connector.CampaignManagement.Campaigns.CreateCampaign(createcampaignDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_team_is_created_with_valid_campaign();
        }
        
        private void the_team_is_created_with_valid_campaign()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var createTeamDto = NewTeamDtoObjectMother.ValidTeamDto();
            createTeamDto.Campaigns.Add(new CampaignInfoDto { Id = _response.Response });
            _response = Connector.TeamManagement.Teams.CreateTeam(createTeamDto);

            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}