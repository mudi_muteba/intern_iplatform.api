using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper;
using TestObjects.Mothers.Team;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Teams
{
    public class when_creating_a_new_team : BaseTest
    {
        private readonly POSTResponseDto<int> _response;

        public when_creating_a_new_team() : base(ApiUserObjectMother.AsAdmin())
        {
            var createTeamDto = NewTeamDtoObjectMother.ValidTeamDto();
            _response = Connector.TeamManagement.Teams.CreateTeam(createTeamDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            the_team_is_created();
        }

        public void the_team_is_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();
        }
    }
}