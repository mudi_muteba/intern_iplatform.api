﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.Models;
using TestHelper;
using Xunit.Extensions;


namespace Acceptance.Tests.Api.SiteMap
{
    public class when_retrieving_all_siteMapBaseNodes : BaseTest
    {
        private readonly List<SiteMapBaseNode> siteMapBaseNodes;

        public when_retrieving_all_siteMapBaseNodes()
        {
            siteMapBaseNodes = new global::Api.Models.SiteMap().GetAllSiteNodes();
        }

        public override void Observe()
        {

        }
        // some sql lite error fails in the installer will come back to it.... as i have to do support
        //[Observation]
        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            Verify_that_ther_are_no_errors_in_routemap();
            Check_duplicate_route_Names();
            Check_duplicate_description();
        }

        private void Verify_that_ther_are_no_errors_in_routemap()
        {
            siteMapBaseNodes.ShouldNotBeNull();
            siteMapBaseNodes[0].Description.ShouldNotBeSameAs("Error");
        }

        private void Check_duplicate_description()
        {
            var tempNodes = siteMapBaseNodes.GroupBy(x => x.Description)
                .Where(y => y.Count() > 1)
                .SelectMany(q => q)
                .ToList();

            tempNodes.ShouldBeEmpty();
        }

        private void Check_duplicate_route_Names()
        {
            var tempNodes = siteMapBaseNodes.GroupBy(x => x.Name)
                .Where(y => y.Count() > 1)
                .SelectMany(q => q)
                .ToList();

            tempNodes.ShouldBeEmpty();
        }
    }
}
