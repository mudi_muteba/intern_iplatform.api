﻿using iPlatform.Api.DTOs.Base.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acceptance.Tests.Api.CustomApp
{
    public class CustomAppTests
    {
        private readonly Connector _Connector;

        public CustomAppTests(Connector connector)
        {
            _Connector = connector;
        }

        public void run_all()
        {
            new WhenRetrievingIndividualByRegisterBddfy().Run(_Connector);
            new WhenRetrievingPolicyStatusByVerifyMemberBddfy().Run(_Connector);
            new WhenRetrievingPolicyInfoByMemberBddfy().Run(_Connector);
            new WhenRetrievingIndividualByIdNumberBddfy().Run(_Connector);
            new WhenUpdatingMemberDetailsBddfy().Run(_Connector);
        }
    }
}
