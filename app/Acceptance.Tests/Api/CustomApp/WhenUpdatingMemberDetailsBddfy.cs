﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.CustomApp;
using iPlatform.Api.DTOs.Individual;
using TestObjects.Mothers.Individual;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.CustomApp
{
    public class WhenUpdatingMemberDetailsBddfy : IBddfyTest
    {
        private CreateIndividualDto _createIndividualDto;
        private CustomAppResponseDto<List<UpdateMemberDetailsDto>> _updateMemberDetailsDtoResponse;

        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            POSTResponseDto<int> response = Connector.IndividualManagement.Individuals.CreateIndividual(_createIndividualDto);
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.IsSuccess.ShouldBeTrue();
        }

        public void When_action()
        {
            UpdateMemberDetailsSearchDto updateMemberDetailsSearchDto = new UpdateMemberDetailsSearchDto()
            {
                prmIDNumber = _createIndividualDto.IdentityNo,
                prmUpdatedFirstName = "TestName",
                prmUpdatedSurname = "TestSurname",
                prmUpdatedCellphone = "0742658633",
                prmUpdatedEmailAddress = "donotcall@donot.dont"
            };
            _updateMemberDetailsDtoResponse = Connector.CustomAppManagement.CustomApp.UpdateMemberDetails(updateMemberDetailsSearchDto);
        }

        public void Then_result()
        {
            _updateMemberDetailsDtoResponse.Success.ShouldBeTrue();
        }
    }
}
