﻿using System.Collections.Generic;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.CustomApp;
using iPlatform.Api.DTOs.Individual;
using TestObjects.Mothers.Individual;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.CustomApp
{
    public class WhenRetrievingIndividualByIdNumberBddfy : IBddfyTest
    {
        private CreateIndividualDto _createIndividualDto;
        private CustomAppResponseDto<List<GetMemberDetailsDto>> _getMemberDetailsResponse;

        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            POSTResponseDto<int> response = Connector.IndividualManagement.Individuals.CreateIndividual(_createIndividualDto);
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.IsSuccess.ShouldBeTrue();
        }

        public void When_action()
        {
            GetMemberDetailsSearchDto getMemberDetailsSearchDto = new GetMemberDetailsSearchDto()
            {
                prmIDNumber = _createIndividualDto.IdentityNo
            };
            _getMemberDetailsResponse = Connector.CustomAppManagement.CustomApp.GetMemberDetails(getMemberDetailsSearchDto);
        }

        public void Then_result()
        {
            //Member details
            _getMemberDetailsResponse.Success.ShouldBeTrue();
        }
    }    
}
