﻿using System.Collections.Generic;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.CustomApp;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Policy;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Policy;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.CustomApp
{
    public class WhenRetrievingIndividualByRegisterBddfy : IBddfyTest
    {
        private CreateIndividualDto _createIndividualDto;
        private CustomAppResponseDto<List<RegisterDto>> _registerResponse;
        private CreatePolicyHeaderDto _createPolicyHeaderDto;

        public Connector Connector { get; set; }
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            POSTResponseDto<int> response = Connector.IndividualManagement.Individuals.CreateIndividual(_createIndividualDto);
            int partyId = response.Response;
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.IsSuccess.ShouldBeTrue();

            response = null;
            _createPolicyHeaderDto = NewPolicyDtoObjectMother.ValidPolicyDto();
            _createPolicyHeaderDto.PartyId = partyId;
            response = Connector.PolicyHeaderManagement.PolicyHeaders.CreatePolicyHeader(_createPolicyHeaderDto);
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.IsSuccess.ShouldBeTrue();
        }

        public void When_action()
        {
            RegisterSearchDto registerSearchDto = new RegisterSearchDto()
            {
                prmIDNumber = _createIndividualDto.IdentityNo,
                prmFirstName = _createIndividualDto.FirstName,
                prmLastName = _createIndividualDto.Surname,
                prmEmailAddress = _createIndividualDto.ContactDetail.Email,
                prmMobileNumber = _createIndividualDto.ContactDetail.Cell,
                prmPolicyNumber = _createPolicyHeaderDto.PolicyNo
            };

            _registerResponse = Connector.CustomAppManagement.CustomApp.Register(registerSearchDto);
        }

        public void Then_result()
        {
            _registerResponse.Success.ShouldBeTrue();
        }
    }
}
