﻿using System.Collections.Generic;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.CustomApp;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Policy;
using TestObjects.Mothers.Individual;
using TestObjects.Mothers.Policy;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.CustomApp
{
    public class WhenRetrievingPolicyStatusByVerifyMemberBddfy : IBddfyTest
    {
        private CreateIndividualDto _createIndividualDto;
        private CreatePolicyHeaderDto _createPolicyHeaderDto;
        private CustomAppResponseDto<List<VerifyMemberDto>> _verifyMemberResponse;

        public Connector Connector { get; set; }
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createIndividualDto = NewIndividualDtoObjectMother.ValidIndividualDto();
            POSTResponseDto<int> response = Connector.IndividualManagement.Individuals.CreateIndividual(_createIndividualDto);
            int partyId = response.Response;
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.IsSuccess.ShouldBeTrue();

            response = null;
            _createPolicyHeaderDto = NewPolicyDtoObjectMother.ValidPolicyDto();
            _createPolicyHeaderDto.PartyId = partyId;
            response = Connector.PolicyHeaderManagement.PolicyHeaders.CreatePolicyHeader(_createPolicyHeaderDto);
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.IsSuccess.ShouldBeTrue();
        }

        public void When_action()
        {
            VerifyMemberSearchDto verifyMemberSearchDto = new VerifyMemberSearchDto
            {
                prmIDNumber = _createIndividualDto.IdentityNo,
                prmCellNumber = _createIndividualDto.ContactDetail.Cell,
                prmPolicyNumber = _createPolicyHeaderDto.PolicyNo
            };

            _verifyMemberResponse = Connector.CustomAppManagement.CustomApp.VerifyMember(verifyMemberSearchDto);
        }

        public void Then_result()
        {
            _verifyMemberResponse.Success.ShouldBeTrue();
        }
    }
}
