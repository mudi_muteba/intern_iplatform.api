﻿using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using TestObjects.Mothers.SettingsQuoteUploads;
using System.Linq;
namespace Acceptance.Tests.Api.SettingsQuoteUploads
{
    public class WhenSavingMultipleSettingsQuoteUploadBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private SaveSettingsQuoteUploadsDto _saveDto;
        private POSTResponseDto<bool> _response;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _saveDto = SettingsQuoteUploadObjectMother.ValidMultipleDto();
        }

        public void When_action()
        {
            _response = Connector.SettingsQuoteUploadManagement.SettingsQuoteUploads.SaveMultiple(_saveDto);
        }

        public void Then_result()
        {
            the_record_has_been_created();
            the_record_should_be_retrieved_by_channelid();
        }

        private void the_record_has_been_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }

        private void the_record_should_be_retrieved_by_channelid()
        {
            var response = Connector.SettingsQuoteUploadManagement.SettingsQuoteUploads.GetByChannelId(2);
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            Assert.True(response.Response.Results.Count() == 3);
        }
    }
}
