﻿using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestStack.BDDfy;
using Xunit.Extensions;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using TestObjects.Mothers.SettingsQuoteUploads;

namespace Acceptance.Tests.Api.SettingsQuoteUploads
{
    public class WhenUpdatingASettingsQuoteUploadBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private CreateSettingsQuoteUploadDto _createDto;
        private POSTResponseDto<int> _response;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createDto = SettingsQuoteUploadObjectMother.ValidDto();
        }

        public void When_action()
        {
            _response = Connector.SettingsQuoteUploadManagement.SettingsQuoteUploads.Create(_createDto);
        }

        public void Then_result()
        {
            the_record_has_been_created();
            the_record_is_updated();
            the_record_should_be_retrieve_by_id_and_validated();
        }

        private void the_record_has_been_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }

        private void the_record_is_updated()
        {
            var response = Connector.SettingsQuoteUploadManagement.SettingsQuoteUpload(_response.Response).Edit(GetEditDto());
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }
        private void the_record_should_be_retrieve_by_id_and_validated()
        {
            var response = Connector.SettingsQuoteUploadManagement.SettingsQuoteUpload(_response.Response).Get();
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.Response.SettingName.ShouldEqual("Test1");
            response.Response.SettingValue.ShouldEqual("Test2");
        }

        private EditSettingsQuoteUploadDto GetEditDto()
        {
            return new EditSettingsQuoteUploadDto
            {
                Id = _response.Response,
                ChannelId = _createDto.ChannelId,
                Environment = _createDto.Environment,
                ProductId = _createDto.ProductId,
                SettingName = "Test1",
                SettingValue = "Test2"
            };
        }
    }
}
