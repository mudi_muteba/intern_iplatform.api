﻿using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestStack.BDDfy;
using Xunit;
using Xunit.Extensions;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using TestObjects.Mothers.SettingsQuoteUploads;
using System.Linq;
namespace Acceptance.Tests.Api.SettingsQuoteUploads
{
    public class WhenCreatingASettingsQuoteUploadBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        private CreateSettingsQuoteUploadDto _createDto;
        private POSTResponseDto<int> _response;

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(g => Given_data())
                .When(w => w.When_action())
                .Then(t => t.Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _createDto = SettingsQuoteUploadObjectMother.ValidDto();
        }

        public void When_action()
        {
            _response = Connector.SettingsQuoteUploadManagement.SettingsQuoteUploads.Create(_createDto);
        }

        public void Then_result()
        {
            the_record_has_been_created();
            the_record_should_be_retrieve_by_id();
            the_record_should_be_retrieved_by_channelid();
            the_record_should_be_deleted();
        }

        private void the_record_has_been_created()
        {
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }

        private void the_record_should_be_retrieve_by_id()
        {
            var response = Connector.SettingsQuoteUploadManagement.SettingsQuoteUpload(_response.Response).Get();
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            response.Response.Channel.Id.ShouldEqual(1);
        }

        private void the_record_should_be_retrieved_by_channelid()
        {
            var response = Connector.SettingsQuoteUploadManagement.SettingsQuoteUploads.GetByChannelId(1);
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            Assert.True(response.Response.Results.Count() == 1);
        }

        private void the_record_should_be_deleted()
        {
            var response = Connector.SettingsQuoteUploadManagement.SettingsQuoteUpload(_response.Response).Delete();
            response.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }
    }
}
