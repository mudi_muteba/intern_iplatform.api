﻿using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.SettingsQuoteUploads
{
    public class SettingsQuoteUploadTests
    {
        private readonly Connector _Connector;

        public SettingsQuoteUploadTests(Connector connector)
        {
            _Connector = connector;
            string token = new ConnectorTestData(_Connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _Connector.SetToken(token);
        }

        public void run_all()
        {
            //Valid data tests
            new WhenCreatingASettingsQuoteUploadBddfy().Run(_Connector);
            new WhenUpdatingASettingsQuoteUploadBddfy().Run(_Connector);
            new WhenSavingMultipleSettingsQuoteUploadBddfy().Run(_Connector);
        }
    }
}
