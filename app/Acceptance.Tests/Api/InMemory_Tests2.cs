﻿using Acceptance.Tests.Api.Admin;
using Acceptance.Tests.Api.AssetBuilders;
using Acceptance.Tests.Api.Campaigns;
using Acceptance.Tests.Api.ChannelEvents;
using Acceptance.Tests.Api.Components.Headers;
using Acceptance.Tests.Api.CustomApp;
using Acceptance.Tests.Api.DisplaySetting;
using Acceptance.Tests.Api.DocumentManagement;
using Acceptance.Tests.Api.EmailCommunicationSettings;
using Acceptance.Tests.Api.Individual.AssetRiskItems;
using Acceptance.Tests.Api.Individual.Bank;
using Acceptance.Tests.Api.Individual.ProposalDefinitionQuestion;
using Acceptance.Tests.Api.Leads;
using Acceptance.Tests.Api.Lookups.VehicleGuide.Setting;
//using Acceptance.Tests.Api.MapVapQuestionDefinitionCover;
using Acceptance.Tests.Api.MapVapQuestionDefinitions;
using Acceptance.Tests.Api.Organizations;
using Acceptance.Tests.Api.OverrideRatingQuestion;
using Acceptance.Tests.Api.SettingsiRate;
using Acceptance.Tests.Api.SettingsQuoteUploads;
using Acceptance.Tests.Api.Users;
using Acceptance.Tests.Api.Users.Authentication;
using Acceptance.Tests.Api.Users.Discounts;
using Acceptance.Tests.PersistenceSpecifications;
using Acceptance.Tests.Queries.Escalations;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.Api
{
    public class InMemory_Tests2 : BaseTest
    {
        public InMemory_Tests2()
            : base(ApiUserObjectMother.AsAdmin())
        {
        }

        public override void Observe()
        {
        }

        [Observation]
        protected override void assert_all()
        {
            new ChannelEventTests(Connector).run_all();
            new VehicleGuideSettingTests(Connector).run_all();
            new SettingsiRateTests(Connector).run_all();
            new SettingsiPersonTests(Connector).run_all();
            new EmailCommunicationSettingTests(Connector).run_all();
            new SettingsQuoteUploadTests(Connector).run_all();
            new OrganizationTests(Connector).run_all();
            new MapVapQuestionDefinitionTests(Connector).run_all();
            //new OverrideRatingQuestionTests(Connector).run_all();
            //new MapVapQuestionDefinitionCoverTests(Connector).run_all();
            //new ComponentHeadersTest(Connector).run_all();
        }
    }
}