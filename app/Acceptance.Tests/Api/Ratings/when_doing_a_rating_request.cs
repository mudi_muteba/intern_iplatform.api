﻿using System;
using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Ratings.Request;
using iPlatform.Api.DTOs.Ratings.Response;
using RestSharp;
using Shared.Retry;
using TestHelper;
using TestObjects.Mothers.Ratings;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.Ratings
{
    public class when_doing_a_rating_request : BaseTest
    {
        private readonly POSTResponseDto<RatingResultDto> _ratingResponse;
        private readonly RatingRequestDto _request;

        public when_doing_a_rating_request() : base(ApiUserObjectMother.AsAdmin())
        {
            _request = RatingRequestObjectMother.FullRequest();
            new TestHelper.Helpers.Workflow().Start();
            _ratingResponse = Connector.Ratings.GetRating(_request);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            ratings_are_returned();
            information_about_the_product_is_returned();
            a_new_individual_is_created();
            a_proposal_is_created_for_the_rating_request();
            a_quote_is_created_for_the_rating_request();
        }
        
        private void ratings_are_returned()
        {
            // we are not to interested in what is returned, 
            // as long as something is returned with http status OK
            // an awefull lot of testing is done with the insurers to ensure
            // the rating provided by iRate is 100%. We are not testing
            // iRate, we are testing iPlatform calling iRate

            PrintDebug(_ratingResponse);

            _ratingResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _ratingResponse.IsSuccess.ShouldBeTrue();

            var ratingResult = _ratingResponse.Response;

            ratingResult.ShouldNotBeNull();

            ratingResult.Policies.ShouldNotBeEmpty();
        }

        private void information_about_the_product_is_returned()
        {
            var ratingResult = _ratingResponse.Response;

            ratingResult.ShouldNotBeNull();

            foreach (var policy in ratingResult.Policies)
            {
                policy.AboutProduct.ShouldNotBeNull();
                string.IsNullOrWhiteSpace(policy.AboutProduct.Code).ShouldBeFalse();
                string.IsNullOrWhiteSpace(policy.AboutProduct.Name).ShouldBeFalse();
                string.IsNullOrWhiteSpace(policy.AboutProduct.ProductType).ShouldBeFalse();

                policy.AboutProduct.Provider.ShouldNotBeNull();
                string.IsNullOrWhiteSpace(policy.AboutProduct.Provider.Code).ShouldBeFalse();
                string.IsNullOrWhiteSpace(policy.AboutProduct.Provider.Description).ShouldBeFalse();
                string.IsNullOrWhiteSpace(policy.AboutProduct.Provider.RegisteredName).ShouldBeFalse();
                string.IsNullOrWhiteSpace(policy.AboutProduct.Provider.TradingName).ShouldBeFalse();
                policy.AboutProduct.Provider.Id.ShouldNotEqual(0);

                policy.AboutProduct.Images.ShouldNotBeEmpty();
                var image = policy.AboutProduct.Images.FirstOrDefault();

                string.IsNullOrWhiteSpace(image.Url).ShouldBeFalse();

                var client = new RestClient(image.Url);
                var imageRequest = new RestRequest(Method.GET);
                var response = client.Execute(imageRequest);

                response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            }
        }

        private void a_new_individual_is_created()
        {
            var proposalResponse = Connector.Ratings.GetProposalByExternalReference(_request.Id.ToString());

            RepeatAction.While(() => proposalResponse.StatusCode != HttpStatusCode.OK)
                .RetryAfter(5.Seconds())
                .UpTo(2.Times())
                .Do(() => proposalResponse = Connector.Ratings.GetProposalByExternalReference(_request.Id.ToString()));

            proposalResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);

            var proposal = proposalResponse.Response;

            var individualResponse = Connector.IndividualManagement.Individual(proposal.PartyId).Get();

            individualResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }

        private void a_proposal_is_created_for_the_rating_request()
        {
            var proposalResponse = Connector.Ratings.GetProposalByExternalReference(_request.Id.ToString());

            RepeatAction.While(() => proposalResponse.StatusCode != HttpStatusCode.OK)
                .RetryAfter(5.Seconds())
                .UpTo(2.Times())
                .Do(() => proposalResponse = Connector.Ratings.GetProposalByExternalReference(_request.Id.ToString()));

            proposalResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }

        private void a_quote_is_created_for_the_rating_request()
        {
            var quoteResponse = Connector.Ratings.GetQuoteByExternalReference(_request.Id.ToString());

            RepeatAction.While(() => quoteResponse.StatusCode != HttpStatusCode.OK)
                .RetryAfter(5.Seconds())
                .UpTo(2.Times())
                .Do(() => quoteResponse = Connector.Ratings.GetQuoteByExternalReference(_request.Id.ToString()));

            quoteResponse.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }

        private void PrintDebug(POSTResponseDto<RatingResultDto> postResponseDto)
        {
            if(!postResponseDto.IsSuccess)
            {
                Console.WriteLine("--- FAILED TO GET RATING FROM iRATE. CHECK API LOGS");
                foreach (var error in postResponseDto.Errors)
                    Console.WriteLine(error.DefaultMessage);
            }

            if (postResponseDto.IsSuccess)
                foreach (var policy in postResponseDto.Response.Policies)
                    foreach (var item in policy.Items)
                        Console.WriteLine("Asset {0} with premium {1}", item.AssetNo, item.PremiumWithFees);
        }
    }
}