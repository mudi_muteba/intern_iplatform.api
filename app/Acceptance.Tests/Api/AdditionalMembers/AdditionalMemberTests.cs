using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.AdditionalMembers
{
    public class AdditionalMemberTests
    {
        private readonly Connector _connector;

        public AdditionalMemberTests(Connector connector)
        {
            _connector = connector;
            var token = new ConnectorTestData(_connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _connector.SetToken(token);
        }

        public void run_all()
        {

            new WhenDisablingAAdditionalmemberBddfy().Run(_connector);
            new WhenEditingAdditionalmemberBddfy().Run(_connector);
            new WhenRetrievingAdditionalmemberByIdBddfy().Run(_connector);
            new WhenCreatingANewAdditionamemberBddfy().Run(_connector);
            //new WhenRetrievingAdditionalmembersByProposaldefinitionidBddfy().Run(_connector); //This test does not seem to exit
        }
    }
}