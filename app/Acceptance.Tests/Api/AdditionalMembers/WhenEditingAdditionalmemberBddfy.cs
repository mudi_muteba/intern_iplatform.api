using System;
using System.Globalization;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using NHibernate.Util;
using TestHelper.Helpers;
using TestObjects.Mothers.AdditionalMembers;
using TestObjects.Mothers.Individual.ProposalDefinition;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.AdditionalMembers
{
    public class WhenEditingAdditionalmemberBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        private int _proposalHeaderId;
        private int _indivdualId;
        private POSTResponseDto<int> _proposalDefinitionResponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createAdditionalMemberDto = NewAdditionalMemberDtoObjectMother.ValidAdditionalMemberDto();

            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _proposalHeaderId = new ConnectorTestData(Connector).ProposalHeaders.Create(_indivdualId);
            var createProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_BUILDINGS(_proposalHeaderId);

            _proposalDefinitionResponse =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .CreateProposalDefinition(createProposalDefinition);
            createAdditionalMemberDto.ProposalDefinitionId = _proposalDefinitionResponse.Response;
            
            _response = Connector.AdditionalMemberManagement.AdditionalMembers.CreateAdditionalMember(createAdditionalMemberDto);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _response.Errors.Any().ShouldBeFalse();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var dto = NewAdditionalMemberDtoObjectMother.ValidEditAdditionalMemberDto();
            dto.ProposalDefinitionId = _proposalDefinitionResponse.Response;
            dto.Id = _response.Response;

            var result = Connector.AdditionalMemberManagement.AdditionalMember(_response.Response).EditAdditionalMember(dto);
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();

            var editresult = Connector.AdditionalMemberManagement.AdditionalMember(_response.Response).Get();
            editresult.Response.Surname.ShouldEqual(dto.Surname);
            editresult.Response.IdNumber.ShouldEqual(dto.IdNumber);
            editresult.Response.SumInsured.Value.ToString(CultureInfo.InvariantCulture).ShouldEqual("250.0");
            editresult.Response.DateOfBirth.Value.ShouldEqual(dto.DateOfBirth);


        }
    }
}