using System.Linq;
using System.Net;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestHelper.Helpers;
using TestObjects.Mothers.AdditionalMembers;
using TestObjects.Mothers.Individual.ProposalDefinition;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Api.AdditionalMembers
{
    public class WhenDisablingAAdditionalmemberBddfy : IBddfyTest
    {
        private POSTResponseDto<int> _response;
        private int _indivdualId;
        private int _proposalHeaderId;
        private POSTResponseDto<int> _proposalDefinitionResponse;
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var createAdditionalMemberDto = NewAdditionalMemberDtoObjectMother.ValidAdditionalMemberDto();

            _indivdualId = new ConnectorTestData(Connector).Individuals.Create();
            _proposalHeaderId = new ConnectorTestData(Connector).ProposalHeaders.Create(_indivdualId);
            var createProposalDefinition = NewProposalDefinitionDtoObjectMother.ValidCreateProposalDefinitionDto_BUILDINGS(_proposalHeaderId);

            _proposalDefinitionResponse =
                Connector.IndividualManagement.Individual(_indivdualId)
                    .Proposal(_proposalHeaderId)
                    .CreateProposalDefinition(createProposalDefinition);
            createAdditionalMemberDto.ProposalDefinitionId = _proposalDefinitionResponse.Response;

            _response = Connector.AdditionalMemberManagement.AdditionalMembers.CreateAdditionalMember(createAdditionalMemberDto);

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _response.Errors.Any().ShouldBeFalse();
            _response.StatusCode.ShouldEqual(HttpStatusCode.OK);
            _response.IsSuccess.ShouldBeTrue();

            var result = Connector.AdditionalMemberManagement.AdditionalMember(_response.Response).Disable();
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
            result.IsSuccess.ShouldBeTrue();
        }
    }
}