using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using TestStack.BDDfy;

namespace Acceptance.Tests.Api.CoverLinks
{
    public class WhenCreatingCoverLinkBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {

        }

        public void When_action()
        {
        }

        public void Then_result()
        {

        }
    }
}