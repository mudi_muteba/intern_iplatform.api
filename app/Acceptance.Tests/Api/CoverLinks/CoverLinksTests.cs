using Acceptance.Tests.Api.AdditionalMembers;
using iPlatform.Api.DTOs.Base.Connector;
using TestHelper.Helpers;
using TestObjects.Mothers.Users;

namespace Acceptance.Tests.Api.CoverLinks
{
    public class CoverLinksTests
    {
        private readonly Connector _connector;

        public CoverLinksTests(Connector connector)
        {
            _connector = connector;
            var token = new ConnectorTestData(_connector).Authentication.Authenticate(AuthenticationObjectMother.RootUser());
            _connector.SetToken(token);
        }

        public void run_all()
        {
            //new WhenCreatingCoverLinkBddfy().Run(_connector);
            //new WhenRetrievingCoverLinkByChannelIdBddfy().Run(_connector);
            //new WhenRetrievingAdditionalmembersByProposaldefinitionidBddfy().Run(_connector); //This test does not seem to exit
        }
    }
}