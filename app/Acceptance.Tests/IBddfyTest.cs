using iPlatform.Api.DTOs.Base.Connector;

namespace Acceptance.Tests
{
    public interface IBddfyTest
    {
        Connector Connector { get; set; }

        void Run(Connector connector);

        void Given_data();
        void When_action();
        void Then_result();
    }
}