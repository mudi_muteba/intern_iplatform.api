﻿using TestObjects.Mothers.Router;
using EasyNetQ;
using TestObjects.Builders;
using Workflow.Messages;
using Workflow.Publisher;
using Workflow.QuoteAcceptance.Domain;
using Workflow.QuoteAcceptance.Transferer;
using Xunit.Extensions;

namespace Acceptance.Tests.Engine.Workflow
{
    public class when_engine_receives_dispatched_king_price_lead_from_router : Specification
    {
       
        private IWorkflowExecutionMessage _message;
        private readonly IBus _bus;
        private readonly IConfigureTransferOfAcceptedQuote _configuration = new TransferLeadConfiguration();
        //private IEnumerable<ITransferLead> _leadDestination;

        public when_engine_receives_dispatched_king_price_lead_from_router()
        {
            _bus = BusBuilder.CreateEngineBus();
            _message = WorkflowExecutionMessageBuilder.ForLeadTransferralMessage(RouteAcceptedQuoteTaskMother.ForKingPrice());
        }

        public override void Observe()
        {
           _bus.Publish(_message);
        }

        [Observation(Skip = "Do not run automatically")] //
        public void then_king_price_lead_should_exist_on_execution_queue()
        {
            true.ShouldBeFalse();
        }
    }
}
