﻿using System;
using Domain.QuoteAcceptance;
using Domain.QuoteAcceptance.Extensions;
using Domain.QuoteAcceptance.Leads;
using Domain.QuoteAcceptance.Metadata;
using Domain.QuoteAcceptance.Metadata.Shared;
using EasyNetQ;
using Workflow.Messages;
using Workflow.Publisher;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Extensions;
using Workflow.QuoteAcceptance.Domain.Infrastructure.Messages;
using Xunit.Extensions;
using TestObjects.Mothers.EmailSetting;

namespace Acceptance.Tests.Router.Communication
{
    public class when_creating_communication_message_for_exception_failure : Specification
    {
        private readonly IWorkflowRoutingMessage _message;
        private LeadTransferEmailMessage _leadTransferEmail;
        private readonly IBus _bus;

        public when_creating_communication_message_for_exception_failure()
        {
            _bus = BusBuilder.CreateRouterBus();
            _message = new WorkflowRoutingMessage();

        }

        public override void Observe()
        {
            _leadTransferEmail = LeadTransferMessage.CreateErrorMailMessage("exeception message", new Exception("This is a unit test exception").Print(), Guid.Empty, InsurerName.Dotsure.Name(), Guid.Empty, new CommunicationMetadata(new LeadTransferralCommsMessage
            {
                Address = "ootam@iplatform.co.za",
                Subject = "TEST QUOTE LEAD",
                TemplateName = "LeadUploadTemplate",
                MessageData = new CommsMessageNullData(),
                AppSettingsEmailSetting = NewEmailSettingDtoObjectMother.ValidEmailSetting()
            }));
            _message.AddMessage(_leadTransferEmail);
            _bus.Publish(_message);
        }

        [Observation(Skip = "To be fixed")]
        public void then_communction_meta_data_should_be_valid()
        {
            var data = _leadTransferEmail.GetCommunicationMetadata();
            data.ShouldBeType<LeadTransferralCommsMessage>();
        }
    }
}
