﻿using Domain.Base.Workflow;
using EasyNetQ;
using TestObjects.Builders;
using TestObjects.Mothers.Router;
using Workflow.Messages;
using Workflow.Publisher;
using Xunit.Extensions;

namespace Acceptance.Tests.Router.Workflow
{
    public class when_dispatching_regent_lead_to_router : Specification
    {
        private readonly ICreateWorkflowRoutingMessage<RouteAcceptedQuoteTask> _routeFactory;
        private readonly RouteAcceptedQuoteTask _task;
        private IWorkflowRoutingMessage _message;
        private readonly IBus _bus;

        public when_dispatching_regent_lead_to_router()
        {
            _bus = BusBuilder.CreateRouterBus();
            _task = RouteAcceptedQuoteTaskMother.ForRegent();
            _routeFactory = QuoteAcceptanceRouterFactoryBuilder.CreateWorkflowRRouteAcceptedQuoteTask();
        }


        public override void Observe()
        {
            _message = _routeFactory.Create(_task);
            _bus.Publish(_message);
        }

        [Observation(Skip = "To be fixed")] //(Skip = "Do not run automatically")
        public void then_regent_message_for_router_should_be_published()
        {
            true.ShouldBeFalse();
        }
    }
}
