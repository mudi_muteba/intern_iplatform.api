﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Domain.Campaigns;
using Domain.Escalations;
using Domain.Individuals;
using Domain.Leads;
using Domain.Party.Addresses;
using Domain.Users;
using iPlatform.Api.DTOs.Escalations;
using iPlatform.Api.DTOs.Leads.Campaign;
using iPlatform.Enums.Escalations;
using iPlatform.Enums.Workflows;
using MasterData;
using TestHelper;
using TestHelper.TestEntities;
using Xunit.Extensions;

namespace Acceptance.Tests.Escalations
{
    public class when_escalation_event_is_raised : BaseTest
    {
        public when_escalation_event_is_raised() : base(ApiUserObjectMother.AsAdmin())
        {
            var plan = new EscalationPlan("Plan 1", EventType.LeadCallBack.ToString());
            var step = plan.AddEscalationPlanStep("Step 1", 1, DelayType.Interval, IntervalType.Minutes, 1);
            step.AddWorkflowMessage(WorkflowMessageType.Email.ToString());

            var address = new Address();
            var individual = new Individual { FirstName = "Brian", Surname = "Baker", Channel = Channels.Channel1 };
            address.Party = individual;
            
            var lead = new Lead(individual);
            var manager = new User("ootam@iplatform.co.za", "123456", new List<int> { 1 }, 1);
            var campaign = new Campaign {Managers = new HashSet<CampaignManager> {new CampaignManager(manager)}, Channel = Channels.Channel1 };
            var campaignLeadBucket = new CampaignLeadBucket ();
            campaignLeadBucket.Agent = Users.Root;
            campaignLeadBucket.Campaign = campaign;
            campaignLeadBucket.Lead = lead;

            plan.TagWithCampaign(campaign);
            plan.TagWithChannel(Channels.Channel1);

            SaveAndFlush(_testData.EmailCommunications.ToArray());
            SaveAndFlush(manager, campaign, plan, lead, campaignLeadBucket);
            SaveAndFlush(EventEnumTypes.LeadCallBack);

            var response = Connector.LeadManagement.Lead.UpdateLeadStatus(new UpdateLeadStatusDto
            {
                Id = lead.Id,
                CampaignId = campaign.Id,
                LeadCallCentreCodeId = LeadCallCentreCodes.CallBackLater.Id,
                CallCentreCode = LeadCallCentreCodes.CallBackLater,
                CallBackDate = DateTime.UtcNow.Add(TimeSpan.FromSeconds(5))
            });

            TestHelper.Helpers.Engine.Start();
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip ="to be fixed")]
        protected override void assert_all()
        {
            should();
        }

        private void should()
        {
            Thread.Sleep(TimeSpan.FromSeconds(80));
            var test = Connector.EscalationManagement.Escalations.SearchHistory(new EscalationHistorySearchDto { PageNumber = 1, PageSize = 20 });
            test.Response.Results.Any(x => x.EscalationWorkflowMessageStatus == EscalationWorkflowMessageStatus.Complete).ShouldBeTrue();
        }
    }
}