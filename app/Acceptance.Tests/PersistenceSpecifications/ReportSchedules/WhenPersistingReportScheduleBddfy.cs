using System.Collections.Generic;
using Domain.Campaigns;
using Domain.Reports.Base;
using Domain.ReportSchedules;
using Domain.Users;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums.ReportSchedules;
using MasterData;
using NHibernate;
using TestHelper.Helpers;
using TestStack.BDDfy;

namespace Acceptance.Tests.PersistenceSpecifications.ReportSchedules
{
    public class WhenPersistingReportScheduleBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        protected ISession Session;

        public WhenPersistingReportScheduleBddfy(ISession session)
        {
            Session = session;
        }
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            var user = new User();
            var campaign = new Campaign();
            var r = new Report();
            var rp = new ReportParam();
            var rs = new ReportSchedule {Name = "Test"};
            Session.Save(user);
            Session.Save(campaign);
            Session.Save(r);
            Session.Save(rs);
            Session.Save(rp);
            Session.Flush();
            new PersistenceSpecification<ReportSchedule>(Session, new CustomEqualityComparer())
                .CheckProperty(c => c.Name, "Name")
                //.CheckReference(c => c.Format, ExportTypes.CSV)
                //.CheckReference(c => c.Frequency, FrequencyTypes.Daily)
                //.CheckProperty(c => c.PeriodType, ReportSchedulePeriodType.CurrentMonth)
                .CheckProperty(c => c.WeekDay, "Wed")
                .CheckProperty(c => c.MonthDay, 1)
                //.CheckProperty(c => c.Time, new DateTime?(DateTime.UtcNow))
                .CheckProperty(c => c.Message, "Message")
                .CheckProperty(c => c.ShouldZip, true)
                //.CheckProperty(c => c.CreatedOn, DateTime.UtcNow)
                .CheckProperty(c => c.Owner, user)
                .CheckList(c => c.Reports, new List<ReportScheduleReport> {new ReportScheduleReport(rs, r)})
                .CheckList(c => c.Recipients, new List<ReportScheduleRecipient> {new ReportScheduleRecipient(rs, user)})
                .CheckList(c => c.Campaigns, new List<ReportScheduleCampaign> {new ReportScheduleCampaign(rs, campaign) })
                .CheckList(c => c.ReportScheduleParams, new List<ReportScheduleParam> { new ReportScheduleParam(rp, "Value") })
                .VerifyTheMappings(rs);
        }
    }
}