﻿using System;
using System.IO;
using Domain.WorkflowRetries;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using Newtonsoft.Json;
using NHibernate;
using TestHelper.Helpers;
using TestStack.BDDfy;

namespace Acceptance.Tests.PersistenceSpecifications.WorkflowRetries
{
    public class WhenPersistingWorkflowRetryBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        protected ISession Session;

        public WhenPersistingWorkflowRetryBddfy(ISession session)
        {
            Session = session;
        }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            var json = JsonConvert.SerializeObject(new { Name = "John", Surname = "Doe" });
            byte[] bytes;
            using (var memStream = new MemoryStream())
            {
                using (var sw = new StreamWriter(memStream))
                {
                    sw.Write(json);
                }
                bytes = memStream.GetBuffer();
            }

            new PersistenceSpecification<WorkflowRetryLog>(Session, new CustomEqualityComparer())
                .CheckProperty(c => c.CorrelationId, Guid.NewGuid())
                .CheckProperty(c => c.RetryCount, 1)
                .CheckProperty(c => c.RetryLimit, 3)
                .CheckProperty(c => c.PublishDate, DateTime.UtcNow.Date)
                .CheckProperty(c => c.MessageDelay, new TimeSpan(0, 0, 5).ToString())
                .CheckProperty(c => c.FuturePublishDate, DateTime.UtcNow.Date)
                .CheckProperty(c => c.Message, bytes)
                .VerifyTheMappings();
        }
    }
}