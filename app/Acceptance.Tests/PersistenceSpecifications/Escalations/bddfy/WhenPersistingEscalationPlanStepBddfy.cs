using System;
using Domain.Escalations;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums.Escalations;
using iPlatform.Enums.Workflows;
using NHibernate;
using TestHelper.Helpers;
using TestStack.BDDfy;

namespace Acceptance.Tests.PersistenceSpecifications.Escalations.bddfy
{
    public class WhenPersistingEscalationPlanStepBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        protected ISession Session;

        public WhenPersistingEscalationPlanStepBddfy(ISession session)
        {
            Session = session;
        }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            //SaveAndFlush(EventEnumTypes.LeadCallBack, WorkflowMessageEnumTypes.LeadTransferal);
            var escalationPlan = new EscalationPlan("Brolink plan", EventType.LeadCallBack.ToString());
            Session.Save(escalationPlan);
            Session.Flush();
            var step = new EscalationPlanStep(escalationPlan, "Step 1", 1, DelayType.Interval, IntervalType.Days, 365, DateTime.UtcNow);
            step.AddWorkflowMessage(WorkflowMessageType.LeadTransferal.ToString());

            new PersistenceSpecification<EscalationPlanStep>(Session, new CustomEqualityComparer())
                .CheckProperty(c => c.Name, step.Name)
                .CheckProperty(c => c.Sequence, step.Sequence)
                .CheckProperty(c => c.DelayType, DelayType.Interval)
                .CheckProperty(c => c.IntervalType, IntervalType.Days)
                .CheckProperty(c => c.IntervalDelay, 365)
                .CheckProperty(c => c.DateTimeDelay, step.DateTimeDelay.Value.Date)
                .CheckReference(c => c.EscalationPlan, step.EscalationPlan)
                .CheckProperty(c => c.IsDeleted, false)
                //.CheckList(c => c.EscalationPlanStepWorkflowMessages, step.EscalationPlanStepWorkflowMessages)
                .VerifyTheMappings(step);
        }
    }
}