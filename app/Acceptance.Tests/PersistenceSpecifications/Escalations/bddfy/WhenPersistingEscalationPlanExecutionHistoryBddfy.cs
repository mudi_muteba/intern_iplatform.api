using System;
using Domain.Escalations;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums.Escalations;
using iPlatform.Enums.Workflows;
using NHibernate;
using TestHelper.Helpers;
using TestStack.BDDfy;

namespace Acceptance.Tests.PersistenceSpecifications.Escalations.bddfy
{
    public class WhenPersistingEscalationPlanExecutionHistoryBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        protected ISession Session;

        public WhenPersistingEscalationPlanExecutionHistoryBddfy(ISession session)
        {
            Session = session;
        }
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            var escalationPlan = new EscalationPlan("Plan 1", EventType.LeadCallBack.ToString());
            var escalationPlanStep = new EscalationPlanStep(escalationPlan, "Step 1", 1, DelayType.Interval, IntervalType.Days, 365, DateTime.UtcNow);
            var escalationPlanStepWorkflowMessage = new EscalationPlanStepWorkflowMessage(escalationPlanStep, WorkflowMessageType.Sms.ToString());
            Session.Save(escalationPlan);
            Session.Save(escalationPlanStep);
            Session.Save(escalationPlanStepWorkflowMessage);

            new PersistenceSpecification<EscalationPlanExecutionHistory>(Session, new CustomEqualityComparer())
                .CheckReference(c => c.EscalationPlanStepWorkflowMessage, escalationPlanStepWorkflowMessage)
                //.CheckProperty(c => c.User, 1)
                //.CheckProperty(c => c.IsDeleted, true)
                .CheckProperty(c => c.EscalationWorkflowMessageStatus, EscalationWorkflowMessageStatus.Complete)
                .VerifyTheMappings();
        }
    }
}