using System;
using Domain.Admin;
using Domain.Escalations;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums.Escalations;
using iPlatform.Enums.Workflows;
using MasterData;
using NHibernate;
using TestHelper.Helpers;
using TestStack.BDDfy;

namespace Acceptance.Tests.PersistenceSpecifications.Escalations.bddfy
{
    public class WhenPersistingEscalationPlanBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        protected ISession Session;

        public WhenPersistingEscalationPlanBddfy(ISession session)
        {
            Session = session;
        }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            var channel = new Channel(105, true, Guid.NewGuid(), DateTime.UtcNow, DateTime.UtcNow, true);

            channel.Country = Countries.SouthAfrica;
            //Session.Save(EventEnumTypes.LeadCallBack);
            Session.Save(channel);
            Session.Flush();
            var plan = new EscalationPlan("Brolink plan", EventType.LeadCallBack.ToString());
            plan.AddEscalationPlanStep("Step 1", 1, DelayType.Interval, IntervalType.Days, 365);
            plan.AddEscalationPlanStep("Step 2", 1, DelayType.Interval, IntervalType.Days, 365);
            new PersistenceSpecification<EscalationPlan>(Session, new CustomEqualityComparer())
                .CheckProperty(c => c.Name, plan.Name)
                .CheckProperty(c => c.IsDeleted, false)
                //.CheckList(c => c.EscalationPlanSteps, plan.EscalationPlanSteps)
                .VerifyTheMappings(plan);
        }
    }
}