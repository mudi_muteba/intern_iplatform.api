using System;
using System.Linq;
using Domain.Admin;
using Domain.Escalations;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums.Workflows;
using MasterData;
using NHibernate;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.PersistenceSpecifications.Escalations
{
    public class WhenTaggingEscalationPlanWithChannelBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }       
        protected ISession Session;
        public WhenTaggingEscalationPlanWithChannelBddfy(ISession session)
        {
            Session = session;
        }
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            var channel = new Channel(107, true, Guid.NewGuid(), DateTime.UtcNow, DateTime.UtcNow, true);
            var escalationPlan = new EscalationPlan("Name", EventType.LeadCallBack.ToString());
            escalationPlan.TagWithChannel(channel);
            escalationPlan.TagWithChannel(channel);
            Session.Save(channel);
           var id = Session.Save(escalationPlan);
            Session.Flush();


            Session.Get<EscalationPlan>(id).ChannelTags.ToList().Count.ShouldEqual(1);
        }
    }
}