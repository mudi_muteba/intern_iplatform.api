using Acceptance.Tests.PersistenceSpecifications.bddfy;
using Acceptance.Tests.PersistenceSpecifications.Escalations;
using Acceptance.Tests.PersistenceSpecifications.Escalations.bddfy;
using Acceptance.Tests.PersistenceSpecifications.OverrideRatingQuestions;
using Acceptance.Tests.PersistenceSpecifications.Relationships;
using Acceptance.Tests.PersistenceSpecifications.ReportSchedules;
using Acceptance.Tests.PersistenceSpecifications.WorkflowRetries;
using Castle.Windsor;
using iPlatform.Api.DTOs.Base.Connector;
using NHibernate;

namespace Acceptance.Tests.PersistenceSpecifications
{
    public class PersistenceSpecificationsTests
    {
        private readonly Connector _connector;
        private readonly ISession _session;
        private readonly WindsorContainer _container;

        public PersistenceSpecificationsTests(Connector connector, ISession session, WindsorContainer container)
        {
            _connector = connector;
            _session = session;
            _container = container;
        }

        public void run_all()
        {
            //new WhenPersistingAUserBddfy(_session).Run(_connector);
            //new WhenPersistingChannelBddfy(_session).Run(_connector);
            //new WhenPersistingChannelEventBddfy(_session).Run(_connector);
            //new WhenPersistingChannelEventTaskBddfy(_session).Run(_connector);
            //new WhenPersistingChannelReferenceBddfy(_session).Run(_connector);
            //new WhenPersistingOrganizationBddfy(_session).Run(_connector);
            //new WhenPersistingUserIndividualBddfy(_session).Run(_connector);
            //new WhenPersistingEscalationPlanBddfy(_session).Run(_connector);
            //new WhenPersistingEscalationPlanStepBddfy(_session).Run(_connector);
            //new WhenPersistingEscalationPlanExecutionHistoryBddfy(_session).Run(_connector);
            //new WhenPersistingEscalationPlanStepWorkflowMessageBddfy(_session).Run(_connector);
            //new WhenTaggingEscalationPlanWithChannelBddfy(_session).Run(_connector);
            //new WhenPersistingWorkflowRetryBddfy(_session).Run(_connector);
            //new WhenPersistingRelationshipBddfy(_session).Run(_connector);
            //new WhenPersistingReportScheduleBddfy(_session).Run(_connector);

            new WhenPersistingOverrideRatingQuestionBddfy(_session).Run(_connector);
        }
    }
}