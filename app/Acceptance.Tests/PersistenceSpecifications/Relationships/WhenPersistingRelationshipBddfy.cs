using Domain.Party;
using Domain.Party.Relationships;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using NHibernate;
using TestHelper.Helpers;
using TestStack.BDDfy;

namespace Acceptance.Tests.PersistenceSpecifications.Relationships
{
    public class WhenPersistingRelationshipBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        protected ISession Session;

        public WhenPersistingRelationshipBddfy(ISession session)
        {
            Session = session;
        }
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            new PersistenceSpecification<Relationship>(Session, new CustomEqualityComparer())
                .CheckProperty(c => c.MasterId, 1)
                .CheckReference(c => c.Party, new Party())
                .CheckReference(c => c.ChildParty, new Party())
                .VerifyTheMappings();
        }
    }
}