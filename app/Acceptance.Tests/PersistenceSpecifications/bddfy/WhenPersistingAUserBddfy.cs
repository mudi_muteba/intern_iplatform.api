using System;
using System.Collections.Generic;
using Domain.Base.Encryption;
using Domain.Individuals;
using Domain.Users;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Api.DTOs.Individual;
using NHibernate;
using TestHelper.Helpers;
using TestStack.BDDfy;

namespace Acceptance.Tests.PersistenceSpecifications.bddfy
{
    public class WhenPersistingAUserBddfy : IBddfyTest
    {
        protected ISession Session;
        private string _hash;
        private User _user;
        private UserLogin _userLogin;

        public WhenPersistingAUserBddfy(ISession session)
        {
            Session = session;
        }

        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _hash = PasswordHash.CreateHash("password");
             
            _user = new User("UserName", _hash, new List<int>());
            _userLogin = new UserLogin(null, "127.0.0.1");
            _user.Login = _userLogin;
            var createIndividualDto = new CreateIndividualDto { Surname = "Surname", FirstName = "FirstName", DisplayName = "when_persisting_a_user" };
            createIndividualDto.SetContext(new DtoContext(_user.Id, _user.UserName, string.Empty));

        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            var propertyValue = Guid.NewGuid();
            new PersistenceSpecification<User>(Session, new CustomEqualityComparer())
                .CheckProperty(c => c.UserName, "user")
                .CheckProperty(c => c.Password, _hash)
                .CheckProperty(c => c.Tokens.PasswordResetToken, propertyValue)
                .CheckProperty(c => c.Status, new UserStatus())
                .CheckProperty(c => c.Login, _userLogin)
                .CheckList(c => c.UserIndividuals, new HashSet<UserIndividual>(new[] { new UserIndividual(_user, new Individual() { Surname = "Surname", FirstName = "FirstName", DisplayName = "when_persisting_a_user" }), }))
                .VerifyTheMappings(_user);

        }
    }
}