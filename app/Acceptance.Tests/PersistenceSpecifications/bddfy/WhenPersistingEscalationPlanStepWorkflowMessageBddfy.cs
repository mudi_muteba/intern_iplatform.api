using Domain.Escalations;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using NHibernate;
using TestHelper.Helpers;
using TestStack.BDDfy;

namespace Acceptance.Tests.PersistenceSpecifications.bddfy
{
    public class WhenPersistingEscalationPlanStepWorkflowMessageBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        protected ISession Session;

        public WhenPersistingEscalationPlanStepWorkflowMessageBddfy(ISession session)
        {
            Session = session;
        }
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            //var workflowMessageEnumType = WorkflowMessageEnumTypes.Email;
            //SaveAndFlush(workflowMessageEnumType);
            new PersistenceSpecification<EscalationPlanStepWorkflowMessage>(Session, new CustomEqualityComparer())
                //.CheckReference(c => c.WorkflowMessageEnumType, WorkflowMessageEnumTypes.Email)
                .CheckProperty(c => c.IsDeleted, false)
                .VerifyTheMappings();

        }
    }
}