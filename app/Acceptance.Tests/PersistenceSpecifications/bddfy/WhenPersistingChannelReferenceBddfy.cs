﻿using System;
using Domain.Admin;
using Domain.Users.Events;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums.Workflows;
using NHibernate;
using TestHelper.Helpers;
using TestStack.BDDfy;
using Domain.Admin.ChannelEvents;

namespace Acceptance.Tests.PersistenceSpecifications.bddfy
{
    public class WhenPersistingChannelReferenceBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        protected ISession Session;
        private ChannelEvent _channelEvent;
        private Channel _channel;

        public WhenPersistingChannelReferenceBddfy(ISession session)
        {
            Session = session;
        }
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            _channel = new Channel(102,true, Guid.NewGuid(), DateTime.UtcNow, DateTime.UtcNow, true);
            _channelEvent = new ChannelEvent(_channel, typeof(UserCreatedEvent).Name);
            _channelEvent.SetTask(WorkflowMessageType.Sms);
            //session.Save(channel);
            //session.Flush();
        }

        public void When_action()
        {
        }

        public void Then_result()
        {



            new PersistenceSpecification<Channel>(Session, new CustomEqualityComparer())
                //.CheckList(c => c.ChannelConfigurations, new[] { channelEvent })
                .VerifyTheMappings(_channel);
        }
    }
}