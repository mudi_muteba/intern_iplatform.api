using Domain.Admin;
using Domain.Admin.ChannelEvents;
using Domain.Individuals;
using Domain.Users;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using NHibernate;
using TestHelper.Helpers;
using TestStack.BDDfy;

namespace Acceptance.Tests.PersistenceSpecifications.bddfy
{
    public class WhenPersistingUserIndividualBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        protected ISession Session;
        private ChannelEvent _channelEvent;
        private Channel _channel;

        public WhenPersistingUserIndividualBddfy(ISession session)
        {
            Session = session;
        }
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            new PersistenceSpecification<UserIndividual>(Session, new CustomEqualityComparer())
                .CheckReference(x => x.Individual, new Individual())
                .CheckReference(x => x.User, new User())
                .VerifyTheMappings();

        }
    }
}