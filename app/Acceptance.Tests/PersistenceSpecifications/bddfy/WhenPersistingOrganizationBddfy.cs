using System;
using Domain.Admin;
using Domain.Organizations;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using NHibernate;
using TestHelper.Helpers;
using TestHelper.TestEntities;
using TestStack.BDDfy;
using Domain.Admin.ChannelEvents;

namespace Acceptance.Tests.PersistenceSpecifications.bddfy
{
    public class WhenPersistingOrganizationBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        protected ISession Session;
        private ChannelEvent _channelEvent;
        private Channel _channel;

        public WhenPersistingOrganizationBddfy(ISession session)
        {
            Session = session;
        }
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            _channel = new Channel(103, true, Guid.NewGuid(), DateTime.UtcNow, DateTime.UtcNow, true);

            Session.Save(_channel);
            Session.Flush();

            new PersistenceSpecification<Organization>(Session, new CustomEqualityComparer())
                .VerifyTheMappings(Organizations.Organization1);

        }
    }
}