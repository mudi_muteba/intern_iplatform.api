using System;
using Domain.Admin;
using Domain.Users.Events;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums.Workflows;
using NHibernate;
using TestHelper.Helpers;
using TestStack.BDDfy;
using Domain.Admin.ChannelEvents;

namespace Acceptance.Tests.PersistenceSpecifications.bddfy
{
    public class WhenPersistingChannelEventTaskBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        protected ISession Session;
        private ChannelEvent _channelEvent;

        public WhenPersistingChannelEventTaskBddfy(ISession session)
        {
            Session = session;
        }
        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
            var channel = new Channel(101,true, Guid.NewGuid(), DateTime.UtcNow, DateTime.UtcNow, true);
            _channelEvent = new ChannelEvent(channel, typeof(UserCreatedEvent).Name);
            _channelEvent.SetTask(WorkflowMessageType.Sms);

            Session.Save(channel);
            Session.Flush();
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            new PersistenceSpecification<ChannelEventTask>(Session, new CustomEqualityComparer())
                .CheckProperty(x => x.TaskName, WorkflowMessageType.Sms)
                .CheckReference(c => c.ChannelEvent, _channelEvent)
                .VerifyTheMappings();

        }
    }
}