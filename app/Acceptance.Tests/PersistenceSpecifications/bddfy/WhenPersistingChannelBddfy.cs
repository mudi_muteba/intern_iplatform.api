using System;
using Domain.Admin;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using NHibernate;
using TestStack.BDDfy;

namespace Acceptance.Tests.PersistenceSpecifications.bddfy
{
    public class WhenPersistingChannelBddfy : IBddfyTest
    {
        protected ISession Session;

        public WhenPersistingChannelBddfy(ISession session)
        {
            Session = session;
        }

        public Connector Connector { get; set; }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            new PersistenceSpecification<Channel>(Session)
                .CheckProperty(c => c.IsActive, true)
                .CheckProperty(c => c.SystemId, Guid.NewGuid())
                .CheckProperty(c => c.ActivatedOn, DateTime.UtcNow.Date)
                .CheckProperty(c => c.DeactivatedOn, DateTime.UtcNow.Date)
                .CheckProperty(c => c.IsDefault, true)
                .CheckProperty(c => c.PasswordStrengthEnabled, true)
                .VerifyTheMappings();
        }
    }
}