﻿using System;
using Domain.Admin;
using Domain.Campaigns;
using Domain.Imports.Leads;
using Domain.Users;
using FluentNHibernate.Testing;
using TestHelper;
using TestHelper.Helpers;
using Xunit.Extensions;
using MasterData;

namespace Acceptance.Tests.PersistenceSpecifications.Leads
{
    public class when_persisting_lead_upload_header : BaseTest
    {
        public override void Observe()
        {

        }

        [Observation(Skip = "no longer used")]
        protected override void assert_all()
        {
            should_persist();
        }
        
        private void should_persist()
        {
            //var campaign = new Campaign { Id = 1 };
            //SaveAndFlush(campaign);

            new PersistenceSpecification<IndividualUploadHeader>(Session, new CustomEqualityComparer())
                .CheckProperty(x => x.LeadImportReference, Guid.NewGuid())
                .CheckReference(x => x.Channel, new Channel(1))
                .CheckProperty(x => x.FileName, "FileName")
                .CheckReference(x => x.CreatedBy, new User(1){ UserName = "", Password = ""})
                .CheckProperty(x => x.DateCreated, DateTime.UtcNow.Date)
                .CheckProperty(x => x.DateUpdated, DateTime.UtcNow.Date)
                .CheckProperty(x => x.FileRecordCount, 1000)
                .CheckProperty(x => x.NewCount, 900)
                .CheckProperty(x => x.ExistingCount, 10)
                .CheckProperty(x => x.DuplicateFileRecordCount, 80)
                .CheckProperty(x => x.FailureCount, 10)
                .CheckProperty(x => x.LeadImportStatus, LeadImportStatuses.Submitted)
                .VerifyTheMappings();
        }
    }
}