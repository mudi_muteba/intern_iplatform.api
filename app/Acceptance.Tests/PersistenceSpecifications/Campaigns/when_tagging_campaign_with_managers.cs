﻿using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Campaigns;
using Domain.Users;
using MasterData;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.PersistenceSpecifications.Campaigns
{
    public class when_tagging_campaign_with_managers : BaseTest
    {
        public override void Observe()
        {
            
        }

        [Observation(Skip = "Speed up the build for testing")]
        protected override void assert_all()
        {
            var user = new User();
            var channel = new Channel(1);
            SaveAndFlush(channel);
            SaveAndFlush(user);
            user.Channels = new List<UserChannel> { new UserChannel(user, channel) };
            user.AuthorisationGroups = new List<UserAuthorisationGroup> { new UserAuthorisationGroup(channel, user, AuthorisationGroups.CallCentreManager) };
            SaveAndFlush(user);

            var campaign = new Campaign();
            campaign.TagWithManager(user);
            campaign.TagWithManager(user);
            SaveAndFlush(campaign);

            Session.Get<Campaign>(1).Managers.ToList().Count.ShouldEqual(1);
        }
    }
}