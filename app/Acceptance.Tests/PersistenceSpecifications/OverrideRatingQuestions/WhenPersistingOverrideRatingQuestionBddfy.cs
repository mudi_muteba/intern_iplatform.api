﻿using System;
using System.Collections.Generic;
using Domain.OverrideRatingQuestion;
using Domain.WorkflowRetries;
using FluentNHibernate.Testing;
using iPlatform.Api.DTOs.Base.Connector;
using NHibernate;
using TestHelper.Helpers;
using TestStack.BDDfy;

namespace Acceptance.Tests.PersistenceSpecifications.OverrideRatingQuestions
{
    public class WhenPersistingOverrideRatingQuestionBddfy : IBddfyTest
    {
        public Connector Connector { get; set; }
        protected ISession Session;

        public WhenPersistingOverrideRatingQuestionBddfy(ISession session)
        {
            Session = session;
        }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
        }

        public void Then_result()
        {
            new PersistenceSpecification<OverrideRatingQuestion>(Session, new CustomEqualityComparer())
              .CheckProperty(c => c.OverrideValue, "OverrideValue")
              .CheckList(c => c.OverrideRatingQuestionChannels, new List<OverrideRatingQuestionChannel> {new OverrideRatingQuestionChannel()})
              .VerifyTheMappings();
        }
    }
}