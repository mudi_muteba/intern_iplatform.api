using System;
using Domain.Admin;
using Domain.AuditLogs;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.PersistenceSpecifications.AuditLogs
{
    public class when_inserting_entities : BaseTest
    {
        public when_inserting_entities() : base(true, true) { }

        public override void Observe() { }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            var channel = new Channel(1);
            SaveAndFlush(channel);

            var auditLog = Session.Get<AuditLog>(1);
            auditLog.ChannelIds.ShouldEqual("1,2");
            auditLog.UserId.ShouldEqual(42);
            auditLog.UserName.ShouldEqual("contextObjectMother@testing.com");
            auditLog.AuditEntryType.ShouldEqual("Insert");
            auditLog.AuditEntryDescription.ShouldEqual(string.Empty);
            auditLog.EntityName.ShouldEqual(typeof(Channel).FullName);
            auditLog.EntityId.ShouldEqual(1);
            auditLog.FieldName.ShouldEqual(string.Empty);
            auditLog.OldValue.ShouldEqual(string.Empty);
            auditLog.NewValue.ShouldEqual(string.Empty);
            auditLog.Timestamp.Date.ShouldEqual(DateTime.Now.Date);
        }
    }
}