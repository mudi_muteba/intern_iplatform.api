﻿using System;
using Domain.Admin;
using Domain.AuditLogs;
using TestHelper;
using Xunit.Extensions;

namespace Acceptance.Tests.PersistenceSpecifications.AuditLogs
{
    public class when_updating_entities : BaseTest
    {
        public when_updating_entities() : base(true, true) { }

        public override void Observe() { }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            var channel = new Channel(1);
            SaveAndFlush(channel);
            channel.Code = "123456";
            SaveAndFlush(channel);

            var auditLog = Session.Get<AuditLog>(2);
            auditLog.ChannelIds.ShouldEqual("1,2");
            auditLog.UserId.ShouldEqual(42);
            auditLog.UserName.ShouldEqual("contextObjectMother@testing.com");
            auditLog.AuditEntryType.ShouldEqual("Update");
            auditLog.AuditEntryDescription.ShouldEqual(string.Empty);
            auditLog.EntityName.ShouldEqual(typeof(Channel).FullName);
            auditLog.EntityId.ShouldEqual(1);
            auditLog.FieldName.ShouldEqual("Code");
            auditLog.OldValue.ShouldEqual(string.Empty);
            auditLog.NewValue.ShouldEqual("123456");
            auditLog.Timestamp.Date.ShouldEqual(DateTime.Now.Date);
        }
    }
}