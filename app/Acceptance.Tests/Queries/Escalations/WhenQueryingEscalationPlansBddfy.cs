using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Escalations;
using Domain.Escalations.Queries;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums.Workflows;
using MasterData.Authorisation;
using NHibernate;
using TestStack.BDDfy;
using Xunit.Extensions;

namespace Acceptance.Tests.Queries.Escalations
{
    public class WhenQueryingEscalationPlansBddfy : IBddfyTest
    {
        private FindEscalationPlanQuery _query;
        public Connector Connector { get; set; }
        protected ISession Session;
        protected WindsorContainer Container;

        public WhenQueryingEscalationPlansBddfy(ISession session, WindsorContainer container)
        {
            Session = session;
            Container = container;
        }

        public void Run(Connector connector)
        {
            Connector = connector;
            this.Given(s => Given_data())
                .When(s => When_action())
                .Then(s => Then_result())
                .BDDfy();
        }

        public void Given_data()
        {
        }

        public void When_action()
        {
            var defaultContextProvider = new DefaultContextProvider();
            defaultContextProvider.SetContext(new ExecutionContext("", 1, "", new List<int> {1}, 1, new SystemAuthorisation(new ChannelAuthorisation(1, new RequiredAuthorisationPoint("", "")))));
            Container.Register(Component.For<IProvideContext>().Instance(defaultContextProvider));
            Container.Register(Component.For<IPublishEvents>().Instance(new NullEventPublisher()));
            var contextProvider = Container.Resolve<IProvideContext>();
            var repository = Container.Resolve<IRepository>();
            Container.Release(contextProvider);
            Container.Release(repository);

            _query = new FindEscalationPlanQuery(contextProvider, repository);
            
            var campaign = new Campaign();
            var plan = new EscalationPlan("Brolink plan", EventType.LeadCallBack.ToString());
            plan.TagWithCampaign(campaign);

            Session.Save(campaign);
            Session.Save(plan);
            Session.Flush();
        }

        public void Then_result()
        {
            _query.WithParameters(new List<int>(), null, 1).ExecuteQuery().Count().ShouldEqual(1);

        }
    }
}