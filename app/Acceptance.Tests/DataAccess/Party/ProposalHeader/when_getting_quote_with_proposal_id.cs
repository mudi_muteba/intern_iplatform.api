﻿using Domain.Base.Execution;
using iPlatform.Api.DTOs.Party.Quotes;
using iPlatform.Api.DTOs.Ratings.Quoting;
using TestHelper;
using TestObjects.Mothers.Proposals;
using Xunit.Extensions;

namespace Acceptance.Tests.DataAccess.Party.ProposalHeader
{
    public class when_getting_quote_with_proposal_id : BaseTest
    {
        private readonly HandlerResult<QuoteHeaderDto> _result;

        public when_getting_quote_with_proposal_id() : base(ApiUserObjectMother.AsAdmin())
        {
            var quoteDto = CreateQuoteDtoObjectMother.ForNewQuote();
            _result = ExecutionPlan.Execute<CreateQuoteDto, QuoteHeaderDto>(quoteDto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "")]
        protected override void assert_all()
        {
            then_quote_with_rating_should_be_created();
        }

        private void then_quote_with_rating_should_be_created()
        {
            _result.ShouldNotBeNull();
            _result.Response.ShouldNotBeNull();
            _result.Response.Quotes.Count.ShouldEqual(1);
        }
    }
}
