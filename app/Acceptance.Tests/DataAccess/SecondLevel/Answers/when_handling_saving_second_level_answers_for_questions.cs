﻿using System.Linq;
using Domain.Base.Execution;
using Domain.SecondLevel.Answers;
using iPlatform.Api.DTOs.SecondLevel.Answers;
using TestHelper;
using TestObjects.Mothers.SecondLevel;
using Xunit.Extensions;

namespace Acceptance.Tests.DataAccess.SecondLevel.Answers
{
    public class when_handling_saving_second_level_answers_for_questions : BaseTest
    {
        private readonly HandlerResult<bool> _result;

        public when_handling_saving_second_level_answers_for_questions()
        {
            var dto = SecondLevelQuestionDefinitionMother.ForSecondLevelQuestionsSaveDto();
            _result = ExecutionPlan.Execute<SecondLevelQuestionsAnswersSaveDto, bool>(dto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            then_answers_to_second_level_questions_should_be_saved();
        }

        private void then_answers_to_second_level_questions_should_be_saved()
        {
            _result.Response.ShouldEqual(true);

            var answers = Repository.GetAll<SecondLevelQuestionSavedAnswer>();
            answers.Count().ShouldEqual(4);
        }
    }
}