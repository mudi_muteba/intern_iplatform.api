﻿using System.Linq;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.SecondLevel.Questions;
using iPlatform.Api.DTOs.SecondLevel.Quotes;
using TestHelper;
using TestObjects.Mothers.SecondLevel;
using Xunit.Extensions;

namespace Acceptance.Tests.DataAccess.SecondLevel.Questions
{
    public class when_getting_second_level_questions_for_quote : BaseTest
    {
        private readonly HandlerResult<SecondLevelQuestionsForQuoteDto> _result;

        public when_getting_second_level_questions_for_quote()
            : base(ApiUserObjectMother.AsAdmin())
        {
            var dto = SecondLevelQuestionDefinitionMother.ForSecondLevelQuoteDto();

            //if you want saved answers
            //ExecutionPlan.Execute<SecondLevelQuestionsAnswersSaveDto, bool>(SecondLevelQuestionDefinitionMother.ForSecondLevelQuestionsSaveDto());
            _result = ExecutionPlan.Execute<SecondLevelQuoteDto, SecondLevelQuestionsForQuoteDto>(dto);
        }

        public override void Observe()
        {
            
        }

        [Observation(Skip = "To be fixed")]
        protected override void assert_all()
        {
            then_correct_second_level_questions_are_returned();
        }

        private void then_correct_second_level_questions_are_returned()
        {
            _result.ShouldNotBeNull();
            _result.Response.ShouldNotBeNull();
            _result.Response.Questions.Count.ShouldEqual(4);

            _result.Response.Questions.FirstOrDefault(w => w.Name.Equals("Policy cancelled or refused renewal")).ShouldNotBeNull();
            _result.Response.Questions.FirstOrDefault(w => w.Name.Equals("Under debt review or administration")).ShouldNotBeNull();
            _result.Response.Questions.FirstOrDefault(w => w.Name.Equals("Any defaults or judgements")).ShouldNotBeNull();
            _result.Response.Questions.FirstOrDefault(w => w.Name.Equals("Insolvent, sequestrated or liquidated")).ShouldNotBeNull();

            _result.Response.QuoteItems.ShouldNotBeNull();
            _result.Response.QuoteItems.Count.ShouldEqual(1);
        }
    }
}
