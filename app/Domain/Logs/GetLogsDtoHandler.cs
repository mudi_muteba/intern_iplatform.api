﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Common.Logging;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Logs;
using Infrastructure.Configuration;
using MasterData.Authorisation;

namespace Domain.Logs
{

    public class GetLogsDtoHandler : BaseDtoHandler<GetLogsDto, LogsResultDto>
    {
        private IProvideContext contextProvider;
        public GetLogsDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            this.repository = repository;
            regex = SetupRegex();
            this.contextProvider = contextProvider;
        }
        private readonly IRepository repository;
        private static readonly ILog log = LogManager.GetLogger<GetLogsDtoHandler>();
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        private Regex regex;

        protected override void InternalHandle(GetLogsDto dto, HandlerResult<LogsResultDto> result)
        {

            var logResult = new LogsResultDto();

            try
            {
                if (contextProvider.Get().Username != "admin@iplatform.co.za")
                {
                    throw new AccessViolationException("Authorisation Failure - You do not have the required Rights");
                }

                var listOfFilePathsToCheck = new List<string>();
                var environment = ConfigurationReader.Platform.iPlatformEnvironment;
                var logsFilePath = string.Format(@"{0}\{1}\{2}", Environment.GetEnvironmentVariable("log_path"), "iPlatform",
                    ConfigurationReader.Platform.WorkflowEnvironment);


                if (string.Equals(environment, "live", StringComparison.InvariantCultureIgnoreCase))
                {
                    //LIVE1
                    listOfFilePathsToCheck.Add(string.Format("\\\\{0}\\{1}", "Nrcdnweb201", logsFilePath.Replace(":", "$")));
                    //LIVE2
                    listOfFilePathsToCheck.Add(string.Format("\\\\{0}\\{1}", "Nrcdnweb202", logsFilePath.Replace(":", "$")));
                }
                else
                {
                    listOfFilePathsToCheck.Add(logsFilePath);
                }

                foreach (var filePath in listOfFilePathsToCheck)
                {
                    var fileName = GetLatestFile(filePath, dto.FileType);

                    if (string.IsNullOrEmpty(fileName))
                    {
                        throw new FileNotFoundException(string.Format("No {0} log files for today found.", dto.FileType));
                    }

                    var previousLog = new LogDto();
                    using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        using (StreamReader sr = new StreamReader(fs, Encoding.Default))
                        {
                            string s = String.Empty;
                            while ((s = sr.ReadLine()) != null)
                            {
                                var currentLog = Read(s, previousLog);
                                if (dto.LogType == LogType.All || string.Equals(currentLog.Type, dto.LogType.ToString(), StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (!Equals(currentLog, previousLog))
                                    {
                                        logResult.Logs.Add(currentLog);
                                        previousLog = currentLog;
                                    }
                                }
                            }
                        }
                    }
                }
                logResult.Logs = logResult.Logs.OrderByDescending(logDto => logDto.DateTime).ToList();
            }
            catch (Exception e)
            {
                logResult.Logs.Add(new LogDto()
                {
                    DateTime = DateTime.Now,
                    Type = LogType.Error.ToString(),
                    Message = e.Message
                });
            }
            result.Processed(logResult);
        }

        private LogDto Read(string txt, LogDto previousLog)
        {
            var log = new LogDto();
            Match m = regex.Match(txt);
            if (m.Success)
            {
                String timestamp1 = m.Groups[1].ToString();
                //String c1 = m.Groups[2].ToString();  // Any Single Character 1
                //String d1 = m.Groups[3].ToString();  // Any Single Digit 1
                //String d2 = m.Groups[4].ToString();  // Any Single Digit 2
                //String d3 = m.Groups[5].ToString();  // Any Single Digit 3
                // String ws1 = m.Groups[6].ToString(); // White Space 1
                String word1 = m.Groups[7].ToString(); // Word 1
                String message = m.Groups[8].ToString(); //Message

                log.DateTime = DateTime.Parse(timestamp1);
                log.Type = word1;
                log.Message = message;
            }
            else
            {
                previousLog.Message += txt;
                return previousLog;
            }
            return log;
        }

        private static Regex SetupRegex()
        {
            string re1 =
                "((?:2|1)\\d{3}(?:-|\\/)(?:(?:0[1-9])|(?:1[0-2]))(?:-|\\/)(?:(?:0[1-9])|(?:[1-2][0-9])|(?:3[0-1]))(?:T|\\s)(?:(?:[0-1][0-9])|(?:2[0-3])):(?:[0-5][0-9]):(?:[0-5][0-9]))";
            // Time Stamp 1
            string re2 = "(,)"; // Any Single Character 1
            string re3 = "(\\d)"; // Any Single Digit 1
            string re4 = "(\\d)"; // Any Single Digit 2
            string re5 = "(\\d)"; // Any Single Digit 3
            string re6 = "(\\s+)"; // White Space 1
            string re7 = "((?:[a-z][a-z]+))"; // Word 1
            string re8 = "(.*)";

            Regex r = new Regex(re1 + re2 + re3 + re4 + re5 + re6 + re7 + re8, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            return r;
        }


        private string GetLatestFile(string path, FileType fileType)
        {
            var fileName = "";
            string[] fileEntries = Directory.GetFiles(path);
            var file = Directory.GetFiles(path, string.Format("{0}*", fileType.ToString())).OrderByDescending(d => new FileInfo(d).CreationTime).FirstOrDefault();

            return file;
        }
    }
}