﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Repository;
using Infrastructure.Configuration;

namespace Domain.Performance
{
    public class PerformanceCounters
    {
        [ThreadStatic]
        private static PerformanceCounters _performanceCounters;
        private readonly Dictionary<string,PerformanceCounter> Counters;
        private IRepository _repository;
        private bool IsEnabled { get; set; }

        private PerformanceCounters()
        {
            Counters = new Dictionary<string, PerformanceCounter>();

            try
            {
                string value = ConfigurationReader.Appsetting.Read("EnablePerformanceCounter", false);
                IsEnabled = Boolean.Parse(value);
            }
            catch (Exception)
            {
                IsEnabled = false;
            }
        }

        public static PerformanceCounters Instance
        {
            get
            {
                if (_performanceCounters == null)
                    _performanceCounters = new PerformanceCounters();
                return _performanceCounters;
            }
        }

        public PerformanceCounters WithRepository(IRepository repository)
        {
            if (_performanceCounters == null)
                _performanceCounters = new PerformanceCounters();

            _repository = repository;
            return _performanceCounters;
        }

        public PerformanceCounter Create(string id)
        {
            if (!IsEnabled)
                return new PerformanceCounter();
            
            var counter = new PerformanceCounter(id).WithRepository(_repository);
            Counters.Add(id,counter);
            return counter;
        }

        public PerformanceCounter Get(string id)
        {
            if (!IsEnabled)
                return new PerformanceCounter();

            return Counters[id];
        }
        public bool StartCounter(string id)
        {
            if (!IsEnabled)
                return false;

            Counters[id].Start();
            return true;
        }

        public bool StopCounter(string id)
        {
            if (!IsEnabled)
                return false;

            if (!Counters.ContainsKey(id))
                return false;

            Counters[id].Stop();
            Counters.Remove(id);
            return true;
        }
    }
}