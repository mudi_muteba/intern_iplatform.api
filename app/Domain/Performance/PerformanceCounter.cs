﻿using System;
using System.Diagnostics;
using Domain.Base;
using Domain.Base.Repository;
using Domain.Users;

namespace Domain.Performance
{
    public class PerformanceCounter : Entity
    {
        private readonly Stopwatch _stopwatch;
        private IRepository _repository;

        public PerformanceCounter()
        {
        }

        public PerformanceCounter(string trackingId)
        {
            _counter++;
            _stopwatch = new Stopwatch();
            TrackingId = trackingId;
        }

        public virtual DateTime StartDateTime { get; protected set; }
        public virtual DateTime StopDateTime { get; protected set; }
        public virtual string ElapsedTime { get; protected set; }
        public virtual string TrackingId { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual int UserId { get; set; }
        public virtual long ApplicationMemory { get; set; }
        private static int _counter { get; set; }

        public virtual PerformanceCounter WithUser(User user)
        {
            UserId = user.Id;
            return this;
        }

        public virtual PerformanceCounter WithUserId(int id)
        {
            UserId = id;
            return this;
        }


        public virtual PerformanceCounter WithDescription(string description)
        {
            Description = description;
            return this;
        }

        public virtual PerformanceCounter WithRepository(IRepository repository)
        {
            _repository = repository;
            return this;
        }

        protected internal virtual PerformanceCounter Start()
        {
            _stopwatch.Start();
            StartDateTime = DateTime.UtcNow;
            return this;
        }

        protected internal virtual PerformanceCounter Stop()
        {
            ApplicationMemory = GC.GetTotalMemory(true);
            _stopwatch.Stop();
            StopDateTime = DateTime.UtcNow;
            ElapsedTime = _stopwatch.Elapsed.ToString();
            _repository.Save(this);
            return this;
        }
    }
}