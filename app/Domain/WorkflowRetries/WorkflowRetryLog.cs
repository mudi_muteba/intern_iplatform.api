﻿using System;
using Domain.Base;
using Infrastructure.NHibernate.Attributes;

namespace Domain.WorkflowRetries
{
    public class WorkflowRetryLog : Entity
    {
        public WorkflowRetryLog() { }

        public WorkflowRetryLog(Guid correlationId, int retryCount, int retryLimit, DateTime publishDate, string messageDelay, DateTime? futurePublishDate, byte[] message)
        {
            CorrelationId = correlationId;
            RetryCount = retryCount;
            RetryLimit = retryLimit;
            PublishDate = publishDate;
            MessageDelay = messageDelay;
            FuturePublishDate = futurePublishDate;
            Message = message;
        }

        [Unique]
        public virtual Guid CorrelationId { get; set; }
        [Unique]
        public virtual int RetryCount { get; set; }
        public virtual int RetryLimit { get; set ; }
        public virtual DateTime PublishDate { get; set; }
        public virtual string MessageDelay { get; set; }
        public virtual DateTime? FuturePublishDate { get; set; }
        public virtual byte[] Message { get; set; }
    }
}