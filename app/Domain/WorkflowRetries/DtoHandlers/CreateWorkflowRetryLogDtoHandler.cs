﻿using System.Collections.Generic;
using System.IO;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.WorkflowRetries;
using MasterData.Authorisation;
using Newtonsoft.Json;

namespace Domain.WorkflowRetries.DtoHandlers
{
    public class CreateWorkflowRetryLogDtoHandler : CreationDtoHandler<WorkflowRetryLog, CreateWorkflowRetryLogDto, int>
    {
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public CreateWorkflowRetryLogDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        protected override void EntitySaved(WorkflowRetryLog entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override WorkflowRetryLog HandleCreation(CreateWorkflowRetryLogDto dto, HandlerResult<int> result)
        {
            var json = JsonConvert.SerializeObject(dto.Message);
            byte[] bytes;
            using (var memStream = new MemoryStream())
            {
                using (var sw = new StreamWriter(memStream))
                {
                    sw.Write(json);
                }
                bytes = memStream.GetBuffer();
            }
            return new WorkflowRetryLog(dto.CorrelationId, dto.RetryCount, dto.RetryLimit, dto.PublishDate, dto.MessageDelay, dto.FuturePublishDate, bytes);
        }
    }
}