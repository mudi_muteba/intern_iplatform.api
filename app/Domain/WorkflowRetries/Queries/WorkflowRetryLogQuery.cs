﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using MasterData.Authorisation;

namespace Domain.WorkflowRetries.Queries
{
    public class WorkflowRetryLogQuery : BaseQuery<WorkflowRetryLog>, IOrderableQuery
    {
        private Guid _correlationId;
        private int _retryCount;
        public override List<RequiredAuthorisationPoint> RequiredRights {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public Dictionary<OrderByField, Func<string>> OrderByDefinition
        {
            get
            {
                return new Dictionary<OrderByField, Func<string>>();
            }
        }

        public WorkflowRetryLogQuery(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<WorkflowRetryLog>())
        {
        }

        public WorkflowRetryLogQuery WithCriteria(Guid correlationId, int retryCount)
        {
            _correlationId = correlationId;
            _retryCount = retryCount;
            return this;
        }

        protected internal override IQueryable<WorkflowRetryLog> Execute(IQueryable<WorkflowRetryLog> query)
        {
            return query.Where(x => x.CorrelationId == _correlationId && x.RetryCount == _retryCount);
        }
    }
}