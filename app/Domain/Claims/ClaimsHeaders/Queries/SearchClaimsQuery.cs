﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using MasterData.Authorisation;
using Domain.Individuals;

namespace Domain.Claims.ClaimsHeaders.Queries
{
    public class SearchClaimsQuery : BaseQuery<ClaimsHeader>, ISearchQuery<ClaimSearchDto>
    {
        private ClaimSearchDto _criteria;

        public SearchClaimsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ClaimsHeader>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ClaimsAuthorisation.List
                };
            }
        }

        public void WithCriteria(ClaimSearchDto criteria)
        {
            _criteria = criteria;
        }

        protected internal override IQueryable<ClaimsHeader> Execute(IQueryable<ClaimsHeader> query)
        {
            if (!string.IsNullOrWhiteSpace(_criteria.ExternalReference))
                query = query.Where(a => a.ExternalReference.Contains(_criteria.ExternalReference));

            if (_criteria.PartyId.HasValue && _criteria.PartyId > 0)
                query = query.Where(a => a.Party.Id == _criteria.PartyId);

            if (_criteria.PolicyHeaderId.HasValue && _criteria.PolicyHeaderId > 0)
                query = query.Where(a => a.PolicyHeader.Id == _criteria.PolicyHeaderId);

            if (!string.IsNullOrWhiteSpace(_criteria.IdNumber))
                query = from ap in Repository.GetAll<Individual>()
                        join p in query on ap.Id equals p.Id
                        where ap.IdentityNo == _criteria.IdNumber
                        select p;

            if (!string.IsNullOrWhiteSpace(_criteria.ContactNumber))
                query = query.Where(a => a.Party.ContactDetail.Cell == _criteria.ContactNumber
                                      || a.Party.ContactDetail.Direct == _criteria.ContactNumber
                                      || a.Party.ContactDetail.Home == _criteria.ContactNumber);

            return query;
        }
    }
}