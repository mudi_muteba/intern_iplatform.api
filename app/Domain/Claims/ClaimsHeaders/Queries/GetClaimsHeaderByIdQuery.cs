﻿using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Claims.ClaimsHeaders.Queries
{

    public class GetClaimsHeaderByIdQuery : BaseQuery<ClaimsHeader>
    {
        private int id;

        public GetClaimsHeaderByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ClaimsHeader>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ClaimsAuthorisation.List
                };
            }
        }

        public GetClaimsHeaderByIdQuery ById(int id)
        {
            this.id = id;
            return this;
        }

        protected internal override IQueryable<ClaimsHeader> Execute(IQueryable<ClaimsHeader> query)
        {
            return query
                .Where(c => c.Id == id);
        }
    }
}
