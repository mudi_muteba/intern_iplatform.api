﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Claims.ClaimsHeaders.Queries
{
    public class GetClaimsQuestionByProductIdQuery : BaseQuery<ProductClaimsQuestionDefinition>
    {
        public GetClaimsQuestionByProductIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProductClaimsQuestionDefinition>())
        {
        }

        private int productId;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetClaimsQuestionByProductIdQuery ByProductId(int productId)
        {
            this.productId = productId;
            return this;
        }

        protected internal override IQueryable<ProductClaimsQuestionDefinition> Execute(IQueryable<ProductClaimsQuestionDefinition> query)
        {
            return query
                .Where(c => c.Product.Id == productId);
        }
    }
}