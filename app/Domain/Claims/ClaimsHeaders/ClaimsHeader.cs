﻿
using AutoMapper;
using Domain.Admin;
using Domain.Base;
using Domain.Claims.ClaimsItems;
using Domain.Claims.ClaimsHeaders.Events;
using Domain.Policies;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Events;

namespace Domain.Claims.ClaimsHeaders
{
    public class ClaimsHeader : ChannelEntity
    {
        public ClaimsHeader()
        {
            CreatedAt = DateTime.UtcNow;
            ModifiedAt = DateTime.UtcNow;
            ClaimsItems = new List<ClaimsItem>();
            IsAccepted = false;
        }

        public ClaimsHeader(PolicyHeader policyHeader)
            : this()
        {
            Party = policyHeader.Party;
            PolicyHeader = policyHeader;
            IsAccepted = false;
        }

        public virtual Party.Party Party { get; protected set; }
        public virtual PolicyHeader PolicyHeader { get; protected set; }
        public virtual string ExternalReference { get; set; }
        public virtual IList<ClaimsItem> ClaimsItems { get; protected set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime ModifiedAt { get; set; }
        public virtual Boolean? IsAccepted { get; set; }
        public virtual DateTime? AcceptedOn { get; set; }
        public virtual DateTime? DateOfLoss { get; set; }
        public virtual int? AcceptedBy { get; set; }
        public virtual void AddClaimsItem(ClaimsItem item)
        {
            item.ClaimsHeader = this;
            ClaimsItems.Add(item);
        }
        public virtual void AddClaimsItems(List<ClaimsItem> claimsItems)
        {
            foreach (var claimsItem in claimsItems)
            {
                claimsItem.ClaimsHeader = this;
                ClaimsItems.Add(claimsItem);
            }

        }
        public static ClaimsHeader Create(ClaimsHeader entity, List<ProductClaimsQuestionDefinition> productQuestions, CreateClaimsHeaderDto dto)
        {
            var claimHeader = Mapper.Map<ClaimsHeader>(dto);

            AddClaimItems(claimHeader, entity.PolicyHeader.PolicyItems, productQuestions, dto.claimDto);

            claimHeader.RaiseEvent(new CreatedClaimEvent(dto, dto.CreateAudit(), new List<ChannelReference>{claimHeader.Channel}));
            return claimHeader;
        }
        public static void AddClaimItems(ClaimsHeader claimsHeader, IList<PolicyItem> policyItems, List<ProductClaimsQuestionDefinition> productQuestions, ClaimsItemDtos claimDto)
        {
            foreach (var item in claimDto.PolicyItems)
            {
                PolicyItem policyItem = policyItems.First(a => a.Id == item.PolicyItemId);
                ClaimsType claimsType = new ClaimsTypes().First(a => a.Id == item.ClaimTypeId);
                var claimsItem = new ClaimsItem(policyItem, claimsType, item.Description);
               
                List<ClaimsQuestionDefinition> questionDefinitions =
                    new ClaimsQuestionDefinitions().Where(a => a.ClaimsTypeId == claimsType.Id && productQuestions.Select(b => b.ClaimsQuestionDefinition.Id).Contains(a.Id)).ToList();

                foreach (ClaimsQuestionDefinition claimsQuestionDefinition in questionDefinitions)
                {
                    claimsItem.AddQuestionAnswer(new ClaimsItemQuestionAnswer(claimsItem, claimsQuestionDefinition,
                        claimsQuestionDefinition.ClaimsQuestion.ClaimsQuestionType));
                }

                claimsHeader.AddClaimsItem(claimsItem);
            }
        }


        public virtual void CreateClaimItems(List<ProductClaimsQuestionDefinition> productQuestions, CreateClaimsItemsDto createClaimsItemsDto)
        {
            foreach (var item in createClaimsItemsDto.claimDto.PolicyItems)
            {
                PolicyItem policyItem = this.PolicyHeader.PolicyItems.First(a => a.Id == item.PolicyItemId);
                ClaimsType claimsType = new ClaimsTypes().First(a => a.Id == item.ClaimTypeId);
                var claimsItem = new ClaimsItem(policyItem, claimsType, item.Description);

                List<ClaimsQuestionDefinition> questionDefinitions =
                    new ClaimsQuestionDefinitions().Where(a => a.ClaimsTypeId == claimsType.Id && productQuestions.Select(b => b.ClaimsQuestionDefinition.Id).Contains(a.Id)).ToList();

                foreach (ClaimsQuestionDefinition claimsQuestionDefinition in questionDefinitions)
                {
                    claimsItem.AddQuestionAnswer(new ClaimsItemQuestionAnswer(claimsItem, claimsQuestionDefinition,
                        claimsQuestionDefinition.ClaimsQuestion.ClaimsQuestionType));
                }

                this.AddClaimsItem(claimsItem);
            }

            RaiseEvent(new CreatedClaimsItemEvent(createClaimsItemsDto, createClaimsItemsDto.CreateAudit(), new List<ChannelReference> { new Channel(createClaimsItemsDto.ChannelId) }));
        }

        public virtual void UpdateClaimItems(List<ProductClaimsQuestionDefinition> productQuestions, UpdateClaimsItemsDto dto)
        {
            //Remove Old Claim Items
            RemoveOldClaimItems(dto.claimDto.PolicyItems);
            //Update Existing Claim Items
            UpdateExistingClaimItems(dto.claimDto.PolicyItems);

            //Add new ClaimsItems from list
            var newClaimsItems = dto.claimDto.PolicyItems.Where(x => !ClaimsItems.Any(y => y.Id == x.Id)).ToList();

            foreach (var item in newClaimsItems)
            {
                PolicyItem policyItem = this.PolicyHeader.PolicyItems.First(a => a.Id == item.PolicyItemId);
                ClaimsType claimsType = new ClaimsTypes().First(a => a.Id == item.ClaimTypeId);
                var claimsItem = new ClaimsItem(policyItem, claimsType, item.Description);

                List<ClaimsQuestionDefinition> questionDefinitions =
                    new ClaimsQuestionDefinitions().Where(a => a.ClaimsTypeId == claimsType.Id && productQuestions.Select(b => b.ClaimsQuestionDefinition.Id).Contains(a.Id)).ToList();

                foreach (ClaimsQuestionDefinition claimsQuestionDefinition in questionDefinitions)
                {
                    claimsItem.AddQuestionAnswer(new ClaimsItemQuestionAnswer(claimsItem, claimsQuestionDefinition,
                        claimsQuestionDefinition.ClaimsQuestion.ClaimsQuestionType));
                }

                this.AddClaimsItem(claimsItem);
            }

            RaiseEvent(new UpdateClaimsItemsEvent( dto, dto.CreateAudit(), new List<ChannelReference> { new Channel(this.Channel.Id) }));
        }

        public virtual void RemoveOldClaimItems(List<ClaimsItemDto> claimItems)
        {
            var toBeDeleted = ClaimsItems.Where(t => !claimItems.Any(y => y.Id == t.Id));

            foreach (var claimsItem in toBeDeleted)
            {
                claimsItem.Delete();
            }
        }

        public virtual void UpdateExistingClaimItems(List<ClaimsItemDto> claimItems)
        {
            var toBeUpdated = ClaimsItems.Where(t => claimItems.Any(y => y.Id == t.Id));

            foreach (var claimsItem in toBeUpdated)
            {
                claimsItem.Description = claimItems.Where(y => y.Id == claimsItem.Id).FirstOrDefault().Description;
                claimsItem.ModifiedAt = DateTime.UtcNow;
            }
        }

        public virtual void Accept(AcceptClaimDto dto)
        {
            IsAccepted = true;

            RaiseEvent(new AcceptedClaimEvent(dto, dto.CreateAudit(), new List<ChannelReference> { new Channel(this.Channel.Id) }));
        }

        public virtual void Disable(DisableClaimDto dto)
        {
            this.Delete();

            RaiseEvent(new DisabledClaimEvent(dto, dto.CreateAudit(), new List<ChannelReference> { new Channel(this.Channel.Id) }));
        }

        public virtual void DisableClaimsItem(DisableClaimItemDto dto)
        {
            RaiseEvent(new DisabledClaimsItemEvent(dto, dto.CreateAudit(), new List<ChannelReference> { new Channel(this.Channel.Id) }));
        }

        public virtual void SaveClaimsItemAnswer(SaveClaimsItemAnswersDto dto)
        {
            RaiseEvent(new SavedClaimsItemAnswersEvent(dto, dto.CreateAudit(), new List<ChannelReference> { new Channel(this.Channel.Id) }));
        }
    
    }
}
