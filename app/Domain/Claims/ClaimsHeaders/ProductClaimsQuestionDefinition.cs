﻿using Domain.Base;
using Domain.Products;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Claims.ClaimsHeaders
{
    public class ProductClaimsQuestionDefinition : Entity
    {
        public ProductClaimsQuestionDefinition()
        {
        }

        public virtual Product Product { get; set; }
        public virtual ClaimsQuestionDefinition ClaimsQuestionDefinition { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual bool? Required { get; set; }
        public virtual bool? FirstPhase { get; set; }
        public virtual bool? SecondPhase { get; set; }
        public virtual ClaimsQuestionGroup ClaimsQuestionGroup { get; set; }
        public virtual ProductClaimsQuestionDefinition ParentProductClaimQuestionDefinition { get; set; }
    }
}
