﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.Logging;
using Domain.Base.Repository;
using Domain.CimsIntegrationLogging;
using Domain.Claims.ClaimsItems;
using Domain.Policies;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Claims.FNOL;
using MasterData;
using iPlatform.Api.DTOs.Claims.Questions;
using iPlatform.Api.DTOs.Policy.FNOL;
using iPlatform.Integration.Services.Cims;
using Newtonsoft.Json;

namespace Domain.Claims.ClaimsHeaders.Helpers
{
    public class SubmitClaimHelper
    {
        private readonly IRepository _repository;
        private Guid _requestId;
        private ClaimFnolMasterDto _claimMessage;
        private readonly ILog log = LogManager.GetLogger<SubmitClaimHelper>();
        public SubmitClaimHelper(IRepository repository)
        {
            _repository = repository;
            _claimMessage = new ClaimFnolMasterDto();
        }


        public AcceptClaimResponseDto SubmitClaim(AcceptClaimDto acceptClaimDto)
        {
            var response = new AcceptClaimResponseDto(acceptClaimDto.Id);

            List<ClaimFnolDto> localClaimsList = new List<ClaimFnolDto>();
            _requestId = Guid.NewGuid();
            try
            {
                var claimHeader = _repository.GetById<ClaimsHeader>(acceptClaimDto.Id);
                if (claimHeader == null)
                {
                    log.ErrorFormat("Error submitting claim: Claimheader with id {0} does not exist", acceptClaimDto.Id.ToString());
                    return response;
                }
                    

                //Get claim items
                var claimItems = _repository.GetAll<ClaimsItem>().Where(x => x.ClaimsHeader.Id == acceptClaimDto.Id && !x.IsDeleted).ToList();

                foreach (var claimItem in claimItems)
                {
                    var claimDetail = BuildClaimItem(claimItem);

                    if (claimDetail != null)
                        localClaimsList.Add(claimDetail);
                }

                _claimMessage.ClaimDetails.AddRange(localClaimsList);

                if (_claimMessage.ClaimDetails.Any())
                    claimHeader.IsAccepted = UploadClaim(claimHeader);

                _repository.Save(claimHeader);
                log.InfoFormat("Successfull submitting claim: Claimheader with id {0} ", acceptClaimDto.Id.ToString());
                return response.Success();

            }
            catch (Exception ex)
            {
                log.Error("Error submitting claim: " + ex.Message);
                log.Error(ex.StackTrace);
            }
            return response;
        }


        private ClaimFnolDto BuildClaimItem(ClaimsItem claimItem)
        {
            try
            {
                var claimItemAnswers = _repository.GetAll<ClaimsItemQuestionAnswer>().Where(x => x.ClaimsItem.Id == claimItem.Id).ToList();

                var causeCodeResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.CauseCode);
                var benefitResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Benefit);
                var areaOfLossResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Accidentareapostalcode);
                var descriptionResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Description);
                var driverNameResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.DriversFullName);
                var driverIDResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.DriversIDNumber);
                var driverLicenseNumberResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.DriversLicenceNumber);
                var driverSurnameResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.DriversFullName);
                var alcoholTestResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.WasTheDriverPersonTestedForDrugsOrAlcohol);
                var estimatedValueResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.ClaimEstimateValue);
                var eventTypeResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.EventType);
                var witnessesResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Wasthereanywitness);
                var funeralCallerEmailResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Calleremailaddress);
                var funeralCallerFNameResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Callername);
                var funeralCallermobileResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Callermobilenumber);
                var funeralCallersurnameResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Callersurname);
                var causeOfDeathResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Causeofdeath);
                var locationResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Preferredlocationforrepairs);
                var roadsideAssitanceCalledResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Wasroadsideassistancecalled);
                var sapsCaseNumberResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.PoliceStationReferenceNumber);
                var policeAtSceneResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.DidThePoliceAttendTheScene);
                var towingCompanyResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Towingcompany);
                var towingContactDetailsResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Towingcompanycontactdetails);
                var vehicleUnderWarrantyResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Vehicleunderwarranty);
                var incidentTimeResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.AccidentTheftTime);

                var witness1NameResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Witness1Name);
                var witness1ContactResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.Witness1PhoneNumber);

                List<ClaimFnolWitnessDto> witnesses = new List<ClaimFnolWitnessDto>();

                if (!String.IsNullOrWhiteSpace(witness1NameResult.Answer))
                {
                    witnesses.Add(new ClaimFnolWitnessDto()
                    {
                        FirstName = witness1NameResult.Answer,
                        Surname = "",
                        ContactDetails = witness1ContactResult.Answer
                    });
                }

                var otherPartyNameResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.OtherPartyName);
                var otherPartyContactDetailsResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.OtherParty1DriverContactdetails);
                var otherPartyDriverNameResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.OtherParty1VehicleOwnerName);
                var otherPartyDriverLicenseNumberResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.OtherParty1DriverLicencenumber);

                var otherParty1VehicleMakeResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.OtherParty1VehicleMake);
                var otherParty1VehicleModelResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.OtherParty1Vehiclemodel);
                var otherParty1VehicleRegNoResult = GetQuestionAnswer(claimItemAnswers, ClaimsQuestions.OtherParty1VehicleRegistrationNumber);

                List<ClaimsFnolThirdPartyDto> thirdparties = new List<ClaimsFnolThirdPartyDto>();
                if (!String.IsNullOrWhiteSpace(otherPartyNameResult.Answer))
                {
                    thirdparties.Add(new ClaimsFnolThirdPartyDto()
                    {
                        ContactDetails = otherPartyContactDetailsResult.Answer,
                        FirstName = otherPartyNameResult.Answer,
                        Surname = "",
                        DriverID = "",
                        DriverLicenceNumber = otherPartyDriverLicenseNumberResult.Answer,
                        DriverSurname = "",
                        DriverFirstName = otherPartyNameResult.Answer,
                        VehicleMake = otherParty1VehicleMakeResult.Answer,
                        VehicleModel = otherParty1VehicleModelResult.Answer,
                        VehicleRegistration = otherParty1VehicleRegNoResult.Answer
                    });
                }

                var originalRequestRecord =
                    _repository.GetAll<CimsConnectIntegrationLog>()
                        .FirstOrDefault(x => x.RequestId == claimItem.PolicyHeaderClaimableItem.RequestId);

                if (originalRequestRecord != null)
                    _claimMessage.OriginalRequest = originalRequestRecord.ResponseData;

                DateTime dateTimeOfLoss = claimItem.ClaimsHeader.DateOfLoss.Value;

                if (!String.IsNullOrWhiteSpace(incidentTimeResult.Answer))
                {
                    try
                    {
                        dateTimeOfLoss = claimItem.ClaimsHeader.DateOfLoss.Value.Add(TimeSpan.Parse(incidentTimeResult.Answer));
                    }
                    catch (Exception)
                    {
                    }
                }


                bool isSpecified = false;

                if (claimItem.PolicyItem.Cover.Id == Covers.AllRisk.Id)
                {
                    var riskITem = (claimItem.PolicyItem as PolicyAllRisk);
                    if (riskITem != null && riskITem.TypeCode != QuestionAnswers.AIGAllRisksClothingPersonalEffectsUnspecified.Id.ToString())
                        isSpecified = true;
                }



                ClaimFnolDto submitClaimDto = new ClaimFnolDto()
                {
                    Id = _requestId,
                    OriginalRequest = "", // "<![CDATA[" + originalRequest + "]]>",
                    ExternalItemIdentifier = claimItem.PolicyHeaderClaimableItem.ExternalSubmitGuid.ToString(), //Change to use column from db
                    PolicyNo = claimItem.PolicyItem.PolicyHeader.PolicyNo,
                    CauseCode = causeCodeResult.Answer,
                    Benefit = benefitResult.Id.ToString(),
                    AreaOfLoss = areaOfLossResult.Answer,
                    CauseCodeId = causeCodeResult.Id,
                    IsSpecified = isSpecified,
                    Cover = claimItem.PolicyItem.Cover.Id.ToString(),
                    DateTimeOfLoss = dateTimeOfLoss,
                    DescriptionOfLoss = descriptionResult.Answer,
                    DriverFirstName = driverNameResult.Answer,
                    DriverID = driverIDResult.Answer,
                    DriverLicenceNumber = driverLicenseNumberResult.Answer,
                    DriverSurname = driverSurnameResult.Answer,
                    DrugAlcoholTest = bool.Parse(String.IsNullOrWhiteSpace(alcoholTestResult.Answer) ? "false" : alcoholTestResult.Answer),
                    EstimatedLoss = estimatedValueResult.Answer,
                    EventType = eventTypeResult.Answer,
                    EventTypeId = eventTypeResult.Id,
                    Witness = bool.Parse(String.IsNullOrWhiteSpace(witnessesResult.Answer) ? "false" : witnessesResult.Answer),
                    FuneralCallerEmailAddress = funeralCallerEmailResult.Answer,
                    FuneralCallerFirstName = funeralCallerFNameResult.Answer,
                    FuneralCallerMobileNumber = funeralCallermobileResult.Answer,
                    FuneralCallerSurname = funeralCallersurnameResult.Answer,
                    FuneralCauseOfDeath = causeOfDeathResult.Answer,
                    ItemDescription = claimItem.PolicyItem.ItemDescription,
                    PreferredRepairLocation = locationResult.Answer,
                    ReportingDateTime = DateTime.UtcNow,
                    RoadsideAssitanceContacted = bool.Parse(String.IsNullOrWhiteSpace(roadsideAssitanceCalledResult.Answer) ? "false" : roadsideAssitanceCalledResult.Answer),
                    SAPSCaseNumber = sapsCaseNumberResult.Answer,
                    SAPSOnScene = bool.Parse(String.IsNullOrWhiteSpace(policeAtSceneResult.Answer) ? "false" : policeAtSceneResult.Answer),
                    TowingCompany = towingCompanyResult.Answer,
                    TowingContactDetails = towingContactDetailsResult.Answer,
                    VehicleDriveable = String.IsNullOrWhiteSpace(towingCompanyResult.Answer),
                    VehicleTowed = !String.IsNullOrWhiteSpace(towingCompanyResult.Answer),
                    VehicleUnderWarranty = bool.Parse(String.IsNullOrWhiteSpace(vehicleUnderWarrantyResult.Answer) ? "false" : vehicleUnderWarrantyResult.Answer),
                    WitnessDetails = witnesses,
                    ThirdPartyDetails = thirdparties
                };
                return submitClaimDto;
            }
            catch (Exception ex)
            {
                log.Error("Error submitting claim item: " + ex.Message);
                log.Error(ex.StackTrace);
            }

            return null;
        }

        private ClaimsQuestionAnswerDto GetQuestionAnswer(List<ClaimsItemQuestionAnswer> answerRepository, ClaimsQuestion question)
        {
            ClaimsQuestionAnswerDto result = new ClaimsQuestionAnswerDto(0, String.Empty, string.Empty);

            var record =
                answerRepository.FirstOrDefault(
                    x => x.ClaimsQuestionDefinition.ClaimsQuestion.Id == question.Id);

            if (record != null)
            {
                if (question.ClaimsQuestionType.Id == ClaimsQuestionTypes.Dropdown.Id)
                {
                    int id = int.Parse(record.Answer);
                    var answer =
                        new ClaimsQuestionAnswers().FirstOrDefault(
                            x => x.ClaimsQuestion.Id == question.Id && x.Id == id);

                    if (answer != null)
                        result = Mapper.Map<ClaimsQuestionAnswerDto>(answer);
                }
                else if (question.ClaimsQuestionType.Id == ClaimsQuestionTypes.Checkbox.Id)
                {
                    bool boolResult = false;
                    bool didParse = bool.TryParse(record.Answer, out boolResult);

                    result.Answer = didParse ? boolResult.ToString() : "false";
                }
                else
                    result.Answer = record.Answer;
            }

            return result;
        }

        public bool UploadClaim(ClaimsHeader claimHeader)
        {
            var submitResult = false;

            try
            {
                CimsPolicies policyIntegration = new CimsPolicies();

                FnolLodgeClaimResponseDto submitResponse = policyIntegration.SubmitFnolClaim(_claimMessage, _requestId);

                _repository.Save(new CimsConnectIntegrationLog()
                {
                    RequestId = _requestId,
                    RequestData = JsonConvert.SerializeObject(_claimMessage),
                    ResponseData = submitResponse.RawResponse
                });

                if (submitResponse.Success)
                {
                    var claimItems = _repository.GetAll<ClaimsItem>().Where(x => x.ClaimsHeader.Id == claimHeader.Id && !x.IsDeleted).ToList();

                    foreach (var claimItem in claimItems)
                    {
                        claimItem.SubmitRequestId = _requestId;
                        if (submitResponse.Data != null)
                        {
                            claimItem.ClaimNumber = submitResponse.Data.ClaimNumber;
                        }
                        _repository.Save(claimItem);
                    }

                    submitResult = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error submitting claim item: " + ex.Message);
                log.Error(ex.StackTrace);
                submitResult = false;
            }

            return submitResult;
        }
    }
}
