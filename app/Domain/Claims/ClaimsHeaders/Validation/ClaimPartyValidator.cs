﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using ValidationMessages.Campaigns;
using System.Collections.Generic;
using ValidationMessages.Individual;
using Domain.Party.Queries;


namespace Domain.Claims.ClaimsHeaders.Validation
{
    public class ClaimPartyValidator : IValidateDto<CreateClaimsHeaderDto>
        , IValidateDto<CreateClaimsItemsDto>, IValidateDto<UpdateClaimsItemsDto>
    {
        private readonly GetPartyByIdQuery query;

        public ClaimPartyValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetPartyByIdQuery(contextProvider, repository);
        }

        public void Validate(CreateClaimsHeaderDto dto, ExecutionResult result)
        {
            ValidateParty(dto.PartyId, result);
        }

        public void Validate(CreateClaimsItemsDto dto, ExecutionResult result)
        {
            ValidateParty(dto.PartyId, result);
        }

        public void Validate(UpdateClaimsItemsDto dto, ExecutionResult result)
        {
            ValidateParty(dto.PartyId, result);
        }

        private void ValidateParty(int partyId, ExecutionResult result)
        {
            var queryResult = GetPartyByIdQuery(partyId);

            if (queryResult.Count() <= 0)
            {
                result.AddValidationMessage(PartyValidationMessages.InvalidId.AddParameters(new[] { partyId.ToString() }));
            }
        }

        private IQueryable<Party.Party> GetPartyByIdQuery(int id)
        {
            return query.WithId(id).ExecuteQuery();
        }
    }
}