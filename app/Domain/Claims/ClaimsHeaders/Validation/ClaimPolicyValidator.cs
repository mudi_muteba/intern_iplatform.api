﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using System.Collections.Generic;
using Domain.Policies.Queries;
using Domain.Policies;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using ValidationMessages.Policy;

namespace Domain.Claims.ClaimsHeaders.Validation
{
    public class ClaimPolicyValidator : IValidateDto<CreateClaimsHeaderDto>
    {
        private readonly GetPolicyByIdQuery query;

        public ClaimPolicyValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetPolicyByIdQuery(contextProvider, repository);
        }

        public void Validate(CreateClaimsHeaderDto dto, ExecutionResult result)
        {
            ValidatePolicy(dto.PolicyHeaderId, result);
        }

        private void ValidatePolicy(int policyHeaderId, ExecutionResult result)
        {
            var queryResult = GetPolicyByIdQuery(policyHeaderId);

            if (queryResult.Count() <= 0)
            {
                result.AddValidationMessage(PolicyValidationMessages.InvalidId.AddParameters(new[] { policyHeaderId.ToString() }));
            }
        }

        private IQueryable<PolicyHeader> GetPolicyByIdQuery(int id)
        {
            return query.WithId(id).ExecuteQuery();
        }
    }
}