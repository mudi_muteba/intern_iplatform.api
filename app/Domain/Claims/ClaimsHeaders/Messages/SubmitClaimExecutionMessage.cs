﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Claims.ClaimsHeaders.Messages
{
    public class SubmitClaimExecutionMessage : WorkflowExecutionMessage
    {
        public SubmitClaimExecutionMessage()
        {
        }

        public SubmitClaimExecutionMessage(
            WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
        }

        public SubmitClaimExecutionMessage(ITaskMetadata metadata,
            WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default)
            : base(metadata, messageType)
        {
        }

    }
}
