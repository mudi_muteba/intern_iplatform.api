﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using Workflow.Messages.Plan.Tasks;

namespace Domain.Claims.ClaimsHeaders.Messages
{
    public class SubmitClaimTaskMetaData: ITaskMetadata
    {
        public AcceptClaimDto AcceptClaimDto { get; set; }
        public string Token { get; set; }
        public SubmitClaimTaskMetaData(AcceptClaimDto acceptClaimDto, string token)
        {
            AcceptClaimDto = acceptClaimDto;
            Token = token;
        }
    }
}
