﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Claims.ClaimsHeaders.Overrides
{
    public class ProductClaimsQuestionDefinitionOverride : IAutoMappingOverride<ProductClaimsQuestionDefinition>
    {
        public void Override(AutoMapping<ProductClaimsQuestionDefinition> mapping)
        {
            mapping.References(x => x.ParentProductClaimQuestionDefinition).Nullable();
        }
    }
}