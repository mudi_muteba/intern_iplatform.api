﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Claims.ClaimsHeaders.Overrides
{
    public class ClaimsHeaderOverride : IAutoMappingOverride<ClaimsHeader>
    {
        public void Override(AutoMapping<ClaimsHeader> mapping)
        {
            mapping.HasMany(c => c.ClaimsItems).Cascade.SaveUpdate();
        }
    }
}