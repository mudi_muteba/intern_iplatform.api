﻿
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Policies;
using System;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Domain.Claims.ClaimsHeaders.Mappings
{
    public class ClaimsMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ClaimsHeader, ListClaimDto>();
            
            Mapper.CreateMap<PagedResults<ClaimsHeader>, PagedResultDto<ListClaimDto>>();

            Mapper.CreateMap<CreateClaimsHeaderDto, ClaimsHeader>()
                .ForMember(t => t.Party, o => o.MapFrom(s => new Party.Party() { Id = s.PartyId }))
                .ForMember(t => t.PolicyHeader, o => o.MapFrom(s => new PolicyHeader() { Id = s.PolicyHeaderId }))
                .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel(s.ChannelId)))
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.ClaimsItems, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                ;

            Mapper.CreateMap<ClaimsHeader, ClaimsHeaderDto>();

            Mapper.CreateMap<PagedResults<ClaimsHeader>, PagedResultDto<ClaimsHeaderDto>>();

            Mapper.CreateMap<HandlerResult<AcceptClaimResponseDto>, POSTResponseDto<AcceptClaimResponseDto>>()
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                ;
        }

        public object HandlerResult { get; set; }
    }
}