﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Claims.ClaimsItems;

namespace Domain.Claims.ClaimsHeaders.Events
{
    public class CreatedClaimsItemEvent : BaseDomainEvent
    {
        public CreateClaimsItemsDto createClaimsItemsDto { get; set; }

        public CreatedClaimsItemEvent(CreateClaimsItemsDto dto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            this.createClaimsItemsDto = dto;
        }
    }
}