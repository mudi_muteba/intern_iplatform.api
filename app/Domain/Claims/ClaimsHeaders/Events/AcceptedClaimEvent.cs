﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;

namespace Domain.Claims.ClaimsHeaders.Events
{
    public class AcceptedClaimEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public AcceptClaimDto AcceptClaimDto { get; set; }

        public AcceptedClaimEvent(AcceptClaimDto dto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            this.AcceptClaimDto = dto;
        }
    }
}