﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;

namespace Domain.Claims.ClaimsHeaders.Events
{
    public class CreatedClaimEvent : BaseDomainEvent
    {
        public CreateClaimsHeaderDto createClaimsHeaderDto { get; set; }

        public CreatedClaimEvent(CreateClaimsHeaderDto dto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            this.createClaimsHeaderDto = dto;
        }
    }
}