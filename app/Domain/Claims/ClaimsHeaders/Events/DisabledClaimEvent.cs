﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;

namespace Domain.Claims.ClaimsHeaders.Events
{
    public class DisabledClaimEvent : BaseDomainEvent
    {
        public DisableClaimDto DisableClaimDto { get; set; }

        public DisabledClaimEvent(DisableClaimDto dto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            this.DisableClaimDto = dto;
        }
    }
}