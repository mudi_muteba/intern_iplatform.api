﻿using Domain.Admin;
using Domain.Base.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Claims.ClaimsItems;

namespace Domain.Claims.ClaimsHeaders.Events
{
    public class SavedClaimsItemAnswersEvent : BaseDomainEvent
    {
        public SaveClaimsItemAnswersDto saveClaimsItemAnswersDto { get; set; }

        public SavedClaimsItemAnswersEvent(SaveClaimsItemAnswersDto dto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            this.saveClaimsItemAnswersDto = dto;
        }
    }
}
