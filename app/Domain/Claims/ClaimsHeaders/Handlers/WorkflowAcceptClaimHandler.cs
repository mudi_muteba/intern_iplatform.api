﻿using System.Collections.Generic;
using Common.Logging;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Claims.ClaimsHeaders.Helpers;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using MasterData.Authorisation;

namespace Domain.Claims.ClaimsHeaders.Handlers
{
    public class WorkflowAcceptClaimHandler : BaseDtoHandler<AcceptClaimDto,AcceptClaimResponseDto>
    {
        private readonly IRepository _repository;
        private readonly ILog log = LogManager.GetLogger<WorkflowAcceptClaimHandler>();
        private readonly SubmitClaimHelper _helper;

        public WorkflowAcceptClaimHandler(IRepository repository, IProvideContext context)
            : base(context)
        {
            _repository = repository;
            _helper = new SubmitClaimHelper(repository);
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(AcceptClaimDto dto, HandlerResult<AcceptClaimResponseDto> result)
        {
            var response = _helper.SubmitClaim(dto);

            result.Processed(response);
        }
    }
}
