﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Repository;

using Domain.Claims.ClaimsHeaders.Events;
using Domain.Claims.ClaimsHeaders.Messages;
using Domain.Claims.ClaimsItems;
using iPlatform.Api.DTOs.Claims.FNOL;
using Workflow.Messages;


namespace Domain.Claims.ClaimsHeaders.Handlers
{
    public class AcceptedClaimEventHandler: BaseEventHandler<AcceptedClaimEvent>
    {
        private readonly IWorkflowRouter _router;
        private readonly IProvideContext _context;
        private readonly ILog log = LogManager.GetLogger<AcceptedClaimEventHandler>();

        public AcceptedClaimEventHandler(IWorkflowRouter router, IProvideContext context)
        {
            _router = router;
            _context = context;
        }
        public override void Handle(AcceptedClaimEvent @event)
        {
            var routingMessage = new WorkflowRoutingMessage();
            routingMessage.AddMessage(new SubmitClaimExecutionMessage(new SubmitClaimTaskMetaData(@event.AcceptClaimDto, _context.Get().Token)));

            _router.Publish(routingMessage);
        }

    }
}
