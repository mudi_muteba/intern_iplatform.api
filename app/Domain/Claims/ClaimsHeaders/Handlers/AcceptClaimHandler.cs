﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using MasterData.Authorisation;
using Domain.Claims;

namespace Domain.Claims.ClaimsHeaders.Handlers
{
    public class AcceptClaimHandler : ExistingEntityDtoHandler<ClaimsHeader, AcceptClaimDto, int>
    {
        public AcceptClaimHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ClaimsAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(ClaimsHeader entity, AcceptClaimDto dto, HandlerResult<int> result)
        {
            entity.Accept(dto);
            result.Processed(entity.Id);
        }
    }
}