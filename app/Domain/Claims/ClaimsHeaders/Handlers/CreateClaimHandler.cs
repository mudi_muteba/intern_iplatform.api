﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using MasterData.Authorisation;
using Domain.Products.Queries;
using Domain.Products;
using System.Linq;
using AutoMapper;
using Domain.Policies.Queries;
using Domain.Policies;
using Domain.Claims.ClaimsHeaders.Queries;

namespace Domain.Claims.ClaimsHeaders.Handlers
{
    public class CreateClaimHandler : CreationDtoHandler<ClaimsHeader, CreateClaimsHeaderDto, int>
    {
        public CreateClaimHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            policyQuery = new GetPolicyByIdQuery(contextProvider, repository);
            claimsQuestionQuery = new GetClaimsQuestionByProductIdQuery(contextProvider, repository);
        }

        private GetPolicyByIdQuery policyQuery;
        private GetClaimsQuestionByProductIdQuery claimsQuestionQuery; 
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ClaimsAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(ClaimsHeader entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override ClaimsHeader HandleCreation(CreateClaimsHeaderDto dto, HandlerResult<int> result)
        {
            //Getting the Policy Header
            PolicyHeader policyHeader = policyQuery.WithId(dto.PolicyHeaderId).ExecuteQuery().FirstOrDefault();

            //Getting list of Questions
            List<ProductClaimsQuestionDefinition> productQuestions =
                   claimsQuestionQuery.ByProductId(policyHeader.Product.Id).ExecuteQuery().ToList();

            var entity = new ClaimsHeader(policyHeader);

            var claimsHeader = ClaimsHeader.Create(entity, productQuestions, dto);

            result.Processed(claimsHeader.Id);
            return claimsHeader;
        }

    }
}