﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Claims.ClaimsHeaders;
using MasterData.Authorisation;

namespace Domain.Claims.ClaimsHeaders.Handlers
{
    public class DisableClaimHandler : ExistingEntityDtoHandler<ClaimsHeader, DisableClaimDto, int>
    {
        public DisableClaimHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ClaimsAuthorisation.Delete
                };
            }
        }

        protected override void HandleExistingEntityChange(ClaimsHeader entity, DisableClaimDto dto, HandlerResult<int> result)
        {
            entity.Disable(dto);
            result.Processed(entity.Id);
        }
    }
}