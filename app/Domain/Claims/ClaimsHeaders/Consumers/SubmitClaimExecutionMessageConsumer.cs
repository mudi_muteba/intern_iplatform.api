﻿using System.Net;
using Common.Logging;
using Domain.Claims.ClaimsHeaders.Messages;
using iPlatform.Api.DTOs.Base.Connector;
using Workflow.Domain;
using Workflow.Messages;
using Infrastructure.Configuration;

namespace Domain.Claims.ClaimsHeaders.Consumers
{
    public class SubmitClaimExecutionMessageConsumer : AbstractMessageConsumer<SubmitClaimExecutionMessage>
    {
        private readonly ILog _log = LogManager.GetLogger<SubmitClaimExecutionMessageConsumer>();
        private readonly IConnector _connector;
        public SubmitClaimExecutionMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector)
            : base(router, executor)
        {
            _connector = connector;
        }

        public override void Consume(SubmitClaimExecutionMessage message)
        {
            var metaData = (SubmitClaimTaskMetaData)message.Metadata;
            _log.InfoFormat("Submitting claim for id {0} ", metaData.AcceptClaimDto.Id);

            _connector.SetToken(metaData.Token);
            var response = _connector.ClaimsHeaderManagement.ClaimsHeaders.AcceptClaim(metaData.AcceptClaimDto);

            if(response.StatusCode == HttpStatusCode.Created && response.Response.IsSuccess)
                _log.InfoFormat("Successfully Submitted claim to fnol with Id: {0} ", metaData.AcceptClaimDto.Id);
            else
                _log.ErrorFormat("Failure to submit Claim with id: {0}", metaData.AcceptClaimDto.Id);
        }
    }
}
