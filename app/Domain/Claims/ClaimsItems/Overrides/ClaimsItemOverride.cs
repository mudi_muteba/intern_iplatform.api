﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Claims.ClaimsItems.Overrides
{
    public class ClaimsItemOverride : IAutoMappingOverride<ClaimsItem>
    {
        public void Override(AutoMapping<ClaimsItem> mapping)
        {
            mapping.HasMany(c => c.Answers).Cascade.SaveUpdate();
            mapping.References(c => c.ClaimsHeader, "ClaimsHeaderId");
            mapping.References(c => c.PolicyItem, "PolicyItemId");

            mapping.IgnoreProperty(x => x.TotalQuestions);
            mapping.IgnoreProperty(x => x.TotalQuestionsAnswered);
        }
    }
}