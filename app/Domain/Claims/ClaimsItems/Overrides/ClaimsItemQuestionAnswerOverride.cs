﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Claims.ClaimsItems.Overrides
{
    public class ClaimsItemQuestionAnswerOverride : IAutoMappingOverride<ClaimsItemQuestionAnswer>
    {
        public void Override(AutoMapping<ClaimsItemQuestionAnswer> mapping)
        {
            mapping.References(x => x.ClaimsItem, "ClaimsItemId");
        }
    }
}