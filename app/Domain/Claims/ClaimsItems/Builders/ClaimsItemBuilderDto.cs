using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.Logging;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions.Builders.OptionsBuilders;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using MasterData;

namespace Domain.Claims.ClaimsItems.Builders
{
    public class ClaimsItemBuilderDto
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(ClaimsItemBuilderDto));
        private readonly IRepository _repository;

        public ClaimsItemBuilderDto(IRepository repository)
        {
            _repository = repository;
        }

        public ClaimsItemDto Build(ClaimsItem definition)
        {
            if (definition == null)
            {
                log.ErrorFormat("ClaimItem Definition not found");
                return null;
            }

            var model = Mapper.Map<ClaimsItemDto>(definition);
          
            return model;
        }
    }
}