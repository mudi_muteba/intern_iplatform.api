﻿using Domain.Base;
using Domain.Policies;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Claims.ClaimsHeaders;

namespace Domain.Claims.ClaimsItems
{
    public class ClaimsItem : Entity
    {
        public ClaimsItem()
        {
            CreatedAt = DateTime.UtcNow;
            ModifiedAt = DateTime.UtcNow;
            Answers = new List<ClaimsItemQuestionAnswer>();
        }

        public ClaimsItem(PolicyItem policyItem, ClaimsType claimsType, string description)
            : this()
        {
            PolicyItem = policyItem;
            ClaimsType = claimsType;
            Description = description ?? string.Empty;
        }

        public ClaimsItem(PolicyHeaderClaimableItem policyClaimablItem, ClaimsType claimsType, string description)
            : this()
        {
            PolicyItem = policyClaimablItem.PolicyItem;
            PolicyHeaderClaimableItem = policyClaimablItem;
            ClaimsType = claimsType;
            Description = description ?? string.Empty;
        }

        public virtual ClaimsHeader ClaimsHeader { get; set; }
        public virtual PolicyItem PolicyItem { get; set; }
        public virtual PolicyHeaderClaimableItem PolicyHeaderClaimableItem { get; set; }
        public virtual ClaimsType ClaimsType { get; set; }
        public virtual string Description { get; set; }
        public virtual string ClaimNumber { get; set; }
        public virtual IList<ClaimsItemQuestionAnswer> Answers { get; protected set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime ModifiedAt { get; set; }
        public virtual Guid SubmitRequestId { get; set; }

        public virtual int TotalQuestions
        {
            get { return Answers.Count(a => { return a.ClaimsQuestionDefinition.Required != null && (a.ClaimsQuestionDefinition.Required.Value == true); }); }
        }
        public virtual int TotalQuestionsAnswered
        {
            get
            {
                return Answers.Count(a =>
                {
                    return a.ClaimsQuestionDefinition.Required != null && (!string.IsNullOrEmpty(a.Answer) && (a.ClaimsQuestionDefinition.Required.Value == true));
                });
            }
        }

        public virtual void AddQuestionAnswer(ClaimsItemQuestionAnswer questionAnswer)
        {
            Answers.Add(questionAnswer);
        }


        public virtual void UpdateQuestionAnswers(SaveClaimsItemAnswersDto dto)
        {
            foreach(var answer in dto.Answers)
            {
                var question  = this.Answers.Where(x => x.Id == answer.Id).FirstOrDefault();

                if (question != null)
                    question.Answer = answer.Answer;
            }
        }

    }
}
