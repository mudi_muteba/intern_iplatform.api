﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Claims.ClaimsItems.Queries
{
    public class GetClaimsItemsByHeaderIdQuery : BaseQuery<ClaimsItem>
    {
        private int _headerId;

        public GetClaimsItemsByHeaderIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ClaimsItem>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ClaimsAuthorisation.List
                };
            }
        }

        public GetClaimsItemsByHeaderIdQuery WithHeaderId(int id)
        {
            _headerId = id;
            return this;
        }

        protected internal override IQueryable<ClaimsItem> Execute(IQueryable<ClaimsItem> query)
        {
            return Repository.GetAll<ClaimsItem>().Where(a => a.ClaimsHeader.Id == _headerId);
        }
    }
}