﻿using Domain.Base;
using MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Claims.ClaimsItems
{
    public class ClaimsItemQuestionAnswer : Entity
    {
        public ClaimsItemQuestionAnswer()
        {

        }
        public ClaimsItemQuestionAnswer(ClaimsItem claimsItem, ClaimsQuestionDefinition claimsQuestionDefinition,
            ClaimsQuestionType claimsQuestionType)
            : this()
        {
            ClaimsItem = claimsItem;
            ClaimsQuestionDefinition = claimsQuestionDefinition;
            ClaimsQuestionType = claimsQuestionType;
            this.Answer = string.Empty;
        }

        public virtual ClaimsItem ClaimsItem { get; protected set; }
        public virtual ClaimsQuestionDefinition ClaimsQuestionDefinition { get; protected set; }
        public virtual ClaimsQuestionType ClaimsQuestionType { get; protected set; }
        public virtual string Answer { get; set; }

    }
}
