﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using System.Collections.Generic;
using Domain.Claims.ClaimsHeaders.Queries;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using ValidationMessages.Claims;
using Domain.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Policy;


namespace Domain.Claims.ClaimsItems.Validation
{
    public class ClaimsHeaderValidator : IValidateDto<CreateClaimsItemsDto>, IValidateDto<UpdateClaimsItemsDto>, IValidateDto<GetPolicyAvailableItemsForFNOLDto>
    {
        private readonly GetClaimsHeaderByIdQuery query;

        public ClaimsHeaderValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetClaimsHeaderByIdQuery(contextProvider, repository);
        }

        public void Validate(CreateClaimsItemsDto dto, ExecutionResult result)
        {
            ValidateClaimsHeader(dto.ClaimsHeaderId, result);
        }

        public void Validate(UpdateClaimsItemsDto dto, ExecutionResult result)
        {
            ValidateClaimsHeader(dto.ClaimsHeaderId, result);
        }

        public void Validate(GetPolicyAvailableItemsForFNOLDto dto, ExecutionResult result)
        {
            if(dto.ClaimsHeaderId > 0)
                ValidateClaimsHeader(dto.ClaimsHeaderId, result);
        }

        private void ValidateClaimsHeader(int claimsHeaderId, ExecutionResult result)
        {
            var queryResult = GetClaimsHeaderByIdQuery(claimsHeaderId);

            if (queryResult.Count() <= 0)
            {
                result.AddValidationMessage(ClaimsHeaderValidationMessages.InvalidId.AddParameters(new[] { claimsHeaderId.ToString() }));
            }

        }

        private IQueryable<ClaimsHeader> GetClaimsHeaderByIdQuery(int id)
        {
            return query.ById(id).ExecuteQuery();
        }
    }
}