﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using System.Collections.Generic;
using Domain.Claims.ClaimsHeaders.Queries;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using ValidationMessages.Claims;
using Domain.Claims.ClaimsItems.Queries;


namespace Domain.Claims.ClaimsItems.Validation
{
    public class ClaimsItemValidator : IValidateDto<DisableClaimItemDto>, IValidateDto<UpdateClaimsItemsDto>
    {
        private readonly GetClaimsItemByIdQuery query;

        public ClaimsItemValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetClaimsItemByIdQuery(contextProvider, repository);
        }

        public void Validate(DisableClaimItemDto dto, ExecutionResult result)
        {
            ValidateClaimsItem(dto.Id, result);
        }

        public void Validate(UpdateClaimsItemsDto dto, ExecutionResult result)
        {
            ValidateClaimsItem(dto.Id, result);
        }

        private void ValidateClaimsItem(int id, ExecutionResult result)
        {
            var queryResult = GetClaimsItemByIdQuery(id);

            if (queryResult.Count() <= 0)
                result.AddValidationMessage(ClaimsItemValidationMessages.InvalidId.AddParameters(new[] { id.ToString() }));
        }

        private IQueryable<ClaimsItem> GetClaimsItemByIdQuery(int id)
        {
            return query.WithId(id).ExecuteQuery();
        }
    }
}