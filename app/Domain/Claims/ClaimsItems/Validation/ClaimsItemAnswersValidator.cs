﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using System.Collections.Generic;
using Domain.Claims.ClaimsHeaders.Queries;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using ValidationMessages.Claims;
using Domain.Claims.ClaimsItems.Queries;


namespace Domain.Claims.ClaimsItems.Validation
{
    public class ClaimsItemAnswersValidator : IValidateDto<SaveClaimsItemAnswersDto>
    {
        private readonly GetClaimsItemByIdQuery query;

        public ClaimsItemAnswersValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetClaimsItemByIdQuery(contextProvider, repository);
        }

        public void Validate(SaveClaimsItemAnswersDto dto, ExecutionResult result)
        {
            var Result = GetClaimsItemByIdQuery(dto.Id);

            foreach (var answer in dto.Answers)
            {
                var question = Result.FirstOrDefault().Answers.Where(x => x.Id == answer.Id).FirstOrDefault();

                if (question == null)
                    result.AddValidationMessage(ClaimsItemValidationMessages.InvalidClaimsItemQuestionAnswerId.AddParameters(new[] { answer.Id.ToString() }));
            }
        }

        private IQueryable<ClaimsItem> GetClaimsItemByIdQuery(int id)
        {
            return query.WithId(id).ExecuteQuery();
        }
    }
}