﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iPlatform.Api.DTOs.Claims.ClaimsItems;

namespace Domain.Claims.ClaimsItems.Mappings
{
    public class ClaimsItemMapper: Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ClaimsItem, BasicClaimsItemDto>();
            Mapper.CreateMap<ClaimsItem, ListClaimsItemDto>()
               .ForMember(t => t.ClaimsHeaderId, o => o.MapFrom(s => s.ClaimsHeader.Id))
               .ForMember(t => t.PolicyDescription, o => o.MapFrom(s => s.PolicyItem.ItemDescription))
               .ForMember(t => t.TotalQuestions, o => o.MapFrom(s => s.Answers.Count()))
               .ForMember(t => t.TotalQuestionsAnswered, o => o.MapFrom(s => s.Answers.Where(a => !string.IsNullOrEmpty(a.Answer)).Count()))
               .ForMember(t => t.Answers, o => o.MapFrom(s => s.Answers));
               ;


            Mapper.CreateMap<ClaimsItem, ClaimsItemDto>()
                 .ForMember(t => t.PolicyItemId, o => o.MapFrom(s => s.PolicyItem.Id))
                 .ForMember(t => t.ClaimTypeId, o => o.MapFrom(s => s.ClaimsType.Id))
                 .ForMember(t => t.ClaimsHeaderId, o => o.MapFrom(s => s.ClaimsHeader.Id))
                 .ForMember(t => t.Answers, o => o.Ignore())
                 ;

            Mapper.CreateMap<ClaimsItemQuestionAnswer, ClaimsItemQuestionAnswerDto>()
                ;
        }
    }
}
