﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using MasterData.Authorisation;
using Domain.Claims.ClaimsHeaders;
using Domain.Policies.Queries;
using Domain.Claims.ClaimsHeaders.Queries;
using System.Linq;

namespace Domain.Claims.ClaimsItems.Handlers
{
    public class UpdateClaimsItemHandler : ExistingEntityDtoHandler<ClaimsHeader, UpdateClaimsItemsDto, int>
    {
        public UpdateClaimsItemHandler(IProvideContext contextProvider, IRepository repository)
                : base(contextProvider, repository)
            {
                policyQuery = new GetPolicyByIdQuery(contextProvider, repository);
                claimsQuestionQuery = new GetClaimsQuestionByProductIdQuery(contextProvider, repository);
            }

        private GetPolicyByIdQuery policyQuery;
        private GetClaimsQuestionByProductIdQuery claimsQuestionQuery; 

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ClaimsAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(ClaimsHeader entity, UpdateClaimsItemsDto dto, HandlerResult<int> result)
        {
            List<ProductClaimsQuestionDefinition> productQuestions =
                   claimsQuestionQuery.ByProductId(entity.PolicyHeader.Product.Id).ExecuteQuery().ToList();

            entity.UpdateClaimItems(productQuestions, dto);
            result.Processed(entity.Id);
        }
    }
}