﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using MasterData.Authorisation;

namespace Domain.Claims.ClaimsItems.Handlers
{
    public class DisableClaimsItemHandler : ExistingEntityDtoHandler<ClaimsItem, DisableClaimItemDto, int>
    {
        public DisableClaimsItemHandler(IProvideContext contextProvider, IRepository repository)
                : base(contextProvider, repository)
            {
            }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ClaimsAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(ClaimsItem entity, DisableClaimItemDto dto, HandlerResult<int> result)
        {
            entity.Delete();

            //Audit
            entity.ClaimsHeader.DisableClaimsItem(dto);
            result.Processed(entity.Id);
        }
    }
}