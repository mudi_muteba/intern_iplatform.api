﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Claims.ClaimsItems;
using MasterData.Authorisation;

using Domain.Claims.ClaimsHeaders;
using Domain.Policies.Queries;
using Domain.Claims.ClaimsHeaders.Queries;
using System.Linq;

namespace Domain.Claims.ClaimsItems.Handlers
{
    public class SaveClaimsItemAnswersHandler : ExistingEntityDtoHandler<ClaimsItem, SaveClaimsItemAnswersDto, int>
    {
        public SaveClaimsItemAnswersHandler(IProvideContext contextProvider, IRepository repository)
                : base(contextProvider, repository)
            {
            }


        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ClaimsAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(ClaimsItem entity, SaveClaimsItemAnswersDto dto, HandlerResult<int> result)
        {
            entity.UpdateQuestionAnswers(dto);
            
            //Audit
            entity.ClaimsHeader.SaveClaimsItemAnswer(dto);
            result.Processed(entity.Id);
        }
    }
}