﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Claims.Overrides
{
    public class OrganizationClaimTypeCategoryOverride : IAutoMappingOverride<OrganizationClaimTypeCategory>
    {
        public void Override(AutoMapping<OrganizationClaimTypeCategory> mapping)
        {
            mapping.References(x => x.Organization).Nullable();
        }
    }
}