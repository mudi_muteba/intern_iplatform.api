﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Claims.Overrides
{
    public class OrganizationClaimTypeExclusionOverride : IAutoMappingOverride<OrganizationClaimTypeExclusion>
    {
        public void Override(AutoMapping<OrganizationClaimTypeExclusion> mapping)
        {
            mapping.References(x => x.Organization).Nullable();
        }
    }
}