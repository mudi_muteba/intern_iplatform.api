﻿using Domain.Base;
using Domain.Organizations;
using MasterData;

namespace Domain.Claims
{
    public class OrganizationClaimTypeCategory : Entity
    {
        public OrganizationClaimTypeCategory()
        {
        }

        public virtual Organization Organization { get; protected set; }
        public virtual ClaimsType ClaimsType { get; protected set; }
        public virtual ClaimsTypeCategory ClaimsTypeCategory { get; protected set; }
        public virtual int VisibleIndex { get; protected set; }
        public virtual string CategoryDisplayName { get; protected set; }
        public virtual string ClaimTypeDisplayName { get; protected set; }

    }
}
