﻿using Domain.Base;
using Domain.Organizations;
using MasterData;

namespace Domain.Claims
{
    public class OrganizationClaimTypeExclusion : Entity
    {
        public OrganizationClaimTypeExclusion()
        {
        }

        public virtual Organization Organization { get; protected set; }
        public virtual ClaimsType ClaimsType { get; protected set; }
    }
}
