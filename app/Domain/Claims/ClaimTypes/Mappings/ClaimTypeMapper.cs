﻿using AutoMapper;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Claims.ClaimTypes;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using Domain.Base.Execution;
using MasterData;

namespace Domain.Claims.ClaimTypes.Mappings
{
    public class ClaimTypeMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<HandlerResult<List<ClaimsTypeDto>>, GETResponseDto<List<ClaimsTypeDto>>>()
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response));

            Mapper.CreateMap<ClaimsTypeCategory, ClaimsTypeCategoryDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.Name, o => o.MapFrom(s => s.Name))
                .ForMember(t => t.DisplayName, o => o.Ignore())
                ;
        }
    }
}
