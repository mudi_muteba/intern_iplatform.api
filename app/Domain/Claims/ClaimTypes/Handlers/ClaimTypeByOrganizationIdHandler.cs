﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Claims.ClaimTypes;
using MasterData;
using System.Linq;
using AutoMapper;

namespace Domain.Claims.Claimstypes.Handlers
{
    public class ClaimTypeByOrganizationIdHandler : BaseDtoHandler<ClaimTypeSearchDto, List<ClaimsTypeDto>>
    {
        public ClaimTypeByOrganizationIdHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            this.repository = repository;
        }

        private IRepository repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void InternalHandle(ClaimTypeSearchDto dto, HandlerResult<List<ClaimsTypeDto>> result)
        {
            var claimsTypeDtos = new ClaimsTypes()
                .Select(ct => new ClaimsTypeDto(ct.Id, ct.Name));

            if (dto.OrganizationId != null && dto.OrganizationId != 0)
            {
                var exclusions = repository.GetAll<OrganizationClaimTypeExclusion>().Where(x => x.Organization.Id == dto.OrganizationId).ToList();

                claimsTypeDtos = claimsTypeDtos.Where(x => !exclusions.Select(y => y.ClaimsType.Id).Contains(x.Id));
                claimsTypeDtos = GetOrganisationCoverInfo(dto.OrganizationId.Value, claimsTypeDtos.ToList());
            }
            else
            {
                var exclusions = repository.GetAll<OrganizationClaimTypeExclusion>().Where(x => x.Organization == null).ToList();

                claimsTypeDtos = claimsTypeDtos.Where(x => !exclusions.Select(y => y.ClaimsType.Id).Contains(x.Id));
                claimsTypeDtos = GetOrganisationCoverInfo(dto.OrganizationId, claimsTypeDtos.ToList());
            }

            result.Processed(claimsTypeDtos.OrderBy(x => x.ClaimsTypeCategory.Name).ThenBy(x => x.VisibleIndex).ToList());
        }


        private IEnumerable<ClaimsTypeDto> GetOrganisationCoverInfo(int? organizationid, List<ClaimsTypeDto> claimsTypeDtos)
        {
            List<OrganizationClaimTypeCategory> orgCategories = new List<OrganizationClaimTypeCategory>();

            if(organizationid > 0 )
                orgCategories = repository.GetAll<OrganizationClaimTypeCategory>().Where(x => x.Organization.Id == organizationid).ToList();
            else
                orgCategories = repository.GetAll<OrganizationClaimTypeCategory>().Where(x => x.Organization == null).ToList();

            if (orgCategories.Any())
            {
                claimsTypeDtos.ForEach(x => {
                    var orgCategory = orgCategories.FirstOrDefault(c => c.ClaimsType.Id == x.Id);
                    if(orgCategory != null)
                    {
                        x.ClaimsTypeCategory = Mapper.Map<ClaimsTypeCategoryDto>(orgCategory.ClaimsTypeCategory);
                        x.ClaimsTypeCategory.DisplayName = orgCategory.CategoryDisplayName;
                        x.DisplayName = orgCategory.ClaimTypeDisplayName;
                        x.VisibleIndex = orgCategory.VisibleIndex;
                    }
                });
            }
            return claimsTypeDtos;
        }
    }
}