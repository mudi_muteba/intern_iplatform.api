﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Claims.ClaimTypes;
using MasterData;
using System.Linq;
using Domain.Claims.ClaimsHeaders;
using iPlatform.Api.DTOs.Claims.Questions;
using iPlatform.Api.DTOs.Claims;

namespace Domain.Claims.Claimstypes.Handlers
{
    public class GetClaimTypeByProductDtoHandler : BaseDtoHandler<GetClaimTypeByProductDto, ClaimsTypeDetailDto>
    {
        public GetClaimTypeByProductDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            this.repository = repository;
        }

        private IRepository repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void InternalHandle(GetClaimTypeByProductDto dto, HandlerResult<ClaimsTypeDetailDto> result)
        {
            var match = new ClaimsTypes().FirstOrDefault(ct => ct.Id == dto.Id);

            if (match == null)
            {
                result.Processed(null);
                return;
            }

            ClaimsTypeDetailDto claimTypeDetailsDto = null;
            if (dto.ProductId != null && dto.ProductId != 0)
            {
                var response = repository.GetAll<ProductClaimsQuestionDefinition>().Where(x => x.Product.Id == dto.ProductId);

                var claimsquestions = from def in new ClaimsQuestionDefinitions()
                                      join question in new ClaimsQuestions() on def.ClaimsQuestion.Id equals question.Id
                                      join prd in response on def.Id equals prd.ClaimsQuestionDefinition.Id
                                      where def.ClaimsTypeId == dto.Id && def.Id == prd.ClaimsQuestionDefinition.Id
                                      orderby def.VisibleIndex, def.ClaimsQuestionGroup.VisibleIndex
                                      select GetProductClaimsQuestionDefinitionDto(prd);

                claimTypeDetailsDto = BuildGroupClaimQuestions(match, claimsquestions.GroupBy(x => x.ClaimsQuestionGroup.Name));
            }
            else
            {
                var claimsquestions = from def in new ClaimsQuestionDefinitions()
                            join question in new ClaimsQuestions() on def.ClaimsQuestion.Id equals question.Id
                            where def.ClaimsTypeId == dto.Id
                            orderby def.VisibleIndex, def.ClaimsQuestionGroup.VisibleIndex
                            select GetProductClaimsQuestionDefinitionDto(def);

                claimTypeDetailsDto = BuildGroupClaimQuestions(match, claimsquestions.GroupBy(x => x.ClaimsQuestionGroup.Name));
            }
            result.Processed(claimTypeDetailsDto);
        }

        private ProductClaimsQuestionDefinitionDto GetProductClaimsQuestionDefinitionDto(ProductClaimsQuestionDefinition prdClaimsQuestion)
        {

            var parentprd = prdClaimsQuestion.ParentProductClaimQuestionDefinition;
            return new ProductClaimsQuestionDefinitionDto() {
                Id = prdClaimsQuestion.Id,
                Name = prdClaimsQuestion.ClaimsQuestionDefinition.Name,
                ClaimsQuestion = prdClaimsQuestion.ClaimsQuestionDefinition.ClaimsQuestion,
                ClaimsQuestionGroup = prdClaimsQuestion.ClaimsQuestionGroup,
                VisibleIndex = prdClaimsQuestion.VisibleIndex,
                DisplayName = prdClaimsQuestion.DisplayName,
                FirstPhase = prdClaimsQuestion.FirstPhase,
                SecondPhase = prdClaimsQuestion.SecondPhase,
                Required = prdClaimsQuestion.Required,
                ClaimsTypeId = prdClaimsQuestion.ClaimsQuestionDefinition.ClaimsTypeId,
                DefaultValue = prdClaimsQuestion.ClaimsQuestionDefinition.DefaultValue,
                RegexPattern = prdClaimsQuestion.ClaimsQuestionDefinition.RegexPattern,
                ToolTip = prdClaimsQuestion.ClaimsQuestionDefinition.ToolTip,
                ParentProductClaimsQuestionDefinitionId = parentprd == null ? 0 : parentprd.Id,
            };
        }
        private ProductClaimsQuestionDefinitionDto GetProductClaimsQuestionDefinitionDto(ClaimsQuestionDefinition claimsQuestion)
        {
            return new ProductClaimsQuestionDefinitionDto()
            {
                Id = claimsQuestion.Id,
                Name = claimsQuestion.Name,
                ClaimsQuestion = claimsQuestion.ClaimsQuestion,
                ClaimsQuestionGroup = claimsQuestion.ClaimsQuestionGroup,
                VisibleIndex = claimsQuestion.VisibleIndex,
                DisplayName = claimsQuestion.DisplayName,
                FirstPhase = claimsQuestion.FirstPhase,
                SecondPhase = claimsQuestion.SecondPhase,
                Required = claimsQuestion.Required,
                ClaimsTypeId = claimsQuestion.ClaimsTypeId,
                DefaultValue = claimsQuestion.DefaultValue,
                RegexPattern = claimsQuestion.RegexPattern,
                ToolTip = claimsQuestion.ToolTip,
                ParentProductClaimsQuestionDefinitionId = 0,
            };
        }
        private ClaimsTypeDetailDto BuildGroupClaimQuestions(ClaimsType match, IEnumerable<IGrouping<string, ProductClaimsQuestionDefinitionDto>> grouping)
        {
            var questionsByGroups = new List<ClaimsQuestionGroupDto>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var group in grouping)
            {
                if (!group.Any())
                    continue;

                var groupId = group.First().ClaimsQuestionGroup.Id;
                var groupname = group.First().ClaimsQuestionGroup.Name;

                var questions = @group.Select(g =>
                {
                    var claimsQuestionTypeDto = new ClaimsQuestionTypeDto(g.ClaimsQuestion.ClaimsQuestionType.Id, g.ClaimsQuestion.ClaimsQuestionType.Name);
                    var mobileclaimsQuestionTypeDto = new ClaimsQuestionTypeDto(g.ClaimsQuestion.MobileClaimsQuestionType.Id, g.ClaimsQuestion.MobileClaimsQuestionType.Name);

                    var possibleAnswers = new ClaimsQuestionAnswers().Where(q => q.ClaimsQuestion.Id == g.ClaimsQuestion.Id)
                        .Select(a => new ClaimsQuestionAnswerDto(a.Id, a.Name, a.Answer));
                        
                    return new ClaimsQuestionDto(g.Id, g.Name, g.DisplayName, claimsQuestionTypeDto, mobileclaimsQuestionTypeDto,  g.VisibleIndex,  g.Required, g.FirstPhase, g.SecondPhase, possibleAnswers.ToList(), g.ParentProductClaimsQuestionDefinitionId);
                });

                questionsByGroups.Add(new ClaimsQuestionGroupDto(groupId, groupname, questions.ToList()));
            }

            return new ClaimsTypeDetailDto(new ClaimsTypeDto(match.Id, match.Name), new ClaimsQuestionStructureDto(questionsByGroups), PhaseRequired(grouping));
        }
        private ClaimsPhasesRequiredDto PhaseRequired(IEnumerable<IGrouping<string, ProductClaimsQuestionDefinitionDto>> grouping)
        {
            var firstPhase = false;
            var secondPhase = false;

            firstPhase = grouping.Any(g => g.Any(q => q.FirstPhase == true));
            secondPhase = grouping.Any(g => g.Any(q => q.SecondPhase == true));

            return new ClaimsPhasesRequiredDto(firstPhase, secondPhase);
        }
    }
}