﻿
using Domain.Base;
namespace Domain.Campaigns
{
    public class CampaignReference : Entity
    {
        public CampaignReference()
        {
        }

        public CampaignReference(Campaign campaign, string name)
        {
            Name = name;
            Campaign = campaign;
        }

        public virtual Campaign Campaign { get; set; }
        public virtual string Name { get; set; }
        public virtual string Reference { get; set; }
    }
}