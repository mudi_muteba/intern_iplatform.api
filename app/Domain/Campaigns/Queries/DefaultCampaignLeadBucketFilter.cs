﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Campaigns.Queries
{
    public class DefaultCampaignLeadBucketFilter : IApplyDefaultFilters<CampaignLeadBucket>
    {
        public IQueryable<CampaignLeadBucket> Apply(ExecutionContext executionContext, IQueryable<CampaignLeadBucket> query)
        {
            return query
                .Where(c => c.IsDeleted == false && !c.Lead.IsDeleted);
        }
    }
}