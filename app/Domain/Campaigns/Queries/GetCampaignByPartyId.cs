﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Campaigns.Queries
{
    public class GetCampaignByPartyId : BaseQuery<Campaign>
    {
        private int _partyId;

        public GetCampaignByPartyId(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new DefaultCampaignFilters()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetCampaignByPartyId ByPartyId(int id)
        {
            _partyId = id;
            return this;
        }

        protected internal override IQueryable<Campaign> Execute(IQueryable<Campaign> query)
        {
            return query.Where(c => c.Agents.Any(a => a.Party.Id == _partyId));
        }
    }
}