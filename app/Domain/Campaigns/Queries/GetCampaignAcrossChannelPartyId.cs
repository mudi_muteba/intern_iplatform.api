﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Campaigns.Queries
{
    public class GetCampaignAcrossChannelPartyId : BaseQuery<Campaign>, IChannelFreeQuery
    {
        private int _partyId;

        public GetCampaignAcrossChannelPartyId(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new DefaultCampaignFilters()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetCampaignAcrossChannelPartyId ByPartyId(int partyId)
        {
            _partyId = partyId;
            return this;
        }

        protected internal override IQueryable<Campaign> Execute(IQueryable<Campaign> query)
        {
            var result =  query.Where(c => c.Agents.Any(a => a.Party.Id == _partyId) || c.Managers.Any(m => m.User.UserIndividuals.Any(ui => ui.Individual.Id == _partyId)));
            return result;
        }
    }
}