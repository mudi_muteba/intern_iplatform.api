﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Campaigns.Queries
{
    public class GetCampaignByNameQuery : BaseQuery<Campaign>
    {
        private string name;

        public GetCampaignByNameQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultCampaignFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    CampaignAuthorisation.List
                };
            }
        }

        public GetCampaignByNameQuery ByName(string name)
        {
            this.name = name;
            return this;
        }

        protected internal override IQueryable<Campaign> Execute(IQueryable<Campaign> query)
        {
            return query
                .Where(c => c.Name == name);
        }
    }
}