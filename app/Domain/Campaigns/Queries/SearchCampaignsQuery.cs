﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;

namespace Domain.Campaigns.Queries
{
    public class SearchCampaignsQuery : BaseQuery<Campaign>, ISearchQuery<CampaignSearchDto>
    {
        private CampaignSearchDto criteria;

        public SearchCampaignsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultCampaignFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    CampaignAuthorisation.List
                };
            }
        }

        public void WithCriteria(CampaignSearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<Campaign> Execute(IQueryable<Campaign> query)
        {

            if (criteria.Tags != null && criteria.Tags.Count > 0)
                query = query.Where(a => a.Tags.Any(x => criteria.Tags.Contains(x.Tag.Name)));

            if (!string.IsNullOrWhiteSpace(criteria.Name))
            {
                query = query.Where(a => a.Name.StartsWith(criteria.Name));
            }

            return query;
        }
    }
}