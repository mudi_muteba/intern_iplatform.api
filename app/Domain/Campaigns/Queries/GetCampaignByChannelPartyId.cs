﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Campaigns.Queries
{
    public class GetCampaignByChannelPartyId : BaseQuery<Campaign>
    {
        private int _partyId;
        private int _channelId;

        public GetCampaignByChannelPartyId(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new DefaultCampaignFilters()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetCampaignByChannelPartyId ByChannelPartyId(int partyId, int channelId)
        {
            _partyId = partyId;
            _channelId = channelId;
            return this;
        }

        protected internal override IQueryable<Campaign> Execute(IQueryable<Campaign> query)
        {
            return query.Where(c => c.Channel.Id == _channelId && c.Agents.Any(a => a.Party.Id == _partyId) || c.Managers.Any(m => m.User.UserIndividuals.Any(ui => ui.Individual.Id == _partyId)));
        }
    }
}