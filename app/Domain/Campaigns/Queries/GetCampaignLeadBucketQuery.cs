﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Campaigns.Queries
{
    public class GetCampaignLeadBucketQuery : BaseQuery<CampaignLeadBucket>
    {
        private int _CampaignId;
        private int _LeadId;
        public GetCampaignLeadBucketQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultCampaignLeadBucketFilter())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public GetCampaignLeadBucketQuery WithDetails(int campaignid, int leadid)
        {
            _CampaignId = campaignid;
            _LeadId = leadid;
            return this;
        }

        protected internal override IQueryable<CampaignLeadBucket> Execute(IQueryable<CampaignLeadBucket> query)
        {
            var result = query
                .Where(c => c.Campaign.Id == _CampaignId && c.Lead.Id == _LeadId);

            return result;
        }
    }
}