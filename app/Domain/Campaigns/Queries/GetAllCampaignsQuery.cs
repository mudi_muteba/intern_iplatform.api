﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Campaigns.Queries
{
    public class GetAllCampaignsQuery : BaseQuery<Campaign>
    {
        public GetAllCampaignsQuery(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository, new DefaultCampaignFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    CampaignAuthorisation.List
                };
            }
        }

        protected internal override IQueryable<Campaign> Execute(IQueryable<Campaign> query)
        {
            return query;
        }
    }
}