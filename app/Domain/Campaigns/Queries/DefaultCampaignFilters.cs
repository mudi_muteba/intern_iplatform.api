﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Campaigns.Queries
{
    public class DefaultCampaignFilters : IApplyDefaultFilters<Campaign>
    {
        public IQueryable<Campaign> Apply(ExecutionContext executionContext, IQueryable<Campaign> query)
        {
            return query
                .Where(c => c.IsDeleted == false);
        }
    }
}