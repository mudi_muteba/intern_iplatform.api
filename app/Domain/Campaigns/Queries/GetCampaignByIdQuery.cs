﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Campaigns.Queries
{
    public class GetCampaignByIdQuery : BaseQuery<Campaign>, IChannelFreeQuery
    {
        private int id;

        public GetCampaignByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultCampaignFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    CampaignAuthorisation.List
                };
            }
        }

        public GetCampaignByIdQuery ById(int id)
        {
            this.id = id;
            return this;
        }

        protected internal override IQueryable<Campaign> Execute(IQueryable<Campaign> query)
        {
            return query
                .Where(c => c.Id == id);
        }
    }
}