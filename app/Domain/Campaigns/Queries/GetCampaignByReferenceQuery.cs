﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Campaigns.Queries
{
    public class GetCampaignByReferenceQuery : BaseQuery<CampaignReference>
    {
        private string _reference;

        public GetCampaignByReferenceQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultCampaignReferenceFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    CampaignAuthorisation.List
                };
            }
        }

        public GetCampaignByReferenceQuery WithReference(string reference)
        {
            _reference = reference;
            return this;
        }

        protected internal override IQueryable<CampaignReference> Execute(IQueryable<CampaignReference> query)
        {
            return query
                .Where(c => c.Reference == _reference);
        }
    }
}