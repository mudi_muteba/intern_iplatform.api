﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Campaigns.Queries
{
    public class GetCampaignByChannelId : BaseQuery<Campaign>
    {
        private int _id;

        public GetCampaignByChannelId(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new DefaultCampaignFilters()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public GetCampaignByChannelId ByChannelId(int id)
        {
            _id = id;
            return this;
        }

        protected internal override IQueryable<Campaign> Execute(IQueryable<Campaign> query)
        {
            return query.Where(c => c.Channel.Id == _id);
        }
    }
}