using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Campaigns.Queries
{
    public class DefaultCampaignReferenceFilters : IApplyDefaultFilters<CampaignReference>
    {
        public IQueryable<CampaignReference> Apply(ExecutionContext executionContext, IQueryable<CampaignReference> query)
        {
            return query
                .Where(c => c.IsDeleted == false);
        }
    }
}