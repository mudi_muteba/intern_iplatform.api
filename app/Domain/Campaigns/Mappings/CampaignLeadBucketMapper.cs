﻿using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using System;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Campaigns;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Individual;
using iPlatform.Api.DTOs.Leads;
using MasterData;
using NHibernate.Proxy;

namespace Domain.Campaigns.Mappings
{
    public class CampaignLeadBucketMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CampaignLeadBucket, CampaignLeadBucketDto>()
                .ForMember(t => t.AgentId, o => o.MapFrom(s => s.Agent.Id))
                .ForMember(t => t.CampaignId, o => o.MapFrom(s => s.Campaign.Id))
                .ForMember(t => t.Lead, o => o.Ignore())
                .ForMember(t => t.Histories, o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    d.Histories.AddRange(s.Histories.Select(Mapper.Map<CampaignLeadBucketVerboseDto>));

                    var individualProxy = s.Lead.Party as INHibernateProxy;

                    if (individualProxy != null)
                        d.Lead =
                            Mapper.Map<ListIndividualDto>(individualProxy.HibernateLazyInitializer.GetImplementation());
                    else
                         d.Lead =
                            Mapper.Map<ListIndividualDto>(s.Lead.Party);

                })
                ;

            Mapper.CreateMap<CampaignLeadBucketVerbose, CampaignLeadBucketVerboseDto>()
                .ForMember(t => t.AgentId, o => o.MapFrom(s => s.Agent.Id))
                .ForMember(t => t.CampaignLeadBucketId, o => o.MapFrom(s => s.CampaignLeadBucket.Id))
                ;

            Mapper.CreateMap<List<CampaignLeadBucketVerbose>, List<CampaignLeadBucketVerboseDto>>()
                ;

            Mapper.CreateMap<Party.Party, ListIndividualDto>()
                ;

            Mapper.CreateMap<LeadCallCentreCode, LeadCallCentreCodeDto>()
                ;

            Mapper.CreateMap<CampaignLeadBucket, ListCampaignLeadBucketDto>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.Campaign.Channel.Id)))
                .ForMember(t => t.AgentId, o => o.MapFrom(s => s.Agent.Id))
                .ForMember(t => t.CampaignId, o => o.MapFrom(s => s.Campaign.Id))
                .ForMember(t => t.Histories, o => o.Ignore())
                .ForMember(t => t.Agent, o => o.MapFrom(s => s.Agent))
                .AfterMap((s, d) =>
                {
                    d.Histories.AddRange(s.Histories.Select(Mapper.Map<CampaignLeadBucketVerboseDto>));
                })
                ;
        }
    }
}