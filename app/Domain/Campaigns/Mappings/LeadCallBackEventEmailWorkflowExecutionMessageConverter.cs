using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using Domain.Admin.Channels.EmailCommunicationSettings;

using Domain.Base.Repository;

using Domain.Campaigns.Events;

using Domain.Emailing;
using Domain.Emailing.Factory;

using Domain.Individuals;

using Shared.Extentions;

namespace Domain.Campaigns.Mappings
{
    public class LeadCallBackEventEmailWorkflowExecutionMessageConverter : TypeConverter<LeadCallBackEvent, EmailWorkflowExecutionMessage>
    {
        private readonly IRepository _repository;

        public LeadCallBackEventEmailWorkflowExecutionMessageConverter(IRepository repository)
        {
            _repository = repository;
        }

        protected override EmailWorkflowExecutionMessage ConvertCore(LeadCallBackEvent source)
        {
           
            var campaignLeadBucket = _repository.GetById<CampaignLeadBucket>(source.CampaignLeadBucketId);

            if (campaignLeadBucket == null)
            {
                this.Error(() => string.Format("Could not get {0} {1}", typeof(CampaignLeadBucket), source.CampaignLeadBucketId));
                return null;
            }

            if (!campaignLeadBucket.Callback)
                return null;

            //added email settings from channelid
            var emailCommunicationSetting = _repository.GetAll<EmailCommunicationSetting>().FirstOrDefault(x => x.Id == source.ChannelId);
            if (emailCommunicationSetting == null)
            {
                this.Error(() => string.Format("Could not get {0} {1}", typeof(EmailCommunicationSetting), source.ChannelId));
                return null;
            }

            var appSettingsEmailSettings = Mapper.Map<AppSettingsEmailSetting>(emailCommunicationSetting);
            var individual = campaignLeadBucket.Lead.Party.As<Individual>();
            var data = new Dictionary<string, string>
            {
                {"UserName", "User"},
                {"Lead", string.Format("{0} {1} ({2})", individual.FirstName, individual.Surname, campaignLeadBucket.Lead.Id)},
                {"CallBackDate", campaignLeadBucket.CallbackDate.ToLocalTime().ToString() },
            };

            var emailCommunicationMessage = new EmailCommunicationMessage(
                @"Escalation",
                new CommunicationMessageData(data),
                string.Join(",", campaignLeadBucket.CampaignManagerEmailAddresses()),
                "Lead call back escalation",
                appSettingsEmailSettings
                );

            return new EmailWorkflowExecutionMessage(emailCommunicationMessage);
        }
    }

    public static class EmailSettingExtensions
    {
        public static AppSettingsEmailSetting Get(this EmailCommunicationSetting setting)
        {
            return new AppSettingsEmailSetting
            {
                CustomFrom = "",
                DefaultBCC = setting.DefaultBCC,
                DefaultContactNumber = setting.DefaultContactNumber,
                DefaultFrom = setting.DefaultFrom,
                Host = setting.Host,
                Password = setting.Password,
                Port = setting.Port,
                SubAccountID = setting.SubAccountID,
                UseDefaultCredentials = setting.UseDefaultCredentials,
                UserName = setting.UserName,
                UseSSL = setting.UseSSL
            };
        }
    }


}