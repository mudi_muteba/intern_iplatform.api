﻿using System.Linq;
using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Campaigns;

namespace Domain.Campaigns.Mappings
{
    public class CampaignReferenceConverter : TypeConverter<CampaignDto, CampaignReference>
    {
        private readonly IRepository _repository;

        public CampaignReferenceConverter(IRepository repository)
        {
            _repository = repository;
        }

        protected override CampaignReference ConvertCore(CampaignDto source)
        {
            var campaign = Mapper.Map<int, Campaign>(source.Id);
            if (campaign == null)
                return null;

            var campaignReference = _repository.GetAll<CampaignReference>().FirstOrDefault(x => x.Campaign.Id.Equals(campaign.Id)) ?? new CampaignReference(campaign, campaign.Name);

            return campaignReference;
        }
    }
}