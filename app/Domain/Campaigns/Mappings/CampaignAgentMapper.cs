﻿
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Campaigns;

namespace Domain.Campaigns.Mappings
{
    public class CampaignAgentMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CampaignAgent, CampaignAgentDto>();
        }
    }
}