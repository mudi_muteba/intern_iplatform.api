﻿using AutoMapper;
using iPlatform.Api.DTOs.Campaigns;

namespace Domain.Campaigns.Mappings
{
    public class CampaignSimilarCampaignMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CampaignSimilarCampaign, CampaignSimilarCampaignDto>();
        }
    }
}