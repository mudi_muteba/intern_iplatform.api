﻿using AutoMapper;
using Domain.Campaigns.Events;
using Domain.Emailing;

namespace Domain.Campaigns.Mappings
{
    public class LeadCallBackMap : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<LeadCallBackEvent, EmailWorkflowExecutionMessage>()
                .ConvertUsing<ITypeConverter<LeadCallBackEvent, EmailWorkflowExecutionMessage>>();
        }
    }
}