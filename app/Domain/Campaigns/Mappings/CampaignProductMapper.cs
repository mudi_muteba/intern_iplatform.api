﻿
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Products;

namespace Domain.Campaigns.Mappings
{
    public class CampaignProductMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CampaignProduct, CampaignProductDto>();

        }
    }
}