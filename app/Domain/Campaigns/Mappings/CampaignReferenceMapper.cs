﻿using AutoMapper;
using iPlatform.Api.DTOs.Campaigns;

namespace Domain.Campaigns.Mappings
{
    public class CampaignReferenceMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CampaignReference, CampaignReferenceDto>();

            Mapper.CreateMap<CampaignDto, CampaignReference>()
                .ConvertUsing<ITypeConverter<CampaignDto, CampaignReference>>();

            Mapper.CreateMap<CreateCampaignReferenceDto, CampaignReference>()
                .ForMember(t => t.Campaign, o => o.MapFrom(s => Mapper.Map<int, Campaign>(s.CampaignId)));
                ;
        }
    }
}