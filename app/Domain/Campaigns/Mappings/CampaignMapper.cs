﻿using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using System;
using System.Linq;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Campaigns;
using System.Collections.Generic;

namespace Domain.Campaigns.Mappings
{


    public class CampaignDefaultDto
    {
        public CampaignDefaultDto()
        {
        }

        public CampaignDefaultDto(int channelId)
        {
            ChannelId = channelId;
        }
        public int ChannelId { get; set; }
    }

    public class DefaultCampaignConverter : TypeConverter<CampaignDefaultDto, Campaign>
    {
        private readonly IRepository _repository;

        public DefaultCampaignConverter(IRepository repository)
        {
            _repository = repository;
        }

        protected override Campaign ConvertCore(CampaignDefaultDto source)
        {
            return _repository.GetAll<Campaign>().OrderBy(x => x.Id).FirstOrDefault(x => !x.IsDisabled && x.DefaultCampaign && x.Channel.Id == source.ChannelId);
        }
    } 

    public class CampaignMapper : Profile
    {
        protected override void Configure()
        {

            Mapper.CreateMap<CampaignDefaultDto, Campaign>()
                .ConvertUsing<ITypeConverter<CampaignDefaultDto, Campaign>>()
                ;

            Mapper.CreateMap<Campaign, ListCampaignDto>()
                ;

            Mapper.CreateMap<Campaign, BasicCampaignDto>()
                ;

            Mapper.CreateMap<List<Campaign>, ListResultDto<ListCampaignDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<PagedResults<Campaign>, PagedResultDto<ListCampaignDto>>();

            Mapper.CreateMap<Campaign, CampaignDto>()
                .ForMember(t => t.Events, o => o.MapFrom(s => s.Audit))
                .ForMember(t => t.Managers, o => o.MapFrom(s => s.Managers.Select(x => x.User.Id)))
                ;

            Mapper.CreateMap<CreateCampaignDto, Campaign>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel(s.ChannelId)))
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.DateCreated, o => o.Ignore())
                .ForMember(t => t.DateUpdated, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.IsUsed, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.Products, o => o.Ignore())
                .ForMember(t => t.Tags, o => o.Ignore())
                .ForMember(t => t.Agents, o => o.Ignore())
                .ForMember(t => t.References, o => o.Ignore())
                .ForMember(t => t.SimilarCampaigns, o => o.Ignore())
                ;

            Mapper.CreateMap<EditCampaignDto, Campaign>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel(s.ChannelId)))
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.DateCreated, o => o.Ignore())
                .ForMember(t => t.DateUpdated, o => o.MapFrom(s => DateTime.UtcNow))
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.IsUsed, o => o.Ignore())
                .ForMember(t => t.SimpleAudit, o => o.Ignore())
                .ForMember(t => t.Products, o => o.Ignore())
                .ForMember(t => t.Tags, o => o.Ignore())
                .ForMember(t => t.Agents, o => o.Ignore())
                .ForMember(t => t.Managers, o => o.Ignore())
                .ForMember(t => t.References, o => o.Ignore())
                .ForMember(t => t.SimilarCampaigns, o => o.Ignore())
                ;


            Mapper.CreateMap<Campaign, CampaignInfoDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.SourceReference, o => o.MapFrom(s => s.Reference))
                ;
        }
    }
}