using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using Domain.Base;
using Domain.Campaigns.Events;
using Domain.Leads;
using Domain.Leads.Extensions;
using Domain.Users;

using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Leads.Campaign;

using Infrastructure.NHibernate.Attributes;

using MasterData;

namespace Domain.Campaigns
{
    public class CampaignLeadBucket : EntityWithAudit
    {
        public CampaignLeadBucket()
        {
            Callback = false;
            CallbackDate = DateTime.UtcNow;
            Comments = "";
            CreatedOn = DateTime.UtcNow;
            Description = "N/A";
            IsDeleted = false;
            LeadCallCentreCode = LeadCallCentreCodes.LeadNotAllocated;
            ModifiedOn = DateTime.UtcNow;
            Histories = new List<CampaignLeadBucketVerbose>();
        }

        public virtual Campaign Campaign { get; set; }
        public virtual Lead Lead { get; set; }
        public virtual User Agent { get; set; }
        public virtual LeadCallCentreCode LeadCallCentreCode { get; set; }
        public virtual bool Callback { get; set; }
        public virtual DateTime CallbackDate { get; set; }
        public virtual string Comments { get; set; }
        public virtual string Description { get; set; }
        public virtual int CallbackCount { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual DateTime ModifiedOn { get; set; }
        public virtual IList<CampaignLeadBucketVerbose> Histories { get; set; }

        [DoNotMap]
        public virtual IEnumerable<string> CampaignManagerEmailAddresses()
        {
            return Campaign != null ? Campaign.ManagerEmailAddresses() : Enumerable.Empty<string>();
        }

        public static CampaignLeadBucket Create(int campaignId, int leadId)
        {
            var entity =  new CampaignLeadBucket
            {
                Campaign = new Campaign {Id = campaignId},
                Lead = new Lead(leadId)
            };

            entity.Histories.Add(CampaignLeadBucketVerbose.Create(entity)); 

            return entity;
        }

        public static CampaignLeadBucket Create(int campaignId, Lead lead)
        {
            var name = lead.Party.DisplayName;
            var description = LeadCallCentreCodes.LeadNotAllocated.EventTemplate;
                description = description.Replace("{LEAD}", name);

            var entity = new CampaignLeadBucket
            {
                Campaign = new Campaign { Id = campaignId },
                Lead = lead,
                Description = description
            };

            entity.Histories.Add(CampaignLeadBucketVerbose.Create(entity));

            return entity;
        }

        #region Update Bucket Status

        public virtual void SetAsAllocated(int userId, CallCentreEventDetailsDto eventDetails)
        {
            var code = Mapper.Map<LeadCallCentreCodeDto>(LeadCallCentreCodes.LeadAllocated);
            var lastCallBackCount = GetLastCallBackCount();

            //Update Bucket
            LeadCallCentreCode = LeadCallCentreCodes.LeadAllocated;
            code.UpdateEventTemplate(eventDetails);
            ModifiedOn = DateTime.UtcNow;
            Description = code.EventTemplate;
            Comments = code.Description;
            Callback = code.ShouldAllowCallRescheduling;
            CallbackCount = code.ShouldAllowCallRescheduling ? lastCallBackCount + 1 : lastCallBackCount;
            CallbackDate = DateTime.UtcNow;
            Agent = new User { Id = userId };

            //Add History
            Histories.Add(new CampaignLeadBucketVerbose
            {
                Agent = new User { Id = userId },
                Description = code.EventTemplate,
                Comments = code.Description,
                CampaignLeadBucket = this,
                LeadCallCentreCode = LeadCallCentreCodes.LeadAllocated,
                Callback = code.ShouldAllowCallRescheduling,
                CallbackCount = code.ShouldAllowCallRescheduling ? lastCallBackCount + 1 : lastCallBackCount,
                CallbackDate = DateTime.UtcNow
            });

            return;
        }

        public virtual void SetAsReallocated(int userId, CallCentreEventDetailsDto eventDetails)
        {
            var code = Mapper.Map<LeadCallCentreCodeDto>(LeadCallCentreCodes.LeadReallocated);
            var lastCallBackCount = GetLastCallBackCount();

            //Update Bucket
            LeadCallCentreCode = LeadCallCentreCodes.LeadReallocated;
            code.UpdateEventTemplate(eventDetails);
            ModifiedOn = DateTime.UtcNow;
            Description = code.EventTemplate;
            Comments = code.Description;
            Callback = code.ShouldAllowCallRescheduling;
            CallbackCount = code.ShouldAllowCallRescheduling ? lastCallBackCount + 1 : lastCallBackCount;
            CallbackDate = DateTime.UtcNow;
            Agent = new User { Id = userId };

            //Add History
            Histories.Add(new CampaignLeadBucketVerbose
            {
                Agent = new User { Id = userId },
                Description = code.EventTemplate,
                Comments = code.Description,
                CampaignLeadBucket = this,
                LeadCallCentreCode = LeadCallCentreCodes.LeadReallocated,
                Callback = code.ShouldAllowCallRescheduling,
                CallbackCount = code.ShouldAllowCallRescheduling ? lastCallBackCount + 1 : lastCallBackCount,
                CallbackDate = DateTime.UtcNow
            });

            return;
        }

        public virtual void UpdateStatus(UpdateLeadStatusDto dto, CallCentreEventDetailsDto eventDetails)
        {
            //Update Histories Recursive Loop
            UpdateHistories(dto, eventDetails);

            //Update bucket
            UpdateBucket(dto, eventDetails);

            //max call back reached delete campaign bucket lead
            if (CallbackCount > 0 && CallbackCount >= Campaign.MaximumLeadCallbackLimit && Campaign.MaximumLeadCallbackLimit > 0)
            {
                //Delete bucket as well
                Delete();
                //Get MaximumContactAttempts
                dto.CallCentreCode = LeadCallCentreCodes.MaximumContactAttemptsReached;
                //Update Bucket
                UpdateBucket(dto, eventDetails);
                //Update Histories
                UpdateHistories(dto, eventDetails);
            }
            else // Escalate to follow up on leads to be called back
                if (dto.CallCentreCode == LeadCallCentreCodes.CallBackLater) 
                    RaiseEvent(new LeadCallBackEvent(Id, CallbackDate,this.Campaign.Channel.Id));

            //TODO: IsFinalStatus

            //Update LeadActivity
            UpdateLeadActivity(dto);
        }
        private void UpdateBucket(UpdateLeadStatusDto dto, CallCentreEventDetailsDto eventDetails)
        {
            var code = Mapper.Map<LeadCallCentreCodeDto>(dto.CallCentreCode);
            dto.CallCentreCodeDto = code;
            code.UpdateEventTemplate(eventDetails);
            LeadCallCentreCode = dto.CallCentreCode;
            ModifiedOn = DateTime.UtcNow;
            Description = code.EventTemplate;
            Comments = dto.Comments;
            Callback = code.ShouldAllowCallRescheduling;
            CallbackCount = code.ShouldAllowCallRescheduling ? CallbackCount + 1 : 0;
            CallbackDate = GetCallBackDate(dto.CallCentreCode, dto);
            Agent = new User {Id = dto.Context.UserId};
        }
        private void UpdateHistories(UpdateLeadStatusDto dto, CallCentreEventDetailsDto eventDetails)
        {
            if (dto.CallCentreCode.ParentId > 0)
            {
                var code = new LeadCallCentreCodes().First(a => a.Id == dto.CallCentreCode.ParentId);
                if(code != null)
                UpdateHistory(code, dto, eventDetails);
            }

            var childcode = Mapper.Map<LeadCallCentreCodeDto>(dto.CallCentreCode);
            childcode.UpdateEventTemplate(eventDetails);
            var lastCount = GetLastCallBackCount();

            ////if exist already do not add again
            Histories.Add(new CampaignLeadBucketVerbose
            {
                Agent = new User { Id = dto.Context.UserId},
                Description = childcode.EventTemplate,
                Comments = dto.Comments,
                CampaignLeadBucket = this,
                LeadCallCentreCode = dto.CallCentreCode,
                Callback = childcode.ShouldAllowCallRescheduling,
                CallbackCount = childcode.ShouldAllowCallRescheduling ? lastCount + 1 : lastCount,
                CallbackDate = GetCallBackDate(dto.CallCentreCode, dto)
            });
        }
        private void UpdateHistory(LeadCallCentreCode code, UpdateLeadStatusDto dto, CallCentreEventDetailsDto eventDetails)
        {
            //update event details
            var codeDto = Mapper.Map<LeadCallCentreCodeDto>(code);
            codeDto.UpdateEventTemplate(eventDetails);

            //get Last callback count from history
            var lastCallBackCount = GetLastCallBackCount();

            //check parent has parent
            if (code.ParentId > 0)
                UpdateHistory(new LeadCallCentreCodes().First(a => a.Id == code.ParentId), dto, eventDetails);

            ////if exist already do not add again
            //if (Histories.All(x => x.LeadCallCentreCode.Id != code.Id))
            Histories.Add(new CampaignLeadBucketVerbose
            {
                Agent = new User { Id = dto.Context.UserId },
                Description = codeDto.EventTemplate,
                Comments = dto.Comments,
                CampaignLeadBucket = this,
                LeadCallCentreCode = code,
                Callback = code.ShouldAllowCallRescheduling,
                CallbackCount = code.ShouldAllowCallRescheduling ? lastCallBackCount + 1 : lastCallBackCount,
                CallbackDate = GetCallBackDate(code, dto)
            });
        }
        private DateTime GetCallBackDate(LeadCallCentreCode code, UpdateLeadStatusDto dto)
        {
            //No Rescheduling
            if (!code.ShouldAllowCallRescheduling)
                return DateTime.UtcNow;

            //No Auto-Reschedule
            if (!Campaign.AutoRescheduleCallbacks)
                return dto.CallBackDate;

            var date = DateTime.UtcNow.AddHours(Campaign.AutoRescheduleCallbackHours);

            //Allow callback after scheduled hours
            if (Campaign.AllowLeadCallbacksOutsideCampaignHours)
                return date;

            //Update date to next day
            if (Convert.ToInt32(date.ToString("HH")) <= Convert.ToInt32(Campaign.CampaignHoursTo)
                && Convert.ToInt32(date.ToString("HH")) >= Convert.ToInt32(Campaign.CampaignHoursFrom))
                date = DateTime.UtcNow.Date.AddDays(1).AddHours(Convert.ToInt32(Campaign.CampaignHoursFrom));
            
            return date;
        }
        private int GetLastCallBackCount()
        {
            var lastCallBackCount = 0;
            var history = Histories.LastOrDefault();
            if (history != null)
                lastCallBackCount = history.CallbackCount;
            return lastCallBackCount;
        }
        private void UpdateLeadActivity(UpdateLeadStatusDto dto)
        {
            //Dead
            if (dto.CallCentreCode.ShouldCreateDeadLeadActivity)
            {
                var deadDto = new DeadLeadActivityDto(dto.CampaignId);
                deadDto.Comment = dto.CallCentreCodeDto.EventTemplate;
                deadDto.SetContext(dto.Context);
                Lead.Dead(deadDto);
                Lead.Activities.Last().Campaign = this.Campaign;
            }
            //Delay
            if (dto.CallCentreCode.ShouldCreateDelayLeadActivity)
            {
                var delayDto = new DelayLeadActivityDto(dto.CampaignId);
                delayDto.Comment = dto.CallCentreCodeDto.EventTemplate;
                delayDto.SetContext(dto.Context);
                Lead.Delay(delayDto);
                Lead.Activities.Last().Campaign = this.Campaign;
            }
            //Loss
            if (dto.CallCentreCode.ShouldCreateLossLeadActivity)
            {
                var lossDto = new LossLeadActivityDto();
                lossDto.Comment = dto.CallCentreCodeDto.EventTemplate;
                lossDto.SetContext(dto.Context);
                Lead.Loss(lossDto);
                Lead.Activities.Last().Campaign = this.Campaign;
            }
        }
        #endregion
        public virtual bool UpdateAgent(User userAgent)
        {
            ModifiedOn = DateTime.UtcNow;
            Agent = userAgent;

            return true;
        }
    }
}