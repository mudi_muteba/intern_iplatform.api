using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Admin;
using Domain.Base;
using Domain.Base.Events;
using Domain.Campaigns.Events;
using Domain.Products;
using Domain.Tags;
using Domain.Users;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Party;
using iPlatform.Api.DTOs.Products;
using iPlatform.Api.DTOs.Tags;
using Infrastructure.NHibernate.Attributes;
using MasterData;
using Newtonsoft.Json;
using Shared;

namespace Domain.Campaigns
{
    public class Campaign : ChannelEntity
    {
        public Campaign()
        {
            DateCreated = SystemTime.Now();
            DateUpdated = SystemTime.Now();
            Products = new List<CampaignProduct>();
            Tags = new List<TagCampaignMap>();
            Agents = new List<CampaignAgent>();
            SimilarCampaigns = new List<CampaignSimilarCampaign>();
        }

        public virtual string Name { get; protected internal set; }
        public virtual string Reference { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual DateTime DateCreated { get; set; }
        public virtual DateTime DateUpdated { get; set; }
        public virtual bool DefaultCampaign { get; set; }
        public virtual bool IsDisabled { get; set; }

        [JsonIgnore]
        public virtual IList<CampaignProduct> Products { get; set; }

        [JsonIgnore]
        public virtual IList<TagCampaignMap> Tags { get; protected set; }

        public virtual IList<CampaignAgent> Agents { get; set; }
        private ISet<CampaignManager> _managers = new HashSet<CampaignManager>();

        public virtual ISet<CampaignManager> Managers
        {
            get { return _managers; }
            set { _managers = value; }
        }

        [JsonIgnore]
        public virtual IList<CampaignReference> References { get; set; }
        public virtual bool IsUsed { get; set; }

        [DoNotMap]
        public virtual IEnumerable<string> ManagerEmailAddresses()
        {
            return Managers.Where(x => x.User != null).Select(x => x.User.UserName).ToList();
        }

        public virtual void SetChannel(Channel channel)
        {
            Channel = channel;
        }

        //Call Centre Campaing Settings
        public virtual IList<CampaignSimilarCampaign> SimilarCampaigns { get; set; }
        public virtual string CampaignHoursFrom { get; set; }
        public virtual string CampaignHoursTo { get; set; }
        public virtual int MaximumLeadsPerAgent { get; set; }
        public virtual int MaximumLeadCallbackLimit { get; set; }
        public virtual bool AutoRescheduleCallbacks { get; set; }
        public virtual int AutoRescheduleCallbackHours { get; set; }
        public virtual bool AllowSimilarCampaignLeadCallbacks { get; set; }
        public virtual bool AllowLeadCallbacksOutsideCampaignHours { get; set; }
        public virtual bool UseLastInFirstOutCallScheme { get; set; }

        //Sales Target Fields
        public virtual int DailySalesTarget { get; set; }
        public virtual decimal DailyPremiumTarget { get; set; }

        //Campaign Working Days
        public virtual int TotalWeekWorkDays { get; set; }

        public virtual bool IsWorkdayMonday { get; set; }
        public virtual bool IsWorkdayTuesday { get; set; }
        public virtual bool IsWorkdayWednesday { get; set; }
        public virtual bool IsWorkdayThursday { get; set; }
        public virtual bool IsWorkdayFriday { get; set; }
        public virtual bool IsWorkdaySaturday { get; set; }
        public virtual bool IsWorkdaySunday { get; set; }
        

        public virtual void TagWithManager(params User[] users)
        {

            if (Managers.FirstOrDefault() != null)
                foreach (var manager in Managers)
                    manager.Delete();
            else
                Managers.Clear();

            foreach (var user in users)
            {
                if (user.AuthorisationGroups != null && user.AuthorisationGroups.All(x => x.AuthorisationGroup != AuthorisationGroups.CallCentreManager))
                    continue;
                var manager = Managers.FirstOrDefault(x => x.User.Id == user.Id);
                if (manager == null)
                    Managers.Add(new CampaignManager(user));
                else
                    manager.IsDeleted = false;
            }
        }

        public static Campaign Create(CreateCampaignDto createCampaignDto)
        {
            var campaign = Mapper.Map<Campaign>(createCampaignDto);

            //Add CampaignProducts
            AddCampaignProducts(campaign, createCampaignDto);

            //Add CampaignTags
            AddCampaignTags(campaign, createCampaignDto);

            //Add CampaignAgents
            AddCampaignAgents(campaign, createCampaignDto);

            campaign.TagWithManager(Mapper.Map<IEnumerable<int>, IEnumerable<User>>(createCampaignDto.Managers).ToArray());

            //Add CampaignReferences
            //AddCampaignReferences(campaign, createCampaignDto);


            //Add CampaignSimilarCampaigns
            AddCampaignSimilarCampaigns(campaign, createCampaignDto);

            campaign.RaiseEvent(new CampaignCreatedEvent(createCampaignDto.Name, createCampaignDto.CreateAudit(), new List<ChannelReference>{campaign.Channel}));
            
            return campaign;
        }

        public virtual void Update(EditCampaignDto editCampaignDto)
        {
            Mapper.Map(editCampaignDto, this);

            //Update CampaignProducts
            UpdateCampaignProducts(editCampaignDto);

            //Update CampaignTags
            UpdateCampaignTags(editCampaignDto);

            //Update CampaignAgents
            UpdateCampaignAgents(editCampaignDto);

            TagWithManager(Mapper.Map<IEnumerable<int>, IEnumerable<User>>(editCampaignDto.Managers).ToArray());

            //Update CampaignReferences
            //UpdateCampaignReferences(editCampaignDto);

            //Update CampaignSimilarCampaigns
            UpdateCampaignSimilarCampaigns(editCampaignDto);

            RaiseEvent(new CampaignUpdatedEvent(this, editCampaignDto.CreateAudit(), new List<ChannelReference> { new Channel(this.Channel.Id) }));
        }

        public virtual void Used()
        {
            IsUsed = true;
        }

        public virtual void Disable(DisableCampaignDto dto)
        {
            IsDisabled = true;
            RaiseEvent(new CampaignDisabledEvent(dto, dto.CreateAudit(), new List<ChannelReference> { new Channel(this.Channel.Id) }));
        }

        public virtual void Delete(DeleteCampaignDto dto)
        {
            Delete();

            RaiseEvent(new CampaignDeletedEvent(dto, dto.CreateAudit(), new List<ChannelReference> { new Channel(this.Channel.Id) }));
        }

        #region Products
        public static void AddCampaignProducts(Campaign entity, CreateCampaignDto dto)
        {
            foreach (var product in dto.Products)
            {
                entity.AddProduct(new Product() { Id = product.Id });
            }
        }

        public virtual void AddProduct(Product product)
        {
            var existing = Products.FirstOrDefault(x => x.Product.Id == product.Id);
            if (existing == null)
                Products.Add(new CampaignProduct { Campaign = this, Product = product });
        }

        public virtual void UpdateCampaignProducts(EditCampaignDto dto)
        {
            //delete products that have been removed from list
            RemoveOldProducts(dto.Products);

            //Add new products from list
            var newCampaignProducts = dto.Products.Where(x => !Products.Any(y => y.Product.Id == x.Id)).ToList();

            foreach (var product in newCampaignProducts)
            {
                AddProduct(new Product() { Id = product.Id });
            }
        }

        public virtual void RemoveOldProducts(List<ProductInfoDto> products)
        {
            var toBeDeleted = Products.Where(t => !products.Any(y => y.Id == t.Product.Id));

            foreach (var productCampaign in toBeDeleted)
            {
                productCampaign.Delete();
            }
        }
        #endregion

        #region Tags

        public static void AddCampaignTags(Campaign entity, CreateCampaignDto dto)
        {
            foreach (var tag in dto.Tags)
            {
                entity.AddTag(new Tag() { Id = tag.Id });
            }
        }

        public virtual void AddTag(Tag tag)
        {
            if (tag == null)
                return;

            if (Tags == null)
                Tags = new List<TagCampaignMap>();

            var existing = Tags.FirstOrDefault(x => x.Tag.Id == tag.Id);

            if (existing == null)
            {
                Tags.Add(new TagCampaignMap { Campaign = this, Tag = tag, IsDeleted = false });
            }
            else
            {
                existing.Tag.Name = tag.Name;
            }
        }

        public virtual void UpdateCampaignTags(EditCampaignDto dto)
        {
            //delete products that have been removed from list
            RemoveOldTags(dto.Tags);

            //Add new products from list
            var newCampaignTags = dto.Tags.Where(x => !this.Tags.Any(y => y.Tag.Id == x.Id)).ToList();

            foreach (var tag in newCampaignTags)
            {
                AddTag(new Tag() { Id = tag.Id });
            }
        }

        public virtual void RemoveOldTags(List<TagDto> tags)
        {
            var finalListOfTags = tags.ToList();

            var toDelete = (
                            from tag in Tags
                            let matchesOnId = finalListOfTags.Any(t => t.Id == tag.Tag.Id)
                            let matchesOnName = finalListOfTags.Any(t => t.Name == tag.Tag.Name)
                            where !matchesOnId && !matchesOnName
                            select tag
                           ).ToList();

            foreach (var tagCampaignMap in toDelete)
            {
                tagCampaignMap.Delete();
            }
        }
        #endregion

        #region Agents
        public static void AddCampaignAgents(Campaign entity, CreateCampaignDto dto)
        {
            foreach (var agent in dto.Agents)
            {
                entity.AddAgent(new Party.Party { Id = agent.Id });
            }
        }

        public virtual void AddAgent(Party.Party agent)
        {
            if (agent == null)
                return;

            if (Agents == null)
                Agents = new List<CampaignAgent>();

            var existing = Agents.FirstOrDefault(x => x.Party.Id == agent.Id);
            if (existing == null)
                Agents.Add(new CampaignAgent { Campaign = this, Party = agent });
        }

        public virtual void UpdateCampaignAgents(EditCampaignDto dto)
        {
            //delete Agents that have been removed from list
            RemoveOldAgents(dto.Agents);

            //Add new Agent from list
            var newCampaignAgents = dto.Agents.Where(x => !this.Agents.Any(y => y.Party.Id == x.Id)).ToList();

            foreach (var party in newCampaignAgents)
            {
                AddAgent(new Party.Party { Id = party.Id });
            }
        }

        public virtual void RemoveOldAgents(List<PartyDto> agents)
        {
            var toBeDeleted = Agents.Where(t => !agents.Any(y => y.Id == t.Party.Id));

            foreach (var campainAgent in toBeDeleted)
            {
                campainAgent.Delete();
            }
        }
        #endregion

        #region References
        public static void AddCampaignReferences(Campaign entity, CreateCampaignDto dto)
        {
            foreach (var reference in dto.References)
            {
                entity.AddReference(new CampaignReference(entity, reference.Name));
            }
        }

        public virtual void AddReference(CampaignReference reference)
        {
            if (reference == null)
                return;

            if (References == null)
                References = new List<CampaignReference>();

                References.Add(reference);
        }

        public virtual void UpdateCampaignReferences(EditCampaignDto dto)
        {
            //delete Agents that have been removed from list
            RemoveOldReferences(dto.References);

            //Add new Agent from list
            var newCampaignReferences = dto.References.Where(x => !this.References.Any(y => y.Id == x.Id)).ToList();

            foreach (var reference in newCampaignReferences)
            {
                AddReference(new CampaignReference(this, reference.Name));
            }
        }

        public virtual void RemoveOldReferences(List<CampaignReferenceDto> references)
        {
            var toBeDeleted = this.References.Where(t => !references.Any(y => y.Id == t.Id));

            foreach (var campainReference in toBeDeleted)
            {
                campainReference.Delete();
            }
        }
        #endregion

        #region Similar Campaigns
        public static void AddCampaignSimilarCampaigns(Campaign entity, CreateCampaignDto dto)
        {
            foreach (var campaign in dto.SimilarCampaigns)
            {
                entity.AddSimilarCampaign(new Campaign() { Id = campaign.Id });
            }
        }

        public virtual void AddSimilarCampaign(Campaign campaign)
        {
            if (campaign == null)
                return;

            if (SimilarCampaigns == null)
                SimilarCampaigns = new List<CampaignSimilarCampaign>();

            var existing = SimilarCampaigns.FirstOrDefault(x => x.SimilarCampaign.Id == campaign.Id);
            if (existing == null)
                SimilarCampaigns.Add(new CampaignSimilarCampaign { Campaign = this, SimilarCampaign = campaign });
        }

        public virtual void UpdateCampaignSimilarCampaigns(EditCampaignDto dto)
        {
            //delete Agents that have been removed from list
            RemoveOldSimilarCampaigns(dto.SimilarCampaigns);

            var list = SimilarCampaigns.ToList();
            //Add new Similar Campaign from list
            var newCampaignSimilarCampaign = dto.SimilarCampaigns.Where(x => !list.Any(y => y.SimilarCampaign.Id == x.Id)).ToList();

            foreach (var campaign in newCampaignSimilarCampaign)
            {
                AddSimilarCampaign(new Campaign() { Id = campaign.Id });
            }
        }

        public virtual void RemoveOldSimilarCampaigns(List<CampaignDto> campaigns)
        {
            var list = SimilarCampaigns.ToList();
            var toBeDeleted = list.Where(t => !campaigns.Any(y => y.Id == t.SimilarCampaign.Id));

            foreach (var similarCampaign in toBeDeleted)
            {
                similarCampaign.Delete();
            }
        }
        #endregion
    }
}