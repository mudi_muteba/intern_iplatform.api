﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Campaigns;

namespace Domain.Campaigns.Events
{
    public class CampaignDeletedEvent : BaseDomainEvent
    {
        public DeleteCampaignDto DeleteCampaignDto { get; set; }

        public CampaignDeletedEvent(DeleteCampaignDto dto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            DeleteCampaignDto = dto;
        }
    }
}