﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Campaigns;

namespace Domain.Campaigns.Events
{
    public class CampaignUpdatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public EditCampaignDto editCampaignDto { get; private set; }
        public CampaignUpdatedEvent(Campaign entity, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            this.editCampaignDto = editCampaignDto;
        }
    }
}