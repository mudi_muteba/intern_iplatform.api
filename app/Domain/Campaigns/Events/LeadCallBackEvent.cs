﻿using System;
using Domain.Base.Events;

namespace Domain.Campaigns.Events
{
    public class LeadCallBackEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public int CampaignLeadBucketId { get; set; }
        public DateTime LeadCallBackDate { get; set; }
        public int ChannelId { get; set; }
        public LeadCallBackEvent(int campaignLeadBucketId, DateTime leadCallBackDate, int channelId)
        {
            CampaignLeadBucketId = campaignLeadBucketId;
            LeadCallBackDate = leadCallBackDate;
            ChannelId = channelId;
        }
    }
}