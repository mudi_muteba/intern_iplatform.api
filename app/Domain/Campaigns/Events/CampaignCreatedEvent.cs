﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;

namespace Domain.Campaigns.Events
{
    public class CampaignCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {
        public string Name { get; private set; }

        public CampaignCreatedEvent(string name, EventAudit audit, IEnumerable<ChannelReference> channelReferences) : base(audit, channelReferences)
        {
            Name = name;
        }
    }
}