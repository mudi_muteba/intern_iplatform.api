﻿using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Campaigns;

namespace Domain.Campaigns.Events
{
    public class CampaignDisabledEvent : BaseDomainEvent
    {
        public DisableCampaignDto DisableCampaignDto { get; set; }

        public CampaignDisabledEvent(DisableCampaignDto dto, EventAudit audit, IEnumerable<ChannelReference> channelReferences)
            : base(audit, channelReferences)
        {
            DisableCampaignDto = dto;
        }
    }
}