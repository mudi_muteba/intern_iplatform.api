﻿using Domain.Base;
using System;

namespace Domain.Campaigns
{
    [Serializable]
    public class CampaignAgent : Entity
    {
        public virtual Campaign Campaign { get; set; }
        public virtual Party.Party Party { get; set; }

    }
}