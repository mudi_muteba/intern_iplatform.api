using System.ComponentModel.DataAnnotations;
using Domain.Base;
using Domain.Users;

namespace Domain.Campaigns
{
    public class CampaignManager : Entity
    {
        [Required]
        public virtual User User { get; set; }

        protected CampaignManager()
        {
        }

        public CampaignManager(User user)
        {
            User = user;
        }
    }
}