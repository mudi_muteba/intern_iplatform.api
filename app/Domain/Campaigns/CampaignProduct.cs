﻿using Domain.Products;
using System;
using Domain.Base;

namespace Domain.Campaigns
{
    [Serializable]
    public class CampaignProduct : Entity 
    {
        public virtual Campaign Campaign { get; set; }
        public virtual Product Product { get; set; }
    }
}