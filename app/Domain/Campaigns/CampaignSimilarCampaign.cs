﻿using Domain.Base;
using System;

namespace Domain.Campaigns
{
    [Serializable]
    public class CampaignSimilarCampaign : Entity
    {
        public virtual Campaign Campaign { get; set; }
        public virtual Campaign SimilarCampaign { get; set; }
    }
}