﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Events;
using Domain.Base.Extentions;
using Domain.Base.Repository;
using Domain.Campaigns.Events;
using Domain.Escalations;
using Domain.Escalations.Extensions;
using Domain.Escalations.Queries;
using Domain.Escalations.Workflow.Messages;
using Domain.Users;
using iPlatform.Enums.Escalations;
using iPlatform.Enums.Workflows;
using Newtonsoft.Json;
using Shared.Extentions;
using Workflow.Messages;

namespace Domain.Campaigns.EventHandlers
{
    public class LeadCallBackEventHandler : BaseEventHandler<LeadCallBackEvent>
    {
        private readonly IWorkflowRouter _router;
        private readonly IRepository _repository;
        private readonly FindEscalationPlanQuery _query;

        public LeadCallBackEventHandler(IWorkflowRouter router, IRepository repository, FindEscalationPlanQuery query)
        {
            _repository = repository;
            _query = query;
            _router = router;
        }

        public override void Handle(LeadCallBackEvent @event)
        {
            if (@event.LeadCallBackDate == new DateTime())
            {
                this.Error(() => string.Format("LeadCallBackDate is empty {0}", @event.PrintProperties()));
                return;
            }

            User user;

            var step = EscalationPlanStep(@event, out user);
            if (step == null)
            {
                this.Error(() => string.Format("Lead call back cannot be escalated, no step found for {0}", @event.PrintProperties()));
                return;
            }

            var initialCorrelationId = Guid.NewGuid();
            var jsonData = JsonConvert.SerializeObject(@event);
            var createEscalationWorkflowMessage = new CreateEscalationWorkflowMessage(initialCorrelationId, step.Id, user.Id,
                EventType.LeadCallBack.ToString(), @event.ChannelId, step.DelayType,
                step.IntervalType, step.IntervalDelay, step.DateTimeDelay, jsonData);

            this.Info(() => string.Format("Publishing {0} message: ", createEscalationWorkflowMessage.PrintProperties()));

            var futurePublishDate = step.DateTimeDelay.HasValue
                ? step.DateTimeDelay.Value.Add(step.GetTimeSpanDelay()) // if specific DateTime was specified in escalation step
                : @event.LeadCallBackDate.Add(step.GetTimeSpanDelay()); // add escalation step delay interval

            _router.PublishDelayed(new WorkflowRoutingMessage().AddMessage(createEscalationWorkflowMessage), futurePublishDate);

            
            var hist = new EscalationPlanExecutionHistory(initialCorrelationId, createEscalationWorkflowMessage.CorrelationId,
                createEscalationWorkflowMessage.ParentCorrelationId, step, null, user,
                EscalationWorkflowMessageStatus.Scheduled);
            _repository.Save(hist);

            this.Info(() => string.Format("Published {0}", createEscalationWorkflowMessage.PrintProperties()));
        }

        private EscalationPlanStep EscalationPlanStep(LeadCallBackEvent @event, out User user)
        {
            user = null;

            var campaignLeadBucket = _repository.GetById<CampaignLeadBucket>(@event.CampaignLeadBucketId);
            if (campaignLeadBucket == null)
            {
                this.Error(() => string.Format("Could not get {0} {1}", typeof (CampaignLeadBucket), @event.CampaignLeadBucketId));
                return null;
            }

            if (campaignLeadBucket.Agent == null)
            {
                this.Error(() => string.Format("{0} {1} agent is null", typeof(CampaignLeadBucket), @event.CampaignLeadBucketId));
                return null;
            }

            var agent = _repository.GetById<User>(campaignLeadBucket.Agent.Id);
            if (agent == null)
            {
                this.Error(() => string.Format("Could not get {0} {1} user is null", typeof(User), campaignLeadBucket.Agent.Id));
                return null;
            }

            user = agent;

            var escalationPlans = _query.WithParameters(new List<int>(), EventType.LeadCallBack.ToString(), campaignLeadBucket.Campaign.Id).ExecuteQuery();
            if (!escalationPlans.Any())
            {
                this.Error(() => string.Format("No escalation plans found for {0} {1} Campaign {2} EventType {3}", typeof(CampaignLeadBucket),
                            @event.CampaignLeadBucketId, campaignLeadBucket.Campaign.Id, EventType.LeadCallBack.ToString()));
                return null;
            }

            var escalationPlan = escalationPlans.FirstOrDefault();
            var sequence = escalationPlan.Sequences.FirstOrDefault();
            if (!escalationPlan.HasStep(sequence))
            {
                this.Error(() => string.Format("Escalation Plan {0} has no starting step sequence", escalationPlan.Id));
                return null;
            }

            return escalationPlan[sequence];
        }
    }
}