using System;
using Domain.Base;
using Domain.Leads;
using Domain.Users;
using MasterData;

namespace Domain.Campaigns
{
    public class CampaignLeadBucketVerbose : Entity
    {
        public CampaignLeadBucketVerbose()
        {
            Callback = false;
            CallbackDate = DateTime.UtcNow;
            Comments = "";
            CreatedOn = DateTime.UtcNow;
            Description = "N/A";
            IsDeleted = false;
            ModifiedOn = DateTime.UtcNow;
            LeadCallCentreCode = LeadCallCentreCodes.LeadNotAllocated;
        }

        public virtual CampaignLeadBucket CampaignLeadBucket { get; set; }
        public virtual User Agent { get; set; }
        public virtual LeadCallCentreCode LeadCallCentreCode { get; set; }
        public virtual bool Callback { get; set; }
        public virtual DateTime CallbackDate { get; set; }
        public virtual string Comments { get; set; }
        public virtual string Description { get; set; }
        public virtual int CallbackCount { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual DateTime ModifiedOn { get; set; }

        public static CampaignLeadBucketVerbose Create(CampaignLeadBucket campaignLeadBucket)
        {
            return new CampaignLeadBucketVerbose
            {
                CampaignLeadBucket = campaignLeadBucket,
                Description = campaignLeadBucket.Description
            };

        }
    }




}