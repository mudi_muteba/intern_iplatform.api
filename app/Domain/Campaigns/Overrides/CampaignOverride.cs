﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Campaigns.Overrides
{
    public class CampaignOverride : IAutoMappingOverride<Campaign>
    {
        public void Override(AutoMapping<Campaign> mapping)
        {
            mapping.HasMany(c => c.Products).Inverse().Cascade.SaveUpdate();
            mapping.HasMany(c => c.Tags).Inverse().Cascade.SaveUpdate();
            mapping.HasMany(c => c.Agents).Inverse().Cascade.SaveUpdate();
            mapping.HasMany(c => c.References).Inverse().Cascade.SaveUpdate();
            mapping.HasMany(c => c.SimilarCampaigns).Inverse().Cascade.SaveUpdate();
            mapping.References(c => c.Channel).Nullable();
            mapping.HasMany(x => x.Managers).Cascade.SaveUpdate();
        }
    }
}