﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Campaigns.Overrides
{
    public class CampaignLeadBucketVerboseOverride : IAutoMappingOverride<CampaignLeadBucketVerbose>
    {
        public void Override(AutoMapping<CampaignLeadBucketVerbose> mapping)
        {
            mapping.References(x => x.Agent).Nullable();
            mapping.References(x => x.CampaignLeadBucket);
        }
    }
}