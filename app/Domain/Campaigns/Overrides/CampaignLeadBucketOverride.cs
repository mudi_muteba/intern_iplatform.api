﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Campaigns.Overrides
{
    public class CampaignLeadBucketOverride : IAutoMappingOverride<CampaignLeadBucket>
    {
        public void Override(AutoMapping<CampaignLeadBucket> mapping)
        {
            mapping.References(x => x.Agent).Nullable().Cascade.None();
            mapping.References(x => x.Campaign);
            mapping.References(x => x.Lead).LazyLoad(Laziness.False);
            mapping.HasMany(x => x.Histories).Cascade.SaveUpdate();
        }
    }
}