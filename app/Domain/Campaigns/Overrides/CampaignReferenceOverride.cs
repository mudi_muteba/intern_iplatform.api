﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Campaigns.Overrides
{
    public class CampaignReferenceOverride : IAutoMappingOverride<CampaignReference>
    {
        public void Override(AutoMapping<CampaignReference> mapping)
        {
            mapping.Table(mapping.GetType().GetGenericArguments()[0].Name);
            mapping.Map(x => x.Name, "Reference");
            mapping.References(x => x.Campaign).Cascade.None();
        } 
    }
}