﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Campaigns.Overrides
{
    public class CampaignManagerOverride : IAutoMappingOverride<CampaignManager>
    {
        public void Override(AutoMapping<CampaignManager> mapping)
        {
            mapping.References(x => x.User);
        }
    }
}