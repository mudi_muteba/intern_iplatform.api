﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;
using System.Linq;

namespace Domain.Campaigns.Handlers
{
    public class DeleteCampaignHandler : ExistingEntityDtoHandler<Campaign, DeleteCampaignDto, int>
    {
        public DeleteCampaignHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }
        private readonly IRepository _repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    CampaignAuthorisation.Delete
                };
            }
        }

        protected override void HandleExistingEntityChange(Campaign entity, DeleteCampaignDto dto, HandlerResult<int> result)
        {
            entity.Delete(dto);
            DeleteCampaignSimilarCampaign(entity);
            result.Processed(entity.Id);
        }


        private void DeleteCampaignSimilarCampaign(Campaign entity)
        {
            var similarCampaigns = _repository.GetAll<CampaignSimilarCampaign>().Where(x => x.SimilarCampaign.Id == entity.Id).ToList() ;
            similarCampaigns.ForEach(x => {

                x.Delete();
                _repository.Save(x);
            });
        }
    }
}