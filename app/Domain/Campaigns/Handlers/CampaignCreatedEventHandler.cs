﻿using Domain.Base.Events;
using Domain.Campaigns.Events;

namespace Domain.Campaigns.Handlers
{
    public class CampaignCreatedEventHandler : BaseEventHandler<CampaignCreatedEvent>
    {
        public override void Handle(CampaignCreatedEvent @event)
        {
        }
    }
}