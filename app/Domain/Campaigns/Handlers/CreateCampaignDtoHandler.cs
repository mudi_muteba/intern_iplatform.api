﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Castle.Core.Internal;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;

namespace Domain.Campaigns.Handlers
{
    public class CreateCampaignDtoHandler : CreationDtoHandler<Campaign, CreateCampaignDto, int>
    {
        public CreateCampaignDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;
        
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    CampaignAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(Campaign entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Campaign HandleCreation(CreateCampaignDto dto, HandlerResult<int> result)
        {
            if (dto.DefaultCampaign)
                RemoveOthersAsDefault();

            var campaign = Campaign.Create(dto);
            result.Processed(campaign.Id);

            return campaign;
        }

        private void RemoveOthersAsDefault()
        {
            var campaigns = _repository.GetAll<Campaign>().Where(x => x.DefaultCampaign);
            if(campaigns.Any())
                campaigns.ForEach(x =>
                {
                    x.DefaultCampaign = false;
                    _repository.Save(x);
                });
        }
    }
}