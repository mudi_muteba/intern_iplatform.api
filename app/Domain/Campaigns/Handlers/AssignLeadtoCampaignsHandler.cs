﻿using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Leads;
using Domain.Users;

using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using iPlatform.Api.DTOs.Campaigns;

using MasterData;
using MasterData.Authorisation;

using ValidationMessages.Leads;

namespace Domain.Campaigns.Handlers
{
    public class AssignLeadtoCampaignsHandler : BaseDtoHandler<AssignLeadCampaignsDto, bool>
    {
        public AssignLeadtoCampaignsHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    CampaignLeadBucketAuthorisation.Assign
                };
            }
        }


        protected override void InternalHandle(AssignLeadCampaignsDto dto, HandlerResult<bool> result)
        {
            var lead = _repository.GetById<Lead>(dto.LeadId);

            if (lead == null)
            {
                result.AllErrorDTOMessages.Add(Mapper.Map<ResponseErrorMessage>(LeadValidationMessages.LeadDoesNotExist));
                result.Processed(false);
                return;
            }

            SaveLeadCampaigns(dto, lead);
            
            result.Processed(true);
        }

        private void SaveLeadCampaigns(AssignLeadCampaignsDto dto, Lead lead)
        {
            var existingLeadBucket = _repository.GetAll<CampaignLeadBucket>();

            foreach (var campaignId in dto.CampaignList)
            {
                if (!existingLeadBucket.Any(x => x.Campaign.Id == campaignId && x.Lead.Id == lead.Id))
                {
                    var entity = CampaignLeadBucket.Create(campaignId, lead);
                    _repository.Save(entity);
                }

                var activity = lead.Activities.FirstOrDefault(x =>
                (x.ActivityType == ActivityTypes.Created && x.Campaign == null)
                ||
                (x.ActivityType == ActivityTypes.Created && x.Campaign.Id == campaignId));
                if (activity != null)
                {
                    activity.Campaign = new Campaign { Id = campaignId };
                    activity.CampaignSourceId = campaignId;
                } 
                else
                    lead.CreatedLeadActivity(new User(dto.Context.UserId), new Campaign {Id = campaignId});
            }

            _repository.Save(lead);
        }
    }
}
