﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Leads;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;
using MoreLinq;
using AutoMapper;

namespace Domain.Campaigns.Handlers
{
    public class CreateCampaignReferenceHandler : CreationDtoHandler<CampaignReference,CreateCampaignReferenceDto, int>
    {
        public CreateCampaignReferenceHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    CampaignLeadBucketAuthorisation.Assign
                };
            }
        }

        protected override void EntitySaved(CampaignReference entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override CampaignReference HandleCreation(CreateCampaignReferenceDto dto, HandlerResult<int> result)
        {
            var entity = Mapper.Map<CampaignReference>(dto);
            return entity;
        }
    }
}
