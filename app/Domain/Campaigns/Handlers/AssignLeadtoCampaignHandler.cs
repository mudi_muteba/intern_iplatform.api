﻿using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Leads;

using iPlatform.Api.DTOs.Campaigns;

using MasterData.Authorisation;

namespace Domain.Campaigns.Handlers
{
    public class AssignLeadtoCampaignHandler : BaseDtoHandler<AssignLeadToCampaignDto, bool>
    {
        public AssignLeadtoCampaignHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    CampaignLeadBucketAuthorisation.Assign
                };
            }
        }


        protected override void InternalHandle(AssignLeadToCampaignDto dto, HandlerResult<bool> result)
        {
            var campaignLeadBucket = _repository.GetAll<CampaignLeadBucket>().Where(x => x.Campaign.Id == dto.Id && !x.Lead.IsDeleted).ToList();

            if (dto.AllLeads)
            {
                var leadEntities = _repository.GetAll<Lead>().ToList();
                var leads = new List<int>();
                leads.AddRange(leadEntities.Select(x => x.Id));

                SaveCampaignLeads(dto.Id, leads, campaignLeadBucket);
            }
            else
            {
                SaveCampaignLeads(dto.Id, dto.Leads, campaignLeadBucket);
            }

            result.Processed(true);
        }

        private void RemoveCampaignLead(List<CampaignLeadBucket> campaignLeads, IEnumerable<int> leads)
        {
            var campaignLeadtodelete = campaignLeads.Where(x => !leads.Contains(x.Lead.Id)).ToList();

            campaignLeadtodelete.ForEach(x =>
            {
                x.Delete();
                _repository.Save(x);
            });
        }

        private void SaveCampaignLeads(int campaignId, List<int> leads, List<CampaignLeadBucket> campaignLeads)
        {
            RemoveCampaignLead(campaignLeads, leads);

            var existingLeadBucket = _repository.GetAll<CampaignLeadBucket>();

            foreach (var leadId in leads)
            {
                if (campaignLeads.All(x => x.Lead.Id != leadId))
                {
                    if (!existingLeadBucket.Any(x => x.Campaign.Id == campaignId && x.Lead.Id == leadId))
                    {
                        var entity = CampaignLeadBucket.Create(campaignId, leadId);
                        _repository.Save(entity);
                    }
                }
            }
        }
    }
}
