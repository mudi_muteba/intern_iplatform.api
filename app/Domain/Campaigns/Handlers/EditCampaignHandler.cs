﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;
using System.Linq;
using MoreLinq;

namespace Domain.Campaigns.Handlers
{
    public class EditCampaignHandler : ExistingEntityDtoHandler<Campaign, EditCampaignDto, int>
    {
        public EditCampaignHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        private readonly IRepository _repository;

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    CampaignAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(Campaign entity, EditCampaignDto dto,
            HandlerResult<int> result)
        {
            if (dto.DefaultCampaign)
                RemoveOthersAsDefault();

            entity.Update(dto);
            result.Processed(entity.Id);
        }

        private void RemoveOthersAsDefault()
        {
            var campaigns = _repository.GetAll<Campaign>().Where(x => x.DefaultCampaign);
            if (campaigns.Any())
                campaigns.ForEach(x =>
                {
                    x.DefaultCampaign = false;
                    _repository.Save(x);
                });
        }
    }
}