﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;

namespace Domain.Campaigns.Handlers
{
    public class DisableCampaignHandler : ExistingEntityDtoHandler<Campaign, DisableCampaignDto, int>
    {
        public DisableCampaignHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    CampaignAuthorisation.Disable
                };
            }
        }

        protected override void HandleExistingEntityChange(Campaign entity, DisableCampaignDto dto, HandlerResult<int> result)
        {
            entity.Disable(dto);
            result.Processed(entity.Id);
        }
    }
}