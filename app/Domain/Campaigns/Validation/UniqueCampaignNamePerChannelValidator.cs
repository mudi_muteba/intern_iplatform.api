﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Campaigns;
using ValidationMessages.Campaigns;

namespace Domain.Campaigns.Validation
{
    public class UniqueCampaignNamePerChannelValidator : IValidateDto<CreateCampaignDto>, IValidateDto<EditCampaignDto>
    {
        private readonly GetCampaignByNameQuery query;

        public UniqueCampaignNamePerChannelValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetCampaignByNameQuery(contextProvider, repository);
        }

        public void Validate(CreateCampaignDto dto, ExecutionResult result)
        {
            var existingCampaign = GetExistingCampaignsByName(dto.Name);

            if (existingCampaign.Any())
            {
                result.AddValidationMessage(CampaignValidationMessages.DuplicateCampaignName.AddParameters(new[] { dto.Name }));
            }
        }

        public void Validate(EditCampaignDto dto, ExecutionResult result)
        {
            var existingCampaign = GetExistingCampaignsByName(dto.Name);

            if (existingCampaign.Any() && existingCampaign.FirstOrDefault().Id != dto.Id)
            {
                result.AddValidationMessage(CampaignValidationMessages.DuplicateCampaignName.AddParameters(new[] { dto.Name }));
            }
        }

        private IQueryable<Campaign> GetExistingCampaignsByName(string campaignName)
        {
            return query.ByName(campaignName).ExecuteQuery();
        }
    }
}