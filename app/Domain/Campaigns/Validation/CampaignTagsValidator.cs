﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Tags;
using ValidationMessages.Campaigns;
using Domain.Products.Queries;
using Domain.Products;
using Domain.Tags;
using System.Collections.Generic;
using Domain.Tags.Queries;

namespace Domain.Campaigns.Validation
{
    public class CampaignTagsValidator : IValidateDto<CreateCampaignDto>, IValidateDto<EditCampaignDto>
    {
        private readonly GetTagByIdQuery query;

        public CampaignTagsValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetTagByIdQuery(contextProvider, repository);
        }

        public void Validate(CreateCampaignDto dto, ExecutionResult result)
        {
            ValidateTag(dto.Tags, result);
        }


        public void Validate(EditCampaignDto dto, ExecutionResult result)
        {
            ValidateTag(dto.Tags, result);
        }


        private void ValidateTag(List<TagDto> tags, ExecutionResult result)
        {
            foreach (var tag in tags)
            {
                var queryResult = GetTagById(tag.Id);

                if (queryResult.Count() <= 0)
                {
                    result.AddValidationMessage(CampaignValidationMessages.InvalidTagId.AddParameters(new[] { tag.Id.ToString() }));
                }
            }
        }

        private IQueryable<Tag> GetTagById(int id)
        {
            return query.WithId(id).ExecuteQuery();
        }
    }
}