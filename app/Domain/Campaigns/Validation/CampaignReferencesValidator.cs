﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Campaigns;
using ValidationMessages.Campaigns;
using Domain.Products.Queries;
using Domain.Products;
using Domain.Party;
using Domain.Party.Queries;
using System.Collections.Generic;


namespace Domain.Campaigns.Validation
{
    public class CampaignReferencesValidator : IValidateDto<CreateCampaignDto>, IValidateDto<EditCampaignDto>
    {

        public CampaignReferencesValidator(IProvideContext contextProvider, IRepository repository)
        {
        }

        public void Validate(CreateCampaignDto dto, ExecutionResult result)
        {
            ValidateAgent(dto.References, result);
        }


        public void Validate(EditCampaignDto dto, ExecutionResult result)
        {
            ValidateAgent(dto.References, result);
        }


        private void ValidateAgent(List<CampaignReferenceDto> references, ExecutionResult result)
        {
            foreach (var reference in references)
            {
                if (string.IsNullOrEmpty(reference.Name))
                {
                    result.AddValidationMessage(CampaignValidationMessages.InvalidCampaignReference.AddParameters(new[] { reference.Name.ToString() }));
                }
            }
        }
    }
}