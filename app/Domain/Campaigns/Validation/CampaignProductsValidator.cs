﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using Domain.Campaigns.Queries;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Products;
using ValidationMessages.Campaigns;
using Domain.Products.Queries;
using Domain.Products;
using System.Collections.Generic;

namespace Domain.Campaigns.Validation
{
    public class CampaignProductsValidator : IValidateDto<CreateCampaignDto>, IValidateDto<EditCampaignDto>
    {
        private readonly GetAllocatedProductByIdQuery query;

        public CampaignProductsValidator(IProvideContext contextProvider, IRepository repository)
        {
            query = new GetAllocatedProductByIdQuery(contextProvider, repository);
        }

        public void Validate(CreateCampaignDto dto, ExecutionResult result)
        {
            ValidateProduct(dto.Products, result);
        }


        public void Validate(EditCampaignDto dto, ExecutionResult result)
        {
            ValidateProduct(dto.Products, result);
        }


        private void ValidateProduct (List<ProductInfoDto> products, ExecutionResult result)
        {
            foreach (var product in products)
            {
                var queryResult = GetAllocatedProductById(product.Id);

                if (queryResult.Count() <= 0)
                {
                    result.AddValidationMessage(CampaignValidationMessages.InvalidProductId.AddParameters(new[] { product.Id.ToString() }));
                }
            }
        }

        private IQueryable<Product> GetAllocatedProductById(int id)
        {
            return query.WithId(id).ExecuteQuery();
        }
    }
}