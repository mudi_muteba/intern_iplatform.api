﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Party;
using ValidationMessages.Campaigns;
using Domain.Party.Queries;
using System.Collections.Generic;
using MoreLinq;


namespace Domain.Campaigns.Validation
{
    public class CampaignAgentsValidator : IValidateDto<CreateCampaignDto>, IValidateDto<EditCampaignDto>
    {
        private readonly GetPartyByIdQuery query;
        private readonly IRepository _repository;

        public CampaignAgentsValidator(IProvideContext contextProvider, IRepository repository)
        {
            _repository = repository;
            query = new GetPartyByIdQuery(contextProvider, repository);
        }

        public void Validate(CreateCampaignDto dto, ExecutionResult result)
        {
            ValidateAgent(dto.Agents, result);
        }


        public void Validate(EditCampaignDto dto, ExecutionResult result)
        {
            ValidateAgent(dto.Agents, result);
        }


        private void ValidateAgent(List<PartyDto> agents, ExecutionResult result)
        {
            int[] agentIds = agents.Select(x => x.Id).ToArray();
            IQueryable<Party.Party> founAgents = _repository.GetAll<Party.Party>().Where(x => agentIds.Contains(x.Id));
            if (!founAgents.Any())
            {
                agents.ForEach(agent =>
                {
                    result.AddValidationMessage(CampaignValidationMessages.InvalidAgentId.AddParameters(new[] { agent.Id.ToString() }));
                });
            }
        }
    }
}