﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.PolicyBindings;
using MasterData;
using System.Collections.Generic;
using System.Linq;

namespace Domain.PolicyBindings
{
    public class PolicyBindingQuestion : Entity
    {
        public PolicyBindingQuestion()
        {
            PolicyBindingQuestionAnswers = new List<PolicyBindingQuestionAnswer>();
            PolicyBindingQuestionValiditions = new List<PolicyBindingQuestionValidition>();
        }

        public virtual string Name { get; set; }
        public virtual QuestionType QuestionType { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual string ToolTip { get; set; }
        public virtual string Regex { get; set; }
        public virtual string Reference { get; set; }
        public virtual IList<PolicyBindingQuestionAnswer> PolicyBindingQuestionAnswers { get; set; }
        public virtual IList<PolicyBindingQuestionValidition> PolicyBindingQuestionValiditions { get; set; }
        

        public static PolicyBindingQuestion Create(PolicyBindingQuestionDto dto)
        {
            PolicyBindingQuestion entity = Mapper.Map<PolicyBindingQuestion>(dto);

            for (int i = 0; i < dto.PolicyBindingQuestionAnswers.Count; i++)
            {
                entity.PolicyBindingQuestionAnswers.Add(PolicyBindingQuestionAnswer.Create(dto.PolicyBindingQuestionAnswers[i]));
            }

            for (int i = 0; i < dto.PolicyBindingQuestionValiditions.Count; i++)
            {
                entity.PolicyBindingQuestionValiditions.Add(PolicyBindingQuestionValidition.Create(dto.PolicyBindingQuestionValiditions[i]));
            }

            return entity;
        }

        public virtual PolicyBindingQuestion Update(PolicyBindingQuestionDto dto)
        {
            Mapper.Map(dto, this);
            UpdateAnswers(dto);
            UpdateValidations(dto);
            return this;
        }

        #region Update Answers
        public virtual void UpdateAnswers(PolicyBindingQuestionDto dto)
        {
            //delete answers that has been removed from dto
            RemoveOldAnwers(dto.PolicyBindingQuestionAnswers);

            //Add new answers from list
            List<PolicyBindingQuestionAnswerDto> newAnswerDtos = dto.PolicyBindingQuestionAnswers.Where(x => !PolicyBindingQuestionAnswers.Any(y => y.Reference == x.Reference)).ToList();
            foreach (PolicyBindingQuestionAnswerDto answerDto in newAnswerDtos)
            {
                PolicyBindingQuestionAnswers.Add(PolicyBindingQuestionAnswer.Create(answerDto));
            }

            //update existing answers
            List<PolicyBindingQuestionAnswerDto> existingAnswerDtos = dto.PolicyBindingQuestionAnswers.Where(x => PolicyBindingQuestionAnswers.Any(y => y.Reference == x.Reference)).ToList();
            foreach(PolicyBindingQuestionAnswerDto answerDto in existingAnswerDtos)
            {
                PolicyBindingQuestionAnswer existingAnswer = PolicyBindingQuestionAnswers.FirstOrDefault(x => x.Reference == answerDto.Reference);
                existingAnswer.Update(answerDto);
            }
        }

        public virtual void RemoveOldAnwers(List<PolicyBindingQuestionAnswerDto> answerDtos)
        {
            List<PolicyBindingQuestionAnswer> toBeDeleted = PolicyBindingQuestionAnswers.Where(t => !answerDtos.Any(y => y.Reference == t.Reference)).ToList();

            foreach (PolicyBindingQuestionAnswer answer in toBeDeleted)
            {
                answer.Delete();
            }
        }
        #endregion
        #region Update Validations
        public virtual void UpdateValidations(PolicyBindingQuestionDto dto)
        {
            //delete answers that has been removed from dto
            RemoveOldValidations(dto.PolicyBindingQuestionValiditions);

            //Add new answers from list
            List<PolicyBindingQuestionValiditionDto> newValidationDtos = dto.PolicyBindingQuestionValiditions.Where(x => !PolicyBindingQuestionValiditions.Any(y => y.Reference == x.Reference)).ToList();
            foreach (PolicyBindingQuestionValiditionDto validationDto in newValidationDtos)
            {
                PolicyBindingQuestionValiditions.Add(PolicyBindingQuestionValidition.Create(validationDto));
            }

            //update existing answers
            List<PolicyBindingQuestionValiditionDto> existingValidationDtos = dto.PolicyBindingQuestionValiditions.Where(x => PolicyBindingQuestionValiditions.Any(y => y.Reference == x.Reference)).ToList();
            foreach (PolicyBindingQuestionValiditionDto validationDto in existingValidationDtos)
            {
                PolicyBindingQuestionValidition existingValidation = PolicyBindingQuestionValiditions.FirstOrDefault(x => x.Reference == validationDto.Reference);
                existingValidation.Update(validationDto);
            }
        }

        public virtual void RemoveOldValidations(List<PolicyBindingQuestionValiditionDto> validationDtos)
        {
            List<PolicyBindingQuestionValidition> toBeDeleted = PolicyBindingQuestionValiditions.Where(t => !validationDtos.Any(y => y.Reference == t.Reference)).ToList();

            foreach (PolicyBindingQuestionValidition validation in toBeDeleted)
            {
                validation.Delete();
            }
        }
        #endregion

    }
}
