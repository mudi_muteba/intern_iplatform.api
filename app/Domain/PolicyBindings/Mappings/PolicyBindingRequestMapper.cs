﻿using AutoMapper;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Products;
using iPlatform.Api.DTOs.PolicyBindings;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using System.Linq;
using MasterData;
using Domain.QuestionDefinitions;
using Domain.Party.Quotes;
using Domain.PolicyBindings.Events;
using iPlatform.Api.DTOs.PolicyBindings.Request;
using iPlatform.Api.DTOs.Products;

namespace Domain.PolicyBindings.Mappings
{
    public class PolicyBindingRequestMapper : Profile
    {
        protected override void Configure()
        {

            Mapper.CreateMap<Quote, PolicyBindingRequestInfoDto>()
                .ForMember(t => t.Source, o => o.Ignore())
                .ForMember(t => t.ExtraITCScore, o => o.MapFrom(s => s.ExtraITCScore))
                .ForMember(t => t.ITCScore, o => o.MapFrom(s => s.ITCScore))
                .ForMember(t => t.InsurerReference, o => o.MapFrom(s => s.InsurerReference))
                .ForMember(t => t.Fees, o => o.MapFrom(s => s.Fees))
                .ForMember(t => t.Items, o => o.Ignore())
                .ForMember(t => t.ProductId, o => o.MapFrom(s => s.Product.Id))
                .ForMember(t => t.ProductCode, o => o.MapFrom(s => s.Product.ProductCode))
                .ForMember(t => t.ProductName, o => o.MapFrom(s => s.Product.Name))
                .ForMember(t => t.AboutProduct, o => o.MapFrom(s => s.Product))
                .ForMember(t => t.InsurerCode, o => o.MapFrom(s => s.Product.ProductOwner.Code))
                .ForMember(t => t.InsurerName, o => o.MapFrom(s => s.Product.ProductOwner.RegisteredName))
                .ForMember(t => t.ExternalRatingRequest, o => o.MapFrom(s => s.ExternalRatingRequest))
                .ForMember(t => t.ExternalRatingResult, o => o.MapFrom(s => s.ExternalRatingResult))
                .AfterMap((s,d) => {
                      d.Source = string.IsNullOrWhiteSpace(s.QuoteHeader.Proposal.Source) ? 
                        "iplatform" : 
                        s.QuoteHeader.Proposal.Source; 
                });


            Mapper.CreateMap<QuoteItem, PolicyBindingRequestItemDto>()
                .ForMember(t => t.Description, o => o.MapFrom(s => s.Description))
                .ForMember(t => t.AssetNumber, o => o.MapFrom(s => s.Asset.AssetNumber))
                .ForMember(t => t.Cover, o => o.MapFrom(s => s.CoverDefinition.Cover))
                .ForMember(t => t.Premium, o => o.MapFrom(s => s.Fees.Premium))
                .ForMember(t => t.DiscountedPremium, o => o.MapFrom(s => s.Fees.DiscountedPremium))
                .ForMember(t => t.DiscountedPercentage, o => o.MapFrom(s => s.Fees.DiscountedPercentage))
                .ForMember(t => t.Sasria, o => o.MapFrom(s => s.Fees.DiscountedPercentage))
                .ForMember(t => t.SasriaShortfall, o => o.MapFrom(s => s.Fees.SasriaShortfall))
                .ForMember(t => t.SasriaCalulated, o => o.MapFrom(s => s.Fees.CalculatedSasria))
                .ForMember(t => t.SumInsured, o => o.MapFrom(s => s.Asset.SumInsured))
                .ForMember(t => t.ExcessBasic, o => o.MapFrom(s => s.Excess.Basic))
                .ForMember(t => t.ExcessCalculated, o => o.MapFrom(s => s.Excess.IsCalculated))
                .ForMember(t => t.ExcessDescription, o => o.MapFrom(s => s.Excess.Description))
                .ForMember(t => t.SumInsuredDescription, o => o.MapFrom(s => s.SumInsuredDescription))
                ;

            Mapper.CreateMap<PolicyBindingQuestionDefinition, PolicyBindingQuestionInfoDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.VisibleIndex, o => o.MapFrom(s => s.VisibleIndex))
                .ForMember(t => t.IsProposalQuestion, o => o.MapFrom(s => s.IsProposalQuestion))
                .ForMember(t => t.QuestionType, o => o.Ignore())
                .ForMember(t => t.Name, o => o.Ignore())
                .ForMember(t => t.ToolTip, o => o.Ignore())
                .ForMember(t => t.Regex, o => o.Ignore())
                .ForMember(t => t.Reference, o => o.Ignore())
                .AfterMap((s,d) => {
                    if(s.IsProposalQuestion)
                    {
                        d.Name = s.QuestionDefinition.DisplayName;
                        d.QuestionType = s.QuestionDefinition.Question.QuestionType;
                        d.ToolTip = s.QuestionDefinition.ToolTip;
                        d.Regex = s.QuestionDefinition.RegexPattern;
                        d.Group = new PolicyBindingQuestionGroupInfoDto
                        {
                            Id = s.QuestionDefinition.Question.QuestionGroup.Id,
                            Name = s.QuestionDefinition.Question.QuestionGroup.Name,
                            VisibleIndex = s.QuestionDefinition.Question.QuestionGroup.VisibleIndex
                        };
                    }
                    else
                    {
                        d.Name = s.PolicyBindingQuestion.Name;
                        d.QuestionType = s.PolicyBindingQuestion.QuestionType;
                        d.ToolTip = s.PolicyBindingQuestion.ToolTip;
                        d.Regex = s.PolicyBindingQuestion.Regex;
                        d.Reference = s.PolicyBindingQuestion.Reference;
                        d.Group = new PolicyBindingQuestionGroupInfoDto
                        {
                            Id = s.PolicyBindingQuestionGroup.Id,
                            Name = s.PolicyBindingQuestionGroup.Name,
                            VisibleIndex = s.PolicyBindingQuestionGroup.VisibleIndex
                        };
                    }

                });


            Mapper.CreateMap<QuestionAnswer, QuestionAnswerDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.Answer, o => o.MapFrom(s => s.Answer))
                ;

            Mapper.CreateMap<PolicyBindingQuestionAnswer, QuestionAnswerDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.Answer, o => o.MapFrom(s => s.Answer))
                ;
        }
    }
}