﻿using AutoMapper;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Products;
using iPlatform.Api.DTOs.PolicyBindings;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;
using System.Linq;
using MasterData;
using Domain.QuestionDefinitions;

namespace Domain.PolicyBindings.Mappings
{
    public class PolicyBindingMapper : Profile
    {
        protected override void Configure()
        {

            Mapper.CreateMap<PolicyBinding, PolicyBindingDto>();
                
            Mapper.CreateMap<PolicyBinding, PolicyBindingListDto>();
            Mapper.CreateMap<PolicyBindingQuestionDefinition, PolicyBindingQuestionDefinitionDto>()
                .ForMember(t => t.PolicyBindingQuestion, o => o.Ignore())
                .AfterMap((s, d) => {

                    if (s.IsProposalQuestion && s.QuestionDefinition != null)
                    {
                        d.PolicyBindingQuestion = Mapper.Map<PolicyBindingQuestionDto>(s.QuestionDefinition);
                        d.QuestionDefinitionId = s.QuestionDefinition.Id;
                    }
                    else
                    {
                        d.PolicyBindingQuestion = Mapper.Map<PolicyBindingQuestionDto>(s.PolicyBindingQuestion);
                    }
                });

            Mapper.CreateMap<QuestionDefinition, PolicyBindingQuestionDto>()
                .ForMember(t => t.Id , o => o.MapFrom(s => s.Id))
                .ForMember(t => t.QuestionType, o => o.MapFrom(s => s.Question.QuestionType))
                .ForMember(t => t.Name, o => o.MapFrom(s => s.DisplayName))
                .ForMember(t => t.VisibleIndex, o => o.MapFrom(s => s.VisibleIndex))
                .ForMember(t => t.Regex, o => o.MapFrom(s => s.RegexPattern))
                .ForMember(t => t.ToolTip, o => o.MapFrom(s => s.ToolTip))
                .ForMember(t => t.PolicyBindingQuestionAnswers, o => o.Ignore())
                .AfterMap((s,d) => {
                    var answers = new QuestionAnswers().Where(x => x.Question.Id == s.Question.Id).ToList();
                    if(answers.Any())
                    {
                        foreach(var answer in answers)
                        {
                            d.PolicyBindingQuestionAnswers.Add(Mapper.Map<PolicyBindingQuestionAnswerDto>(answer));
                        }
                    }
                
                });

            Mapper.CreateMap<QuestionAnswer, PolicyBindingQuestionAnswerDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.Name, o => o.MapFrom(s => s.Name))
                .ForMember(t => t.Answer, o => o.MapFrom(s => s.Answer))
                .ForMember(t => t.VisibleIndex, o => o.MapFrom(s => s.VisibleIndex))
                ;

            Mapper.CreateMap<PolicyBindingQuestion, PolicyBindingQuestionDto>();
            Mapper.CreateMap<PolicyBindingQuestionGroup, PolicyBindingQuestionGroupDto>();
            Mapper.CreateMap<PolicyBindingQuestionAnswer, PolicyBindingQuestionAnswerDto>();
            Mapper.CreateMap<PolicyBindingQuestionValidition, PolicyBindingQuestionValiditionDto>();
            Mapper.CreateMap<PolicyBindingAnswer, PolicyBindingAnswerDto>()
                .ForMember(t => t.QuoteItemId, o => o.MapFrom(s => s.QuoteItemId))
                .ForMember(t => t.QuoteId, o => o.MapFrom(s => s.QuoteId))
                ;

            Mapper.CreateMap<SavePolicyBindingDto, CreatePolicyBindingDto>();
            Mapper.CreateMap<SavePolicyBindingDto, UpdatePolicyBindingDto>();


            Mapper.CreateMap<CreatePolicyBindingDto, PolicyBinding>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.CoverDefinition, o => o.MapFrom(s => new CoverDefinition { Id = s.CoverDefinitionId }))
                .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel { Id = s.ChannelId }))
                .ForMember(t => t.PolicyBindingQuestionDefinitions, o => o.Ignore())
                ;

            Mapper.CreateMap<PolicyBindingQuestionDefinitionDto, PolicyBindingQuestionDefinition>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.PolicyBindingQuestionGroup, o => o.Ignore())
                .ForMember(t => t.PolicyBindingQuestion, o => o.Ignore())
                .ForMember(t => t.QuestionDefinition, o => o.MapFrom(s => new QuestionDefinition { Id = s.QuestionDefinitionId }))
                ;
               
            Mapper.CreateMap<PolicyBindingQuestionDto, PolicyBindingQuestion>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.PolicyBindingQuestionAnswers, o => o.Ignore())
                .ForMember(t => t.PolicyBindingQuestionValiditions, o => o.Ignore())
                ;

            Mapper.CreateMap<PolicyBindingQuestionAnswerDto, PolicyBindingQuestionAnswer>()
                .ForMember(t => t.Id, o => o.Ignore())
                ;
            Mapper.CreateMap<PolicyBindingQuestionGroupDto, PolicyBindingQuestionGroup>()
                .ForMember(t => t.Id, o => o.Ignore())
                ;
            Mapper.CreateMap<PolicyBindingQuestionValiditionDto, PolicyBindingQuestionValidition>()
                .ForMember(t => t.Id, o => o.Ignore())
                ;

            Mapper.CreateMap<HandlerResult<PolicyBindingDetailsDto>, POSTResponseDto<PolicyBindingDetailsDto>>()
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                ;

            Mapper.CreateMap<PolicyBindingAnswerDto, PolicyBindingAnswer>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Answer, o => o.MapFrom(s => s.Answer))
                .ForMember(t => t.IsProposalQuestion, o => o.MapFrom(s => s.IsProposalQuestion))
                .ForMember(t => t.QuoteItemId, o => o.MapFrom(s => s.QuoteItemId))
                .ForMember(t => t.QuoteId, o => o.MapFrom(s => s.QuoteId))
                .ForMember(t => t.PolicyBindingQuestionDefinition, o => o.MapFrom(s => new PolicyBindingQuestionDefinition { Id = s.PolicyBindingQuestionDefinitionId }))
                ;

            Mapper.CreateMap<HandlerResult<PolicyBindingNavigationInfoDto>, POSTResponseDto<PolicyBindingNavigationInfoDto>>()
                .ForMember(t => t.Response, o => o.MapFrom(s => s.Response))
                ;

            Mapper.CreateMap<UpdatePolicyBindingDto, PolicyBinding>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.CoverDefinition, o => o.MapFrom(s => new CoverDefinition { Id = s.CoverDefinitionId }))
                .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel { Id = s.ChannelId }))
                .ForMember(t => t.PolicyBindingQuestionDefinitions, o => o.Ignore())
                ;

            Mapper.CreateMap<PolicyBindingConditionalRuleDto, PolicyBindingConditionalRule>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Answer, o => o.MapFrom(s => s.Answer))
                .ForMember(t => t.Reference, o => o.MapFrom(s => s.Reference))
                .ForMember(t => t.Note, o => o.MapFrom(s => s.Note))
                .ForMember(t => t.QuestionFormComparison, o => o.MapFrom(s => new QuestionComparisons().FirstOrDefault(x => x.Id == s.QuestionFormComparisonId)))
                ;

            Mapper.CreateMap<PolicyBindingConditionalRule, PolicyBindingConditionalRuleDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.Answer, o => o.MapFrom(s => s.Answer))
                .ForMember(t => t.Reference, o => o.MapFrom(s => s.Reference))
                .ForMember(t => t.Note, o => o.MapFrom(s => s.Note))
                .ForMember(t => t.QuestionFormComparisonId, o => o.MapFrom(s => s.QuestionFormComparison.Id))
                ;
        }
    }
}