﻿using AutoMapper;
using Domain.Admin;
using Domain.Base;
using Domain.Products;
using iPlatform.Api.DTOs.PolicyBindings;
using System.Collections.Generic;
using System.Linq;
namespace Domain.PolicyBindings
{
    public class PolicyBinding : Entity
    {
        public PolicyBinding()
        {
            PolicyBindingQuestionDefinitions = new List<PolicyBindingQuestionDefinition>();
        }

        public virtual string Name { get; set; }
        public virtual Channel Channel { get; set; }
        public virtual CoverDefinition CoverDefinition { get; set; }
        public virtual string Reference { get; set; }
        public virtual IList<PolicyBindingQuestionDefinition> PolicyBindingQuestionDefinitions { get; set; }

        #region Create
        public static PolicyBinding Create(CreatePolicyBindingDto dto, List<PolicyBindingQuestion> questions, List<PolicyBindingQuestionGroup> groups, List<PolicyBindingConditionalRule> conditionalRules)
        {
            PolicyBinding entity = Mapper.Map<PolicyBinding>(dto);
            CreateQuestions(entity, dto.PolicyBindingQuestionDefinitions, questions, groups, conditionalRules);
            return entity;
        }

        private static void CreateQuestions(PolicyBinding entity, List<PolicyBindingQuestionDefinitionDto> dtos, List<PolicyBindingQuestion> questions, List<PolicyBindingQuestionGroup> groups, List<PolicyBindingConditionalRule> conditionalRules)
        {
            for(int i = 0; i < dtos.Count; i++)
            {
                PolicyBindingQuestionDefinition questionDefinition = PolicyBindingQuestionDefinition.Create(dtos[i], questions, groups, conditionalRules);
                entity.PolicyBindingQuestionDefinitions.Add(questionDefinition);
            }
        }

        #endregion
        #region Update
        public virtual void Update(UpdatePolicyBindingDto dto, List<PolicyBindingQuestion> questions, List<PolicyBindingQuestionGroup> groups, List<PolicyBindingConditionalRule> conditionalRules)
        {
            Mapper.Map(dto, this);
            UpdateQuestionDefinitions(dto, questions, groups, conditionalRules);

        }
        public virtual void UpdateQuestionDefinitions(UpdatePolicyBindingDto dto, List<PolicyBindingQuestion> questions, List<PolicyBindingQuestionGroup> groups, List<PolicyBindingConditionalRule> conditionalRules)
        {
            //delete answers that has been removed from dto
            RemoveOldQuestionDefinitions(dto.PolicyBindingQuestionDefinitions);

            //Add new answers from list
            List<PolicyBindingQuestionDefinitionDto> newQuestionDefinitionDtos = dto.PolicyBindingQuestionDefinitions
                .Where(x => !PolicyBindingQuestionDefinitions.Any(y => y.PolicyBindingQuestion.Reference == x.PolicyBindingQuestion.Reference ||
                                                                        (
                                                                            x.QuestionDefinitionId > 1 &&
                                                                            y.QuestionDefinition != null &&
                                                                            y.QuestionDefinition.Id == x.QuestionDefinitionId
                                                                        )
                                                                 )
                      ).ToList();

            foreach (PolicyBindingQuestionDefinitionDto newQuestionDefinitionDto in newQuestionDefinitionDtos)
            {
                PolicyBindingQuestionDefinitions.Add(PolicyBindingQuestionDefinition.Create(newQuestionDefinitionDto, questions, groups, conditionalRules));
            }

            //update existing answers
            List<PolicyBindingQuestionDefinitionDto> existingQuestionDefinitionDtos = dto.PolicyBindingQuestionDefinitions
                .Where(x => PolicyBindingQuestionDefinitions.Any(y => y.PolicyBindingQuestion.Reference == x.PolicyBindingQuestion.Reference)
                      ).ToList();

            foreach (PolicyBindingQuestionDefinitionDto questionDefinitionDto in existingQuestionDefinitionDtos)
            {
                PolicyBindingQuestionDefinition existingQuestionDefinition = PolicyBindingQuestionDefinitions
                        .FirstOrDefault(x => x.PolicyBindingQuestion.Reference == questionDefinitionDto.PolicyBindingQuestion.Reference);

                existingQuestionDefinition.Update(questionDefinitionDto, questions, groups, conditionalRules);
            }
        }

        public virtual void RemoveOldQuestionDefinitions(List<PolicyBindingQuestionDefinitionDto> questionDefinitionDtos)
        {
            List<PolicyBindingQuestionDefinition> toBeDeleted = PolicyBindingQuestionDefinitions
                .Where(t => !questionDefinitionDtos.Any(y => y.PolicyBindingQuestion.Reference == t.PolicyBindingQuestion.Reference || 
                                                            (
                                                                t.QuestionDefinition != null && 
                                                                y.QuestionDefinitionId > 0 && 
                                                                y.QuestionDefinitionId == t.QuestionDefinition.Id
                                                            )
                                                       )
                      ).ToList();

            foreach (PolicyBindingQuestionDefinition question in toBeDeleted)
            {
                question.Delete();
            }
        }
        #endregion

    }
}
