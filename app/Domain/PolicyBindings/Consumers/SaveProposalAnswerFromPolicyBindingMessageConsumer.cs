﻿using Common.Logging;
using Infrastructure.Configuration;
using Workflow.Domain;
using Workflow.Messages;
using Domain.PolicyBindings.Messages;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums;
using iPlatform.Api.DTOs.Party.ProposalDefinition;

namespace Domain.PolicyBindings.Consumers
{
    public class SaveProposalAnswerFromPolicyBindingMessageConsumer : AbstractMessageConsumer<SaveProposalAnswerFromPolicyBindingMessage>
    {
        private readonly IConnector m_Connector;

        public SaveProposalAnswerFromPolicyBindingMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector) : base(router, executor)
        {
            m_Connector = connector;
            var token = m_Connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            m_Connector.SetToken(token);
        }

        public override void Consume(SaveProposalAnswerFromPolicyBindingMessage message)
        {
            UpdateProposalAnswerFromPolicyBindingDto dto = new UpdateProposalAnswerFromPolicyBindingDto
            {
                Answer = message.Answer.Answer,
                PolicyBindingQuestionDefinitionId = message.Answer.PolicyBindingQuestionDefinitionId,
                ProposalDefinitionId = message.Answer.ProposalDefinitionId,
                ProposalHeaderId = message.Answer.ProposalHeaderId,
                QuestionDefinitionId = message.Answer.QuestionDefinitionId,
                QuoteItemId = message.Answer.QuoteItemId
            };

            var response = m_Connector.IndividualManagement.Individual(message.Answer.PartyId).ProposalDefinitionQuestion(message.Answer.ProposalDefinitionId).UpdateQuestionAnswer(dto);
        }
    }
}
