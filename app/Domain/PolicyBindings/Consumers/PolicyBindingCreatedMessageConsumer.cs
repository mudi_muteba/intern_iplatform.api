﻿using Infrastructure.Configuration;
using Workflow.Domain;
using Workflow.Messages;
using Domain.PolicyBindings.Messages;
using iPlatform.Api.DTOs.Base.Connector;
using iPlatform.Enums;
using iPlatform.Api.DTOs.Activities;

namespace Domain.PolicyBindings.Consumers
{
    public class PolicyBindingCreatedMessageConsumer : AbstractMessageConsumer<PolicyBindingCreatedMessage>
    {
        private readonly IConnector m_Connector;

        public PolicyBindingCreatedMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, IConnector connector) : base(router, executor)
        {
            m_Connector = connector;
            var token = m_Connector.Authentication.Authenticate(ConfigurationReader.Connector.Email, ConfigurationReader.Connector.Password, "127.0.0.1", "INTERNAL", ApiRequestType.Workflow).Token;
            m_Connector.SetToken(token);
        }

        public override void Consume(PolicyBindingCreatedMessage message)
        {
            PolicyBindingCreatedLeadActivityDto dto = new PolicyBindingCreatedLeadActivityDto(message.Event.LeadId, message.Event.CampaignId, message.Event.UserId, message.Event.QuoteId, message.Event.ChannelId);

            var response = m_Connector.LeadManagement.Lead.PolicyBindingCreated(dto);
        }
    }
}
