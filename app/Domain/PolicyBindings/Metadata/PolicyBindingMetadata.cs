﻿using System;
using System.Runtime.Serialization;
using Domain.QuoteAcceptance.Infrastructure;
using Workflow.Messages.Plan.Tasks;
using Domain.QuoteAcceptance.Metadata;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.PolicyBindings.Metadata
{
    public class PolicyBindingMetadata : AbstractMetadata
    {
        public PolicyBindingMetadata() : base()
        {
            
        }

        public PolicyBindingMetadata(ITaskMetadata taskMetadata, ApiMetadata apiMetadata, PolicyBindingRequestDto policyBinding, string productCode,
            RetryStrategy retryStrategy, CommunicationMetadata mail, Guid channelId)
            : base(taskMetadata, apiMetadata, productCode, retryStrategy, mail, channelId)
        {
            PolicyBinding = policyBinding;
        }

        [DataMember] 
        public PolicyBindingRequestDto PolicyBinding { get; set; }

    }
}