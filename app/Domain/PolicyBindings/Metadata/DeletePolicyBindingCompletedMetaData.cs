﻿using System;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Metadata.Shared
{
    public class DeletePolicyBindingCompletedMetaData : ITaskMetadata
    {
        public DeletePolicyBindingCompletedMetaData()
        {
        }

        public DeletePolicyBindingCompletedMetaData(int quoteId, Guid requestId)
        {
            QuoteId = quoteId;
            RequestId = requestId;
        }

        public int QuoteId { get; set; }
        public Guid RequestId { get; set; }
    }
}