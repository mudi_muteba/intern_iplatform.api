﻿using System;
using System.Linq;
using Domain.Base.Workflow;
using Domain.QuoteAcceptance.Metadata;
using Workflow.Messages.Plan.Tasks;

namespace Domain.QuoteAcceptance.Infrastructure.Factories
{
    public class CreatePolicyBindingTransferTaskMetadataFactory : AbstractTaskMetadataFactory<RoutePolicyBindingTask>
    {
        public override ITaskMetadata Create(RoutePolicyBindingTask command)
        {
            var configuration =
                InsurerMetadata.Configurations.FirstOrDefault(
                    w =>
                        w.Key.Any(product => product.Equals(command.ProductCode, StringComparison.CurrentCultureIgnoreCase)));

            if (configuration.Value == null)
            {
                throw new Exception(string.Format("There is no task metadata configured for product code {0}", command.ProductCode));
            }

            return configuration.Value.Generate(command.Dto, command.ProductCode, command.ChannelId, command.SettingsQuoteUploads);
        }
    }
}