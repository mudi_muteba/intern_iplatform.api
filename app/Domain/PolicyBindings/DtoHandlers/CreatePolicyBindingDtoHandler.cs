﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.PolicyBindings;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;

namespace Domain.PolicyBindings.DtoHandlers
{
    public class CreatePolicyBindingDtoHandler : CreationDtoHandler<PolicyBinding, CreatePolicyBindingDto, int>
    {
        private readonly IRepository m_Repository;
        private readonly IExecutionPlan m_ExecutionPlan;
        public CreatePolicyBindingDtoHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider, repository)
        {
            m_Repository = repository;
            m_ExecutionPlan = executionPlan;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    PolicyBindingAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(PolicyBinding entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override PolicyBinding HandleCreation(CreatePolicyBindingDto dto, HandlerResult<int> result)
        {
            List<PolicyBindingQuestion> questions = m_Repository.GetAll<PolicyBindingQuestion>().ToList();
            List<PolicyBindingQuestionGroup> groups = m_Repository.GetAll<PolicyBindingQuestionGroup>().ToList();
            List<PolicyBindingConditionalRule> conditionalRules = m_Repository.GetAll<PolicyBindingConditionalRule>().ToList();
            PolicyBinding entity = PolicyBinding.Create(dto, questions, groups, conditionalRules);

            return entity;
        }
    }
}
