﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.PolicyBindings;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;

namespace Domain.PolicyBindings.DtoHandlers
{
    public class UpdatePolicyBindingDtoHandler : ExistingEntityDtoHandler<PolicyBinding, UpdatePolicyBindingDto, int>
    {
        private readonly IRepository m_Repository;
        private readonly IExecutionPlan m_ExecutionPlan;
        public UpdatePolicyBindingDtoHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider, repository)
        {
            m_Repository = repository;
            m_ExecutionPlan = executionPlan;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    PolicyBindingAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(PolicyBinding entity, UpdatePolicyBindingDto dto, HandlerResult<int> result)
        {
            List<PolicyBindingQuestion> questions = m_Repository.GetAll<PolicyBindingQuestion>().ToList();
            List<PolicyBindingQuestionGroup> groups = m_Repository.GetAll<PolicyBindingQuestionGroup>().ToList();
            List<PolicyBindingConditionalRule> conditionalRules = m_Repository.GetAll<PolicyBindingConditionalRule>().ToList();
            entity.Update(dto, questions, groups, conditionalRules);

            result.Processed(entity.Id);
        }
    }
}
