﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.PolicyBindings;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;
using Domain.Party.Quotes;

namespace Domain.PolicyBindings.DtoHandlers
{
    public class GetPolicyBindingFirstAssetDtoHandler : BaseDtoHandler<GetPolicyBindingFirstAssetDto, int>
    {
        private readonly IRepository m_Repository;
        public GetPolicyBindingFirstAssetDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    PolicyBindingAuthorisation.List
                };
            }
        }

        protected override void InternalHandle(GetPolicyBindingFirstAssetDto dto, HandlerResult<int> result)
        {
            int quoteItemId = 0;
            Quote quote = m_Repository.GetById<Quote>(dto.QuoteId);

            foreach (QuoteItem quoteItem in quote.Items.OrderBy(x => x.Id).ToList())
            {
                var policyBinding = m_Repository.GetAll<PolicyBinding>().FirstOrDefault(x => x.CoverDefinition.Id == quoteItem.CoverDefinition.Id && x.Channel.Id == dto.ChannelId);

                if (policyBinding != null)
                {
                    quoteItemId = quoteItem.Id;
                    break;
                }
            }
            result.Processed(quoteItemId == 0 ? dto.QuoteId : quoteItemId);
        }
    }
}
