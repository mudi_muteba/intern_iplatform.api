﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.PolicyBindings;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;
using System;
using Domain.Party.Quotes;
using AutoMapper;
using Domain.Party.ProposalHeaders;
using NHibernate.Proxy;
using Domain.Leads;
using Domain.Party.ProposalDefinitions;
using MasterData;
using Domain.Products.Fees;
using Domain.QuestionDefinitions;
using Domain.Products;
using Domain.Activities;

namespace Domain.PolicyBindings.DtoHandlers
{
    public class GetPolicyBindingByQuoteDtoHandler : BaseDtoHandler<GetPolicyBindingByQuoteDto, PolicyBindingDetailsDto>
    {
        private readonly IRepository m_Repository;
        public GetPolicyBindingByQuoteDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    PolicyBindingAuthorisation.List
                };
            }
        }

        protected override void InternalHandle(GetPolicyBindingByQuoteDto dto, HandlerResult<PolicyBindingDetailsDto> result)
        {
            PolicyBindingDetailsDto response = new PolicyBindingDetailsDto();
            Quote quote = m_Repository.GetById<Quote>(dto.QuoteId);
            QuoteItem quoteItem = m_Repository.GetById<QuoteItem>(dto.QuoteItemId);
            CoverDefinition coverDef = null;

            if (quoteItem != null)
            {
                coverDef = quoteItem.CoverDefinition;
            }

            if ((quoteItem != null && quoteItem.Quote.Id != dto.QuoteId) || quoteItem == null)
            {
                coverDef = m_Repository.GetAll<CoverDefinition>().FirstOrDefault(x => x.Cover == Covers.GeneralPolicyBinding && x.Product.Id == quote.Product.Id);
            }
            

            var policyBinding = m_Repository.GetAll<PolicyBinding>().FirstOrDefault(x => x.CoverDefinition.Id == coverDef.Id && x.Channel.Id == dto.ChannelId);

            if (policyBinding == null)
            {
                Log.ErrorFormat("No PolicyBinding questions found for quoteItemId: {0} and channelId: {1}", dto.QuoteItemId, dto.ChannelId);
                return;
            }

            response.PolicyBinding = Mapper.Map<PolicyBindingDto>(policyBinding);
            response.CurrentQuoteItemId = dto.QuoteItemId;
            response.QuoteItemDescription = quoteItem.Description;
            response.QuoteId = dto.QuoteId;

            //map lead info also including proposalHeaderId and proposalDefinitionId
            GetLeadInfo(dto, response, quote, quoteItem);
            GetQuoteItemNavigation(dto, response, quote);
            GetQuoteInformation(response, quote, dto.ChannelId);

            //Raiseevent for policyBinding Created
            bool leadActivityExist = m_Repository.GetAll<PolicyBindingCreatedLeadActivity>().Any(x => x.Quote.Id == dto.QuoteId);
            if(!leadActivityExist)
            {
                quote.PolicyBind(response, dto.CampaignId, dto.Context.UserId, dto.ChannelId);
                m_Repository.Save(quote);
            }
            result.Processed(response);
        }

        private void GetQuoteInformation(PolicyBindingDetailsDto response, Quote quote, int ChannelId)
        {
            ProductFee fee = m_Repository.GetAll<ProductFee>().FirstOrDefault(x => x.Channel.Id == ChannelId && x.Product.Id == quote.Product.Id);


            response.QuoteInformation = new PolicyBindingQuoteDto
            {
                OneTimeFee = quote.Fees,
                Premium = quote.TotalPremium,
                ProductCode = quote.Product.ProductCode,
                InsurerCode = quote.Product.ProductOwner.Code,
                InsurerName = quote.Product.ProductOwner.DisplayName,
                Sasria = quote.Items.Sum(x => x.Fees.Sasria),
                PaymentPlan = fee != null ? fee.PaymentPlan: PaymentPlans.Monthly
            };
        }

        private void GetQuoteItemNavigation(GetPolicyBindingByQuoteDto dto, PolicyBindingDetailsDto response, Quote quote)
        {
            List<int> validQuoteItems = new List<int>();

            foreach (QuoteItem quoteItem in quote.Items.Where(x => !x.IsVap).OrderBy(x => x.Id).ToList())
            {
                var policyBinding = m_Repository.GetAll<PolicyBinding>().FirstOrDefault(x => x.CoverDefinition.Id == quoteItem.CoverDefinition.Id && x.Channel.Id == dto.ChannelId);

                if (policyBinding != null)
                {
                    List<PolicyBindingAnswer> answers = m_Repository.GetAll<PolicyBindingAnswer>().Where(x => x.QuoteItemId == quoteItem.Id).ToList();

                    if (quoteItem.Id == dto.QuoteItemId)
                    {
                        MapAnswers(response, answers);
                    }

                    response.Navigations.Add(new PolicyBindingNavigationDto
                    {
                        QuoteItemDescription = quoteItem.Description,
                        QuoteItemId = quoteItem.Id,
                        TotalQuestions = policyBinding.PolicyBindingQuestionDefinitions.Count(),
                        TotalAnswered = answers.Count(x => !string.IsNullOrEmpty(x.Answer))
                    });

                    validQuoteItems.Add(quoteItem.Id);
                }
            }

            //General PolicyBinding based on product and channel
            CoverDefinition coverDef = m_Repository.GetAll<CoverDefinition>().FirstOrDefault(x => x.Cover == Covers.GeneralPolicyBinding && x.Product.Id == quote.Product.Id);
            if (coverDef != null)
            {
                var generalPolicyBinding = m_Repository.GetAll<PolicyBinding>().FirstOrDefault(x => x.CoverDefinition.Id == coverDef.Id && x.Channel.Id == dto.ChannelId);

                if (generalPolicyBinding != null)
                {
                    List<PolicyBindingAnswer> answers = m_Repository.GetAll<PolicyBindingAnswer>().Where(x => x.QuoteId == dto.QuoteId && x.QuoteItemId == dto.QuoteItemId).ToList();

                    if (quote.Id == dto.QuoteItemId)
                    {
                        MapAnswers(response, answers);
                    }

                    response.Navigations.Add(new PolicyBindingNavigationDto
                    {
                        QuoteItemDescription = generalPolicyBinding.Name,
                        QuoteItemId = quote.Id,
                        TotalQuestions = generalPolicyBinding.PolicyBindingQuestionDefinitions.Count(),
                        TotalAnswered = answers.Count(x => !string.IsNullOrEmpty(x.Answer)),
                    });

                    //use quote.Id as quoteitem.id for general section
                    validQuoteItems.Add(quote.Id);
                }
            }

            GetNavigationPolicyBinding(validQuoteItems, dto, response);
        }
        private void GetNavigationPolicyBinding(List<int> quoteItems, GetPolicyBindingByQuoteDto dto, PolicyBindingDetailsDto response)
        {

            for (int i = 0; i < quoteItems.Count; i++)
            {
                if (quoteItems[i] == dto.QuoteItemId)
                {
                    //previous quoteitemid
                    if (i > 0)
                    {
                        response.PreviousQuoteItemId = quoteItems[i - 1];
                    }

                    //next quoteitemid
                    if (quoteItems.Count != 1 && i + 1 != quoteItems.Count)
                    {
                        response.NextQuoteItemId = quoteItems[i + 1];
                    }
                }
            }

            if(response.NextQuoteItemId == 0 && response.PreviousQuoteItemId == 0)
            {
                response.PreviousQuoteItemId = quoteItems[quoteItems.Count - 1];
            }
        }
        private void MapAnswers(PolicyBindingDetailsDto response, List<PolicyBindingAnswer> answers)
        {
            foreach(PolicyBindingQuestionDefinitionDto question in response.PolicyBinding.PolicyBindingQuestionDefinitions.Where(x => !x.IsProposalQuestion).ToList())
            {
                var answer = answers.FirstOrDefault(x => x.PolicyBindingQuestionDefinition.Id == question.Id);
                if(answer != null)
                {
                    question.Answer = answer.Answer;
                }
            }

            if(response.PolicyBinding.PolicyBindingQuestionDefinitions.Any(x => x.IsProposalQuestion))
            {
                MapProposalAnswers(response);
            }

        }

        private void MapProposalAnswers(PolicyBindingDetailsDto response)
        {
            List<ProposalQuestionAnswer> proposalAnswers = m_Repository.GetAll<ProposalQuestionAnswer>().Where(x => x.ProposalDefinition.Id == response.ProposalDefinitionId).ToList();

            foreach(PolicyBindingQuestionDefinitionDto question in response.PolicyBinding.PolicyBindingQuestionDefinitions.Where(x => x.IsProposalQuestion).ToList())
            {
                var answer = proposalAnswers.FirstOrDefault(x => x.QuestionDefinition.Id == question.PolicyBindingQuestion.Id);
                if(answer != null)
                {
                    question.Answer = answer.Answer;
                }
            }
        }

        private void GetLeadInfo (GetPolicyBindingByQuoteDto dto, PolicyBindingDetailsDto response, Quote quote, QuoteItem quoteItem)
        {
            ProposalHeader proposalheader = quote.QuoteHeader.Proposal;
            response.ProposalHeaderId = proposalheader.Id;

            ProposalDefinition proposaldefinition = proposalheader.ProposalDefinitions.FirstOrDefault(x => x.Asset.AssetNo == quoteItem.Asset.AssetNumber);
            if(proposaldefinition != null)
            {
                response.ProposalDefinitionId = proposaldefinition.Id;
            }

            var party = proposalheader.Party;
            if (party != null)
            {
                response.PartyId = party.Id;
                Lead lead = m_Repository.GetAll<Lead>().FirstOrDefault(x => x.Party.Id == party.Id);
                if(lead != null)
                {
                    response.LeadId = lead.Id;
                    response.LeadName = lead.Party.DisplayName;
                }
            }
        }
    }
}
