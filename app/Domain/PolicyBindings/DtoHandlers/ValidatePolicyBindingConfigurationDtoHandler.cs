﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.PolicyBindings;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;
using System;
using Domain.Party.Quotes;
using AutoMapper;
using Domain.Party.ProposalHeaders;
using NHibernate.Proxy;
using Domain.Leads;
using Domain.Party.ProposalDefinitions;
using MasterData;
using Domain.Products.Fees;
using Domain.QuestionDefinitions;
using Domain.Products;
using Domain.Activities;
using iPlatform.Api.DTOs.Base;

namespace Domain.PolicyBindings.DtoHandlers
{
    public class ValidatePolicyBindingConfigurationDtoHandler : BaseDtoHandler<ValidatePolicyBindingConfigurationDto, bool>
    {
        private readonly IRepository m_Repository;
        public ValidatePolicyBindingConfigurationDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    PolicyBindingAuthorisation.List
                };
            }
        }

        protected override void InternalHandle(ValidatePolicyBindingConfigurationDto dto, HandlerResult<bool> result)
        {
            Quote quote = m_Repository.GetById<Quote>(dto.QuoteId);
            List<int> coverDefinitionIds = quote.Items.Select(x => x.CoverDefinition.Id).ToList();


            CoverDefinition coverDefinition = m_Repository.GetAll<CoverDefinition>().FirstOrDefault(x => x.Product.Id == quote.Product.Id && x.Cover.Id == Covers.GeneralPolicyBinding.Id);

            if(coverDefinition != null)
            {
                coverDefinitionIds.Add(coverDefinition.Id);
            }

            IEnumerable<PolicyBinding> policyBindings = m_Repository.GetAll<PolicyBinding>().Where(x => x.Channel.Id == dto.ChannelId && coverDefinitionIds.Contains(x.CoverDefinition.Id));

            if (policyBindings.Any())
            {
                result.Processed(true);
                return;
            }

            result.Processed(false);
        }
    }
}
