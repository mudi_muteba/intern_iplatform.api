﻿
using System;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Campaigns;
using Domain.Party.Quotes;
using Infrastructure.Configuration;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Campaigns;
using iPlatform.Api.DTOs.Ratings.Quoting;
using iPlatform.Api.DTOs.PolicyBindings;
using iPlatform.Api.DTOs.QuoteAcceptance;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;
using Domain.Activities;
using Domain.Admin.Queries;
using Domain.Admin.SettingsiRateUser;
using MasterData;
using Domain.Individuals;
using NHibernate.Proxy;
using Domain.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.PolicyBindings.Request;
using Domain.Users;
using Domain.Party.Contacts;
using Domain.PolicyBindings.Builder;
using Newtonsoft.Json;
using Domain.PolicyBindings.Extensions;
using Domain.Base.Events;
using Domain.Base.Culture;
using iPlatform.Api.DTOs.Activities;
using iPlatform.Api.DTOs.AuditLeadLog;
using Domain.AuditLeadlog.Events;
using Domain.Leads;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Policy;

namespace Domain.PolicyBindings.DtoHandlers
{
    public class SubmitPolicyBindingDtoHandler : ExistingEntityDtoHandler<Quote, SubmitPolicyBindingDto, SubmitPolicyBindingResponseDto>
    {
        private readonly IRepository m_Repository;
        private readonly IProvideContext m_ContextProvider;
        private readonly IPublishEvents m_EventPublisher;
        private readonly IExecutionPlan m_executionPlan;
        public SubmitPolicyBindingDtoHandler(IProvideContext contextProvider, IRepository repository, IPublishEvents eventPublisher, IExecutionPlan executionPlan)
            : base(contextProvider, repository)
        {
            m_Repository = repository;
            m_EventPublisher = eventPublisher;
            m_ContextProvider = contextProvider;
            m_executionPlan = executionPlan;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    SecondLevelUnderwritingAuthorisation.CanAcceptQuote
                };
            }
        }

        protected override void HandleExistingEntityChange(Quote entity, SubmitPolicyBindingDto dto, HandlerResult<SubmitPolicyBindingResponseDto> result)
        {
            //Build complete policy Request
            PolicyBindingRequestDto requestDto = BuildRequest(entity, dto);

            //Logging Full policybinding request as json
            string jsonobject = JsonConvert.SerializeObject(requestDto);
            Log.DebugFormat("policy binding object: {0}", jsonobject);

            var createPolicyWhenQuoteAcceptedDtoResponse = m_executionPlan.Execute<CreatePolicyWhenQuoteAcceptedDto, int>(
                new CreatePolicyWhenQuoteAcceptedDto()
                {
                    PolicyNo = "Pending",
                    Reference = Guid.NewGuid(),
                    QuoteId = entity.Id,
                    QuoteAcceptedLeadActivityId = 0
                }).CreatePOSTResponse();
            if (createPolicyWhenQuoteAcceptedDtoResponse.IsSuccess)
            {
                Log.Info(String.Format("DONE PolicyHeader for Quote {0}", entity.Id));
            }
            else
            {
                result.AddErrors(createPolicyWhenQuoteAcceptedDtoResponse.Errors);
                return;
            }
            //Publish Event
            Publish(requestDto);

            m_Repository.Save(entity);

            //Create Policy Binding Completed Lead Activity
            CreatePolicyBindingLeadActivity(requestDto, entity);

            result.Processed(SubmitPolicyBindingResponseDto.GetSuccess());
        }

        #region Build Request

        private PolicyBindingRequestDto BuildRequest(Quote entity, SubmitPolicyBindingDto dto)
        {
            PolicyBindingRequestDto responseDto = new PolicyBindingRequestDto();
            responseDto.RequestId = Guid.NewGuid();
            responseDto.ApiURL = ConfigurationReader.Connector.BaseUrl;
            responseDto.Environment = ConfigurationReader.Platform.iPlatformEnvironment;
            responseDto.QuotePlatformId = entity.PlatformId;
            responseDto.SetNumberFormat(dto.Context.GetCultureParam());
            responseDto.Comments = dto.Comments;
            responseDto.QuoteId = entity.Id;

            var channel = m_Repository.GetById<Channel>(dto.ChannelId);

            LeadActivity leadActivity = entity.QuoteHeader.Proposal.LeadActivity;

            responseDto.LeadId = leadActivity.Lead.Id;
            responseDto.PartyId = m_Repository.GetById<Lead>(leadActivity.Lead.Id).Party.Id;
            responseDto.InsuredInfo = GetInsuredInfo(leadActivity);
            responseDto.ChannelInfo = Mapper.Map<ChannelDto>(channel);
            responseDto.EchoTCF = new EchoTCFDto(ConfigurationReader.EchoTCF.BaseUrl, ConfigurationReader.EchoTCF.Token, ConfigurationReader.EchoTCF.ByPass);
            responseDto.AgentDetail = new AgentDetailDto(ExecutionContext.UserId, ExecutionContext.Username, ExecutionContext.UserFullname) { Comments = dto.Comments };
            responseDto.CampaignInfo = GetCampaign(dto);
            responseDto.RequestInfo = GetRequestInfo(entity, dto);
            responseDto.UserRatingSettings = GetRatingUserConfiguration();
            var agentPartyId = m_Repository.GetById<User>(ExecutionContext.UserId).UserIndividuals.FirstOrDefault();
            if (agentPartyId != null)
            {
                responseDto.AgentPartyId = agentPartyId.Individual.Id;
            }

            var campaignLeadBucket = m_Repository.GetAll<CampaignLeadBucket>().Where(x => x.Lead.Id == leadActivity.Lead.Id && x.Agent != null).ToList();
            if (campaignLeadBucket.Count > 0)
            {
                if (campaignLeadBucket.FirstOrDefault() != null)
                {
                    var campaignAgent = campaignLeadBucket.FirstOrDefault();
                    if (campaignAgent != null && campaignAgent.Agent != null)
                    {
                        if (campaignAgent.Agent != null)
                        {
                            if (campaignAgent.Agent.Individuals != null)
                            {
                                if (campaignAgent.Agent.Individuals.FirstOrDefault() != null)
                                {
                                    var agentIndividual = campaignAgent.Agent.Individuals.FirstOrDefault();
                                    if (agentIndividual != null)
                                    {
                                        responseDto.AgentId = agentIndividual.Id;
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if (dto.BrokerUserId != 0)
            {
                var brokerUser = m_Repository.GetById<User>(dto.BrokerUserId);

                entity.BrokerUserId = brokerUser.Id;
                entity.BrokerUserExternalReference = brokerUser.ExternalReference;

                responseDto.BrokerUserId = entity.BrokerUserId;
                responseDto.BrokerUserExternalReference = entity.BrokerUserExternalReference;
            }

            if (dto.AccountExecutiveUserId != 0)
            {
                var accountExecutiveUser = m_Repository.GetById<User>(dto.AccountExecutiveUserId);

                entity.AccountExecutiveUserId = accountExecutiveUser.Id;
                entity.AccountExecutiveExternalReference = accountExecutiveUser.ExternalReference;

                responseDto.AccountExecutiveUserId = entity.AccountExecutiveUserId;
                responseDto.AccountExecutiveExternalReference = entity.AccountExecutiveExternalReference;
            }

            if (dto.ChannelId != 0)
            {
                entity.ChannelId = channel.Id;
                entity.ChannelExternalReference = channel.ExternalReference;

                responseDto.Country = channel.Country;//SmartUK add country for channel

                responseDto.ChannelId = entity.ChannelId;
                //this gets overwritten in the LeadFactory in IDS
                responseDto.ChannelExternalReference = entity.ChannelExternalReference;

            }
            return responseDto;
        }
        private InsuredInfoDto GetInsuredInfo(LeadActivity leadActivity)
        {
            InsuredInfoDto insuredInfo = new InsuredInfoDto();

            if (leadActivity.Lead.Party.PartyType == PartyTypes.Individual)
            {
                var party = leadActivity.Lead.Party as Individual;

                if (party == null)
                {
                    var individual = leadActivity.Lead.Party as INHibernateProxy;
                    if (individual != null)
                    {
                        party = individual.HibernateLazyInitializer.GetImplementation() as Individual;
                    }
                }

                insuredInfo = Mapper.Map<InsuredInfoDto>(party);


                List<PartyCorrespondencePreference> correspondenceList = m_Repository.GetAll<PartyCorrespondencePreference>()
                        .Where(a => a.Party.Id == party.Id).ToList();

                insuredInfo.CommunicationPreferences.AddRange(Mapper.Map<List<PartyCorrespondencePreferenceDto>>(correspondenceList));
            }

            return insuredInfo;
        }
        private CampaignInfoDto GetCampaign(SubmitPolicyBindingDto dto)
        {
            var campaign = m_Repository.GetById<Campaign>(dto.CampaignId);
            var campaignInfo = Mapper.Map<CampaignInfoDto>(campaign);
            var userIndividual = m_Repository.GetAll<UserIndividual>().FirstOrDefault(x => x.User.Id == dto.Context.UserId);
            var userDisplayName = userIndividual != null ? userIndividual.Individual.DisplayName : string.Empty;
            var userReference = userIndividual != null ? userIndividual.User.ExternalReference : string.Empty;
            if (userIndividual == null)
            {
                var contact = m_Repository.GetAll<Contact>().FirstOrDefault(x => x.ContactDetail.Email == dto.Context.Username);
                userDisplayName = contact != null ? contact.DisplayName : string.Empty;

            }

            campaignInfo.AgentName = string.IsNullOrEmpty(userDisplayName) ? ExecutionContext.Username : userDisplayName;
            campaignInfo.AgentCode = userReference;
            return campaignInfo;
        }
        private PolicyBindingRequestInfoDto GetRequestInfo(Quote entity, SubmitPolicyBindingDto dto)
        {
            PolicyRequestBuilder builder = new PolicyRequestBuilder(m_Repository, entity, dto.ChannelId, dto.Context.UserId);
            PolicyBindingRequestInfoDto requestInfo = builder.Build();

            return requestInfo;
        }

        #endregion

        private void Publish(PolicyBindingRequestDto dto)
        {
            SubmitPolicyBindingEventBuilder builder = new SubmitPolicyBindingEventBuilder(m_Repository, m_ContextProvider, ExecutionContext);

            var @event = builder.CreateEvent(dto);

            m_EventPublisher.Publish(@event);
        }


        private void CreatePolicyBindingLeadActivity(PolicyBindingRequestDto requestDto, Quote entity)
        {
            var activityDto = new PolicyBindingCompletedLeadActivityDto
            {
                CampaignId = requestDto.CampaignInfo.Id,
                ChannelId = requestDto.ChannelInfo.Id,
                UserId = requestDto.AgentDetail.UserId,
                Comments = requestDto.Comments,
                RequestId = requestDto.RequestId,
                QuoteId = requestDto.QuoteId
            };

            var activity = entity.QuoteHeader.Proposal.LeadActivity;

            var activityProxy = activity as INHibernateProxy;

            if (activityProxy != null)
            {
                activity = (ProposalLeadActivity)activityProxy.HibernateLazyInitializer.GetImplementation();
            }

            var lead = entity.QuoteHeader.Proposal.LeadActivity.Lead;

            //Creating Quote Accepted Lead Activity
            LeadActivity quoteActivity = lead.QuoteAccepted(entity.Id, ExecutionContext.UserId, activity.Campaign);

            //Creating Policy Binding Completed Lead Activity
            LeadActivity policyActivity = lead.PolicyBindingCompleted(activityDto, entity);

            m_Repository.Save(lead);

            entity.Accept();

            RaiseEditAuditLeadLogEvent(entity, requestDto.ChannelInfo.Id == 0 ? ExecutionContext.ActiveChannelId : requestDto.ChannelInfo.Id);
        }

        private void RaiseEditAuditLeadLogEvent(Quote quote, int ChannelId)
        {
            var channel = m_Repository.GetById<Channel>(ChannelId);
            EditAuditLeadLogDto editAuditLeadLogDto = new EditAuditLeadLogDto()
            {
                LeadId = quote.QuoteHeader.Proposal.LeadActivity.Lead.Id,
                Timestamp = DateTime.UtcNow,
                IsAccepted = quote.IsAccepted,
                ProductId = quote.Product.Id,
                ProductName = quote.Product.Name,
                ProductCode = quote.Product.ProductCode,
                SystemId = channel.SystemId,
                EnvironmentType = new AdminConfigurationReader().EnvironmentTypeKey
            };
            quote.RaiseEvent(new EditAuditLeadLogEvent(editAuditLeadLogDto));
        }

        private List<SettingsiRateUserDto> GetRatingUserConfiguration()
        {
            var ratingsUserSettingsQuery = new GetRatingUserConfigurationByUser(m_ContextProvider, m_Repository);
            var user = m_ContextProvider.Get().UserId;
            var ratingUserConfiguration = ratingsUserSettingsQuery.ForUser(user).ExecuteQuery().ToList();
            return ratingUserConfiguration.Any() ? Mapper.Map<List<SettingsiRateUser>, List<SettingsiRateUserDto>>(ratingUserConfiguration) : new List<SettingsiRateUserDto>();
        }
    }


}
