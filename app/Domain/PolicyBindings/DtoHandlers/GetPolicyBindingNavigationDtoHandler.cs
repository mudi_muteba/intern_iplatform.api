﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.PolicyBindings;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;
using System;
using Domain.Party.Quotes;
using AutoMapper;
using Domain.Party.ProposalHeaders;
using NHibernate.Proxy;
using Domain.Leads;
using Domain.Party.ProposalDefinitions;
using MasterData;
using Domain.Products.Fees;
using Domain.QuestionDefinitions;
using Domain.Products;
using Domain.Activities;
using iPlatform.Api.DTOs.Base;

namespace Domain.PolicyBindings.DtoHandlers
{
    public class GetPolicyBindingNavigationDtoHandler : BaseDtoHandler<GetPolicyBindingNavigationDto, PolicyBindingNavigationInfoDto>
    {
        private readonly IRepository m_Repository;
        public GetPolicyBindingNavigationDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    PolicyBindingAuthorisation.List
                };
            }
        }

        protected override void InternalHandle(GetPolicyBindingNavigationDto dto, HandlerResult<PolicyBindingNavigationInfoDto> result)
        {
            Quote quote = m_Repository.GetById<Quote>(dto.QuoteId);
            PolicyBindingNavigationInfoDto response = GetQuoteInformation(dto, quote);
            response.Navigations = GetQuoteItemNavigation(dto, quote);

            result.Processed(response);

        }

        private PolicyBindingNavigationInfoDto GetQuoteInformation(GetPolicyBindingNavigationDto dto, Quote quote)
        {
            ProductFee fee = m_Repository.GetAll<ProductFee>().FirstOrDefault(x => x.Channel.Id == dto.ChannelId && x.Product.Id == quote.Product.Id);

            PolicyBindingNavigationInfoDto result  = new PolicyBindingNavigationInfoDto
            {
                OneTimeFee = quote.Fees,
                Premium = quote.TotalPremium,
                ProductCode = quote.Product.ProductCode,
                InsurerCode = quote.Product.ProductOwner.Code,
                InsurerName = quote.Product.ProductOwner.DisplayName,
                Sasria = quote.Items.Sum(x => x.Fees.Sasria),
                PaymentPlan = fee != null ? fee.PaymentPlan: PaymentPlans.Monthly
            };

            ProposalHeader proposalheader = quote.QuoteHeader.Proposal;

            var party = proposalheader.Party;
            if (party != null)
            {
                result.PartyId = party.Id;
                result.LeadName = party.DisplayName;
            }
            return result;
        }

        private List<PolicyBindingNavigationDto> GetQuoteItemNavigation(GetPolicyBindingNavigationDto dto, Quote quote)
        {
            List<PolicyBindingNavigationDto> navigations = new List<PolicyBindingNavigationDto>();

            foreach (QuoteItem quoteItem in quote.Items.Where(x => !x.IsVap).OrderBy(x => x.Id).ToList())
            {
                var policyBinding = m_Repository.GetAll<PolicyBinding>().FirstOrDefault(x => x.CoverDefinition.Id == quoteItem.CoverDefinition.Id && x.Channel.Id == dto.ChannelId);

                if (policyBinding != null)
                {
                    List<PolicyBindingAnswer> answers = m_Repository.GetAll<PolicyBindingAnswer>().Where(x => x.QuoteItemId == quoteItem.Id).ToList();


                    navigations.Add(new PolicyBindingNavigationDto
                    {
                        QuoteItemDescription = quoteItem.Description,
                        QuoteItemId = quoteItem.Id,
                        TotalQuestions = policyBinding.PolicyBindingQuestionDefinitions.Count(),
                        TotalAnswered = answers.Count(x => !string.IsNullOrEmpty(x.Answer)),
                        Cover = quoteItem.CoverDefinition.Cover
                    });
                }
            }

            //General PolicyBinding based on product and channel
            CoverDefinition coverDef = m_Repository.GetAll<CoverDefinition>().FirstOrDefault(x => x.Cover == Covers.GeneralPolicyBinding && x.Product.Id == quote.Product.Id);
            if (coverDef != null)
            {
                var generalPolicyBinding = m_Repository.GetAll<PolicyBinding>().FirstOrDefault(x => x.CoverDefinition.Id == coverDef.Id && x.Channel.Id == dto.ChannelId);

                if (generalPolicyBinding != null)
                {
                    List<PolicyBindingAnswer> answers = m_Repository.GetAll<PolicyBindingAnswer>().Where(x => x.QuoteId == dto.QuoteId && x.QuoteItemId == dto.QuoteId).ToList();

                    navigations.Add(new PolicyBindingNavigationDto
                    {
                        QuoteItemDescription = generalPolicyBinding.Name,
                        QuoteItemId = quote.Id,
                        TotalQuestions = generalPolicyBinding.PolicyBindingQuestionDefinitions.Count(),
                        TotalAnswered = answers.Count(x => !string.IsNullOrEmpty(x.Answer)),
                        Cover = coverDef.Cover
                    });
                }
            }

            return navigations;
        }

        private void GetLeadInfo (GetPolicyBindingByQuoteDto dto, Quote quote)
        {
            
        }
    }
}
