﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.PolicyBindings;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Admin;

namespace Domain.PolicyBindings.DtoHandlers
{
    public class SavePolicyBindingDtoHandler : BaseDtoHandler<SavePolicyBindingDto, int>
    {
        private readonly IRepository m_Repository;
        private readonly IExecutionPlan m_ExecutionPlan;
        public SavePolicyBindingDtoHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider)
        {
            m_Repository = repository;
            m_ExecutionPlan = executionPlan;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    PolicyBindingAuthorisation.Create
                };
            }
        }

        protected override void InternalHandle(SavePolicyBindingDto dto, HandlerResult<int> result)
        {
            //Get Channel Id
            var channel = m_Repository.GetAll<Channel>().FirstOrDefault(x => x.SystemId == dto.ChannelSystemId);


            //Get Cover Definition Id
            var coverdefinition = m_Repository.GetAll<CoverDefinition>().FirstOrDefault(x => x.Product.Id == dto.ProductId && x.Cover.Id == dto.CoverId);
            dto.CoverDefinitionId = coverdefinition.Id;

            //Validate PolicyBinding Exist
            var entity = m_Repository.GetAll<PolicyBinding>().Where(x => x.Reference == dto.Reference
                                                                    && x.Channel.SystemId == dto.ChannelSystemId
                                                                    && x.CoverDefinition.Id == coverdefinition.Id);
            if (entity.Any())
            {
                //update
                UpdatePolicyBindingDto updateDto = Mapper.Map<UpdatePolicyBindingDto>(dto);
                updateDto.ChannelId = channel.Id;
                updateDto.Id = entity.First().Id;
                var response = m_ExecutionPlan.Execute<UpdatePolicyBindingDto, int>(updateDto);
                result.Processed(response.Response);
            }
            else
            {
                //Create
                CreatePolicyBindingDto createDto = Mapper.Map<CreatePolicyBindingDto>(dto);
                createDto.ChannelId = channel.Id;
                var response = m_ExecutionPlan.Execute<CreatePolicyBindingDto, int>(createDto);
                result.Processed(response.Response);
            }
        }
    }
}
