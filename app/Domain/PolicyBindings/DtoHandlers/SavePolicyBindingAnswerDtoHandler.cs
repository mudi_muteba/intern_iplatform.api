﻿using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.PolicyBindings;
using MasterData.Authorisation;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Admin;

namespace Domain.PolicyBindings.DtoHandlers
{
    public class SavePolicyBindingAnswerDtoHandler : BaseDtoHandler<SavePolicyBindingAnswerDto, bool>
    {
        private readonly IRepository m_Repository;
        private readonly IExecutionPlan m_ExecutionPlan;
        public SavePolicyBindingAnswerDtoHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan)
            : base(contextProvider)
        {
            m_Repository = repository;
            m_ExecutionPlan = executionPlan;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    PolicyBindingAuthorisation.Create
                };
            }
        }

        protected override void InternalHandle(SavePolicyBindingAnswerDto dto, HandlerResult<bool> result)
        {
            List<PolicyBindingAnswer> existingAnswers = m_Repository.GetAll<PolicyBindingAnswer>().Where(x => x.QuoteItemId == dto.Answers.First().QuoteItemId).ToList();
            foreach(PolicyBindingAnswerDto answer in dto.Answers)
            {
                PolicyBindingAnswer existingAnswer = existingAnswers.FirstOrDefault(x => x.PolicyBindingQuestionDefinition.Id == answer.PolicyBindingQuestionDefinitionId);

                if (existingAnswer == null)
                {
                    PolicyBindingAnswer entity = PolicyBindingAnswer.Create(answer);
                    m_Repository.Save(entity);
                }
                else
                {
                    existingAnswer.Update(answer);
                    m_Repository.Save(existingAnswer);
                }
            }
            result.Processed(true);
        }
    }
}
