﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.PolicyBindings;
using System.Collections.Generic;
using System.Linq;

namespace Domain.PolicyBindings
{
    public class PolicyBindingQuestionGroup : Entity
    {
        public PolicyBindingQuestionGroup()
        {

        }

        public virtual string Name { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual string Reference { get; set; }


        public static PolicyBindingQuestionGroup Create(PolicyBindingQuestionGroupDto dto)
        {
            PolicyBindingQuestionGroup entity = Mapper.Map<PolicyBindingQuestionGroup>(dto);
            return entity;
        }

        public virtual PolicyBindingQuestionGroup Update(PolicyBindingQuestionGroupDto dto)
        {
            Mapper.Map(dto, this);
            return this;
        }
    }
}
