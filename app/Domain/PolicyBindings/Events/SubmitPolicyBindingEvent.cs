﻿
using System;
using System.Collections.Generic;
using Domain.Admin;
using Domain.Base.Events;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.Ratings.Events
{
    public class SubmitPolicyBindingEvent : BaseDomainEvent, IProductRelatedDomainEvent
    {
        public SubmitPolicyBindingEvent(PolicyBindingRequestDto dto, string productCode, string insurerCode, EventAudit audit, ChannelReference channelReference, List<SettingsQuoteUploadDto> settingsQuoteUploads) 
            : base(audit, new List<ChannelReference>() { channelReference })
        {
            Dto = dto;
            ProductCode = productCode;
            InsurerCode = insurerCode;
            SettingsQuoteUploads = settingsQuoteUploads;
        }

        public PolicyBindingRequestDto Dto { get; set; }
        public string ProductCode { get; set; }
        public string InsurerCode { get; set; }
        public List<SettingsQuoteUploadDto> SettingsQuoteUploads { get; set; }
    }
}