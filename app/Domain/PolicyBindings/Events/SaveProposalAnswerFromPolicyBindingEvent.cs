﻿using Domain.Base.Events;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.PolicyBindings.Events
{
    public class SaveProposalAnswerFromPolicyBindingEvent : BaseDomainEvent, ExpressDomainEvent
    {

        public PolicyBindingAnswerDto Answer { get; set; }
        public SaveProposalAnswerFromPolicyBindingEvent(PolicyBindingAnswerDto answer)
        {
            Answer = answer;
        }
    }
}
