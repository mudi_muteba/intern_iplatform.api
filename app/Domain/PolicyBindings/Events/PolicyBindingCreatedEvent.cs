﻿using Domain.Base.Events;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.PolicyBindings.Events
{
    public class PolicyBindingCreatedEvent : BaseDomainEvent, ExpressDomainEvent
    {

        public int QuoteId { get; set; }
        public int UserId { get; set; }
        public int PartyId { get; set; }
        public int LeadId { get; set; }
        public int CampaignId { get; set; }
        public int ChannelId { get; set; }
        public PolicyBindingCreatedEvent()
        {
        }
    }
}
