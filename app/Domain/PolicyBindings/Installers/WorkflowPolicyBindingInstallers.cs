﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Domain.Base.Workflow;
using Domain.QuoteAcceptance.Infrastructure.Factories;

namespace Domain.PolicyBindings.Installers
{
    public class WorkflowPolicyBindingInstallers : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining<CreatePolicyBindingTransferTaskMetadataFactory>()
                .BasedOn(typeof (ICreateTaskMetdata<>))
                .WithServiceAllInterfaces()
                .LifestyleTransient());
        }
    }
}
