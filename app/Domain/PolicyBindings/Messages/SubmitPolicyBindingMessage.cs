﻿using System.Runtime.Serialization;
using Domain.Base.Workflow;
using Domain.QuoteAcceptance.Infrastructure;
using iPlatform.Enums.Workflows;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.PolicyBindings.Messages
{
    [WorkflowExecutionMessageType(WorkflowMessageType.PolicyBinding)]
    public class SubmitPolicyBindingMessage : WorkflowExecutionMessage
    {
        [DataMember]
        public PolicyBindingRequestDto Dto { get; set; }

        [DataMember]
        public IRetryStrategy RetryStrategy { get; set; }


        public SubmitPolicyBindingMessage(PolicyBindingRequestDto dto, ITaskMetadata metadata, IRetryStrategy retryStrategy) : base(metadata)
        {
            Dto = dto;
            RetryStrategy = retryStrategy;
            ExecutionPlan = new ExecutionPlan();
        }

        public SubmitPolicyBindingMessage(PolicyBindingRequestDto dto, IRetryStrategy retryStrategy)
        {
            Dto = dto;
            RetryStrategy = retryStrategy;
            ExecutionPlan = new ExecutionPlan();
        }

        public SubmitPolicyBindingMessage()
        {
            RetryStrategy = new RetryStrategy(2,2);
            ExecutionPlan = new ExecutionPlan();
        }
    }
}