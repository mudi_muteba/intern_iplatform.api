﻿using Domain.QuoteAcceptance.Metadata.Shared;
using System;
using Workflow.Messages;
using Workflow.Messages.Plan.Tasks;

namespace Domain.PolicyBindings.Messages
{
    public class DeletePolicyBindingCompletedMessage : WorkflowExecutionMessage
    {
        public DeletePolicyBindingCompletedMessage(ITaskMetadata metadata)
            : base(metadata)
        {

        }

        public static DeletePolicyBindingCompletedMessage Create(int quoteId, Guid requestId)
        {
            return new DeletePolicyBindingCompletedMessage(new DeletePolicyBindingCompletedMetaData(quoteId, requestId));
        }
    }
}
