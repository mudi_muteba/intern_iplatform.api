﻿using iPlatform.Api.DTOs.PolicyBindings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workflow.Messages;

namespace Domain.PolicyBindings.Messages
{
    public class SaveProposalAnswerFromPolicyBindingMessage : WorkflowExecutionMessage
    {
        public PolicyBindingAnswerDto Answer { get; set; }
        public SaveProposalAnswerFromPolicyBindingMessage(PolicyBindingAnswerDto answer,
             WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
            Answer = answer;
        }
    }
}
