﻿using Domain.PolicyBindings.Events;
using Workflow.Messages;

namespace Domain.PolicyBindings.Messages
{
    public class PolicyBindingCreatedMessage : WorkflowExecutionMessage
    {
        public PolicyBindingCreatedEvent Event { get; set; }
        public PolicyBindingCreatedMessage(PolicyBindingCreatedEvent policyBindingCreatedEvent,
             WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType)
        {
            Event = policyBindingCreatedEvent;
        }
    }
}
