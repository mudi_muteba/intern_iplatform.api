﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.PolicyBindings.Overrides
{
    public class PolicyBindingAnswerOverride : IAutoMappingOverride<PolicyBindingAnswer>
    {
        public void Override(AutoMapping<PolicyBindingAnswer> mapping)
        {
            mapping.References(x => x.PolicyBindingQuestionDefinition).Cascade.None().LazyLoad(Laziness.NoProxy);
        }
    }
}