﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.PolicyBindings.Overrides
{
    public class PolicyBindingConditionalRuleOverride : IAutoMappingOverride<PolicyBindingConditionalRule>
    {
        public void Override(AutoMapping<PolicyBindingConditionalRule> mapping)
        {
            mapping.References(x => x.QuestionFormComparison).LazyLoad(Laziness.NoProxy);
        }
    }
}
