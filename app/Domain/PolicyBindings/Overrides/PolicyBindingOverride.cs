﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.PolicyBindings.Overrides
{
    public class PolicyBindingOverride : IAutoMappingOverride<PolicyBinding>
    {
        public void Override(AutoMapping<PolicyBinding> mapping)
        {
            mapping.References(x => x.Channel).LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.CoverDefinition).LazyLoad(Laziness.NoProxy);
            mapping.HasMany(x => x.PolicyBindingQuestionDefinitions).Cascade.SaveUpdate();
        }
    }
}