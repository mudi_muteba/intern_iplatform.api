﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.PolicyBindings.Overrides
{
    public class PolicyBindingQuestionOverride : IAutoMappingOverride<PolicyBindingQuestion>
    {
        public void Override(AutoMapping<PolicyBindingQuestion> mapping)
        {
            mapping.HasMany(x => x.PolicyBindingQuestionAnswers).Cascade.SaveUpdate();
            mapping.HasMany(x => x.PolicyBindingQuestionValiditions).Cascade.SaveUpdate();
            mapping.References(x => x.QuestionType).LazyLoad(Laziness.NoProxy);
        }
    }
}