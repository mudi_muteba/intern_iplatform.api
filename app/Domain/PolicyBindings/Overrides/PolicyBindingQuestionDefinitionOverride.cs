﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.PolicyBindings.Overrides
{
    public class PolicyBindingQuestionDefinitionOverride : IAutoMappingOverride<PolicyBindingQuestionDefinition>
    {
        public void Override(AutoMapping<PolicyBindingQuestionDefinition> mapping)
        {
            mapping.References(x => x.PolicyBindingQuestionGroup).Cascade.SaveUpdate().LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.PolicyBindingQuestion).Cascade.SaveUpdate().LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.QuestionDefinition).LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.PolicyBindingConditionalRule).Cascade.SaveUpdate().LazyLoad(Laziness.NoProxy);
        }
    }
}