﻿using System.Linq;
using Domain.Party.ProposalDefinitions;
using Domain.Party.Quotes;
using iPlatform.Api.DTOs.Ratings.Request;
using Domain.Base.Repository;
using MasterData;
using System.Collections.Generic;
using Domain.Party.Assets;
using Domain.QuestionDefinitions;
using Domain.Party.Quotes.Builders;
using Common.Logging;
using Domain.Individuals;
using Domain.Party.Contacts;
using Domain.Components.ComponentDefinitions;
using Domain.Components.ComponentQuestionAnswers;
using Domain.Components.ComponentQuestionDefinitions;

namespace Domain.PolicyBindings.Builder
{
    public class RatingRequestItemBuilder
    {
        private readonly Quote m_Quote;
        private readonly QuoteItem m_Item;
        private readonly ProposalDefinition m_ProposalDefinition;
        private readonly IRepository m_Repository;
        protected static readonly ILog m_Log = LogManager.GetLogger<RatingRequestItemBuilder>();
        public RatingRequestItemBuilder(IRepository repository, Quote quote, QuoteItem item)
        {
            m_Repository = repository;
            m_Quote = quote;
            m_Item = item;

            if(quote != null)
            {
                m_ProposalDefinition = quote.QuoteHeader.Proposal.ProposalDefinitions.FirstOrDefault(x => x.Cover.Id == item.CoverDefinition.Cover.Id && x.Asset.AssetNo == item.Asset.AssetNumber);
                
                if(m_ProposalDefinition == null)
                {
                    m_ProposalDefinition = quote.QuoteHeader.Proposal.ProposalDefinitions.FirstOrDefault(x => x.Cover.Id == item.CoverDefinition.Cover.Id && x.Id == item.Asset.AssetNumber);
                }
            }
        }


        public RatingRequestItemDto Build()
        {
            var requestItem = new RatingRequestItemDto()
            {
                AssetNo = m_Item.Asset.AssetNumber,
                Cover = m_Item.CoverDefinition.Cover,
                RatingDate = m_Quote.CreatedAt,
                ProductCode = m_Quote.Product.ProductCode,
                IsVap = m_Item.IsVap
            };

            if (m_ProposalDefinition != null)
            {
                AppendCoverAnswers(requestItem, m_Quote, m_Item);
                AppendFuneralMembers(m_ProposalDefinition, requestItem);

                //Accessories
                AppendExtras(m_ProposalDefinition, requestItem);
                AppendComponentLossHistory(m_ProposalDefinition, requestItem);

                //added Driver loss builder
                //var ratingRequestDtoBuilder = new RatingRequestDtoBuilder(m_Repository);
                //ratingRequestDtoBuilder.AppendDriverLoss(m_ProposalDefinition, requestItem);
            }

            return requestItem;
        }

        private void AppendCoverAnswers(RatingRequestItemDto requestItem, Quote entity, QuoteItem item)
        {
            List<int> coverDefQuestionIds = m_Repository.GetAll<QuestionDefinition>().Where(a => a.CoverDefinition.Cover.Id == item.CoverDefinition.Cover.Id && 
            a.CoverDefinition.Product.Id == m_ProposalDefinition.Product.Id).Select(a => a.Id).Distinct().ToList();


            IQueryable<int> mappedQuestionIds = m_Repository.GetAll<MapQuestionDefinition>()
                .Where(a => item.CoverDefinition.Id != a.ParentId && item.CoverDefinition.Cover.Id == item.CoverDefinition.Cover.Id)
                .Select(a => a.ChildId).Distinct();

            IQueryable<int> questionDefinitionIds = m_Repository.GetAll<QuestionDefinition>().Where(
                a => coverDefQuestionIds.Contains(a.Id) && !mappedQuestionIds.Contains(a.Id))
                .Select(x => x.Id);

            List<ProposalQuestionAnswer> proposalQuestionAnswers = m_Repository.GetAll<ProposalQuestionAnswer>()
                .Where(x => questionDefinitionIds.Contains(x.QuestionDefinition.Id) && x.ProposalDefinition.Id == m_ProposalDefinition.Id).ToList();


            var requestItemBuilder = new RatingRequestItemQuestionAnswerBuilderFactory();

            foreach (var questionAnswer in proposalQuestionAnswers)
            {
                //cater for empty answer in boolean
                if (questionAnswer.QuestionDefinition != null && questionAnswer.QuestionDefinition.Question.QuestionType == QuestionTypes.Checkbox)
                {
                    questionAnswer.Answer = questionAnswer.Answer.ConvertToBoolString();
                }

                m_Log.InfoFormat("Adding question '{0}' with answer '{1}' to rating request",
                    questionAnswer.QuestionDefinition.Question.Name,
                    questionAnswer.Answer);

                var requestItemAnswer = requestItemBuilder.Create(questionAnswer);
                requestItem.Answers.Add(requestItemAnswer);
            }
        }
        private void AppendFuneralMembers(ProposalDefinition definition, RatingRequestItemDto requestItem)
        {
            List<AdditionalMembers> members = m_Repository.GetAll<AdditionalMembers>()
                    .Where(a => a.ProposalDefinition.Id == definition.Id && a.IsDeleted != true)
                    .ToList();

            foreach (AdditionalMembers item in members)
            {
                var funeralMember = new RatingRequestItemFuneralDto
                {
                    MemberIdentifier = item.Id.ToString(),
                    MemberIdNumber = item.IdNumber,
                };

                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AIGFuneralRelationship,
                    QuestionAnswer = item.MemberRelationship.Name
                });
                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AdditionalMemberDateOnCover,
                    QuestionAnswer = item.DateOnCover
                });
                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AdditionalMemberIdNumber,
                    QuestionAnswer = item.IdNumber
                });
                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AdditionalMemberInitials,
                    QuestionAnswer = item.Initials
                });
                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AdditionalMemberStudent,
                    QuestionAnswer = item.IsStudent
                });
                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AdditionalMemberSurname,
                    QuestionAnswer = item.Surname
                });
                funeralMember.FuneralAnswers.Add(new RatingRequestItemFuneralAnswerDto
                {
                    Question = Questions.AdditionalMemberDateOfBirth,
                    QuestionAnswer = item.DateOfBirth
                });

                requestItem.FuneralMembers.Add(funeralMember);
            }
        }
        private static void AppendExtras(ProposalDefinition definition, RatingRequestItemDto requestItem)
        {
            if (definition.Asset == null)
            {
                return;
            }

            foreach (AssetExtras assetExtra in definition.Asset.Extras.Where(a => a.Selected).ToList())
            {
                var item = new RatingRequestItemAccessoriesDto();

                var extra = new RatingRequestItemAccessoriesAnswerDto
                {
                    Question = Questions.AccessoriesCategory,
                    QuestionAnswer = assetExtra.AccessoryType == null ? null : assetExtra.AccessoryType.Answer
                };

                item.AccessoryAnswers.Add(extra);

                item.AccessoryAnswers.Add(new RatingRequestItemAccessoriesAnswerDto
                {
                    Question = Questions.AccessoriesDescription,
                    QuestionAnswer = assetExtra.Description
                });

                item.AccessoryAnswers.Add(new RatingRequestItemAccessoriesAnswerDto
                {
                    Question = Questions.AccessoriesSuminsured,
                    QuestionAnswer = assetExtra.Value
                });

                item.AccessoryAnswers.Add(new RatingRequestItemAccessoriesAnswerDto
                {
                    Question = Questions.AccessoriesIsFactoryFitted,
                    QuestionAnswer = !assetExtra.IsUserDefined
                });


                requestItem.Accessories.Add(item);
            }
        }
        #region Component Builders
        private void AppendComponentPolicyHolderLossHistory(int partyId, RatingRequestDto request, string idNumber)
        {
            var ratingLossHistoryDto = new RatingLossHistoryDto()
            {
                PartyId = partyId,
                IdentityNumber = idNumber,
                IsPolicyHolder = true,
                PartyType = PartyTypes.Individual,
                Items = ComponentAnswerBuilder(partyId)
            };
        }
        private void AppendComponentLossHistory(ProposalDefinition definition, RatingRequestItemDto requestItem)
        {
            var ratingLossHistoryDto = new RatingLossHistoryDto();

            //Set IdentityNumber
            ratingLossHistoryDto.IdentityNumber = definition.ProposalQuestionAnswers.Where(a => a.QuestionDefinition.Question == Questions.MainDriverIDNumber)
                    .Select(a => a.Answer)
                    .FirstOrDefault();
            if (ratingLossHistoryDto.IdentityNumber == null)
            {
                return;
            }

            //Set Party ID of idNumber
            var individual = m_Repository.GetAll<Individual>()
                .Where(x => x.IdentityNo == ratingLossHistoryDto.IdentityNumber)
                .FirstOrDefault();

            if (individual != null)
            {
                ratingLossHistoryDto.PartyId = individual.Id;
            }
            else
            {
                ratingLossHistoryDto.PartyId = m_Repository.GetAll<Contact>()
                    .Where(x => x.IdentityNo == ratingLossHistoryDto.IdentityNumber)
                    .FirstOrDefault().Id;
            }

            //Set PartyType
            ratingLossHistoryDto.PartyType = m_Repository.GetById<Party.Party>(ratingLossHistoryDto.PartyId).PartyType;

            //Set IsPolicyHolder 
            ratingLossHistoryDto.IsPolicyHolder = (ratingLossHistoryDto.PartyId == definition.Party.Id) ? true : false;

            ratingLossHistoryDto.Items = ComponentAnswerBuilder(ratingLossHistoryDto.PartyId);

            requestItem.LossHistoryComponent.Add(ratingLossHistoryDto);
        }
        private List<RatingComponentItemDto> ComponentAnswerBuilder(int partyId)
        {
            var ratingComponentItems = new List<RatingComponentItemDto>();
            var componentDefinitionList = m_Repository.GetAll<ComponentDefinition>().Where(x => x.PartyId == partyId)
                .ToList();

            foreach (var item in componentDefinitionList)
            {
                var Answers = m_Repository.GetAll<ComponentQuestionAnswer>()
                    .Where(x => x.ComponentDefinitionId == item.Id);

                foreach (var answer in Answers)
                {
                    var questionDefinition = m_Repository.GetById<ComponentQuestionDefinition>(answer.ComponentQuestionDefinitionId);
                    var question = new ComponentQuestions().FirstOrDefault(x => x.Id == questionDefinition.Id);

                    if (question != null)
                        ratingComponentItems.Add(new RatingComponentItemDto()
                        {
                            ComponentDefinitionId = answer.ComponentDefinitionId,
                            Question = question.Name,
                            Answer = answer.Answer,
                            QuestionGroup = question.ComponentQuestionGroup,
                            QuestionType = question.QuestionType
                        });
                }
            }

            return ratingComponentItems;
        }
        #endregion
    }
}
