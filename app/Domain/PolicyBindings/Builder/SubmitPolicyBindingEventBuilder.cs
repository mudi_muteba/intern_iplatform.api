﻿using Common.Logging;
using Domain.Admin;
using Domain.Admin.Queries;
using Domain.Admin.SettingsQuoteUploads;
using Domain.Admin.SettingsQuoteUploads.Queries;
using Domain.Base.Events;
using Domain.Base.Execution;
using Domain.Base.Execution.Exceptions;
using Domain.Base.Repository;
using Domain.Ratings.Events;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using iPlatform.Api.DTOs.PolicyBindings;
using System.Collections.Generic;
using System.Linq;
using ValidationMessages.Channels;

namespace Domain.PolicyBindings.Extensions
{
    public class SubmitPolicyBindingEventBuilder
    {
        private readonly IRepository m_Repository;
        private readonly ExecutionContext m_ExecutionContext;
        private readonly IProvideContext m_ProvideContext;
        private readonly GetSettingsQuoteAcceptanceQuery m_SettingsQuoteAcceptanceQuery;
        private readonly GetChannelEventConfigurationQuery m_GetChannelEventConfigurationQuery;

        private readonly ILog m_Log = LogManager.GetLogger<SubmitPolicyBindingEventBuilder>();
        public SubmitPolicyBindingEventBuilder(IRepository repository, IProvideContext providecontext, ExecutionContext executionContext)
        {
            m_Repository = repository;
            m_ExecutionContext = executionContext;
            m_ProvideContext = providecontext;
            m_SettingsQuoteAcceptanceQuery = new GetSettingsQuoteAcceptanceQuery(providecontext, repository);
            m_GetChannelEventConfigurationQuery = new GetChannelEventConfigurationQuery(providecontext, repository);
        }
        public SubmitPolicyBindingEvent CreateEvent(PolicyBindingRequestDto dto)
        {
            SubmitPolicyBindingEvent @event = new SubmitPolicyBindingEvent(
                                                    dto,
                                                    dto.RequestInfo.ProductCode,
                                                    dto.RequestInfo.InsurerCode,
                                                    m_ExecutionContext.CreateAudit(),
                                                    GetChannel(dto.ChannelInfo.Id),
                                                    GetSettingsQuoteUpload(dto));


            return @event;
        }

        private List<SettingsQuoteUploadDto> GetSettingsQuoteUpload(PolicyBindingRequestDto dto)
        {
            var settings = m_SettingsQuoteAcceptanceQuery
                                .WithChannelId(dto.ChannelInfo.Id)
                                .WithProductId(dto.RequestInfo.ProductId)
                                .WithEnvironment(dto.Environment)
                                .IsPolicyBinding(true)
                                .ExecuteQuery().ToList();

            if (!settings.Any())
            {
                m_Log.ErrorFormat("No SettingsQuoteUpload found for Channel:{0} and Product:{1} and Environment:{2}", dto.ChannelInfo.Id, dto.RequestInfo.ProductId, dto.Environment);
            }
            return GetSettingsQuoteUploadDto(settings);
        }

        private List<SettingsQuoteUploadDto> GetSettingsQuoteUploadDto(List<SettingsQuoteUpload> settings)
        {
            var settingsDto = new List<SettingsQuoteUploadDto>();

            settings.ForEach(x =>
            {
                settingsDto.Add(new SettingsQuoteUploadDto()
                {
                    SettingName = x.SettingName,
                    SettingValue = x.SettingValue
                });
            });

            //Adding SettingQuoteUpload Logging
            if (settingsDto.Any())
            {
                m_Log.InfoFormat("Using Settings below for Upload/Acceptance");
            }
                
            settingsDto.ForEach(x => {
                m_Log.InfoFormat("NAME/VALUE: {0} : {1}", x.SettingName, x.SettingValue);
            });

            return settingsDto;
        }

        private Channel GetChannel(int channelId)
        {
            Channel channel = m_Repository.GetById<Channel>(channelId);

            bool channelEventExist = channel.ChannelConfigurations.Any(x => x.EventName == typeof(SubmitPolicyBindingEvent).Name);

            if (!channelEventExist)
            {
                throw new ValidationException(ChannelValidationMessages.NoChannelEventConfiguration(typeof(SubmitPolicyBindingEvent).Name));
            }

            return channel;
        }


    }
}
