﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterData;
using iPlatform.Api.DTOs.PolicyBindings.Request;
using AutoMapper;

namespace Domain.PolicyBindings.Builder
{
    internal class PolicyBindingItemBuilderFactory
    {
        private readonly Dictionary<Func<PolicyBindingAnswer, bool>, Func<PolicyBindingAnswer, PolicyBindingItemAnswerBuilder>> mapping =
            new Dictionary<Func<PolicyBindingAnswer, bool>, Func<PolicyBindingAnswer, PolicyBindingItemAnswerBuilder>>();

        public PolicyBindingItemBuilderFactory()
        {
            //default -> textbox, checkbox, date, bank, bank branch, Address, 
            mapping.Add(qa => qa.GetQuestionType().Id == QuestionTypes.Textbox.Id, qa => new DefaultQuestionTypeBuilder(qa));
            mapping.Add(qa => qa.GetQuestionType().Id == QuestionTypes.Checkbox.Id, qa => new DefaultQuestionTypeBuilder(qa));
            mapping.Add(qa => qa.GetQuestionType().Id == QuestionTypes.Date.Id, qa => new DefaultQuestionTypeBuilder(qa));
            mapping.Add(qa => qa.GetQuestionType().Id == QuestionTypes.Bank.Id, qa => new DefaultQuestionTypeBuilder(qa));
            mapping.Add(qa => qa.GetQuestionType().Id == QuestionTypes.BankBranch.Id, qa => new DefaultQuestionTypeBuilder(qa));
            mapping.Add(qa => qa.GetQuestionType().Id == QuestionTypes.Address.Id, qa => new DefaultQuestionTypeBuilder(qa));
            mapping.Add(qa => qa.GetQuestionType().Id == QuestionTypes.Asset.Id, qa => new DefaultQuestionTypeBuilder(qa));

            //dropdown
            mapping.Add(qa => qa.GetQuestionType().Id == QuestionTypes.Dropdown.Id, qa => new DropdownQuestionTypeBuilder(qa));

            //Year of Manufacture
            mapping.Add(qa => qa.GetQuestionType().Id == QuestionTypes.Dropdown.Id && qa.IsProposalQuestion &&
                    qa.PolicyBindingQuestionDefinition.QuestionDefinition.Question.Id == 
                    Questions.YearOfManufacture.Id, qa => new YearOfManufactureQuestionTypeBuilder(qa));

        }

        public ItemPolicyBindingQuestionAnswerDto Create(PolicyBindingAnswer questionAnswer)
        {
            var match = mapping.FirstOrDefault(q => q.Key(questionAnswer));

            if (match.Value == null)
            {
                throw new Exception(string.Format("No question reader for question type {0}", questionAnswer.GetQuestionType().Id));
            }
                
            return match.Value(questionAnswer).Build();
        }
    }

    internal class YearOfManufactureQuestionTypeBuilder : PolicyBindingItemAnswerBuilder
    {
        public YearOfManufactureQuestionTypeBuilder(PolicyBindingAnswer policyBindingAnswer)
            : base(policyBindingAnswer)
        {
        }

        protected override void InternalBuild(ItemPolicyBindingQuestionAnswerDto questionAnswer)
        {
            questionAnswer.Answer = m_PolicyBindingAnswer.Answer;
            questionAnswer.Question.QuestionType = QuestionTypes.Textbox;
        }
    }

    internal class DefaultQuestionTypeBuilder : PolicyBindingItemAnswerBuilder
    {
        public DefaultQuestionTypeBuilder(PolicyBindingAnswer policyBindingAnswer)
            : base(policyBindingAnswer)
        {
        }

        protected override void InternalBuild(ItemPolicyBindingQuestionAnswerDto questionAnswer)
        {
            questionAnswer.Answer = m_PolicyBindingAnswer.Answer;
        }
    }

    internal class DropdownQuestionTypeBuilder : PolicyBindingItemAnswerBuilder
    {
        public DropdownQuestionTypeBuilder(PolicyBindingAnswer policyBindingAnswer)
            : base(policyBindingAnswer)
        {
        }

        protected override void InternalBuild(ItemPolicyBindingQuestionAnswerDto questionAnswer)
        {
            int selectedId;
            if (!int.TryParse(m_PolicyBindingAnswer.Answer, out selectedId))
            {
                return;
            }
            var possibleAnswers = GetPossibleAnswers();

            questionAnswer.Answer = possibleAnswers.FirstOrDefault(pa => pa.Id == selectedId).Answer;
        }

        private IEnumerable<QuestionAnswerDto> GetPossibleAnswers()
        {
            List<QuestionAnswerDto> AnswersDto = new List<QuestionAnswerDto>(); 
            if(m_PolicyBindingAnswer.IsProposalQuestion)
            {
                List<QuestionAnswer> answers  = new QuestionAnswers()
                    .Where(x => x.Question.Id == m_PolicyBindingAnswer.PolicyBindingQuestionDefinition.QuestionDefinition.Question.Id)
                    .ToList();
                AnswersDto = Mapper.Map<List<QuestionAnswerDto>>(answers);
            }
            else
            {
                var answers = m_PolicyBindingAnswer.PolicyBindingQuestionDefinition.PolicyBindingQuestion.PolicyBindingQuestionAnswers;
                AnswersDto = Mapper.Map<List<QuestionAnswerDto>>(answers);
            }
            return AnswersDto;
        }
    }

    internal abstract class PolicyBindingItemAnswerBuilder
    {
        protected readonly PolicyBindingAnswer m_PolicyBindingAnswer;
        protected PolicyBindingItemAnswerBuilder(PolicyBindingAnswer policyBindingAnswer)
        {
            this.m_PolicyBindingAnswer = policyBindingAnswer;
        }

        public ItemPolicyBindingQuestionAnswerDto Build()
        {
            var questionAnswer = new ItemPolicyBindingQuestionAnswerDto
            {
                Question = Mapper.Map<PolicyBindingQuestionInfoDto>(m_PolicyBindingAnswer.PolicyBindingQuestionDefinition)
            };

            InternalBuild(questionAnswer);

            return questionAnswer;
        }

        protected abstract void InternalBuild(ItemPolicyBindingQuestionAnswerDto questionAnswer);
    }
}