﻿using System.Linq;
using Common.Logging;
using Domain.Base.Repository;
using Domain.Party.Quotes;
using iPlatform.Api.DTOs.PolicyBindings.Request;
using AutoMapper;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Ratings.Request;
using MasterData;
using Domain.Individuals;
using NHibernate.Proxy;
using Domain.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Party.CorrespondencePreferences;
using iPlatform.Api.DTOs.Users;
using Domain.Users;
using Domain.Party.Quotes.Builders;
using iPlatform.Api.DTOs.Ratings.Quoting;

namespace Domain.PolicyBindings.Builder
{
    public class PolicyRequestBuilder
    {
        private readonly Quote m_Quote;
        private readonly IRepository m_Repository;
        private readonly int m_ChannelId;
        private readonly int m_UserId;
        protected static readonly ILog m_Log = LogManager.GetLogger<RatingRequestItemBuilder>();

        public PolicyRequestBuilder(IRepository repository, Quote quote, int channelId, int userId)
        {
            m_Repository = repository;
            m_Quote = quote;
            m_ChannelId = channelId;
            m_UserId = userId;
        }


        public PolicyBindingRequestInfoDto Build()
        {
            PolicyBindingRequestInfoDto requestInfo = Mapper.Map<PolicyBindingRequestInfoDto>(m_Quote);
            requestInfo.Persons = GetPersons();
            var proposal = m_Quote.QuoteHeader.Proposal;
            var proposalDefinitions = proposal.ProposalDefinitions.Where(a => a.IsDeleted != true).ToList();

            for (int i = 0; i < m_Quote.Items.Count(); i++)
            {
                PolicyBindingRequestItemDto item = Mapper.Map<PolicyBindingRequestItemDto>(m_Quote.Items[i]);

                item.RatingRequestItem = GetRatingQuestions(m_Quote, m_Quote.Items[i]);
                item.PolicyBindingQuestions = GetPolicyBindingQuestions(m_Quote, m_Quote.Items[i]);

                requestInfo.Items.Add(item);
            }

            requestInfo.PolicyBindingQuestions = GetPolicyBindingQuestions(m_Quote, null);
            requestInfo.UserInfo = GetUserInfo();

            return requestInfo;
        }

        private List<RatingRequestPersonDto> GetPersons()
        {
            var proposal = m_Quote.QuoteHeader.Proposal;
            var ratingRequestDtoBuilder = new RatingRequestDtoBuilder(m_Repository);
            var persons = ratingRequestDtoBuilder.CreatePersons(proposal.Party).ToList();
            var members = ratingRequestDtoBuilder.RatingRequestPersonDtos(proposal.Party, proposal.ProposalDefinitions);
            persons.AddRange(members);
            return persons;
        }
        private List<RatingRequestAddressDto> GetRatingAddresses(Party.Party party)
        {
            var addresses = party.Addresses.Where(a => a.IsDeleted != true).ToList();

            return addresses.Select(address => new RatingRequestAddressDto
            {
                Address1 = address.Line1,
                Address2 = address.Line2,
                Address3 = address.Line3,
                Address4 = address.Line4,
                Complex = address.Complex,
                Province = address.StateProvince != null ? address.StateProvince.Name : "",
                City = address.Line3,
                Suburb = address.Line4,
                AddressType = address.AddressType,
                DefaultAddress = address.IsDefault,
                PostalCode = address.Code,
                ExternalAddressID = address.Id.ToString(),
                Country = address.StateProvince != null ? address.StateProvince.Country.Name : "",
            }).ToList();
        }
        private RatingRequestItemDto GetRatingQuestions(Quote entity, QuoteItem item)
        {
            RatingRequestItemBuilder ratingRequestItemBuilder = new RatingRequestItemBuilder(m_Repository, entity, item);

            RatingRequestItemDto ratingRequestItem = ratingRequestItemBuilder.Build();

            UpdateRiskAddress(entity, ratingRequestItem);

            return ratingRequestItem;
        }
        private void UpdateRiskAddress(Quote entity, RatingRequestItemDto ratingRequestItem)
        {
            foreach(var question in ratingRequestItem.Answers)
            {
                if(question.Question.QuestionType.Equals(QuestionTypes.Address) &&
                    question.Question.Name == "Risk Address" && 
                    string.IsNullOrEmpty(question.QuestionAnswer.ToString()))
                {
                    var lead = entity.QuoteHeader.Proposal.LeadActivity.Lead;
                    if(lead != null)
                    {
                        var address = lead.Party.Addresses.FirstOrDefault();
                        if(address!= null)
                        {
                            question.QuestionAnswer = address.Id;
                        }
                    }
                }
            }
        }
        private List<ItemPolicyBindingQuestionAnswerDto> GetPolicyBindingQuestions(Quote entity, QuoteItem item)
        {
            PolicyBindingItemBuilder policyBindingItemBuilder = new PolicyBindingItemBuilder(m_Repository, entity, item, m_ChannelId);

            List<ItemPolicyBindingQuestionAnswerDto> questions = policyBindingItemBuilder.Build();

            return questions;
        }
        private UserInfoDto GetUserInfo()
        {
            var userInfo = new UserInfoDto { UserId = m_UserId };
            var user = m_Repository.GetById<User>(m_UserId);
            if(user != null)
            {
                userInfo.UserName = user.UserName;
                userInfo.IsAccountExecutive = user.IsAccountExecutive;
                userInfo.IsBroker = user.IsBroker;
                userInfo.IsBillable = user.IsBillable;
                userInfo.ExternalReference = user.ExternalReference;
            }
            var userIndividual = user.UserIndividuals.FirstOrDefault();
            if(userIndividual != null)
            {
                userInfo.IndividualId = userIndividual.Individual.Id;
                userInfo.LastName = userIndividual.Individual.Surname;
                userInfo.FirstName = userIndividual.Individual.FirstName;
            }

            return userInfo;
        }

    }
}
