﻿using System.Linq;
using Domain.Party.ProposalDefinitions;
using Domain.Party.Quotes;
using iPlatform.Api.DTOs.Ratings.Request;
using Domain.Base.Repository;
using MasterData;
using System.Collections.Generic;
using Domain.Party.Assets;
using Domain.QuestionDefinitions;
using Domain.Party.Quotes.Builders;
using Common.Logging;
using Domain.Individuals;
using Domain.Party.Contacts;
using Domain.Components.ComponentDefinitions;
using Domain.Components.ComponentQuestionAnswers;
using Domain.Components.ComponentQuestionDefinitions;
using iPlatform.Api.DTOs.PolicyBindings.Request;
using System;

namespace Domain.PolicyBindings.Builder
{
    public class PolicyBindingItemBuilder
    {
        private readonly Quote m_Quote;
        private readonly QuoteItem m_Item;
        private readonly IRepository m_Repository;
        private readonly int m_ChannelId;
        protected static readonly ILog m_Log = LogManager.GetLogger<PolicyBindingItemBuilder>();
        public PolicyBindingItemBuilder(IRepository repository, Quote quote, QuoteItem item, int channelId)
        {
            m_Repository = repository;
            m_Quote = quote;
            m_Item = item;
            m_ChannelId = channelId;
        }


        public List<ItemPolicyBindingQuestionAnswerDto> Build()
        {
            List<ItemPolicyBindingQuestionAnswerDto> items = new List<ItemPolicyBindingQuestionAnswerDto>();

            //if null then get general section
            int itemid = m_Item == null ? m_Quote.Id : m_Item.Id;

            var policyBindingAnswers = m_Repository.GetAll<PolicyBindingAnswer>()
                .Where(x => x.QuoteId == m_Quote.Id && x.QuoteItemId == itemid && x.PolicyBindingQuestionDefinition.PolicyBindingQuestion.QuestionType != QuestionTypes.Label).ToList();

            foreach (var policyBindingAnswer in policyBindingAnswers)
            {
                //cater for empty answer in boolean
                if (policyBindingAnswer.GetQuestionType() == QuestionTypes.Checkbox)
                {
                    policyBindingAnswer.Answer = policyBindingAnswer.Answer.ConvertToBoolString();
                }

                

                m_Log.InfoFormat("Adding question '{0}' with answer '{1}' to Policy binding request",
                    policyBindingAnswer.GetQuestionName(), policyBindingAnswer.Answer);

                PolicyBindingItemBuilderFactory builder = new PolicyBindingItemBuilderFactory();

                var requestItemAnswer = builder.Create(policyBindingAnswer);

                //debit date
                if (!policyBindingAnswer.PolicyBindingQuestionDefinition.IsProposalQuestion &&
                    policyBindingAnswer.PolicyBindingQuestionDefinition.PolicyBindingQuestion.QuestionType.Id == QuestionTypes.Dropdown.Id &&
                    policyBindingAnswer.PolicyBindingQuestionDefinition.PolicyBindingQuestion.Name == "Debt Order Date")
                {

                    int dayofmonth = GetDayofMonth(requestItemAnswer.Answer.ToString());

                    var startdateQuestion = policyBindingAnswers.FirstOrDefault(x => !x.IsProposalQuestion && x.PolicyBindingQuestionDefinition.Name == "Start Date");
                    DateTime startDate = DateTime.Now;
                    DateTime.TryParse(startdateQuestion.Answer, out startDate);


                    var debitDate = new DateTime(DateTime.Now.Year, startDate.Month, dayofmonth);
                    requestItemAnswer.Answer = debitDate.ToString("u");
                }

                items.Add(requestItemAnswer);
            }
            return items;
        }
        private int GetDayofMonth(string answer)
        {
            int dayofmonth = 1;

            string strDayofMonth = answer.Substring(0, 2);

            if (int.TryParse(strDayofMonth, out dayofmonth))
            {
                return dayofmonth;
            }

            strDayofMonth = answer.Substring(0, 1);
            if (int.TryParse(strDayofMonth, out dayofmonth))
            {
                return dayofmonth;
            }

            return dayofmonth;
        }
    }


    
}
