﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.PolicyBindings
{
    public class PolicyBindingQuestionAnswer : Entity
    {
        public PolicyBindingQuestionAnswer()
        {

        }

        public virtual string Name { get; set; }
        public virtual string Answer { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual string Reference { get; set; }

        public static PolicyBindingQuestionAnswer Create(PolicyBindingQuestionAnswerDto dto)
        {
            PolicyBindingQuestionAnswer entity = Mapper.Map<PolicyBindingQuestionAnswer>(dto);
            return entity;
        }
        public virtual PolicyBindingQuestionAnswer Update(PolicyBindingQuestionAnswerDto dto)
        {
            Mapper.Map(dto, this);
            return this;
        }
    }
}
