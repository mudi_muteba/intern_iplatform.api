﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.PolicyBindings;
using MasterData;

namespace Domain.PolicyBindings
{
    public class PolicyBindingQuestionValidition : Entity
    {
        public PolicyBindingQuestionValidition()
        {

        }

        public virtual string Message { get; set; }
        public virtual string Value1 { get; set; }
        public virtual string Value2 { get; set; }
        public virtual QuestionAlert QuestionAlert { get; set; }
        public virtual QuestionComparison QuestionComparison { get; set; }
        public virtual string Reference { get; set; }
        public static PolicyBindingQuestionValidition Create(PolicyBindingQuestionValiditionDto dto)
        {
            PolicyBindingQuestionValidition entity = Mapper.Map<PolicyBindingQuestionValidition>(dto);
            return entity;
        }
        public virtual PolicyBindingQuestionValidition Update(PolicyBindingQuestionValiditionDto dto)
        {
            Mapper.Map(dto, this);
            return this;
        }
    }
}