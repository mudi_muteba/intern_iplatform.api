﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.PolicyBindings.Queries
{
    public class SearchPolicyBindingsQuery : BaseQuery<PolicyBinding>, ISearchQuery<SearchPolicyBindingDto>
    {
        private SearchPolicyBindingDto m_Criteria;

        public SearchPolicyBindingsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<PolicyBinding>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    PolicyBindingAuthorisation.List
                };
            }
        }

        public void WithCriteria(SearchPolicyBindingDto criteria)
        {
            m_Criteria = criteria;
        }

        protected internal override IQueryable<PolicyBinding> Execute(IQueryable<PolicyBinding> query)
        {
            if(m_Criteria.ChannelId > 0)
            {
                query = query.Where(x => x.Channel.Id == m_Criteria.ChannelId);
            }

            if (m_Criteria.CoverDefinitionId > 0)
            {
                query = query.Where(x => x.CoverDefinition.Id == m_Criteria.CoverDefinitionId);
            }

            if (m_Criteria.Reference > 0)
            {
                query = query.Where(x => x.Reference == m_Criteria.Reference.ToString());
            }

            if (!string.IsNullOrEmpty(m_Criteria.Name))
            {
                query = query.Where(x => x.Name.ToUpper() == m_Criteria.Name.ToUpper());
            }

            return query;
        }
    }
}