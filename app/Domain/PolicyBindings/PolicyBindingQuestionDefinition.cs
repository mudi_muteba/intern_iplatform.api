﻿using System.Linq;
using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.PolicyBindings;
using System.Collections.Generic;
using Domain.QuestionDefinitions;

namespace Domain.PolicyBindings
{
    public class PolicyBindingQuestionDefinition : Entity
    {
        public PolicyBindingQuestionDefinition()
        {
        }

        public virtual string Name { get; set; }
        public virtual PolicyBindingQuestionGroup PolicyBindingQuestionGroup { get; set; }
        public virtual PolicyBindingQuestion PolicyBindingQuestion { get; set; }
        public virtual int VisibleIndex { get; set; }
        public virtual bool IsProposalQuestion { get; set; }
        public virtual QuestionDefinition QuestionDefinition { get; set; }
        public virtual PolicyBindingConditionalRule PolicyBindingConditionalRule { get; set; }

        public static PolicyBindingQuestionDefinition Create(PolicyBindingQuestionDefinitionDto dto, List<PolicyBindingQuestion> questions, List<PolicyBindingQuestionGroup> groups, List<PolicyBindingConditionalRule> conditionalRules)
        {
            PolicyBindingQuestionDefinition entity = Mapper.Map<PolicyBindingQuestionDefinition>(dto);

            //verifying if group exist to create or update
            PolicyBindingQuestionGroup group = groups.FirstOrDefault(x => x.Reference == dto.PolicyBindingQuestionGroup.Reference);
            if (group == null)
            {
                entity.PolicyBindingQuestionGroup = PolicyBindingQuestionGroup.Create(dto.PolicyBindingQuestionGroup);
                groups.Add(entity.PolicyBindingQuestionGroup);
            }
            else
            {
                entity.PolicyBindingQuestionGroup = group.Update(dto.PolicyBindingQuestionGroup);
            }


            //verifying if question exist create ou update
            PolicyBindingQuestion question = questions.FirstOrDefault(x => x.Reference == dto.PolicyBindingQuestion.Reference);
            if (question == null)
            {
                entity.PolicyBindingQuestion = PolicyBindingQuestion.Create(dto.PolicyBindingQuestion);
            }
            else
            {
                entity.PolicyBindingQuestion = question.Update(dto.PolicyBindingQuestion);
            }

            //verifying if conditional rule exist create ou update
            if (dto.PolicyBindingConditionalRule != null)
            {
                PolicyBindingConditionalRule conditionalRule =
                    conditionalRules.FirstOrDefault(x => x.Reference == dto.PolicyBindingConditionalRule.Reference);
                if (conditionalRule == null)
                {
                    entity.PolicyBindingConditionalRule =
                        PolicyBindingConditionalRule.Create(dto.PolicyBindingConditionalRule);
                }
                else
                {
                    entity.PolicyBindingConditionalRule = conditionalRule.Update(dto.PolicyBindingConditionalRule);
                }
            }
            return entity;
        }

        public virtual PolicyBindingQuestionDefinition Update(PolicyBindingQuestionDefinitionDto dto, List<PolicyBindingQuestion> questions, List<PolicyBindingQuestionGroup> groups, List<PolicyBindingConditionalRule> conditionalRules)
        {
            Mapper.Map(dto, this);

            PolicyBindingQuestionGroup group = groups.FirstOrDefault(x => x.Reference == dto.PolicyBindingQuestionGroup.Reference);
            if (group == null)
            {
                PolicyBindingQuestionGroup = PolicyBindingQuestionGroup.Create(dto.PolicyBindingQuestionGroup);
            }
            else
            {
                PolicyBindingQuestionGroup = group.Update(dto.PolicyBindingQuestionGroup);
            }
            //verifying if question exist create ou update
            PolicyBindingQuestion question = questions.FirstOrDefault(x => x.Reference == dto.PolicyBindingQuestion.Reference);
            if (question == null)
            {
                PolicyBindingQuestion = PolicyBindingQuestion.Create(dto.PolicyBindingQuestion);
            }
            else
            {
                PolicyBindingQuestion = question.Update(dto.PolicyBindingQuestion);
            }

            //verifying if conditional rule exist create ou update
            if (dto.PolicyBindingConditionalRule != null)
            {
                PolicyBindingConditionalRule conditionalRule =
                    conditionalRules.FirstOrDefault(x => x.Reference == dto.PolicyBindingConditionalRule.Reference);
                if (conditionalRule == null)
                {
                    PolicyBindingConditionalRule = PolicyBindingConditionalRule.Create(dto.PolicyBindingConditionalRule);
                }
                else
                {
                    PolicyBindingConditionalRule = conditionalRule.Update(dto.PolicyBindingConditionalRule);
                }
            }

            return this;
        }
    }
}
