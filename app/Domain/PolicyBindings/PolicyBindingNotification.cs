﻿using ToolBox.Communication.Interfaces;

namespace Domain.PolicyBindings
{
    public class PolicyBindingNotification : ICommunicationMessageData
    {
        public PolicyBindingNotification() { }

        public PolicyBindingNotification(string firstname, string emailbody, string fromname)
        {
            FirstName = firstname;
            EmailBody = emailbody;
            FromName = fromname;
        }
        
        public string FirstName { get; private set; }
        public string EmailBody { get; private set; }
        public string FromName { get; set; }
    }
}
