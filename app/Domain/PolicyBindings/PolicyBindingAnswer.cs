﻿using AutoMapper;
using Domain.Base;
using Domain.Party.Quotes;
using Domain.PolicyBindings.Events;
using iPlatform.Api.DTOs.PolicyBindings;
using MasterData;

namespace Domain.PolicyBindings
{
    public class PolicyBindingAnswer : EntityWithAudit
    {
        public PolicyBindingAnswer()
        {

        }

        public virtual int QuoteId { get; set; }
        public virtual int QuoteItemId { get; set; }
        public virtual PolicyBindingQuestionDefinition PolicyBindingQuestionDefinition { get; set; }
        public virtual string Answer { get; set; }
        public virtual bool IsProposalQuestion { get; set; }
        public virtual QuestionType GetQuestionType()
        {
            QuestionType questionType = PolicyBindingQuestionDefinition.PolicyBindingQuestion.QuestionType;
            if (IsProposalQuestion)
            {
                questionType = PolicyBindingQuestionDefinition.QuestionDefinition.Question.QuestionType;
            }
            return questionType;
        }

        public virtual string GetQuestionName()
        {
            string displayName = PolicyBindingQuestionDefinition.PolicyBindingQuestion.Name;
            if (IsProposalQuestion)
            {
                displayName = PolicyBindingQuestionDefinition.QuestionDefinition.DisplayName;
            }
            return displayName;
        }



        public static PolicyBindingAnswer Create(PolicyBindingAnswerDto dto)
        {
            PolicyBindingAnswer entity = Mapper.Map<PolicyBindingAnswer>(dto);

            if(dto.IsProposalQuestion)
            {
                entity.RaiseEvent(new SaveProposalAnswerFromPolicyBindingEvent(dto));
            }


            return entity;
        }
        public virtual void Update(PolicyBindingAnswerDto dto)
        {
            if (dto.IsProposalQuestion)
            {
                RaiseEvent(new SaveProposalAnswerFromPolicyBindingEvent(dto));
            }

            Mapper.Map(dto, this);
        }

    }
}
