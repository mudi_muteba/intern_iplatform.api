﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.PolicyBindings;
using MasterData;

namespace Domain.PolicyBindings
{
    public class PolicyBindingConditionalRule : Entity
    {
        public PolicyBindingConditionalRule()
        {
        }

        public virtual string Note { get; set; }
        public virtual string Answer { get; set; }
        public virtual string Reference { get; set; }
        public virtual QuestionComparison QuestionFormComparison { get; set; }

        public static PolicyBindingConditionalRule Create(PolicyBindingConditionalRuleDto dto)
        {
            var entity = Mapper.Map<PolicyBindingConditionalRule>(dto);
            return entity;
        }

        public virtual PolicyBindingConditionalRule Update(PolicyBindingConditionalRuleDto dto)
        {
            Mapper.Map(dto, this);
            return this;
        }
    }
}
