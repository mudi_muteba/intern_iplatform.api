﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Runtime.Serialization.Formatters.Binary;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Emailing;
using Domain.Emailing.Factory;
using iPlatform.Api.DTOs.PolicyBindings;
using iPlatform.Api.DTOs.Reports.Base;
using ToolBox.Templating.Interfaces;

namespace Domain.PolicyBindings.EventHandlers
{
    public class PolicyBindingNotificationDtoHandler : BaseDtoHandler<PolicyBindingNotificationDto, ReportEmailResponseDto>
    {
        private readonly IRepository m_Repository;
        private readonly ITemplateEngine m_TemplateEngine;
        private readonly IProvideContext m_ContextProvider;

        public PolicyBindingNotificationDtoHandler(IProvideContext contextProvider, IRepository repository, ITemplateEngine templateEngine) : base(contextProvider)
        {
            m_Repository = repository;
            m_TemplateEngine = templateEngine;
            m_ContextProvider = contextProvider;
        }

        public override List<MasterData.Authorisation.RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<MasterData.Authorisation.RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(PolicyBindingNotificationDto dto, HandlerResult<ReportEmailResponseDto> result)
        {
            var channel = m_Repository.GetAll<Channel>().FirstOrDefault(x => x.Id == dto.ChannelId && x.IsDeleted != true);

            if (channel == null || !channel.EmailCommunicationSettings.Any())
                return;

            var emailSettings = Mapper.Map<AppSettingsEmailSetting>(channel.EmailCommunicationSettings.FirstOrDefault());

            var assignAgent = m_Repository.GetById<Domain.Party.Party>(dto.AgentId);

            if (assignAgent != null)
            {
                dto.AssignAgentName = assignAgent.DisplayName;
                dto.AssignAgentEmail = assignAgent.ContactDetail != null ? assignAgent.ContactDetail.Email : "";
            }

            var loggedAgentName = string.Empty;
            var loggedAgentEmail = string.Empty;

            if (dto.LoggedAgent != null)
            {
                loggedAgentName = dto.LoggedAgent.Name;
                loggedAgentEmail = dto.LoggedAgent.EmailAddress;
            }

            if (!string.IsNullOrEmpty(loggedAgentEmail))
            {
                const string mailRegards = "iPlatform";
                var usernamePlusName = "iplatform@iplatform.co.za";
                const string template = "PolicyBindingNotification";

                var policyBindingNotification = new PolicyBindingNotification("Team", loggedAgentName, mailRegards);
                var emailCommunicationMessage = new EmailCommunicationMessage(template, policyBindingNotification, loggedAgentEmail, "Binding Detail Report", m_TemplateEngine, emailSettings);

                if (!string.IsNullOrEmpty(dto.AssignAgentEmail))
                {
                    if (dto.AssignAgentEmail != loggedAgentEmail)
                    {
                        emailCommunicationMessage.CCAddresses = dto.AssignAgentEmail;
                    }
                }

                var fileToSend = new Attachment(new MemoryStream(dto.Bytefile), "policybindingreport.pdf");
                emailCommunicationMessage.Attachments.Add(fileToSend);

                var message = new MailMessageFactory().Create(emailCommunicationMessage, emailSettings.AddCustomFrom("test@from.com"), true, usernamePlusName);

                new SmtpClientAdapter(emailSettings).Send(message);

                result.Processed(new ReportEmailResponseDto
                {
                    Success = true,
                    Body = message.Body
                });
            }
        }
    }
}