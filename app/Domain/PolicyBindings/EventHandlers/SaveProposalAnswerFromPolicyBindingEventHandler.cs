﻿using Common.Logging;
using Domain.Base.Events;
using Workflow.Messages;
using Domain.PolicyBindings.Events;
using Domain.PolicyBindings.Messages;

namespace Domain.AuditLeadlog.Handlers
{
    public class SaveProposalAnswerFromPolicyBindingEventHandler : BaseEventHandler<SaveProposalAnswerFromPolicyBindingEvent>
    {
        private readonly IWorkflowRouter m_Router;

        public SaveProposalAnswerFromPolicyBindingEventHandler(IWorkflowRouter router)
        {
            m_Router = router;
        }

        public override void Handle(SaveProposalAnswerFromPolicyBindingEvent @event)
        {
            m_Router.Publish(
                new WorkflowRoutingMessage().AddMessage(
                    new SaveProposalAnswerFromPolicyBindingMessage(@event.Answer)));
        }
    }
}
