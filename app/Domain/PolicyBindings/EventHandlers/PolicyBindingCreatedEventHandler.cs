﻿using Common.Logging;
using Domain.Base.Events;
using Workflow.Messages;
using Domain.PolicyBindings.Events;
using Domain.PolicyBindings.Messages;

namespace Domain.AuditLeadlog.Handlers
{
    public class PolicyBindingCreatedEventHandler : BaseEventHandler<PolicyBindingCreatedEvent>
    {
        private readonly IWorkflowRouter m_Router;

        public PolicyBindingCreatedEventHandler(IWorkflowRouter router)
        {
            m_Router = router;
        }

        public override void Handle(PolicyBindingCreatedEvent @event)
        {
            m_Router.Publish(
                new WorkflowRoutingMessage().AddMessage(
                    new PolicyBindingCreatedMessage(@event)));
        }
    }
}
