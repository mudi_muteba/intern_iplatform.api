﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.PolicyBindings;
using ValidationMessages;

namespace Domain.PolicyBindings.Validations
{
    public class PolicyBindingValidator :  IValidateDto<UpdatePolicyBindingDto>
    {
        private readonly IRepository m_Repository;

        public PolicyBindingValidator(IRepository repository)
        {
            m_Repository = repository;
        }

        private void ValidatePolicyBinding (int id, ExecutionResult result)
        {
            var policybinding = GetPolicyBinding(id);

            if (policybinding == null)
            {
                result.AddValidationMessage(PolicyBindingValidationMessages.InvalidId.AddParameters(new[] { id.ToString() }));
            }
        }

        private PolicyBinding GetPolicyBinding(int id)
        {
            return m_Repository.GetById<PolicyBinding>(id);
        }
        public void Validate(UpdatePolicyBindingDto dto, ExecutionResult result)
        {
            ValidatePolicyBinding(dto.Id, result);
        }
    }
}