﻿using System;
using Domain.Base;
using Infrastructure.NHibernate.Attributes;

namespace Domain.Products
{
    public class AllocatedProduct : ChannelEntity
    {
        public virtual string ProductCode { get; protected internal set; }
        public virtual string Name { get; protected internal set; }
        public virtual DateTime? StartDate { get; protected internal set; }
        public virtual DateTime? EndDate { get; protected internal set; }
        public virtual int ChannelId { get; protected internal set; }
        public virtual int ProductProviderId { get; protected internal set; }
        public virtual string ProductProviderTradingName { get; protected internal set; }
        public virtual int ProductOwnerId { get; protected internal set; }
        public virtual string ProductOwnerTradingName { get; protected internal set; }
        public virtual bool IsSelectedExcess { get; protected internal set; }
        public virtual bool IsVoluntaryExcess { get; protected internal set; }
        public virtual bool ShowInReporting { get; set; }
        public virtual bool BenefitAcceptanceCheck { get; set; }
        public virtual bool HideSasriaLine { get; set; }
        public virtual bool HideCompareButton { get; set; }
    }
}