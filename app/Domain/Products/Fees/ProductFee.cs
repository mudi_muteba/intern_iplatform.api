using AutoMapper;
using Domain.Admin;
using Domain.Base;
using Domain.Organizations;
using iPlatform.Api.DTOs.Products;
using MasterData;

namespace Domain.Products.Fees
{
    public class ProductFee : EntityWithAudit
    {
        protected ProductFee()
        {
            AllowRefund = true;
            AllowProRata = true;
        }

        public virtual int MasterId { get; protected internal set; }

        public virtual Product Product { get; protected internal set; }
        public virtual ProductFeeType ProductFeeType { get; protected internal set; }
        public virtual PaymentPlan PaymentPlan { get; protected internal set; }
        public virtual Organization Brokerage { get; protected internal set; }

        public virtual decimal Value { get; protected internal set; }
        public virtual decimal MinValue { get; protected internal set; }
        public virtual decimal MaxValue { get; protected internal set; }

        public virtual bool IsOnceOff { get; protected internal set; }
        public virtual bool IsPercentage { get; protected internal set; }

        public virtual bool AllowRefund { get; protected internal set; }
        public virtual bool AllowProRata { get; protected internal set; }
        public virtual Channel Channel { get; protected internal set; }

        public static ProductFee Create(CreateProductFeeDto dto)
        {
            var entity = Mapper.Map<ProductFee>(dto);
            return entity;
        }

        public virtual void Update(EditProductFeeDto dto)
        {
            Mapper.Map(dto, this);
        }

        public virtual void Disable(DisableProductFeeDto dto)
        {
            Delete();
        }
    }
}