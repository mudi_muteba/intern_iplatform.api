using System;
using System.Linq;
using Common.Logging;
using Domain.Base.Repository;
using MasterData;
using System.Collections.Generic;
using System.Configuration;

namespace Domain.Products.Fees
{
    public class FeeCalculator
    {
        private readonly IRepository repository;
        private readonly PaymentPlan paymentPlan;

        private static readonly ILog log = LogManager.GetLogger<FeeCalculator>();

        public FeeCalculator(IRepository repository, PaymentPlan paymentPlan)
        {
            this.repository = repository;
            this.paymentPlan = paymentPlan;

            products = repository.GetAll<Product>().ToList();
        }

        private readonly List<Product> products;

        public decimal Calculate(string productCode, decimal premium, string source = "")
        {
            if (premium <= 0) return 0m;

            var product = products
                .FirstOrDefault(p => p.ProductCode == productCode);

            if (product == null)
            {
                log.ErrorFormat("No fees calculated - Product with code ({0}) was not found.", productCode);
                return 0m;
            }

            var fees = product
                .ProductFees
                .Where(fee => fee != null && fee.PaymentPlan != null)
                .Where(fee => fee.PaymentPlan.Id == paymentPlan.Id)
                .ToList();

            log.InfoFormat("Calculating fees from {0} fee structures for product with code ({1}).", fees.Count, productCode);

            if (!string.IsNullOrWhiteSpace(source) && source.ToLower() == "aigwidget")
            {
                var setting = new AppSettingsReader().GetValue("AIGWidgetFeeDiscount", typeof(decimal));
                var value = Convert.ToDecimal(setting);
                return fees.Sum(fee => (fee.IsPercentage ? fee.Value / 100 * premium : fee.Value)) * value;
            }

            return fees.Sum(fee => (fee.IsPercentage ? fee.Value / 100 * premium : fee.Value));
        }

        public decimal Calculate(string productCode, decimal premium, int channelId, string source = "")
        {
            if (premium <= 0) return 0m;

            var product = products
                .FirstOrDefault(p => p.ProductCode == productCode);

            if (product == null)
            {
                log.ErrorFormat("No fees calculated - Product with code ({0}) was not found.", productCode);
                return 0m;
            }

            var fees = product
                .ProductFees
                .Where(fee => fee != null && fee.PaymentPlan != null)
                .Where(fee => fee.PaymentPlan.Id == paymentPlan.Id)
                .ToList();

            fees = fees.Count(fee => fee.Channel!=null && fee.Channel.Id == channelId) > 0 ? fees.Where(fee => fee.Channel != null).Where(fee => fee.Channel.Id == channelId).ToList() : fees.Where(fee => fee.Channel == null).ToList();

            log.InfoFormat("Calculating fees from {0} fee structures for product with code ({1}).", fees.Count, productCode);

            if (!string.IsNullOrWhiteSpace(source) && source.ToLower() == "aigwidget")
            {
                var setting = new AppSettingsReader().GetValue("AIGWidgetFeeDiscount", typeof(decimal));
                var value = Convert.ToDecimal(setting);
                return fees.Sum(fee => (fee.IsPercentage ? fee.Value / 100 * premium : fee.Value)) * value;
            }

            return fees.Sum(fee => (fee.IsPercentage ? fee.Value / 100 * premium : fee.Value));
        }
    }
}