﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Products;
using MasterData.Authorisation;

namespace Domain.Products.Fees.Handlers
{
    public class EditProductFeeHandler : ExistingEntityDtoHandler<ProductFee, EditProductFeeDto, int>
    {
        public EditProductFeeHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ProductFeeAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(ProductFee entity, EditProductFeeDto dto, HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}