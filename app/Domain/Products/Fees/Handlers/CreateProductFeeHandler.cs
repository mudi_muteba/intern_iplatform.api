﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Products;
using MasterData.Authorisation;

namespace Domain.Products.Fees.Handlers
{
    public class CreateProductFeeHandler : CreationDtoHandler<ProductFee, CreateProductFeeDto, int>
    {
        private IRepository repository;
        public CreateProductFeeHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
            this.repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProductFeeAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(ProductFee entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override ProductFee HandleCreation(CreateProductFeeDto dto, HandlerResult<int> result)
        {
            var entity = ProductFee.Create(dto);
            result.Processed(entity.Id);

            return entity;
        }
    }
}