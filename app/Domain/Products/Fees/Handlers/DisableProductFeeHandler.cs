﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Products;
using MasterData.Authorisation;

namespace Domain.Products.Fees.Handlers
{
    public class DisableProductFeeHandler : ExistingEntityDtoHandler<ProductFee, DisableProductFeeDto, int>
    {
        public DisableProductFeeHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ProductFeeAuthorisation.Delete
                };
            }
        }

        protected override void HandleExistingEntityChange(ProductFee entity, DisableProductFeeDto dto, HandlerResult<int> result)
        {
            entity.Disable(dto);
            result.Processed(entity.Id);
        }
    }
}