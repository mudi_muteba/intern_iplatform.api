﻿using Domain.Base;

namespace Domain.Products
{
    public class ProductReference : EntityWithAudit
    {
        public virtual string Name { get; protected internal set; }
    }
}