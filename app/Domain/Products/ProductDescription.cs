﻿using Domain.Base;

namespace Domain.Products
{
    public class ProductDescription : Entity
    {
         public virtual Product Product { get; protected set; }
         public virtual int Index { get; protected set; }
         public virtual string Description { get; protected set; }
    }
}