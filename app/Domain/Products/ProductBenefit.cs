using Domain.Base;

namespace Domain.Products
{
    public class ProductBenefit : EntityWithAudit
    {
        public virtual CoverDefinition CoverDefinition { get; protected internal set; }

        public virtual string Name { get; protected internal set; }
        public virtual string Value { get; protected internal set; }
        public virtual bool ShowToClient { get; protected internal set; }
        public virtual int VisibleIndex { get; protected internal set; }
    }
}