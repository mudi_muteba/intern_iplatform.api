﻿using Domain.Base.Execution;
using Domain.Base.Validation;
using Domain.Products.Queries;
using System.Collections.Generic;
using System.Linq;
using ValidationMessages;
using iPlatform.Api.DTOs.Admin.ChannelEvents;

namespace Domain.Products.Validations
{
    public class ProductCodeValidator : IValidateDto<CreateChannelEventDto>, IValidateDto<EditChannelEventDto>
    {
        private readonly GetProductByCodeQuery getProductByCodeQuery;

        public ProductCodeValidator(GetProductByCodeQuery query)
        {
            getProductByCodeQuery = query;
        }

        private void ValidateProductCode (string code, ExecutionResult result)
        {
            var product = GetProductByCode(code);

            if (!product.Any())
                result.AddValidationMessage(ProductValidationMessages.InvalidProductCode.AddParameters(new[] { code }));
        }

        private List<Product> GetProductByCode(string code)
        {
            return getProductByCodeQuery.WithCode(code).ExecuteQuery().ToList();
        }

        public void Validate(EditChannelEventDto dto, ExecutionResult result)
        {
            ValidateProductCode(dto.ProductCode, result);
        }

        public void Validate(CreateChannelEventDto dto, ExecutionResult result)
        {
            ValidateProductCode(dto.ProductCode, result);
        }
    }
}