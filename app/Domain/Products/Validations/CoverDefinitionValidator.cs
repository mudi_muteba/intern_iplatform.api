﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using iPlatform.Api.DTOs.PolicyBindings;
using ValidationMessages;
using System.Linq;
namespace Domain.Products.Validations
{
    public class CoverDefinitionValidator :  
        IValidateDto<SavePolicyBindingDto>
    {
        private readonly IRepository m_Repository;

        public CoverDefinitionValidator(IRepository repository)
        {
            m_Repository = repository;
        }

        private void ValidateCoverDefinition(int productId, int coverId, ExecutionResult result)
        {
            var coverdefinition = GetCoverDefinition(productId, coverId);

            if (coverdefinition == null)
            {
                result.AddValidationMessage(PolicyBindingValidationMessages.NoCoverDefinition.AddParameters(new[] { coverId.ToString(), productId.ToString(),  }));
            }
        }

        private CoverDefinition GetCoverDefinition(int productId, int coverId)
        {
            return m_Repository.GetAll<CoverDefinition>().FirstOrDefault(x => x.Product.Id == productId && x.Cover.Id == coverId);
        }
        public void Validate(SavePolicyBindingDto dto, ExecutionResult result)
        {
            ValidateCoverDefinition(dto.ProductId, dto.CoverId, result);
        }
    }
}