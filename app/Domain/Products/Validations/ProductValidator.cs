﻿using Domain.Base.Execution;
using Domain.Base.Repository;
using Domain.Base.Validation;
using ValidationMessages.Campaigns;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Admin.SettingsiPersons;
using iPlatform.Api.DTOs.MapVapQuestionDefinitions;
using iPlatform.Api.DTOs.PolicyBindings;

namespace Domain.Products.Validations
{
    public class ProductValidator :  
        IValidateDto<EditSettingsQuoteUploadsDto>, 
        IValidateDto<CreateSettingsiRateDto>, 
        IValidateDto<EditSettingsiRateDto>,
        IValidateDto<EditMapVapQuestionDefinitionDto>,
        IValidateDto<CreateMapVapQuestionDefinitionDto>,
        IValidateDto<SavePolicyBindingDto>
    {
        private readonly IRepository _repository;

        public ProductValidator(IRepository repository)
        {
            _repository = repository;
        }

        private void ValidateProduct (int productId, ExecutionResult result)
        {
            var product = GetAllocatedProductById(productId);

            if (product == null)
            {
                result.AddValidationMessage(CampaignValidationMessages.InvalidProductId.AddParameters(new[] { productId.ToString() }));
            }
        }

        private Product GetAllocatedProductById(int id)
        {
            return _repository.GetById<Product>(id);
        }

        public void Validate(EditMapVapQuestionDefinitionDto dto, ExecutionResult result)
        {
            ValidateProduct(dto.ProductId, result);
        }
        public void Validate(CreateMapVapQuestionDefinitionDto dto, ExecutionResult result)
        {
            ValidateProduct(dto.ProductId, result);
        }

        public void Validate(EditSettingsiPersonDto dto, ExecutionResult result)
        {
            ValidateProduct(dto.ProductId, result);
        }

        public void Validate(CreateSettingsiPersonDto dto, ExecutionResult result)
        {
            ValidateProduct(dto.ProductId, result);
        }

        public void Validate(EditSettingsQuoteUploadsDto dto, ExecutionResult result)
        {
            ValidateProduct(dto.ProductId, result);
        }

        public void Validate(CreateSettingsiRateDto dto, ExecutionResult result)
        {
            ValidateProduct(dto.ProductId, result);
        }

        public void Validate(EditSettingsiRateDto dto, ExecutionResult result)
        {
            ValidateProduct(dto.ProductId, result);
        }
        public void Validate(SavePolicyBindingDto dto, ExecutionResult result)
        {
            ValidateProduct(dto.ProductId, result);
        }
    }
}