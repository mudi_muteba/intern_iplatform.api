﻿using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.CoverDefinitions;
using iPlatform.Api.DTOs.Products;

namespace Domain.Products.Mappings
{
    public class CoverDefinifionMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<List<CoverDefinition>, List<ListCoverDefinitionDto>>();
            Mapper.CreateMap<CoverDefinition, ListCoverDefinitionDto>()
                .ForMember(t => t.DisplayName, o => o.MapFrom(s => s.DisplayName))
                .ForMember(t => t.Cover, o => o.MapFrom(s => s.Cover))
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.Product, o => o.MapFrom(s => s.Product))
                .ForMember(t => t.CoverDefinitionType, o => o.MapFrom(s => s.CoverDefinitionType))
                .ForMember(t => t.VisibleIndex, o => o.MapFrom(s => s.VisibleIndex))
                ;
            Mapper.CreateMap<CoverDefinition, BasicCoverDefinitionDto>();
            Mapper.CreateMap<CoverDefinition, CoverDefinitionDto>();
            Mapper.CreateMap<CoverDefinition, CoverDefinitionViewDto>();

            Mapper.CreateMap<List<CoverDefinition>, ListResultDto<ListCoverDefinitionDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));


            Mapper.CreateMap<CoverDefinition, CoverDefinitionInfoDto>();
        }
    }
}