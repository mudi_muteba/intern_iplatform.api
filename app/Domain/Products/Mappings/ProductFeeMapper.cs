﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Admin;
using Domain.Base.Repository;
using Domain.Organizations;
using Domain.Products.Fees;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Organisations;
using iPlatform.Api.DTOs.Products;

namespace Domain.Products.Mappings
{
    public class ProductFeeMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ProductFee, ProductFeeDto>()
                .ForMember(t => t.MaxValue, o => o.MapFrom(s => new MoneyDto(s.MaxValue)))
                .ForMember(t => t.MinValue, o => o.MapFrom(s => new MoneyDto(s.MinValue)))
                .ForMember(t => t.Value, o => o.MapFrom(s => new MoneyDto(s.Value)))
                .ForMember(t => t.ProductId, o => o.MapFrom(s => s.Product.Id))
                ;

            Mapper.CreateMap<List<ProductFee>, ListResultDto<ProductFeeDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<CreateProductFeeDto, ProductFee>()
                .ForMember(t => t.MaxValue, o => o.MapFrom(s => s.MaxValue))
                .ForMember(t => t.MinValue, o => o.MapFrom(s => s.MinValue))
                .ForMember(t => t.Value, o => o.MapFrom(s => s.Value))
                .ForMember(t => t.Brokerage, o => o.MapFrom(s => s.BrokerageId != null ? new Organization() { Id = (int)s.BrokerageId } : null))
                .ForMember(t => t.Channel, opt => opt.ResolveUsing(model => model.ChannelId == 0 ? null : new Channel() { Id = model.ChannelId }))
                .ForMember(t => t.Product, opt => opt.ResolveUsing(model => new Product() { Id = model.ProductId }))
               ;

            Mapper.CreateMap<EditProductFeeDto, ProductFee>()
               .ForMember(t => t.MaxValue, o => o.MapFrom(s => s.MaxValue))
               .ForMember(t => t.MinValue, o => o.MapFrom(s => s.MinValue))
               .ForMember(t => t.Value, o => o.MapFrom(s => s.Value))
               .ForMember(t => t.Brokerage, o => o.MapFrom(s => s.BrokerageId != null ? new Organization() { Id = (int)s.BrokerageId } : null))
               .ForMember(t => t.Channel, opt => opt.ResolveUsing(model => model.ChannelId == 0 ? null : new Channel() { Id = model.ChannelId }))
               .ForMember(t => t.Product, opt => opt.ResolveUsing(model => new Product() { Id = model.ProductId }))
              ;

            Mapper.CreateMap<PagedResults<ProductFee>, PagedResultDto<ListProductFeeDto>>();

            Mapper.CreateMap<ProductFee, ListProductFeeDto>()
                .ForMember(t => t.Channel, o=>o.MapFrom(s=>s.Channel))
                .ForMember(t => t.Brokerage, o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.Brokerage != null)
                    {
                        d.Brokerage = s.Brokerage.Id == 0 ? new OrganisationDto() : Mapper.Map<OrganisationDto>(s.Brokerage);
                    }
                    else
                    {
                        d.Brokerage = Mapper.Map<OrganisationDto>(s.Brokerage);
                    }
                })
                .ForMember(t => t.Product, o => o.MapFrom(s => s.Product))
                .ForMember(t => t.MaxValue, o => o.MapFrom(s => new MoneyDto(s.MaxValue)))
                .ForMember(t => t.MinValue, o => o.MapFrom(s => new MoneyDto(s.MinValue)))
                .ForMember(t => t.Value, o => o.MapFrom(s => new MoneyDto(s.Value)))
                ;

            Mapper.CreateMap<List<ProductFee>, ListResultDto<ListProductFeeDto>>()
               .ForMember(t => t.Results, o => o.MapFrom(s => s))
               ;

        }
    }
}