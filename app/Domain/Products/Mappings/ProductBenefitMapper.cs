﻿using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;

namespace Domain.Products.Mappings
{
    public class ProductBenefitMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ProductBenefit, ProductBenefitDto>();

            Mapper.CreateMap<List<ProductBenefit>, ListResultDto<ProductBenefitDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));
        }
    }
}