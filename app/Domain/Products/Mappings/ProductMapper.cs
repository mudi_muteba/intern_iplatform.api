﻿using System.Linq;
using AutoMapper;
using Domain.Base.Repository;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Organisations;
using iPlatform.Api.DTOs.Products;
using MasterData;
using System.Collections.Generic;

namespace Domain.Products.Mappings
{
    public class ProductMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<List<AllocatedProduct>, ListResultDto<ProductInfoDto>>()
                .ForMember(t => t.Results, o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    s.ForEach(x =>
                    {

                        d.Results.Add(Mapper.Map<ProductInfoDto>(x));
                    });
                });

            Mapper.CreateMap<PagedResults<AllocatedProduct>, PagedResultDto<ListAllocatedProductDto>>();
            Mapper.CreateMap<PagedResults<Product>, PagedResultDto<ListProductDto>>();
            Mapper.CreateMap<List<AllocatedProduct>, PagedResultDto<ListAllocatedProductDto>>()
                .ForMember(t => t.Results, o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    s.ForEach(x =>
                    {

                        d.Results.Add(Mapper.Map<ListAllocatedProductDto>(x));
                    });
                })

                ;

            Mapper.CreateMap<AllocatedProduct, ProductInfoDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.Name, o => o.MapFrom(s => s.Name))
                .ForMember(t => t.ProductCode, o => o.MapFrom(s => s.ProductCode))
                .ForMember(t => t.ChannelId, o => o.MapFrom(s => s.ChannelId))
                ;


            Mapper.CreateMap<AllocatedProduct, AllocatedProductDto>();
            Mapper.CreateMap<List<AllocatedProduct>, List<AllocatedProductDto>>();

            Mapper.CreateMap<ProductBenefit, ProductBenefitDto>();
            Mapper.CreateMap<Product, AllocatedProductDto>();
            Mapper.CreateMap<Product, ProductInfoDto>();
            Mapper.CreateMap<AllocatedProductDto, Product>();
            Mapper.CreateMap<QuestionDefinition, QuestionDefinitionDto>()
                .ForMember(s => s.PossibleAnswers,
                    o => o.MapFrom(s => new QuestionAnswers().Where(qa => qa.Question.Id == s.Question.Id)));

            Mapper.CreateMap<Product, ProposalFormDto>()
                .ConvertUsing<ITypeConverter<Product, ProposalFormDto>>();

            Mapper.CreateMap<AllocatedProduct, ListAllocatedProductDto>()
                .ForMember(s => s.ProductProvider, o => o.MapFrom(s => new ListOrganizationDto()
                {
                    Id = s.ProductProviderId,
                    Name = s.ProductProviderTradingName
                }))
                .ForMember(s => s.ProductOwner, o => o.MapFrom(s => new ListOrganizationDto()
                {
                    Id = s.ProductOwnerId,
                    Name = s.ProductOwnerTradingName
                }))
                ;

            Mapper.CreateMap<Product, AllocatedProductDto>()
                .ForMember(t => t.Covers, o => o.MapFrom(s => s.CoverDefinitions))
                .ForMember(t => t.Fees, o => o.MapFrom(s => s.ProductFees));

            Mapper.CreateMap<Product, ListProductDto>()
                .ForMember(t => t.Code, o => o.MapFrom(s => s.ProductCode))
                ;
            Mapper.CreateMap<Product, ProductByCoverDto>()
                .ForMember(t => t.Code, o => o.MapFrom(s => s.ProductCode))
                ;


            Mapper.CreateMap<List<Product>, ListResultDto<ListProductDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<List<Product>, ListResultDto<ProductInfoDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<Product, AboutProductDto>()
                .ForMember(t => t.Name, o => o.MapFrom(s => s.Name))
                .ForMember(t => t.Code, o => o.MapFrom(s => s.ProductCode))
                .ForMember(t => t.ProductType, o => o.MapFrom(s => s.ProductType.Name))
                .ForMember(t => t.Provider, o => o.Ignore())
                .ForMember(t => t.Images, o => o.Ignore())
                .AfterMap((s,d) => {
                    d.Provider = new ProductProviderDto
                    {
                        Code = s.ProductProvider.Code,
                        Id = s.ProductProvider.Id,
                        RegisteredName = s.ProductProvider.RegisteredName,
                        Description = s.ProductProvider.Description,
                        TradingName = s.ProductProvider.TradingName
                    };
                    d.Images = new List<ProductImageDto>
                    {
                        new ProductImageDto
                        {
                            Url = s.ImageName
                        }
                    };
                });
        }
    }
}