﻿using System.Collections.Generic;
using AutoMapper;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;

namespace Domain.Products.Mappings
{
    public class ProductAdditionalExcessMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ProductAdditionalExcess, ProductAdditionalExcessDto>();

            Mapper.CreateMap<List<ProductAdditionalExcess>, ListResultDto<ProductAdditionalExcessDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));
        }
    }
}