using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.Products;

namespace Domain.Products.Mappings
{
    public class ProposalFormConverter : TypeConverter<Product, ProposalFormDto>
    {
        protected override ProposalFormDto ConvertCore(Product source)
        {
            if (source == null)
                return null;

            var target = new ProposalFormDto();

            target.Product = Mapper.Map<Product, AllocatedProductDto>(source);
            target.Groups = new List<ProposalFormGroupDto>();

            foreach (var coverDefinition in source.CoverDefinitions)
            {
                foreach (var questionDefinition in coverDefinition.QuestionDefinitions)
                {
                    if (questionDefinition.Question == null) continue;
                    if (questionDefinition.Question.QuestionGroup == null) continue;

                    var existingGrouping = target.Groups.FirstOrDefault(g => g.Name.Equals(questionDefinition.Question.QuestionGroup.Name));
                    if (existingGrouping == null)
                    {
                        existingGrouping = new ProposalFormGroupDto()
                        {
                            Id = questionDefinition.Question.QuestionGroup.Id,
                            Name = questionDefinition.Question.QuestionGroup.Name,
                            VisibleIndex = questionDefinition.Question.QuestionGroup.VisibleIndex,
                            QuestionGroupTranslationKey = questionDefinition.QuestionGroupTranslationKey
                        };
                        target.Groups.Add(existingGrouping);
                    }

                    existingGrouping.Questions.Add(Mapper.Map<QuestionDefinition, QuestionDefinitionDto>(questionDefinition));
                }
            }

            return target;
        }
    }
}