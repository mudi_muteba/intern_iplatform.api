﻿using Domain.QuestionDefinitions;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Products.Overrides
{
    public class QuestionDefinitionOverride : IAutoMappingOverride<QuestionDefinition>
    {
        public void Override(AutoMapping<QuestionDefinition> mapping)
        {
            mapping.References(x => x.Parent, "ParentId");

            mapping.HasMany(x => x.Children)
                .Table("QuestionDefinition")
                .Cascade.SaveUpdate()
                .KeyColumn("ParentId");
        }
    }
}