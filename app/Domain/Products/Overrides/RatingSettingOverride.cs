﻿using Domain.Ratings;
using Domain.Ratings.Configuration;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Products.Overrides
{
    public class RatingSettingOverride :IAutoMappingOverride<RatingSetting>
    {
        public void Override(AutoMapping<RatingSetting> mapping)
        {
            mapping.Table("SettingsiRate");
        }
    }
}