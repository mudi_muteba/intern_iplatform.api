using Domain.Products.Fees;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Products.Overrides
{
    public class ProductFeeOverride : IAutoMappingOverride<ProductFee>
    {
        public void Override(AutoMapping<ProductFee> mapping)
        {
            mapping.References(m => m.Brokerage, "BrokerageId");
            mapping.References(m => m.Channel);
            mapping.References(m => m.Product);
        }
    }
}