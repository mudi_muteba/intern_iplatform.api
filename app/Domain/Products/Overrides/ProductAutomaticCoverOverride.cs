using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Products.Overrides
{
    public class ProductAutomaticCoverOverride : IAutoMappingOverride<ProductAutomaticCovers>
    {
        public void Override(AutoMapping<ProductAutomaticCovers> mapping)
        {
            mapping.References(x => x.Cover).Nullable().LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.Product).LazyLoad(Laziness.NoProxy);
            mapping.References(x => x.CoverDefinition).LazyLoad(Laziness.NoProxy);

        }
    }
}