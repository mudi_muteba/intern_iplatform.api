using Domain.QuestionDefinitions;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Products.Overrides
{
    public class MapQuestionDefinitionOverride : IAutoMappingOverride<MapQuestionDefinition>
    {
        public void Override(AutoMapping<MapQuestionDefinition> mapping)
        {
      
        }
    }
}