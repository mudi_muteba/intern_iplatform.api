﻿using Domain.Base.Repository.Customisation;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Products.Overrides
{
    public class ProductAdditionalExcessOverride : IAutoMappingOverride<ProductAdditionalExcess>
    {
        public void Override(AutoMapping<ProductAdditionalExcess> mapping)
        {
            mapping.Map(x => x.Category).Length(4000);
            mapping.Map(x => x.Description).Length(4000);
        }
    }
}