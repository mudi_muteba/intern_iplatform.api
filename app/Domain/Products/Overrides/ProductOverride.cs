﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Products.Overrides
{
    public class ProductOverride : IAutoMappingOverride<Product>
    {
        public void Override(AutoMapping<Product> mapping)
        {
            mapping.References(x => x.ProductProvider, "ProductProviderId")
                .LazyLoad(Laziness.NoProxy)
                .Fetch.Join();

            mapping.References(x => x.ProductOwner, "ProductOwnerId")
                .LazyLoad(Laziness.NoProxy)
                .Fetch.Join();
        }
    }
}