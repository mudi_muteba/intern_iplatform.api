using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using FluentNHibernate.Mapping;

namespace Domain.Products.Overrides
{
    public class CoverDefinitionOverride : IAutoMappingOverride<CoverDefinition>
    {
        public void Override(AutoMapping<CoverDefinition> mapping)
        {
            mapping.References(x => x.Parent, "ParentId");
            mapping.References(x => x.Cover).LazyLoad(Laziness.NoProxy);
            mapping.HasMany(x => x.Children)
                .Cascade.SaveUpdate()
                .KeyColumn("ParentId");
            mapping.Map(x => x.CoverLevelWording).CustomType("StringClob").CustomSqlType("ntext");
        }
    }
}