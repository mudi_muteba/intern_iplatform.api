﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Products.Overrides
{
    public class AllocatedProductOverride : IAutoMappingOverride<AllocatedProduct>
    {
        public void Override(AutoMapping<AllocatedProduct> mapping)
        {
            mapping.SchemaAction.None();
            mapping.ReadOnly();
            mapping.Table("vw_product_allocation");
        }
    }
}