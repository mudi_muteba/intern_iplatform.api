using Domain.Products.Fees;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Products.Overrides
{
    public class ProductBenefitOverride : IAutoMappingOverride<ProductBenefit>
    {
        public void Override(AutoMapping<ProductBenefit> mapping)
        {
            //mapping.IgnoreProperty(m => m.CoverDefinition);
        }
    }
}