﻿using System;
using System.Collections.Generic;
using Domain.Base;
using Domain.Organizations;
using Domain.Products.Fees;
using MasterData;
using Domain.Tags;
using Infrastructure.NHibernate.Attributes;

namespace Domain.Products
{
    public class Product : EntityWithAudit
    {
        public Product(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public Product()
        {
            CoverDefinitions = new List<CoverDefinition>();
            ProductFees = new List<ProductFee>();
            Tags = new List<TagProductMap>();
        }

        public virtual int MasterId { get; set; }
        public virtual string Name { get; set; }
        public virtual Organization ProductOwner { get; set; }
        public virtual Organization ProductProvider { get; set; }
        public virtual ProductType ProductType { get; set; }
        public virtual IList<CoverDefinition> CoverDefinitions { get; protected set; }
        public virtual IList<TagProductMap> Tags { get; protected set; }
        public virtual IList<ProductFee> ProductFees { get; protected set; }
        public virtual string AgencyNumber { get; set; }
        public virtual string ProductCode { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual string ImageName { get; set; }
        public virtual int QuoteExpiration { get; set; }
        public virtual bool IsSelectedExcess { get; set; }
        public virtual bool IsVoluntaryExcess { get; set; }
        public virtual bool ShowInReporting { get; set; }
        public virtual bool BenefitAcceptanceCheck { get; set; }
        public virtual bool HideSasriaLine { get; set; }
        public virtual bool HideCompareButton { get; set; }

        public static int MultiQuoteProductId
        {
            get { return 27; }
        }
        
    }
}