using System.Collections.Generic;
using AutoMapper;
using Domain.Admin.ChannelEvents;
using Domain.Base;
using Domain.QuestionDefinitions;
using iPlatform.Api.DTOs.Admin.CoverDefinition;
using MasterData;

namespace Domain.Products
{
    public class CoverDefinition : EntityWithAudit
    {
        public CoverDefinition()
        {
            QuestionDefinitions = new List<QuestionDefinition>();
            Children = new List<CoverDefinition>();
            Benefits = new List<ProductBenefit>();
        }

        public virtual IList<ProductBenefit> Benefits { get; protected internal set; }

        public virtual Product Product { get; protected internal set; } 

        public virtual string DisplayName { get; protected internal set; }

        public virtual string CoverLevelWording { get; protected internal set; }

        public virtual Cover Cover { get; protected internal set; }

        public virtual CoverDefinition Parent { get; protected internal set; }
        public virtual IList<CoverDefinition> Children { get; protected internal set; }

        public virtual IList<QuestionDefinition> QuestionDefinitions { get; protected internal set; }
        public virtual CoverDefinitionType CoverDefinitionType { get; protected internal set; }

        public virtual int VisibleIndex { get; protected internal set; }
        public virtual int MasterId { get; protected internal set; }

        public virtual void AddChildCover(CoverDefinition coverDefinition)
        {
            coverDefinition.Parent = this;
            coverDefinition.Product = Product;

            Children.Add(coverDefinition);
        }

        public virtual void AddQuestionDefinition(QuestionDefinition definition)
        {
            definition.CoverDefinition = this;
            QuestionDefinitions.Add(definition);
        }

        public virtual void AddBenefit(ProductBenefit benefit)
        {
            benefit.CoverDefinition = this;
            Benefits.Add(benefit);
        }

        public virtual CoverDefinition CreateSubCover(CoverDefinition subCover)
        {
            subCover.Parent = this;
            subCover.Product = Product;
            subCover.CoverDefinitionType = CoverDefinitionTypes.SubCover;

            return subCover;
        }

        public virtual CoverDefinition CreateSubCover()
        {
            return CreateSubCover(new CoverDefinition());
        }

        public virtual CoverDefinition CreateExtension(CoverDefinition extension)
        {
            extension.Parent = this;
            extension.Product = Product;
            extension.CoverDefinitionType = CoverDefinitionTypes.Extension;

            return extension;
        }

        public virtual CoverDefinition CreateExtension()
        {
            return CreateExtension(new CoverDefinition());
        }

        public static CoverDefinition Create(CreateCoverDefinitionDto createDto)
        {
            var entity = Mapper.Map<CoverDefinition>(createDto);
            return entity;
        }

        public virtual void Update(EditCoverDefinitionDto editDto)
        {
            Mapper.Map(editDto, this);
        }
    }
}