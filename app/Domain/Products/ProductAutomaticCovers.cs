
using Domain.Base;
using MasterData;

namespace Domain.Products
{
    public class ProductAutomaticCovers : Entity
    {
        public virtual string Description { get; protected internal set; }

        public virtual Product Product { get; protected internal set; }
        public virtual Cover Cover { get; protected internal set; }
        public virtual CoverDefinition CoverDefinition { get; protected internal set; }

        public virtual bool IsCompulsary { get; protected internal set; }
    }
}