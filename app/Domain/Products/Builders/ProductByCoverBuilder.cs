﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Products;

namespace Domain.Products.Builders
{
    public class ProductByCoverBuilder
    {
        private readonly int _channelId;
        private readonly IRepository _repository;

        public ProductByCoverBuilder(IRepository repository, int channelId)
        {
            _repository = repository;
            _channelId = channelId;
        }

        public List<ListProductByCoverDto> Build(IQueryable<AllocatedProduct> products)
        {
            IQueryable<CoverDefinition> coverDefinitions = _repository.GetAll<CoverDefinition>();

            List<CoverDefinition> query = (from p in products
                join c in coverDefinitions on p.Id equals (c.Product ?? new Product()).Id
                where p.ChannelId == _channelId
                select c).ToList();


            List<ListProductByCoverDto> list = query.GroupBy(a => a.Cover).Select(a => new ListProductByCoverDto
            {
                Products = a.Select(x => MapProductCover(x.Product, x.Id)).ToList(),
                Cover = a.Key
            }).ToList();


            return list;
        }

        private ProductByCoverDto MapProductCover(Product product, int id)
        {
            var result = Mapper.Map<ProductByCoverDto>(product);
            result.CoverDefinitionId = id;
            return result;
        }
    }
}