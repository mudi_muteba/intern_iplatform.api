﻿using Domain.Base;

namespace Domain.Products
{
    public class ProductAdditionalExcess : Entity
    {
        public virtual CoverDefinition CoverDefinition { get; protected internal set; }

        public virtual int Index { get; protected set; }

        public virtual string Category { get; protected set; }
        public virtual string Description { get; protected set; }


        public virtual decimal ActualExcess { get; protected set; }
        public virtual decimal MinExcess { get; protected set; }
        public virtual decimal MaxExcess { get; protected set; }


        public virtual decimal Percentage { get; protected set; }
        public virtual bool IsPercentageOfClaim { get; protected set; }
        public virtual bool IsPercentageOfItem { get; protected set; }
        public virtual bool IsPercentageOfSumInsured { get; protected set; }


        public virtual bool ShouldApplySelectedExcess { get; protected set; }
        public virtual bool ShouldDisplayExcessValues { get; protected set; }
    }
}