﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Products.Queries
{
    public class GetBenefitsByCoverIdQuery : BaseQuery<ProductBenefit>
    {
        private int productId;
        private int coverId;
        public GetBenefitsByCoverIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProductBenefit>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetBenefitsByCoverIdQuery WithCriterias(int productid, int coverid)
        {
            this.productId = productid;
            this.coverId = coverid;
            return this;
        }


        protected internal override IQueryable<ProductBenefit> Execute(IQueryable<ProductBenefit> query)
        {
            query = query.Where(x => x.CoverDefinition.Id == coverId && x.CoverDefinition.Product.Id == productId);
            return query;
        }
    }
}