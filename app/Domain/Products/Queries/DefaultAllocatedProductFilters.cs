using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Shared;

namespace Domain.Products.Queries
{
    public class DefaultAllocatedProductFilters : IApplyDefaultFilters<AllocatedProduct>
    {
        public IQueryable<AllocatedProduct> Apply(ExecutionContext executionContext, IQueryable<AllocatedProduct> query)
        {
            var date = SystemTime.Now();

            return query
                .Where(p => p.StartDate <= date || p.StartDate == null)
                .Where(p => p.EndDate >= date || p.EndDate == null);
        }
    }
}