﻿using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Products.Queries
{
    public class GetAllocatedProductByIdQuery : BaseQuery<Product>
    {
        private int _id;

        public GetAllocatedProductByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultProductFilters()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetAllocatedProductByIdQuery WithId(int id)
        {
            _id = id;
            return this;
        }

        protected internal override IQueryable<Product> Execute(IQueryable<Product> query)
        {
            var context = ContextProvider.Get();

            return (
                from ap in Repository.GetAll<AllocatedProduct>()
                join p in query on ap.Id equals p.Id
                join c in Repository.GetAll<Channel>() on ap.ChannelId equals c.Id
                where context.Channels.Contains(c.Id)
                where p.Id == _id
                select p
                );
        }
    }
}