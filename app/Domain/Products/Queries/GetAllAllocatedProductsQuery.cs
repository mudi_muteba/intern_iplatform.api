﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using Shared;
using Domain.Admin;
using Domain.Admin.SettingsiRates;

namespace Domain.Products.Queries
{
    public class GetAllAllocatedProductsQuery : BaseQuery<AllocatedProduct>
    {
        public GetAllAllocatedProductsQuery(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository, new NoDefaultFilters<AllocatedProduct>())
        {
            _repository = repository;
        }
        private readonly IRepository _repository;
        public int channelid;
        public override List<RequiredAuthorisationPoint> RequiredRights {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetAllAllocatedProductsQuery WithChannelId(int channelid)
        {
            this.channelid = channelid;
            return this;
        }

        protected internal override IQueryable<AllocatedProduct> Execute(IQueryable<AllocatedProduct> query)
        {
            var date = SystemTime.Now();
            var mulproducts = _repository.GetAll<Product>().Where(x => x.IsDeleted != true
                            && (x.StartDate <= date || x.StartDate == null)
                            && (x.EndDate >= date || x.EndDate == null)
                            );

            var multiquote = (from channels in _repository.GetAll<Channel>().Where(x => x.IsDeleted != true)
                             from products in mulproducts
                             select new AllocatedProduct
                             {
                                 Id = products.Id,
                                 ProductCode = products.ProductCode,
                                 Name = products.Name,
                                 StartDate = products.StartDate,
                                 EndDate = products.StartDate,
                                 ProductOwnerId = products.ProductOwner.Id,
                                 ProductOwnerTradingName = products.ProductOwner.TradingName,
                                 ProductProviderId = products.ProductProvider.Id,
                                 ProductProviderTradingName = products.ProductProvider.TradingName,
                                 ChannelId = channels.Id,
                                 ShowInReporting = products.ShowInReporting
                             }).ToList();

            var result = _repository.GetAll<SettingsiRate>()
                        .Where(x => x.IsDeleted != true
                        && x.Channel != null
                        && x.Product.IsDeleted != true
                        && (x.Product.StartDate <= date || x.Product.StartDate == null)
                        && (x.Product.EndDate >= date || x.Product.EndDate == null)
                        && x.Channel.IsDeleted != true)
                        .Select(x =>
                        new AllocatedProduct
                        {
                            Id = x.Product.Id,
                            ProductCode = x.Product.ProductCode,
                            Name = x.Product.Name,
                            StartDate = x.Product.StartDate,
                            EndDate = x.Product.StartDate,
                            ProductOwnerId = x.Product.ProductOwner.Id,
                            ProductOwnerTradingName = x.Product.ProductOwner.TradingName,
                            ProductProviderId = x.Product.ProductProvider.Id,
                            ProductProviderTradingName = x.Product.ProductProvider.TradingName,
                            ChannelId = x.Channel.Id,
                            ShowInReporting = x.Product.ShowInReporting
                        }).ToList();

            multiquote.ForEach(x => result.Add(x));

            var newquery = result.AsQueryable();

            if (channelid > 0)
                newquery = newquery.Where(x => x.ChannelId == channelid);

            return newquery;
        }
    }
}