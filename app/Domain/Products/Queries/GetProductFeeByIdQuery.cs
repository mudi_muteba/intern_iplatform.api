﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Products.Fees;
using MasterData.Authorisation;

namespace Domain.Products.Queries
{
    public class GetProductFeeByIdQuery : BaseQuery<ProductFee>
    {
        private int id;
        public GetProductFeeByIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProductFee>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ProductFeeAuthorisation.List
                };
            }
        }
        public GetProductFeeByIdQuery WithCriterias(int productFeeId)
        {
            this.id = productFeeId;
            return this;
        }


        protected internal override IQueryable<ProductFee> Execute(IQueryable<ProductFee> query)
        {
            if (id > 0)
                query = query.Where(x => x.Id == id);

            return query;
        }
    }
}