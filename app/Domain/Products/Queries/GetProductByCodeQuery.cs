﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Products.Queries
{
    public class GetProductByCodeQuery : BaseQuery<Product>, IContextFreeQuery
    {
        public GetProductByCodeQuery(IProvideContext contextProvider, IRepository repository) : 
            base(contextProvider, repository, new NoDefaultFilters<Product>())
        {
        }

        public string Code { get; set; }
        public override List<RequiredAuthorisationPoint> RequiredRights {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public GetProductByCodeQuery WithCode(string code)
        {
            Code = code;
            return this;
        }

        protected internal override IQueryable<Product> Execute(IQueryable<Product> query)
        {
            if (!string.IsNullOrEmpty(Code))
                query = query.Where(x => x.ProductCode == Code);

            return query;
        }
    }
}