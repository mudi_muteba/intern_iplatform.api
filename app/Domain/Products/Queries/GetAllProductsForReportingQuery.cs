﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Products.Queries
{
    public class GetAllProductsForReportingQuery : BaseQuery<Product>, IChannelFreeQuery
    {
        public GetAllProductsForReportingQuery(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository, new NoDefaultFilters<Product>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IQueryable<Product> Execute(IQueryable<Product> query)
        {
            return query.Where(x => x.ShowInReporting);
        }
    }
}