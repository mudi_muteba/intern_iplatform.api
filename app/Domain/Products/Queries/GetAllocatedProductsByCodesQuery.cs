using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Products.Queries
{
    public class GetAllocatedProductsByCodesQuery : BaseQuery<Product>
    {
        private readonly List<string> _codes = new List<string>();

        public GetAllocatedProductsByCodesQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultProductFilters()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetAllocatedProductsByCodesQuery WithCodes(IEnumerable<string> codes)
        {
            foreach (var code in codes)
                WithCode(code);

            return this;
        }

        public GetAllocatedProductsByCodesQuery WithCode(string code)
        {
            if(_codes.Any(c => string.Equals(c, code, StringComparison.InvariantCultureIgnoreCase)))
                return this;

            _codes.Add(code);

            return this;
        }

        protected internal override IQueryable<Product> Execute(IQueryable<Product> query)
        {
            var context = ContextProvider.Get();

            return (
                from ap in Repository.GetAll<AllocatedProduct>()
                join p in query on ap.Id equals p.Id
                join c in Repository.GetAll<Channel>() on ap.ChannelId equals c.Id
                where context.Channels.Contains(c.Id)
                where _codes.Contains(p.ProductCode)
                select p
                );
        }
    }
}