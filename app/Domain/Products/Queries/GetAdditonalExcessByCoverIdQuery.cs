﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Products.Queries
{
    public class GetAdditonalExcessByCoverIdQuery : BaseQuery<ProductAdditionalExcess>
    {
        private int productId;
        private int coverId;
        public GetAdditonalExcessByCoverIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProductAdditionalExcess>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetAdditonalExcessByCoverIdQuery WithCriterias(int productid, int coverid)
        {
            this.productId = productid;
            this.coverId = coverid;
            return this;
        }


        protected internal override IQueryable<ProductAdditionalExcess> Execute(IQueryable<ProductAdditionalExcess> query)
        {
            query = query.Where(x => x.CoverDefinition.Id == coverId && x.CoverDefinition.Product.Id == productId);
            return query;
        }
    }
}