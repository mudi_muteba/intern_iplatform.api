using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Shared;

namespace Domain.Products.Queries
{
    public class DefaultProductFilters : IApplyDefaultFilters<Product>
    {
        public IQueryable<Product> Apply(ExecutionContext executionContext, IQueryable<Product> query)
        {
            var date = SystemTime.Now();

            return query
                .Where(p => p.StartDate <= date || p.StartDate == null)
                .Where(p => p.EndDate >= date || p.EndDate == null);
        }
    }
}