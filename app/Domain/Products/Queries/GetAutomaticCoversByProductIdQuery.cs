﻿using System.Collections.Generic;
using System.Linq;
using Domain.Admin;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Products.Queries
{
    public class GetAutomaticCoversByProductIdQuery : BaseQuery<ProductAutomaticCovers>
    {
        private int m_ProductId;
        private int m_CoverId;

        public GetAutomaticCoversByProductIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProductAutomaticCovers>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetAutomaticCoversByProductIdQuery WithProductId(int id)
        {
            m_ProductId = id;
            return this;
        }

        public GetAutomaticCoversByProductIdQuery WithProductIdAndCoverDefinitionId(int productId, int coverId)
        {
            WithProductId(productId);
            m_CoverId = coverId;
            return this;
        }

        protected internal override IQueryable<ProductAutomaticCovers> Execute(IQueryable<ProductAutomaticCovers> query)
        {
            var result = from ac in Repository.GetAll<ProductAutomaticCovers>()
                join p in query on ac.Id equals p.Id
                where p.Product.Id == m_ProductId
                select p;

            if (m_CoverId > 0)
            {
                result = from ac2 in result
                    where ac2.Cover.Id == m_CoverId
                    select ac2;
            }
            return result;
        }
    }
}