﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Products.Fees;
using iPlatform.Api.DTOs.Products;
using MasterData.Authorisation;

namespace Domain.Products.Queries
{
    public class SearchProductFeeQuery : BaseQuery<ProductFee>, ISearchQuery<SearchProductFeeDto>
    {
        private SearchProductFeeDto criteria;

        public SearchProductFeeQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProductFee>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithCriteria(SearchProductFeeDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<ProductFee> Execute(IQueryable<ProductFee> query)
        {
            if (!string.IsNullOrEmpty(criteria.Channel))
                query = query.Where(x => x.Channel.Name.StartsWith(criteria.Channel));

            if (!string.IsNullOrEmpty(criteria.Product))
                query = query.Where(x => x.Product.Name.StartsWith(criteria.Product));

            return query.Where(x => x.Product.IsDeleted == false);
        }
    }
}