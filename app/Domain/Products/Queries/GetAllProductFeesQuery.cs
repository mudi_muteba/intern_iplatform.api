﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Products.Fees;
using MasterData.Authorisation;

namespace Domain.Products.Queries
{
    public class GetAllProductFeesQuery : BaseQuery<ProductFee>
    {
        public GetAllProductFeesQuery(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository, new NoDefaultFilters<ProductFee>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ProductFeeAuthorisation.List
                };
            }
        }

        protected internal override IQueryable<ProductFee> Execute(IQueryable<ProductFee> query)
        {
            return query;
        }
    }
}