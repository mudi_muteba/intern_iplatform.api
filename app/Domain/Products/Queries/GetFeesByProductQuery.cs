﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using Domain.Products.Fees;

namespace Domain.Products.Queries
{
    public class GetFeesByProductQuery : BaseQuery<ProductFee>
    {
        private int productId;
        private int id;
        private int _channelId;
        public GetFeesByProductQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ProductFee>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ProductAuthorisation.List
                };
            }
        }

        public GetFeesByProductQuery WithProductId(int productid)
        {
            this.productId = productid;
            return this;
        }

        public GetFeesByProductQuery WithProductIdChannelId(int productid, int channelId)
        {
            this.productId = productid;
            this._channelId = channelId;
            return this;
        }

        public GetFeesByProductQuery WithProductFeeIdProductIdChannelId(int productid, int id, int channelId)
        {
            this.productId = productid;
            this._channelId = channelId;
            this.id = id;
            return this;
        }

        public GetFeesByProductQuery WithCriterias(int productid, int id)
        {
            this.productId = productid;
            this.id = id;
            return this;
        }


        protected internal override IQueryable<ProductFee> Execute(IQueryable<ProductFee> query)
        {
            query = query.Where(x => x.Product.Id == productId);

            if (id > 0)
                query = query.Where(x => x.Id == id);

            if (_channelId > 0)
                query = query.Where(c => c.Channel.Id == _channelId);

            return query;
        }
    }
}