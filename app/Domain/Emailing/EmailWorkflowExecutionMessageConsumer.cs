﻿using AutoMapper;
using Domain.Emailing.Factory;
using Shared.Extentions;
using ToolBox.Communication.Emailing;
using ToolBox.Communication.Interfaces;
using ToolBox.Templating.Interfaces;
using Workflow.Domain;
using Workflow.Messages;

namespace Domain.Emailing
{
    public class EmailWorkflowExecutionMessageConsumer : AbstractMessageConsumer<EmailWorkflowExecutionMessage>
    {

        private readonly ITemplateEngine _templateEngine;

        public EmailWorkflowExecutionMessageConsumer(IWorkflowRouter router, IWorkflowExecutor executor, ITemplateEngine templateEngine)
            : base(router, executor)
        {
            _templateEngine = templateEngine;
        }

        public override void Consume(EmailWorkflowExecutionMessage message)
        {
            if(message.CommunicationMessage.AppSettingsEmailSetting== null)
            {
                this.Error(() => string.Format("mail not sent to {0}, missing settings in message", message.CommunicationMessage.Address));
                return;
            }

            var sendCommunication = new EmailCommunicator(_templateEngine, new SmtpClientAdapter(message.CommunicationMessage.AppSettingsEmailSetting), message.CommunicationMessage.AppSettingsEmailSetting);
            this.Info(() => string.Format("Sending mail to {0}", message.CommunicationMessage.Address));
            sendCommunication.Send(message.CommunicationMessage);
            this.Info(() => string.Format("Sent mail to {0}", message.CommunicationMessage.Address));
        }
    }
}