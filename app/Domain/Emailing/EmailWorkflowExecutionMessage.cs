﻿using Domain.Base.Workflow;
using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;
using iPlatform.Enums.Workflows;
using Workflow.Messages;

namespace Domain.Emailing
{
    [WorkflowExecutionMessageType(WorkflowMessageType.Email)]
    public class EmailWorkflowExecutionMessage : WorkflowExecutionMessage
    {
        public EmailCommunicationMessage CommunicationMessage { get; set; }
        

        public EmailWorkflowExecutionMessage() { }

        public EmailWorkflowExecutionMessage(EmailCommunicationMessage communicationMessage, WorkflowExecutionMessageType messageType = WorkflowExecutionMessageType.Default) : base(messageType) 
        {
            CommunicationMessage = communicationMessage;
        }
    }
}