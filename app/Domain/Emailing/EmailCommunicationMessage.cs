﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using ToolBox.Communication.Interfaces;
using Workflow.Messages.Mail;
using System.Collections.Generic;
using System.Net.Mail;
using ToolBox.Templating.Interfaces;
using iPlatform.Api.DTOs.Admin.EmailCommunicationSettings;
using Domain.Emailing.Factory;

namespace Domain.Emailing
{
    [DataContract]
    public class EmailCommunicationMessage : CommunicationMessage<ICommunicationMessageData>
    {
        [DataMember]
        public override string Address { get; set; }
        [DataMember]
        public override string CCAddresses { get; set; }
        [DataMember]
        public override string BCCAddresses { get; set; }
        [DataMember]
        public override string Subject { get; set; }
        [DataMember]
        public override string Body { get; set; }
        [DataMember]
        public override string TemplateName { get; set; }

        public override List<Attachment> Attachments { get; set; }

        public override List<EmbeddedResource> EmbeddedResources { get; set; }

        [DataMember, JsonConverter(typeof(JsonConcreteTypeConverter<CommunicationMessageData>))]
        public override ICommunicationMessageData MessageData { get; set; }

        [DataMember]
        public AppSettingsEmailSetting AppSettingsEmailSetting { get; set; }

        protected EmailCommunicationMessage()
        {
            Attachments = new List<Attachment>();
            EmbeddedResources = new List<EmbeddedResource>();
        }

        public EmailCommunicationMessage(string templateName, ICommunicationMessageData messageData, string to, string subject, AppSettingsEmailSetting appSettingsEmailSettings)
            : base(templateName, messageData)
        {
            Address = to;
            Subject = subject;

            Attachments = new List<Attachment>();
            EmbeddedResources = new List<EmbeddedResource>();
            AppSettingsEmailSetting = appSettingsEmailSettings;
        }

        public EmailCommunicationMessage(string templateName, ICommunicationMessageData messageData, string to, string subject, ITemplateEngine templateEngine, AppSettingsEmailSetting appSettingsEmailSettings)
            : base(templateName, messageData)
        {
            Address = to;
            Subject = subject;

            Attachments = new List<Attachment>();
            EmbeddedResources = new List<EmbeddedResource>();

            Generate(templateEngine);
        }

        public EmailCommunicationMessage(string templateName, ICommunicationMessageData messageData, string to, string subject, List<Attachment> attachments, List<EmbeddedResource> embeddedResources , AppSettingsEmailSetting appSettingsEmailSettings)
            : base(templateName, messageData)
        {
            Address = to;
            Subject = subject;

            Attachments = attachments;
            EmbeddedResources = embeddedResources;
        }
    }
}