﻿using System.Collections.Generic;
using ToolBox.Communication.Interfaces;

namespace Domain.Emailing
{
    public class CommunicationMessageData : ICommunicationMessageData
    {
        public IDictionary<string, string> Data { get; set; }

        public CommunicationMessageData(IDictionary<string, string> data)
        {
            Data = data;
        }
    }
}