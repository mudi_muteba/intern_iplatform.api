﻿using System;

using Common.Logging;

using ToolBox.Communication.Emailing;

namespace Domain.Emailing.Factory
{
    public abstract class EmailSettings : IEmailSettings
    {
        public abstract string Host { get; }
        public abstract int Port { get; }
        public abstract bool UseSSL { get; }
        public abstract bool UseDefaultCredentials { get; }
        public abstract string UserName { get; }
        public abstract string Password { get; }
        public abstract string SubAccountID { get; }
        public abstract string DefaultFrom { get; }

        private static readonly ILog log = LogManager.GetLogger<EmailSettings>();

        public void Validate()
        {
            log.InfoFormat("Validating email settings of type '{0}'", GetType());

            if (string.IsNullOrEmpty(DefaultFrom))
            {
                log.ErrorFormat("Invalid sender");
                throw new InvalidOperationException("Invalid sender");
            }

            if (string.IsNullOrEmpty(Host))
            {
                log.ErrorFormat("Invalid host");
                throw new InvalidOperationException("Invalid host");
            }
        }
    }
}
