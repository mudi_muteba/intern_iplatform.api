﻿using System.Net;
using System.Net.Mail;
using Shared.Extentions;
using ToolBox.Communication.Emailing;

namespace Domain.Emailing.Factory
{
    public class SmtpClientAdapter : IDispatchEmails
    {
        private readonly IEmailSettings _settings;

        public SmtpClientAdapter(IEmailSettings settings)
        {
            _settings = settings;
        }

        public void Send(MailMessage message)
        {
            this.Info(() => "Sending mail message");

            var client = new SmtpClient
            {
                Host = _settings.Host,
                Port = _settings.Port,
                EnableSsl = _settings.UseSSL,
                UseDefaultCredentials = _settings.UseDefaultCredentials,
            };

            if (client.Credentials == null)
                client.Credentials = new NetworkCredential(_settings.UserName, _settings.Password);

            using (var sender = new SmtpClientSender(client))
            {
                sender.Send(message);
            }
        }
    }
}
