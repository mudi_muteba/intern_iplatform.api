﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

using Common.Logging;

using ToolBox.Communication.Interfaces;
using ToolBox.Communication.Emailing;

namespace Domain.Emailing.Factory
{
    public class MailMessageFactory
    {
        private static readonly ILog log = LogManager.GetLogger<MailMessageFactory>();

        public MailMessage Create<TMessageType>(CommunicationMessage<TMessageType> message, IEmailSettings settings, bool isContactUS, string displaName)
            where TMessageType : ICommunicationMessageData
        {
            log.InfoFormat("Creating mail message from communication message type '{0}'", message.GetType());

            var toAddresses = message.Address.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var ccAddresses = message.CCAddresses.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var bccAddresses = message.BCCAddresses.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            

            var mailMessage = new MailMessage
            {
                Subject = message.Subject,
                IsBodyHtml = true,
                Body = message.Body,
                BodyEncoding = Encoding.UTF8,
                From = isContactUS == false ? new MailAddress(settings.DefaultFrom) : new MailAddress(displaName, displaName)
            };

            foreach (var address in toAddresses)
                mailMessage.To.Add(address);

            foreach (var ccAddress in ccAddresses)
                mailMessage.CC.Add(ccAddress);

            foreach (var bccAddress in bccAddresses)
                mailMessage.Bcc.Add(bccAddress);

            // Custom Header for Mandrill Sub-account
            if (!string.IsNullOrWhiteSpace(settings.SubAccountID))
                mailMessage.Headers.Add("X-MC-Subaccount", settings.SubAccountID);

            //Attachments
            foreach (var attachment in message.Attachments)
                mailMessage.Attachments.Add(attachment);

            //Embedded resources
            //dont want attachments on contact us emails
            if (isContactUS == false)
            {
                if (message.EmbeddedResources.Count > 0)
                    mailMessage.AlternateViews.Add(AttachedLinkedResources(mailMessage, message.EmbeddedResources));
            }

            return mailMessage;
        }

        public AlternateView AttachedLinkedResources(MailMessage mailMessage, List<EmbeddedResource> embeddedResources)
        {
            foreach (var embeddedResource in embeddedResources)
                mailMessage.Body = mailMessage.Body.Replace(embeddedResource.Attribute, string.Format(embeddedResource.Value, embeddedResource.LinkedResource.ContentId));

            var alternateView = AlternateView.CreateAlternateViewFromString(
                mailMessage.Body,
                null,
                "text/html"
                );

            foreach (var embeddedResource in embeddedResources)
                alternateView.LinkedResources.Add(embeddedResource.LinkedResource);

            return alternateView;
        }
    }
}
