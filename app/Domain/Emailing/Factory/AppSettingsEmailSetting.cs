﻿using Common.Logging;
using System;
using ToolBox.Communication.Emailing;

namespace Domain.Emailing.Factory
{
    public class AppSettingsEmailSetting : IEmailSettings
    {
        private static readonly ILog log = LogManager.GetLogger<AppSettingsEmailSettings>();
        public string CustomFrom { get; set; }

        public AppSettingsEmailSetting() { }
        


        public AppSettingsEmailSetting AddCustomFrom(string customFrom)
        {
            CustomFrom = customFrom;
            return this;
        }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool UseSSL { get; set; }
        public bool UseDefaultCredentials { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DefaultFrom { get; set; }
        public string DefaultBCC { get; set; }
        public string DefaultContactNumber { get; set; }
        public string SubAccountID { get; set; }

        public void Validate()
        {
            log.InfoFormat("Validating email settings of type '{0}'", GetType());

            if (string.IsNullOrEmpty(DefaultFrom))
            {
                log.ErrorFormat("Invalid sender");
                throw new InvalidOperationException("Invalid sender");
            }

            if (string.IsNullOrEmpty(Host))
            {
                log.ErrorFormat("Invalid host");
                throw new InvalidOperationException("Invalid host");
            }
        }
    }
}
