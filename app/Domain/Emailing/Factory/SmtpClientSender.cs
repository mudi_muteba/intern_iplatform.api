﻿using System;
using System.Net.Mail;

using Common.Logging;

namespace Domain.Emailing.Factory
{
    internal class SmtpClientSender : IDisposable
    {
        private readonly SmtpClient client;
        private bool disposed;

        private static readonly ILog log = LogManager.GetLogger<SmtpClientSender>();

        public SmtpClientSender(SmtpClient client)
        {
            this.client = client;
        }

        public void Send(MailMessage message)
        {
            if (message == null)
            {
                log.ErrorFormat("Message is null. Will not send email");
                throw new ArgumentNullException("message", "Can not send a null mail message");
            }

            if (client == null)
            {
                log.ErrorFormat("Client is null. Will not send email");
                throw new ArgumentNullException("client", "The SMTP client is null");
            }

            log.InfoFormat("Sending email");

            client.Send(message);

            log.InfoFormat("Email sent");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~SmtpClientSender()
        {
            Dispose(false);
        }

        protected void Dispose(bool disposing)
        {
            log.InfoFormat("Disposing of SmtpClient");
            if (disposed) return;
            if (disposing)
            {
                if (client != null)
                {
                    client.Dispose();
                    log.InfoFormat("SmtpClient disposed");
                }
            }

            disposed = true;
        }
    }
}
