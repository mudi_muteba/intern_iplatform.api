﻿using System;
using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.JsonDataStores;

namespace Domain.JsonDataStores
{
    public class JsonDataStore : Entity
    {
        public virtual string SessionId { get; set; }
        public virtual int ChannelId { get; set; }
        public virtual int PartyId { get; set; }
        public virtual string Section { get; set; }
        public virtual int SavePoint { get; set; }
        public virtual string Store { get; set; }
        public virtual DateTime TransactionDate { get; set; }

        public JsonDataStore()
        {
            
        }

        public static JsonDataStore Create(CreateJsonDataStoreDto dto)
        {
            var jsonStorage = Mapper.Map<JsonDataStore>(dto);

            return jsonStorage;
        }
    }
}
