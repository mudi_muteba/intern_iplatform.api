﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.JsonDataStores.Queries
{
    public class GetJsonDataStoreByPartyId : BaseQuery<JsonDataStore>
    {
        public GetJsonDataStoreByPartyId(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<JsonDataStore>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public int partyId;
        public void WithPartyId(int partyId)
        {
            this.partyId = partyId;
        }

        protected internal override IQueryable<JsonDataStore> Execute(IQueryable<JsonDataStore> query)
        {
            return query
                .Where(c => c.PartyId == partyId);
        }
    }
}
