﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Occupations.Queries;
using FluentNHibernate.Utils;
using iPlatform.Api.DTOs.JsonDataStores;
using MasterData.Authorisation;
using NHibernate.Linq;
using NHibernate.Util;

namespace Domain.JsonDataStores.Queries
{
    public class SearchJsonDataStoreQuery : BaseQuery<JsonDataStore>, ISearchQuery<JsonDataStoreSearchDto>
    {
        private JsonDataStoreSearchDto criteria;

        public SearchJsonDataStoreQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<JsonDataStore>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public void WithCriteria(JsonDataStoreSearchDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<JsonDataStore> Execute(IQueryable<JsonDataStore> query)
        {
            if (criteria.GetLatestByPartyId)
            {
                query = query.Where(x => x.PartyId == criteria.PartyId)
                    .OrderByDescending(x => x.Id)
                    .Take(1);
            }
            else if (!string.IsNullOrWhiteSpace(criteria.SessionId))
                query = query.Where(x => x.SessionId.Contains(criteria.SessionId));

            return query;
        }
    }
}
