﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.JsonDataStores.Queries
{
    public class GetJsonDataStoreById : BaseQuery<JsonDataStore>
    {
        public GetJsonDataStoreById(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<JsonDataStore>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        public int id;
        public void WithGuid(int id)
        {
            this.id = id;
        }

        protected internal override IQueryable<JsonDataStore> Execute(IQueryable<JsonDataStore> query)
        {
            return query
                .Where(x => x.Id == id);
        }
    }
}
