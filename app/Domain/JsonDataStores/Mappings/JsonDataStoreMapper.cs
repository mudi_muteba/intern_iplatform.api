﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.JsonDataStores;

namespace Domain.JsonDataStores.Mappings
{
    public class JsonDataStoreMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateJsonDataStoreDto, JsonDataStore>();

            Mapper.CreateMap<JsonDataStore, JsonDataStoreDto>();

            Mapper.CreateMap<JsonDataStoreDto, JsonDataStore>();

            Mapper.CreateMap<List<JsonDataStore>, ListResultDto<JsonDataStoreDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<List<JsonDataStore>, ListResultDto<JsonDataStore>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<PagedResults<JsonDataStore>, PagedResultDto<JsonDataStoreDto>>();
        }
    }
}
