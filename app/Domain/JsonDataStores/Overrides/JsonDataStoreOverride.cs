﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.JsonDataStores.Overrides
{
    public class JsonDataStoreOverride : IAutoMappingOverride<JsonDataStore>
    {
        public void Override(AutoMapping<JsonDataStore> mapping)
        {
            mapping.Map(x => x.Store).Length(10000);
        }
    }
}