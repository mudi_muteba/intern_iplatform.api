﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.JsonDataStores;
using MasterData.Authorisation;

namespace Domain.JsonDataStores.Handlers
{
    public class CreateJsonDataStoreDtoHandler : CreationDtoHandler<JsonDataStore, CreateJsonDataStoreDto, int>
    {
        public CreateJsonDataStoreDtoHandler(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {

                };
            }
        }

        protected override void EntitySaved(JsonDataStore entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override JsonDataStore HandleCreation(CreateJsonDataStoreDto dto, HandlerResult<int> result)
        {
            var jsonStorageEntry = JsonDataStore.Create(dto);

            result.Processed(jsonStorageEntry.Id);

            return jsonStorageEntry;
        }
    }
}
