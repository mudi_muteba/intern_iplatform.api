﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using Domain.Party.ProposalDefinitions;
using iPlatform.Api.DTOs.AdditionalMembers;

namespace Domain.AdditionalMember.Handlers
{
    public class DisableAdditionalMemberHandler : ExistingEntityDtoHandler<AdditionalMembers, DisableAdditionalMemberDto, int>
    {
        public DisableAdditionalMemberHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    AdditionalMembersAuthorisation.Delete
                };
            }
        }

        protected override void HandleExistingEntityChange(AdditionalMembers entity, DisableAdditionalMemberDto dto,
            HandlerResult<int> result)
        {
            entity.Disable();
            result.Processed(entity.Id);
        }
    }
}