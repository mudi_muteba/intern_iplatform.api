﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions;
using iPlatform.Api.DTOs.AdditionalMembers;
using MasterData.Authorisation;

namespace Domain.AdditionalMember.Handlers
{
    public class EditAdditionalMemberHandler : ExistingEntityDtoHandler<AdditionalMembers, EditAdditionalMemberDto, int>
    {
        public EditAdditionalMemberHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    AdditionalMembersAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(AdditionalMembers entity, EditAdditionalMemberDto dto,
            HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}