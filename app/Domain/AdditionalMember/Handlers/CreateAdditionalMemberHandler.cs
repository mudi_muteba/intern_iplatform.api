﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Party.ProposalDefinitions;
using iPlatform.Api.DTOs.AdditionalMembers;
using MasterData.Authorisation;

namespace Domain.AdditionalMember.Handlers
{
    public class CreateAdditionalMemberHandler : CreationDtoHandler<AdditionalMembers, CreateAdditionalMemberDto, int>
    {
        public CreateAdditionalMemberHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    AdditionalMembersAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(AdditionalMembers entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override AdditionalMembers HandleCreation(CreateAdditionalMemberDto dto, HandlerResult<int> result)
        {
            var additionalMember = AdditionalMembers.Create(dto);

            result.Processed(additionalMember.Id);

            return additionalMember;
        }

        
    }
}