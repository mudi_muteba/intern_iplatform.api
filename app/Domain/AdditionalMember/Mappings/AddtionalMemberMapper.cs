﻿using System;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Repository;
using MasterData;
using Domain.Party.ProposalDefinitions;
using iPlatform.Api.DTOs.AdditionalMembers;
using iPlatform.Api.DTOs.Base;

namespace Domain.AdditionalMember.Mappings
{
    public class AdditionalMemberMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateAdditionalMemberDto, AdditionalMembers>()
                .ForMember(t => t.ProposalDefinition, o => o.MapFrom(s => new ProposalDefinition() { Id = s.ProposalDefinitionId }))
                .ForMember(t => t.MemberRelationship, o => o.MapFrom(s => new MemberRelationship() { Id = s.MemberRelationshipId }))
                .ForMember(t => t.Gender, o => o.MapFrom(s => new Gender() { Id = s.GenderId }))
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                ;

            Mapper.CreateMap<AdditionalMembers,ListAdditionalMemberDto>()
                .ForMember(t => t.ProposalDefinitionId, o => o.MapFrom(s => s.ProposalDefinition.Id))
                .ForMember(t => t.SumInsured, o => o.MapFrom(s => new MoneyDto(Convert.ToDecimal(s.SumInsured))))
                ;

            Mapper.CreateMap<List<AdditionalMembers>, ListResultDto<ListAdditionalMemberDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<EditAdditionalMemberDto, AdditionalMembers>()
                .ForMember(t => t.ProposalDefinition, o => o.MapFrom(s => new ProposalDefinition() { Id = s.ProposalDefinitionId }))
                .ForMember(t => t.MemberRelationship, o => o.MapFrom(s => new MemberRelationship() { Id = s.MemberRelationshipId }))
                .ForMember(t => t.Gender, o => o.MapFrom(s => new Gender() { Id = s.GenderId }))
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                ;

            Mapper.CreateMap<CreateAdditionalMemberDto, EditAdditionalMemberDto>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.SumInsured, o => o.MapFrom(s => s.SumInsured))
                .ForMember(t => t.Surname, o => o.MapFrom(s => s.Surname))
                .ForMember(t => t.Initials, o => o.MapFrom(s => s.Initials))
                .ForMember(t => t.IdNumber, o => o.MapFrom(s => s.IdNumber))
                .ForMember(t => t.IsStudent, o => o.MapFrom(s => s.IsStudent))
                .ForMember(t => t.MemberRelationshipId, o => o.MapFrom(s => s.MemberRelationshipId))
                .ForMember(t => t.GenderId, o => o.MapFrom(s => s.GenderId))
                .ForMember(t => t.ProposalDefinitionId, o => o.MapFrom(s => s.ProposalDefinitionId))
                .ForMember(t => t.DateOnCover, o => o.MapFrom(s => s.DateOnCover))
                ;
        }
    }
}