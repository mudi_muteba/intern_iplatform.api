﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.FuneralMembers;
using Domain.FuneralMembers.Queries;
using Domain.Party.ProposalDefinitions;
using MasterData.Authorisation;

namespace Domain.AdditionalMember.Queries
{
    public class GetAdditionalMembersByProposalDefinitionIdQuery : BaseQuery<AdditionalMembers>
    {
        private int id;

        public GetAdditionalMembersByProposalDefinitionIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<AdditionalMembers>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    AdditionalMembersAuthorisation.List
                };
            }
        }

        public GetAdditionalMembersByProposalDefinitionIdQuery ByProposalDefinitionId(int id)
        {
            this.id = id;
            return this;
        }

        protected internal override IQueryable<AdditionalMembers> Execute(IQueryable<AdditionalMembers> query)
        {
            return query
                .Where(c => c.ProposalDefinition.Id == id);
        }
    }
}