﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using Domain.Party.ProposalDefinitions;

namespace Domain.AdditionalMember.Overrides
{
    public class AdditionalMemberOverride : IAutoMappingOverride<AdditionalMembers>
    {
        public void Override(AutoMapping<AdditionalMembers> mapping)
        {
            mapping.References(x => x.ProposalDefinition, "ProposalDefinitionId");
        }
    }
}
