﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.Admin.ChannelEvents;
using iPlatform.Enums.Workflows;

namespace Domain.Admin.ChannelEvents
{
    public class ChannelEventTask : Entity
    {
        public virtual ChannelEvent ChannelEvent { get; protected internal set; }
        public virtual WorkflowMessageType TaskName { get; protected internal set; }

        protected ChannelEventTask() { }

        public ChannelEventTask(ChannelEvent channelEvent, WorkflowMessageType taskName)
        {
            ChannelEvent = channelEvent;
            TaskName = taskName;
        }

        public static ChannelEventTask Create(ChannelEventTaskDto dto, ChannelEvent channelEvent)
        {
            var entity =  Mapper.Map<ChannelEventTask>(dto);
            entity.ChannelEvent = channelEvent;
            return entity;
        }

        public virtual void Update(ChannelEventTaskDto editDto)
        {
            Mapper.Map(editDto, this);
        }
    }
}