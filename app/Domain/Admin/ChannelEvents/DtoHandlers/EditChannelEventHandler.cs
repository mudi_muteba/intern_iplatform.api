﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Admin;
using System.Linq;
using Domain.Admin.ChannelEvents;
using iPlatform.Api.DTOs.Admin.ChannelEvents;

namespace Domain.Admin.Handlers
{
    public class EditChannelEventHandler : ExistingEntityDtoHandler<ChannelEvent, EditChannelEventDto, int>
    {
        public EditChannelEventHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ChannelEventAuthorisation.Edit
                };
            }
        }

        protected override void HandleExistingEntityChange(ChannelEvent entity, EditChannelEventDto dto,
            HandlerResult<int> result)
        {

            entity.Update(dto);

            result.Processed(entity.Id);
        }
    }
}