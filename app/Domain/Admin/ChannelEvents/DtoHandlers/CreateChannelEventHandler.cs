﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Admin;
using System.Linq;
using Domain.Users;
using AutoMapper;
using iPlatform.Api.DTOs.Admin.ChannelEvents;
using Domain.Admin.ChannelEvents;

namespace Domain.AdminChannelEvent.Handlers
{
    public class CreateChannelEventHandler : CreationDtoHandler<ChannelEvent, CreateChannelEventDto, int>
    {
        public CreateChannelEventHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            this.repository = repository;
        }
        private readonly IRepository repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                   ChannelEventAuthorisation.Create
                };
            }
        }

        protected override void EntitySaved(ChannelEvent entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override ChannelEvent HandleCreation(CreateChannelEventDto dto, HandlerResult<int> result)
        {
            var entity = ChannelEvent.Create(dto);

            return entity;
        }
    }
}