﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.ChannelEvents;
using MasterData.Authorisation;

namespace Domain.Admin.ChannelEvents.DtoHandlers
{
    public class SaveChannelEventsDtoHandler : BaseDtoHandler<SaveChannelEventsDto, bool>
    {
        private readonly IRepository m_Repository;
        public SaveChannelEventsDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(SaveChannelEventsDto dto, HandlerResult<bool> result)
        {
            var channelEvents = m_Repository.GetAll<ChannelEvent>()
               .Where(x => x.Channel.Id == dto.ChannelId && !x.IsDeleted).ToList();

            //remove old channel event not listed in the dto
            RemoveOldSettings(dto, channelEvents);

            //Add new channelevent
            AddNewSettings(dto, channelEvents);
            result.Processed(true);
        }

        private void RemoveOldSettings(SaveChannelEventsDto dto, List<ChannelEvent> channelEvents)
        {
            var toBeDeleted = channelEvents.Where(t => !dto.ChannelEvents.Any(y => y.ProductCode.Equals(t.ProductCode, StringComparison.InvariantCultureIgnoreCase) && y.EventName.Equals(t.EventName))).ToList();

            foreach (var channelevent in toBeDeleted)
            {
                channelevent.Delete();
                m_Repository.Save(channelevent);
            }
        }

        private void AddNewSettings(SaveChannelEventsDto dto, List<ChannelEvent> channelEvents)
        {
            var newChannelEvents = dto.ChannelEvents.Where(x => !channelEvents.Any(y => y.ProductCode.Equals(x.ProductCode, StringComparison.InvariantCultureIgnoreCase) && y.EventName.Equals(x.EventName))).ToList();
            foreach (var channelevent in newChannelEvents)
            {
                var entity = ChannelEvent.Create(channelevent);
                m_Repository.Save(entity);
            }
        }
    }
}