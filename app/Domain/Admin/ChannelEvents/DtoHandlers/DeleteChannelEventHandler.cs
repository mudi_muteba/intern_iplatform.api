﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using Domain.Admin.ChannelEvents;
using iPlatform.Api.DTOs.Admin.ChannelEvents;

namespace Domain.Admin.Handlers
{
    public class DeleteChannelEventHandler : ExistingEntityDtoHandler<ChannelEvent, DeleteChannelEventDto, int>
    {
        public DeleteChannelEventHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    ChannelEventAuthorisation.Delete
                };
            }
        }

        protected override void HandleExistingEntityChange(ChannelEvent entity, DeleteChannelEventDto dto, HandlerResult<int> result)
        {

            entity.Delete();
            result.Processed(entity.Id);
        }
    }
}