﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.ChannelEvents.Queries
{
    public class GetAllChannelEventsQuery : BaseQuery<ChannelEvent>
    {
        public GetAllChannelEventsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ChannelEvent>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        protected internal override IQueryable<ChannelEvent> Execute(IQueryable<ChannelEvent> query)
        {
            return query;
        }
    }
}