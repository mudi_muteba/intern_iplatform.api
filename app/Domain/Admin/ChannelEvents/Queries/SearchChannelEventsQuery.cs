﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Admin.ChannelEvents;

namespace Domain.Admin.ChannelEvents.Queries
{
    public class SearchChannelEventsQuery : BaseQuery<ChannelEvent>, ISearchQuery<SearchChannelEventsDto>
    {
        public SearchChannelEventsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ChannelEvent>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }
        private SearchChannelEventsDto criteria;

        public void WithCriteria(SearchChannelEventsDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<ChannelEvent> Execute(IQueryable<ChannelEvent> query)
        {
            if (!string.IsNullOrEmpty(criteria.EventName))
                query = query.Where(x => x.EventName.StartsWith(criteria.EventName));

            if (!string.IsNullOrEmpty(criteria.ProductCode))
                query = query.Where(x => x.ProductCode.StartsWith(criteria.ProductCode));

            if (criteria.ChannelId>0)
                query = query.Where(x => x.Channel.Id == criteria.ChannelId);

            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);

            return query;
        }
    }
}