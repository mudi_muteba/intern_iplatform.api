﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base;
using iPlatform.Enums.Workflows;
using Infrastructure.NHibernate.Attributes;
using Newtonsoft.Json;
using AutoMapper;
using iPlatform.Api.DTOs.Admin.ChannelEvents;

namespace Domain.Admin.ChannelEvents
{
    public class ChannelEvent : Entity
    {
        [JsonIgnore]
        private ISet<ChannelEventTask> _channelEventTasks = new HashSet<ChannelEventTask>();

        protected ChannelEvent() { }

        public ChannelEvent(Channel channel, string eventName)
        {
            Channel = channel;
            EventName = eventName;
        }

        public ChannelEvent(Channel channel, string eventName, string productCode)
        {
            Channel = channel;
            EventName = eventName;
            ProductCode = productCode;
        }

        public virtual string ProductCode { get; protected internal set; }
        public virtual string EventName { get; protected internal set; }
        public virtual Channel Channel { get; protected internal set; }
        [JsonIgnore]
        public virtual ISet<ChannelEventTask> Tasks
        {
            get { return _channelEventTasks; }
            protected internal set { _channelEventTasks = value; }
        }

        [DoNotMap]
        public virtual IEnumerable<WorkflowMessageType> TaskNames
        {
            get { return Tasks.Select(x => x.TaskName); }
        }

        public virtual void SetTask(WorkflowMessageType taskName)
        {
            var existingTask = _channelEventTasks.FirstOrDefault(x => x.TaskName == taskName);
            if (existingTask == null)
            {
                existingTask = new ChannelEventTask(this, taskName);
                _channelEventTasks.Add(existingTask);
            }
        }

        public static ChannelEvent Create(CreateChannelEventDto createDto)
        {
            var entity = Mapper.Map<ChannelEvent>(createDto);
            AddTasks(entity, createDto.Tasks);
            return entity;
        }

        public virtual void Update(EditChannelEventDto editDto)
        {
            Mapper.Map(editDto, this);
        }

        public static void AddTasks(ChannelEvent entity, List<ChannelEventTaskDto> tasks)
        {
            foreach (var task in tasks)
            {
                entity.Tasks.Add(ChannelEventTask.Create(task, entity));
            }
        }

        public virtual void UpdateTasks(EditChannelEventDto dto)
        {
            RemoveOldTasks(dto.Tasks);

            //existing tasks
            var existingTasks = Tasks.Where(x => dto.Tasks.Any(y => y.Id == x.Id)).ToList();
            foreach (var task in existingTasks)
            {
                var Taskdto = dto.Tasks.FirstOrDefault(x => x.Id == task.Id);
                task.Update(Taskdto);
            }

            //new tasks
            var newTasks = dto.Tasks.Where(x => !Tasks.Any(y => y.Id == x.Id)).ToList();
            AddTasks(this, newTasks);
        }

        public virtual void RemoveOldTasks(List<ChannelEventTaskDto> tasks)
        {
            var toBeDeleted = Tasks.Where(t => !tasks.Any(y => y.Id == t.Id));

            foreach (var task in toBeDeleted)
            {
                task.Delete();
            }
        }
    }
}