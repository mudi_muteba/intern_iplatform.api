﻿using Domain.Admin;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Admin.ChannelEvents.Overrides
{
    public class ChannelEventTaskOverride : IAutoMappingOverride<ChannelEventTask>
    {
        public void Override(AutoMapping<ChannelEventTask> mapping)
        {
            mapping.Map(x => x.TaskName).CustomType<int>();
            mapping.References(x => x.ChannelEvent).Cascade.SaveUpdate();
        }
    }
}