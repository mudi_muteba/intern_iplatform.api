﻿using Domain.Admin;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Admin.ChannelEvents.Overrides
{
    public class ChannelEventOverride : IAutoMappingOverride<ChannelEvent>
    {
        public void Override(AutoMapping<ChannelEvent> mapping)
        {
            mapping.References(x => x.Channel).Cascade.SaveUpdate();
            mapping.HasMany(x => x.Tasks).Cascade.SaveUpdate();
        }
    }
}