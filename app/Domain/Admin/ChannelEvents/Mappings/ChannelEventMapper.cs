﻿using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.ChannelEvents;
using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;


namespace Domain.Admin.ChannelEvents.Mappings
{
    public class ChannelEventMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ChannelEvent, ChannelEventDto>();

            Mapper.CreateMap<ChannelEvent, ChannelEventInfoDto>();

            Mapper.CreateMap<List<ChannelEvent>, ListResultDto<ChannelEventDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;
            Mapper.CreateMap<PagedResults<ChannelEvent>, PagedResultDto<ChannelEventDto>>();

            Mapper.CreateMap<CreateChannelEventDto, ChannelEvent>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.ChannelId)))
                .ForMember(t => t.ProductCode, o => o.MapFrom(s => s.ProductCode))
                .ForMember(t => t.EventName, o => o.MapFrom(s => s.EventName))
                .ForMember(t => t.Tasks, o => o.Ignore())
                .ForMember(t => t.ChannelTags, o => o.Ignore())
                ;

            Mapper.CreateMap<EditChannelEventDto, ChannelEvent>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.ChannelId)))
                .ForMember(t => t.ProductCode, o => o.MapFrom(s => s.ProductCode))
                .ForMember(t => t.EventName, o => o.MapFrom(s => s.EventName))
                .ForMember(t => t.Tasks, o => o.Ignore())
                .ForMember(t => t.ChannelTags, o => o.Ignore())
                ;
        }
    }
}