﻿using AutoMapper;
using iPlatform.Api.DTOs.Admin.ChannelEvents;


namespace Domain.Admin.ChannelEvents.Mappings
{
    public class ChannelEventTaskMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<ChannelEventTask, ChannelEventTaskDto>();

            Mapper.CreateMap<ChannelEventTaskDto, ChannelEventTask>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.TaskName, o => o.MapFrom(s => s.TaskName))
                ;
        }
    }
}