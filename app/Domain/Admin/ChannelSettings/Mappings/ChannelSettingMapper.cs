﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.ChannelSettings;
using iPlatform.Api.DTOs.Base;
using MasterData;

namespace Domain.Admin.ChannelSettings.Mappings
{
    public class ChannelSettingMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateChannelSettingDto, ChannelSetting>()
                 .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel() { Id = s.ChannelId }))
                 .ForMember(t => t.ChannelSettingType, o => o.MapFrom(s => new ChannelSettingTypes().FirstOrDefault(x => x.Id == s.ChannelSettingTypeId)))
                 .ForMember(t => t.ChannelSettingValue, o => o.MapFrom(s => s.ChannelSettingValue))
                 .ForMember(t => t.ChannelSettingName, o => o.MapFrom(s => s.ChannelSettingName))
                 .ForMember(t => t.ChannelSettingDescription, o => o.MapFrom(s => s.ChannelSettingDescription))
                 .ForMember(t => t.ChannelSettingDataType, o => o.MapFrom(s => new DataTypes().FirstOrDefault(x => x.Id == s.ChannelSettingDataTypeId)))
                 .ForMember(t => t.ChannelEnvironment, o => o.MapFrom(s => s.ChannelEnvironmentId != null ?  new ChannelEnvironment() { Id = (int) s.ChannelEnvironmentId } : null))
                 .ForMember(t => t.ChannelSettingOwner, o => o.MapFrom(s => new ChannelSettingOwners().FirstOrDefault(x => x.Id == s.ChannelSettingOwnerId)))
                 ;

            Mapper.CreateMap<UpdateChannelSettingDto, ChannelSetting>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel() {Id = s.ChannelId}))
                .ForMember(t => t.ChannelSettingType,
                    o => o.MapFrom(s => new ChannelSettingTypes().FirstOrDefault(x => x.Id == s.ChannelSettingTypeId)))
                .ForMember(t => t.ChannelSettingValue, o => o.MapFrom(s => s.ChannelSettingValue))
                .ForMember(t => t.ChannelSettingName, o => o.MapFrom(s => s.ChannelSettingName))
                .ForMember(t => t.ChannelSettingDescription, o => o.MapFrom(s => s.ChannelSettingDescription))
                .ForMember(t => t.ChannelSettingDataType,
                    o => o.MapFrom(s => new DataTypes().FirstOrDefault(x => x.Id == s.ChannelSettingDataTypeId)))
                .ForMember(t => t.ChannelEnvironment,
                    o =>
                        o.MapFrom(
                            s =>
                                s.ChannelEnvironmentId != null
                                    ? new ChannelEnvironment() {Id = (int) s.ChannelEnvironmentId}
                                    : null))
                .ForMember(t => t.ChannelSettingOwner,
                    o => o.MapFrom(s => new ChannelSettingOwners().FirstOrDefault(x => x.Id == s.ChannelSettingOwnerId)))
                ;

            Mapper.CreateMap<List<ChannelSetting>, ListResultDto<ListChannelSettingDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                .AfterMap((s, d) =>
                {
                    s.ForEach(x =>
                    {
                        d.Results.Add(Mapper.Map<ListChannelSettingDto>(x));
                    });
                });

            Mapper.CreateMap<PagedResults<ChannelSetting>, PagedResultDto<ListChannelSettingDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s.Results));

            Mapper.CreateMap<ChannelSetting, ListChannelSettingDto>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => s.Channel))
                .ForMember(t => t.ChannelSettingName, o => o.MapFrom(s => s.ChannelSettingName))
                .ForMember(t => t.ChannelSettingType,
                    o => o.MapFrom(s => new ChannelSettingTypes().FirstOrDefault(x => x.Id == s.ChannelSettingType.Id)))
                .ForMember(t => t.ChannelSettingValue, o => o.MapFrom(s => s.ChannelSettingValue))
                .ForMember(t => t.ChannelSettingDescription, o => o.MapFrom(s => s.ChannelSettingDescription))
                .ForMember(t => t.ChannelSettingDataType,
                    o => o.MapFrom(s => new DataTypes().FirstOrDefault(x => x.Id == s.ChannelSettingDataType.Id)))
                .ForMember(t => t.ChannelSettingOwner,
                    o => o.MapFrom(s => new ChannelSettingOwners().FirstOrDefault(x => x.Id == s.ChannelSettingOwner.Id)));


            Mapper.CreateMap<List<ChannelSetting>, ListResultDto<ChannelSettingDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<ChannelSetting, ChannelSettingDto>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => s.Channel))
                .ForMember(t => t.ChannelSettingName, o => o.MapFrom(s => s.ChannelSettingName))
                .ForMember(t => t.ChannelSettingType,
                    o => o.MapFrom(s => new ChannelSettingTypes().FirstOrDefault(x => x.Id == s.ChannelSettingType.Id)))
                .ForMember(t => t.ChannelSettingValue, o => o.MapFrom(s => s.ChannelSettingValue))
                .ForMember(t => t.ChannelSettingDescription, o => o.MapFrom(s => s.ChannelSettingDescription))
                .ForMember(t => t.ChannelSettingDataType,
                    o => o.MapFrom(s => new DataTypes().FirstOrDefault(x => x.Id == s.ChannelSettingDataType.Id)))
                .ForMember(t => t.ChannelSettingOwner,
                    o => o.MapFrom(s => new ChannelSettingOwners().FirstOrDefault(x => x.Id == s.ChannelSettingOwner.Id)));
        }
    }
}