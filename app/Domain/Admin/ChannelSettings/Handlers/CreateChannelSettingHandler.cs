﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.ChannelSettings;
using MasterData.Authorisation;

namespace Domain.Admin.ChannelSettings.Handlers
{
    public class CreateChannelSettingHandler : CreationDtoHandler<ChannelSetting, CreateChannelSettingDto, int>
    {
        private IRepository repository;
        public CreateChannelSettingHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
            this.repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                    
                };
            }
        }

        protected override void EntitySaved(ChannelSetting entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override ChannelSetting HandleCreation(CreateChannelSettingDto dto, HandlerResult<int> result)
        {
            var entity = ChannelSetting.Create(dto);
            result.Processed(entity.Id);

            return entity;
        }
    }
}