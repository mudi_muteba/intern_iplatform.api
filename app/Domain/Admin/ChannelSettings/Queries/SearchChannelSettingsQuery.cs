﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.ChannelSettings;
using MasterData.Authorisation;

namespace Domain.Admin.ChannelSettings.Queries
{
    public class SearchChannelSettingsQuery : BaseQuery<ChannelSetting>, ISearchQuery<SearchChannelSettingDto>
    {
        private SearchChannelSettingDto _criteria;

        public SearchChannelSettingsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultChannelSettingFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithCriteria(SearchChannelSettingDto criteria)
        {
            this._criteria = criteria;
        }

        protected internal override IQueryable<ChannelSetting> Execute(IQueryable<ChannelSetting> query)
        {
            if (!string.IsNullOrEmpty(_criteria.ChannelSettingName))
                query = query.Where(x => x.ChannelSettingName.StartsWith(_criteria.ChannelSettingName));

            if (_criteria.ChannelId >0)
                query = query.Where(x => x.Channel.Id == _criteria.ChannelId);

            return query;
        }
    }
}