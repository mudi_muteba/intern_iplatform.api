﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.ChannelSettings;
using MasterData.Authorisation;

namespace Domain.Admin.ChannelSettings.Queries
{
    public class GetChannelSettingsByChannelIdQuery : BaseQuery<ChannelSetting>, ISearchQuery<ChannelSettingSearchDto>
    {
        private ChannelSettingSearchDto _criteria;

        public GetChannelSettingsByChannelIdQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<ChannelSetting>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>(){};
            }
        }

        public void WithCriteria(ChannelSettingSearchDto criteria)
        {
            this._criteria = criteria;
        }

        public GetChannelSettingsByChannelIdQuery WithChannelId(int channelId)
        {
            this._criteria = new ChannelSettingSearchDto { ChannelId = channelId };
            return this;
        }

        public GetChannelSettingsByChannelIdQuery WithEnvironment(int channelId, int channelEnvironmentId)
        {
            this._criteria = new ChannelSettingSearchDto { ChannelId = channelId, ChannelEnvironmentId = channelEnvironmentId };
            return this;
        }


        public GetChannelSettingsByChannelIdQuery WithValues(int channelId, string settingName)
        {
            this._criteria = new ChannelSettingSearchDto { ChannelId = channelId, ChannelSettingName = settingName  };
            return this;
        }

        protected internal override IQueryable<ChannelSetting> Execute(IQueryable<ChannelSetting> query)
        {
            if (_criteria.ChannelId == 0)
                query = query.Where(a => a.Channel.IsDefault == true);
            else
                query = query.Where(a => a.Channel.Id == _criteria.ChannelId);

            if (_criteria.ChannelEnvironmentId != 0)
                query = query.Where(a => a.ChannelEnvironment.Id == _criteria.ChannelEnvironmentId);

            if (!string.IsNullOrEmpty(_criteria.ChannelSettingName))
                query = query.Where(a => a.ChannelSettingName.ToLower() == _criteria.ChannelSettingName.ToLower());

            return query;
        }
    }
}