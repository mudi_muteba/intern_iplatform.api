﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Admin.ChannelSettings.Queries
{
    public class DefaultChannelSettingFilters : IApplyDefaultFilters<ChannelSetting>
    {
        public IQueryable<ChannelSetting> Apply(ExecutionContext executionContext, IQueryable<ChannelSetting> query)
        {
            return query.Where(c => c.IsDeleted == false);
        }
    }
}