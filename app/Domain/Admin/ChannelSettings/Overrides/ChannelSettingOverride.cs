﻿using Domain.Admin;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Admin.ChannelSettings.Overrides
{
    public class ChannelSettingOverride : IAutoMappingOverride<ChannelSetting>
    {
        public void Override(AutoMapping<ChannelSetting> mapping)
        {
            mapping.References(x => x.ChannelSettingDataType, "ChannelSettingDataTypeId");
        }
    }
}