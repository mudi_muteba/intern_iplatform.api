﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Domain.Admin.Queries;
using Domain.Admin.ChannelSettings.Queries;
using Domain.Base.Execution;
using Domain.Base.Repository;

namespace Domain.Admin.ChannelSettings
{
    public class ChannelSettingsHelper
    {
        private readonly int _channelId;

        private readonly GetAllChannelsQuery _channelsQuery;
        private readonly GetChannelEnvironmentsByChannelId _environmentQuery;
        private readonly GetChannelSettingsByChannelIdQuery _settingsQuery;

        private readonly string _environment;

        private readonly List<ChannelSetting> _channelSettings;
        private readonly List<ChannelSetting> _defaultSettings;
        private readonly ChannelEnvironment _channelEnvironment;

        private const string Environment = "Environment";

        public readonly Channel Channel;

        public ChannelSettingsHelper(IProvideContext contextProvider, IRepository repository, int channelId = -1)
        {
            //Should technically be the only app setting until a central auth Api exists
            _environment = ConfigurationManager.AppSettings[Environment] ?? "DEV";

            //Queries
            _channelsQuery = new GetAllChannelsQuery(contextProvider, repository);
            _environmentQuery = new GetChannelEnvironmentsByChannelId(contextProvider, repository);
            _settingsQuery = new GetChannelSettingsByChannelIdQuery(contextProvider, repository);

            //Default settings
            _defaultSettings = _settingsQuery.WithChannelId(0).ExecuteQuery().ToList();

            //Use default channelId if no channelId passed, 0 is base default, so ignore it since helper exposes default setting methods as well
            Channel = _channelsQuery.ExecuteQuery().FirstOrDefault(x => x.IsDefault);
            _channelId = (channelId <= 0) ? Channel.Id : channelId;

            //Get the channel settings based on current environment
            _channelEnvironment = _environmentQuery.WithChannelId(_channelId).WithEnvironment(_environment).ExecuteQuery().FirstOrDefault();

            //Use base default channel settings if environment not found
            _channelSettings = (_channelEnvironment == null) ? _defaultSettings : _settingsQuery.WithEnvironment(_channelId, _channelEnvironment.Id).ExecuteQuery().ToList();
        }

        public string GetChannelSetting(string settingName)
        {
            var result = _channelSettings.Where(x => x.ChannelSettingName == settingName).FirstOrDefault();

            return result != null ? result.ChannelSettingValue : string.Empty;
        }

        public bool GetChannelSettingAsBoolean(string settingName)
        {
            var result = _channelSettings.Where(x => x.ChannelSettingName == settingName).FirstOrDefault();

            return result != null ? (result.ChannelSettingValue != "0") : false;
        }

        public string GetDefaultChannelSetting(string settingName)
        {
            var result = _defaultSettings.Where(x => x.ChannelSettingName == settingName).FirstOrDefault();

            return result != null ? result.ChannelSettingValue : string.Empty;
        }

        public bool GetDefaultChannelSettingAsBoolean(string settingName)
        {
            var result = _defaultSettings.Where(x => x.ChannelSettingName == settingName).FirstOrDefault();

            return result != null ? (result.ChannelSettingValue != "0") : false;
        }
    }
}
