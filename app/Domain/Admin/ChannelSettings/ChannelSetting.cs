﻿using AutoMapper;
using Domain.Base;
using iPlatform.Api.DTOs.Admin.ChannelSettings;
using MasterData;

namespace Domain.Admin.ChannelSettings
{
    public class ChannelSetting : Entity
    {
        public ChannelSetting()
        {

        }

        public virtual Channel Channel { get; set; }
        public virtual ChannelEnvironment ChannelEnvironment { get; set; }
        public virtual ChannelSettingType ChannelSettingType { get; set; }
        public virtual ChannelSettingOwner ChannelSettingOwner { get; set; }
        public virtual DataType ChannelSettingDataType { get; set; }

        public virtual string ChannelSettingName { get; set; }
        public virtual string ChannelSettingValue { get; set; }
        public virtual string ChannelSettingDescription { get; set; }

        public static ChannelSetting Create(CreateChannelSettingDto dto)
        {
            var entity = Mapper.Map<ChannelSetting>(dto);

            return entity;
        }

        public virtual void Update(UpdateChannelSettingDto dto)
        {
            Mapper.Map(dto, this);
        }
    }
}
