﻿using Domain.Base;

namespace Domain.Admin
{
    public class ChannelEnvironment : Entity
    {
        public ChannelEnvironment() { }

        public virtual Channel Channel { get; set; }
        public virtual string Key { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual bool IsDefault { get; set; }
    }
}