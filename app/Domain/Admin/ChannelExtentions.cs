﻿using System.Collections.Generic;

namespace Domain.Admin
{
    public static class ChannelExtentions
    {
        public static IEnumerable<ChannelReference> CreateChannelReferences(this int channelId)
        {
            return new List<ChannelReference>()
            {
                new Channel(channelId)
            };
        }
    }
}