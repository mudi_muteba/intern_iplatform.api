﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.CoverDefinition;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Products;
using MasterData;

namespace Domain.Admin.CoverDefinitions.Mappings
{
    public class CoverDefinitionMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CoverDefinition, CoverDefinitionDto>()
                .ForMember(t => t.QuestionDefinitions, o => o.Ignore())
                ;
    
            Mapper.CreateMap<List<CoverDefinition>, ListResultDto<CoverDefinitionDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;
            Mapper.CreateMap<PagedResults<CoverDefinition>, PagedResultDto<CoverDefinitionDto>>();

            Mapper.CreateMap<CreateCoverDefinitionDto, CoverDefinition>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.DisplayName, o => o.MapFrom(s => s.DisplayName))
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product { Id = s.Product.Id }))
                .ForMember(t => t.Cover, o => o.MapFrom(s => new Covers().FirstOrDefault(x => x.Id == s.Cover.Id)))
                .ForMember(t => t.CoverDefinitionType, o => o.MapFrom(s => new CoverDefinitionTypes().FirstOrDefault(x => x.Id == s.CoverDefinitionType.Id)))
                .ForMember(t => t.ChannelTags, o => o.Ignore())
                ;

            Mapper.CreateMap<EditCoverDefinitionDto, CoverDefinition>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.DisplayName, o => o.MapFrom(s => s.DisplayName))
                .ForMember(t => t.Product, o => o.MapFrom(s => Mapper.Map<int, Product>(s.Product.Id)))
                .ForMember(t => t.Cover, o => o.MapFrom(s => new Covers().FirstOrDefault(x => x.Id == s.Cover.Id)))
                .ForMember(t => t.CoverDefinitionType, o => o.MapFrom(s => new CoverDefinitionTypes().FirstOrDefault(x => x.Id == s.CoverDefinitionType.Id)))
                .ForMember(t => t.ChannelTags, o => o.Ignore())
                ;
        }
    }
}