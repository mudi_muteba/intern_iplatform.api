﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.CoverDefinition;
using MasterData.Authorisation;

namespace Domain.Admin.CoverDefinitions.Handlers
{
    public class CreateCoverDefinitionEventHandler : CreationDtoHandler<CoverDefinition, CreateCoverDefinitionDto, int>
    {
        public CreateCoverDefinitionEventHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            this.repository = repository;
        }
        private readonly IRepository repository;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(CoverDefinition entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override CoverDefinition HandleCreation(CreateCoverDefinitionDto dto, HandlerResult<int> result)
        {
            var entity = CoverDefinition.Create(dto);

            return entity;
        }
    }
}