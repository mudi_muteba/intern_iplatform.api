﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.CoverDefinition;
using MasterData.Authorisation;

namespace Domain.Admin.CoverDefinitions.Handlers
{
    public class EditCoverDefinitionEventHandler : ExistingEntityDtoHandler<CoverDefinition, EditCoverDefinitionDto, int>
    {
        public EditCoverDefinitionEventHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(CoverDefinition entity, EditCoverDefinitionDto dto, HandlerResult<int> result)
        {
            entity.Update(dto);

            result.Processed(entity.Id);
        }
    }
}