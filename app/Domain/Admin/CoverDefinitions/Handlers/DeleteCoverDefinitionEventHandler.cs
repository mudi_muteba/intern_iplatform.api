﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.CoverDefinition;
using MasterData.Authorisation;

namespace Domain.Admin.CoverDefinitions.Handlers
{
    public class DeleteCoverDefinitionEventHandler : ExistingEntityDtoHandler<CoverDefinition, DeleteCoverDefinitionDto, int>
    {
        public DeleteCoverDefinitionEventHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(CoverDefinition entity, DeleteCoverDefinitionDto dto, HandlerResult<int> result)
        {

            entity.Delete();
            result.Processed(entity.Id);
        }
    }
}