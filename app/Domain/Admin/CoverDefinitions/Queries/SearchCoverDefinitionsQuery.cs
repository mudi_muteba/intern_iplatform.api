﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.CoverDefinition;
using MasterData.Authorisation;

namespace Domain.Admin.CoverDefinitions.Queries
{
   public class SearchCoverDefinitionsQuery : BaseQuery<CoverDefinition>, ISearchQuery<SearchCoverDefinitionsDto>
    {
        public SearchCoverDefinitionsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<CoverDefinition>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }
        private SearchCoverDefinitionsDto _criteria;

        public void WithCriteria(SearchCoverDefinitionsDto criteria)
        {
            this._criteria = criteria;
        }

        protected internal override IQueryable<CoverDefinition> Execute(IQueryable<CoverDefinition> query)
        {
            if (!string.IsNullOrEmpty(_criteria.DisplayName))
                query = query.Where(x => x.DisplayName.StartsWith(_criteria.DisplayName));

            if (!string.IsNullOrEmpty(_criteria.CoverName))
                query = query.Where(x => x.Cover.Name.StartsWith(_criteria.CoverName));

            if (!string.IsNullOrEmpty(_criteria.ProductName))
                query = query.Where(x => x.Product.Name.StartsWith(_criteria.ProductName));

            if (_criteria.ProductId > 0)
                query = query.Where(x => x.Product.Id == _criteria.ProductId);

            if (_criteria.CoverId > 0)
                query = query.Where(x => x.Cover.Id == _criteria.CoverId);

            if (_criteria.Id > 0)
                query = query.Where(x => x.Id == _criteria.Id);

            return query;
        }
    }
}