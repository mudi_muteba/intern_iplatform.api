﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Products;
using MasterData.Authorisation;

namespace Domain.Admin.CoverDefinitions.Queries
{
    public class GetAllCoverDefinitionsQuery : BaseQuery<CoverDefinition>
    {
        public GetAllCoverDefinitionsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<CoverDefinition>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        protected internal override IQueryable<CoverDefinition> Execute(IQueryable<CoverDefinition> query)
        {
            return query;
        }
    }
}