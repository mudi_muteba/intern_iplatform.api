﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

using FluentNHibernate.Mapping;

namespace Domain.Admin.Channels.SMSCommunicationSettings.Overrides
{
    public class SMSCommunicationSettingOverride : IAutoMappingOverride<SMSCommunicationSetting>
    {
        public void Override(AutoMapping<SMSCommunicationSetting> mapping)
        {
            mapping.References(x => x.Channel, "ChannelId").LazyLoad(Laziness.NoProxy);
        }
    }
}