﻿using AutoMapper;

using Domain.Base;

using iPlatform.Api.DTOs.Admin.SMSCommunicationSettings;

namespace Domain.Admin.Channels.SMSCommunicationSettings
{
    public class SMSCommunicationSetting : EntityWithAudit
    {
        public SMSCommunicationSetting() { }

        public virtual Channel Channel { get; set; }

        public virtual string SMSProviderName { get; set; }
        public virtual string SMSUserName { get; set; }
        public virtual string SMSPassword { get; set; }
        public virtual string SMSUrl { get; set; }

        public static SMSCommunicationSetting Create(CreateSMSCommunicationSettingDto dto)
        {
            return Mapper.Map<SMSCommunicationSetting>(dto);
        }
    }
}