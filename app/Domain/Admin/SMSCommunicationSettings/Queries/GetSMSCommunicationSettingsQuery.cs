﻿using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using MasterData.Authorisation;

namespace Domain.Admin.Channels.SMSCommunicationSettings.Queries
{
    public class GetSMSCommunicationSettingsQuery : BaseQuery<SMSCommunicationSetting>
    {
        private int _channelId;

        protected internal GetSMSCommunicationSettingsQuery() { }

        public GetSMSCommunicationSettingsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<SMSCommunicationSetting>()) { }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get { return new List<RequiredAuthorisationPoint>(); }
        }

        public GetSMSCommunicationSettingsQuery WithChannelId(int channelId)
        {
            _channelId = channelId;
            return this;
        }

        protected internal override IQueryable<SMSCommunicationSetting> Execute(IQueryable<SMSCommunicationSetting> query)
        {
            query.Where(x => x.Channel.Id == _channelId);
            return query;
        }
    }
}