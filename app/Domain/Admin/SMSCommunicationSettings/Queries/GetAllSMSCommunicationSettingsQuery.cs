﻿using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using MasterData.Authorisation;

namespace Domain.Admin.Channels.SMSCommunicationSettings.Queries
{
    public class GetAllSMSCommunicationSettingsQuery : BaseQuery<SMSCommunicationSetting>
    {
        public GetAllSMSCommunicationSettingsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<SMSCommunicationSetting>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected internal override IQueryable<SMSCommunicationSetting> Execute(IQueryable<SMSCommunicationSetting> query)
        {
            return query;
        }
    }
}