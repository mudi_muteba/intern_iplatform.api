﻿using System;
using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Admin.SMSCommunicationSettings;

using MasterData.Authorisation;

namespace Domain.Admin.Channels.SMSCommunicationSettings.Queries
{
    public class SearchSMSCommunicationSettingsQuery : BaseQuery<SMSCommunicationSetting>, ISearchQuery<SearchSMSCommunicationSettingDto>
    {
        private SearchSMSCommunicationSettingDto _criteria;

        public SearchSMSCommunicationSettingsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new NoDefaultFilters<SMSCommunicationSetting>())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithCriteria(SearchSMSCommunicationSettingDto criteria)
        {
            this._criteria = criteria;
        }

        protected internal override IQueryable<SMSCommunicationSetting> Execute(IQueryable<SMSCommunicationSetting> query)
        {

            if (!string.IsNullOrEmpty(_criteria.SMSProviderName))
                query = query.Where(x => x.SMSProviderName.StartsWith(_criteria.SMSProviderName));

            if (!string.IsNullOrEmpty(_criteria.SMSUserName))
                query = query.Where(x => x.SMSUserName.StartsWith(_criteria.SMSUserName));

            if (!string.IsNullOrEmpty(_criteria.SMSUrl))
                query = query.Where(x => x.SMSUserName.StartsWith(_criteria.SMSUrl));

            if (_criteria.Channel.SystemId != Guid.Empty)
                query = query.Where(x => x.Channel.Id == _criteria.Channel.Id);

            return query;
        }
    }
}