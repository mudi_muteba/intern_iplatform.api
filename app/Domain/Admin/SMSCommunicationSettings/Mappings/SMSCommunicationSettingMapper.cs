﻿using System;
using System.Collections.Generic;

using AutoMapper;

using Domain.Base.Repository;

using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Admin.SMSCommunicationSettings;
using iPlatform.Api.DTOs.Base;

namespace Domain.Admin.Channels.SMSCommunicationSettings.Mappings
{
    public class SMSCommunicationSettingMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<SMSCommunicationSetting, SMSCommunicationSettingDto>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => Mapper.Map<int, ChannelInfoDto>(s.Channel.Id)))
                ;

            Mapper.CreateMap<Guid, ChannelInfoDto>()
                .ForMember(t => t.SystemId, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<CreateSMSCommunicationSettingDto, SMSCommunicationSetting>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => Mapper.Map<int, Channel>(s.ChannelId)))
                ;

            Mapper.CreateMap<PagedResults<SMSCommunicationSetting>, PagedResultDto<SMSCommunicationSettingDto>>();

            Mapper.CreateMap<List<SMSCommunicationSetting>, ListResultDto<SMSCommunicationSettingDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<CreateSMSCommunicationSettingDto, SMSCommunicationSetting>()
                ;
        }
    }
}