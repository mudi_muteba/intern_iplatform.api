﻿using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;

using iPlatform.Api.DTOs.Admin.SMSCommunicationSettings;

using MasterData.Authorisation;

namespace Domain.Admin.Channels.SMSCommunicationSettings.Handlers
{
    public class CreateMultipleSMSCommunicationSettingsHandler : BaseDtoHandler<CreateMultipleSMSCommunicationSettingsDto, int>
    {
        private IRepository repository;
        public CreateMultipleSMSCommunicationSettingsHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider)
        {
            this.repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(CreateMultipleSMSCommunicationSettingsDto dto, HandlerResult<int> result)
        {
            dto.SMSCommunicationSettings
                .GroupBy(x => x.ChannelId)
                .Select(x => x.First())
                .ToList()
                .ForEach(x =>
                {
                    repository.GetAll<SMSCommunicationSetting>()
                        .Where(y => !y.IsDeleted && y.Channel.Id == x.ChannelId)
                        .ToList()
                        .ForEach(y =>
                        {
                            y.Delete();
                            repository.Save(y);
                        });
                });

            dto.SMSCommunicationSettings
                .ForEach(x =>
                {
                    repository.Save(SMSCommunicationSetting.Create(x));
                });

            result.Processed(1);
        }
    }
}