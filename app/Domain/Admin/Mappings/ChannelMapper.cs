﻿using AutoMapper;
using Domain.Base.Repository;
using System;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Base.Culture;
using System.Linq;
using Domain.Organizations;
using Domain.Users;
using iPlatform.Api.DTOs.Users;
using Domain.Base.Execution;
using iPlatform.Api.DTOs.Base.Infrastructure.Responses;

namespace Domain.Admin.Mappings
{
    public class ChannelMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Guid, Channel>()
                .ConvertUsing<ITypeConverter<Guid, Channel>>()
                ;

            Mapper.CreateMap<Channel, ChannelDto>()
                .ForMember(t => t.EmailCommunicationSetting, o => o.MapFrom(s => s.EmailCommunicationSettings.FirstOrDefault()))
                ;

            Mapper.CreateMap<IEnumerable<Channel>, SystemChannels>()
                .ForMember(t => t.Channels, o => o.MapFrom(s => s))
            ;
            Mapper.CreateMap<Channel, ListChannelDto>()
                .ForMember(t => t.ParentChannel, o => o.Condition(s => s.ParentChannel != null && s.ParentChannel.Id != 0))
                ;

            Mapper.CreateMap<UserChannel, UserChannelDto>()
                .ForMember(t => t.Code, o => o.MapFrom(s => s.Channel != null ? s.Channel.Code : string.Empty))
                .ForMember(t => t.Name, o => o.MapFrom(s => s.Channel != null ? s.Channel.Name : string.Empty))
                ;

            Mapper.CreateMap<PagedResults<Channel>, PagedResultDto<ListChannelDto>>();
            Mapper.CreateMap<List<Channel>, ListResultDto<ListChannelDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s))
                ;

            Mapper.CreateMap<Channel, CultureParameter>()
                .ForMember(t => t.DateFormat, o => o.MapFrom(s => s.DateFormat ?? string.Empty))
                .ForMember(t => t.LanguageId, o => o.MapFrom(s => s.Language != null ? s.Language.Id : 2))
                .ForMember(t => t.CurrencyId, o => o.MapFrom(s => s.Currency != null ? s.Currency.Id : 1))
                .ForMember(t => t.CountryId, o => o.MapFrom(s => s.Country != null ? s.Country.Id : 197))
                ;

            Mapper.CreateMap<CreateChannelDto, Channel>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.ActivatedOn, o => o.MapFrom(s => s.IsActive == true ? DateTime.UtcNow : (DateTime?)null))
                ;

            Mapper.CreateMap<EditChannelDto, Channel>()
                .ForMember(t => t.Audit, o => o.Ignore())
                .ForMember(t => t.IsActive, o => o.Ignore())
                .ForMember(t => t.ActivatedOn, o => o.Ignore())
                .ForMember(t => t.Organization, o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    var org = Mapper.Map<int, Organization>(s.OrganizationId);

                    if(org == null)
                        org = new Organization {Id = s.OrganizationId};

                    d.Organization = org;

                });

            Mapper.CreateMap<Channel, ChannelInfoDto>()
                .ForMember(t => t.Id, o => o.MapFrom(s => s.Id))
                .ForMember(t => t.SystemId, o => o.MapFrom(s => s.SystemId))
                .ForMember(t => t.Name, o => o.MapFrom(s => s.Name))
                .ForMember(t => t.Code, o => o.MapFrom(s => s.Code))
                ;
        }
    }
}