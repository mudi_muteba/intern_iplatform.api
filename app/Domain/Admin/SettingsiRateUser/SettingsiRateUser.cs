﻿using AutoMapper;
using Domain.Base;
using Domain.Products;
using Domain.Users;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;

namespace Domain.Admin.SettingsiRateUser
{
    public class SettingsiRateUser : Entity
    {
        public SettingsiRateUser()
        {

        }

        public virtual Product Product { get; set; }
        public virtual User User { get; set; }
        public virtual string RatingUsername { get; set; }
        public virtual string RatingPassword { get; set; }

        public static SettingsiRateUser Create(CreateSettingsiRateUserDto dto)
        {
            var entity = Mapper.Map<SettingsiRateUser>(dto);
            return entity;
        }

        public virtual void Update(EditSettingsiRateUserDto dto)
        {
            Mapper.Map(dto, this);
        }

        public virtual void Disable(DisableSettingsiRateUserDto dto)
        {
            Mapper.Map(dto, this);
            Delete();
        }
    }
}
