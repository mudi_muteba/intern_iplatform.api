﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Base.Repository;
using Domain.Products;
using Domain.Users;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Base;

namespace Domain.Admin.SettingsiRateUser.Mappings
{
    public class SettingsiRateUserMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<SettingsiRateUser, SettingsiRateUserDto>();

            Mapper.CreateMap<EditSettingsiRateUserDto, SettingsiRateUser>()
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product() { Id = s.ProductId }))
                .ForMember(t => t.User, o => o.MapFrom(s => new User() { Id = s.UserId }));

            Mapper.CreateMap<PagedResults<SettingsiRateUser>, PagedResultDto<ListSettingsiRateUserDto>>();

            Mapper.CreateMap<SettingsiRateUser, ListSettingsiRateUserDto>();

            Mapper.CreateMap<CreateSettingsiRateUserDto, SettingsiRateUser>()
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product() { Id = s.ProductId }))
                .ForMember(t => t.User, o => o.MapFrom(s => new User() { Id = s.UserId }));

            Mapper.CreateMap<List<SettingsiRateUser>, ListResultDto<ListSettingsiRateUserDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<SaveSettingsiRateUserDto, CreateSettingsiRateUserDto>();
        }
    }
}