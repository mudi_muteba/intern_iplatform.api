﻿using System.Collections.Generic;

using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiRateUser.Handlers
{
    public class DisableSettingsiRateUserHandler : ExistingEntityDtoHandler<SettingsiRateUser, DisableSettingsiRateUserDto, int>
    {
        public DisableSettingsiRateUserHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void HandleExistingEntityChange(SettingsiRateUser entity, DisableSettingsiRateUserDto dto, HandlerResult<int> result)
        {
            entity.Disable(dto);
            result.Processed(entity.Id);
        }
    }
}