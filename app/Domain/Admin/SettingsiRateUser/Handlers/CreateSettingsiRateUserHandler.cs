﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Users;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiRateUser.Handlers
{
    public class CreateSettingsiRateUserHandler : CreationDtoHandler<SettingsiRateUser, CreateSettingsiRateUserDto, int>
    {
        private IRepository m_Repository;
        public CreateSettingsiRateUserHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
            this.m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void EntitySaved(SettingsiRateUser entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override SettingsiRateUser HandleCreation(CreateSettingsiRateUserDto dto, HandlerResult<int> result)
        {
            var entity = SettingsiRateUser.Create(dto);
            result.Processed(entity.Id);

            return entity;
        }
    }
}