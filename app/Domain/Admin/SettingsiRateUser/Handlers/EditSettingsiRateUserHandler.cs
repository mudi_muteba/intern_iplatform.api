﻿using System.Collections.Generic;
using System.Linq;
using Domain.Admin.SettingsiRates;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiRateUser.Handlers
{
    public class EditSettingsiRateUserHandler : ExistingEntityDtoHandler<SettingsiRateUser, EditSettingsiRateUserDto, int>
    {
        private IRepository m_Repository;

        public EditSettingsiRateUserHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
            m_Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected override void HandleExistingEntityChange(SettingsiRateUser entity, EditSettingsiRateUserDto dto, HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}