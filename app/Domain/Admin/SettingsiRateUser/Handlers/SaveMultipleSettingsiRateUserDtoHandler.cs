﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Products;
using Domain.Users;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using MasterData.Authorisation;
using NHibernate;

namespace Domain.Admin.SettingsiRateUser.Handlers
{
    public class SaveMultipleSettingsiRateUserDtoHandler : BaseDtoHandler<SaveMultipleSettingsiRateUserDto, bool>
    {
        private readonly IRepository m_Repository;
        private ISession m_Session;
        public SaveMultipleSettingsiRateUserDtoHandler(IProvideContext contextProvider, IRepository repository, ISession session)
            : base(contextProvider)
        {
            m_Repository = repository;
            m_Session = session;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }


        protected override void InternalHandle(SaveMultipleSettingsiRateUserDto dto, HandlerResult<bool> result)
        {
            var settings = m_Repository.GetAll<SettingsiRateUser>()
                .Where(x => x.Product.Id == dto.ProductId && !x.IsDeleted).ToList();

            var products = m_Repository.GetAll<Product>().Where(x => !x.IsDeleted).ToList();

            RemoveOldSettings(dto, settings);
            UpdateExistingSettings(dto, settings, products);
            AddNewSettings(dto, settings, products);
            result.Processed(true);
        }

        private void RemoveOldSettings(SaveMultipleSettingsiRateUserDto dto, List<SettingsiRateUser> settings)
        {
            var toBeDeleted = settings.Where(t => dto.SettingsiRateUser.All(y => y.ProductId != t.Product.Id)).ToList();

            foreach (var setting in toBeDeleted)
            {
                setting.Delete();
                m_Repository.Save(setting);
            }
        }
        private void UpdateExistingSettings(SaveMultipleSettingsiRateUserDto dto, List<SettingsiRateUser> settings, List<Product> products)
        {
            var toBeUpdated = settings.Where(t => dto.SettingsiRateUser.Any(y => t.Product.Id == y.ProductId)).ToList();

            foreach (var setting in toBeUpdated)
            {
                var dtosetting = dto.SettingsiRateUser.First(x => x.ProductId.Equals(setting.Product.Id));

                setting.RatingUsername = dtosetting.RatingUsername;
                setting.RatingPassword = dtosetting.RatingPassword;
                setting.Product = products.FirstOrDefault(x => x.Id == dtosetting.ProductId);
                setting.User = new User { Id = dtosetting.UserId };

                m_Repository.Save(setting);
            }
        }

        private void AddNewSettings(SaveMultipleSettingsiRateUserDto dto, List<SettingsiRateUser> settings, List<Product> products)
        {
            var newSettings = dto.SettingsiRateUser.Where(x => settings.All(y => y.Product.Id != x.ProductId)).ToList();

            foreach (var setting in newSettings)
            {
                var createSettingsiRateUser = Mapper.Map<CreateSettingsiRateUserDto>(setting);
                var entity = SettingsiRateUser.Create(createSettingsiRateUser);
                m_Repository.Save(entity);
            }
        }
    }
}