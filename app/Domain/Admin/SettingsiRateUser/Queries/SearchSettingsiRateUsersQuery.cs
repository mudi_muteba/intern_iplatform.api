﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRateUser;
using iPlatform.Api.DTOs.Base;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiRateUser.Queries
{
    public class SearchSettingsiRateUsersQuery : BaseQuery<SettingsiRateUser>, ISearchQuery<SearchSettingsiRateUserDto>
    {
        private SearchSettingsiRateUserDto m_Criteria;

        public SearchSettingsiRateUsersQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultSettingsiRateUserFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithCriteria(SearchSettingsiRateUserDto criteria)
        {
            this.m_Criteria = criteria;
        }

        protected internal override IQueryable<SettingsiRateUser> Execute(IQueryable<SettingsiRateUser> query)
        {

            if (!string.IsNullOrEmpty(m_Criteria.User))
            { query = query.Where(x => x.User.UserName.Contains(m_Criteria.User)); }

            if (!string.IsNullOrEmpty(m_Criteria.Product))
            { query = query.Where(x => x.Product.Name.Contains(m_Criteria.Product)); }

            if (m_Criteria.UserId > 0)
                query = query.Where(x => x.User.Id == m_Criteria.UserId);

            if (m_Criteria.ProductId > 0)
            { query = query.Where(x => x.Product.Id == m_Criteria.ProductId); }

            if (!string.IsNullOrEmpty(m_Criteria.RatingUsername))
            { query = query.Where(x => x.RatingUsername.Contains(m_Criteria.RatingUsername)); }

            if (!string.IsNullOrEmpty(m_Criteria.RatingPassword))
            { query = query.Where(x => x.RatingPassword.Contains(m_Criteria.RatingPassword)); }

            query = AddOrdering(query);

            return query;
        }

        private IQueryable<SettingsiRateUser> AddOrdering(IQueryable<SettingsiRateUser> query)
        {
            query = new OrderByBuilder<SettingsiRateUser>(OrderByDefinition).Build(query, m_Criteria.OrderBy);

            return query;
        }

        private Dictionary<OrderByField, Func<string>> OrderByDefinition
        {
            get
            {
                return new Dictionary<OrderByField, Func<string>>()
                {
                    {new OrderByField("User"), () => "User.Username"},
                    {new OrderByField("Product"), () => "Product.Name"},
                    {new OrderByField("RatingUsername"), () => "RatingUsername"},
                    {new OrderByField("RatingPassword"), () => "RatingPassword"},
                };
            }
        }
    }
}