﻿using System.Collections.Generic;
using System.Linq;

using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiRateUser.Queries
{
    public class GetAllSettingsiRateUsersQuery : BaseQuery<SettingsiRateUser>
    {
        public GetAllSettingsiRateUsersQuery(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository, new DefaultSettingsiRateUserFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    CampaignAuthorisation.List
                };
            }
        }

        protected internal override IQueryable<SettingsiRateUser> Execute(IQueryable<SettingsiRateUser> query)
        {
            query = query.OrderBy(q => q.Product)
                .ThenBy(q => q.User);

            return query;
        }
    }
}