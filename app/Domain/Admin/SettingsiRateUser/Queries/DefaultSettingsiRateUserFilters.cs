﻿using System.Linq;
using Domain.Admin.SettingsiRates;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Admin.SettingsiRateUser.Queries
{
    public class DefaultSettingsiRateUserFilters : IApplyDefaultFilters<SettingsiRateUser>
    {
        public IQueryable<SettingsiRateUser> Apply(ExecutionContext executionContext, IQueryable<SettingsiRateUser> query)
        {
            return query.Where(c => c.IsDeleted == false && c.Product.IsDeleted == false);
        }
    }
}