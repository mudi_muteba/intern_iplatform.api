﻿using Domain.Admin.SettingsiRates;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Admin.SettingsiRateUser.Overrides
{
    public class SettingsiRateUserOverrides : IAutoMappingOverride<SettingsiRateUser>
    {
        public void Override(AutoMapping<SettingsiRateUser> mapping)
        {
            mapping.References(x => x.Product);
            mapping.References(x => x.User);
        }
    }
}
