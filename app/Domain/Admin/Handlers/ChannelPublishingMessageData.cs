﻿using iPlatform.Api.DTOs.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.Communication.Interfaces;

namespace Domain.Admin.Handlers
{
    public class ChannelPublishingMessageData: ICommunicationMessageData
    {
        public ChannelPublishingMessageData() { }

        public ChannelPublishingMessageData(IEnumerable<Publishlog> logs, string firstname, string emailbody, string fromname)
        {
            Logs = logs;
            FirstName = firstname;
            EmailBody = emailbody;
            FromName = fromname;
        }

        public IEnumerable<Publishlog> Logs { get; private set; }
        public string FirstName { get; private set; }
        public string EmailBody { get; private set; }
        public string FromName { get; set; }
    }
}
