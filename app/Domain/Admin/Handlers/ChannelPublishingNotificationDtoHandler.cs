﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Admin.ChannelSettings;
using Domain.Admin.Queries;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Emailing;
using Domain.Emailing.Factory;
using Domain.Individuals.Queries;
using Domain.Organizations.Queries;
using Domain.Reports.Base;
using Domain.Reports.Base.Handlers;
using Domain.Reports.Base.Queries;
using Domain.Reports.Base.StoredProcedures;
using Domain.SignFlow.Handlers;
using Domain.Users;
using iPlatform.Api.DTOs.ContactUs;
using iPlatform.Api.DTOs.Reports.Base;
using iPlatform.Api.DTOs.Admin;
using ToolBox.Communication.Interfaces;
using ToolBox.Templating.Interfaces;
using Workflow.Messages;

namespace Domain.Admin.Handlers
{
    public class ChannelPublishingNotificationDtoHandler : BaseDtoHandler<ChannelPublishingNotificationDto, ReportEmailResponseDto>
    {
        private readonly IRepository repository;
        private readonly ITemplateEngine templateEngine;
        private readonly IProvideContext contextProvider;
        private readonly IExecutionPlan executionPlan;
        
        private readonly GetChannelById channelQuery;

        public ChannelPublishingNotificationDtoHandler(IProvideContext _contextProvider, IRepository _repository, ITemplateEngine _templateEngine, IExecutionPlan _executionPlan) : base(_contextProvider) 
        {
            repository = _repository;
            templateEngine = _templateEngine;
            contextProvider = _contextProvider;
            executionPlan = _executionPlan;
            
            channelQuery = new GetChannelById(contextProvider, repository);
        }

        public override List<MasterData.Authorisation.RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<MasterData.Authorisation.RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(ChannelPublishingNotificationDto dto, HandlerResult<ReportEmailResponseDto> result)
        {
            var channel = repository.GetAll<Channel>().FirstOrDefault(x => x.IsDefault && x.IsDeleted != true);


            if (channel == null || !channel.EmailCommunicationSettings.Any())
                return;

            var emailSettings = Mapper.Map<AppSettingsEmailSetting>(channel.EmailCommunicationSettings.FirstOrDefault());
            var enableInsurerTemplate = ConfigurationManager.AppSettings[@"Reports/Storage"].ToString() != "false";

            var userId = dto.Context.UserId;
            var mailRegards = "iPlatform";
            var usernamePlusName = dto.FromEmail;
            var template = "PublishingChannelTemplate";

            var channelPublishingMessageData = new ChannelPublishingMessageData(dto.Logs, "Team", dto.Body, mailRegards);
            var emailCommunicationMessage = new EmailCommunicationMessage(template, channelPublishingMessageData, dto.Email, dto.Subject, templateEngine, emailSettings);


            if (enableInsurerTemplate)
            {
                var logo = string.Format(@"{0}content\reports\logos\iplatform.png", AppDomain.CurrentDomain.BaseDirectory);

                var footerLogo = new EmbeddedResource();
                footerLogo.Attribute = "footerLogo";
                footerLogo.Value = @"<img src=""cid:{0}"" title=""Logo"" />";
                footerLogo.LinkedResource = new LinkedResource(logo);

                emailCommunicationMessage.EmbeddedResources.Add(footerLogo);
            }
            var message = new MailMessageFactory().Create(emailCommunicationMessage, emailSettings.AddCustomFrom(dto.FromEmail), true, usernamePlusName);

                new SmtpClientAdapter(emailSettings).Send(message);

                result.Processed(new ReportEmailResponseDto
                {
                    Success = true,
                    Body = message.Body
                });
        }
    }
}
