﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Campaigns;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Admin;
using System.Linq;

namespace Domain.Admin.Handlers
{
    public class EditChannelHandler : ExistingEntityDtoHandler<Channel, EditChannelDto, int>
    {
        public EditChannelHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(Channel entity, EditChannelDto dto,
            HandlerResult<int> result)
        {
            //defaulted
            if (dto.IsDefault)
            {
                var oldDefaulted = repository.GetAll<Channel>().Where(x => x.IsDefault == true).FirstOrDefault();

                if (oldDefaulted != null)
                    oldDefaulted.IsDefault = false;
            }

            entity.Update(dto);

            result.Processed(entity.Id);
        }
    }
}