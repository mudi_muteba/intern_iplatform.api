﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Admin;
using System.Linq;
using Domain.Users.Queries;

namespace Domain.Admin.Handlers
{
    public class CreateChannelHandler : CreationDtoHandler<Channel, CreateChannelDto, int>
    {
        public CreateChannelHandler(IProvideContext contextProvider, IRepository repository, FindUserByUserNameQuery userQuery)
            : base(contextProvider, repository)
        {
            this._repository = repository;
            this.userQuery = userQuery;
        }
        private readonly IRepository _repository;
        private readonly FindUserByUserNameQuery userQuery;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                   
                };
            }
        }

        protected override void EntitySaved(Channel entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override Channel HandleCreation(CreateChannelDto dto, HandlerResult<int> result)
        {
            var channel = Channel.Create(dto);

            if(dto.IsDefault)
            {
                var oldDefaulted = _repository.GetAll<Channel>().Where(x => x.IsDefault == true).FirstOrDefault();

                if (oldDefaulted != null)
                oldDefaulted.IsDefault = false;
            }

            result.Processed(channel.Id);
            return channel;
        }
    }
}