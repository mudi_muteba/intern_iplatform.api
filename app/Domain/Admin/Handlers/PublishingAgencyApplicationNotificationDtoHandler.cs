﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Emailing;
using Domain.Emailing.Factory;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Reports.Base;
using ToolBox.Communication.Interfaces;
using ToolBox.Templating.Interfaces;

namespace Domain.Admin.Handlers
{
    public class PublishingAgencyApplicationNotificationDtoHandler : BaseDtoHandler<PublishingAgencyApplicationNotificationDto, ReportEmailResponseDto>
    {
        private readonly IRepository m_Repository;
        private readonly ITemplateEngine m_TemplateEngine;
        private readonly IProvideContext m_ContextProvider;

        public PublishingAgencyApplicationNotificationDtoHandler(IProvideContext contextProvider, IRepository repository, ITemplateEngine templateEngine) : base(contextProvider)
        {
            m_Repository = repository;
            m_TemplateEngine = templateEngine;
            m_ContextProvider = contextProvider;
        }

        public override List<MasterData.Authorisation.RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<MasterData.Authorisation.RequiredAuthorisationPoint> { };
            }
        }

        protected override void InternalHandle(PublishingAgencyApplicationNotificationDto dto, HandlerResult<ReportEmailResponseDto> result)
        {
            var channel = m_Repository.GetAll<Channel>().FirstOrDefault(x => x.IsDefault && x.IsDeleted != true);

            if (channel == null || !channel.EmailCommunicationSettings.Any())
                return;

            var emailSettings = Mapper.Map<AppSettingsEmailSetting>(channel.EmailCommunicationSettings.FirstOrDefault());
            var enableInsurerTemplate = ConfigurationManager.AppSettings[@"Reports/Storage"].ToString() != "false";

            var mailRegards = "iPlatform";
            var usernamePlusName = dto.FromEmail;
            var template = "PublishingAgencyApplication";

            var publishingAgencyApplicationNotification = new PublishingAgencyApplicationNotification(dto.BrokerageData, "Team", dto.Body, mailRegards);
            var emailCommunicationMessage = new EmailCommunicationMessage(template, publishingAgencyApplicationNotification, dto.Email, dto.Subject, m_TemplateEngine, emailSettings);


            if (enableInsurerTemplate)
            {
                var logo = string.Format(@"{0}content\reports\logos\iplatform.png", AppDomain.CurrentDomain.BaseDirectory);

                var footerLogo = new EmbeddedResource();
                footerLogo.Attribute = "footerLogo";
                footerLogo.Value = @"<img src=""cid:{0}"" title=""Logo"" />";
                footerLogo.LinkedResource = new LinkedResource(logo);

                emailCommunicationMessage.EmbeddedResources.Add(footerLogo);
            }
            var message = new MailMessageFactory().Create(emailCommunicationMessage, emailSettings.AddCustomFrom(dto.FromEmail), true, usernamePlusName);

            new SmtpClientAdapter(emailSettings).Send(message);

            result.Processed(new ReportEmailResponseDto
            {
                Success = true,
                Body = message.Body
            });
        }
    }
}

