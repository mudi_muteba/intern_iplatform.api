﻿using iPlatform.Api.DTOs.Admin;
using ToolBox.Communication.Interfaces;

namespace Domain.Admin.Handlers
{
    public class PublishingAgencyApplicationNotification : ICommunicationMessageData
    {
        public PublishingAgencyApplicationNotification() { }

        public PublishingAgencyApplicationNotification(BrokerageData brokerageData, string firstname, string emailbody, string fromname)
        {
            BrokerageData = brokerageData;
            FirstName = firstname;
            EmailBody = emailbody;
            FromName = fromname;
        }

        public BrokerageData BrokerageData { get; set; }
        public string FirstName { get; private set; }
        public string EmailBody { get; private set; }
        public string FromName { get; set; }
    }
}
