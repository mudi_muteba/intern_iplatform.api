﻿using System.Collections.Generic;
using Castle.Core.Internal;
using Domain.Base;
using Domain.Admin.ChannelEvents;
namespace Domain.Admin
{
    public abstract class ChannelReference : EntityWithAudit
    {
        public virtual IList<ChannelEvent> ChannelConfigurations { get; protected internal set; }

        protected ChannelReference()
        {
            ChannelConfigurations = ChannelConfigurations ?? new List<ChannelEvent>();
        }

        public ChannelReference(int id) : this()
        {
            Id = id;
        }

        public override string ToString()
        {
            return string.Format("Channel: {0}", Id);
        }

        public virtual void AddChannelConfiguration(IEnumerable<ChannelEvent> configurations)
        {
            if (configurations != null) configurations.ForEach(x => ChannelConfigurations.Add(x));
        }
    }
}