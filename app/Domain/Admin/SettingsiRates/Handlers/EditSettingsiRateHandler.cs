﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiRates.Handlers
{
    public class EditSettingsiRateHandler : ExistingEntityDtoHandler<SettingsiRate, EditSettingsiRateDto, int>
    {
        private IRepository _Repository;

        public EditSettingsiRateHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
            this._Repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(SettingsiRate entity, EditSettingsiRateDto dto, HandlerResult<int> result)
        {
            Channel channel = _Repository.GetAll<Channel>()
                                    .FirstOrDefault(c => c.Id == dto.ChannelId);
            entity.Update(dto, channel);
            result.Processed(entity.Id);
        }
    }
}