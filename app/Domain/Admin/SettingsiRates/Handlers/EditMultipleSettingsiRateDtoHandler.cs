﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiRates.Handlers
{
    public class EditMultipleSettingsiRateDtoHandler : ExistingEntityDtoHandler<SettingsiRate, EditMultipleSettingsiRateDto, bool>
    {
        private IRepository _repository;

        public EditMultipleSettingsiRateDtoHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
            this._repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(SettingsiRate entity, EditMultipleSettingsiRateDto dto, HandlerResult<bool> result)
        {
            // Perform differencial delete
            var settingsiRates = new List<SettingsiRate>();

            if (dto != null)
            {
                if (dto.EditSettingsiRateDtos.FirstOrDefault(c => c.ChannelId > 0) != null)
                {
                    var channelId = dto.EditSettingsiRateDtos[1].ChannelId;
                    settingsiRates =
                        _repository.GetAll<SettingsiRate>()
                            .ToList()
                            .Where(c => c.Channel.Id == channelId)
                            .ToList();

                    var productIdList = new List<int>();
                    foreach (var item in dto.EditSettingsiRateDtos)
                    {
                        productIdList.Add(item.ProductId);

                        var settingsiRateToDelete = from c in settingsiRates where c.Product.Id.Equals(productIdList) select c ;
                    }
                }
            }

            // Update remaining
        }
    }
}