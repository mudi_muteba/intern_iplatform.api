﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiRates.Handlers
{
    public class CreateSettingsiRateHandler : CreationDtoHandler<SettingsiRate, CreateSettingsiRateDto, int>
    {
        private IRepository repository;
        public CreateSettingsiRateHandler(IProvideContext contextProvider, IRepository repository): base(contextProvider, repository)
        {
            this.repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(SettingsiRate entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override SettingsiRate HandleCreation(CreateSettingsiRateDto dto, HandlerResult<int> result)
        {
            Channel channel = repository.GetAll<Channel>()
                .FirstOrDefault(c => c.Id == dto.ChannelId);

            var entity = SettingsiRate.Create(dto, channel);
            result.Processed(entity.Id);

            return entity;
        }
    }
}