﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiRates.Handlers
{
    public class DisableSettingsiRateHandler : ExistingEntityDtoHandler<SettingsiRate, DisableSettingsiRateDto, int>
    {
        public DisableSettingsiRateHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(SettingsiRate entity, DisableSettingsiRateDto dto, HandlerResult<int> result)
        {
            entity.Disable(dto);
            result.Processed(entity.Id);
        }
    }
}