﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using MasterData.Authorisation;
using NHibernate;

namespace Domain.Admin.SettingsiRates.Handlers
{
    public class SaveMultipleSettingsiRateDtoHandler : BaseDtoHandler<SaveMultipleSettingsiRateDto, bool>
    {
        private IRepository _repository;
        private ISession _session;
        public SaveMultipleSettingsiRateDtoHandler(IProvideContext contextProvider, IRepository repository, ISession session)
            : base(contextProvider)
        {
            _repository = repository;
            _session = session;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }


        protected override void InternalHandle(SaveMultipleSettingsiRateDto dto, HandlerResult<bool> result)
        {
            var settings = _repository.GetAll<SettingsiRate>()
                .Where(x => x.Channel.Id == dto.ChannelId && !x.IsDeleted).ToList();

            var channels = _repository.GetAll<Channel>().Where(x => !x.IsDeleted).ToList();

            RemoveOldSettings(dto, settings);
            UpdateExistingSettings(dto, settings, channels);
            AddNewSettings(dto, settings, channels);
            result.Processed(true);
        }

        private void RemoveOldSettings(SaveMultipleSettingsiRateDto dto, List<SettingsiRate> settings)
        {
            var toBeDeleted = settings.Where(t => dto.SettingsiRate.All(y => y.ProductId != t.Product.Id)).ToList();

            foreach (var setting in toBeDeleted)
            {
                setting.Delete();
                _repository.Save(setting);
            }
        }
        private void UpdateExistingSettings(SaveMultipleSettingsiRateDto dto, List<SettingsiRate> settings, List<Channel> channels)
        {
            var toBeUpdated = settings.Where(t => dto.SettingsiRate.Any(y => t.Product.Id == y.ProductId)).ToList();

            foreach (var setting in toBeUpdated)
            {
                var dtosetting = dto.SettingsiRate.First(x => x.ProductId.Equals(setting.Product.Id));

                setting.Password = dtosetting.Password;
                setting.AgentCode = dtosetting.AgentCode;
                setting.BrokerCode = dtosetting.BrokerCode;
                setting.AuthCode = dtosetting.AuthCode;
                setting.SchemeCode = dtosetting.SchemeCode;
                setting.Token = dtosetting.Token;
                setting.Environment = dtosetting.Environment;
                setting.UserId = dtosetting.UserId;
                setting.SubscriberCode = dtosetting.SubscriberCode;
                setting.CompanyCode = dtosetting.CompanyCode;
                setting.UwCompanyCode = dtosetting.UwCompanyCode;
                setting.UwProductCode = dtosetting.UwProductCode;
                setting.ProductCode = dtosetting.ProductCode;
                setting.Product = new Product {Id = dtosetting.ProductId};
                setting.Channel = channels.FirstOrDefault(x => x.Id == dtosetting.ChannelId);
                setting.ChannelSystemId = channels.FirstOrDefault(x => x.Id == dtosetting.ChannelId).SystemId;

                _repository.Save(setting);
            }
        }

        private void AddNewSettings(SaveMultipleSettingsiRateDto dto, List<SettingsiRate> settings, List<Channel> channels)
        {
            var newSettings = dto.SettingsiRate.Where(x => settings.All(y => y.Product.Id != x.ProductId)).ToList();
            var channel = channels.FirstOrDefault(x => !x.IsDeleted && x.Id == dto.ChannelId);

            foreach (var setting in newSettings)
            {
                var createSettingsiRate = Mapper.Map<CreateSettingsiRateDto>(setting);
                var entity = SettingsiRate.Create(createSettingsiRate, channel);
                _repository.Save(entity);
            }
        }
    }
}