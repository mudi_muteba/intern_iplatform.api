﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiRates.Handlers
{
    public class CreateMultipleSettingsiRateDtoHandler : BaseDtoHandler<CreateMultipleSettingsiRateDto, bool>
    {
        public CreateMultipleSettingsiRateDtoHandler(IProvideContext contextProvider, IRepository repository, IExecutionPlan executionPlan) : base(contextProvider)
        {
            Repository = repository;
            ExecutionPlan = executionPlan;
        }

        private IRepository Repository { get; set; }

        private IExecutionPlan ExecutionPlan { get; set; }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void InternalHandle(CreateMultipleSettingsiRateDto dto, HandlerResult<bool> result)
        {
            try
            {
                foreach (var item in dto.CreateSettingsiRateDtos)
                {
                    ExecutionPlan.Execute<CreateSettingsiRateDto, int>(item);
                }

                result.Processed(true);
            }
            catch
            {
                result.Processed(false);
            }
        }
    }
}