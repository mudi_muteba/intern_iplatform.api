﻿using Domain.Base;
using Domain.Products;
using System;
using AutoMapper;
using iPlatform.Api.DTOs.Admin.SettingsiRates;

namespace Domain.Admin.SettingsiRates
{
    public class SettingsiRate: Entity
    {
        public SettingsiRate()
        {

        }

        public virtual Product Product { get; set; }
        public virtual string Password { get; set; }
        public virtual string AgentCode { get; set; }
        public virtual string BrokerCode { get; set; }
        public virtual string AuthCode { get; set; }
        public virtual string SchemeCode { get; set; }
        public virtual string Token { get; set; }
        public virtual Channel Channel { get; set; }
        public virtual Guid ChannelSystemId { get; set; }
        public virtual string Environment { get; set; }
        public virtual string UserId { get; set; }
        public virtual string SubscriberCode { get; set; }
        public virtual string CompanyCode { get; set; }
        public virtual string UwCompanyCode { get; set; }
        public virtual string UwProductCode { get; set; }
        public virtual string ProductCode { get; set; }

        public static SettingsiRate Create(CreateSettingsiRateDto dto, Channel channel)
        {
            var entity = Mapper.Map<SettingsiRate>(dto);
            entity.Channel = channel;
            entity.ChannelSystemId = channel.SystemId;
            return entity;
        }

        public virtual void Update(EditSettingsiRateDto dto, Channel channel)
        {
            Mapper.Map(dto, this);
            Channel = channel;
            ChannelSystemId = channel.SystemId;
        }

        public virtual void Disable(DisableSettingsiRateDto dto)
        {
            Delete();
        }
    }
}
