﻿using AutoMapper;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base;
using System.Collections.Generic;

namespace Domain.Admin.SettingsiRates.Mappings
{
    public class SettingsiRateMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<SettingsiRate, SettingsiRateDto>();

            Mapper.CreateMap<EditSettingsiRateDto, SettingsiRate>()
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product() { Id = s.ProductId }))
                .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel() { Id = s.ChannelId }));

            Mapper.CreateMap<PagedResults<SettingsiRate>, PagedResultDto<ListSettingsiRateDto>>();

            Mapper.CreateMap<SettingsiRate, ListSettingsiRateDto>();

            Mapper.CreateMap<CreateSettingsiRateDto, SettingsiRate>()
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product() { Id = s.ProductId }))
                .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel() { Id = s.ChannelId }));

            Mapper.CreateMap<List<SettingsiRate>, ListResultDto<ListSettingsiRateDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));

            Mapper.CreateMap<SaveSettingsiRateDto, CreateSettingsiRateDto>();
        }
    }
}