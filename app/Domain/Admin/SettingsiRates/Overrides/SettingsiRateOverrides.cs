﻿using System;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace Domain.Admin.SettingsiRates.Overrides
{
    public class SettingsiRateOverrides : IAutoMappingOverride<SettingsiRate>
    {
        public void Override(AutoMapping<SettingsiRate> mapping)
        {
            mapping.References(x => x.Product);
            mapping.References(x => x.Channel);
        }
    }
}
