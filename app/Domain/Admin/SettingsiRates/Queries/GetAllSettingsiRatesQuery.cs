﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.SettingsiRates.Queries
{
    public class GetAllSettingsiRatesQuery : BaseQuery<SettingsiRate>
    {
        public GetAllSettingsiRatesQuery(IProvideContext contextProvider, IRepository repository) 
            : base(contextProvider, repository, new DefaultSettingsiRateFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                    CampaignAuthorisation.List
                };
            }
        }

        protected internal override IQueryable<SettingsiRate> Execute(IQueryable<SettingsiRate> query)
        {
            query = query.OrderBy(q => q.Product)
                .ThenBy(q => q.Channel);

            return query;
        }
    }
}