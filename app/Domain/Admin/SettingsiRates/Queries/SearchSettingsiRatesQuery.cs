﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Admin.SettingsiRates;
using iPlatform.Api.DTOs.Base;
using System;

namespace Domain.Admin.SettingsiRates.Queries
{
    public class SearchSettingsiRatesQuery : BaseQuery<SettingsiRate>, ISearchQuery<SearchSettingsiRateDto>
    {
        private SearchSettingsiRateDto criteria;

        public SearchSettingsiRatesQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultSettingsiRateFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithCriteria(SearchSettingsiRateDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<SettingsiRate> Execute(IQueryable<SettingsiRate> query)
        {
            if (!string.IsNullOrEmpty(criteria.SchemeCode))
                query = query.Where(x => x.SchemeCode.StartsWith(criteria.SchemeCode));

            if (!string.IsNullOrEmpty(criteria.BrokerCode))
                query = query.Where(x => x.BrokerCode.StartsWith(criteria.BrokerCode));

            if (!string.IsNullOrEmpty(criteria.AgentCode))
                query = query.Where(x => x.AgentCode.StartsWith(criteria.AgentCode));

            if (!string.IsNullOrEmpty(criteria.SubscriberCode))
                query = query.Where(x => x.SubscriberCode.StartsWith(criteria.SubscriberCode));

            if (!string.IsNullOrEmpty(criteria.Channel))
                query = query.Where(x => x.Channel.Name.StartsWith(criteria.Channel));

            if (!string.IsNullOrEmpty(criteria.Product))
                query = query.Where(x => x.Product.Name.StartsWith(criteria.Product));

            if (!string.IsNullOrEmpty(criteria.Environment))
                query = query.Where(x => x.Environment.StartsWith(criteria.Environment));

            if (criteria.ChannelId > 0)
                query = query.Where(x => x.Channel.Id == criteria.ChannelId);

            if (criteria.ProductId > 0)
                query = query.Where(x => x.Product.Id == criteria.ProductId);

            if (criteria.ChannelSystemId != Guid.Empty)
                query = query.Where(x => x.Channel.SystemId == criteria.ChannelSystemId);

            query = AddOrdering(query);

            return query;
        }

        private IQueryable<SettingsiRate> AddOrdering(IQueryable<SettingsiRate> query)
        {
            query = new OrderByBuilder<SettingsiRate>(OrderByDefinition).Build(query, criteria.OrderBy);

            return query;
        }

        public Dictionary<OrderByField, Func<string>> OrderByDefinition
        {
            get
            {
                return new Dictionary<OrderByField, Func<string>>()
                {
                    {new OrderByField("SchemeCode"), () => "SchemeCode"},
                    {new OrderByField("BrokerCode"), () => "BrokerCode"},
                    {new OrderByField("AgentCode"), () => "AgentCode"},
                    {new OrderByField("SubscriberCode"), () => "SubscriberCode"},
                    {new OrderByField("Channel"), () => "Channel.Name"},
                    {new OrderByField("Product"), () => "Product.Name"},
                    {new OrderByField("Environment"), () => "Environment"},
                };
            }
        }
    }
}