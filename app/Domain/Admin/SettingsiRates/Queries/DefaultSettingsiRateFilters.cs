﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Admin.SettingsiRates.Queries
{
    public class DefaultSettingsiRateFilters : IApplyDefaultFilters<SettingsiRate>
    {
        public IQueryable<SettingsiRate> Apply(ExecutionContext executionContext, IQueryable<SettingsiRate> query)
        {
            return query.Where(c => c.IsDeleted == false && c.Product.IsDeleted == false);
        }
    }
}