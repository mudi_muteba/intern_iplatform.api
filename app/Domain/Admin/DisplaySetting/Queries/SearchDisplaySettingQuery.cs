﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using iPlatform.Api.DTOs.Base;
using MasterData.Authorisation;

namespace Domain.Admin.DisplaySetting.Queries
{
    public class SearchDisplaySettingQuery : BaseQuery<DisplaySetting>, ISearchQuery<SearchDisplaySettingDto>
    {
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        private SearchDisplaySettingDto _SearchDisplaySettingDto;

        public SearchDisplaySettingQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultDisplaySettingFilter())
        {
        }

        public void WithCriteria(SearchDisplaySettingDto criteria)
        {
            _SearchDisplaySettingDto = criteria;
        }

        protected internal override IQueryable<DisplaySetting> Execute(IQueryable<DisplaySetting> query)
        {
            if (!String.IsNullOrEmpty(_SearchDisplaySettingDto.ChannelName))
                query = query.Where(q => q.Channel.Name.Contains(_SearchDisplaySettingDto.ChannelName));
            if (!String.IsNullOrEmpty(_SearchDisplaySettingDto.ProductName))
                query = query.Where(q => q.Product.Name.Contains(_SearchDisplaySettingDto.ProductName));

            query = AddOrdering(query);

            return query;
        }

        private IQueryable<DisplaySetting> AddOrdering(IQueryable<DisplaySetting> query)
        {
            query = new OrderByBuilder<DisplaySetting>(OrderByDefinition).Build(query, _SearchDisplaySettingDto.OrderBy);

            return query;
        }

        public Dictionary<OrderByField, Func<string>> OrderByDefinition
        {
            get
            {
                return new Dictionary<OrderByField, Func<string>>()
                {
                    {new OrderByField("ChannelName"), () => "Channel.Name"},
                    {new OrderByField("ProductName"), () => "Product.Name"}
                };
            }
        }
    }
}
