﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;

namespace Domain.Admin.DisplaySetting.Queries
{
    public class GetAllDisplaySettingsQuery : BaseQuery<DisplaySetting>
    {
        public GetAllDisplaySettingsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultDisplaySettingFilter())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        protected internal override IQueryable<DisplaySetting> Execute(IQueryable<DisplaySetting> query)
        {
            query = query.OrderBy(q => q.Channel.Name)
                .ThenBy(q => q.Product.Name);

            return query;
        }
    }
}
