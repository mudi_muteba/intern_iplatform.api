﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;

namespace Domain.Admin.DisplaySetting.Queries
{
    public class DefaultDisplaySettingFilter : IApplyDefaultFilters<DisplaySetting>
    {
        public IQueryable<DisplaySetting> Apply(ExecutionContext executionContext, IQueryable<DisplaySetting> query)
        {
            return query.Where(q => !q.IsDeleted);
        }
    }
}
