﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using Domain.Individuals;
using Domain.Policies;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using iPlatform.Api.DTOs.Individual;
using MasterData.Authorisation;

namespace Domain.Admin.DisplaySetting.Queries
{
    public class RequiresThirdPartyQuery : BaseQuery<DisplaySetting>
    {
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        private readonly IRepository _Repository;
        private int _PartyId;
        private int _ChannelId;

        public RequiresThirdPartyQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultDisplaySettingFilter())
        {
            _Repository = repository;
        }

        public void WithPartyIdAndChannelId(int partyId, int channelId)
        {
            _PartyId = partyId;
            _ChannelId = channelId;
        }

        protected internal override IQueryable<DisplaySetting> Execute(IQueryable<DisplaySetting> query)
        {
            List<PolicyHeader> policyHeaderList = _Repository.GetAll<PolicyHeader>().Where(ph => ph.Party.Id == _PartyId).ToList();
            int[] productIds = policyHeaderList.Select(ph => ph.Product.Id).ToArray();
            
            query = query.Where(q => productIds.Contains(q.Product.Id) &&
                                     q.Channel.Id == _ChannelId &&
                                     q.ThirdPartyDisplay);

            return query;
        }
    }
}
