﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base.Repository;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using iPlatform.Api.DTOs.Base;

namespace Domain.Admin.DisplaySetting.Mappings
{
    public class DisplaySettingMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateDisplaySettingDto, DisplaySetting>()
                .ForMember(d => d.Channel, o => o.MapFrom(s => new Channel() { Id = s.ChannelId }))
                .ForMember(d => d.Product, o => o.MapFrom(s => new Product() { Id = s.ProductId }));

            Mapper.CreateMap<EditDisplaySettingDto, DisplaySetting>()
                .ForMember(d => d.Channel, o => o.MapFrom(s => new Channel() { Id = s.ChannelId }))
                .ForMember(d => d.Product, o => o.MapFrom(s => new Product() { Id = s.ProductId }));

            Mapper.CreateMap<DisplaySetting, DisplaySettingDto>();

            Mapper.CreateMap<DisplaySetting, ListDisplaySettingDto>();

            Mapper.CreateMap<IEnumerable<DisplaySetting>, ListResultDto<ListDisplaySettingDto>>()
                .ForMember(d => d.Results, o => o.MapFrom(s => s.ToList()));

            Mapper.CreateMap<PagedResults<DisplaySetting>, PagedResultDto<ListDisplaySettingDto>>();
        }
    }
}
