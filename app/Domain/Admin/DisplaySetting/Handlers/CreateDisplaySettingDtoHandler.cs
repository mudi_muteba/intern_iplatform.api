﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using MasterData.Authorisation;

namespace Domain.Admin.DisplaySetting.Handlers
{
    public class CreateDisplaySettingDtoHandler : CreationDtoHandler<DisplaySetting, CreateDisplaySettingDto, int>
    {
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public CreateDisplaySettingDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        protected override void EntitySaved(DisplaySetting entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override DisplaySetting HandleCreation(CreateDisplaySettingDto dto, HandlerResult<int> result)
        {
            DisplaySetting displaySetting = DisplaySetting.Create(dto);
            result.Processed(displaySetting.Id);
            return displaySetting;
        }
    }
}
