﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.DisplaySetting;
using MasterData.Authorisation;

namespace Domain.Admin.DisplaySetting.Handlers
{
    public class EditDisplaySettingDtoHandler : ExistingEntityDtoHandler<DisplaySetting, EditDisplaySettingDto, int>
    {
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>();
            }
        }

        public EditDisplaySettingDtoHandler(IProvideContext contextProvider, IRepository repository) : base(contextProvider, repository)
        {
        }

        protected override void HandleExistingEntityChange(DisplaySetting entity, EditDisplaySettingDto dto, HandlerResult<int> result)
        {
            entity.Update(dto);
            result.Processed(entity.Id);
        }
    }
}
