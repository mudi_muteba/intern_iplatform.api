﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Base;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.DisplaySetting;

namespace Domain.Admin.DisplaySetting
{
    public class DisplaySetting : Entity
    {
        public virtual Channel Channel { get; set; }

        public virtual Product Product { get; set; }

        public virtual bool ThirdPartyDisplay { get; set; }

        public virtual string PolicyEndPointUrl { get; set; }

        public virtual string PolicyEndPointUserName { get; set; }
               
        public virtual string PolicyEndPointPassword { get; set; }

        public virtual string PolicyScheduleEndPointUrl { get; set; }

        public virtual string PolicyScheduleEndPointUserName { get; set; }

        public virtual string PolicyScheduleEndPointPassword { get; set; }

        public virtual string ClaimEndPointUrl { get; set; }

        public virtual string ClaimEndPointUserName { get; set; }

        public virtual string ClaimEndPointPassword { get; set; }

        public virtual string FinanceEndPointUrl { get; set; }

        public virtual string FinanceEndPointUserName { get; set; }

        public virtual string FinanceEndPointPassword { get; set; }

        public static DisplaySetting Create(CreateDisplaySettingDto createDisplaySettingDto)
        {
            DisplaySetting displaySetting = Mapper.Map<DisplaySetting>(createDisplaySettingDto);
            return displaySetting;
        }

        public virtual void Update(EditDisplaySettingDto editDisplaySettingDto)
        {
            Mapper.Map(editDisplaySettingDto, this);
        }

        public virtual void Delete(DeleteDisplaySettingDto deleteDisplaySettingDto)
        {
            Delete();
        }
    }
}
