﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using System;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;

namespace Domain.Admin.SettingsQuoteUploads.Queries
{
    public class SearchSettingsQuoteUploadQuery : BaseQuery<SettingsQuoteUpload>, ISearchQuery<SearchSettingsQuoteUploadDto>
    {
        private SearchSettingsQuoteUploadDto criteria;

        public SearchSettingsQuoteUploadQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultSettingsQuoteUploadFilters())
        {
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {

                };
            }
        }

        public void WithCriteria(SearchSettingsQuoteUploadDto criteria)
        {
            this.criteria = criteria;
        }

        protected internal override IQueryable<SettingsQuoteUpload> Execute(IQueryable<SettingsQuoteUpload> query)
        {
            if (!string.IsNullOrEmpty(criteria.SettingName))
                query = query.Where(x => x.SettingName.StartsWith(criteria.SettingName));

            if (!string.IsNullOrEmpty(criteria.Channel))
                query = query.Where(x => x.Channel.Name.StartsWith(criteria.Channel));

            if (!string.IsNullOrEmpty(criteria.Product))
                query = query.Where(x => x.Product.Name.StartsWith(criteria.Product));

            if (!string.IsNullOrEmpty(criteria.Environment))
                query = query.Where(x => x.Environment.StartsWith(criteria.Environment));

            if (criteria.ChannelId > 0)
                query = query.Where(x => x.Channel.Id == criteria.ChannelId);

            if (criteria.ProductId > 0)
                query = query.Where(x => x.Product.Id == criteria.ProductId);

            if (criteria.ChannelSystemId != Guid.Empty)
                query = query.Where(x => x.Channel.SystemId == criteria.ChannelSystemId);

            return query;
        }
    }
}