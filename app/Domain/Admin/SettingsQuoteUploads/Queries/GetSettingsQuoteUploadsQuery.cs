﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using System;

namespace Domain.Admin.SettingsQuoteUploads.Queries
{
    public class GetSettingsQuoteUploadsQuery : BaseQuery<SettingsQuoteUpload>
    {
        public GetSettingsQuoteUploadsQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultSettingsQuoteUploadFilters())
        {
        }

        private int channelId;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }
        public GetSettingsQuoteUploadsQuery WithChannelId(int channelId)
        {
            this.channelId = channelId;
            return this;
        }

        protected internal override IQueryable<SettingsQuoteUpload> Execute(IQueryable<SettingsQuoteUpload> query)
        {
            return query.Where(c => c.Channel.Id == channelId);
        }
    }
}