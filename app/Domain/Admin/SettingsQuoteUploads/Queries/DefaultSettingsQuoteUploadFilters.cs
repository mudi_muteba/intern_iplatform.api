﻿using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Admin.SettingsQuoteUploads;

namespace Domain.Admin.SettingsQuoteUploads.Queries
{
    public class DefaultSettingsQuoteUploadFilters : IApplyDefaultFilters<SettingsQuoteUpload>
    {
        public IQueryable<SettingsQuoteUpload> Apply(ExecutionContext executionContext, IQueryable<SettingsQuoteUpload> query)
        {
            return query
                .Where(c => c.IsDeleted == false);
        }
    }
}