﻿using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Queries;
using Domain.Base.Repository;
using MasterData.Authorisation;
using System;

namespace Domain.Admin.SettingsQuoteUploads.Queries
{
    public class GetSettingsQuoteAcceptanceQuery : BaseQuery<SettingsQuoteUpload>
    {
        public GetSettingsQuoteAcceptanceQuery(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository, new DefaultSettingsQuoteUploadFilters())
        {
        }

        private int m_ChannelId;
        private int m_ProductId;
        private string m_Environment;
        private bool m_isPolicyBinding;
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }
        public GetSettingsQuoteAcceptanceQuery WithChannelId(int channelId)
        {
            this.m_ChannelId = channelId;
            return this;
        }

        public GetSettingsQuoteAcceptanceQuery WithProductId(int productId)
        {
            this.m_ProductId = productId;
            return this;
        }

        public GetSettingsQuoteAcceptanceQuery WithEnvironment(string environment)
        {
            this.m_Environment = environment;
            return this;
        }

        public GetSettingsQuoteAcceptanceQuery IsPolicyBinding(bool ispolicyBinding)
        {
            this.m_isPolicyBinding = ispolicyBinding;
            return this;
        }

        protected internal override IQueryable<SettingsQuoteUpload> Execute(IQueryable<SettingsQuoteUpload> query)
        {
            if(m_ChannelId > 0)
            {
                query = query.Where(c => c.Channel.Id == m_ChannelId);
            }
                

            if (m_ProductId > 0)
            {
                query = query.Where(c => c.Product.Id == m_ProductId);
            }
                

            if (!string.IsNullOrEmpty(m_Environment))
            {
                query = query.Where(c => c.Environment.ToUpper() == m_Environment.ToUpper());
            }


            query = query.Where(c => c.IsPolicyBinding == m_isPolicyBinding);

            return query;
        }
    }
}