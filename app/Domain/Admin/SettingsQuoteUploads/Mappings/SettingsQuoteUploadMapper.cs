﻿using AutoMapper;
using System.Linq;
using Domain.Admin.SettingsQuoteUploads;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using Domain.Products;
using System.Collections.Generic;
using iPlatform.Api.DTOs.Base;

namespace Domain.Admin.Mappings
{
    public class SettingsQuoteUploadMapper : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<SettingsQuoteUpload, SettingsQuoteUploadDto>();

            Mapper.CreateMap<CreateSettingsQuoteUploadDto, SettingsQuoteUpload>()
                .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel { Id = s.ChannelId}))
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product { Id = s.ProductId }))
                .ForMember(t => t.IsDeleted, o => o.MapFrom(s => false))
                ;

            Mapper.CreateMap<EditSettingsQuoteUploadDto, SettingsQuoteUpload>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Channel, o => o.MapFrom(s => new Channel { Id = s.ChannelId }))
                .ForMember(t => t.Product, o => o.MapFrom(s => new Product { Id = s.ProductId }))
                ;

            Mapper.CreateMap<EditSettingsQuoteUploadDetailDto, SettingsQuoteUpload>()
                .ForMember(t => t.Id, o => o.Ignore())
                .ForMember(t => t.IsDeleted, o => o.Ignore())
                .ForMember(t => t.Channel, o => o.Ignore())
                .ForMember(t => t.Product, o => o.Ignore())
                .ForMember(t => t.Environment, o => o.Ignore())
                ;

            Mapper.CreateMap<List<SettingsQuoteUpload>, ListResultDto<SettingsQuoteUploadDto>>()
                .ForMember(t => t.Results, o => o.MapFrom(s => s));
        }
    }
}