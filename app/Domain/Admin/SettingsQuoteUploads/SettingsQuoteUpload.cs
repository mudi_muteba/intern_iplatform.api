﻿using AutoMapper;
using Domain.Base;
using Domain.Products;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using System;

namespace Domain.Admin.SettingsQuoteUploads
{
    public class SettingsQuoteUpload : Entity
    {
        public SettingsQuoteUpload()
        {
            IsDeleted = false;
        }
        public virtual Product Product { get; set; }
        public virtual string SettingName { get; set; }
        public virtual string SettingValue { get; set; }
        public virtual Channel Channel { get; set; }
        public virtual string Environment { get; set; }
        public virtual bool IsPolicyBinding { get; set; }

        public static SettingsQuoteUpload Create(CreateSettingsQuoteUploadDto dto)
        {
            var entity = Mapper.Map<SettingsQuoteUpload>(dto);
            return entity;
        }

        public static SettingsQuoteUpload Create(int channelId, int productId, string environment, string settingName, string settingValue)
        {
            var entity = new SettingsQuoteUpload
            {
                Channel = new Channel { Id = channelId },
                Environment = environment,
                Product = new Product { Id = productId },
                SettingName = settingName,
                SettingValue = settingValue
            };
            return entity;
        }

        public virtual void Update(EditSettingsQuoteUploadDto dto)
        {
            Mapper.Map(dto, this);
        }

        public virtual void Update(int channelId, int productId, string environment, string settingName, string settingValue)
        {
            Channel = new Channel { Id = channelId };
            Product = new Product { Id = productId };
            Environment = environment;
            SettingName = settingName;
            SettingValue = settingValue;
        }
    }
}
