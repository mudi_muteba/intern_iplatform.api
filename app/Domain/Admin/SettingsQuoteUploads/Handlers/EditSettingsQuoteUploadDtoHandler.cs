﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using Domain.Admin.SettingsQuoteUploads;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;

namespace Domain.SettingsQuoteUploads.Handlers
{
    public class EditSettingsQuoteUploadDtoHandler : ExistingEntityDtoHandler<SettingsQuoteUpload, EditSettingsQuoteUploadDto, int>
    {
        private IRepository _repository;

        public EditSettingsQuoteUploadDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            _repository = repository;
        }

        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>()
                {
                };
            }
        }

        protected override void HandleExistingEntityChange(SettingsQuoteUpload entity, EditSettingsQuoteUploadDto dto, HandlerResult<int> result)
        {

                entity.Update(dto);
                result.Processed(entity.Id);
        }
    }
}