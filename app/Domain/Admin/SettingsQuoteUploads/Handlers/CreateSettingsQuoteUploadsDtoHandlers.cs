﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;
using MasterData.Authorisation;
using NHibernate;

namespace Domain.Admin.SettingsQuoteUploads.Handlers
{
    public class CreateSettingsQuoteUploadsDtoHandler : BaseDtoHandler<SaveSettingsQuoteUploadsDto, bool>
    {
        private IRepository m_Repository;
        private ISession _session;
        public CreateSettingsQuoteUploadsDtoHandler(IProvideContext contextProvider, IRepository repository, ISession session)
            : base(contextProvider)
        {
            m_Repository = repository;
            _session = session;
        }


        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }


        protected override void InternalHandle(SaveSettingsQuoteUploadsDto dto, HandlerResult<bool> result)
        {
            var settings = m_Repository.GetAll<SettingsQuoteUpload>()
                .Where(x => x.Channel.Id == dto.ChannelId && !x.IsDeleted && x.Environment.ToUpper() == dto.Environment.ToUpper() && x.IsPolicyBinding == dto.IsPolicyBinding).ToList();

            //delete Settings that have been removed from list
            RemoveOldSettings(dto, settings);

            //Update existings settings
            UpdateExistingSettings(dto, settings);

            //Add new records
            AddNewSettings(dto, settings);
            result.Processed(true);
        }

        private void RemoveOldSettings(SaveSettingsQuoteUploadsDto dto, List<SettingsQuoteUpload> settings)
        {
            var toBeDeleted = settings.Where(t => !dto.Settings.Any(y => y.SettingName.Equals(t.SettingName, StringComparison.InvariantCultureIgnoreCase) && y.ProductId == t.Product.Id && t.IsPolicyBinding == dto.IsPolicyBinding)).ToList();

            foreach (var setting in toBeDeleted)
            {
                setting.Delete();
                m_Repository.Save(setting);
            }
        }
        private void UpdateExistingSettings(SaveSettingsQuoteUploadsDto dto, List<SettingsQuoteUpload> settings)
        {
            var toBeUpdated = settings.Where(t => dto.Settings.Any(y => y.SettingName.Equals(t.SettingName, StringComparison.InvariantCultureIgnoreCase) && t.Product.Id == y.ProductId && t.IsPolicyBinding == dto.IsPolicyBinding)).ToList();

            foreach (var setting in toBeUpdated)
            {
                var dtosetting = dto.Settings.First(x => x.SettingName.Equals(setting.SettingName, StringComparison.InvariantCultureIgnoreCase));
                setting.SettingValue = dtosetting.SettingValue;
                setting.IsPolicyBinding = dto.IsPolicyBinding;

                m_Repository.Save(setting);
            }
        }

        private void AddNewSettings(SaveSettingsQuoteUploadsDto dto, List<SettingsQuoteUpload> settings)
        {
            var newSettings = dto.Settings.Where(x => !settings.Any(y => y.SettingName.Equals(x.SettingName, StringComparison.InvariantCultureIgnoreCase) && y.Product.Id == x.ProductId && y.IsPolicyBinding == dto.IsPolicyBinding)).ToList();

            foreach (var setting in newSettings)
            {
                var entity = SettingsQuoteUpload.Create(dto.ChannelId, setting.ProductId, dto.Environment, setting.SettingName, setting.SettingValue);
                entity.IsPolicyBinding = dto.IsPolicyBinding;
                m_Repository.Save(entity);
            }
        }
    }
}