﻿using System.Collections.Generic;
using Domain.Base.Execution;
using Domain.Base.Handlers;
using Domain.Base.Repository;
using MasterData.Authorisation;
using iPlatform.Api.DTOs.Admin.SettingsQuoteUploads;

namespace Domain.Admin.SettingsQuoteUploads.Handlers
{
    public class CreateSettingsQuoteUploadDtoHandler : CreationDtoHandler<SettingsQuoteUpload, CreateSettingsQuoteUploadDto, int>
    {
        private IRepository repository;
        public CreateSettingsQuoteUploadDtoHandler(IProvideContext contextProvider, IRepository repository)
            : base(contextProvider, repository)
        {
            this.repository = repository;
        }

        
        public override List<RequiredAuthorisationPoint> RequiredRights
        {
            get
            {
                return new List<RequiredAuthorisationPoint>
                {
                };
            }
        }

        protected override void EntitySaved(SettingsQuoteUpload entity, HandlerResult<int> result)
        {
            result.Processed(entity.Id);
        }

        protected override SettingsQuoteUpload HandleCreation(CreateSettingsQuoteUploadDto dto, HandlerResult<int> result)
        {

            var entity = SettingsQuoteUpload.Create(dto);
            result.Processed(entity.Id);

            return entity;
        }
    }
}